Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Fees_FEEConcessionTrans
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Property ACD_ID() As Integer
        Get
            Return ViewState("ACD_ID")
        End Get
        Set(ByVal value As Integer)
            ViewState("ACD_ID") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Not IsPostBack Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            hylReviseNo.Visible = False
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            ACD_ID = 0
            lblAcdYear.Text = ""
            fillVATCodes()
            SET_VAT_DROPDOWN_RIGHT()
            txtTotalFee.Attributes.Add("ReadOnly", "ReadOnly")
            gvFeeDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvMonthly.Attributes.Add("bordercolor", "#1b80b6")
            gvHistory.Attributes.Add("bordercolor", "#1b80b6")
            txtRefHEAD.Attributes.Add("ReadOnly", "ReadOnly")
            txtConcession_Head.Attributes.Add("ReadOnly", "ReadOnly")

            gvFeeDetails.DataBind()
            gvMonthly.DataBind()

            Dim CurBsUnit As String = Session("sBsuid")
            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CONCESSION_TRANS And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CONCESSION_APPROVAL And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CONCESSION_CANCEL_APPROVAL) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                radPercentage.Checked = True
                'ddlAcademicYear.DataSource = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
                'ddlAcademicYear.DataTextField = "ACY_DESCR"
                'ddlAcademicYear.DataValueField = "ACD_ID"
                'ddlAcademicYear.DataBind()
                'ddlAcademicYear.SelectedIndex = -1
                'ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                'UsrSelStudent1.ACD_ID = Session("Current_ACD_ID")
                'BindFee()
                'setAcademicyearDate()
            End If
            If ViewState("datamode") = "view" Then
                Dim FCH_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FCH_ID").Replace(" ", "+"))
                'Added by Athira 23/sept
                ViewState("FCH_ID") = FCH_ID
                Dim vFEE_CON As FEEConcessionTransaction ' Acd_is is tracked from fch_id so no need to pass acd_id
                vFEE_CON = FEEConcessionTransaction.GetDetails(FCH_ID, 2, 0, Session("sBSUID"), False)
                If Not vFEE_CON Is Nothing Then
                    uscStudentPicker1.LoadStudentsByID(vFEE_CON.FCH_STU_ID)
                    Session("FEE_CONS_TRAN") = vFEE_CON
                    txtDate.Text = Format(vFEE_CON.FCH_DT, OASISConstants.DateFormat)
                    txtFromDT.Text = Format(vFEE_CON.FCH_DTFROM, OASISConstants.DateFormat)
                    txtToDT.Text = Format(vFEE_CON.FCH_DTTO, OASISConstants.DateFormat)
                    h_FCM_ID_HEAD.Value = vFEE_CON.FCH_FCM_ID
                    txtRefHEAD.Text = vFEE_CON.FCH_REF_NAME
                    H_REFID_HEAD.Value = vFEE_CON.FCH_REF_ID
                    h_FCM_ID_HEAD.Value = vFEE_CON.FCH_FCM_ID
                    'H_REFID_SUB.Value = vFEE_CON.FCH_EMP_ID
                    'labGrade.Text = "Grade :" & vFEE_CON.GRM_DISPLAY
                    hylReviseNo.Text = "Revise RefNo : " & vFEE_CON.CANC_RECNO
                    chkNextAcdConc.Checked = vFEE_CON.NextAcd_Concession
                    ACD_ID = vFEE_CON.FCH_ACD_ID




                    If vFEE_CON.FCH_FCH_ID <> 0 Then
                        hylReviseNo.Visible = True
                        hylReviseNo.NavigateUrl = "FeeConcessionCancellation.aspx?FCH_ID=" & Encr_decrData.Encrypt(vFEE_CON.FCH_FCH_ID) &
                                      "&MainMnu_code=" & Encr_decrData.Encrypt(OASISConstants.MNU_FEE_CONCESSION_CANCEL_APPROVAL) & "&datamode=" & Encr_decrData.Encrypt("view")
                    Else
                        hylReviseNo.Visible = False
                    End If

                    'h_FCH_ID.Value = vFEE_CON.FCH_ID
                    txtConcession_Head.Text = UtilityObj.GetDataFromSQL("SELECT FCM_DESCR FROM " _
                    & " FEES.FEE_CONCESSION_M WITH ( NOLOCK ) WHERE FCM_ID =" & h_FCM_ID_HEAD.Value, ConnectionManger.GetOASIS_FEESConnectionString)
                    h_FCT_ID_HEAD.Value = UtilityObj.GetDataFromSQL("SELECT FCM_FCT_ID FROM " _
                    & " FEES.FEE_CONCESSION_M WITH ( NOLOCK ) WHERE FCM_ID =" & h_FCM_ID_HEAD.Value, ConnectionManger.GetOASIS_FEESConnectionString)


                    txtRefHEAD.Text = vFEE_CON.FCH_REF_NAME
                    txtRefHEAD.Text = FEEConcessionTransaction.GetReference(h_FCT_ID_HEAD.Value, vFEE_CON.FCH_REF_ID, Session("sBsuid"))
                    'UsrSelStudent1.IsStudent = True
                    'UsrSelStudent1.SetStudentDetails(vFEE_CON.FCH_STU_ID)

                    'Added By Nikunj (14-June-2020)
                    Session("liSelectedSiblingStudent") = vFEE_CON.FCH_STU_SIBLING_ID
                    chkIsDifferentSchool.Checked = vFEE_CON.FCH_IS_DIFFERENT_S_B
                    Display_Student_Details(H_REFID_HEAD.Value, h_FCT_ID_HEAD.Value)


                    txtRemarks.Text = vFEE_CON.FCH_REMARKS
                    h_STUD_ID.Value = vFEE_CON.FCH_STU_ID
                    'If Not ddlAcademicYear.Items.FindByValue(vFEE_CON.FCH_ACD_ID) Is Nothing Then
                    '    ddlAcademicYear.SelectedIndex = -1
                    '    ddlAcademicYear.Items.FindByValue(vFEE_CON.FCH_ACD_ID).Selected = True
                    'End If
                    Set_Collection()
                    SetStudentDetails()
                    GridBind()
                    DissableControls(True)
                    If GetStatus(FCH_ID) <> "" Then
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), "2", ViewState("datamode"))
                    End If
                End If

            ElseIf ViewState("datamode") = "add" Then
                pnlView.Enabled = True
                If Not Request.QueryString("SID") Is Nothing Then
                    h_STUD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("SID").Replace(" ", "+"))
                    uscStudentPicker1.LoadStudentsByID(h_STUD_ID.Value)
                End If
                Session("FEE_CONS_TRAN") = Nothing
            End If
            bindHistory()
            lblAlert.Text = getErrorMessage("642")
            lblAlert.CssClass = "alert alert-info"
            HIDE_TAX_FIELDS()
        End If
    End Sub
    Protected Sub btn_GridLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_GridLoad.Click
        Display_Student_Details(H_REFID_HEAD.Value, h_FCT_ID_HEAD.Value)
    End Sub
    'Added By Nikunj (14-June-2020)
    Public Sub Display_Student_Details(ByVal CommonID As String, ByVal refType As ConcessionType)
        Grid_Sibling.Visible = False
        Grid_Sibling.DataSource = Nothing
        Grid_Sibling.DataBind()
        txtRefHEAD.Visible = True

        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@COMMON_ID", SqlDbType.BigInt)
        pParms(0).Value = CommonID
        pParms(1) = New SqlClient.SqlParameter("@REFTYPE", SqlDbType.Int)
        pParms(1).Value = refType
        Dim dsData As New DataSet
        dsData = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, CommandType.StoredProcedure, "[FEES].[F_GET_DETAILS_OF_STUD_EMP]", pParms)
        If Not dsData Is Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
            Grid_Sibling.Visible = True
            Grid_Sibling.DataSource = dsData
            Grid_Sibling.DataBind()

            txtRefHEAD.Visible = False
        End If

    End Sub
    Private Sub SET_VAT_DROPDOWN_RIGHT()
        Dim bHasRight As Boolean = Convert.ToBoolean(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CASE WHEN COUNT(USER_NAME)>0 THEN 1 ELSE 0 END AS bEXIST FROM OASIS.TAX.VW_VAT_ENABLE_USERS WHERE USER_NAME='" & Session("sUsr_name").ToString & "' AND USER_SOURCE='FEES'"))
        If Not bHasRight Then
            ddlTAX.Enabled = False
        Else
            ddlTAX.Enabled = True
        End If
        Dim bHasRight2 As Boolean = Convert.ToBoolean(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BSU_bVATEnabled,0) BSU_bVATEnabled FROM OASIS..BUSINESSUNIT_M WITH (NOLOCK) WHERE BSU_ID='" & Session("sBsuId").ToString & "'"))
        If Not bHasRight2 Then
            'ddlTAX.Enabled = False
            rblTaxCalculation.Enabled = False
        Else
            'ddlTAX.Enabled = True
            rblTaxCalculation.Enabled = True
        End If
    End Sub
    Private Sub CalculateVAT(ByVal FEEID As Integer, ByVal Amount As Double, ByVal txDate As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXCodeAndAmount('FEES','" & Session("sBsuId") & "','FEE','" & FEEID & "','" & txDate & "'," & Amount & ",'')")
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ddlTAX.SelectedValue = ds.Tables(0).Rows(0)("TAX_CODE").ToString

        End If

    End Sub
    Private Sub CALCULATE_TAX(ByRef Amount As TextBox, ByRef lblTAX As Label, ByRef lblNet As Label, ByVal IsInclusiveTax As Integer)
        If IsNumeric(Amount.Text) Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount_Ext(" & Amount.Text & ",'" & ddlTAX.SelectedValue & "'," & IsInclusiveTax & ")")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblTAX.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
                If IsInclusiveTax Then
                    Amount.Text = (Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT")) - Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT"))).ToString

                End If
                lblNet.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
            End If
        End If
    End Sub
    Protected Sub txtDAmount_TextChanged(sender As Object, e As EventArgs) 'Handles txtAmount.TextChanged
        Try
            Dim gvr As GridViewRow = sender.parent.parent
            Dim txtDAmount As New TextBox, lblTAXAmount As New Label, lblNETTotal As New Label, dblTotal As Double = 0
            txtDAmount = gvr.FindControl("txtAmount")
            lblTAXAmount = gvr.FindControl("lblTaxAmount")
            lblNETTotal = gvr.FindControl("lblNetAmount")
            CALCULATE_TAX(txtDAmount, lblTAXAmount, lblNETTotal, IIf(rblTaxCalculation.SelectedValue = "I", 1, 0))
            For Each gvro As GridViewRow In gvMonthly.Rows
                Dim txtDAmount2 As TextBox = gvro.FindControl("txtAmount")
                Dim lblNETTotal2 As Label = gvro.FindControl("lblNetAmount")
                dblTotal += CDbl(lblNETTotal2.Text)
            Next
        Catch ex As Exception

        End Try


    End Sub

    Protected Sub ddlTAX_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTAX.SelectedIndexChanged
        If gvMonthly.Rows.Count > 0 Then
            Dim txtDAmount As New TextBox, lblTAXAmount As New Label, lblNETTotal As New Label, dblGridTotal As Double = 0
            For Each gvr As GridViewRow In gvMonthly.Rows
                txtDAmount = gvr.FindControl("txtAmount")
                lblTAXAmount = gvr.FindControl("lblTaxAmount")
                lblNETTotal = gvr.FindControl("lblNetAmount")
                dblGridTotal += CDbl(txtDAmount.Text)
                CALCULATE_TAX(txtDAmount, lblTAXAmount, lblNETTotal, IIf(rblTaxCalculation.SelectedValue = "I", 1, 0))
            Next

        End If
    End Sub
    Sub fillVATCodes()
        Dim dtACD As DataTable = GetTAXCode()
        ddlTAX.DataSource = dtACD
        ddlTAX.DataTextField = "TAX_DESCR"
        ddlTAX.DataValueField = "TAX_CODE"
        ddlTAX.DataBind()


    End Sub
    Function GetTAXCode() As DataTable
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        Dim dsData As New DataSet
        dsData = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.[GET_TAX_CODES]", pParms)


        'Dim sql_query As String = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES ORDER BY TAX_ID "
        'Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        'CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If

    End Function
    'Protected Sub rblTaxCalculation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblTaxCalculation.SelectedIndexChanged
    '    'AutoFill()
    '    'If gvMonthly.Rows.Count > 0 Then
    '    '    Dim txtDAmount As New TextBox, lblTAXAmount As New Label, lblNETTotal As New Label, dblGridTotal As Double = 0
    '    '    For Each gvr As GridViewRow In gvMonthly.Rows
    '    '        txtDAmount = gvr.FindControl("txtAmount")
    '    '        lblTAXAmount = gvr.FindControl("lblTaxAmount")
    '    '        lblNETTotal = gvr.FindControl("lblNetAmount")
    '    '        dblGridTotal += CDbl(txtDAmount.Text)
    '    '        CALCULATE_TAX(txtDAmount, lblTAXAmount, lblNETTotal, IIf(rblTaxCalculation.SelectedValue = "I", 1, 0))
    '    '    Next

    '    'End If
    'End Sub
    Protected Sub ddlFeeType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFeeType.SelectedIndexChanged
        Set_Collection()
        CalculateVAT(ddlFeeType.SelectedValue, 0, Now.Date)
        ddlTAX_SelectedIndexChanged(Nothing, Nothing)
    End Sub


    Private Sub HIDE_TAX_FIELDS() 'hide tax related fields if the unit is not TAX enabled
        Dim bShow As Boolean = False
        If Not Session("BSU_bVATEnabled") Is Nothing AndAlso IsNumeric(Session("BSU_bVATEnabled")) Then
            bShow = Convert.ToBoolean(Session("BSU_bVATEnabled"))
        End If
        tr_Message.Visible = bShow
    End Sub
    Function GetStatus(ByVal FPHID As String) As String
        GetStatus = ""
        Dim Qry As String = "SELECT ISNULL(FCH_REJTDBY,'')FCH_REJTDBY, ISNULL(FCH_bPOSTED,0)FCH_bPOSTED FROM FEES.FEE_CONCESSION_H WITH ( NOLOCK ) WHERE FCH_ID=" & FPHID & ""
        Dim dtStatus As DataTable = Mainclass.getDataTable(Qry, ConnectionManger.GetOASIS_FEESConnectionString)
        If Not dtStatus Is Nothing AndAlso dtStatus.Rows.Count > 0 Then
            If dtStatus.Rows(0)("FCH_REJTDBY").ToString.Trim <> "" Then 'Rejected
                GetStatus = "REJECTED"
            ElseIf dtStatus.Rows(0)("FCH_bPOSTED") Then 'Posted
                GetStatus = "POSTED"
            End If
        End If

    End Function

    Sub BindFee()
        ddlFeeType.DataSource = FEEConcessionTransaction.GetConcessionFEEType(Session("sBsuid"), ACD_ID)
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()
        'CalculateVAT(ddlFeeType.SelectedValue, 0, Now.Date)
    End Sub

    Sub EnabeYN(ByVal LockYN As Boolean)
        txtFromDT.Enabled = LockYN
        txtToDT.Enabled = LockYN
    End Sub

    Private Sub DissableControls(ByVal dissable As Boolean)
        txtDate.Enabled = Not dissable
        txtFromDT.Enabled = Not dissable
        txtToDT.Enabled = Not dissable
        txtRefHEAD.ReadOnly = dissable
        txtRemarks.ReadOnly = dissable
        txtAmount.ReadOnly = dissable

        'UsrSelStudent1.IsReadOnly = dissable
        uscStudentPicker1.IsReadonly = dissable
        Image1.Enabled = Not dissable
        imgRefHead.Enabled = Not dissable
        imgToDT.Enabled = Not dissable
        ImagefromDate.Enabled = Not dissable

        'ddlAcademicYear.Enabled = Not dissable
        ddlFeeType.Enabled = Not dissable
        radAmount.Enabled = Not dissable
        radPercentage.Enabled = Not dissable
        gvFeeDetails.Columns(9).Visible = Not dissable
        btnDetAdd.Enabled = Not dissable
        btnDetCancel.Enabled = Not dissable

    End Sub

    Protected Sub btnDetAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetAdd.Click
        If ViewState("datamode") <> "edit" Then
            If FEEConcessionTransaction.CheckPreviousConcession(h_FCT_ID_HEAD.Value, ACD_ID,
                    Session("sBsuid"), h_STUD_ID.Value) Then
                Me.programmaticModalPopup.Show()
            End If
        End If

        Select Case h_FCT_ID_HEAD.Value
            Case 1, 2
                If H_REFID_HEAD.Value = "" Then

                    'lblError.Text = "Please specify the Reference No"
                    usrMessageBar.ShowNotification("Please specify the Reference No", UserControls_usrMessageBar.WarningType.Danger)
                    Return
                ElseIf Not FEEConcessionTransaction.ValidateReference(h_FCT_ID_HEAD.Value, H_REFID_HEAD.Value, Session("sBSUID")) Then

                    'lblError.Text = "The Reference No is not Valid "
                    usrMessageBar.ShowNotification("The Reference No is not Valid", UserControls_usrMessageBar.WarningType.Danger)
                    Return
                Else
                    'lblError.Text = ""
                    'usrMessageBar.ShowNotification("", UserControls_usrMessageBar.WarningType.Information)
                End If
                If ViewState("datamode") <> "edit" Then
                    If AlertForRefRepetition(h_FCT_ID_HEAD.Value, H_REFID_HEAD.Value) Then
                        Me.programmaticModalPopup.Show()
                    End If
                End If
        End Select
        Dim vFEE_CON As FEEConcessionTransaction
        vFEE_CON = Session("FEE_CONS_TRAN")
        If vFEE_CON Is Nothing Then
            vFEE_CON = New FEEConcessionTransaction
        End If
        Dim str_Stu_Join_Date As String = FeeCommon.GetStudentJoinDate(Session("sBsuid"), h_STUD_ID.Value, False)
        Dim dt_Join_Date, dt_From_Date As DateTime

        If IsDate(txtDate.Text) Then
            dt_From_Date = CDate(txtDate.Text)
        Else
            'lblError.Text = "Invalid document date!!!"
            usrMessageBar.ShowNotification("Invalid document date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        If IsDate(str_Stu_Join_Date) Then
            dt_Join_Date = CDate(str_Stu_Join_Date)
            'If dt_From_Date < dt_Join_Date Then
            '    lblError.Text = "Document date is less than Student join date!!!"
            '    Exit Sub
            'End If
        Else
            'lblError.Text = "Invalid Student Join Date!!!"
            usrMessageBar.ShowNotification("Invalid Student Join Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        Dim amttot As Double
        Dim taxtot As Double
        For Each gvr As GridViewRow In gvMonthly.Rows
            'Get a programmatic reference to the CheckBox control
            Dim lblId As Label = CType(gvr.FindControl("lblId"), Label)
            Dim lblActualAmount As Label = CType(gvr.FindControl("lblActualAmount"), Label)

            Dim txtChargeDate As TextBox = CType(gvr.FindControl("txtChargeDate"), TextBox)
            Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)
            Dim lblNetAmount As Label = CType(gvr.FindControl("lblNetAmount"), Label)
            Dim lblTaxAmount As Label = CType(gvr.FindControl("lblTaxAmount"), Label)
            amttot = amttot + FeeCollection.GetDoubleVal(lblNetAmount.Text)
            taxtot = taxtot + FeeCollection.GetDoubleVal(lblTaxAmount.Text)
            Dim dblActualAmount As Decimal = lblActualAmount.Text
            If Not txtAmount Is Nothing And Not txtAmount.Text = 0 Then
                If Not IsNumeric(txtAmount.Text) Or Not IsDate(txtChargeDate.Text) Then
                    'lblError.Text = "Invalid Amount/Date!!!"
                    usrMessageBar.ShowNotification("Invalid Amount/Date!!!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                Else
                    If dblActualAmount < Convert.ToDecimal(txtAmount.Text) Then
                        'lblError.Text = "Concession exceeds actual amount !!!"
                        usrMessageBar.ShowNotification("Concession exceeds actual amount !!!", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                End If
            Else
                usrMessageBar.ShowNotification("Please enter a valid concession amount. Concession amount should be greater than 0", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
        Next

        If radAmount.Checked Then
            txtAmount.Text = amttot
        ElseIf radPercentage.Checked Then
            'Nothing
        End If
        'txtAmount.Text = amttot
        lbl_tot_netamount.Text = amttot
        lbl_tot_taxamount.Text = taxtot
        If Not FEEConcessionTransaction.DuplicateFeeType(vFEE_CON.SUB_DETAILS, ddlFeeType.SelectedValue, Session("FEE_CON_EID")) Then
            If btnDetAdd.Text = "Update" Then
                vFEE_CON.SUB_DETAILS = EditSubDetails(Session("FEE_CON_EID"), vFEE_CON.SUB_DETAILS)
                btnDetAdd.Text = "Add"
                Session("FEE_CONS_TRAN") = vFEE_CON
                Session("FEE_CON_EID") = Nothing
            Else
                AddDetails()
                Set_Collection()
            End If
            ClearSubDetails()
        Else
            'lblError.Text = "Cannot repeat Fee Concession Type"
            usrMessageBar.ShowNotification("Cannot repeat Fee Concession Type", UserControls_usrMessageBar.WarningType.Danger)
            Return
        End If

        GridBind()
    End Sub

    Private Function EditSubDetails(ByVal EID As Integer, ByVal arrSUBList As ArrayList) As ArrayList
        If arrSUBList Is Nothing Then
            Return New ArrayList
        End If
        Dim vFEE_CON_SUB As FEE_CONC_TRANC_SUB
        For i As Integer = 0 To arrSUBList.Count - 1
            vFEE_CON_SUB = arrSUBList(i)
            If vFEE_CON_SUB.FCD_ID = EID Then
                EditSubDetails_Monthly(vFEE_CON_SUB.SubList_Monthly, vFEE_CON_SUB.FCD_ACTUALAMT, vFEE_CON_SUB.FCD_TAX_AMOUNT, vFEE_CON_SUB.FCD_NET_AMOUNT)
                arrSUBList.Remove(vFEE_CON_SUB)
                vFEE_CON_SUB.FCD_AMOUNT = CDbl(txtAmount.Text)
                If radAmount.Checked Then
                    vFEE_CON_SUB.FCD_AMTTYPE = 1
                ElseIf radPercentage.Checked Then
                    vFEE_CON_SUB.FCD_AMTTYPE = 2
                End If
                vFEE_CON_SUB.FCD_ISINCLUSIVE_TAX = IIf(rblTaxCalculation.SelectedValue = "I", 1, 0)
                vFEE_CON_SUB.FCD_TAX_CODE = ddlTAX.SelectedValue

                vFEE_CON_SUB.FCD_ID = EID
                vFEE_CON_SUB.FCD_FCM_ID = h_FCM_ID_HEAD.Value
                vFEE_CON_SUB.FCD_FEE_ID = ddlFeeType.SelectedValue
                If txtRefHEAD.Text <> "" Then vFEE_CON_SUB.FCD_REF_ID = CInt(H_REFID_HEAD.Value) _
                Else vFEE_CON_SUB.FCD_REF_ID = 0
                vFEE_CON_SUB.FEE_DESCR = ddlFeeType.SelectedItem.Text
                vFEE_CON_SUB.FCM_DESCR = txtConcession_Head.Text
                arrSUBList.Add(vFEE_CON_SUB)
                EditSubDetails_Monthly(vFEE_CON_SUB.SubList_Monthly, vFEE_CON_SUB.FCD_ACTUALAMT, vFEE_CON_SUB.FCD_TAX_AMOUNT, vFEE_CON_SUB.FCD_NET_AMOUNT)
                Exit For
            End If
        Next
        Return arrSUBList
    End Function

    Private Function EditSubDetails_Monthly(ByVal arrSUBListMonthly As ArrayList, ByRef AMT As Double, ByRef TAXAMT As Double, ByRef NETAMT As Double) As ArrayList
        If arrSUBListMonthly Is Nothing Then
            Return New ArrayList
        End If
        AMT = 0
        TAXAMT = 0
        NETAMT = 0
        Dim vFEE_CON_SUB_Monthly As FEE_CONC_TRANC_SUB_MONTHLY
        For i As Integer = 0 To arrSUBListMonthly.Count - 1
            vFEE_CON_SUB_Monthly = arrSUBListMonthly(i)
            vFEE_CON_SUB_Monthly.FMD_bDelete = True
        Next

        For Each gvr As GridViewRow In gvMonthly.Rows
            'Get a programmatic reference to the CheckBox control
            Dim lblId As Label = CType(gvr.FindControl("lblId"), Label)
            Dim lblActualAmount As Label = CType(gvr.FindControl("lblActualAmount"), Label)
            Dim txtChargeDate As TextBox = CType(gvr.FindControl("txtChargeDate"), TextBox)
            Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)
            Dim lblTaxAmount As Label = CType(gvr.FindControl("lblTaxAmount"), Label)
            Dim lblNetAmount As Label = CType(gvr.FindControl("lblNetAmount"), Label)

            If Not txtAmount Is Nothing Then
                If Not IsNumeric(txtAmount.Text) Or Not IsDate(txtChargeDate.Text) Then
                    'lblError.Text = "Invalid Amount/Date!!!"
                    usrMessageBar.ShowNotification("Invalid Amount/Date!!!", UserControls_usrMessageBar.WarningType.Danger)
                    Return arrSUBListMonthly
                End If

                For i As Integer = 0 To arrSUBListMonthly.Count - 1
                    vFEE_CON_SUB_Monthly = arrSUBListMonthly(i)
                    If lblId.Text = vFEE_CON_SUB_Monthly.FMD_REF_ID Then
                        vFEE_CON_SUB_Monthly.FMD_AMOUNT = CDbl(txtAmount.Text)
                        vFEE_CON_SUB_Monthly.FMD_TAXAMOUNT = CDbl(lblTaxAmount.Text)
                        vFEE_CON_SUB_Monthly.FMD_NETAMOUNT = CDbl(lblNetAmount.Text)
                        vFEE_CON_SUB_Monthly.FMD_ORG_AMOUNT = CDbl(lblActualAmount.Text)
                        vFEE_CON_SUB_Monthly.FMD_DATE = txtChargeDate.Text
                        AMT += vFEE_CON_SUB_Monthly.FMD_AMOUNT
                        TAXAMT += vFEE_CON_SUB_Monthly.FMD_TAXAMOUNT
                        NETAMT += vFEE_CON_SUB_Monthly.FMD_NETAMOUNT
                        vFEE_CON_SUB_Monthly.FMD_bDelete = False
                        Exit For
                    End If
                Next
            End If
        Next
        Return arrSUBListMonthly
    End Function

    Private Function AlertForRefRepetition(ByVal REF_TYP As ConcessionType, ByVal REF_ID As Integer) As Boolean
        Return FEEConcessionTransaction.RefRepetition(REF_TYP, REF_ID)
    End Function

    Private Sub GridBind()
        Dim vFEE_CON As FEEConcessionTransaction = Session("FEE_CONS_TRAN")
        Dim dttab As New DataTable
        If Not vFEE_CON Is Nothing Then
            dttab = GetDataTableFromObjects(vFEE_CON.SUB_DETAILS)
        End If
        gvFeeDetails.DataSource = dttab
        gvFeeDetails.DataBind()
        If dttab.Rows.Count > 1 Then
            EnabeYN(False)
        Else
            EnabeYN(True)
        End If
    End Sub

    Private Function GetDataTableFromObjects(ByVal arrList As ArrayList) As DataTable
        If arrList Is Nothing Then Return Nothing
        Dim dtDt As DataTable = CreateDataTableForFeeDetails()
        For i As Integer = 0 To arrList.Count - 1
            Dim vFEE_CON As FEE_CONC_TRANC_SUB = arrList(i)
            Dim dr As DataRow = dtDt.NewRow()
            dr("FCD_ID") = vFEE_CON.FCD_ID
            dr("FCM_DESCR") = vFEE_CON.FCM_DESCR
            dr("FEE_DESCR") = vFEE_CON.FEE_DESCR
            dr("AMT_TYPE") = IIf(vFEE_CON.FCD_AMTTYPE = 1, "Amount", "Percentage")
            If vFEE_CON.FCD_AMTTYPE = 1 Then
                dr("PERCENTAGE") = "N/A"
                dr("AMOUNT") = AccountFunctions.Round(vFEE_CON.FCD_AMOUNT)
                dr("TAXAMOUNT") = AccountFunctions.Round(vFEE_CON.FCD_TAX_AMOUNT)
                dr("NETAMOUNT") = AccountFunctions.Round(vFEE_CON.FCD_NET_AMOUNT)
            Else
                dr("PERCENTAGE") = AccountFunctions.Round(vFEE_CON.FCD_AMOUNT)
                dr("AMOUNT") = AccountFunctions.Round(vFEE_CON.FCD_ACTUALAMT)
                dr("TAXAMOUNT") = AccountFunctions.Round(vFEE_CON.FCD_TAX_AMOUNT)
                dr("NETAMOUNT") = AccountFunctions.Round(vFEE_CON.FCD_NET_AMOUNT)
            End If
            dtDt.Rows.Add(dr)
        Next
        Return dtDt
    End Function

    Private Function CreateDataTableForFeeDetails() As DataTable
        Dim dtDt As New DataTable()
        Dim dcFCD_ID As New DataColumn("FCD_ID", System.Type.[GetType]("System.Int32"))
        Dim dcFCM_DESCR As New DataColumn("FCM_DESCR", System.Type.[GetType]("System.String"))
        Dim dcFEE_DESCR As New DataColumn("FEE_DESCR", System.Type.[GetType]("System.String"))
        Dim dcAMT_TYPE As New DataColumn("AMT_TYPE", System.Type.[GetType]("System.String"))
        Dim dcPERCENTAGE As New DataColumn("PERCENTAGE", System.Type.[GetType]("System.String"))
        Dim dcAMOUNT As New DataColumn("AMOUNT", System.Type.[GetType]("System.Decimal"))
        Dim dcTAXAMOUNT As New DataColumn("TAXAMOUNT", System.Type.[GetType]("System.Decimal"))
        Dim dcNETAMOUNT As New DataColumn("NETAMOUNT", System.Type.[GetType]("System.Decimal"))

        dtDt.Columns.Add(dcFCD_ID)
        dtDt.Columns.Add(dcFCM_DESCR)
        dtDt.Columns.Add(dcFEE_DESCR)
        dtDt.Columns.Add(dcAMT_TYPE)
        dtDt.Columns.Add(dcPERCENTAGE)
        dtDt.Columns.Add(dcAMOUNT)
        dtDt.Columns.Add(dcTAXAMOUNT)
        dtDt.Columns.Add(dcNETAMOUNT)
        Return dtDt
    End Function

    Private Sub AddDetails()
        If Session("FEE_CONS_TRAN") Is Nothing Then
            Session("FEE_CONS_TRAN") = New FEEConcessionTransaction
        End If
        Dim vFEE_CON As FEEConcessionTransaction = Session("FEE_CONS_TRAN")
        Dim vFEE_CON_SUB As New FEE_CONC_TRANC_SUB
        vFEE_CON_SUB.FCD_AMOUNT = CDbl(txtAmount.Text)

        If radAmount.Checked Then
            vFEE_CON_SUB.FCD_AMTTYPE = 1
        ElseIf radPercentage.Checked Then
            vFEE_CON_SUB.FCD_AMTTYPE = 2
        End If

        vFEE_CON_SUB.FCD_ISINCLUSIVE_TAX = IIf(rblTaxCalculation.SelectedValue = "I", 1, 0)
        vFEE_CON_SUB.FCD_TAX_CODE = ddlTAX.SelectedValue

        vFEE_CON_SUB.FCD_ID = GETNEXTFCD_ID(vFEE_CON.SUB_DETAILS)
        vFEE_CON_SUB.FCD_FCM_ID = h_FCM_ID_HEAD.Value
        vFEE_CON_SUB.FCD_FEE_ID = ddlFeeType.SelectedValue
        vFEE_CON_SUB.FCD_SCH_ID = ViewState("SCH_ID")
        If CInt(h_FCT_ID_HEAD.Value) = ConcessionType.Staff Then
            vFEE_CON_SUB.FCD_REF_BSU_ID = FEEConcessionTransaction.GetEmployeeBSU(H_REFID_HEAD.Value, H_REFID_HEAD.Value)
        End If

        If txtRefHEAD.Text <> "" Then vFEE_CON_SUB.FCD_REF_ID = CInt(H_REFID_HEAD.Value) _
        Else vFEE_CON_SUB.FCD_REF_ID = 0
        vFEE_CON_SUB.FEE_DESCR = ddlFeeType.SelectedItem.Text
        vFEE_CON_SUB.FCM_DESCR = txtConcession_Head.Text
        If (vFEE_CON.SUB_DETAILS Is Nothing) Then
            vFEE_CON.SUB_DETAILS = New ArrayList
        End If
        vFEE_CON_SUB = AddDetails_Monthly(vFEE_CON_SUB, vFEE_CON_SUB.FCD_ID, vFEE_CON_SUB.FCD_ACTUALAMT, vFEE_CON_SUB.FCD_TAX_AMOUNT, vFEE_CON_SUB.FCD_NET_AMOUNT)
        vFEE_CON.SUB_DETAILS.Add(vFEE_CON_SUB)
        Session("FEE_CONS_TRAN") = vFEE_CON
    End Sub

    Private Function AddDetails_Monthly(ByVal vFEE_CON_SUB As FEE_CONC_TRANC_SUB, ByVal vFMD_FCD_ID As Integer, ByRef PER_AMT As Double, ByRef TAX_AMT As Double, ByRef NET_AMT As Double) As FEE_CONC_TRANC_SUB
        PER_AMT = 0
        For Each gvr As GridViewRow In gvMonthly.Rows
            Dim vFEE_CON_Monthly As New FEE_CONC_TRANC_SUB_MONTHLY
            'Get a programmatic reference to the CheckBox control
            Dim lblId As Label = CType(gvr.FindControl("lblId"), Label)
            Dim lblActualAmount As Label = CType(gvr.FindControl("lblActualAmount"), Label)
            Dim lblProRateAmount As Label = CType(gvr.FindControl("lblProRateAmount"), Label)
            Dim txtChargeDate As TextBox = CType(gvr.FindControl("txtChargeDate"), TextBox)
            Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)
            Dim lblTaxAmount As Label = CType(gvr.FindControl("lblTaxAmount"), Label)
            Dim lblNetAmount As Label = CType(gvr.FindControl("lblNetAmount"), Label)
            If Not txtAmount Is Nothing Then
                vFEE_CON_Monthly.FMD_AMOUNT = Convert.ToDecimal(txtAmount.Text)
                vFEE_CON_Monthly.FMD_TAXAMOUNT = Convert.ToDecimal(lblTaxAmount.Text)
                vFEE_CON_Monthly.FMD_NETAMOUNT = Convert.ToDecimal(lblNetAmount.Text)
                vFEE_CON_Monthly.FMD_ORG_AMOUNT = CDbl(lblActualAmount.Text)
                vFEE_CON_Monthly.FMD_DATE = CDate(txtChargeDate.Text)
                vFEE_CON_Monthly.FMD_FCD_ID = vFMD_FCD_ID
                vFEE_CON_Monthly.FMD_REF_ID = lblId.Text
                vFEE_CON_Monthly.FMD_REF_NAME = gvr.Cells(0).Text
                vFEE_CON_Monthly.FMD_PRO_AMOUNT = CDbl(lblProRateAmount.Text)
                PER_AMT += vFEE_CON_Monthly.FMD_AMOUNT
                TAX_AMT += vFEE_CON_Monthly.FMD_TAXAMOUNT
                NET_AMT += vFEE_CON_Monthly.FMD_NETAMOUNT
            End If
            If vFEE_CON_SUB.SubList_Monthly Is Nothing Then
                vFEE_CON_SUB.SubList_Monthly = New ArrayList
            End If
            vFEE_CON_SUB.SubList_Monthly.Add(vFEE_CON_Monthly)
        Next
        Return vFEE_CON_SUB
    End Function

    Private Function GETNEXTFCD_ID(ByVal arrSUBList As ArrayList) As Integer
        Dim vFEE_CON_SUB As New FEE_CONC_TRANC_SUB
        Dim FCD_MAX_ID As Integer = 0
        If arrSUBList Is Nothing OrElse arrSUBList.Count <= 0 Then
            Return 1
        End If
        For i As Integer = 0 To arrSUBList.Count - 1
            vFEE_CON_SUB = arrSUBList(i)
            If vFEE_CON_SUB.FCD_ID > FCD_MAX_ID Then
                FCD_MAX_ID = vFEE_CON_SUB.FCD_ID
            End If
        Next
        Return FCD_MAX_ID + 1
    End Function

    Private Function GETNEXTFMD_ID(ByVal arrSUBList As ArrayList) As Integer
        Dim vFEE_CON_SUB_MONTHLY As New FEE_CONC_TRANC_SUB_MONTHLY
        Dim FCD_MAX_ID As Integer = 0
        If arrSUBList Is Nothing OrElse arrSUBList.Count <= 0 Then
            Return 1
        End If
        For i As Integer = 0 To arrSUBList.Count - 1
            vFEE_CON_SUB_MONTHLY = arrSUBList(i)
            If vFEE_CON_SUB_MONTHLY.FMD_ID > FCD_MAX_ID Then
                FCD_MAX_ID = vFEE_CON_SUB_MONTHLY.FMD_ID
            End If
        Next
        Return FCD_MAX_ID + 1
    End Function

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblFCD_ID As New Label
        lblFCD_ID = TryCast(sender.parent.FindControl("lblFCD_ID"), Label)
        Dim vFEE_CON As FEEConcessionTransaction = Session("FEE_CONS_TRAN")
        Dim vFEE_CON_SUB As FEE_CONC_TRANC_SUB
        Dim arrSUBDET As ArrayList = vFEE_CON.SUB_DETAILS

        For i As Integer = 0 To arrSUBDET.Count
            vFEE_CON_SUB = arrSUBDET(i)
            If vFEE_CON_SUB.FCD_ID = lblFCD_ID.Text Then

                ddlFeeType.SelectedIndex = -1
                ddlFeeType.Items.FindByValue(vFEE_CON_SUB.FCD_FEE_ID).Selected = True

                If vFEE_CON_SUB.FCD_ISINCLUSIVE_TAX = True Then
                    rblTaxCalculation.SelectedValue = "I"
                Else
                    rblTaxCalculation.SelectedValue = "E"
                End If

                'ddlTAX.SelectedValue = vFEE_CON_SUB.FCD_TAX_CODE
                If vFEE_CON_SUB.FCD_TAX_CODE.ToString = "" Then
                    CalculateVAT(ddlFeeType.SelectedValue, 0, Now.Date)
                End If



                GetMonthlyList(vFEE_CON_SUB.SubList_Monthly)
                Session("FEE_CON_EID") = lblFCD_ID.Text

                txtAmount.Text = vFEE_CON_SUB.FCD_AMOUNT

                H_REFID_HEAD.Value = vFEE_CON_SUB.FCD_REF_ID

                txtConcession_Head.Text = UtilityObj.GetDataFromSQL("SELECT FCM_DESCR FROM " _
              & " FEES.FEE_CONCESSION_M WHERE FCM_ID =" & h_FCM_ID_HEAD.Value, ConnectionManger.GetOASIS_FEESConnectionString)
                h_FCT_ID_HEAD.Value = UtilityObj.GetDataFromSQL("SELECT FCM_FCT_ID FROM " _
                          & " FEES.FEE_CONCESSION_M WHERE FCM_ID =" & h_FCM_ID_HEAD.Value, ConnectionManger.GetOASIS_FEESConnectionString)
                h_FCM_ID_HEAD.Value = vFEE_CON_SUB.FCD_FCM_ID
                txtRefHEAD.Text = FEEConcessionTransaction.GetReference(h_FCT_ID_HEAD.Value, vFEE_CON_SUB.FCD_REF_ID, Session("sBsuid"))

                Select Case vFEE_CON_SUB.FCD_AMTTYPE
                    Case 1
                        radAmount.Checked = True
                        radPercentage.Checked = False
                    Case 2
                        radAmount.Checked = False
                        radPercentage.Checked = True
                End Select
                btnDetAdd.Text = "Update"
                txtFromDT.Enabled = True
                txtToDT.Enabled = True
                If ViewState("datamode") <> "edit" Then
                    Set_Collection()
                End If
                EnabeYN(False)
                Exit For
            End If
        Next
        If ViewState("datamode") <> "edit" Then
            'Set_Collection()
            AutoFill()
        End If
    End Sub

    Private Sub GetMonthlyList(ByVal arrSubList_Monthly As ArrayList)
        If ViewState("datamode") <> "edit" Then
            SetStudentDetails()
        End If
        Dim FromDT As Date = txtFromDT.Text
        Dim dtTermorMonths As New DataTable
        Dim dtNew As DataTable
        dtNew = FEE_CONC_TRANC_SUB_MONTHLY.CreateDataTableFeeConcessionMonthly
        Dim STR_SCH_ID As Integer
        Dim TOTALAMOUNT As Decimal = 0
        dtTermorMonths = FeeCommon.GetFee_MonthorTerm_OASIS(ddlFeeType.SelectedItem.Value,
                        ACD_ID, Session("sBSUID"), h_STUD_ID.Value,
                        txtFromDT.Text, txtToDT.Text, TOTALAMOUNT, STR_SCH_ID)
        For i As Integer = 0 To dtTermorMonths.Rows.Count - 1
            Dim dr As DataRow
            dr = dtNew.NewRow
            dr("id") = dtTermorMonths.Rows(i)("ID")
            dr("Descr") = dtTermorMonths.Rows(i)("descr")
            dr("FDD_DATE") = dtTermorMonths.Rows(i)("FDD_DATE")
            If ViewState("datamode") <> "edit" Then
                dr("FDD_AMOUNT") = dtTermorMonths.Rows(i)("FDD_AMOUNT")
            End If
            dr("CUR_AMOUNT") = 0
            dr("PRO_AMOUNT") = dtTermorMonths.Rows(i)("FDD_PROAMOUNT")
            dtNew.Rows.Add(dr)
        Next
        'txtTotalFee.Text = TOTALAMOUNT
        'Session("tempMonthly") = dtNew
        'gvMonthly.DataSource = dtNew
        'gvMonthly.DataBind()
        '----------------------------------------------------------
        Dim dt As New DataTable
        dt = FEE_CONC_TRANC_SUB_MONTHLY.CreateDataTableFeeConcessionMonthly
        Dim decTotalfee As Decimal = 0
        For Each vSubMonthly As FEE_CONC_TRANC_SUB_MONTHLY In arrSubList_Monthly
            Dim dt_temp As DataTable = Session("tempMonthly")
            Dim dr As DataRow
            dr = dt.NewRow
            dr("id") = vSubMonthly.FMD_REF_ID
            dr("DFlag") = True
            For Each row In dt_temp.Rows
                If (row("Id") = vSubMonthly.FMD_REF_ID) Then
                    dr("DFlag") = False
                    Exit For
                End If
            Next
            dr("Descr") = "" 'vSubMonthly.
            dr("FDD_DATE") = vSubMonthly.FMD_DATE
            dr("CUR_AMOUNT") = vSubMonthly.FMD_AMOUNT
            dr("TAX_AMOUNT") = vSubMonthly.FMD_TAXAMOUNT
            dr("NET_AMOUNT") = vSubMonthly.FMD_NETAMOUNT
            dr("FDD_AMOUNT") = vSubMonthly.FMD_ORG_AMOUNT
            dr("DESCR") = vSubMonthly.FMD_REF_NAME
            decTotalfee = decTotalfee + vSubMonthly.FMD_ORG_AMOUNT
            dr("PRO_AMOUNT") = vSubMonthly.FMD_PRO_AMOUNT
            If vSubMonthly.FMD_PRO_AMOUNT = 0 Then
                Dim dv As New DataView(dtNew)
                dv.RowFilter = "ID=" & vSubMonthly.FMD_REF_ID
                If dv.Count > 0 Then
                    dr("PRO_AMOUNT") = dv(0)("PRO_AMOUNT")
                Else
                    dr("PRO_AMOUNT") = vSubMonthly.FMD_ORG_AMOUNT
                End If
            End If

            'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT
            dt.Rows.Add(dr)
        Next
        txtTotalFee.Text = decTotalfee
        Dim dtView As DataView = New DataView(dt)
        dtView.RowFilter = "DFlag = False"
        gvMonthly.DataSource = dtView.ToTable
        Session("tempMonthly") = dtView.ToTable
        gvMonthly.DataBind()
    End Sub

    Protected Sub btnDetCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetCancel.Click
        ClearSubDetails()
    End Sub

    Private Sub ClearSubDetails()
        txtAmount.Text = ""
        txtTotalFee.Text = ""
        btnDetAdd.Text = "Add"
        Session("FEE_CON_EID") = Nothing
        gvMonthly.DataSource = Nothing
        gvMonthly.DataBind()
    End Sub

    Protected Sub lnkBtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblFCD_ID As New Label
        lblFCD_ID = TryCast(sender.parent.FindControl("lblFCD_ID"), Label)

        Dim vFEE_CON As FEEConcessionTransaction = Session("FEE_CONS_TRAN")
        Dim vFEE_CON_SUB As FEE_CONC_TRANC_SUB
        Dim arrSUBDET As ArrayList = vFEE_CON.SUB_DETAILS
        For i As Integer = 0 To arrSUBDET.Count
            vFEE_CON_SUB = arrSUBDET(i)
            If vFEE_CON_SUB.FCD_ID = lblFCD_ID.Text Then
                'vFEE_CON_SUB.bDelete = True
                vFEE_CON.SUB_DETAILS.Remove(vFEE_CON_SUB)
                Exit For
            End If
        Next
        GridBind()
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If gvFeeDetails.Rows.Count = 0 Then
            'lblError.Text = "Invalid Feedetails..!"
            usrMessageBar.ShowNotification("Invalid Feedetails..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim str_error As String = CheckErrors()
        Dim dtDocDate, dtFromDT, dtToDT As DateTime
        dtDocDate = CDate(txtDate.Text)
        dtFromDT = CDate(txtFromDT.Text)
        dtToDT = CDate(txtToDT.Text)
        If dtToDT < dtFromDT Then
            str_error = str_error & "From date is greater than to date<br />"
        End If
        If h_FCT_ID_HEAD.Value = "" Then
            str_error = str_error & "<br />Please select concession type"
        End If
        Select Case h_FCT_ID_HEAD.Value
            Case 1, 2
                If H_REFID_HEAD.Value = "" Then
                    str_error = str_error & "<br />Please specify the Reference No (Header)"
                ElseIf Not FEEConcessionTransaction.ValidateReference(h_FCT_ID_HEAD.Value, H_REFID_HEAD.Value, Session("sBsuid")) Then
                    str_error = str_error & "<br />The Reference No is not Valid (Header)"
                End If
        End Select
        If str_error <> "" Then
            'lblError.Text = "Please check the Following : <br>" & str_error
            usrMessageBar.ShowNotification("Please check the Following : <br>" & str_error, UserControls_usrMessageBar.WarningType.Information)
            Exit Sub
        End If
        Dim vFEE_CONS_TRAN As New FEEConcessionTransaction
        If Not Session("FEE_CONS_TRAN") Is Nothing Then
            vFEE_CONS_TRAN = Session("FEE_CONS_TRAN")
        End If
        If ViewState("datamode") = "edit" And Request.QueryString("FCH_ID") <> "" Then
            vFEE_CONS_TRAN.FCH_ID = Encr_decrData.Decrypt(Request.QueryString("FCH_ID").Replace(" ", "+"))
        Else
            vFEE_CONS_TRAN.FCH_ID = 0
        End If
        vFEE_CONS_TRAN.FCH_ACD_ID = ACD_ID
        vFEE_CONS_TRAN.FCH_BSU_ID = Session("sbsuid")
        vFEE_CONS_TRAN.FCH_DT = dtDocDate
        vFEE_CONS_TRAN.FCH_DTFROM = dtFromDT
        vFEE_CONS_TRAN.FCH_DTTO = dtToDT
        vFEE_CONS_TRAN.FCH_DRCR = "CR"
        vFEE_CONS_TRAN.FCH_FCM_ID = h_FCM_ID_HEAD.Value
        'vFEE_CONS_TRAN.FCH_ISINCLUSIVE_TAX = IIf(rblTaxCalculation.SelectedValue = "I", 1, 0)

        If txtRefHEAD.Text <> "" Then vFEE_CONS_TRAN.FCH_REF_ID = CInt(H_REFID_HEAD.Value) _
      Else vFEE_CONS_TRAN.FCH_REF_ID = 0
        ' vFEE_CONS_TRAN.FCH_REF_ID = txtRefID.Text
        vFEE_CONS_TRAN.FCH_REMARKS = txtRemarks.Text
        vFEE_CONS_TRAN.FCH_STU_ID = h_STUD_ID.Value

        'Added by Nikunj (14-June-2020)
        vFEE_CONS_TRAN.FCH_IS_DIFFERENT_S_B = chkIsDifferentSchool.Checked
        If Not Session("liSelectedSiblingStudent") Is Nothing Then
            vFEE_CONS_TRAN.FCH_STU_SIBLING_ID = Session("liSelectedSiblingStudent")
        Else
            vFEE_CONS_TRAN.FCH_STU_SIBLING_ID = ""
        End If


        vFEE_CONS_TRAN.STU_NAME = uscStudentPicker1.STU_NAME
        vFEE_CONS_TRAN.FCH_USER = CType(Session("sUsr_name"), String)
        vFEE_CONS_TRAN.FCH_ROUNDOFF = IIf(chk_rndoff.Checked = True, 1, 0)
        vFEE_CONS_TRAN.NextAcd_Concession = chkNextAcdConc.Checked
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            Dim trans As SqlTransaction
            trans = conn.BeginTransaction("FEE_CONCESSION_TRANS")
            Dim NEW_FCH_ID As String = String.Empty
            Dim retVal As Integer
            If ViewState("datamode") = "edit" Then
                retVal = FEEConcessionTransaction.DELETEFEE_CONCESSION_D(vFEE_CONS_TRAN.FCH_ID,
                Session("sBsuid"), Session("sUsr_name"), conn, trans)
            End If
            If retVal = 0 Then retVal = FEEConcessionTransaction.F_SaveFEE_CONCESSION_H(vFEE_CONS_TRAN,
                                NEW_FCH_ID, h_FCT_ID_HEAD.Value, conn, trans)
            If retVal <> 0 Then
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(retVal)
                usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                trans.Commit()
                ViewState("FCH_ID") = NEW_FCH_ID
                'lblError.Text = "Data updated Successfully"
                usrMessageBar.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                ' Session("FEE_CONS_TRAN") = Nothing
                ' ClearDetails()
                ViewState("datamode") = "view"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim str_KEY As String = "INSERT"
                If ViewState("datamode") = "edit" Then
                    str_KEY = "EDIT"
                End If
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, NEW_FCH_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page, txtRemarks.Text)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                'If ViewState("datamode") <> "view" Then
                '    PrintConcession(ViewState("FCH_ID"))
                'Else
                '    PrintConcession(Encr_decrData.Decrypt(Request.QueryString("FCH_ID").Replace(" ", "+")))
                'End If
            End If
        End Using
    End Sub

    Private Function CheckErrors() As String
        If Not FEEConcessionTransaction.PeriodBelongstoAcademicYear(CDate(txtFromDT.Text), CDate(txtToDT.Text), ACD_ID, Session("sBSUID")) Then
            Return "The date period doesnot belongs to current Academic Year"
        End If
        Return ""
    End Function

    Private Sub ClearDetails()
        setAcademicyearDate()
        txtConcession_Head.Text = ""
        txtRefHEAD.Text = ""
        txtRemarks.Text = ""
        txtAmount.Text = ""
        lblAcdYear.Text = ""
        txtTotalFee.Text = ""
        'labGrade.Text = ""
        'UsrSelStudent1.ClearDetails()
        uscStudentPicker1.ClearDetails()
        h_STUD_ID.Value = ""
        h_FCM_ID_HEAD.Value = ""
        h_FCT_ID_HEAD.Value = ""
        h_FCT_ID_HEAD.Value = ""
        H_REFID_HEAD.Value = ""
        txtRefHEAD.Text = ""

        chkCopyPrev.Checked = False
        radAmount.Checked = False
        radPercentage.Checked = True
        Session("FEE_CONS_TRAN") = Nothing
        GridBind()
        ClearSubDetails()
    End Sub

    Protected Sub chkCopyPrev_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCopyPrev.CheckedChanged
        Try
            Dim FEE_CON As FEEConcessionTransaction
            If h_STUD_ID.Value = "" Then Return
            'Dim prev_ACD_SEL_INDEX As Integer = ddlAcademicYear.SelectedIndex - 1
            Dim prev_ACD_ID As String = FEEConcessionTransaction.GET_PREVIOUS_ACADEMIC_YEAR(uscStudentPicker1.STU_ACD_ID) ' ddlAcademicYear.Items(prev_ACD_SEL_INDEX).Value
            FEE_CON = FEEConcessionTransaction.GetDetails(h_STUD_ID.Value, 1, prev_ACD_ID, Session("sBSUID"), False)
            FEE_CON.FCH_ACD_ID = uscStudentPicker1.STU_ACD_ID
            Session("FEE_CONS_TRAN") = FEE_CON
            GridBind()
        Catch
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        ClearDetails()
        Session("FEE_CONS_TRAN") = Nothing
        ClearSubDetails()
        DissableControls(False)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        pnlView.Enabled = True
        'lblError.Text = "Editing of concession disabled..."
        'Exit Sub
        ViewState("datamode") = "edit"
        Dim FEE_CON As FEEConcessionTransaction = Session("FEE_CONS_TRAN")
        FEE_CON.bEdit = True
        Session("FEE_CONS_TRAN") = FEE_CON
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        DissableControls(False)
        EnabeYN(False)
        'UsrSelStudent1.IsReadOnly = True
        uscStudentPicker1.IsReadonly = True
        txtDate.Text = Format(Now, "dd/MMM/yyyy")
        UtilityObj.beforeLoopingControls(Me.Page)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        If ViewState("datamode") = "add" Then
            Call ClearDetails()
            Session("FEE_CONS_TRAN") = Nothing
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            ViewState("datamode") = "none"
        ElseIf ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub imgRefHead_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgRefHead.Click
        If H_REFID_HEAD.Value <> "" Then
            If AlertForRefRepetition(h_FCM_ID_HEAD.Value, H_REFID_HEAD.Value) Then
                ViewState("CLICK_TYPE") = "HEAD"
                Me.programmaticModalPopup.Show()
            End If
        End If
    End Sub

    'Protected Sub ddlFeeType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFeeType.SelectedIndexChanged
    '    Set_Collection()
    'End Sub

    Sub SetStudentDetails()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString,
                       CommandType.Text, " FEES.GetStudentDetailsForConcession '" & h_STUD_ID.Value & "'")
        gvStudentDetails.DataSource = ds.Tables(0)
        gvStudentDetails.DataBind()
        If ds.Tables(1).Rows.Count > 0 Then
            ''''DATE
            Dim JoinDt As Date = ds.Tables(1).Rows(0)("STU_DOJ")
            If JoinDt > CDate(txtFromDT.Text) Then
                txtFromDT.Text = Format(JoinDt, OASISConstants.DateFormat)
            End If
            'If JoinDt > CDate(txtDate.Text) Then
            '    txtDate.Text = Format(JoinDt, OASISConstants.DateFormat)
            'End If
            ''''CONCESSION TYPE
            If Not ds.Tables(1).Rows(0)("FCM_ID") Is System.DBNull.Value AndAlso Not ds.Tables(1).Rows(0)("FCM_ID") Is Nothing Then
                If txtConcession_Head.Text = "" Then
                    txtConcession_Head.Text = ds.Tables(1).Rows(0)("FCM_DESCR")
                    h_FCM_ID_HEAD.Value = ds.Tables(1).Rows(0)("FCM_ID")
                    h_FCT_ID_HEAD.Value = ds.Tables(1).Rows(0)("FCT_ID")
                End If
            End If
            ''''REF ID
            If Not ds.Tables(1).Rows(0)("EMP_ID") Is System.DBNull.Value AndAlso ds.Tables(1).Rows(0)("EMP_ID") <> "" Then
                If txtRefHEAD.Text = "" Then
                    txtRefHEAD.Text = ds.Tables(1).Rows(0)("EMP_ID")
                    H_REFID_HEAD.Value = ds.Tables(1).Rows(0)("EMP_NAME")
                End If
            End If
            If ddlFeeType.SelectedValue = "6" Then 'if transport fees, added by jacob on 06/aug/2019
                If FeeCommon.GetStudentWithLocation_Transport(uscStudentPicker1.STU_ID, uscStudentPicker1.STU_NO, uscStudentPicker1.STU_NAME,
            Session("sBsuId"), True, H_Location.Value, txtLocation.Text, txtDate.Text, ACD_ID) Then
                    trTptArea.Visible = True
                    Dim FCH_ID As Integer = 0
                    If Request.QueryString("FCH_ID") <> "" Then
                        FCH_ID = Encr_decrData.Decrypt(Request.QueryString("FCH_ID").Replace(" ", "+"))
                    End If
                    If FEEConcessionTransactionBB.CheckPreviousConcession(ACD_ID, Session("sBsuId"), h_STUD_ID.Value, FCH_ID) Then
                        Me.programmaticModalPopup.Show()
                    End If
                Else
                    'lblError.Text = "Invalid Student #/Area Not Found!!!"
                    usrMessageBar.ShowNotification("Invalid Student #/Area Not Found!!!", UserControls_usrMessageBar.WarningType.Danger)
                End If
            End If
        End If
    End Sub

    Sub Set_Collection()
        If ViewState("datamode") <> "edit" Then
            SetStudentDetails()
        End If
        If ddlFeeType.Items.Count > 0 Then
            If ddlFeeType.SelectedValue = "6" Then
                GET_TRANSPORT_FEES_DATA()
                Exit Sub
            Else
                trTptArea.Visible = False
                H_Location.Value = "0"
                txtLocation.Text = ""
            End If
            Dim FromDT As Date = txtFromDT.Text
            Dim dtTermorMonths As New DataTable
            Dim dt As DataTable
            dt = FEE_CONC_TRANC_SUB_MONTHLY.CreateDataTableFeeConcessionMonthly
            Dim STR_SCH_ID As Integer
            Dim TOTALAMOUNT As Decimal = 0
            dtTermorMonths = FeeCommon.GetFee_MonthorTerm_OASIS(ddlFeeType.SelectedItem.Value,
                            ACD_ID, Session("sBSUID"), h_STUD_ID.Value,
                            txtFromDT.Text, txtToDT.Text, TOTALAMOUNT, STR_SCH_ID)
            For i As Integer = 0 To dtTermorMonths.Rows.Count - 1
                Dim dr As DataRow
                dr = dt.NewRow
                dr("id") = dtTermorMonths.Rows(i)("ID")
                dr("Descr") = dtTermorMonths.Rows(i)("descr")
                dr("FDD_DATE") = dtTermorMonths.Rows(i)("FDD_DATE")
                dr("FDD_AMOUNT") = dtTermorMonths.Rows(i)("FDD_AMOUNT")
                dr("CUR_AMOUNT") = 0
                dr("PRO_AMOUNT") = dtTermorMonths.Rows(i)("FDD_PROAMOUNT")
                'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT,FDD_AMOUNT
                dt.Rows.Add(dr)
            Next

            txtTotalFee.Text = TOTALAMOUNT
            ViewState("SCH_ID") = STR_SCH_ID

            Session("tempMonthly") = dt
            gvMonthly.DataSource = dt
            gvMonthly.DataBind()
            bindHistory()
            If ViewState("datamode") <> "edit" Then
                AutoFill()
            End If
        Else
            usrMessageBar.ShowNotification("Unable to get Fee type(s) for the academic year", UserControls_usrMessageBar.WarningType.Danger)
        End If

    End Sub

    Sub GET_TRANSPORT_FEES_DATA()
        SetStudentDetails()
        If H_Location.Value <> "" Then
            Dim iMonths, iSCH_ID As Integer
            Dim dtTermorMonths As New DataTable
            Dim TOTALAMOUNT As Decimal = 0
            Dim STR_SCH_ID As String = FeeCommon.GetFeeSCH_ID_ForConcession_Transport(h_STUD_ID.Value,
            ddlFeeType.SelectedItem.Value, ACD_ID, Session("sBSUID"),
            H_Location.Value, TOTALAMOUNT, txtFromDT.Text)
            ViewState("SCH_ID") = STR_SCH_ID
            txtTotalFee.Text = TOTALAMOUNT
            If IsNumeric(STR_SCH_ID) Then
                iSCH_ID = CInt(STR_SCH_ID)
                If iSCH_ID = 2 Then
                    dtTermorMonths = FeeCommon.GetFee_MonthorTermforConcession_Transport(ddlFeeType.SelectedItem.Value,
                    ACD_ID, Session("sBSUID"), True, h_STUD_ID.Value, H_Location.Value, txtFromDT.Text)
                Else
                    dtTermorMonths = FeeCommon.GetFee_MonthorTermforConcession_Transport(ddlFeeType.SelectedItem.Value,
                    ACD_ID, Session("sBSUID"), False, h_STUD_ID.Value, H_Location.Value, txtFromDT.Text)
                End If
                Select Case iSCH_ID
                    Case 0 '0 Monthly 
                        iMonths = 10
                    Case 1 '1 Bi-Monthly
                        iMonths = 5
                    Case 2 '2 Quarterly 
                        iMonths = 3
                    Case 3 '3 Half Year
                        iMonths = 2
                    Case 4, 5 '4 Annual 
                        iMonths = 1
                    Case Else '5 Once Only 
                        iMonths = 0
                End Select
            End If
            Dim dt As DataTable
            dt = FEE_CONC_TRANC_SUB_MONTHLY_BB.CreateDataTableFeeConcessionMonthly
            dt.Columns.Add("PRO_AMOUNT", GetType(Decimal))

            Select Case iMonths
                Case 10, 3, 1
                    For i As Integer = 0 To iMonths - 1
                        If i < dtTermorMonths.Rows.Count Then
                            Dim dr As DataRow
                            dr = dt.NewRow
                            dr("id") = dtTermorMonths.Rows(i)("ID")
                            dr("Descr") = dtTermorMonths.Rows(i)("descr")
                            dr("FDD_DATE") = dtTermorMonths.Rows(i)("FDD_DATE")
                            dr("FDD_AMOUNT") = dtTermorMonths.Rows(i)("FDD_AMOUNT")
                            'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT
                            dr("PRO_AMOUNT") = dtTermorMonths.Rows(i)("FDD_AMOUNT")
                            dr("CUR_AMOUNT") = 0
                            dt.Rows.Add(dr)
                        End If
                    Next
                Case 5, 2
                    For i As Integer = 0 To iMonths - 1
                        If i < dtTermorMonths.Rows.Count Then
                            Dim dr As DataRow
                            dr = dt.NewRow
                            dr("id") = dtTermorMonths.Rows(i * 10 / iMonths)("ID")
                            dr("Descr") = dtTermorMonths.Rows(i)("descr")
                            dr("FDD_DATE") = dtTermorMonths.Rows(i * 10 / iMonths)("FDD_DATE")
                            dr("FDD_AMOUNT") = dtTermorMonths.Rows(i)("FDD_AMOUNT")
                            dr("PRO_AMOUNT") = dtTermorMonths.Rows(i)("FDD_AMOUNT")
                            dr("CUR_AMOUNT") = 0
                            'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT,FDD_AMOUNT
                            dt.Rows.Add(dr)
                        End If
                    Next
            End Select
            Session("tempMonthly") = dt
            gvMonthly.DataSource = dt
            gvMonthly.DataBind()
            bindHistory()
        Else
            gvMonthly.DataBind()
            'lblError.Text = "Select Area"
            usrMessageBar.ShowNotification("Select Area", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub

    Sub bindHistory()
        gvHistory.DataSource = FEEConcessionTransaction.GetConcessionHistory(Session("sBsuid"), ACD_ID, h_STUD_ID.Value)
        gvHistory.DataBind()
    End Sub

    Sub AutoFill()
        'Set_Collection()
        If IsNumeric(txtTotalFee.Text) And IsNumeric(txtAmount.Text) Then
            Dim decTotalAmountorPerc As Decimal = Convert.ToDecimal(txtAmount.Text)
            Dim decTotalFee As Decimal = Convert.ToDecimal(txtTotalFee.Text)
            If decTotalFee > 0 Then
                Dim decWeightagePercentage As Decimal
                If radAmount.Checked Then
                    decWeightagePercentage = decTotalAmountorPerc / decTotalFee * 100
                Else
                    decWeightagePercentage = decTotalAmountorPerc
                End If
                If Not Session("tempMonthly") Is Nothing Then
                    Dim iTotalSplitup As Integer = Session("tempMonthly").Rows.Count
                    Dim decSplitAmount As Decimal = 0
                    If radPercentage.Checked Then
                        If decTotalAmountorPerc > 100 Then
                            'lblError.Text = "% exceeds 100"
                            usrMessageBar.ShowNotification("% exceeds 100", UserControls_usrMessageBar.WarningType.Information)
                            Exit Sub
                        End If
                        decSplitAmount = decTotalFee / iTotalSplitup * decTotalAmountorPerc / 100
                    Else
                        If decTotalAmountorPerc > decTotalFee Then
                            'lblError.Text = "Amount exceeds total amount"
                            usrMessageBar.ShowNotification("Amount exceeds total amount", UserControls_usrMessageBar.WarningType.Information)
                            Exit Sub
                        End If
                        decSplitAmount = decTotalAmountorPerc / iTotalSplitup
                    End If

                    Dim RoundDigit As Integer = 0
                    If chk_rndoff.Checked Then
                        RoundDigit = 0
                    Else
                        RoundDigit = 3
                    End If
                    For i As Integer = 0 To iTotalSplitup - 1 ' Actual Amount FDD_AMOUNT
                        Session("tempMonthly").rows(i)("CUR_AMOUNT") = Math.Round(Session("tempMonthly").rows(i)("PRO_AMOUNT") * decWeightagePercentage / 100, RoundDigit)
                    Next
                    gvMonthly.DataSource = Session("tempMonthly")
                    gvMonthly.DataBind()

                    Dim amttot As Double = 0.0
                    Dim taxtot As Double = 0.0
                    If gvMonthly.Rows.Count > 0 Then
                        Dim txtDAmount As New TextBox, lblTAXAmount As New Label, lblNETTotal As New Label, dblGridTotal As Double = 0
                        For Each gvr As GridViewRow In gvMonthly.Rows
                            txtDAmount = gvr.FindControl("txtAmount")
                            lblTAXAmount = gvr.FindControl("lblTaxAmount")
                            lblNETTotal = gvr.FindControl("lblNetAmount")
                            dblGridTotal += CDbl(txtDAmount.Text)
                            CALCULATE_TAX(txtDAmount, lblTAXAmount, lblNETTotal, IIf(rblTaxCalculation.SelectedValue = "I", 1, 0))
                            amttot = amttot + FeeCollection.GetDoubleVal(lblNETTotal.Text)
                            taxtot = taxtot + FeeCollection.GetDoubleVal(lblTAXAmount.Text)
                        Next
                        lbl_tot_netamount.Text = amttot
                        lbl_tot_taxamount.Text = taxtot
                    End If
                End If
            End If
        End If
    End Sub

    Protected Sub lnkFill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFill.Click
        AutoFill()

    End Sub

    'Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
    '    setAcademicyearDate()
    '    BindFee()
    '    UsrSelStudent1.ClearDetails()
    '    h_STUD_ID.Value = ""
    '    If chkNextAcdConc.Checked Then
    '        UsrSelStudent1.ACD_ID = 0
    '    Else
    '        UsrSelStudent1.ACD_ID = ddlAcademicYear.SelectedValue
    '    End If
    'End Sub

    Sub setAcademicyearDate()
        Dim DTFROM As String = ""
        Dim DTTO As String = ""

        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, ACD_ID, Session("sBsuid"))
        txtFromDT.Text = DTFROM
        txtToDT.Text = DTTO
        txtDate.Text = Format(Now.Date, OASISConstants.DateFormat)
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbHistory.Click

    End Sub

    Protected Sub ImagefromDate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Set_Collection()
    End Sub

    Protected Sub txtFromDT_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_Collection()
    End Sub

    Private Sub PrintConcession(ByVal IntFCH_ID As Integer)
        Session("ReportSource") = FEEConcessionTransaction.PrintConcession(IntFCH_ID, Session("sBsuid"), Session("sUsr_name"))
        h_print.Value = "print"
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If ViewState("datamode") <> "view" And ViewState("datamode") <> "edit" Then
            PrintConcession(ViewState("FCH_ID"))
        Else
            PrintConcession(ViewState("FCH_ID"))
        End If
    End Sub

    Protected Sub UsrSelStudent1_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uscStudentPicker1.StudentNoChanged
        h_STUD_ID.Value = uscStudentPicker1.STU_ID
        H_REFID_HEAD.Value = ""
        txtRefHEAD.Text = ""
        ACD_ID = uscStudentPicker1.STU_ACD_ID
        If chkNextAcdConc.Checked Then
            ACD_ID = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(NEXT_ACD_ID ,0) NEXT_ACD_ID FROM dbo.[fN_GetNextAcademicYearAndGrade]('" & Session("sBsuId") & "'," & uscStudentPicker1.STU_ACD_ID & ",'',0,0)")
        End If
        lblAcdYear.Text = GET_ACADEMICYEAR_DESCR(ACD_ID)
        If ACD_ID = 0 Then
            usrMessageBar.ShowNotification("Next academic year is not configured for the unit, please contact phoenix support", UserControls_usrMessageBar.WarningType.Danger)
            chkNextAcdConc.Checked = False
            lblAcdYear.Text = ""
        End If
        'labGrade.Text = "Grade : " & UsrSelStudent1.GRM_DISPLAY
        Session("FEE_CONS_TRAN") = Nothing
        setAcademicyearDate()
        BindFee()
        ddlFeeType_SelectedIndexChanged(Nothing, Nothing)
        GridBind()
        Set_Collection()
        radPercentage.Checked = True

    End Sub

    Protected Sub txtToDT_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_Collection()
    End Sub

    Protected Sub imgToDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Set_Collection()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Request.QueryString("FCH_ID") <> "" Then
                Dim FCH_ID As String = Encr_decrData.Decrypt(Request.QueryString("FCH_ID").Replace(" ", "+"))
                Dim retVal As Integer = FEEConcessionTransaction.DELETEFEE_CONCESSION_H(FCH_ID, Session("sBsuid"), Session("sUsr_name"))
                If retVal <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                Else
                    'lblError.Text = "Record Deleted Successfully"
                    usrMessageBar.ShowNotification("Record Deleted Successfully", UserControls_usrMessageBar.WarningType.Success)
                    ClearDetails()

                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            ElseIf ViewState("FCH_ID") <> "" Then
                Dim FCH_ID As String = ViewState("FCH_ID")
                Dim retVal As Integer = FEEConcessionTransaction.DELETEFEE_CONCESSION_H(FCH_ID, Session("sBsuid"), Session("sUsr_name"))
                If retVal <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                Else
                    'lblError.Text = "Record Deleted Successfully"
                    usrMessageBar.ShowNotification("Record Deleted Successfully", UserControls_usrMessageBar.WarningType.Success)
                    ClearDetails()

                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            End If
        Catch ex As Exception
            'lblError.Text = "Record could not be Deleted"
            usrMessageBar.ShowNotification("Record could not be Deleted", UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message, Page.Title)
        End Try
    End Sub

    Protected Sub chkNextAcdConc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkNextAcdConc.CheckedChanged
        'If chkNextAcdConc.Checked Then
        '    UsrSelStudent1.ACD_ID = 0
        'Else
        '    UsrSelStudent1.ACD_ID = ddlAcademicYear.SelectedValue
        'End If
        UsrSelStudent1_StudentNoChanged(Nothing, Nothing)
    End Sub

    Private Function GET_ACADEMICYEAR_DESCR(ByVal pACD_ID As Integer) As String
        GET_ACADEMICYEAR_DESCR = ""
        Try
            Dim Q As String = "SELECT ACY_DESCR + ' (' + CLM_SHORT + ')' FROM dbo.ACADEMICYEAR_D AS A WITH(NOLOCK) INNER JOIN dbo.CURRICULUM_M AS B WITH(NOLOCK) ON ACD_CLM_ID = CLM_ID INNER JOIN dbo.ACADEMICYEAR_M AS C WITH(NOLOCK) ON A.ACD_ACY_ID = C.ACY_ID WHERE ACD_ID = " & pACD_ID
            GET_ACADEMICYEAR_DESCR = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, Q)
        Catch ex As Exception

        End Try
    End Function
End Class
