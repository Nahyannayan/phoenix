<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FeeReminderExclude.aspx.vb" Inherits="Fees_FeeReminderExcludeInclude"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">
        //         Sys.Application.add_load(  
        //function CheckForPrint()  
        //     {
        //       if (document.getElementById('<%= h_print.ClientID %>' ).value!='')
        //       { 
        //       document.getElementById('<%= h_print.ClientID %>' ).value=''; 
        //      showModelessDialog ('../Reports/ASPX Report/RptViewerModal.aspx', '',"dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;"); 
        //       }
        //     } 
        //    ); 


        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            try {

                var chk_state = document.getElementById("chkAL").checked;
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                        if (document.forms[0].elements[i].type == 'checkbox') {
                            document.forms[0].elements[i].checked = chk_state;
                        }
                }
            }
            catch (ex) { }
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server" Text="Fee Reminder Exclude"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" CssClass="error"></asp:Label>--%>
                <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%">
                            <asp:HiddenField ID="h_STUD_ID" runat="server" />
                            <span class="field-label">Academic Year </span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">From </span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImgDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                ErrorMessage="Please Select Date" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                        <td align="left" width="20%"><span class="field-label">To </span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="Please Select Date" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <span class="field-label">
                                <asp:RadioButtonList ID="rblListing" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="LA">List All Students</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="XL">Import Excel</asp:ListItem>
                                    <asp:ListItem Value="AM">Add Manually</asp:ListItem>
                                </asp:RadioButtonList></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="error" colspan="4" runat="server" id="lblError2"></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <% If Me.rblListing.SelectedValue = "XL" Then%>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select File </span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:FileUpload ID="FileUpload1" runat="server" /></td>
                                    <td align="left" colspan="2">
                                        <asp:Button ID="btnImport" runat="server" CssClass="button" Text="Import" />
                                        <asp:HyperLink ID="lnkFormatFile" runat="server">Click here to get the formatted Excel file</asp:HyperLink>
                                    </td>

                                </tr>
                            </table>
                            <%End If%>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <% If Me.rblListing.SelectedValue = "AM" Then%>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Students </span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtStudNo" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgStudentN" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="GetStudentMulti(); return false;" />
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:LinkButton
                                            ID="lblAddNewStudent" runat="server" CausesValidation="False">Add</asp:LinkButton>
                                    </td>
                                    <td align="left" width="30%"></td>
                                </tr>
                            </table>
                            <% End If%>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <%If Me.lblMessage.Text.Trim <> "" Then%>
                            <center><asp:Label ID="lblMessage" runat="server" CssClass="error"></asp:Label></center>
                            <%End If%>
                            <asp:GridView ID="gvFEEReminder" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found"
                                Width="98%" AllowPaging="True" PageSize="30" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>
                                            <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkControl" type="checkbox" value='<%# Bind("STU_ID") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="FCH_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student#">
                                        <FooterTemplate>
                                            <asp:Label ID="lblCount" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <HeaderTemplate>
                                            Student # 
                                            <br />
                                            <asp:TextBox ID="txtStuno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchno" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Name<br />
                                            <asp:TextBox ID="txtStuname" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STUNAME") %>'></asp:Label><br />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            Grade<br />
                                            <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnGradeSearchd" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# bind("GRM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <HeaderTemplate>
                                            Section<br />
                                            <asp:TextBox ID="txtSection" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSectionSearchd" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# bind("SCT_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:RadioButton ID="RadExcludeSelected" runat="server" Checked="True" Text="Exclude Selected"
                                GroupName="SELECTION" Visible="False" />
                            <asp:RadioButton ID="RadIncludeSelected" runat="server" Text="Include Selected" GroupName="SELECTION"
                                Visible="False" />
                            Exclude Selected Students
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:CheckBox ID="chkPrint" runat="server" Checked="True" Text="Print after Save" CssClass="field-label"
                                Visible="False" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>


                <script type="text/javascript" language="javascript">
                    function GetStudentSingle() {
                        var sFeatures;
                        var sFeatures;
                        sFeatures = "dialogWidth: 750px; ";
                        sFeatures += "dialogHeight: 475px; ";
                        sFeatures += "help: no; ";
                        sFeatures += "resizable: no; ";
                        sFeatures += "scroll: yes; ";
                        sFeatures += "status: no; ";
                        sFeatures += "unadorned: no; ";
                        var NameandCode;
                        var result;
                        //var url = "../../ShowStudent.aspx?type=NO";
                        var url = "ShowStudent.aspx?type=NO";
                        //result = window.showModalDialog(url, "", sFeatures)
                        var oWnd = radopen(url, "pop_getstudent");
                        <%--if (result != '' && result != undefined) {
                            NameandCode = result.split('||');
                            document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                            document.getElementById('<%=txtStudNo.ClientID %>').value = NameandCode[1];
                            return true;
                        }
                        else {
                            return false;
                        }--%>
                    }


                    function GetStudentMulti() {
                        var SingleYN = "2";
                        if (SingleYN == "1") {
                            GetStudentSingle()
                        }
                        else {

                            var sFeatures;
                            var sFeatures;
                            sFeatures = "dialogWidth: 875px; ";
                            sFeatures += "dialogHeight: 600px; ";
                            sFeatures += "help: no; ";
                            sFeatures += "resizable: no; ";
                            sFeatures += "scroll: yes; ";
                            sFeatures += "status: no; ";
                            sFeatures += "unadorned: no; ";
                            var NameandCode;
                            var result;

                            var STUD_TYP = false;
                            var url;
                            if (STUD_TYP == true) {
                                url = "ShowStudentMulti.aspx?TYPE=ENQUIRY&MULTI_SEL=true&bsu=";
                                //result = window.showModalDialog(url, "", sFeatures);
                                var oWnd = radopen(url, "pop_getmultistudent");
                            }
                            else {
                                url = "ShowStudentMulti.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu=";
                                //result = window.showModalDialog(url, "", sFeatures);
                                var oWnd = radopen(url, "pop_getmultistudent");
                            }
                           <%-- if (result != '' && result != undefined) {
                                document.getElementById('<%=txtStudNo.ClientID %>').value = 'Multiple Students selected';
                                document.getElementById('<%=h_STUD_ID.ClientID %>').value = result;
                            }--%>
                        }
                        //return true;
                    }


                    function autoSizeWithCalendar(oWindow) {
                        var iframe = oWindow.get_contentFrame();
                        var body = iframe.contentWindow.document.body;

                        var height = body.scrollHeight;
                        var width = body.scrollWidth;

                        var iframeBounds = $telerik.getBounds(iframe);
                        var heightDelta = height - iframeBounds.height;
                        var widthDelta = width - iframeBounds.width;

                        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                        oWindow.center();
                    }

                    function OnClientClose1(oWnd, args) {
                        //get the transferred arguments
                        var arg = args.get_argument();
                        if (arg) {

                            NameandCode = arg.NameandCode.split('||');
                            document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                            document.getElementById('<%=txtStudNo.ClientID %>').value = NameandCode[1];
                            __doPostBack('<%=h_STUD_ID.ClientID%>', 'TextChanged');
                        }
                    }

                    function OnClientClose2(oWnd, args) {
                        //get the transferred arguments
                        var arg = args.get_argument();
                        if (arg) {
                            NameandCode = arg.NameandCode;
                            document.getElementById('<%=txtStudNo.ClientID %>').value = 'Multiple Students selected';
                            document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode;
                            __doPostBack('<%=h_STUD_ID.ClientID%>', 'TextChanged');
                        }
                    }
                </script>

                <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
                    ReloadOnShow="true" runat="server" EnableShadow="true">
                    <Windows>
                        <telerik:RadWindow ID="pop_getstudent" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                            OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
                        </telerik:RadWindow>
                    </Windows>
                    <Windows>
                        <telerik:RadWindow ID="pop_getmultistudent" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                            OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
                        </telerik:RadWindow>
                    </Windows>
                </telerik:RadWindowManager>


                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server"
                        type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden"
                            value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_Selected_Value" runat="server" type="hidden" value="=" />
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="ImgDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <asp:Panel ID="Panel1" runat="server">
                </asp:Panel>
                <asp:HiddenField ID="h_print" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>
