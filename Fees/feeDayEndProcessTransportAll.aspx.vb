﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeDayEndProcessTransportAll
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                gvHistory.Attributes.Add("bordercolor", "#1b80b6")

                If Session("sUsr_name") = "" Or ViewState("MainMnu_code") <> "F300435" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))

                    Select Case ViewState("MainMnu_code").ToString
                        Case "F300435"
                            lblHead.Text = "Day End Process All"
                    End Select
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    txtDate.Text = Format(Now.Date, OASISConstants.DateFormat)
                    ddlBUnit.DataBind()
                    ddlBUnit.Enabled = False
                    If Not ddlBUnit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
                        ddlBUnit.ClearSelection()
                        ddlBUnit.Items.FindByValue(Session("sBsuid")).Selected = True
                    End If 
                    BindHistory()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
            'btnprint.Visible = False
        End If
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim STATUS As Integer
        Dim str_err As String = ""
        If IsDate(txtDate.Text) = False Then
            str_err = str_err & "<br />" & "Please Enter Valid Date"
        End If
        

        Dim strfDate As String = txtDate.Text.Trim
        str_err = str_err & DateFunctions.checkdate(strfDate)
        Dim str_FYear As String = UtilityObj.GetDataFromSQL("SELECT FYR_ID FROM FINANCIALYEAR_S " _
        & " WHERE '" & strfDate & "' BETWEEN FYR_FROMDT AND FYR_TODT AND ISNULL(FYR_bCLOSE,0)=0", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        If str_FYear = "" Or str_FYear = "--" Then
            str_err = str_err & "<br />" & "Check Financial Year"
        End If
        If str_err <> "" Then
            'lblError.Text = str_err
            usrMessageBar.ShowNotification(str_err, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        Else
            txtDate.Text = strfDate
        End If
        Try
            Dim JHD_NEWDOCNO As String = ""
            Dim trans As SqlTransaction
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim conn As New SqlConnection(str_conn)
            conn.Open()
            trans = conn.BeginTransaction("SampleTkransaction")
            Try
                STATUS = FeeDayendProcess.F_GenerateCollectionVoucher_Provider_Transport(Session("sBsuid"), _
                txtDate.Text, Session("sUsr_name"), _
                ddlBUnit.SelectedItem.Value, chkHardClose.Checked, conn, trans)
                If STATUS = 0 Then
                    trans.Commit()
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ddlBUnit.SelectedItem.Value, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                    'btnprint.Visible = True
                    BindHistory()
                Else
                    trans.Rollback()
                End If
                'lblError.Text = getErrorMessage(STATUS)
                usrMessageBar.ShowNotification(getErrorMessage(STATUS), UserControls_usrMessageBar.WarningType.Danger)
            Catch ex As Exception
                trans.Rollback()
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = getErrorMessage("1000")
                usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub ddlBUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBUnit.SelectedIndexChanged
        BindHistory()
    End Sub

    Sub BindHistory()
        If IsDate(txtDate.Text) Then
            bindData()
            Dim dsDayendDetails As DataSet = FeeDayendProcess.GetDayEndHistory(False, _
            ddlBUnit.SelectedItem.Value, txtDate.Text, True)
            If dsDayendDetails.Tables.Count > 0 Then
                gvHistory.DataSource = dsDayendDetails.Tables(0)
                gvHistory.DataBind()
            End If
            If dsDayendDetails.Tables.Count > 1 Then
                gvCloseStatus.DataSource = dsDayendDetails.Tables(1)
                gvCloseStatus.DataBind()
            End If

            gvDayend.DataSource = FeeDayendProcess.GETDAYENDSTATUS(ddlBUnit.SelectedItem.Value, txtDate.Text, True)
            gvDayend.DataBind()
        End If
    End Sub

    Protected Sub imgFrom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgFrom.Click
        BindHistory()
    End Sub

    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        BindHistory()
    End Sub

    Sub bindData()
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ASONDT", SqlDbType.VarChar)
        pParms(0).Value = txtDate.Text
    
        pParms(1) = New SqlClient.SqlParameter("@PROVIDER_BSU_ID", SqlDbType.VarChar)
        pParms(1).Value = ddlBUnit.SelectedItem.Value
        Dim _table As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
                                CommandType.StoredProcedure, "[TRANSPORT].[Dayend_Status]", pParms)
        gvDayendDetails.DataSource = Nothing
        gvDayendDetails.DataBind()
        gvDayendDetails.DataSource = _table.Tables(0)
        gvDayendDetails.DataBind()
        If _table.Tables.Count > 0 Then
            gvNextstatus.DataSource = Nothing
            gvNextstatus.DataBind()
            gvNextstatus.DataSource = _table.Tables(1)
            gvNextstatus.DataBind()
        End If
    End Sub

    Protected Sub gvHistory_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvHistory.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'Dim LnkBtn As LinkButton = New LinkButton()
            If e.Row.RowIndex < 3 Then
                Dim docNoArr As String()
                Dim docNoStr As String = ""
                docNoArr = e.Row.Cells(1).Text.ToString().Split(",")
                e.Row.Cells(1).Text = ""
                For x As Integer = 0 To docNoArr.Length - 1
                    Dim docNo As String
                    'docNo = "<a onclick = 'getPrint(" & "'" & docNoArr(x) & "'" & " )' style = 'cursor:hand'>" & docNoArr(x) & "</a>"

                    docNo = "<a href=" & Chr(34) & "#" & Chr(34) & " onclick=" & Chr(34) & _
                "javascript:getPrint( '" + docNoArr(x).Trim() + "', '" + e.Row.Cells(0).Text + "' ); " & Chr(34) & _
                " title = " & Chr(34) & " Click for print.. " & docNoArr(x) & Chr(34) & " style=" & Chr(34) & "cursor:hand;" & _
                " text-decoration: underline;" & Chr(34) & " class=" & Chr(34) & "gridheader_Link" & Chr(34) & " >" & docNoArr(x) & "</a>"

                    e.Row.Cells(1).Text += docNo & " "
                Next
                e.Row.Cells(1).Style("cursor") = "hand"
            Else
                e.Row.Cells(1).Text = e.Row.Cells(1).Text.Replace(",", "  : ")
            End If
        End If
    End Sub
End Class

