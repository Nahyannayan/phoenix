Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeRefundFeeView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            '  gvRefundFee.Attributes.Add("bordercolor", "#1b80b6")
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif" 
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_REFUND_FEE_APPROVAL _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_REFUND_REQUEST) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_REFUND_REQUEST
                        lblHead.Text = "Fee Refund Request"
                        hlAddNew.NavigateUrl = "feeRefundFee.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    Case OASISConstants.MNU_FEE_REFUND_FEE_APPROVAL
                        lblHead.Text = "Fee Refund Approval"
                        hlAddNew.Visible = False
                        rbRejected.Visible = False
                        rbOpen.Visible = False
                        rbPosted.Visible = False
                End Select

                ddlAcademicYear.Items.Clear()
                ddlAcademicYear.DataSource = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
                ddlAcademicYear.DataTextField = "ACY_DESCR"
                ddlAcademicYear.DataValueField = "ACD_ID"
                ddlAcademicYear.DataBind()
                ddlAcademicYear.SelectedIndex = -1
                ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True

                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                ' lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvRefundFee.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvRefundFee.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim ds As New DataSet
            Dim str_Filter As String = String.Empty
            Dim lstrOpr As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty

            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            If gvRefundFee.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtStuno
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvRefundFee.HeaderRow.FindControl("txtStuno")
                lstrCondn1 = Trim(txtSearch.Text.Trim)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn1)
                '   -- 1  txtStuname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvRefundFee.HeaderRow.FindControl("txtStuname")
                lstrCondn2 = txtSearch.Text.Trim
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn2)
                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvRefundFee.HeaderRow.FindControl("txtPeriod")
                lstrCondn3 = txtSearch.Text.Trim
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FRH_ACT_ID", lstrCondn3)
                '   -- 3   txtTDate
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvRefundFee.HeaderRow.FindControl("txtFrom")
                lstrCondn4 = txtSearch.Text.Trim
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FRH_DATE", lstrCondn4)
                '   -- 5  city
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvRefundFee.HeaderRow.FindControl("txtRemarks")
                lstrCondn5 = txtSearch.Text.Trim
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FRH_NARRATION", lstrCondn5)
            End If
           
            If rbOpen.Checked Then
                str_Filter = str_Filter & " and isnull(FRH_bPosted,0)=0 and isnull(FRH_bDeleted,0)=0 "
            End If
            If rbPosted.Checked Then
                str_Filter = str_Filter & " and isnull(FRH_bPosted,0)=1 and isnull(FRH_bDeleted,0)=0 "
            End If
            If rbRejected.Checked Then
                str_Filter = str_Filter & " and  isnull(FRH_bDeleted,0)=1 "
            End If
            ''''FCH_ID,FCH_STU_ID, FCH_DT,FCH_REMARKS,PERIOD,STU_NAME,STU_NO,FCM_DESCR, AMOUNT
            Dim str_cond As String = String.Empty
            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.MNU_FEE_REFUND_REQUEST
                    str_Sql = "SELECT * FROM FEES.VW_OSO_FEE_REFUND_TRANS" & _
                              " WHERE   FRH_BSU_ID='" & Session("sBsuid") & _
                              "' AND FRH_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' " & str_Filter & " order by FRH_DATE desc "
                Case OASISConstants.MNU_FEE_REFUND_FEE_APPROVAL
                    str_Sql = "SELECT * FROM FEES.VW_OSO_FEE_REFUND_TRANS " & _
                              " WHERE isnull(FRH_bPosted,0)=0 and isnull(FRH_bDeleted,0)=0 AND FRH_BSU_ID='" & _
                              Session("sBsuid") & "'" & str_Filter & " order by FRH_DATE desc "
            End Select
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            gvRefundFee.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvRefundFee.DataBind()
                Dim columnCount As Integer = gvRefundFee.Rows(0).Cells.Count
                gvRefundFee.Rows(0).Cells.Clear()
                gvRefundFee.Rows(0).Cells.Add(New TableCell)
                gvRefundFee.Rows(0).Cells(0).ColumnSpan = columnCount
                gvRefundFee.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvRefundFee.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvRefundFee.DataBind()
            End If
            txtSearch = gvRefundFee.HeaderRow.FindControl("txtStuno")
            txtSearch.Text = lstrCondn1
            txtSearch = gvRefundFee.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn2
            txtSearch = gvRefundFee.HeaderRow.FindControl("txtPeriod")
            txtSearch.Text = lstrCondn3
            txtSearch = gvRefundFee.HeaderRow.FindControl("txtFrom")
            txtSearch.Text = lstrCondn4
            txtSearch = gvRefundFee.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn5
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvRefundFee.PageIndexChanging
        gvRefundFee.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRefundFee.RowDataBound
        Try
            Dim lblFRH_ID As New Label
            lblFRH_ID = TryCast(e.Row.FindControl("lblFRH_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblFRH_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")

                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_REFUND_REQUEST
                        hlEdit.NavigateUrl = "feeRefundFee.aspx?view_id=" & Encr_decrData.Encrypt(lblFRH_ID.Text) & _
                                       "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    Case OASISConstants.MNU_FEE_REFUND_FEE_APPROVAL
                        hlEdit.NavigateUrl = "feeRefundFee.aspx?view_id=" & Encr_decrData.Encrypt(lblFRH_ID.Text) & _
                                   "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                End Select

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub   

    Protected Sub rbOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbOpen.CheckedChanged
        GridBind()
    End Sub

    Protected Sub rbPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPosted.CheckedChanged
        GridBind()
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbRejected.CheckedChanged
        GridBind()
    End Sub

End Class
