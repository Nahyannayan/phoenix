﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Xml
Imports System.Text
Imports System.Data.SqlTypes
Imports UtilityObj
Partial Class Fees_feeTranspotReminderAddDetailNew
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'ClientScript.RegisterStartupScript(Me.GetType(), _
                '"script", "<script language='javascript'>  CheckOnPostback(); </script>")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                gvExcel.Attributes.Add("bordercolor", "#1b80b6")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F300199" And ViewState("MainMnu_code") <> "F300236") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                txtAsOnDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                ddlBusinessunit.DataBind()
                'BindAcademicYear(ddlBusinessunit.SelectedValue)
                FillACD()
                trFind.Visible = False
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                
                If ViewState("datamode") = "view" Then
                    trFind.Visible = False
                    Dim FRH_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FRH_ID").Replace(" ", "+"))
                    Dim vFEE_REM As FEEReminderTransport = FEEReminderTransport.GetHeaderDetails(FRH_ID)
                    If vFEE_REM Is Nothing Then
                        Exit Sub
                    End If
                    
                    Me.ddlBusinessunit.SelectedValue = vFEE_REM.FRH_BSU_ID
                    ddlBusinessunit_SelectedIndexChanged(sender, e)
                    Me.ddlAcademicYear.SelectedValue = vFEE_REM.FRH_ACD_ID
                    Me.ddlReminder.SelectedValue = vFEE_REM.FRH_Level
      
                    ddlAcademicYear_SelectedIndexChanged(sender, e)
                    LoadBuses(Me.ddlBusinessunit.SelectedValue)
                    SelectAllBuses()
                    txtAsOnDate.Text = Format(vFEE_REM.FRH_ASONDATE, OASISConstants.DateFormat)
                    txtDate.Text = Format(vFEE_REM.FRH_DT, OASISConstants.DateFormat)
                    GetSavedDetails(FRH_ID)
                    If ViewState("MainMnu_code") = "F300236" Then
                        btnSMS.Visible = True
                    End If
                Else
                    ddlAcademicYear_SelectedIndexChanged(sender, e)
                    LoadBuses(Me.ddlBusinessunit.SelectedValue)
                    SelectAllBuses()
                    Me.trFilter.Visible = False
                    Me.Trbuttons.Visible = True
                    btnSMS.Visible = False
                End If
               

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub
    Private Sub LoadBuses(ByVal BSU_ID As String)
        Dim qry As String = ""
        qry = "SELECT BNO_ID,BNO_DESCR FROM (SELECT DISTINCT BNO_ID,BNO_DESCR from TRANSPORT.TRIPS_M A " & _
        "INNER JOIN TRANSPORT.TRIPS_D C ON A.TRP_ID=C.TRD_TRP_ID " & _
        "INNER JOIN TRANSPORT.BUSNOS_M D ON C.TRD_BNO_ID=BNO_ID " & _
        "WHERE BNO_BSU_ID='" & BSU_ID & "' UNION SELECT 0,'-'  ) A ORDER BY A.BNO_DESCR "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, qry)
        Me.cblBusNos.DataSource = ds.Tables(0)
        Me.cblBusNos.DataTextField = "BNO_DESCR"
        Me.cblBusNos.DataValueField = "BNO_ID"
        Me.cblBusNos.DataBind()

    End Sub
    Sub SelectAllBuses()
        Dim li As Telerik.Web.UI.RadComboBoxItem
        For Each li In cblBusNos.Items
            Dim chk As CheckBox = DirectCast(li.FindControl("chkComboItem"), CheckBox)
            chk.Checked = True
        Next
    End Sub
    Sub SelectAllGrades(Optional ByVal State As Boolean = True)
        Dim li As Telerik.Web.UI.RadComboBoxItem
        For Each li In cblGrade.Items
            Dim chk As CheckBox = DirectCast(li.FindControl("chkComboItem"), CheckBox)
            chk.Checked = State
        Next
    End Sub

    Private Sub doInsert()
        Dim level As Integer = Me.ddlReminder.SelectedValue
        Dim NewFshId As String = ""
        'If radSecRM.Checked Then
        '    level = 2
        'ElseIf radThirdRM.Checked Then
        '    level = 3
        'End If
        Dim FctId As String = ""
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim retval As Integer
        Try
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FRH_ID", SqlDbType.Int)
            pParms(0).Value = 0
            pParms(1) = New SqlClient.SqlParameter("@FRH_BSU_ID", SqlDbType.VarChar)
            pParms(1).Value = Session("sBsuid")
            pParms(2) = New SqlClient.SqlParameter("@FRH_STU_BSU_ID", SqlDbType.VarChar)
            pParms(2).Value = ddlBusinessunit.SelectedItem.Value
            pParms(3) = New SqlClient.SqlParameter("@FRH_ACD_ID", SqlDbType.Int)
            pParms(3).Value = ddlAcademicYear.SelectedItem.Value
            pParms(4) = New SqlClient.SqlParameter("@FRH_DT", SqlDbType.DateTime)
            pParms(4).Value = CDate(txtDate.Text)
            pParms(5) = New SqlClient.SqlParameter("@FRH_ASONDATE", SqlDbType.DateTime)
            pParms(5).Value = CDate(txtAsOnDate.Text)
            pParms(6) = New SqlClient.SqlParameter("@FRH_Level", SqlDbType.Int)
            pParms(6).Value = level
            pParms(7) = New SqlClient.SqlParameter("@bDelete", SqlDbType.Bit)
            pParms(7).Value = False
            pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(8).Direction = ParameterDirection.ReturnValue

            pParms(9) = New SqlClient.SqlParameter("@NEW_FRH_ID", SqlDbType.VarChar, 20)
            pParms(9).Direction = ParameterDirection.Output
            retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[FEES].[F_SAVEFEE_REMINDER_H]", pParms)
            NewFshId = pParms(9).Value
            If pParms(8).Value <> "0" Or NewFshId = "" Then
                'lblError.Text = pParms(8).Value.ToString()
                usrMessageBar2.ShowNotification(pParms(8).Value.ToString(), UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                objConn.Close()
                Exit Sub
            End If

            For Each grow As GridViewRow In gvExcel.Rows

                Dim ChkSelect As HtmlInputCheckBox = CType(grow.FindControl("ChkSelect"), HtmlInputCheckBox)
                If ChkSelect IsNot Nothing And ChkSelect.Checked Then
                    Dim pParms1(5) As SqlClient.SqlParameter
                    pParms1(0) = New SqlClient.SqlParameter("@FRD_FRH_ID", SqlDbType.Int)
                    pParms1(0).Value = NewFshId
                    pParms1(1) = New SqlClient.SqlParameter("@FRD_ACD_ID", SqlDbType.VarChar)
                    pParms1(1).Value = ddlAcademicYear.SelectedItem.Value
                    pParms1(2) = New SqlClient.SqlParameter("@FRD_STU_ID", SqlDbType.VarChar)
                    pParms1(2).Value = ChkSelect.Value
                    pParms1(3) = New SqlClient.SqlParameter("@FRD_DUE_AMT", SqlDbType.Decimal)
                    pParms1(3).Value = Convert.ToDecimal(grow.Cells(5).Text)
                    pParms1(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms1(4).Direction = ParameterDirection.ReturnValue
                    retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "FEES.F_SAVEFEE_REMINDER_D_NEW", pParms1)
                    If pParms1(4).Value <> "0" Then
                        'lblError.Text = getErrorMessage(pParms1(4).Value)
                        usrMessageBar2.ShowNotification(getErrorMessage(pParms1(4).Value), UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        objConn.Close()
                        grow.BackColor = Drawing.Color.BurlyWood
                        Exit Sub
                    End If
                End If

            Next
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar2.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            objConn.Close()
        End Try
        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, NewFshId, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
        stTrans.Commit()
        If objConn.State = ConnectionState.Open Then
            objConn.Close()
        End If
        'lblError.Text = getErrorMessage(0)
        usrMessageBar2.ShowNotification(getErrorMessage(0), UserControls_usrMessageBar.WarningType.Success)
        ClearDetails()
        If ChkPrint.Checked Then
            AftersavePrint(NewFshId)
        End If
    End Sub
    Function CheckAlreadySendSMS() As Integer
        h_Confirm.Value = "0"
        Dim _table As New DataSet
        Dim dDate As Date = CDate(txtDate.Text)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim sqlStr As String = ""
        Dim ChkSMS As Integer = 0
        Try

            For Each grow As GridViewRow In gvExcel.Rows

                Dim ChkSelect As HtmlInputCheckBox = CType(grow.FindControl("ChkSelect"), HtmlInputCheckBox)
                If ChkSelect IsNot Nothing And ChkSelect.Checked Then

                    sqlStr = " SELECT FRH.FRH_ID FROM FEES.FEE_REMINDER_D AS FRD INNER JOIN " & _
                " FEES.FEE_REMINDER_H AS FRH ON FRD.FRD_FRH_ID = FRH.FRH_ID " & _
                " WHERE MONTH(FRH.FRH_DT) =MONTH('" & dDate & "')  AND YEAR(FRH.FRH_DT) = YEAR('" & dDate & "')" & _
                " AND FRD.FRD_STU_ID = " & ChkSelect.Value & ""

                    _table = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sqlStr)

                    If (_table.Tables(0).Rows.Count > 0) Then
                        grow.BackColor = Drawing.Color.BurlyWood
                        ChkSMS = 1
                        h_Confirm.Value = "1"
                    End If

                End If
            Next

        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar2.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
        End Try
        Return ChkSMS
    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If h_Confirm.Value = "0" Then
            Dim ChkSMS As Integer = CheckAlreadySendSMS()
            If ChkSMS = 1 Then
                'lblError.Text = "Selected Records SMS already send this Month..Continue Click Save..!"
                usrMessageBar2.ShowNotification("Selected Records SMS already send this Month..Continue Click Save..!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
        End If
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar2.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Try
            doInsert()

            h_Confirm.Value = "0"

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub
    Private Sub ClearDetails()
        ViewState("GetSelectedGrades") = String.Empty
        ViewState("GetSelectedBuses") = String.Empty
        gvExcel.DataSource = Nothing
        gvExcel.DataBind()
        'lblError.Text = ""
    End Sub

    

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        ClearDetails()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'BindAcademicYear(ddlBusinessunit.SelectedValue)
        ClearDetails()
        FillACD()
        ddlAcademicYear_SelectedIndexChanged(sender, e)
        LoadBuses(Me.ddlBusinessunit.SelectedValue)
        SelectAllBuses()
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBusinessunit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        getBind()
    End Sub
    Private Sub getBind()
        ClearDetails()
        GetSelectedBuses()
        GetSelectedGrades()
        Trbuttons.Visible = False
        Dim _table As New DataSet
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Try
            Dim cmd As SqlCommand
            cmd = New SqlCommand("[FEES].[GET_TRANSPORT_FEE_REMINDER]", ConnectionManger.GetOASISTransportConnection)
            cmd.CommandType = CommandType.StoredProcedure

            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
            pParms(0).Value = Session("sBsuid")
            cmd.Parameters.Add(pParms(0))
            pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.Int)
            pParms(1).Value = ddlBusinessunit.SelectedItem.Value
            cmd.Parameters.Add(pParms(1))
            pParms(2) = New SqlClient.SqlParameter("@ASONDT", SqlDbType.DateTime)
            pParms(2).Value = CDate(txtDate.Text)
            cmd.Parameters.Add(pParms(2))
            pParms(3) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
            pParms(3).Value = ddlAcademicYear.SelectedItem.Value
            cmd.Parameters.Add(pParms(3))
            pParms(4) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar)
            pParms(4).Value = Me.txtStudNo.Text.Trim
            cmd.Parameters.Add(pParms(4))
            pParms(5) = New SqlClient.SqlParameter("@STU_NAME", SqlDbType.VarChar)
            pParms(5).Value = Me.txtStudName.Text.Trim
            cmd.Parameters.Add(pParms(5))
            pParms(6) = New SqlClient.SqlParameter("@STU_GRM", SqlDbType.VarChar)
            pParms(6).Value = ViewState("GetSelectedGrades")
            cmd.Parameters.Add(pParms(6))
            pParms(7) = New SqlClient.SqlParameter("@MIN_NET_OS", SqlDbType.VarChar)
            pParms(7).Value = Me.txtMinNetOS.Text.Trim
            cmd.Parameters.Add(pParms(7))
            pParms(8) = New SqlClient.SqlParameter("@STU_BUS_NO", SqlDbType.VarChar)
            pParms(8).Value = ViewState("GetSelectedBuses")
            cmd.Parameters.Add(pParms(8))
            cmd.CommandTimeout = 0
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = cmd
            'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey
            adpt.SelectCommand.CommandTimeout = 0
            _table.Clear()
            adpt.Fill(_table)

            '_table = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[FEES].[GetBaseDataForReminder]", pParms)
            gvExcel.DataSource = _table.Tables(0)
            gvExcel.DataBind()

            If _table.Tables(0).Rows.Count > 0 Then
                Trbuttons.Visible = True
                'Me.trFilter.Visible = True
            End If

        Catch ex As Exception

            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar2.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)

        End Try
    End Sub
    Private Sub GetSavedDetails(ByVal FRH_ID As Integer)
        GetSelectedBuses()
        GetSelectedGrades()
        trFilter.Visible = True
        Trbuttons.Visible = False
        Dim _table As New DataSet
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Try
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FRH_ID", SqlDbType.Int)
            pParms(0).Value = FRH_ID

            pParms(1) = New SqlClient.SqlParameter("@FILTER", SqlDbType.Int) '9 for all
            pParms(1).Value = IIf(radAll.Checked, 9, IIf(radSent.Checked, 2, IIf(radNotSent.Checked, 0, IIf(radEmailFailed.Checked, 1, 9))))

            pParms(2) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar)
            pParms(2).Value = Me.txtStudNo.Text.Trim

            pParms(3) = New SqlClient.SqlParameter("@STU_NAME", SqlDbType.VarChar)
            pParms(3).Value = Me.txtStudName.Text.Trim

            pParms(4) = New SqlClient.SqlParameter("@STU_GRM", SqlDbType.VarChar)
            pParms(4).Value = ViewState("GetSelectedGrades")

            pParms(5) = New SqlClient.SqlParameter("@MIN_NET_OS", SqlDbType.VarChar)
            pParms(5).Value = Me.txtMinNetOS.Text.Trim

            pParms(6) = New SqlClient.SqlParameter("@STU_BUS_NO", SqlDbType.VarChar)
            pParms(6).Value = IIf(Me.h_checkAll.Value, "", ViewState("GetSelectedBuses"))

            _table = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.GETTRANSPORTFEEREMINDER", pParms)
            gvExcel.DataSource = _table.Tables(0)
            gvExcel.DataBind()

            If _table.Tables(0).Rows.Count > 0 Then
                'Me.gvExcel.Columns(4).Visible = False
                'Me.gvExcel.Columns(5).Visible = False
                'Me.gvExcel.Columns(6).Visible = False

                Me.gvExcel.Columns(6).Visible = radEmailFailed.Checked
            End If
            Me.TrEmail.Visible = True
        Catch ex As Exception

            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar2.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Sub AftersavePrint(ByVal FrhId As String)
        'Session("ReportSource") = FEEReminder.PrintReminder(FrhId, Session("sUsr_name"), False)
        Session("ReportSource") = FEEReminderTransport.PrintReminder(FrhId, Session("sUsr_name"), Session("sBsuid"), "")
        h_print.Value = "print"
    End Sub

    Protected Sub btnEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmail.Click
        'Me.lblError.Text = ""
        getselected()

        If ViewState("Checked") Is Nothing Or ViewState("Checked") = "" Then
            'Me.lblError.Text = "Please select the students"
            usrMessageBar2.ShowNotification("Please select the students", UserControls_usrMessageBar.WarningType.Danger)
        Else
            Dim FRH_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FRH_ID").Replace(" ", "+"))
            If doEMail(FRH_ID) = False Then
                'lblError.Text = "Sending E-Mail was not successful"
                usrMessageBar2.ShowNotification("Sending E-Mail was not successful", UserControls_usrMessageBar.WarningType.Danger)
            Else
                ClearDetails()
                ViewState("Checked") = ""
                'lblError.Text = "E-Mail sent successfully"
                usrMessageBar2.ShowNotification("E-Mail sent successfully", UserControls_usrMessageBar.WarningType.Success)
            End If
        End If
    End Sub

    Protected Sub btnSMS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSMS.Click
        'Me.lblError.Text = ""
        getselected()
        If ViewState("Checked") Is Nothing Or ViewState("Checked") = "" Then
            'Me.lblError.Text = "Please select the students"
            usrMessageBar2.ShowNotification("Please select the students", UserControls_usrMessageBar.WarningType.Danger)
        End If
        SEND_SMS()
    End Sub

    Sub getselected()
        Dim chk As New System.Web.UI.HtmlControls.HtmlInputCheckBox
        ViewState("Checked") = ""
        For Each gvr As GridViewRow In gvExcel.Rows
            chk = DirectCast(gvr.FindControl("ChkSelect"), System.Web.UI.HtmlControls.HtmlInputCheckBox)
            If chk.Checked Then
                ViewState("Checked") = IIf(ViewState("Checked") Is Nothing Or ViewState("Checked") = "", gvr.Cells(1).Text, ViewState("Checked") + "|" + gvr.Cells(1).Text)
            End If
        Next
    End Sub
    Private Function doEMail(ByVal FRH_ID As String) As Boolean
        Dim sqlParam(7) As SqlParameter
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        sqlParam(0) = Mainclass.CreateSqlParameter("@FRD_STU_ID", ViewState("Checked"), SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@FRH_ID", FRH_ID, SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@AUD_WINUSER", Page.User.Identity.Name.ToString(), SqlDbType.VarChar)
        sqlParam(4) = Mainclass.CreateSqlParameter("@Aud_form", Master.MenuName.ToString(), SqlDbType.VarChar)
        sqlParam(5) = Mainclass.CreateSqlParameter("@Aud_user", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(6) = Mainclass.CreateSqlParameter("@Aud_module", Session("sModule"), SqlDbType.VarChar)
        sqlParam(7) = Mainclass.CreateSqlParameter("@v_ReturnMsg", Session("sModule"), SqlDbType.VarChar, True)
        Dim str_success As String
        str_success = Mainclass.ExecuteParamQRY(str_conn, "[dbo].[EMailFeeReminders]", sqlParam)
        If str_success <> 0 Then
            'lblError.Text = sqlParam(7).Value
            usrMessageBar2.ShowNotification(sqlParam(7).Value, UserControls_usrMessageBar.WarningType.Danger)
            doEMail = False
        Else
            doEMail = True
        End If
    End Function

    Protected Sub chkEMail_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radSent.CheckedChanged, radAll.CheckedChanged, radNotSent.CheckedChanged, radEmailFailed.CheckedChanged
        Dim FRH_ID As Integer = 0
        Try
            FRH_ID = Encr_decrData.Decrypt(Request.QueryString("FRH_ID").Replace(" ", "+"))
        Catch ex As Exception
            FRH_ID = 0
        End Try
        GetSavedDetails(FRH_ID)
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Dim FRH_ID As Integer = 0
        Try
            FRH_ID = Encr_decrData.Decrypt(Request.QueryString("FRH_ID").Replace(" ", "+"))
        Catch ex As Exception
            FRH_ID = 0
        End Try
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            getBind()
        Else
            GetSavedDetails(FRH_ID)
        End If

    End Sub

    Protected Sub btnCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel2.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ElseIf ViewState("datamode") = "view" Then
            Response.Redirect(ViewState("ReferrerUrl"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Private Sub GetSelectedBuses()
        ViewState("GetSelectedBuses") = String.Empty
        Dim Li As Telerik.Web.UI.RadComboBoxItem
        For Each Li In cblBusNos.Items
            Dim chk As CheckBox = DirectCast(Li.FindControl("chkComboItem"), CheckBox)
            Dim lbl As Label = DirectCast(Li.FindControl("lblComboItem"), Label)
            If chk.Checked = True Then
                ViewState("GetSelectedBuses") = IIf(ViewState("GetSelectedBuses").Trim = "", lbl.Text, ViewState("GetSelectedBuses") + "|" + lbl.Text)
            Else
                Me.h_checkAll.Value = False
            End If
        Next
        'ViewState("GetSelectedBuses") = cblBusNos.CheckedItems
    End Sub
    Private Sub GetSelectedGrades()
        ViewState("GetSelectedGrades") = String.Empty
        Dim Li As Telerik.Web.UI.RadComboBoxItem
        For Each Li In cblGrade.Items
            Dim chk As CheckBox = DirectCast(Li.FindControl("chkComboItem"), CheckBox)
            Dim hdf As HiddenField = DirectCast(Li.FindControl("hdnComboItem"), HiddenField)
            If chk.Checked = True Then
                ViewState("GetSelectedGrades") = IIf(ViewState("GetSelectedGrades").Trim = "", hdf.Value, ViewState("GetSelectedGrades") + "|" + hdf.Value)
            End If
        Next
        'ViewState("GetSelectedBuses") = cblBusNos.CheckedItems
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ClearDetails()
        Dim qry As String = "SELECT distinct GRM_ID,ISNULL(GRM_DISPLAY,'')+' - '+isnull(sct_descr,'') GRM_DESCR FROM OASIS.dbo.GRADE_BSU_M A " & _
        "LEFT JOIN OASIS.dbo.SECTION_M B ON A.GRM_ID=B.SCT_GRM_ID AND A.GRM_BSU_ID=B.SCT_BSU_ID " & _
        "WHERE GRM_BSU_ID='" & Me.ddlBusinessunit.SelectedValue & "' AND GRM_ACD_ID='" & Me.ddlAcademicYear.SelectedValue & "' "
        Me.cblGrade.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, qry)
        Me.cblGrade.DataBind()
        SelectAllGrades()
        Me.txtDate.Text = Format(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ACD_ENDDT FROM dbo.ACADEMICYEAR_D WHERE ACD_BSU_ID='" & Me.ddlBusinessunit.SelectedValue & "' AND ACD_ID='" & Me.ddlAcademicYear.SelectedValue & "'"), OASISConstants.DateFormat)
    End Sub
    Private Sub SEND_SMS()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim FRH_ID As Long = Encr_decrData.Decrypt(Request.QueryString("FRH_ID").Replace(" ", "+"))
            For Each grow As GridViewRow In gvExcel.Rows
                Dim ChkSelect As HtmlInputCheckBox = CType(grow.FindControl("ChkSelect"), HtmlInputCheckBox)
                If ChkSelect IsNot Nothing AndAlso ChkSelect.Checked AndAlso ChkSelect.Disabled = False Then
                    Dim pParms(5) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@FRH_ID", SqlDbType.BigInt)
                    pParms(0).Value = FRH_ID
                    pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                    pParms(1).Value = Session("sBsuid")
                    pParms(2) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
                    pParms(2).Value = Me.ddlBusinessunit.SelectedValue
                    pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                    pParms(3).Direction = ParameterDirection.ReturnValue
                    pParms(4) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
                    pParms(4).Value = ChkSelect.Value
                    Dim retval As Integer
                    retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[TRANSPORT].[SEND_REMINDER_SMS]", pParms)
                    If pParms(3).Value <> "0" Then
                        'lblError.Text = getErrorMessage(pParms(3).Value)
                        usrMessageBar2.ShowNotification(getErrorMessage(pParms(3).Value), UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        objConn.Close()
                        Exit Sub
                    End If
                End If
            Next
            stTrans.Commit()
            usrMessageBar2.ShowNotification("SMS processed for the selected students", UserControls_usrMessageBar.WarningType.Danger)
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar2.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            objConn.Close()
            'Exit Sub
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        'lblError.Text = getErrorMessage("0")

    End Sub

    Protected Sub gvExcel_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvExcel.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If ViewState("MainMnu_code") = "F300236" Then 'if fee reminder sms
                Try
                    Dim ChkSelect As HtmlInputCheckBox = CType(e.Row.FindControl("ChkSelect"), HtmlInputCheckBox)
                    Dim IsSMSSent As Boolean = Convert.ToBoolean(gvExcel.DataKeys(e.Row.RowIndex)(0))
                    If ChkSelect IsNot Nothing Then
                        ChkSelect.Disabled = IsSMSSent 'if sms already sent, disables check box
                    End If
                Catch ex As Exception

                End Try
            End If
        End If
    End Sub
End Class
