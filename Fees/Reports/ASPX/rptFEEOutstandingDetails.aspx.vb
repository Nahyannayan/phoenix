Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptFEEOutstandingDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim MainMnu_code As String 
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            BindGrade()
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_REP_NORM_FEEOUTSTANDING
                    lblReportCaption.Text = "Fee Outstanding Details"
                Case OASISConstants.MNU_FEE_REP_NORM_FEE_BALANCELIST
                    lblReportCaption.Text = "Fee Balance details"
                    tr_Grade.Visible = True
                Case "F725156"
                    lblReportCaption.Text = "YearEnd Students Dues"
                    trChargeDate.Visible = True
                    trRdbutton.Visible = False
                    ChkGrade.Visible = True
                    txtChargeDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                Case "F725083"
                    lblReportCaption.Text = "Fee Outstanding Details - New"
            End Select

            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If bNoData Then
                txtFromDate.Text = Session("RPTFromDate")
            Else
                txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            End If
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
            'usrMessageBar2.ShowNotification("", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub

    Private Sub BindGrade()
        Try
            Dim conn_str As String = ConnectionManger.GetOASISConnectionString
            Dim sql_query As String = "SELECT DISTINCT GRD_DISPLAY, GRD_ID, GRD_DISPLAYORDER FROM GRADE_M order by GRD_DISPLAYORDER"
            Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, sql_query)
            If (dsData IsNot Nothing) AndAlso (dsData.Tables(0).Rows.Count > 0) Then
                Dim dr As DataRow = dsData.Tables(0).NewRow
                dr(0) = "ALL"
                dr(1) = "ALL"
                dsData.Tables(0).Rows.Add(dr)
            End If
            ddlGrade.DataSource = dsData
            ddlGrade.DataTextField = "GRD_DISPLAY"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
            If ddlGrade.Items.Count > 0 Then
                ddlGrade.Items.FindByValue("ALL").Selected = True
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If UtilityObj.IsFutureDate(txtFromDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Session("RPTFromDate") = txtFromDate.Text
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_NORM_FEEOUTSTANDING
                GenerateFeeOutStandingDetails()
            Case OASISConstants.MNU_FEE_REP_NORM_FEE_BALANCELIST
                GenerateFeeBalanceList()
            Case "F725156"
                YearEndStudentsDues()
            Case "F725083"
                F_FEEOUTSTANDINGSTUDENTWISE_FEES()
            Case "F733041"
                F_FEEOUTSTANDINGSTUDENTWISE_SERVICES()
        End Select
    End Sub
    Private Sub F_FEEOUTSTANDINGSTUDENTWISE_SERVICES()
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim cmd As New SqlCommand("FEES.F_FEEOUTSTANDINGSTUDENTWISE_FEES")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpSTU_BSU_IDs As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 300)
        sqlpSTU_BSU_IDs.Value = UtilityObj.GetBSUnitWithSeperator(UsrBSUnits1.GetSelectedNode(), OASISConstants.SEPARATOR_PIPE)
        cmd.Parameters.Add(sqlpSTU_BSU_IDs)

        Dim sqlpAsOnDT As New SqlParameter("@Dt", SqlDbType.DateTime)
        sqlpAsOnDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpAsOnDT)

        Dim sqlpTyp As New SqlParameter("@Typ", SqlDbType.TinyInt)

        If radDetailed.Checked Or rbParent.Checked Then
            sqlpTyp.Value = 0
        ElseIf radGradeSummary.Checked Then
            sqlpTyp.Value = 1
        ElseIf radSummary.Checked Then
            sqlpTyp.Value = 2
        End If
        cmd.Parameters.Add(sqlpTyp)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDATE") = txtFromDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        'repSource.IncludeBSUImage = True
        If radDetailed.Checked Then
            params("RPT_CAPTION") = "FEE OUTSTANDING STUDENTWISE DETAILS"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_FEEOUTSTANDING_SERVICE.rpt"
        ElseIf Me.rbParent.Checked Then
            params("RPT_CAPTION") = "FEE OUTSTANDING PARENT WISE DETAILS"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_FEEOS_ParentWise_SERVICE.rpt"
        ElseIf radGradeSummary.Checked Then
            params("RPT_CAPTION") = "FEE OUTSTANDING GRADE WISE DETAILS"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_FEEOUTSTANDING_GRADEWISE_SUMMARY_SERVICE.rpt"
        ElseIf radSummary.Checked Then
            params("RPT_CAPTION") = "FEE OUTSTANDING SUMMARY AS ON : " & txtFromDate.Text.ToString()
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_FEEOUTSTANDING_SUMMARY_SERVICE.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub F_FEEOUTSTANDINGSTUDENTWISE_FEES()
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.F_FEEOUTSTANDINGSTUDENTWISE_FEES")
        cmd.CommandType = CommandType.StoredProcedure 

        Dim sqlpSTU_BSU_IDs As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 1000)
        sqlpSTU_BSU_IDs.Value = UtilityObj.GetBSUnitWithSeperator(UsrBSUnits1.GetSelectedNode(), OASISConstants.SEPARATOR_PIPE)
        cmd.Parameters.Add(sqlpSTU_BSU_IDs)

        Dim sqlpAsOnDT As New SqlParameter("@Dt", SqlDbType.DateTime)
        sqlpAsOnDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpAsOnDT)

        Dim sqlpTyp As New SqlParameter("@Typ", SqlDbType.TinyInt)

        If radDetailed.Checked Or rbParent.Checked Then
            sqlpTyp.Value = 0
        ElseIf radGradeSummary.Checked Then
            sqlpTyp.Value = 1
        ElseIf radSummary.Checked Then
            sqlpTyp.Value = 2
        End If
        cmd.Parameters.Add(sqlpTyp)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDATE") = txtFromDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        'repSource.IncludeBSUImage = True
        If radDetailed.Checked Then
            params("RPT_CAPTION") = "FEE OUTSTANDING STUDENTWISE DETAILS"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_FEEOUTSTANDING.rpt"
        ElseIf Me.rbParent.Checked Then
            params("RPT_CAPTION") = "FEE OUTSTANDING PARENT WISE DETAILS"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_FEEOS_ParentWise.rpt"
        ElseIf radGradeSummary.Checked Then
            params("RPT_CAPTION") = "FEE OUTSTANDING GRADE WISE DETAILS"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_FEEOUTSTANDING_GRADEWISE_SUMMARY.rpt"
        ElseIf radSummary.Checked Then
            params("RPT_CAPTION") = "FEE OUTSTANDING SUMMARY AS ON : " & txtFromDate.Text.ToString()
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_FEEOUTSTANDING_SUMMARY.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeBalanceList()
        Dim vType As Int16 = 0
        If radDetailed.Checked Then
            vType = 0
        ElseIf radGradeSummary.Checked Then
            vType = 1
        ElseIf radSummary.Checked Then
            vType = 2
        End If

        Dim cmd As New SqlCommand("[FEES].[F_GETFEEBALANCEREPORT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_IDs", SqlDbType.VarChar, 300)
        sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpDT As New SqlParameter("@DT", SqlDbType.DateTime)
        sqlpDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpDT)

        Dim sqlpGRD_ID As New SqlParameter("@GRD_ID", SqlDbType.VarChar, 20)
        sqlpGRD_ID.Value = ddlGrade.SelectedItem.Value
        cmd.Parameters.Add(sqlpGRD_ID)

        Dim sqlpTyp As New SqlParameter("@Typ", SqlDbType.TinyInt)
        sqlpTyp.Value = vType
        cmd.Parameters.Add(sqlpTyp)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = Session("BSU_Name")
        params("FromDATE") = txtFromDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        Select Case vType
            Case 0
                params("RPT_CAPTION") = " FEE BALANCE LIST DETAILS"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEEOUTSTANDING.rpt"
            Case 1
                params("RPT_CAPTION") = " FEE BALANCE LIST GRADE WISE SUMMARY"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEEOUTSTANDING_GRADE_SUMMARY.rpt"
            Case 2
                params("RPT_CAPTION") = " FEE BALANCE LIST SUMMARY"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEEOUTSTANDING_BSU_SUMMARY.rpt"
        End Select
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeOutStandingDetails()
        Dim vType As Int16 = 0
        If radDetailed.Checked Then
            vType = 0
        ElseIf radGradeSummary.Checked Then
            vType = 1
        ElseIf radSummary.Checked Then
            vType = 2
        ElseIf rbParent.Checked Then
            vType = 3
        End If

        Dim cmd As New SqlCommand("[FEES].[F_GETFEEOUTSTANDINGREPORT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_IDs", SqlDbType.VarChar, 300)
        sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpDT As New SqlParameter("@DT", SqlDbType.DateTime)
        sqlpDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpDT)

        Dim sqlpTyp As New SqlParameter("@Typ", SqlDbType.TinyInt)
        sqlpTyp.Value = vType
        cmd.Parameters.Add(sqlpTyp)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = Session("BSU_Name")
        params("FromDATE") = txtFromDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        Select Case vType
            Case 0
                params("RPT_CAPTION") = " FEE OUTSTANDING DETAILS"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEEOUTSTANDING.rpt"
            Case 1
                params("RPT_CAPTION") = " FEE OUTSTANDING GRADE WISE SUMMARY"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEEOUTSTANDING_GRADE_SUMMARY.rpt"
            Case 2
                params("RPT_CAPTION") = " FEE OUTSTANDING SUMMARY"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEEOUTSTANDING_BSU_SUMMARY.rpt"
            Case 3
                params("RPT_CAPTION") = " FEE OUTSTANDING DETAILS - PARENT WISE"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEEOUTSTANDING_PARENT.rpt"
        End Select

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Private Sub YearEndStudentsDues()
       

        Dim cmd As New SqlCommand("[FEES].[F_GETFEEOUTSTANDING_ADHOC_YEAREND]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpDT As New SqlParameter("@DT", SqlDbType.DateTime)
        sqlpDT.Value = CDate(txtChargeDate.Text)
        cmd.Parameters.Add(sqlpDT)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_IDs", SqlDbType.VarChar, 500)
        sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpCuttOff As New SqlParameter("@CutOffDt", SqlDbType.DateTime)
        sqlpCuttOff.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpCuttOff)



        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = "YearEnd Students Dues Details"
        params("DateHead") = "As on Date :" & txtFromDate.Text.ToString() '& "  Charge till Date : " & txtChargeDate.Text.ToString()
        params("DateCharge") = "Charge till Date :" & txtChargeDate.Text.ToString()
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If ChkGrade.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/YearendStudentsDuesClass.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/YearendStudentsDues.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
