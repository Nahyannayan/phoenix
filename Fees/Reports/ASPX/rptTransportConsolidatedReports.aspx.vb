Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptFEEOutstandingDetails
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTFromDate") = txtFromDate.Text
        If UsrTransportBSUnits1.GetSelectedNode() = String.Empty Then
            'lblError.Text = "***Please select atleast one business Unit***"
            usrMessageBar.ShowNotification("***Please select atleast one business Unit***", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEEOUTSTANDING, _
            OASISConstants.MNU_FEE_REP_TRANSPORT_FEEOUTSTANDING_CONSOLIDATED
                GenerateFeeOutStandingDetails()
            Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEEADVANCE_CONSOLIDATED, _
            OASISConstants.MNU_FEE_REP_TRANSPORT_MONTHLY_ADVANCESUMMARY
                GenerateFeeAdvanceReport()
            Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEEADJUSTMENT_CONSOLIDATED
                GenerateFeeAdjustmentReport()
            Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEECONCESSION_CONSOLIDATED
                GenerateFEEConcession()
            Case OASISConstants.MNU_FEE_TRANS_REP_CHARGE_OVERALL
                GenerateFeeChargeOverAllReport()
            Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEERECONCILIATION_CONSOLIDATED
                GenerateFeeReconciliationReport()
        End Select
    End Sub

    Private Sub GenerateFeeReconciliationReport()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_GETFEERECONCILIATIONCONSOLIDATED]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTUBSU_ID As New SqlParameter("@STU_BSU_IDs", SqlDbType.VarChar, 1000)
        sqlpSTUBSU_ID.Value = UtilityObj.GetBSUnitWithSeperator(UsrTransportBSUnits1.GetSelectedNode(), OASISConstants.SEPARATOR_PIPE)
        cmd.Parameters.Add(sqlpSTUBSU_ID)

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpExclude As New SqlParameter("@BExcludePDc", SqlDbType.Bit)
        If chkExclude.Checked = True Then
            sqlpExclude.Value = 1
        Else
            sqlpExclude.Value = 0
        End If
        cmd.Parameters.Add(sqlpExclude)


        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        Dim monthDesc As String = CDate(txtFromDate.Text).ToString("MMMM").ToUpper
        If chkExclude.Checked = False Then
            params("RPT_CAPTION") = "TRANSPORT FEE RECONCILIATION CONSOLIDATED REPORT WITH PDC"
        Else
            params("RPT_CAPTION") = "TRANSPORT FEE RECONCILIATION CONSOLIDATED REPORT WITHOUT PDC"
        End If
        'repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEERECONCILIATION_CONSOLIDATED.rpt"
        If chkExclude.Checked = False Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEERECONCIL_CONSOLIDATED.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEERECONCIL_CONSOLIDATED_EXCLUDE_PDC.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub


    Private Sub GenerateFeeAdvanceReport()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand("FEES.[F_FEEADVANCESTUDENTWISE]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTU_BSU_IDs As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 1000)
        sqlpSTU_BSU_IDs.Value = UtilityObj.GetBSUnitWithSeperator(UsrTransportBSUnits1.GetSelectedNode(), OASISConstants.SEPARATOR_PIPE)
        cmd.Parameters.Add(sqlpSTU_BSU_IDs)

        Dim sqlpAsOnDT As New SqlParameter("@Dt", SqlDbType.DateTime)
        sqlpAsOnDT.Value = CDate(txtAsOnDate.Text)
        cmd.Parameters.Add(sqlpAsOnDT)

        Dim sqlpTyp As New SqlParameter("@Typ", SqlDbType.TinyInt)
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If radDetailed.Checked Then
            sqlpTyp.Value = 0
        ElseIf radGradeSummary.Checked Then
            sqlpTyp.Value = 1
        ElseIf radSummary.Checked Then
            sqlpTyp.Value = 2
        End If
        cmd.Parameters.Add(sqlpTyp)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDATE") = txtAsOnDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If radDetailed.Checked Then
            params("RPT_CAPTION") = " TRANSPORT FEE ADVANCE DETAILS"

            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEEADVANCE.rpt"
        ElseIf radGradeSummary.Checked Then
            params("RPT_CAPTION") = " TRANSPORT FEE ADVANCE GRADE WISE DETAILS"

            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEEADVANCE_GRADEWISE_SUMMARY.rpt"
            'params("bSummary") = False
        ElseIf radSummary.Checked Then
            params("RPT_CAPTION") = " TRANSPORT FEE ADVANCE SUMMARY"

            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEEADVANCE_SUMMARY.rpt"
            'params("bSummary") = True
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeChargeOverAllReport()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand
        cmd = New SqlCommand("[FEES].[F_GETTRANSPORTFEECHARGE_OVER_ALL]")

        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.Xml)
        sqlpBSU_ID.Value = UtilityObj.GenerateXML(UsrTransportBSUnits1.GetSelectedNode(), XMLType.BSUName)
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFROMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFROMDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FROMDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        params("RPT_CAPTION") = "FEE CHARGE REPORT(CONSOLIDATED)"
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEECHARGEREPORT_SUMMARY.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Private Sub GenerateFEEConcession()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        Dim cmd As New SqlCommand("FEES.[F_GETTRANSPORTCONCESSIONDETAILS]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTU_BSU_IDs As New SqlParameter("@STU_BSU_IDs", SqlDbType.Xml)
        sqlpSTU_BSU_IDs.Value = UtilityObj.GenerateXML(UsrTransportBSUnits1.GetSelectedNode(), XMLType.BSUName)
        cmd.Parameters.Add(sqlpSTU_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpbSummary As New SqlParameter("@bSummary", SqlDbType.Bit)
        sqlpbSummary.Value = 1
        cmd.Parameters.Add(sqlpbSummary)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        params("BSU_NAME") = "GEMS"
        repSource.IncludeBSUImage = True
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptTransportFEECONCESSION_Summary.rpt"
         Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeAdjustmentReport()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand("FEES.[F_GETREPORTFEEADJUSTMENT_CONSOLIDATED]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTUBSU_ID As New SqlParameter("@STU_BSU_IDs", SqlDbType.Xml)
        sqlpSTUBSU_ID.Value = UtilityObj.GenerateXML(UsrTransportBSUnits1.GetSelectedNode(), XMLType.BSUName)
        cmd.Parameters.Add(sqlpSTUBSU_ID)

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True

        params("RPT_CAPTION") = "TRANSPORT FEE ADJUSTMENT REPORT (CONSOLIDATED)"
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEECHARGEREPORT_SUMMARY.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    'Private Sub GenerateFeeAdvanceReport()
    '    Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    '    Dim cmd As New SqlCommand("FEES.[F_GET_TRANSPORTFEEADVANCE_CONSOLIDATED]")
    '    cmd.CommandType = CommandType.StoredProcedure

    '    Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
    '    sqlpBSU_ID.Value = Session("sBSUID")
    '    cmd.Parameters.Add(sqlpBSU_ID)

    '    Dim sqlpSTUBSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.Xml)
    '    sqlpSTUBSU_ID.Value = UtilityObj.GenerateXML(UsrTransportBSUnits1.GetSelectedNode(), XMLType.BSUName)
    '    cmd.Parameters.Add(sqlpSTUBSU_ID)

    '    Dim sqlpASOnDT As New SqlParameter("@ASOnDT", SqlDbType.DateTime)
    '    sqlpASOnDT.Value = CDate(txtAsOnDate.Text)
    '    cmd.Parameters.Add(sqlpASOnDT)

    '    cmd.Connection = New SqlConnection(str_conn)
    '    Dim repSource As New MyReportClass
    '    Dim params As New Hashtable
    '    params("userName") = Session("sUsr_name")
    '    params("FromDT") = txtAsOnDate.Text
    '    params("TODT") = ""
    '    repSource.Parameter = params
    '    repSource.Command = cmd
    '    repSource.IncludeBSUImage = True

    '    params("RPT_CAPTION") = "TRANSPORT FEE ADVANCE REPORT(CONSOLIDATED)"
    '    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEECHARGEREPORT_SUMMARY.rpt"
    '    Session("ReportSource") = repSource
    '    Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
    'End Sub

    Private Sub GenerateFeeOutStandingDetails()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand("FEES.F_FEEOUTSTANDINGSTUDENTWISE")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTU_BSU_IDs As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 1000)
        sqlpSTU_BSU_IDs.Value = UtilityObj.GetBSUnitWithSeperator(UsrTransportBSUnits1.GetSelectedNode(), OASISConstants.SEPARATOR_PIPE)
        cmd.Parameters.Add(sqlpSTU_BSU_IDs)

        Dim sqlpAsOnDT As New SqlParameter("@Dt", SqlDbType.DateTime)
        sqlpAsOnDT.Value = CDate(txtAsOnDate.Text)
        cmd.Parameters.Add(sqlpAsOnDT)

        Dim sqlpTyp As New SqlParameter("@Typ", SqlDbType.TinyInt)
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If radDetailed.Checked Then
            sqlpTyp.Value = 0
        ElseIf radGradeSummary.Checked Then
            sqlpTyp.Value = 1
        ElseIf radSummary.Checked Then
            sqlpTyp.Value = 2
        End If
        cmd.Parameters.Add(sqlpTyp)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDATE") = txtAsOnDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If radDetailed.Checked Then
            params("RPT_CAPTION") = " TRANSPORT FEE OUTSTANDING STUDENTWISE DETAILS"
            If radBuswise.Checked Then
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEEOUTSTANDING_BUSWISE.rpt"
            ElseIf radGradeWise.Checked Then
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEEOUTSTANDING.rpt"
            End If
        ElseIf radGradeSummary.Checked Then
            params("RPT_CAPTION") = " TRANSPORT FEE OUTSTANDING GRADE WISE DETAILS"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEEOUTSTANDING_GRADEWISE_SUMMARY.rpt"
            'params("bSummary") = False
        ElseIf radSummary.Checked Then
            params("RPT_CAPTION") = " TRANSPORT FEE OUTSTANDING SUMMARY AS ON : " & txtAsOnDate.Text.ToString()
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEEOUTSTANDING_SUMMARY.rpt"
            'params("bSummary") = True
        End If
            Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    'Private Sub GenerateFeeOutStandingDetails()
    '    Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
    '    Dim cmd As New SqlCommand("[FEES].[F_GETTRANSPORTFEEOUTSTANDINGREPORT_CONSOLIDATED]")
    '    cmd.CommandType = CommandType.StoredProcedure

    '    Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
    '    sqlpBSU_ID.Value = Session("sBSUID")
    '    cmd.Parameters.Add(sqlpBSU_ID)

    '    Dim sqlpSTU_BSU_IDs As New SqlParameter("@STU_BSU_IDs", SqlDbType.Xml)
    '    sqlpSTU_BSU_IDs.Value = UtilityObj.GenerateXML(UsrTransportBSUnits1.GetSelectedNode(), XMLType.BSUName)
    '    cmd.Parameters.Add(sqlpSTU_BSU_IDs)

    '    Dim sqlpAsOnDT As New SqlParameter("@AsOnDT", SqlDbType.DateTime)
    '    sqlpAsOnDT.Value = CDate(txtAsOnDate.Text)
    '    cmd.Parameters.Add(sqlpAsOnDT)

    '    cmd.Connection = New SqlConnection(str_conn)
    '    Dim repSource As New MyReportClass
    '    Dim params As New Hashtable
    '    params("userName") = Session("sUsr_name")
    '    params("FromDATE") = txtAsOnDate.Text
    '    params("TODATE") = txtAsOnDate.Text
    '    repSource.Parameter = params
    '    repSource.Command = cmd
    '    repSource.IncludeBSUImage = True
    '    params("RPT_CAPTION") = " FEE OUTSTANDING DETAILS(CONSOLIDATED)"
    '    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEEOUTSTANDING_CONSOLIDATED.rpt"
    '    Session("ReportSource") = repSource
    '    Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
    'End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If

            trPeriod.Visible = True
            trAsonDate.Visible = False
            trSummary.Visible = False
            trExclude.Visible = False
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEEADVANCE_CONSOLIDATED, _
                    OASISConstants.MNU_FEE_REP_TRANSPORT_MONTHLY_ADVANCESUMMARY
                    lblReportCaption.Text = "Transport Fee Advance Report"
                    trAsonDate.Visible = True
                    trPeriod.Visible = False
                    trSummary.Visible = True
                Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEEADJUSTMENT_CONSOLIDATED
                    lblReportCaption.Text = "Fee Adjustments Consolidated Report"
                    'Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEEOUTSTANDING_CONSOLIDATED
                    '    trAsonDate.Visible = True
                    '    trPeriod.Visible = False
                    '    lblReportCaption.Text = "Fee Outstanding Consolidated Report"
                    '    trSummary.Visible = True
                Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEEOUTSTANDING, _
                OASISConstants.MNU_FEE_REP_TRANSPORT_FEEOUTSTANDING_CONSOLIDATED
                    trAsonDate.Visible = True
                    trPeriod.Visible = False
                    lblReportCaption.Text = "Transport Fee Outstanding Report"
                    trSummary.Visible = True
                Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEECONCESSION_CONSOLIDATED
                    lblReportCaption.Text = "Fee Concession Consolidated Report"
                Case OASISConstants.MNU_FEE_TRANS_REP_CHARGE_OVERALL
                    lblReportCaption.Text = "Fee Charge Consolidated Report"
                Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEERECONCILIATION_CONSOLIDATED
                    trAsonDate.Visible = False
                    trPeriod.Visible = True
                    trSummary.Visible = False
                    trSortby.Visible = False
                    trExclude.Visible = True
                    lblReportCaption.Text = "Transport Fee Reconciliation Report"
            End Select

            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If bNoData Then
                txtFromDate.Text = Session("RPTFromDate")
                txtToDate.Text = Session("RPTToDate")
                txtAsOnDate.Text = Session("RPTToDate")
            Else
                txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                txtFromDate.Text = Format(DateTime.Now.AddMonths(-3), OASISConstants.DateFormat)
                txtAsOnDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            End If
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
