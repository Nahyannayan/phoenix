Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptMultiSportsSeriesCollection
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            lblReportCaption.Text = "Multi Sports Series Collection Details"
            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        GenerateCollectionReport()
    End Sub

    Private Sub GenerateCollectionReport()
        Dim cmd As New SqlCommand("[EVT].[GET_FEE_COLLECTION_DETAIL]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlParam(6) As SqlClient.SqlParameter
        sqlParam(0) = New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlParam(0).Value = Session("sBsuid")
        cmd.Parameters.Add(sqlParam(0))

        sqlParam(1) = New SqlParameter("@FROMDT", SqlDbType.VarChar)
        sqlParam(1).Value = txtFromDate.Text
        cmd.Parameters.Add(sqlParam(1))

        sqlParam(2) = New SqlParameter("@TODT", SqlDbType.VarChar)
        sqlParam(2).Value = txtToDate.Text
        cmd.Parameters.Add(sqlParam(2))

        cmd.Connection = New SqlConnection(ConnectionManager.getConnection("OASIS_ENRConnectionString").ConnectionString)

        'Dim ds As DataTable = Mainclass.getDataTable("[FEES].[F_GetFeeRefundDetails]", sqlParam, cmd.Connection.ConnectionString)
        'SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "[FEES].[F_GetFeeRefundDetails]", cmd.Parameters)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        Dim caption As String = "MULTI SPORTS SERIES FEE COLLECTION DETAILS"

        params("userName") = Session("sUsr_name")
        params("RptHead") = caption
        params("FromDt") = Me.txtFromDate.Text
        params("ToDt") = Me.txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = Session("sBsuid").ToString
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_MultiSports_Collection.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx?isExport=true", True)
        Else
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
