<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTransportFEEChargeDiscrepancies.aspx.vb" Inherits="Fees_Reports_ASPX_rptTransportFEEChargeDiscrepancies" Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ Register Src="../../../UserControls/usrTransportBSUnits.ascx" TagName="usrTransportBSUnits"
    TagPrefix="uc2" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
           <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                <br />
                <table width="100%">
                    <tr>
                        <td align="left" class="matters" colspan="4">
                            <asp:RadioButton ID="radStud" runat="server" Checked="True" GroupName="STUD_ENQ" CssClass="field-label"
                                Text="Student" />
                            <asp:RadioButton ID="radEnq" runat="server" GroupName="STUD_ENQ" Text="Enquiry" CssClass="field-label" /></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" valign="top"  ><span class="field-label">Business Unit</span></td>
                        <td align="left" class="matters" colspan="3" valign="top">
                            <div class="checkbox-list">

                            <uc2:usrTransportBSUnits ID="UsrTransportBSUnits1" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">From Date</span></td>
                        <td align="left" class="matters" width="30%" >
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputbox"  ></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                        ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                            ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                            ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" class="matters" width="20%"><span class="field-label">To Date</span></td>
                        <td align="left" class="matters"  width="30%" >
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="inputbox"  ></asp:TextBox>
                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" /><asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                    runat="server" ControlToValidate="txtToDate" ErrorMessage="To Date required"
                                    ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="4">
                            <asp:RadioButton ID="radClassWise" runat="server" Checked="True" GroupName="GROUP" CssClass="field-label"
                                Text="Class wise"></asp:RadioButton><asp:RadioButton ID="radBusWise" runat="server" GroupName="GROUP" CssClass="field-label"
                                    Text="Bus wise"></asp:RadioButton></td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4"  >
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>

