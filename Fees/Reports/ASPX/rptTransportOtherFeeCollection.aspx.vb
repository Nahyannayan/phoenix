Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Fees_Reports_ASPX_rptTransportOtherFeeCollection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            UsrBSUnits1.MenuCode = MainMnu_code
            Select Case MainMnu_code
                Case "F715076"
                    lblReportCaption.Text = "Other FeeCollection (Transport)"
                    UsrBSUnits1.Visible = False
            End Select
            ViewState("FEE_ID") = ""
            LoadFeeTypes()
        End If

    End Sub
    Sub LoadFeeTypes()
        Dim ds As DataSet
        Dim qry As String = "select FEE_ID,FEE_DESCR from FEES.FEES_M where FEE_ID in(132,49,8,6,85)"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, qry)
        Me.rblFilter.DataTextField = "FEE_DESCR"
        Me.rblFilter.DataValueField = "FEE_ID"

        Me.rblFilter.DataSource = ds.Tables(0)
        Me.rblFilter.DataBind()
    End Sub

    Sub getselected()
        ViewState("FEE_ID") = ""
        For Each Li As ListItem In rblFilter.Items
            If Li.Selected = True Then
                ViewState("FEE_ID") = IIf(ViewState("FEE_ID") = "", Li.Value, ViewState("FEE_ID") + "|" + Li.Value)
            End If
        Next
    End Sub

    Sub TransportOnline()
        If UsrTransportBSUnits1.GetSelectedNode = "" Then
            '   lblError.Text = "Please Select Business Unit..!"
            usrMessageBar.ShowNotification("Please Select Business Unit..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        getselected()
        If ViewState("FEE_ID") = "" Then
            '      lblError.Text = "Please Select Fee Type..!"
            usrMessageBar.ShowNotification("Please Select Fee Type..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim Mode As Boolean = False
        Dim Head As String = "Other Fee Collections Details"
        Dim RptName = "../../FEES/REPORTS/RPT/RptTransportOtherFeeCollection.rpt"
        If Me.RBLConsolidation.SelectedValue = 2 Then 'Consolidate IncomeHeads as Row
            Mode = True
            RptName = "../../FEES/REPORTS/RPT/RptTransportOtherFeeCollectionConsolidate.rpt"
            Head = "Other Fee Collections Summary"
        ElseIf Me.RBLConsolidation.SelectedValue = 1 Then 'Consolidate IncomeHeads as Column
            Mode = True
            RptName = "../../FEES/REPORTS/RPT/RptTransportOtherFeeCollectionCrossTab.rpt"
            Head = "Other Fee Collections Summary"
        End If

        Dim cmd As New SqlCommand("Transport.OtherFeeCollectionReport")
        cmd.CommandType = CommandType.StoredProcedure
        Dim pBSU_ID As New SqlParameter("@BSU_ID", Session("sBsuid"))
        Dim pDTFRom As New SqlParameter("@DTFRom", txtFromDate.Text)
        Dim pDTTo As New SqlParameter("@DTTo", txtToDate.Text)
        Dim pSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", UsrTransportBSUnits1.GetSelectedNode.Replace("||", "@"))
        Dim pMODE As New SqlParameter("@MODE", Mode)
        Dim pFilter As New SqlParameter("@FEE_ID", ViewState("FEE_ID"))
        Dim pConsolidateType As New SqlParameter("@ConsolidateType", Me.RBLConsolidation.SelectedValue)
        cmd.Parameters.Add(pBSU_ID)
        cmd.Parameters.Add(pDTFRom)
        cmd.Parameters.Add(pDTTo)
        cmd.Parameters.Add(pSTU_BSU_ID)
        cmd.Parameters.Add(pMODE)
        cmd.Parameters.Add(pFilter)
        cmd.Parameters.Add(pConsolidateType)
        Dim Params As New Hashtable
        Params("RptHead") = Head
        Params("DateHead") = "For the period " & txtFromDate.Text & " to " & txtToDate.Text
        Params("userName") = Session("sUsr_name")
        cmd.Connection = New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        Dim RptClass As New MyReportClass
        RptClass.IncludeBSUImage = True
        RptClass.HeaderBSUID = Session("sBsuid")
        RptClass.Command = cmd
        RptClass.Parameter = Params
        RptClass.ResourceName = RptName
        Session("ReportSource") = RptClass
        ReportLoadSelection()
        ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub BtnShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnShow.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "F715076"
                TransportOnline()
        End Select
    End Sub


End Class
