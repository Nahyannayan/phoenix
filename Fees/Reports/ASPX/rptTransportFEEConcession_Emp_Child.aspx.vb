Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptTransportFEEConcession_Emp_Child
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If

            Select Case MainMnu_code
                Case "F733031"
                    lblReportCaption.Text = "Transport Fee Concession for Staff's Children"
            End Select

            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            
            txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            txtFromDate.Text = Format(DateTime.Now.AddMonths(-3), OASISConstants.DateFormat)
            usrBSUnits1.MenuCode = MainMnu_code
            BindAcademicYear()

        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
    End Sub

    Private Sub BindAcademicYear()
        Try
            Dim conn_str As String = ConnectionManger.GetOASISConnectionString
            Dim sql_query As String = "SELECT TOP 4 ACY_ID,ACY_DESCR FROM dbo.ACADEMICYEAR_M WHERE ISNULL(ACY_bSHOW,0)=1 ORDER BY ACY_ID DESC"
            Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, sql_query)

            Dim ACY_ID_CURRENT As Int16 = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT MAX(ACD_ACY_ID) FROM dbo.ACADEMICYEAR_D WHERE GETDATE() BETWEEN ACD_STARTDT AND ACD_ENDDT")

            ddlAcademicYear.DataSource = dsData
            ddlAcademicYear.DataTextField = "ACY_DESCR"
            ddlAcademicYear.DataValueField = "ACY_ID"
            ddlAcademicYear.DataBind()
            If ddlAcademicYear.Items.Count > 0 Then
                ddlAcademicYear.Items.FindByValue(ACY_ID_CURRENT).Selected = True
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "F733031"
                GenerateStaffChildrenTransportFeeConcession()
        End Select
    End Sub

    Private Sub GenerateStaffChildrenTransportFeeConcession()
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.RPT_STUDENT_CONCESSION_REQUEST")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFRMDT As New SqlParameter("@FROM_DT", SqlDbType.VarChar)
        sqlpFRMDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TO_DT", SqlDbType.VarChar)
        sqlpTODT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpACY_ID As New SqlParameter("@ACY_ID", SqlDbType.Int)
        sqlpACY_ID.Value = ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpACY_ID)

        Dim sqlpSTATUS As New SqlParameter("@STATUS", SqlDbType.VarChar, 2)
        sqlpSTATUS.Value = ddlStatus.SelectedValue
        cmd.Parameters.Add(sqlpSTATUS)

        Dim sqlpEMPLOYEE_BSUIDs As New SqlParameter("@EMPLOYEE_BSUIDs", SqlDbType.VarChar)
        sqlpEMPLOYEE_BSUIDs.Value = Me.usrBSUnits1.GetSelectedNode.Replace("||", "|")
        cmd.Parameters.Add(sqlpEMPLOYEE_BSUIDs)

        Dim sqlpSTUBSU_ID As New SqlParameter("@STUDENT_BSUIDs", SqlDbType.VarChar)
        sqlpSTUBSU_ID.Value = Me.UsrTransportBSUnits1.GetSelectedNode().Replace("||", "|")
        cmd.Parameters.Add(sqlpSTUBSU_ID)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RPT_CAPTION") = "Transport Fee Concession Request for Staff's Children"
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = False
        Dim monthDesc As String = CDate(txtFromDate.Text).ToString("MMMM").ToUpper

        If rblGroupby.SelectedValue = "NE" Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEConcession_Emp_Child.rpt"
        ElseIf rblGroupby.SelectedValue = "EU" Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEConcession_Emp_Child_EmpBSU.rpt"
        ElseIf rblGroupby.SelectedValue = "SU" Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEConcession_Emp_Child_StuBSU.rpt"
        ElseIf rblGroupby.SelectedValue = "ST" Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEConcession_Emp_Child_Status.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
