Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_rptStudentAgingSummary
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            txtFromDate.Text = Format(Date.Now, "dd/MMM/yyyy")
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_STUDENT_AGING_SUMMARY
                    lblReportCaption.Text = "Fee Ageing Summary"
                Case "F700012"
                    lblReportCaption.Text = "FEE AGEING SUMMARY (Consolidated)"
                    chkGradeSummary.Visible = False
                    chkGradeSummary.Checked = False
                    chkBSUSummary.Checked = True
                    chkBSUSummary.Enabled = False
            End Select


            Page.Title = OASISConstants.Gemstitle
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTFromDate") = txtFromDate.Text
        If UtilityObj.IsFutureDate(txtFromDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If IsDate(txtFromDate.Text) Then
            If IsDate(txtFromDate.Text) And IsDate(ViewState("DTFROM")) Then
                If CDate(txtFromDate.Text) < CDate(ViewState("DTFROM")) Or CDate(txtFromDate.Text) > CDate(ViewState("DTTO")) Then
                    'lblError.Text = "Invalid Date"
                    usrMessageBar2.ShowNotification("Invalid Date", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            End If
        Else
            'lblError.Text = "Invalid Date (Check Academic Year atart date and end date)"
            usrMessageBar2.ShowNotification("Invalid Date (Check Academic Year atart date and end date)", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_STUDENT_AGING_SUMMARY
                rptAGINGSummary_BSU()
            Case "F700012"
                rptAGINGSummary_BSU()
        End Select
    End Sub


    Private Sub rptAGINGSummary_BSU()
        Dim str_bsuids As String = UsrBSUnits1.GetSelectedNode("|")
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If str_bsuids = "" Then
            'lblError.Text = "***Please select atleast one business Unit***"
            usrMessageBar2.ShowNotification("***Please select atleast one business Unit***", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.rptAGINGSummary_BSU")
        cmd.CommandType = CommandType.StoredProcedure
        'EXEC FEES.AGINGSummary @BSU_ID='125016',
        '@FROMDOCDT='12-AUG-2008' ,@Bkt1=30,
        '@Bkt2 =60,
        '@Bkt3 =90,
        '@Bkt4 =120,
        '@Bkt5 =180  
        cmd.Parameters.AddWithValue("@Bkt1", txtBkt1.Text)
        cmd.Parameters.AddWithValue("@Bkt2", txtBkt2.Text)
        cmd.Parameters.AddWithValue("@Bkt3", txtBkt3.Text)
        cmd.Parameters.AddWithValue("@Bkt4", txtBkt4.Text)
        cmd.Parameters.AddWithValue("@Bkt5", txtBkt5.Text)
        cmd.Parameters.AddWithValue("@Bkt6", txtBkt6.Text)
        cmd.Parameters.AddWithValue("@FilterBy", Me.rblStuStatus.SelectedValue)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpMode As New SqlParameter("@Mode", SqlDbType.VarChar, 15)
        If chkGradeSummary.Checked Or chkBSUSummary.Checked Then
            sqlpMode.Value = "GRADE"
            'ElseIf chkBSUSummary.Checked Then
            '    sqlpMode.Value = "BSU"
        Else
            sqlpMode.Value = "STUDENT"
        End If
        cmd.Parameters.Add(sqlpMode)

        Dim sqlpSTU_BSU_IDS As New SqlParameter("@BSU_IDS", SqlDbType.VarChar, 400)
        sqlpSTU_BSU_IDS.Value = str_bsuids
        cmd.Parameters.Add(sqlpSTU_BSU_IDS)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = Session("BSU_Name")
        params("FromDATE") = txtFromDate.Text
        params("BKT1") = "0 - " & txtBkt1.Text
        params("BKT2") = CInt(txtBkt1.Text) + 1 & " - " & txtBkt2.Text
        params("BKT3") = CInt(txtBkt2.Text) + 1 & " - " & txtBkt3.Text
        params("BKT4") = CInt(txtBkt3.Text) + 1 & " - " & txtBkt4.Text
        params("BKT5") = CInt(txtBkt4.Text) + 1 & " - " & txtBkt5.Text
        params("BKT6") = CInt(txtBkt5.Text) + 1 & " - " & txtBkt6.Text
        params("BKT7") = " > " & CInt(txtBkt6.Text) + 1
        params("Currency") = Session("BSU_CURRENCY")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = Session("sBsuid")
        If chkGradeSummary.Checked Then
            params("RPT_CAPTION") = "Student Fee Ageing"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rpfFeeAgingSummaryBSU_Grade.rpt"
        ElseIf chkBSUSummary.Checked Then
            params("RPT_CAPTION") = "Student Fee Ageing Summary by Businessunit"
            If MainMnu_code = "F700012" Then
                params("RPT_CAPTION") = "FEE AGEING SUMMARY (Consolidated)"
            End If
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rpfFeeAgingSummaryBSU_BSU.rpt"
        Else
            params("RPT_CAPTION") = "Student Fee Ageing"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rpfFeeAgingSummaryBSU_Student.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub chkBSUSummary_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkGradeSummary.Checked Then
            chkGradeSummary.Checked = False
        End If
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
