Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Fees_Reports_ASPX_rptFEEOutstandingDetailsGrade
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Dim MainMnu_code As String = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            BindTree_GradeSection(Session("current_ACD_ID"))
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Select Case MainMnu_code
                Case "F725125"
                    lblReportCaption.Text = "Students Due List"
                    FillShifts()
                    TrAcademic.Visible = True
                    FillACD()
                Case "F725155"
                    tr_Gender.Visible = True
                    lblReportCaption.Text = "Students Due/Advance List"
                    FillShifts()
            End Select
            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If bNoData Then
                txtFromDate.Text = Session("RPTFromDate")
            Else
                txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            End If
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub
    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSuid"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If UtilityObj.IsFutureDate(txtFromDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Session("RPTFromDate") = txtFromDate.Text
        Select Case MainMnu_code
            Case "F725125"
                F_GETFEEOUTSTANDINGREPORTGrade()
            Case "F725155"
                F_GETFEEOUTSTANDINGREPORTGrade_gender()
        End Select
    End Sub

    Sub FillShifts()
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim SqlStr As String = "SELECT SHF_CODE,SHF_DESCR FROM SHIFTS_M WHERE SHF_BSU_ID = '" & Session("sBsuid") & "' GROUP BY SHF_CODE,SHF_DESCR ORDER BY SHF_DESCR "
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.Text, SqlStr)
        ddShifts.DataSource = ds.Tables(0)
        ddShifts.DataTextField = "SHF_DESCR"
        ddShifts.DataValueField = "SHF_CODE"
        ddShifts.DataBind()
        ddShifts.Items.Insert(0, "")
        ddShifts.SelectedValue = "0"
    End Sub

    Private Sub F_GETFEEOUTSTANDINGREPORTGrade()
        'Get grade and section
        Dim checkedNodes As TreeNodeCollection = tvGrade.CheckedNodes
        Dim str_Grades As String = ""
        Dim str_Section As String = ""
        If tvGrade.CheckedNodes.Count > 0 Then
            For Each node As TreeNode In tvGrade.CheckedNodes
                If Not node Is Nothing Then
                    If node.Text <> "Select All" Then
                        If node.Parent.Text = "Select All" Then '  node.Parent.Value, node.Value
                            If str_Grades = "" Then
                                str_Grades = node.Value
                            Else
                                str_Grades = str_Grades & "|" & node.Value
                            End If
                        Else
                            If str_Section = "" Then
                                str_Section = node.Value
                            Else
                                str_Section = str_Section & "|" & node.Value
                            End If
                        End If
                    End If
                End If
            Next
        End If

        Dim cmd As New SqlCommand("FEES.F_GETFEEOUTSTANDINGREPORTGrade")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpDT As New SqlParameter("@DT", SqlDbType.DateTime)
        sqlpDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpDT)

        Dim sqlpGrades As New SqlParameter("@Grades", SqlDbType.VarChar)
        sqlpGrades.Value = str_Grades
        cmd.Parameters.Add(sqlpGrades)

        Dim sqlpSections As New SqlParameter("@Sections", SqlDbType.VarChar)
        sqlpSections.Value = str_Section
        cmd.Parameters.Add(sqlpSections)

        Dim sqlpMode As New SqlParameter("@Mode", SqlDbType.Int)
        If chkAll.Checked Then
            sqlpMode.Value = 1
        Else
            sqlpMode.Value = 0
        End If
        cmd.Parameters.Add(sqlpMode)

        Dim sqlpSHFCODE As New SqlParameter("@SHF_CODE", SqlDbType.VarChar)
        sqlpSHFCODE.Value = "%"
        If ddShifts.SelectedItem.Text <> "" Then
            sqlpSHFCODE.Value = ddShifts.SelectedItem.Value
        End If
        cmd.Parameters.Add(sqlpSHFCODE)
        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDATE") = txtFromDate.Text
        params("ShiftHead") = ""
        params("RoundOffVal") = Session("BSU_ROUNDOFF")
        If ddShifts.SelectedItem.Text <> "" Then
            params("ShiftHead") = " ( " & ddShifts.SelectedItem.Text & " ) "
        End If
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEEOUTSTANDING_M.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub F_GETFEEOUTSTANDINGREPORTGrade_gender()
        'Get grade and section
        Dim checkedNodes As TreeNodeCollection = tvGrade.CheckedNodes
        Dim str_Grades As String = ""
        Dim str_Section As String = ""
        If tvGrade.CheckedNodes.Count > 0 Then
            For Each node As TreeNode In tvGrade.CheckedNodes
                If Not node Is Nothing Then
                    If node.Text <> "Select All" Then
                        If node.Parent.Text = "Select All" Then '  node.Parent.Value, node.Value
                            If str_Grades = "" Then
                                str_Grades = node.Value
                            Else
                                str_Grades = str_Grades & "|" & node.Value
                            End If
                        Else
                            If str_Section = "" Then
                                str_Section = node.Value
                            Else
                                str_Section = str_Section & "|" & node.Value
                            End If
                        End If
                    End If
                End If
            Next
        End If

        Dim cmd As New SqlCommand("FEES.F_GETFEEOUTSTANDINGREPORTGrade_gender")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpDT As New SqlParameter("@DT", SqlDbType.DateTime)
        sqlpDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpDT)

        Dim sqlpGrades As New SqlParameter("@Grades", SqlDbType.VarChar)
        sqlpGrades.Value = str_Grades
        cmd.Parameters.Add(sqlpGrades)

        Dim sqlpSections As New SqlParameter("@Sections", SqlDbType.VarChar)
        sqlpSections.Value = str_Section
        cmd.Parameters.Add(sqlpSections)

        Dim sqlpMode As New SqlParameter("@Mode", SqlDbType.Int)
        If chkAll.Checked Then
            sqlpMode.Value = 1
        Else
            sqlpMode.Value = 0
        End If
        cmd.Parameters.Add(sqlpMode)

        Dim sqlpSHFCODE As New SqlParameter("@SHF_CODE", SqlDbType.VarChar)
        sqlpSHFCODE.Value = "%"
        If ddShifts.SelectedItem.Text <> "" Then
            sqlpSHFCODE.Value = ddShifts.SelectedItem.Value
        End If
        cmd.Parameters.Add(sqlpSHFCODE)

        Dim sqlpGender As New SqlParameter("@Gender", SqlDbType.VarChar)
        sqlpGender.Value = ddlGender.SelectedItem.Value
        cmd.Parameters.Add(sqlpGender)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDATE") = txtFromDate.Text
        params("ShiftHead") = ""
        If ddShifts.SelectedItem.Text <> "" Then
            params("ShiftHead") = " ( " & ddShifts.SelectedItem.Text & " ) "
        End If
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEEOUTSTANDING_M_Grade.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub 

    Sub BindTree_GradeSection(ByVal ACD_ID As String) 'for orginating tree view for the grade and section
        'Dim ACD_ID As String = Session("current_ACD_ID")
        Dim dtTable As DataTable = PopulateGrade_Section(ACD_ID)
        ' PROCESS Filter
        Dim dvGRD_DESCR As New DataView(dtTable, "", "GRD_DESCR", DataViewRowState.OriginalRows)
        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim drGRD_DESCR As DataRow
        For i As Integer = 0 To dtTable.Rows.Count - 1
            drGRD_DESCR = dtTable.Rows(i)
            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drGRD_DESCR("GRD_DESCR") Then
                    contains = True
                End If
            End While
            Dim trNodeGRD_DESCR As New TreeNode(drGRD_DESCR("GRD_DESCR"), drGRD_DESCR("GRD_ID"))
            If contains Then
                Continue For
            End If
            Dim strGRADE_SECT As String = "GRD_DESCR = '" & _
            drGRD_DESCR("GRD_DESCR") & "'"
            Dim dvSCT_DESCR As New DataView(dtTable, strGRADE_SECT, "SCT_DESCR", DataViewRowState.OriginalRows)
            Dim ienumGRADE_SECT As IEnumerator = dvSCT_DESCR.GetEnumerator
            While (ienumGRADE_SECT.MoveNext())
                Dim drGRADE_SECT As DataRowView = ienumGRADE_SECT.Current
                Dim trNodeMONTH_DESCR As New TreeNode(drGRADE_SECT("SCT_DESCR"), drGRADE_SECT("SCT_ID"))
                trNodeGRD_DESCR.ChildNodes.Add(trNodeMONTH_DESCR)
            End While
            trSelectAll.ChildNodes.Add(trNodeGRD_DESCR)
        Next
        tvGrade.Nodes.Clear()
        tvGrade.Nodes.Add(trSelectAll)
        tvGrade.DataBind()
    End Sub

    Private Function PopulateGrade_Section(ByVal ACD_ID As String) As DataTable
        Dim dsData As DataSet = Nothing
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "EXEC [GETACD_GRADE_SECTION_TREE] @ACD_ID=" & ACD_ID & ",@bINCLUDE_TEMP_SECTION=1 "

            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindTree_GradeSection(ddlAcademicYear.SelectedItem.Value)
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
