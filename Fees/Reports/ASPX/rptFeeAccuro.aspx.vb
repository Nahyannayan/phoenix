﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data

Partial Class Fees_Reports_ASPX_rptFeeAccuro
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            lblReportCaption.Text = "Catering Cash Collection Report"
            txtFeeCounter.Text = Session("sUsr_name")
            hfFEECounter.Value = Session("sUsr_name")
            txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
    End Sub

    Private Sub GetDailyReport()
        Dim cmd As New SqlCommand("getCashCollection")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFromDT As New SqlParameter("@FromDate ", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@ToDate ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpToDT)

        Dim sqlpUserName As New SqlParameter("@UserName", SqlDbType.VarChar)
        If CheckEmployee.Checked Then
            sqlpUserName.Value = Session("sBsuid")
        Else
            sqlpUserName.Value = hfFEECounter.Value
        End If
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpSummary As New SqlParameter("@Summary", SqlDbType.VarChar)
        sqlpSummary.Value = IIf(rbnDetailed.SelectedItem.Text = "Summary", 1, 0)
        cmd.Parameters.Add(sqlpSummary)

        cmd.Connection = New SqlConnection(WebConfigurationManager.ConnectionStrings("POSDATAConnectionString").ConnectionString) 'ConnectionManger.GetOASIS_FEESConnectionString
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")

        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        If CDate(txtFromDate.Text) = CDate(txtToDate.Text) Then
            params("ReportHead") = txtFromDate.Text.ToString()
        Else
            params("ReportHead") = txtFromDate.Text.ToString() & " To " & txtToDate.Text.ToString()
        End If

        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptAccuroCollection.rpt"
        Session("ReportSource") = repSource
        ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        GetDailyReport()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
