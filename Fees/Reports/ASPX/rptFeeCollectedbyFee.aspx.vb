Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_rptFeeCollectedbyFee
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            UsrBSUnits1.MenuCode = MainMnu_code
            Select Case MainMnu_code
                Case "F726095"
                    TrFeeType.Style("display") = "none"
                    lblReportCaption.Text = "Query Collection By Fees"
                    TrRemarks.Visible = True
                Case "F726096"
                    lblReportCaption.Text = "Query Charge By Fees"

            End Select

            'BindBusinessUnit()
            FillFeeType()
            If txtFromDate.Text = "" Then
                txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            End If
            txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)

            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTFromDate") = txtFromDate.Text
        Session("RPTToDate") = txtToDate.Text
        If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMnu_code
            Case "F726095"
                GetFeeCollectedbyFee()
            Case "F726096"
                GetFeeChargebyFee()

        End Select
    End Sub

    Private Sub GetFeeCollectedbyFee()
        Dim cmd As New SqlCommand("FEES.GetFeeCollectedbyFee")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("@")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpFEE_ID As New SqlParameter("@FEE_ID", SqlDbType.Int)
        sqlpFEE_ID.Value = ddFee.SelectedItem.Value
        cmd.Parameters.Add(sqlpFEE_ID)

        Dim sqlpNARR As New SqlParameter("@NARRATION", SqlDbType.VarChar)
        sqlpNARR.Value = txtRemarks.Text.Trim()
        cmd.Parameters.Add(sqlpNARR)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        Dim BSU As String = SqlHelper.ExecuteScalar(cmd.Connection.ConnectionString, CommandType.Text, "SELECT isnull(BSU_NAME,'') FROM OASIS.dbo.BUSINESSUNIT_M where BSU_ID='" & Session("sBSUID") & "'")
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("FEE_DESCR") = ddFee.SelectedItem.Text
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = BSU
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = False

        ''SUBREPORT1
        Dim repSourceSubRep(0) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        Dim cmdSubSummary As New SqlCommand
        cmdSubSummary = cmd.Clone
        repSourceSubRep(0).Command = cmdSubSummary
        repSource.SubReport = repSourceSubRep

        params("RPT_CAPTION") = "List Of students Who have Paid " & ddFee.SelectedItem.Text
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeGetFeeCollectedbyFee.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GetFeeChargebyFee()
        Dim cmd As New SqlCommand("FEES.GetFeeChargeByFeetype")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("@")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpFEE_ID As New SqlParameter("@FEE_ID", SqlDbType.Int)
        sqlpFEE_ID.Value = ddFee.SelectedItem.Value
        cmd.Parameters.Add(sqlpFEE_ID)

        Dim sqlpFSH_SOURCE As New SqlParameter("@FSH_SOURCE", SqlDbType.VarChar)
        sqlpFSH_SOURCE.Value = ddFeeType.SelectedItem.Value
        cmd.Parameters.Add(sqlpFSH_SOURCE)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim BSU As String = SqlHelper.ExecuteScalar(cmd.Connection.ConnectionString, CommandType.Text, "SELECT isnull(BSU_NAME,'') FROM OASIS.dbo.BUSINESSUNIT_M where BSU_ID='" & Session("sBSUID") & "'")
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("FEE_DESCR") = ddFee.SelectedItem.Text
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = BSU
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If ddFeeType.SelectedItem.Value = "" Then
            params("RPT_CAPTION") = "List Of students Fee Charge " & ddFee.SelectedItem.Text
        Else
            params("RPT_CAPTION") = "List Of students Fee Charge " & ddFee.SelectedItem.Text & " ( " & ddFeeType.SelectedItem.Text & " ) "
        End If

        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeGetFeeChargedbyFee.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    'Private Sub BindBusinessUnit()
    '    'ddlBSUnit.DataSource = FeeCommon.GETBSUFORUSER(Session("sUsr_name"))
    '    'ddlBSUnit.DataTextField = "bsu_name"
    '    'ddlBSUnit.DataValueField = "bsu_id"
    '    'ddlBSUnit.DataBind()
    '    'ddlBSUnit.Items.FindByValue(Session("sBsuid")).Selected = True


    '    Dim ds As New DataSet
    '    ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
    '    CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
    '    & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
    '    ddlBSUnit.DataSource = ds.Tables(0)
    '    ddlBSUnit.DataTextField = "bsu_name"
    '    ddlBSUnit.DataValueField = "bsu_id"
    '    ddlBSUnit.DataBind()
    '    ddlBSUnit.SelectedIndex = -1
    '    If Not ddlBSUnit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
    '        ddlBSUnit.Items.FindByValue(Session("sBsuid")).Selected = True
    '    End If
    'End Sub


    Sub FillFeeType()
        ddFee.Items.Clear()
        Dim dtFeeType As DataTable = FeeTypes()
        ddFee.DataSource = dtFeeType
        ddFee.DataTextField = "FEE_DESCR"
        ddFee.DataValueField = "FEE_ID"
        ddFee.DataBind()

    End Sub

    'Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    FillFeeType(ddlBSUnit.SelectedItem.Value.ToString())
    'End Sub

    'Protected Sub btnLoadFees_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoadFees.Click
    '    Me.ddFee.Items.Clear()
    '    FillFeeType(UsrBSUnits1.GetSelectedNode("@"))
    'End Sub
    Public Shared Function FeeTypes() As DataTable
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.Text, "SELECT FEE_ID,FEE_DESCR FROM FEES.FEES_M order BY FEE_ORDER")
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
