<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptFEECollectionDetailsDonations.aspx.vb" Inherits="Fees_Reports_ASPX_rptFEECollectionDetailsDonation" Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function ToggleSearchBSUnit() {
            if (document.getElementById('<%=hfBSU.ClientID %>').value == 'display') {
                document.getElementById('<%=trBSUnit.ClientID %>').style.display = '';

             document.getElementById('<%=hfBSU.ClientID %>').value = 'none';
         }
         else {
             document.getElementById('<%=trBSUnit.ClientID %>').style.display = 'none';

             document.getElementById('<%=hfBSU.ClientID %>').value = 'display';
         }
         return false;
     }

     function HideAll() {
         document.getElementById('<%=trBSUnit.ClientID %>').style.display = 'none';
        }
        function SearchHide() {
            document.getElementById('trBSUnit').style.display = 'none';
        }

        function GetBSUName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../../../PayRoll/Reports/Aspx/SelIDDESC.aspx?ID=BSU", "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }
        }

        function CheckOnPostback() {
            if (document.getElementById('<%=hfBSU.ClientID %>').value == 'none') {
                document.getElementById('<%=trBSUnit.ClientID %>').style.display = '';

             }
             return false;
         }

         function GetFEECounter() {
             var sFeatures;
             sFeatures = "dialogWidth: 729px; ";
             sFeatures += "dialogHeight: 445px; ";
             sFeatures += "help: no; ";
             sFeatures += "resizable: no; ";
             sFeatures += "scroll: yes; ";
             sFeatures += "status: no; ";
             sFeatures += "unadorned: no; ";
             var NameandCode;
             var result;
             var type;
             if (document.getElementById('<%=h_bTransport.ClientID %>').value == 1) {
                type = 'FEE_TRN_USER';
            }
            else {
                type = 'FEE_RPT_USER';
            }
            result = window.showModalDialog("../../../Common/PopupForm.aspx?multiSelect=false&ID=" + type, "", sFeatures)
            if (result != "" && result != "undefined") {
                NameandCode = result.split('___');
                document.getElementById('<%=hfFEECounter.ClientID %>').value = NameandCode[0];
               document.getElementById('<%=txtFeeCounter.ClientID %>').value = NameandCode[1];
            }
            return false;
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
          <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                <br />
                <table width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">From Date</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputbox"  ></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                    ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" class="matters" width="20%"><span class="field-label">To Date</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="inputbox"  ></asp:TextBox>
                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr id="trBSUnit" runat="server">
                        <td align="left" class="matters"><span class="field-label">Business Unit</span></td>
                        <td align="left" class="matters">
                            <div class="checkbox-list">

                            <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                            </div>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="trUserName" runat="server">
                        <td align="left" class="matters"><span class="field-label">User Name</span></td>
                        <td align="right" class="matters"   style="text-align: left"
                            valign="top">
                            <asp:TextBox ID="txtFeeCounter" runat="server" CssClass="inputbox"  >
                            </asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetFEECounter();return false;"></asp:ImageButton></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr runat="server" id="trChkConciliation">
                        <td align="right" class="matters" colspan="4" style="text-align: left"
                            valign="top">
                            <asp:CheckBox ID="chkReconciliation" runat="server" Text="Reconciliation" CssClass="field-label"></asp:CheckBox></td>
                    </tr>
                    <tr runat="server" id="trchkGroup">
                        <td align="right" class="matters" colspan="4" style="text-align: left"
                            valign="top">
                            <asp:CheckBox ID="chkGroup" runat="server" Text="View in Group Currency" CssClass="field-label"></asp:CheckBox>
                            <asp:CheckBox ID="ChkPdcExclude" runat="server" Text="Exclude PDC" Visible="False" CssClass="field-label"></asp:CheckBox></td>
                    </tr>
                    <tr id="trSource" runat="server">
                        <td align="left" class="matters"><span class="field-label">Source</span></td>
                        <td align="left" class="matters"  
                            valign="top">
                            <asp:DropDownList ID="ddlSource" runat="server">
                                <asp:ListItem Selected="True" Value="S">School</asp:ListItem>
                                <asp:ListItem Value="C">Company</asp:ListItem>
                                <asp:ListItem Value="ALL">All</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="trGroupBy" runat="server">
                        <td align="left" class="matters"><span class="field-label">Group By</span></td>
                        <td align="left" class="matters" 
                            valign="top">
                            <asp:RadioButtonList ID="grpGroupBy" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True">Source</asp:ListItem>
                                <asp:ListItem Value="BusinessUnit">Business Unit</asp:ListItem>
                                <asp:ListItem>Mode</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4"  >
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <input id="hfBSU" runat="server" type="hidden" />
                <input id="h_BSUID" runat="server" type="hidden" />
                <input id="h_bTransport" runat="server" type="hidden" />
                <input id="hfFEECounter" runat="server" type="hidden" />
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>

