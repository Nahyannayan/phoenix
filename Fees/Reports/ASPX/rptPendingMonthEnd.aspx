<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPendingMonthEnd.aspx.vb" Inherits="Fees_Reports_ASPX_rptPendingMonthEnd" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                  <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                <br />
                <table align="center"  cellpadding="4" cellspacing="0"
                    style="width: 100%;">
                    <%-- <tr class ="subheader_img">
      <td align="left" colspan="2" style="height: 19px">
        <asp:Label ID="lblReportCaption" runat="server"></asp:Label></td>
    </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">As On
        Date</span></td>
                        <td align="left"  width="30%" style="text-align: left" id="tdToDate1" runat="server">
                            <asp:TextBox ID="txtAsOnDate" runat="server" ></asp:TextBox>
                            <asp:ImageButton ID="imgAsOnDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" /><br />
                            <asp:CheckBox ID="chkDetailed" runat="server" Text="Detailed" CssClass="checkbox-list"/>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAsOnDate"
                                ErrorMessage="As On Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="revAsOndate" runat="server" ControlToValidate="txtAsOnDate" Display="Dynamic"
                                    ErrorMessage="Enter the AsOn Date in given format dd/mmm/yyyy e.g. 21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>&nbsp;
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left"  colspan="2">
                       <div class="checkbox-list">    <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" /></div> 
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />

                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calAsOnDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgAsOnDate" TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>
