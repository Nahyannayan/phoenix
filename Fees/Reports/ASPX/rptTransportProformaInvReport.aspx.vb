Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class Fees_Reports_ASPX_rptTransportProformaInvReport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            Select Case MainMnu_code
                Case "F725158"
                    lblReportCaption.Text = "Proforma Invoice Report"
            End Select
            Me.txtToDate.Text = Format(Date.Now, OASISConstants.DateFormat)
            BindBusinessUnit()
            BindAcademicYear(Me.ddlBSU.SelectedValue)
            PopulateMonths()
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
        
    End Sub
    Sub BindBusinessUnit()
        ddlBSU.DataSource = FeeCommon.SERVICES_BSU_M(Session("sUsr_name"), Session("sBsuid"))
        ddlBSU.DataBind()
    End Sub
    Private Sub BindAcademicYear(ByVal BSU_ID As Integer)
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBSU.SelectedItem.Value)
        ddlACDYear.DataSource = dtACD
        ddlACDYear.DataTextField = "ACY_DESCR"
        ddlACDYear.DataValueField = "ACD_ID"
        ddlACDYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlACDYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
    Protected Sub ddlBSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSU.SelectedIndexChanged
        BindAcademicYear(Me.ddlBSU.SelectedValue)
        ddlACDYear_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub ddlACDYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlACDYear.SelectedIndexChanged
        PopulateMonths()
    End Sub
    Private Sub PopulateMonths()
        Dim dtTable As DataTable = FeeTranspotInvoice.PopulateMonthsInAcademicYearID(ddlACDYear.SelectedValue)
        ' PROCESS Filter
        Dim dvTRM_DESCRIPTION As New DataView(dtTable, "", "TRM_DESCRIPTION", DataViewRowState.OriginalRows)

        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim ienumTRM_DESCRIPTION As IEnumerator = dvTRM_DESCRIPTION.GetEnumerator
        Dim drTRM_DESCRIPTION As DataRowView
        While (ienumTRM_DESCRIPTION.MoveNext())
            'Processes List
            drTRM_DESCRIPTION = ienumTRM_DESCRIPTION.Current

            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drTRM_DESCRIPTION("TRM_DESCRIPTION") Then
                    contains = True
                End If
            End While
            Dim trNodeTRM_DESCRIPTION As New TreeNode(drTRM_DESCRIPTION("TRM_DESCRIPTION"), drTRM_DESCRIPTION("TRM_ID"))
            If contains Then
                Continue While
            End If
            'If trSelectAll.ChildNodes.Contains(trNodeTRM_DESCRIPTION) Then
            '    Continue While
            'End If
            Dim strAMS_MONTH As String = "TRM_DESCRIPTION = '" & _
            drTRM_DESCRIPTION("TRM_DESCRIPTION") & "'"
            Dim dvAMS_MONTH As New DataView(dtTable, strAMS_MONTH, "AMS_ID", DataViewRowState.OriginalRows)
            Dim ienumAMS_MONTH As IEnumerator = dvAMS_MONTH.GetEnumerator
            While (ienumAMS_MONTH.MoveNext())
                Dim drMONTH_DESCR As DataRowView = ienumAMS_MONTH.Current
                Dim trNodeMONTH_DESCR As New TreeNode(drMONTH_DESCR("MONTH_DESCR"), drMONTH_DESCR("AMS_ID"))
                trNodeTRM_DESCRIPTION.ChildNodes.Add(trNodeMONTH_DESCR)
            End While
            trSelectAll.ChildNodes.Add(trNodeTRM_DESCRIPTION)
        End While
        trMonths.Nodes.Clear()
        trMonths.Nodes.Add(trSelectAll)
        trMonths.DataBind()
    End Sub

    Private Function FillMonthsSelected() As String
        Try
            Dim str_Selected As String = ""
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If str_Selected = "" Then
                    str_Selected = "For the month(s) " & node.Text
                Else
                    str_Selected = str_Selected & ", " & node.Text
                End If
            Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Private Function FillTermsSelected() As String
        Try
            Dim str_Selected As String = ""
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    If node.Value <> "ALL" Then
                        If str_Selected = "" Then
                            str_Selected = "For the Term(s) " & node.Text
                        Else
                            str_Selected = str_Selected & ", " & node.Text
                        End If
                    End If
                Else
                    Continue For
                End If
            Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Private Function GetTermsSelected() As String
        Try
            Dim str_Selected As String = ""
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    If node.Value <> "ALL" Then
                        If str_Selected = "" Then
                            str_Selected = node.Value & "|"
                        Else
                            str_Selected = str_Selected & node.Value & "|"
                        End If
                    End If
                Else
                    Continue For
                End If
            Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Private Function GetMonthsSelected() As String
        Try
            Dim str_Selected As String = ""
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If str_Selected = "" Then
                    str_Selected = node.Value & "|"
                Else
                    str_Selected = str_Selected & node.Value & "|"
                End If
            Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        h_BSUID.Value = ""
        Select Case MainMnu_code
            Case "F725158"
                GenerateProformaInvReport()
        End Select
    End Sub

    Private Sub GenerateProformaInvReport()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand("TRANSPORT.GET_PROFORMA_REPORT")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTUBSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSTUBSU_ID.Value = Me.ddlBSU.SelectedValue
        cmd.Parameters.Add(sqlpSTUBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = Me.ddlACDYear.SelectedValue
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpTERMorMONTH As New SqlParameter("@TERMorMONTH", SqlDbType.VarChar, 1)
        sqlpTERMorMONTH.Value = Me.ddlACDYear.SelectedValue
        cmd.Parameters.Add(sqlpTERMorMONTH)

        Dim sqlpTERMorMONTHIDs As New SqlParameter("@TERMorMONTH_IDs", SqlDbType.VarChar)
        sqlpTERMorMONTHIDs.Value = IIf(rbMonthly.Checked, GetMonthsSelected, GetTermsSelected)
        cmd.Parameters.Add(sqlpTERMorMONTHIDs)

        Dim sqlpTODT As New SqlParameter("@ASON_DATE", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpbSummary As New SqlParameter("@bSummary", SqlDbType.SmallInt)
        sqlpbSummary.Value = Me.RBLSummary.SelectedValue
        cmd.Parameters.Add(sqlpbSummary)

        cmd.Connection = New SqlConnection(str_conn)
        cmd.CommandTimeout = 0
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")

        params("DateHead") = "AS ON " & Me.txtToDate.Text

        If RBLSummary.SelectedValue = 0 Then
            params("RptHead") = "Proforma Invoice Report"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/RptTransportProformaInv_Summary.rpt"
        ElseIf RBLSummary.SelectedValue = 1 Then
            params("RptHead") = "Proforma Invoice Report - GroupBy Company"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/RptTransportProformaInv_Summary.rpt"
        Else
            params("RptHead") = "Proforma Invoice detail Report"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/RptTransportProformaInv_Detail.rpt"
        End If
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True


        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
   
End Class

