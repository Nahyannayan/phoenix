Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports GemBox.Spreadsheet

Partial Class Fees_Reports_ASPX_TransdocnoDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            txtFeeCounter.Attributes.Add("readonly", "readonly")
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle

            txtFromDate.Text = Format(DateTime.Now.AddMonths(-1), OASISConstants.DateFormat)
            txtTodate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            BindBusinessUnit()

            Select Case ViewState("MainMnu_code").ToString
                Case "F725150"
                    lblReportCaption.Text = "Fee Charge Posting Reconciliation"
                    TrFeeSource.Visible = True
                Case "F726067"
                    lblReportCaption.Text = "Fee Collection (Audit)"
                    trUser.Visible = False
                Case "F726068"
                    lblReportCaption.Text = "SO Payment Status"
                    trUser.Visible = False
                    trSerachDate.Visible = True
                Case "F726069"
                    lblReportCaption.Text = "TC Fee Pending Status"
                    trUser.Visible = False
                    trSerachDate.Visible = False
                Case "F726071"
                    lblReportCaption.Text = "Mashreq Online Payments"
                    trUser.Visible = False
                    trSerachDate.Visible = False
                    TrFeeSource.Visible = False
                Case "F726072"
                    lblReportCaption.Text = "Monthend Voucher Support"
                    trUser.Visible = False
                    trSerachDate.Visible = False
                    TrFeeSource.Visible = False
                    tdToDate1.ColSpan = "3"
                    TdToDate2.Visible = False
                    tdToDate3.Visible = False
                Case "F726077"
                    lblReportCaption.Text = "Monthly Recognition"
                    trUser.Visible = False
                    trSerachDate.Visible = False
                    TrFeeSource.Visible = False
                    trEmpBSUWise.Visible = True
                    trFilterDate.Visible = True
                Case "F704005"
                    lblReportCaption.Text = "Query SMS Status"
                    trUser.Visible = False
                    trSerachDate.Visible = False
                    TrFeeSource.Visible = True
                    ddlFeeSource.Visible = False
                    ddlSmsfilter.Visible = True
                    lblCombotext.Text = "Status"
                    txtFromDate.Text = Format(New DateTime(Now.Year, Now.Month, 1, 0, 0, 0), OASISConstants.DateFormat)
                Case "F704012"
                    lblReportCaption.Text = "Query EMAIL Status"
                    trUser.Visible = False
                    trSerachDate.Visible = False
                    TrFeeSource.Visible = True
                    ddlFeeSource.Visible = False
                    ddlSmsfilter.Visible = True
                    lblCombotext.Text = "Status"
                    txtFromDate.Text = Format(New DateTime(Now.Year, Now.Month, 1, 0, 0, 0), OASISConstants.DateFormat)
                Case "F726088"
                    lblReportCaption.Text = "Fee Adjustment by Type"
                    trUser.Visible = False
                    trSerachDate.Visible = False
                    TrFeeSource.Visible = True
                    lblCombotext.Text = "Adjustment Type"
                    'If IsPostBack = False Then
                    getAdjReason()
                    'End If
                Case "F704010"
                    lblReportCaption.Text = "FGB Online Payments"
                    trUser.Visible = False
                    trSerachDate.Visible = False
                    TrFeeSource.Visible = False
                Case "F300237"
                    lblReportCaption.Text = "Query Transport SMS Status"
                    trUser.Visible = False
                    trSerachDate.Visible = False
                    TrFeeSource.Visible = False
                    ddlFeeSource.Visible = False
                    ddlSmsfilter.Visible = True
                    lblCombotext.Text = "Status"
                    txtFromDate.Text = Format(New DateTime(Now.Year, Now.Month, 1, 0, 0, 0), OASISConstants.DateFormat)
                    BindBusinessUnit(True)
            End Select
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            ' lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            ' lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case ViewState("MainMnu_code").ToString
            Case "F725150"
                FeeChargeMode()
            Case "F726067"
                ReceiptSummary()
            Case "F726068"
                SOReport()
            Case "F726069"
                TCPending()
            Case "F726071"
                MashreqOnlinePayments()
            Case "F726072"
                MonthendVoucherSupport()
            Case "F726077"
                MonthlyRecognition()
            Case "F704005"
                FEE_REMINDER_SMSREPORTALL()
            Case "F704012"
                FEE_ChequeReturn_EmailLOG()
            Case "F726088"
                FeeAdjustmentbyType()
            Case "F704010"
                MashreqOnlinePayments()
            Case "F300237"
                FEE_TRANSPORT_REMINDER_SMSREPORTALL()
        End Select
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Private Sub FeeAdjustmentbyType()
        Dim cmd As New SqlCommand("[FEES].[ADJUSTMENT_REASON]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFromDT As New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TODATE ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtTodate.Text)
        cmd.Parameters.Add(sqlpToDT)

        Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedItem.Value
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpARM_ID As New SqlParameter("@ARM_ID", SqlDbType.Int)
        sqlpARM_ID.Value = ddlFeeSource.SelectedItem.Value
        cmd.Parameters.Add(sqlpARM_ID)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BsuName") = ddlBSUnit.SelectedItem.Text
        params("RptHead") = "Fee Adjustment By Type "
        params("DateHead") = txtFromDate.Text & " To " & txtTodate.Text
        repSource.Parameter = params
        repSource.Command = cmd

        repSource.ResourceName = "../../FEES/REPORTS/RPT/AdjustmentReasondetails.rpt"
        Session("ReportSource") = repSource
        '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub ReceiptSummary()
        Dim cmd As New SqlCommand("[FEES].[ReceiptSummary_Feetype]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFromDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TODT ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtTodate.Text)
        cmd.Parameters.Add(sqlpToDT)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedItem.Value
        cmd.Parameters.Add(sqlpBSU_ID)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        GetBSU_RoundOff(ddlBSUnit.SelectedValue)
        Dim params As New Hashtable
        params("RoundOffVal") = ViewState("RoundOffVal")
        params("userName") = Session("sUsr_name")
        params("BsuName") = ddlBSUnit.SelectedItem.Text
        params("ReportHead") = txtFromDate.Text & " To " & txtTodate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd

        repSource.ResourceName = "../../FEES/REPORTS/RPT/FeeCollectionSummaryDate.rpt"
        Session("ReportSource") = repSource
        '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Public Sub GetBSU_RoundOff(ByVal BSU_ID As String)
        'Author(--Swapna)
        'Date   --18/Aug/2011
        'Purpose--Get selected BSU's round off
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim query As String = ""

        query = "SELECT  BSU_ROUNDOFF FROM BUSINESSUNIT_M WHERE BSU_ID = '" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(query, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

        While reader.Read
            ViewState("RoundOffVal") = reader.Item("BSU_ROUNDOFF")
        End While

    End Sub
    Private Sub MonthendVoucherSupport()
        Dim cmd As New SqlCommand("[FEES].[MonthendVoucherSupport]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFromDT As New SqlParameter("@ASONDATE", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedItem.Value
        cmd.Parameters.Add(sqlpBSU_ID)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = ddlBSUnit.SelectedItem.Text
        params("DateHead") = "Monthend voucher support As On " & txtFromDate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd

        repSource.ResourceName = "../../FEES/REPORTS/RPT/MonthendVoucherSupport.rpt"
        Session("ReportSource") = repSource
        '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub FeeChargeMode()
        If txtFeeCounter.Text = "" Then
            'lblError.Text = "Please Select DocNo..!"
            usrMessageBar.ShowNotification("Please Select DocNo..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim cmd As New SqlCommand("[FEES].[TRNSALL_LIST]")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedItem.Value
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFromDT As New SqlParameter("@FROMDT ", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TODT ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtTodate.Text)
        cmd.Parameters.Add(sqlpToDT)

        Dim sqlpMode As New SqlParameter("@MODE ", SqlDbType.VarChar)
        sqlpMode.Value = ddlFeeSource.SelectedItem.Text
        cmd.Parameters.Add(sqlpMode)

        Dim sqlpDOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar)
        sqlpDOCNO.Value = txtFeeCounter.Text
        cmd.Parameters.Add(sqlpDOCNO)
        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BsuName") = ddlBSUnit.SelectedItem.Text
        params("ReportHead") = "Fee Charge ( " & sqlpDOCNO.Value & " ) Posting Reconciliation" ' for the period " & txtFromDate.Text & " To " & txtTodate.Text
        params("ReportDate") = "" & txtFromDate.Text & " To " & txtTodate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd

        repSource.ResourceName = "../../FEES/REPORTS/RPT/TransallFeecharge.rpt"
        Session("ReportSource") = repSource
        '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub BindBusinessUnit(Optional ByVal bTransport As Boolean = False)
        If bTransport Then
            ddlBSUnit.DataSource = FeeCommon.SERVICES_BSU_M(Session("sUsr_name"), Session("sBsuid"))
            ddlBSUnit.DataTextField = "BSU_NAME"
            ddlBSUnit.DataValueField = "SVB_BSU_ID"
            ddlBSUnit.DataBind()

        Else
            ddlBSUnit.DataSource = FeeCommon.GETBSUFORUSER(Session("sUsr_name"))
            ddlBSUnit.DataTextField = "bsu_name"
            ddlBSUnit.DataValueField = "bsu_id"
            ddlBSUnit.DataBind()
            ddlBSUnit.Items.FindByValue(Session("sBsuid")).Selected = True
        End If

    End Sub

    Private Sub getAdjReason()
        ddlFeeSource.Items.Clear()
        ddlFeeSource.DataSource = FeeCommon.getADJREASON(True)
        ddlFeeSource.DataTextField = "ARM_DESCR"
        ddlFeeSource.DataValueField = "ARM_ID"
        ddlFeeSource.DataBind()
    End Sub

    Private Sub SOReport()
        Dim cmd As New SqlCommand("[FEES].[StrikeOf_Report]")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedItem.Value
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFromDT As New SqlParameter("@FROMDT ", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TODT ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtTodate.Text)
        cmd.Parameters.Add(sqlpToDT)

        Dim sqlpMode As New SqlParameter("@MODE ", SqlDbType.VarChar)
        sqlpMode.Value = "SOD"
        Dim HeadCaption As String = " ( StrikeOf Date ) "
        If RdLDA.Checked Then
            sqlpMode.Value = "LDA"
            HeadCaption = " (Last Date Of Attendence) "
        End If
        cmd.Parameters.Add(sqlpMode)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BsuName") = ddlBSUnit.SelectedItem.Text
        params("ReportHead") = txtFromDate.Text & " To " & txtTodate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd

        repSource.ResourceName = "../../FEES/REPORTS/RPT/StrikeOfReport.rpt"
        Session("ReportSource") = repSource
        '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub MashreqOnlinePayments()
        Dim cmd As New SqlCommand("[FEES].[Mashq_InternetCollection]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedItem.Value
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFromDT As New SqlParameter("@FROMDT ", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TODT ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtTodate.Text)
        cmd.Parameters.Add(sqlpToDT)

        Dim sqlpFCL_SOURCE As New SqlParameter("@FCL_SOURCE", SqlDbType.VarChar, 20)
        Dim params As New Hashtable
        If ViewState("MainMnu_code").ToString = "F726071" Then
            params("RPT_CAPTION") = "MASHREQ ONLINE PAYMENTS"
            sqlpFCL_SOURCE.Value = "MSQONLINE"
        ElseIf ViewState("MainMnu_code").ToString = "F704010" Then
            params("RPT_CAPTION") = "FGB ONLINE PAYMENTS"
            sqlpFCL_SOURCE.Value = "FGBONLINE"
        End If
        cmd.Parameters.Add(sqlpFCL_SOURCE)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass

        params("userName") = Session("sUsr_name")
        params("BsuName") = ddlBSUnit.SelectedItem.Text
        params("ReportHead") = txtFromDate.Text & " To " & txtTodate.Text

        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        repSource.ResourceName = "../../FEES/REPORTS/RPT/MashqInternetCollection.rpt"
        Session("ReportSource") = repSource
        '    Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub TCPending()
        Dim cmd As New SqlCommand("[FEES].[TCFee_Pending]")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedItem.Value
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFromDT As New SqlParameter("@FROMDT ", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TODT ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtTodate.Text)
        cmd.Parameters.Add(sqlpToDT)

        Dim sqlpMode As New SqlParameter("@STAGE ", SqlDbType.VarChar)
        sqlpMode.Value = "TC"
        cmd.Parameters.Add(sqlpMode)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BsuName") = ddlBSUnit.SelectedItem.Text
        params("ReportHead") = "TC Fee Pending Status Details for the period " & txtFromDate.Text & " To " & txtTodate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd

        repSource.ResourceName = "../../FEES/REPORTS/RPT/TcfeePending.rpt"
        Session("ReportSource") = repSource
        ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub MonthlyRecognition()
        Dim cmd As New SqlCommand("[FEES].[MonthlyRecognition]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedItem.Value
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFromDT As New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TODATE ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtTodate.Text)
        cmd.Parameters.Add(sqlpToDT)

        Dim sqlpEMP_BSU_WISE As New SqlParameter("@EMP_BSU_WISE ", SqlDbType.Bit)
        sqlpEMP_BSU_WISE.Value = chkEmpBSUWise.Checked
        cmd.Parameters.Add(sqlpEMP_BSU_WISE)

        Dim sqlpFILTERBY_DT As New SqlParameter("@FILTERBY_DT", SqlDbType.VarChar, 2)
        sqlpFILTERBY_DT.Value = IIf(Me.RdbChargeDT.Checked, "CD", "RR")
        cmd.Parameters.Add(sqlpFILTERBY_DT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = ddlBSUnit.SelectedItem.Text
        params("DateHead") = txtFromDate.Text & " To " & txtTodate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        repSource.ResourceName = "../../FEES/REPORTS/RPT/Monthlyeventuation.rpt"
        Session("ReportSource") = repSource
        '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub FEE_ChequeReturn_EmailLOG()
        Dim cmd As New SqlCommand("[FEES].[rptFEE_CHQRETURN_EMAILREPORT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFILTERBY As New SqlParameter("@FILTERBY", SqlDbType.VarChar, 20)
        sqlpFILTERBY.Value = ddlSmsfilter.SelectedItem.Value
        cmd.Parameters.Add(sqlpFILTERBY)

        Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedItem.Value
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFromDT As New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TODATE ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtTodate.Text)
        cmd.Parameters.Add(sqlpToDT)


        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = "EMAIL Status Report"
        params("DateHead") = "For the period from " & txtFromDate.Text & " to " & txtTodate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_ChequeReturn_EmailReport.rpt"
        Session("ReportSource") = repSource
        ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub FEE_REMINDER_SMSREPORTALL()
        Dim cmd As New SqlCommand("[FEES].[rptFEE_REMINDER_SMSREPORT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFILTERBY As New SqlParameter("@FILTERBY", SqlDbType.VarChar, 20)
        sqlpFILTERBY.Value = ddlSmsfilter.SelectedItem.Value
        cmd.Parameters.Add(sqlpFILTERBY)

        Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedItem.Value
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFromDT As New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TODATE ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtTodate.Text)
        cmd.Parameters.Add(sqlpToDT)


        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = "SMS Status Report"
        params("DateHead") = txtFromDate.Text & " to " & txtTodate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_REMINDER_SMSREPORTALL.rpt"
        Session("ReportSource") = repSource
        ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub FEE_TRANSPORT_REMINDER_SMSREPORTALL()
        Dim cmd As New SqlCommand("[TRANSPORT].[rptFEE_REMINDER_SMSREPORT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFILTERBY As New SqlParameter("@FILTERBY", SqlDbType.VarChar, 20)
        sqlpFILTERBY.Value = ddlSmsfilter.SelectedItem.Value
        cmd.Parameters.Add(sqlpFILTERBY)

        Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSUID", SqlDbType.VarChar, 20)
        sqlpSTU_BSU_ID.Value = ddlBSUnit.SelectedItem.Value
        cmd.Parameters.Add(sqlpSTU_BSU_ID)

        Dim sqlpFromDT As New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TODATE ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtTodate.Text)
        cmd.Parameters.Add(sqlpToDT)


        cmd.Connection = New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = "SMS Status Report"
        params("DateHead") = "For the period from " & txtFromDate.Text & " to " & txtTodate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_REMINDER_SMSREPORTALL.rpt"
        Session("ReportSource") = repSource
        '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

End Class
