Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptTransportChequeBounceDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            lblReportCaption.Text = "Fee Cheque Return Details"
            chkPosted.Visible = True
            BindBusinessUnit()
            Me.txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            Me.txtFromDate.Text = Format(DateTime.Now.AddMonths(-3), OASISConstants.DateFormat)
            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        CHEQUEBOUNCE_DETAILS()
    End Sub

    Private Sub CHEQUEBOUNCE_DETAILS()
        h_STU_BSUID.Value = usrTrBSU.GetSelectedNode().ToString.Replace("||", "|")

        Dim cmd As New SqlCommand("[FEES].[F_GETCHEQUEBOUNCED]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar)
        sqlpSTU_BSU_ID.Value = h_STU_BSUID.Value
        cmd.Parameters.Add(sqlpSTU_BSU_ID)


        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpPosted As New SqlParameter("@POSTED", SqlDbType.Bit)
        sqlpPosted.Value = Me.chkPosted.Checked
        cmd.Parameters.Add(sqlpPosted)

        Dim sqlpSTU_TYP As New SqlParameter("@STU_TYP", SqlDbType.VarChar, 2)
        If radEnq.Checked Then
            sqlpSTU_TYP.Value = "E"
        ElseIf radStud.Checked Then
            sqlpSTU_TYP.Value = "S"
        End If
        cmd.Parameters.Add(sqlpSTU_TYP)

        Dim sqlpTYPE As New SqlParameter("@TYPE", SqlDbType.Char)
        If rdoTypeFilter.SelectedValue = 0 Then
            sqlpTYPE.Value = "A"
        ElseIf rdoTypeFilter.SelectedValue = 1 Then
            sqlpTYPE.Value = "B"
        ElseIf rdoTypeFilter.SelectedValue = 2 Then
            sqlpTYPE.Value = "R"
        End If
        cmd.Parameters.Add(sqlpTYPE)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        params("RPT_CAPTION") = "Cheque Return Report" & IIf(Me.chkPosted.Checked, " (Posted)", "")
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptTRANSPORT_ChequeBounceReport.rpt"

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub BindBusinessUnit()
        usrTrBSU.GetSelectedNode()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
