Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Fees_Reports_ASPX_rptOnlineFeeCollection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            UsrBSUnits1.MenuCode = MainMnu_code
            Select Case MainMnu_code
                Case "F730365"
                    lblReportCaption.Text = "Online FeeCollection (Transport)"
                    UsrBSUnits1.Visible = False
                Case "F730377"
                    lblReportCaption.Text = "Online FeeCollection (Fees)"
                    UsrTransportBSUnits1.Visible = False
            End Select

        End If
        
    End Sub
    
    Sub TransportOnline()
        If UsrTransportBSUnits1.GetSelectedNode = "" Then
            'lblError.Text = "Please Select Business Unit..!"
            usrMessageBar.ShowNotification("Please Select Business Unit..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim Mode As Boolean = False
        Dim Head As String = " Online Collections Details"
        Dim RptName = "../../FEES/REPORTS/RPT/RptOnlineTransportCollection.rpt"
        If ChkConsolidation.Checked Then
            Mode = True
            RptName = "../../FEES/REPORTS/RPT/RptOnlineTransportCollectionConsolidate.rpt"
            Head = " Online Collections Summary"
        End If
        Head = Head + IIf(rblFilter.SelectedValue = 0, "", " - " & Me.rblFilter.SelectedItem.Text)
        Dim cmd As New SqlCommand("Transport.OnlinePayment")
        cmd.CommandType = CommandType.StoredProcedure
        Dim pBSU_ID As New SqlParameter("@BSU_ID", Session("sBsuid"))
        Dim pDTFRom As New SqlParameter("@DTFRom", txtFromDate.Text)
        Dim pDTTo As New SqlParameter("@DTTo", txtToDate.Text)
        Dim pSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", UsrTransportBSUnits1.GetSelectedNode.Replace("||", "@"))
        Dim pMODE As New SqlParameter("@MODE", Mode)
        Dim pFilter As New SqlParameter("@CPM_ID", Me.rblFilter.SelectedValue)
        cmd.Parameters.Add(pBSU_ID)
        cmd.Parameters.Add(pDTFRom)
        cmd.Parameters.Add(pDTTo)
        cmd.Parameters.Add(pSTU_BSU_ID)
        cmd.Parameters.Add(pMODE)
        cmd.Parameters.Add(pFilter)
        Dim Params As New Hashtable
        Params("RptHead") = Head
        Params("DateHead") = "For the period " & txtFromDate.Text & " to " & txtToDate.Text
        Params("userName") = Session("sUsr_name")
        cmd.Connection = New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        Dim RptClass As New MyReportClass
        RptClass.IncludeBSUImage = True
        RptClass.HeaderBSUID = Session("sBsuid")
        RptClass.Command = cmd
        RptClass.Parameter = Params
        RptClass.ResourceName = RptName
        Session("ReportSource") = RptClass
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Sub FeesOnline()
        If UsrBSUnits1.GetSelectedNode = "" Then
            'lblError.Text = "Please Select Business Unit..!"
            usrMessageBar.ShowNotification("Please Select Business Unit..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim Mode As Boolean = False
        Dim Head As String = " Online Collections Details"
        Dim RptName = "../../FEES/REPORTS/RPT/RptOnlineFeeCollection.rpt"
        If ChkConsolidation.Checked Then
            Mode = True
            RptName = "../../FEES/REPORTS/RPT/RptOnlineFeeCollectionConsolidate.rpt"
            Head = "Online Collections Summary"
        End If
        Head = Head + IIf(rblFilter.SelectedValue = 0, "", " - " & Me.rblFilter.SelectedItem.Text)
        Dim cmd As New SqlCommand("FEES.OnlinePayment")
        cmd.CommandType = CommandType.StoredProcedure
        Dim pDTFRom As New SqlParameter("@DTFRom", txtFromDate.Text)
        Dim pDTTo As New SqlParameter("@DTTo", txtToDate.Text)
        Dim pSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", UsrBSUnits1.GetSelectedNode().Replace("||", "@"))
        Dim pMODE As New SqlParameter("@MODE", Mode)
        Dim pFilter As New SqlParameter("@CPM_ID", Me.rblFilter.SelectedValue)
        cmd.Parameters.Add(pDTFRom)
        cmd.Parameters.Add(pDTTo)
        cmd.Parameters.Add(pSTU_BSU_ID)
        cmd.Parameters.Add(pMODE)
        cmd.Parameters.Add(pFilter)
        Dim Params As New Hashtable
        Params("RptHead") = Head
        Params("DateHead") = "For the period " & txtFromDate.Text & " to " & txtToDate.Text
        Params("userName") = Session("sUsr_name")
        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim RptClass As New MyReportClass
        RptClass.Command = cmd
        RptClass.Parameter = Params
        RptClass.ResourceName = RptName
        Session("ReportSource") = RptClass
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Protected Sub BtnShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnShow.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "F730365"
                TransportOnline()
            Case "F730377"
                FeesOnline()
        End Select
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    
End Class
