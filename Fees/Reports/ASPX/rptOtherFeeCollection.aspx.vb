﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data

Partial Class Fees_Reports_ASPX_rptOtherFeeCollection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            lblReportCaption.Text = "Other Fee Collection Report"
            txtFromDate.Text = Format(DateTime.Now.AddMonths(-1), OASISConstants.DateFormat)
            txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            chkAllCollections.Attributes.Add("onClick", "SelectAll();")
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(sender As Object, e As EventArgs) Handles btnGenerateReport.Click
        If Not IsDate(txtFromDate.Text) Then
            'Me.lblError.Text = "Please enter a valid from date"
            usrMessageBar2.ShowNotification("Please enter a valid from date", UserControls_usrMessageBar.WarningType.Danger)
            Me.txtFromDate.Text = ""
            txtFromDate.Focus()
            Exit Sub
        ElseIf Not IsDate(txtToDate.Text) Then
            'Me.lblError.Text = "Please enter a valid to date"
            usrMessageBar2.ShowNotification("Please enter a valid to date", UserControls_usrMessageBar.WarningType.Danger)
            txtToDate.Text = ""
            txtToDate.Focus()
            Exit Sub
        End If
        If CDate(txtFromDate.Text) > CDate(txtToDate.Text) Then
            'Me.lblError.Text = "From Date cannot be greater than To Date"
            usrMessageBar2.ShowNotification("From Date cannot be greater than To Date", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        GET_OTH_COLLECTION()
    End Sub
    Private Sub GET_OTH_COLLECTION()
        Dim cmd As New SqlCommand("FEES.OTH_FEE_COLLECTION_REPORT")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSUIDs As New SqlParameter("@BSU_IDs ", SqlDbType.VarChar)
        sqlpBSUIDs.Value = UsrBSUnits1.GetSelectedNode("|")
        cmd.Parameters.Add(sqlpBSUIDs)

        Dim sqlpFromDT As New SqlParameter("@FROM_DT ", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TO_DT ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpToDT)

        Dim sqlpCollection As New SqlParameter("@COL_ID ", SqlDbType.Int)
        sqlpCollection.Value = IIf(chkAllCollections.Checked, 0, CInt(ddlCollection.SelectedValue))
        cmd.Parameters.Add(sqlpCollection)

        Dim sqlpSummary As New SqlParameter("@bDETAILED", SqlDbType.Bit)
        sqlpSummary.Value = CBool(rblSelection.SelectedValue)
        cmd.Parameters.Add(sqlpSummary)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")

        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        If CDate(txtFromDate.Text) = CDate(txtToDate.Text) Then
            params("ReportHead") = "Other Fee Collection " & IIf(rblSelection.SelectedValue = "0", "Summary", "Detail") '& " as On " & txtFromDate.Text.ToString()
        Else
            params("ReportHead") = "Other Fee Collection " & IIf(rblSelection.SelectedValue = "0", "Summary", "Detail") '& " From " & txtFromDate.Text.ToString() & " To " & txtToDate.Text.ToString()
        End If
        params("ReportDate") = "" & txtFromDate.Text.ToString() & " To " & txtToDate.Text.ToString()
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptOtherFeeCollectionDAX.rpt"
        If rblSelection.SelectedValue = "0" Then 'Summary
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptOtherFeeCollectionSummaryDAX.rpt"
        End If

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Protected Sub btncancel_Click(sender As Object, e As EventArgs) Handles btncancel.Click

    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
