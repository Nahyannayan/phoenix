﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports GemBox.Spreadsheet
Partial Class Fees_Reports_ASPX_TransConMonRec
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            txtFromDate.Text = Format(DateTime.Now.AddMonths(-1), OASISConstants.DateFormat)
            txtTodate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            lblReportCaption.Text = "Monthly Recognition"
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        MonthlyRecognitionTransport()
    End Sub
    Private Sub MonthlyRecognitionTransport()

        h_BSUID.Value = Me.UsrTransportBSUnits1.GetSelectedNode()
        If h_BSUID.Value = "" Then
            'lblError.Text = "Please select at least one business unit."
            usrMessageBar.ShowNotification("Please select at least one business unit.", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        Dim cmd As New SqlCommand("[FEES].[MonthlyRecognition]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)

        sqlpBSU_ID.Value = h_BSUID.Value
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFromDT As New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TODATE ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtTodate.Text)
        cmd.Parameters.Add(sqlpToDT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = ""
        params("DateHead") = "Concession Monthly Recognition For the Period " & txtFromDate.Text & " To " & txtTodate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        repSource.ResourceName = "../../FEES/REPORTS/RPT/TransportMonthlyConcessionRecognition.rpt"
        'repSource.ResourceName = "../../FEES/REPORTS/RPT/Monthlyeventuation.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class
