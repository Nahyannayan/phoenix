Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptFEEChargeDetails
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEECHARGEDETAILS
                    lblReportCaption.Text = "Fee Charge by Student Detailed Report"
                Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEECHARGEREVERSAL
                    lblReportCaption.Text = "Fee Charge Reversal Report"
                    trSummary.Visible = False
            End Select

            BindBusinessUnit()
            'BindAcademicYear(ddlBSUnit.SelectedValue)
            FillACD()
            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If Not IsDate(txtFromDate.Text) Or Not IsDate(txtToDate.Text) Then
            'lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEECHARGEDETAILS
                GenerateFeeChargeDetails()
            Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEECHARGEREVERSAL
                GenerateFeeChargeReversalReport()
        End Select
    End Sub

    Private Sub GenerateFeeChargeReversalReport()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand
        cmd = New SqlCommand("FEES.[F_GETTRANSPORTFEECHARGE_REVERSAL]")

        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSTU_BSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpSTU_BSU_ID)

        Dim sqlpMODE As New SqlParameter("@MODE", SqlDbType.VarChar, 10)
        sqlpMODE.Value = "ALL"
        cmd.Parameters.Add(sqlpMODE)

        Dim sqlpFROMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFROMDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FROMDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        params("ACY_DESCR") = ddlAcademicYear.SelectedItem.Text

        params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("RPT_CAPTION") = "FEE CHARGE REVERSAL REPORT"
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEECHARGEREPORT_CLASSWISE.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeChargeDetails()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand
        If chkBUSSummary.Checked Then
            cmd = New SqlCommand("FEES.[F_GETTRANSPORTFEEMONTHLYCHARGE]")
        Else
            cmd = New SqlCommand("[FEES].[F_GETTRANSPORTFEECHARGED]")
        End If
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSTU_BSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpSTU_BSU_ID)

        Dim sqlpMODE As New SqlParameter("@MODE", SqlDbType.VarChar, 10)
        sqlpMODE.Value = "ALL"
        cmd.Parameters.Add(sqlpMODE)

        Dim sqlpFROMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFROMDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FROMDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        params("ACY_DESCR") = ddlAcademicYear.SelectedItem.Text

        params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If radAreaWise.Checked Then
            params("RPT_CAPTION") = "TRANSPORT FEE CHARGE REPORT AREAWISE DETAILS"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEECHARGEREPORT_AREAWISE.rpt"
        ElseIf radBuswise.Checked Then
            params("RPT_CAPTION") = "TRANSPORT FEE CHARGE REPORT  BUSWISE DETAILS"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEECHARGEREPORT_BUSWISE.rpt"
        ElseIf radclasswise.Checked Then
            params("RPT_CAPTION") = "TRANSPORT FEE CHARGE REPORT  GRADEWISE DETAILS"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEECHARGEREPORT_CLASSWISE.rpt"
        End If
        'If chkBUSSummary.Checked Then
        '    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEECHARGEREPORT_BUSWISE.rpt"
        'Else
        '    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEECHARGEREPORT_CLASSWISE.rpt"
        'End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub BindBusinessUnit()
        ddlBSUnit.DataBind()
    End Sub

    Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'BindAcademicYear(ddlBSUnit.SelectedValue)
        FillACD()
    End Sub

    

    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, ddlAcademicYear.SelectedItem.Value, ddlBSUnit.SelectedValue)
        txtFromDate.Text = DTFROM
        'txtToDate.Text = DTTO
        txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
    End Sub
    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBSUnit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        SetAcademicYearDate()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
