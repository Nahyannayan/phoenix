<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptFeeTransportFeeAging.aspx.vb" Inherits="Fees_Reports_ASPX_rptFeeTransportFeeAging" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<%@ Register Src="../../../UserControls/usrTransportBSUnits.ascx" TagName="usrTransportBSUnits" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary
                                ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" class="matters" width="30%" valign="top">
                            <div class="checkbox-list">

                                <uc2:usrTransportBSUnits ID="UsrTransportBSUnits1" runat="server" />
                            </div>
                        </td>
                        <td align="left" class="matters" width="20%"><span class="field-label">As On</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                    ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                        ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr runat="server">
                        <td><span class="field-label">Options</span></td>
                        <td align="left" class="matters" colspan="3">
                            <asp:RadioButtonList ID="rblSummary" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="0"><span class="field-label">Detailed</span></asp:ListItem>
                                <asp:ListItem Value="1"><span class="field-label">Summary By Grade</span></asp:ListItem>
                                <asp:ListItem Value="2"><span class="field-label">Summary by BusinessUnit</span></asp:ListItem>
                            </asp:RadioButtonList></td>
                    </tr>
                    <tr runat="server">
                        <td align="left" class="matters"><span class="field-label">Student Status</span></td>
                        <td align="left" class="matters">
                            <asp:RadioButtonList ID="cblActive" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="2"><span class="field-label">All</span></asp:ListItem>
                                <asp:ListItem Value="1"><span class="field-label">Active</span></asp:ListItem>
                                <asp:ListItem Value="0"><span class="field-label">InActive</span></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td><span class="field-label">Ageing Buckets</span></td>
                        <td>
                            <asp:TextBox ID="txtBkt1" runat="server" MaxLength="3"></asp:TextBox>

                            <asp:TextBox ID="txtBkt2" runat="server" MaxLength="3"></asp:TextBox>

                            <asp:TextBox ID="txtBkt3" runat="server" MaxLength="3"></asp:TextBox>

                            <asp:TextBox ID="txtBkt4" runat="server" MaxLength="3"></asp:TextBox>

                            <asp:TextBox ID="txtBkt5" runat="server" MaxLength="3"></asp:TextBox>

                            <asp:TextBox ID="txtBkt6" runat="server" MaxLength="3"></asp:TextBox>

                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
