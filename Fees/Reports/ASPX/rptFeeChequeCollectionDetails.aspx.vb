Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class Fees_Reports_ASPX_rptFeeChequeCollectionDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ClientScript.RegisterStartupScript(Me.GetType(), _
        "script", "<script language='javascript'>  CheckOnPostback(); </script>")
        If Not Page.IsPostBack Then
            hfBSU.Value = "display"
        End If
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            Select Case MainMnu_code 
                Case "F726063"
                    lblReportCaption.Text = "Fee Cheque Collection Summary"
                    h_bTransport.Value = 0
                    trUsrName.Visible = False
                    chkSummary.Text = "Bank wise Summary"
            End Select
            SetAcademicYearDate()
            FillBSUNames(True)
            txtFeeCounter.Attributes.Add("ReadOnly", "ReadOnly")
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)

        Else
            lblError.Text = ""
        End If
        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
        If IsPostBack Then
            FillBSUNames()
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)

            Exit Sub
        End If
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        Select Case MainMnu_code 
            Case "F726063"
                F_RPTCollectionData_Cheque()
        End Select
    End Sub

    Private Sub F_RPTCollectionData_Cheque()
        Dim cmd As New SqlCommand("[FEES].[F_RPTCollectionData_Cheque]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@FOR_BSU_IDs", SqlDbType.VarChar)
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        sqlpFOR_BSU_IDs.Value = h_BSUID.Value.Replace("||", "|")
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@AsonDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtToDate.Text
        params("ToDT") = ""
        repSource.Parameter = params
        repSource.IncludeBSUImage = False
        repSource.Command = cmd
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Dim sqlpbLvl As New SqlParameter("@Lvl", SqlDbType.TinyInt)

        If rbWithPdc.Checked Then
            sqlpbLvl.Value = 61
        ElseIf rbWoPDC.Checked Then
            sqlpbLvl.Value = 62
        ElseIf rbOnlyPDC.Checked Then
            sqlpbLvl.Value = 63
        End If

        'params("RPT_CAPTION") = "Fee Cheque Collection Report For The Period :" & txtFromDate.Text.ToString() & " To " & txtToDate.Text.ToString()
        params("RPT_CAPTION") = "Fee Cheque Collection Report "
        If chkSummary.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEChqPaymentDetBank_FEE.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEChqPaidDetails_FEE.rpt"
        End If
        cmd.Parameters.Add(sqlpbLvl)
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, Session("Current_ACD_ID"), Session("sBsuid"))
        txtFromDate.Text = DTFROM
        'txtToDate.Text = DTTO
        txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        If txtFromDate.Text = "" Then
            txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        End If
    End Sub

    Private Sub FillBSUNames(Optional ByVal bGetAll As Boolean = False)
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If bGetAll Then h_BSUID.Value = Session("sBSUID")
        FillBSUNames(h_BSUID.Value, True)
    End Sub

    Private Function FillBSUNames(ByVal BSUIDs As String, ByVal bGetMulti As Boolean) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        If bGetMulti Then
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
        Else
            condition = "'" & BSUIDs & "'"
        End If
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID SVB_BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID SVB_BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class

