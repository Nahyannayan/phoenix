<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptFeeStudentFeeAging.aspx.vb" Inherits="Fees_Reports_ASPX_rptFeeStudentFeeAging" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(
        function () {
            if ($("#<%=chkGradeSummary.ClientID%>").prop("checked") == false) {
                $("#<%=chkNote.ClientID%>").prop("disabled", false);
                $("#<%=chkNote.ClientID%>").prop("checked", true);
                $("#<%=rblStuCount.ClientID%>").hide(550);
            }
            else {
                $("#<%=chkNote.ClientID%>").prop("disabled", true);
                $("#<%=chkNote.ClientID%>").prop("checked", false);
                $("#<%=rblStuCount.ClientID%>").show(500);
            }
        });
        $("#<%=chkGradeSummary.ClientID%>").live("click",
        function () {
            if ($("#<%=chkGradeSummary.ClientID%>").prop("checked") == false) {
                $("#<%=chkNote.ClientID%>").prop("disabled", false);
                $("#<%=chkNote.ClientID%>").prop("checked", true);
                $("#<%=rblStuCount.ClientID%>").hide(550);
            }
            else {
                $("#<%=chkNote.ClientID%>").prop("disabled", true);
                $("#<%=chkNote.ClientID%>").prop("checked", false);
                $("#<%=rblStuCount.ClientID%>").show(500);
            }
        });
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" EnableViewState="False" SkinID="LabelError">
                            </asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <asp:ValidationSummary
                                ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlBSUnit" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="trAcademicYear" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="trGrade" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Grade</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" Enabled="False">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">As On</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                    ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                        ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"></td>
                        <td align="left" colspan="3">
                            <asp:CheckBox ID="chkGradeSummary" runat="server" Text="Summary By Grade" Checked="True" CssClass="field-label" />
                            <span class="field-label">
                                <asp:RadioButtonList ID="rblStuCount" runat="server">
                                    <asp:ListItem Selected="True" Value="EC">Without Student Count</asp:ListItem>
                                    <asp:ListItem Value="IC">With Student Count</asp:ListItem>
                                </asp:RadioButtonList></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Filter by Student status</span></td>
                        <td align="left" colspan="3">
                            <span class="field-label">
                                <asp:RadioButton ID="rbAll" runat="server" Checked="True" GroupName="Active" Text="All" />
                                <asp:RadioButton ID="rbActive" runat="server" GroupName="Active" Text="Active" />
                                <asp:RadioButton ID="rbInactive" runat="server" GroupName="Active" Text="Inactive" />
                                <asp:CheckBox ID="chkNote" runat="server" Text="Show Notes" />
                            </span>
                        </td>
                    </tr>
                    <tr runat="server" id="trSummary">
                        <td align="left" colspan="3"></td>
                    </tr>
                    <tr runat="server">
                        <td align="left" width="20%"><span class="field-label">Ageing Buckets</span></td>

                        <td align="left" colspan="3">
                            <table>
                                <tr>
                                    <td align="left" width="13%">
                                        <asp:TextBox ID="txtBkt1" runat="server" MaxLength="3" >30</asp:TextBox>
                                    </td>
                                    <td align="left" width="13%">
                                        <asp:TextBox ID="txtBkt2" runat="server" MaxLength="3" >60</asp:TextBox>
                                    </td>
                                    <td align="left" width="13%">
                                        <asp:TextBox ID="txtBkt3" runat="server" MaxLength="3" >90</asp:TextBox>
                                    </td>
                                    <td align="left" width="13%">
                                        <asp:TextBox ID="txtBkt4" runat="server" MaxLength="3" >120</asp:TextBox>
                                    </td>
                                    <td align="left" width="14%">
                                        <asp:TextBox ID="txtBkt5" runat="server" MaxLength="3" >150</asp:TextBox>
                                    </td>
                                    <td align="left" width="14%">
                                        <asp:TextBox ID="txtBkt6" runat="server" MaxLength="3" >180</asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>


