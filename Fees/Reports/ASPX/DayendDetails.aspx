<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="DayendDetails.aspx.vb" Inherits="Fees_Reports_ASPX_DayendDetails" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">

    <script type="text/javascript" language="javascript">

        function Numeric_Only() {
            //alert(event.keyCode)
            if (event.keyCode < 48 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 45)
                { return false; }
                event.keyCode = 0
            }

        }

        function UpdateSum() {

            var sum = 0.0;

            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/txtDAmount/) > 0) {
                    CheckAmount(document.forms[0].elements[i])
                    sum += parseFloat(document.forms[0].elements[i].value);

                }

                if (document.forms[0].elements[i].name.search(/txtPrvAmount/) > 0) {
                    document.forms[0].elements[i].innerText = sum.toFixed(2);
                }
            }



        }

        function valueCopy(Amnt, Obj) {
            document.getElementById(Obj).value = Number(Amnt).toFixed(2);
        }

        function CheckAmount(e) {
            var amt;
            amt = parseFloat(e.value)
            if (isNaN(amt))
                amt = 0;
            e.value = amt.toFixed(2);
            return true;
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Consolidated Dayend Status
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                    <tr>
                        <td colspan="4" align="left">
                             <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                            <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"
                                CssClass="error"></asp:Label>--%>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <%--<tr class="subheader_img">
            <td colspan="2" style="height: 19px" align="left">
                Consolidated Dayend Status</td>
        </tr>--%>
                    <tr>
                        <td width="20%"><span class="field-label">Date</span> </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True" TabIndex="2"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                                PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Find" CausesValidation="False"
                                OnClick="btnFind_Click" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
                <br />
                <table align="center" width="100%">
                    <tr class="title-bg" runat="server" id="tr1" visible="false">
                        <td align="left" >Consolidated Dayend Status Details</td>
                    </tr>
                    <tr>
                        <td align="center" >
                            <asp:GridView ID="gvDayendDetails" runat="server" Width="100%" EmptyDataText="No Details Added" CssClass="table table-bordered table-row"
                                 AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="SLNO" HeaderText="S.No">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BSU_NAME" HeaderText="Business Unit"></asp:BoundField>
                                    <asp:BoundField DataField="FPD_DATE" HeaderText="Date"></asp:BoundField>
                                    <asp:BoundField DataField="FPD_LOGDATE" HeaderText="Log Date"></asp:BoundField>
                                    <asp:BoundField DataField="FPD_USER" HeaderText="User"></asp:BoundField>
                                    <asp:BoundField DataField="FPD_RETMESSAGE" HeaderText="Status"></asp:BoundField>
                                    <asp:BoundField DataField="FPD_PROCESSTIME" DataFormatString="{0:dd/MMM/yyyy hh:mm:ss tt}" HeaderText="Process Time"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                   
                    <tr class="title-bg" runat="server" id="tr2" visible="false">
                        <td align="left">Not Done Details</td>
                    </tr>
                    <tr>
                        <td align="center" >
                            <asp:GridView ID="gvNextstatus" runat="server" Width="100%" EmptyDataText="No Details Added" CssClass="table table-bordered table-row"
                               AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="SLNO" HeaderText="S.No">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BSU_NAME" HeaderText="Business Unit"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
