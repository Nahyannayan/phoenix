<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTransportConsolidatedReports.aspx.vb" Inherits="rptFEEOutstandingDetails" Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ Register Src="../../../UserControls/usrTransportBSUnits.ascx" TagName="usrTransportBSUnits"
    TagPrefix="uc1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                <br />
                <table width="100%">
                    <tr runat="server" visible="false">
                        <td align="left" class="matters" colspan="4">
                            <asp:RadioButton ID="radStud" runat="server" Checked="True" GroupName="STUD_ENQ"
                                Text="Student" />
                            <asp:RadioButton ID="radEnq" runat="server" GroupName="STUD_ENQ" Text="Enquiry" /></td>
                    </tr>
                    <tr runat="server" id="trAsonDate">
                        <td align="left" class="matters" width="20%"><span class="field-label">As On Date</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:TextBox ID="txtAsOnDate" runat="server" CssClass="inputbox">
                            </asp:TextBox><asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False"
                                ImageUrl="~/Images/calendar.gif" OnClientClick="return false" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAsOnDate"
                                ErrorMessage="To Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAsOnDate"
                                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr runat="server" id="trPeriod">
                        <td align="left" class="matters"><span class="field-label">From Date</span></td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputbox"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                    ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                        ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" class="matters"><span class="field-label">To Date</span></td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="inputbox">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Business Unit</span></td>
                        <td align="left" class="matters">
                            <uc1:usrTransportBSUnits ID="UsrTransportBSUnits1" runat="server" />

                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr id="trExclude" runat="server">
                        <td align="left" class="matters" colspan="4">
                            <asp:CheckBox ID="chkExclude" runat="server" Text="PDC Exclude"></asp:CheckBox></td>
                    </tr>
                    <tr id="trSortby" runat="server">
                        <td align="left" class="matters"><span class="field-label">Sort By</span></td>
                        <td align="left" class="matters" colspan="3">
                            <asp:RadioButton ID="radGradeWise" runat="server" Checked="True" GroupName="SORT" CssClass="field-label"
                                Text="Grade Wise"></asp:RadioButton>
                            <asp:RadioButton ID="radBuswise" runat="server" GroupName="SORT" CssClass="field-label" Text="Bus wise"></asp:RadioButton></td>
                    </tr>
                    <tr id="trSummary" runat="server">
                        <td align="left" class="matters" colspan="4">
                            <asp:RadioButton ID="radDetailed" runat="server" Checked="True" Text="Detailed" CssClass="field-label" ValidationGroup="SUMMARY" GroupName="SUMMARY" />
                            <asp:RadioButton ID="radGradeSummary" runat="server" Text="Gradewise Summary" GroupName="SUMMARY" CssClass="field-label" />
                            <asp:RadioButton ID="radSummary" runat="server" Text="Summary" ValidationGroup="SUMMARY" CssClass="field-label" GroupName="SUMMARY" /></td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtToDate" PopupButtonID="imgToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calAsOnDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton1" TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calAsOnDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>

