Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class Fees_Reports_ASPX_rptTransportParentCallingList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            Select Case MainMnu_code
                Case "F725157"
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Parents Calling List"
            End Select
            Me.txtCutoffAmt.Text = "0.00"
            Me.txtToDate.Text = Format(Date.Now, OASISConstants.DateFormat)
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
            'usrMessageBar2.ShowNotification("", UserControls_usrMessageBar.WarningType.Danger)
        End If
        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        h_BSUID.Value = UsrTransportBSUnits1.GetSelectedNode()
        Select Case MainMnu_code
            Case "F725157"
                GenerateParentsCallingList()
        End Select
    End Sub

    Private Sub GenerateParentsCallingList()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand("TRANSPORT.GET_PARENTS_CALLINGLIST")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTUBSU_ID As New SqlParameter("@STU_BSU_IDs", SqlDbType.VarChar, 300)
        sqlpSTUBSU_ID.Value = UtilityObj.GetBSUnitWithSeperator(UsrTransportBSUnits1.GetSelectedNode(), OASISConstants.SEPARATOR_PIPE)
        cmd.Parameters.Add(sqlpSTUBSU_ID)

        Dim sqlpCUT_OFF_AMT As New SqlParameter("@CUT_OFF_AMT", SqlDbType.Decimal)
        If IsNumeric(Me.txtCutoffAmt.Text.Trim) Then
            sqlpCUT_OFF_AMT.Value = CDbl(Me.txtCutoffAmt.Text.Trim)
        Else
            sqlpCUT_OFF_AMT.Value = 0
        End If
        'sqlpCUT_OFF_AMT.Value = IIf(IsNumeric(Me.txtCutoffAmt.Text.Trim), CDbl(Me.txtCutoffAmt.Text.Trim), 0)
        cmd.Parameters.Add(sqlpCUT_OFF_AMT)

        Dim sqlpTODT As New SqlParameter("@ASON_DATE", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpbActive As New SqlParameter("@bActive", SqlDbType.SmallInt)
        sqlpbActive.Value = Me.rblActive.SelectedValue
        cmd.Parameters.Add(sqlpbActive)

        cmd.Connection = New SqlConnection(str_conn)
        cmd.CommandTimeout = 0
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = "PARENTS CALLING LIST"
        params("DateHead") = "AS ON " & Me.txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True

        repSource.ResourceName = "../../FEES/REPORTS/RPT/RptTransportParentCallingList.rpt"

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

