<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptTransportParentCallingList.aspx.vb" Inherits="Fees_Reports_ASPX_rptTransportParentCallingList"
    Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %> 
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrTransportBSUnits.ascx" TagName="usrTransportBSUnits"
    TagPrefix="uctBSU" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <script language="javascript" type="text/javascript">

                </script>
                 <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                <%--<asp:Label ID="lblError" runat="server" CssClass="error">                    
                </asp:Label>--%>
                <asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                <br />
                <table align="center" cellpadding="0" cellspacing="0" width="100%">

                    <tr id="trBSUnit" runat="server">
                        <td align="left" width="20%"><span class="field-label">Business Unit</span>
                        </td>

                        <td align="left" colspan="3">
                            <div class="checkbox-list-full">
                                <uctBSU:usrTransportBSUnits ID="UsrTransportBSUnits1" runat="server"></uctBSU:usrTransportBSUnits>
                            </div>
                        </td>
                    </tr>
                    <tr runat="server">
                         <td align="left" width="20%"></td>
                        <td align="left" width="30%">
                         <span class="field-label">   <asp:RadioButtonList ID="rblActive" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="2">All</asp:ListItem>
                                <asp:ListItem Value="1">Active</asp:ListItem>
                                <asp:ListItem Value="0">InActive</asp:ListItem>
                            </asp:RadioButtonList></span></td>
                        <td align="left" width="20%"><span class="field-label">Cut-off Amount</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCutoffAmt" runat="server">0.00</asp:TextBox>
                        </td>
                    </tr>

                    <tr runat="server">
                        <td align="left" width="20%"><span class="field-label">AsOn Date</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" />
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <input id="hfBSU" runat="server" type="hidden" />
                <input id="h_BSUID" runat="server" type="hidden" />
                <input id="hfFEECounter" runat="server" type="hidden" />
                <asp:HiddenField ID="h_bTransport" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>
