<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptTransportOtherFeeCollection.aspx.vb" Inherits="Fees_Reports_ASPX_rptTransportOtherFeeCollection" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Src="../../../UserControls/usrTransportBSUnits.ascx" TagName="usrTransportBSUnits"
    TagPrefix="uc1" %>
<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function CheckBoxListSelect(cbControl, state) {
            var chkBoxList = document.getElementById(cbControl);
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            for (var i = 0; i < chkBoxCount.length; i++) {
                chkBoxCount[i].checked = state;
            }

            return false;
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                 <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                <table width="100%">
                    <%-- <tr class="subheader_img">
            <td align="left" colspan="4" style="height: 19px">
                <asp:Label ID="lblReportCaption" runat="server"></asp:Label></td>
        </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left"  colspan="2">
                            <div class="checkbox-list-full">
                                <uc1:usrTransportBSUnits ID="UsrTransportBSUnits1" runat="server"></uc1:usrTransportBSUnits>
                                 <uc1:usrBSUnits ID="UsrBSUnits1" runat="server"></uc1:usrBSUnits>
                            </div>                          
                             </td>
                     
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Date From</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox runat="server" ID="txtFromDate"></asp:TextBox><asp:Image
                                ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif"></asp:Image>
                        </td>
                        <td align="left" width="20%"><span class="field-label">To Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox runat="server" ID="txtToDate">
                            </asp:TextBox><asp:Image ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif"></asp:Image></td>
                    </tr>
                    <tr>

                        <td align="left" width="20%"><span class="field-label">Select </span><a id="A1" href="#" onclick="javascript: CheckBoxListSelect ('<%= rblFilter.ClientID %>',true)">All</a> | <a id="A2" href="#" onclick="javascript: CheckBoxListSelect ('<%= rblFilter.ClientID %>',false)">None</a></td>
                        <td colspan="2">
                            <div class="checkbox-list-full">
                                <asp:CheckBoxList ID="rblFilter" runat="server">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <%--<asp:CheckBox ID="ChkConsolidation" runat="server" Text="Consolidation"></asp:CheckBox>--%>
                            <asp:RadioButtonList ID="RBLConsolidation" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="0">Detailed</asp:ListItem>
                                <asp:ListItem Value="1">Consolidate IncomeHeads as Column</asp:ListItem>
                                <asp:ListItem Value="2">Consolidate IncomeHeads as Row</asp:ListItem>
                            </asp:RadioButtonList></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="BtnShow" runat="server" Text="Generate Report" CssClass="button"
                                OnClick="BtnShow_Click" />
                            <asp:Button ID="BtnCancel" runat="server" Text="Cancel" CssClass="button" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>
