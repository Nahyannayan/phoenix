﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptOtherFeeCollection.aspx.vb" Inherits="Fees_Reports_ASPX_rptOtherFeeCollection" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Src="~/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../../../Scripts/jquery-1.10.2.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=chkAllCollections.ClientID%>').attr('checked', true);
            SelectAll();
        });
        function SelectAll() {
            var chk = $('#<%=chkAllCollections.ClientID%>');
            if (chk.is(':checked') == true)
                $('#<%=ddlCollection.ClientID%>').prop('disabled', true);
            else
                $('#<%=ddlCollection.ClientID%>').prop('disabled', false);
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
             <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                <br />
                <table align="center" cellpadding="0" cellspacing="0" width="100%">
                   
                    <tr>
                        <td align="left" width="20%"><span class="field-label">From Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server" ></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" />
                            <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                    ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%"><span class="field-label">To Date</span></td>
                          <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server" ></asp:TextBox>
                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" />
                            <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgToDate" TargetControlID="txtToDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtToDate" TargetControlID="txtToDate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Units</span></td>
                        <td align="left" colspan="3">
                            <div class="checkbox-list-full"> 
                            <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                                </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Collection</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlCollection" runat="server" DataSourceID="sdsCollection" DataTextField="COL_DESCR"
                                DataValueField="COL_ID">
                            </asp:DropDownList><asp:CheckBox ID="chkAllCollections" Text="All" runat="server" />
                        </td>
                        <td align="left" colspan="2">
                            <asp:RadioButtonList ID="rblSelection" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="0">Summary</asp:ListItem>
                                <asp:ListItem Value="1">Detailed</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="sdsCollection" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                    SelectCommand="SELECT [COL_ID], [COL_DESCR], [COL_ACT_ID] FROM [COLLECTION_M] WHERE ([COL_ID] <> 1)"></asp:SqlDataSource>
            </div>
        </div>
    </div>
</asp:Content>

