Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptFEEChargeDetails
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            gvStudentDetails.Attributes.Add("bordercolor", "#1b80b6")
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            SetStudentGrid()
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_REP_NORM_FEECHARGEDETAILS
                    lblReportCaption.Text = "Fee Charge by Student Detailed Report"
                Case "F725099"
                    lblReportCaption.Text = "Other Fee Charge Detailed Report"
                    radStudent.Text = "Posted"
                    radEnquiry.Text = "Open"

            End Select

            BindBusinessUnit()
            'BindAcademicYear(ddlBSUnit.SelectedValue)
            FillACD()
            BindGrade()

            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If chkStudentby.Checked And h_STUD_ID.Value = "" Then
            'lblError.Text = "Please select student(s)"
            usrMessageBar2.ShowNotification("Please select student(s)", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_NORM_FEECHARGEDETAILS
                GenerateFeeChargeDetails()
            Case "F725099"
                OtherFeeChargeDetails()
        End Select
    End Sub

    Public Sub GetBSU_RoundOff(ByVal BSU_ID As String)
        'Author(--Swapna)
        'Date   --18/Aug/2011
        'Purpose--Get selected BSU's round off
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim query As String = ""

        query = "SELECT  BSU_ROUNDOFF FROM BUSINESSUNIT_M WHERE BSU_ID = '" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(query, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

        While reader.Read
            ViewState("RoundOffVal") = reader.Item("BSU_ROUNDOFF")
        End While

    End Sub

    Private Sub GenerateFeeChargeDetails()
        Dim cmd As New SqlCommand("[FEES].[F_GETFEECHARGED_OASIS]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(h_STUD_ID.Value, XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)

        Dim sqlpMODE As New SqlParameter("@MODE", SqlDbType.VarChar, 10)
        If chkStudentby.Checked Then
            sqlpMODE.Value = "STUDENT"
        Else
            sqlpMODE.Value = "ALL"
        End If
        cmd.Parameters.Add(sqlpMODE)

        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        If radEnquiry.Checked Then
            sqlpSTU_TYPE.Value = "E"
        Else
            sqlpSTU_TYPE.Value = "S"
        End If
        cmd.Parameters.Add(sqlpSTU_TYPE) 
 
        Dim sqlpFROMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFROMDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpGRD_ID As New SqlParameter("@GRD_ID", SqlDbType.VarChar, 20)
        sqlpGRD_ID.Value = ddlGrade.SelectedValue
        cmd.Parameters.Add(sqlpGRD_ID)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        Dim caption = "FEE Charge Report"
        If chkStudentby.Checked Then
            caption = caption & " Studentwise"
        End If
        If ChkSummary.Checked Then
            caption = caption & " Summary"
        End If
        
        GetBSU_RoundOff(ddlBSUnit.SelectedValue)
        params("userName") = Session("sUsr_name")
        params("FROMDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text

        params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("RPT_CAPTION") = caption
        params("ACY_DESCR") = ddlAcademicYear.SelectedItem.Text
        params("RoundOffVal") = ViewState("RoundOffVal")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If ChkSummary.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEECHARGEREPORT_SUMMARY.rpt"

        Else
            If chkStudentby.Checked Then
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEECHARGEREPORT_Student.rpt"
            Else
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEECHARGEREPORT.rpt"
            End If
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx?isExport=true", True)
            ReportLoadSelectionExport()
        Else
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        End If
    End Sub


    Private Sub OtherFeeChargeDetails()
        Dim Status As String = "Posted"
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("[FEES].[Report_FEEOTHCHARGE]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTU_IDS As New SqlParameter("@STUIDs", SqlDbType.VarChar)
        If chkStudentby.Checked Then
            sqlpSTU_IDS.Value = h_STUD_ID.Value.Replace("||", "@")
        Else
            sqlpSTU_IDS.Value = ""
        End If
        cmd.Parameters.Add(sqlpSTU_IDS)

        Dim sqlpFROMDT As New SqlParameter("@FRM_DATE", SqlDbType.DateTime)
        sqlpFROMDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDT)

        Dim sqlpTODT As New SqlParameter("@TO_DATE", SqlDbType.DateTime)
        sqlpTODT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpGRD_ID As New SqlParameter("@GRD_ID", SqlDbType.VarChar, 20)
        If ddlGrade.SelectedValue = "-1" Then
            sqlpGRD_ID.Value = "%"
        Else
            sqlpGRD_ID.Value = ddlGrade.SelectedValue
        End If
        cmd.Parameters.Add(sqlpGRD_ID)

        Dim sqlpPOSTED As New SqlParameter("@FOH_Bposted", SqlDbType.VarChar, 20)
        If radEnquiry.Checked Then
            sqlpPOSTED.Value = "0"
            Status = "Open"
        Else
            sqlpPOSTED.Value = "1"
        End If
        cmd.Parameters.Add(sqlpPOSTED)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("ReportHead") = "OTHER FEE CHARGE DETAILS  ( " & Status & " ) FROM  :" & txtFromDate.Text.ToString() & " : .TO. : " & txtToDate.Text.ToString()
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = False
        repSource.ResourceName = "../../FEES/REPORTS/RPT/FeeOtherChargedetails.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx?isExport=true", True)
            ReportLoadSelectionExport()
        Else
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        End If
    End Sub

    Private Sub BindGrade()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim sql_query As String = "SELECT GRM_DISPLAY, GRM_GRD_ID FROM GRADE_BSU_M " & _
        " INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID WHERE GRM_ACD_ID = " & _
        ddlAcademicYear.SelectedValue & " AND GRM_BSU_ID ='" & ddlBSUnit.SelectedValue & "' "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, sql_query)
        If (dsData IsNot Nothing) AndAlso (dsData.Tables(0).Rows.Count > 0) Then
            Dim dr As DataRow = dsData.Tables(0).NewRow
            dr(0) = "ALL"
            dr(1) = "-1"
            dsData.Tables(0).Rows.Add(dr)
        End If
        ddlGrade.DataSource = dsData
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
        If ddlGrade.Items.Count > 0 Then
            ddlGrade.Items.FindByValue("-1").Selected = True
        End If
    End Sub

    Private Sub BindBusinessUnit()
        ddlBSUnit.DataSource = FeeCommon.GETBSUFORUSER(Session("sUsr_name"))
        ddlBSUnit.DataTextField = "bsu_name"
        ddlBSUnit.DataValueField = "bsu_id"
        ddlBSUnit.DataBind()
        ddlBSUnit.Items.FindByValue(Session("sBsuid")).Selected = True
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        SetAcademicYearDate()
        BindGrade()
    End Sub

    Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSUnit.SelectedIndexChanged
        FillACD()
        BindGrade()
    End Sub

    Protected Sub chkStudentby_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkStudentby.CheckedChanged
        SetStudentGrid()
    End Sub

    Sub SetStudentGrid()
        If chkStudentby.Checked Then
            imgStudent.Enabled = True
            gvStudentDetails.Visible = True
        Else
            imgStudent.Enabled = False
            h_STUD_ID.Value = ""
            gvStudentDetails.Visible = False
            FillSTUNames(h_STUD_ID.Value)
        End If
    End Sub

    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, ddlAcademicYear.SelectedItem.Value, ddlBSUnit.SelectedValue)
        txtFromDate.Text = DTFROM
        DTTO = Format(Now.Date, OASISConstants.DateFormat)
        txtToDate.Text = DTTO
        ViewState("DTFROM") = DTFROM
        ViewState("DTTO") = DTTO
    End Sub
    Protected Sub h_STUD_ID_ValueChanged(sender As Object, e As EventArgs) Handles h_STUD_ID.ValueChanged
        Dim ERR_MSG As String = DirectCast(sender, HiddenField).Value
        FillSTUNames(h_STUD_ID.Value)
        'h_DEPTID.Value = ""
    End Sub

    Private Function FillSTUNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        If radEnquiry.Checked Then
            str_Sql = " SELECT STU_NO , STU_NAME FROM FEES.vw_OSO_ENQUIRY_COMP WHERE STU_ID IN (" + condition + ")"
        Else
            str_Sql = " SELECT STU_NO , STU_NAME FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID IN (" + condition + ")"
        End If
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
        gvStudentDetails.DataSource = ds
        gvStudentDetails.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBSUnit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        SetAcademicYearDate()
    End Sub

    Protected Sub lblAddNewStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        AddStudent()
    End Sub

    Private Sub AddStudent()
        If txtStudName.Text <> "" Then
            Dim str_stuid As String = FeeCommon.GetTransportStudentID(txtStudName.Text, ddlBSUnit.SelectedItem.Value, radEnquiry.Checked, False)
            If str_stuid <> "" Then
                If h_STUD_ID.Value.EndsWith("||") Then
                    h_STUD_ID.Value = h_STUD_ID.Value & str_stuid
                Else
                    h_STUD_ID.Value = h_STUD_ID.Value & "||" & str_stuid
                End If
                txtStudName.Text = ""
                FillSTUNames(h_STUD_ID.Value)
            End If
        End If
    End Sub

    Protected Sub txtStudName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        AddStudent()
    End Sub

    Protected Sub radEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        h_STUD_ID.Value = ""
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Protected Sub radStudent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        h_STUD_ID.Value = ""
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExporttoexcel.Click
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub

    Sub ReportLoadSelectionExport()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx?isExport=true');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx?isExport=true','_blank');", True)
        End If
    End Sub
End Class
