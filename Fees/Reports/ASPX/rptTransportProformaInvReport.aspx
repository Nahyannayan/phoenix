<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptTransportProformaInvReport.aspx.vb" Inherits="Fees_Reports_ASPX_rptTransportProformaInvReport"
    Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrTransportBSUnits.ascx" TagName="usrTransportBSUnits"
    TagPrefix="uctBSU" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                <br />
                <table width="100%">
                    <tr id="trBSUnit" runat="server">
                        <td align="left" class="matters" width="20%"><span class="field-label">Business Unit</span>
                        </td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlBSU" DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover"
                                PopupControlID="PanelTree" PopupPosition="Center" TargetControlID="pnlLink" PopDelay="25" />
                        </td>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span>
                        </td>
                        <td align="right" class="matters" style="text-align: left" width="30%">
                            <asp:DropDownList ID="ddlACDYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td align="left" class="matters"><span class="field-label">Term/Months</span>
                        </td>
                        <td align="right" class="matters" style="text-align: left">
                            <asp:RadioButton ID="rbTermly" runat="server" Text="Termly" Checked="True" GroupName="trm" CssClass="field-label"
                                AutoPostBack="True" />
                            <asp:RadioButton ID="rbMonthly" runat="server" Text="Monthly" GroupName="trm" AutoPostBack="True" CssClass="field-label" />
                            <asp:Panel ID="pnlLink" runat="server">
                                <asp:LinkButton ID="lnkBtnSelectmonth" runat="server" OnClientClick="return false;"
                                    TabIndex="15">Select Month/Term </asp:LinkButton>
                            </asp:Panel>
                            <asp:Panel ID="PanelTree" runat="server" CssClass="panel-cover"
                                Style="display: none">
                                <div>
                                    <asp:TreeView ID="trMonths" ShowCheckBoxes="all" onclick="client_OnTreeNodeChecked();"
                                        runat="server">
                                    </asp:TreeView>
                                    <div style="text-align: center;">
                                        <asp:Button ID="btnNarrInsert" runat="server" CssClass="button_small" Text="Fill Narration" />

                                    </div>
                                </div>
                            </asp:Panel>
                        </td>

                        <td align="left" class="matters"><span class="field-label">AsOn Date</span>
                        </td>
                        <td align="right" class="matters" style="text-align: left">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="inputbox"></asp:TextBox><asp:ImageButton
                                ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" />
                        </td>
                    </tr>
                    <tr runat="server">
                        <td align="right" class="matters" style="text-align: left" colspan="4">
                            <asp:RadioButtonList ID="RBLSummary" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="0"><span class="field-label">Summary</span></asp:ListItem>
                                <asp:ListItem Value="1"><span class="field-label">Groupby Company</span></asp:ListItem>
                                <asp:ListItem Value="2"><span class="field-label">Detailed</span></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <input id="hfBSU" runat="server" type="hidden" />
                <input id="h_BSUID" runat="server" type="hidden" />
                <input id="hfFEECounter" runat="server" type="hidden" />
                <asp:HiddenField ID="h_bTransport" runat="server" />
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
