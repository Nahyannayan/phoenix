Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptFEEChargedAdv
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            'Me.trFeeType.Visible = False
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            lblReportCaption.Text = "Fee charged in advance"
            BindFeeTypes()
            
            SelectAllTreeView(trvFeeTypes.Nodes, True)
            For i As Integer = 0 To trvFeeTypes.Nodes.Count - 1
                trvFeeTypes.Nodes(i).CollapseAll()
            Next
            'BindAcademicYear(ddlBSUnit.SelectedValue)
            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            Dim DTFROM As String = String.Empty
            Dim DTTO As String = String.Empty
            FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, Session("Current_ACD_ID"), Session("sBsuid"))
            txtAsOnDate.Text = DTFROM
            If txtAsOnDate.Text = "" Then
                txtAsOnDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            End If
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        GenerateFeeChargedInAdvance()
    End Sub

    Private Sub GenerateFeeChargedInAdvance()
        Dim cmd As New SqlCommand("FEES.SP_CHARGE_INADV_RPT")
        cmd.CommandType = CommandType.StoredProcedure
        UpdateFeesSelected()

        Dim sqlParam(4) As SqlClient.SqlParameter
        sqlParam(0) = New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlParam(0).Value = Session("sBsuid")
        cmd.Parameters.Add(sqlParam(0))
        sqlParam(1) = New SqlParameter("@ASONDT", SqlDbType.DateTime)
        sqlParam(1).Value = Me.txtAsOnDate.Text
        cmd.Parameters.Add(sqlParam(1))
        sqlParam(3) = New SqlParameter("@FEE_IDS", SqlDbType.VarChar)
        sqlParam(3).Value = ViewState("FEE_IDs")
        cmd.Parameters.Add(sqlParam(3))
        sqlParam(4) = New SqlParameter("@GroupBy", SqlDbType.VarChar)
        sqlParam(4).Value = Me.rblFilter1.SelectedValue
        cmd.Parameters.Add(sqlParam(4))

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Session("CMD") = cmd
        'Dim ds As DataTable = Mainclass.getDataTable("[FEES].[F_GetFeeRefundDetails]", sqlParam, cmd.Connection.ConnectionString)
        'SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "[FEES].[F_GetFeeRefundDetails]", cmd.Parameters)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        Dim caption As String = "Fee Charged in Advance"

        params("userName") = Session("sUsr_name")
        params("ASONDT") = txtAsOnDate.Text
        params("BSU_NAME") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT BSU_NAME FROM oasis..BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "'")
        params("RPT_CAPTION") = caption
        If Me.rblFilter1.SelectedValue <> "S" Then
            params("GroupBy") = Me.rblFilter1.SelectedValue
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptChargedInAdvanceSummary.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptChargedInAdvance.rpt"
        End If
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx?isExport=true", True)
        Else
            '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
            'Response.Redirect("../../../RptToHTML.aspx", True)
        End If
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExporttoexcel.Click
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

    Public Sub BindFeeTypes()
        trvFeeTypes.Nodes.Clear()
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty ' SSC_ID hard coded as 4(Others)
        str_Sql = " SELECT DISTINCT ISNULL(FEE_CHRG_INADV_ACT_ID,10) SSC_ID,ACT_NAME SSC_DESC "
        str_Sql = str_Sql & " FROM FEES.FEES_M FM INNER JOIN OASISFIN..ACCOUNTS_M ON ACT_ID=FEE_CHRG_INADV_ACT_ID ORDER BY SSC_ID,SSC_DESC"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("SSC_DESC").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("SSC_ID").ToString()
                trvFeeTypes.Nodes.Add(ParentNode)
            Next

        End If
        Dim nodes(trvFeeTypes.Nodes.Count) As TreeNode
        Dim mI As Int32 = 0
        For Each node As TreeNode In trvFeeTypes.Nodes
            Dim childNodes As Integer = BindChildNodes(node)
            If childNodes < 1 Then
                nodes(mI) = node
                mI += 1
            End If
        Next
        Dim j As Integer = trvFeeTypes.Nodes.Count
        While j >= 0
            If Not IsNothing(nodes(j)) Then
                trvFeeTypes.Nodes.Remove(nodes(j))
            End If
            j -= 1
        End While
        trvFeeTypes.ExpandAll()
    End Sub
    Public Function BindChildNodes(ByVal ParentNode As TreeNode) As Integer

        Dim childnodeValue = ParentNode.Value
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty
        'str_Sql = " SELECT DISTINCT ISNULL(FEE_ID, 0) FEE_ID,LTRIM(RTRIM(ISNULL(FEE_DESCR,''))) FEE_DESCR " & _
        '          "FROM FEES.FEES_M FM LEFT JOIN OASIS.dbo.SERVICES_SYS_M SYM ON FM.FEE_SVC_ID=SYM.SVC_ID " & _
        '          "LEFT JOIN OASIS.dbo.SERVICES_CATEGORY SC ON SYM.SVC_SSC_ID=SC.SSC_ID WHERE ISNULL(SSC_ID,4)='" & childnodeValue & "' " & _
        '          "ORDER BY ISNULL(FEE_ID, 0),LTRIM(RTRIM(ISNULL(FEE_DESCR,''))) "

        str_Sql = " SELECT DISTINCT ISNULL(FEE_ID, 0) FEE_ID,LTRIM(RTRIM(ISNULL(FEE_DESCR,''))) FEE_DESCR "
        str_Sql = str_Sql & "         FROM FEES.FEES_M FM WHERE ISNULL(FEE_CHRG_INADV_ACT_ID,4)='" & childnodeValue & "' "
        str_Sql = str_Sql & "         ORDER BY ISNULL(FEE_ID, 0),LTRIM(RTRIM(ISNULL(FEE_DESCR,''))) "

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("FEE_DESCR").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("FEE_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If
        Return ds.Tables(0).Rows.Count
        'For Each node As TreeNode In ParentNode.ChildNodes
        '    BindChildNodes(node)
        'Next
    End Function

    Private Sub UpdateFeesSelected()
        Try
            Dim FEE_IDs As String = ""
            For Each node As TreeNode In trvFeeTypes.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If FEE_IDs.Trim() = "" Then
                    FEE_IDs = node.Value
                Else
                    FEE_IDs = FEE_IDs + "|" + node.Value
                End If
            Next
            ViewState("FEE_IDs") = FEE_IDs
        Catch
            ViewState("FEE_IDs") = ""
        End Try
    End Sub
    Private Sub SelectAllTreeView(ByVal nodes As TreeNodeCollection, ByVal selAll As Boolean)
        For Each node As TreeNode In nodes
            node.Checked = selAll
            If node.ChildNodes.Count > 0 Then
                SelectAllTreeView(node.ChildNodes, selAll)
            End If
        Next
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
