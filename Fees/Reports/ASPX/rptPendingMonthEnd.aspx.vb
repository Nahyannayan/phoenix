Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
imports GemBox.Spreadsheet  

Partial Class Fees_Reports_ASPX_rptPendingMonthEnd
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            txtAsOnDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            Select Case ViewState("MainMnu_code").ToString
                Case "F704015"
                    lblReportCaption.Text = "Month End Pending Status"
            End Select
            UsrBSUnits1.MenuCode = ViewState("MainMnu_code").ToString
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            '   lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            '    lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case ViewState("MainMnu_code").ToString
            Case "F704015"
                GetMonthendPendingStatus()
        End Select
    End Sub

    Private Sub GetMonthendPendingStatus()
        Dim cmd As New SqlCommand("FEES.GetMonthendPendingStatus")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFromDT As New SqlParameter("@Asondate", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtAsOnDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpbDetailed As New SqlParameter("@bDetailed", SqlDbType.Bit)
        sqlpbDetailed.Value = chkDetailed.Checked
        cmd.Parameters.Add(sqlpbDetailed)


        Dim sqlpFOR_BSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlpFOR_BSU_IDs.Value = UsrBSUnits1.GetSelectedNode("|")
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("DateHead") = txtAsOnDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        If chkDetailed.Checked Then
            params("RptHead") = "Month End Pending Status - Detailed"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptPendingMonthEndDetail_detailed.rpt"
        Else
            params("RptHead") = "Month End Pending Status"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptPendingMonthEndDetail.rpt"
        End If
        Session("ReportSource") = repSource
        ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
