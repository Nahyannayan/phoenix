Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_rptFeeStudentDueAdvanceAging
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            txtFromDate.Text = Format(Date.Now, "dd/MMM/yyyy")
            ViewState("MainMnu_Caption") = Mainclass.GetMenuCaption(MainMnu_code)
            lblReportCaption.Text = ViewState("MainMnu_Caption")
            BindBusinessUnit()
            BindAcademicYear(ddlBSUnit.SelectedValue)
            BindGrade()
            Page.Title = OASISConstants.Gemstitle
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTFromDate") = txtFromDate.Text
        If UtilityObj.IsFutureDate(txtFromDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If IsDate(txtFromDate.Text) Then
            If IsDate(txtFromDate.Text) And IsDate(ViewState("DTFROM")) Then
                If CDate(txtFromDate.Text) < CDate(ViewState("DTFROM")) Or CDate(txtFromDate.Text) > CDate(ViewState("DTTO")) Then
                    'lblError.Text = "Invalid Date"
                    usrMessageBar2.ShowNotification("Invalid Date", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            End If
        Else
            'lblError.Text = "Invalid Date (Check Academic Year start date and end date)"
            usrMessageBar2.ShowNotification("Invalid Date (Check Academic Year start date and end date)", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMnu_code
            Case "F725113"
                rptDueAdvanceAGINGSummary()
        End Select
    End Sub

    Private Sub rptDueAdvanceAGINGSummary()
        Dim cmd As New SqlCommand("FEES.SP_RPT_STUDENT_DUE_ADVANCE_AGING")
        cmd.CommandType = CommandType.StoredProcedure

        Dim CaptionHead As String = "Student Fee Ageing"
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFROMDOCDT As New SqlParameter("@ASONDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpFilterBy As New SqlParameter("@FilterBy", SqlDbType.VarChar, 15)
        sqlpFilterBy.Value = False
        If rbAll.Checked Then
            sqlpFilterBy.Value = "ALL"
            CaptionHead = ViewState("MainMnu_Caption") & " (Consolidated)"
        ElseIf rbActive.Checked Then
            sqlpFilterBy.Value = "ACTIVE"
            CaptionHead = ViewState("MainMnu_Caption") & " (Active)"
        Else
            sqlpFilterBy.Value = "INACTIVE"
            CaptionHead = ViewState("MainMnu_Caption") & " (Inactive)"
        End If
        cmd.Parameters.Add(sqlpFilterBy)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        GetBSU_RoundOff(ddlBSUnit.SelectedValue)
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("FromDATE") = txtFromDate.Text
        params("RoundOffVal") = ViewState("RoundOffVal")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = ddlBSUnit.SelectedItem.Value
        params("RPT_CAPTION") = CaptionHead
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rpfFeeDueAdvanceAgingSummary_Student.rpt"

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub GetBSU_RoundOff(ByVal BSU_ID As String)
        'Author(--Swapna)
        'Date   --18/Aug/2011
        'Purpose--Get selected BSU's round off
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim query As String = ""

        query = "SELECT  BSU_ROUNDOFF FROM BUSINESSUNIT_M WHERE BSU_ID= '" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(query, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

        While reader.Read
            ViewState("RoundOffVal") = reader.Item("BSU_ROUNDOFF")
        End While

    End Sub

    Private Sub BindGrade()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim sql_query As String = "SELECT GRM_DISPLAY, GRM_GRD_ID FROM GRADE_BSU_M " & _
        " INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID WHERE GRM_ACD_ID = " & _
        ddlAcademicYear.SelectedValue & " AND GRM_BSU_ID ='" & ddlBSUnit.SelectedValue & "' "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, sql_query)
        If (dsData IsNot Nothing) AndAlso (dsData.Tables(0).Rows.Count > 0) Then
            Dim dr As DataRow = dsData.Tables(0).NewRow
            dr(0) = "ALL"
            dr(1) = "-1"
            dsData.Tables(0).Rows.Add(dr)
        End If
        ddlGrade.DataSource = dsData
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
        If ddlGrade.Items.Count > 0 Then
            ddlGrade.Items.FindByValue("-1").Selected = True
        End If
    End Sub

    Private Sub BindBusinessUnit()
        ddlBSUnit.DataSource = FeeCommon.GETBSUFORUSER(Session("sUsr_name"))
        ddlBSUnit.DataTextField = "bsu_name"
        ddlBSUnit.DataValueField = "bsu_id"
        ddlBSUnit.DataBind()
        ddlBSUnit.Items.FindByValue(Session("sBsuid")).Selected = True
    End Sub

    Private Sub BindAcademicYear(ByVal BSU_ID As Integer)
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBSUnit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSUnit.SelectedIndexChanged
        BindAcademicYear(ddlBSUnit.SelectedValue)
        BindGrade()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
