<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptOnlineFeeCollection.aspx.vb" Inherits="Fees_Reports_ASPX_rptOnlineFeeCollection" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ Register Src="../../../UserControls/usrTransportBSUnits.ascx" TagName="usrTransportBSUnits" TagPrefix="uc1" %>
<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table width="100%">
                    <tr class="matters">
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" width="30%">
                            <div class="checkbox-list">
                                <uc1:usrTransportBSUnits ID="UsrTransportBSUnits1" runat="server"></uc1:usrTransportBSUnits>
                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server"></uc1:usrBSUnits>
                            </div>
                        </td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                    </tr>
                    <tr class="matters">
                        <td align="left"><span class="field-label">Date From</span></td>
                        <td align="left">
                            <asp:TextBox runat="server" ID="txtFromDate"></asp:TextBox><asp:Image ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif"></asp:Image>
                        </td>
                        <td align="left"><span class="field-label">To Date</span></td>
                        <td align="left">
                            <asp:TextBox runat="server" ID="txtToDate">
                            </asp:TextBox><asp:Image ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif"></asp:Image></td>
                    </tr>
                    <tr class="matters">
                        <td align="left"></td>
                        <td align="left" colspan="3">
                            <asp:RadioButtonList ID="rblFilter" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="3"><span class="field-label">GEMS Co-Branded</span></asp:ListItem>
                                <asp:ListItem Value="2"><span class="field-label">Others</span></asp:ListItem>
                                <asp:ListItem Selected="True" Value="0"><span class="field-label">All</span></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td align="left" colspan="4">
                            <asp:CheckBox ID="ChkConsolidation" runat="server" Text="Consolidation" CssClass="field-label"></asp:CheckBox></td>
                    </tr>
                    <tr class="matters">
                        <td colspan="4" align="center">
                            <asp:Button ID="BtnShow" runat="server" Text="Generate Report" CssClass="button" OnClick="BtnShow_Click" />
                            <asp:Button ID="BtnCancel" runat="server" Text="Cancel" CssClass="button" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>

