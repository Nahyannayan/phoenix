Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Fees_Reports_ASPX_rptFEEReceiptSummary
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ClientScript.RegisterStartupScript(Me.GetType(), _
        "script", "<script language='javascript'>  CheckOnPostback(); </script>")
        If Not Page.IsPostBack Then
            hfBSU.Value = "display"
        End If
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Dim MainMnu_code As String
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_TRANS_COLL_PDC_SCH
                    lblReportCaption.Text = "Transport Fee PDC Schedule"
                    chkSummary.Checked = True
                    h_bTransport.Value = 1
                Case OASISConstants.MNU_FEE_COLLECTION_DETAILS
                    lblReportCaption.Text = "Collection Summary Report"
                    chkSummary.Visible = False
                    trChkConciliation.Visible = False
                    h_bTransport.Value = 0
                    txtFeeCounter.Text = Session("sUsr_name")
                    hfFEECounter.Value = Session("sUsr_name")
                    CheckEmployee.Visible = True
                Case OASISConstants.MNU_FEE_NORM_COLL_PDC_SCH
                    lblReportCaption.Text = "Fee PDC Schedule"
                    chkSummary.Checked = True
                    trChkConciliation.Visible = False
                    trChkPDC.Visible = True
                    h_bTransport.Value = 0
                Case OASISConstants.MNU_FEE_NORM_DAILY_CHQ_COLLECTION
                    lblReportCaption.Text = "Daily Cheque Collection"
                    chkSummary.Visible = True
                    chkSummary.Text = "Deposit Slip"
                    chkPDCOnly.Visible = True
                    chkPDCOnly.Text = "Cheque Wise Report"
                    trChkConciliation.Visible = True
                    h_bTransport.Value = 0
                Case OASISConstants.MNU_TRAN_CHEQUSTATUS
                    lblReportCaption.Text = "Cheque Status"
                    chkSummary.Text = "Pdc"
                    TrUsrName.Visible = False
                    h_bTransport.Value = 0
                Case OASISConstants.MNU_TRAN_CHEQUHAND
                    lblReportCaption.Text = "Cheque In Hand"
                    chkSummary.Visible = False
                    TrUsrName.Visible = False
                    chkPDCOnly.Visible = True
                    h_bTransport.Value = 0
            End Select
            FillBSUNames(True)
            txtFeeCounter.Attributes.Add("ReadOnly", "ReadOnly")
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If bNoData Then
                txtFromDate.Text = Session("RPTFromDate")
            Else
                txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            End If

        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If

        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
        If IsPostBack Then
            FillBSUNames()
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select isnull(BSU_bFeesMulticurrency,0) from BUSINESSUNIT_M where BSU_ID='" & Session("sBSUID") & "'"
        Dim MultiCurrency As Boolean = Convert.ToBoolean(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
        If MultiCurrency = True Then
            Me.chkFC.Visible = True
        Else
            Me.chkFC.Visible = False
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Session("RPTFromDate") = txtFromDate.Text
        If UtilityObj.IsFutureDate(txtFromDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        UsrBSUnits1.MenuCode = MainMnu_code
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_COLLECTION_DETAILS
                GenerateFeeCollectionSummary()
            Case OASISConstants.MNU_FEE_TRANS_COLL_PDC_SCH
                GenerateTransportFeeColl_CHQ_Summary()
            Case OASISConstants.MNU_FEE_NORM_COLL_PDC_SCH
                GenerateFeeColl_PDC_Schedule()
            Case OASISConstants.MNU_FEE_NORM_DAILY_CHQ_COLLECTION
                GenerateChequeCollectionDetails()
            Case OASISConstants.MNU_TRAN_CHEQUSTATUS
                FeeChequDetails(False)
            Case OASISConstants.MNU_TRAN_CHEQUHAND
                FeeChequDetails(True)
        End Select

    End Sub


    Private Sub FeeChequDetails(ByVal ChqHand As Boolean)
        Dim cmd As New SqlCommand("[FEES].[Cheque_Status]")
        cmd.CommandType = CommandType.StoredProcedure
        Dim IDs As String = h_BSUID.Value.Replace("||", "|")
        Dim sqlpPDC As New SqlParameter("@PDC", SqlDbType.Int)
        Dim RptName As String = "../../FEES/REPORTS/RPT/TRANSFEE_ChqStatusCollection.rpt"
        Dim HeadrName As String = "Transport Cheque Status Dated :" & txtFromDate.Text
        sqlpPDC.Value = 0

        If chkSummary.Checked Then
            sqlpPDC.Value = 1
            RptName = "../../FEES/REPORTS/RPT/TRANSFEE_ChqStatus.rpt"
        End If
        If ChqHand = True Then
            RptName = "../../FEES/REPORTS/RPT/TRANSFEE_ChqStatusPdc.rpt"
            HeadrName = "Transport Cheque In Hand Dated :" & txtFromDate.Text
            sqlpPDC.Value = 2
        End If

        cmd.Parameters.Add(sqlpPDC)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@STU_BSU_IDS", SqlDbType.VarChar)
        sqlpFOR_BSU_IDs.Value = h_BSUID.Value.Replace("||", "|")
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@Dt", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpbPDCOnly As New SqlParameter("@bPDCOnly", SqlDbType.Bit)
        sqlpbPDCOnly.Value = chkPDCOnly.Checked
        cmd.Parameters.Add(sqlpbPDCOnly)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("ReportHead") = HeadrName
        repSource.Parameter = params
        'repSource.SubReport = repSourceSubRep
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = RptName

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateChequeCollectionDetails()
        Dim cmd As New SqlCommand("[FEES].[F_RPT_NormalFeeCollectionData]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@FOR_BSU_IDs", SqlDbType.Xml)
        sqlpFOR_BSU_IDs.Value = UtilityObj.GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpbGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpbGroupCurrency.Value = False
        cmd.Parameters.Add(sqlpbGroupCurrency)

        Dim sqlpPdcExclude As New SqlParameter("@PdcExclude", SqlDbType.Bit)
        sqlpPdcExclude.Value = True
        cmd.Parameters.Add(sqlpPdcExclude)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Dim sqlpbLvl As New SqlParameter("@Lvl", SqlDbType.TinyInt)
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_NORM_DAILY_CHQ_COLLECTION
                If chkPDCOnly.Checked Then ' Chequewise
                    params("RPT_CAPTION") = "Daily Cheque Collection Details (By Cheque)"
                    sqlpbLvl.Value = 7
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_Daily_ChqCollDetails_byChq.rpt"
                Else
                    params("RPT_CAPTION") = "Daily Cheque Collection Details"
                    sqlpbLvl.Value = 6
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_Daily_ChqCollDetails.rpt"
                End If
                If chkSummary.Checked Then
                    params("RPT_CAPTION") = "Cheque Deposit Slip"
                    sqlpbLvl.Value = 8
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_Daily_ChqCollDetails_b4dayend.rpt"
                End If
        End Select
        cmd.Parameters.Add(sqlpbLvl)
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Public Sub GetBSU_RoundOff(ByVal BSU_ID As String)
        'Author(--Swapna)
        'Date   --18/Aug/2011
        'Purpose--Get selected BSU's round off
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim query As String = ""

        query = "SELECT  BSU_ROUNDOFF FROM BUSINESSUNIT_M WHERE BSU_ID = '" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(query, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

        While reader.Read
            ViewState("RoundOffVal") = reader.Item("BSU_ROUNDOFF")
        End While

    End Sub
    Private Sub GenerateFeeCollectionSummary()
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand
        If Me.chkFC.Checked = True Then
            cmd.CommandText = "[FEES].[F_GET_FEECOLLECTION_FC]"
        Else
            cmd.CommandText = "[FEES].[F_GET_FEECOLLECTION]"
        End If

        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        sqlpBSU_ID.Value = UtilityObj.GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        If CheckEmployee.Checked Then
            sqlpUserName.Value = ""
        End If
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpDT As New SqlParameter("@DT", SqlDbType.DateTime)
        sqlpDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpDT)

        Dim repSourceSubRep(1) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        Dim cmdSubOtherTypes As SqlCommand = cmd.Clone()
        If Me.chkFC.Checked = True Then
            cmdSubOtherTypes.CommandText = "[FEES].[F_GET_FEECOLL_TOTAL_FC]"
        Else
            cmdSubOtherTypes.CommandText = "[FEES].[F_GET_FEECOLL_TOTAL]"
        End If
        '"Select * from  FEES.vw_OSO_FEE_RECEIPTOTHERS "
        cmdSubOtherTypes.Connection = New SqlConnection(str_conn)
        repSourceSubRep(0).Command = cmdSubOtherTypes

        'Select * from  FEES.vw_OSO_FEE_RECEIPTOTHERS
        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("DATE") = txtFromDate.Text

        Dim RoundOffVal As Integer = Session("BSU_ROUNDOFF")
        Dim separator As String() = New String() {"||"}
        Dim myValarr() As String = h_BSUID.Value.Split(separator, StringSplitOptions.RemoveEmptyEntries)
        If myValarr.Length > 1 Then
            'If sqlpBSU_ID.Value <> "" Then
            RoundOffVal = 2
        Else

            GetBSU_RoundOff(myValarr(0))
            RoundOffVal = ViewState("RoundOffVal")

        End If
        params("RoundOffVal") = RoundOffVal
        'params("FromDT") = txtFromDate.Text
        'params("ToDT") = txtToDate.Text
        repSource.Parameter = params
        repSource.SubReport = repSourceSubRep
        'repSource.IncludeBSUImage = True
        repSource.Command = cmd
        If Me.chkFC.Checked = True Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEReceiptSummarywithFC.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEReceiptSummary.rpt"
        End If

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeColl_PDC_Schedule()
        Dim cmd As New SqlCommand("[FEES].[RptGetFEEPDCSchedule]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@FOR_BSU_IDs", SqlDbType.VarChar)
        sqlpFOR_BSU_IDs.Value = h_BSUID.Value.Replace("||", "|")
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@Dt", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)
        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("DATE") = txtFromDate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        repSource.IncludeBSUImage = True

        Dim sqlpbGroupby As New SqlParameter("@Groupby", SqlDbType.TinyInt)
        If rbSummary.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_PDC_ScheduleSummary.rpt"
            params("RPT_CAPTION") = "PDC Schedule Report (Summary)"
            sqlpbGroupby.Value = 1
        ElseIf rbBank.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_PDC_Schedule.rpt"
            params("RPT_CAPTION") = "PDC Schedule Report"
            sqlpbGroupby.Value = 2
        ElseIf rbDetailed.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_ChqPaidDetails_pdc.rpt"
            params("RPT_CAPTION") = "PDC Schedule Report (Detailed)"
            sqlpbGroupby.Value = 3
        End If
        cmd.Parameters.Add(sqlpbGroupby)
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateTransportFeeColl_CHQ_Summary()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim cmd As New SqlCommand("[FEES].[RptGetPDCSchedule]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@FOR_BSU_IDs", SqlDbType.Xml)
        sqlpFOR_BSU_IDs.Value = UtilityObj.GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@Dt", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpbGroupCurrency As New SqlParameter("@bDetail", SqlDbType.Bit)
        sqlpbGroupCurrency.Value = chkSummary.Checked
        cmd.Parameters.Add(sqlpbGroupCurrency)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("DATE") = txtFromDate.Text
        params("RPT_CAPTION") = "PDC Schedule Report"
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If chkSummary.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEChqColl_ScheduleSummary.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEChqColl_Schedule.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function FillBSUNamesTransport(ByVal BSUIDs As String, Optional ByVal bgetAll As Boolean = False) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim conn As New SqlConnection(str_conn)
        Dim ds As New DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        If bgetAll Then
            condition = ""
        Else
            condition = " SVB_BSU_ID in(" & condition & ")"
        End If
        Try
            Dim cmd As New SqlCommand
            cmd.CommandText = "[GETUNITSFORTRANSPORTFEES]"
            cmd.Parameters.AddWithValue("@Usr_Name", Session("sUsr_Name"))
            cmd.Parameters.AddWithValue("@PROVIDER_BSU_ID", Session("sBsuid"))
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = cmd
            adpt.Fill(ds)
            If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
                Return False
            End If
            Dim dv As New DataView(ds.Tables(0))
            dv.RowFilter = condition
            If bgetAll Then
                h_BSUID.Value = ""
                For i As Integer = 0 To dv.Table.Rows.Count - 1
                    h_BSUID.Value += dv.Item(i)("SVB_BSU_ID") & "||"
                Next
            End If
            Return True
        Catch
            Return False
        Finally
            conn.Close()
        End Try

    End Function

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function FillBSUNames(Optional ByVal bGetAll As Boolean = False) As Boolean
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_TRANS_COLL_PDC_SCH
                Return FillBSUNamesTransport(h_BSUID.Value, bGetAll)
            Case OASISConstants.MNU_FEE_NORM_COLL_PDC_SCH, _
             OASISConstants.MNU_FEE_COLLECTION_DETAILS, _
             OASISConstants.MNU_FEE_NORM_DAILY_CHQ_COLLECTION
                If bGetAll Then
                    h_BSUID.Value = Session("sBSUID")
                End If
                Return FillBSUNames(h_BSUID.Value, True)
        End Select
    End Function

    Private Function FillBSUNames(ByVal BSUIDs As String, ByVal bGetMulti As Boolean) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        If bGetMulti Then
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
        Else
            condition = "'" & BSUIDs & "'"
        End If
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID SVB_BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID SVB_BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

