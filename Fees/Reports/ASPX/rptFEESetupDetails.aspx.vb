Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptFEESetupDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_REP_NORM_FEESETUP
                    lblReportCaption.Text = "Fee Setup Details"
                Case "F725126"
                    lblReportCaption.Text = "Student List"
                    trAcademic.Visible = False
                    trStudent.Visible = True
            End Select
            BindBusinessUnit()
            FillACD()
            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
         Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_NORM_FEESETUP
                GenerateFeeSetupDetails()
            Case "F725126"
                GetStudentMaster()
        End Select
    End Sub

    Private Sub GenerateFeeSetupDetails()
        Dim cmd As New SqlCommand("[FEES].[F_GETFEESETUPREPORT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpACD_ID)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        'params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        params("RPT_CAPTION") = " FEE SETUP DETAILS"
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEESETUP.rpt"

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GetStudentMaster()
        Dim cmd As New SqlCommand("[FEES].[GetStudentMaster]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@isSTUDENT", SqlDbType.Bit)
        sqlpACD_ID.Value = radStud.checked
        cmd.Parameters.Add(sqlpACD_ID)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If radStud.Checked Then
            params("RPT_CAPTION") = "Student List"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEStudentMaster_Student.rpt"
        Else
            params("RPT_CAPTION") = "Enquiry List"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEStudentMaster_Enquiry.rpt"
        End If


        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub BindBusinessUnit()
        ddlBSUnit.DataSource = FeeCommon.GETBSUFORUSER(Session("sUsr_name"))
        ddlBSUnit.DataTextField = "bsu_name"
        ddlBSUnit.DataValueField = "bsu_id"
        ddlBSUnit.DataBind()
        ddlBSUnit.Items.FindByValue(Session("sBsuid")).Selected = True
    End Sub

    Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSUnit.SelectedIndexChanged
        FillACD()
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBSUnit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        ' SetAcademicYearDate()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
  End Class
