<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ConsolidateSummary.aspx.vb"
    Inherits="Fees_Reports_ASPX_ConsolidateSummary" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %> 
<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError"></asp:Label>--%>
                            <asp:ValidationSummary
                                ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="5" cellspacing="0" width="100%" >
                    <%--    <tr class ="subheader_img">
            <td align="left" colspan="4" style="height: 19px">
                <asp:Label ID="lblReportCaption" runat="server"></asp:Label></td>
        </tr>--%>
                    <tr runat="server" id="TrBsu">
                        <td id="Td1" runat="server" align="left" width="20%" ><span class="field-label">Business Unit</span></td>
                        <td id="Td3" runat="server" align="left" colspan="2">
                            <div class="checkbox-list-full-height">
                            <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" /></div>
                        </td>
                        
                    </tr>
                    <tr runat="server" id="TrBssc">
                        <td id="Td11" runat="server" align="left" ><span class="field-label">Service Category</span></td>
                        <td id="Td33" runat="server" align="left" >
                            <div style="border: 1px outset white; text-align: left">
                                <asp:UpdatePanel ID="up2" runat="server">
                                    <ContentTemplate>
                                        <asp:TreeView ID="trvServices" runat="server"
                                            onclick="client_OnTreeNodeChecked();" ShowCheckBoxes="all">
                                        </asp:TreeView>
                                        <div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="trAcademicYear" runat="server" visible="false">
                        <td align="left"  width="20%" runat="server"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%" runat="server">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2"></td>
                    </tr>

                    <tr id="trSummary" runat="server">
                        <td align="left"  runat="server" width="20%" id="TdSummary1"></td>
                        <td align="left"  runat="server" width="30%" id="TdSummary2">
                            <asp:CheckBox ID="chkDetailed" runat="server" Text="Show Summary" />
                        </td>
                        <td colspan="2"></td>
                    </tr>

                    <tr id="trCharges" runat="server">
                        <td align="left"  runat="server" width="20%" id="TdCharge1"></td>
                        <td align="left" runat="server"  width="30%" id="TdCharge2">
                            <asp:CheckBox ID="chkCharge" runat="server" Text="View ChargeVsCollection" />
                        </td>
                        <td colspan="2"></td>
                    </tr>

                    <tr id="trStuStatus" runat="server" visible="false">
                        <td align="left" width="20%" ><span class="field-label">Student Status</span></td>
                        <td align="left" width="30%" >
                            <asp:RadioButtonList ID="rblStuStatus" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="AC">Active</asp:ListItem>
                                <asp:ListItem Value="IA">In-Active</asp:ListItem>
                                <asp:ListItem Selected="True" Value="BO">Both</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td colspan="2"></td>
                    </tr>

                    <tr>
                        <td align="left"  width="20%" runat="server" id="TdFdate1"><span class="field-label">From Date</span></td>
                        <td align="left"  width="30%" runat="server" id="TdFdate3">
                            <asp:TextBox ID="txtFromDate" runat="server" ></asp:TextBox>
                              <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                             ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                            ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                            ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%" >
                            <asp:Label ID="labTodate" runat="server" Text="To Date" CssClass="field-label"></asp:Label></td>

                        <td align="left" width="30%"  >
                            <asp:TextBox ID="txtToDate" runat="server" ></asp:TextBox><asp:ImageButton
                                ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" /><asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                    runat="server" ControlToValidate="txtToDate" ErrorMessage="To Date required"
                                    ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="revToDate" runat="server" ControlToValidate="txtToDate" Display="Dynamic"
                                        EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>


