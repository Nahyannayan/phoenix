Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Fees_Reports_ASPX_rptFeeCollectionReports
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_REP_NORM_HEADVSCOLLECTION
                    lblReportCaption.Text = "Fee Head vs. Collection"
            End Select
            SetAcademicYearDate() 
            txtFeeCounter.Attributes.Add("ReadOnly", "ReadOnly")
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim str_bsuids As String = UsrBSUnits1.GetSelectedNode("|")
        If str_bsuids = "" Then
            'lblError.Text = "***Please select atleast one business Unit***"
            usrMessageBar2.ShowNotification("***Please select atleast one business Unit***", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_NORM_HEADVSCOLLECTION
                RPTFEECOLLECTIONSUMMARY_FEEHEAD(str_bsuids)
        End Select
    End Sub

    Private Sub RPTFEECOLLECTIONSUMMARY_FEEHEAD(ByVal p_str_bsuids As String)
        Dim cmd As New SqlCommand("[FEES].[RPTFEECOLLECTIONSUMMARY_FEEHEAD]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSUIDs", SqlDbType.VarChar, 400)
        sqlpBSU_ID.Value = p_str_bsuids
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
       
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionSummaryHeadWise.rpt"
         
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, Session("Current_ACD_ID"), Session("sBsuid"))
        txtFromDate.Text = DTFROM
        txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat) ' DTTO
        If txtFromDate.Text = "" Then
            txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

