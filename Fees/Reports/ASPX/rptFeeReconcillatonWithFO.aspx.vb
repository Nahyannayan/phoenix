Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_rptFeeReconcillatonWithFO
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("MainMnu_code") = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_REP_NORM_FEERECONCILIATION_WITHFO
                    lblReportCaption.Text = "Fee Reconciliation (With FO)"
                Case "F725101"
                    lblReportCaption.Text = "Fee Reconciliation (With FO) Detailed"
            End Select
            BindBusinessUnit()
            BindAcademicYear(ddlBSUnit.SelectedValue)
            Page.Title = OASISConstants.Gemstitle
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""

        End If 
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If IsDate(txtFromDate.Text) And IsDate(txtToDate.Text) Then
            Dim Str_Error As String = ""
            If CDate(txtFromDate.Text) < CDate(ViewState("DTFROM")) Then
                Str_Error = "From cannot be less than academic year start date<br />"
            ElseIf CDate(txtFromDate.Text) > CDate(ViewState("DTTO")) Then
                Str_Error = Str_Error & "To cannot be greater than academic year end date<br />"
            End If
            If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
                Str_Error = Str_Error & "Invalid Date(Future) !!!"
            End If
            If Str_Error <> "" Then
                'lblError.Text = Str_Error
                usrMessageBar2.ShowNotification(Str_Error, UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
        Else
            'lblError.Text = "Invalid Date"
            usrMessageBar2.ShowNotification("Invalid Date", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        'MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTFromDate") = txtFromDate.Text
        Session("RPTToDate") = txtToDate.Text
        Select Case ViewState("MainMnu_code")
            Case OASISConstants.MNU_FEE_REP_NORM_FEERECONCILIATION_WITHFO
                FeeChargeReconcillation()
            Case "F725101"
                FeeChargeReconcillationDetailed()
        End Select
    End Sub

    Private Sub FeeChargeReconcillation()
        '        ALTER PROCEDURE [FEES].[rptFeeChargeReconcillation 
        '@FromDt datetime='1-SEP-2008',
        '@ToDt datetime='1-JUN-2009',
        '@BSU_ID VARCHAR(20)='125016',
        '@ACD_ID INT =80
        Dim cmd As New SqlCommand("[FEES].[rptFeeChargeReconcillation]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpACD_ID)  

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        'params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True 
        repSource.HeaderBSUID = ddlBSUnit.SelectedItem.Value
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeReconcillationWithFO.rpt"

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub FeeChargeReconcillationDetailed()
        '        ALTER PROCEDURE [FEES].[rptFeeChargeReconcillation 
        '@FromDt datetime='1-SEP-2008',
        '@ToDt datetime='1-JUN-2009',
        '@BSU_ID VARCHAR(20)='125016',
        '@ACD_ID INT =80
        Dim cmd As New SqlCommand("[FEES].[rptFeeChargeReconcillation_Detailed]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        'params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = ddlBSUnit.SelectedItem.Value
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeReconcillationWithFO_Detailed.rpt"

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
 
    Private Sub BindBusinessUnit()
        ddlBSUnit.DataSource = FeeCommon.GETBSUFORUSER(Session("sUsr_name"))
        ddlBSUnit.DataTextField = "bsu_name"
        ddlBSUnit.DataValueField = "bsu_id"
        ddlBSUnit.DataBind()
        ddlBSUnit.Items.FindByValue(Session("sBsuid")).Selected = True
    End Sub

    Private Sub BindAcademicYear(ByVal BSU_ID As Integer)  
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBSUnit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        SetAcademicYearDate()
    End Sub

    Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSUnit.SelectedIndexChanged
        BindAcademicYear(ddlBSUnit.SelectedValue)
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        setAcademicyearDate()
    End Sub

    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, ddlAcademicYear.SelectedItem.Value, ddlBSUnit.SelectedValue)
        ViewState("DTFROM") = DTFROM
        ViewState("DTTO") = DTTO
        Select Case ViewState("MainMnu_code")
            Case "F725101"
                Dim startDate As New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
                Dim endDate As DateTime = startDate.AddMonths(1).AddDays(-1)
                If (startDate >= CDate(DTFROM) And startDate <= CDate(DTTO)) AndAlso (endDate <= CDate(DTTO) And endDate >= CDate(DTFROM)) Then
                    DTFROM = startDate.ToString(OASISConstants.DataBaseDateFormat)
                    DTTO = endDate.ToString(OASISConstants.DataBaseDateFormat)
                    If UtilityObj.IsFutureDate(DTFROM, DTTO) Then
                        DTTO = DateTime.Now.ToString(OASISConstants.DataBaseDateFormat)
                    End If
                Else
                    GetStartandEndDates(CDate(DTTO).Month, CDate(DTTO).Year, startDate, endDate)
                    DTFROM = startDate.ToString(OASISConstants.DataBaseDateFormat)
                    DTTO = endDate.ToString(OASISConstants.DataBaseDateFormat)
                End If
                
        End Select
        txtFromDate.Text = DTFROM
        'DTTO = Format(Now.Date, OASISConstants.DateFormat)
        txtToDate.Text = DTTO
        
    End Sub

    Sub GetStartandEndDates(ByVal Month As Int16, ByVal Year As Int32, ByRef StartDate As DateTime, ByRef EndDate As DateTime)
        Dim startDt As New DateTime(Year, Month, 1)
        Dim endDt As DateTime = startDt.AddMonths(1).AddDays(-1)
        StartDate = startDt
        EndDate = endDt
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
