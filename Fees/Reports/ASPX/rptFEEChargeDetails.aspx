<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptFEEChargeDetails.aspx.vb" Inherits="rptFEEChargeDetails" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function GetStudent() {
            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 875px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var STUD_TYP = document.getElementById('<%=radEnquiry.ClientID %>').checked;
            var url;
            if (STUD_TYP == true) {
                url = "../../ShowStudentMulti.aspx?TYPE=ENQUIRY&MULTI_SEL=true&bsu=" + document.getElementById('<%= ddlBSUnit.ClientID %>').value;
                //result = window.showModalDialog(url, "", sFeatures);
                var oWnd = radopen(url, "pop_enq");
            }
            else {
                url = "../../ShowStudentMulti.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu=" + document.getElementById('<%= ddlBSUnit.ClientID %>').value;
                //result = window.showModalDialog(url, "", sFeatures);
                var oWnd = radopen(url, "pop_stu");
            }
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=txtStudName.ClientID %>').value = 'Multiple Students selected';
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = result;
            }
            return true;--%>
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


        function OnClientEnqClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);       
                document.getElementById('<%=txtStudName.ClientID %>').value = 'Multiple Students selected';
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode;
                __doPostBack('<%=h_STUD_ID.ClientID%>', "");
            }
        }
        function OnClientStuClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode;
                //alert(NameandCode[0]);
                document.getElementById('<%=txtStudName.ClientID %>').value = 'Multiple Students selected';
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode;
                __doPostBack('<%=h_STUD_ID.ClientID%>', "");

            }
        }
    </script>


     <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_enq" runat="server" Behaviors="Close,Move" OnClientClose="OnClientEnqClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_stu" runat="server" Behaviors="Close,Move" OnClientClose="OnClientStuClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

      
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlBSUnit" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Grade</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">From Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                        runat="server" ControlToValidate="txtFromDate" ErrorMessage="From Date required"
                        ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                            ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                            ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%"><span class="field-label">To Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" /><asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                    runat="server" ControlToValidate="txtToDate" ErrorMessage="To Date required"
                                    ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="revToDate" runat="server" ControlToValidate="txtToDate" Display="Dynamic"
                                        EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                             <asp:CheckBox ID="chkStudentby" runat="server" AutoPostBack="True" Text="By Student" CssClass="field-label" />
                        </td>
                        <td align="left" width="30%">                           
                             <asp:RadioButton ID="radStudent" runat="server" Checked="True" GroupName="STUD_ENQ" Text="Student" AutoPostBack="True" OnCheckedChanged="radStudent_CheckedChanged"  CssClass="field-label">
                             </asp:RadioButton>
                            <asp:RadioButton ID="radEnquiry" runat="server" GroupName="STUD_ENQ" Text="Enquiry" AutoPostBack="True" OnCheckedChanged="radEnquiry_CheckedChanged"  CssClass="field-label">
                            </asp:RadioButton>
                            <asp:CheckBox ID="ChkSummary" runat="server" Text="Summary" Checked="True" CssClass="field-label" /> 
                        </td>

                        <td align="left" colspan="2">                           
                            <asp:TextBox ID="txtStudName" runat="server" Width="61%" AutoPostBack="True" OnTextChanged="txtStudName_TextChanged"></asp:TextBox>
                            <asp:LinkButton ID="lblAddNewStudent" runat="server" CausesValidation="False" OnClick="lblAddNewStudent_Click">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetStudent(); return false;" /><br />
                            <br />
                            <asp:GridView ID="gvStudentDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Student ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudID" runat="server" Text='<%# bind("STU_NO") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudentName" runat="server" Text='<%# bind("STU_NAME") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="Delete">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server">Delete</asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

