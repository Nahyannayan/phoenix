Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_rptFeeAdvancesSummary
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Dim MainMnu_code As String
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If

            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_REP_NORM_MONTHLY_ADVANCESUMMARY
                    lblReportCaption.Text = "Fee Monthly Advance"
                    TrYear.Visible = False
            End Select
            ViewState("FEE_IDs") = ""
            BindBusinessUnit()
            FillACD()
            BindGrade()
            BindFeeTypes()
            txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            Page.Title = OASISConstants.Gemstitle
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If UtilityObj.IsFutureDate(txtFromDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_NORM_MONTHLY_ADVANCESUMMARY
                If rblFilterBy.SelectedValue = "3" Then 'Feewise
                    GenerateFeeAdvanceDetail()
                Else
                    GenerateFeeOutStandingDetails()
                End If
        End Select
    End Sub
    Public Sub GetBSU_RoundOff(ByVal BSU_ID As String)
        'Author(--Swapna)
        'Date   --18/Aug/2011
        'Purpose--Get selected BSU's round off
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim query As String = ""

        query = "SELECT  BSU_ROUNDOFF FROM BUSINESSUNIT_M WHERE BSU_ID= '" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(query, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

        While reader.Read
            ViewState("RoundOffVal") = reader.Item("BSU_ROUNDOFF")
        End While

    End Sub

    Private Sub GenerateFeeOutStandingDetails()
        Dim RepType As Integer
        Dim StuType As String = "A"
        UpdateFeeSelected()
        If radEnq.Checked Then
            StuType = "E"
        ElseIf radStud.Checked Then
            StuType = "S"
        ElseIf RadInactiv.Checked Then
            StuType = "I"
        End If
        Dim cmd As New SqlCommand("FEES.F_FEEADVANCESTUDENTWISE")
        cmd.CommandType = CommandType.StoredProcedure

        RepType = rblFilterBy.SelectedValue

        Dim sqlpFRMDT As New SqlParameter("@Dt", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSTU_BSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpSTU_BSU_ID)

        Dim sqlpType As New SqlParameter("@Typ", SqlDbType.SmallInt)
        sqlpType.Value = RepType
        cmd.Parameters.Add(sqlpType)

        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 20)
        sqlpSTU_TYPE.Value = StuType
        cmd.Parameters.Add(sqlpSTU_TYPE)

        Dim sqlGRMID As New SqlParameter("@STU_GRD_ID", SqlDbType.VarChar)
        sqlGRMID.Value = ddlGrade.SelectedItem.Text.ToString()
        cmd.Parameters.Add(sqlGRMID)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        GetBSU_RoundOff(ddlBSUnit.SelectedValue)
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("FromDATE") = txtFromDate.Text
        params("RoundOffVal") = ViewState("RoundOffVal")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = ddlBSUnit.SelectedItem.Value
        If RepType = 0 Or RepType = 2 Then
            params("RPT_CAPTION") = "Fee Advance Details"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rpfFeeAdvanceSummary_New.rpt"
        ElseIf RepType = 1 Then
            params("RPT_CAPTION") = "Fee Advance Grade wise Summary"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rpfFeeAdvanceSummary_Gradewise.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/RptviewerNew.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeAdvanceDetail()
        Dim RepType As Integer
        Dim StuType As String = "A"
        UpdateFeeSelected()
        If radEnq.Checked Then
            StuType = "E"
        ElseIf radStud.Checked Then
            StuType = "S"
        ElseIf RadInactiv.Checked Then
            StuType = "I"
        End If
        Dim cmd As New SqlCommand("FEES.GET_FEEADVANCE_FEEWISE")
        cmd.CommandType = CommandType.StoredProcedure

        RepType = rblFilterBy.SelectedValue

        Dim sqlpFRMDT As New SqlParameter("@Dt", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSTU_BSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpSTU_BSU_ID)

        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 20)
        sqlpSTU_TYPE.Value = StuType
        cmd.Parameters.Add(sqlpSTU_TYPE)

        Dim sqlGRMID As New SqlParameter("@STU_GRD_ID", SqlDbType.VarChar)
        sqlGRMID.Value = ddlGrade.SelectedItem.Text.ToString()
        cmd.Parameters.Add(sqlGRMID)

        Dim sqlFEEID As New SqlParameter("@FEE_IDs", SqlDbType.VarChar)
        sqlFEEID.Value = ViewState("FEE_IDs")
        cmd.Parameters.Add(sqlFEEID)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        GetBSU_RoundOff(ddlBSUnit.SelectedValue)
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("FromDATE") = txtFromDate.Text
        params("RoundOffVal") = ViewState("RoundOffVal")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = ddlBSUnit.SelectedItem.Value

        If RepType = 3 Then
            params("RPT_CAPTION") = "Fee Advance Fee wise Summary"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rpfFeeAdvanceSummary_Feewise.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub BindGrade()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim sql_query As String = "SELECT DISTINCT GRM_DISPLAY, GRM_GRD_ID FROM dbo.GRADE_BSU_M WITH(NOLOCK) WHERE GRM_ACD_ID = " & _
        ddlAcademicYear.SelectedValue & " AND GRM_BSU_ID ='" & ddlBSUnit.SelectedValue & "' "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, sql_query)
        If (dsData IsNot Nothing) AndAlso (dsData.Tables(0).Rows.Count > 0) Then
            Dim dr As DataRow = dsData.Tables(0).NewRow
            dr(0) = "ALL"
            dr(1) = "-1"
            dsData.Tables(0).Rows.Add(dr)
        End If
        ddlGrade.DataSource = dsData
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
        If ddlGrade.Items.Count > 0 Then
            ddlGrade.Items.FindByValue("-1").Selected = True
        End If
    End Sub

    Private Sub BindBusinessUnit()
        ddlBSUnit.DataSource = FeeCommon.GETBSUFORUSER(Session("sUsr_name"))
        ddlBSUnit.DataTextField = "bsu_name"
        ddlBSUnit.DataValueField = "bsu_id"
        ddlBSUnit.DataBind()
        ddlBSUnit.Items.FindByValue(Session("sBsuid")).Selected = True
    End Sub

    Private Sub BindFeeTypes()
        trvFeeType.Nodes.Clear()
        PopulateRootLevel()
        trvFeeType.DataBind()
        trvFeeType.CollapseAll()
    End Sub
    Private Sub PopulateRootLevel()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
        Dim str_Sql As String
        trvFeeType.Nodes.Clear()
        str_Sql = "select 0 as FEE_ID,'All' as FEE_DESCRIPTION,count(*) childnodecount from  (SELECT  DISTINCT FEE_ID , FEE_DESCR  AS FEE_DESCRIPTION  FROM FEES.FEES_M WITH(NOLOCK)  )a"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), trvFeeType.Nodes)
    End Sub
    Private Sub PopulateNodes(ByVal dt As DataTable, _
    ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("FEE_DESCRIPTION").ToString()
            tn.Value = dr("FEE_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub
    Private Sub PopulateTree() 'Generate Tree
        PopulateRootLevel()
        trvFeeType.DataBind()
        trvFeeType.CollapseAll()
    End Sub
    Private Sub PopulateSubLevel(ByVal parentid As String, _
    ByVal parentNode As TreeNode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
        Dim ds As New DataSet
        Dim Parms(1) As SqlClient.SqlParameter
        Parms(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[FEES].[GET_FEETYPE_WITH_CHECKALL]", Parms)

        PopulateNodes(ds.Tables(0), parentNode.ChildNodes)
    End Sub
    Protected Sub trvFeeType_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles trvFeeType.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateSubLevel(str, e.Node)
    End Sub

    Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSUnit.SelectedIndexChanged
        FillACD()
        BindGrade()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        SetAcademicYearDate()
    End Sub

    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, ddlAcademicYear.SelectedItem.Value, ddlBSUnit.SelectedValue)
        'txtFromDate.Text = DTFROM 
        ViewState("DTFROM") = DTFROM
        ViewState("DTTO") = DTTO
    End Sub

    Private Sub UpdateFeeSelected()
        Try
            Dim FEE_IDs As String = ""
            For Each node As TreeNode In trvFeeType.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If FEE_IDs.Trim() = "" Then
                    FEE_IDs = node.Value
                Else
                    FEE_IDs = FEE_IDs + "|" + node.Value
                End If
            Next
            ViewState("FEE_IDs") = FEE_IDs
        Catch
            ViewState("FEE_IDs") = ""
        End Try
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBSUnit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        SetAcademicYearDate()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
