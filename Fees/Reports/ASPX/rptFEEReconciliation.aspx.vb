Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptFEEReconciliation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If

            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_REP_NORM_FEERECONCILIATION
                    chkGradeSummary.Text = "Gradewise Summary"
                    lblReportCaption.Text = "Fee Reconciliation"
                Case OASISConstants.MNU_FEE_REP_NORM_FEEWISERECONCILIATION
                    chkGradeSummary.Text = "Gradewise Summary"
                    lblReportCaption.Text = "Feewise Reconciliation"
                Case "F726090"
                    chkGradeSummary.Text = "Gradewise Summary"
                    lblReportCaption.Text = "Feewise Reconciliation (With Non Revenue)"
            End Select

            BindBusinessUnit()
            'BindAcademicYear(ddlBSUnit.SelectedValue)
            FillACD()
            BindGrade()

            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTFromDate") = txtFromDate.Text
        Session("RPTToDate") = txtToDate.Text
        If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_NORM_FEERECONCILIATION
                If chkFeeHead.Checked Then
                    GenerateFEEReconciliationFEEHead()
                Else
                    GenerateFEEReconciliation()
                End If
            Case OASISConstants.MNU_FEE_REP_NORM_FEEWISERECONCILIATION
                GenerateFEEwiseReconciliation()
            Case "F726090"
                If chkFeeHead.Checked Then
                    GenerateFEEReconciliationFEEHeadWithNonRevenue()
                Else
                    GenerateFEEReconciliationWithNonRevenue()
                End If

        End Select
    End Sub
    Public Sub GetBSU_RoundOff(ByVal BSU_ID As String)
        'Author(--Swapna)
        'Date   --18/Aug/2011
        'Purpose--Get selected BSU's round off
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim query As String = ""

        query = "SELECT  BSU_ROUNDOFF FROM BUSINESSUNIT_M WHERE BSU_ID = '" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(query, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

        While reader.Read
            ViewState("RoundOffVal") = reader.Item("BSU_ROUNDOFF")
        End While

    End Sub
    Private Sub GenerateFEEwiseReconciliation()
        Dim StuTyp As String = "A"
        Dim Caption As String = "ALL"
        If radStud.Checked Then
            StuTyp = "S"
            Caption = "STUDENT"
        ElseIf radEnq.Checked Then
            StuTyp = "E"
            Caption = "ENQUIRY"
        End If
        Dim cmd As New SqlCommand("[FEES].[F_GETFEEWISERECONCILIATION_OASIS]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpbSTUDENT As New SqlParameter("@bSTUDENT", SqlDbType.VarChar)
        sqlpbSTUDENT.Value = StuTyp
        cmd.Parameters.Add(sqlpbSTUDENT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd

        repSource.IncludeBSUImage = True
        params("RPT_CAPTION") = " FEEWISE RECONCILIATION (" & Caption & ")"
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEERECONCILIATIONFEEwise.rpt"
        'repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEERECONCILIATIONFEEsummary.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFEEReconciliationFEEHead()
        Dim cmd As New SqlCommand("[FEES].[F_GETFEERECONCILIATIONFEEHEADREPORT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpSTU_TYP As New SqlParameter("@STU_TYP", SqlDbType.VarChar, 2)
        If radEnq.Checked Then
            sqlpSTU_TYP.Value = "E"
        ElseIf radStud.Checked Then
            sqlpSTU_TYP.Value = "S"
        End If
        cmd.Parameters.Add(sqlpSTU_TYP)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        GetBSU_RoundOff(ddlBSUnit.SelectedValue)

        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        params("RoundOffVal") = ViewState("RoundOffVal")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        params("RPT_CAPTION") = " FEE RECONCILIATION (WITH FEE HEAD)"
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEERECONCILIATIONFEEHEAD.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFEEReconciliation()
        Dim cmd As New SqlCommand("[FEES].[F_GETFEERECONCILIATIONSTUDENTREPORT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpGRD_ID As New SqlParameter("@GRD_ID", SqlDbType.VarChar, 10)
        sqlpGRD_ID.Value = ddlGrade.SelectedValue
        cmd.Parameters.Add(sqlpGRD_ID)

        Dim sqlpSTU_TYP As New SqlParameter("@STU_TYP", SqlDbType.VarChar, 2)
        If radEnq.Checked Then
            sqlpSTU_TYP.Value = "E"
        ElseIf radStud.Checked Then
            sqlpSTU_TYP.Value = "S"
        End If
        cmd.Parameters.Add(sqlpSTU_TYP)

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        GetBSU_RoundOff(ddlBSUnit.SelectedValue)
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        params("RoundOffVal") = ViewState("RoundOffVal")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        Dim monthDesc As String = CDate(txtFromDate.Text).ToString("MMMM").ToUpper
        params("RPT_CAPTION") = " FEE RECONCILIATION "
        If chkGradeSummary.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEERECONCILIATIONGRADEWISE.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEERECONCILIATIONSTUD.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFEEReconciliationWithNonRevenue()
        Dim cmd As New SqlCommand("[FEES].[F_GETFEERECONCILIATION_NONREV_REPORT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpGRD_ID As New SqlParameter("@GRD_ID", SqlDbType.VarChar, 10)
        sqlpGRD_ID.Value = ddlGrade.SelectedValue
        cmd.Parameters.Add(sqlpGRD_ID)

        Dim sqlpSTU_TYP As New SqlParameter("@STU_TYP", SqlDbType.VarChar, 2)
        If radEnq.Checked Then
            sqlpSTU_TYP.Value = "E"
        ElseIf radStud.Checked Then
            sqlpSTU_TYP.Value = "S"
        End If
        cmd.Parameters.Add(sqlpSTU_TYP)

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        GetBSU_RoundOff(ddlBSUnit.SelectedValue)
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        params("RoundOffVal") = ViewState("RoundOffVal")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        Dim monthDesc As String = CDate(txtFromDate.Text).ToString("MMMM").ToUpper
        params("RPT_CAPTION") = " FEE RECONCILIATION (With Non-Revenue)"
        If chkGradeSummary.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEERECONCILIATIONGRADEWISE_NonRev.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEERECONCILIATIONSTUD_NonRev.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Private Sub GenerateFEEReconciliationFEEHeadWithNonRevenue()
        Dim cmd As New SqlCommand("[FEES].[F_GETFEERECONCILIATION_FEEHEAD_NONREV_REPORT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        GetBSU_RoundOff(ddlBSUnit.SelectedValue)

        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        params("RoundOffVal") = ViewState("RoundOffVal")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        params("RPT_CAPTION") = " FEE RECONCILIATION FEE HEAD-WISE(WITH NON-REVENUE)"
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEERECONCILIATIONFEEHEAD_NonRev.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub BindGrade()
        Try
            Dim conn_str As String = ConnectionManger.GetOASISConnectionString
            Dim sql_query As String = "SELECT GRM_DISPLAY, GRM_GRD_ID FROM GRADE_BSU_M " & _
            " INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID WHERE GRM_ACD_ID = " & _
            ddlAcademicYear.SelectedValue & " AND GRM_BSU_ID ='" & ddlBSUnit.SelectedValue & "' "
            Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, sql_query)
            If (dsData IsNot Nothing) AndAlso (dsData.Tables(0).Rows.Count > 0) Then
                Dim dr As DataRow = dsData.Tables(0).NewRow
                dr(0) = "ALL"
                dr(1) = "-1"
                dsData.Tables(0).Rows.Add(dr)
            End If
            ddlGrade.DataSource = dsData
            ddlGrade.DataTextField = "GRM_DISPLAY"
            ddlGrade.DataValueField = "GRM_GRD_ID"
            ddlGrade.DataBind()
            If ddlGrade.Items.Count > 0 Then
                ddlGrade.Items.FindByValue("-1").Selected = True
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BindBusinessUnit()
        ddlBSUnit.DataSource = FeeCommon.GETBSUFORUSER(Session("sUsr_name"))
        ddlBSUnit.DataTextField = "bsu_name"
        ddlBSUnit.DataValueField = "bsu_id"
        ddlBSUnit.DataBind()
        ddlBSUnit.Items.FindByValue(Session("sBsuid")).Selected = True
    End Sub

    Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSUnit.SelectedIndexChanged
        'BindAcademicYear(ddlBSUnit.SelectedValue)
        FillACD()
        BindGrade()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        SetAcademicYearDate()
    End Sub

    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, ddlAcademicYear.SelectedItem.Value, ddlBSUnit.SelectedValue)
        txtFromDate.Text = DTFROM
        'txtToDate.Text = DTTO
        txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBSUnit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        SetAcademicYearDate()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
