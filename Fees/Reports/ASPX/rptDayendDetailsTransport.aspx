﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptDayendDetailsTransport.aspx.vb" Inherits="Fees_rptDayendDetailsTransport" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table cellspacing="0" width="100%">
                    <tr valign="bottom">
                        <td align="left" colspan="3" valign="bottom">
                              <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                            <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>--%>
                        </td>
                    </tr>
                </table>
                <table style="border-collapse: collapse" cellpadding="3" cellspacing="0" width="100%">
                    <%--  <tr>
            <td id="Td1" runat="server" colspan="2" class="subheader_img" align="left" 
                style="height: 19px">
                <asp:Label ID="lblHead" runat="server"></asp:Label></td>
        </tr>--%>
                    <tr >
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" OnClientClick="return getDate(2);"
                                TabIndex="4" />
                        </td>
                        <td  align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlBUnit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_PROVIDER_BSU_ID" 
                                TabIndex="5" Enabled="False">
                            </asp:DropDownList></td>
                    </tr>                   
                    <tr class="title-bg">
                        <td  align="left" colspan="4">Consolidated Dayend Status Details</td>
                    </tr>
                    <tr>
                        <td  align="center" colspan="4">
                            <asp:GridView ID="gvDayendDetails" runat="server" Width="100%" EmptyDataText="No Details Added"  CssClass="table table-bordered table-row" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="SLNO" HeaderText="S.No">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BSU_NAME" HeaderText="Business Unit"></asp:BoundField>
                                    <asp:BoundField DataField="FPD_DATE" HeaderText="Date"></asp:BoundField>
                                    <asp:BoundField DataField="FPD_LOGDATE" HeaderText="Log Date"></asp:BoundField>
                                    <asp:BoundField DataField="FPD_USER" HeaderText="User"></asp:BoundField>
                                    <asp:BoundField DataField="FPD_RETMESSAGE" HeaderText="Status"></asp:BoundField>
                                    <asp:BoundField DataField="FPD_PROCESSTIME" HeaderText="Process Time"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                   
                    <tr class="title-bg">
                        <td align="left" colspan="4">Not Done Details</td>
                    </tr>
                    <tr>
                        <td  align="center" colspan="4">
                            <asp:GridView ID="gvNextstatus" runat="server" Width="100%" EmptyDataText="No Details Added" CssClass="table table-bordered table-row" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="SLNO" HeaderText="S.No">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BSU_NAME" HeaderText="Business Unit"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetTransportProviders" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFrom" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>

</asp:Content>


