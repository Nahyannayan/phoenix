Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptTermDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Select Case MainMnu_code
                Case "F725121"
                    lblReportCaption.Text = "Term Setup Details"
            End Select
            BindBusinessUnit()
            FillACD()
            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "F725121"
                GET_TERM_SETUP()
        End Select
    End Sub

    Private Sub GET_TERM_SETUP()
        
        'Response.Redirect("../ASPX/rptFeeReportViewerMS.aspx?TYPE=" & Encr_decrData.Encrypt("TERM_DETAIL") & "&ACD_ID=" & Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue) & "&ACD_DESCR=" & Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) & "", True)

        GenerateReport()
    End Sub
    Sub GenerateReport()
        Dim RptTYPE As String = "TERM_DETAIL"
        'If Not Request.QueryString("TYPE").Replace(" ", "+") Is Nothing AndAlso Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+")) <> "" Then
        '    RptTYPE = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
        'End If
        Select Case RptTYPE
            Case "TERM_DETAIL"
                Dim ACD_ID As Integer = 0, ACD_DESCR As String = ""

                If Not ddlAcademicYear.SelectedValue Is Nothing AndAlso ddlAcademicYear.SelectedValue <> "" Then
                    ACD_ID = ddlAcademicYear.SelectedValue
                    If IsNumeric(ACD_ID) AndAlso ACD_ID <> 0 Then
                        Dim param As New Hashtable
                        If Not ddlAcademicYear.SelectedItem.Text Is Nothing AndAlso ddlAcademicYear.SelectedItem.Text <> "" Then
                            ACD_DESCR = ddlAcademicYear.SelectedItem.Text
                        End If
                        param.Add("@IMG_BSU_ID", ddlBSUnit.SelectedValue) 'Session("sbsuid")
                        param.Add("@IMG_TYPE", "LOGO")
                        param.Add("userName", Session("sUsr_name"))
                        param.Add("RPT_CAPTION", "Term Setup Details for the Academic Year " & ACD_DESCR)
                        param.Add("@ACD_ID", ACD_ID)

                        Dim rptClass As New rptClass

                        With rptClass
                            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName") 'ConnectionManger.GetOASISConnection.Database
                            .reportPath = Server.MapPath("../RPT/rptTermDetails.rpt")
                            .reportParameters = param
                        End With
                        Session("rptClass") = rptClass
                        ReportLoadSelection()
                    End If
                End If

        End Select


    End Sub

   
    Private Sub BindBusinessUnit()
        ddlBSUnit.DataSource = FeeCommon.GETBSUFORUSER(Session("sUsr_name"))
        ddlBSUnit.DataTextField = "bsu_name"
        ddlBSUnit.DataValueField = "bsu_id"
        ddlBSUnit.DataBind()
        ddlBSUnit.Items.FindByValue(Session("sBsuid")).Selected = True
    End Sub

    Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSUnit.SelectedIndexChanged
        FillACD()
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBSUnit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
