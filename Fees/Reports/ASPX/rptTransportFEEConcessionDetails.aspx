<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTransportFEEConcessionDetails.aspx.vb" Inherits="rptFEEConcessionDetails" Title="Untitled Page" %>
 <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../../../UserControls/usrTransportBSUnits.ascx" TagName="usrTransportBSUnits"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function ToggleSearchConcession() {
            if (document.getElementById('<%=hfConcession.ClientID %>').value == 'display') {
                document.getElementById('<%=trConcession.ClientID %>').style.display = '';
                document.getElementById('<%=trSelConcession.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusConcession.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusConcession.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfConcession.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trConcession.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelConcession.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusConcession.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusConcession.ClientID %>').style.display = '';
                document.getElementById('<%=hfConcession.ClientID %>').value = 'display';
            }
            return false;
        }

        function HideAll() {
            document.getElementById('<%=trConcession.ClientID %>').style.display = 'none';
        }
        function SearchHide() {
            document.getElementById('trConcession').style.display = 'none';
        }

        <%--function GetConcessionType() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("popup.aspx?ID=CONC", "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_ConcessionType.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }
        }

        function CheckOnPostback() {
            if (document.getElementById('<%=hfConcession.ClientID %>').value == 'none') {
                document.getElementById('<%=trConcession.ClientID %>').style.display = '';
                document.getElementById('<%=trSelConcession.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusConcession.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusConcession.ClientID %>').style.display = 'none';
            }
            return false;
        }--%>

    </script>
     <script>
         function GetConcessionType() {
             var oWnd = radopen("popup.aspx?ID=CONC", "pop_GetConcessionType");

         }



 function OnClientClose1(oWnd, args) {

     //get the transferred arguments
     var arg = args.get_argument();
     if (arg) {

         NameandCode = arg.NameandCode.split('||');

         document.getElementById('<%=h_ConcessionType.ClientID %>').value = arg.NameandCode;
         document.getElementById('<%=txtConcession.ClientID%>').value = arg.NameandCode;
           __doPostBack('<%= txtConcession.ClientID%>', 'TextChanged');
            }
        }

         function autoSizeWithCalendar(oWindow) {
             var iframe = oWindow.get_contentFrame();
             var body = iframe.contentWindow.document.body;

             var height = body.scrollHeight;
             var width = body.scrollWidth;

             var iframeBounds = $telerik.getBounds(iframe);
             var heightDelta = height - iframeBounds.height;
             var widthDelta = width - iframeBounds.width;

             if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
             if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
             oWindow.center();
         }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_GetConcessionType" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                <br />
                <table width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">From Date</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputbox"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                    ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" class="matters" width="20%"><span class="field-label">To Date</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="inputbox"></asp:TextBox>
                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Business Units</span></td>
                        <td align="left" class="matters" colspan="3">
                            <div class="checkbox-list">
                                <uc1:usrTransportBSUnits ID="UsrTransportBSUnits1" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr id="trSelConcession" runat="server">
                        <td class="matters" colspan="4" style="text-align: left" valign="top">
                            <asp:ImageButton ID="imgPlusConcession" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchConcession();return false;" CausesValidation="False" />
                           <span class="field-label"> Select Concession Filter</span></td>
                    </tr>
                    <tr id="trConcession" runat="server">
                        <td align="left" valign="top" class="matters">
                            <asp:ImageButton ID="imgMinusConcession" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchConcession();return false;"
                                 CausesValidation="False" />
                            <span class="field-label">Concession Type</span></td>
                        <td align="right" valign="top" class="matters"   style="text-align: left">
                            <asp:TextBox ID="txtConcession" runat="server" CssClass="inputbox" AutoPostBack="true" OnTextChanged="txtConcession_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgGetConcession" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetConcessionType();return false;" /></td>
                        <td colspan="2">
                            <asp:GridView ID="grdConc" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-row table-bordered" AllowPaging="True" PageSize="5">
                                <Columns>
                                    <asp:TemplateField HeaderText="Conc. ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBSUID" runat="server" Text='<%# Bind("FCM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FCM_DESCR" HeaderText="Conc. Type" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtnConcDelete" runat="server" OnClick="lnkbtnconcDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>

                <input id="hfBSU" runat="server" type="hidden" />
                <input id="hfConcession" runat="server" type="hidden" />
                <input id="h_BSUID" runat="server" type="hidden" />
                <input id="h_ConcessionType" runat="server" type="hidden" />
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>

