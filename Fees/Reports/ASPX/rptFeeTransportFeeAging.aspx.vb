Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_rptFeeTransportFeeAging
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            UsrTransportBSUnits1.GetSelectedNode()
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            txtFromDate.Text = Format(Date.Now, "dd/MMM/yyyy")
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_TRANSPORTFEE_AGING
                    lblReportCaption.Text = "Transport Fee Ageing"
            End Select
            Page.Title = OASISConstants.Gemstitle
            GetAgeingBucket()
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTFromDate") = txtFromDate.Text
        If IsDate(txtFromDate.Text) Then
            If IsDate(txtFromDate.Text) And IsDate(ViewState("DTFROM")) Then
                If CDate(txtFromDate.Text) < CDate(ViewState("DTFROM")) Or CDate(txtFromDate.Text) > CDate(ViewState("DTTO")) Then
                    'lblError.Text = "Invalid Date"
                    usrMessageBar.ShowNotification("Invalid Date", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            End If
        Else
            'lblError.Text = "Invalid Date (Check Academic Year atart date and end date)"
            usrMessageBar.ShowNotification("Invalid Date (Check Academic Year atart date and end date)", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_TRANSPORTFEE_AGING
                rptAGINGSummary()
        End Select
    End Sub


    Private Sub rptAGINGSummary()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand("FEES.rptAGINGSummary")
        cmd.CommandType = CommandType.StoredProcedure
        'EXEC FEES.AGINGSummary @BSU_ID='125016',
        '@FROMDOCDT='12-AUG-2008' ,@Bkt1=30,
        '@Bkt2 =60,
        '@Bkt3 =90,
        '@Bkt4 =120,
        '@Bkt5 =180 
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)

        cmd.Parameters.AddWithValue("@Bkt1", txtBkt1.Text)
        cmd.Parameters.AddWithValue("@Bkt2", txtBkt2.Text)
        cmd.Parameters.AddWithValue("@Bkt3", txtBkt3.Text)
        cmd.Parameters.AddWithValue("@Bkt4", txtBkt4.Text)
        cmd.Parameters.AddWithValue("@Bkt5", txtBkt5.Text)
        cmd.Parameters.AddWithValue("@Bkt6", txtBkt6.Text)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpMode As New SqlParameter("@Mode", SqlDbType.VarChar, 15)
        sqlpMode.Value = IIf(Me.rblSummary.SelectedValue = 0, "STUDENT", IIf(Me.rblSummary.SelectedValue = 1, "GRADE", "BSU"))
        'If chkGradeSummary.Checked Then
        '    sqlpMode.Value = "GRADE"
        'Else
        '    sqlpMode.Value = "STUDENT"
        'End If
        cmd.Parameters.Add(sqlpMode)


        Dim str_bsuids As String = UsrTransportBSUnits1.GetSelectedNode().ToString.Replace("||", "|")
        'For Each item As ListItem In chklistBusinessUnit.Items
        '    If (item.Selected) Then
        '        'loop through the each checkbox and insert it
        '        str_bsuids = str_bsuids & item.Value & "|"
        '    End If
        'Next


        Dim sqlpSTU_BSU_IDS As New SqlParameter("@STU_BSU_IDS", SqlDbType.VarChar, 5000)
        sqlpSTU_BSU_IDS.Value = str_bsuids
        cmd.Parameters.Add(sqlpSTU_BSU_IDS)

        Dim sqlpStatus As New SqlParameter("@Status", SqlDbType.VarChar, 1)
        sqlpStatus.Value = Me.cblActive.SelectedValue
        cmd.Parameters.Add(sqlpStatus)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = Session("BSU_Name")
        params("FromDATE") = txtFromDate.Text
        params("BKT1") = "0 - " & txtBkt1.Text
        params("BKT2") = txtBkt1.Text & " - " & txtBkt2.Text
        params("BKT3") = txtBkt2.Text & " - " & txtBkt3.Text
        params("BKT4") = txtBkt3.Text & " - " & txtBkt4.Text
        params("BKT5") = txtBkt4.Text & " - " & txtBkt5.Text
        params("BKT6") = txtBkt5.Text & " - " & txtBkt6.Text
        params("BKT7") = " > " & txtBkt6.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = Session("sBsuid")
        If Me.cblActive.SelectedValue = "2" Then
            params("RPT_CAPTION") = "Student Fee Ageing"
        ElseIf Me.cblActive.SelectedValue = "1" Then
            params("RPT_CAPTION") = "Student Fee Ageing(Active)"
        ElseIf Me.cblActive.SelectedValue = "0" Then
            params("RPT_CAPTION") = "Student Fee Ageing(Inactive)"
        End If
        'If chkGradeSummary.Checked Then
        '    repSource.ResourceName = "../../FEES/REPORTS/RPT/rpfFeeTransportAgingSummary_Grade.rpt"
        'Else
        '    repSource.ResourceName = "../../FEES/REPORTS/RPT/rpfFeeTransportAgingSummary_Student.rpt"
        'End If
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rpfFeeTransportAgingSummary_" & IIf(Me.rblSummary.SelectedValue = 0, "Student.rpt", IIf(Me.rblSummary.SelectedValue = 1, "Grade.rpt", "BSU.rpt"))
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Sub GetAgeingBucket()
        Dim qry As String = "SELECT ISNULL(BUS_AGEING_BUCKET,'30|60|90|120|150|180') AS BUS_AGEING_BUCKET FROM dbo.BUSINESSUNIT_SUB WITH(NOLOCK) WHERE BUS_BSU_ID='" & Session("sBsuId") & "' "
        Dim BUCKET() As String
        Dim BUS_AGEING_BUCKET As String = ""
        BUCKET = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry).ToString.Split("|")
        If Not BUCKET Is Nothing AndAlso BUCKET.Length > 0 Then
            If BUCKET.Length >= 6 Then
                txtBkt1.Text = BUCKET(0)
                txtBkt2.Text = BUCKET(1)
                txtBkt3.Text = BUCKET(2)
                txtBkt4.Text = BUCKET(3)
                txtBkt5.Text = BUCKET(4)
                txtBkt6.Text = BUCKET(5)
            Else
                Dim len As Int16, HighestValue As Integer, j As Int16
                len = BUCKET.Length
                j = len - 1
                While (j >= 0)
                    If BUCKET(j) <> "" AndAlso IsNumeric(BUCKET(j)) Then
                        HighestValue = BUCKET(j)
                        len = j + 1
                        Exit While
                    End If
                    j = j - 1
                End While
                ReDim Preserve BUCKET(5)
                For i As Int16 = len To 5 Step 1
                    BUCKET(i) = HighestValue + 30
                    HighestValue = BUCKET(i)
                Next
                txtBkt1.Text = BUCKET(0)
                txtBkt2.Text = BUCKET(1)
                txtBkt3.Text = BUCKET(2)
                txtBkt4.Text = BUCKET(3)
                txtBkt5.Text = BUCKET(4)
                txtBkt6.Text = BUCKET(5)
            End If
        Else
            txtBkt1.Text = "30"
            txtBkt2.Text = "60"
            txtBkt3.Text = "90"
            txtBkt4.Text = "120"
            txtBkt5.Text = "150"
            txtBkt6.Text = "180"
        End If

    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class
