﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_rptDayendDetailsTransport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64 


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then 
            Try
                'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If Session("sUsr_name") = "" Or ViewState("MainMnu_code") <> "F704020" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))

                    Select Case ViewState("MainMnu_code").ToString
                        Case "F704020"
                            lblHead.Text = "View Day End Status"
                    End Select
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    txtDate.Text = Format(Now.Date, OASISConstants.DateFormat)
                    ddlBUnit.DataBind()
                    ddlBUnit.Enabled = False
                    If Not ddlBUnit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
                        ddlBUnit.ClearSelection()
                        ddlBUnit.Items.FindByValue(Session("sBsuid")).Selected = True
                    End If
                    bindData()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
        End If
    End Sub 
     
    Protected Sub ddlBUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBUnit.SelectedIndexChanged
        bindData()
    End Sub 

    Protected Sub imgFrom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgFrom.Click
        bindData()
    End Sub

    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        bindData()
    End Sub

    Sub bindData()
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ASONDT", SqlDbType.VarChar)
        pParms(0).Value = txtDate.Text

        pParms(1) = New SqlClient.SqlParameter("@PROVIDER_BSU_ID", SqlDbType.VarChar)
        pParms(1).Value = ddlBUnit.SelectedItem.Value
        Dim _table As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
                                CommandType.StoredProcedure, "[TRANSPORT].[Dayend_Status]", pParms)
        gvDayendDetails.DataSource = Nothing
        gvDayendDetails.DataBind()
        gvDayendDetails.DataSource = _table.Tables(0)
        gvDayendDetails.DataBind()
        If _table.Tables.Count > 0 Then
            gvNextstatus.DataSource = Nothing
            gvNextstatus.DataBind()
            gvNextstatus.DataSource = _table.Tables(1)
            gvNextstatus.DataBind()
        End If
    End Sub 
     
End Class

