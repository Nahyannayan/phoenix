Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class Fees_Reports_ASPX_rptTRANSPORT_PDC_SCHEDULE
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            Select Case MainMnu_code
                Case "F715041"
                    lblReportCaption.Text = "Transport Fee PDC Schedule"
            End Select
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)

            SetAcademicYearDate()
            txtFeeCounter.Attributes.Add("ReadOnly", "ReadOnly")
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            ' lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            '   lblError.Text = ""
        End If

        'txtToDate.Attributes.Add("ReadOnly", "Readonly")
        'txtFromDate.Attributes.Add("ReadOnly", "Readonly")

        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        h_BSUID.Value = UsrTransportBSUnits1.GetSelectedNode()
        Select Case MainMnu_code
          
            Case "F715041"
                GenerateTransport_PDC_Schedule()

        End Select
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, Session("Current_ACD_ID"), Session("sBsuid"))
        txtFromDate.Text = DTFROM
        'txtToDate.Text = DTTO
        If txtFromDate.Text = "" Then
            txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
        End If
        txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
    End Sub

    Private Sub GenerateTransport_PDC_Schedule()
        Dim cmd As New SqlCommand("[FEES].[RptGetFEEPDCSchedule_DateRange]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@FOR_BSU_IDs", SqlDbType.VarChar)
        sqlpFOR_BSU_IDs.Value = h_BSUID.Value.Replace("||", "|")
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FromDt", SqlDbType.DateTime)
        sqlpFromDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDt As New SqlParameter("@ToDt", SqlDbType.DateTime)
        sqlpToDt.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpToDt)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpbGroupby As New SqlParameter("@Groupby", SqlDbType.TinyInt)
        sqlpbGroupby.Value = RBLFilter.SelectedValue
        cmd.Parameters.Add(sqlpbGroupby)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        repSource.IncludeBSUImage = True

        If RBLFilter.SelectedValue = 0 Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptTRANSPORT_PDC_SCHEDULE_Summary.rpt"
            params("RPT_CAPTION") = "PDC Schedule Report (Summary)"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptTRANSPORT_PDC_SCHEDULE.rpt"
            params("RPT_CAPTION") = "PDC Schedule Report (Detailed)"
        End If


        Session("ReportSource") = repSource
        '    Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

