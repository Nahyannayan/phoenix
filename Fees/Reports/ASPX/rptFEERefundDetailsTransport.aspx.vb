﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_rptFEERefundDetailsTransport
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            SetStudentGrid()
            lblReportCaption.Text = "Fee Refund Details"
            'Me.usrBSUnits1.MenuCode = MainMnu_code
            UsrTransportBSUnits1.GetSelectedNode()
            BindFeeTypes()
            SelectAllTreeView(trvFeeTypes.Nodes, True)
            For i As Integer = 0 To trvFeeTypes.Nodes.Count - 1
                trvFeeTypes.Nodes(i).CollapseAll()
            Next
            'BindAcademicYear(ddlBSUnit.SelectedValue)
            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            Dim DTFROM As String = String.Empty
            Dim DTTO As String = String.Empty
            FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, Session("Current_ACD_ID"), Session("sBsuid"))
            txtFromDate.Text = DTFROM
            If txtFromDate.Text = "" Then
                txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            End If
            txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        End If


        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        GenerateFeeRefundDetails()
    End Sub

    Private Sub GenerateFeeRefundDetails()
        Dim cmd As New SqlCommand("[FEES].[F_GetFeeRefundDetailsTransport]")
        cmd.CommandType = CommandType.StoredProcedure
        UpdateFeesSelected()

        Dim sqlParam(7) As SqlClient.SqlParameter
        sqlParam(0) = New SqlParameter("@BSU_IDs", SqlDbType.VarChar)

        sqlParam(0).Value = UsrTransportBSUnits1.GetSelectedNode().ToString.Replace("||", "|")
        cmd.Parameters.Add(sqlParam(0))
        sqlParam(1) = New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlParam(1).Value = UtilityObj.GenerateXML(h_STUD_ID.Value, XMLType.STUDENT)
        cmd.Parameters.Add(sqlParam(1))
        sqlParam(2) = New SqlParameter("@MODE", SqlDbType.VarChar)
        If h_STUD_ID.Value <> "" Then
            sqlParam(2).Value = "STUDENT"

        Else
            sqlParam(2).Value = "ALL"
        End If
        cmd.Parameters.Add(sqlParam(2))
        sqlParam(4) = New SqlParameter("@FROMDT", SqlDbType.VarChar)
        sqlParam(4).Value = txtFromDate.Text
        cmd.Parameters.Add(sqlParam(4))

        sqlParam(5) = New SqlParameter("@TODT", SqlDbType.VarChar)
        sqlParam(5).Value = txtToDate.Text
        cmd.Parameters.Add(sqlParam(5))

        sqlParam(6) = New SqlParameter("@bPOSTED", SqlDbType.Bit)
        sqlParam(6).Value = True
        cmd.Parameters.Add(sqlParam(6))
        sqlParam(7) = New SqlParameter("@LogIn_BSU_ID", SqlDbType.VarChar)
        'sqlParam(7).Value = ViewState("FEE_IDs")
        sqlParam(7).Value = Session("sBsuid")
        cmd.Parameters.Add(sqlParam(7))

            cmd.Connection = New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)

        'Dim ds As DataTable = Mainclass.getDataTable("[FEES].[F_GetFeeRefundDetails]", sqlParam, cmd.Connection.ConnectionString)
        'SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "[FEES].[F_GetFeeRefundDetails]", cmd.Parameters)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        Dim caption As String = "FEE REFUND DETAILS"

        params("userName") = Session("sUsr_name")
        params("FROMDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text

        params("BSU_NAME") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT BSU_NAME FROM oasis..BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "'")
        params("RPT_CAPTION") = caption
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeRefundDetailsTransport.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx?isExport=true", True)
        Else
            '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        End If
    End Sub



    Sub SetStudentGrid()
        'If chkStudentby.Checked Then
        '    imgStudent.Enabled = True
        '    gvStudentDetails.Visible = True
        'Else
        '    imgStudent.Enabled = False
        '    h_STUD_ID.Value = ""
        '    gvStudentDetails.Visible = False
        '    FillSTUNames(h_STUD_ID.Value)
        'End If
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExporttoexcel.Click
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

    Public Sub BindFeeTypes()
        trvFeeTypes.Nodes.Clear()
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty ' SSC_ID hard coded as 4(Others)
        str_Sql = " SELECT DISTINCT ISNULL(SSC_ID,4) SSC_ID,LTRIM(RTRIM(ISNULL(SC.SSC_DESC,'Others'))) SSC_DESC " & _
                  "FROM FEES.FEES_M FM LEFT JOIN OASIS.dbo.SERVICES_SYS_M SYM ON FM.FEE_SVC_ID=SYM.SVC_ID " & _
                  "LEFT JOIN OASIS.dbo.SERVICES_CATEGORY SC ON SYM.SVC_SSC_ID=SC.SSC_ID ORDER BY SSC_ID,SSC_DESC"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("SSC_DESC").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("SSC_ID").ToString()
                trvFeeTypes.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In trvFeeTypes.Nodes
            BindChildNodes(node)
        Next
        trvFeeTypes.ExpandAll()
    End Sub
    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim childnodeValue = ParentNode.Value
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = " SELECT DISTINCT ISNULL(FEE_ID, 0) FEE_ID,LTRIM(RTRIM(ISNULL(FEE_DESCR,''))) FEE_DESCR " & _
                  "FROM FEES.FEES_M FM LEFT JOIN OASIS.dbo.SERVICES_SYS_M SYM ON FM.FEE_SVC_ID=SYM.SVC_ID " & _
                  "LEFT JOIN OASIS.dbo.SERVICES_CATEGORY SC ON SYM.SVC_SSC_ID=SC.SSC_ID WHERE ISNULL(SSC_ID,4)='" & childnodeValue & "' " & _
                  "ORDER BY ISNULL(FEE_ID, 0),LTRIM(RTRIM(ISNULL(FEE_DESCR,''))) "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("FEE_DESCR").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("FEE_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If
        'For Each node As TreeNode In ParentNode.ChildNodes
        '    BindChildNodes(node)
        'Next
    End Sub

    Private Sub UpdateFeesSelected()
        Try
            Dim FEE_IDs As String = ""
            For Each node As TreeNode In trvFeeTypes.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If FEE_IDs.Trim() = "" Then
                    FEE_IDs = node.Value
                Else
                    FEE_IDs = FEE_IDs + "|" + node.Value
                End If
            Next
            ViewState("FEE_IDs") = FEE_IDs
        Catch
            ViewState("FEE_IDs") = ""
        End Try
    End Sub
    Private Sub SelectAllTreeView(ByVal nodes As TreeNodeCollection, ByVal selAll As Boolean)
        For Each node As TreeNode In nodes
            node.Checked = selAll
            If node.ChildNodes.Count > 0 Then
                SelectAllTreeView(node.ChildNodes, selAll)
            End If
        Next
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
