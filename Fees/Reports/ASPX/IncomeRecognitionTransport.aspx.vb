﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_IncomeRecognitionTransport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        'UsrBSUnits1.MenuCode = ViewState("MainMnu_code")
        UsrTransportBSUnits1.GetSelectedNode()
        Page.Title = OASISConstants.Gemstitle
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
        If Not IsPostBack Then
            UtilityObj.NoOpen(Me.Header)
            SetAcademicYearDate()
            txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
            txtTodate.Attributes.Add("ReadOnly", "Readonly")
            lblrptCaption.Text = "Income Recognition"
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        'h_BSUID.Value = UsrBSUnits1.GetSelectedNode()

        h_BSUID.Value = UsrTransportBSUnits1.GetSelectedNode().ToString.Replace("||", "|")
        'Dim str_bsuids As String = UsrTransportBSUnits1.GetSelectedNode().ToString.Replace("||", "|")
        'sqlpSTU_BSU_IDS.Value = str_bsuids
        'cmd.Parameters.Add(sqlpSTU_BSU_IDS)


        GenerateNormal()
    End Sub

    Private Sub GenerateNormal()
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_conn1 As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim BsuId As String = h_BSUID.Value
        BsuId = BsuId.Replace("|", "@")
        Dim Mode As String = "NORMAL"
        If chkConsolidation.Checked = True Then
            Mode = "CONSOLD"
        End If

        Dim cmd As New SqlCommand("FEES.rptINCOME_recognitionTransport", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFROMDATE As New SqlParameter("@FROM_DATE", SqlDbType.DateTime)
        sqlpFROMDATE.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDATE)

        Dim sqlpTODATE As New SqlParameter("@TO_DATE", SqlDbType.DateTime)
        sqlpTODATE.Value = txtTodate.Text
        cmd.Parameters.Add(sqlpTODATE)

        Dim sqlpBSUID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        If BsuId.Equals("") Then
            sqlpBSUID.Value = DBNull.Value
        Else
            sqlpBSUID.Value = BsuId.ToString()
        End If

        cmd.Parameters.Add(sqlpBSUID)

        Dim sqlpMODE As New SqlParameter("@MODE", SqlDbType.VarChar)
        sqlpMODE.Value = Mode
        cmd.Parameters.Add(sqlpMODE)

        Dim SQLLoginBSUID As New SqlParameter("@Login_BSU_ID", SqlDbType.VarChar)
        SQLLoginBSUID.Value = Session("sBsuid")
        cmd.Parameters.Add(SQLLoginBSUID)



        objConn.Close()
        objConn.Open()

        If 1 = 1 Then


            cmd.Connection = New SqlConnection(str_conn1)
        

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")


            If Mode = "NORMAL" Then
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptIncomeRecognitionTransport.rpt"
                params("ReportHeading") = "Fee Income Date Between.." & txtFromDate.Text.ToString() & " .. To .." & txtTodate.Text.ToString()
            Else
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptIncomeRecognitionConsolTransport.rpt"
                params("ReportHeading") = "Consolidate Fee Income Date Between.." & txtFromDate.Text.ToString() & " .. To .." & txtTodate.Text.ToString()
                params("BsuShotname") = GetBSUName(BsuId)
            End If
            repSource.Parameter = params
            repSource.Command = cmd

            Session("ReportSource") = repSource
            'repSource.IncludeBSUImage = True
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            'If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            '    Response.Redirect("rptviewer.aspx?isExport=true", True)
            'Else
            '    Response.Redirect("rptviewer.aspx", True)
            'End If

            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Else
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        End If
        objConn.Close()
    End Sub

    Private Function GetBSUName(ByVal BSUName As String) As String
        'BSUName = BSUName.Replace("@", ",")
        'BSUName = BSUName.Substring(0, BSUName.Length - 1)
        Dim retString As String = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MainDBO").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT BSU_SHORTNAME FROM BUSINESSUNIT_M  WHERE BSU_ID IN(SELECT ID FROM OASISFIN.dbo.fnSplitMe('" + BSUName + "','@')) ORDER BY BSU_SHORTNAME "
        Dim _dataset As DataSet
        _dataset = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If _dataset.Tables(0).Rows.Count < 1 Then
            retString = "All BSU"
            Return retString
        End If
        For i As Integer = 0 To _dataset.Tables(0).Rows.Count - 1
            If i = 0 Then
                retString = _dataset.Tables(0).Rows(i)(0).ToString()
            Else
                retString += ":" & _dataset.Tables(0).Rows(i)(0).ToString()

            End If
        Next
        Return retString
    End Function

    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        'grdBSU.DataSource = ds
        'grdBSU.DataBind()
        Return True
    End Function

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, Session("Current_ACD_ID"), Session("sBsuid"))
        txtFromDate.Text = DTFROM
        'txtToDate.Text = DTTO
        txtTodate.Text = Format(Now.Date, OASISConstants.DateFormat)
        If txtFromDate.Text = "" Then
            txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        End If
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class