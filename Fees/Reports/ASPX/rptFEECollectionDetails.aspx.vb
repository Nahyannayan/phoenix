Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class Fees_Reports_ASPX_rptFEEReceiptSummary
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ClientScript.RegisterStartupScript(Me.GetType(), _
        "script", "<script language='javascript'>  CheckOnPostback(); </script>")
        If Not Page.IsPostBack Then
            hfBSU.Value = "display"
        End If
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            chkGroup.Text = ""
            trChkConciliation.Visible = False
            chkReconciliation.Visible = False
            trchkGroup.Visible = True
            chkGroup.Visible = True
            chkGroup.Text = "View in Group Currency"
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_REP_NORM_COLLECTION_DET
                    h_bTransport.Value = 0
                    lblReportCaption.Text = "Fee Collection Detailed Report"
                Case OASISConstants.MNU_FEE_REP_COLLECTION_DET
                    lblReportCaption.Text = "Transport Fee Collection Detailed Report"
                    h_bTransport.Value = 1
                Case OASISConstants.MNU_FEE_REP_COLLECTION_SUMMARY
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Collection Summary Report"
                Case OASISConstants.MNU_FEE_REP_NORM_COLLECTION_SUMMARY
                    h_bTransport.Value = 0
                    lblReportCaption.Text = "Fee Collection Summary Report(Normal)"
                Case OASISConstants.MNU_FEE_REP_COLL_SUMMARY_SCH
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Collection School Summary Report"
                    trChkConciliation.Visible = True
                    chkReconciliation.Visible = True
                Case OASISConstants.MNU_FEE_REP_NORM_COLL_SUMMARY_SCH
                    h_bTransport.Value = 0
                    lblReportCaption.Text = "Fee Collection School Summary Report(Normal)"
                    trChkConciliation.Visible = True
                    chkReconciliation.Visible = True
                Case OASISConstants.MNU_FEE_REP_COLL_SUMMARY_EMP
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Collection Employee Summary Report"
                Case OASISConstants.MNU_FEE_REP_NORM_COLL_SUMMARY_EMP
                    h_bTransport.Value = 0
                    lblReportCaption.Text = "Fee Collection Employee Summary Report"
                Case OASISConstants.MNU_FEE_REP_COLL_SUMMARY_EMP_SCH
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Collection School & Employee Summary Report"
                    trChkConciliation.Visible = True
                    chkReconciliation.Visible = True
                Case OASISConstants.MNU_FEE_REP_CHQ_COLL_SUMMARY
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Cheque Collection Details"
                    chkGroup.Text = "Bank wise Summary"
                Case OASISConstants.MNU_FEE_REP_NORM_CHQ_COLL_SUMMARY
                    h_bTransport.Value = 0
                    lblReportCaption.Text = "Fee Cheque Collection Details"
                    chkGroup.Text = "Bank wise Summary"
                    ChkPdcExclude.Visible = True
                Case OASISConstants.MNU_FEE_TRAN_COLL_AREAWISE
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Collection Areawise Report"
                    trchkGroup.Visible = False
                    chkGroup.Visible = False
                Case OASISConstants.MNU_FEE_TRAN_COLL_STUDENTS
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Collection Students Report"
                    trchkGroup.Visible = False
                    chkGroup.Visible = False
                Case OASISConstants.MNU_FEE_NORM_COLL_STUDENTS
                    h_bTransport.Value = 0
                    lblReportCaption.Text = "Fee Collection Students Report"
                    trchkGroup.Visible = False
                    chkGroup.Visible = False
                Case OASISConstants.MNU_FEE_REP_CHQ_CLEARANCE
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Cheque Clearance Report"
                    trchkGroup.Visible = True
                    chkGroup.Visible = True
                    chkGroup.Text = "PDC Only"
                Case OASISConstants.MNU_FEE_NORM_CHQ_CLEARANCE
                    h_bTransport.Value = 0
                    lblReportCaption.Text = "Fee Cheque Clearance Report"
                    trchkGroup.Visible = False
                    chkGroup.Visible = False
                Case OASISConstants.MNU_FEE_TRANS_COLL_VIEW_ALL
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Fee Collection School & Summary Report "
                    trchkGroup.Visible = False
                    chkGroup.Visible = False
                Case OASISConstants.MNU_FEE_REP_CREDIT_CARD_COLL_SUMMARY
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Credit Card Collection Details"
                    chkGroup.Text = "Bank wise Summary"
                Case OASISConstants.MNU_FEE_TRANS_REP_CHARGE_OVERALL
                    lblReportCaption.Text = "Transport Fee Charge (Over All)"
                    trChkConciliation.Visible = False
                    trchkGroup.Visible = False
                Case OASISConstants.MNU_FEE_FEERECEIPTCANCELLATION_FEES
                    lblReportCaption.Text = "Fee Receipt Cancellation"
                    trChkConciliation.Visible = False
                    trchkGroup.Visible = False
                Case OASISConstants.MNU_FEE_NORM_COLL_PDC_SCH_FEE
                    trChkConciliation.Visible = False
                    trchkGroup.Visible = True
                    chkGroup.Text = "Summary"
                    lblReportCaption.Text = "Fee PDC Schedule"
                    trChkConciliation.Visible = False 
                    h_bTransport.Value = 0

            End Select
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
             
            SetAcademicYearDate()
            txtFeeCounter.Attributes.Add("ReadOnly", "ReadOnly")
            FillBSUNames(True)
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If

        'txtToDate.Attributes.Add("ReadOnly", "Readonly")
        'txtFromDate.Attributes.Add("ReadOnly", "Readonly")

        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
        FillBSUNames()
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

       

        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_TRAN_COLL_AREAWISE, _
                            OASISConstants.MNU_FEE_REP_NORM_COLLECTION_DET, _
                            OASISConstants.MNU_FEE_REP_NORM_COLLECTION_SUMMARY, _
                            OASISConstants.MNU_FEE_REP_NORM_COLL_SUMMARY_SCH, _
                            OASISConstants.MNU_FEE_REP_NORM_COLL_SUMMARY_EMP, _
                            OASISConstants.MNU_FEE_NORM_COLL_STUDENTS, _
                            OASISConstants.MNU_FEE_TRANS_REP_CHARGE_OVERALL, _
                            OASISConstants.MNU_FEE_FEERECEIPTCANCELLATION_FEES
                If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
                    'lblError.Text = "Invalid Date !!!"
                    usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
        End Select
        
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_TRAN_COLL_AREAWISE
                GenerateTransportFEECollectionAreawise()
            Case OASISConstants.MNU_FEE_REP_NORM_COLLECTION_DET, _
                OASISConstants.MNU_FEE_REP_NORM_COLLECTION_SUMMARY, _
                OASISConstants.MNU_FEE_REP_NORM_COLL_SUMMARY_SCH, _
                OASISConstants.MNU_FEE_REP_NORM_COLL_SUMMARY_EMP, _
                OASISConstants.MNU_FEE_REP_NORM_CHQ_COLL_SUMMARY, _
                OASISConstants.MNU_FEE_NORM_COLL_STUDENTS
                GenerateNormalFeeCollectionDetails()
            Case OASISConstants.MNU_FEE_REP_CHQ_CLEARANCE
                GenerateFeeCollectionChequeClearanceDatewise()
            Case OASISConstants.MNU_FEE_NORM_CHQ_CLEARANCE
                GenerateNormalFeeCollectionChequeClearanceDatewise()
            Case OASISConstants.MNU_FEE_TRANS_REP_CHARGE_OVERALL
                GenerateFeeChargeOverAllReport()
            Case OASISConstants.MNU_FEE_FEERECEIPTCANCELLATION_FEES
                F_GETFEERECEIPTCANCELLATION_FEES()
            Case OASISConstants.MNU_FEE_NORM_COLL_PDC_SCH_FEE
                GenerateFeeColl_PDC_Schedule()
            Case Else
                GenerateFeeCollectionDetails()
        End Select
    End Sub

    Private Sub F_GETFEERECEIPTCANCELLATION_FEES()
        Dim cmd As New SqlCommand("[FEES].[F_GETFEERECEIPTCANCELLATION_FEES]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@FOR_BSU_IDs", SqlDbType.VarChar)
        sqlpFOR_BSU_IDs.Value = h_BSUID.Value.Replace("||", "|")
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        params("Currency") = Session("BSU_CURRENCY")
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        params("RPT_CAPTION") = "Cancelled Students Fees Receipt"
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_Fee_REC_CANCELLATION.rpt"

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeChargeOverAllReport()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand
        cmd = New SqlCommand("[FEES].[F_GETTRANSPORTFEECHARGE_OVER_ALL]")

        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.Xml)
        sqlpBSU_ID.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFROMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFROMDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FROMDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        params("RPT_CAPTION") = "FEE CHARGE REPORT(OVER ALL)"
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEECHARGEREPORT_SUMMARY.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateNormalFeeCollectionDetails()
        Dim cmd As New SqlCommand("[FEES].[F_RPT_NormalFeeCollectionData]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@FOR_BSU_IDs", SqlDbType.Xml)
        sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpbGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpbGroupCurrency.Value = chkGroup.Checked
        cmd.Parameters.Add(sqlpbGroupCurrency)

        Dim sqlpPdcExclude As New SqlParameter("@PdcExclude", SqlDbType.Bit)
        'If ChkPdcExclude.Checked Then
        sqlpPdcExclude.Value = ChkPdcExclude.Checked
        'Else
        'sqlpPdcExclude.Value = 1
        'End If
        cmd.Parameters.Add(sqlpPdcExclude)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Dim sqlpbLvl As New SqlParameter("@Lvl", SqlDbType.TinyInt)
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_NORM_COLLECTION_DET
                sqlpbLvl.Value = 0
                params("RPT_CAPTION") = "Fee Collection Detailed Report"
                params("Summary") = False
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_CollectionDetails.rpt"
            Case OASISConstants.MNU_FEE_REP_NORM_COLLECTION_SUMMARY
                sqlpbLvl.Value = 1
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_CollectionSummary.rpt"
            Case OASISConstants.MNU_FEE_REP_NORM_COLL_SUMMARY_SCH
                If chkReconciliation.Checked Then
                    sqlpbLvl.Value = 5
                    params("RPT_CAPTION") = "Fee Collection School Summary Report (Reconciliation)"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_Coll_BSU_Summary.rpt"
                Else
                    sqlpbLvl.Value = 2
                    params("RPT_CAPTION") = "Fee Collection School Summary Report"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_CollectionDetSummary.rpt"
                End If
            Case OASISConstants.MNU_FEE_REP_NORM_COLL_SUMMARY_EMP
                sqlpbLvl.Value = 3
                params("RPT_CAPTION") = "Fee Collection Employee Summary Report"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_Coll_EMP_Summary.rpt"
            Case OASISConstants.MNU_FEE_REP_COLL_SUMMARY_EMP_SCH
                If chkReconciliation.Checked Then
                    sqlpbLvl.Value = 5
                    params("RPT_CAPTION") = "Fee Collection School & Employee Summary Report(Reconciliation)"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_STU_BSU_SUMMARY.rpt"
                Else
                    sqlpbLvl.Value = 4
                    params("RPT_CAPTION") = "Fee Collection School & Employee Summary Report"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionBSUDetSummary.rpt"
                End If
            Case OASISConstants.MNU_FEE_REP_NORM_CHQ_COLL_SUMMARY
                sqlpbLvl.Value = 6
                params("RPT_CAPTION") = "Fee Cheque Collection Report"
                If ChkPdcExclude.Checked Then
                    params("RPT_CAPTION") = "Fee Cheque Collection Report (Exclude PDC)"
                End If
                If chkGroup.Checked Then
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_ChqPaymentDetBank.rpt"
                Else
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_ChqPaidDetails.rpt"
                End If
            Case OASISConstants.MNU_FEE_REP_CHQ_CLEARANCE
                sqlpbLvl.Value = 6
                params("RPT_CAPTION") = "Fee Cheque Clearance Report"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEChqPaymentDetBank.rpt"

            Case OASISConstants.MNU_FEE_NORM_COLL_STUDENTS
                sqlpbLvl.Value = 5
                params("RPT_CAPTION") = "Fee Collection School & Student Summary Report(Reconciliation)"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_STU_BSU_SUMMARY.rpt"

        End Select
        cmd.Parameters.Add(sqlpbLvl)
        repSource.IncludeBSUImage = True
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateTransportFEECollectionAreawise()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_TRAN_FEECOLL_STUD]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        params("RPT_CAPTION") = "Transport Fee Collection Areawise Report"
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        repSource.ResourceName = "../../fees/reports/rpt/rptFEEColl_Transp_Area.rpt"

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeCollectionChequeClearanceDatewise()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_RPTChequeClearanceDate]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@FOR_BSU_IDs", SqlDbType.Xml)
        sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpbGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpbGroupCurrency.Value = chkGroup.Checked
        cmd.Parameters.Add(sqlpbGroupCurrency)

        Dim sqlpbPDCOnly As New SqlParameter("@bPDCOnly", SqlDbType.Bit)
        sqlpbPDCOnly.Value = chkGroup.Checked
        cmd.Parameters.Add(sqlpbPDCOnly)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        params("RPT_CAPTION") = "Cheque Clearance Report"
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEChqBankPaymentWiseReport.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateNormalFeeCollectionChequeClearanceDatewise()
        Dim cmd As New SqlCommand("[FEES].[F_RPTNormalFeeChequeClearanceDate]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@FOR_BSU_IDs", SqlDbType.VarChar)
        sqlpFOR_BSU_IDs.Value = h_BSUID.Value.Replace("||", "|")
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpbGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpbGroupCurrency.Value = chkGroup.Checked
        cmd.Parameters.Add(sqlpbGroupCurrency)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        params("RPT_CAPTION") = "Cheque Clearance Report"
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_ChqBankPaymentWiseReport.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeCollectionDetails()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_RPTCollectionData]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@FOR_BSU_IDs", SqlDbType.Xml)
        sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpbGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpbGroupCurrency.Value = chkGroup.Checked
        cmd.Parameters.Add(sqlpbGroupCurrency)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Dim sqlpbLvl As New SqlParameter("@Lvl", SqlDbType.TinyInt)
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_COLLECTION_DET
                sqlpbLvl.Value = 0
                params("RPT_CAPTION") = "Fee Collection Detailed Report"
                params("Summary") = False
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionDetails.rpt"
            Case OASISConstants.MNU_FEE_REP_COLLECTION_SUMMARY
                sqlpbLvl.Value = 1
                'params("Summary") = False
                params("RPT_CAPTION") = "Fee Collection Summary Report"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionDetSummary.rpt"
            Case OASISConstants.MNU_FEE_REP_COLL_SUMMARY_SCH
                If chkReconciliation.Checked Then
                    sqlpbLvl.Value = 5
                    params("RPT_CAPTION") = "Fee Collection School Summary Report (Reconciliation)"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEColl_BSU_Summary.rpt"
                Else
                    sqlpbLvl.Value = 2
                    params("RPT_CAPTION") = "Fee Collection School Summary Report"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionDetSummary.rpt"
                End If
            Case OASISConstants.MNU_FEE_REP_COLL_SUMMARY_EMP
                sqlpbLvl.Value = 3
                params("RPT_CAPTION") = "Fee Collection Employee Summary Report"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionDetSummary.rpt"
            Case OASISConstants.MNU_FEE_REP_COLL_SUMMARY_EMP_SCH
                If chkReconciliation.Checked Then
                    sqlpbLvl.Value = 5
                    params("RPT_CAPTION") = "Fee Collection School & Employee Summary Report(Reconciliation)"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEColl_EMP_BSU_Summary.rpt"
                Else
                    sqlpbLvl.Value = 4
                    params("RPT_CAPTION") = "Fee Collection School & Employee Summary Report"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionBSUDetSummary.rpt"
                End If
            Case OASISConstants.MNU_FEE_REP_CHQ_COLL_SUMMARY
                sqlpbLvl.Value = 6
                params("RPT_CAPTION") = "Fee Cheque Collection Report"
                If chkGroup.Checked Then
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEChqPaymentDetBank.rpt"
                Else
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEChqPaidDetails.rpt"
                End If
            Case OASISConstants.MNU_FEE_REP_CREDIT_CARD_COLL_SUMMARY
                sqlpbLvl.Value = 8
                params("RPT_CAPTION") = "Fee Credit Card Collection Report"
                If chkGroup.Checked Then
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECreditCardPaidDetails.rpt"
                Else
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECreditCardPaidDetails.rpt"
                End If

            Case OASISConstants.MNU_FEE_REP_CHQ_CLEARANCE
                sqlpbLvl.Value = 6
                params("RPT_CAPTION") = "Fee Cheque Clearance Report"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEChqBankPaymentWiseReport.rpt"

            Case OASISConstants.MNU_FEE_TRAN_COLL_STUDENTS
                sqlpbLvl.Value = 5
                params("RPT_CAPTION") = "Fee Collection School & Student Summary Report(Reconciliation)"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEColl_TRANS_STU_BSU.rpt"
            Case OASISConstants.MNU_FEE_TRANS_COLL_VIEW_ALL
                sqlpbLvl.Value = 5
                params("RPT_CAPTION") = "Fee Collection Sumary Report(Reconciliation)"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEColl_TRANS_Full.rpt"
        End Select
        cmd.Parameters.Add(sqlpbLvl)
       
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value, False) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            'grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames()
        End If
    End Sub

    Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
        'FillBSUNames()
    End Sub

    Private Function FillBSUNamesTransport(ByVal BSUIDs As String, Optional ByVal bgetAll As Boolean = False) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim conn As New SqlConnection(str_conn)
        Dim ds As New DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        If bgetAll Then
            condition = ""
        Else
            condition = " SVB_BSU_ID in(" & condition & ")"
        End If
        Try
            Dim cmd As New SqlCommand
            cmd.CommandText = "[GETUNITSFORTRANSPORTFEES]"
            cmd.Parameters.AddWithValue("@Usr_Name", Session("sUsr_Name"))
            cmd.Parameters.AddWithValue("@PROVIDER_BSU_ID", Session("sBsuid"))
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = cmd
            adpt.Fill(ds)
            If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
                Return False
            End If
            Dim dv As New DataView(ds.Tables(0))
            dv.RowFilter = condition
            If bgetAll Then
                h_BSUID.Value = ""
                For i As Integer = 0 To dv.Table.Rows.Count - 1
                    h_BSUID.Value += dv.Item(i)("SVB_BSU_ID") & "||"
                Next
            End If
            Return True
        Catch
            Return False
        Finally
            conn.Close()
        End Try

    End Function

    Private Function FillBSUNames(ByVal BSUIDs As String, ByVal bGetMulti As Boolean) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        If bGetMulti Then
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
        Else
            condition = "'" & BSUIDs & "'"
        End If
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID SVB_BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        'grdBSU.DataSource = ds
        'grdBSU.DataBind()
        'txtBSUName.Text = ""
        'Dim bval As Boolean = dr.Read
        'While (bval)
        '    txtBSUName.Text += dr(0).ToString()
        '    bval = dr.Read()
        '    If bval Then
        '        txtBSUName.Text += "||"
        '    End If
        'End While
        Return True
    End Function

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function FillBSUNames(Optional ByVal bGetAll As Boolean = False) As Boolean
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_TRAN_COLL_AREAWISE, _
                 OASISConstants.MNU_FEE_REP_CHQ_CLEARANCE
                FillBSUNamesTransport(h_BSUID.Value, bGetAll)
            Case OASISConstants.MNU_FEE_REP_NORM_COLLECTION_DET, _
                OASISConstants.MNU_FEE_REP_NORM_COLLECTION_SUMMARY, _
                OASISConstants.MNU_FEE_REP_NORM_COLL_SUMMARY_SCH, _
                OASISConstants.MNU_FEE_REP_NORM_COLL_SUMMARY_EMP, _
                OASISConstants.MNU_FEE_REP_NORM_CHQ_COLL_SUMMARY, _
                OASISConstants.MNU_FEE_NORM_COLL_STUDENTS, _
                OASISConstants.MNU_FEE_NORM_CHQ_CLEARANCE
                If bGetAll Then h_BSUID.Value = Session("sBSUID")
                Return FillBSUNames(h_BSUID.Value, True)
            Case Else
                Return FillBSUNamesTransport(h_BSUID.Value, bGetAll)
        End Select

    End Function

    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, Session("Current_ACD_ID"), Session("sBsuid"))
        txtFromDate.Text = DTFROM
        'txtToDate.Text = DTTO
        If txtFromDate.Text = "" Then
            txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
        End If
        txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
    End Sub

    Private Sub GenerateFeeColl_PDC_Schedule()
        Dim cmd As New SqlCommand("[FEES].[RptGetFEEPDCSchedule_DateRange]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@FOR_BSU_IDs", SqlDbType.VarChar)
        sqlpFOR_BSU_IDs.Value = h_BSUID.Value.Replace("||", "|")
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FromDt", SqlDbType.DateTime)
        sqlpFromDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDt As New SqlParameter("@ToDt", SqlDbType.DateTime)
        sqlpToDt.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpToDt)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        repSource.IncludeBSUImage = True

        Dim sqlpbGroupby As New SqlParameter("@Groupby", SqlDbType.TinyInt)
        If chkGroup.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_ChqPaidDetails_pdc_DatePeriod_Summary.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_ChqPaidDetails_pdc_DatePeriod.rpt"
        End If

        params("RPT_CAPTION") = "PDC Schedule Report (Detailed)"
        sqlpbGroupby.Value = 3

        cmd.Parameters.Add(sqlpbGroupby)
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

