Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptFEESrvTxnDetails
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            gvStudentDetails.Attributes.Add("bordercolor", "#1b80b6")
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            SetStudentGrid()
            lblReportCaption.Text = "Service Fee Transaction Details"
            Me.usrBSUnits1.MenuCode = MainMnu_code
            BindFeeTypes()
            SelectAllTreeView(trvFeeTypes.Nodes, True)
            For i As Integer = 0 To trvFeeTypes.Nodes.Count - 1
                trvFeeTypes.Nodes(i).CollapseAll()
            Next
            'BindAcademicYear(ddlBSUnit.SelectedValue)
            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            Dim DTFROM As String = String.Empty
            Dim DTTO As String = String.Empty
            FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, Session("Current_ACD_ID"), Session("sBsuid"))
            txtFromDate.Text = DTFROM
            If txtFromDate.Text = "" Then
                txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            End If
            txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        End If


        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            ' lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            ' lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
            '   lblError.Text = "Invalid Date !!!"
            usrMessageBar.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        GenerateServiceFeeTxnDetail()
    End Sub

    Private Sub GenerateServiceFeeTxnDetail()
        Dim cmd As New SqlCommand("FEES.GMA_GLA_TRANS_DETAIL")
        
        cmd.CommandType = CommandType.StoredProcedure
        UpdateFeesSelected()

        Dim sqlParam(6) As SqlClient.SqlParameter
        sqlParam(0) = New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlParam(0).Value = usrBSUnits1.GetSelectedNode().Replace("||", "|")
        cmd.Parameters.Add(sqlParam(0))
        sqlParam(1) = New SqlParameter("@STU_ID", SqlDbType.VarChar)
        sqlParam(1).Value = h_STUD_ID.Value
        cmd.Parameters.Add(sqlParam(1))
        sqlParam(2) = New SqlParameter("@SVC_Status", SqlDbType.TinyInt)
        sqlParam(2).Value = Me.rblActiv.SelectedValue
        cmd.Parameters.Add(sqlParam(2))

        sqlParam(3) = New SqlParameter("@FROMDT", SqlDbType.VarChar)
        sqlParam(3).Value = txtFromDate.Text
        cmd.Parameters.Add(sqlParam(3))

        sqlParam(4) = New SqlParameter("@TODT", SqlDbType.VarChar)
        sqlParam(4).Value = txtToDate.Text
        cmd.Parameters.Add(sqlParam(4))

        sqlParam(5) = New SqlParameter("@bDetailed", SqlDbType.Bit)
        sqlParam(5).Value = Me.chkDetail.Checked
        cmd.Parameters.Add(sqlParam(5))

        sqlParam(6) = New SqlParameter("@FEE_ID", SqlDbType.VarChar)
        sqlParam(6).Value = ViewState("FEE_IDs")
        cmd.Parameters.Add(sqlParam(6))

        If MainMnu_code = "F733042" Then
            cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_SERVICESConnectionString)
        Else
            cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        End If



        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        Dim caption As String = "SERVICE FEE TRANSACTION DETAILS"

        params("userName") = Session("sUsr_name")
        params("FROMDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text

        params("BSU_NAME") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT BSU_NAME FROM oasis..BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "'")
        params("RPT_CAPTION") = caption
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True

        If MainMnu_code = "F733042" Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEESrvTxnDetails_service.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEESrvTxnDetails.rpt"
        End If

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx?isExport=true", True)
        Else
            ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        End If
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub   

    Sub SetStudentGrid()
        'If chkStudentby.Checked Then
        '    imgStudent.Enabled = True
        '    gvStudentDetails.Visible = True
        'Else
        '    imgStudent.Enabled = False
        '    h_STUD_ID.Value = ""
        '    gvStudentDetails.Visible = False
        '    FillSTUNames(h_STUD_ID.Value)
        'End If
    End Sub

    Private Function FillSTUNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        If radEnquiry.Checked Then
            str_Sql = " SELECT STU_NO , STU_NAME FROM FEES.vw_OSO_ENQUIRY_COMP WHERE STU_ID IN (" + condition + ")"
        Else
            str_Sql = " SELECT STU_NO , STU_NAME FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID IN (" + condition + ")"
        End If
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
        gvStudentDetails.DataSource = ds
        gvStudentDetails.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        FillSTUNames(h_STUD_ID.Value)
    End Sub


    Protected Sub lblAddNewStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        AddStudent()
    End Sub

    Private Sub AddStudent()
        If txtStudName.Text <> "" Then
            Dim str_stuid As String = FeeCommon.GetTransportStudentID(txtStudName.Text, Session("sBsuid"), radEnquiry.Checked, False)
            If str_stuid <> "" Then
                If h_STUD_ID.Value.EndsWith("||") Then
                    h_STUD_ID.Value = h_STUD_ID.Value & str_stuid
                Else
                    h_STUD_ID.Value = h_STUD_ID.Value & "||" & str_stuid
                End If
                txtStudName.Text = ""
                FillSTUNames(h_STUD_ID.Value)
            End If
        End If
    End Sub

    Protected Sub txtStudName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        AddStudent()
    End Sub

    Protected Sub radEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        h_STUD_ID.Value = ""
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Protected Sub radStudent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        h_STUD_ID.Value = ""
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExporttoexcel.Click
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

    Public Sub BindFeeTypes()
        trvFeeTypes.Nodes.Clear()
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty ' SSC_ID hard coded as 4(Others)
        str_Sql = " SELECT DISTINCT ISNULL(SSC_ID,6) SSC_ID,LTRIM(RTRIM(ISNULL(SC.SSC_DESC,'Others'))) SSC_DESC " & _
                  "FROM FEES.FEES_M FM LEFT JOIN OASIS.dbo.SERVICES_SYS_M SYM ON FM.FEE_SVC_ID=SYM.SVC_ID " & _
                  "LEFT JOIN OASIS.dbo.SERVICES_CATEGORY SC ON SYM.SVC_SSC_ID=SC.SSC_ID ORDER BY SSC_ID,SSC_DESC"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("SSC_DESC").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("SSC_ID").ToString()
                trvFeeTypes.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In trvFeeTypes.Nodes
            BindChildNodes(node)
        Next
        trvFeeTypes.ExpandAll()
    End Sub
    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim childnodeValue = ParentNode.Value
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = " SELECT DISTINCT ISNULL(FEE_ID, 0) FEE_ID,LTRIM(RTRIM(ISNULL(FEE_DESCR,''))) FEE_DESCR " & _
                  "FROM FEES.FEES_M FM LEFT JOIN OASIS.dbo.SERVICES_SYS_M SYM ON FM.FEE_SVC_ID=SYM.SVC_ID " & _
                  "LEFT JOIN OASIS.dbo.SERVICES_CATEGORY SC ON SYM.SVC_SSC_ID=SC.SSC_ID WHERE ISNULL(SSC_ID,6)='" & childnodeValue & "' " & _
                  "ORDER BY ISNULL(FEE_ID, 0),LTRIM(RTRIM(ISNULL(FEE_DESCR,''))) "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("FEE_DESCR").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("FEE_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If
        'For Each node As TreeNode In ParentNode.ChildNodes
        '    BindChildNodes(node)
        'Next
    End Sub

    Private Sub UpdateFeesSelected()
        Try
            Dim FEE_IDs As String = ""
            For Each node As TreeNode In trvFeeTypes.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If FEE_IDs.Trim() = "" Then
                    FEE_IDs = node.Value
                Else
                    FEE_IDs = FEE_IDs + "|" + node.Value
                End If
            Next
            ViewState("FEE_IDs") = FEE_IDs
        Catch
            ViewState("FEE_IDs") = ""
        End Try
    End Sub
    Private Sub SelectAllTreeView(ByVal nodes As TreeNodeCollection, ByVal selAll As Boolean)
        For Each node As TreeNode In nodes
            node.Checked = selAll
            If node.ChildNodes.Count > 0 Then
                SelectAllTreeView(node.ChildNodes, selAll)
            End If
        Next
    End Sub
End Class
