Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports GemBox.Spreadsheet

Partial Class Fees_Reports_ASPX_rptFGBOnlinePayments
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            usrBSUnits1.MenuCode = ViewState("MainMnu_code")
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle

            txtFromDate.Text = Format(DateTime.Now.AddMonths(-1), OASISConstants.DateFormat)
            txtTodate.Text = Format(DateTime.Now, OASISConstants.DateFormat)

            Select Case ViewState("MainMnu_code").ToString
                Case "F704014"
                    lblReportCaption.Text = "FGB Online Payments-Consolidated"
            End Select
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            ' lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            '  lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case ViewState("MainMnu_code").ToString
            Case "F704014"
                FGBOnlinePayments()
        End Select
    End Sub

    Private Sub FGBOnlinePayments()
        Dim cmd As New SqlCommand("[FEES].[FGB_InternetCollection]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlpBSU_ID.Value = usrBSUnits1.GetSelectedNode("|")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFromDT As New SqlParameter("@FROMDT ", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TODT ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtTodate.Text)
        cmd.Parameters.Add(sqlpToDT)

        Dim sqlpFCL_SOURCE As New SqlParameter("@FCL_SOURCE", SqlDbType.VarChar, 20)
        sqlpFCL_SOURCE.Value = "FGBONLINE"
        cmd.Parameters.Add(sqlpFCL_SOURCE)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("RPT_CAPTION") = "FGB ONLINE PAYMENTS - CONSOLIDATED"
        params("userName") = Session("sUsr_name")
        params("ReportHead") = txtFromDate.Text & " To " & txtTodate.Text

        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeFGBOnlinePayments_Consolidated.rpt"
        Session("ReportSource") = repSource
        '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
