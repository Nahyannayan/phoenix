<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTransportCollectionDetails.aspx.vb" Inherits="Fees_Reports_ASPX_rptFEEReceiptSummary" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Src="../../../UserControls/usrTransportBSUnits.ascx" TagName="usrTransportBSUnits"
    TagPrefix="uc1" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function GetFEECounter() {

            var NameandCode;
            var result;
            var type;
            if (document.getElementById('<%=h_bTransport.ClientID %>').value == 1) {
                type = 'FEE_TRN_USER';
            }
            else {
                type = 'FEE_RPT_USER';
            }
            result = radopen("../../../Common/PopupForm.aspx?multiSelect=false&ID=" + type, "pop_up")
            <%--if (result != "" && result != "undefined") {
                NameandCode = result.split('___');
                document.getElementById('<%=hfFEECounter.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtFeeCounter.ClientID %>').value = NameandCode[1];
            }
            return false;--%>
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }



        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();           
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hfFEECounter.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtFeeCounter.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtFeeCounter.ClientID%>', 'TextChanged');
            }
        }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                <asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                <br />
                <table align="center" style="width: 100%;">
                    <%--<tr class="subheader_img">
                        <td align="left" colspan="4" style="height: 19px">
                            <asp:Label ID="lblReportCaption" runat="server"></asp:Label></td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">From Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                    ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%"><span class="field-label">To Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server" Width="122px"></asp:TextBox>
                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr id="trBSUnit" runat="server">
                        <td align="left" valign="top"><span class="field-label">Business Unit</span></td>
                        <td align="left" valign="top" colspan="2"
                            style="text-align: left">
                            <div class="checkbox-list">
                                <uc1:usrTransportBSUnits ID="UsrTransportBSUnits1" runat="server" />
                            </div>
                        </td>
                        
                    </tr>
                    <tr runat="server" id="trUsrName">
                        <td align="left" valign="top"><span class="field-label">User Name</span></td>
                        <td align="left" style="text-align: left"
                            valign="top">
                            <asp:TextBox ID="txtFeeCounter" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetFEECounter(); return false;" /></td>
                    </tr>
                    <tr id="trBSUSummary" runat="server">
                        <td align="left" style="text-align: left" valign="top">
                            <asp:CheckBox ID="chkBSUSummary" runat="server" Text="Unitwise Summary" CssClass="field-label" /></td>
                        <td colspan="3"></td>
                    </tr>
                    <tr id="trGroupCurrency" runat="server">
                        <td align="left" style="text-align: left"
                            valign="top">
                            <asp:CheckBox ID="chkGroup" runat="server" Text="View in Group Currency" CssClass="field-label"></asp:CheckBox></td>
                        <td colspan="3"></td>
                    </tr>
                    <tr id="trSummary" runat="server">
                        <td align="left" style="text-align: left" valign="top">
                            <asp:CheckBox ID="chkReconciliation_Summary" runat="server" Text="Summary" CssClass="field-label"></asp:CheckBox></td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <input id="hfBSU" runat="server" type="hidden" />
                <input id="h_BSUID" runat="server" type="hidden" />
                <input id="hfFEECounter" runat="server" type="hidden" />
                <asp:HiddenField ID="h_bTransport" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>

