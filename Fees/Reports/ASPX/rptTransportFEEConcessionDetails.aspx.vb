Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptFEEConcessionDetails
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case Else
                If h_ConcessionType.Value = "" Then
                    h_ConcessionType.Value = GetAllConcessionIDs()
                End If
                GenerateFEEConcession()
        End Select
    End Sub

    Private Function GetAllConcessionIDs() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT FEES.FEE_CONCESSION_M.FCM_ID FROM FEES.FEE_CONCESSION_M"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        Dim str_ret As String = String.Empty
        While (dr.Read())
            If str_ret <> "" Then
                str_ret += "||"
            End If
            str_ret += dr(0).ToString
        End While
        Return str_ret
    End Function

    Private Sub GenerateFEEConcession()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString

        Dim cmd As New SqlCommand("FEES.[F_GETTRANSPORTCONCESSIONDETAILS]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTU_BSU_IDs As New SqlParameter("@STU_BSU_IDs", SqlDbType.Xml)
        sqlpSTU_BSU_IDs.Value = GenerateXML(UsrTransportBSUnits1.GetSelectedNode(), XMLType.BSUName)
        cmd.Parameters.Add(sqlpSTU_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpConcTypes As New SqlParameter("@ConcTypes", SqlDbType.Xml)
        sqlpConcTypes.Value = GenerateXML(h_ConcessionType.Value, XMLType.ConcessionType)
        cmd.Parameters.Add(sqlpConcTypes)


        Dim sqlpbSummary As New SqlParameter("@bSummary", SqlDbType.Bit)
        sqlpbSummary.Value = 0
        cmd.Parameters.Add(sqlpbSummary)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        params("BSU_NAME") = "GEMS"
        repSource.IncludeBSUImage = True
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptTransportFEECONCESSIONDETAILS.rpt"
         Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.ConcessionType
                elements(0) = "CONC_DETAILS"
                elements(1) = "CONC_DETAIL"
                elements(2) = "CONC_ID"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If IDs(i) <> "" Then
                    XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                    XMLEBSUID = xmlDoc.CreateElement(elements(2))
                    XMLEBSUID.InnerText = IDs(i)
                    XMLEBSUDetail.AppendChild(XMLEBSUID)
                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Function FillConcessionDetails(ByVal ConcIDs As String) As Boolean
        Dim IDs As String() = ConcIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT FEES.FEE_CONCESSION_M.FCM_DESCR, FEES.FEE_CONCESSION_M.FCM_ID " & _
        " FROM FEES.FEE_CONCESSION_M WHERE FCM_ID IN(" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        grdConc.DataSource = ds
        grdConc.DataBind()
        Return True
    End Function

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ClientScript.RegisterStartupScript(Me.GetType(), _
        "script", "<script language='javascript'>  CheckOnPostback(); </script>")
        If Not Page.IsPostBack Then
            hfBSU.Value = "display"
            hfConcession.Value = "none"
        End If
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            lblReportCaption.Text = "Transport Fee Concession Details"
            'Select Case MainMnu_code
            '        lblReportCaption.Text = "Fee Collection Details"
            'End Select
            txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            txtFromDate.Text = Format(DateTime.Now.AddMonths(-2), OASISConstants.DateFormat)
            h_BSUID.Value = Session("sBSUID")
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If

        'txtToDate.Attributes.Add("ReadOnly", "Readonly")
        'txtFromDate.Attributes.Add("ReadOnly", "Readonly")

        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
        FillConcessionDetails(h_ConcessionType.Value)
    End Sub

    Protected Sub grdConc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdConc.PageIndexChanging
        grdConc.PageIndex = e.NewPageIndex
        FillConcessionDetails(h_ConcessionType.Value)
    End Sub

    Protected Sub lnkbtnconcDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCONCID As New Label
        lblCONCID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblCONCID Is Nothing Then
            h_ConcessionType.Value = h_ConcessionType.Value.Replace(lblCONCID.Text, "").Replace("||||", "||")
            If Not FillConcessionDetails(h_ConcessionType.Value) Then
                h_ConcessionType.Value = lblCONCID.Text
            End If
            grdConc.PageIndex = grdConc.PageIndex
            FillConcessionDetails(h_ConcessionType.Value)
        End If
    End Sub

    Protected Sub txtConcession_TextChanged(sender As Object, e As EventArgs)
        txtConcession.Text = ""
        FillConcessionDetails(h_ConcessionType.Value)
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

