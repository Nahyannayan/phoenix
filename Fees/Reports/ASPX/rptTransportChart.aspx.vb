Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Transport_Reports_Aspx_rptTransportChart
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If

            Select Case MainMnu_code
                Case OASISConstants.MNU_TRAN_REP_STUDENT_COUNT_CHART
                    lblReportCaption.Text = "Students Count ( Transport )"
                    labTodate.Text = "As on Date"
                    TdFdate1.Visible = False
                    '  TdFdate2.Visible = False
                    TdFdate3.Visible = False

                Case OASISConstants.MNU_TRAN_REP_COLLECTION_SUMMARY
                    lblReportCaption.Text = "Collection Summary"
            End Select
            txtFromDate.Text = Format(Date.Now.AddMonths(-6), "dd/MMM/yyyy")
            txtToDate.Text = Format(Date.Now, "dd/MMM/yyyy")
            Page.Title = OASISConstants.Gemstitle

        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            ' lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If MainMnu_code = OASISConstants.MNU_TRAN_REP_STUDENT_COUNT_CHART Then
            StudentCount()
        Else
            If IsDate(txtFromDate.Text) And IsDate(txtToDate.Text) Then
                Dim Str_Error As String = ""

                If CDate(txtFromDate.Text) < CDate(ViewState("DTFROM")) Then
                    Str_Error = "From cannot be less that academic year start date<br />"

                End If
                If CDate(txtToDate.Text) < CDate(ViewState("DTTO")) Then
                    Str_Error = Str_Error & "To cannot be less that academic year end date<br />"
                End If
                If Str_Error <> "" Then
                    '  lblError.Text = Str_Error
                    usrMessageBar.ShowNotification(Str_Error, UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            Else
                '   lblError.Text = "Invalid Date"
                usrMessageBar.ShowNotification("Invalid Date", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            Session("RPTFromDate") = txtFromDate.Text
            Session("RPTToDate") = txtToDate.Text




            Select Case MainMnu_code
                'Case OASISConstants.MNU_TRAN_REP_STUDENT_COUNT_CHART
                ' F_GetCollectionChart("STUDENT", "../../FEES/REPORTS/RPT/rptTranStudentCountChart.rpt")
                Case OASISConstants.MNU_TRAN_REP_COLLECTION_SUMMARY
                    F_GetCollectionChart("COLLECTION", "../../FEES/REPORTS/RPT/rptTranStudentCollectionChart.rpt")
            End Select
        End If
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Private Sub StudentCount()
        '   calling StudentCountChart.rpt over here.
        Session("ReportSource") = FeeReports.StudentCountTransport(txtToDate.Text, OASISConstants.TransportBSU_BrightBus, Session("F_YEAR"))
        '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    

    Private Sub F_GetCollectionChart(ByVal p_ReportBy As String, ByVal p_ReportPath As String)
        ' [FEES].[F_GetCollectionChart](@Typ varchar(20),@DTFROm datetime,@DTTO datetime)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_GetCollectionChart]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@Typ", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = p_ReportBy
        cmd.Parameters.Add(sqlpBSU_ID)

        'Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        'sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
        'cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpFRMDT As New SqlParameter("@DTFROm", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        'params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = Session("sBsuid")
        repSource.ResourceName = p_ReportPath

        Session("ReportSource") = repSource
        ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

     

     


     

     


     


End Class
