Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_rptFeeReconcillatonWithFO
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If

            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEERECONCILIATION_WITHFO
                    lblReportCaption.Text = "Fee Reconciliation (With FO)"
            End Select

            BindBusinessUnit()
            BindAcademicYear(ddlBSUnit.SelectedValue)
           

            Page.Title = OASISConstants.Gemstitle
           
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If 
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        If IsDate(txtFromDate.Text) And IsDate(txtToDate.Text) Then
            Dim Str_Error As String = ""

            If CDate(txtFromDate.Text) < CDate(ViewState("DTFROM")) Then
                Str_Error = "From cannot be less that academic year start date<br />"

            End If
            If CDate(txtToDate.Text) < CDate(ViewState("DTTO")) Then
                Str_Error = Str_Error & "To cannot be less that academic year end date<br />" 
            End If
            If Str_Error <> "" Then
                ' lblError.Text = Str_Error
                usrMessageBar.ShowNotification(Str_Error, UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
        Else
            ' lblError.Text = "Invalid Date"
            usrMessageBar.ShowNotification("Invalid Date", UserControls_usrMessageBar.WarningType.Danger)

            Exit Sub
        End If

         

        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTFromDate") = txtFromDate.Text
        Session("RPTToDate") = txtToDate.Text
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_TRANSPORT_FEERECONCILIATION_WITHFO
                FeeChargeReconcillation()
        End Select
    End Sub


    Private Sub FeeChargeReconcillation()
        '        ALTER PROCEDURE [FEES].[rptFeeChargeReconcillation 
        '@FromDt datetime='1-SEP-2008',
        '@ToDt datetime='1-JUN-2009',
        '@BSU_ID VARCHAR(20)='125016',
        '@ACD_ID INT =80
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand("[FEES].[rptFeeChargeReconcillation]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSTU_BSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpSTU_BSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpACD_ID)  

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        'params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True 
        repSource.HeaderBSUID = ddlBSUnit.SelectedItem.Value
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptTransportFeeReconcillationWithFO.rpt"

        Session("ReportSource") = repSource
        ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
 
    Private Sub BindBusinessUnit()
         ddlBSUnit.DataBind()
    End Sub

    Private Sub BindAcademicYear(ByVal BSU_ID As Integer)  
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBSUnit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        SetAcademicYearDate()
    End Sub


    Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSUnit.SelectedIndexChanged
        BindAcademicYear(ddlBSUnit.SelectedValue)
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        setAcademicyearDate()
    End Sub


    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, ddlAcademicYear.SelectedItem.Value, ddlBSUnit.SelectedValue)
        txtFromDate.Text = DTFROM
        txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        'ViewState("DTFROM") = DTFROM
        'ViewState("DTTO") = DTTO
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class
