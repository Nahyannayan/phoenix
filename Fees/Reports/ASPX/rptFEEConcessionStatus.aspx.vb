Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class rptFEEConcessionStatus
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim MainMnu_code As String
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            If MainMnu_code = "F725145" Then
                Page.Title = OASISConstants.Gemstitle
                lblReportCaption.Text = "Fee Concession Details"
            End If
            SetAcademicYearDate()
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "F725145"
                GenerateFEEConcession()
        End Select
    End Sub


    Private Sub GenerateFEEConcession()
        Dim cmd As New SqlCommand("[FEES].[Concession_Status]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSUIDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpBSUIDs.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSUIDs)

        Dim sqlpFromDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpbGroupByBSU As New SqlParameter("@BPosted", SqlDbType.Bit)
        sqlpbGroupByBSU.Value = chkPosted.Checked
        cmd.Parameters.Add(sqlpbGroupByBSU)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        repSource.IncludeBSUImage = True
        repSource.Parameter = params
        repSource.Command = cmd
        params("ReportHead") = "CONCESSION HISTORY"
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEConcession_Status.rpt"
      
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, Session("Current_ACD_ID"), Session("sBsuid"))
        txtFromDate.Text = DTFROM
        'txtToDate.Text = DTTO
        If txtFromDate.Text = "" Then
            txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
        End If
        txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

