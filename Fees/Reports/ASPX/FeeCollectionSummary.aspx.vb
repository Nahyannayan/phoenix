Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Partial Class Fees_Reports_ASPX_FeeCollectionSummary
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            If MainMnu_code = "F726062" Then
                lblReportCaption.Text = "Fee Collection Summary as on " & txtFromDate.Text.ToString()
                txtFeeCounter.Text = Session("sUsr_name")
                hfFEECounter.Value = Session("sUsr_name")
                CheckEmployee.Visible = True
            ElseIf MainMnu_code = "F726072" Then
                lblReportCaption.Text = "Fee Collection Summary Pay At School as on " & txtFromDate.Text.ToString()
                txtFeeCounter.Text = Session("sUsr_name")
                hfFEECounter.Value = Session("sUsr_name")
                CheckEmployee.Visible = True
            ElseIf MainMnu_code = "F725140" Then
                trUser.Visible = False
                lblReportCaption.Text = "Student Balance List"
            End If
            txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select isnull(BSU_bFeesMulticurrency,0) from BUSINESSUNIT_M where BSU_ID='" & Session("sBSUID") & "'"
        Dim MultiCurrency As Boolean = Convert.ToBoolean(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
        If MultiCurrency = True Then
            Me.chkFC.Visible = True
        Else
            Me.chkFC.Visible = False
        End If
        If MainMnu_code = "F726072" Then
            Me.chkFC.Visible = False
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If MainMnu_code = "F726062" Then
            GenerateFeeCollectionDetails()
        ElseIf MainMnu_code = "F726072" Then
            GenerateFeeCollectionDetailsPayAtSchool()
        ElseIf MainMnu_code = "F725140" Then
            GetStudentBalanceList()
        End If
    End Sub

    Private Sub GenerateFeeCollectionDetails()
        Dim cmd As New SqlCommand
        If Me.chkFC.Checked = True Then
            cmd.CommandText = "[FEES].[FEE_CollectionSummary_FC]"
        Else
            cmd.CommandText = "[FEES].[FEE_CollectionSummary]"
        End If
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpUserName As New SqlParameter("@UserName", SqlDbType.VarChar)
        sqlpUserName.Value = hfFEECounter.Value
        If CheckEmployee.Checked Then
            sqlpUserName.Value = ""
            hfFEECounter.Value = ""
        End If
        cmd.Parameters.Add(sqlpUserName)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("EmpID") = hfFEECounter.Value
        params("RoundOffVal") = Session("BSU_ROUNDOFF")
        'params("FromDate") = CDate(txtFromDate.Text)
        repSource.Parameter = params
        repSource.IncludeBSUImage = False
        repSource.Command = cmd

        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(2) = New MyReportClass
        Dim cmdSubSummary As New SqlCommand
        Dim cmdSubFeetype As New SqlCommand
        Dim cmdSubPDC As New SqlCommand

        ''SUBREPORT1
        If Me.chkFC.Checked = True Then
            cmdSubFeetype.CommandText = "[FEES].[FEE_CollectionTyps_FC]"
        Else
            cmdSubFeetype.CommandText = "[FEES].[FEE_CollectionTyps]"
        End If

        cmdSubFeetype.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        cmdSubFeetype.CommandType = CommandType.StoredProcedure

        cmdSubFeetype.Parameters.AddWithValue("@BSUID", Session("sBsuid"))
        cmdSubFeetype.Parameters.AddWithValue("@FRMDT", CDate(txtFromDate.Text))
        cmdSubFeetype.Parameters.AddWithValue("@UserName", hfFEECounter.Value)
        ''SUBREPORT2
        cmdSubPDC = cmdSubFeetype.Clone
        cmdSubSummary = cmdSubFeetype.Clone
        If Me.chkFC.Checked = True Then
            cmdSubPDC.CommandText = "[FEES].[FEE_CollectionTyps_FC]"
            cmdSubPDC.CommandText = "[FEES].[FEE_CollectionTyps_PDC_FC]"
        Else
            cmdSubPDC.CommandText = "[FEES].[FEE_CollectionTyps]"
            cmdSubPDC.CommandText = "[FEES].[FEE_CollectionTyps_PDC]"
        End If


        repSourceSubRep(0).Command = cmdSubSummary
        repSourceSubRep(1).Command = cmdSubFeetype
        repSourceSubRep(2).Command = cmdSubPDC
        repSource.SubReport = repSourceSubRep
        params("ReportHead") = "Fee Collection Summary Report As On " & txtFromDate.Text.ToString()
        If chkFC.Checked = True Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeCollectionReceiptSummaryFC.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeCollectionReceiptSummary.rpt"
        End If

        txtFeeCounter.Text = Session("sUsr_name")
        hfFEECounter.Value = Session("sUsr_name")
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeCollectionDetailsPayAtSchool()
        Dim cmd As New SqlCommand
        If Me.chkFC.Checked = True Then
            cmd.CommandText = "[FEES].[FEE_CollectionSummary_FC]"
        Else
            cmd.CommandText = "[FEES].[FEE_CollectionSummary_PayAtSchool]"
        End If
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpUserName As New SqlParameter("@UserName", SqlDbType.VarChar)
        sqlpUserName.Value = hfFEECounter.Value
        If CheckEmployee.Checked Then
            sqlpUserName.Value = ""
            hfFEECounter.Value = ""
        End If
        cmd.Parameters.Add(sqlpUserName)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("EmpID") = hfFEECounter.Value
        params("RoundOffVal") = Session("BSU_ROUNDOFF")
        'params("FromDate") = CDate(txtFromDate.Text)
        repSource.Parameter = params
        repSource.IncludeBSUImage = False
        repSource.Command = cmd

        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(2) = New MyReportClass
        Dim cmdSubSummary As New SqlCommand
        Dim cmdSubFeetype As New SqlCommand
        Dim cmdSubPDC As New SqlCommand

        ''SUBREPORT1
        If Me.chkFC.Checked = True Then
            cmdSubFeetype.CommandText = "[FEES].[FEE_CollectionTyps_FC]"
        Else
            cmdSubFeetype.CommandText = "[FEES].[FEE_CollectionTyps_PayAtSchool]"
        End If

        cmdSubFeetype.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        cmdSubFeetype.CommandType = CommandType.StoredProcedure

        cmdSubFeetype.Parameters.AddWithValue("@BSUID", Session("sBsuid"))
        cmdSubFeetype.Parameters.AddWithValue("@FRMDT", CDate(txtFromDate.Text))
        cmdSubFeetype.Parameters.AddWithValue("@UserName", hfFEECounter.Value)
        ''SUBREPORT2
        cmdSubPDC = cmdSubFeetype.Clone
        cmdSubSummary = cmdSubFeetype.Clone
        If Me.chkFC.Checked = True Then
            cmdSubPDC.CommandText = "[FEES].[FEE_CollectionTyps_FC]"
            cmdSubPDC.CommandText = "[FEES].[FEE_CollectionTyps_PDC_FC]"
        Else
            cmdSubPDC.CommandText = "[FEES].[FEE_CollectionTyps_PayAtSchool]"
            cmdSubPDC.CommandText = "[FEES].[FEE_CollectionTyps_PDC_PayAtSchool]"
        End If


        repSourceSubRep(0).Command = cmdSubSummary
        repSourceSubRep(1).Command = cmdSubFeetype
        repSourceSubRep(2).Command = cmdSubPDC
        repSource.SubReport = repSourceSubRep
        params("ReportHead") = "Fee Collection Summary Pay at any School As On " & txtFromDate.Text.ToString()
        If chkFC.Checked = True Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeCollectionReceiptSummaryFC.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeCollectionReceiptSummaryPayAtSchool.rpt"
        End If

        txtFeeCounter.Text = Session("sUsr_name")
        hfFEECounter.Value = Session("sUsr_name")
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GetStudentBalanceList()
        Dim cmd As New SqlCommand("GetStudentBalanceList")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFromDT As New SqlParameter("@DT ", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        'Dim sqlpUserName As New SqlParameter("@UserName", SqlDbType.VarChar)
        'sqlpUserName.Value = hfFEECounter.Value
        'cmd.Parameters.Add(sqlpUserName)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")

        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        params("ReportHead") = "Student Balance List" ' Report As On " & txtFromDate.Text.ToString()
        params("ReportDate") = "Report As On " & txtFromDate.Text.ToString()
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeStudentBalanceList.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class



