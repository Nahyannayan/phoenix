Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Fees_Reports_ASPX_rptTransportChequeCollectionDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ClientScript.RegisterStartupScript(Me.GetType(), _
        "script", "<script language='javascript'>  CheckOnPostback(); </script>")
        If Not Page.IsPostBack Then
            hfBSU.Value = "display"
        End If
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            Select Case MainMnu_code 
                Case OASISConstants.MNU_FEE_REP_CHQ_COLL_SUMMARY
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Cheque Collection Details"
                    chkGroup.Text = "Bank wise Summary"
            End Select
            txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            txtFromDate.Text = Format(DateTime.Now.AddDays(-(DateTime.Now.Day - 1)), OASISConstants.DateFormat)
            'txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            txtFeeCounter.Attributes.Add("ReadOnly", "ReadOnly")
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            '   lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            ' lblError.Text = ""
        End If

        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        h_BSUID.Value = UsrTransportBSUnits1.GetSelectedNode
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_CHQ_COLL_SUMMARY
                GenerateFeeCollectionDetails()
        End Select
    End Sub

    Private Sub GenerateFeeCollectionDetails()
        Dim cmd As New SqlCommand("[FEES].[F_RPTCollectionData_Cheque]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@FOR_BSU_IDs", SqlDbType.Xml)
        'sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        h_BSUID.Value = UsrTransportBSUnits1.GetSelectedNode()
        sqlpFOR_BSU_IDs.Value = UtilityObj.GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
9:      cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@AsonDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        'Dim sqlpbGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        'sqlpbGroupCurrency.Value = chkGroup.Checked
        'cmd.Parameters.Add(sqlpbGroupCurrency)


        cmd.Connection = New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtToDate.Text
        params("ToDT") = ""
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Dim sqlpbLvl As New SqlParameter("@Lvl", SqlDbType.TinyInt)
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_CHQ_COLL_SUMMARY
                If rbWithPdc.Checked Then
                    sqlpbLvl.Value = 61
                ElseIf rbWoPDC.Checked Then
                    sqlpbLvl.Value = 62
                ElseIf rbOnlyPDC.Checked Then
                    sqlpbLvl.Value = 63
                End If

                params("RPT_CAPTION") = "Fee Cheque Collection Report For The Period :" & txtFromDate.Text.ToString() & " To " & txtToDate.Text.ToString()
                If chkGroup.Checked Then
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEChqPaymentDetBank.rpt"
                Else
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEChqPaidDetails.rpt"
                End If
        End Select
        cmd.Parameters.Add(sqlpbLvl)
        Session("ReportSource") = repSource
        ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub 

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

End Class

