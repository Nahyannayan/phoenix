Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Fees_Reports_ASPX_rptFEEReceiptSummary
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ClientScript.RegisterStartupScript(Me.GetType(), _
        "script", "<script language='javascript'>  CheckOnPostback(); </script>")
        If Not Page.IsPostBack Then
            hfBSU.Value = "display"
        End If
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            trBSUSummary.Visible = False
            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.MNU_FEE_REP_SUMMARY
                    'FillBSUNamesTransport("", True)
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Collection Summary Report"
                Case OASISConstants.MNU_FEE_REP_NORM_SUMMARY
                    'FillBSUNames(Session("sbsuid"), False)
                    h_bTransport.Value = 0
                    lblReportCaption.Text = "Collection Summary Report"
                Case OASISConstants.MNU_FEE_NORM_COLL_CREDIT_CRD
                    'FillBSUNames(Session("sbsuid"), False)
                    lblReportCaption.Text = "Fee Collection Credit Card"
                    h_bTransport.Value = 0
                    chkReconciliation_Summary.Enabled = False
                Case OASISConstants.MNU_NORM_FEE_SPLITUP_DETAILS
                    'FillBSUNames(Session("sbsuid"), False)
                    trUsrName.Visible = False
                    lblReportCaption.Text = "Fee Collection Details"
                    h_bTransport.Value = 0 
                Case OASISConstants.MNU_FEE_REP_COLLECTION_DET
                    lblReportCaption.Text = "Transport Fee Collection Detailed Report"
                    h_bTransport.Value = 1
                    trSummary.Visible = False
                Case OASISConstants.MNU_FEE_REP_COLLECTION_SUMMARY
                    h_bTransport.Value = 1
                    trSummary.Visible = False
                    lblReportCaption.Text = "Transport Fee Collection Summary Report"
                Case OASISConstants.MNU_FEE_REP_COLL_SUMMARY_SCH
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Collection School Summary Report"
                    trSummary.Visible = True
                    chkReconciliation_Summary.Text = "Reconciliation"
                Case OASISConstants.MNU_FEE_REP_COLL_SUMMARY_EMP
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Collection Employee Summary Report"
                    trSummary.Visible = False
                Case OASISConstants.MNU_FEE_REP_COLL_SUMMARY_EMP_SCH
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Collection School & Employee Summary Report"
                    trSummary.Visible = True
                    chkReconciliation_Summary.Text = "Reconciliation"
                Case OASISConstants.MNU_FEE_REP_CHQ_COLL_SUMMARY
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Cheque Collection Details"
                    trSummary.Visible = False
                    chkGroup.Text = "Bank wise Summary"
                Case OASISConstants.MNU_FEE_TRAN_COLL_AREAWISE
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Collection Areawise Report"
                    trSummary.Visible = False
                    trGroupCurrency.Visible = False
                Case OASISConstants.MNU_FEE_TRAN_COLL_STUDENTS
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Collection Students Report"
                    trSummary.Visible = False
                    trGroupCurrency.Visible = False
                Case OASISConstants.MNU_FEE_TRAN_COLL_CREDIT_CRD
                    lblReportCaption.Text = "Transport Fee Collection Credit Card"
                    h_bTransport.Value = 1
                    trSummary.Visible = True
                    trGroupCurrency.Visible = False
                    chkReconciliation_Summary.Text = "Summary"
                Case OASISConstants.MNU_FEE_REP_CREDIT_CARD_COLL_SUMMARY
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Transport Fee Credit Card Collection Details"
                    chkGroup.Text = "Bank wise Summary"
                    trSummary.Visible = False
                    trGroupCurrency.Visible = False
                Case OASISConstants.MNU_FEE_TRANS_COLL_VIEW_ALL
                    lblReportCaption.Text = "Transport Fee Collection (Over All)"
                    trSummary.Visible = False
                    trBSUSummary.Visible = True
                    h_bTransport.Value = 1
            End Select
            txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            txtFromDate.Text = Format(DateTime.Now.AddDays(-(DateTime.Now.Day - 1)), OASISConstants.DateFormat)
            FillUserName()
            txtFeeCounter.Attributes.Add("ReadOnly", "ReadOnly")
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            ' lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            ' lblError.Text = ""
        End If
        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
     End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        h_BSUID.Value = UsrTransportBSUnits1.GetSelectedNode
        Select Case ViewState("MainMnu_code").ToString
            Case OASISConstants.MNU_FEE_TRAN_COLL_AREAWISE
                GenerateTransportFEECollectionAreawise()
            Case OASISConstants.MNU_FEE_REP_SUMMARY
                GenerateFeeCollectionSummary()
            Case OASISConstants.MNU_FEE_TRAN_COLL_CREDIT_CRD
                GenerateFEE_Coll_CreditCard()
            Case OASISConstants.MNU_FEE_REP_COLLECTION_DET, _
            OASISConstants.MNU_FEE_REP_COLLECTION_SUMMARY, _
            OASISConstants.MNU_FEE_REP_COLL_SUMMARY_SCH, _
            OASISConstants.MNU_FEE_REP_COLL_SUMMARY_EMP, _
            OASISConstants.MNU_FEE_REP_COLL_SUMMARY_EMP_SCH, _
            OASISConstants.MNU_FEE_REP_CHQ_COLL_SUMMARY, _
            OASISConstants.MNU_FEE_TRAN_COLL_STUDENTS, _
            OASISConstants.MNU_FEE_REP_CREDIT_CARD_COLL_SUMMARY, _
              OASISConstants.MNU_FEE_TRANS_COLL_VIEW_ALL
                GenerateFeeCollectionDetails()
        End Select
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Private Sub GenerateFEE_Coll_CreditCard()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_FEE_COLL_TRAN_CREDIT_CARD]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        'sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        h_BSUID.Value = UsrTransportBSUnits1.GetSelectedNode()
        sqlpFOR_BSU_IDs.Value = UtilityObj.GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        params("RPT_CAPTION") = "Transport Fee Collection(Credit Card)"
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        If chkReconciliation_Summary.Checked Then
            repSource.ResourceName = "../../fees/reports/rpt/rptFEEColl_TRAN-CREDIT_CRD_Summary.rpt"
        Else
            repSource.ResourceName = "../../fees/reports/rpt/rptFEEColl_TRAN-CREDIT_CRD.rpt"
        End If
        Session("ReportSource") = repSource
        '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateTransportFEECollectionAreawise()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_TRAN_FEECOLL_STUD]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        'sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        h_BSUID.Value = UsrTransportBSUnits1.GetSelectedNode()
        sqlpFOR_BSU_IDs.Value = UtilityObj.GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        params("RPT_CAPTION") = "Transport Fee Collection Areawise Report"
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        repSource.ResourceName = "../../fees/reports/rpt/rptFEEColl_Transp_Area.rpt"

        Session("ReportSource") = repSource
        '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeCollectionDetails()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_RPTCollectionData]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@FOR_BSU_IDs", SqlDbType.Xml)
        'sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        h_BSUID.Value = UsrTransportBSUnits1.GetSelectedNode()
        sqlpFOR_BSU_IDs.Value = UtilityObj.GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpbGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpbGroupCurrency.Value = chkGroup.Checked
        cmd.Parameters.Add(sqlpbGroupCurrency)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Dim sqlpbLvl As New SqlParameter("@Lvl", SqlDbType.TinyInt)
        Select Case ViewState("MainMnu_code").ToString
            Case OASISConstants.MNU_FEE_REP_COLLECTION_DET
                sqlpbLvl.Value = 0
                params("RPT_CAPTION") = "Fee Collection Detailed Report"
                params("Summary") = False
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionDetails.rpt"
            Case OASISConstants.MNU_FEE_REP_COLLECTION_SUMMARY
                sqlpbLvl.Value = 1
                'params("Summary") = False
                params("RPT_CAPTION") = "Fee Collection Summary Report"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionDetSummary.rpt"
            Case OASISConstants.MNU_FEE_REP_COLL_SUMMARY_SCH
                If chkReconciliation_Summary.Checked Then
                    sqlpbLvl.Value = 5
                    params("RPT_CAPTION") = " Transport Fee Collection School Summary Report (Reconciliation)"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEColl_BSU_Summary.rpt"
                Else
                    sqlpbLvl.Value = 2
                    params("RPT_CAPTION") = "Fee Collection School Summary Report"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionDetSummary.rpt"
                End If
            Case OASISConstants.MNU_FEE_REP_COLL_SUMMARY_EMP
                sqlpbLvl.Value = 3
                params("RPT_CAPTION") = "Fee Collection Employee Summary Report"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionDetSummary.rpt"
            Case OASISConstants.MNU_FEE_REP_COLL_SUMMARY_EMP_SCH
                If chkReconciliation_Summary.Checked Then
                    sqlpbLvl.Value = 5
                    params("RPT_CAPTION") = "Fee Collection School & Employee Summary Report(Reconciliation)"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEColl_EMP_BSU_Summary.rpt"
                Else
                    sqlpbLvl.Value = 4
                    params("RPT_CAPTION") = "Fee Collection School & Employee Summary Report"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionBSUDetSummary.rpt"
                End If
            Case OASISConstants.MNU_FEE_REP_CHQ_COLL_SUMMARY
                sqlpbLvl.Value = 6
                params("RPT_CAPTION") = "Fee Cheque Collection Report"
                If chkGroup.Checked Then
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEChqPaymentDetBank.rpt"
                Else
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEChqPaidDetails.rpt"
                End If
            Case OASISConstants.MNU_FEE_REP_CREDIT_CARD_COLL_SUMMARY
                sqlpbLvl.Value = 8
                params("RPT_CAPTION") = "Fee Credit Card Collection Report"
                If chkGroup.Checked Then
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECreditCardPaidDetails.rpt"
                Else
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECreditCardPaidDetails.rpt"
                End If

            Case OASISConstants.MNU_FEE_REP_CHQ_CLEARANCE
                sqlpbLvl.Value = 6
                params("RPT_CAPTION") = "Fee Cheque Clearance Report"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEChqBankPaymentWiseReport.rpt"

            Case OASISConstants.MNU_FEE_TRAN_COLL_STUDENTS
                sqlpbLvl.Value = 5
                params("RPT_CAPTION") = "Fee Collection School & Student Summary Report(Reconciliation)"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEColl_TRANS_STU_BSU.rpt"
            Case OASISConstants.MNU_FEE_TRANS_COLL_VIEW_ALL
                If Me.chkBSUSummary.Checked Then
                    sqlpbLvl.Value = 9
                    params("RPT_CAPTION") = "UnitWise Collection Summary"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_Transp_UnitWiseCollectionSummary.rpt"
                Else
                    sqlpbLvl.Value = 5
                    params("RPT_CAPTION") = "Fee Collection Sumary Report(Reconciliation)"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEColl_TRANS_Full.rpt"
                End If

        End Select
        cmd.Parameters.Add(sqlpbLvl)
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeCollectionSummary()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_TRANSPORT_FEECOLLECTION]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        'sqlpBSU_ID.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        h_BSUID.Value = UsrTransportBSUnits1.GetSelectedNode()
        sqlpBSU_IDs.Value = UtilityObj.GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpBSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        'repSource.IncludeBSUImage = True
        If chkReconciliation_Summary.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionSummary.rpt"
        ElseIf txtFeeCounter.Text = "" Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionBSUSummary.rpt"
        Else
            params("Feecounter") = txtFeeCounter.Text
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionCounterSummary.rpt"
        End If
        Session("ReportSource") = repSource
        '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Private Sub FillUserName()
        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        'Dim cmd As New SqlCommand("[FEES].[F_RPTCollectionData]")
        'Dim str_sql As String = "select COUNT(*) from FEES.FEECOLLECTION_H WHERE FCL_EMP_ID = '" & Session("sUsr_Name") & "'"
        'Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_sql)
        'If count > 0 Then
        '    hfFEECounter.Value = Session("sUsr_Name")
        '    txtFeeCounter.Text = Session("sUsr_Name")
        'End If
    End Sub
   
End Class

