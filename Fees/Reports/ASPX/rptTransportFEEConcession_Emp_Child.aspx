<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptTransportFEEConcession_Emp_Child.aspx.vb" Inherits="rptTransportFEEConcession_Emp_Child"
    Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrTransportBSUnits.ascx" TagName="usrTransportBSUnits"
    TagPrefix="uc1" %>
<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
          <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                <br />
                <table width="100%">
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Employee
                Business Unit</span>
                        </td>
                        <td   class="matters" colspan="3" style="text-align: left"
                            valign="top">
                            <div class="checkbox-list">

                            <uc2:usrBSUnits ID="usrBSUnits1" runat="server" />
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Student
                Business Unit</span>
                        </td>
                        <td   class="matters" colspan="3" style="text-align: left"
                            valign="top">
                            <div class="checkbox-list">
                            <uc1:usrTransportBSUnits ID="UsrTransportBSUnits1" runat="server" />
                                </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span>
                        </td>
                        <td  width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" CssClass="dropDownCssIR">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Status</span>
                        </td>
                        <td  width="30%">
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="dropDownCssIR">
                                <asp:ListItem Selected="True" Value="X">ALL</asp:ListItem>
                                <asp:ListItem Value="P">PENDING</asp:ListItem>
                                <asp:ListItem Value="A">APPROVED</asp:ListItem>
                                <asp:ListItem Value="R">REJECTED</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">From Date</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputbox"  ></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                        runat="server" ControlToValidate="txtFromDate" ErrorMessage="From Date required"
                        ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                            ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                            ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
                        </td>
                        <td align="left" class="matters" colspan="1"><span class="field-label">To Date</span>
                        </td>
                        <td align="left" class="matters" colspan="1">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="inputbox"  ></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" /><asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                    runat="server" ControlToValidate="txtToDate" ErrorMessage="To Date required"
                                    ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="revToDate" runat="server" ControlToValidate="txtToDate" Display="Dynamic"
                                        EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Group By</span></td>
                        <td align="left" class="matters" colspan="3">
                            <asp:RadioButtonList ID="rblGroupby" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="NE"><span class="field-label">None</span></asp:ListItem>
                                <asp:ListItem Value="ST"><span class="field-label">Status</span></asp:ListItem>
                                <asp:ListItem Value="EU"><span class="field-label">Employee&#39;s Unit</span></asp:ListItem>
                                <asp:ListItem Value="SU"><span class="field-label">Student&#39;s Unit</span></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" class="matters" colspan="4"  >
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter SessionField="sUsr_name" Type="String" DefaultValue="" Name="USR_ID"></asp:SessionParameter>
                        <asp:SessionParameter SessionField="sBsuid" Type="String" DefaultValue="" Name="BSU_ID"></asp:SessionParameter>
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
