﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptGetFeeStructure.aspx.vb" Inherits="Fees_Reports_ASPX_rptGetFeeStructure" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Student Service Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" style="width: 100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>--%>
                             <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" cellpadding="2" cellspacing="0"
                                style="width: 100%;">
                                <%--  <tr class="subheader_img">
                        <td align="center" colspan="4" style="height: 1px" valign="middle">
                            <div align="left">
                                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                    <asp:Label ID="lblCaption" runat="server" Text="Student Service Report"></asp:Label></font></div>
                        </td>
                    </tr>--%>
                                <tr id="trBSUnit" runat="server">
                                    <td align="left" valign="top" style="width: 20%"><span class="field-label">Business Unit</span></td>

                                    <td id="Td3" runat="server" align="left" colspan="2">
                                        <div class="checkbox-list ">
                                            <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td valign="top" style="width: 20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" colspan="2"
                                        >
                                       <div class="checkbox-list ">  <asp:TreeView ID="trvAcdId" runat="server"
                                            onclick="client_OnTreeNodeChecked();" ShowCheckBoxes="all">
                                            <NodeStyle CssClass="treenode" />
                                        </asp:TreeView> </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td valign="top" style="width: 20%"><span class="field-label">Fee Type</span></td>
                                    <td align="left" colspan="2"
                                       >
                                         <div class="checkbox-list "><asp:TreeView ID="trvFeeType" runat="server"
                                            onclick="client_OnTreeNodeChecked();" ShowCheckBoxes="all">
                                            <NodeStyle CssClass="treenode" />
                                        </asp:TreeView></div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr id="trSBSUnit" runat="server">
                                    <td align="left" valign="top" style="width: 20%"><span class="field-label">Student Business Unit</span></td>

                                    <td id="Td1" runat="server" align="left" colspan="2">
                                       <div class="checkbox-list "> <uc1:usrBSUnits ID="sBsuUnit" runat="server" /></div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr id="FeeConsGrp" runat="server">
                                    <td width="20%" align="left"><span class="field-label">Group By</span>
                                    </td>
                                    <td align="left">
                                        <asp:RadioButton ID="rbWBSU" runat="server" Checked="True" GroupName="FCgrp" Text="Employee Business Unit" 
                                            CssClass="field-label" AutoPostBack="True" />

                                        <asp:RadioButton ID="rbCategory" runat="server" GroupName="FCgrp" Text="Category"
                                            CssClass="field-label" AutoPostBack="True" />

                                        <asp:RadioButton ID="rbSBSU" runat="server" GroupName="FCgrp" Text="Student Business Unit"
                                            CssClass="field-label" AutoPostBack="True" />
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                            <asp:HiddenField ID="h_acdID" runat="server" />
                            <asp:HiddenField ID="h_FeeType" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

