<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeSetupforAcademicYear.aspx.vb"
    Inherits="Fees_Reports_ASPX_FeeSetupforAcademicYear" %>

<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="0" cellspacing="0" width="65%">
                    <tr>
                        <td align="left">
                            <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                            <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError"></asp:Label>--%>
                            <asp:ValidationSummary
                                ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="5" cellspacing="0" width="100%" >
                   <%-- <tr class="subheader_img">
                        <td align="left" colspan="2" style="height: 19px">
                            <asp:Label ID="lblReportCaption" runat="server"></asp:Label></td>
                    </tr>--%>
                    <tr runat="server" id="TrBsu">
                        <td id="Td1" runat="server" align="left" width="20%" ><span class="field-label"> Unit</span></td>
                        <td id="Td3" runat="server" align="left" >
                            <div class="checkbox-list ">
                            <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" /></div>
                        </td>
                    </tr>
                    <tr runat="server" id="TrBssc">
                        <td id="Td11" runat="server" align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td id="Td33" runat="server" align="left" >
                            <div >
                                <asp:UpdatePanel ID="up2" runat="server">
                                    <ContentTemplate>
                                        <div  class="checkbox-list ">
                                            <asp:TreeView ID="trvAcdYear" runat="server" onclick="client_OnTreeNodeChecked();" ShowCheckBoxes="All">
                                            </asp:TreeView>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                    <tr runat="server" id="Tr1">
                        <td id="Td5" runat="server" align="left" width="20%"><span class="field-label">Fee Head</span></td>
                        <td id="Td6" runat="server" align="left" >
                            <div>
                                <asp:UpdatePanel ID="UP3" runat="server">
                                    <ContentTemplate>
                                        <div  class="checkbox-list ">
                                            <asp:TreeView ID="trvFeeH" runat="server" onclick="client_OnTreeNodeChecked();" ShowCheckBoxes="All">
                                            </asp:TreeView>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                    <tr runat="server" id="TrSummary">
                        <td id="Td2" runat="server" align="left" ></td>
                        <td id="Td4" runat="server" align="left" >
                            <asp:CheckBox ID="chkSummary" runat="server" Text="View Summary" />
                        </td>
                    </tr>

                    <tr>
                        <td align="center"  colspan="2">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>


