Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_rptFeeStudentFeeAging
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle

            txtFromDate.Text = Format(Date.Now, "dd/MMM/yyyy")
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_STUDENT_AGING
                    lblReportCaption.Text = "Student Fee Ageing"
            End Select
            BindBusinessUnit()
            BindAcademicYear(ddlBSUnit.SelectedValue)
            BindGrade()
            Me.chkNote.Checked = False
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTFromDate") = txtFromDate.Text
        'If UtilityObj.IsFutureDate(txtFromDate.Text) Then
        '    lblError.Text = "Invalid Date !!!"
        '    Exit Sub
        'End If
        If IsDate(txtFromDate.Text) Then
            If IsDate(txtFromDate.Text) And IsDate(ViewState("DTFROM")) Then
                If CDate(txtFromDate.Text) < CDate(ViewState("DTFROM")) Or CDate(txtFromDate.Text) > CDate(ViewState("DTTO")) Then
                    'lblError.Text = "Invalid Date"
                    usrMessageBar2.ShowNotification("Invalid Date", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            End If
        Else
            'lblError.Text = "Invalid Date (Check Academic Year atart date and end date)"
            usrMessageBar2.ShowNotification("Invalid Date (Check Academic Year atart date and end date)", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_STUDENT_AGING
                rptAGINGSummary()
        End Select
    End Sub

    Private Sub rptAGINGSummary()
        Dim cmd As New SqlCommand("FEES.rptAGINGSummary")
        cmd.CommandType = CommandType.StoredProcedure

        Dim CaptionHead As String = "Student Fee Ageing"
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBSUnit.SelectedValue
        cmd.Parameters.Add(sqlpBSU_ID)

        cmd.Parameters.AddWithValue("@Bkt1", txtBkt1.Text)
        cmd.Parameters.AddWithValue("@Bkt2", txtBkt2.Text)
        cmd.Parameters.AddWithValue("@Bkt3", txtBkt3.Text)
        cmd.Parameters.AddWithValue("@Bkt4", txtBkt4.Text)
        cmd.Parameters.AddWithValue("@Bkt5", txtBkt5.Text)
        cmd.Parameters.AddWithValue("@Bkt6", txtBkt6.Text)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpMode As New SqlParameter("@Mode", SqlDbType.VarChar, 15)
        If chkGradeSummary.Checked Then
            sqlpMode.Value = "GRADE"
        Else
            sqlpMode.Value = "STUDENT"
        End If
        cmd.Parameters.Add(sqlpMode)

        Dim sqlpFilterBy As New SqlParameter("@FilterBy", SqlDbType.VarChar, 15)
        sqlpFilterBy.Value = False
        If rbAll.Checked Then
            sqlpFilterBy.Value = "ALL"
            CaptionHead = "Student Fee Ageing (Consolidated)"
        ElseIf rbActive.Checked Then
            sqlpFilterBy.Value = "ACTIVE"
            CaptionHead = "Student Fee Ageing (Active)"
        Else
            sqlpFilterBy.Value = "INACTIVE"
            CaptionHead = "Students Fee Ageing (Inactive)"
        End If
        cmd.Parameters.Add(sqlpFilterBy)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        GetBSU_RoundOff(ddlBSUnit.SelectedValue)
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = ddlBSUnit.SelectedItem.Text
        params("FromDATE") = txtFromDate.Text
        params("BKT1") = "0 - " & txtBkt1.Text
        params("BKT2") = CInt(txtBkt1.Text) + 1 & " - " & txtBkt2.Text
        params("BKT3") = CInt(txtBkt2.Text) + 1 & " - " & txtBkt3.Text
        params("BKT4") = CInt(txtBkt3.Text) + 1 & " - " & txtBkt4.Text
        params("BKT5") = CInt(txtBkt4.Text) + 1 & " - " & txtBkt5.Text
        params("BKT6") = CInt(txtBkt5.Text) + 1 & " - " & txtBkt6.Text
        params("BKT7") = " > " & CInt(txtBkt6.Text) + 1
        params("RoundOffVal") = ViewState("RoundOffVal")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = ddlBSUnit.SelectedItem.Value
        If chkGradeSummary.Checked Then
            If rblStuCount.SelectedValue = "IC" Then
                params("RPT_CAPTION") = CaptionHead & " - With Student Count"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeAgingSummary_GradeWithCount.rpt"
            Else
                params("RPT_CAPTION") = CaptionHead
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rpfFeeAgingSummary_Grade.rpt"
            End If
        Else
            params("RPT_CAPTION") = CaptionHead
            If chkNote.Checked Then
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rpfFeeAgingSummary_StudentWithNote.rpt"
            Else
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rpfFeeAgingSummary_Student.rpt"
            End If

            If rbInactive.Checked Then
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rpfFeeAgingSummary_Student_Notonroll.rpt"
            End If
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub GetBSU_RoundOff(ByVal BSU_ID As String)
        'Author(--Swapna)
        'Date   --18/Aug/2011
        'Purpose--Get selected BSU's round off
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim query As String = ""

        query = "SELECT BSU_ROUNDOFF FROM BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID= '" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(query, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

        While reader.Read
            ViewState("RoundOffVal") = reader.Item("BSU_ROUNDOFF")
        End While

    End Sub

    Private Sub BindGrade()
        ddlGrade.DataSource = FeeCommon.GetGradesinAcademicYear(ddlAcademicYear.SelectedValue, ddlBSUnit.SelectedValue)
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
        If ddlGrade.Items.Count > 0 Then
            ddlGrade.Items.FindByValue("-1").Selected = True
        End If
    End Sub

    Private Sub BindBusinessUnit()
        ddlBSUnit.DataSource = FeeCommon.GETBSUFORUSER(Session("sUsr_name"))
        ddlBSUnit.DataTextField = "bsu_name"
        ddlBSUnit.DataValueField = "bsu_id"
        ddlBSUnit.DataBind()
        ddlBSUnit.Items.FindByValue(Session("sBsuid")).Selected = True
    End Sub

    Private Sub BindAcademicYear(ByVal BSU_ID As Integer)
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBSUnit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSUnit.SelectedIndexChanged
        BindAcademicYear(ddlBSUnit.SelectedValue)
        BindGrade()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
    End Sub

    Protected Sub chkGradeSummary_CheckedChanged(sender As Object, e As EventArgs) 'Handles chkGradeSummary.CheckedChanged
        rblStuCount.Visible = chkGradeSummary.Checked
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
