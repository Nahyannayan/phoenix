Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_ConsolidateSummary
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Me.chkDetailed.Visible = False
            TrBssc.Style("display") = "none"
            TdCharge1.Style("display") = "none"
            TdCharge2.Style("display") = "none"
            txtFromDate.Text = Format(Date.Now.AddMonths(-6), "dd/MMM/yyyy")
            Select Case MainMnu_code
                Case "F700005"
                    lblReportCaption.Text = "Fee Collection Summary (Consolidated)"
                    txtFromDate.Text = Format(Date.Now.AddDays(-1), "dd/MMM/yyyy")
                Case "F726089"
                    lblReportCaption.Text = "Fee Collection Detail"
                    TrBssc.Style("display") = ""
                    TdCharge1.Style("display") = ""
                    TdCharge2.Style("display") = ""
                    Me.chkDetailed.Visible = True
                    txtFromDate.Text = Format(Date.Now.AddMonths(-1), "dd/MMM/yyyy")
                    BindServiceCategory()
                    SelectAllTreeView(trvServices.Nodes, True)
                    For i As Integer = 0 To trvServices.Nodes.Count - 1
                        trvServices.Nodes(i).CollapseAll()
                    Next
                Case "F700020"
                    lblReportCaption.Text = "Fee Collection Summary (Services)"
                    TrBssc.Style("display") = ""
                    'TdCharge1.Style("display") = ""
                    'TdCharge2.Style("display") = ""
                    Me.chkDetailed.Visible = False

                    txtFromDate.Text = Format(Date.Now.AddMonths(-1), "dd/MMM/yyyy")
                    BindServiceCategory()
                    SelectAllTreeView(trvServices.Nodes, True)
                    For i As Integer = 0 To trvServices.Nodes.Count - 1
                        trvServices.Nodes(i).CollapseAll()
                    Next
                Case "F700006"
                    lblReportCaption.Text = "Fee Charge Summary (Consolidated)"
                Case "F700007"
                    lblReportCaption.Text = "Fee Concession Summary (Consolidated)"
                Case "F700008"
                    lblReportCaption.Text = "Fee Adjustment Summary (Consolidated)"
                Case "F700009"
                    lblReportCaption.Text = "Fee Outstanding Summary (Consolidated)"
                    TdFdate1.Style("display") = "none"
                    trStuStatus.Visible = True
                    TdFdate3.Style("display") = "none"
                Case "F700011"
                    lblReportCaption.Text = "Fee Advance Summary (Consolidated)"
                    TdFdate1.Style("display") = "none"
                    'TdFdate2.Style("display") = "none"
                    TdFdate3.Style("display") = "none"
                Case "F700013"
                    TdFdate1.Style("display") = "none"
                    TdFdate3.Style("display") = "none"
                    Me.chkDetailed.Visible = True
                    If Me.chkDetailed.Checked = True Then
                        lblReportCaption.Text = "Fee Advance Detailed(Studentwise)"
                    Else
                        lblReportCaption.Text = "Fee Advance Detailed"
                    End If
                Case "F700014"
                    Me.chkDetailed.Visible = True
                    TdFdate1.Style("display") = "none"
                    TdFdate3.Style("display") = "none"
                    If Me.chkDetailed.Checked = True Then
                        lblReportCaption.Text = "Fee Outstanding Detailed(Studentwise)"
                    Else
                        lblReportCaption.Text = "Fee Outstanding Detailed"
                    End If
                Case "F725093"
                    lblReportCaption.Text = Mainclass.GetMenuCaption(MainMnu_code)
                Case "F740203"
                    Me.BindAcademicYear()
                    lblReportCaption.Text = "Performace Invoice Generation Summary"
                    Me.TrBssc.Visible = False
                    trAcademicYear.Visible = True
                    Me.chkDetailed.Visible = True
                    Me.trSummary.Visible = False
            End Select

            txtToDate.Text = Format(Date.Now, "dd/MMM/yyyy")
            Page.Title = OASISConstants.Gemstitle
            ViewState("SSC_IDs") = ""

        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            ' lblError.Text = "No Records with specified condition"          
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            ' lblError.Text = ""           
        End If
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        Session("RPTFromDate") = txtFromDate.Text
        Session("RPTToDate") = txtToDate.Text

        Select Case MainMnu_code

            Case "F700005"
                Collection("COLLECTION")
            Case "F725006"
                Collection("COLLECTIONSUMMARY")
            Case "F700006"
                Collection("FEE MONTHLY CHARGE")
            Case "F700007"
                Collection("CONCESSION MONTHLY CHARGE")
            Case "F700008"
                Collection("FEE Adjustment")
            Case "F700009"
                Collection("OUTSTANDING")
            Case "F700011"
                Collection("ADVANCE")
            Case "F725093"
                FeeReconcilationSummary()
            Case "F700013"
                FeeAdvOrOSdetail("ADVANCE")
            Case "F700014"
                FeeAdvOrOSdetail("OUTSTANDING")
            Case "F700020"
                FeeCollectionSummaryServices()
            Case "F726089"
                Feecollectiondetail()
            Case "F740203"
                Me.GeneratePerformaInvoiceGenerationSummary()
        End Select

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Phoenixbeta/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Phoenixbeta/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Private Sub FeeCollectionSummaryServices()
        UpdateServicesSelected()
        Dim RptHead As String = ""
        RptHead = Mainclass.GetMenuCaption(MainMnu_code)
        Dim dateHead = "From " & txtFromDate.Text.ToString() & " To " & txtToDate.Text.ToString()

        Dim strConn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand
        cmd.CommandText = "[FEES].[F_ServiceFeeCollectionSummary]"
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRMDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlBSUID As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlBSUID.Value = UsrBSUnits1.GetSelectedNode().Replace("||", "@")
        cmd.Parameters.Add(sqlBSUID)

        Dim sscID As New SqlParameter("@SSC_ID", SqlDbType.VarChar)
        sscID.Value = ViewState("SSC_IDs")
        cmd.Parameters.Add(sscID)

        cmd.Connection = New SqlConnection(strConn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = RptHead
        If txtFromDate.Text.Equals(txtToDate.Text) Then
            dateHead = "As On " & txtToDate.Text.ToString()
        End If
        params("DateHead") = dateHead
        params("FromDate") = txtFromDate.Text.Trim
        params("ToDate") = txtToDate.Text.Trim
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../../FEES/REPORTS/RPT/ServiceFeeCollectionSummaryrpt.rpt"

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub FeeReconcilationSummary()

        Dim RptHead As String = ""
        RptHead = Mainclass.GetMenuCaption(MainMnu_code)
        Dim dateHead = "From " & txtFromDate.Text.ToString() & " To " & txtToDate.Text.ToString()

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.GetReconciliationSummary")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRMDT As New SqlParameter("@DTFROm", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlBSUID As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlBSUID.Value = UsrBSUnits1.GetSelectedNode()
        cmd.Parameters.Add(sqlBSUID)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = RptHead
        If txtFromDate.Text.Equals(txtToDate.Text) Then
            dateHead = "As On " & txtToDate.Text.ToString()
        End If
        params("DateHead") = dateHead

        repSource.Parameter = params
        repSource.Command = cmd
        Dim RptName = "../../FEES/REPORTS/RPT/rptReconSummary.rpt"
        repSource.ResourceName = RptName

        Session("ReportSource") = repSource
        ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Private Sub FeeAdvOrOSdetail(ByVal Type As String)

        Dim RptHead As String = ""
        RptHead = Mainclass.GetMenuCaption(MainMnu_code)
        Dim dateHead = " AsOn " & txtToDate.Text.ToString()

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.GetAdvanceOrOutstandingDetail")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpTODT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlBSUID As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlBSUID.Value = UsrBSUnits1.GetSelectedNode().Replace("||", "@")
        cmd.Parameters.Add(sqlBSUID)

        Dim sqlTyp As New SqlParameter("@Typ", SqlDbType.VarChar)
        sqlTyp.Value = Type
        cmd.Parameters.Add(sqlTyp)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = RptHead
        If txtFromDate.Text.Equals(txtToDate.Text) Then
            dateHead = "As On " & txtToDate.Text.ToString()
        End If
        params("DateHead") = dateHead

        repSource.Parameter = params
        repSource.Command = cmd
        Dim RptName = ""
        Select Case MainMnu_code
            Case "F700013"
                If Me.chkDetailed.Checked = False Then
                    RptName = "../../FEES/REPORTS/RPT/rptBuStFeeAdvOSdetail.rpt"
                Else
                    RptName = "../../FEES/REPORTS/RPT/rptFeeAdvanceOrOSdetail.rpt"
                End If
            Case "F700014"
                If Me.chkDetailed.Checked = False Then
                    RptName = "../../FEES/REPORTS/RPT/rptBuStFeeAdvOSdetail.rpt"
                Else
                    RptName = "../../FEES/REPORTS/RPT/rptFeeAdvanceOrOSdetail.rpt"
                End If
        End Select
        repSource.ResourceName = RptName

        Session("ReportSource") = repSource
        '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Private Sub Feecollectiondetail()
        UpdateServicesSelected()
        Dim RptHead As String = ""
        RptHead = Mainclass.GetMenuCaption(MainMnu_code)
        Dim dateHead = "From " & txtFromDate.Text.ToString() & " To " & txtToDate.Text.ToString()

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand
        If Me.chkCharge.Checked = True Then
            cmd.CommandText = "FEES.F_ServiceFeeCollection"
        Else
            cmd.CommandText = IIf(Me.chkDetailed.Checked, "[FEES].[F_ServiceFeeCollectionSummary]", "FEES.GetCollectionDetail")
        End If

        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRMDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlBSUID As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlBSUID.Value = UsrBSUnits1.GetSelectedNode().Replace("||", "@")
        cmd.Parameters.Add(sqlBSUID)


        Dim stuNo As New SqlParameter("@STUNO", SqlDbType.VarChar)
        stuNo.Value = ""
        If Me.chkDetailed.Checked = False Then
            cmd.Parameters.Add(stuNo) '@SSC_ID
        End If


        Dim sscID As New SqlParameter("@SSC_ID", SqlDbType.VarChar)
        sscID.Value = ViewState("SSC_IDs")
        cmd.Parameters.Add(sscID)


        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = RptHead + IIf(Me.chkDetailed.Checked, " (Summary)", "")
        If txtFromDate.Text.Equals(txtToDate.Text) Then
            dateHead = "As On " & txtToDate.Text.ToString()
        End If
        params("DateHead") = dateHead
        params("FromDate") = txtFromDate.Text.Trim
        params("ToDate") = txtToDate.Text.Trim
        repSource.Parameter = params
        repSource.Command = cmd
        Dim RptName = ""
        If Me.chkCharge.Checked = True Then
            If Me.chkDetailed.Checked = True Then
                RptName = "../../FEES/REPORTS/RPT/rptServiceFeeCollectionSummary.rpt"
            Else
                RptName = "../../FEES/REPORTS/RPT/rptServiceFeeCollection.rpt"
            End If

        Else
            If Me.chkDetailed.Checked = True Then
                RptName = "../../FEES/REPORTS/RPT/rptPhotographyFeeCollectionSummary.rpt"
            Else
                RptName = "../../FEES/REPORTS/RPT/rptFeeCollectionDetailCrosstab.rpt"
            End If

        End If

        repSource.ResourceName = RptName

        Session("ReportSource") = repSource
        '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Private Sub Collection(ByVal txtType As String)

        Dim RptHead = ""
        Dim dateHead = "From " & txtFromDate.Text.ToString() & " To " & txtToDate.Text.ToString()
        Dim RptName = "../../FEES/REPORTS/RPT/ConsolidateCollection.rpt"
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "F700005"
                RptHead = "FEE COLLECTION SUMMARY (Consolidated)"
                RptName = "../../FEES/REPORTS/RPT/ConsolidateCollectionSummary.rpt"
            Case "F725006"
                RptHead = "COLLECTION SUMMARY (Monthly)"
                RptName = "../../FEES/REPORTS/RPT/rptFEE_COLLECTION_MONTHLY.rpt"
            Case "F700006"
                RptHead = "FEE CHARGE SUMMARY (Consolidated)"
            Case "F700007"
                RptHead = "FEE CONCESSION SUMMARY (Consolidated)"
            Case "F700008"
                RptHead = "FEE ADJUSTMENT SUMMARY (Consolidated)"
            Case "F700009"
                RptHead = "FEE OUTSTANDING SUMMARY (Consolidated)"
                dateHead = "As On " & txtToDate.Text.ToString()
            Case "F700011"
                RptHead = "FEE ADVANCE SUMMARY (Consolidated)"
                dateHead = "As On " & txtToDate.Text.ToString()


        End Select

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("[FEES].[Consolidate_Summary]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@Typ", SqlDbType.VarChar, 50)
        sqlpBSU_ID.Value = txtType
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFRMDT As New SqlParameter("@DTFROm", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlBSUID As New SqlParameter("@BSUIDs", SqlDbType.VarChar)
        sqlBSUID.Value = UsrBSUnits1.GetSelectedNode().Replace("||", "@")
        cmd.Parameters.Add(sqlBSUID)

        If MainMnu_code = "F700009" Then
            Dim sqlSTUSTATUS As New SqlParameter("@STU_STATUS", SqlDbType.VarChar, 2)
            sqlSTUSTATUS.Value = rblStuStatus.SelectedValue
            cmd.Parameters.Add(sqlSTUSTATUS)
        End If


        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = RptHead
        If txtFromDate.Text.Equals(txtToDate.Text) Then
            dateHead = "As On " & txtToDate.Text.ToString()
        End If
        params("DateHead") = dateHead

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = RptName
        repSource.IncludeBSUImage = True
        Session("ReportSource") = repSource
        '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GeneratePerformaInvoiceGenerationSummary()

        Dim RptHead = "Performa Invoice Generation Summary"
        Dim dateHead = "From " & txtFromDate.Text.ToString() & " To " & txtToDate.Text.ToString()
        Dim RptName = "../../FEES/REPORTS/RPT/rptPerformaInvoiceGenerationSummary.rpt"
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        'Select Case MainMnu_code
        '    Case "F700005"
        '        RptHead = "FEE COLLECTION SUMMARY (Consolidated)"
        '        RptName = "../../FEES/REPORTS/RPT/ConsolidateCollectionSummary.rpt"
        '    Case "F725006"
        '        RptHead = "COLLECTION SUMMARY (Monthly)"
        '        RptName = "../../FEES/REPORTS/RPT/rptFEE_COLLECTION_MONTHLY.rpt"
        '    Case "F700006"
        '        RptHead = "FEE CHARGE SUMMARY (Consolidated)"
        '    Case "F700007"
        '        RptHead = "FEE CONCESSION SUMMARY (Consolidated)"
        '    Case "F700008"
        '        RptHead = "FEE ADJUSTMENT SUMMARY (Consolidated)"
        '    Case "F700009"
        '        RptHead = "FEE OUTSTANDING SUMMARY (Consolidated)"
        '        dateHead = "As On " & txtToDate.Text.ToString()
        '    Case "F700011"
        '        RptHead = "FEE ADVANCE SUMMARY (Consolidated)"
        '        dateHead = "As On " & txtToDate.Text.ToString()
        'End Select

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("[FEES].[PERFORMA_INVOICE_GENERATION_SUMMARY]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlBSUID As New SqlParameter("@BSU_IDS", SqlDbType.VarChar)
        sqlBSUID.Value = UsrBSUnits1.GetSelectedNode().Replace("||", "|")
        cmd.Parameters.Add(sqlBSUID)

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpAcYear As New SqlParameter("@ACY_ID", SqlDbType.Int)
        sqlpAcYear.Value = Me.ddlAcademicYear.SelectedValue
        cmd.Parameters.Add(sqlpAcYear)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("Rpt_Caption") = RptHead
        params("FromDt") = Me.txtFromDate.Text
        params("ToDt") = Me.txtToDate.Text
        
        repSource.Parameter = params
        repSource.Command = cmd
        'repSource.IncludeBSUImage = True
        'repSource.HeaderBSUID = Session("sBsuid")
        repSource.ResourceName = RptName

        Session("ReportSource") = repSource
        '    Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub UpdateServicesSelected()
        Try
            Dim SSC_IDs As String = ""
            For Each node As TreeNode In trvServices.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If SSC_IDs.Trim() = "" Then
                    SSC_IDs = node.Value
                Else
                    SSC_IDs = SSC_IDs + "@" + node.Value
                End If
            Next
            ViewState("SSC_IDs") = SSC_IDs
        Catch
            ViewState("SSC_IDs") = ""
        End Try
    End Sub
    Private Sub SelectAllTreeView(ByVal nodes As TreeNodeCollection, ByVal selAll As Boolean)
        For Each node As TreeNode In nodes
            node.Checked = selAll
            If node.ChildNodes.Count > 0 Then
                SelectAllTreeView(node.ChildNodes, selAll)
            End If
        Next
    End Sub
    Public Sub BindServiceCategory()
        trvServices.Nodes.Clear()
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = " exec FEES.GetServiceFeeList 'P' "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("SSC_DESC").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("SSC_ID").ToString()
                trvServices.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In trvServices.Nodes
            BindChildNodes(node)
        Next

        trvServices.ExpandAll()


    End Sub

    Public Sub BindAcademicYear()
        Dim ds As DataSet
        'Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT A.ACY_ID, A.ACY_DESCR FROM dbo.ACADEMICYEAR_M A WHERE A.ACY_bSHOW = 1"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Me.ddlAcademicYear.DataValueField = "ACY_ID"
            Me.ddlAcademicYear.DataTextField = "ACY_DESCR"
            Me.ddlAcademicYear.DataSource = ds.Tables(0)
            Me.ddlAcademicYear.DataBind()
        End If

    End Sub
    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim childnodeValue = ParentNode.Value
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = " exec FEES.GetServiceFeeList '" & childnodeValue & "' "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("ServiceDesc").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("SSC_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If


        'For Each node As TreeNode In ParentNode.ChildNodes
        '    BindChildNodes(node)
        'Next


    End Sub
End Class
