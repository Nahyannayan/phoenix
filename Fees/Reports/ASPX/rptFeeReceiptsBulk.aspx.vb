Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports CrystalDecisions.Shared

Partial Class Fees_Reports_ASPX_rptFeeReceiptsBulk
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String
    Shared reportHeader As String
    Private Property bBSUTaxable() As Boolean
        Get
            Return ViewState("bBSUTaxable")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bBSUTaxable") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            hfBSU.Value = "display"
        End If
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnDownloadReceipts)
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            bBSUTaxable = DirectCast(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_bFEE_TAXABLE,0) FROM dbo.BUSINESSUNIT_SUB WHERE BUS_BSU_ID='" & Session("sBsuid") & "'"), Boolean)
            Select Case MainMnu_code
                Case "F720017"
                    lblReportCaption.Text = "Fee Receipts - Bulk"
                    h_bTransport.Value = 0
                    btnDownloadReceipts.Visible = True
                    h_bTransport.Value = 0
            End Select
            SetAcademicYearDate()
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
    End Sub
    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, Session("Current_ACD_ID"), Session("sBsuid"))
        txtFromDate.Text = DTFROM
        'txtToDate.Text = DTTO
        txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        If txtFromDate.Text = "" Then
            txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        End If
    End Sub
    Protected Sub btnDownloadReceipts_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDownloadReceipts.Click
        If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "F720017"
                If DateDiff(DateInterval.Day, CDate(txtFromDate.Text), CDate(txtToDate.Text)) > 31 Then
                    'lblError.Text = "Date Interval cannot be greater than one month."
                    usrMessageBar2.ShowNotification("Date Interval cannot be greater than one month.", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
                GenerateBulkReceipts("DOWNLOAD", IIf(rbOnline.Checked, "INTERNET", "COUNTER"), Session("sBsuid"), False, 1)
        End Select
    End Sub
    Public Function GenerateBulkReceipts(ByVal Action As String, ByVal Source As String, ByVal BSUID As String, ByVal ForceEmail As Boolean, ByVal Counter As Int16) As String
        GenerateBulkReceipts = ""
        Try

            Dim str_Sql, strFilter As String
            strFilter = " FCL_BSU_ID='" & BSUID & _
                "' AND FCL_SOURCE='" & Source & "' AND FCL_DATE BETWEEN '" & CDate(txtFromDate.Text).ToString("dd/MMM/yyyy") & "' AND '" & CDate(txtToDate.Text).ToString("dd/MMM/yyyy") & "'"
            str_Sql = " SELECT * FROM [FEES].[VW_OSO_FEES_EMAIL_FEERECEIPT] WHERE " & strFilter
            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.CommandType = Data.CommandType.Text
            ' check whether Data Exits
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim ds As New DataSet
            Dim repSource As New MyReportClass
            SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
            repSource.ReportUniqueName = BSUID & "_" & Source
            If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
                cmd.Connection = New SqlConnection(str_conn)

                Dim params As New Hashtable
                params("UserName") = "SYSTEM"
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.IncludeBSUImage = False
                '''''''''''''''
                Dim repSourceSubRep(2) As MyReportClass
                repSourceSubRep(0) = New MyReportClass
                repSourceSubRep(1) = New MyReportClass
                repSourceSubRep(2) = New MyReportClass
                Dim cmdHeader As New SqlCommand("getBsuInFoWithImage", New SqlConnection(ConnectionManger.GetOASISConnectionString))
                cmdHeader.CommandType = CommandType.StoredProcedure
                cmdHeader.Parameters.AddWithValue("@IMG_BSU_ID", BSUID)
                cmdHeader.Parameters.AddWithValue("@IMG_TYPE", "LOGO")
                repSourceSubRep(1).Command = cmdHeader

                Dim cmdSubEarn As New SqlCommand
                cmdSubEarn.CommandText = "FEES.GetReceiptPrint_Online_Bulk"
                cmdSubEarn.CommandType = CommandType.StoredProcedure
                cmdSubEarn.Parameters.AddWithValue("@FCL_BSU_ID", BSUID)
                cmdSubEarn.Parameters.AddWithValue("@FromDate", CDate(txtFromDate.Text).ToString("dd/MMM/yyyy"))
                cmdSubEarn.Parameters.AddWithValue("@ToDate", CDate(txtToDate.Text).ToString("dd/MMM/yyyy"))
                cmdSubEarn.Parameters.AddWithValue("@Source", Source)
                cmdSubEarn.Parameters.AddWithValue("@Selection", 0)

                cmdSubEarn.Connection = New SqlConnection(str_conn)
                repSourceSubRep(0).Command = cmdSubEarn

                Dim cmdSubPayment As New SqlCommand
                cmdSubPayment.CommandText = "FEES.GetReceiptPrint_Online_Bulk"
                cmdSubPayment.CommandType = CommandType.StoredProcedure
                cmdSubPayment.Parameters.AddWithValue("@FCL_BSU_ID", BSUID)
                cmdSubPayment.Parameters.AddWithValue("@FromDate", CDate(txtFromDate.Text).ToString("dd/MMM/yyyy"))
                cmdSubPayment.Parameters.AddWithValue("@ToDate", CDate(txtToDate.Text).ToString("dd/MMM/yyyy"))
                cmdSubPayment.Parameters.AddWithValue("@Source", Source)
                cmdSubPayment.Parameters.AddWithValue("@Selection", 1)
                cmdSubPayment.Connection = New SqlConnection(str_conn)
                repSourceSubRep(2).Command = cmdSubPayment
                repSource.SubReport = repSourceSubRep

                'repSource.ResourceName = "~/Reports/RPT/rptFeeReceipt.rpt"
                Dim bBSUTaxable As Boolean

                bBSUTaxable = DirectCast(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_bFEE_TAXABLE,0) FROM dbo.BUSINESSUNIT_SUB WHERE BUS_BSU_ID='" & BSUID & "'"), Boolean)
                If bBSUTaxable Then
                    repSource.ResourceName = HttpContext.Current.Server.MapPath("~/Fees/Reports/RPT/rptFeeReceipt_Tax_Bulk.rpt")
                Else
                    repSource.ResourceName = HttpContext.Current.Server.MapPath("~/Fees/Reports/RPT/rptFeeReceipt_Bulk.rpt")
                End If
            End If
            If Not repSource Is Nothing Then
                Dim repClassVal As New RepClass

                repClassVal = New RepClass
                repClassVal.ResourceName = repSource.ResourceName
                repClassVal.SetDataSource(ds.Tables(0))
                If repSource.IncludeBSUImage Then
                    If repSource.HeaderBSUID Is Nothing Then
                        IncludeBSUmage(repClassVal, BSUID)
                    Else
                        IncludeBSUmage(repClassVal, repSource.HeaderBSUID)
                    End If

                End If

                SubReport(repSource, repSource, repClassVal)

                Dim pdfFilePath As String
                Dim pdfFileName As String
                pdfFileName = "OASIS_Fee_Receipt-" & IIf(Source = "INTERNET", "ONLINE", Source) & "_" & CDate(txtToDate.Text).ToString("ddMMMyyyy") & "-" & CDate(txtToDate.Text).ToString("ddMMMyyyy") & "_" & Counter.ToString & ".pdf"

                pdfFilePath = WebConfigurationManager.AppSettings.Item("OnlineFeeReceipt")
                ' pdfFilePath = "C:\0Junk\"
                pdfFilePath += pdfFileName

                repClassVal.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                Dim ienum As IDictionaryEnumerator = repSource.Parameter.GetEnumerator()
                While ienum.MoveNext()
                    repClassVal.SetParameterValue(ienum.Key, ienum.Value)
                End While
                Try
                    If System.IO.File.Exists(pdfFilePath) Then
                        System.IO.File.Delete(pdfFilePath)
                    End If
                Catch ex As Exception

                End Try

                repClassVal.ExportToDisk(ExportFormatType.PortableDocFormat, pdfFilePath)
                If Action.ToUpper = "DOWNLOAD" Or Action.ToUpper = "" Then
                    Dim BytesData As Byte()
                    BytesData = ConvertFiletoBytes(pdfFilePath)
                    Dim ContentType, Extension As String
                    ContentType = "application/pdf"
                    Extension = GetFileExtension(ContentType)
                    Dim Title As String = "Receipt"
                    If repSource.ReportUniqueName.ToString.Split("_").Length > 1 Then
                        Title = repSource.ReportUniqueName.ToString.Split("_")(1)
                    End If
                    DownloadFile(System.Web.HttpContext.Current, BytesData, ContentType, Extension, Title, pdfFileName)
                    repClassVal.Close()
                    repClassVal.Dispose()
                    repSource = Nothing
                    repClassVal = Nothing
                End If
            End If
        Catch ex As Exception
            Throw ex
            '  GenerateBulkReceipts = ex.Message
        End Try
    End Function

    Public Shared Function ConvertFiletoBytes(ByVal FilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(FilePath) = True Then
            Throw New ArgumentNullException("File Name Cannot be Null or Empty", "FilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(FilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Shared Sub DownloadFile(ByVal context As HttpContext, ByVal BytesData As Byte(), ByVal contentType As String, ByVal Extension As String, ByVal Title As String, ByVal FileName As String)
        context.Response.Clear()
        context.Response.AddHeader("Content-Length", BytesData.Length.ToString())
        context.Response.ContentType = contentType
        context.Response.AddHeader("Expires", "0")
        context.Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
        context.Response.AddHeader("Pragma", "public")
        context.Response.AddHeader("Content-Disposition", "attachment; filename=" & FileName)
        context.Response.BinaryWrite(BytesData)
        context.Response.Flush()
        context.Response.End()
    End Sub
    Private Shared Function GetFileExtension(ByVal ContentType As String) As String
        ' Dim filePath As String = fuUpload.PostedFile.FileName
        Select Case ContentType
            Case "application/vnd.ms-word"
                GetFileExtension = ".doc"
                Exit Select
            Case "application/vnd.ms-word"
                GetFileExtension = ".docx"
                Exit Select
            Case "application/vnd.ms-excel"
                GetFileExtension = ".xls"
                Exit Select
            Case "image/jpg"
                GetFileExtension = ".jpg"
                Exit Select
            Case "image/png"
                GetFileExtension = ".png"
                Exit Select
            Case "image/gif"
                GetFileExtension = ".gif"
                Exit Select
            Case "application/pdf"
                GetFileExtension = ".pdf"
                Exit Select
        End Select
    End Function
    Private Shared Sub SubReport(ByVal repSource As MyReportClass, ByVal subRep As MyReportClass, ByRef repClassVal As RepClass)
        If subRep.SubReport IsNot Nothing Then
            Dim ii As Integer = 0
            If subRep.IncludeBSUImage Then
                ii = 1
            End If
            For i As Integer = 0 To subRep.SubReport.Length - 1
                Dim myrep As MyReportClass = subRep.SubReport(i)
                If myrep IsNot Nothing Then
                    If myrep.SubReport IsNot Nothing Then
                        SubReport(repSource, subRep, repClassVal)
                    Else
                        Dim objConn As SqlConnection = repSource.Command.Connection
                        'objConn.Close()
                        objConn.Open()
                        Dim ds As New DataSet
                        Dim adpt As New SqlDataAdapter
                        adpt.SelectCommand = myrep.Command
                        adpt.Fill(ds)
                        objConn.Close()
                        If String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Transport.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Portrait.rpt", True) <> 0 Then
                            repClassVal.Subreports(i + ii).SetDataSource(ds.Tables(0))
                        Else
                            reportHeader = repClassVal.Subreports(i + ii).Name
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Private Shared Sub IncludeBSUmage(ByRef repClassVal As RepClass, ByVal BSUID As String)
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(lstrConn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("getBsuInFoWithImage", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpIMG_BSU_ID As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 100)
        If BSUID = "" Then
            sqlpIMG_BSU_ID.Value = BSUID
        Else
            sqlpIMG_BSU_ID.Value = BSUID
        End If

        cmd.Parameters.Add(sqlpIMG_BSU_ID)

        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmd.Parameters.Add(sqlpIMG_TYPE)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        objConn.Close()
        If Not repClassVal.Subreports("rptHeader.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Transport.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Transport.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Portrait.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Portrait.rpt").SetDataSource(ds.Tables(0))
        End If
        'repClassVal.Subreports("bsuimage").SetDataSource(dtDt)

        'repClassVal.Subreports("BSUIMAGE").SetDataSource(dtDt)
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

