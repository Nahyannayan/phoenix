Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Fees_Reports_ASPX_rptFEECreditCardDaily
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ClientScript.RegisterStartupScript(Me.GetType(), _
        "script", "<script language='javascript'>  CheckOnPostback(); </script>")
        If Not Page.IsPostBack Then
            hfBSU.Value = "display"
        End If
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_REP_CREDITCARDDAILY
                    FillBSUNames(Session("sbsuid"), False)
                    trSelBSU.Visible = False
                    trBSUnit.Visible = True
                    trUsrName.Visible = True
                    lblReportCaption.Text = "Credit Card Daily Details"
                    h_bTransport.Value = 0
            End Select
            'txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            'txtFromDate.Text = Format(DateTime.Now.AddDays(-(DateTime.Now.Day - 1)), OASISConstants.DateFormat)
            SetAcademicYearDate()
            'FillUserName()
            FillBSUNames(True)
            txtFeeCounter.Attributes.Add("ReadOnly", "ReadOnly")
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If

        'txtToDate.Attributes.Add("ReadOnly", "Readonly")
        'txtFromDate.Attributes.Add("ReadOnly", "Readonly")

        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
        If IsPostBack Then
            FillBSUNames()
        End If
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_CREDITCARDDAILY
                h_BSUID.Value = UsrBSUnits1.GetSelectedNode
                GenerateCreditCardDailyDetails()
        End Select
    End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNamesTransport(h_BSUID.Value) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            'grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNamesTransport(h_BSUID.Value)
        End If
    End Sub

    Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_SUMMARY, _
                OASISConstants.MNU_FEE_TRAN_COLL_CREDIT_CRD
                FillBSUNamesTransport("", True)
            Case OASISConstants.MNU_FEE_REP_NORM_SUMMARY
            Case OASISConstants.MNU_FEE_NORM_COLL_CREDIT_CRD
            Case OASISConstants.MNU_NORM_FEE_SPLITUP_DETAILS
                FillBSUNames(h_BSUID.Value, True)
        End Select
    End Sub

    Private Function FillBSUNamesTransport(ByVal BSUIDs As String, Optional ByVal bgetAll As Boolean = False) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim conn As New SqlConnection(str_conn)
        Dim ds As New DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        If bgetAll Then
            condition = ""
        Else
            condition = " SVB_BSU_ID in(" & condition & ")"
        End If
        Try
            Dim cmd As New SqlCommand
            cmd.CommandText = "[GETUNITSFORTRANSPORTFEES]"
            cmd.Parameters.AddWithValue("@Usr_Name", Session("sUsr_Name"))
            cmd.Parameters.AddWithValue("@PROVIDER_BSU_ID", Session("sBsuid"))
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = cmd
            adpt.Fill(ds)
            If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
                Return False
            End If
            Dim dv As New DataView(ds.Tables(0))
            dv.RowFilter = condition
            If bgetAll Then
                h_BSUID.Value = ""
                For i As Integer = 0 To dv.Table.Rows.Count - 1
                    h_BSUID.Value += dv.Item(i)("SVB_BSU_ID") & "||"
                Next
            End If
            Return True
        Catch
            Return False
        Finally
            conn.Close()
        End Try
    End Function

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, Session("Current_ACD_ID"), Session("sBsuid"))
        txtFromDate.Text = DTFROM
        'txtToDate.Text = DTTO
        If txtFromDate.Text = "" Then
            txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
        End If
        txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)

    End Sub

    Private Sub FillBSUNames(Optional ByVal bGetAll As Boolean = False)
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_SUMMARY, _
                OASISConstants.MNU_FEE_TRAN_COLL_CREDIT_CRD
                FillBSUNamesTransport("", bGetAll)
            Case OASISConstants.MNU_FEE_REP_NORM_SUMMARY, _
            OASISConstants.MNU_FEE_NORM_COLL_CREDIT_CRD, _
            OASISConstants.MNU_NORM_FEE_SPLITUP_DETAILS
                If bGetAll Then h_BSUID.Value = Session("sBSUID")
                FillBSUNames(h_BSUID.Value, True)
        End Select
    End Sub

    Private Function FillBSUNames(ByVal BSUIDs As String, ByVal bGetMulti As Boolean) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        If bGetMulti Then
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
        Else
            condition = "'" & BSUIDs & "'"
        End If
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID SVB_BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID SVB_BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function


    Private Sub GenerateCreditCardDailyDetails()
        Dim cmd As New SqlCommand("[FEES].[F_GET_FEE_CREDIT_CARD_DAILY]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.VarChar)
        sqlpFOR_BSU_IDs.Value = h_BSUID.Value.Replace("||", "|")
        'sqlpFOR_BSU_IDs.Value = UsrBSUnits1.GetSelectedNode("|")
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpSource As New SqlParameter("@FCL_SOURCE", SqlDbType.VarChar)
        If ddSource.SelectedItem IsNot Nothing Then
            sqlpSource.Value = ddSource.SelectedValue
        Else
            sqlpSource.Value = "All"
        End If
        cmd.Parameters.Add(sqlpSource)
        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        params("RPT_CAPTION") = "Daily Report of Credit Card"
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        'If chkSummary.Checked Then
        'repSource.ResourceName = "../../fees/reports/rpt/rptFEEColl_TRAN-CREDIT_CRD_Summary.rpt"
        'Else
        repSource.ResourceName = "../../fees/reports/rpt/rptFEE_NORM__CREDITCARDDaily.rpt"
        'End If

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

