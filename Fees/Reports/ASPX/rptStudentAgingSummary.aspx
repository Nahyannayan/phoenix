<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudentAgingSummary.aspx.vb" Inherits="Fees_Reports_ASPX_rptStudentAgingSummary" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                        <td align="left" colspan="3">
                            <div class="checkbox-list-full">
                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server"></uc1:usrBSUnits>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">As On</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                    ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                        ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student Status</span></td>
                        <td align="left" width="30%">
                            <span class="field-label">
                                <asp:RadioButtonList ID="rblStuStatus" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True">ALL</asp:ListItem>
                                    <asp:ListItem>ACTIVE</asp:ListItem>
                                    <asp:ListItem>INACTIVE</asp:ListItem>
                                </asp:RadioButtonList></span>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr runat="server" id="trSummary">
                        <td align="left" width="20%"></td>
                        <td align="left" colspan="3">
                            <asp:CheckBox ID="chkGradeSummary" runat="server" Text="Summary By Grade" Checked="True" CssClass="field-label" />
                            <asp:CheckBox ID="chkBSUSummary" runat="server" AutoPostBack="True" OnCheckedChanged="chkBSUSummary_CheckedChanged"
                                Text="Summary By Business Unit" CssClass="field-label"></asp:CheckBox></td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td align="left" width="20%"><span class="field-label">Ageing Buckets</span></td>

                        <td align="left" colspan="3">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="13%">
                                        <asp:TextBox ID="txtBkt1" runat="server" MaxLength="3">30</asp:TextBox>
                                    </td>
                                    <td align="left" width="13%">
                                        <asp:TextBox ID="txtBkt2" runat="server" MaxLength="3">60</asp:TextBox>
                                    </td>
                                    <td align="left" width="13%">
                                        <asp:TextBox ID="txtBkt3" runat="server" MaxLength="3">90</asp:TextBox>
                                    </td>
                                    <td align="left" width="13%">
                                        <asp:TextBox ID="txtBkt4" runat="server" MaxLength="3">120</asp:TextBox>
                                    </td>
                                    <td align="left" width="14%">
                                        <asp:TextBox ID="txtBkt5" runat="server" MaxLength="3">150</asp:TextBox>
                                    </td>
                                    <td align="left" width="14%">
                                        <asp:TextBox ID="txtBkt6" runat="server" MaxLength="3">180</asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>
