<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="TransdocnoDetails.aspx.vb" Inherits="Fees_Reports_ASPX_TransdocnoDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function GetFEECounter() {
           
            var NameandCode;
            var result;
            var FrmDt = document.getElementById('<%=txtFromDate.ClientID%>').value
            var ToDt = document.getElementById('<%=txtTodate.ClientID%>').value

            var type = 'TRANDOCNO';
            var BsuId = document.getElementById('<%=ddlBSUnit.ClientID%>').value

            result = radopen("../../../Common/PopupForm.aspx?multiSelect=false&ID=" + type + "&bsu=" + BsuId + "&FRMDT=" + FrmDt + "&TODT=" + ToDt, "pop_up")
           <%-- if (result != "" && result != "undefined") {
                NameandCode = result.split('___');
                document.getElementById('<%=hfFEECounter.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtFeeCounter.ClientID %>').value = NameandCode[1];
            }
            return false;--%>
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }



 function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode .split('||');
               document.getElementById('<%=hfFEECounter.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtFeeCounter.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtFeeCounter.ClientID%>', 'TextChanged');
            }
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                <asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                <br />
                <table align="center" cellpadding="4" cellspacing="0" style="width: 100%;">
                    <%-- <tr class="subheader_img">
            <td align="left" colspan="4" style="height: 19px">
                <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
            </td>
        </tr>--%>
                    <tr runat="server">
                        <td width="20%" align="left"><span class="field-label">Business Unit</span>
                        </td>
                        <td  width="30%" align="left" style="text-align: left" >
                            <asp:DropDownList ID="ddlBSUnit" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="left"  width="20%"><span class="field-label">Date</span>
                        </td>
                        <td align="left"  width="30%" style="text-align: left" id="tdToDate1" runat="server">
                            <asp:TextBox ID="txtFromDate" runat="server"  ></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                    runat="server" ControlToValidate="txtFromDate" ErrorMessage="From Date required"
                                    ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                        ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g. 21/Sep/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
                        </td>
                        <td align="left"  width="20%" style="text-align: left" id="TdToDate2" runat="server"><span class="field-label">To Date</span>
                        </td>
                        <td id="tdToDate3"  width="30%" runat="server">
                            <asp:TextBox ID="txtTodate" runat="server"  >
                            </asp:TextBox>
                            <asp:ImageButton ID="ImgTo" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" /><asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                    runat="server" ControlToValidate="txtTodate" Display="Dynamic" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g. 21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtTodate"
                                Display="Dynamic" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g. 21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr runat="server" visible="false" id="TrFeeSource">
                        <td align="left"  width="20%">
                            <asp:Label ID="lblCombotext" runat="server" Text="Fee Source" CssClass="field-label"> </asp:Label>
                        </td>
                        <td align="left"  width="30%"  style="text-align: left">
                            <asp:DropDownList ID="ddlFeeSource" runat="server">
                                <asp:ListItem Selected="True">FEE MONTHLY CHARGE</asp:ListItem>
                                <asp:ListItem>FEE Adjustment</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlSmsfilter" runat="server" Visible="False">
                                <asp:ListItem>All</asp:ListItem>
                                <asp:ListItem>Success</asp:ListItem>
                                <asp:ListItem>Failed</asp:ListItem>
                                <asp:ListItem>Pending</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                         <td colspan="2"></td>
                    </tr>
                    <tr runat="server" id="trSerachDate" visible="false">
                        <td align="left"  width="20%"><span class="field-label">Search Date</span>
                        </td>
                        <td align="left"  width="30%" style="text-align: left">
                            <asp:RadioButton ID="RdSOD" runat="server" Checked="True" GroupName="Rd" Text="StrikeOf Date"></asp:RadioButton>
                            <asp:RadioButton ID="RdLDA" runat="server" GroupName="Rd" Text="Last Date Of Attendence"></asp:RadioButton>
                        </td>
                         <td colspan="2"></td>
                    </tr>
                    <tr runat="server" id="trFilterDate" visible="false">
                        <td align="left"></td>
                        <td align="left"  width="30%" style="text-align: left">
                            <asp:RadioButton ID="RdbChargeDT" runat="server" Checked="True" GroupName="RDF" Text="Filter By Charge Date"></asp:RadioButton>
                            <asp:RadioButton ID="RdbRRDate" runat="server" GroupName="RDF" Text="Filter by Revenue Recognized date"></asp:RadioButton>
                        </td>
                         <td colspan="2"></td>
                    </tr>
                    <tr id="trUser" runat="server">
                        <td align="left"  width="20%"><span class="field-label">DocNo</span>
                        </td>
                        <td align="left"  width="30%"  style="text-align: left">
                            <asp:TextBox ID="txtFeeCounter" runat="server" ></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetFEECounter(); return false;" />
                        </td>
                         <td colspan="2"></td>
                    </tr>
                    <tr id="trEmpBSUWise" runat="server" visible="false">
                        <td align="left"  width="20%" >
                            <asp:CheckBox ID="chkEmpBSUWise" runat="server" CssClass="radiobutton" Text="View Employee Working Business Unit Wise"/>
                        </td>
                         <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalTodate" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="ImgTo" TargetControlID="txtTodate">
                </ajaxToolkit:CalendarExtender>
                <input id="hfFEECounter" runat="server" type="hidden" />
            </div>
        </div>
    </div>
</asp:Content>
