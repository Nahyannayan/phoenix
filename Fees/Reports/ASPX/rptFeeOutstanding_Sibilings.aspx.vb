Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Fees_Reports_ASPX_rptFeeOutstanding_Sibilings
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            lblReportCaption.Text = "Outstanding Details - Siblings"
            InitializePage()
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If

    End Sub

    Sub InitializePage()
        ViewState("FEE_IDs") = ""
        txtFromDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        BindBusinessUnit()
        ddBusinessunit.Items.FindByValue(Session("sBsuid")).Selected = True
        BindFeeTypes()
        'SelectAllTreeView(trvFeeTypes.Nodes, True)
        For i As Integer = 0 To trvFeeTypes.Nodes.Count - 1
            trvFeeTypes.Nodes(i).CollapseAll()
        Next
    End Sub

    Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID as SVB_BSU_ID, BSU_NAME from [fn_GetBusinessUnits] ('" & Session("sUsr_name") & "') order by BSU_NAME")
        ddBusinessunit.DataSource = ds.Tables(0)
        ddBusinessunit.DataBind()
    End Sub

    Public Sub GetBSU_RoundOff(ByVal BSU_ID As String)
        'Author(--Swapna)
        'Date   --18/Aug/2011
        'Purpose--Get selected BSU's round off
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim query As String = ""

        query = "SELECT  BSU_ROUNDOFF FROM BUSINESSUNIT_M WHERE BSU_ID = '" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(query, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

        While reader.Read
            ViewState("RoundOffVal") = reader.Item("BSU_ROUNDOFF")
        End While

    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        GetOutstandingDetails_Sibilings()
    End Sub


    Private Sub GetOutstandingDetails_Sibilings()
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("[FEES].[FEE_OUTSTANDING_SIBLINGS]")
        UpdateFeesSelected()
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddBusinessunit.SelectedItem.Value
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFromDT As New SqlParameter("@ASONDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpFEE_IDs As New SqlParameter("@FEE_IDs", SqlDbType.VarChar)
        If ViewState("MainMnu_code") = "F725190" Then
            sqlpFEE_IDs.Value = "" 'ViewState("FEE_IDs")
        Else
            sqlpFEE_IDs.Value = "5" 'ViewState("FEE_IDs")
        End If
        cmd.Parameters.Add(sqlpFEE_IDs)

        Dim sqlpFILTERby As New SqlParameter("@FILTERBY", SqlDbType.VarChar)
        sqlpFILTERby.Value = rblFilter.SelectedValue 'ViewState("FEE_IDs")
        cmd.Parameters.Add(sqlpFILTERby)

        cmd.Connection = New SqlConnection(str_conn)

        Dim repSource As New MyReportClass
        GetBSU_RoundOff(ddBusinessunit.SelectedValue)
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("ASON") = txtFromDate.Text
        params("RPT_CAPTION") = lblReportCaption.Text
        params("FeeHeads") = "TUITION FEES" 'ViewState("FEE_DESCRs")

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = ddBusinessunit.SelectedItem.Value
        If ViewState("MainMnu_code") = "F725190" Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptOutstanding_Siblings_LDA.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptOutstanding_Siblings.rpt"
        End If

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Public Sub BindFeeTypes()
        trvFeeTypes.Nodes.Clear()
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty ' SSC_ID hard coded as 4(Others)
        str_Sql = " SELECT DISTINCT ISNULL(SSC_ID,4) SSC_ID,LTRIM(RTRIM(ISNULL(SC.SSC_DESC,'Others'))) SSC_DESC " & _
                  "FROM FEES.FEES_M FM LEFT JOIN OASIS.dbo.SERVICES_SYS_M SYM ON FM.FEE_SVC_ID=SYM.SVC_ID " & _
                  "LEFT JOIN OASIS.dbo.SERVICES_CATEGORY SC ON SYM.SVC_SSC_ID=SC.SSC_ID ORDER BY SSC_ID,SSC_DESC"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("SSC_DESC").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("SSC_ID").ToString()
                trvFeeTypes.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In trvFeeTypes.Nodes
            BindChildNodes(node)
        Next
        trvFeeTypes.ExpandAll()
    End Sub
    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim childnodeValue = ParentNode.Value
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = " SELECT DISTINCT ISNULL(FEE_ID, 0) FEE_ID,LTRIM(RTRIM(ISNULL(FEE_DESCR,''))) FEE_DESCR " & _
                  "FROM FEES.FEES_M FM LEFT JOIN OASIS.dbo.SERVICES_SYS_M SYM ON FM.FEE_SVC_ID=SYM.SVC_ID " & _
                  "LEFT JOIN OASIS.dbo.SERVICES_CATEGORY SC ON SYM.SVC_SSC_ID=SC.SSC_ID WHERE ISNULL(SSC_ID,4)='" & childnodeValue & "' " & _
                  "ORDER BY ISNULL(FEE_ID, 0),LTRIM(RTRIM(ISNULL(FEE_DESCR,''))) "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("FEE_DESCR").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("FEE_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If
        'For Each node As TreeNode In ParentNode.ChildNodes
        '    BindChildNodes(node)
        'Next
    End Sub

    Private Sub UpdateFeesSelected()
        Try
            Dim FEE_IDs As String = ""
            Dim FEE_DESCRs As String = ""
            For Each node As TreeNode In trvFeeTypes.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If FEE_IDs.Trim() = "" Then
                    FEE_IDs = node.Value
                Else
                    FEE_IDs = FEE_IDs + "|" + node.Value
                End If
                If FEE_DESCRs.Trim() = "" Then
                    FEE_DESCRs = node.Text
                Else
                    FEE_DESCRs = FEE_DESCRs + " | " + node.Text
                End If
            Next
            ViewState("FEE_IDs") = FEE_IDs
            ViewState("FEE_DESCRs") = FEE_DESCRs
        Catch
            ViewState("FEE_IDs") = ""
            ViewState("FEE_DESCRs") = ""
        End Try
    End Sub
    Private Sub SelectAllTreeView(ByVal nodes As TreeNodeCollection, ByVal selAll As Boolean)
        For Each node As TreeNode In nodes
            node.Checked = selAll
            If node.ChildNodes.Count > 0 Then
                SelectAllTreeView(node.ChildNodes, selAll)
            End If
        Next
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

