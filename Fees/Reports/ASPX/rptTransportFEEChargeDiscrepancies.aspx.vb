
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Fees_Reports_ASPX_rptTransportFEEChargeDiscrepancies
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim blnGroup As Boolean
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTFromDate") = txtFromDate.Text
        Session("RPTToDate") = txtToDate.Text
        If radClassWise.Checked Then
            blnGroup = False
        Else
            blnGroup = True
        End If
        Select Case MainMnu_code
            Case "F730332"
                GenerateTransportChargeDiscrepancies(blnGroup)

        End Select
    End Sub

   
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If

            Select Case MainMnu_code
                'Case "F730330"
                Case "F730332"
                    lblReportCaption.Text = "Transport Fee Charge Discrepancies"

            End Select

            BindBusinessUnit()
            
            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If bNoData Then
                txtFromDate.Text = Session("RPTFromDate")
                txtToDate.Text = Session("RPTToDate")
            Else
                txtFromDate.Text = Format(DateTime.Now.AddDays(-DateTime.Now.Day + 1), OASISConstants.DateFormat)
                txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            End If
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
    End Sub


    Private Sub BindBusinessUnit()

        UsrTransportBSUnits1.GetSelectedNode()
    End Sub

    
    Private Sub GenerateTransportChargeDiscrepancies(ByVal bBuswiseSummary As Boolean)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim cmd As New SqlCommand
        If bBuswiseSummary Then
            cmd = New SqlCommand("[FEES].[F_GETTRANSPORTCHARGEDISCREPANCIES_BUSWISE]")
        Else
            cmd = New SqlCommand("[FEES].[F_GETTRANSPORTCHARGEDISCREPANCIES]")

        End If
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_IDs", SqlDbType.Xml)
        'sqlpSTU_BSU_ID.Value = ddlBSUnit.SelectedValue
        sqlpSTU_BSU_ID.Value = UtilityObj.GenerateXML(UsrTransportBSUnits1.GetSelectedNode(), XMLType.BSUName)
        cmd.Parameters.Add(sqlpSTU_BSU_ID)

       

        Dim sqlpSERVICE_ID As New SqlParameter("@SERVICE_ID", SqlDbType.Int)
        sqlpSERVICE_ID.Value = 1
        cmd.Parameters.Add(sqlpSERVICE_ID)

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpSTU_TYP As New SqlParameter("@STU_TYP", SqlDbType.VarChar, 2)
        If radEnq.Checked Then
            sqlpSTU_TYP.Value = "E"
        ElseIf radStud.Checked Then
            sqlpSTU_TYP.Value = "S"
        End If
        cmd.Parameters.Add(sqlpSTU_TYP)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        'params("BSU_NAME") = ddlBSUnit.SelectedItem.Text

        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        params("RPT_CAPTION") = "CHARGE DISCREPANCIES"
        If bBuswiseSummary Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_Transp_ChargeDiscrepancies_BUSWISE.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_Transp_ChargeDiscrepancies.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
