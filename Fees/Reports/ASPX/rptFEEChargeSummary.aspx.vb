Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class rptFEEChargeSummary
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            UsrBSUnits1.MenuCode = MainMnu_code
            Select Case MainMnu_code
                Case "F725137"
                    lblReportCaption.Text = "Consolidated Fee Charge Report"
            End Select
            txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
            FillFeeType()
            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMnu_code
            Case "F725137"
                GenerateFeeChargeReport()
                'Case "F725099"
                '    OtherFeeChargeDetails()
        End Select
    End Sub
    Private Function GenerateBSUXML(ByVal BSUIDs As String) As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Try
            BSUDetails = xmlDoc.CreateElement("BSU_DETAILS")
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement("BSU_DETAIL")
                XMLEBSUID = xmlDoc.CreateElement("BSU_ID")
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    Private Sub GenerateFeeChargeReport()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim cmd As New SqlCommand
            If chkSummary.Checked Then
                cmd.CommandText = "[FEES].[RPT_CONSOLIDATEDFEECHARGE_SUMMARY]"
            Else
                cmd.CommandText = "[FEES].[RPT_CONSOLIDATEDFEECHARGE]"
            End If
            cmd.CommandType = CommandType.StoredProcedure
            Dim strXMLBSUNames As String
            'Generate the XML in the form <BSU_DETAILS><BSU_ID>IDhere</BSU_ID></BSU_DETAILS>
            h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
            strXMLBSUNames = GenerateBSUXML(h_BSUID.Value) 'txtBSUNames.Text)
            Dim sqlpBSU_ID As New SqlParameter("@BSUIDs", SqlDbType.VarChar)
            sqlpBSU_ID.Value = h_BSUID.Value
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
            sqlpFromDT.Value = CDate(txtFromDate.Text)
            cmd.Parameters.Add(sqlpFromDT)

            Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
            sqlpTODT.Value = CDate(txtToDate.Text)
            cmd.Parameters.Add(sqlpTODT)

            Dim sqlpFEE As New SqlParameter("@FEEID", SqlDbType.Int)
            sqlpFEE.Value = ddFee.SelectedValue
            cmd.Parameters.Add(sqlpFEE)

            Dim sqlpFSH_SOURCE As New SqlParameter("@FSH_SOURCE", SqlDbType.VarChar)
            sqlpFSH_SOURCE.Value = ddFeeType.SelectedItem.Value
            cmd.Parameters.Add(sqlpFSH_SOURCE)

            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDT") = txtFromDate.Text
            params("ToDT") = txtToDate.Text
            params("RPT_CAPTION") = "Consolidated Fee Charge Report"
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            If chkSummary.Checked Then
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptConsolidatedFeeChargeSummary.rpt"
            Else
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptConsolidatedFeeCharge.rpt"
            End If
            Session("ReportSource") = repSource
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExporttoexcel.Click
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub
    Sub FillFeeType()
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "[FEES].[FEE_GETALLFEESFORCOLLECTION]")
        If Not dsData Is Nothing Then
            Dim dtFeeType As DataTable = dsData.Tables(0)
            ddFee.DataSource = dtFeeType
            ddFee.DataTextField = "FEE_DESCR"
            ddFee.DataValueField = "FEE_ID"
            ddFee.DataBind()
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
