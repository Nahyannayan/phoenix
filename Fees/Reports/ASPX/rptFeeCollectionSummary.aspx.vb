﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_rptFeeCollectionSummary
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ClientScript.RegisterStartupScript(Me.GetType(), _
        "script", "<script language='javascript'>  CheckOnPostback(); </script>")
        If Not Page.IsPostBack Then
            hfBSU.Value = "display"
        End If
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Dim MainMnu_code As String
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            Select Case MainMnu_code
                Case "F715075"
                    lblReportCaption.Text = "Transport Fee Collection Summary For a Day"
                    chkSummary.Visible = False
                    TrUsrName.Visible = False
                    trChkConciliation.Visible = False
                    h_bTransport.Value = 0
            End Select

            txtFeeCounter.Attributes.Add("ReadOnly", "ReadOnly")
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If bNoData Then
                txtFromDate.Text = Session("RPTFromDate")
            Else
                txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            End If

        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            '  lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If

      
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Session("RPTFromDate") = txtFromDate.Text
        If UtilityObj.IsFutureDate(txtFromDate.Text) Then
            ' lblError.Text = "Invalid Date !!!"
            usrMessageBar.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "F715075"
                FEE_CollectionSummary()
        End Select
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub FEE_CollectionSummary()
        Dim cmd As New SqlCommand("[TRANSPORT].[FEE_CollectionSummary]")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddlBusinessunit.SelectedItem.Value
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpUserName As New SqlParameter("@UserName", SqlDbType.VarChar)
        sqlpUserName.Value = hfFEECounter.Value
        If CheckEmployee.Checked Then
            sqlpUserName.Value = ""
            hfFEECounter.Value = ""
        End If
        cmd.Parameters.Add(sqlpUserName)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("EmpID") = hfFEECounter.Value
        params("RoundOffVal") = Session("BSU_ROUNDOFF")
        'params("FromDate") = CDate(txtFromDate.Text)
        repSource.Parameter = params
        repSource.IncludeBSUImage = False
        repSource.Command = cmd

        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(2) = New MyReportClass
        Dim cmdSubSummary As New SqlCommand
        Dim cmdSubFeetype As New SqlCommand
        Dim cmdSubPDC As New SqlCommand

        ''SUBREPORT1
        cmdSubFeetype.CommandText = "[TRANSPORT].[FEE_CollectionTyps]"
        cmdSubFeetype.Connection = New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        cmdSubFeetype.CommandType = CommandType.StoredProcedure

        cmdSubFeetype.Parameters.AddWithValue("@BSUID", ddlBusinessunit.SelectedItem.Value)
        cmdSubFeetype.Parameters.AddWithValue("@FRMDT", CDate(txtFromDate.Text))
        cmdSubFeetype.Parameters.AddWithValue("@UserName", hfFEECounter.Value)
        ''SUBREPORT2
        cmdSubPDC = cmdSubFeetype.Clone
        cmdSubSummary = cmdSubFeetype.Clone
        cmdSubPDC.CommandText = "[TRANSPORT].[FEE_CollectionTyps]"
        cmdSubPDC.CommandText = "[TRANSPORT].[FEE_CollectionTyps_PDC]"
        repSourceSubRep(0).Command = cmdSubSummary
        repSourceSubRep(1).Command = cmdSubFeetype
        repSourceSubRep(2).Command = cmdSubPDC
        repSource.SubReport = repSourceSubRep
        params("ReportHead") = txtFromDate.Text.ToString()
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeCollectionReceiptSummary_Transport.rpt"
        Session("ReportSource") = repSource
        ' Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

End Class

