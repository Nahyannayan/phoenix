﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_rptGetFeeStructure
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = ViewState("MainMnu_code")
            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F733400" And ViewState("MainMnu_code") <> "F704013") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim lnk As LinkButton = TryCast(UsrBSUnits1.FindControl("lnkQuickList"), LinkButton)
                lnk.Visible = False

                Dim lnk1 As LinkButton = TryCast(sBsuUnit.FindControl("lnkQuickList"), LinkButton)
                lnk1.Visible = False

                If ViewState("MainMnu_code") = "F733400" Then
                    trSBSUnit.Visible = False
                    FeeConsGrp.Visible = False
                    lblCaption.Text = "GEMS Fee Structure"
                Else
                    lblCaption.Text = "Fee concession report staff wise "

                End If
                BindActivityTypes()
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        End If
    End Sub
    Public Sub BindActivityTypes()
        trvFeeType.Nodes.Clear()
        trvAcdId.Nodes.Clear()

        PopulateRootLevel()

        trvFeeType.DataBind()
        trvFeeType.CollapseAll()

        trvAcdId.DataBind()
        trvAcdId.CollapseAll()
    End Sub
    Private Sub PopulateRootLevel()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
        Dim str_conn1 As String = WebConfigurationManager.ConnectionStrings("MainDBO").ConnectionString

        Dim str_Sql As String
        Dim str_Sql1 As String

        trvFeeType.Nodes.Clear()
        trvAcdId.Nodes.Clear()

        str_Sql = "select 0 as FEE_ID,'All' as FEE_DESCRIPTION,count(*) childnodecount from  (SELECT  DISTINCT FEE_ID , FEE_DESCR  AS FEE_DESCRIPTION  FROM FEES.FEES_M  )a"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), trvFeeType.Nodes)


        str_Sql1 = "select 0 as ACY_ID  ,'All' as ACY_DESCRIPTION ,count(*) childnodecount from  (SELECT  DISTINCT ACY_ID , ACY_DESCR  AS ACY_DESCRIPTION  FROM ACADEMICYEAR_M  )a"
        Dim ds1 As New DataSet
        ds1 = SqlHelper.ExecuteDataset(str_conn1, CommandType.Text, str_Sql1)
        PopulateACDIdNodes(ds1.Tables(0), trvAcdId.Nodes)

    End Sub
    Private Sub PopulateACDIdNodes(ByVal dt As DataTable, _
            ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("ACY_DESCRIPTION").ToString()
            tn.Value = dr("ACY_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            'If tn.Value = Session("sBsuid") Then
            '    tn.Checked = True
            'End If
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, _
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("FEE_DESCRIPTION").ToString()
            tn.Value = dr("FEE_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            'If tn.Value = Session("sBsuid") Then
            '    tn.Checked = True
            'End If
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub
    Private Sub PopulateTree() 'Generate Tree
        PopulateRootLevel()
        trvFeeType.DataBind()
        trvFeeType.CollapseAll()
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        UpdateFeeSelected()
        UpdateACYSelected()

        If ViewState("MainMnu_code") = "F733400" Then
            GenerateFeeSetupDetails()
        Else
            GenerateFeeConcessionReportStaffWise()
        End If
    End Sub
    Private Sub GenerateFeeSetupDetails()
        Dim cmd As New SqlCommand("[FEES].[F_GETFEESTURUCTUREREPORT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlBSUID As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlBSUID.Value = UsrBSUnits1.GetSelectedNode().Replace("||", "|")
        cmd.Parameters.Add(sqlBSUID)

        Dim sqlpACD_ID As New SqlParameter("@ACY_ID", SqlDbType.VarChar)
        sqlpACD_ID.Value = ViewState("ACY_IDs")
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpFEE_ID As New SqlParameter("@FEE_ID", SqlDbType.VarChar)
        sqlpFEE_ID.Value = ViewState("FEE_IDs")
        cmd.Parameters.Add(sqlpFEE_ID)


        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        params("RPT_CAPTION") = " GEMS FEE STRUCTURE"
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_STRUCTURE.rpt"

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub GenerateFeeConcessionReportStaffWise()
        Dim cmd As New SqlCommand("[FEES].[GETFEECONCESSIONSTAFFWISEREPORT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlBSUID As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlBSUID.Value = UsrBSUnits1.GetSelectedNode().Replace("||", "|")
        cmd.Parameters.Add(sqlBSUID)

        Dim sqlpACD_ID As New SqlParameter("@ACY_ID", SqlDbType.VarChar)
        sqlpACD_ID.Value = ViewState("ACY_IDs")
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpFEE_ID As New SqlParameter("@FEE_ID", SqlDbType.VarChar)
        sqlpFEE_ID.Value = ViewState("FEE_IDs")
        'sqlpFEE_ID.Value = "1|2|145"
        cmd.Parameters.Add(sqlpFEE_ID)

        Dim sqlsBSUID As New SqlParameter("@SBSU_IDs", SqlDbType.VarChar)
        sqlsBSUID.Value = sBsuUnit.GetSelectedNode().Replace("||", "|")
        cmd.Parameters.Add(sqlsBSUID)


        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True

        If rbWBSU.Checked = True Then
            params("RPT_CAPTION") = "Fee Concession Report Staff Wise (Group By Employee Business Unit)"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeConcessionReportStaffWise_BSU.rpt"
        ElseIf rbCategory.Checked = True Then
            params("RPT_CAPTION") = "Fee Concession Report Staff Wise (Group By Category)"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeConcessionReportStaffWise_CATEGORY.rpt"
        Else
            params("RPT_CAPTION") = "Fee Concession Report Staff Wise (Group By Student Business Unit)"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeConcessionReportStaffWise_SBSU.rpt"
        End If



        Session("ReportSource") = repSource
        '   Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub



    Private Sub UpdateFeeSelected()
        Try
            Dim FEE_IDs As String = ""
            For Each node As TreeNode In trvFeeType.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If FEE_IDs.Trim() = "" Then
                    FEE_IDs = node.Value
                Else
                    FEE_IDs = FEE_IDs + "|" + node.Value
                End If
            Next
            ViewState("FEE_IDs") = FEE_IDs
        Catch
            ViewState("FEE_IDs") = ""
        End Try
    End Sub
    Private Sub UpdateACYSelected()
        Try
            Dim ACY_IDs As String = ""
            For Each node As TreeNode In trvAcdId.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If ACY_IDs.Trim() = "" Then
                    ACY_IDs = node.Value
                Else
                    ACY_IDs = ACY_IDs + "|" + node.Value
                End If
            Next
            ViewState("ACY_IDs") = ACY_IDs
        Catch
            ViewState("ACY_IDs") = ""
        End Try
    End Sub

    Protected Sub trvFeeType_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles trvFeeType.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateSubLevel(str, e.Node)
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As String, _
        ByVal parentNode As TreeNode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
        Dim ds As New DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[FEES].[GET_FEETYPE_WITH_CHECKALL]")
        PopulateNodes(ds.Tables(0), parentNode.ChildNodes)
    End Sub

    Protected Sub trvAcdId_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles trvAcdId.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateAcdIdSubLevel(str, e.Node)
    End Sub
    Private Sub PopulateAcdIdSubLevel(ByVal parentid As String, _
        ByVal parentNode As TreeNode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
        Dim ds As New DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[FEES].[GET_ACDID_WITH_CHECKALL]")
        PopulateACDIdNodes(ds.Tables(0), parentNode.ChildNodes)
    End Sub

End Class


