<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptFEEConcessionDetails.aspx.vb" Inherits="rptFEEConcessionDetails" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function ToggleSearchConcession() {
            if (document.getElementById('<%=hfConcession.ClientID %>').value == 'display') {
                document.getElementById('<%=trConcession.ClientID %>').style.display = '';
                document.getElementById('<%=trSelConcession.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgMinusConcession.ClientID %>').style.display = '';
                document.getElementById('<%=imgPlusConcession.ClientID %>').style.display = 'none';
                document.getElementById('<%=hfConcession.ClientID %>').value = 'none';
            }
            else {
                document.getElementById('<%=trConcession.ClientID %>').style.display = 'none';
                document.getElementById('<%=trSelConcession.ClientID %>').style.display = '';
                document.getElementById('<%=imgMinusConcession.ClientID %>').style.display = 'none';
                document.getElementById('<%=imgPlusConcession.ClientID %>').style.display = '';
                document.getElementById('<%=hfConcession.ClientID %>').value = 'display';
            }
            return false;
        }

        function HideAll() {
            document.getElementById('<%=trConcession.ClientID %>').style.display = 'none';
        }
        function SearchHide() {
            document.getElementById('trConcession').style.display = 'none';
        }

        function GetConcessionType() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = "popup.aspx?ID=CONC";
            //result = window.showModalDialog("popup.aspx?ID=CONC", "", sFeatures)
            var oWnd = radopen(url, "pop_fee");
            <%--if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_ConcessionType.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else
            {
                return false;
            }--%>
     }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(arg.NameandCode);
                document.getElementById('<%=h_ConcessionType.ClientID %>').value = NameandCode;
                __doPostBack('<%= h_ConcessionType.ClientID%>', 'TextChanged');
            }
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_fee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <%-- <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                <asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                <br />
                <table align="center" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">From Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                        ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%"><span class="field-label">To Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr id="trBSUnit" runat="server">
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <div class="checkbox-list-full">
                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr id="trSelConcession" runat="server">
                        <td align="left" colspan="4">
                            <asp:ImageButton ID="imgPlusConcession" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchConcession();return false;" CausesValidation="False" />
                            <span class="field-label">Select Concession Filter</span></td>
                    </tr>
                    <tr id="trConcession" runat="server">
                        <td align="left" width="20%">
                            <asp:ImageButton ID="imgMinusConcession" runat="server" Style="display: none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchConcession();return false;" CausesValidation="False" /><span class="field-label">Concession Type</span></td>

                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtConcession" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgGetConcession" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetConcessionType(); return false;" /><br />
                            <asp:GridView ID="grdConc" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row" AllowPaging="True" PageSize="5">
                                <Columns>
                                    <asp:TemplateField HeaderText="Conc. ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBSUID" runat="server" Text='<%# Bind("FCM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FCM_DESCR" HeaderText="Conc. Type" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtnConcDelete" runat="server" OnClick="lnkbtnconcDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trDetails" runat="server">
                        <td align="left" colspan="4">
                            <asp:RadioButton ID="radDetailed" runat="server" Checked="True" GroupName="REPTYPE"
                                Text="Detailed" CssClass="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="radcatDetails" runat="server" GroupName="REPTYPE" Text="Categorised Details" CssClass="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="RdContype" runat="server" GroupName="REPTYPE" Text="Concession Typewise" CssClass="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="radCatSummary" runat="server" GroupName="REPTYPE" Text="Categorised Summary" CssClass="field-label"></asp:RadioButton></td>
                    </tr>
                    <tr id="trGroupby" runat="server">
                        <td align="left" colspan="4">
                            <asp:CheckBox ID="chkGroupByBSU" runat="server" Text="Group By Business Unit" CssClass="field-label"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                &nbsp;
    <input id="hfConcession" runat="server" type="hidden" />&nbsp;
    <input id="h_ConcessionType" runat="server" type="hidden" />
            </div>
        </div>
    </div>

</asp:Content>

