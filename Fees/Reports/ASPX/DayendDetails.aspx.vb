Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_Reports_ASPX_DayendDetails
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainObj As New Mainclass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            'ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBSuid")
            Dim USR_NAME As String = Session("sUsr_name")
            InitialiseCompnents()
        End If
    End Sub

    Sub bindData(ByVal Mode As Boolean)
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ASONDT", SqlDbType.VarChar)
        pParms(0).Value = txtFrom.Text
        pParms(1) = New SqlClient.SqlParameter("@MODE", SqlDbType.Bit)
        pParms(1).Value = Mode
        Dim _table As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "[FEES].[Dayend_Status]", pParms)
        If Mode = True Then
            gvDayendDetails.DataSource = Nothing
            gvDayendDetails.DataBind()
            gvDayendDetails.DataSource = _table.Tables(0)
            gvDayendDetails.DataBind() 
        Else
            gvNextstatus.DataSource = Nothing
            gvNextstatus.DataBind()
            gvNextstatus.DataSource = _table.Tables(0)
            gvNextstatus.DataBind() 
        End If
    End Sub

    Sub InitialiseCompnents()
        'gvDayendDetails.Attributes.Add("bordercolor", "#1b80b6")
        '   gvNextstatus.Attributes.Add("bordercolor", "#1b80b6")
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        tr1.Visible = True
        tr2.Visible = True
        bindData(True)
        bindData(False)
    End Sub

End Class
