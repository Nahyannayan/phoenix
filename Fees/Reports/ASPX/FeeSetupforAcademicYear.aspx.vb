Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_FeeSetupforAcademicYear
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            BINDACADEMICYEAR()
            BindFees()
            'SelectAllTreeView(trvAcdYear.Nodes, True)
            CollapseTreeView(Me.trvAcdYear)
            CollapseTreeView(Me.trvFeeH)
            Select Case MainMnu_code
                Case "F700018"
                    lblReportCaption.Text = "Fee Setup Detail"
                Case "F725093"
                    lblReportCaption.Text = Mainclass.GetMenuCaption(MainMnu_code)
            End Select

            Page.Title = OASISConstants.Gemstitle
        End If
        ViewState("ACDY_IDs") = ""
        ViewState("FEE_IDs") = ""
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            '   lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            '  lblError.Text = ""
        End If
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        Select Case MainMnu_code
            Case "F700018"
                FeeSetupForACDY()
        End Select

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub FeeSetupForACDY()
        ViewState("ACDY_IDs") = GetSelectedNodes(Me.trvAcdYear)
        ViewState("FEE_IDs") = GetSelectedNodes(Me.trvFeeH)
        Dim RptHead As String = ""
        RptHead = Mainclass.GetMenuCaption(MainMnu_code)
        Dim dateHead = ""

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.GetFeeSetupforAcademicYears")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpACY As New SqlParameter("@ACY_ID", SqlDbType.VarChar)
        sqlpACY.Value = ViewState("ACDY_IDs")
        cmd.Parameters.Add(sqlpACY)

        Dim sqlpSumry As New SqlParameter("@Summary", SqlDbType.Bit)
        sqlpSumry.Value = Convert.ToByte(Me.chkSummary.Checked)
        cmd.Parameters.Add(sqlpSumry)

        Dim sqlBSUID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlBSUID.Value = UsrBSUnits1.GetSelectedNode("@")
        cmd.Parameters.Add(sqlBSUID)

        Dim sqlFEEID As New SqlParameter("@FEE_ID", SqlDbType.VarChar)
        sqlFEEID.Value = ViewState("FEE_IDs")
        cmd.Parameters.Add(sqlFEEID)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = RptHead
        params("DateHead") = dateHead

        repSource.Parameter = params
        repSource.Command = cmd
        Dim RptName = "../../FEES/REPORTS/RPT/FeeSetupCrossTab.rpt"
        repSource.ResourceName = RptName

        Session("ReportSource") = repSource
        '  Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub SelectAllTreeView(ByVal nodes As TreeNodeCollection, ByVal selAll As Boolean)
        For Each node As TreeNode In nodes
            node.Checked = selAll
            If node.ChildNodes.Count > 0 Then
                SelectAllTreeView(node.ChildNodes, selAll)
            End If
        Next
    End Sub
    Private Sub CollapseTreeView(ByVal Tree As TreeView)
        For i As Integer = 0 To Tree.Nodes.Count - 1
            Tree.Nodes(i).CollapseAll()
        Next
    End Sub
    Public Sub RootNode(ByVal Tree As TreeView)
        Tree.Nodes.Clear()
        Dim tn As New TreeNode()
        tn.Text = "All"
        tn.Value = "0"
        tn.Target = "_self"
        tn.NavigateUrl = "javascript:void(0)"
        Tree.Nodes.Add(tn)
    End Sub
    Public Sub BINDACADEMICYEAR()
        RootNode(Me.trvAcdYear)
        For Each node As TreeNode In trvAcdYear.Nodes
            BindChildNodes(node, "ACDY")
        Next
    End Sub
    Public Sub BindFees()
        RootNode(Me.trvFeeH)
        For Each node As TreeNode In trvFeeH.Nodes
            BindChildNodes(node, "Fee")
        Next
    End Sub
    Private Function GetSelectedNodes(ByVal Tree As TreeView) As String
        Try
            Dim ACDY_IDs As String = ""
            For Each node As TreeNode In Tree.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If ACDY_IDs.Trim() = "" Then
                    ACDY_IDs = node.Value
                Else
                    ACDY_IDs = ACDY_IDs + "@" + node.Value
                End If
            Next
            GetSelectedNodes = ACDY_IDs
        Catch
            GetSelectedNodes = ""
        End Try
    End Function
    Public Sub BindChildNodes(ByVal ParentNode As TreeNode, ByVal Tree As String)

        Dim childnodeValue = ParentNode.Value
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty
        Dim NText, NValue As String
        Dim ACFY As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "SELECT ACD_ACY_ID FROM ACADEMICYEAR_D where ACD_ID='" & FeeCommon.GetCurrentAcademicYear(Session("sBsuid")) & "'")
        If Tree = "Fee" Then
            str_Sql = " SELECT DISTINCT FEE_ID,FEE_DESCR,FEE_ORDER FROM FEES.FEES_M A INNER JOIN FEES.FEESETUP_S B ON B.FSP_FEE_ID=A.FEE_ID ORDER BY FEE_ORDER "
            NText = "FEE_DESCR"
            NValue = "FEE_ID"
        Else
            str_Sql = " select ACY_ID,ACY_DESCR from ACADEMICYEAR_M WHERE ACY_ID IN (SELECT distinct ACD_ACY_ID from ACADEMICYEAR_D B INNER JOIN FEES.FEESETUP_S C ON B.ACD_ID=C.FSP_ACD_ID) order by ACY_ID desc "
            NText = "ACY_DESCR"
            NValue = "ACY_ID"
        End If

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item(NText).ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item(NValue).ToString()
                If Tree = "ACDY" And ACFY = ChildNode.Value Then
                    ChildNode.Checked = True
                End If

                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If
    End Sub
    'Sub FillACD()
    '    Dim str_Sql As String = "select ACY_ID,ACY_DESCR from ACADEMICYEAR_M order by ACY_ID desc"
    '    Dim dsACD As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
    '    ddlAcademicYear.DataSource = dsACD.Tables(0)
    '    ddlAcademicYear.DataTextField = "ACY_DESCR"
    '    ddlAcademicYear.DataValueField = "ACY_ID"
    '    ddlAcademicYear.DataBind()
    '    Dim CFY As String = FeeCommon.GetCurrentAcademicYear(Session("sBSUID"))
    '    For Each rowACD As DataRow In dsACD.Tables(0).Rows
    '        If CFY = rowACD("ACY_ID").ToString Then
    '            ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
    '            Exit For
    '        End If
    '    Next
    'End Sub
End Class
