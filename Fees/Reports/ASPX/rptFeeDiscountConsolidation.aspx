<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptFeeDiscountConsolidation.aspx.vb" Inherits="Fees_Reports_ASPX_rptFeeDiscountConsolidation" Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td align="left">
                              <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                            <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError"></asp:Label>--%>
                            <asp:ValidationSummary
                                ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="5" cellspacing="0" width="100%" >
                    <%--  <tr class ="subheader_img">
            <td align="left" colspan="10" style="height: 19px">
                <asp:Label ID="lblReportCaption" runat="server"></asp:Label></td>
        </tr>--%>
                    <tr runat="server" id="TrBsu">
                        <td id="Td1" width="20%" runat="server" align="left" ><span class="field-label">Business Unit</span></td>
                        <td id="Td3" runat="server" colspan="2" width="30%" align="left"  >
                            <div class="checkbox-list-full-height">
                            <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" /></div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left"  width="20%" runat="server" id="TdFdate1"><span class="field-label">From Date</span></td>
                        <td align="left"  width="30%"  runat="server" id="TdFdate3">
                            <asp:TextBox ID="txtFromDate" runat="server" ></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                        ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                            ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                            ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%"  >
                            <asp:Label ID="labTodate" runat="server" Text="To Date" CssClass="field-label"></asp:Label></td>
                        <td align="left" width="30%"  >
                            <asp:TextBox ID="txtToDate" runat="server" ></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" /><asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                    runat="server" ControlToValidate="txtToDate" ErrorMessage="To Date required"
                                    ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="revToDate" runat="server" ControlToValidate="txtToDate" Display="Dynamic"
                                        EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td runat="server"  width="20%" align="left" ><span class="field-label">Report Options</span></td>
                        <td runat="server"  colspan="2" align="left"  >
                            <asp:CheckBox ID="chkSummary" runat="server" Text="Summary"></asp:CheckBox>
                            <asp:DropDownList ID="ddPaymentOptions" runat="server" Width="50%">
                                <asp:ListItem>All</asp:ListItem>
                                <asp:ListItem>Online</asp:ListItem>
                                <asp:ListItem>Counter</asp:ListItem>
                            </asp:DropDownList></td>
                        <td align="left" width="30%">
                            <asp:CheckBox ID="chkDiscount" runat="server" Text="With Discount Only"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td id="Td2" runat="server"  width="20%" align="left" ><span class="field-label">Filter by Cards</span></td>
                        <td id="Td4" runat="server"  width="30%" align="left"  >
                            <div class="checkbox-list">
                                <asp:Repeater ID="rptCards" runat="server">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkCard" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "cpm_Descr") %>'></asp:CheckBox>
                                        <asp:HiddenField ID="hdnId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "cpm_Id") %>' />
                                        <br />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" >
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>
