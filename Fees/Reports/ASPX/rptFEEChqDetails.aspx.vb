Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class Fees_Reports_ASPX_rptFEEReceiptSummary
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String
    Private Property bBSUTaxable() As Boolean
        Get
            Return ViewState("bBSUTaxable")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bBSUTaxable") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ClientScript.RegisterStartupScript(Me.GetType(), _
        "script", "<script language='javascript'>  CheckOnPostback(); </script>")
        If Not Page.IsPostBack Then
            hfBSU.Value = "display"
        End If
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            bBSUTaxable = DirectCast(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_bFEE_TAXABLE,0) FROM dbo.BUSINESSUNIT_SUB WHERE BUS_BSU_ID='" & Session("sBsuid") & "'"), Boolean)
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_REP_SUMMARY
                    FillBSUNamesTransport("", True)
                    h_bTransport.Value = 1
                    lblReportCaption.Text = "Collection Summary Report"
                Case OASISConstants.MNU_FEE_REP_NORM_SUMMARY
                    FillBSUNames(Session("sbsuid"), False)
                    h_bTransport.Value = 0
                    chkSummary.Enabled = False
                    lblReportCaption.Text = "Collection Summary Report"
                Case OASISConstants.MNU_FEE_TRAN_COLL_CREDIT_CRD
                    FillBSUNamesTransport("", True)
                    lblReportCaption.Text = "Transport Fee Collection Credit Card"
                    h_bTransport.Value = 1
                Case OASISConstants.MNU_FEE_NORM_COLL_CREDIT_CRD
                    FillBSUNames(Session("sbsuid"), False)
                    lblReportCaption.Text = "Fee Collection Credit Card"
                    h_bTransport.Value = 0
                    chkSummary.Enabled = False
                Case OASISConstants.MNU_NORM_FEE_SPLITUP_DETAILS
                    FillBSUNames(Session("sbsuid"), False)
                    trUsrName.Visible = True
                    rbByPayment.Checked = True
                    lblReportCaption.Text = "Daily Receipt Journal"
                    h_bTransport.Value = 0
                    rbByCollection.Visible = Not bBSUTaxable
                    rbByPayment.Visible = Not bBSUTaxable
                    chkSummary.Visible = Not bBSUTaxable
                Case "F720011"
                    lblReportCaption.Text = "Other Fee Collection Details"
                    h_bTransport.Value = 0
                    trUsrName.Visible = False
                    chkSummary.Text = "Payment Mode"
                Case "F720012"
                    lblReportCaption.Text = "Total Fee Collection Summary"
                    h_bTransport.Value = 0
                    trUsrName.Visible = False
                    chkSummary.Enabled = False
                Case "F720077"
                    lblReportCaption.Text = "Discount Scheme"
                    h_bTransport.Value = 0
                    rblDiscountFilter.Visible = True
                    trUsrName.Visible = False
                    chkSummary.Visible = False
                    rbByCollection.Visible = False
                    rbByPayment.Visible = False
            End Select
            SetAcademicYearDate()
            FillBSUNames(True)
            txtFeeCounter.Attributes.Add("ReadOnly", "ReadOnly")
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
        If IsPostBack Then
            FillBSUNames()
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_SUMMARY
                GenerateFeeCollectionSummary()
            Case OASISConstants.MNU_FEE_REP_NORM_SUMMARY
                GenerateNormalFEECollectionSummary()
            Case OASISConstants.MNU_FEE_TRAN_COLL_CREDIT_CRD
                GenerateFEE_Coll_CreditCard()
            Case OASISConstants.MNU_FEE_NORM_COLL_CREDIT_CRD
                GenerateNormalFEECreditCard()
            Case OASISConstants.MNU_NORM_FEE_SPLITUP_DETAILS
                If bBSUTaxable Then
                    GenerateFeeReceiptDetailsWithTAX()
                Else
                    If rbByCollection.Checked Then
                        GenerateNormalFeeCollection()
                    Else
                        GenerateFeeCollectionDetails()
                    End If
                End If

            Case "F720011"
                OtherFeeCollectionDetails()
            Case "F720012"
                TotalFeeCollection()
            Case "F720077"
                DiscountScheme()
            Case Else
                GenerateFEECollection()
        End Select
    End Sub

    Private Sub DiscountScheme()
        Dim cmd As New SqlCommand("[FEES].[DIS_RptFeeDiscount]")
        cmd.CommandType = CommandType.StoredProcedure
        Dim Type As Integer = 1
        Select Case rblDiscountFilter.SelectedValue
            Case "D"
                Type = 2
            Case "C"
                Type = 1
            Case "M"
                Type = 3
        End Select

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpFOR_BSU_IDs.Value = UsrBSUnits1.GetSelectedNode().Replace("||", "|")
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpType As New SqlParameter("@Typ", SqlDbType.Int)
        sqlpType.Value = Type
        cmd.Parameters.Add(sqlpType)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        If Type = 1 Then 'Consolidated
            params("userName") = Session("sUsr_name")
            params("RptHead") = "Details of Advance Fee Collections"
            params("SmallHead") = txtToDate.Text
            params("DateHead") = "As on " & txtToDate.Text
            repSource.ResourceName = "../../fees/reports/rpt/DiscountdetailsConsolidate.rpt"
        ElseIf Type = 2 Then 'Detailed
            params("userName") = Session("sUsr_name")
            params("RptHead") = "Details of Advance Fee Collections"
            params("SmallHead") = "As on " & txtToDate.Text
            params("DateHead") = "For the period " & txtFromDate.Text & " TO  " & txtToDate.Text
            repSource.ResourceName = "../../fees/reports/rpt/Discountdetails.rpt"
        ElseIf Type = 3 Then 'Monthly splitup
            params("userName") = Session("sUsr_name")
            params("RptHead") = "Details of Advance Fee Collections"
            params("SmallHead") = "As on " & txtToDate.Text
            params("DateHead") = "For the period " & txtFromDate.Text & " TO  " & txtToDate.Text
            repSource.ResourceName = "../../fees/reports/rpt/Discountdetails_crosstab.rpt"
        End If
        repSource.Parameter = params
        repSource.IncludeBSUImage = False
        repSource.Command = cmd
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeCollectionDetails()
        Dim cmd As New SqlCommand("[FEES].[F_RPTCollectionData_Oasis]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@FOR_BSU_IDs", SqlDbType.Xml)
        sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpbGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpbGroupCurrency.Value = True
        cmd.Parameters.Add(sqlpbGroupCurrency)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Dim sqlpbLvl As New SqlParameter("@Lvl", SqlDbType.TinyInt)

        sqlpbLvl.Value = 5
        If chkSummary.Checked Then
            params("RPT_CAPTION") = "Daily Receipt Journal Summary- By Payment"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEColl_Normal_Full_Summary.rpt"
        Else
            params("RPT_CAPTION") = "Daily Receipt Journal - By Payment"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEColl_Normal_Full.rpt"
        End If
        cmd.Parameters.Add(sqlpbLvl)
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateNormalFEECreditCard()
        Dim cmd As New SqlCommand("[FEES].[F_GET_FEE_COLL_CREDIT_CARD]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        params("RPT_CAPTION") = "Fee Collection(Credit Card)"
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        If chkSummary.Checked Then
            repSource.ResourceName = "../../fees/reports/rpt/rptFEEColl_TRAN-CREDIT_CRD_Summary.rpt"
        Else
            repSource.ResourceName = "../../fees/reports/rpt/rptFEE_NORM__CREDIT_CRD.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateCreditCardCollectionReport()
        Dim cmd As New SqlCommand("[FEES].[F_GET_FEE_COLL_CREDIT_CARD]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        params("RPT_CAPTION") = "Transport Fee Collection(Credit Card)"
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        If chkSummary.Checked Then
            repSource.ResourceName = "../../fees/reports/rpt/rptFEEColl_TRAN-CREDIT_CRD_Summary.rpt"
        Else
            repSource.ResourceName = "../../fees/reports/rpt/rptFEEColl_TRAN-CREDIT_CRD.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFEE_Coll_CreditCard()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_FEE_COLL_TRAN_CREDIT_CARD]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        params("RPT_CAPTION") = "Transport Fee Collection(Credit Card)"
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        If chkSummary.Checked Then
            repSource.ResourceName = "../../fees/reports/rpt/rptFEEColl_TRAN-CREDIT_CRD_Summary.rpt"
        Else
            repSource.ResourceName = "../../fees/reports/rpt/rptFEEColl_TRAN-CREDIT_CRD.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateNormalFEECollectionSummary()
        Dim cmd As New SqlCommand("[FEES].[F_NORMAL_FEECOLLECTION]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        sqlpBSU_ID.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        params("RPT_CAPTION") = "Fee Collection Details"
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        'repSource.IncludeBSUImage = True
        If chkSummary.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionSummary.rpt"
        ElseIf txtFeeCounter.Text = "" Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_Coll_BSU_Summary.rpt"
        Else
            params("Feecounter") = txtFeeCounter.Text
            params.Remove("RPT_CAPTION")
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionCounterSummary.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeCollectionSummary()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_TRANSPORT_FEECOLLECTION]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        sqlpBSU_ID.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd
        If chkSummary.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionSummary.rpt"
        ElseIf txtFeeCounter.Text = "" Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionBSUSummary.rpt"
        Else
            params("Feecounter") = txtFeeCounter.Text
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECollectionCounterSummary.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateNormalFeeCollection()
        Dim cmd As New SqlCommand("FEES.F_RPT_FEE_DETAILS")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        If chkSummary.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_COLL_TYPES_Summary.rpt"
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_COLL_TYPES_Detailed.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub OtherFeeCollectionDetails()
        Dim cmd As New SqlCommand("[FEES].[OtherFeeCollection_DETAILS]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")

        If chkSummary.Checked Then
            repSource.ResourceName = "../../FEES/REPORTS/RPT/OtherfeecollectiondetailsSummary.rpt"
            params("ReportHead") = "Other Fee Collection Details ( Payment Mode)  " & txtFromDate.Text.ToString() & " ..To.. " & txtToDate.Text.ToString()
        Else
            repSource.ResourceName = "../../FEES/REPORTS/RPT/OtherfeeCollectiondetails.rpt"
            params("ReportHead") = "Other Fee Collection Details For The Period..  " & txtFromDate.Text.ToString() & " ..To.. " & txtToDate.Text.ToString()
        End If
        repSource.Parameter = params
        repSource.Command = cmd
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub TotalFeeCollection()
        Dim cmd As New SqlCommand("[FEES].[TotalFeeCollection]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        sqlpFOR_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")

        repSource.ResourceName = "../../FEES/REPORTS/RPT/TotalFeecollectionSummary.rpt"
        params("ReportHead") = "Total Fee Collection Summary for the period " & txtFromDate.Text.ToString() & " to " & txtToDate.Text.ToString()

        repSource.Parameter = params
        repSource.Command = cmd

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFEECollection()
        Dim cmd As New SqlCommand("FEES.F_GETCONCESSIONDETAILS")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSUIDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        sqlpBSUIDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpBSUIDs)

        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpbSummary As New SqlParameter("@bSummary", SqlDbType.Bit)
        sqlpbSummary.Value = True
        cmd.Parameters.Add(sqlpbSummary)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        repSource.IncludeBSUImage = True
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEECONCESSIONDETAILS_Summary.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFeeReceiptDetailsWithTAX()
        Dim cmd As New SqlCommand("FEES.GET_DAILY_RECEIPT")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlpFOR_BSU_IDs.Value = h_BSUID.Value.Replace("||", "|")
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpFromDT As New SqlParameter("@FROM_DT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@TO_DT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RPT_CAPTION") = "DAILY RECEIPTS JOURNAL"
        params("FROMDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_COLL_RECEIPT_DETAIL_TAX.rpt"

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNamesTransport(h_BSUID.Value) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            FillBSUNamesTransport(h_BSUID.Value)
        End If
    End Sub

    Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_SUMMARY, _
                OASISConstants.MNU_FEE_TRAN_COLL_CREDIT_CRD
                FillBSUNamesTransport("", True)
            Case OASISConstants.MNU_FEE_REP_NORM_SUMMARY
            Case OASISConstants.MNU_FEE_NORM_COLL_CREDIT_CRD
            Case OASISConstants.MNU_NORM_FEE_SPLITUP_DETAILS
                FillBSUNames(h_BSUID.Value, True)
        End Select
    End Sub

    Private Function FillBSUNamesTransport(ByVal BSUIDs As String, Optional ByVal bgetAll As Boolean = False) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim conn As New SqlConnection(str_conn)
        Dim ds As New DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        If bgetAll Then
            condition = ""
        Else
            condition = " SVB_BSU_ID in(" & condition & ")"
        End If
        Try
            Dim cmd As New SqlCommand
            cmd.CommandText = "[GETUNITSFORTRANSPORTFEES]"
            cmd.Parameters.AddWithValue("@Usr_Name", Session("sUsr_Name"))
            cmd.Parameters.AddWithValue("@PROVIDER_BSU_ID", Session("sBsuid"))
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = cmd
            adpt.Fill(ds)
            If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
                Return False
            End If
            Dim dv As New DataView(ds.Tables(0))
            dv.RowFilter = condition
            If bgetAll Then
                h_BSUID.Value = ""
                For i As Integer = 0 To dv.Table.Rows.Count - 1
                    h_BSUID.Value += dv.Item(i)("SVB_BSU_ID") & "||"
                Next
            End If
            Return True
        Catch
            Return False
        Finally
            conn.Close()
        End Try
    End Function

    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, Session("Current_ACD_ID"), Session("sBsuid"))
        txtFromDate.Text = DTFROM
        'txtToDate.Text = DTTO
        txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        If txtFromDate.Text = "" Then
            txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        End If
    End Sub

    Private Sub FillBSUNames(Optional ByVal bGetAll As Boolean = False)
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case OASISConstants.MNU_FEE_REP_SUMMARY, _
                OASISConstants.MNU_FEE_TRAN_COLL_CREDIT_CRD
                FillBSUNamesTransport("", bGetAll)
            Case OASISConstants.MNU_FEE_REP_NORM_SUMMARY, _
            OASISConstants.MNU_FEE_NORM_COLL_CREDIT_CRD, _
            OASISConstants.MNU_NORM_FEE_SPLITUP_DETAILS
                If bGetAll Then h_BSUID.Value = Session("sBSUID")
                FillBSUNames(h_BSUID.Value, True)
        End Select
    End Sub

    Private Function FillBSUNames(ByVal BSUIDs As String, ByVal bGetMulti As Boolean) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        If bGetMulti Then
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
        Else
            condition = "'" & BSUIDs & "'"
        End If
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID SVB_BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID SVB_BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

