Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Fees_Reports_ASPX_rptFeeStudentLedgerTransport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            BindBusinessUnit()
            Try
                ddBusinessunit.Items.FindByValue(Session("sBsuid")).Selected = True
            Catch ex As Exception
            End Try

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_TRANSPORT_STUDENT_LEDGER
                    lblReportCaption.Text = "Student Ledger"
                Case OASISConstants.MNU_FEE_TRANSPORT_STATEMENT_REPORT
                    lblReportCaption.Text = "STATEMENT OF TRANSPORT FEES"

            End Select
            txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            txtFromDate.Text = Format(DateTime.Now.AddMonths(-3), OASISConstants.DateFormat)
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)

        Else
            lblError.Text = ""
        End If
    End Sub


    Sub BindBusinessUnit()
        ddBusinessunit.DataSource = FeeCommon.SERVICES_BSU_M(Session("sUsr_name"), Session("sBsuid"))
        ddBusinessunit.DataBind()
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If h_STUD_ID.Value = "" Then
            'lblError.Text = "Please Select Student(s)"
            usrMessageBar.ShowNotification("Please Select Student(s)", UserControls_usrMessageBar.WarningType.Danger)

            Exit Sub
        End If
        'If UtilityObj.IsFutureDate(txtFromDate.Text, txtToDate.Text) Then
        '    lblError.Text = "Invalid Date !!!"
        '    Exit Sub
        'End If
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code 
            Case OASISConstants.MNU_FEE_TRANSPORT_STUDENT_LEDGER
                F_rptStudentLedger_Transport()
            Case OASISConstants.MNU_FEE_TRANSPORT_STATEMENT_REPORT
                F_rptFEEStatement_Transport()
        End Select
    End Sub

    Private Sub F_rptFEEStatement_Transport()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        cmd.CommandType = CommandType.StoredProcedure
 
        'ALTER procedure rptStudentLedger
        ' @BSU_ID VARCHAR(20)='125016',
        '@STU_IDS VARCHAR(2000)='',
        '@STU_TYPE VARCHAR(5)='S',
        '@FromDT DATETIME ='1-JUN-2008',
        '@ToDT DATETIME='1-JUN-2008'

        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(h_STUD_ID.Value, XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
 
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)
 
        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSTU_BSU_ID.Value = ddBusinessunit.SelectedItem.Value
        cmd.Parameters.Add(sqlpSTU_BSU_ID)
 
        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        If radEnquiry.Checked Then
            sqlpSTU_TYPE.Value = "E"
        Else
            sqlpSTU_TYPE.Value = "S"
        End If
        cmd.Parameters.Add(sqlpSTU_TYPE)
 
        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)
 
        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)
 
        cmd.Connection = New SqlConnection(str_conn)

        Dim cmdSub As New SqlCommand()
        cmdSub.CommandType = CommandType.Text
        cmdSub.CommandText = "SELECT FRM_REMARKS, FRM_SHORT_DESCR, FRM_SIGNATORY FROM FEES.FEE_REMINDER_M " & _
        " WHERE FRM_BSU_ID = '" & ddBusinessunit.SelectedItem.Value & "' AND FRM_Level = 4"
        cmdSub.Connection = New SqlConnection(ConnectionManger.GetOASISConnectionString)
        Dim repSourceSubRep(1) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub

        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("PAY_DATE") = "" 'FeeCommon.GetPayDateforTransport(ddBusinessunit.SelectedItem.Value)
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        repSource.SubReport = repSourceSubRep
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = Session("sBsuid")
        'repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFeeTransportStatementOfAccount.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub


    Private Sub F_rptStudentLedger_Transport()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        Dim cmdSub1 As New SqlCommand("[FEES].[F_rptStudentLedgerSummary]")
        Dim cmdSub2 As New SqlCommand("[FEES].[F_rptStudentPDCData]")
        cmd.CommandType = CommandType.StoredProcedure
        cmdSub1.CommandType = CommandType.StoredProcedure
        cmdSub2.CommandType = CommandType.StoredProcedure
        'ALTER procedure rptStudentLedger
        ' @BSU_ID VARCHAR(20)='125016',
        '@STU_IDS VARCHAR(2000)='',
        '@STU_TYPE VARCHAR(5)='S',
        '@FromDT DATETIME ='1-JUN-2008',
        '@ToDT DATETIME='1-JUN-2008'

        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(h_STUD_ID.Value, XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
        cmdSub1.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)
        cmdSub2.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)
        cmdSub1.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
        cmdSub2.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSTU_BSU_ID.Value = ddBusinessunit.SelectedItem.Value
        cmd.Parameters.Add(sqlpSTU_BSU_ID)
        cmdSub1.Parameters.AddWithValue("@STU_BSU_ID", ddBusinessunit.SelectedItem.Value)
        cmdSub2.Parameters.AddWithValue("@STU_BSU_ID", ddBusinessunit.SelectedItem.Value)

        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        If radEnquiry.Checked Then
            sqlpSTU_TYPE.Value = "E"
        Else
            sqlpSTU_TYPE.Value = "S"
        End If
        cmd.Parameters.Add(sqlpSTU_TYPE)
        cmdSub1.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)
        cmdSub2.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)


        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)
        cmdSub1.Parameters.AddWithValue("@FromDT", CDate(txtFromDate.Text))
        cmdSub2.Parameters.AddWithValue("@FromDT", CDate(txtFromDate.Text))

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)
        cmdSub1.Parameters.AddWithValue("@ToDT", CDate(txtToDate.Text))
        cmdSub2.Parameters.AddWithValue("@ToDT", CDate(txtToDate.Text))


        cmd.Connection = New SqlConnection(str_conn)
        cmdSub1.Connection = New SqlConnection(str_conn)
        cmdSub2.Connection = New SqlConnection(str_conn)
        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub1
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(1).Command = cmdSub2
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        repSource.SubReport = repSourceSubRep

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = Session("sBsuid")
        'repSource.IncludeBSUImage = True

        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFee_Transport_StudentLedgerNext.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub


    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim lblBSUID As New Label
        'lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        'If Not lblBSUID Is Nothing Then
        '    h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
        '    If Not FillBSUNames(h_BSUID.Value) Then
        '        h_BSUID.Value = lblBSUID.Text
        '    End If
        '    grdBSU.PageIndex = grdBSU.PageIndex
        '    FillBSUNames(h_BSUID.Value)
        'End If
    End Sub


    Protected Sub lblAddNewStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddNewStudent.Click
        AddStudent()
    End Sub

    Private Sub AddStudent()
            Dim str_stuid As String = FeeCommon.GetTransportStudentID(txtStudName.Text, ddBusinessunit.SelectedItem.Value, False, True)
            If str_stuid <> "" Then
                If h_STUD_ID.Value.EndsWith("||") Then
                    h_STUD_ID.Value = h_STUD_ID.Value & str_stuid
                Else
                    h_STUD_ID.Value = h_STUD_ID.Value & "||" & str_stuid
                End If
                txtStudName.Text = ""
                FillSTUNames(h_STUD_ID.Value)
            End If
    End Sub


    Private Function FillSTUNames(ByVal STU_IDs As String) As Boolean
        Dim IDs As String() = STU_IDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = " SELECT STU_NO , STU_NAME FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvStudentDetails.DataSource = ds
        gvStudentDetails.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function


    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        FillSTUNames(h_STUD_ID.Value)
    End Sub


    Protected Sub gvStudentDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudentDetails.PageIndexChanging
        gvStudentDetails.PageIndex = e.NewPageIndex
        FillSTUNames(h_STUD_ID.Value)
    End Sub


    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBusinessunit.SelectedIndexChanged
        h_STUD_ID.Value = ""
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Protected Sub txtStudName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If txtStudName.Text <> "The following student(s) selected" Then
            AddStudent()     'Removed to fill the grid
        Else
            txtStudName.Text = ""
            FillSTUNames(h_STUD_ID.Value)
            'h_STUD_ID.Value = ""
        End If


    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

