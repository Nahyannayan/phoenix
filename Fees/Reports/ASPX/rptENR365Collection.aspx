<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptENR365Collection.aspx.vb" Inherits="rptENR365Collection" Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
 <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
<%--    <script language="javascript" type="text/javascript">
        function GetStudent() {
            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 875px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var STUD_TYP = document.getElementById("<%=radEnquiry.ClientID %>").checked;
            var url;
            if (STUD_TYP == true) {
                url = "../../ShowStudentMulti.aspx?TYPE=ENQUIRY&MULTI_SEL=true&bsu=" + '<%= Session("sBsuid") %>';
                result = window.showModalDialog(url, "", sFeatures);
            }
            else {
                url = "../../ShowStudentMulti.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu=" + '<%= Session("sBsuid") %>';

                result = window.showModalDialog(url, "", sFeatures);
            }
            if (result != '' && result != undefined) {

                document.getElementById('<%=txtStudName.ClientID %>').value = 'Multiple Students selected';
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = result;
            }
            return true;
        }
    </script>--%>
    <script>
        function GetStudent() {
            var STUD_TYP = document.getElementById("<%=radEnquiry.ClientID %>").checked;
            var url;
            if (STUD_TYP == true) {
                url = "../../ShowStudentMulti.aspx?TYPE=ENQUIRY&MULTI_SEL=true&bsu=" + '<%= Session("sBsuid") %>';
            }
            else {
                url = "../../ShowStudentMulti.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu=" + '<%= Session("sBsuid") %>';

            }
            var oWnd = radopen(url, "pop_student");
        }

 function OnClientClose1(oWnd, args) {

     //get the transferred arguments
     var arg = args.get_argument();
     if (arg) {

         NameandCode = arg.NameandCode.split('||');

         document.getElementById('<%=txtStudName.ClientID %>').value = arg.NameandCode ;
         document.getElementById('<%=h_STUD_ID.ClientID %>').value = arg.NameandCode;
          __doPostBack('<%= txtStudName.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                            <br />
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Business Unit</span>
                        </td>
                        <td align="left" class="matters" width="30%">
                            <div class="checkbox-list">
                                <uc1:usrBSUnits ID="usrBSUnits1" runat="server" />
                            </div>
                        </td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Fee Type</span>
                        </td>
                        <td align="left" class="matters">
                            <div class="checkbox-list">
                                <asp:TreeView ID="trvFeeTypes" runat="server" onclick="client_OnTreeNodeChecked();"
                                    ShowCheckBoxes="all">
                                </asp:TreeView>
                            </div>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">From Date</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtFromDate" runat="server"  ></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                        runat="server" ControlToValidate="txtFromDate" ErrorMessage="From Date required"
                        ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                            ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                            ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
                        </td>
                        <td align="left" class="matters" colspan="1"><span class="field-label">To Date</span>
                        </td>
                        <td align="left" class="matters" colspan="1">
                            <asp:TextBox ID="txtToDate" runat="server"  ></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false" /><asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                    runat="server" ControlToValidate="txtToDate" ErrorMessage="To Date required"
                                    ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="revToDate" runat="server" ControlToValidate="txtToDate" Display="Dynamic"
                                        EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="4">
                            <asp:RadioButtonList ID="rblSelect" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="AS"><span class="field-label">Activity/School</span></asp:ListItem>
                                <asp:ListItem Value="SA"><span class="field-label">School/Activity</span></asp:ListItem>
                            </asp:RadioButtonList></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Student</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:RadioButton ID="radStudent" runat="server" Checked="True" GroupName="STUD_ENQ" CssClass="field-label"
                                Text="Student" AutoPostBack="True" OnCheckedChanged="radStudent_CheckedChanged"></asp:RadioButton>
                            <asp:RadioButton ID="radEnquiry" runat="server" GroupName="STUD_ENQ" Text="Enquiry" CssClass="field-label"
                                AutoPostBack="True" OnCheckedChanged="radEnquiry_CheckedChanged"></asp:RadioButton>
                            <br />
                            <asp:TextBox ID="txtStudName" runat="server" AutoPostBack="True" OnTextChanged="txtStudName_TextChanged"></asp:TextBox>
                            <asp:LinkButton ID="lblAddNewStudent" runat="server" CausesValidation="False" OnClick="lblAddNewStudent_Click">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetStudent(); return false;" />
                        </td>
                        <td colspan="2">
                            <asp:GridView ID="gvStudentDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-row table-bordered" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Student ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudID" runat="server" Text='<%# bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudentName" runat="server" Text='<%# bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="Delete">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4">
                            <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>

</asp:Content>
