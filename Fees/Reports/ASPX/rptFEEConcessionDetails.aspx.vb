Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class rptFEEConcessionDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ClientScript.RegisterStartupScript(Me.GetType(), _
        "script", "<script language='javascript'>  CheckOnPostback(); </script>")
        If Not Page.IsPostBack Then
            hfConcession.Value = "display"
        End If
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Dim MainMnu_code As String
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle

            lblReportCaption.Text = "Fee Concession Details"

            SetAcademicYearDate()
        End If

        If Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+")) = "F730370" Then
            trBSUnit.Visible = False
            trConcession.Visible = False
            trGroupby.Visible = False
            trDetails.Visible = False
            trSelConcession.Visible = False
            lblReportCaption.Text = "Approved Tuition Fee Concession requests"
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
        FillConcessionDetails(h_ConcessionType.Value)
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "F730370"
                GenerateTuitionFeeApprovals()
            Case Else
                If h_ConcessionType.Value = "" Then GetAllConcession()
                GenerateFEEConcession()
        End Select
    End Sub

    Private Sub GetAllConcession()
        Dim str_Sql As String = "SELECT FEES.FEE_CONCESSION_M.FCM_ID FROM FEES.FEE_CONCESSION_M "
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
        Dim str_concession As String = String.Empty
        While (dr.Read())
            str_concession = str_concession + "||" + dr(0).ToString
        End While
        h_ConcessionType.Value = str_concession
    End Sub
    Public Sub GetBSU_RoundOff(ByVal BSU_ID As String)
        'Author(--Swapna)
        'Date   --18/Aug/2011
        'Purpose--Get selected BSU's round off
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim query As String = ""

        query = "SELECT  BSU_ROUNDOFF FROM BUSINESSUNIT_M WHERE BSU_ID = '" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(query, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

        While reader.Read
            ViewState("RoundOffVal") = reader.Item("BSU_ROUNDOFF")
        End While

    End Sub
    Private Sub GenerateTuitionFeeApprovals()
        Dim SpName = "FEES.GET_APPROVED_TUITION_FEE_REQUEST_LIST"



        Dim cmd As New SqlCommand(SpName)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSUIDs As New SqlParameter("@LOG_BSU_ID", SqlDbType.VarChar)
        sqlpBSUIDs.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSUIDs)

        Dim sqlpFromDT As New SqlParameter("@FROMDT", SqlDbType.Date)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)





        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.Date)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)



        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim RoundOffVal As Integer = Session("BSU_ROUNDOFF")
        Dim separator As String() = New String() {"||"}
        Dim myValarr() As String = UsrBSUnits1.GetSelectedNode().Split(separator, StringSplitOptions.RemoveEmptyEntries)
        'If myValarr.Length > 1 Then
        '    'If sqlpBSU_ID.Value <> "" Then
        '    RoundOffVal = 2
        'Else

        '    GetBSU_RoundOff(myValarr(0))
        '    RoundOffVal = ViewState("RoundOffVal")

        'End If

        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        params("RoundOffVal") = RoundOffVal
        repSource.IncludeBSUImage = True
        repSource.Parameter = params
        repSource.Command = cmd

        params("RPT_CAPTION") = "Approved Tuition Fee Concession requests"
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TUITION_FEE_APPROVAL_LIST.rpt"

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Sub GenerateFEEConcession()
        Dim SpName = "FEES.F_GETCONCESSIONDETAILS"
        SpName = "FEES.F_GETCONCESSIONDETAILS_Next"
        Dim vType As Integer
        If radDetailed.Checked Then
            vType = 1
            'SpName = "FEES.F_GETCONCESSIONDETAILS_Next"
        ElseIf radcatDetails.Checked Then
            vType = 2
        ElseIf radCatSummary.Checked Then
            'vType = 3
            vType = 1
        ElseIf RdContype.Checked Then
            vType = 1
        End If

        Dim cmd As New SqlCommand(SpName)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSUIDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        sqlpBSUIDs.Value = GenerateXML(UsrBSUnits1.GetSelectedNode(), XMLType.BSUName)
        cmd.Parameters.Add(sqlpBSUIDs)

        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)


        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpConcTypes As New SqlParameter("@ConcTypes", SqlDbType.Xml)
        sqlpConcTypes.Value = GenerateXML(h_ConcessionType.Value, XMLType.ConcessionType)
        cmd.Parameters.Add(sqlpConcTypes)

        Dim sqlpbGroupByBSU As New SqlParameter("@bGroupByBSU", SqlDbType.Bit)
        sqlpbGroupByBSU.Value = chkGroupByBSU.Checked
        cmd.Parameters.Add(sqlpbGroupByBSU)

        Dim sqlpRepType As New SqlParameter("@RepType", SqlDbType.Int)
        sqlpRepType.Value = vType
        cmd.Parameters.Add(sqlpRepType)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim RoundOffVal As Integer = Session("BSU_ROUNDOFF")
        Dim separator As String() = New String() {"||"}
        Dim myValarr() As String = UsrBSUnits1.GetSelectedNode().Split(separator, StringSplitOptions.RemoveEmptyEntries)
        If myValarr.Length > 1 Then
            'If sqlpBSU_ID.Value <> "" Then
            RoundOffVal = 2
        Else

            GetBSU_RoundOff(myValarr(0))
            RoundOffVal = ViewState("RoundOffVal")

        End If

        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        params("RoundOffVal") = RoundOffVal
        repSource.IncludeBSUImage = True
        repSource.Parameter = params
        repSource.Command = cmd

        Select Case vType
            Case 1

                If RdContype.Checked Then
                    params("RPT_CAPTION") = "FEE CONCESSION STUDENT DETAILS CATEGORYWISE "
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_CONCESSION_TYPE.rpt"
                ElseIf radCatSummary.Checked Then
                    params("RPT_CAPTION") = "FEE CONCESSION CATEGORISED SUMMARY"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_CONCESSION_TYPE_SUMMARY.rpt"
                Else
                    params("RPT_CAPTION") = "FEE CONCESSION STUDENT WISE DETAILS"
                    repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_CONCESSION_CAT_STU_DETAILS.rpt"
                End If

            Case 2
                params("RPT_CAPTION") = "FEE CONCESSION CATEGORISED DETAILS"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_CONCESSION_CAT_DETAILS.rpt"
            Case 3
                params("RPT_CAPTION") = "FEE CONCESSION CATEGORISED SUMMARY"
                repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_CONCESSION_CAT_SUMMARY.rpt"


        End Select
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function FillConcessionDetails(ByVal ConcIDs As String) As Boolean
        Dim IDs As String() = ConcIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_Sql As String
        Dim ds As DataSet = Nothing
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "" & IDs(i) & ""
            i += 1
        Next

        If condition = String.Empty Then
            condition = "-1"
        End If
        str_Sql = "SELECT FEES.FEE_CONCESSION_M.FCM_DESCR, FEES.FEE_CONCESSION_M.FCM_ID " & _
    " FROM FEES.FEE_CONCESSION_M WHERE FCM_ID IN(" + condition + ")"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        grdConc.DataSource = ds
        grdConc.DataBind()
        Return True
    End Function

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function



    Protected Sub grdConc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdConc.PageIndexChanging
        grdConc.PageIndex = e.NewPageIndex
        FillConcessionDetails(h_ConcessionType.Value)
    End Sub

    Protected Sub lnkbtnconcDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCONCID As New Label
        lblCONCID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblCONCID Is Nothing Then
            h_ConcessionType.Value = h_ConcessionType.Value.Replace(lblCONCID.Text, "-1").Replace("||||", "||")
            If Not FillConcessionDetails(h_ConcessionType.Value) Then
                h_ConcessionType.Value = lblCONCID.Text
            End If
            grdConc.PageIndex = grdConc.PageIndex
            FillConcessionDetails(h_ConcessionType.Value)
        End If

    End Sub

    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, Session("Current_ACD_ID"), Session("sBsuid"))
        txtFromDate.Text = DTFROM
        'txtToDate.Text = DTTO
        txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        If txtFromDate.Text = "" Then
            txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

