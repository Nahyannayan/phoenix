<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTransport_FEEChargedAdv.aspx.vb" Inherits="rptTransport_FEEChargedAdv" Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<%@ Register Src="../../../UserControls/usrTransportBSUnits.ascx" TagName="usrTransportBSUnits" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE");
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length;
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" width="70%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <table width="100%">

                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Business Units</span></td>
                        <td align="left" class="matters"  colspan="3">
                            <div class="checkbox-list">

                                <uc1:usrTransportBSUnits ID="usrTransportBSUnits1" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr id="trFeeType" runat="server">
                        <td align="left" class="matters"><span class="field-label">Fee Type</span></td>
                        <td align="left" class="matters" colspan="3">
                            <div class="checkbox-list">
                                <asp:TreeView ID="trvFeeTypes" runat="server"
                                    onclick="client_OnTreeNodeChecked();" ShowCheckBoxes="all">
                                </asp:TreeView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"  width="20%"><span class="field-label">As On Date</span></td>

                        <td align="left" class="matters"  width="30%">
                            <asp:TextBox ID="txtAsOnDate" runat="server" Width="112px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                runat="server" ControlToValidate="txtAsOnDate" ErrorMessage="From Date required"
                                ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator
                                ID="revFromdate" runat="server" ControlToValidate="txtAsOnDate" Display="Dynamic"
                                ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td  width="20%"></td>
                        <td  width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Options</span></td>

                        <td align="left" class="matters" colspan="3">
                            <asp:RadioButtonList ID="rblFilter1" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="S"><span class="field-label">Student Wise</span></asp:ListItem>
                                <asp:ListItem Value="F"><span class="field-label">Summary by Fee Type</span></asp:ListItem>
                                <asp:ListItem Value="A"><span class="field-label">Summary by Accounts</span></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4">
                            <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_ACTIDs" runat="server" />
                <asp:HiddenField ID="h_Mode" runat="server" />
                <asp:HiddenField ID="h_ACTTYPE" runat="server"></asp:HiddenField>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>

