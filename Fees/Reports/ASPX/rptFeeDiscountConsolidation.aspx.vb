Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_Reports_ASPX_rptFeeDiscountConsolidation
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Dim dt As DataTable = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "select * from (select cpm_id, cpm_descr from OASISFIN.dbo.CREDITCARD_PROVD_M union select 0, 'All Discounts' ) a order by cpm_id").Tables(0)
            rptCards.DataSource = dt
            rptCards.DataBind()
            Select Case MainMnu_code
                Case "F701019"
                    lblReportCaption.Text = "NBAD GEMS Co Brand Credit card Discount Summary "
            End Select
            txtFromDate.Text = Format(Date.Now.AddMonths(-6), "dd/MMM/yyyy")
            txtToDate.Text = Format(Date.Now, "dd/MMM/yyyy")
            Page.Title = OASISConstants.Gemstitle
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            ' lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            ' lblError.Text = ""
        End If
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTFromDate") = txtFromDate.Text
        Session("RPTToDate") = txtToDate.Text

        Select Case MainMnu_code 
            Case "F701019"
                F_RPTCollectionData_NBAD()
        End Select
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub F_RPTCollectionData_NBAD()
        Dim dateHead As String = "From " & txtFromDate.Text.ToString() & " To " & txtToDate.Text.ToString()
        Dim RptName As String
        If chkSummary.Checked Then
            RptName = "../../FEES/REPORTS/RPT/rptFeeDiscountCollectionData_NBADSum.rpt"
        Else
            RptName = "../../FEES/REPORTS/RPT/rptFeeDiscountCollectionData_NBAD.rpt"
        End If  
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_RPTCollectionData_NBAD]")
        cmd.CommandType = CommandType.StoredProcedure 

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpPayment As New SqlParameter("@PaymentType", SqlDbType.VarChar)
        sqlpPayment.Value = ddPaymentOptions.SelectedItem.Value
        cmd.Parameters.Add(sqlpPayment)

        Dim sqlpSummary As New SqlParameter("@Summary", SqlDbType.Bit)
        sqlpSummary.Value = chkSummary.Checked
        cmd.Parameters.Add(sqlpSummary)

        Dim sqlpOnlyDiscount As New SqlParameter("@OnlyDiscount", SqlDbType.Bit)
        sqlpOnlyDiscount.Value = chkDiscount.Checked
        cmd.Parameters.Add(sqlpOnlyDiscount)

        Dim sqlBSUID As New SqlParameter("@FOR_BSU_IDs", SqlDbType.VarChar)
        sqlBSUID.Value = UsrBSUnits1.GetSelectedNode().Replace("||", "|")
        cmd.Parameters.Add(sqlBSUID)

        Dim strCards As String = "", strDefCards As String = "", strDefForCards As String = ""
        Dim strForCards As String = "For Cards : "
        For Each childItem As Control In rptCards.Items
            Dim chkCard As CheckBox = CType(childItem.FindControl("chkCard"), CheckBox)
            Dim hdnId As HiddenField = CType(childItem.FindControl("hdnId"), HiddenField)
            If chkCard.Checked Then
                strCards &= hdnId.Value & ","
                strForCards &= chkCard.Text & ","
            End If
            strDefCards = hdnId.Value
            strDefForCards = "For Card : " & chkCard.Text
        Next
        If strCards = "" Then
            strCards = strDefCards
            strForCards = strDefForCards
        Else
            strCards = Left(strCards, strCards.Length - 1)
            strForCards = Left(strForCards, strForCards.Length - 1)
        End If

        Dim sqlCards As New SqlParameter("@FOR_Cards", SqlDbType.VarChar)
        sqlCards.Value = strCards
        cmd.Parameters.Add(sqlCards)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("ForCards") = strForCards
        params("RptHead") = "NBAD GEMS Co Brand Credit card Discount Summary "
        If txtFromDate.Text.Equals(txtToDate.Text) Then
            dateHead = "As On " & txtToDate.Text.ToString()
        End If
        params("DateHead") = dateHead

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = RptName

        Session("ReportSource") = repSource
        '     Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class
