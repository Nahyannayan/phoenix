Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class Fees_Reports_ASPX_rptFEEReceiptSummary
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ClientScript.RegisterStartupScript(Me.GetType(), _
        "script", "<script language='javascript'>  CheckOnPostback(); </script>")
        If Not Page.IsPostBack Then
            hfBSU.Value = "display"
        End If
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle


            If MainMnu_code = "F726061" Then
                trSummary.Style("display") = "inline"
                If chkReconciliation_Summary.Checked Then
                    lblReportCaption.Text = "Fee Collection Credit Card Summary"
                Else
                    lblReportCaption.Text = "Fee Collection Credit Card"
                End If
            End If

            txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            txtFromDate.Text = Format(DateTime.Now.AddDays(-(DateTime.Now.Day - 1)), OASISConstants.DateFormat)
            txtFeeCounter.Attributes.Add("ReadOnly", "ReadOnly")
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If

        'txtToDate.Attributes.Add("ReadOnly", "Readonly")
        'txtFromDate.Attributes.Add("ReadOnly", "Readonly")

        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
     End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If MainMnu_code = "F726061" Then
            FeeCollectionCreditcardDetails()
        End If
    End Sub

    Private Sub FeeCollectionCreditcardDetails()
        Dim cmd As New SqlCommand("[FEES].[FEE_COLL_CREDITCARD_OASIS]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFOR_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.VarChar)
        sqlpFOR_BSU_IDs.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpFOR_BSU_IDs)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpFromDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("ToDT") = txtToDate.Text
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd

        If chkReconciliation_Summary.Checked Then
            params("RPT_CAPTION") = "Fee Collection Summary (Credit Card)"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEColl_NORMAL_CREDIT_CRD_Summary.rpt"
        Else
            params("RPT_CAPTION") = "Fee Collection Report (Credit Card)"
            repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEEColl_NORMAL_CREDIT_CRD.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

