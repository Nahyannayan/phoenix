Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeOtherFeeCollectionView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Try
                hlAddNew.NavigateUrl = "feeOtherFeeCollection.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim q = "SELECT BSU_ID FROM FEES.VW_OTH_FEE_DAX_ENABLE WHERE BSU_ID='" & Session("sBsuId") & "'"
                Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, q)
                If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    ViewState("bIsOnDAX") = 1
                Else
                    ViewState("bIsOnDAX") = 0
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code").ToString <> OASISConstants.MNU_FEE_COLLECTION_OTHER _
                                         And ViewState("MainMnu_code").ToString <> "F300255") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_FEE_COLLECTION_OTHER
                            lblHead.Text = "Other Fee Collection"
                            ViewState("trantype") = "A"
                        Case "F300255"
                            lblHead.Text = "Transport Other Fee Collection"
                            ViewState("trantype") = "A"
                    End Select
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                ' lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""
            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7, lstrCondn8 As String
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            lstrCondn8 = ""
            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtReceiptno FOC_RECNO
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FOC_RECNO", lstrCondn1)

                '   -- 2  txtDate
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FOC_DATE", lstrCondn2)

                '   -- 3  txtGrade
                larrSearchOpr = h_selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "CLT_DESCR", lstrCondn3)

                '   -- 4   txtStuno
                larrSearchOpr = h_selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ACT_NAME", lstrCondn4)

                '   -- 5  txtStuname
                larrSearchOpr = h_selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "COL_DESCR", lstrCondn5)

                '   -- 6  txtAmount
                larrSearchOpr = h_selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
                lstrCondn6 = txtSearch.Text
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FOC_AMOUNT", lstrCondn6)

                '   -- 7  txtDesc
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
                lstrCondn7 = txtSearch.Text
                If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FOC_NARRATION", lstrCondn7)

                '   -- 8  txtInvNo
                larrSearchOpr = h_selected_menu_8.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtInvno")
                lstrCondn8 = txtSearch.Text
                If (lstrCondn8 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FOC_INVOICE_NO", lstrCondn8)
            End If
            Dim STR_RECEIPT As String = "FEES.VW_OSO_FEES_RECEIPT_OTHER"

            If rdDeleted.Checked Then
                str_Filter = str_Filter & " and isnull(FOC_bDELETED,0)=1"
            End If
            str_Sql = "SELECT * FROM " & STR_RECEIPT & " where  FOC_BSU_ID='" & Session("sBSUID") & "'" _
            & " " & str_Filter _
            & " ORDER BY FOC_DATE DESC ,FOC_RECNO DESC"

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If

            txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn5

            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = lstrCondn6

            txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
            txtSearch.Text = lstrCondn7

            txtSearch = gvJournal.HeaderRow.FindControl("txtInvno")
            txtSearch.Text = lstrCondn8

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Dim lblInvNo As Label = DirectCast(e.Row.FindControl("lblInvNo"), Label)
                Dim lblInvNo As LinkButton = DirectCast(e.Row.FindControl("lblInvNo"), LinkButton)


                Dim lblBatchNo As Label = DirectCast(e.Row.FindControl("lblBatchNo"), Label)
                Dim lbBatchReceipt As LinkButton = DirectCast(e.Row.FindControl("LbBatchReceipt"), LinkButton)

                Dim rowView As DataRowView = e.Row.DataItem
                If Not lblInvNo Is Nothing AndAlso lblInvNo.Text <> "" AndAlso Not lblInvNo Is Nothing AndAlso Not rowView Is Nothing Then
                    Dim queryString = "&ID=" & Encr_decrData.Encrypt(rowView("FOC_ID")) & ""
                    lblInvNo.Attributes.Add("onClick", "return ShowWindowWithClose('FeeReportInterpose.aspx?TYPE=" & Encr_decrData.Encrypt("OTHCOLINV") & queryString & "','INVOICE','45%','85%');")
                End If

                If lblInvNo Is Nothing OrElse lblInvNo.Text = "" Then
                    lblInvNo.Enabled = False
                End If

                If lblBatchNo.Text <> "0" Then
                    lbBatchReceipt.Visible = True
                Else
                    lbBatchReceipt.Visible = False
                End If

            End If

            'Dim lblELA_ID As New Label
            'lblELA_ID = TryCast(e.Row.FindControl("lblELA_ID"), Label)
            'Dim hlEdit As New HyperLink
            'hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            'If hlEdit IsNot Nothing And lblELA_ID IsNot Nothing Then
            '    ViewState("datamode") = Encr_decrData.Encrypt("view")
            '    If ViewState("bIsOnDAX") = 1 Then
            '        hlEdit.NavigateUrl = "feeCollection.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            '    Else
            '        hlEdit.NavigateUrl = "feeCollection.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            '    End If
            'End If



        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbVoucher_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblReceipt As LinkButton = sender.Parent.parent.findcontrol("lblReceipt")
            Dim lblFOCDT As Label = sender.Parent.parent.findcontrol("lblFOCDT")
            'ViewState("recno") = lblReceipt.text
            PrintReceipt(lblReceipt.Text, lblFOCDT.Text)
            h_print.Value = "print"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub lbBatchReceipt_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblBatchReceipt As Label = sender.Parent.parent.findcontrol("lblBatchNo")
            PrintBatchReceipt(lblBatchReceipt.Text)
            h_print.Value = "print"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub PrintBatchReceipt(ByVal p_Receiptno As String)
        If ViewState("bIsOnDAX") = 1 Then
            PrintBatch(p_Receiptno)
        End If
    End Sub
    Private Sub PrintBatch(Optional ByVal FOC_BATCH_NO As String = "")
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
            Dim cmd As New SqlCommand
            Dim ClientId As String = ""
            cmd.CommandText = "FEES.OSO_FEES_BATCH_RECEIPT_OTHER"
            Dim sqlParam(2) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@FOC_BATCH_NO", FOC_BATCH_NO, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@FOC_BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            Dim repSource As New MyReportClass
            repSource.Command = cmd
            repSource.Parameter = params
            repSource.ResourceName = "../../Fees/Reports/rpt/rptOtherFeeCollectionBatchReceipt.rpt"
            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub PrintReceipt(ByVal p_Receiptno As String, ByVal FOCDT As String)
        If ViewState("bIsOnDAX") = 1 And CDate(FOCDT) > CDate("30/Apr/2016") Then
            Session("ReportSource") = FeeCollectionOther.PrintReceipt_DAX(p_Receiptno, Session("sUsr_name"), Session("sBsuid"), _
        ConnectionManger.GetOASIS_FEESConnectionString, False)
        Else
            Session("ReportSource") = FeeCollectionOther.PrintReceipt(p_Receiptno, Session("sUsr_name"), Session("sBsuid"), _
                    ConnectionManger.GetOASIS_FEESConnectionString)
        End If

    End Sub

    Protected Sub rdDeleted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdDeleted.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        gridbind()
    End Sub

    
End Class
