Imports System.Data
Imports System.Data.SqlClient

Partial Class fees_FeeTransportReminderTemplate
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Not IsPostBack Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            txtUserName.Attributes.Add("ReadOnly", "ReadOnly")
            If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.FEECreditCardMachine Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ddlBusinessUnit.DataBind()
            End If
            If ViewState("datamode") = "view" Then
                Dim vCCM_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("CCM_ID").Replace(" ", "+"))

                Dim vCMM As FEE_MST_CC_MACHINE = FEE_MST_CC_MACHINE.GetDetails(vCCM_ID, ConnectionManger.GetOASISConnectionString)
                ddlBusinessUnit.ClearSelection()
                ddlBusinessUnit.SelectedValue = vCMM.CMM_ALLOC_BSU_ID
                txtBankCode.Text = vCMM.CCM_BANK
                txtBankDescr.Text = vCMM.CCM_BANKNAME
                txtDescription.Text = vCMM.CCM_DESCR
                txtUserName.Text = vCMM.CMM_USR_ID
                Session("sFEE_MST_CCMACHINE") = vCMM
                DissableControls(True)
            End If
        End If
    End Sub

    Private Sub DissableControls(ByVal bDissable As Boolean)
        ddlBusinessUnit.Enabled = Not bDissable
        txtDescription.ReadOnly = bDissable
        imgBank.Enabled = Not bDissable
        ImageButton1.Enabled = Not bDissable
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Not set_bankaccount() Then
            'lblError.Text = " Please select Bank"
            usrMessageBar.ShowNotification("Please select Bank", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim vCMM As New FEE_MST_CC_MACHINE
        If Not Session("sFEE_MST_CCMACHINE") Is Nothing Then
            vCMM = Session("sFEE_MST_CCMACHINE")
        End If
        vCMM.CMM_ALLOC_BSU_ID = ddlBusinessUnit.SelectedValue
        vCMM.CCM_BANK = txtBankCode.Text
        vCMM.CCM_DESCR = txtDescription.Text
        vCMM.CMM_BSU_ID = Session("sBSUID")
        vCMM.CMM_USR_ID = txtUserName.Text

        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction("FEE_CC_MST")
        Dim retVal As Integer = FEE_MST_CC_MACHINE.SaveDetails(vCMM, conn, trans)
        If retVal <> 0 Then
            trans.Rollback()
            'lblError.Text = UtilityObj.getErrorMessage(retVal)
            usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
        Else
            trans.Commit()
            'lblError.Text = "Data updated Successfully"
            usrMessageBar.ShowNotification("Please select Bank", UserControls_usrMessageBar.WarningType.Danger)
            ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
            'If flagAudit <> 0 Then
            '    Throw New ArgumentException("Could not process your request")
            'End If
        End If

    End Sub

    Private Sub ClearDetails()
        ddlBusinessUnit.ClearSelection()
        txtDescription.Text = ""
        txtBankCode.Text = ""
        txtBankDescr.Text = ""
        txtUserName.Text = ""
        hfFEECounter.Value = ""
        Session("sFEE_MST_CCMACHINE") = Nothing
    End Sub

    Private Function set_bankaccount() As Boolean
        Dim str_bankact_name As String = UtilityObj.GetDataFromSQL("SELECT ACT_ID+'|'+ACT.ACT_NAME " _
        & " FROM VW_OSA_ACCOUNTS_M AS ACT " _
        & " WHERE (ACT_BSU_ID like '%" & Session("sBsuid") & "%') AND ACT_ID ='" & txtBankCode.Text & "'", ConnectionManger.GetOASISFINConnectionString)
        If str_bankact_name <> "--" Then
            txtBankCode.Text = str_bankact_name.Split("|")(0)
            txtBankDescr.Text = str_bankact_name.Split("|")(1)
            Return True
        Else
            txtBankDescr.Text = ""
            Return False
        End If
    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        Dim vCCM As New FEE_MST_CC_MACHINE
        If Not Session("sFEE_MST_CCMACHINE") Is Nothing Then
            vCCM = Session("sFEE_MST_CCMACHINE")
            vCCM.bEdit = True
            Session("sFEE_MST_CCMACHINE") = vCCM
        End If
        DissableControls(False)
        UtilityObj.beforeLoopingControls(Me.Page)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearDetails()
        DissableControls(False)
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
     End Sub

    Protected Sub txtBankCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        set_bankaccount()
    End Sub
End Class
