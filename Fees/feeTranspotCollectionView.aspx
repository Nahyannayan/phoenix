<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeTranspotCollectionView.aspx.vb" Inherits="Fees_feeTranspotCollectionView" Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%--    <link href="../cssfiles/FeePopUp.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/Popup.css" rel="Stylesheet" type="text/css"></link>
    <link href="../Scripts/JQuery-ui-1.10.3.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>--%>
    <script type="text/javascript" lang="javascript">
        Sys.Application.add_load(
      function CheckForPrint() {
          if (document.getElementById('<%= h_print.ClientID %>').value != '') {
              if (isIE())
                  showModelessDialog('FeeReceiptTransport.aspx' + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
              else
                  Popup('FeeReceiptTransport.aspx' + document.getElementById('<%= h_print.ClientID %>').value);
              document.getElementById('<%= h_print.ClientID %>').value = '';
          }
      }
    );

      function disableBtn(btn) {
          document.getElementById(btn).disabled = true;
          return false;
      }
      function isIE() {
          ua = navigator.userAgent;
          /* MSIE used to detect old browsers and Trident used to newer ones*/
          var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

          return is_ie;
      }
    </script>
 <style>
        .RadComboBoxDropDown .rcbItem>label, .RadComboBoxDropDown .rcbHovered>label, .RadComboBoxDropDown .rcbDisabled>label, .RadComboBoxDropDown .rcbLoading>label, .RadComboBoxDropDown .rcbCheckAllItems>label, .RadComboBoxDropDown .rcbCheckAllItemsHovered>label {
    display: inline;
    float: left;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border:0!important;
}
.RadComboBox_Default .rcbInner {
    padding:10px;
    border-color: #dee2da!important;
    border-radius: 6px!important;
    box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
    width:80%;
    background-image: none !important;
    background-color:transparent !important;
}
.RadComboBox_Default .rcbInput {
    font-family:'Nunito', sans-serif !important;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border: 0!important;
    box-shadow: none;
}
.RadComboBox_Default .rcbActionButton {
    border: 0px;
    background-image: none !important;
    height:100% !important;
    color:transparent !important;
    background-color:transparent !important;
}
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:UpdatePanel runat="server" ID="UP1">
                    <ContentTemplate>



                        <table   width="100%">
                            <tr valign="top">
                                <td valign="top" align="left" colspan="2">
                                    <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                                    <asp:Label ID="lblError" runat="server" SkinID="LabelError"></asp:Label>
                                </td>
                            </tr>
                        </table>

                        <table  width="100%">
                            <tr valign="top">
                                <td align="left" valign="middle"  width="20%"><span class="field-label">Select Business Unit</span></td>
                                <td align="left" class="matters" valign="middle" width="30%">
                                    <telerik:RadComboBox ID="ddlBusinessunit" runat="server" Filter="Contains"   AutoPostBack="true" RenderMode="Lightweight" Width="100%"
                                         ZIndex="2000" ToolTip="Type in unit name or short code" DataSourceID="odsSERVICES_BSU_M" DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"></telerik:RadComboBox><br />
                                    
                                </td>

                                <td align="left" class="matters" valign="middle" width="10%">
                                    <telerik:RadComboBox ID="ddlTopFilter" runat="server" AutoPostBack="true" Filter="Contains" RenderMode="Lightweight" Width="100%" ZIndex="2000">
                                        <Items>
                                            <telerik:RadComboBoxItem runat="server" Value="10" Text="Top 10" />
                                            <telerik:RadComboBoxItem runat="server" Value="100" Text="Top 100" />
                                            <telerik:RadComboBoxItem runat="server" Text="All" />
                                        </Items>
                                    </telerik:RadComboBox></td>
                                <td align="left" class="matters" valign="middle" width="10%">
                                    <span class="field-label">Email Status</span></td>
                                <td align="left" width="30%">
                                    <asp:RadioButtonList ID="rblEmailStatus" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True" Value="0"><span class="field-label">All</span></asp:ListItem>
                                        <asp:ListItem Value="1"><span class="field-label">Emailed</span></asp:ListItem>
                                        <asp:ListItem Value="2"><span class="field-label">Failed</span></asp:ListItem>
                                    </asp:RadioButtonList></td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" colspan="5">
                                    <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                                        EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="25"  CssClass="table table-row table-bordered">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <%--<asp:CheckBox ID="chkSelectH" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelectH_CheckedChanged" />--%>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Docno">
                                                <HeaderTemplate>
                                                    Receipt#<br />
                                                                            <asp:TextBox ID="txtReceiptno" runat="server" SkinID="Gridtxt"  ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnReceiptSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                                OnClick="ImageButton1_Click" />

                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblReceipt" runat="server" Text='<%# Bind("FCL_RECNO") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_FCLID" Value='<%# Bind("FCL_ID") %>' runat="server" />
                                                    <asp:HiddenField ID="hf_Emailed" Value='<%# Bind("FCL_bEMAILED") %>' runat="server" />
                                                    <asp:HiddenField ID="hf_STU_NO" Value='<%# Bind("STU_NO")%>' runat="server" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <HeaderTemplate>
                                                    Date<br />
                                                                            <asp:TextBox ID="txtDate" runat="server" SkinID="Gridtxt"  ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                                OnClick="ImageButton1_Click" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("FCL_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Grade">
                                                <HeaderTemplate>
                                                    Grade<br />
                                                                            <asp:TextBox ID="txtGrade" runat="server" SkinID="Gridtxt" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnGradeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                                OnClick="ImageButton1_Click" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("GRD_DISPLAY") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Student ID">
                                                <HeaderTemplate>
                                                    Student ID<br />
                                                                            <asp:TextBox ID="txtStuno" runat="server" SkinID="Gridtxt"  ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnstunoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                                OnClick="ImageButton1_Click" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name">
                                                <HeaderTemplate>
                                                    Name<br />
                                                                            <asp:TextBox ID="txtStuname" runat="server" SkinID="Gridtxt" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnstunameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                                OnClick="ImageButton1_Click" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount">
                                                <HeaderTemplate>
                                                    Amount<br />
                                                                            <asp:TextBox ID="txtAmount" runat="server" SkinID="Gridtxt"  ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnAmountSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                                OnClick="ImageButton1_Click" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("FCL_AMOUNT") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <HeaderTemplate>
                                                    Description<br />
                                                                            <asp:TextBox ID="txtDesc" runat="server" SkinID="Gridtxt"  ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnDescSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                                OnClick="ImageButton1_Click" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("FCL_NARRATION") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Print Receipt">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbVoucher" OnClick="lbVoucher_Click" runat="server">Print</asp:LinkButton>
                                                    <br />
                                                    <asp:LinkButton ID="lbExport" runat="server" OnClick="lbExport_Click">ViewReceipt</asp:LinkButton>
                                                </ItemTemplate>

                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Emailed">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblbEmailed" runat="server" Text='<%# Bind("bEMAILED")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btngvEmail" runat="server" ToolTip="Email Receipt" AlternateText="EMAIL" ImageUrl="~/Images/Misc/email.gif" OnClick="btngvEmail_Click" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnDownload" runat="server" ToolTip="Download Receipt" AlternateText="DOWNLOAD" ImageUrl="~/Images/Misc/pdf.gif" OnClick="btnPDF_Click" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" class="matters" colspan="5">
                                    <asp:Button ID="btnEmail" runat="server" CssClass="button" Text="Send Email" />
                                    <%--<asp:Button ID="btnHidden" runat="server" Width="1pt" Height="1pt" Text="Send EMail" />--%>
                                    <%-- <ajaxToolkit:ModalPopupExtender ID="MPE1" TargetControlID="btnHidden" BackgroundCssClass="ModalPopupBG"
                            PopupControlID="pnlEmail" OkControlID="btnClose" CancelControlID="btnCael"
                            DropShadow="true" PopupDragHandleControlID="gridheader" Drag="true" runat="server">
                        </ajaxToolkit:ModalPopupExtender>--%>
                                </td>

                            </tr>
                        </table>

                        <div id="testpopup" class="darkPanelM" runat="server" style="display:none ;position: fixed;top: 100px;left: 45%;z-index: 1000;min-width: 292px;">
                            <div id="divboxpanelconfirm" runat="server" class="panel-cover" style="height: 50%;   margin-left: 10%; margin-top: 15%;">
                                <div class="holderInner" style="height: 90%; width: 99%;">
                                    <center>
                            <table  width="100%">
                                <tr class="title-bg">
                                    <td>
                                        <span class="title-bg" ></span>
                                        CONFIRM
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <span class="ui-icon ui-icon-info"  ></span>
                                        <asp:Label ID="lblMsg"  runat="server"></asp:Label><br />
                                        <br />
                                        <br />
                                        <center>
                                            <asp:GridView ID="gvEmailStatus" AutoGenerateColumns="False" ShowHeader="true" runat="server" cssclass="table table-row table-bordered"
                                                  AllowPaging="true" PageSize="10" EmptyDataText="">
                                                <Columns>
                                                    <asp:BoundField DataField="REC_NO" HeaderText="Receipt.No" />
                                                    <asp:BoundField DataField="STU_NO" HeaderText="Student.No" />
                                                    <asp:BoundField DataField="RET_MESSAGE" HeaderText="Message" />
                                                </Columns>
                                            </asp:GridView>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btnOkay" CssClass="button" runat="server" Text="Yes, Send to all" OnClientClick="this.disabled = true; this.value = 'Please wait...';"
                                            UseSubmitBehavior="false" />
                                        <asp:Button ID="btnEmailUnSent" CssClass="button" runat="server" Visible="false" Text="No, Send to remaining only" OnClientClick="this.disabled = true; this.value = 'Please wait...';" UseSubmitBehavior="false" />
                                        <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </center>
                                </div>
                            </div>
                        </div>
                        <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                            SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                            <SelectParameters>
                                <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                                <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                        <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                        <asp:HiddenField ID="h_selected_menu_3" runat="server" />
                        <asp:HiddenField ID="h_selected_menu_4" runat="server" />
                        <asp:HiddenField ID="h_selected_menu_5" runat="server" />
                        <asp:HiddenField ID="h_selected_menu_6" runat="server" />
                        <asp:HiddenField ID="h_selected_menu_7" runat="server" />
                        <asp:HiddenField ID="h_print" runat="server" />
                        <asp:HiddenField ID="h_Export" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        
 <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
