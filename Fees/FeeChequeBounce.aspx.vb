Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class FEES_FEECHEQUEBOUNCE
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            gvFEEReminder.Attributes.Add("bordercolor", "#1b80b6")
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CHEQUE_BOUNCE Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                txtFromDate.Text = Format(DateTime.Now.AddDays(-1 * DateTime.Now.Day + 1), OASISConstants.DateFormat)
                txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                'txtCHQBankDescr.Attributes.Add("ReadOnly", "ReadOnly")
                'txtCHQBankCode.Attributes.Add("ReadOnly", "ReadOnly")
                'txtBankDescr.Attributes.Add("ReadOnly", "ReadOnly")

                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                If ViewState("datamode") = "view" Then
                    Exit Sub
                End If
                'GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Sub chk_bankcode()
        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            'lblError.Text = "Invalid bank selected"
            usrMessageBar2.ShowNotification("Invalid bank selected", UserControls_usrMessageBar.WarningType.Danger)
            txtBankCode.Focus()
        Else
            'lblError.Text = ""
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEReminder.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvFEEReminder.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim ds As New DataSet
            Dim str_Filter As String = String.Empty
            Dim lstrOpr As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty

            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox

            If gvFEEReminder.Rows.Count > 0 Then
                '   -- 1   txtStuNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuNo")
                lstrCondn1 = Trim(txtSearch.Text.Trim)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn1)
                '   -- 1  txtStuname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuname")
                lstrCondn2 = txtSearch.Text.Trim
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn2)
                '   -- 1  txtChqno
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtChqno")
                lstrCondn3 = txtSearch.Text.Trim
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCQ_CHQNO", lstrCondn3)
                '   -- 1  txtRecno
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtRecno")
                lstrCondn4 = txtSearch.Text.Trim
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCL_RECNO", lstrCondn4)
                '   -- 1  txtBank
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtBank")
                lstrCondn5 = txtSearch.Text.Trim
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BNK_DESCRIPTION", lstrCondn5)
            End If
            'str_Sql = "SELECT * FROM FEES.vw_OSA_CHEQUEBOUNCELIST " & _
            '" WHERE FCL_BSU_ID = '" & Session("sBSUID") & "' " & _
            '"AND (FCQ_ID NOT IN " & _
            '" (SELECT FCR_FCQ_ID FROM OASIS.FEES.FEECHEQUERETURN_H))"
            'str_Sql = str_Sql & GetFilter() & str_Filter & " order by fcq_date desc,FCQ_ID"
            'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            ds = GetData(Session("sBsuid"), str_Filter)
            ViewState("gvFEEReminder") = ds.Tables(0)
            BindGrid()
            txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuNo")
            txtSearch.Text = lstrCondn1
            txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn2
            txtSearch = gvFEEReminder.HeaderRow.FindControl("txtChqno")
            txtSearch.Text = lstrCondn3
            txtSearch = gvFEEReminder.HeaderRow.FindControl("txtRecno")
            txtSearch.Text = lstrCondn4
            txtSearch = gvFEEReminder.HeaderRow.FindControl("txtBank")
            txtSearch.Text = lstrCondn5
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub BindGrid()
        Dim dt As DataTable = DirectCast(ViewState("gvFEEReminder"), DataTable)
        gvFEEReminder.DataSource = dt
        If dt.Rows.Count = 0 Then
            dt.Rows.Add(dt.NewRow())
            gvFEEReminder.DataBind()
            Dim columnCount As Integer = gvFEEReminder.Rows(0).Cells.Count
            gvFEEReminder.Rows(0).Cells.Clear()
            gvFEEReminder.Rows(0).Cells.Add(New TableCell)
            gvFEEReminder.Rows(0).Cells(0).ColumnSpan = columnCount
            gvFEEReminder.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvFEEReminder.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvFEEReminder.DataBind()
        End If
    End Sub

    Public Function GetFilter() As String
        Dim str_filter As String = String.Empty
        If txtFromDate.Text.Trim <> "" And txtToDate.Text <> "" Then
            str_filter = " AND FCQ_DATE BETWEEN '" & txtFromDate.Text & "' AND '" & txtToDate.Text & "' "
        End If
        Return str_filter
    End Function

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEReminder.PageIndexChanging
        gvFEEReminder.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar2.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim trans As SqlTransaction = objConn.BeginTransaction("FEE_CHQ_CANCEL")

        Dim dtDate As DateTime = CDate(txtDate.Text)
        Try
            Dim vFRH_ID As String
            Dim retVal As Integer
            For Each gvr As GridViewRow In gvFEEReminder.Rows
                Dim chkControl As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
                Dim lblFCL_STU_ID As Label = CType(gvr.FindControl("lblFCL_STU_ID"), Label)
                Dim lblFCQ_ID As Label = CType(gvr.FindControl("lblFCQ_ID"), Label)
                Dim lblFCL_STU_TYPE As Label = CType(gvr.FindControl("lblFCL_STU_TYPE"), Label)
                If Not chkControl Is Nothing And Not lblFCL_STU_ID Is Nothing And Not lblFCQ_ID Is Nothing Then
                    If chkControl.Checked Then
                        retVal = FEECHEQUEBOUNCE.F_SAVEFEECHEQUERETURN_H(0, txtDate.Text, Session("sBsuid"), lblFCL_STU_TYPE.Text, lblFCL_STU_ID.Text, _
                                           CDec(txtBankcharge.Text), txtNarration.Text, False, Session("sBsuid"), lblFCQ_ID.Text, vFRH_ID, trans)
                    End If
                    If retVal <> 0 Then
                        Exit For
                    End If
                End If
            Next
            If retVal <> 0 Then
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(retVal)
                usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                trans.Commit()
                'lblError.Text = "Data updated Successfully"
                usrMessageBar2.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                ViewState("datamode") = "none"
                txtNarration.Text = ""
                txtBankcharge.Text = ""
                GridBind()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim str_KEY As String = "INSERT"
                If ViewState("datamode") <> "edit" Then
                    str_KEY = "EDIT"
                End If
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable("", vFRH_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
            End If
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub ClearDetails()
        txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        'lblError.Text = ""
        txtBankcharge.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        ClearDetails()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub imgBank_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBank.Click
        chk_bankcode()
    End Sub

    Protected Sub txtBankCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBankCode.TextChanged
        chk_bankcode()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        GridBind()
    End Sub

    Private Function GetData(ByVal BSU_ID As String, ByVal FILTER As String) As DataSet
        Dim pParms(3) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID

        pParms(1) = New SqlClient.SqlParameter("@FROM_DT", SqlDbType.VarChar, 20)
        pParms(1).Value = txtFromDate.Text

        pParms(2) = New SqlClient.SqlParameter("@TO_DT", SqlDbType.VarChar, 20)
        pParms(2).Value = txtToDate.Text

        pParms(3) = New SqlClient.SqlParameter("@FILTER", SqlDbType.VarChar)
        pParms(3).Value = FILTER

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GET_CHEQUE_BOUNCE_LIST", pParms)
        If Not dsData Is Nothing Then
            Return dsData
        Else
            Return Nothing
        End If
    End Function

End Class
