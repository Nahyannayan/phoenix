<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FEEConcessionTransBB.aspx.vb" Inherits="Fees_FEEConcessionTransBB" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        <%-- function GetStudent() {
            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 750px; ";
            sFeatures += "dialogHeight: 675px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            url = "ShowStudentTransport.aspx?type=STUD&COMP_ID=-1&bsu=" + document.getElementById('<%= ddlBusinessunit.ClientID %>').value;
            result = window.showModalDialog(url, "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('||');
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtStud_Name.ClientID %>').value = NameandCode[1];
            document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];
            return true;
        }
        else {
            return false;
        }
    }--%>

        <%--function GETReference(HEAD_SUB) {
        var sFeatures;
        var sFeatures;
        var NameandCode;
        var result;
        var REF_TYPE;
        var STUD_ID;
        var url;
        var h_FCT_ID_HEAD = '<%=h_FCT_ID_HEAD.ClientID %>';
        var h_FCT_ID_DET = '<%=h_FCT_ID_DET.ClientID %>';
        var H_REFID_HEAD = '<%=H_REFID_HEAD.ClientID %>';
        var txtRefHEAD = '<%=txtRefHEAD.ClientID %>';
        var txtRefIDsub = '<%=txtRefIDsub.ClientID %>';
        var H_REFID_SUB = '<%=H_REFID_SUB.ClientID %>';
        if (HEAD_SUB == 0) {
            REF_TYPE = document.getElementById(h_FCT_ID_HEAD).value;
        }
        else {
            REF_TYPE = document.getElementById(h_FCT_ID_DET).value;
        }
        if (REF_TYPE == "") {
            alert('Please Select Concession Type')
            return false;
        }

        STUD_ID = document.getElementById('<%=h_STUD_ID.ClientID %>').value;
        if (STUD_ID == "") {
            alert('Please Select Student')
            return false;
        }
        if (REF_TYPE == 1) {
            sFeatures = "dialogWidth: 750px; ";
            sFeatures += "dialogHeight: 475px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            url = "ShowStudent.aspx?type=REFERBB&STU_ID=" + STUD_ID + "&bsu=" + document.getElementById('<%= ddlBusinessunit.ClientID %>').value;
         }
         else if (REF_TYPE == 2) {
             sFeatures = "dialogWidth: 460px; ";
             sFeatures += "dialogHeight: 370px; ";
             sFeatures += "help: no; ";
             sFeatures += "resizable: no; ";
             sFeatures += "scroll: no; ";
             sFeatures += "status: no; ";
             sFeatures += "unadorned: no; ";
             url = '../common/PopupFormIDhidden.aspx?iD=REFEP_CONC&MULTISELECT=FALSE&CONCTYP=' + REF_TYPE + '&CONC_FCM_ID=' + document.getElementById('<%= h_FCM_ID_HEAD.ClientID %>').value;
        }
        else {
            return false;
        }
    result = window.showModalDialog(url, "", sFeatures)
    if (result != '' && result != undefined) {
        if (HEAD_SUB == 0) {
            if (REF_TYPE == 1) {
                NameandCode = result.split('||');
                document.getElementById(H_REFID_HEAD).value = NameandCode[0];
                document.getElementById(txtRefHEAD).value = NameandCode[1];
                if (document.getElementById(txtRefIDsub).value == '' && (document.getElementById(h_FCT_ID_DET).value == document.getElementById(h_FCT_ID_HEAD).value)) {
                    document.getElementById(H_REFID_SUB).value = NameandCode[0];
                    document.getElementById(txtRefIDsub).value = NameandCode[1];
                }
            }
            else if (REF_TYPE == 2) {
                NameandCode = result.split('___');
                document.getElementById(H_REFID_HEAD).value = NameandCode[0];
                document.getElementById(txtRefHEAD).value = NameandCode[1];
                if (document.getElementById(txtRefIDsub).value == '' && (document.getElementById(h_FCT_ID_DET).value == document.getElementById(h_FCT_ID_HEAD).value)) {
                    document.getElementById(H_REFID_SUB).value = NameandCode[0];
                    document.getElementById(txtRefIDsub).value = NameandCode[1];
                }
            }
        }
        else {
            if (REF_TYPE == 1) {
                NameandCode = result.split('||');
                document.getElementById(H_REFID_SUB).value = NameandCode[0];
                document.getElementById(txtRefIDsub).value = NameandCode[1];
            }
            else if (REF_TYPE == 2) {
                NameandCode = result.split('___');
                document.getElementById(H_REFID_SUB).value = NameandCode[0];
                document.getElementById(txtRefIDsub).value = NameandCode[1];
            }
        }
        return false;
    }
    else {
        return false;
    }
}--%>
        <%--  function GetConcession(HEAD) {
            var sFeatures, url;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 500px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            url = "../common/PopupFormIDhidden.aspx?iD=CONCESSION&MULTISELECT=FALSE";
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            if (HEAD == 1) {
                document.getElementById('<%= h_FCT_ID_HEAD.ClientID %>').value = NameandCode[0];
        document.getElementById('<%= h_FCM_ID_HEAD.ClientID %>').value = NameandCode[1];
        document.getElementById('<%= txtConcession_Head.ClientID %>').value = NameandCode[2];
        document.getElementById('<%= H_REFID_HEAD.ClientID %>').value = '';
        document.getElementById('<%= txtRefHEAD.ClientID %>').value = '';


        document.getElementById('<%= h_FCT_ID_DET.ClientID %>').value = NameandCode[0];
        document.getElementById('<%= h_FCM_ID_DET.ClientID %>').value = NameandCode[1];
        document.getElementById('<%= txtConcession_Det.ClientID %>').value = NameandCode[2];
        document.getElementById('<%= H_REFID_SUB.ClientID %>').value = '';
        document.getElementById('<%= txtRefIDsub.ClientID %>').value = '';

        if (document.getElementById('<%= txtConcession_Det.ClientID %>').value == '') {
            document.getElementById('<%= h_FCT_ID_DET.ClientID %>').value = NameandCode[0];
            document.getElementById('<%= h_FCM_ID_DET.ClientID %>').value = NameandCode[1];
            document.getElementById('<%= txtConcession_Det.ClientID %>').value = NameandCode[2];
        }
    }
    else {
        document.getElementById('<%= h_FCT_ID_DET.ClientID %>').value = NameandCode[0];
        document.getElementById('<%= h_FCM_ID_DET.ClientID %>').value = NameandCode[1];
        document.getElementById('<%= txtConcession_Det.ClientID %>').value = NameandCode[2];
        document.getElementById('<%= H_REFID_SUB.ClientID %>').value = '';
        document.getElementById('<%= txtRefIDsub.ClientID %>').value = '';
    }
    return false;
}--%>
<%--function getLOCATION() {
    var sFeatures, url;
    sFeatures = "dialogWidth: 600px; ";
    sFeatures += "dialogHeight: 500px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;

    url = "../common/PopupFormIDhidden.aspx?iD=LOCATION&bsu=" + document.getElementById('<%= ddlBusinessunit.ClientID %>').value + "&MULTISELECT=FALSE";
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%= H_Location.ClientID %>').value = NameandCode[0];
        document.getElementById('<%= txtLocation.ClientID %>').value = NameandCode[1];
            return true;
        }--%>
        Sys.Application.add_load(
                  function CheckForPrint() {
                      if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                          document.getElementById('<%= h_print.ClientID %>').value = '';
                          showModalDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                      }
                  }
                    );

                  function autoSizeWithCalendar(oWindow) {
                      var iframe = oWindow.get_contentFrame();
                      var body = iframe.contentWindow.document.body;

                      var height = body.scrollHeight;
                      var width = body.scrollWidth;

                      var iframeBounds = $telerik.getBounds(iframe);
                      var heightDelta = height - iframeBounds.height;
                      var widthDelta = width - iframeBounds.width;

                      if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                      if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                      oWindow.center();
                  }

    </script>

    <script>
        function GetStudent() {

            url = "ShowStudentTransport.aspx?type=STUD&COMP_ID=-1&bsu=" + document.getElementById('<%= ddlBusinessunit.ClientID %>').value;
            var oWnd = radopen(url, "pop_student");

        }


        function OnClientClose1(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStud_Name.ClientID %>').value = NameandCode[1];
                document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];
                __doPostBack('<%= txtStdNo.ClientID%>', 'TextChanged');
            }
        }
        function GETReference(HEAD_SUB) {
            var REF_TYPE;
            var STUD_ID;
            var url;
            var h_FCT_ID_HEAD = '<%=h_FCT_ID_HEAD.ClientID %>';
            var h_FCT_ID_DET = '<%=h_FCT_ID_DET.ClientID %>';
            var H_REFID_HEAD = '<%=H_REFID_HEAD.ClientID %>';
            var txtRefHEAD = '<%=txtRefHEAD.ClientID %>';
            var txtRefIDsub = '<%=txtRefIDsub.ClientID %>';
            var H_REFID_SUB = '<%=H_REFID_SUB.ClientID %>';

            if (HEAD_SUB == 0) {
                REF_TYPE = document.getElementById(h_FCT_ID_HEAD).value;
            }
            else {
                REF_TYPE = document.getElementById(h_FCT_ID_DET).value;
            }

            if (REF_TYPE == "") {
                alert('Please Select Concession Type')
                return false;
            }

            STUD_ID = document.getElementById('<%=h_STUD_ID.ClientID %>').value;
            if (STUD_ID == "") {
                alert('Please Select Student')
                return false;
            }
            if (REF_TYPE == 1) {
                url = "ShowStudent.aspx?type=REFERBB&STU_ID=" + STUD_ID + "&Stu_Bsu_Id=" + document.getElementById('<%= ddlBusinessunit.ClientID %>').value + "&bsu=" + document.getElementById('<%= ddlBusinessunit.ClientID %>').value;
                
            }
            else if (REF_TYPE == 2) {
                url = '../common/PopupFormIDhidden.aspx?iD=REFEP_CONC&MULTISELECT=FALSE&CONCTYP=' + REF_TYPE + '&CONC_FCM_ID=' + document.getElementById('<%= h_FCM_ID_HEAD.ClientID %>').value;
               
            }
            else {
                return false;
            }
            document.getElementById('<%= hf_HEAD_SUB.ClientID%>').value = HEAD_SUB
            var oWnd = radopen(url, "pop_GETReference");
        }
        function OnClientClose2(oWnd, args) {
            var REF_TYPE;
            var h_FCT_ID_HEAD = '<%=h_FCT_ID_HEAD.ClientID %>';
            var h_FCT_ID_DET = '<%=h_FCT_ID_DET.ClientID %>';
            var H_REFID_HEAD = '<%=H_REFID_HEAD.ClientID %>';
            var txtRefHEAD = '<%=txtRefHEAD.ClientID %>';
            var txtRefIDsub = '<%=txtRefIDsub.ClientID %>';
            var H_REFID_SUB = '<%=H_REFID_SUB.ClientID %>';
            var HEAD_SUB = document.getElementById('<%= hf_HEAD_SUB.ClientID%>').value;
            //get the transferred arguments
            if (HEAD_SUB == 0) {
                REF_TYPE = document.getElementById(h_FCT_ID_HEAD).value;
            }
            else {
                REF_TYPE = document.getElementById(h_FCT_ID_DET).value;
            }

            var arg = args.get_argument();
            if (arg) {

                if (HEAD_SUB == 0) {
                    if (REF_TYPE == 1) {
                        NameandCode = arg.NameandCode.split('||');
                        document.getElementById(H_REFID_HEAD).value = NameandCode[0];
                        document.getElementById(txtRefHEAD).value = NameandCode[1];
                        if (document.getElementById(txtRefIDsub).value == '' && (document.getElementById(h_FCT_ID_DET).value == document.getElementById(h_FCT_ID_HEAD).value)) {
                            document.getElementById(H_REFID_SUB).value = NameandCode[0];
                            document.getElementById(txtRefIDsub).value = NameandCode[1];
                        }
                    }
                    else if (REF_TYPE == 2) {
                        NameandCode = arg.NameCode.split('||');
                        document.getElementById(H_REFID_HEAD).value = NameandCode[0];
                        document.getElementById(txtRefHEAD).value = NameandCode[1];
                        if (document.getElementById(txtRefIDsub).value == '' && (document.getElementById(h_FCT_ID_DET).value == document.getElementById(h_FCT_ID_HEAD).value)) {
                            document.getElementById(H_REFID_SUB).value = NameandCode[0];
                            document.getElementById(txtRefIDsub).value = NameandCode[1];
                        }
                    }
                }
                else {
                    if (REF_TYPE == 1) {
                        NameandCode = arg.NameandCode.split('||');
                        document.getElementById(H_REFID_SUB).value = NameandCode[0];
                        document.getElementById(txtRefIDsub).value = NameandCode[1];
                    }
                    else if (REF_TYPE == 2) {
                        NameandCode = arg.NameCode.split('||');
                        document.getElementById(H_REFID_SUB).value = NameandCode[0];
                        document.getElementById(txtRefIDsub).value = NameandCode[1];
                    }
                }
            }
        }

        function getLOCATION() {
            var url;


            url = "../common/PopupFormIDhidden.aspx?iD=LOCATION&bsu=" + document.getElementById('<%= ddlBusinessunit.ClientID %>').value + "&MULTISELECT=FALSE";
        var oWnd = radopen(url, "pop_getLOCATION");

    }

    function OnClientClose3(oWnd, args) {

        //get the transferred arguments
        var arg = args.get_argument();
        if (arg) {

            NameandCode = arg.NameandCode.split('||');

            document.getElementById('<%= H_Location.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= txtLocation.ClientID %>').value = NameandCode[1];

            }
        }

        function GetConcession(HEAD) {
            document.getElementById('<%= hf_HEAD_SUB.ClientID%>').value = HEAD

            var url;
            url = "../common/PopupFormIDhidden.aspx?iD=CONCESSION&MULTISELECT=FALSE";
            var oWnd = radopen(url, "pop_GetConcession");
        }

        function OnClientClose4(oWnd, args) {
            var HEAD = document.getElementById('<%= hf_HEAD_SUB.ClientID%>').value;
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameCode.split('||');

                if (HEAD == 1) {
                    document.getElementById('<%= h_FCT_ID_HEAD.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%= h_FCM_ID_HEAD.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%= txtConcession_Head.ClientID %>').value = NameandCode[2];
                    document.getElementById('<%= H_REFID_HEAD.ClientID %>').value = '';
                    document.getElementById('<%= txtRefHEAD.ClientID %>').value = '';


                    document.getElementById('<%= h_FCT_ID_DET.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%= h_FCM_ID_DET.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%= txtConcession_Det.ClientID %>').value = NameandCode[2];
                    document.getElementById('<%= H_REFID_SUB.ClientID %>').value = '';
                    document.getElementById('<%= txtRefIDsub.ClientID %>').value = '';

                    if (document.getElementById('<%= txtConcession_Det.ClientID %>').value == '') {
                        document.getElementById('<%= h_FCT_ID_DET.ClientID %>').value = NameandCode[0];
                         document.getElementById('<%= h_FCM_ID_DET.ClientID %>').value = NameandCode[1];
                         document.getElementById('<%= txtConcession_Det.ClientID %>').value = NameandCode[2];
                     }
                 }
                 else {
                     document.getElementById('<%= h_FCT_ID_DET.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%= h_FCM_ID_DET.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%= txtConcession_Det.ClientID %>').value = NameandCode[2];
                    document.getElementById('<%= H_REFID_SUB.ClientID %>').value = '';
                    document.getElementById('<%= txtRefIDsub.ClientID %>').value = '';
                }


            }
        }


    </script>
    <asp:ValidationSummary ID="ValidationSummary3" runat="server" CssClass="error" ValidationGroup="MAINERROR" />

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_clickforprint" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

        <Windows>
            <telerik:RadWindow ID="pop_GETReference" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getLOCATION" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_GetConcession" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Transport Fee Concession
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <link href="../cssfiles/Popup.css?1=2" type="text/css" rel="stylesheet" />


                <table width="100%" align="center">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="VALSUB" CssClass="error" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" ValidationGroup="VALMAIN" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select Business Unit</span></td>
                        <td align="left" colspan="2" valign="middle">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" SkinID="DropDownListNormal"
                                TabIndex="5">
                            </asp:DropDownList>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList
                                ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="Image1" runat="Server" AlternateText="Click to show calendar"
                                ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtDate"
                                ErrorMessage="Date Required" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="VALMAIN">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">From Date</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtFromDT" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="ImagefromDate" runat="server" AlternateText="Click to show calendar"
                                ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFromDT"
                                ErrorMessage="From Date Reqired" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtFromDT"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="VALMAIN">*</asp:RegularExpressionValidator></td>
                        <td align="left"><span class="field-label">To Date</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtToDT" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgToDT" runat="server" AlternateText="Click to show calendar"
                                ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtToDT"
                                ErrorMessage="To Date Required" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtToDT"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="VALMAIN">*</asp:RegularExpressionValidator>
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="txtToDT"
                                ControlToValidate="txtFromDT" ErrorMessage="From Date should be less than To Date"
                                Operator="LessThan" Type="Date" ValidationGroup="VALMAIN" Enabled="False">*</asp:CompareValidator></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Student Code</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtStdNo" runat="server" AutoPostBack="True" OnTextChanged="txtStdNo_TextChanged"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgCompany" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetStudent(); return false;" />
                        </td>
                        <td align="left"><span class="field-label">Student Name</span></td>
                        <td>
                            <asp:TextBox ID="txtStud_Name" runat="server"></asp:TextBox>&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtStud_Name"
                    ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator><br />
                            <asp:CheckBox ID="chkCopyPrev" runat="server" Text="Copy From Previous Academic Year" AutoPostBack="True" Enabled="False" Visible="False" />
                            <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" PopupControlID="Panel1"
                                Position="Left" TargetControlID="lbHistory">
                            </ajaxToolkit:PopupControlExtender>
                        </td>

                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Area</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtLocation" runat="server" AutoPostBack="True"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgLocation" runat="server" Height="25px" ImageAlign="AbsMiddle"
                    ImageUrl="~/Images/Misc/Route.png" OnClientClick="getLOCATION();return false;" TabIndex="30"
                    Width="24px" Visible="False" /></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Concession Type</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtConcession_Head" runat="server"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgConcession_Head" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetConcession(1);return false;" /></td>

                        <td align="left"><span class="field-label">Ref. Details</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtRefHEAD" runat="server"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgRefHead" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GETReference(0);return false;" /></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Narration</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="445px" SkinID="MultiText"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtRemarks"
                                ErrorMessage="Please specify remarks" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left"></td>
                        <td align="left" colspan="3"></td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg" colspan="4">Concession Details 
                <asp:LinkButton ID="lbHistory" runat="server" OnClientClick="return false;">(View History)</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Concession Type</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtConcession_Det" runat="server"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgConcession_Det" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetConcession(0);return false;" /></td>
                        <td align="left"><span class="field-label">Ref. Details</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtRefIDsub" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgRefSub" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GETReference(1);return false;" /></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Fee Type</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlFeeType" runat="server" AutoPostBack="True" SkinID="DropDownListNormal" DataSourceID="odsGetFEETYPE_M" DataTextField="FEE_DESCR" DataValueField="FEE_ID">
                            </asp:DropDownList>
                        </td>
                        <td align="left"><span class="field-label">Total Fee</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtTotalFee" runat="server" AutoCompleteType="disabled" Width="122px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Charge Type</span></td>
                        <td align="left">
                            <asp:RadioButton ID="radAmount" runat="server" Text="Amount" ValidationGroup="AMTTYPE" GroupName="ChargeType" CssClass="field-label" />
                            <asp:RadioButton ID="radPercentage" runat="server" Text="Percentage" ValidationGroup="AMTTYPE" GroupName="ChargeType" CssClass="field-label" /></td>
                        <td align="left"><span class="field-label">Amount/Percentage</span> </td>
                        <td align="left">
                            <asp:TextBox ID="txtAmount" runat="server" AutoCompleteType="disabled"></asp:TextBox>
                            <asp:LinkButton ID="lnkFill" runat="server">Auto Fill</asp:LinkButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Amount required" ControlToValidate="txtAmount" ValidationGroup="VALSUB">*</asp:RequiredFieldValidator>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Amount should be valid"
                    Operator="DataTypeCheck" Type="Double" ControlToValidate="txtAmount" ValidationGroup="VALSUB">*</asp:CompareValidator></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Label ID="lblAlert" CssClass="alert alert-info" runat="server"></asp:Label></td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="left" class="title-bg" colspan="4">Monthly/Termly Split up</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvMonthly" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" CellPadding="4" SkinID="GridViewNormal">
                                <Columns>
                                    <asp:BoundField DataField="DESCR" HeaderText="Term/Month" InsertVisible="False" ReadOnly="True" />
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmount" runat="server" AutoCompleteType="Disabled" Text='<%# Bind("CUR_AMOUNT") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Charge Dt.">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>

                                            <asp:TextBox ID="txtChargeDate" runat="server" Text='<%# Bind("FDD_DATE" ) %>' Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="imgChargeDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                TabIndex="4" />
                                            <ajaxToolkit:CalendarExtender ID="CalChargeDate" runat="server" CssClass="MyCalendar"
                                                Format="dd/MMM/yyyy" PopupButtonID="imgChargeDate" PopupPosition="TopLeft" TargetControlID="txtChargeDate">
                                            </ajaxToolkit:CalendarExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FDD_AMOUNT" HeaderText="Actual Amount">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="ID" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="VALSUB" Visible="False" /><asp:Button ID="btnDetCancel" runat="server" CssClass="button" Text="Cancel" Visible="False" />
                            <asp:GridView ID="gvFeeDetails" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Data Added Yet..." SkinID="GridViewNormal" Visible="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="FEE_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFCD_ID" runat="server" Text='<%# bind("FCD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Concession Type">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# bind("FCM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fee Type">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# bind("FEE_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount Type">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# bind("AMT_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# bind("AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" Visible="False">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBtnDelete" runat="server" OnClick="lnkBtnDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" /><asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" /><asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button" Text="Delete" OnClick="btnDelete_Click" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');" /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="VALMAIN" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" OnClick="btnPrint_Click" /></td>
                    </tr>
                </table>
                <asp:Button ID="hiddenTargetControlForModalPopup" runat="server" Style="display: none" />&nbsp;
    <ajaxToolkit:ModalPopupExtender
        ID="programmaticModalPopup" runat="server" BackgroundCssClass="modalBackground"
        BehaviorID="programmaticModalPopupBehavior" DropShadow="True" PopupControlID="programmaticPopup"
        PopupDragHandleControlID="programmaticPopupDragHandle" RepositionMode="RepositionOnWindowScroll"
        TargetControlID="hiddenTargetControlForModalPopup">
    </ajaxToolkit:ModalPopupExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="Image1" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="Calendarextender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDT" TargetControlID="txtToDT">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="Calendarextender2" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="ImagefromDate" TargetControlID="txtFromDT">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtToDT">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtFromDT">
                </ajaxToolkit:CalendarExtender>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsGetFEETYPE_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetFEES_M_TRANSPORT" TypeName="FeeCommon"></asp:ObjectDataSource>
                <asp:HiddenField ID="h_FCT_ID_HEAD" runat="server" />
                <asp:HiddenField ID="h_FCM_ID_HEAD" runat="server" />
                <asp:HiddenField ID="h_FCM_ID_DET" runat="server" />
                <asp:HiddenField ID="H_REFID_HEAD" runat="server" />
                <asp:HiddenField ID="h_FCT_ID_DET" runat="server" />
                <asp:HiddenField ID="H_REFID_SUB" runat="server" />
                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <asp:HiddenField ID="H_Location" runat="server" />
                <asp:HiddenField ID="h_print" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hf_HEAD_SUB" runat="server" />
                <asp:Panel ID="Panel1" runat="server" Style="display: block">
                    <table cellpadding="3" cellspacing="0">
                        <tr>
                            <td align="left">
                                <asp:GridView ID="gvHistory" runat="server" AutoGenerateColumns="False" SkinID="GridViewView">
                                    <Columns>
                                        <asp:BoundField DataField="FCH_DT" HeaderText="Date" SortExpression="FCH_DT" />
                                        <asp:BoundField DataField="FCD_AMOUNT" HeaderText="Amount" SortExpression="FCD_AMOUNT" />
                                        <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee" SortExpression="FEE_DESCR" />
                                        <asp:BoundField DataField="FCM_DESCR" HeaderText="Concession" SortExpression="FCM_DESCR" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="programmaticPopup" runat="server" CssClass="screenCenter" Width="276px" Style="display: none">
                    <br />
                    <br />
                    There is already one or more concession availed for the Reference Selected.Please
        check the history.<br />
                    <div align="center">
                        <asp:Button ID="ButtonOk" runat="server" CssClass="button" Text="OK" Width="29px" />&nbsp;
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>

