﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Partial Class Fees_Emp_TuitionFeeConcessionFinList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
            Try
                ViewState("MainMnu_code") = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+")))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+")))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "F351077" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    'Select Case ViewState("MainMnu_code").ToString
                    '    Case OASISConstants.MNU_FEE_COLLECTION

                    '        ViewState("trantype") = "A"
                    'End Select

                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Protected Sub gridbind()

        Dim APPROVAL_STATUS As String = ""
        If rad1.Checked Then
            APPROVAL_STATUS = "ALL"
        ElseIf rad2.Checked Then
            APPROVAL_STATUS = "A"
        ElseIf rad3.Checked Then
            APPROVAL_STATUS = "R"
        ElseIf rad4.Checked Then
            APPROVAL_STATUS = "P"
        End If


        Dim str_Filter As String = ""

        Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7, lstrCondn8 As String

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        Dim CurBsUnit As String = Session("sBsuid")
        lstrCondn1 = ""
        lstrCondn2 = ""
        lstrCondn3 = ""
        lstrCondn4 = ""
        lstrCondn5 = ""
        lstrCondn6 = ""
        lstrCondn7 = ""
        lstrCondn8 = ""
        str_Filter = ""

        If gvDetails.Rows.Count > 0 Then
            ' --- Initialize The Variables
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            '   --- FILTER CONDITIONS ---
            '   -- 1   txtReceiptno FCL_RECNO
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtEmployeeId")
            lstrCondn1 = Trim(Mainclass.cleanString(txtSearch.Text))
            If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_ID", lstrCondn1)

            '   -- 2  txtDate
            larrSearchOpr = h_selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtEmployeeNo")
            lstrCondn2 = Trim(Mainclass.cleanString(txtSearch.Text))
            If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPNO", lstrCondn2)

            '   -- 3  txtGrade
            larrSearchOpr = h_selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtEmployeeName")
            lstrCondn3 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMPLOYEE_NAME", lstrCondn3)

            '   -- 4   txtStuno
            larrSearchOpr = h_selected_menu_4.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtDate")
            lstrCondn4 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRP_JOIN_DATE", lstrCondn4)

            '   -- 5  txtStuname
            larrSearchOpr = h_selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtDesignation")
            lstrCondn5 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "DESIGNATION", lstrCondn5)

            '   -- 6  txtAmount
            larrSearchOpr = h_selected_menu_6.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtDepartment")
            lstrCondn6 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "DPT_DESCR", lstrCondn6)

            '   -- 8  txtGrade
            larrSearchOpr = h_selected_menu_8.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtMobileNumber")
            lstrCondn8 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn8 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MOBILE_NUMBER", lstrCondn8)



        End If

        Dim con As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim param(4) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@OPTION", 2)
        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        param(2) = New SqlClient.SqlParameter("@APPROVAL_STATUS", APPROVAL_STATUS)
        param(3) = New SqlClient.SqlParameter("@FILTER_BY", str_Filter)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[FEES].[GET_ESS_TUITION_FEE_CONCESSION_LIST]", param)

        gvDetails.DataSource = ds.Tables(0)
        gvDetails.DataBind()


        If gvDetails.Rows.Count > 0 Then

            txtSearch = gvDetails.HeaderRow.FindControl("txtEmployeeId")
            txtSearch.Text = lstrCondn1

            txtSearch = gvDetails.HeaderRow.FindControl("txtEmployeeNo")
            txtSearch.Text = lstrCondn2

            txtSearch = gvDetails.HeaderRow.FindControl("txtEmployeeName")
            txtSearch.Text = lstrCondn3

            txtSearch = gvDetails.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn4

            txtSearch = gvDetails.HeaderRow.FindControl("txtDesignation")
            txtSearch.Text = lstrCondn5

            txtSearch = gvDetails.HeaderRow.FindControl("txtDepartment")
            txtSearch.Text = lstrCondn6

            txtSearch = gvDetails.HeaderRow.FindControl("txtMobileNumber")
            txtSearch.Text = lstrCondn8


        End If
    End Sub

    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        gvDetails.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub hlview_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblEmployeeId As New Label
            Dim url As String
            Dim viewid As String

            lblEmployeeId = TryCast(sender.FindControl("lblEmployeeId"), Label)
            viewid = lblEmployeeId.Text

            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String

            Dim MainMnu_code As String = Request.QueryString("MainMnu_code")
            Dim Mnucode As String = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Fees\Emp_TuitionFeeConcessionFinApproval.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)


            Response.Redirect(url)
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
End Class
