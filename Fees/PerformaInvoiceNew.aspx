﻿<%@ Page Title="" Language="VB" Theme="General" MasterPageFile="~/mainMasterPage.master" EnableViewState="true" AutoEventWireup="false" CodeFile="PerformaInvoiceNew.aspx.vb" Inherits="Fees_PerformaInvoiceNew" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div>


        <style>
            .RadComboBox_Default .rcbReadOnly {
                background-image: none !important;
                background-color: transparent !important;
            }

            .RadComboBox_Default .rcbDisabled {
                background-color: rgba(0,0,0,0.01) !important;
            }

                .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
                    background-color: transparent !important;
                    border-radius: 0px !important;
                    border: 0px !important;
                    padding: initial !important;
                    box-shadow: none !important;
                }

            .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
                display: inline;
                float: left;
            }

            .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
                border: 0 !important;
            }

            .RadComboBox_Default .rcbInner {
                padding: 10px;
                border-color: #dee2da !important;
                border-radius: 6px !important;
                box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
                width: 80%;
            }

            .RadComboBox_Default .rcbInput {
                font-family: 'Nunito', sans-serif !important;
            }

            .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
                border: 0 !important;
                box-shadow: none;
            }

            .RadComboBox_Default .rcbActionButton {
                border: 0px;
                background-image: none !important;
                height: 100% !important;
                color: transparent !important;
                background-color: transparent !important;
            }

            .darkPanlAlumini {
                width: 100%;
                height: 100%;
                position: fixed;
                left: 0%;
                top: 0%;
                background: rgba(0,0,0,0.2) !important;
                /*display: none;*/
                display: block;
            }

            .inner_darkPanlAlumini {
                left: 10%;
                top: 10%;
                position: fixed;
                width: 100%;
            }
        </style>
        <script type="text/javascript">

            Sys.Application.add_load(
                function CheckForPrint() {
                    if ($("#<%= h_print.ClientID %>").val() != '') {
                        $('#<%= h_print.ClientID %>').val('');
                        var result = radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up3');
                        return false;
                    }
                });

                $(document).ready(function () {
                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    prm.add_initializeRequest(InitializeRequest);
                    prm.add_endRequest(EndRequest);
                    //CheckForPrint();
                    InitCompanyAutoComplete();
                    InitStudentAutoComplete();
                });
                function InitializeRequest(sender, args) {
                }
                function EndRequest(sender, args) {
                    // after update occur on UpdatePanel re-init the Autocomplete
                    InitCompanyAutoComplete();
                    InitStudentAutoComplete();
                    //CheckForPrint();
                }

                function InitCompanyAutoComplete() {
                    $("#<%=txtCompanyDescr.ClientID%>").autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: "/PHOENIXBETA/Fees/PerformaInvoiceNew.aspx/GetCompany",
                                data: "{ 'prefix': '" + request.term + "'}",
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    response($.map(data.d, function (item) {
                                        return {
                                            label: item.split('-')[0],
                                            val: item.split('-')[1]
                                        }
                                    }))
                                },
                                error: function (response) {
                                    alert(response.responseText);
                                },
                                failure: function (response) {
                                    alert(response.responseText);
                                }
                            });
                        },
                        select: function (e, i) {
                            $("#<%=h_Company_ID.ClientID%>").val(i.item.val);
                        },
                        minLength: 1
                    });
                    }
                    function InitStudentAutoComplete() {
                        var GroupSel = $('#<%=chkGroupInvoice.ClientID %>').is(':checked');


                        $("#<%=txtStuNo.ClientID%>").autocomplete({
                            source: function (request, response) {
                                $.ajax({
                                    url: "/PHOENIXBETA/Fees/ShowStudentMulti.aspx/GetStudents",
                                    data: "{ 'prefix': '" + request.term + "','type':'" + getType() + "','COMP_ID':'" + getCompanyId() + "','bNewStudent':'" + $('#<%=chkNewStudent.ClientID%>').is(':checked') + "','ACD_ID':'" + $('#<%=ddlAcademicYear.ClientID %> option:selected').val() + "','IsService':'" + $('#<%=chkService.ClientID %>').is(':checked') + "','EXL_TYPE':'PI','EXCL_TC':'" + $('#<%=chkExcludeTC.ClientID%>').is(':checked') + "','FeeTypes':'" + $('#<%=h_FeeTypes.ClientID%>').val() + "','SearchFor':'STU_NO'}",
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    response($.map(data.d, function (item) {
                                        return {
                                            label: item.split('|')[0],
                                            val: item.split('|')[1],
                                            name: item.split('|')[2],
                                            grade: item.split('|')[3],
                                            stugrdid: item.split('|')[4]
                                        }
                                    }))
                                },
                                error: function (response) {
                                    alert(response.responseText);
                                },
                                failure: function (response) {
                                    alert(response.responseText);
                                }
                            });
                        },
                        select: function (e, i) {
                            $("#<%=h_Students.ClientID%>").val(i.item.val);
                            $("#<%=txtStuName.ClientID%>").val(i.item.name);
                            //$("#<%=lblGrade.ClientID%>").text(i.item.grade);
                            $("#<%=h_STU_GRD_ID.ClientID%>").val(i.item.stugrdid);
                        },
                        minLength: 1
                    });
                        //For student Name autocomplete
                    $("#<%=txtStuName.ClientID%>").autocomplete({
                            source: function (request, response) {
                                $.ajax({
                                    url: "/PHOENIXBETA/Fees/ShowStudentMulti.aspx/GetStudents",
                                    data: "{ 'prefix': '" + request.term + "','type':'" + getType() + "','COMP_ID':'" + getCompanyId() + "','bNewStudent':'" + $('#<%=chkNewStudent.ClientID%>').is(':checked') + "','ACD_ID':'" + $('#<%=ddlAcademicYear.ClientID %> option:selected').val() + "','IsService':'" + $('#<%=chkService.ClientID %>').is(':checked') + "','EXL_TYPE':'PI','EXCL_TC':'" + $('#<%=chkExcludeTC.ClientID%>').is(':checked') + "','FeeTypes':'" + $('#<%=h_FeeTypes.ClientID%>').val() + "','SearchFor':'STU_NAME'}",
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    response($.map(data.d, function (item) {
                                        return {
                                            label: item.split('|')[0],
                                            val: item.split('|')[1],
                                            stuno: item.split('|')[2],
                                            grade: item.split('|')[3],
                                            stugrdid: item.split('|')[4]
                                        }
                                    }))
                                },
                                error: function (response) {
                                    alert(response.responseText);
                                },
                                failure: function (response) {
                                    alert(response.responseText);
                                }
                            });
                        },
                        select: function (e, i) {
                            $("#<%=h_Students.ClientID%>").val(i.item.val);
                            $("#<%=txtStuNo.ClientID%>").val(i.item.stuno);
                            //$("#<%=lblGrade.ClientID%>").text(i.item.grade);
                            $("#<%=h_STU_GRD_ID.ClientID%>").val(i.item.stugrdid);
                        },
                        minLength: 1
                    });
                }
                function getType() {
                    var IsEnquiry = $('#<%=chkEnquiry.ClientID%>').is(':checked');
                var TYPE = "STUD_COMP";
                if (IsEnquiry == true)
                    TYPE = "ENQ_COMP";
                return TYPE;
            }
            function getCompanyId() {
                var CompFilter = $('#<%=chkCompanyFilter.ClientID%>').is(':checked');
                var COMP_ID = $('#<%=h_Company_ID.ClientID %>').val();
                if (CompFilter != true) {
                    COMP_ID = "-1";
                }
                return COMP_ID;
            }
            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;
                var height = body.scrollHeight;
                var width = body.scrollWidth;
                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;
                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }

            function GetCompany() {

                var NameandCode;
                var result;
                var url = "../Common/popupForm.aspx?ID=COMPANY&multiSelect=false";
                result = radopen(url, "pop_up");
                <%--if (result != '' && result != undefined) {
                    NameandCode = result.split('___');
                    document.getElementById('<%=h_Company_ID.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtCompanyDescr.ClientID %>').value = NameandCode[1];
                    return true;
                }
                else {
                    return false;
                }--%>
            }

            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    document.getElementById('<%=h_Company_ID.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtCompanyDescr.ClientID %>').value = NameandCode[1];
                }
            }

            function client_OnTreeNodeChecked() {
                var obj = window.event.srcElement;
                var treeNodeFound = false;
                var checkedState;
                if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                    var treeNode = obj;
                    checkedState = treeNode.checked;
                    do {
                        obj = obj.parentElement;
                    } while (obj.tagName != "TABLE")
                    var parentTreeLevel = obj.rows[0].cells.length;
                    var parentTreeNode = obj.rows[0].cells[0];
                    var tables = obj.parentElement.getElementsByTagName("TABLE");
                    var numTables = tables.length
                    if (numTables >= 1) {
                        for (i = 0; i < numTables; i++) {
                            if (tables[i] == obj) {
                                treeNodeFound = true;
                                i++;
                                if (i == numTables) {
                                    return;
                                }
                            }
                            if (treeNodeFound == true) {
                                var childTreeLevel = tables[i].rows[0].cells.length;
                                if (childTreeLevel > parentTreeLevel) {
                                    var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                    var inputs = cell.getElementsByTagName("INPUT");
                                    inputs[0].checked = checkedState;
                                }
                                else {
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            function GetStudent() {

                var NameandCode;
                var result;
                var GroupSel = $('#<%=chkGroupInvoice.ClientID %>').is(':checked');
                var COMP_ID = $('#<%=h_Company_ID.ClientID %>').val();
                var STUD_TYP = $('#<%=chkEnquiry.ClientID%>').is(':checked');
                var CompFilter = $('#<%=chkCompanyFilter.ClientID%>').is(':checked');
                var chkService = $('#<%=chkService.ClientID %>').is(':checked');
                var hFeeTypes = document.getElementById('<%=h_FeeTypes.ClientID%>').value;
                //alert(hFeeTypes);
                var NewStud = $('#<%=chkNewStudent.ClientID %>').is(':checked');
                var NextAcademicYear = $('#<%=chkNextAcademicYear.ClientID%>').is(':checked');
                var ExcludeTC = $('#<%=chkExcludeTC.ClientID%>').is(':checked');
                var ACD_ID = $('#<%=ddlAcademicYear.ClientID %> option:selected').val();
                if (CompFilter != true) {
                    COMP_ID = "-1";
                }
                var TC = "0";
                if (ExcludeTC == true)
                    TC = "1";
                var url;
                if (STUD_TYP == true) {//Enquiry
                    if (GroupSel == true) {
                        url = "ShowStudentMulti.aspx?MainMnu_code=NU1ZtE0Dp1A=&TYPE=ENQ_COMP&EXL_TYPE=PI&COMP_ID=" + COMP_ID + "&MULTI_SEL=" + GroupSel + "&ACD_ID=" + ACD_ID + "&IsService=" + chkService + "&FeeTypes=" + hFeeTypes + "&NewStudent=" + NewStud + "&EXCL_TC=" + TC;
                    }
                    else {
                        url = "ShowStudent.aspx?MainMnu_code=NU1ZtE0Dp1A=&TYPE=ENQ_COMP&EXL_TYPE=PI&COMP_ID=" + COMP_ID + "&IsService=" + chkService + "&ACD_ID=" + ACD_ID + "&FeeTypes=" + hFeeTypes + "&NewStudent=" + NewStud + "&EXCL_TC=" + TC + "&NextAcadYear=" + NextAcademicYear;
                    }
                }
                else {//Enrolled
                    if (GroupSel == true) {
                        url = "ShowStudentMulti.aspx?MainMnu_code=NU1ZtE0Dp1A=&TYPE=STUD_COMP&EXL_TYPE=PI&COMP_ID=" + COMP_ID + "&MULTI_SEL=" + GroupSel + "&ACD_ID=" + ACD_ID + "&IsService=" + chkService + "&FeeTypes=" + hFeeTypes + "&NewStudent=" + NewStud + "&EXCL_TC=" + TC;
                    }
                    else {
                        url = "ShowStudent.aspx?MainMnu_code=NU1ZtE0Dp1A=&TYPE=STUD_COMP&EXL_TYPE=PI&COMP_ID=" + COMP_ID + "&IsService=" + chkService + "&ACD_ID=" + ACD_ID + "&FeeTypes=" + hFeeTypes + "&NewStudent=" + NewStud + "&EXCL_TC=" + TC + "&NextAcadYear=" + NextAcademicYear;
                    }
                }

                result = radopen(url, "pop_up2");
                    <%--if (GroupSel == false) {
                        if (result != '' && result != undefined) {
                            NameandCode = result.split('||');
                            $('#<%=txtStuName.ClientID%>').val(NameandCode[1]);
                            $('#<%=txtStuNo.ClientID%>').val(NameandCode[2]);
                            $('#<%=h_Students.ClientID%>').val(NameandCode[0]);
                        }
                        return false;
                    }
                    else {
                        if (result != '' && result != undefined) {
                            NameandCode = result.split('||');
                            $('#<%=txtStuNo.ClientID%>').val(NameandCode.length);
                            $('#<%=txtStuName.ClientID%>').val('Student(s) selected');
                            $('#<%=h_Students.ClientID%>').val(result);
                        }
                        return false;
                    }--%>
            }

            function OnClientClose2(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                var GroupSel = $('#<%=chkGroupInvoice.ClientID %>').is(':checked');
                if (arg) {
                    NameandCode = arg.NameandCode.split('||');
                    if (GroupSel == false) {
                           <%-- NameandCode = result.split('||');--%>
                        $('#<%=txtStuName.ClientID%>').val(NameandCode[1]);
                        $('#<%=txtStuNo.ClientID%>').val(NameandCode[2]);
                        $('#<%=h_Students.ClientID%>').val(NameandCode[0]);
                            <%--document.getElementById('<%=h_Students.ClientID %>').value = NameandCode[0];
                            document.getElementById('<%=txtStuNo.ClientID%>').value = NameandCode[2];
                            document.getElementById('<%=txtStuName.ClientID%>').value = NameandCode[1];
                             __doPostBack('<%= txtStuName.ClientID%>', 'TextChanged'); --%>
                    }
                    else {
                        //NameandCode = result.split('||');
                        $('#<%=txtStuNo.ClientID%>').val(NameandCode.length);
                        $('#<%=txtStuName.ClientID%>').val('Student(s) selected');
                        $('#<%=h_Students.ClientID%>').val(arg.NameandCode);
                           <%--  document.getElementById('<%=h_Students.ClientID %>').value = arg.NameandCode;
                            document.getElementById('<%=txtStuNo.ClientID%>').value = NameandCode.length;
                            document.getElementById('<%=txtStuName.ClientID%>').value = 'Student(s) selected'
                             __doPostBack('<%= txtStuName.ClientID%>', 'TextChanged'); --%>
                    }
                }
            }

            function GroupInvoice() {
                var GroupSel = $('#<%=chkGroupInvoice.ClientID %>').is(':checked');
                if (GroupSel == true) {
                    $('#<%=txtStuNo.ClientID%>').attr("disabled", "disabled");
                    $('#<%=txtStuName.ClientID%>').attr("disabled", "disabled");
                }
                else {
                    $('#<%=txtStuNo.ClientID%>').attr("disabled", "");
                    $('#<%=txtStuName.ClientID%>').attr("disabled", "");
                }
            }
            function ShowEditFee(student, stuid) {
                $('#<%=lblStudent.ClientID%>').text(student);
                $('#<%=h_stu_selected4edit.ClientID%>').val(student + '|' + stuid);
                $('#<%=divStuEdit.ClientID%>').show();
                return false;
            }
            function HideEditFee() {
                $('#<%=lblStudent.ClientID%>').text('');
                $('#<%=h_stu_selected4edit.ClientID%>').val('');
                $('#<%=divStuEdit.ClientID%>').hide();
                return false;
            }
            function CheckDuplicat() {
                var StuNo = document.getElementById('<%=h_duplicate.ClientID %>').value;
                if (StuNo != "") {
                    var Ret = confirm('Duplicate Invoice..  \n ' + String(StuNo).substring(0, String(StuNo).length - 3) + '.\n Do you want to Continue..? ');
                    return Ret;
                }
                else
                    return true;
            }
            function ShowStudentData(STU_ID, BSU_ID, STUDENT) {
                $("#<%=divStudentData.ClientID%>").show(150);
                $("#<%=lblStudent_Data.ClientID%>").text(STUDENT);
                //if ($.browser.msie)
                    <%-- if (navigator.userAgent.indexOf("MSIE") != -1)
                    { $("#<%=divInner.ClientID%>").addClass("darkPanelIETop"); }
                    else
                    { $("#<%=divInner.ClientID%>").addClass("darkPanelMTop"); }--%>
                var url = '../StudentServices/StudPro_detailsNew.aspx?id=' + STU_ID + '&bsuID=' + BSU_ID;
                $("#<%=ifrStuData.ClientID%>").attr('src', url);
                return false;
            }
            function HideStudentData() {
                $("#<%=divStudentData.ClientID%>").hide(100);
                    $("#<%=ifrStuData.ClientID%>").attr('src', 'about:blank');
                    return false;
                }
                function AlertMe() {
                    var bDisabled = $("#<%=trACD.ClientID%>").attr('disabled');
                    if (bDisabled == true) {
                        alert('Please clear the selected student(s) if you need to modify');
                    }
                }
        </script>
        <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
            ReloadOnShow="true" runat="server" EnableShadow="true">
            <Windows>
                <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                    OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                    OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="620px">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move"
                    OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="720px">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <%--<telerik:RadButton RenderMode="Lightweight" ID="Button1" runat="server" Text="Get Checked Items" OnClick="Button1_Click" />--%>
    </div>
    <div>
        <div class="card mb-3">
            <div class="card-header letter-space">
                <i class="fa fa-money mr-3"></i>
                <asp:Label ID="lblHeader" runat="server" Text="Fee Pro forma invoice"></asp:Label>
            </div>
            <div class="card-body">
                <div class="table-responsive m-auto">

                    <asp:Literal ID="itemsClientSide" runat="server" />
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" align="center">
                        <tr>
                            <td align="left">
                                <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                                <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server"
                                    CssClass="error" ValidationGroup="MAINERROR" />
                            </td>
                        </tr>
                    </table>
                    <table align="center" cellpadding="5" cellspacing="0" width="100%">
                        <%--<tr class="subheader_img">
                <td align="left" colspan="6" style="height: 19px">
                    <asp:Label ID="lblHeader" runat="server" Text="Fee Pro forma invoice"></asp:Label></td>
            </tr>--%>
                        <tr id="tr2" runat="server">
                            <td align="left" width="20%"><span class="field-label">Invoice Caption</span>
                            </td>

                            <td align="left" colspan="2">
                                <asp:TextBox ID="txtInvoiceCaption" runat="server" Width="75%" Text="proforma invoice"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <tr id="trACD" runat="server">
                            <td align="left" width="20%"><span class="field-label">Academic Year</span>
                            </td>

                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                </asp:DropDownList><br />
                                <asp:CheckBox ID="chkNextAcademicYear" runat="server" Text="Next Academic Year Invoice" CssClass="field-label" />
                            </td>
                            <td align="left" width="20%"><span class="field-label">Date</span></td>

                            <td align="left" width="30%" style="vertical-align: top;">
                                <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="Image1" runat="Server" AlternateText="Click to show calendar"
                                    ImageUrl="~/Images/calendar.gif" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDate"
                                    ErrorMessage="Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="revFromdate" runat="server" ControlToValidate="txtDate" Display="Dynamic"
                                        ErrorMessage="Enter the Date in given format dd/mmm/yyyy e.g.  21/Sep/2007" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
                                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" PopupButtonID="Image1"
                                    TargetControlID="txtDate" Format="dd/MMM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="txtDate"
                                    TargetControlID="txtDate" Format="dd/MMM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>
                        <tr id="trTerm" runat="server">
                            <td align="left" width="20%"><span class="field-label">Invoicing Period</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                    <ContentTemplate>
                                        <table>
                                            <tr>
                                                <td align="left">
                                                    <asp:RadioButtonList ID="rblInvPeriod" RepeatDirection="Horizontal" AutoPostBack="true" runat="server">
                                                        <asp:ListItem Selected="True" Value="T">Termly</asp:ListItem>
                                                        <asp:ListItem Value="M">Monthly</asp:ListItem>
                                                    </asp:RadioButtonList></td>
                                                <td align="left">
                                                    <asp:LinkButton ID="lnkBtnSelectmonth" runat="server" OnClientClick="return false;">Select Month</asp:LinkButton>
                                                    <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" runat="server" TargetControlID="lnkBtnSelectmonth"
                                                        PopupControlID="Panel2" CommitProperty="value" Position="Bottom" CommitScript="" />
                                                    <asp:Panel ID="Panel2" runat="server" CssClass="panel-cover">
                                                        <div style="text-align: left">
                                                            <asp:UpdatePanel runat="server" ID="up2">
                                                                <ContentTemplate>
                                                                    <asp:TreeView ID="trMonths" ShowCheckBoxes="all" onclick="client_OnTreeNodeChecked();"
                                                                        runat="server">
                                                                    </asp:TreeView>
                                                                    <div>
                                                                    </div>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td></td>
                            <td>
                                <asp:Panel ID="pnlCurrency" runat="server" Width="100%">
                                    <table>
                                        <tr>
                                            <td><span class="field-label">Currency</span></td>
                                            <td>
                                                <asp:DropDownList ID="ddCurrency" runat="server" AutoPostBack="True" Style="width: 100px;">
                                                </asp:DropDownList>
                                            </td>
                                            <td><span class="field-label">Exchange Rate</span>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblExgRate" runat="server" Text="1"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr id="trFilters" runat="server">
                            <td align="left" width="20%"><span class="field-label">Filters</span></td>

                            <td align="left" colspan="3">
                                <asp:CheckBox ID="chkService" runat="server" Text="Service Invoice" AutoPostBack="true" CssClass="field-label" />
                                <asp:CheckBox ID="chkExcludePaidAdvance" Text="Exclude Advance Fees" runat="server" CssClass="field-label" />
                                <asp:CheckBox ID="chkAccStatus" Text="Account Status" runat="server" CssClass="field-label" />
                                <asp:CheckBox ID="chkXcludeDue" runat="server" Text="Exclude Previous Outstanding" CssClass="field-label" />
                            </td>
                        </tr>
                        <tr id="trCompany" runat="server">
                            <td align="left" width="20%"><span class="field-label">Company</span></td>

                            <td align="left" colspan="2">
                                <asp:TextBox ID="txtCompanyDescr" runat="server" ToolTip="Type in if company name is known or click search image" Width="90%" placeholder="Type in or Search Company Name"></asp:TextBox>
                                <asp:ImageButton ID="imgCompany" runat="server" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="GetCompany(); return false;" />
                                <asp:LinkButton ID="lbtnClearCompany" runat="server" CausesValidation="False">Clear</asp:LinkButton>
                                <%--   <ajaxToolkit:TextBoxWatermarkExtender ID="TBWE3" runat="server"
                                    TargetControlID="txtCompanyDescr"
                                    WatermarkText="Type in or Search Company Name"
                                    WatermarkCssClass="watermarked" />--%>
                                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server"
                                    TargetControlID="lbtnClearCompany"
                                    ConfirmText="Are you sure you want to remove the company selection? It will also clear the selected students." />
                                <asp:HiddenField ID="h_Company_ID" runat="server" />

                            </td>
                            <td align="left">
                                <asp:CheckBox ID="chkCompanyFilter" Text="Filter Students" runat="server" CssClass="field-label" />
                            </td>
                        </tr>
                        <tr id="trFeeType" runat="server">
                            <td align="left" width="20%"><span class="field-label">Fee Type(s)</span></td>

                            <td align="left" colspan="2">
                                <telerik:RadComboBox CloseDropDownOnBlur="true" RenderMode="Lightweight" ID="ddlFeeType" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true"
                                    Width="100%" CheckedItemsTexts="DisplayAllInInput">
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Narration</span></td>

                            <td align="left" colspan="2">
                                <asp:TextBox ID="txtRemarks" runat="server" Height="46px" SkinID="MultiText" TabIndex="160"
                                    TextMode="MultiLine" Width="75%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="title-bg">Student(s) Detail
                            </td>
                        </tr>
                        <tr id="trStudentFilters" runat="server">
                            <td colspan="4">
                                <asp:CheckBox ID="chkEnquiry" Text="Enquiry" runat="server" CssClass="field-label" />
                                <asp:CheckBox ID="chkNewStudent" Text="New Student" runat="server" CssClass="field-label" />
                                <asp:CheckBox ID="chkGroupInvoice" Text="Group Invoice" runat="server" CssClass="field-label" />
                                <asp:CheckBox ID="chkExcludeTC" Text="Exclude future TC Student(s)" runat="server" CssClass="field-label" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table align="center" cellpadding="5" cellspacing="0" style="width: 100%;">
                                    <tr>
                                        <td><span class="field-label">Select Student(s)</span></td>

                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="right" width="25%">
                                                                    <asp:TextBox ID="txtStuNo" runat="server" ToolTip="Type in if student id is known or click search image" placeholder="ID"></asp:TextBox>

                                                                </td>
                                                                <td align="center">-
                                                                </td>
                                                                <td align="left" width="75%">
                                                                    <asp:TextBox ID="txtStuName" runat="server" ToolTip="Type in if student name is known or click search image" placeholder="NAME"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClientClick="GetStudent(); return false;" />
                                                                    <asp:HiddenField ID="h_Students" runat="server" />
                                                                    <%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                                                        TargetControlID="txtStuNo"
                                                                        WatermarkText="ID"
                                                                        WatermarkCssClass="watermarked" />--%>
                                                                    <%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                                                        TargetControlID="txtStuName"
                                                                        WatermarkText="NAME"
                                                                        WatermarkCssClass="watermarked" />--%>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:LinkButton ID="lblAddNewStudent" runat="server" CausesValidation="False">Add</asp:LinkButton>
                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td align="left"></td>
                                                    <td>
                                                        <asp:LinkButton ID="lbtnRemoveAll" runat="server" CausesValidation="False">Clear All</asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="cbe" runat="server"
                                                            TargetControlID="lbtnRemoveAll"
                                                            ConfirmText="Are you sure you want to clear the selected student(s)?" />
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblGrade"></asp:Label>
                                                        <asp:HiddenField ID="h_STU_GRD_ID" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:GridView ID="gvStudentDetails" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row"
                                                DataKeyNames="STU_ID">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Student ID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStudID" runat="server" Text='<%# Bind("STU_ID")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="#">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSlno" runat="server" Text="<%# Container.DataItemIndex + 1%>"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Student ID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStud_no" runat="server" Text='<%# bind("STU_NO") %>'></asp:Label>
                                                            <%--<asp:LinkButton ID="lblStud_no" runat="server" Text='<%# Bind("STU_NO")%>' CausesValidation="False" ToolTip="Click here for the Student details"></asp:LinkButton>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Student Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStudentName" runat="server" Text='<%# Bind("STU_NAME")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Grade">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("STU_GRD_ID")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Amount">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lblAmount" runat="server" Text='<%# Eval("STU_AMOUNT", "{0:0,0.00}")%>' OnClientClick="return false;"
                                                                CausesValidation="False" ToolTip="Click here for the Split up details"></asp:LinkButton>
                                                            <%--<asp:Label ID="lblAmount" runat="server" Text='<%# Eval("STU_AMOUNT", "{0:0,0.00}")%>'></asp:Label>--%>
                                                            <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" DynamicServiceMethod="GetDynamicContent"
                                                                DynamicContextKey='<%# Eval("STU_ID")%>' DynamicControlID="Panel1" TargetControlID="lblAmount"
                                                                PopupControlID="Panel1" Position="Right">
                                                            </ajaxToolkit:PopupControlExtender>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="FCAmount">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFCAmount" runat="server" Text='0'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                                            <ajaxToolkit:ConfirmButtonExtender ID="gvDelConfirmButtonExtender" runat="server"
                                                                TargetControlID="lnkDelete"
                                                                ConfirmText="Are you sure you want to delete the Student?" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <asp:HiddenField ID="h_stu_selected4edit" runat="server" />
                                            <asp:Panel ID="Panel1" runat="server" BackColor="lightgray">
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:CheckBox ID="chkPrintReciept" runat="server" Text="Print Receipt After Save" CssClass="field-label"
                                    Checked="True" />
                                <asp:CheckBox ID="chkAnually" runat="server" Text="Generate Annual Receipt" CssClass="field-label" />
                                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" OnClick="btnAdd_Click"
                                    Text="Add" />
                                <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR"
                                    OnClientClick="return CheckDuplicat()" />
                                <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Delete" />
                                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server"
                                    TargetControlID="btnDelete"
                                    ConfirmText="Are you sure you want to delete the Invoice?" />
                                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                    <!--Popup for additional fees -->
                    <div id="divStuEdit" class="darkPanlAlumini" runat="server" style="display: none; z-index: 3000 !important;">
                        <div class="panel-cover inner_darkPanlAlumini" style="width: 70% !important; height: 70% !important;">
                            <div class="darkPanelFooter">
                                <asp:Label ID="lblStudent" EnableViewState="true" CssClass="TitlePl" runat="server"></asp:Label>
                                <asp:Button ID="Button2" type="button" runat="server" Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                                    ForeColor="White" Text="X" CausesValidation="false" OnClientClick="return HideEditFee();"></asp:Button>

                            </div>
                            <div class="holderInner" style="overflow: scroll; width: 100% !important; height: 95% !important;">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel2">
                                    <ContentTemplate>
                                        <center>
                                <table cellpadding="2" style="width: 100%; height: auto; border: thin !important;">
                                    <tr>
                                        <td colspan="4" align="center">
                                            <asp:Label ID="lblAddFeeError" runat="server" class="error"></asp:Label>
                                            <asp:ValidationSummary ID="ValidationSummary2" CssClass="error" runat="server" ValidationGroup="EditFEETYPE" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  align="left" width="20%"><span class="field-label">Fee Type</span>
                                        </td>
                                        
                                        <td  align="left" wdth="30%">
                                            <asp:DropDownList ID="ddlAddFeeType" runat="server" width="100%">
                                            </asp:DropDownList>
                                        </td>
                                        <td  rowspan="3" style="width: 100%; vertical-align: top;">
                                            <asp:GridView ID="gvAddFeeDet" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row"
                                                 EmptyDataText="No details added">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="FEE ID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFEE_ID" runat="server" Text='<%# bind("FEE_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Fee Type">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStudentName" runat="server" Text='<%# bind("FEE_TYPE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Remarks">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# bind("FEE_ADJ_REMARKS") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Amount">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("FEE_ADJ_AMT", "{0:0,0.00}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDeleteFEEADDADJ" runat="server" OnClick="lnkDeleteFEEADDADJ_Click">Delete</asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  align="left" width="20%"><span class="field-label">Amount</span></td>
                                       
                                        <td  align="left" width="30%">
                                            <asp:TextBox ID="txtAddFeeAmount" runat="server" Style="width: 100%;"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers, Custom"
                                                ValidChars=".-" TargetControlID="txtAddFeeAmount" />
                                            <asp:CompareValidator ID="cmpAmtVer" runat="server" ControlToValidate="txtAddFeeAmount"
                                                ErrorMessage="Amount cannot be Zero" ValueToCompare="0" Operator="NotEqual" Type="Double"
                                                ValidationGroup="EditFEETYPE">*</asp:CompareValidator>

                                        </td>
                                    </tr>
                                    <tr id="Tr1" runat="server">
                                        <td align="left" width="20%" ><span class="field-label">Comments</span></td>
                                      
                                        <td align="left" width="20%" >
                                            <asp:TextBox ID="txtAddFeeRemarks" runat="server" TextMode="MultiLine" width="100%" ></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="center" align="left" colspan="4">
                                            <asp:Button ID="btnFeeAddAmt" runat="server" CssClass="button" OnClick="btnFeeAddAmt_Click" Text="Add" ValidationGroup="EditFEETYPE" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center"  align="left" colspan="4">
                                            <asp:CheckBox ID="chkApplyToAll" runat="server" Text="Apply to all Students" />
                                            <asp:Button ID="btnSaveAddFee" runat="server" OnClick="btnSaveAddFee_Click" CssClass="button" Text="Save" />
                                            <asp:Button ID="btnSaveAddFeeCancel" runat="server" OnClick="btnSaveAddFeeCancel_Click" CssClass="button" Text="Cancel" /></td>
                                    </tr>
                                </table>
                            </center>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <!-- Popup for Student Details-->
                    <div id="divStudentData" class="darkPanlAlumini" runat="server" style="display: none; z-index: 3000 !important;">
                        <div id="divInner" runat="server" class="panel-cover inner_darkPanlAlumini" style="width: 70% !important; height: 70% !important;">
                            <div class="darkPanelFooter">
                                <asp:Label ID="lblStudent_Data" EnableViewState="true" CssClass="TitlePl" runat="server"></asp:Label>
                                <asp:Button ID="Button1" type="button" runat="server" Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: lightgray;"
                                    ForeColor="White" Text="X" CausesValidation="false" OnClientClick="return HideStudentData();"></asp:Button>
                            </div>
                            <div class="holderInner" style="width: 97% !important; height: 95% !important; overflow: hidden !important;">
                                <iframe id="ifrStuData" name="ifrStuData" src="about:blank" style="width: 100%; height: 100%;" runat="server" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="h_duplicate" runat="server" />
                    <asp:HiddenField ID="h_FeeTypes" runat="server" />
                    <asp:HiddenField ID="h_print" runat="server" Value="" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

