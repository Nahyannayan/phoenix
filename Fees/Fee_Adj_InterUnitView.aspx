<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Fee_Adj_InterUnitView.aspx.vb" Inherits="Fees_Fee_Adj_InterUnitView" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fee Adjustment Inter Unit"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td width="50%" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                        <td align="right">
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">
                    <%--<tr class="subheader_BlueTableView" valign="top" height="19px">
            <th align="left" colspan="2" valign="middle">
                <asp:Label ID="lblHeader" runat="server" Text="Fee Adjustment Inter Unit"></asp:Label></th>
        </tr>--%>
                    <tr>
                        <td align="left" width="50%">
                            <asp:RadioButton ID="radStud" runat="server" AutoPostBack="True" Checked="True" Text="Student" GroupName="STU_TYPE" />
                            <asp:RadioButton ID="radEnq" runat="server" Text="Enquiry" AutoPostBack="True" GroupName="STU_TYPE" /></td>
                        <td align="right" width="50%">
                            <asp:RadioButton ID="radRequested" runat="server" AutoPostBack="True" Checked="True" Text="Requested" GroupName="STU_APROVE" CausesValidation="True" />
                            <asp:RadioButton ID="radApproved" runat="server" AutoPostBack="True" Text="Approved" GroupName="STU_APROVE" />
                            <asp:RadioButton ID="radReject" runat="server" Text="Reject" AutoPostBack="True" GroupName="STU_APROVE" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:GridView ID="gvFEEAdjustments" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30" CssClass="table table-bordered table-row" EnableModelValidation="True">
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="FAI_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFAI_ID" runat="server" Text='<%# Bind("FAI_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Student ID">
                                        <HeaderTemplate>
                                            Student ID<br />
                                            <asp:TextBox ID="txtstudno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudnoSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Name<br />
                                            <asp:TextBox ID="txtstudname" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudnameSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date<br />
                                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("FAI_DATE", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            Grade<br />
                                            <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnGradeSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGRD_DISPLAY" runat="server" Text='<%# Bind("GRD_DISPLAY")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderTemplate>
                                            Remarks<br />
                                            <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnRemarksSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("FAI_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="STATUS" HeaderText="Status(OB)" />
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <p></p>
            </div>
        </div>
    </div>


</asp:Content>
