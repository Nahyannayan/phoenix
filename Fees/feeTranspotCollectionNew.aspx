<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="feeTranspotCollectionNew.aspx.vb" Inherits="Fees_feeTranspotCollectionNew"
    Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/uscStudentPicker.ascx" TagPrefix="uc2" TagName="uscStudentPicker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="true">
    <style type="text/css">
        /*bootstrap class overrides starts here*/
        .card-body {
            padding: 0.25rem !important;
        }

        table th, table td {
            padding: 0.1rem !important;
        }

        .bg-dark div input.button, .bg-dark table td input.button, .bg-dark table td div input.button {
            padding: 6px !important;
            margin: 2px !important;
        }

        table td select, div select, table td input[type=text], table td input[type=date], table td input[type=password], table td textarea, table td select:disabled, div select:disabled, table td input[type=text]:disabled, table td textarea:disabled, table td input[type=password]:disabled {
            padding: 6px !important;
        }
        /*bootstrap class overrides ends here*/

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: /*10px*/ 0px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script type="text/javascript" lang="javascript">
        Sys.Application.add_load(
    function CheckForPrint() {
        ShowHideBankAccount();
        if (document.getElementById('<%= h_print.ClientID %>').value != '') {
            if (isIE())
                showModelessDialog('FeeReceiptTransport.aspx' + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
            else
                Popup('FeeReceiptTransport.aspx' + document.getElementById('<%= h_print.ClientID %>').value);
            document.getElementById('<%= h_print.ClientID %>').value = '';
        }
    }
    );
    function ShowHideBankAccount() {
        if ($('#<%= hf_BNK_TYPE.ClientID%>').val() != 'BANKTRF') {
            $("#tdhBA").hide();
            $("#tdBA1").hide();
            $('#<%=ddlTotchqs.ClientID%>').prop('disabled', false);
        }
        else {
            $("#tdhBA").show();
            $("#tdBA1").show();
            $('#<%=ddlTotchqs.ClientID%>').prop('selectedIndex', 0);
            $('#<%=ddlTotchqs.ClientID%>').prop('disabled', true);
        }
    }
    function isIE() {
        ua = navigator.userAgent;
        /* MSIE used to detect old browsers and Trident used to newer ones*/
        var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

        return is_ie;
    }
    function CheckNumber(ctrl) {
        var temp;
        temp = parseFloat(ctrl.value);
        if (isNaN(temp))
            ctrl.value = 0;
        else
            ctrl.value = temp.toFixed(2);
    }

    function getRoundOff() {
        var roundOff = '<%=Session("BSU_ROUNDOFF") %>';
        var amt;
        amt = parseFloat(roundOff)
        if (isNaN(amt))
            amt = 2;
        return amt;
    }

    function getLOCATION() {
        var chk;
        chk = 'notchecked';
        for (i = 0; i < document.forms[0].elements.length; i++) {
            if (document.forms[0].elements[i].type == 'checkbox') {
                if (document.forms[0].elements[i].checked == true)
                    chk = 'checked';
            }
        }
        if (chk == 'notchecked') {
            alert('Please select the term')
            return false;
        }
        var sFeatures, url;
        sFeatures = "dialogWidth: 600px; ";
        sFeatures += "dialogHeight: 500px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var dropFind = $find("<%= ddlBusinessunit.ClientID%>");
        var BSU_ID = dropFind.get_value();
        url = "../common/PopupFormIDhidden.aspx?iD=LOCATION&bsu=" + BSU_ID + "&MULTISELECT=FALSE";
        result = window.showModalDialog(url, "", sFeatures);
        if (result == '' || result == undefined) {
            return false;
        }
        NameandCode = result.split('___');
        document.getElementById('<%= H_Location.ClientID %>').value = NameandCode[0];
        document.getElementById('<%= txtLocation.ClientID %>').value = NameandCode[1];
        return true;
    }

    function CheckAmount(e) {
        var amt;
        amt = parseFloat(e.value)
        if (isNaN(amt))
            amt = 0;
        e.value = amt.toFixed(2);
        return true;
    }

    function UpdateSum(txtCr, txtCash, txtChq) {
        var txtCCTotal, txtBankTotal, txtCashTotal, txtCrCharge,
            txtTotal, txtReturn, txtReceivedTotal, txtBalance, RewardsTotal;

        txtCCTotal = parseFloat(document.getElementById('<%=txtCCTotal.ClientID %>').value);
        if (isNaN(txtCCTotal))
            txtCCTotal = 0;

        txtCrCharge = parseFloat(document.getElementById('<%=txtCrCharge.ClientID %>').value);
        if (isNaN(txtCrCharge))
            txtCrCharge = 0;

        txtBankTotal = parseFloat(document.getElementById('<%=txtBankTotal.ClientID %>').value);
        if (isNaN(txtBankTotal))
            txtBankTotal = 0;
        txtCashTotal = parseFloat(document.getElementById('<%=txtCashTotal.ClientID %>').value);
        if (isNaN(txtCashTotal))
            txtCashTotal = 0;
        txtTotal = parseFloat(document.getElementById('<%=txtTotal.ClientID %>').value);
        if (isNaN(txtTotal))
            txtTotal = 0;
        RewardsTotal = parseFloat(document.getElementById('<%=txtRewardsAmount.ClientID%>').value);
            if (isNaN(RewardsTotal))
                RewardsTotal = 0;

        txtReceivedTotal = txtCCTotal + txtBankTotal + txtCashTotal + txtCrCharge + RewardsTotal;

        txtBankTotal = txtBankTotal;
        txtBalance = 0;
        if (txtCashTotal > 0)
            if (txtReceivedTotal > (txtTotal + txtCrCharge)) {
                txtBalance = txtReceivedTotal - txtTotal;
                if (txtBalance > txtCashTotal)
                    txtBalance = 0;
            }
        document.getElementById('<%=txtReceivedTotal.ClientID %>').value = txtReceivedTotal.toFixed(getRoundOff());
        document.getElementById('<%=txtCCTotal.ClientID %>').value = txtCCTotal.toFixed(getRoundOff());
        document.getElementById('<%=txtBankTotal.ClientID %>').value = txtBankTotal.toFixed(getRoundOff());

        document.getElementById('<%=txtCashTotal.ClientID %>').value = txtCashTotal.toFixed(getRoundOff());
        document.getElementById('<%=txtBankTotal.ClientID %>').value = txtBankTotal.toFixed(getRoundOff());
        document.getElementById('<%=txtBalance.ClientID %>').value = txtBalance.toFixed(getRoundOff());
        document.getElementById('<%=txtRewardsAmount.ClientID%>').value = RewardsTotal.toFixed(getRoundOff());
        if (txtCCTotal >= 0 && txtCr != "")
            CalculateRate(txtCCTotal);

    }

    function CalculateRate(CardAmount) {
        var temp = new Array();
        temp = document.getElementById('<%=hfCrCharges.ClientID %>').value.split("|");
        var ddlist = document.getElementById('<%=ddCreditcard.ClientID %>');
        //            var txtRecvdTotal = 0;
        var txtRecvdTotal = parseFloat(document.getElementById('<%=txtReceivedTotal.ClientID %>').value);
        if (isNaN(txtRecvdTotal))
            txtRecvdTotal = 0;
        if (txtRecvdTotal > 0) {
            txtRecvdTotal = txtRecvdTotal - CardAmount;
        }
        var txtCrCharge = parseFloat(document.getElementById('<%=txtCrCharge.ClientID %>').value);
        if (isNaN(txtCrCharge))
            txtCrCharge = 0;
        if (txtCrCharge > 0) {
            txtRecvdTotal = txtRecvdTotal - txtCrCharge;
        }
        if (temp.length > 0) {
            for (var i = 0; i < temp.length; i++) {
                if (temp[i].split("=")[0] == ddlist.options[ddlist.selectedIndex].value) {
                    document.getElementById('<%=txtCrCharge.ClientID %>').value = Math.ceil(CardAmount * temp[i].split("=")[1] / 100.00).toFixed(getRoundOff());
                    document.getElementById('<%=txtReceivedTotal.ClientID %>').value = txtRecvdTotal + CardAmount + Math.ceil(CardAmount * (temp[i].split("=")[1] / 100.00));
                }
            }
        }
    }

    function HideAll() {
        document.getElementById('<%= pnlCheque.ClientID %>').style.display = 'none';
        document.getElementById('<%= pnlCreditCard.ClientID %>').style.display = 'none';
        return false;
    }

    function SetTotal(result) {
        for (i = 0; i < document.forms[0].elements.length; i++)
            if (document.forms[0].elements[i].name.search(/txtAmountToPay/) > 0) {
                if (document.forms[0].elements[i].type == 'text') {
                    var NameandCode;
                    NameandCode = result.split('||');
                    document.forms[0].elements[i].value = NameandCode[0];
                    document.getElementById('<%=txtTotal.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtRemarks.ClientID %>').value = 'Transport fee for ' + NameandCode[1];
                    break;
                }
            }
    }
    function client_OnTreeNodeChecked() {
        var obj = window.event.srcElement;
        var treeNodeFound = false;
        var checkedState;
        if (obj.tagName == "INPUT" && obj.type == "checkbox") {
            var treeNode = obj;
            checkedState = treeNode.checked;
            do {
                obj = obj.parentElement;
            } while (obj.tagName != "TABLE")
            var parentTreeLevel = obj.rows[0].cells.length;
            var parentTreeNode = obj.rows[0].cells[0];
            var tables = obj.parentElement.getElementsByTagName("TABLE");
            var numTables = tables.length
            if (numTables >= 1) {
                for (i = 0; i < numTables; i++) {
                    if (tables[i] == obj) {
                        treeNodeFound = true;
                        i++;
                        if (i == numTables) {
                            return;
                        }
                    }
                    if (treeNodeFound == true) {
                        var childTreeLevel = tables[i].rows[0].cells.length;
                        if (childTreeLevel > parentTreeLevel) {
                            var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                            var inputs = cell.getElementsByTagName("INPUT");
                            inputs[0].checked = checkedState;
                        }
                        else {
                            return;
                        }
                    }
                }
            }
        }
    }

    </script>

    <script lang="javascript" type="text/javascript">

        function ShowGridColumn(bShow) {
            var display = "";
            if (bShow == false)
                display = "none";
            var th = $("[id*=gvChequeDetails] th:contains('Account')");
            th.css("display", display);
            $("[id*=gvChequeDetails] tr").each(function () {
                $(this).find("td").eq(th.index()).css("display", display);
                //alert($(this).find("td").find($("[ @id *= 'txtDBankAct']")).val());
            });
            var gvhequedetails = document.getElementById('<%=gvChequeDetails.ClientID %>');
            for (var rowId = 1; rowId < gvhequedetails.rows.length; rowId++) {
                var txtbx = gvhequedetails.rows[rowId].cells[1].children[0];
                var hfield = gvhequedetails.rows[rowId].cells[1].children[2];
                if (bShow == false) {
                    txtbx.value = '';
                    hfield.value = '';
                }
            }
        }
        function ValidateAddCheque() {
            if (document.getElementById('<%= hf_BNK_TYPE.ClientID%>').value == 'BANKTRF') {
                if ($('#<%=hfBankAct1.ClientID%>').val() == '' || $('#<%=txtBankAct.ClientID%>').val() == '') {
                    alert('Please select a bank account if Bank Transfer(TT) is chosen.');
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        }
        function ShowMoreInfo(mode) {
            var url = '#';
            var W = '';
            var H = '';
            var Title = '';
            var STU_ID = document.getElementById('<%= h_Student_no.ClientID%>').value;

            var dropFind = $find("<%= ddlBusinessunit.ClientID%>");
            var BSU_ID = dropFind.get_value();

            var ACD_ID = GetAcademicYear();
            if (mode <= 9) {
                W = "80%";
                H = "70%";
            }
            else {
                W = "60%";
                H = "60%";
            }
            if (mode == 1) {
                url = "../common/PopupShowData.aspx?id=RECEIPTHISTORY_TRANSPORT&stuid=" + STU_ID;
                Title = 'Payment History';
            }
            else if (mode == 2) {
                url = "../common/PopupShowData.aspx?id=CHARGEHISTORY_TRANSPORT&stuid=" + STU_ID;
                Title = 'Charge Details';
            }
            else if (mode == 3) {
                url = "../common/PopupShowData.aspx?id=SIBBLINGS_TRANSPORT&stuid=" + STU_ID + "&bsu=" + BSU_ID;
                Title = 'Sibling Informaion';
            }
            else if (mode == 4) {
                url = "../common/PopupShowData.aspx?id=CONCESSION_TRANSPORT&stuid=" + STU_ID + "&bsu=" + BSU_ID;
                Title = 'Concession Details';
            }
            else if (mode == 5) {
                url = "../common/PopupShowData.aspx?id=ADJUSTMENT_TRANSPORT&stuid=" + STU_ID + "&bsu=" + BSU_ID;
                Title = "Fee Adjustment";
            }
            else if (mode == 6) {
                url = "../common/PopupShowData.aspx?id=CHQBOUNCE_TRANSPORT&stuid=" + STU_ID;
                Title = "Cheque History";
            }
            else if (mode == 7) {
                url = "../common/PopupShowData.aspx?id=ADVPAY_TRANSPORT&stuid=" + STU_ID;
                Title = "Advance Receipts";
            }
            else if (mode == 8) {
                url = "../common/PopupShowData.aspx?id=AGING_TRANSPORT&stuid=" + STU_ID + "&bsu=" + BSU_ID;
                Title = "Ageing";
            }
            else if (mode == 10) {
                url = "../common/PopupShowData.aspx?id=CHARGEHISTORYTERMLY_TRANSPORT&stuid=" + STU_ID + "&bsu=" + BSU_ID + "&ACDID=" + ACD_ID + "&SBL=" + document.getElementById('<%= H_Location.ClientID %>').value;
                Title = "Fee (Termly)";
            }
            else if (mode == 11) {
                url = "../common/PopupShowData.aspx?id=CHARGEHISTORYMONTHLY_TRANSPORT&stuid=" + STU_ID + "&bsu=" + BSU_ID + "&ACDID=" + ACD_ID + "&SBL=" + document.getElementById('<%= H_Location.ClientID %>').value;
                    Title = "Fee (Monthly)";
                }


    ShowWindowWithClose(url, Title, W, H);
    return false;
}
function ShowTitle(title) {
    if (title != '') {
        var container = '<div id="fancybox-custom-title-container" style="text-align:left !important;" class="darkPanelFooter"><span id="fancybox-custom-title" CssClass="TitlePl">' + title + '</span></div>';
        return container;
    }
}
function ShowWindowWithClose(gotourl, pageTitle, w, h) {
    $.fancybox({
        type: 'iframe',
        //maxWidth: 300,
        href: gotourl,
        maxHeight: 600,
        fitToView: true,
        padding: 6,
        width: w,
        height: h,
        autoSize: false,
        openEffect: 'none',
        showLoading: true,
        closeClick: true,
        closeEffect: 'fade',
        'closeBtn': true,
        afterLoad: function () {
            this.title = ShowTitle(pageTitle);
        },
        helpers: {
            overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
            title: { type: 'inside' }
        },
        onComplete: function () {
            $("#fancybox-wrap").css({ 'top': '90px' });
        }
    });

    return false;
}

function getStudent() {
    var url, stuName;
    stuName = '<%= uscStudentPicker.STU_NAME%>';
    var prevAcd = document.getElementById('<%= h_PREV_ACD.ClientID %>').value;
    //var BSU_ID = document.getElementById('<%= ddlBusinessunit.ClientID %>').value;
    var dropFind = $find("<%= ddlBusinessunit.ClientID%>");
    var BSU_ID = dropFind.get_value();
    var ACD_ID = GetAcademicYear();
    if ('<%= uscStudentPicker.STU_TYPE%>' == 'E')
        url = "ShowStudentTransport.aspx?type=STU_TRAN&bsu=" + BSU_ID + '&acd=' + ACD_ID + '&prevacd=' + prevAcd + '&codeorname=' + document.getElementById(stuName).value;
    else
        url = "ShowStudentTransport.aspx?type=ENQ_COMP&COMP_ID=-1&bsu=" + BSU_ID;
    var oWnd = radopen(url, "pop_student");
}
function GetAcademicYear() {
    var ACD_ID = '<%= uscStudentPicker.STU_ACD_ID%>';
    return ACD_ID;
}
function OnClientClose1(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg) {
        NameandCode = arg.NameandCode.split('||');
        document.getElementById('<%= h_Student_no.ClientID %>').value = NameandCode[0];
                <%--document.getElementById('<%= txtStudentname.ClientID %>').value = NameandCode[1];
                document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];--%>
        __doPostBack('<%= h_Student_no.ClientID%>', 'ValueChanged');
    }
}
function Showdata(mode) {
    var url;

    var STU_ID = document.getElementById('<%= h_Student_no.ClientID %>').value;
    var dropFind = $find("<%= ddlBusinessunit.ClientID%>");
    var BSU_ID = dropFind.get_value();
    var ACD_ID = GetAcademicYear();

    var result;
    if (mode == 12)
        url = "FeeCalculateFeeTransport.aspx?stuid=" + STU_ID + "&bsu=" + BSU_ID + "&SBL=" + document.getElementById('<%= H_Location.ClientID %>').value + "&acd=" + ACD_ID;
    else if (mode == 9)
        url = "feeSetUpServicesAndCharge.aspx?stuid=" + '<%= uscStudentPicker.STU_NO%>' + "&bsu=" + BSU_ID + "&acd=" + ACD_ID;

    var oWnd = radopen(url, "pop_Showdata");
}

function OnClientClose2(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg) {

        NameandCode = arg.NameandCode.split('||');
        SetTotal(NameandCode[0])
    }
}
function getBankOrEmirate(mode, ctrl, Obj2, ctrl2) {
    var url;
    document.getElementById('<%= hf_mode.ClientID%>').value = mode
    document.getElementById('<%= hf_ctrl.ClientID%>').value = ctrl
    document.getElementById('<%= hf_obj.ClientID%>').value = Obj2
    document.getElementById('<%= hf_ctrl2.ClientID%>').value = ctrl2

    if (mode == 1)
        url = "../common/PopupBanks.aspx?iD=BANK&MULTISELECT=FALSE";
    else
        url = "../common/PopupFormIDhidden.aspx?iD=EMIRATE&MULTISELECT=FALSE";
    var oWnd = radopen(url, "pop_getBankOrEmirate");
}

function OnClientClose3(oWnd, args) {
    var mode = document.getElementById('<%= hf_mode.ClientID%>').value;
    var ctrl = document.getElementById('<%= hf_ctrl.ClientID%>').value;
    var Obj2 = document.getElementById('<%= hf_obj.ClientID%>').value;
    var ctrl2 = document.getElementById('<%= hf_ctrl2.ClientID%>').value;
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg) {

        NameandCode = arg.NameandCode.split('||');
        if (mode == 1) {
            if (ctrl == 1) {
                document.getElementById('<%= h_BankId.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= txtBank.ClientID %>').value = NameandCode[1];
                document.getElementById('<%= hf_BNK_TYPE.ClientID%>').value = NameandCode[2];
                if (NameandCode[2] == "BANKTRF") {
                    $("#tdhBA").show(250);
                    $("#tdBA1").show(250);
                    $('#<%=ddlTotchqs.ClientID%>').prop('selectedIndex', 0);
                    $('#<%=ddlTotchqs.ClientID%>').prop('disabled', true);
                }
                else {
                    $("#tdhBA").hide(250);
                    $("#tdBA1").hide(250);
                    $('#<%=hfBankAct1.ClientID%>').val('');
                    $('#<%=txtBankAct.ClientID%>').val('');
                    $('#<%=ddlTotchqs.ClientID%>').prop('disabled', false);
                }
            }
            else {
                document.getElementById(Obj2).value = NameandCode[0];
                document.getElementById(ctrl).value = NameandCode[1];
                document.getElementById(ctrl2).value = NameandCode[2];

                if (NameandCode[2] == "BANKTRF")
                    ShowGridColumn(true);
                else
                    ShowGridColumn(false);
            }

        }
    }
}
function getBankAct(mode, ctrl, Obj2) {
    document.getElementById('<%= hf_mode.ClientID%>').value = mode
    document.getElementById('<%= hf_ctrl.ClientID%>').value = ctrl
    document.getElementById('<%= hf_obj.ClientID%>').value = Obj2

    var txtBankCode;
    if (mode == 1) txtBankCode = $('#<%=txtBankAct.ClientID%>').val();

    var oWnd = radopen("../ACCOUNTS/PopUp.aspx?ShowType=BANK&codeorname=" + txtBankCode, "pop_getBankAct");

}

function OnClientClose7(oWnd, args) {

    var mode = document.getElementById('<%= hf_mode.ClientID%>').value;
    var ctrl = document.getElementById('<%= hf_ctrl.ClientID%>').value;
    var Obj2 = document.getElementById('<%= hf_obj.ClientID%>').value;
    var arg = args.get_argument();
    if (arg) {

        NameandCode = arg.NameandCode.split('||');
        if (mode == 1) {
            if (ctrl == 1) {
                $('#<%=hfBankAct1.ClientID%>').val(NameandCode[0]);
                $('#<%=txtBankAct.ClientID%>').val(NameandCode[0]);
            }
            else {
                $('#' + ctrl + '').val(NameandCode[0]);
                $('#' + Obj2 + '').val(NameandCode[0]);
            }
        }
    }
}



function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_Showdata" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getBankOrEmirate" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getBankAct" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose7"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Transport Fee Collection
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table style="width: 100%;">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table style="width: 100%;">
                    <tr>
                        <td align="left" style="width: 15%;"><span class="field-label">Business Unit</span>
                        </td>
                        <td align="left" style="width: 55%;" colspan="2">
                            <telerik:RadComboBox ID="ddlBusinessunit" runat="server" Filter="Contains" Width="100%" RenderMode="Lightweight" AutoPostBack="true" ZIndex="2000" ToolTip="Type in unit name or short code" DataSourceID="odsSERVICES_BSU_M" DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"></telerik:RadComboBox>
                        </td>
                        <td style="width: 30%;">
                            <span class="field-label">Date</span>
                            <asp:TextBox ID="txtFrom" runat="server" TabIndex="15" AutoPostBack="True" Style="width: 60%; min-width: 25% !important"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="20" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 15%;"><span class="field-label">Student</span></td>
                        <td align="left" colspan="3">
                            <uc2:uscStudentPicker runat="server" ID="uscStudentPicker" />
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 40%;">
                                        <span class="field-label">Area</span>
                                        <asp:TextBox ID="txtLocation" runat="server" AutoPostBack="True" Style="width: 50% !important;" onclick="Showdata(12);return false;"></asp:TextBox>
                                        <asp:ImageButton ID="imgLocation" runat="server" ImageUrl="~/Images/Misc/Route.png"
                                            OnClientClick="return getLOCATION();" ImageAlign="AbsMiddle"
                                            TabIndex="30" Visible="False" />
                                        <asp:LinkButton ID="lnkSetArea" runat="server" OnClientClick="Showdata(9);return false;"
                                            OnClick="lnkSetArea_Click">Set Area</asp:LinkButton>
                                        <asp:Label ID="lblChqBalance" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 15%;">
                                        <asp:Panel ID="pnlMonthterm" runat="server" Style="width: 100%;">
                                            <asp:Label ID="lblNoStudent" runat="server" EnableViewState="False" CssClass="error"
                                                ForeColor="Red"></asp:Label>
                                            <asp:RadioButton ID="rbMonthly" runat="server" AutoPostBack="True" GroupName="trm" CssClass="field-label"
                                                Text="Monthly" />
                                            <asp:RadioButton ID="rbTermly" runat="server" AutoPostBack="True" Checked="True" CssClass="field-label"
                                                GroupName="trm" Text="Termly" />
                                        </asp:Panel>
                                    </td>
                                    <td style="width: 20%;">
                                        <asp:LinkButton ID="lnkBtnSelectmonth" runat="server" OnClientClick="return false;"
                                            TabIndex="15" Style="cursor: pointer;">Select Month/Term</asp:LinkButton>
                                        <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover"
                                            PopDelay="25" PopupControlID="PanelTree" PopupPosition="Center" TargetControlID="lnkBtnSelectmonth">
                                        </ajaxToolkit:HoverMenuExtender>
                                    </td>
                                    <td align="right" style="width: 25%;">
                                        <asp:LinkButton ID="lbtnMore" OnClientClick="return false;" Style="margin-right: 25%; background-color: orange; border: 3pt solid orange;" runat="server">More Info</asp:LinkButton>
                                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" PopupControlID="Panel1"
                                            Position="Bottom" TargetControlID="lbtnMore">
                                        </ajaxToolkit:PopupControlExtender>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="title-bg">
                        <td colspan="4" align="left">Fee Details
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvFeeCollection" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                CssClass="table table-row table-bordered" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="FSR_FEE_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFSR_FEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="C.P">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ChkPost" runat="server" Enabled="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OPENING" DataFormatString="{0:0.00}" HeaderText="Opening">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MONTHLY_AMOUNT" DataFormatString="{0:0.00}" HeaderText="Charge">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CONC_AMOUNT" DataFormatString="{0:0.00}" HeaderText="Concession">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ADJUSTMENT" DataFormatString="{0:0.00}" HeaderText="Adjustment">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PAID_AMOUNT" DataFormatString="{0:0.00}" HeaderText="Paid">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CLOSING" DataFormatString="{0:0.00}" HeaderText="Due">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Paying Now">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmountToPay" AutoCompleteType="Disabled" runat="server" AutoPostBack="True"
                                                onblur="return CheckAmount(this);" onFocus="this.select();" OnTextChanged="txtAmountToPay_TextChanged"
                                                Style="text-align: right" TabIndex="52" Text='<%# Bind("Amount", "{0:0.00}") %>'></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmountToPay" runat="server" FilterType="Numbers, Custom"
                                                ValidChars="." TargetControlID="txtAmountToPay" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FSH_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("CLOSING") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="right">
                            <asp:LinkButton ID="lbtnAddMore" runat="server" Style="font-weight: bold;">Add More Fee Head(s)</asp:LinkButton>
                        </td>
                    </tr>
                    <tr id="tr_AddFee1" runat="server" class="title-bg" visible="false">
                        <td colspan="4" align="left">Add More Fee Head(s)</td>
                    </tr>
                    <tr id="tr_AddFee" runat="server" visible="false">
                        <td align="center" colspan="4">
                            <table style="width: 100%;" cellpadding="3">
                                <tr class="title-bg">
                                    <td align="center">Fee Type</td>
                                    <td align="center">Charge &amp; Post</td>
                                    <td align="center">Amount Paid</td>
                                    <td align="center">Amount</td>
                                    <td align="center">Tax Amount</td>
                                    <td align="center">Net Amount</td>
                                    <td align="center"></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:DropDownList ID="ddlFeeType" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="center">
                                        <asp:CheckBox ID="ChkChgPost" runat="server" Text="Charge & Post" Checked="True"></asp:CheckBox>
                                    </td>
                                    <td align="center">
                                        <asp:RadioButtonList ID="rblVAT" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                            <asp:ListItem Value="1">Including VAT</asp:ListItem>
                                            <asp:ListItem Value="2">Excluding VAT</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td align="center">

                                        <asp:TextBox ID="txtAmount" runat="server" onblur="CheckNumber(this)" Style="text-align: right" Text="0.00" AutoCompleteType="Disabled" AutoPostBack="True"
                                            TabIndex="45"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmount" runat="server" FilterType="Numbers, Custom"
                                            ValidChars="." TargetControlID="txtAmount" />

                                    </td>
                                    <td align="center">
                                        <asp:TextBox ID="txtVATAmount" Enabled="false" runat="server" Style="text-align: right" Text="0.00" onblur="CheckNumber(this)" AutoCompleteType="Disabled"
                                            TabIndex="46"></asp:TextBox>
                                    </td>
                                    <td align="center">
                                        <asp:TextBox ID="txtAmountAdd" Enabled="false" Style="text-align: right" runat="server" Text="0.00" onblur="CheckNumber(this)" AutoCompleteType="Disabled"
                                            TabIndex="47"></asp:TextBox>
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btnAddDetails" runat="server" CssClass="button" Text="Add" CausesValidation="False"
                                            TabIndex="50" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table style="width: 100%;">
                                <tr class="title-bg-lite">
                                    <th style="width: 15%; text-align: center;">
                                        <span class="field-label">GEMS Points</span>
                                    </th>
                                    <th style="width: 15%; text-align: center;">
                                        <span class="field-label">Cheque</span>
                                    </th>
                                    <th style="width: 55%; text-align: center;"><span class="field-label">Credit card</span>
                                    </th>
                                    <th style="width: 15%; text-align: center;"><span class="field-label">Cash</span>
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtRewardsAmount" Style="text-align: right;" runat="server" onFocus="this.select();" AutoCompleteType="Disabled" autocomplete="off">0.00</asp:TextBox>
                                        <span style="display: inline-block !important; margin-left: 2px;">
                                            <asp:Button ID="btnRedeem" runat="server" CssClass="button" Text="..." /></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtBankTotal" Style="text-align: right; width: 100%; min-width: 10% !important" runat="server" AutoCompleteType="Disabled" autocomplete="off"
                                            onblur="UpdateSum('','','CHQ');" onfocus="this.select();" TabIndex="60" Text="0.00"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtBankTotal" runat="server" FilterType="Numbers, Custom"
                                            ValidChars="." TargetControlID="txtBankTotal" />
                                        <ajaxToolkit:PopupControlExtender ID="pceCheque" runat="server" PopupControlID="pnlCheque"
                                            Position="Bottom" TargetControlID="txtBankTotal">
                                        </ajaxToolkit:PopupControlExtender>
                                    </td>
                                    <td>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td align="left" style="width: 50%;">
                                                    <asp:TextBox ID="txtCCTotal" onFocus="this.select();" Style="text-align: right;" runat="server" autocomplete="off"
                                                        AutoCompleteType="Disabled" onblur="UpdateSum('CRD','','');" TabIndex="70" Text="0.00"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtCCTotal" runat="server" FilterType="Numbers, Custom"
                                                        ValidChars="." TargetControlID="txtCCTotal" />
                                                    <ajaxToolkit:PopupControlExtender ID="pceCreditCard" runat="server" PopupControlID="pnlCreditCard"
                                                        Position="Bottom" TargetControlID="txtCCTotal">
                                                    </ajaxToolkit:PopupControlExtender>
                                                </td>
                                                <td style="width: 30%;">
                                                    <span>Credit Card Charge</span>
                                                </td>
                                                <td align="left" style="width: 20%;">

                                                    <asp:TextBox ID="txtCrCharge" Style="text-align: right; width: 100%;" runat="server" TabIndex="215"
                                                        Enabled="False">0.00</asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCashTotal" onFocus="this.select();" Style="text-align: right; width: 100%; min-width: 10% !important" autocomplete="off"
                                            runat="server" AutoCompleteType="Disabled" onblur="CheckNumber(this);UpdateSum('','CSH','');"
                                            TabIndex="80" Width="100%" Text="0.00"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtCashTotal" runat="server" FilterType="Numbers, Custom"
                                            ValidChars="." TargetControlID="txtCashTotal" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="table" style="width: 100%;">
                    <tr>
                        <td align="left" style="width: 15%;"
                            valign="middle" rowspan="3">
                            <span class="field-label">Narration</span>
                        </td>
                        <td align="left" style="width: 65%;"
                            valign="middle" rowspan="3">
                            <asp:TextBox ID="txtRemarks" runat="server" SkinID="MultiText" TabIndex="100" Width="100%"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td align="right"
                            valign="middle" style="width: 10%;"><span class="field-label">Total Fee</span>
                        </td>
                        <td align="left" style="width: 10%;"
                            valign="middle">
                            <asp:TextBox ID="txtTotal" SkinID="TextBoxCollection" Text="0.00" Style="text-align: right; width: 100%;" runat="server" TabIndex="210"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right"
                            valign="middle"><span class="field-label">Total Received</span>
                        </td>
                        <td align="left"
                            valign="middle">
                            <asp:TextBox ID="txtReceivedTotal" Style="text-align: right; width: 100%;" runat="server" TabIndex="215"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right"
                            valign="middle"><span class="field-label">Balance</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtBalance" runat="server" Style="text-align: right; width: 100%;" TabIndex="220"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" valign="bottom">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" TabIndex="155" Text="Save"
                                OnClientClick="UpdateSum('','','');" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" TabIndex="158" />
                            <asp:Button ID="btnNext" runat="server" CausesValidation="False" CssClass="button"
                                Text="Next" TabIndex="160" OnClick="btnNext_Click" />
                            <asp:CheckBox ID="chkRemember" runat="server" Text="Remember Payment Info" TabIndex="165" CssClass="field-label" />
                        </td>
                    </tr>
                </table>


                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                    PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" SelectMethod="SERVICES_BSU_M"
                    TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <%-- <asp:ObjectDataSource ID="odsGetFEETYPE_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="FEE_GETFEESFORCOLLECTION" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlBusinessunit" PropertyName="SelectedValue" Name="BSU_ID"
                            Type="String"></asp:ControlParameter>
                         <asp:ControlParameter ControlID="ddlAcademicYear" PropertyName="SelectedValue" Name="ACD_ID"
                            Type="Int32"></asp:ControlParameter>
                    </SelectParameters>
                </asp:ObjectDataSource>--%>
                <asp:HiddenField ID="h_Student_no" runat="server" />
                <asp:HiddenField ID="H_Location" runat="server" />
                <asp:HiddenField ID="h_du" runat="server" />
                <asp:HiddenField ID="h_PREV_ACD" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:Panel ID="Panel1" runat="server" Style="display: none" CssClass="panel-cover">
                    <table>
                        <tr>
                            <td align="left">
                                <img align="absMiddle" src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="lbtnLedger" runat="server">Student Ledger</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img align="absMiddle" src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="lbtnAudit" runat="server">Student Audit</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img align="absMiddle" src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="return ShowMoreInfo(1);">View Payment History</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton3" runat="server" OnClientClick="return ShowMoreInfo(2);">View Charge Details</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="lnkAdvReciept" runat="server" OnClientClick="return ShowMoreInfo(7);">View Advance Receipts</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton4" runat="server" OnClientClick="return ShowMoreInfo(3);">Sibling Informaion</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton5" runat="server" OnClientClick="return ShowMoreInfo(8);">Ageing</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton7" runat="server" OnClientClick="return ShowMoreInfo(4);">Concession Details</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton6" runat="server" OnClientClick="return ShowMoreInfo(5);">Fee Adjustment</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="return ShowMoreInfo(6);">Cheque History</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton8" runat="server" OnClientClick="return ShowMoreInfo(10);">Fee (Termly)</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton9" runat="server" OnClientClick="return ShowMoreInfo(11);">Fee (Monthly)</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlCreditCard" runat="server" Style="display: none" CssClass="panel-cover">
                    <table width="100%">
                        <tr class="title-bg">
                            <td align="left" colspan="2"><span class="field-label">Credit Card       </span>

                                <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/close_red.gif"
                                    OnClientClick="return HideAll();" Style="text-align: right" TabIndex="71" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="matters_Colln" rowspan="1"><span class="field-label">Card Type</span>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddCreditcard" runat="server" DataSourceID="SqlCreditCard" DataTextField="CRI_DESCR"
                                    DataValueField="CRR_ID" TabIndex="72" SkinID="DropDownListNormal" onChange="UpdateSum('CRD','','');">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="matters_Colln" rowspan="1"><span class="field-label">Authorisation Code</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtCreditno" runat="server" AutoCompleteType="Disabled" TabIndex="73"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="matters_Colln"><span class="field-label">Machine # </span>
                            </td>
                            <td align="left" colspan="1" style="font-size: 12pt">
                                <asp:DropDownList ID="ddMachineList" runat="server" DataSourceID="sqlMachineList"
                                    DataTextField="CCM_DESCR" DataValueField="CCM_ID" TabIndex="74">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="sqlMachineList" runat="server" ConnectionString="<%$ ConnectionStrings:OASIS_FEESConnectionString %>"
                                    SelectCommand="FEES.F_GETCREDITCARDFORUSER" SelectCommandType="StoredProcedure">
                                    <SelectParameters>
                                        <asp:SessionParameter SessionField="sBSUID" Type="String" Name="vCCM_BSU_ID"></asp:SessionParameter>
                                        <asp:ControlParameter PropertyName="SelectedValue" Type="String" Name="vCCM_ALLOC_BSU_ID"
                                            ControlID="ddlBusinessunit"></asp:ControlParameter>
                                        <asp:SessionParameter SessionField="sUsr_name" Type="String" Name="vCCM_USR_ID"></asp:SessionParameter>
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </table>
                    <asp:SqlDataSource ID="SqlCreditCard" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                        SelectCommand="exec GetCreditCardListForCollection 'TRANSPORT'"></asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnlCheque" runat="server" Style="display: none; width: 75% !important;" CssClass="panel-cover">
                    <table width="100%">
                        <tr class="title-bg">
                            <td align="left">Bank
                            </td>
                            <td align="left" id="tdhBA">Bank Account
                            </td>
                            <td align="left">Emirate
                            </td>
                            <td align="left">Cheque No
                            </td>
                            <td align="left">Cheque Date
                            </td>
                            <td align="left">Total
                            </td>
                            <td align="center">
                                <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/close_red.gif"
                                    OnClientClick="return HideAll();" TabIndex="157" />
                            </td>
                        </tr>
                        <tr class="gridheader_pop_orange">
                            <td align="left">
                                <asp:TextBox ID="txtBank" runat="server" SkinID="TextBoxCollection"
                                    TabIndex="61"></asp:TextBox><asp:ImageButton ID="ImageButton6" runat="server"
                                        ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif" TabIndex="62" OnClientClick="getBankOrEmirate(1,1,1); return false;" />
                                <asp:HiddenField ID="hf_BNK_TYPE" runat="server" />
                            </td>
                            <td id="tdBA1" align="left">
                                <asp:TextBox ID="txtBankAct" runat="server" onfocus="this.blur();" TabIndex="63"
                                    SkinID="TextBoxCollection"></asp:TextBox>
                                <asp:ImageButton ID="imgBankAct" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                    TabIndex="64" OnClientClick="getBankAct(1,1); return false;" /><asp:HiddenField ID="hfBankAct1" runat="server" />
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlEmirateMain" runat="server" DataSourceID="SqlEmirate_m"
                                    DataTextField="EMR_DESCR" DataValueField="EMR_CODE" SkinID="DropDownListNormal"
                                    TabIndex="65">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtChequeNo" runat="server" AutoCompleteType="Disabled" SkinID="TextBoxCollection"
                                    TabIndex="66"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtChqdate" runat="server" SkinID="TextBoxCollection" TabIndex="67"></asp:TextBox>
                                <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/calendar.gif"
                                    TabIndex="68" />
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlTotchqs" runat="server" TabIndex="69">
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>6</asp:ListItem>
                                    <asp:ListItem>7</asp:ListItem>
                                    <asp:ListItem>8</asp:ListItem>
                                    <asp:ListItem>9</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="center">
                                <asp:LinkButton ID="LinkButton10" runat="server">Apply</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td colspan="7" align="left">
                                <asp:GridView ID="gvChequeDetails" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                    Width="100%" OnRowDataBound="GridView1_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Bank">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtDBank" runat="server" SkinID="TextBoxCollection"
                                                    TabIndex="71" OnPreRender="txtDBank_PreRender"></asp:TextBox><asp:ImageButton
                                                        ID="imgBank1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                                        TabIndex="75" />
                                                <asp:HiddenField ID="h_Bank" runat="server" />
                                                <asp:HiddenField ID="hf_DBNK_TYPE" runat="server" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Account">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtDBankAct" runat="server" SkinID="TextBoxCollection"
                                                    TabIndex="71" onfocus="this.blur();"></asp:TextBox><asp:ImageButton
                                                        ID="imgDBankAct" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                                        TabIndex="75" />
                                                <asp:HiddenField ID="hf_DBankAct" runat="server" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Emirate">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlEmirate" runat="server" DataSourceID="SqlEmirate_m" DataTextField="EMR_DESCR"
                                                    DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="78">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cheque#">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtChqno" runat="server" AutoCompleteType="Disabled" SkinID="TextBoxCollection"
                                                    TabIndex="80"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtChqDate" Text='<%# Bind("DATE") %>' runat="server" SkinID="TextBoxCollection"
                                                    TabIndex="87"></asp:TextBox><asp:ImageButton ID="imgChequedate"
                                                        runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="88" />
                                                <ajaxToolkit:CalendarExtender ID="calCheque" runat="server" CssClass="MyCalendar"
                                                    Enabled="True" Format="dd/MMM/yyyy" PopupButtonID="imgChequedate" PopupPosition="TopLeft"
                                                    TargetControlID="txtChqDate">
                                                </ajaxToolkit:CalendarExtender>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtChequeTotal" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'
                                                    runat="server" AutoCompleteType="Disabled" onblur="UpdateSum('','','CHQ');" onfocus="this.select();"
                                                    SkinID="TextBoxCollection" Style="text-align: right" TabIndex="85"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="lblBankInvalid" runat="server" EnableViewState="False" SkinID="LabelError"></asp:Label>

                    <asp:SqlDataSource ID="SqlEmirate_m" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                        SelectCommand="SELECT [EMR_CODE], [EMR_DESCR] FROM [EMIRATE_M]"></asp:SqlDataSource>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" CssClass="MyCalendar" runat="server"
                        PopupButtonID="ImageButton5" TargetControlID="txtChqdate" Format="dd/MMM/yyyy">
                    </ajaxToolkit:CalendarExtender>
                </asp:Panel>
                <asp:Panel ID="PanelTree" runat="server" CssClass="panel-cover"
                    Style="display: none">
                    <div>
                        <asp:TreeView ID="trMonths" runat="server" onclick="client_OnTreeNodeChecked();"
                            ShowCheckBoxes="all">
                        </asp:TreeView>
                        <div style="text-align: center">
                            <asp:Button ID="btnNarrInsert" runat="server" CssClass="button" Text="Fill"
                                OnClick="btnNarrInsert_Click" />

                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hfCrCharges" runat="server" />
                <asp:HiddenField ID="h_BankId" runat="server" />
                <asp:HiddenField ID="hf_mode" runat="server" />
                <asp:HiddenField ID="hf_obj" runat="server" />
                <asp:HiddenField ID="hf_ctrl" runat="server" />
                <asp:HiddenField ID="hf_ctrl2" runat="server" />

            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
    <script type="text/javascript">
        function setTptRewardsValue(Amt) {
            $("#<%=txtRewardsAmount.ClientID%>").val(Amt);
            $.fancybox.close();
            UpdateSum('0', '0', '0', '0');
        }
    </script>
</asp:Content>
