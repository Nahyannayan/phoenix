Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class StudRecordView
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0
    Dim MainMnu_code As String = String.Empty
    Dim Encr_decrData As New Encryption64


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                gvStudentRecord.Attributes.Add("bordercolor", "#1b80b6")
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or MainMnu_code <> OASISConstants.MNU_FEE_STUDENT_MASTER Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"

                    callYEAR_DESCRBind()
                    Call gridbind()
                    If ViewState("datamode") = "edit" Then
                        Dim msg As String = String.Empty
                        msg = Encr_decrData.Decrypt(Request.QueryString("msg").Replace(" ", "+"))

                        'lblError.Text = msg
                        usrMessageBar.ShowNotification(msg, UserControls_usrMessageBar.WarningType.Danger)
                        ViewState("datamode") = "none"
                    End If
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

    End Sub

    Protected Sub lblStud_Name_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("action") = "select"
    End Sub

    Protected Sub ddlgvDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudentRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvStudentRecord.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudentRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudentRecord.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudentRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvStudentRecord.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvStudentRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudentRecord.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try
            Dim CurrentDatedType As String = String.Empty

            Dim ddlOPENONLINEH As New DropDownList

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_JOINDT As String = String.Empty
            Dim str_filter_STUDID As String = String.Empty
            Dim str_filter_STUDNAME As String = String.Empty
            Dim str_filter_GRADE As String = String.Empty
            Dim iDate As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

            Dim ds As New DataSet

            'CODE MODIFIED BY lijo 26-02-09
            str_Sql = " select eqs_id,STU_DOJ,Stu_ID,STU_No,STU_PASPRTNAME,GRM_DISPLAY,STU_bActive,STU_BSU_ID,ACD_CLM_ID,STU_ACD_ID,STU_GRD_ID,GRD_DISPLAYORDER,EQS_DOC_STG_ID from( " & _
                      " SELECT DISTINCT ISNULL(ENQUIRY_SCHOOLPRIO_S.EQS_ID, 0) AS EQS_ID, STUDENT_M.STU_DOJ , STUDENT_M.STU_ID , " & _
                      " ISNULL(STUDENT_M.STU_NO, '') AS STU_NO , ISNULL(STUDENT_M.STU_PASPRTNAME, '') AS STU_PASPRTNAME, ISNULL(GRADE_BSU_M.GRM_DISPLAY, '')+isnull(SECTION_M.SCT_DESCR,'') " & _
                      " AS GRM_DISPLAY, STUDENT_M.STU_bActive, STUDENT_M.STU_BSU_ID , ACADEMICYEAR_D.ACD_CLM_ID, " & _
                      " STUDENT_M.STU_ACD_ID, STUDENT_M.STU_GRD_ID, GRADE_M.GRD_DISPLAYORDER, " & _
                      " CASE WHEN EQS_DOC_STG_ID < 7 THEN '~/images/cross.gif' ELSE '~/Images/tick.gif' END AS EQS_DOC_STG_ID " & _
                      " FROM   STUDENT_M INNER JOIN  ACADEMICYEAR_D ON STUDENT_M.STU_ACD_ID = ACADEMICYEAR_D.ACD_ID INNER JOIN " & _
                      " GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND STUDENT_M.STU_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND " & _
                      "  STUDENT_M.STU_ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND STUDENT_M.STU_BSU_ID = GRADE_BSU_M.GRM_BSU_ID AND " & _
                      " ACADEMICYEAR_D.ACD_BSU_ID = GRADE_BSU_M.GRM_BSU_ID INNER JOIN " & _
                      " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID INNER JOIN " & _
                      " SECTION_M ON STUDENT_M.STU_SCT_ID = SECTION_M.SCT_ID AND STUDENT_M.STU_ACD_ID = SECTION_M.SCT_ACD_ID AND " & _
                      " STUDENT_M.STU_GRD_ID = SECTION_M.SCT_GRD_ID LEFT OUTER JOIN " & _
                      " ENQUIRY_SCHOOLPRIO_S ON STUDENT_M.STU_EQM_ENQID = ENQUIRY_SCHOOLPRIO_S.EQS_EQM_ENQID AND " & _
                     "  ENQUIRY_SCHOOLPRIO_S.EQS_BSU_ID = '" & Session("sBsuid") & "' " & _
                    " WHERE     (STUDENT_M.STU_CURRSTATUS <> 'CN') AND (CONVERT(datetime, STUDENT_M.STU_LEAVEDATE) > CONVERT(datetime, '" & iDate & "') OR " & _
                     " CONVERT(datetime, STUDENT_M.STU_LEAVEDATE) IS NULL) AND (STUDENT_M.STU_BSU_ID = '" + Session("SBSUID") + "') AND (ACADEMICYEAR_D.ACD_CLM_ID = '" & Session("CLM") & "') " & _
                     " AND (STUDENT_M.STU_CURRSTATUS = 'EN'))A WHERE A.STU_ID<>'' "


            Dim ACD_ID_year As String

            ACD_ID_year = " AND A.STU_ACD_ID='" & ddlAca_Year.SelectedItem.Value & "'"
            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_JOINDT As String = String.Empty
            Dim str_STUDID As String = String.Empty
            Dim str_STUDNAME As String = String.Empty
            Dim str_GRADE As String = String.Empty



            If gvStudentRecord.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvStudentRecord.HeaderRow.FindControl("txtJoinDT")
                str_JOINDT = txtSearch.Text
                Dim DOJ As String = "  DATENAME(day,A.STU_DOJ)+'/'+left(DATENAME(month,A.STU_DOJ),3)+'/'+DATENAME(year,A.STU_DOJ)"
                If str_search = "LI" Then
                    str_filter_JOINDT = " AND " & DOJ & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_JOINDT = "  AND  NOT " & DOJ & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_JOINDT = " AND " & DOJ & "  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_JOINDT = " AND " & DOJ & " NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_JOINDT = " AND " & DOJ & " LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_JOINDT = " AND " & DOJ & " NOT LIKE '%" & txtSearch.Text & "'"
                End If



                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvStudentRecord.HeaderRow.FindControl("txtstud_ID")
                str_STUDID = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_STUDID = " AND isnull(A.STU_NO,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_STUDID = "  AND  NOT isnull(A.STU_NO,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_STUDID = " AND isnull(A.STU_NO,'')  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_STUDID = " AND isnull(A.STU_NO,'') NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_STUDID = " AND isnull(A.STU_NO,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_STUDID = " AND isnull(A.STU_NO,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvStudentRecord.HeaderRow.FindControl("txtstud_Name")

                str_STUDNAME = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_STUDNAME = " AND isnull(A.STU_PASPRTNAME,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_STUDNAME = "  AND  NOT isnull(A.STU_PASPRTNAME,'')  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_STUDNAME = " AND isnull(A.STU_PASPRTNAME,'')   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_STUDNAME = " AND isnull(A.STU_PASPRTNAME,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_STUDNAME = " AND isnull(A.STU_PASPRTNAME,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_STUDNAME = " AND isnull(A.STU_PASPRTNAME,'')  NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvStudentRecord.HeaderRow.FindControl("txtStud_Grade")

                str_GRADE = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_GRADE = " AND isnull(A.GRM_DISPLAY,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_GRADE = "  AND  NOT isnull(A.GRM_DISPLAY,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_GRADE = " AND isnull(A.GRM_DISPLAY,'') LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_GRADE = " AND isnull(A.GRM_DISPLAY,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_GRADE = " AND isnull(A.GRM_DISPLAY,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_GRADE = " AND isnull(A.GRM_DISPLAY,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If




            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & ACD_ID_year & str_filter_JOINDT & str_filter_STUDID & str_filter_STUDNAME & str_filter_GRADE & "ORDER BY A.GRD_DISPLAYORDER,A.GRM_DISPLAY")
            '& ViewState("str_filter_Year") & str_filter_C_DESCR & str_filter_STARTDT & str_filter_ENDDT & CurrentDatedType & str_filter_OPENONLINE & "  order by  a.Y_DESCR")

            If ds.Tables(0).Rows.Count > 0 Then

                gvStudentRecord.DataSource = ds.Tables(0)
                gvStudentRecord.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(6) = True

                gvStudentRecord.DataSource = ds.Tables(0)
                Try
                    gvStudentRecord.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvStudentRecord.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvStudentRecord.Rows(0).Cells.Clear()
                gvStudentRecord.Rows(0).Cells.Add(New TableCell)
                gvStudentRecord.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudentRecord.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudentRecord.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            txtSearch = gvStudentRecord.HeaderRow.FindControl("txtJOINDT")
            txtSearch.Text = str_JOINDT
            txtSearch = gvStudentRecord.HeaderRow.FindControl("txtSTUD_ID")
            txtSearch.Text = str_STUDID
            txtSearch = gvStudentRecord.HeaderRow.FindControl("txtSTUD_Name")
            txtSearch.Text = str_STUDNAME
            txtSearch = gvStudentRecord.HeaderRow.FindControl("txtSTUD_Grade")
            txtSearch.Text = str_GRADE


            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub btnSearchStud_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchStud_ID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchJOINDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchGrade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvStudentRecord_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudentRecord.PageIndexChanging
        gvStudentRecord.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        '  ViewState("Y_DESCR") = sender.SelectedItem.Text
        gridbind()
    End Sub

    Public Sub callYEAR_DESCRBind()

        Try
            Dim Active_year As String
            Using Activereader As SqlDataReader = AccessStudentClass.GetActive_ACD_4_Grade(Session("sBsuid"), Session("CLM"))
                While (Activereader.Read())

                    Active_year = Convert.ToString(Activereader("Y_DESCR"))
                End While

            End Using

            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAca_Year.Items.Clear()
                di = New ListItem("Not Selected", "0")

                ddlAca_Year.Items.Add(di)
                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAca_Year.Items.Add(di)


                    End While
                End If
            End Using

            Dim ItemTypeCounter As Integer = 0
            For ItemTypeCounter = 0 To ddlAca_Year.Items.Count - 1
                If ddlAca_Year.Items(ItemTypeCounter).Text = Active_year Then
                    ddlAca_Year.SelectedIndex = ItemTypeCounter
                End If
            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblSTUD_ID As New Label
            Dim url As String
            Dim viewid As String
            lblSTUD_ID = TryCast(sender.FindControl("lblStudentID"), Label)
            viewid = lblSTUD_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            MainMnu_code = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("feeStudRecordEdit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub gvStudentRecord_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvStudentRecord.SelectedIndexChanged
        If ViewState("action") = "select" Then
            ViewState("action") = ""
            Dim lbleqsId As Label
            lbleqsId = gvStudentRecord.SelectedRow.FindControl("lbleqsid")
            Dim docStgId As Integer
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT ISNULL(EQS_DOC_STG_ID,0) FROM ENQUIRY_SCHOOLPRIO_S WHERE EQS_ID=" + lbleqsId.Text
            docStgId = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            Dim ib As Image
            ib = gvStudentRecord.SelectedRow.FindControl("imgDoc")
            If docStgId >= 7 Then
                ib.ImageUrl = "~/images/tick.gif"
            Else
                ib.ImageUrl = "~/images/cross.gif"
            End If
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

End Class
