﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="RewardDetailSearch.aspx.vb" Inherits="Fees_RewardsSearchSearch" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%--<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc2" %>--%>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>--%>
<%@ Register Src="~/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server" MasterPageFile="~/mainMasterPage.master">

    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>​
    <link type="text/css" href="../Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />


     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>   Gems Rewards Profile Search
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr valign="bottom">
                        <td align="left" colspan="3" valign="bottom">
                            <asp:Label ID="lblError" runat="server"   EnableViewState="False"></asp:Label></td>
                    </tr>
                </table>
                <table width="100%">
                       <tr>
                              
                        <td align="right" valign="middle" colspan="2">

                            <span class="field-label">
                             
                            <asp:RadioButton ID="rbStaff" runat="server" GroupName="user" Text="Staff" AutoPostBack="True"  />
                            <asp:RadioButton ID="rbParent" runat="server" GroupName="user" Text="Parent" AutoPostBack="True" checked="true"/>
                                </span>
                           </td>
                                                  
                        </tr>
                    <tr class="matters">
                      
                        <td class="matters" align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlBUnit" runat="server"  DataTextField="BSU_NAME" DataValueField="BSU_ID" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td class="matters" align="left" width="20%"><span class="field-label">Select Academic Year*</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server"   AutoPostBack="True">
                            </asp:DropDownList></td>
                          
                       

                        <tr>
                              <td class="matters" align="left" width="20%"><span class="field-label">Grade</span></td>
                        <td align="left" width="30%">


                                <asp:DropDownList ID="ddlgrade" runat="server"  AutoPostBack="True">
                            </asp:DropDownList></td>
                            <td class="matters" align="left" width="20%"><span class="field-label">Section</span></td>
                        <td align="left" width="30%">
                         

                            <asp:DropDownList ID="ddlSection" runat="server"  AutoPostBack="True">
                            </asp:DropDownList></td>
</td>

                                                  
                        </tr>

                  

                    <tr>
                          <td class="matters" align="left" width="20%"><span class="field-label">Student No/Employee No</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStudentNo" runat="server" ></asp:TextBox>
                            <asp:TextBox ID="txtEmpno" runat="server"  visible="false"></asp:TextBox>
                            </td>
                        <td class="matters" align="left" width="20%"><span class="field-label">Student Name/Employee Name</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStudentName" runat="server" ></asp:TextBox>
                             <asp:TextBox ID="txtEmployeename" runat="server" visible="false"></asp:TextBox>
                            </td>
                       
                    </tr>
                    <tr>
                         <td class="matters" align="left" width="20%"><span class="field-label">Mob No</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtMobNo" runat="server" ></asp:TextBox>
                            </td>
                        <td class="matters" align="left" width="20%"><span class="field-label">Email</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtEmail" runat="server" ></asp:TextBox>
                            </td>
                    </tr>
                        <tr>
                         <td class="matters" align="left" width="20%"><span class="field-label">Parent UserName</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtParentUsername" runat="server" ></asp:TextBox>
                            </td>
                        <td class="matters" align="left" width="20%"><span class="field-label">Parent Name</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtParentName" runat="server" ></asp:TextBox>
                            </td>
                    </tr>
                        <tr>
                         <td class="matters" align="left" width="20%"><span class="field-label">Customer Id</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCustomerID" runat="server" ></asp:TextBox>
                            </td>
                              <td class="matters" align="left" width="20%"><span class="field-label">Persistent Id</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtPersistentId" runat="server" ></asp:TextBox>
                            </td>
                        </tr>
                  <tr>
                                    <td align="center"  colspan="5">
                                        <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search"    />&nbsp;&nbsp;
                                    </td>
                                </tr><tr>
                                                            <td align="center" colspan="4"  valign="top">
                                        <asp:GridView ID="gvStudfeeSearch" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                             PageSize="20">
                                            <RowStyle CssClass="griditem"  HorizontalAlign="Center" />
                                          
                                            <Columns>

                                         

                                                <asp:TemplateField HeaderText="Student No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstudid" runat="server" Text='<%# Bind("STU_ID")%>' visible="false"></asp:Label>
                                                        <asp:Label ID="lblstudno" runat="server" Text='<%# Bind("STU_NO")%>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student Name" >
                                                    <ItemTemplate >
                                                        <asp:Label ID="lblstudname" runat="server" Text='<%# Bind("STU_NAME")%>' align="left"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Gender">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStudgender" runat="server" Text='<%# Bind("STU_GENDER")%>' ></asp:Label>


                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Mobile">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblmob" runat="server" Text='<%# Bind("PARENT_MOBILE")%>' ></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                     <asp:TemplateField HeaderText="Email">
                                                    <ItemTemplate>
                                                        <asp:Label ID="blbemail" runat="server" Text='<%# Bind("PARENT_EMAIL")%>' ></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                               
                                                     <asp:TemplateField HeaderText="Details">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkBtnView" text="View" runat="server" align="center" ></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                               <%-- <asp:ButtonField CommandName="View" Text="View" HeaderText="Details" Visible="true">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>--%>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>

                                    </td></tr>
                </table>
               <script type="text/javascript" lang="javascript">
                   function ShowWindowWithClose(gotourl, pageTitle, w, h) {
                       $.fancybox({
                           type: 'iframe',
                           //maxWidth: 300,
                           href: gotourl,
                           //maxHeight: 600,
                           fitToView: true,
                           padding: 6,
                           width: w,
                           height: h,
                           autoSize: false,
                           openEffect: 'none',
                           showLoading: true,
                           closeClick: true,
                           closeEffect: 'fade',
                           'closeBtn': true,
                           afterLoad: function () {
                               this.title = '';//ShowTitle(pageTitle);
                           },
                           helpers: {
                               overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                               title: { type: 'inside' }
                           },
                           onComplete: function () {
                               $("#fancybox-wrap").css({ 'top': '90px' });
                           },
                           onCleanup: function () {
                               var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                               if (hfPostBack == "Y")
                                   window.location.reload(true);
                           }
                       });

                       return false;
                   }
    </script>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>

</asp:Content>

