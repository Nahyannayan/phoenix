Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Fees_Reports_ASPX_FeeStudentDiscount
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            BindBusinessUnit()
            lblReportCaption.Text = "Calculate Fee Discount"
            ddBusinessunit.Items.FindByValue(Session("sBsuid")).Selected = True
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle

            'txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            'txtFromDate.Text = Format(DateTime.Now.AddMonths(-3), OASISConstants.DateFormat)
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            '  lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub

    Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID as SVB_BSU_ID, BSU_NAME from [fn_GetBusinessUnits] ('" & Session("sUsr_name") & "') order by BSU_NAME")
        ddBusinessunit.DataSource = ds.Tables(0)
        ddBusinessunit.DataBind()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Try
            If h_STUD_ID.Value = "" Then
                ' lblError.Text = "Please Select Student"
                usrMessageBar.ShowNotification("Please Select Student", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim sqlparam(7) As SqlParameter
            ' [dbo].[GetDiscountedFeesPayable_COBRAND](@STU_ID bigint,@CPM_ID int,@CLT_ID int,@AMOUNT numeric(18,3))
            sqlparam(0) = Mainclass.CreateSqlParameter("@STU_ID", h_STUD_ID.Value, SqlDbType.VarChar)
            sqlparam(1) = Mainclass.CreateSqlParameter("@STU_TYPE", "S", SqlDbType.VarChar)
            'sqlparam(2) = Mainclass.CreateSqlParameter("@CPM_ID", 3, SqlDbType.VarChar)
            sqlparam(3) = Mainclass.CreateSqlParameter("@CLT_ID", 3, SqlDbType.VarChar)
            sqlparam(4) = Mainclass.CreateSqlParameter("@AMOUNT", Val(txtAmount.Text), SqlDbType.Float)
            sqlparam(5) = Mainclass.CreateSqlParameter("@DISCOUNT", 0, SqlDbType.Float, True)
            sqlparam(6) = Mainclass.CreateSqlParameter("@CRR_ID", 0, SqlDbType.Int)
            sqlparam(7) = Mainclass.CreateSqlParameter("@FEE_ID", 5, SqlDbType.Int)
            Mainclass.ExecuteParamQRY(str_conn, "GetDiscountedFeesPayable_COBRAND", sqlparam)
            txtDiscount.Text = sqlparam(5).Value
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub h_STUD_ID_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim sqlStr As String
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            sqlStr = "SELECT STU_NO,GRD_DISPLAY,STU_DOJ  FROM VW_OSO_STUDENT_M where STU_ID='" & h_STUD_ID.Value & "'"
            Dim myDT As DataTable
            myDT = Mainclass.getDataTable(sqlStr, str_conn)
            If myDT.Rows.Count > 0 Then
                txtStudNo.Text = myDT.Rows(0).Item("STU_NO")
                txtDOJ.Text = Format(CDate(myDT.Rows(0).Item("STU_DOJ")), "dd/MMM/yyyy")
                txtGrade.Text = myDT.Rows(0).Item("GRD_DISPLAY")
            End If
            Dim sqlparam(1) As SqlParameter
            sqlparam(0) = Mainclass.CreateSqlParameter("@STU_ID", h_STUD_ID.Value, SqlDbType.VarChar)
            sqlparam(1) = Mainclass.CreateSqlParameter("@OUT_VALUE", 0, SqlDbType.Float, True)
            Mainclass.ExecuteParamQRY(str_conn, "[dbo].[GETSTUDENT_OUTSTANDING_AMOUNT]", sqlparam)
            txtOutstandingAmount.Text = sqlparam(1).Value
        Catch ex As Exception

        End Try
    End Sub


End Class

