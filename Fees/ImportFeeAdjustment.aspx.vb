Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb

Partial Class Fees_ImportFeeAdjustment
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property VSgvStLedger() As DataTable
        Get
            Return ViewState("gvStLedger")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvStLedger") = value
        End Set
    End Property
    Private Property VSgvExcelImport() As DataTable
        Get
            Return ViewState("gvExcelImport")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvExcelImport") = value
        End Set
    End Property
    Private Property VSGridRowCount() As Integer
        Get
            Return ViewState("GridRowCount")
        End Get
        Set(ByVal value As Integer)
            ViewState("GridRowCount") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New AjaxControlToolkit.ToolkitScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnImport)
        If Not IsPostBack Then
            Dim MainMnu_code As String

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            'lblError.Text = ""
            ViewState("datamode") = "none"
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                'disable the control based on the rights 
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            ClearAllFields()
            FillACD()
            FillAdjReason()
            SetAcademicYearDate()
            Me.txtAdjDT.Text = Format(Now.Date, OASISConstants.DateFormat)
            lnkXcelFormat.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDOWNLOAD") & "&Title=" & Encr_decrData.Encrypt("ADJUSTMENT") & "&Path=" & Encr_decrData.Encrypt(Web.Configuration.WebConfigurationManager.AppSettings("StudentPhotoPath").ToString() & "/Template/StudentAdjustmentsFormatFile.xls")
        End If
    End Sub
    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
    Sub FillAdjReason()
        '
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, "SELECT ARM_ID, ARM_DESCR FROM FEES.ADJ_REASON_M where ARM_bSHOW=1 order by ARM_DESCR")
        ddlAdjType.DataSource = ds.Tables(0)
        ddlAdjType.DataTextField = "ARM_DESCR"
        ddlAdjType.DataValueField = "ARM_ID"
        ddlAdjType.DataBind()
    End Sub
    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, FeeCommon.GetCurrentAcademicYear(Session("sBSUID")), Session("sBSUID"))
        txtFrom.Text = DTFROM
        If txtFrom.Text = "" Then
            txtFrom.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
        End If
        txtTo.Text = Format(Now.Date, OASISConstants.DateFormat)
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        UsrSelStudent1.ACD_ID = ddlAcademicYear.SelectedValue
        SetAcademicYearDate()
    End Sub

    Protected Sub UsrSelStudent1_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles UsrSelStudent1.StudentNoChanged
        StudentDetail()
    End Sub
    Sub StudentDetail()
        Me.lblName.Text = UsrSelStudent1.STUDENT_NAME
        Me.lblDOJ.Text = Format(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "select STU_DOJ from OASIS..STUDENT_M where STU_ID=" & UsrSelStudent1.STUDENT_ID & ""), OASISConstants.DateFormat)
        Me.lblGrade.Text = UsrSelStudent1.STUDENT_GRADE_ID
    End Sub
    Private Sub StudentLedger(ByVal STUID As String, ByVal BSUID As String, ByVal FromDT As String, ByVal ToDT As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(STUID, XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = BSUID
        cmd.Parameters.Add(sqlpBSU_ID)
        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        sqlpSTU_TYPE.Value = "S"
        cmd.Parameters.Add(sqlpSTU_TYPE)
        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = FromDT 'DateTime.Now.AddYears(-2).ToShortDateString
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = ToDT 'DateTime.Now.ToShortDateString
        cmd.Parameters.Add(sqlpTODT)
        cmd.Connection = New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        adpt.SelectCommand = cmd
        'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adpt.SelectCommand.CommandTimeout = 0
        ds.Clear()
        adpt.Fill(ds)
        Dim bal As Decimal = 0.0
        ds.Tables(0).Columns.Add("Balance")
        ds.Tables(0).AcceptChanges()
        For Each Dr As DataRow In ds.Tables(0).Rows
            bal += Convert.ToDecimal(Dr("DEBIT")) - Convert.ToDecimal(Dr("CREDIT"))
            Dr("Balance") = String.Format("{0:n2}", bal)
        Next
        ViewState("gvStLedger") = ds.Tables(0)
        bindLedger()
    End Sub
    Private Sub StudentLedgerWithTrans(ByVal STUID As String, ByVal BSUID As String, ByVal FromDT As String, ByVal ToDT As String, ByRef Trans As SqlTransaction)
        'Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(STUID, XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = BSUID
        cmd.Parameters.Add(sqlpBSU_ID)
        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        sqlpSTU_TYPE.Value = "S"
        cmd.Parameters.Add(sqlpSTU_TYPE)
        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = FromDT 'DateTime.Now.AddYears(-2).ToShortDateString
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = ToDT 'DateTime.Now.ToShortDateString
        cmd.Parameters.Add(sqlpTODT)
        cmd.Transaction = Trans
        cmd.Connection = Trans.Connection
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        adpt.SelectCommand = cmd
        'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adpt.SelectCommand.CommandTimeout = 0
        ds.Clear()
        adpt.Fill(ds)
        Dim bal As Decimal = 0.0
        ds.Tables(0).Columns.Add("Balance")
        ds.Tables(0).AcceptChanges()
        For Each Dr As DataRow In ds.Tables(0).Rows
            bal += Convert.ToDecimal(Dr("DEBIT")) - Convert.ToDecimal(Dr("CREDIT"))
            Dr("Balance") = String.Format("{0:n2}", bal)
        Next
        If DirectCast(ViewState("gvStLedger"), DataTable).Rows.Count < ds.Tables(0).Rows.Count Then
            ViewState("GridRowCount") = DirectCast(ViewState("gvStLedger"), DataTable).Rows.Count
        Else
            ViewState("GridRowCount") = 0
        End If
        ViewState("gvStLedger") = ds.Tables(0)
        bindLedger()
    End Sub
    Sub bindLedger()
        If Convert.ToString(VSgvStLedger) <> "" AndAlso VSgvStLedger.Rows.Count > 0 Then
            Me.trLedger.Visible = True
            gvStLedger.DataSource = VSgvStLedger
            gvStLedger.DataBind()
        Else
            Me.trLedger.Visible = False
        End If

    End Sub

    Private Sub ClearAllFields()
        Me.btnProceedImpt.Visible = False
        Me.lblError2.Text = ""
        'lblError.Text = ""
        Me.lblMessage.Text = ""
        'Me.lblError.Text = ""
        Me.btnImport.Visible = True
        Me.btnCommitImprt.Visible = False
        Me.gvExcelImport.Visible = True
        UsrSelStudent1.ClearDetails()
        If Not VSgvExcelImport Is Nothing Then
            VSgvExcelImport.Clear()
        End If
        If Not VSgvStLedger Is Nothing Then
            VSgvStLedger.Clear()
        End If
        BindExcel()
        SetAcademicYearDate()
        bindLedger()
        TabContainer1.ActiveTabIndex = 0
        Dim dsSummary As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "GET_FEEADJUSTMENTS_IMPORT_SUMMARY '0'")
        Me.gvImportSmry.DataSource = dsSummary.Tables(0)
        Me.gvImportSmry.DataBind()

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Call ClearAllFields()
    End Sub

    Private Dr As Decimal = 0
    Private i As Int32 = 0
    'Private Bal As Decimal = 0

    Protected Sub gvStLedger_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStLedger.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            e.Row.Cells(4).Text = "TOTAL"
            e.Row.Cells(5).Text = String.Format("{0:n2}", VSgvStLedger.Compute("Sum(DEBIT)", ""))
            e.Row.Cells(6).Text = String.Format("{0:n2}", VSgvStLedger.Compute("Sum(CREDIT)", ""))
            e.Row.Cells(7).Text = String.Format("{0:n2}", Convert.ToDecimal(e.Row.Cells(5).Text) - Convert.ToDecimal(e.Row.Cells(6).Text))
        ElseIf e.Row.RowType = DataControlRowType.DataRow Then
            i += 1
            If VSGridRowCount <> 0 And i > VSGridRowCount Then ' code to change the color of newly added rows in ledger
                e.Row.BackColor = Drawing.Color.BlanchedAlmond
            End If
        End If
    End Sub

    Protected Sub btnLedger_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLedger.Click

        StudentLedger(UsrSelStudent1.STUDENT_ID, Session("sBsuid"), Me.txtFrom.Text, Me.txtTo.Text)
    End Sub

    Protected Sub gvStLedger_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStLedger.PageIndexChanging
        gvStLedger.PageIndex = e.NewPageIndex
        bindLedger()
    End Sub

    Protected Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click
        'UpLoadDBF()
        Dim conn As New OleDbConnection
        'Me.lblError.Text = ""
        Dim errString As String = ""
        Try
            Dim str_conn As String = ""
            Dim cmd As New OleDbCommand
            Dim da As New OleDbDataAdapter
            Dim ds As DataSet
            Dim dtXcel As DataTable
            Dim FileName As String = ""
            If (FileUpload1.HasFile) Then
                Dim strFileType As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToString().ToLower()
                FileName = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload1.FileName.ToString()
                If strFileType = ".xls" Or strFileType = ".xlsx" Then
                    If File.Exists(FileName) Then
                        File.Delete(FileName)
                    End If
                    Me.FileUpload1.SaveAs(FileName)
                    str_conn = IIf(strFileType = ".xls", "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName + ";Extended Properties=""Excel 8.0;HDR=YES;""" _
                                    , "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileName & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2""")
                Else
                    'Me.lblError.Text = "Only Excel files are allowed"
                    errString = "Only Excel files are allowed"
                    usrMessageBar2.ShowNotification(errString, UserControls_usrMessageBar.WarningType.Danger)
                    Exit Try
                End If

                Dim Query = "SELECT * FROM [Sheet1$]"
                conn = New OleDbConnection(str_conn)
                'If conn.State = ConnectionState.Closed Then conn.Open()
                'cmd = New OleDbCommand(Query, conn)
                'da = New OleDbDataAdapter(cmd)
                'ds = New DataSet()
                'da.Fill(ds)
                'If conn.State = ConnectionState.Open Then
                '    conn.Close()
                'End If
                'dtXcel = ds.Tables(0)
                dtXcel = Mainclass.FetchFromExcelIntoDataTable(FileName, 1, 1, 5)
                If dtXcel.Rows.Count > 0 Then
                    VSgvExcelImport = dtXcel
                    BindExcel()
                    ValidateAndSave(dtXcel)
                Else
                    'Me.lblError.Text += "Empty Document!!"
                    errString = errString + "Empty Document!!"
                    usrMessageBar2.ShowNotification(errString, UserControls_usrMessageBar.WarningType.Danger)
                End If

            Else
                'Me.lblError.Text += "File not Found!!"
                errString = errString + "File not Found!!"
                usrMessageBar2.ShowNotification(errString, UserControls_usrMessageBar.WarningType.Danger)
                Me.FileUpload1.Focus()
            End If

            File.Delete(FileName) 'delete the file after copying the contents
        Catch
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            'Me.lblError.Text += Err.Description
            errString = errString + Err.Description
            usrMessageBar2.ShowNotification(errString, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Sub BindExcel()
        If Not VSgvExcelImport Is Nothing AndAlso VSgvExcelImport.Rows.Count > 0 Then
            Me.trGvImport.Visible = True
            Me.lblMessage.Text = "Click Proceed to Import the data"
            Me.gvExcelImport.DataSource = VSgvExcelImport
            Me.gvExcelImport.DataBind()
        Else
            Me.lblMessage.Text = ""
            Me.trGvImport.Visible = False
        End If

    End Sub

    Private Sub ValidateAndSave(ByRef DT As DataTable)
        Dim pParms(11) As SqlClient.SqlParameter
        Dim ErrorLog As String = ""
        Me.lblError2.Text = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim qry As String = "select ISNULL(MAX(FAI_BATCHNO),0)+1 from [DataImport].[FEE_ADJUSTMENT_IMPORT]"
        Dim FAI_BATCHNO As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, qry)
        ViewState("FAI_BATCHNO") = FAI_BATCHNO
        Dim i = 1
        Dim StuType As String = "S"
        If rbEnquiry.Checked Then
            StuType = "E"
        Else
            StuType = "S"
        End If


        Dim cmd As New SqlCommand
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim Success As Boolean = True
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        For Each Dr As DataRow In DT.Rows
            cmd.Dispose()
            cmd = New SqlCommand("FEES.ValidateAndInsertExceldata", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(pParms(1))
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
            pParms(0).Value = Session("sBsuid")
            cmd.Parameters.Add(pParms(0))
            pParms(2) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar)
            pParms(2).Value = Dr("STU_NO").ToString
            cmd.Parameters.Add(pParms(2))
            pParms(3) = New SqlClient.SqlParameter("@FEE_DESC", SqlDbType.VarChar)
            pParms(3).Value = Dr("FEE_DESCR").ToString
            cmd.Parameters.Add(pParms(3))
            pParms(4) = New SqlClient.SqlParameter("@DRCR", SqlDbType.VarChar, 2)
            pParms(4).Value = Dr("DRCR").ToString
            cmd.Parameters.Add(pParms(4))
            pParms(5) = New SqlClient.SqlParameter("@AMOUNT", SqlDbType.Decimal)
            pParms(5).Value = Dr("AMOUNT").ToString
            cmd.Parameters.Add(pParms(5))
            pParms(6) = New SqlClient.SqlParameter("@DATE", SqlDbType.DateTime)
            pParms(6).Value = Me.txtAdjDT.Text
            cmd.Parameters.Add(pParms(6))
            pParms(7) = New SqlClient.SqlParameter("@ADJ_TYPE_ID", SqlDbType.Int)
            pParms(7).Value = Me.ddlAdjType.SelectedValue
            cmd.Parameters.Add(pParms(7))
            pParms(8) = New SqlClient.SqlParameter("@NARRATION", SqlDbType.VarChar)
            pParms(8).Value = Me.txtNarration.Text.Trim
            cmd.Parameters.Add(pParms(8))
            pParms(9) = New SqlClient.SqlParameter("@FAI_BATCHNO", SqlDbType.Int)
            pParms(9).Value = FAI_BATCHNO
            cmd.Parameters.Add(pParms(9))
            pParms(11) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar)
            pParms(11).Value = StuType
            cmd.Parameters.Add(pParms(11))
            pParms(10) = New SqlClient.SqlParameter("@FAI_USER", SqlDbType.VarChar)
            pParms(10).Value = Session("sUsr_name")
            cmd.Parameters.Add(pParms(10))
            Dim retval As String
            cmd.ExecuteNonQuery()
            '            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "FEES.ValidateAndInsertExceldata", pParms)
            retval = pParms(1).Value
            If retval <> 0 Then
                Success = False
                If Me.gvExcelImport.Rows.Count > i Then
                    Me.gvExcelImport.Rows(i - 1).BackColor = Drawing.Color.Red
                End If
                ErrorLog = ErrorLog & "@" & retval & "#" & Convert.ToString(Dr("STU_NO")) ' keeping errono and line in a string variable for further use
            End If
            i += 1
        Next
        If Success = True Then
            stTrans.Commit()
        Else
            stTrans.Rollback()
        End If
        If ErrorLog <> "" Then
            Me.btnProceedImpt.Visible = False
            Me.btnImport.Visible = True
            Me.lblError2.Text = ""
            ShowErrorLog(ErrorLog)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, "delete from [DataImport].[FEE_ADJUSTMENT_IMPORT] where FAI_BATCHNO='" & FAI_BATCHNO & "'")
        Else
            Me.btnProceedImpt.Visible = True
            Me.btnImport.Visible = False
        End If
        If objConn.State = ConnectionState.Open Then
            objConn.Close()
        End If
    End Sub
    Private Sub ShowErrorLog(ByVal Err As String)
        Dim ReturnVal() As String = Err.Split("@")
        For i As Integer = 0 To ReturnVal.Length - 1
            If ReturnVal(i).Trim <> "" Then
                Dim aa() As String = ReturnVal(i).Split("#")
                Me.lblError2.Text = Me.lblError2.Text & "Student No(" & aa(1) & ") : "
                Me.lblError2.Text = Me.lblError2.Text & IIf(aa(0) = "100", "Not found,", IIf(aa(0) = "101", "No Fee Description,", IIf(aa(0) = "102", "Amount not found,", IIf(aa(0) = "103", "Select Fee Adjustment type,", ""))))
            End If
        Next
    End Sub

    Protected Sub gvExcelImport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvExcelImport.PageIndexChanging
        Me.gvExcelImport.PageIndex = e.NewPageIndex
        BindExcel()
    End Sub

    Protected Sub btnProceedImpt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceedImpt.Click
        ImportAdjustments(False)
    End Sub
    Private Sub ImportAdjustments(ByVal Commit As Boolean)
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        Dim dsSummary As New DataSet
        Dim sqlParam(2) As SqlParameter
        Dim sqlParam2(1) As SqlParameter

        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim cmd As New SqlCommand
            cmd.Dispose()
            cmd = New SqlCommand("IMPORT_FEE_ADJUSTMENTS", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            Dim retval As String = ""
            sqlParam(0) = Mainclass.CreateSqlParameter("@ReturnValue", retval, SqlDbType.VarChar, True, 200)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@FAI_BATCHNO", ViewState("FAI_BATCHNO"), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))

            'Mainclass.ExecuteParamQRY(objConn, stTrans, "IMPORT_FEE_ADJUSTMENTS", sqlParam)
            cmd.ExecuteNonQuery()
            retval = sqlParam(0).Value
            'Me.lblError.Text += retval

            If retval Is Nothing Or retval = "" Then
                dsSummary = SqlHelper.ExecuteDataset(stTrans, CommandType.Text, "GET_FEEADJUSTMENTS_IMPORT_SUMMARY '" & ViewState("FAI_BATCHNO") & "'")
                If dsSummary.Tables(0).Rows.Count > 0 Then
                    Me.btnCommitImprt.Visible = True
                    Me.trGvImport.Visible = False
                    Me.gvImportSmry.DataSource = dsSummary.Tables(0)
                    Me.gvImportSmry.DataBind()
                Else
                    Me.btnCommitImprt.Visible = False
                    Me.trGvImport.Visible = True
                End If
            End If

            If Commit = True Then
                Me.lblMessage.Text = ""
                stTrans.Commit()
                ClearAllFields()
                'Me.lblError.Text = "Data Saved successfully"
                usrMessageBar2.ShowNotification("Data Saved successfully", UserControls_usrMessageBar.WarningType.Success)
            Else
                Me.lblMessage.Text = "Click the Commit button to commit the transaction"
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'Me.lblError.Text = ex.Message
            usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        Me.btnProceedImpt.Visible = False
        Me.btnImport.Visible = True
    End Sub
    Protected Sub btnCommitImprt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCommitImprt.Click
        ImportAdjustments(True)
    End Sub

    Protected Sub gvExcelImport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvExcelImport.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim DR, CR As New Double
            VSgvExcelImport.Columns(3).DataType = GetType(String)
            e.Row.Cells(2).Text = "TOTAL (DR-CR)"
            DR = 0 : CR = 0
            For Each dtr As DataRow In VSgvExcelImport.Rows
                If dtr("DRCR") = "DR" Then
                    DR = DR + dtr("AMOUNT")
                ElseIf dtr("DRCR") = "CR" Then
                    CR = CR + dtr("AMOUNT")
                End If
            Next
            'DR = Convert.ToDouble(IIf(VSgvExcelImport.Compute("Sum(AMOUNT)", "DRCR = 'DR'").Equals(DBNull.Value), 0, VSgvExcelImport.Compute("Sum(AMOUNT)", "DRCR = 'DR'")))
            'CR = Convert.ToDouble(IIf(VSgvExcelImport.Compute("Sum(AMOUNT)", "DRCR = 'CR'").Equals(DBNull.Value), 0, VSgvExcelImport.Compute("Sum(AMOUNT)", "DRCR = 'CR'")))
            e.Row.Cells(4).Text = String.Format("{0:n2}", DR - CR)
            e.Row.Cells(0).Text = "Records(" & VSgvExcelImport.Rows.Count & ")"
            'ElseIf e.Row.RowType = DataControlRowType.DataRow Then
            '    i += 1
            '    If Val(ViewState("GridRowCount")) <> 0 And i > Val(ViewState("GridRowCount")) Then ' code to change the color of newly added rows in ledger
            '        e.Row.BackColor = Drawing.Color.BlanchedAlmond
            '    End If
        End If
    End Sub


    'Protected Sub lnkBLedger_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim lnkButton As LinkButton = TryCast(sender, LinkButton)
    '    If lnkButton.CommandName = "LEDGER" Then
    '        Dim row As GridViewRow = DirectCast(lnkButton.NamingContainer, GridViewRow)
    '        Dim stuid As Integer = Me.gvStudentCharges.DataKeys(row.RowIndex)("STU_ID")
    '        UsrSelStudent1.SetStudentDetails(stuid)
    '        'UsrSelStudent1.DataBind()
    '        UsrSelStudent1_StudentNoChanged(sender, e)
    '        'StudentLedger(stuid, Me.ddlBSU.SelectedValue, Me.txtFromDT.Text, Me.txtToDT.Text)
    '        'Me.txtFrom.Text = Me.txtFromDT.Text
    '        'Me.txtTo.Text = Me.txtToDT.Text
    '        TabContainer1.ActiveTabIndex = 1
    '    End If
    'End Sub

End Class
