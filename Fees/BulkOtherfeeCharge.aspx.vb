Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports Microsoft.VisualBasic
Partial Class Fees_BulkOtherfeeCharge
    Inherits BasePage

    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                ' gvGroup.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
               

                Session("liUserList") = New ArrayList

                Bind_All()
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid2(str_Sid_img(2))
       
    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    

    Private Sub Gridbind(Optional ByVal STUD_ID As String = "")
        Try
            Dim str_search, str_mode As String
           
            str_mode = Request.QueryString("ShowType") 'PARTY_D

            Dim str_filter_Date As String = String.Empty
            Dim str_filter_Fee As String = String.Empty
            Dim str_filter_Narration As String = String.Empty
            Dim str_Date As String = String.Empty
            Dim str_Fee As String = String.Empty
            Dim str_Narration As String = String.Empty

            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtSearchDate")
                str_Date = txtSearch.Text
                str_filter_Date = SetCondn(str_search, "FOH_DATE", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtSearchFee")
                str_Fee = txtSearch.Text
                str_filter_Fee = SetCondn(str_search, "FEE_DESCR", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtSearchNarration")
                str_Narration = txtSearch.Text
                str_filter_Narration = SetCondn(str_search, "FOH_NARRATION", Trim(txtSearch.Text))



            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
            Dim str_Sql As String = " SELECT OTH.FOH_ID, oasis.[dbo].[fnFormatDateToDisplay]( OTH.FOH_DATE) AS FOH_DATE, FEM.FEE_DESCR, " _
            & " OTH.FOH_NARRATION FROM FEES.FEEOTHCHARGE_H AS OTH INNER JOIN FEES.FEES_M AS FEM ON OTH.FOH_FEE_ID = FEM.FEE_ID " _
            & " WHERE ISNULL(FOH_bDELETED,0) = 0 AND FOH_BSU_ID ='" & Session("sBSuid") & "'"
            Dim str_orderby As String = " ORDER BY FEM.FEE_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text,
            str_Sql & str_filter_Date & str_filter_Fee &
            str_filter_Narration & str_orderby)

            'If (IsEnglish = False) Then
            '    For Each mRow As DataRow In ds.Tables(0).Rows
            '        mRow("FOH_DATE") = ConvertMonthName(mRow("FOH_DATE"))
            '    Next
            'End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = GetLocalResourceObject("Your Search query does not match any records. Kindly try with some other keywords.")
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtSearchDate")
            txtSearch.Text = str_Date
            txtSearch = gvGroup.HeaderRow.FindControl("txtSearchFee")
            txtSearch.Text = str_Fee
            txtSearch = gvGroup.HeaderRow.FindControl("txtSearchNarration")
            txtSearch.Text = str_Narration
            

            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        SetChk(Me.Page)
        Bind_All()
    End Sub

    Protected Sub DDAccountType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Bind_All()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkControl As New HtmlInputCheckBox
        Dim lbClose As New LinkButton
        lbClose = sender
        chkControl = sender.Parent.FindControl("chkControl")
       
        If (Not chkControl Is Nothing) Then

            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & chkControl.Value & "' ")
            'Response.Write("window.close();")
            'Response.Write("} </script>")


            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & chkControl.Value & "' ; ")
            Response.Write("var oWnd = GetRadWindow('" & chkControl.Value & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")

            h_SelectedId.Value = "Close"
        End If
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvGroup.PageIndex = 0
        Bind_All()
    End Sub

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Protected Sub chkSelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelAll.CheckedChanged
        If chkSelAll.Checked Then
            Dim str_Filter As String = GetFilter()
            Dim BUnitreaderSuper As SqlDataReader
           
            BUnitreaderSuper = GetAllStudentsList(str_Filter)
            
            If BUnitreaderSuper.HasRows = True Then
                While (BUnitreaderSuper.Read())
                    Session("liUserList").Remove(BUnitreaderSuper(0).ToString)
                    Session("liUserList").Add(BUnitreaderSuper(0).ToString)
                End While
            End If
        Else
            Session("liUserList").Clear()
        End If
        Bind_All()
        SetChk(Me.Page)

    End Sub

    Private Function GetFilter() As String
        Dim str_search As String

        Dim str_filter_Date As String = String.Empty
        Dim str_filter_Fee As String = String.Empty
        Dim str_filter_Narration As String = String.Empty
        Dim str_Date As String = String.Empty
        Dim str_Fee As String = String.Empty
        Dim str_Narration As String = String.Empty

        Dim txtSearch As New TextBox

        Dim str_Sid_search() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_search = h_Selected_menu_1.Value.Split("__")
        str_search = str_Sid_search(0)
        txtSearch = gvGroup.HeaderRow.FindControl("txtSearchDate")
        str_Date = txtSearch.Text
        str_filter_Date = SetCondn(str_search, "FOH_DATE", Trim(txtSearch.Text))
        ''name
        str_Sid_search = h_selected_menu_2.Value.Split("__")
        str_search = str_Sid_search(0)
        txtSearch = gvGroup.HeaderRow.FindControl("txtSearchFee")
        str_Fee = txtSearch.Text
        str_filter_Fee = SetCondn(str_search, "FEE_DESCR", Trim(txtSearch.Text))
        ''control
        str_Sid_search = h_Selected_menu_3.Value.Split("__")
        str_search = str_Sid_search(0)
        txtSearch = gvGroup.HeaderRow.FindControl("txtSearchNarration")
        str_Narration = txtSearch.Text
        str_filter_Narration = SetCondn(str_search, "FOH_NARRATION", Trim(txtSearch.Text))

        Return str_filter_Date & str_filter_Fee & str_filter_Narration
    End Function

   
    

    Private Function GetAllStudentsList(ByVal str_Filter As String) As SqlDataReader
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
        Dim str_Sql As String = " SELECT OTH.FOH_ID, REPLACE(CONVERT(CHAR, OTH.FOH_DATE, 106), ' ', '/') AS FOH_DATE, FEM.FEE_DESCR, " _
          & " OTH.FOH_NARRATION FROM FEES.FEEOTHCHARGE_H AS OTH INNER JOIN FEES.FEES_M AS FEM ON OTH.FOH_FEE_ID = FEM.FEE_ID "
        str_Sql = str_Sql & str_Filter
        Return SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
    End Function

    Protected Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        SetChk(Me.Page)
        h_SelectedId.Value = ""
        For i As Integer = 0 To Session("liUserList").Count - 1
            If h_SelectedId.Value <> "" Then
                h_SelectedId.Value += "||"
            End If
            h_SelectedId.Value += Session("liUserList")(i).ToString
        Next
        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        'Response.Write("window.close();")
        'Response.Write("} </script>")


        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.NameandCode ='" & h_SelectedId.Value & "' ; ")
        Response.Write("var oWnd = GetRadWindow('" & h_SelectedId.Value & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")

    End Sub

    Sub Bind_All()
        Gridbind()
    End Sub

End Class
