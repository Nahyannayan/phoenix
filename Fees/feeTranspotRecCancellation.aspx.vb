Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeTranspotCollection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "F300190" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            ddBusinessunit.DataBind()
            ViewState("ID") = 1
            ClearAll()
            GET_COOKIE()
        End If
    End Sub

    Function ValidateRecieptNo(ByVal recNo As String, ByVal vBSUID As String) As Boolean
        Dim CommandText As String = "SELECT COUNT(FCL_ID) FROM [FEES].[FEECOLLECTION_H] WHERE FCL_BSU_ID='" & Session("sBsuid") & "' " & _
            "AND FCL_STU_BSU_ID = '" & vBSUID & "' AND FCL_RECNO='" & recNo & "' AND ISNULL(FCL_bDELETED,0) = 0"
        Dim vCount As Integer = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, CommandText)
        If vCount > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Function GET_RECEIPT_DETAILS(ByVal pSTU_BSU_ID As String, Optional ByVal pRECEIPTNO As String = "", Optional ByVal pFCL_ID As Long = 0) As Boolean
        GET_RECEIPT_DETAILS = False
        Try
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@PROVIDER_BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuid")
            pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = pSTU_BSU_ID
            pParms(2) = New SqlClient.SqlParameter("@RECEIPT_NO", SqlDbType.VarChar, 20)
            pParms(2).Value = pRECEIPTNO
            pParms(3) = New SqlClient.SqlParameter("@FCL_ID", SqlDbType.BigInt)
            pParms(3).Value = pFCL_ID

            Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
              CommandType.StoredProcedure, "FEES.GET_RECEIPT_DETAILS", pParms)
            If Not dsData Is Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
                GET_RECEIPT_DETAILS = True
                lblStuNo.Text = dsData.Tables(0).Rows(0)("STU_NO").ToString
                lblStudentName.Text = dsData.Tables(0).Rows(0)("STU_NAME").ToString
                lblReceiptNo.Text = dsData.Tables(0).Rows(0)("FCL_RECNO").ToString
                txtRecieptNo.Text = lblReceiptNo.Text
                lblReceiptDate.Text = dsData.Tables(0).Rows(0)("FCL_DATE").ToString
                lblAmount.Text = Format(FeeCollection.GetDoubleVal(dsData.Tables(0).Rows(0)("FCL_AMOUNT")), "#,##0.00")
                lblReceiptCreatedOn.Text = dsData.Tables(0).Rows(0)("FCL_LOGDATE").ToString
                lblReceiptCreatedBy.Text = dsData.Tables(0).Rows(0)("FCL_EMP_ID").ToString
            Else
                usrMessageBar.ShowNotification("Unable to find the receipt details", UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            usrMessageBar.ShowNotification("Error : " & ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Function

    Private Function CheckErrors() As Boolean
        CheckErrors = True
        Try
            If CDate(txtDate.Text) > Date.Now Then
                'lblError.Text = "Future Date is not allowed"
                usrMessageBar.ShowNotification("Future Date is not allowed", UserControls_usrMessageBar.WarningType.Danger)
                Return False
            End If
        Catch
            'lblError.Text = "Date is not valid"
            usrMessageBar.ShowNotification("Invalid date selection", UserControls_usrMessageBar.WarningType.Danger)
            Return False
        End Try
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Not CheckErrors() Then Return
        If Not ValidateRecieptNo(txtRecieptNo.Text, ddBusinessunit.SelectedValue) Then
            usrMessageBar.ShowNotification("Receipt No is not Valid...", UserControls_usrMessageBar.WarningType.Danger)
            Return
        End If
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim conn As SqlConnection = New SqlConnection(str_conn)
        Dim trans As SqlTransaction = Nothing
        Try
            conn.Open()
            trans = conn.BeginTransaction("DeleteReceipt_TRANS")

            Dim cmd As New SqlCommand("[DeleteReceipt]", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpAUD_BSU_ID As New SqlParameter("@AUD_BSU_ID", SqlDbType.VarChar, 20)
            sqlpAUD_BSU_ID.Value = ddBusinessunit.SelectedValue
            cmd.Parameters.Add(sqlpAUD_BSU_ID)

            Dim sqlpFCL_RECNO As New SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 20)
            sqlpFCL_RECNO.Value = txtRecieptNo.Text
            cmd.Parameters.Add(sqlpFCL_RECNO)


            Dim sqlpAUD_USER As New SqlParameter("@AUD_USER", SqlDbType.VarChar, 50)
            sqlpAUD_USER.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpAUD_USER)

            Dim sqlpAUD_REMARKS As New SqlParameter("@AUD_REMARKS", SqlDbType.VarChar, 100)
            sqlpAUD_REMARKS.Value = txtRemarks.Text
            cmd.Parameters.Add(sqlpAUD_REMARKS)

            Dim sqlpFromDT As New SqlParameter("@DT", SqlDbType.DateTime)
            sqlpFromDT.Value = CDate(txtDate.Text)
            cmd.Parameters.Add(sqlpFromDT)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            Dim iReturnvalue As Integer = retSValParam.Value
            If iReturnvalue <> 0 Then
                trans.Rollback()
                'lblError.Text = "Error occured while deleting : " & UtilityObj.getErrorMessage(iReturnvalue)
                usrMessageBar.ShowNotification("Error occured while deleting : " & UtilityObj.getErrorMessage(iReturnvalue), UserControls_usrMessageBar.WarningType.Danger)
            Else
                trans.Commit()
                'lblError.Text = "Receipt deleted Successfully.."
                usrMessageBar.ShowNotification("Receipt deleted Successfully..", UserControls_usrMessageBar.WarningType.Success)
                ClearAll()
            End If
        Catch ex As Exception
            'lblError.Text = "Error occured while deleting  " & ex.Message
            usrMessageBar.ShowNotification("Error occured while deleting  " & ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            trans.Rollback()
        Finally
            If conn.State <> ConnectionState.Closed Then
                conn.Dispose()
            End If
        End Try
    End Sub

    Private Sub ClearAll()
        txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
        txtRecieptNo.Text = ""
        txtRemarks.Text = ""
        ddBusinessunit.ClearSelection()
        lblAmount.Text = "0.00"
        lblReceiptCreatedBy.Text = ""
        lblReceiptCreatedOn.Text = ""
        lblReceiptDate.Text = ""
        lblReceiptNo.Text = ""
        lblStudentName.Text = ""
        lblStuNo.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub txtRecieptNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRecieptNo.TextChanged
        GET_RECEIPT_DETAILS(ddBusinessunit.SelectedValue, txtRecieptNo.Text.Trim)
    End Sub

    Private Sub SET_COOKIE()
        Dim OasisSettingsCookie As HttpCookie
        If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
            OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
        Else
            OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
        End If
        OasisSettingsCookie.Values("TPTBSUID") = ddBusinessunit.SelectedValue
        OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
        Response.Cookies.Add(OasisSettingsCookie)
    End Sub
    Private Sub GET_COOKIE()
        If Not Request.Cookies("OASIS_SETTINGS") Is Nothing Then
            ddBusinessunit.SelectedValue = Request.Cookies("OASIS_SETTINGS")("TPTBSUID")
        End If
    End Sub

    Protected Sub ddBusinessunit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddBusinessunit.SelectedIndexChanged
        SET_COOKIE()
    End Sub

    Protected Sub hfFCLID_ValueChanged(sender As Object, e As EventArgs) Handles hfFCLID.ValueChanged
        GET_RECEIPT_DETAILS(ddBusinessunit.SelectedValue, "", hfFCLID.Value)
    End Sub
End Class
