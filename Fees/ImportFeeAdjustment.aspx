<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ImportFeeAdjustment.aspx.vb" Inherits="Fees_ImportFeeAdjustment"
    Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script src="http://ajax.microsoft.com/ajax/jquery/jquery-1.3.2.js" type="text/javascript"></script>

    <script src="http://ajax.microsoft.com/ajax/beta/0911/Start.debug.js" type="text/javascript"></script>

    <script src="http://ajax.microsoft.com/ajax/beta/0911/extended/ExtendedControls.debug.js"
        type="text/javascript"></script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server" Text="Import Fee Adjustment"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="FEE_COUNTER" />
                <br />
                <table id="Table1" align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
                    <%--<tr class="subheader_img">
                        <td align="left" valign="middle" style="height: 20px">
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            <table align="left" width="100%">
                                <tr>
                                    <td align="left">
                                        <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
                                            <asp:TabPanel ID="TabPanel2" HeaderText="Adjustment Import" runat="server">
                                                <ContentTemplate>
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">Date</span>
                                                                </td>

                                                                <td align="left" width="30%">
                                                                    <asp:TextBox ID="txtAdjDT" TabIndex="2" runat="server" AutoPostBack="True"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgAdjDT" TabIndex="4" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                                                    <asp:CalendarExtender ID="CalendarExtender4" runat="server" CssClass="MyCalendar"
                                                                        TargetControlID="txtAdjDT" Enabled="True" PopupButtonID="imgAdjDT" Format="dd/MMM/yyyy">
                                                                    </asp:CalendarExtender>
                                                                </td>
                                                                <td align="left" width="20%"></td>
                                                                <td align="left" width="30%"></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">Select File</span>
                                                                </td>

                                                                <td align="left" width="30%">
                                                                    <asp:FileUpload ID="FileUpload1" runat="server"></asp:FileUpload>
                                                                </td>
                                                                <td align="left" colspan="2">
                                                                    <asp:HyperLink ID="lnkXcelFormat" runat="server">Click here to get the formatted Excel file</asp:HyperLink></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">Adjustment Type</span>
                                                                </td>
                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddlAdjType" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" width="20%"></td>
                                                                <td align="left" width="30%"></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">Student Type</span></td>
                                                                <td align="left" colspan="3">
                                                                    <asp:RadioButton ID="rbEnrollment" TabIndex="1" runat="server" Text="Enrollment#"
                                                                        AutoPostBack="True" Checked="True" GroupName="mode"></asp:RadioButton><asp:RadioButton ID="rbEnquiry"
                                                                            runat="server" Text="Enquiry#" AutoPostBack="True"
                                                                            GroupName="mode"></asp:RadioButton></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">Narration</span>
                                                                </td>
                                                                <td align="left" colspan="3">
                                                                    <asp:TextBox ID="txtNarration" runat="server" Height="100px" TextMode="MultiLine"
                                                                        MaxLength="300"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" colspan="4">
                                                                    <asp:Button ID="btnImport" runat="server" Text="Load" CssClass="button"></asp:Button>
                                                                    <asp:Button ID="btnProceedImpt" runat="server" Text="Proceed" CssClass="button" Visible="False"></asp:Button>
                                                                    <asp:Button ID="btnCommitImprt" runat="server" Text="Commit" CssClass="button" Visible="False"></asp:Button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="matters" align="center" colspan="3">
                                                                    <asp:Label ID="lblError2" runat="server" CssClass="error"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" colspan="4">
                                                                    <asp:Label ID="lblMessage" runat="server" CssClass="error"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trGvImport" runat="server">
                                                                <td id="Td1" align="left" colspan="4" runat="server">
                                                                    <center>
                                                            <asp:GridView ID="gvExcelImport" runat="server" CssClass="table table-bordered table-row" OnPageIndexChanging="gvExcelImport_PageIndexChanging"
                                                                AllowPaging="True" EmptyDataText="No Data" Width="100%" AutoGenerateColumns="False"
                                                                ShowFooter="True" >
                                                                <Columns>
                                                                    <asp:BoundField DataField="STU_NO" HeaderText="Student.No">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="STU_NAME" HeaderText="Name">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Description">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DRCR" HeaderText="DRCR">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="AMOUNT" HeaderText="AMOUNT">
                                                                        <FooterStyle HorizontalAlign="Right"  >
                                                                        </FooterStyle>
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <FooterStyle ></FooterStyle>
                                                            </asp:GridView>
                                                        </center>
                                                                </td>
                                                            </tr>
                                                            <tr id="trgvImportSmry" runat="server">
                                                                <td id="Td2" colspan="4" runat="server">
                                                                    <center>
                                                            <asp:GridView ID="gvImportSmry" runat="server" CssClass="table table-bordered table-row" OnPageIndexChanging="gvExcelImport_PageIndexChanging"
                                                                AllowPaging="True" EmptyDataText="No Data" Width="100%" AutoGenerateColumns="False"
                                                                PageSize="20" >
                                                                <Columns>
                                                                    <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Description">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DRAMOUNT" DataFormatString="{0:n2}" HeaderText="Debit">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="CRAMOUNT" DataFormatString="{0:n2}" HeaderText="Credit">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </center>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:TabPanel>
                                            <asp:TabPanel ID="TabPanel1" HeaderText="Student Ledger" runat="server">
                                                <HeaderTemplate>
                                                    Student Ledger
                                                </HeaderTemplate>
                                                <ContentTemplate>
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">Academic Year</span>
                                                                </td>

                                                                <td align="left" width="30%">
                                                                    <asp:DropDownList ID="ddlAcademicYear" runat="server" SkinID="DropDownListNormal"
                                                                        AutoPostBack="True">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" width="20%"></td>
                                                                <td align="left" width="30%"></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">Student</span>
                                                                </td>
                                                               
                                                                <td align="left" colspan="4">
                                                                    <uc1:usrSelStudent ID="UsrSelStudent1" runat="server" ></uc1:usrSelStudent>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">From Date</span>
                                                                </td>                                                             
                                                                <td align="left" width="30%">
                                                                    <asp:TextBox ID="txtFrom" TabIndex="2" runat="server" AutoPostBack="True"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgFrom" TabIndex="4" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                                                    <asp:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                                                                        TargetControlID="txtFrom" Enabled="True" PopupButtonID="imgFrom" Format="dd/MMM/yyyy">
                                                                    </asp:CalendarExtender>
                                                                </td>
                                                                <td align="left" width="20%"><span class="field-label">To Date</span>
                                                                </td>                                                              
                                                                <td align="left" width="30%">
                                                                    <asp:TextBox ID="txtTo" TabIndex="2" runat="server" AutoPostBack="True"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgTo" TabIndex="4" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                                                        TargetControlID="txtTo" Enabled="True" PopupButtonID="imgTo" Format="dd/MMM/yyyy">
                                                                    </asp:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">Student</span>
                                                                </td>                                                              
                                                                <td align="left" width="30%">
                                                                    <asp:Label ID="lblName" runat="server"  CssClass="field-value"></asp:Label>
                                                                </td>
                                                                <td align="left" width="20%"><span class="field-label">DOJ</span>
                                                                </td>                                                               
                                                               <td align="left" width="30%">
                                                                    <asp:Label ID="lblDOJ" runat="server"  CssClass="field-value"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">Grade</span>
                                                                </td>                                                               
                                                                <td align="left" width="30%">
                                                                    <asp:Label ID="lblGrade" runat="server"  CssClass="field-value"></asp:Label>
                                                                </td>
                                                                <td align="center" colspan="2">
                                                                    <asp:Button ID="btnLedger" runat="server" Text="Show Ledger" CssClass="button" ></asp:Button>
                                                                </td>
                                                            </tr>
                                                            <tr id="trLedger" runat="server">
                                                                <td class="matters" align="center" colspan="4" runat="server">
                                                                    <asp:GridView ID="gvStLedger" runat="server" CssClass="table table-bordered table-row" AllowPaging="True"
                                                                        EmptyDataText="No Data" Width="100%" AutoGenerateColumns="False" ShowFooter="True">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="DOCDATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Doc.Date">
                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Description">
                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Grade">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblGrade" runat="server" Text='<%#Bind("GRD_DISPLAY") %>'>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="RECNO" HeaderText="RefNo">
                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="COLLECTIONTYPE" HeaderText="Collection Type">
                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="DEBIT" DataFormatString="{0:n2}" HeaderText="Debit">
                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="CREDIT" DataFormatString="{0:n2}" HeaderText="Credit">
                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="Balance" DataFormatString="{0:n2}" HeaderText="Balance">
                                                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                        <FooterStyle HorizontalAlign="Right" VerticalAlign="Bottom"></FooterStyle>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:TabPanel>
                                        </asp:TabContainer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
