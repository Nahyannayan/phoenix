﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FEEFGBOnlineCollection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub gvExcel_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvExcel.RowDataBound
        If Not e.Row.RowIndex.Equals(-1) AndAlso IsNumeric(e.Row.Cells(6).Text.ToString()) Then
            Amount.Text = Convert.ToDouble(Amount.Text.ToString()) + Convert.ToDouble(e.Row.Cells(6).Text.ToString())
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        Dim fileType As String = "DBF"
        If RdExcel.Checked Then
            fileType = "EXCEL"
        End If
        If (h_Editid.Value = "0") Then
            If Not uploadFile.HasFile Then
                ' lblError.Text = "Select Particular File...!"
                usrMessageBar.ShowNotification("Select Particular File...!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            If RdDbf.Checked Then
                If Not (uploadFile.FileName.EndsWith("dbf", StringComparison.OrdinalIgnoreCase)) Then
                    '   lblError.Text = "Invalid file type.. Only DBF files are alowed.!"
                    usrMessageBar.ShowNotification("Invalid file type.. Only DBF files are alowed.!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            Else
                If Not (uploadFile.FileName.EndsWith("xls", StringComparison.OrdinalIgnoreCase) Or uploadFile.FileName.EndsWith("xlsx", StringComparison.OrdinalIgnoreCase)) Then
                    ' lblError.Text = "Invalid file type.. Only Excel files are alowed.!"
                    usrMessageBar.ShowNotification("Invalid file type.. Only Excel files are alowed.!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            End If
            UpLoadDBF(fileType)
            'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else 
        End If
    End Sub

    Protected Sub RdNotprocessing_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RdNotprocessing.CheckedChanged
        bindData()
        RdNotprocessing.Checked = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            ' lblError.Text = "Invalid Bank Selected"
            usrMessageBar.ShowNotification("Invalid Bank Selected", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Try
            If CheckForErrors.Equals(True) Then
                Exit Sub
            End If
            doInsert()
            doClear()
        Catch ex As Exception
            Errorlog(ex.Message)
            '  lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
            usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Private Sub InsertTemtable(ByVal _table As DataTable)
        If _table.Rows.Count > 0 Then
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim SqlCmd As New SqlCommand("FEES.Insert_FEECOLLECTION_Temp", objConn, stTrans)
            SqlCmd.CommandType = CommandType.StoredProcedure
            Dim iReturnvalue As Integer = 0

            Dim x As Integer, Col As Integer
            Try
                For x = 0 To _table.Rows.Count - 1
                    Col = 0
                    For Col = 5 To _table.Columns.Count - 2 Step 2
                        If _table.Columns(Col).Caption <> "AMOUNT" And _table.Columns(Col).Caption <> "REFERENCE" Then
                            SqlCmd.Parameters.AddWithValue("@FCT_NO", _table.Rows(x)("REFERENCE"))
                            SqlCmd.Parameters.AddWithValue("@FCT_DATE", _table.Rows(x)("DATE"))
                            SqlCmd.Parameters.AddWithValue("@FCT_STUDENTID", _table.Rows(x)("STUDENTID"))
                            SqlCmd.Parameters.AddWithValue("@FCT_BANK", "FGB")
                            SqlCmd.Parameters.AddWithValue("@FCT_SOURCE", "FGBONLINE")
                            SqlCmd.Parameters.AddWithValue("@FCT_BSU_ID", Session("sBsuid"))
                            SqlCmd.Parameters.AddWithValue("@FCT_TOTAL", _table.Rows(x)("AMOUNT"))
                            SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                            SqlCmd.Parameters.AddWithValue("@FCT_FEECODE", _table.Rows(x)(Col))
                            SqlCmd.Parameters.AddWithValue("@FCT_PAID", _table.Rows(x)(Col + 1))
                            ' Col = Col + 1


                            SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                            SqlCmd.ExecuteNonQuery()
                            iReturnvalue = CInt(SqlCmd.Parameters("@ReturnValue").Value)

                            If iReturnvalue <> 0 Then
                                stTrans.Rollback()
                                'lblError.Text = getErrorMessage(iReturnvalue.ToString())
                                usrMessageBar.ShowNotification(getErrorMessage(iReturnvalue.ToString()), UserControls_usrMessageBar.WarningType.Danger)
                                Exit Sub
                            End If
                            SqlCmd.Parameters.Clear()
                        End If
                    Next
                Next
                If iReturnvalue <> 0 Then
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(iReturnvalue.ToString())
                    usrMessageBar.ShowNotification(getErrorMessage(iReturnvalue.ToString()), UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
                stTrans.Commit()

            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                '  lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
                usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try

            h_Editid.Value = "1"
            btnSave.Visible = True
            btnFind.Enabled = False
            bindData()

        End If
    End Sub

    Private Sub UpLoadDBF(ByVal flieType As String)
        Try
            If uploadFile.HasFile Then
                Dim FName As String = "FGB" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy ") & "-" & Date.Now.ToLongTimeString()
                FName += uploadFile.FileName.ToString().Substring(uploadFile.FileName.Length - 4)
                Dim filePath As String = WebConfigurationManager.ConnectionStrings("Documentpath").ConnectionString
                If Not Directory.Exists(filePath & "\OnlineExcel") Then
                    Directory.CreateDirectory(filePath & "\OnlineExcel")
                End If
                Dim FolderPath As String = filePath & "\OnlineExcel\"
                filePath = filePath & "\OnlineExcel\" & FName.Replace(":", "@")

                If uploadFile.HasFile Then
                    uploadFile.SaveAs(filePath)
                    Try
                        If flieType = "DBF" Then
                            getdata(FolderPath, uploadFile.FileName)
                        ElseIf flieType = "EXCEL" Then
                            getdataExcel(filePath)
                        End If
                        If File.Exists(filePath) Then
                            File.Delete(filePath)
                        End If
                    Catch ex As Exception
                        Errorlog(ex.Message)
                        '  lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
                        usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)
                    End Try
                End If
            End If
        Catch ex As Exception
            ' lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
            usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)
        End Try
       
    End Sub

    Public Sub getdataExcel(ByVal filePath As String)
        Try
            Dim excelQuery As String = ""
            'and year(DT) = " & Session("F_YEAR") & "   
            excelQuery = " SELECT * FROM  [TableName] "

            'excelQuery = "SELECT * from [Sheet1$]"
            Dim _table As DataTable
            'Commented by shakeel and added a new logic FetchFromExcelIntoDataTable..if not working then uncommnet it and comment the new logic
            '_table = Mainclass.FetchFromExcel(excelQuery, filePath)
            _table = Mainclass.FetchFromExcelIntoDataTable(filePath, 1, 1, 13)
            If _table.Rows.Count.Equals(0) Then
                Throw New Exception("Could not process of your  request.Date not match with excel..!")
            End If
            If h_Editid.Value.Equals("0") Then
                InsertTemtable(_table)
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            ' lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
            usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Public Sub getdata(ByVal filePath As String, ByVal Fname As String)
        Try
            Dim Query As String = ""
            'Query = " SELECT NO,DATE,STUDENTID,CASH,FEECODE,PAID,CHEQUE,CHKNO,CHKDATE,BANK  FROM  " & filePath & Fname
            Query = " SELECT * FROM  " & filePath & Fname
            Dim _table As DataTable
            _table = Mainclass.FecthFromDBFodbc(Query, filePath)
            If _table.Rows.Count.Equals(0) Then
                Throw New Exception("Could not process of you are request.date not match with excel..!")
            End If
            If h_Editid.Value.Equals("0") Then
                InsertTemtable(_table)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            '   lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )"
            usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED) & "( " & ex.Message & " )", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Private Sub bindData()
        Dim Query As String = ""
        Amount.Text = 0
        Query = " SELECT FEES.FEECOLLECTION_Temp.FCT_ID, " & _
                " VW_OSO_STUDENT_M.STU_NAME,  " & _
                " VW_OSO_STUDENT_M.STU_NO,  " & _
                " FEES.FEECOLLECTION_Temp.FCT_DATE DOCDT,  " & _
                "  FEES.FEECOLLECTION_Temp.FCT_PAID AMOUNT,  " & _
                "  FEES.FEES_M.FEE_DESCR  FEE_DESCR, FCT_SOURCE  NARRATION, FEE_ID  FEE_ID  " & _
                " FROM  " & _
                " FEES.FEECOLLECTION_Temp INNER JOIN " & _
                " FEES.FEES_M ON FEES.FEECOLLECTION_Temp.FCT_FEE_ID = FEES.FEES_M.FEE_ID INNER JOIN " & _
                " VW_OSO_STUDENT_M ON FEES.FEECOLLECTION_Temp.FCT_STU_ID = VW_OSO_STUDENT_M.STU_ID " & _
                " WHERE FCT_bProcessed = 0 and FCT_SOURCE='FGBONLINE' AND FCT_BSU_ID = '" & Session("sBsuid") & "' " & _
                "  ORDER BY VW_OSO_STUDENT_M.STU_NAME "
        Dim _table As DataTable = MainObj.getRecords(Query, "OASIS_FEESConnectionString")
        gvExcel.DataSource = _table
        gvExcel.DataBind()
        If _table.Rows.Count.Equals(0) Then
            ' lblError.Text = "No data found..."
            usrMessageBar.ShowNotification("No data found...", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If gvExcel.Rows.Count > 0 Then
            gvExcel.FooterRow.Cells(3).Text = "Total "
            gvExcel.FooterRow.Cells(6).Text = Format(Amount.Text, "#,###0.00")
            gvExcel.FooterRow.Cells(6).HorizontalAlign = HorizontalAlign.Right
            btnSave.Visible = True
        End If
        Amount.Text = Format(Convert.ToDouble(Amount.Text), "#,###0.00")
    End Sub

    Sub initialize_components()
        txtHDocdate.Text = DateTime.Now.ToString("dd/MMM/yyyy") 
    End Sub

    Private Function CheckForErrors() As Boolean
        Dim retValue As Boolean = True
        If txtHDocdate.Text.Equals("") Then
            '  lblError.Text = "Invalid Date...!"
            usrMessageBar.ShowNotification("Invalid Date...!", UserControls_usrMessageBar.WarningType.Danger)

        ElseIf h_Editid.Equals("0") Then
            ' lblError.Text = "Could not prosess you are request Please contact Admin...!"
            usrMessageBar.ShowNotification("Could not prosess you are request Please contact Admin...!", UserControls_usrMessageBar.WarningType.Danger)
        Else
            retValue = False

        End If
        Return retValue
    End Function

    Private Sub doInsert()
        Dim FctId As String = ""
        For Each grow As GridViewRow In gvExcel.Rows
            Dim lblFCT_ID As New Label
            lblFCT_ID = grow.FindControl("lblFCT_ID")
            FctId += lblFCT_ID.Text.ToString() & "@"
        Next
        Dim _parameter As String(,) = New String(6, 1) {}
        _parameter(0, 0) = "@FCT_BSUID"
        _parameter(0, 1) = Session("sBsuid")
        _parameter(1, 0) = "@FCT_DATE"
        _parameter(1, 1) = Convert.ToDateTime(txtHDocdate.Text.ToString()).ToString("dd-MMM-yyyy")
        _parameter(2, 0) = "@FCTID"
        _parameter(2, 1) = FctId
        _parameter(3, 0) = "@FCT_UserName"
        _parameter(3, 1) = Session("sUsr_name")
        _parameter(4, 0) = "@FCL_BANK_ACT_ID"
        _parameter(4, 1) = txtBankCode.Text

        MainObj.doExcutiveRetvalue("FEES.Save_FEECOLLECTION_FGB", _parameter, "OASIS_FEESConnectionString", "")
        ' lblError.Text = MainObj.MESSAGE
        usrMessageBar.ShowNotification(MainObj.MESSAGE, UserControls_usrMessageBar.WarningType.Danger)

        If MainObj.SPRETVALUE.Equals(0) Then
            'If (ChkPrint.Checked) Then
            '    printVoucher(FctId.Replace("@", ",") & "0")
            'End If
            'btnAdd.Visible = True
            btnSave.Visible = False
            btnFind.Enabled = True
        End If
    End Sub

    Private Sub doClear()
        gvExcel.DataSource = Nothing
        gvExcel.DataBind()
        h_Editid.Value = "0"
        btnSave.Visible = False
        ' RdExcel.Visible = False
        btnFind.Enabled = True
        ' btnAdd.Visible = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnFind)

        If Page.IsPostBack = False Then
            Amount.Text = ""
            doClear()

            initialize_components()

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A200354"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "F300306") Then
                'And ViewState("MainMnu_code") <> "A200310" And ViewState("MainMnu_code") <> "A200351"
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            UtilityObj.beforeLoopingControls(Me.Page)
        End If
        gvExcel.Attributes.Add("bordercolor", "#1b80b6")
    End Sub

    Protected Sub txtBankCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBankCode.TextChanged
        chk_bankcode()
    End Sub

    Sub chk_bankcode()
        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            ' lblError.Text = "Invalid bank selected"
            usrMessageBar.ShowNotification("Invalid bank selected", UserControls_usrMessageBar.WarningType.Danger)
            txtBankCode.Focus()
        Else
            lblError.Text = ""
            txtBankCode.Focus()
        End If
    End Sub

End Class
