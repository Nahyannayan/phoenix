﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeCardQuery.aspx.vb" Inherits="Fees_FeeCardQuery" Title="Loyalty Card Query" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" language="javascript">
        var isShift = false;
        function isNumeric(keyCode) {
            if (keyCode == 16) isShift = true;

            return ((keyCode >= 48 && keyCode <= 57 || keyCode == 8 || keyCode == 39 || keyCode == 46 || keyCode == 37 ||
            (keyCode >= 96 && keyCode <= 105)) && isShift == false);
        }

        function keyUP(keyCode) {
            if (keyCode == 16)
                isShift = false;

        }

        function ajaxToolkit_CalendarExtenderClientShowing(e) {
            if (!e.get_selectedDate() || !e.get_element().value)
                e._selectedDate = (new Date()).getDateOnly();

        }
        function HideAll() {
            document.getElementById('<%= pnlLeaveFilter.ClientID %>').style.display = 'none';
            return false;
        }
        function HideUnHide() {
            //var a = document.getElementById('<%= pnlLeaveFilter.ClientID %>;')
        //alert(document.getElementById('<%= pnlLeaveFilter.ClientID %>;'));
        if (document.getElementById('trLeave').style.display == 'none')
            document.getElementById('trLeave').style.display = '';
        else
            document.getElementById('trLeave').style.display = 'none';
        return false;
    }
    function CallEvent() {
        document.getElementById("ctl00_cphMasterpage_btnview").click();
        return false;
    }
    function checkDate(sender, args) {
    }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Loyalty Card Query
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tblAddLedger" runat="server" width="100%">
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table style="width: 100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">School<span style="color: #c00000">*</span></span></td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:DropDownList ID="ddlSchool" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                                    <td class="matters" align="left" width="20%"><span class="field-label">Student Name<span style="color: #c00000">*</span></span></td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:TextBox ID="txtStudentName" runat="server" MaxLength="100"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Date of Birth<span style="color: #c00000">*</span></span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtDOB" runat="server" CssClass="inputbox"></asp:TextBox>
                                        <asp:ImageButton ID="lnkDOB" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtDOB" PopupButtonID="lnkDOB" OnClientDateSelectionChanged="checkDate" OnClientShowing="ajaxToolkit_CalendarExtenderClientShowing">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                               
                                    <td align="left" class="matters"><span class="field-label">Grade<span style="color: #c00000">*</span></span></td>
                                    <td class="matters" align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                                    </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Division</span></td>
                                    <td class="matters" align="left">
                                        <asp:DropDownList ID="ddlSection" runat="server"></asp:DropDownList></td>
                             
                                    <td align="left" class="matters"><span class="field-label">Bsu Id<span style="color: #c00000">*</span></span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtBSUId" ReadOnly="true" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td><span class="field-label">Student Id<span style="color: #c00000">*</span></span></td>
                                    <td>
                                        <asp:TextBox ID="txtStudentId" runat="server" MaxLength="8"></asp:TextBox></td>
                               
                                    <td align="left" class="matters"><span class="field-label">Parent's Mobile No.</span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtMobileNo" runat="server" MaxLength="20"></asp:TextBox></td>
                                    </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="4">

                                        <asp:Button ID="btnview" runat="server" CssClass="button" Text="View" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="2">
                                        <asp:Button ID="btnSearch" runat="server" CausesValidation="False" CssClass="button" Text="Search" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                                        <asp:Button ID="btnClear" runat="server" CausesValidation="False" CssClass="button" Text="Clear" /></td>
                                    <td align="center" class="matters" colspan="2">
                                        <asp:LinkButton ID="lnkView" runat="server"
                                            OnClientClick="return HideUnHide();"  >View Leavers List</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr id="trLeave" style="display: none">
                                    <td align="center" class="matters" colspan="4">
                                        <asp:Panel ID="pnlLeaveFilter" runat="server" Width="100%">
                                            <table width="100%">
                                                <tr  >
                                                    <td width="20%"><span class="field-label">From Date</span></td>
                                                    <td width="30%">
                                                        <asp:TextBox ID="txtFrom" runat="server" CssClass="inputbox"></asp:TextBox>
                                                        <asp:ImageButton ID="lnkFrom" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtFrom" PopupButtonID="lnkFrom" OnClientDateSelectionChanged="checkDate" OnClientShowing="ajaxToolkit_CalendarExtenderClientShowing">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </td>
                                                    <td width="20%"><span class="field-label">To Date</span></td>
                                                    <td width="30%">
                                                        <asp:TextBox ID="txtTo" runat="server" CssClass="inputbox"></asp:TextBox>
                                                        <asp:ImageButton ID="lnkTo" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtTo" PopupButtonID="lnkTo" OnClientDateSelectionChanged="checkDate" OnClientShowing="ajaxToolkit_CalendarExtenderClientShowing">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnLoad" CausesValidation="false" OnClientClick="return CallEvent();" runat="server" Text="View" CssClass="button" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="4">
                                        <asp:HiddenField ID="h_EntryId" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_Mode" runat="server" />
                                        <asp:HiddenField ID="h_ColWidths" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="4">
                                        <asp:Panel ID="pnlResult" runat="server">
                                            <table style="width: 100%">
                                                <tr class="title-bg">
                                                    <td align="center" colspan="3" valign="middle">
                                                        <div align="center">Query Results</div>
                                                    </td>
                                                </tr>
                                                <asp:Repeater ID="rptResult" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td class="matters" align="left"><%#DataBinder.Eval(Container.DataItem, "label")%></td>
                                                            <td class="matters" align="left">
                                                                <asp:TextBox ID="txtResult" ReadOnly="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Text") %>'></asp:TextBox></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>

