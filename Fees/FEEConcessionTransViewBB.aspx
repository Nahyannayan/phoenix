<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FEEConcessionTransViewBB.aspx.vb" Inherits="Fees_FEEConcessionTransViewBB"
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
                function CheckForPrint() {
                    if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                        document.getElementById('<%= h_print.ClientID %>').value = '';
                        //showModalDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                        var url = "../Reports/ASPX Report/RptViewerModalView.aspx";
                        var oWnd = radopen(url, "pop_clickforprint");
                    }
                }
                    );



                function autoSizeWithCalendar(oWindow) {
                    var iframe = oWindow.get_contentFrame();
                    var body = iframe.contentWindow.document.body;

                    var height = body.scrollHeight;
                    var width = body.scrollWidth;

                    var iframeBounds = $telerik.getBounds(iframe);
                    var heightDelta = height - iframeBounds.height;
                    var widthDelta = width - iframeBounds.width;

                    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                    oWindow.center();
                }
            


                function change_chk_state(Checkbox) {
                    
                   var gvGroup = document.getElementById("<%=gvFEEConcessionDet.ClientID%>");
            for (i = 1; i < gvGroup.rows.length; i++) {
                var checkboxlist = gvGroup.rows[i].cells[9].getElementsByTagName("INPUT");
                for (j = 0; j < checkboxlist.length ; j++) {
                    gvGroup.rows[i].cells[9].getElementsByTagName("INPUT")[j].checked = Checkbox.checked;
                }
            }
        }
    </script>



    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_clickforprint" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table align="center" width="100%">
                    <tr valign="top">
                        <td valign="top" align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError"></asp:Label></td>
                        <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select Business Unit</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" SkinID="DropDownListNormal"
                                TabIndex="5">
                            </asp:DropDownList>
                        </td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList></td>
                        <td align="right" width="20%">
                            <asp:Label ID="lblDate" runat="server" CssClass="field-label" Visible="False">Date</asp:Label></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True" Visible="False"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4"
                                Visible="False" />
                        </td>

                    </tr>
                    <tr>
                        <td align="right" colspan="4">
                            <asp:RadioButton ID="rbUnposted" runat="server" AutoPostBack="True" Checked="True"
                                CssClass="radiobutton" GroupName="POST" Text="Open" />
                            <asp:RadioButton ID="rbPosted" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Text="Posted" />
                            <asp:RadioButton ID="rbRejected" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Text="Rejected" OnCheckedChanged="rbRejected_CheckedChanged" />
                            <asp:RadioButton ID="rbAll" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Height="16px" Text="All" /></td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="4">
                            <asp:GridView ID="gvFEEConcessionDet" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30" SkinID="GridViewView">
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="FCH_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFCH_ID" runat="server" Text='<%# Bind("FCH_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student ID">
                                        <HeaderTemplate>
                                            Student ID<br />
                                            <asp:TextBox ID="txtStuno" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchno" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="BtnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Name<br />
                                            <asp:TextBox ID="txtStuname" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" OnClick="BtnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Concession">
                                        <HeaderTemplate>
                                            Concession
                                <asp:TextBox ID="txtConcession" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnConcession" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="BtnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("FCM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date<br />
                                            <asp:TextBox ID="txtFrom" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnFrom" OnClick="BtnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("FCH_DT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Period">
                                        <HeaderTemplate>
                                            Period<br />
                                            <asp:TextBox ID="txtPeriod" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" OnClick="BtnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPeriod" runat="server" Text='<%# Bind("PERIOD")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderTemplate>
                                            Doc No<br />
                                            <asp:TextBox ID="txtRemarks" runat="server" Width="75%"></asp:TextBox>
                                                   
                                                        <asp:ImageButton ID="btnAmtSearcha" OnClick="BtnSearch_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                            ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("FCH_DOCNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FCH_BSU_SLNO" HeaderText="SL.No." />
                                    <asp:BoundField DataField="AMOUNT" HeaderText="Amount" />
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Post" Visible="False">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkPost" runat="server" />
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Post<input id="chkSelectall" onclick="change_chk_state(this)" type="checkbox" />
                                            &nbsp;
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbVoucher" runat="server" OnClick="lbVoucher_Click">Print</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FCH_STATUS" HeaderText="Status" />
                                </Columns>
                            </asp:GridView>
                            <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" Visible="False" />
                            <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" Visible="False" OnClick="btnReject_Click" />
                            <ajaxToolkit:CalendarExtender
                                ID="calendarButtonExtender" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                                SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                                <SelectParameters>
                                    <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                                    <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>




                <asp:HiddenField ID="h_print" runat="server" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server"
                        type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden"
                            value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />

            </div>
        </div>
    </div>
</asp:Content>
