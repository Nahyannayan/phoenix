Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports GemBox.Spreadsheet
Imports System.Collections.Generic
Imports System.Linq

Partial Class Fees_Onlinereview
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainObj As Mainclass = New Mainclass()
    Dim TAmnt As Double = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnFind)
        If Page.IsPostBack = False Then
            doClear()
            initialize_components()
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A200354"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "F300301") Then
                'And ViewState("MainMnu_code") <> "A200310" And ViewState("MainMnu_code") <> "A200351"
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                If ViewState("MainMnu_code").Equals("F300301") Then

                End If
            End If
            UtilityObj.beforeLoopingControls(Me.Page)
        End If
        gvExcel.Attributes.Add("bordercolor", "#1b80b6")
        btnSave.Visible = True
    End Sub

    Public Sub getdata(ByVal filePath As String, ByVal Fname As String)
        Try
            Dim Query As String = ""
            'Query = " SELECT NO,DATE,STUDENTID,CASH,FEECODE,PAID,CHEQUE,CHKNO,CHKDATE,BANK  FROM  " & filePath & Fname
            Query = " SELECT * FROM  " & filePath & Fname
            Dim _table As DataTable
            _table = Mainclass.FecthFromDBFodbc(Query, filePath)
            If _table.Rows.Count.Equals(0) Then
                Throw New Exception("Could not process of you are request.date not match with excel..!")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar2.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Public Sub getdataExcel(ByVal filePath As String)
        Try
            Dim excelQuery As String = ""
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            '  Dim ef As ExcelFile = New ExcelFile
            Dim ef = ExcelFile.Load(filePath)
            Dim mRowObj As ExcelRow
            mRowObj = ef.Worksheets(0).Rows(1)
            Dim allocatedcolumns As Integer = mRowObj.AllocatedCells.Count()

            excelQuery = " SELECT * FROM  [TableName]"
            'excelQuery = "SELECT * from [Sheet1$]"
            Dim _table As New DataTable
            _table = Mainclass.FetchFromExcelIntoDataTable(filePath, 1, 1, allocatedcolumns)
            If _table.Rows.Count.Equals(0) Then
                Throw New Exception("Could not process of you are request.Invalid data in excel..!")
            End If

            Dim i As Int16 = 0
            'Dim dCol As DataColumn
            'Dim _table_temp As New DataTable
            '_table_temp = _table.Copy
            '_table_temp.Clear()
            'For Each dCol In _table_temp.Columns
            '    If dCol.ColumnName <> "MerchantTransactionReference" And dCol.ColumnName <> "AcquirerResponseCode" Then
            '        _table.Columns.Remove(dCol.ColumnName)
            '    ElseIf dCol.ColumnName = "MerchantTransactionReference" Then
            '        _table.Columns(dCol.ColumnName).ColumnName = "FOCID"
            '    ElseIf dCol.ColumnName = "AcquirerResponseCode" Then
            '        _table.Columns(dCol.ColumnName).ColumnName = "STATUS"
            '    End If
            'Next

            Dim dtView As New DataView(_table)
            If (rbnindus.Checked = True) Then
                dtView.RowFilter = "Status = 'Paid'" ' Order Status = Captured
            ElseIf (rbnmigs.Checked = True) Then
                dtView.RowFilter = "AcquirerResponseCode = '0'" ' Order Status = Captured
            ElseIf (rbnmpgs.Checked = True) Then
                dtView.RowFilter = "OrderStatus = 'Captured'"
                Dim VerifyColumns = VerifyMPGSColumn(_table, New List(Of String)() From {"AuthorizationCode", "MerchantID", "OrderID", "OrderStatus", "CapturedAmount(amountonly)", "PaymentMethod"})
                If (VerifyColumns <> "") Then
                    usrMessageBar2.ShowNotification("Missing Column(s) [" + VerifyColumns + "]", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            ElseIf (rbnebpg.Checked = True) Then
                dtView.RowFilter = "status = 'C'"
            End If

            Session("ExcelTable") = Nothing
            Session("ExcelTable") = dtView.ToTable()
            getRefID(Session("ExcelTable"))



            'While i < allocatedcolumns
            '    If _table.Columns(i).ColumnName <> "MerchantTransactionReference" Or _table.Columns(i).ColumnName <> "AcquirerResponseCode" Then
            '        _table.Columns.RemoveAt(i)
            '        _table.AcceptChanges()
            '        i -= 1
            '    ElseIf _table.Columns(i).ColumnName = "MerchantTransactionReference" Then
            '        _table.Columns(i).ColumnName = "FOCID"
            '    ElseIf _table.Columns(i).ColumnName = "AcquirerResponseCode" Then
            '        _table.Columns(i).ColumnName = "STATUS"
            '    End If
            '    i += 1
            'End While

            '_table.Columns(5).ColumnName = "FOCID"
            '_table.Columns(13).ColumnName = "STATUS"

            'Dim dtView As New DataView(_table)
            'dtView.RowFilter = "STATUS = '0'"
            'Session("ExcelTable") = Nothing
            'Session("ExcelTable") = dtView.ToTable()
            'getRefID(Session("ExcelTable"))
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar2.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Private Function VerifyMPGSColumn(ByVal MPGSTable As DataTable, ByVal columnsNames As List(Of String)) As String

        Dim strColumn As String = ""
        Try
            If MPGSTable IsNot Nothing AndAlso MPGSTable.Rows.Count > 0 Then
                For Each columnName As String In columnsNames
                    If Not MPGSTable.Columns.Contains(columnName) Then
                        strColumn += columnName + "  "
                    End If
                Next
            End If
        Catch ex As Exception
        End Try
        Return strColumn
    End Function

    Sub initialize_components()
        Session("ExcelTable") = New DataTable
    End Sub

    Private Sub doClear()
        gvExcel.DataSource = Nothing
        gvExcel.DataBind()
        Session("ExcelTable") = Nothing
        HidUpload.Value = ""
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            doInsert()
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar2.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Private Sub UpLoadDBF()
        Try
            If uploadFile.HasFile Then
                Dim FName As String = "Online" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString()
                FName += uploadFile.FileName.ToString().Substring(uploadFile.FileName.Length - 4)
                Dim filePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
                If Not Directory.Exists(filePath & "\OnlineExcel") Then
                    Directory.CreateDirectory(filePath & "\OnlineExcel")
                End If
                Dim FolderPath As String = filePath & "\OnlineExcel\"
                filePath = filePath & "\OnlineExcel\" & FName.Replace(":", "@")

                If uploadFile.HasFile Then
                    If File.Exists(filePath) Then
                        File.Delete(filePath)
                    End If
                    uploadFile.SaveAs(filePath)

                    Try
                        getdataExcel(filePath)
                    Catch ex As Exception
                        Errorlog(ex.Message)
                        'lblError.Text = ex.Message 'getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
                        usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                    End Try
                End If
            End If
        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        If Not uploadFile.HasFile Then
            'lblError.Text = "Select Particular File...!"
            usrMessageBar2.ShowNotification("Select Particular File...! ", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        'If (rbnmpgs.Checked = False AndAlso rbnmigs.Checked = False AndAlso rbnindus.Checked = False) Then
        '    usrMessageBar2.ShowNotification("Select Collection...!  ", UserControls_usrMessageBar.WarningType.Danger)
        '    Exit Sub
        'End If
        doClear()
        If Not (uploadFile.FileName.EndsWith("xls", StringComparison.OrdinalIgnoreCase) Or uploadFile.FileName.EndsWith("xlsx", StringComparison.OrdinalIgnoreCase) Or uploadFile.FileName.EndsWith("csv", StringComparison.OrdinalIgnoreCase)) Then
            'lblError.Text = "Invalid file type.. Only Excel files are allowed.!"
            usrMessageBar2.ShowNotification("Invalid file type.. Only Excel files are allowed.!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        UpLoadDBF()
    End Sub

    Sub getDetails(ByVal FocId As String)
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
        pParms(0).Value = Session("sBsuID")
        pParms(1) = New SqlClient.SqlParameter("@FOC_IDs", SqlDbType.VarChar)
        pParms(1).Value = FocId
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString,
          CommandType.StoredProcedure, "FEES.OnlineFee_Review", pParms)
        gvExcel.DataSource = dsData.Tables(0)
        gvExcel.DataBind()
        If dsData.Tables(0).Rows.Count > 0 Then
            gvExcel.FooterRow.Cells(0).Text = dsData.Tables(0).Rows.Count.ToString()
            gvExcel.FooterRow.Cells(5).Text = TAmnt.ToString("##,#.00")
        Else
            'Me.lblError.Text &= " No Accounts are Listing"
            usrMessageBar2.ShowNotification(" No Accounts are Listing", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub

    Sub getMPGSDetails(ByVal _table As DataTable)

        Dim OrderIDs = _table.AsEnumerable().[Select](Function(s) s.Field(Of String)("OrderID")).Distinct().ToList()
        Dim Orders = String.Join(",", OrderIDs)
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ORDERID", SqlDbType.VarChar)
        pParms(0).Value = Orders

        Dim DS As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString,
          CommandType.StoredProcedure, "FEES.GETMPGSFEECOLLECTION", pParms)

        If (DS.Tables.Count > 0) Then
            Dim MPGSTable = From row1 In DS.Tables(0).AsEnumerable()
                            Join row2 In _table.AsEnumerable()
                            On row1.Field(Of Int64)("FCO_FCO_ID") Equals row2.Field(Of String)("OrderID")
                            Select row2

            If (MPGSTable.Count > 0) Then
                gvMPGS.DataSource = MPGSTable.CopyToDataTable()
                gvMPGS.DataBind()
            End If

        End If

    End Sub

    Sub getRefID(ByVal idTable As DataTable)

        Dim ColumnName As String = "MerchantTransactionReference"

        If (rbnmpgs.Checked = True) Then
            ColumnName = "OrderID"
        End If
        If (rbnindus.Checked = True) Then
            ColumnName = "TransactionRef"
        End If
        If (rbnebpg.Checked = True) Then
            ColumnName = "merchant_tranid"
        End If

        Dim x As Int32 = 0
        Dim FocId As String = ""
        For x = 0 To idTable.Rows.Count - 1
            If idTable.Rows(x)(ColumnName).ToString() <> "" Then ' 
                If FocId = "" Then
                    FocId = idTable.Rows(x)(ColumnName).ToString()
                Else
                    FocId += "@" & idTable.Rows(x)(ColumnName).ToString()
                End If
            End If
        Next
        HidUpload.Value = FocId
        getDetails(FocId)
    End Sub

    'Sub getRefID(ByVal idTable As DataTable)
    '    Dim x As Int32 = 0
    '    Dim FocId As String = ""
    '    For x = 0 To idTable.Rows.Count - 1
    '        If idTable.Rows(x)("FOCID").ToString() <> "" Then
    '            If FocId = "" Then
    '                FocId = idTable.Rows(x)("FOCID").ToString()
    '            Else
    '                FocId += "@" & idTable.Rows(x)("FOCID").ToString()
    '            End If
    '        End If
    '    Next
    '    HidUpload.Value = FocId
    '    getDetails(FocId)
    'End Sub

    Private Sub doInsert()
        Dim ReferenceColumnName As String = "MerchantTransactionReference"
        Dim ResponseCodeColumnName As String = "AcquirerResponseCode"
        If (rbnmpgs.Checked = True) Then
            ReferenceColumnName = "OrderID" 'Order Status = Captured
            ResponseCodeColumnName = "OrderStatus"
        End If
        If (rbnindus.Checked = True) Then
            ReferenceColumnName = "TransactionRef" 'Order Status = Captured
            ResponseCodeColumnName = "ReconStatus"
        End If
        If (rbnebpg.Checked = True) Then
            ReferenceColumnName = "merchant_tranid" 'Order Status = Captured
            ResponseCodeColumnName = "status"
        End If
        Dim FctId As String = ""
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim _parameter As String(,) = New String(6, 1) {}
        Try
            For Each grow As GridViewRow In gvExcel.Rows

                Dim ChkSelect As HtmlInputCheckBox = CType(grow.FindControl("ChkSelect"), HtmlInputCheckBox)
                Dim hdnAmount As HiddenField = CType(grow.FindControl("hdnAmount"), HiddenField)
                Dim hdnFCO_FCO_ID As HiddenField = CType(grow.FindControl("hdnFCO_FCO_ID"), HiddenField)

                If ChkSelect IsNot Nothing And ChkSelect.Checked Then
                    Dim filterExp As String = ReferenceColumnName & "='" & ChkSelect.Value & "'"
                    'Dim filterExp As String = "MerchantTransactionReference='" & hdnFCO_FCO_ID.Value & "'"
                    Dim _dtRow() As DataRow = Session("ExcelTable").Select(filterExp)
                    Dim pParms(20) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.Int)
                    '  pParms(0).Value = hdnFCO_FCO_ID.Value 
                    pParms(0).Value = ChkSelect.Value
                    pParms(1) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCECODE", SqlDbType.VarChar)
                    If (rbnmpgs.Checked = True) Then
                        pParms(1).Value = "0"
                    ElseIf (rbnindus.Checked = True) Then
                        pParms(1).Value = "0"
                    ElseIf (rbnebpg.Checked = True) Then
                        pParms(1).Value = _dtRow(0).Item("host_response_code").ToString()
                    Else
                        pParms(1).Value = _dtRow(0).Item("AcquirerResponseCode").ToString()
                    End If


                    pParms(2) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCEDESCR", SqlDbType.VarChar)
                    If (rbnmpgs.Checked = True) Then
                        pParms(2).Value = If(_dtRow(0).Item("OrderStatus").ToString() = "Captured", "Transaction Successful", "Unable to be determined")
                    ElseIf (rbnindus.Checked = True) Then
                        pParms(2).Value = If(_dtRow(0).Item("Status").ToString() = "Paid", "Transaction Successful", "Unable to be determined")
                    ElseIf (rbnebpg.Checked = True) Then
                        pParms(2).Value = If(_dtRow(0).Item("status").ToString() = "C", "Transaction Successful", "Unable to be determined")
                    Else
                        pParms(2).Value = _dtRow(0).Item("ResponseCode").ToString()
                    End If

                    pParms(3) = New SqlClient.SqlParameter("@FCO_VPC_MESSAGE", SqlDbType.VarChar)
                    If (rbnmpgs.Checked = True) Then
                        pParms(3).Value = If(_dtRow(0).Item("OrderStatus").ToString() = "Captured", "0", "")
                    ElseIf (rbnindus.Checked = True) Then
                        pParms(3).Value = If(_dtRow(0).Item("Status").ToString() = "Paid", "0", "")
                    ElseIf (rbnebpg.Checked = True) Then
                        pParms(3).Value = If(_dtRow(0).Item("status").ToString() = "C", "0", "")
                    Else
                        pParms(3).Value = _dtRow(0).Item("ResponseCode").ToString()
                    End If
                    pParms(4) = New SqlClient.SqlParameter("@FCO_VPC_RECNO", SqlDbType.VarChar)

                    If (rbnmpgs.Checked = True) Then
                        pParms(4).Value = If(_dtRow(0).Item("OrderStatus").ToString() = "Captured", "0", "")
                    ElseIf (rbnindus.Checked = True) Then
                        pParms(4).Value = If(_dtRow(0).Item("Status").ToString() = "Paid", "0", "")
                    ElseIf (rbnebpg.Checked = True) Then
                        pParms(4).Value = _dtRow(0).Item("transaction_id").ToString()
                    Else
                        pParms(4).Value = _dtRow(0).Item("TransactionID").ToString()
                    End If

                    pParms(5) = New SqlClient.SqlParameter("@FCO_VPC_TRANNO", SqlDbType.VarChar)

                    If (rbnmpgs.Checked = True) Then
                        pParms(5).Value = If(_dtRow(0).Item("OrderStatus").ToString() = "Captured", "0", "")
                    ElseIf (rbnindus.Checked = True) Then
                        pParms(4).Value = If(_dtRow(0).Item("Status").ToString() = "Paid", "0", "")
                    ElseIf (rbnebpg.Checked = True) Then
                        pParms(5).Value = _dtRow(0).Item("transaction_id").ToString()
                    Else
                        pParms(5).Value = _dtRow(0).Item("TransactionID").ToString()
                    End If

                    pParms(6) = New SqlClient.SqlParameter("@FCO_VPC_ACQRESCODE", SqlDbType.VarChar)
                    If (rbnmpgs.Checked = True) Then
                        pParms(6).Value = "00"
                    ElseIf (rbnindus.Checked = True) Then
                        pParms(6).Value = "00"
                    ElseIf (rbnebpg.Checked = True) Then
                        pParms(6).Value = _dtRow(0).Item("host_response_code").ToString()
                    Else
                        pParms(6).Value = _dtRow(0).Item("AcquirerResponseCode").ToString()
                    End If

                    pParms(7) = New SqlClient.SqlParameter("@FCO_VPC_BANKAUTID", SqlDbType.VarChar)

                    If (rbnmpgs.Checked = True) Then
                        pParms(7).Value = _dtRow(0).Item("AuthorizationCode").ToString()
                    ElseIf (rbnindus.Checked = True) Then
                        pParms(7).Value = _dtRow(0).Item("TransactionRef").ToString()
                    ElseIf (rbnebpg.Checked = True) Then
                        pParms(7).Value = _dtRow(0).Item("transaction_id").ToString()
                    Else
                        pParms(7).Value = _dtRow(0).Item("AuthorisationCode").ToString()
                    End If
                    pParms(8) = New SqlClient.SqlParameter("@FCO_VPC_BATCHNO", SqlDbType.VarChar)

                    If (rbnmpgs.Checked = True) Then
                        pParms(8).Value = _dtRow(0).Item("OrderID").ToString()
                    ElseIf (rbnindus.Checked = True) Then
                        pParms(8).Value = _dtRow(0).Item("TransactionRef").ToString()
                    ElseIf (rbnebpg.Checked = True) Then
                        pParms(8).Value = _dtRow(0).Item("merchant_tranid").ToString()
                    Else
                        pParms(8).Value = _dtRow(0).Item("BatchNumber").ToString()
                    End If

                  

                    pParms(9) = New SqlClient.SqlParameter("@FCO_VPC_CARDTYPE", SqlDbType.VarChar)
                    If (rbnmpgs.Checked = True) Then
                        Dim CardType As String = ""
                        Select Case _dtRow(0).Item("PaymentMethod").ToString()
                            Case "Mastercard"
                                CardType = "MC"
                            Case "Visa"
                                CardType = "VC"
                            Case "AmericanExpress"
                                CardType = "AE"
                        End Select
                        pParms(9).Value = CardType
                    ElseIf (rbnindus.Checked = True) Then
                        'pParms(9).Value = If(_dtRow(0).Item("ChallanPaymentMode").ToString() = "Debit Card", "DC", "CC")
                        If (_dtRow(0).Item("ChallanPaymentMode").ToString() = "Debit Card") Then
                            pParms(9).Value = "DC"
                        ElseIf (_dtRow(0).Item("ChallanPaymentMode").ToString() = "Credit Card") Then
                            pParms(9).Value = "CD"
                        ElseIf (_dtRow(0).Item("ChallanPaymentMode").ToString() = "Internet Banking") Then
                            pParms(9).Value = "IB"
                        End If
                    ElseIf (rbnebpg.Checked = True) Then
                        If (_dtRow(0).Item("card_type").ToString() = "A") Then
                            pParms(9).Value = "AE"
                        ElseIf (_dtRow(0).Item("card_type").ToString() = "M") Then
                            pParms(9).Value = "VCMC"
                        ElseIf (_dtRow(0).Item("card_type").ToString() = "V") Then
                            pParms(9).Value = "VCMC"
                        End If
                    Else
                        pParms(9).Value = "MC"
                        If _dtRow(0).Item(18).ToString().Trim() = "Visa" Then
                            pParms(9).Value = "VC"
                        ElseIf _dtRow(0).Item(18).ToString().Trim() = "American Express" Then
                            pParms(9).Value = "AE"
                        End If
                    End If

                    pParms(10) = New SqlClient.SqlParameter("@FCO_VPC_HASHCODE_RESULT", SqlDbType.VarChar)
                    pParms(10).Value = ""

                    pParms(11) = New SqlClient.SqlParameter("@FCO_VPC_ORDERINFO", SqlDbType.VarChar)
                    If (rbnmpgs.Checked = True) Then
                        pParms(11).Value = _dtRow(0).Item("OrderID").ToString()
                    ElseIf (rbnindus.Checked = True) Then
                        pParms(11).Value = _dtRow(0).Item("TransactionRef").ToString()
                    ElseIf (rbnebpg.Checked = True) Then
                        pParms(11).Value = _dtRow(0).Item("merchant_tranid").ToString()
                    Else
                        pParms(11).Value = _dtRow(0).Item("OrderReference").ToString()
                    End If


                    pParms(12) = New SqlClient.SqlParameter("@FCO_VPC_AMOUNT", SqlDbType.Decimal)

                    If (rbnmpgs.Checked = True) Then
                        pParms(12).Value = (Convert.ToDecimal(_dtRow(0).Item("CapturedAmount(amountonly)").ToString()) * 100).ToString()
                    ElseIf (rbnindus.Checked = True) Then
                        pParms(12).Value = _dtRow(0).Item("ChallanTotalAmount").ToString()
                    ElseIf (rbnebpg.Checked = True) Then
                        pParms(12).Value = (Convert.ToDecimal(_dtRow(0).Item("amount").ToString()) * 100).ToString()
                    Else
                        pParms(12).Value = (Convert.ToDecimal(_dtRow(0).Item("Amount").ToString()) * 100).ToString()

                    End If


                    'pParms(12).Value = (Convert.ToDecimal(CInt(hdnAmount.Value.ToString())) * 100).ToString()

                    pParms(13) = New SqlClient.SqlParameter("@FCO_VPC_MERCHANT_ID", SqlDbType.VarChar)
                    If (rbnindus.Checked = True) Then
                        pParms(13).Value = _dtRow(0).Item("MerchantAccountNumber").ToString()
                    ElseIf (rbnebpg.Checked = True) Then
                        pParms(13).Value = _dtRow(0).Item("merchant_acc_no").ToString()
                    Else
                        pParms(13).Value = _dtRow(0).Item("MerchantID").ToString()
                    End If

                    pParms(14) = New SqlClient.SqlParameter("@FCO_VPC_COMMAD", SqlDbType.VarChar)
                    pParms(14).Value = ""
                    pParms(15) = New SqlClient.SqlParameter("@FCO_VPC_VERSION", SqlDbType.VarChar)
                    pParms(15).Value = ""
                    pParms(16) = New SqlClient.SqlParameter("@FCO_VPC_3DS_INFO", SqlDbType.VarChar)
                    pParms(16).Value = ""
                    pParms(17) = New SqlClient.SqlParameter("@RESPONSE", SqlDbType.VarChar, 50)
                    pParms(17).Direction = ParameterDirection.Output
                    pParms(18) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 50)
                    pParms(18).Direction = ParameterDirection.Output
                    pParms(19) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                    pParms(19).Direction = ParameterDirection.ReturnValue
                    Dim retval As Integer
                    retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[FEES].[F_SaveFEECOLLECTION_H_ONLINE_PAYMENT]", pParms)
                    If pParms(19).Value = "0" And pParms(18).Value <> "" Then
                        'lblError.Text = pParms(17).Value.ToString()
                        usrMessageBar2.ShowNotification(pParms(17).Value.ToString(), UserControls_usrMessageBar.WarningType.Success)
                        HidUpload.Value = HidUpload.Value.Replace(ChkSelect.Value & "@", "")
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, pParms(18).Value, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                    Else
                        'lblError.Text = pParms(17).Value.ToString()
                        usrMessageBar2.ShowNotification(pParms(17).Value.ToString(), UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        objConn.Close()
                        grow.BackColor = Drawing.Color.BurlyWood
                        Exit Sub
                    End If
                End If

            Next
        Catch sqex As SqlException
            Errorlog("Error in online fee reconciliation, Error:" & sqex.Message, "PHOENIX")
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar2.ShowNotification(sqex.Message, UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            objConn.Close()
        Catch ex As Exception
            Errorlog("Error in online fee reconciliation, Error:" & ex.Message, "PHOENIX")
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar2.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            objConn.Close()
        End Try
        stTrans.Commit()
        If objConn.State = ConnectionState.Open Then
            objConn.Close()
        End If
        getDetails(HidUpload.Value)
    End Sub

    Protected Sub gvExcel_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            TAmnt += Convert.ToDouble(e.Row.Cells(5).Text)
        End If
    End Sub

    Protected Sub rbnmpgs_CheckedChanged(sender As Object, e As EventArgs)
        doClear()
    End Sub
    Protected Sub rbnmigs_CheckedChanged(sender As Object, e As EventArgs)
        doClear()
    End Sub
    Protected Sub rbnindus_CheckedChanged(sender As Object, e As EventArgs)
        doClear()
    End Sub
End Class

