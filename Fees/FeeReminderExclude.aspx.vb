Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb

Partial Class Fees_FEEReminderExcludeInclude
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property VSgvFEEReminder() As DataTable
        Get
            Return ViewState("gvFEEReminder")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvFEEReminder") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnImport)
        If Page.IsPostBack = False Then
            gvFEEReminder.Attributes.Add("bordercolor", "#1b80b6")
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'ClientScript.RegisterStartupScript(Me.GetType(), _
                '"script", "<script language='javascript'>  CheckOnPostback(); </script>")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_REMINDEREXCLUDE Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                ddlAcademicYear.Items.Clear()
                ddlAcademicYear.DataSource = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
                ddlAcademicYear.DataTextField = "ACY_DESCR"
                ddlAcademicYear.DataValueField = "ACD_ID"
                ddlAcademicYear.DataBind()
                ddlAcademicYear.SelectedIndex = -1
                ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                Session("liUserList") = New ArrayList
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                If ViewState("datamode") = "view" Then
                    Exit Sub
                End If
                If rblListing.SelectedValue = "XL" Or rblListing.SelectedValue = "AM" Then
                    Me.gvFEEReminder.Columns(0).Visible = False
                Else
                    Me.gvFEEReminder.Columns(0).Visible = True
                End If
                rblListing_SelectedIndexChanged(sender, e)
                lnkFormatFile.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDOWNLOAD") & "&Title=" & Encr_decrData.Encrypt("EXCLUDE LIST") & "&Path=" & Encr_decrData.Encrypt(Web.Configuration.WebConfigurationManager.AppSettings("StudentPhotoPath").ToString() & "/Template/StudentXcludeList.xls")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub
    Protected Sub rblListing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblListing.SelectedIndexChanged
        Me.gvFEEReminder.DataSource = Nothing
        Me.gvFEEReminder.DataBind()
        Me.lblMessage.Text = ""
        Me.lblError2.innerHTML = ""
        If Me.rblListing.SelectedValue = "LA" Then
            GridBind()
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEReminder.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvFEEReminder.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim ds As New DataSet
            Dim Dv As DataView
            Dim str_Filter, lstrOpr, str_Sql, lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6 As String
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            str_Filter = ""
            If gvFEEReminder.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtStuNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuNo")
                lstrCondn1 = Trim(txtSearch.Text.Trim)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn1)

                '   -- 1  txtStuname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuname")
                lstrCondn2 = txtSearch.Text.Trim
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STUNAME", lstrCondn2)

                '   -- 2  txtGrade
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtGrade")
                lstrCondn3 = txtSearch.Text.Trim
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRM_DESCR", lstrCondn3)

                '   -- 5  Section
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEReminder.HeaderRow.FindControl("txtSection")
                lstrCondn5 = txtSearch.Text.Trim
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "SCT_DESCR", lstrCondn5)

            End If
            ''''FCH_ID,FCH_STU_ID, FCH_DT,FCH_REMARKS,PERIOD,STU_NAME,STU_NO,FCM_DESCR, AMOUNT
            Dim str_cond As String = String.Empty
            Dim FromDate As Date = CDate(txtFromDate.Text)
            Dim ToDate As Date = CDate(txtToDate.Text)
            
            str_Sql = "exec FEES.GetDataForFeeRemiderExclude '" & Session("sBsuid") & "'"
            Dv = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql).Tables(0).DefaultView
            Dv.RowFilter = "1=1" & str_Filter
            gvFEEReminder.DataSource = Dv
            gvFEEReminder.DataBind()
            
            txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvFEEReminder.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn2

            txtSearch = gvFEEReminder.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn3

            txtSearch = gvFEEReminder.HeaderRow.FindControl("txtSection")
            txtSearch.Text = lstrCondn4

            'txtSearch = gvFEEReminder.HeaderRow.FindControl("txtPeriod")
            'txtSearch.Text = lstrCondn5

            'txtSearch = gvFEEReminder.HeaderRow.FindControl("txtRemarks")
            'txtSearch.Text = lstrCondn6
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
   Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()

        sTemp.Append("<table >") ' border=1 bordercolor=#1b80b6 bgcolor=#ffcccc cellpadding=1 cellspacing=0>")
        sTemp.Append("<tr>")
        sTemp.Append("<td colspan='4'><b>The FEE Advance Payment Details</b></td>")
        sTemp.Append("</tr>")
        sTemp.Append("<tr>")
        sTemp.Append("<td><b>FEE DESCR</b></td>")
        sTemp.Append("<td><b>CHG.AMT</b></td>")
        sTemp.Append("<td><b>PAID AMT</b></td>")
        sTemp.Append("<td><b>ADV. AMT</b></td>")
        sTemp.Append("</tr>")

        Dim STUD_ID As Integer = contextKey
        Dim AsOnDate As Date = HttpContext.Current.Session("sAsOnDate")
        Dim BSU_ID As String = HttpContext.Current.Session("sBSUID")

        Dim conn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Try
            conn.Open()
            Dim drread As SqlDataReader = FEEReminder.GetAdvancedAmount(STUD_ID, AsOnDate, BSU_ID, conn)
            While (drread.Read())
                sTemp.Append("<tr>")
                sTemp.Append("<td>" & drread("FEE_DESCR").ToString() & "</td>")
                sTemp.Append("<td>" & Format(drread("CHARGE_AMOUNT"), "0.00") & "</td>")
                sTemp.Append("<td>" & Format(drread("PAID_AMT"), "0.00") & "</td>")
                sTemp.Append("<td>" & Format(drread("ADV_AMT"), "0.00") & "</td>")
                sTemp.Append("</tr>")
            End While
            drread.Close()
        Catch ex As Exception
        Finally
            sTemp.Append("</table>")
            conn.Close()
        End Try

        Return sTemp.ToString()
    End Function

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
   Public Shared Function GetAmountDue(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()

        sTemp.Append("<table >") ' border=1 bordercolor=#1b80b6 bgcolor=#ffcccc cellpadding=1 cellspacing=0>")
        sTemp.Append("<tr>")
        sTemp.Append("<td colspan='6'><b>The FEE Due Details</b></td>")
        sTemp.Append("</tr>")
        sTemp.Append("<tr>")
        sTemp.Append("<td><b>ACAD.Yr</b></td>")
        sTemp.Append("<td><b>GRADE</b></td>")
        sTemp.Append("<td><b>FEE DESCR</b></td>")
        sTemp.Append("<td><b>CHG.AMT</b></td>")
        sTemp.Append("<td><b>PAID AMT</b></td>")
        sTemp.Append("<td><b>DUE. AMT</b></td>")
        sTemp.Append("</tr>")

        Dim STUD_ID As Integer = contextKey
        Dim AsOnDate As Date = HttpContext.Current.Session("sAsOnDate")
        Dim BSU_ID As String = HttpContext.Current.Session("sBSUID")
        Dim Level As String = HttpContext.Current.Session("sLevel")
 
        Dim conn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)

        Try
            Dim drread As SqlDataReader = FEEReminder.GetDueAmount(STUD_ID, AsOnDate, BSU_ID, Level)
            While (drread.Read())
                sTemp.Append("<tr>")
                sTemp.Append("<td>" & drread("ACY_DESCR").ToString() & "</td>")
                sTemp.Append("<td>" & drread("GRM_DISPLAY").ToString() & "</td>")
                sTemp.Append("<td>" & drread("FEE_DESCR").ToString() & "</td>")
                sTemp.Append("<td>" & Format(drread("CHARGE_AMT"), "0.00") & "</td>")
                sTemp.Append("<td>" & Format(drread("PAID_AMT"), "0.00") & "</td>")
                sTemp.Append("<td>" & Format(drread("DUE_AMT"), "0.00") & "</td>")
                sTemp.Append("</tr>")
            End While
            drread.Close()
        Catch ex As Exception
        Finally
            sTemp.Append("</table>")
        End Try
        Return sTemp.ToString()
    End Function

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEReminder.PageIndexChanging
        gvFEEReminder.PageIndex = e.NewPageIndex
        SetChk(Page)
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFEEReminder.RowDataBound
        Try
            'Dim lblSTU_ID As New Label
            'lblSTU_ID = TryCast(e.Row.FindControl("lblSTU_ID"), Label)
            Dim chkSel As HtmlInputCheckBox
            chkSel = TryCast(e.Row.FindControl("chkControl"), HtmlInputCheckBox)
            chkSel.Checked = list_exist(chkSel.Value)

            '    Dim lblFCH_ID As New Label
            '    lblFCH_ID = TryCast(e.Row.FindControl("lblFCH_ID"), Label)
            '    Dim hlEdit As New HyperLink
            '    hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            '    If hlEdit IsNot Nothing And lblFCH_ID IsNot Nothing Then
            '        ViewState("datamode") = Encr_decrData.Encrypt("view")
            '        hlEdit.NavigateUrl = "FeeConcessionTrans.aspx?FCH_ID=" & Encr_decrData.Encrypt(lblFCH_ID.Text) & _
            '        "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            '    End If
            If e.Row.RowType = DataControlRowType.Footer Then
                Dim lblCount As Label = DirectCast(e.Row.FindControl("lblCount"), Label)
                lblCount.Text = VSgvFEEReminder.Rows.Count
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar2.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        SetChk(Page)
        'If (Session("liUserList") Is Nothing) OrElse (Session("liUserList").Count <= 0) Then
        '    'lblError.Text = "Please Select atleast 1 Student..."
        '    'Exit Sub
        'End If
        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim trans As SqlTransaction
        Dim stud_ids As Hashtable = GetStudentList()
        trans = conn.BeginTransaction("FEE_REMINDER")
        Dim dtFromdate As DateTime = CDate(txtFromDate.Text)
        Dim dtToDate As DateTime = CDate(txtToDate.Text)
        Dim vFEE_REM As New FEEReminder
        vFEE_REM.FRH_ACD_ID = ddlAcademicYear.SelectedValue
        vFEE_REM.FRH_ASONDATE = dtFromdate
        vFEE_REM.FRH_DT = dtToDate
        'If radFirstRM.Checked Then
        '    vFEE_REM.FRH_Level = ReminderType.FIRST
        'ElseIf radSecRM.Checked Then
        '    vFEE_REM.FRH_Level = ReminderType.SECOND
        'ElseIf radThirdRM.Checked Then
        '    vFEE_REM.FRH_Level = ReminderType.THIRD
        'End If
        vFEE_REM.FRH_BSU_ID = Session("sBSUID")
        'Try
        '    Dim vFRH_ID As Integer
        '    Dim retVal As Integer = FEEReminder.SaveFEEReminderDetails(stud_ids, vFEE_REM, vFRH_ID, conn, trans)
        '    If retVal > 0 Then
        '        trans.Rollback()
        '        lblError.Text = UtilityObj.getErrorMessage(retVal)
        '    Else
        '        trans.Commit()
        '        lblError.Text = "Data updated Successfully"
        '        ViewState("datamode") = "none"
        '        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        '        Dim str_KEY As String = "INSERT"
        '        If ViewState("datamode") <> "edit" Then
        '            str_KEY = "EDIT"
        '        End If
        '        Dim flagAudit As Integer = UtilityObj.operOnAudiTable("", vFRH_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page)
        '        If flagAudit <> 0 Then
        '            Throw New ArgumentException("Could not process your request")
        '        End If
        '        If chkPrint.Checked Then
        '            PrintReminder(vFRH_ID)
        '        End If
        '    End If
        'Finally
        '    conn.Close()
        'End Try
        If (Session("liUserList") Is Nothing) OrElse (Session("liUserList").Count <= 0) Then
            'lblError.Text = "Please Select atleast 1 Student..."
            'Exit Sub
        End If
        
        Try
            Dim vFRH_ID As Integer
            'Dim retVal As Integer = FEEReminderExclude.SaveFEEReminderExclude(stud_ids, vFEE_REMX, conn, trans)
            Dim cmd As SqlCommand
            cmd = New SqlCommand("[FEES].[F_SAVEFEE_REMINDEREXCLUDE]", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim str_xml As String = FEEReminder.GetStudentDetailsXML(stud_ids)

            Dim sqlpFRE_STU_ID As New SqlParameter("@FRE_STU_ID", SqlDbType.Xml)
            sqlpFRE_STU_ID.Value = str_xml
            cmd.Parameters.Add(sqlpFRE_STU_ID)

            Dim sqlpFRE_BSU_ID As New SqlParameter("@FRE_BSU_ID", SqlDbType.VarChar, 20)
            sqlpFRE_BSU_ID.Value = Session("sBSUID")
            cmd.Parameters.Add(sqlpFRE_BSU_ID)

            Dim sqlpFRE_STU_ACD_ID As New SqlParameter("@FRE_STU_ACD_ID", SqlDbType.Int)
            sqlpFRE_STU_ACD_ID.Value = ddlAcademicYear.SelectedValue
            cmd.Parameters.Add(sqlpFRE_STU_ACD_ID)

            Dim sqlpFRE_FROMDT As New SqlParameter("@FRE_FROMDT", SqlDbType.DateTime)
            sqlpFRE_FROMDT.Value = dtFromdate
            cmd.Parameters.Add(sqlpFRE_FROMDT)

            Dim sqlpFRE_TODT As New SqlParameter("@FRE_TODT", SqlDbType.DateTime)
            sqlpFRE_TODT.Value = dtToDate
            cmd.Parameters.Add(sqlpFRE_TODT)

            Dim sqlpFRE_bDELETE As New SqlParameter("@FRE_bDELETE", SqlDbType.TinyInt)
            sqlpFRE_bDELETE.Value = 0
            cmd.Parameters.Add(sqlpFRE_bDELETE)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            Dim retVal As Integer
            retVal = retSValParam.Value
            If retVal > 0 Then
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(retVal)
                usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                trans.Commit()
                'lblError.Text = "Data updated Successfully"
                usrMessageBar2.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim str_KEY As String = "INSERT"
                If ViewState("datamode") <> "edit" Then
                    str_KEY = "EDIT"
                End If
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable("", vFRH_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                'If chkPrint.Checked Then
                '    PrintReminder(vFRH_ID)
                'End If
            End If
            GridBind()
            Response.Redirect("FeeReminderExcludeView.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            conn.Close()
        End Try
    End Sub

    Private Function GetStudentList() As Hashtable
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_cond As String = String.Empty
        Dim FromDate As Date = CDate(txtFromDate.Text)
        Dim httab As New Hashtable
        'While (dr.Read())
        '    httab(dr("FSH_STU_ID")) = False
        'End While
        If Me.rblListing.SelectedValue = "XL" Or Me.rblListing.SelectedValue = "AM" Then
            For Each dr As DataRow In VSgvFEEReminder.Rows
                httab(dr("STU_ID")) = False
            Next
        Else
            If (Session("liUserList") IsNot Nothing) AndAlso (Session("liUserList").Count > 0) Then
                Dim bSelExclude As Boolean = RadIncludeSelected.Checked
                For Each STU_ID As Integer In Session("liUserList")
                    httab(STU_ID) = bSelExclude
                Next
            End If
        End If

        Return httab
    End Function

    Protected Sub txtToDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtToDate.TextChanged
        GridBind()
    End Sub 

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Private Sub ClearDetails()
        txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        RadExcludeSelected.Checked = True
        'radThirdRM.Checked = True
        Me.lblMessage.Text = ""
        Me.txtStudNo.Text = ""
        h_STUD_ID.Value = ""
        'lblError.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        ClearDetails()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub txtAmountLimit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub lnkAmtDue_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    '-----------Import functions---------------------
    Protected Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim conn As New OleDbConnection
        'Me.lblError.Text = ""
        Try
            Dim str_conn As String = ""
            Dim cmd As New OleDbCommand
            Dim da As New OleDbDataAdapter
            Dim dtXcel As DataTable
            Dim FileName As String = ""
            If (FileUpload1.HasFile) Then
                Dim strFileType As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToString().ToLower()
                FileName = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload1.FileName.ToString()
                If strFileType = ".xls" Or strFileType = ".xlsx" Then
                    If File.Exists(FileName) Then
                        File.Delete(FileName)
                    End If
                    Me.FileUpload1.SaveAs(FileName)
                    str_conn = IIf(strFileType = ".xls", "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName + ";Extended Properties=""Excel 8.0;HDR=YES;""" _
                                    , "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileName & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2""")
                Else
                    'Me.lblError.Text = "Only Excel files are allowed"
                    usrMessageBar2.ShowNotification("Only Excel files are allowed", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Try
                End If

                Dim Query = "SELECT * FROM [Sheet1$]"
                conn = New OleDbConnection(str_conn)
                'If conn.State = ConnectionState.Closed Then conn.Open()
                'cmd = New OleDbCommand(Query, conn)
                'da = New OleDbDataAdapter(cmd)
                'ds = New DataSet()
                'da.Fill(ds)
                'If conn.State = ConnectionState.Open Then
                '    conn.Close()
                'End If
                'dtXcel = ds.Tables(0)
                dtXcel = Mainclass.FetchFromExcelIntoDataTable(FileName, 1, 1, 4)
                If dtXcel.Rows.Count > 0 Then
                    'VSgvFEEReminder = dtXcel
                    BindExcel(dtXcel)
                Else
                    'Me.lblError.Text += "Empty Document!!"
                    usrMessageBar2.ShowNotification("Empty Document!!", UserControls_usrMessageBar.WarningType.Danger)
                End If
            Else
                'Me.lblError.Text += "File not Found!!"
                usrMessageBar2.ShowNotification("File not Found!!", UserControls_usrMessageBar.WarningType.Danger)
                Me.FileUpload1.Focus()
            End If

            File.Delete(FileName) 'delete the file after copying the contents
        Catch
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            'Me.lblError.Text += Err.Description
            usrMessageBar2.ShowNotification(Err.Description, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Sub BindExcel(ByVal dt As DataTable)
        Dim str_error As String = ""
        dt.Columns.Add("STU_ID", System.Type.GetType("System.Int32"))
        dt.Columns.Add("STU_NO", System.Type.GetType("System.String"))
        dt.Columns.Add("STUNAME", System.Type.GetType("System.String"))
        dt.Columns.Add("GRM_DESCR", System.Type.GetType("System.String"))
        dt.Columns.Add("SCT_DESCR", System.Type.GetType("System.String"))
        dt.AcceptChanges()
        Dim fromdate As DateTime = Convert.ToDateTime(dt.Rows(0)("FROMDATE")).ToShortDateString
        Dim todate As DateTime = Convert.ToDateTime(dt.Rows(0)("TODATE")).ToShortDateString

        Me.txtFromDate.Text = Format(fromdate, OASISConstants.DateFormat)
        Me.txtToDate.Text = Format(todate, OASISConstants.DateFormat)

        Dim i As Int32 = 1
        Dim j As Integer = dt.Rows.Count - 1
        For j = dt.Rows.Count - 1 To 0 Step -1
            Dim dr As DataRow = dt.Rows(j)
            Dim qry As String = "SELECT STU_ID,STU_NO,STU_NAME AS STUNAME,GRM_DESCR,SCT_DESCR FROM OASIS_FEES.dbo.VW_OSO_STUDENT_M WHERE STU_NO='" & dr("STUDENTNO") & "' AND STU_BSU_ID='" & Session("sBsuid") & "' "
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry)
            If ds.Tables(0).Rows.Count > 0 Then
                dt.Rows(j)("STU_ID") = ds.Tables(0).Rows(0)("STU_ID")
                dt.Rows(j)("STU_NO") = ds.Tables(0).Rows(0)("STU_NO")
                dt.Rows(j)("STUNAME") = ds.Tables(0).Rows(0)("STUNAME")
                dt.Rows(j)("GRM_DESCR") = ds.Tables(0).Rows(0)("GRM_DESCR")
                dt.Rows(j)("SCT_DESCR") = ds.Tables(0).Rows(0)("SCT_DESCR")
            Else
                If i >= 6 Then
                    str_error = str_error & " <br /> "
                    i = 1
                End If
                str_error = str_error & IIf(str_error = "", "", ",") & dr("STUDENTNO")
                i += 1
                dt.Rows.RemoveAt(j)
                dt.AcceptChanges()
            End If
        Next

        VSgvFEEReminder = dt
        Me.gvFEEReminder.DataSource = dt
        Me.gvFEEReminder.DataBind()
        If str_error <> "" Then
            Me.lblError2.innerHTML = " Student No(s) " & str_error & " cannot be imported"
        End If
        Me.lblMessage.Text = "No of Students : " & dt.Rows.Count
    End Sub

    Protected Sub txtStudNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStudNo.TextChanged
        AddStudent()
    End Sub
    Protected Sub lblAddNewStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddNewStudent.Click
        AddStudent()
    End Sub
    Protected Sub imgStudentN_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudentN.Click
        FillSTUNames(h_STUD_ID.Value)
    End Sub
    Protected Sub h_STUD_ID_ValueChanged(sender As Object, e As EventArgs) Handles h_STUD_ID.ValueChanged
        FillSTUNames(h_STUD_ID.Value)
    End Sub
    Private Sub AddStudent()
        If txtStudNo.Text <> "" Then
            Dim str_stuid As String = FeeCommon.GetTransportStudentID(txtStudNo.Text, Session("sBsuid"), False, False)
            If str_stuid <> "" Then
                If h_STUD_ID.Value.EndsWith("||") Then
                    h_STUD_ID.Value = h_STUD_ID.Value & str_stuid
                Else
                    h_STUD_ID.Value = h_STUD_ID.Value & "||" & str_stuid
                End If
                txtStudNo.Text = ""
                FillSTUNames(h_STUD_ID.Value)
                h_STUD_ID.Value = ""
            End If
        End If
    End Sub
    Private Function FillSTUNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_Sql As String
        Dim DTSelectedStudents As DataTable
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        Dim TableName As String = ""
        TableName = "FEES.VW_OSO_STUDENT_DETAILS"
        str_Sql = " SELECT STU_ID,STU_NO,STU_NAME AS STUNAME,GRM_DESCR,SCT_DESCR FROM " & TableName & " WHERE STU_ID IN (" + condition + ")"
        DTSelectedStudents = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql).Tables(0)
        If gvFEEReminder.Rows.Count > 0 And DTSelectedStudents.Rows.Count > 0 Then
            Dim dtExisting As DataTable = VSgvFEEReminder
            dtExisting.Merge(RemoveDuplicateRows(DTSelectedStudents))
            VSgvFEEReminder = dtExisting
        Else
            VSgvFEEReminder = DTSelectedStudents
        End If

        BindSelectedStudents(VSgvFEEReminder)
        If DTSelectedStudents Is Nothing Or DTSelectedStudents.Rows.Count <= 0 Then
            Return False
        Else
            h_STUD_ID.Value = ""
        End If
        Return True
    End Function
    Private Sub BindSelectedStudents(ByVal DTT As DataTable)
        gvFEEReminder.DataSource = DTT
        gvFEEReminder.DataBind()
    End Sub
    Private Function RemoveDuplicateRows(ByVal DT2 As DataTable) As DataTable
        For Each gvr As GridViewRow In Me.gvFEEReminder.Rows
            Dim StudentID As String = DirectCast(gvr.FindControl("lblSTU_ID"), Label).Text
            For j As Int16 = DT2.Rows.Count - 1 To 0 Step -1
                Dim dr As DataRow = DT2.Rows(j)
                If dr("STU_ID").ToString = StudentID Then
                    DT2.Rows.Remove(dr)
                    j -= 1
                End If
            Next
        Next

        Return DT2
    End Function

End Class
