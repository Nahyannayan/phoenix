Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeSetupforAcademicView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "F300125" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademic.DataBind()
                    ddlAcademic.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                    hlAddnew.NavigateUrl = "feeSetupforAcademic.aspx" & "?MainMnu_code=" _
                    & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add") 
                    gridbind()
                    FillNewAcadamicYear()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim ds As New DataSet
            Dim lstrCondn1, lstrCondn2, lstrCondn4, lstrCondn5, str_Sql, str_Filter As String
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   from dt 
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtFDate")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FSP_FROMDT", lstrCondn1)
                '   -- 1  to dt
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtTDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STM_DESCR", lstrCondn2)
                '   -- 3   age
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAgegroup")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRD_DISPLAY", lstrCondn4)
                '   -- 4   type
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtFeeTypes")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FEE_TYPES", lstrCondn5)
            End If
            str_Sql = " SELECT FSP_FROMDT, db.GRD_DISPLAY ,db.STM_DESCR ,db.GRD_ID ,db.GRD_DISPLAYORDER ,db.FSP_STM_ID  ,STUFF(" _
                & "   (   SELECT ',' + RTRIM(LTRIM(FEE_DESCR))" _
                & " FROM FEES.FEESETUP_S A INNER JOIN FEES.FEES_M ON FEE_ID = A.FSP_FEE_ID" _
                & " WHERE DB.FSP_BSU_ID = A.FSP_BSU_ID And DB.FSP_ACD_ID = A.FSP_ACD_ID And DB.FSP_STM_ID = A.FSP_STM_ID And DB.GRD_ID = A.FSP_GRD_ID" _
                & " FOR XML PATH('')), 1, 1,'') FEE_TYPES FROM" _
                & "  (   SELECT   FSP.FSP_ACD_ID ,FSP.FSP_BSU_ID ,  MIN(FSP.FSP_FROMDT) FSP_FROMDT,  ISNULL(GRM.GRM_DISPLAY, GRD.GRD_DISPLAY) GRD_DISPLAY, STM.STM_DESCR, GRD.GRD_ID, GRD.GRD_DISPLAYORDER, FSP.FSP_STM_ID" _
                & "    FROM FEES.FEESETUP_S AS FSP" _
                & "  INNER JOIN OASIS..GRADE_M AS GRD ON FSP.FSP_GRD_ID = GRD.GRD_ID" _
                & "  INNER JOIN OASIS..STREAM_M AS STM ON FSP.FSP_STM_ID = STM.STM_ID" _
                & "  INNER JOIN FEES.FEES_M ON FEE_ID = FSP.FSP_FEE_ID" _
                & "   LEFT OUTER JOIN GRADE_BSU_M GRM ON GRM_ACD_ID = FSP_ACD_ID AND GRM_GRD_ID = FSP_GRD_ID AND GRM_STM_ID = FSP_STM_ID" _
                & " WHERE (FSP.FSP_BSU_ID = '" & Session("sbsuid") & "') AND (FSP.FSP_ACD_ID = '" & ddlAcademic.SelectedItem.Value & "')" _
                & " GROUP BY  GRM.GRM_DISPLAY, GRD.GRD_DISPLAY, STM.STM_DESCR, GRD.GRD_ID, GRD.GRD_DISPLAYORDER, FSP.FSP_STM_ID,FSP.FSP_ACD_ID ,FSP.FSP_BSU_ID ) db" _
                & " WHERE 1 = 1 " & str_Filter & " ORDER BY GRD_DISPLAYORDER"
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            txtSearch = gvJournal.HeaderRow.FindControl("txtFDate")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtAgegroup")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtFeeTypes")
            txtSearch.Text = lstrCondn5

            txtSearch = gvJournal.HeaderRow.FindControl("txtTDate")
            txtSearch.Text = lstrCondn2
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblGRD_ID As New Label
            lblGRD_ID = TryCast(e.Row.FindControl("lblGRD_ID"), Label)

            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)

            Dim lbStrId As New Label
            lbStrId = TryCast(e.Row.FindControl("lbStrId"), Label)

            If hlEdit IsNot Nothing And lblGRD_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "feeSetupforAcademic.aspx?viewid=" & Encr_decrData.Encrypt(lblGRD_ID.Text) & _
                "&MainMnu_code=" & Request.QueryString("MainMnu_code") & _
                "&datamode=" & ViewState("datamode") & _
                "&acdid=" & Encr_decrData.Encrypt(ddlAcademic.SelectedItem.Value) & _
                "&StrId=" & Encr_decrData.Encrypt(lbStrId.Text)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub ddAcademic_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademic.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub ChkCopy_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlAcdYearCopy.Items.Count = 0 Then
            'lblError.Text = "Please Set New Acadamic Year..!"
            usrMessageBar.ShowNotification("Please Set New Acadamic Year..!", UserControls_usrMessageBar.WarningType.Warning)
            Exit Sub
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "2000"
            Dim lblGRD_ID As New Label
            Dim lbStrId As New Label

            Dim ChkSelect As New CheckBox
            For Each gvr As GridViewRow In gvJournal.Rows
                ChkSelect = CType(gvr.FindControl("ChkSelect"), CheckBox)
                lblGRD_ID = TryCast(gvr.FindControl("lblGRD_ID"), Label)
                lbStrId = TryCast(gvr.FindControl("lbStrId"), Label)

                If ChkSelect IsNot Nothing Then
                    If ChkSelect.Checked Then
                        retval = Copy_FEESETUP(stTrans, lblGRD_ID.Text, lbStrId.Text)
                        If retval <> "0" Then
                            stTrans.Rollback()
                            'lblError.Text = getErrorMessage(retval) & " Grade : " & lblGRD_ID.Text.ToString()
                            usrMessageBar.ShowNotification(getErrorMessage(retval) & " Grade : " & lblGRD_ID.Text.ToString(), UserControls_usrMessageBar.WarningType.Danger)

                            Exit Sub
                        Else
                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ddlAcdYearCopy.SelectedItem.Value, "Insert", Page.User.Identity.Name.ToString, Me.Page, " FeeSetup Copied")
                            ChkSelect.Checked = False
                        End If
                    End If
                End If
            Next
            If retval = "2000" Then
                'lblError.Text = "Please Select Item For Copy..!"
                usrMessageBar.ShowNotification("Please Select Item For Copy..!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            If retval = "0" Then
                stTrans.Commit()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    
    Public Function Copy_FEESETUP(ByVal p_stTrans As SqlTransaction, ByVal grdID As String, ByVal Stream As String) As String
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FROM_DATE", SqlDbType.VarChar)
        pParms(0).Value = FeeCommon.GetACDStartDate(ddlAcdYearCopy.SelectedItem.Value)
        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(1).Value = ddlAcademic.SelectedItem.Value
        pParms(2) = New SqlClient.SqlParameter("@NEWACD_ID", SqlDbType.Int)
        pParms(2).Value = ddlAcdYearCopy.SelectedItem.Value
        pParms(3) = New SqlClient.SqlParameter("@FSP_GRD_ID", SqlDbType.VarChar)
        pParms(3).Value = grdID
        pParms(4) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
        pParms(4).Value = Session("sBsuid")
        pParms(5) = New SqlClient.SqlParameter("@FSP_STM_ID", SqlDbType.VarChar)
        pParms(5).Value = Stream
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "[FEES].[Copy_FEESETUP]", pParms)
        Return pParms(6).Value
    End Function

    Sub FillNewAcadamicYear()
        ddlAcdYearCopy.DataSource = FeeCommon.GetAcademicYear(Session("sBsuid"), "NEW")
        ddlAcdYearCopy.DataTextField = "ACY_DESCR"
        ddlAcdYearCopy.DataValueField = "ACD_ID"
        ddlAcdYearCopy.DataBind()
    End Sub

    Protected Sub ChkCopy_CheckedChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        If ChkCopy.Checked Then
            TrNew.Visible = True
        Else
            TrNew.Visible = False
        End If
    End Sub

End Class
