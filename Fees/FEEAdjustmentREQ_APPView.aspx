<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FEEAdjustmentREQ_APPView.aspx.vb" Inherits="FEEAdjustmentREQ_APPView" Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
        function divIMG(pId, val, ctrl1, pImg) {
            var path;

            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }

            if (pId == 1) {
                document.getElementById("<%=getid("mnu_1_img") %>").src = path;
            }
            else if (pId == 2) {
                document.getElementById("<%=getid("mnu_2_img") %>").src = path;
            }
            else if (pId == 3) {
                document.getElementById("<%=getid("mnu_3_img") %>").src = path;
                }
                else if (pId == 4) {
                    document.getElementById("<%=getid("mnu_4_img") %>").src = path;
               }
               else if (pId == 5) {
                   document.getElementById("<%=getid("mnu_5_img") %>").src = path;
               }
               else if (pId == 6) {
                   document.getElementById("<%=getid("mnu_6_img") %>").src = path;
             }
             else if (pId == 7) {
                 document.getElementById("<%=getid("mnu_7_img") %>").src = path;
               }

    document.getElementById(ctrl1).value = val + '__' + path;
}
    </script>

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fee Adjustments..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr >
                        <td  align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                           <%-- <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                             <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                        <td align="right">
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="50%">
                            <asp:RadioButton ID="radStud" runat="server" AutoPostBack="True" Checked="True" Text="Student" GroupName="STU_TYPE" />
                            <asp:RadioButton ID="radEnq" runat="server" Text="Enquiry" AutoPostBack="True" GroupName="STU_TYPE" /></td>
                        <td align="right" width="50%">
                            <asp:RadioButton ID="radRequested" runat="server" AutoPostBack="True" Checked="True" Text="Requested" GroupName="STU_APROVE" CausesValidation="True" />
                            <asp:RadioButton ID="radApproved" runat="server" AutoPostBack="True" Text="Approved" GroupName="STU_APROVE" />
                            <asp:RadioButton ID="radReject" runat="server" Text="Reject" AutoPostBack="True" GroupName="STU_APROVE" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:GridView ID="gvFEEAdjustments" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="FCH_ID" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblFAH_ID" runat="server" Text='<%# Bind("FAH_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Student ID" >
                                        <HeaderTemplate>
                                            Student ID<br />
                                                            <asp:TextBox ID="txtstudno" runat="server" Width="80%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnStudnoSearch" OnClick="ImageButton1_Click" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Name" >
                                        <HeaderTemplate>
                                            Name<br />
                                                            <asp:TextBox ID="txtstudname" runat="server" Width="80%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnStudnameSearch" OnClick="ImageButton1_Click" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>





                                    <asp:TemplateField HeaderText="Date" >
                                        <HeaderTemplate>
                                           Date<br />
                                                            <asp:TextBox ID="txtDate" runat="server" Width="80%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"></asp:ImageButton>
                                                           
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("FAH_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Doc No" >
                                        <HeaderTemplate>
                                           Doc No<br />
                                                            <asp:TextBox ID="txtAcademicYear" runat="server"  Width="80%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnAcademicYearSearch" OnClick="ImageButton1_Click" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"></asp:ImageButton>
                                                       
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblACD_YEAR" runat="server" Text='<%# Bind("FAR_DOCNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" >
                                        <HeaderTemplate>
                                           Remarks<br />
                                                            <asp:TextBox ID="txtRemarks" runat="server" Width="80%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnRemarksSearch" OnClick="ImageButton1_Click" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"></asp:ImageButton>
                                                      
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("FAH_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ARM_DESCR" >
                                        <HeaderTemplate>
                                           Reason<br />
                                                            <asp:TextBox ID="txtReason" runat="server" Width="80%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnReasonSearch" OnClick="ImageButton1_Click" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"></asp:ImageButton>
                                                     
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblReason" runat="server" Text='<%# Bind("ARM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View" >
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

                <p></p>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        cssdropdown.startchrome("chromemenu1");
        cssdropdown.startchrome("chromemenu2");
        cssdropdown.startchrome("chromemenu3");
        cssdropdown.startchrome("chromemenu4");
        cssdropdown.startchrome("chromemenu5");
    </script>
</asp:Content>
