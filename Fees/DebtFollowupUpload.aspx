﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="DebtFollowupUpload.aspx.vb" Inherits="Fees_DebtFollowupUpload" %>

<%--<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>--%>
<%--<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>--%>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>--%>


<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpageNew" runat="Server">
    <%----%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">
    <%--    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></ajaxToolkit:ToolkitScriptManager>--%>
    <script language="javascript" type="text/javascript">

        function CommitButton() {

            var intervalHandle = setInterval(function () {
                if ($("#<%=h_Processeing.ClientID %>").val() == '1') {

                    var dt = $('#ctl00_cphMasterpage_hdBATCHNO').val();
                    var $ajaxImage = $("#ajaxImage");
                    var $progressbar = $("#progressbar");
                    var $fsProgress = $("#fsProgress");
                    var $statusDiv = $("#statusDiv");
                    var $startProcessButton = $("#<%= btnCommitImprt.ClientID%>");
                    var $PostbackButton = $("#<%= btnPostback.ClientID%>");
                    var $lblError = $("#<%= lblError.ClientID%>");
                    var $h_status = $("#<%= h_STATUS.ClientID%>");
                    var $h_errormessage = $("#<%= h_errormessage.ClientID%>");
                    var $h_IsSuccess = $("#<%= h_IsSuccess.ClientID%>");

                    var $ClearButton = $("#<%= btnClear.ClientID%>");
                    var postData = { BATCH_NO: $('#ctl00_cphMasterpage_hdBATCHNO').val() }

                    var xhr = $.ajax({
                        url: "../GetBulkProcessStatus.asmx/GetStatusPer",
                        data: JSON.stringify(postData),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (response, status, xhr) {
                            if (response.d.ISERROR) {
                                $h_status.val("")
                                $startProcessButton.removeAttr("disabled");
                                $ajaxImage.hide(500);
                                $progressbar.hide(500);
                                $fsProgress.hide(500);
                                clearInterval(intervalHandle);
                                $startProcessButton.removeAttr("disabled");
                                $h_errormessage.val(response.d.MESSAGE);
                                $("#ctl00_cphMasterpage_h_Processeing").val('');
                                $h_IsSuccess.val("0");
                                $statusDiv.hide();
                                $('#error-message').html(response.d.MESSAGE);
                                $('#alert-error-popup').show();
                                xhr.abort();
                            }
                            else {

                                $startProcessButton.attr("disabled", "disabled");
                                if ($fsProgress.is(':visible') == false)
                                    $fsProgress.show();
                                if ($ajaxImage.is(':visible') == false)
                                    $ajaxImage.show();
                                if ($progressbar.is(':visible') == false)
                                    $progressbar.show();

                                $statusDiv.html(response.d.PERCENTAGE + "% " + response.d.WORKDESCRIPTION);
                                $progressbar.progressbar({
                                    value: parseInt(response.d.PERCENTAGE)
                                });


                                if (response.d.ISCOMPLETE) {
                                    clearInterval(intervalHandle);
                                    $startProcessButton.removeAttr("disabled");
                                    $h_errormessage.val(response.d.MESSAGE);
                                    $h_IsSuccess.val("1");
                                    $("#ctl00_cphMasterpage_h_Processeing").val('');
                                    $h_status.val("0")
                                    xhr.abort();
                                    $ajaxImage.hide(500);
                                    $progressbar.hide(500);
                                    $fsProgress.hide(500);
                                    $PostbackButton.click();
                                    $ClearButton.click();
                                }
                                else
                                    $h_status.val("");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $h_status.val("")
                            $startProcessButton.removeAttr("disabled");
                            $ajaxImage.hide(500);
                            $progressbar.hide(500);
                            $fsProgress.hide(500);
                            clearInterval(intervalHandle);
                            $startProcessButton.removeAttr("disabled");
                            $("#ctl00_cphMasterpage_h_Processeing").val('');
                            xhr.abort();
                        },
                    });
                }
            }, 2000);
        }

        // A $(document).ready() block.
        $(document).ready(function () {
            var $ajaxImage = $("#ajaxImage");
            var $progressbar = $("#progressbar");
            var $fsProgress = $("#fsProgress");
            // Hide the AJAX spinner image and the progress bar, by default.
            $ajaxImage.hide(500);
            $progressbar.hide(500);
            $fsProgress.hide(500);
        });


    </script>
    <style type="text/css">
        #alert-error-popup {
            position: fixed;
            top: 100px;
            left: 45%;
            z-index: 1000;
            min-width: 200px;
        }

        .field-rb-label > label {
            font-weight: bold !important;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>

    <div id="alert-error-popup" style="display: none" class="sticky-top alert alert-danger alert-dismissable" role="alert"><i class="fa fa-info"></i><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><span id="error-message">  </span></div>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Debt FollowUp Upload
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="card-body">
                    <div class="table-responsive">
                        <table align="center" width="100%">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False" CssClass="error"></asp:Label>
                                    <uc1:usrMessageBar runat="server" ID="usrMessageBar1" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table align="center" width="100%">
                            <tr>
                                <td align="right" width="25%">
                                    <span class="field-label">Select File</span>
                                </td>
                                <td align="left" width="75%">
                                    <asp:FileUpload ID="FileUpload1" runat="server" Width="255px"></asp:FileUpload>
                                    <asp:Button ID="btnLoadData" runat="server" Text="Load Data" CssClass="button"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table width="100%">
                            <tr>
                                <td align="center">
                                    <fieldset id="fsProgress" style="width: 50%; display: none">
                                        <legend>Processing Status</legend>
                                        <div id="ajaxImage" style="display: inline;">
                                            <img alt="" src="../Images/Misc/AjaxLoading.gif" />
                                        </div>
                                        <div id="statusDiv"></div>
                                        <div id="progressbar" style="height: 20px; width: 100%"></div>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table style="width: 100%;">
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="Grid_StudentDetails" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                                        CssClass="table table-bordered table-row" EmptyDataText="No records"
                                        OnPageIndexChanging="Grid_StudentDetails_PageIndexChanging"
                                        PageSize="10" Style="width: auto; font-size: small !important;">
                                        <RowStyle Wrap="False" />
                                        <EmptyDataRowStyle Wrap="False" />
                                        <Columns>
                                            <asp:BoundField DataField="BSU" HeaderText="Business Unit" HeaderStyle-CssClass="field-label" />
                                            <asp:BoundField DataField="STUNO" HeaderText="Student No" HeaderStyle-CssClass="field-label" />
                                            <asp:BoundField DataField="AMOUNT" HeaderText="Amount" HeaderStyle-CssClass="field-label" />
                                            <asp:BoundField DataField="COMMENTS" HeaderText="Comments" HeaderStyle-CssClass="field-label" />
                                            <asp:BoundField DataField="USER" HeaderText="Followed up user" HeaderStyle-CssClass="field-label" />
                                            <asp:BoundField DataField="ERROR_MSG" HeaderText="Message" HtmlEncode="False" />
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table style="width: 100%;">
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnProcess" runat="server" Text="Commit" CssClass="button" Visible="false" OnClientClick="CommitButton()"></asp:Button>
                                    <asp:Button ID="btnCommitImprt" runat="server" Text="Validate" CssClass="button" Visible="False"></asp:Button>
                                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="button" Visible="false"></asp:Button>
                                    <%--<asp:HiddenField ID="hdrblAdjFor" runat="server" />--%>
                                    <asp:HiddenField ID="hdBATCHNO" runat="server" />
                                    <asp:HiddenField ID="h_IsSuccess" runat="server" />
                                    <asp:HiddenField ID="h_STATUS" runat="server" />
                                    <asp:HiddenField ID="h_errormessage" runat="server" />
                                    <asp:HiddenField ID="h_Processeing" runat="server" />
                                    <asp:Button ID="btnPostback" runat="server" CssClass="button" Style="display: none;" CausesValidation="False" Height="1px" TabIndex="5000" Width="1px" />
                                    <asp:Button ID="btnClear" runat="server" CssClass="button" Style="display: none;" CausesValidation="False" Height="1px" TabIndex="5000" Width="1px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnProcess" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                    <span style="border-width: 0px; position: fixed; padding: 20px; background-color: #FFFFFF; font-size: 30px; left: 40%; top: 40%; border-radius: 50px;">Please wait ...</span>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
</asp:Content>
