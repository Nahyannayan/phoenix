Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FEEConcessionTransViewBB
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            gvFEEConcessionDet.Attributes.Add("bordercolor", "#1b80b6")
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_APPROVAL _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_CANCEL _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_CANCEL_APPROVAL) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION
                        lblHead.Text = "Transport Fee Concessions Transaction..."
                        hlAddNew.NavigateUrl = "FeeConcessionTransBB.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                        rbAll.Visible = True
                        rbPosted.Visible = True
                        rbUnposted.Visible = True
                        rbRejected.Visible = True
                    Case OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_APPROVAL
                        lblHead.Text = "Transport Fee Concessions Approval..."
                        gvFEEConcessionDet.Columns(10).Visible = True
                        btnPost.Visible = True
                        btnReject.Visible = True
                        hlAddNew.Visible = False
                        lblDate.Visible = True
                        txtFrom.Visible = True
                        imgFrom.Visible = True
                        rbAll.Visible = False
                        rbPosted.Visible = False
                        rbUnposted.Visible = False
                        rbRejected.Visible = False
                        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
                    Case OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_CANCEL
                        hlAddNew.NavigateUrl = "FeeConcessionTransCancelBB.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                        lblHead.Text = "Transport Fee Concessions Cancellation..."
                        rbAll.Visible = True
                        rbPosted.Visible = True
                        rbUnposted.Visible = True
                        rbRejected.Visible = True
                    Case OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_CANCEL_APPROVAL
                        lblHead.Text = "Transport Fee Concessions Cancellation Approval ..."
                        gvFEEConcessionDet.Columns(10).Visible = True
                        btnPost.Visible = True
                        btnReject.Visible = True
                        hlAddNew.Visible = False
                        lblDate.Visible = True
                        txtFrom.Visible = True
                        imgFrom.Visible = True
                        btnPost.Text = "Approve"
                        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
                        rbAll.Visible = False
                        rbPosted.Visible = False
                        rbUnposted.Visible = False
                        rbRejected.Visible = False
                End Select
                ddlBusinessunit.DataBind()
                If Not ddlBusinessunit.Items.FindByValue(Session("PROVIDER_BSU_ID")) Is Nothing Then
                    ddlBusinessunit.ClearSelection()
                    ddlBusinessunit.Items.FindByValue(Session("PROVIDER_BSU_ID")).Selected = True
                End If
                FillACD()
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEConcessionDet.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvFEEConcessionDet.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim ds As New DataSet
            Dim str_Filter As String = String.Empty
            Dim lstrOpr As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim lstrCondn6 As String = String.Empty
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            If gvFEEConcessionDet.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtStuNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuNo")
                lstrCondn1 = Trim(txtSearch.Text.Trim)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn1)
                '   -- 1  txtStuname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuname")
                lstrCondn2 = txtSearch.Text.Trim
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn2)
                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtConcession")
                lstrCondn3 = txtSearch.Text.Trim
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCM_DESCR", lstrCondn3)
                '   -- 3   txtTDate
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtFrom")
                lstrCondn4 = txtSearch.Text.Trim
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCH_DT", lstrCondn4)
                '   -- 5  city
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtPeriod")
                lstrCondn5 = txtSearch.Text.Trim
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "PERIOD", lstrCondn5)
                '   -- 5  city
                larrSearchOpr = h_Selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRemarks")
                lstrCondn6 = txtSearch.Text.Trim
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCH_DOCNO", lstrCondn6)
            End If
            ''''FCH_ID,FCH_STU_ID, FCH_DT,FCH_REMARKS,PERIOD,STU_NAME,STU_NO,FCM_DESCR, AMOUNT
            If rbPosted.Checked Then
                str_Filter = str_Filter & " AND  ISNULL(FCH_bPosted,0)=1"
            ElseIf rbUnposted.Checked Then
                str_Filter = str_Filter & " AND  ISNULL(FCH_bPosted,0)=0 AND ISNULL(FCH_REJTDBY,'')=''"
            ElseIf rbRejected.Checked Then
                str_Filter = str_Filter & " AND  ISNULL(FCH_REJTDBY,'')<>''"
            End If
            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION
                    str_Sql = "SELECT * FROM FEES.vw_OSO_FEES_GETFEECONCESSIONTRANS" & _
                              " WHERE FCH_DRCR='CR' AND FCH_STU_BSU_ID='" & ddlBusinessunit.SelectedItem.Value & "' " & _
                              " AND FCH_BSU_ID='" & Session("sBsuid") & "' " & _
                              " AND FCH_ID NOT IN( SELECT ISNULL(FCH_FCH_ID,0)  FROM  FEES.FEE_CONCESSION_H) " & _
                              " AND FCH_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' " & str_Filter
                Case OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_APPROVAL
                    str_Sql = "SELECT * FROM FEES.vw_OSO_FEES_GETFEECONCESSIONTRANS" & _
                              " WHERE FCH_DRCR='CR' AND ISNULL(FCH_bPosted,0)=0  " & _
                              " AND FCH_ID NOT IN( SELECT   ISNULL(FCH_FCH_ID,0)  FROM  FEES.FEE_CONCESSION_H) AND FCH_STU_BSU_ID='" & ddlBusinessunit.SelectedItem.Value & "'" & _
                              " AND FCH_BSU_ID='" & Session("sBsuid") & "' " & _
                              " AND FCH_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' " & str_Filter
                Case OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_CANCEL
                    str_Sql = "SELECT * FROM FEES.vw_OSO_FEES_GETFEECONCESSIONTRANS" _
                              & " WHERE FCH_DRCR='DR' AND   ISNULL(FCH_FCH_ID, 0) <>0 " _
                              & " AND FCH_BSU_ID='" & Session("sBsuid") & "' " _
                              & " AND FCH_STU_BSU_ID='" & ddlBusinessunit.SelectedItem.Value & "' AND FCH_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' " & str_Filter
                Case OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_CANCEL_APPROVAL
                    str_Sql = "SELECT * FROM FEES.vw_OSO_FEES_GETFEECONCESSIONTRANS" & _
                              " WHERE FCH_DRCR='DR' AND  ISNULL(FCH_bPosted,0)=0 AND FCH_STU_BSU_ID='" & ddlBusinessunit.SelectedItem.Value & "'" & _
                              " AND FCH_BSU_ID='" & Session("sBsuid") & "' " & _
                              " AND FCH_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' " & str_Filter
            End Select
            str_Sql = str_Sql & " ORDER BY FCH_DT DESC"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvFEEConcessionDet.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEConcessionDet.DataBind()
                Dim columnCount As Integer = gvFEEConcessionDet.Rows(0).Cells.Count
                gvFEEConcessionDet.Rows(0).Cells.Clear()
                gvFEEConcessionDet.Rows(0).Cells.Add(New TableCell)
                gvFEEConcessionDet.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEConcessionDet.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEConcessionDet.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEConcessionDet.DataBind()
            End If
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuNo")
            txtSearch.Text = lstrCondn1
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn2
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtConcession")
            txtSearch.Text = lstrCondn3
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtFrom")
            txtSearch.Text = lstrCondn4
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtPeriod")
            txtSearch.Text = lstrCondn5
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn6
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEConcessionDet.PageIndexChanging
        gvFEEConcessionDet.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFEEConcessionDet.RowDataBound
        Try
            Dim lblFCH_ID As New Label
            lblFCH_ID = TryCast(e.Row.FindControl("lblFCH_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblFCH_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")

                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION, OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_APPROVAL
                        hlEdit.NavigateUrl = "FeeConcessionTransBB.aspx?FCH_ID=" & Encr_decrData.Encrypt(lblFCH_ID.Text) & _
                                       "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode") & "&bsu=" & ddlBusinessunit.SelectedItem.Value
                    Case OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_CANCEL, OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_CANCEL_APPROVAL
                        hlEdit.NavigateUrl = "FeeConcessionTransCancelBB.aspx?FCH_ID=" & Encr_decrData.Encrypt(lblFCH_ID.Text) & _
                                   "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode") & "&bsu=" & ddlBusinessunit.SelectedItem.Value
                End Select
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub ddlAcademicYearYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Dim str_ids As String = ""
        If Not IsDate(txtFrom.Text) Then
            'lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retval As Integer
                For Each gvr As GridViewRow In gvFEEConcessionDet.Rows
                    Dim lblFCH_ID As Label = CType(gvr.FindControl("lblFCH_ID"), Label)
                    Dim chkPost As CheckBox = CType(gvr.FindControl("chkPost"), CheckBox)
                    If Not lblFCH_ID Is Nothing Then
                        If IsNumeric(lblFCH_ID.Text) And chkPost.Checked Then
                            str_ids = str_ids & "," & str_ids
                            retval = FeeDayendProcess.F_POSTCONCESSION_Transoport(Session("sBsuid"), txtFrom.Text, _
                            lblFCH_ID.Text, ddlBusinessunit.SelectedItem.Value, stTrans)
                            If retval <> 0 Then
                                Exit For
                            End If
                        End If
                    End If
                Next
                If (retval = 0) And str_ids = "" Then
                    stTrans.Rollback()
                    'lblError.Text = "Please Select Atleast One Concession!!!"
                    usrMessageBar.ShowNotification("Please Select Atleast One Concession!!!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
                If (retval = 0) Then
                    'stTrans.Rollback()
                    stTrans.Commit()
                    'lblError.Text = getErrorMessage(0)
                    usrMessageBar.ShowNotification(getErrorMessage(0), UserControls_usrMessageBar.WarningType.Success)
                    GridBind()
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_ids, "Posting", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                Else
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(retval)
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If

            Catch ex As Exception
                'lblError.Text = getErrorMessage("1000")
                usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        Session("PROVIDER_BSU_ID") = ddlBusinessunit.SelectedItem.Value
        FillACD()
        GridBind()
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBusinessunit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Protected Sub rbUnposted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbUnposted.CheckedChanged
        GridBind()
    End Sub

    Protected Sub rbPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPosted.CheckedChanged
        GridBind()
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        GridBind()
    End Sub

    Protected Sub rbRejected_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_ids As String = ""
        If Not IsDate(txtFrom.Text) Then
            'lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retval As Integer
                For Each gvr As GridViewRow In gvFEEConcessionDet.Rows
                    Dim lblFCH_ID As Label = CType(gvr.FindControl("lblFCH_ID"), Label)
                    Dim chkPost As CheckBox = CType(gvr.FindControl("chkPost"), CheckBox)
                    If Not lblFCH_ID Is Nothing Then
                        If IsNumeric(lblFCH_ID.Text) And chkPost.Checked Then
                            str_ids = str_ids & "," & str_ids
                            retval = FeeDayendProcess.REJECT_CONCESSION_Transport(lblFCH_ID.Text, stTrans, CType(Session("sUsr_name"), String))
                            If retval <> 0 Then
                                Exit For
                            End If
                        End If
                    End If
                Next
                If (retval = 0) And str_ids = "" Then
                    stTrans.Rollback()
                    'lblError.Text = "Please Select Atleast One Concession!!!"
                    usrMessageBar.ShowNotification("Please Select Atleast One Concession!!!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
                If (retval = 0) Then
                    stTrans.Commit()
                    'lblError.Text = getErrorMessage(0)
                    usrMessageBar.ShowNotification(getErrorMessage(0), UserControls_usrMessageBar.WarningType.Success)
                    GridBind()
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_ids, "Posting", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                Else
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(retval)
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If

            Catch ex As Exception
                'lblError.Text = getErrorMessage("1000")
                usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lbVoucher_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblFCH_ID As Label = sender.Parent.parent.findcontrol("lblFCH_ID")
            
            PrintConcession(lblFCH_ID.Text)
            h_print.Value = "print"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub PrintConcession(ByVal IntFCH_ID As Integer)
        Select Case ViewState("MainMnu_code").ToString
            Case OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_CANCEL
                Session("ReportSource") = FEEConcessionTransactionBB.PrintConcessionCancel(IntFCH_ID, Session("sBsuId"), Session("sUsr_name"))
                h_print.Value = "print"
            Case Else
                Session("ReportSource") = FEEConcessionTransactionBB.PrintConcession(IntFCH_ID, Session("sBsuId"), Session("sUsr_name"))
                h_print.Value = "print"
        End Select
    End Sub
End Class

