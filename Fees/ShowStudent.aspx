<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowStudent.aspx.vb" Inherits="ShowStudent" Theme="General" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <base target="_self" />
    <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

    <!-- Bootstrap header files ends here -->

    <%--       <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>--%>
    <script src="../Scripts/jquery-1.10.2.js" type="text/javascript"></script>
    <script>
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
    <style type="text/css">
        /*body{
        line-height:1.4 !important;
    }
    .button { 
    width: auto !important;
    min-width: 10%;
    padding: 8px !important; 
    background-color:#8dc24c !important;
    background-image:none !important; 
    height:auto !important;
    margin:4px;
}
        .ui-dialog-content {
            background-color:#FFF !important;
        }*/

        /*table class overrides starts here*/
        /*table {
        width: 100%;    
        margin-left:auto; 
        margin-right:auto;*/

        /*border-right: #8dc24c 1pt solid;
	    border-bottom: #8dc24c 1pt solid;
	    border-top: #8dc24c 1pt solid;
	    border-left: #8dc24c 1pt solid;*/
        /*}
    
    table th{
        padding: 0.5rem;
        vertical-align: middle;
        background-color:#8dc24c !important;
    } 
      .table-sm {
            width: 90%;       
            font-size: 10px !important;
            border:0px solid #8dc24c !important;

            margin-left:auto; 
            margin-right:auto;

            border-right: #8dc24c 1pt solid;
	        border-bottom: #8dc24c 1pt solid;
	        border-top: #8dc24c 1pt solid;
	        border-left: #8dc24c 1pt solid;
        }

     .table {
          width:100%;
     }
    .table th,
    .table td {
        padding: 0.0rem;
        vertical-align: middle;
        border:none !important;*/
        /*border-top: 0px solid #dee2e6 !important;*/
        /*}
    .table-striped tbody tr:nth-of-type(odd) {
        background-color: #e3f3e7;
    }

      input.form-control {
        width: auto;
        }
    table td input.button { 
    width: auto !important;
    min-width: 10%;
    padding: 8px !important; 
    background-color:#8dc24c !important;
    background-image:none !important; 
    height:auto !important;
    margin: 13px 12px 12px 10px;
}*/
        /*table class overrides ends here*/


        /*.header{    
       font-weight:bold;
       text-align:center;*/
        /*background-color:#8dc24c !important;*/
        /*}*/
        /*.link {
        color: darkblue;
        font-weight: bold; 
        text-decoration: underline;
    }
     .table-noborder {
          border:none !important;
          width:90% !important;
      }
    
     .form-control {
  border: 0px solid #ced4da !important;  
}*/
    </style>
    <script language="javascript" type="text/javascript">


        function SetValuetoParent(stuid) {
            //alert(stuid);
            //alert(window.parent.document.p);
            parent.setValue(stuid);
            return false;
        }

        function SetValuetoReferenceParent(result, REF_TYPE) {
            //alert(result);
            parent.setReferenceValue(result, REF_TYPE);
            return false;
        }

        function SetValuetoCancelConParent(result) {
            //alert(result);
            parent.setCancelConValue(result);
            return false;
        }
        function SetStudentValuetoParent(stuid) {
            //alert(stuid);
            parent.setStudentValue(stuid);
            return false;
        }
        function SetAllStudentsValue(stuid) {
            //alert(stuid);
            parent.SetAllStudentValue(stuid);
            return false;
        }
        function SetReverseStudentsParentValue(stuid) {
            //alert(stuid);
            parent.SetReverseStudentsValue(stuid);
            return false;
        }

        function setAllStudentParentValue(stuid) {
            //alert(stuid);
            parent.setAllStudentValue(stuid, false);
            return false;
        }
        function SetLostStudentsValue(stuid) {
            //alert(stuid);
            parent.SetLostStudentsParentValue(stuid, false);
            return false;
        }
        function SetSalesClientParentValue(stuid) {
            //alert(stuid);
            parent.SetSalesClientValue(stuid, false);
            return false;
        }

    </script>



</head>
<body onload="listen_window();">
    <form id="form1" runat="server">

        <table width="100%" align="center">
            <tr>
                <td align="left" width="20%"></td>
                <td align="left" width="30%"></td>
                <td align="left" width="20%"></td>
                <td align="left" width="30%"></td>
            </tr>

            <tr>
                <td align="left" width="20%"><span class="field-label">Academic Year    </span></td>
                <td align="left" width="30%">
                    <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                    </asp:DropDownList></td>
                <td align="left" width="20%" id="enrlbl_id" runat="server"><span class="field-label">Filter    </span></td>
                <td align="left" width="30%" id="enrdrp_id" runat="server">
                    <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True">
                       <%-- <asp:ListItem Value="1">All</asp:ListItem>
                        <asp:ListItem Value="0">ReEnrolment</asp:ListItem>--%>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <table width="100%" align="center" class="table-responsive">
            <tr>
                <td align="center">
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Data" AllowPaging="True" PageSize="20" CssClass="table table-bordered table-row">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Student#                                     
                                            <br />
                                    <asp:TextBox ID="txtCode" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCode" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Student Name<br />

                                    <asp:TextBox ID="txtName" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%----%>
                                    &nbsp;<asp:LinkButton ID="lbCodeSubmit" OnClick="LinkButton1_Click" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Grade<br />

                                    <asp:TextBox ID="txtGrade" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchControl" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("GRD_DISPLAY") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("PARENT_NAME") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    Parent Name 
                                    <br />

                                    <asp:TextBox ID="txtPName" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                        OnClick="ImageButton1_Click" />


                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Parent Mobile">
                                <HeaderTemplate>
                                    Parent Mobile      
                                    <br />

                                    <asp:TextBox ID="txtMobile" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                    <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                        OnClick="btnSearchName_Click" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("PARENT_MOBILE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ServiceRequest">
                                <HeaderTemplate>
                                    ServiceRequest<br />

                                    <asp:TextBox ID="txtSR" runat="server" SkinID="Gridtxt"></asp:TextBox>
                                    <asp:ImageButton ID="btnSR" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                        OnClick="btnSearchName_Click" />


                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblSFRef" runat="server" Text='<%# Bind("SF_ENQID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                    <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                    <input id="hf_STUID" runat="server" type="hidden" value="" />
                </td>
            </tr>
        </table>
        <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
        </script>

    </form>
</body>
</html>
