Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeRevenueRecognition
    Inherits PageLongProcess
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                'collect the url of the file to be redirected in view state
                imgStudent.Enabled = False
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                gvRevReco.Attributes.Add("bordercolor", "#1b80b6")
                gvCharging.Attributes.Add("bordercolor", "#1b80b6")
                gvFOMonthend.Attributes.Add("bordercolor", "#1b80b6")
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                ddlBUnit.DataBind()
                ddlBUnit.Items.FindByValue(Session("sBsuid")).Selected = True
                FillACD()
                If Session("sUsr_name") = "" _
                Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_REVENUE_RECOGNITION _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CHARGING _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ADJTO_FINANCE _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_POST_CHARGING) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))
                    chkHardClose.Visible = False
                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_FEE_REVENUE_RECOGNITION
                            lblHead.Text = "Revenue Recognition"
                            chkHardClose.Visible = True
                        Case OASISConstants.MNU_FEE_CHARGING
                            lblHead.Text = "Fee Charging"
                            tr_student_pick.Visible = True
                        Case OASISConstants.MNU_FEE_POST_CHARGING
                            lblHead.Text = "Post Fee Charging"
                        Case OASISConstants.MNU_FEE_ADJTO_FINANCE
                            lblHead.Text = "Post Adjustment To Finance"
                    End Select
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    setDate()
                    GridbindAll()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
        End If
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("result") = Nothing
        Session("message") = Nothing
        If Not OASISConstants.MNU_FEE_CHARGING.Equals(ViewState("MainMnu_code").ToString()) Then
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            objConn.Open() '
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim Retval As String = FeeCommon.CheckDayEnd(Session("sBsuid"), txtDate.Text, stTrans)

            If Retval <> "0" Then
                'lblError.Text = getErrorMessage(Retval)
                usrMessageBar.ShowNotification(getErrorMessage(Retval), UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
        End If
        h_Processeing.Value = "1"

        ' Me.lblError.Text = SaveAll("")
        usrMessageBar.ShowNotification(SaveAll(""), UserControls_usrMessageBar.WarningType.Information)
        'Dim LongProcess As New LongRun(AddressOf SaveAll)
        'Dim ar As IAsyncResult = LongProcess.BeginInvoke("CHECK", New AsyncCallback(AddressOf CallBackMethod), Nothing)
        'Session("result") = ar
    End Sub

    Function SaveAll(ByVal names As String) As String
        If Not Master.IsSessionMatchesForSave() Then
            Return OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
        End If
        H_DOCNO.Value = ""
        lblViewVoucher.Visible = False
        btnPrint.Visible = False
        Dim STATUS As Integer
        Dim STR_NEW_DOCNO As String = ""
        If IsDate(txtDate.Text) = False Then
            Return "Please Enter Valid Date"
        End If
        If ViewState("MainMnu_code").ToString = OASISConstants.MNU_FEE_CHARGING Then
            Return FeeCharge()
        End If
        Dim transaction As SqlTransaction
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        transaction = objConn.BeginTransaction("SampleTransaction")
        Try
            Dim objFDP As New FeeDayendProcess
            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.MNU_FEE_REVENUE_RECOGNITION
                    STATUS = FeeDayendProcess.F_POSTFEE_REVENUE(ddlBUnit.SelectedItem.Value, _
                   txtDate.Text, ddlAcademic.SelectedItem.Value, STR_NEW_DOCNO, chkHardClose.Checked, transaction)
                    H_DOCNO.Value = STR_NEW_DOCNO
                    If STR_NEW_DOCNO <> "" Then
                        lblViewVoucher.Visible = True
                        btnPrint.Visible = True
                    End If
                Case OASISConstants.MNU_FEE_POST_CHARGING
                    objFDP.POSTFEECHARGE(ddlBUnit.SelectedItem.Value, _
                    txtDate.Text, Session("sUsr_name"), ddlAcademic.SelectedItem.Value)
                    STATUS = objFDP.STATUS
                    'STATUS = objFDP.F_POSTFEECHARGE(ddlBUnit.SelectedItem.Value, _
                    'txtDate.Text, Session("sUsr_name"), ddlAcademic.SelectedItem.Value, transaction)
                    STR_NEW_DOCNO = objFDP.pbNEW_DOCNO
                    If STR_NEW_DOCNO <> "" Then
                        H_DOCNO.Value = STR_NEW_DOCNO
                        lblViewVoucher.Visible = True
                        btnPrint.Visible = True
                    End If
                Case OASISConstants.MNU_FEE_ADJTO_FINANCE
                    STATUS = FeeDayendProcess.F_POSTFEESADJUSTMENT_MONTHLY(ddlBUnit.SelectedItem.Value, _
                    txtDate.Text, STR_NEW_DOCNO, transaction)
                    If STR_NEW_DOCNO <> "" Then
                        H_DOCNO.Value = STR_NEW_DOCNO
                        lblViewVoucher.Visible = True
                        btnPrint.Visible = True
                    End If
            End Select
            If STATUS = 0 Then
                transaction.Commit()
                h_STUD_ID.Value = ""
                FillSTUNames(h_STUD_ID.Value)
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ddlBUnit.SelectedItem.Value, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                If ViewState("MainMnu_code") = OASISConstants.MNU_FEE_ADJTO_FINANCE Then
                    Return "New Document Generated : " & STR_NEW_DOCNO & "<br />Data Successfully Updated"
                ElseIf ViewState("MainMnu_code") = OASISConstants.MNU_FEE_POST_CHARGING Then
                    Return ""
                Else
                    Return getErrorMessage(STATUS)
                End If
            Else
                transaction.Rollback()
                Return getErrorMessage(STATUS)
            End If
        Catch ex As Exception
            transaction.Rollback()
            UtilityObj.Errorlog(ex.Message)
            Return getErrorMessage("1000")
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function

    Function FeeCharge() As String
        Dim STATUS As Integer

        If Not chkStudentby.Checked Then
            ''Geet students here
            h_STUD_ID.Value = ""
            Dim BUnitreaderSuper As SqlDataReader

            BUnitreaderSuper = GetAllStudentsList()
            If BUnitreaderSuper.HasRows = True Then
                While (BUnitreaderSuper.Read())
                    h_STUD_ID.Value = h_STUD_ID.Value & "||" & BUnitreaderSuper(0).ToString
                End While
            End If
        End If
        Try
            'Dim IDs As String() = h_STUD_ID.Value.Replace("||", "|").Split("|")
            Dim objFDP As New FeeDayendProcess
            objFDP.DOMONTHEND_STUDENTWISE(h_STUD_ID.Value.Replace("||", "|"), ddlBUnit.SelectedItem.Value, txtDate.Text, ddlAcademic.SelectedItem.Value)
            'STATUS = objFDP.STATUS

            Return "" 'getErrorMessage(STATUS)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return getErrorMessage("1000")
        End Try
        UtilityObj.operOnAudiTable(Master.MenuName, ddlBUnit.SelectedItem.Value, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
        Return getErrorMessage(STATUS)
    End Function

    Private Function GetAllStudentsList() As SqlDataReader
        Dim str_Sql As String = "  SELECT STU_ID FROM VW_OSO_STUDENT_M  WHERE STU_BSU_ID='" & Session("sBSUID") & "' AND STU_bActive =1 " & _
        " AND STU_ACD_ID='" & ddlAcademic.SelectedItem.Value & "'"
        Return SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
    End Function

    Protected Sub ddlBUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBUnit.SelectedIndexChanged
        FillACD()
        setDate()
        GridbindAll()
    End Sub

    Protected Sub ddlAcademic_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademic.SelectedIndexChanged
        setAcademicyearDate()
    End Sub

    Sub setDate()
        Dim str_condition As String = "MONTHEND"
        Select Case ViewState("MainMnu_code").ToString
            Case OASISConstants.MNU_FEE_REVENUE_RECOGNITION
                str_condition = "MONTHEND"
            Case OASISConstants.MNU_FEE_CHARGING
                str_condition = "DAYEND"
            Case OASISConstants.MNU_FEE_POST_CHARGING
                str_condition = "MONTHLY CHARGE POSTING"
            Case OASISConstants.MNU_FEE_ADJTO_FINANCE
                str_condition = "MONTHEND"
        End Select
        Dim sql_query As String = "SELECT MAX(FTA_TRANDT) FROM FEES.FEE_TRNSALL " _
        & " WHERE FTA_TRANTYPE='" & str_condition & "' AND FTA_BSU_ID='" & ddlBUnit.SelectedItem.Value & "' "
        If ViewState("MainMnu_code").ToString = OASISConstants.MNU_FEE_REVENUE_RECOGNITION Then
            sql_query = sql_query & " AND ISNULL(FTA_bHARDCLOSE,0)=1"
        End If
        Dim DTFROM As String = UtilityObj.GetDataFromSQL(sql_query, ConnectionManger.GetOASIS_FEESConnectionString)
        If IsDate(DTFROM) Then
            txtDate.Text = Format(CDate(DTFROM), OASISConstants.DateFormat)
        Else
            setAcademicyearDate()
        End If
        Select Case ViewState("MainMnu_code").ToString
            Case OASISConstants.MNU_FEE_REVENUE_RECOGNITION
                Dim dt As DateTime = Convert.ToDateTime(txtDate.Text)
                txtDate.Text = New DateTime(dt.Year, dt.Month, 1).AddMonths(2).AddDays(-1).ToString("dd/MMM/yyyy") 'Need Last day of next month
            Case OASISConstants.MNU_FEE_CHARGING
                txtDate.Text = Convert.ToDateTime(txtDate.Text).AddDays(1).ToString("dd/MMM/yyyy")
            Case OASISConstants.MNU_FEE_POST_CHARGING
                txtDate.Text = Convert.ToDateTime(txtDate.Text).AddDays(1).ToString("dd/MMM/yyyy")
            Case OASISConstants.MNU_FEE_ADJTO_FINANCE
                Dim dt As DateTime = Convert.ToDateTime(txtDate.Text)
                txtDate.Text = New DateTime(dt.Year, dt.Month, 1).AddMonths(2).AddDays(-1).ToString("dd/MMM/yyyy") 'Need Last day of next month
        End Select
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBUnit.SelectedItem.Value)
        ddlAcademic.DataSource = dtACD
        ddlAcademic.DataTextField = "ACY_DESCR"
        ddlAcademic.DataValueField = "ACD_ID"
        ddlAcademic.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademic.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        setDate()
    End Sub

    Sub GridbindAll()
        Dim sql_query As String = "SELECT MAX(FTA_TRANDT) FROM FEES.FEE_TRNSALL WHERE FTA_TRANTYPE='MONTHEND' " _
        & " AND FTA_BSU_ID='" & ddlBUnit.SelectedItem.Value & "' AND   FTA_ACD_ID='" & ddlAcademic.SelectedItem.Value & "'"
        Dim sql_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        sql_query = "SELECT   top(6)   REPLACE(CONVERT(VARCHAR(11), FTA_TRANDT, 113), ' ', '/') as Date  ," _
        & " FTA_DESCRIPTION as 'Desc', OASIS.dbo.fn_GetMonth ( FTA_MONTH) Month,  " _
        & " FTA_REMARKS as Remarks FROM FEES.FEE_TRNSALL" _
        & " WHERE FTA_TRANTYPE='MONTHEND' " _
        & " AND FTA_BSU_ID='" & ddlBUnit.SelectedItem.Value & "' AND   FTA_ACD_ID='" & ddlAcademic.SelectedItem.Value & "'"
        gvRevReco.DataSource = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        gvRevReco.DataBind()
        sql_query = "SELECT  top(6)    REPLACE(CONVERT(VARCHAR(11), FTA_TRANDT, 113), ' ', '/') as Date  ," _
              & " FTA_DESCRIPTION as 'Desc', OASIS.dbo.fn_GetMonth ( FTA_MONTH) Month,  " _
              & " FTA_REMARKS as Remarks FROM FEES.FEE_TRNSALL" _
              & " WHERE FTA_TRANTYPE='MONTHLY FEE CHARGE' " _
              & " AND FTA_BSU_ID='" & ddlBUnit.SelectedItem.Value & "' AND   FTA_ACD_ID='" & ddlAcademic.SelectedItem.Value & "'"
        gvCharging.DataSource = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        gvCharging.DataBind()
        gvFOMonthend.DataBind()
        sql_query = "SELECT  top(6)  REPLACE(CONVERT(VARCHAR(11), FTA_TRANDT, 113), ' ', '/') as Date  ," _
                     & " FTA_DESCRIPTION as 'Desc', OASIS.dbo.fn_GetMonth ( FTA_MONTH) Month,  " _
                     & " FTA_REMARKS as Remarks FROM FEES.FEE_TRNSALL" _
                     & " WHERE FTA_TRANTYPE='MONTHLY CHARGE POSTING' " _
                     & " AND FTA_BSU_ID='" & ddlBUnit.SelectedItem.Value & "' AND   FTA_ACD_ID='" & ddlAcademic.SelectedItem.Value & "'"
        gvPosting.DataSource = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        gvPosting.DataBind()
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim lblBSUID As New Label
        'lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        'If Not lblBSUID Is Nothing Then
        '    h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
        '    If Not FillBSUNames(h_BSUID.Value) Then
        '        h_BSUID.Value = lblBSUID.Text
        '    End If
        '    grdBSU.PageIndex = grdBSU.PageIndex
        '    FillBSUNames(h_BSUID.Value)
        'End If
    End Sub

    Private Function FillSTUNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = " SELECT STU_NO, STU_NAME FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
        gvStudentDetails.DataSource = ds
        gvStudentDetails.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Protected Sub gvStudentDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudentDetails.PageIndexChanging
        gvStudentDetails.PageIndex = e.NewPageIndex
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Sub setAcademicyearDate()
        Dim DTFROM As String = ""
        Dim DTTO As String = ""
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, ddlAcademic.SelectedItem.Value, Session("sBsuid"))
        txtDate.Text = DateFunctions.EndDateofMonth(DTFROM)
    End Sub

    Protected Sub lblAddNewStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddNewStudent.Click
        'h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        'FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        'Session("ReportSource") = AccountsReports.JournalVouchers(H_DOCNO.Value, String.Format("{0:dd/MMM/yyyy}", CDate(txtDate.Text)), ddlBUnit.SelectedItem.Value, "", H_DOCNO.Value, "JV")
        'Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
    End Sub

    Protected Sub chkStudentby_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkStudentby.CheckedChanged
        If chkStudentby.Checked Then
            imgStudent.Enabled = True
        Else
            imgStudent.Enabled = False
            h_STUD_ID.Value = ""
            FillSTUNames(h_STUD_ID.Value)
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetMsg() As String
        Dim str_message As String = IIf(HttpContext.Current.Session("message") Is Nothing, "", HttpContext.Current.Session("message"))
        HttpContext.Current.Session("message") = Nothing
        Return str_message
    End Function

    Protected Sub txtStudName_TextChanged(sender As Object, e As EventArgs)
        FillSTUNames(h_STUD_ID.Value)
    End Sub
End Class
