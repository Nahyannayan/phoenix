﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowAdvInvoices.aspx.vb" Inherits="Fees_ShowAdvInvoices" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  <%--  <link type="text/css" rel="stylesheet" href="../cssfiles/SiteStyle.css?1=2" />
    <link type="text/css" rel="stylesheet" href="../cssfiles/StyleSheet.css?1=2" />
   <link type="text/css" rel="stylesheet" href="../cssfiles/title.css?1=2" />--%>
      <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet"/>

</head>
<body>
    <form id="form1" runat="server">
        <div runat="server" id="divInvoice">
            <asp:GridView ID="gvAdvances" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row" EmptyDataText="No invoices to show" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Ivoice No">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtnInvNo" runat="server" Text='<%# Bind("FSH_INVOICE_NO") %>'></asp:LinkButton>
                            <asp:HiddenField ID="hf_FSH_ID" runat="server" Value='<%# Bind("FSH_ID")%>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblFooterText" runat="server" Text="TOTAL"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="FSH_INVOICE_DATE" HeaderText="Invoice Date" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="false" />
                    <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Description" />
                    <asp:BoundField DataField="FSH_NARRATION" HeaderText="Narration" />
                    <asp:TemplateField HeaderText="Debit">
                        <FooterTemplate>
                            <asp:Label ID="lblTotalDebit" runat="server">0.00</asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("DEBIT", "{0:0.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Credit">
                        <FooterTemplate>
                            <asp:Label ID="lblTotalCredit" runat="server" Text="0.00"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("CREDIT", "{0:0.00}") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div runat="server" id="divLedger">
            <asp:GridView ID="gvStuLedger" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row" EmptyDataText="No ledger data to show" ShowFooter="True">
                <Columns>
                    <asp:BoundField DataField="DOC DATE" HeaderText="Date" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="false" />
                    <asp:BoundField DataField="FEE" HeaderText="Fee" />
                    <asp:BoundField DataField="GRADE" HeaderText="Grade" />
                    <asp:BoundField DataField="REF No." HeaderText="Ref No" />
                    <asp:TemplateField HeaderText="Transaction">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("TRANSACTION") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("TRANSACTION") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Debit">
                       
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("DEBIT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Credit">
                      
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("CREDIT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Balance">
                       
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("[NET AMOUNT]") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
