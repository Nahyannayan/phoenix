﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Services
Imports System.Collections.Generic

Partial Class Fees_FeeGenerateAdvanceTaxInvoice
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            'Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = "add" 'Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If Session("sUsr_name") = "" Or (ViewState("MainMnu_code") <> "F300172" And ViewState("MainMnu_code") <> "F300173") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            ViewState("BATCH_NO") = 0
            ViewState("bShowPeriod") = "false"
            ShowMessage("", True)
            Clear()
            LoadAcademicYear()
            PopulateTerms_Months()
            LoadFeeTypes()
            SelectAllTreeView(trMonths.Nodes, True)
            SHOWHIDE_PERIOD()
            If Session("sBusper") = True Or Session("sroleid") = "204" Then
                trBulkStudents.Visible = True
            Else
                trBulkStudents.Visible = False
            End If
        End If
    End Sub

    Sub LoadAcademicYear()
        Dim dtACD As DataTable = FeeCommon.GetAcademicYear_CURRENT_NEXT(Session("sBsuId"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACD_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
    Private Sub PopulateTerms_Months()
        If rblTermOrMonth.SelectedValue = "M" Then
            lnkBtnSelectmonth.Text = "Select Month(s)"
            PopulateMonths()
        Else
            lnkBtnSelectmonth.Text = "Select Term(s)"
            PopulateTerms()
        End If
    End Sub
    Private Sub PopulateMonths()
        Dim dtTable As DataTable = clsTaxFunctions.PopulateMonthsInAcademicYear(ddlAcademicYear.SelectedValue, Session("sbsuid"))
        ' PROCESS Filter
        Dim dvTRM_DESCRIPTION As New DataView(dtTable, "", "TRM_DESCRIPTION", DataViewRowState.OriginalRows)

        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim ienumTRM_DESCRIPTION As IEnumerator = dvTRM_DESCRIPTION.GetEnumerator
        Dim drTRM_DESCRIPTION As DataRowView
        While (ienumTRM_DESCRIPTION.MoveNext())
            'Processes List
            drTRM_DESCRIPTION = ienumTRM_DESCRIPTION.Current

            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drTRM_DESCRIPTION("TRM_DESCRIPTION") Then
                    contains = True
                End If
            End While
            Dim trNodeTRM_DESCRIPTION As New TreeNode(drTRM_DESCRIPTION("TRM_DESCRIPTION"), drTRM_DESCRIPTION("AMS_MONTH"))
            If contains Then
                Continue While
            End If

            Dim strAMS_MONTH As String = "TRM_DESCRIPTION = '" & _
            drTRM_DESCRIPTION("TRM_DESCRIPTION") & "'"
            Dim dvAMS_MONTH As New DataView(dtTable, strAMS_MONTH, "AMS_MONTH", DataViewRowState.OriginalRows)
            Dim ienumAMS_MONTH As IEnumerator = dvAMS_MONTH.GetEnumerator
            While (ienumAMS_MONTH.MoveNext())
                Dim drMONTH_DESCR As DataRowView = ienumAMS_MONTH.Current
                Dim trNodeMONTH_DESCR As New TreeNode(drMONTH_DESCR("MONTH_DESCR"), drMONTH_DESCR("AMS_ID"))
                trNodeTRM_DESCRIPTION.ChildNodes.Add(trNodeMONTH_DESCR)
            End While
            trSelectAll.ChildNodes.Add(trNodeTRM_DESCRIPTION)
        End While
        trMonths.Nodes.Clear()
        trMonths.Nodes.Add(trSelectAll)
        trMonths.DataBind()
    End Sub

    Private Sub PopulateTerms()
        Dim dtTable As DataTable = clsTaxFunctions.PopulateTermsInAcademicYear(ddlAcademicYear.SelectedValue, Session("sbsuid"))
        ' PROCESS Filter
        Dim dvTRM_DESCRIPTION As New DataView(dtTable, "", "TRM_DESCRIPTION", DataViewRowState.OriginalRows)

        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim ienumTRM_DESCRIPTION As IEnumerator = dvTRM_DESCRIPTION.GetEnumerator
        Dim drTRM_DESCRIPTION As DataRowView
        While (ienumTRM_DESCRIPTION.MoveNext())
            'Processes List
            drTRM_DESCRIPTION = ienumTRM_DESCRIPTION.Current

            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drTRM_DESCRIPTION("TRM_DESCRIPTION") Then
                    contains = True
                End If
            End While
            Dim trNodeTRM_DESCRIPTION As New TreeNode(drTRM_DESCRIPTION("TRM_DESCRIPTION"), drTRM_DESCRIPTION("TRM_ID"))
            If contains Then
                Continue While
            End If
            trSelectAll.ChildNodes.Add(trNodeTRM_DESCRIPTION)
        End While
        trMonths.Nodes.Clear()
        trMonths.Nodes.Add(trSelectAll)
        trMonths.DataBind()
    End Sub

    Private Sub SelectAllTreeView(ByVal nodes As TreeNodeCollection, ByVal selAll As Boolean)
        For Each node As TreeNode In nodes
            node.Checked = selAll
            If node.ChildNodes.Count > 0 Then
                SelectAllTreeView(node.ChildNodes, selAll)
            End If
        Next
    End Sub

    Private Sub LoadFeeTypes()
        Dim ds As DataSet = clsTaxFunctions.GET_FEE_TYPE_FOR_ADV_TAX_INVOICE(Session("sBsuId"))
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ddlFeeType.DataSource = ds.Tables(0)
            ddlFeeType.DataTextField = "FEE_DESCR"
            ddlFeeType.DataValueField = "FEE_ID"
            ddlFeeType.DataBind()
        End If
    End Sub
    Private Sub Clear()
        Session("gvStudents") = Nothing
        ViewState("chkAll") = False
        BindGrid()
        txtStudent.Text = ""
        txtStudentBulk.Text = ""
        txtNarration.Text = ""
        h_STU_ID.Value = ""
    End Sub
    Private Sub BindGrid()
        gvStudents.DataSource = Session("gvStudents")
        gvStudents.DataBind()
    End Sub
    Private Function VALIDATE_TERM_SELECTION() As Boolean
        Try
            VALIDATE_TERM_SELECTION = True
            If GET_SELECTED_TERMS() = "" OrElse GET_SELECTED_TERMS() = "ALL" Then
                If rblTermOrMonth.SelectedValue = "T" Then
                    ShowMessage("Please select Terms", True)
                Else
                    ShowMessage("Please select Months(Period)", True)
                End If
                VALIDATE_TERM_SELECTION = False
            End If
            'Dim dtDt As New DataTable
            'Dim cMonthID As New DataColumn("MonthID", System.Type.GetType("System.Int32"))
            'Dim cMonthDescr As New DataColumn("MonthDescr", System.Type.GetType("System.String"))
            'dtDt.Columns.Add(cMonthID)
            'dtDt.Columns.Add(cMonthDescr)
            'Dim dr As DataRow
            'For Each node As TreeNode In trMonths.CheckedNodes
            '    If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
            '        Continue For
            '    End If
            '    dr = dtDt.NewRow()
            '    dr("MonthID") = node.Value
            '    dr("MonthDescr") = node.Text
            '    dtDt.Rows.Add(dr)
            'Next
            'If dtDt Is Nothing OrElse dtDt.Rows.Count <= 0 Then
            '    VALIDATE_TERM_SELECTION = False
            'Else
            '    Session("dtMonths") = dtDt
            '    VALIDATE_TERM_SELECTION = True
            'End If
        Catch
            VALIDATE_TERM_SELECTION = False
        End Try
    End Function
    Protected Sub rblTermOrMonth_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblTermOrMonth.SelectedIndexChanged
        PopulateTerms_Months()
        Clear()
    End Sub

    <WebMethod()> _
    Public Shared Function GetStudent(ByVal BSUID As String, ByVal STUTYPE As String, ByVal prefix As String, ByVal COMPID As String) As String()
        
        Return clsTaxFunctions.GetStudent(BSUID, STUTYPE, prefix, COMPID)
    End Function
    <WebMethod()> _
    Public Shared Function GetCompany(ByVal prefix As String) As String()
        Dim company As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASISConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "SELECT COMP_ID,COMP_NAME FROM dbo.COMP_LISTED_M WITH(NOLOCK) WHERE ISNULL(COMP_bACTIVE,0)=1 AND ISNULL(COMP_COUNTRY,'')='" & HttpContext.Current.Session("BSU_COUNTRY_ID") & "' AND " & "COMP_NAME like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        company.Add(String.Format("{0}-{1}", sdr("COMP_NAME").ToString.Replace("-", " "), sdr("COMP_ID")))
                    End While
                End Using
                conn.Close()
            End Using
            Return company.ToArray()
        End Using
    End Function
    Private Function GET_SELECTED_TERMS() As String
        GET_SELECTED_TERMS = ""
        Dim seperator As String = ""
        For Each node As TreeNode In trMonths.CheckedNodes
            If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                Continue For
            End If
            If node.Value <> "ALL" Then
                GET_SELECTED_TERMS = GET_SELECTED_TERMS & seperator & node.Value
                seperator = "|"
            End If
        Next
    End Function
    Private Function FillMonthsSelected() As String
        Try
            Dim str_Selected As String = ""
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If node.Value <> "ALL" Then
                    If str_Selected = "" Then
                        str_Selected = "For the month(s) " & Left(node.Text, 3).ToUpper
                    Else
                        str_Selected = str_Selected & ", " & Left(node.Text, 3).ToUpper
                    End If
                End If
            Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Private Function FillTermsSelected() As String
        Try
            Dim str_Selected As String = ""
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If node.Value <> "ALL" Then
                    If str_Selected = "" Then
                        str_Selected = "For the Term(s) " & node.Text
                    Else
                        str_Selected = str_Selected & ", " & node.Text
                    End If
                End If
            Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function
    Private Sub SHOWHIDE_PERIOD()
        If ddlFeeType.Items.Count > 0 AndAlso ddlAcademicYear.Items.Count > 0 Then
            Dim SCH_ID As Integer = FeeCommon.GET_FEE_COLLECTION_SCHEDULE_ID(Session("sBsuId"), ddlAcademicYear.SelectedValue, ddlFeeType.SelectedValue)
            If SCH_ID = 0 OrElse SCH_ID = 2 Then 'if not Monthly or not Quarterly, show period selection
                tdPeriod1.Visible = True
                tdPeriod2.Visible = True
                tdterms1.Visible = True
                tdterms2.Visible = True
                Panel2.Visible = True
                trMessage.Visible = True
                If SCH_ID = 0 Then
                    rblTermOrMonth.SelectedValue = "M"
                ElseIf SCH_ID = 2 Then
                    rblTermOrMonth.SelectedValue = "T"
                End If
                PopulateTerms_Months()
            Else
                tdPeriod1.Visible = False
                tdPeriod2.Visible = False
                tdterms1.Visible = False
                tdterms2.Visible = False
                Panel2.Visible = False
                trMessage.Visible = False
            End If
        End If
    End Sub

    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        If Message <> "" Then
            If bError Then
                ' lblError.CssClass = "diverrorPopUp"
                usrMessageBar.ShowNotification(Message, UserControls_usrMessageBar.WarningType.Danger)
            Else
                'lblError.CssClass = "divvalidPopUp"
                usrMessageBar.ShowNotification(Message, UserControls_usrMessageBar.WarningType.Success)
            End If
        Else
            '  usrMessageBar.ShowNotification(Message, UserControls_usrMessageBar.WarningType.Information)
            'lblError.CssClass = ""
        End If
        'lblError.Text = Message
    End Sub
    Private Function VALIDATE_PAGE() As Boolean
        VALIDATE_PAGE = True
        If ddlFeeType.Items.Count <= 0 Then
            ShowMessage("Fee type selection not found!", True)
            VALIDATE_PAGE = False
            Exit Function
        End If
        If gvStudents.Rows.Count <= 0 Then
            ShowMessage("Please add Student(s)", True)
            VALIDATE_PAGE = False
            Exit Function
        End If
        If txtNarration.Text.Trim = "" Then
            ShowMessage("Narration cannot be empty", True)
            VALIDATE_PAGE = False
            Exit Function
        End If
        For Each gvr As GridViewRow In gvStudents.Rows
            Dim txtAmount As TextBox = gvr.FindControl("txtAmount")
            If Not txtAmount Is Nothing Then
                If IsNumeric(txtAmount.Text) Then
                    If CDbl(txtAmount.Text) <= 0 Then
                        ShowMessage("Please enter a valid amount for Student Id " & gvr.Cells(1).Text, True)
                        VALIDATE_PAGE = False
                        Exit Function
                    End If
                Else
                    txtAmount.Text = "0.00"
                End If
            End If
        Next
    End Function

    Private Function VALID_ADD_STUDENT() As Boolean
        VALID_ADD_STUDENT = True
        If ddlFeeType.Items.Count <= 0 Then
            ShowMessage("Fee type selection not found!", True)
            VALID_ADD_STUDENT = False
            Exit Function
        End If
        If ddlAcademicYear.Items.Count <= 0 Then
            ShowMessage("Academic year selection not found!", True)
            VALID_ADD_STUDENT = False
            Exit Function
        End If
    End Function
    Protected Sub imgStudent_Click(sender As Object, e As ImageClickEventArgs) Handles imgStudent.Click
        lbtnLoadStuDetail_Click(Nothing, Nothing)
    End Sub
    Protected Sub lbtnLoadStuDetail_Click(sender As Object, e As EventArgs) Handles lbtnLoadStuDetail.Click
        If Not VALID_ADD_STUDENT() Then
            Exit Sub
        End If
        Dim objclsTaxFunctions As New clsTaxFunctions
        objclsTaxFunctions.PROVIDER_BSU_ID = Session("sBsuId")
        objclsTaxFunctions.STU_BSU_ID = Session("sBsuId")
        objclsTaxFunctions.FEE_ID = ddlFeeType.SelectedValue
        'Dim STU_ID_COUNT As Integer = h_STU_ID.Value.Split("||").Length
        'If STU_ID_COUNT = 1 AndAlso IsNumeric(h_STU_ID.Value) And txtStudentBulk.Text.Trim = "" Then
        '    objclsTaxFunctions.STU_ID = h_STU_ID.Value
        '    If objclsTaxFunctions.GET_STUDENT_DETAILS(rblStuType.SelectedValue) Then
        '        txtStudent.Text = objclsTaxFunctions.STU_NO & " - " & objclsTaxFunctions.STU_NAME
        '    Else
        '        txtStudent.Text = ""
        '    End If
        'End If
        objclsTaxFunctions.ACD_ID = ddlAcademicYear.SelectedValue
        objclsTaxFunctions.STU_IDs = h_STU_ID.Value
        objclsTaxFunctions.STU_TYPE = rblStuType.SelectedValue
        objclsTaxFunctions.Period = GET_SELECTED_TERMS()
        objclsTaxFunctions.BTerm = IIf(rblTermOrMonth.SelectedValue = "T", True, False)
        Session("gvStudents") = objclsTaxFunctions.GET_FEE_SETUP_FOR_ADV_TAX_INVOICE(Session("sBsuId"))
        BindGrid()
        h_STU_ID.Value = ""
        txtNarration.Focus()
    End Sub
    Protected Sub lbtnClearStudent_Click(sender As Object, e As EventArgs) Handles lbtnClearStudent.Click
        h_STU_ID.Value = ""
        txtStudent.Text = ""
    End Sub
    Protected Sub lbtnDelete_Click(sender As Object, e As EventArgs)
        Dim gvr As GridViewRow = DirectCast(DirectCast(sender, LinkButton).NamingContainer, GridViewRow)
        If Not gvr Is Nothing Then
            Dim ID As Integer = gvStudents.DataKeys(gvr.RowIndex)(0)
            DirectCast(Session("gvStudents"), DataTable).Rows(ID - 1).Delete()
            DirectCast(Session("gvStudents"), DataTable).Rows(ID - 1).AcceptChanges()
            clsTaxFunctions.REARRANGE_DATATABLE()
            BindGrid()
        End If
    End Sub

    Protected Sub rblStuType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblStuType.SelectedIndexChanged
        Clear()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Clear()
        PopulateTerms_Months()
        SelectAllTreeView(trMonths.Nodes, True)
        SHOWHIDE_PERIOD()
    End Sub
    Protected Sub ddlFeeType_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlFeeType.SelectedIndexChanged
        Clear()
        SHOWHIDE_PERIOD()
    End Sub
    Protected Sub gvStudents_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvStudents.PageIndexChanging
        For Each gvr As GridViewRow In gvStudents.Rows
            Dim txtAmount As TextBox = gvr.FindControl("txtAmount")
            Dim ID As Integer = gvStudents.DataKeys(gvr.RowIndex)(0)
            If Not txtAmount Is Nothing AndAlso IsNumeric(txtAmount.Text) Then
                DirectCast(Session("gvStudents"), DataTable).Rows(ID - 1)("AMOUNT") = txtAmount.Text
            End If
        Next
        gvStudents.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If VALIDATE_PAGE() Then
            Try
                ShowMessage("", False)
                Dim objclsTaxFns As New clsTaxFunctions
                objclsTaxFns.STU_BSU_ID = Session("sBsuId")
                objclsTaxFns.PROVIDER_BSU_ID = Session("sBsuId")
                objclsTaxFns.ACD_ID = ddlAcademicYear.SelectedValue
                objclsTaxFns.MIT_DATE = Format(Date.Now, OASISConstants.DataBaseDateFormat)
                objclsTaxFns.STU_TYPE = rblStuType.SelectedValue
                objclsTaxFns.FEE_ID = ddlFeeType.SelectedValue
                objclsTaxFns.BTerm = IIf(rblTermOrMonth.SelectedValue = "T", True, False)
                objclsTaxFns.MIT_TERM_IDs = GET_SELECTED_TERMS()
                objclsTaxFns.MIT_NARRATION = Left(txtNarration.Text.Trim, 200)
                objclsTaxFns.User = Session("sUsr_name")
                objclsTaxFns.MIT_BATCH_NO = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT ISNULL(MAX(MIT_BATCH_NO),0)+1 AS BATCH_NO FROM FEES.ADVANCE_TAX_INVOICES")
                ViewState("BATCH_NO") = objclsTaxFns.MIT_BATCH_NO
                Dim SuccessfulSavedCount As Integer = 0
                For Each gvr As GridViewRow In gvStudents.Rows
                    Dim txtAmount As TextBox = gvr.FindControl("txtAmount")
                    Dim STU_ID As Integer = gvStudents.DataKeys(gvr.RowIndex)(1)
                    Dim chk As CheckBox = gvr.FindControl("chkReverseInv")
                    Dim lblAdvBalance As Label = gvr.FindControl("lblAdvBalance")
                    Dim retmessage As String = ""
                    If Not txtAmount Is Nothing Then
                        If IsNumeric(txtAmount.Text) Then
                            objclsTaxFns.INV_AMOUNT = CDbl(txtAmount.Text)
                            objclsTaxFns.STU_ID = STU_ID
                            objclsTaxFns.bReversePrevInvoice = chk.Checked
                            If chk.Checked Then 'Reverse existing advance tax amount
                                objclsTaxFns.AIR_AMOUNT = CDbl(lblAdvBalance.Text)
                            Else
                                objclsTaxFns.AIR_AMOUNT = 0
                            End If
                            Dim resultVal As Integer = objclsTaxFns.GENERATE_ADV_TAX_INVOICE(retmessage)
                            If resultVal <> 0 Then
                                ShowMessage(Me.lblError.Text & " | " & "Row " & gvr.RowIndex + 1 & "-" & UtilityObj.getErrorMessage(resultVal), True)
                            ElseIf resultVal = 0 Then
                                SuccessfulSavedCount += 1
                            End If
                            If retmessage <> "" Then
                                ShowMessage(Me.lblError.Text & " | " & retmessage, True)
                            End If
                        End If
                    End If
                Next
                If SuccessfulSavedCount = gvStudents.Rows.Count Then
                    ShowMessage("Invoices created succesfully for " & SuccessfulSavedCount & " Student(s)", False)
                    Clear()
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                ShowMessage(ex.Message, True)
            End Try
        End If
    End Sub

    Protected Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click
        If txtStudentBulk.Text.Trim <> "" Then
            Dim STU_NOs() As String = txtStudentBulk.Text.Trim.Split(",")
            If STU_NOs.Length > 0 Then
                h_STU_ID.Value = ""
                h_STU_ID.Value = clsTaxFunctions.GET_STU_ID_PADDED(Session("sBsuId"), txtStudentBulk.Text.Trim.Replace("'", ""))
                If h_STU_ID.Value <> "" Then
                    lbtnLoadStuDetail_Click(Nothing, Nothing)
                    h_STU_ID.Value = ""
                    txtStudentBulk.Text = ""
                Else
                    ShowMessage("Unable to fetch Student details.", True)
                End If
            Else
                ShowMessage("Unable to find any student Id(s).", True)
            End If
        End If
    End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        ShowMessage("", False)
        ViewState("BATCH_NO") = 0
        Clear()
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        SelectAllTreeView(trMonths.Nodes, True)
    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click

    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ShowMessage("", False)
            ViewState("BATCH_NO") = 0
            Clear()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub lbtnClearCompany_Click(sender As Object, e As EventArgs) Handles lbtnClearCompany.Click
        Me.chkCompanyFilter.Checked = False
        Me.h_Company_ID.Value = "-1"
        Me.txtCompanyDescr.Text = ""
    End Sub

    Protected Sub gvStudents_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvStudents.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lbtnVwInvoices As LinkButton = DirectCast(e.Row.FindControl("lbtnVwInvoices"), LinkButton)
            Dim lbtnLedger As LinkButton = DirectCast(e.Row.FindControl("lbtnLedger"), LinkButton)
            'Dim lblAdvBalance As Label = DirectCast(e.Row.FindControl("lblAdvBalance"), Label)

            Dim STU_ID As Long = gvStudents.DataKeys(e.Row.RowIndex).Values("STU_ID")
            If Not lbtnVwInvoices Is Nothing Then
                Dim QueryString = "&ID=" & Encr_decrData.Encrypt(STU_ID) & "&FID=" & Encr_decrData.Encrypt(ddlFeeType.SelectedValue) & "&STYP=" & Encr_decrData.Encrypt(rblStuType.SelectedValue) & ""
                lbtnVwInvoices.Attributes.Add("onClick", "return ShowWindowWithClose('ShowAdvInvoices.aspx?TYPE=" & Encr_decrData.Encrypt("INV") & QueryString & "', 'ADVANCE INVOICES', '40%', '50%');")
                QueryString = "&ID=" & Encr_decrData.Encrypt(STU_ID) & "&FID=" & Encr_decrData.Encrypt(ddlFeeType.SelectedValue) & "&STYP=" & Encr_decrData.Encrypt(rblStuType.SelectedValue) & ""
                lbtnLedger.Attributes.Add("onClick", "return ShowWindowWithClose('ShowAdvInvoices.aspx?TYPE=" & Encr_decrData.Encrypt("STULEDGER") & QueryString & "', 'STUDENT LEDGER', '60%', '60%');")
            End If
            'If Not lblAdvBalance Is Nothing Then
            '    Dim objclsTaxFunctions As New clsTaxFunctions
            '    objclsTaxFunctions.PROVIDER_BSU_ID = Session("sBsuId")
            '    objclsTaxFunctions.STU_BSU_ID = Session("sBsuId")
            '    objclsTaxFunctions.STU_ID = STU_ID
            '    objclsTaxFunctions.FEE_ID = ddlFeeType.SelectedValue
            '    objclsTaxFunctions.bSUMMARY = True
            '    objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT(rblStuType.SelectedValue)
            '    lblAdvBalance.Text = Format(objclsTaxFunctions.BALANCE, "#,##0.00")
            'End If
        ElseIf e.Row.RowType = DataControlRowType.Header Then
            Dim chkRevAll As CheckBox = DirectCast(e.Row.FindControl("chkReverseAll"), CheckBox)
            If Not chkRevAll Is Nothing Then
                chkRevAll.Checked = ViewState("chkAll")
            End If
        End If
    End Sub

    Protected Sub chkReverseAll_CheckedChanged(sender As Object, e As EventArgs)
        Dim chkRevAll As CheckBox = DirectCast(sender, CheckBox)
        If Not chkRevAll is Nothing Then
            For Each dr As DataRow In DirectCast(Session("gvStudents"), DataTable).Rows
                dr("bREVERSE") = chkRevAll.Checked
            Next
            ViewState("chkAll") = chkRevAll.Checked
            BindGrid()
        End If
    End Sub

    Protected Sub btnFillNarration_Click(sender As Object, e As EventArgs) Handles btnFillNarration.Click
        If tdPeriod1.Visible Then
            If rblTermOrMonth.SelectedValue = "M" Then
                Dim narration As String = FillMonthsSelected()
                If narration = "" Then
                    ShowMessage("None of the Month(s) are selected", True)
                    Exit Sub
                End If
                txtNarration.Text = narration & " of " & ddlAcademicYear.SelectedItem.Text
            Else
                Dim narration As String = FillTermsSelected()
                If narration = "" Then
                    ShowMessage("None of the Term(s) are selected", True)
                    Exit Sub
                End If
                txtNarration.Text = narration & " of " & ddlAcademicYear.SelectedItem.Text
            End If
        Else
            txtNarration.Text = "For the academic year " & ddlAcademicYear.SelectedItem.Text
        End If

    End Sub

    Protected Sub chkReverseInv_CheckedChanged(sender As Object, e As EventArgs)
        Dim chkReverseInv As CheckBox = DirectCast(sender, CheckBox)
        Dim gvr As GridViewRow = DirectCast(chkReverseInv.NamingContainer, GridViewRow)
        If Not gvr Is Nothing Then
            Dim ID As Integer = gvStudents.DataKeys(gvr.RowIndex)(0)
            DirectCast(Session("gvStudents"), DataTable).Rows(ID - 1)("bREVERSE") = chkReverseInv.Checked
            BindGrid()
        End If
    End Sub
  
    Protected Sub h_STU_ID_ValueChanged(sender As Object, e As EventArgs)
        lbtnLoadStuDetail_Click(Nothing, Nothing)
    End Sub
End Class
