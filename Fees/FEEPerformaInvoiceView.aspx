<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FEEPerformaInvoiceView.aspx.vb" Inherits="FEEPerformaInvoiceView" Theme="General" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
   function CheckForPrint() {
       if (document.getElementById('<%= h_print.ClientID %>').value != '') {
           document.getElementById('<%= h_print.ClientID %>').value = '';
           var result = radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up');
           return false;
       }
   }
    );

   function ChangeAllCheckBoxStates(checkState) {
       var chk_state = document.getElementById("chkAL").checked;
       for (i = 0; i < document.forms[0].elements.length; i++) {
           if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
               if (document.forms[0].elements[i].type == 'checkbox') {
                   document.forms[0].elements[i].checked = chk_state;
               }
       }
   }

   function ConfirmSendingEmail() {
       return confirm('Are you sure you want send the Email..?');
   }

   function autoSizeWithCalendar(oWindow) {
       var iframe = oWindow.get_contentFrame();
       var body = iframe.contentWindow.document.body;
       var height = body.scrollHeight;
       var width = body.scrollWidth;
       var iframeBounds = $telerik.getBounds(iframe);
       var heightDelta = height - iframeBounds.height;
       var widthDelta = width - iframeBounds.width;
       if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
       if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
       oWindow.center();
   }


    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="720px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            View Fee Pro forma Invoice
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <%--  <table border="0" cellpadding="5" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td align="left" >                             
                            
                        </td>
                        <td align="right"></td>
                    </tr>
                </table>--%>
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr valign="top">
                        <th align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                            <%--<asp:Label ID="lblError" runat="server" EnableViewState="false" ></asp:Label>--%>
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                        </th>
                        <%--<th align="left" colspan="2" valign="middle" >View Fee Pro forma Invoice...</th>--%>
                        <th align="right" colspan="3" valign="middle">
                            <asp:LinkButton ID="lbtnExport" runat="server">Export Selected to Excel</asp:LinkButton>
                            <span>&nbsp;&nbsp;</span>
                            <asp:LinkButton ID="LinkButton1" runat="server">Print Selected</asp:LinkButton></th>
                    </tr>
                    <tr>
                        <td align="left" valign="top" width="15%">
                            <asp:RadioButton ID="radStudent" runat="server" GroupName="StudType" Text="Student" AutoPostBack="True" Checked="True" CssClass="field-label" />
                            <asp:RadioButton ID="radEnquiry" runat="server" GroupName="StudType" Text="Enquiry" AutoPostBack="True" CssClass="field-label" /></td>
                        <td align="left" valign="top" width="25%">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <span class="field-label">Academic Year</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddAcademic" runat="server" AutoPostBack="True"
                                            DataSourceID="odsGetBSUAcademicYear" DataTextField="ACY_DESCR" DataValueField="ACD_ID" SkinID="DropDownListNormal">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" width="30%">

                            <asp:RadioButton ID="radAll" runat="server" GroupName="Email" Text="All" CssClass="field-label"
                                AutoPostBack="True" Checked="True" />
                            <asp:RadioButton ID="radSent" runat="server" GroupName="Email" Text="Emailed" CssClass="field-label"
                                AutoPostBack="True" />
                            <asp:RadioButton ID="radProcessed" runat="server" GroupName="Email" Text="Processed" CssClass="field-label"
                                AutoPostBack="True" />
                            <asp:RadioButton ID="radNotSent" runat="server" GroupName="Email" CssClass="field-label"
                                Text="Not Emailed" AutoPostBack="True" />
                            <asp:RadioButton ID="radEmailFailed" runat="server" GroupName="Email" Text="Failed" CssClass="field-label"
                                AutoPostBack="True" />
                        </td>

                        <td align="right" width="20%">
                            <asp:RadioButton ID="radAllType" runat="server" GroupName="FeeType" Text="All" CssClass="field-label"
                                AutoPostBack="True" Checked="True" />
                            <asp:RadioButton ID="radFeeType" runat="server" GroupName="FeeType" Text="Fee" CssClass="field-label"
                                AutoPostBack="True" />
                            <asp:RadioButton ID="radServiceType" runat="server" GroupName="FeeType" CssClass="field-label"
                                Text="Service" AutoPostBack="True" /></td>
                    </tr>
                    <tr runat="server" id="postInv"  visible="false">
                        <td align="right" colspan="4">
                            <asp:RadioButton ID="radAllPosted" runat="server" GroupName="FeePosted" Text="All" CssClass="field-label"
                                AutoPostBack="True"  />
                             <asp:RadioButton ID="radNotPosted" runat="server" GroupName="FeePosted" CssClass="field-label"
                                Text="Not Posted" AutoPostBack="True" Checked="True"/>
                            <asp:RadioButton ID="radPosted" runat="server" GroupName="FeePosted" Text="Posted" CssClass="field-label"
                                AutoPostBack="True" />
                           </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="4">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                Width="100%" AllowPaging="True" PageSize="30">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>
                                            <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("FPH_INOICENO") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Inv. No">
                                        <HeaderTemplate>
                                            Inv. No
                                            <br />
                                            <asp:TextBox ID="txtInvNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnInvNoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblInvNo" runat="server" Text='<%# bind("FPH_INOICENO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date
                                            <br />
                                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="COMP_ID">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCOMP_ID" runat="server" Text='<%# bind("FPH_COMP_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Company Name">
                                        <HeaderTemplate>
                                            Company/E-Mail ID<br />
                                            <asp:TextBox ID="txtCompany" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCompanySearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("COMP_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Academic Year">
                                        <HeaderTemplate>
                                            Grade<br />
                                            <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("GRD_SEC") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student #">
                                        <HeaderTemplate>
                                            Student#
                                            <br />
                                            <asp:TextBox ID="txtStudNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudNoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudNo" runat="server" Text='<%# bind("STU_NO") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <HeaderTemplate>
                                            Student Name
                                            <br />
                                            <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudnameSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudName" runat="server" Text='<%# bind("STUD_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FPH_EmailStatus" HeaderText="Email log Description" />
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblPrintReciept" OnClick="lblPrintReciept_Click" runat="server">Proforma Invoice</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" colspan="4">
                            <asp:Button ID="btnEMail" runat="server" Text="Send E-Mail"
                                CssClass="button" CausesValidation="False" OnClientClick="return ConfirmSendingEmail('C');" OnClick="btnEMail_Click"></asp:Button>
                            <asp:RadioButtonList ID="rblContacts" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem Selected="True" Value="P">Primary Contact</asp:ListItem>
                                <asp:ListItem Value="S">Secondary Contact</asp:ListItem>
                                <asp:ListItem Value="B">Both</asp:ListItem>
                            </asp:RadioButtonList>
                             <asp:Button ID="btnPostInv" runat="server" Text="Post Invoice"
                                CssClass="button" CausesValidation="False" OnClick="btnPostInv_Click"></asp:Button>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_print" runat="server" />

                <asp:ObjectDataSource ID="odsGetBSUAcademicYear" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetBSUAcademicYear" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </div>
    </div>
</asp:Content>
