<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FeeReceipt.aspx.vb" Inherits="ParentLogin_FeeReceipt" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Receipt</title>
    <base target="_self" />
    <style>
        .img {
            display: block;
        }

        .ReceiptCaption {
            FONT-WEIGHT: bold;
            FONT-SIZE: 10pt;
            COLOR: #000095;
            FONT-FAMILY: Verdana;
            HEIGHT: 19px;
        }

        .matters_print {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #000095;
        }

        .matters_heading {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 13px;
            font-weight: bold;
            color: #000095;
        }

        .matters_normal {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            color: #000095;
        }

        .matters_grid {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            color: #000095;
            TEXT-INDENT: 20px;
        }

        .matters_small {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 8px;
            color: #000095;
        }

        .Printbg {
            vertical-align: middle;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 8px;
            color: #000095;
        }

        .PrintSource {
            vertical-align: bottom;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 8px;
            color: #000095;
        }

        .style1 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            color: #000095;
            width: 50px;
        }

        .columnwidth {
            max-width: 15% !important;
            width: 25% !important;
        }

        .Firstcolumnwidth {
            max-width: 30% !important;
            width: 30% !important;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function PrintReceiptExport() {
            document.getElementById('tr_Print').style.display = 'none';
            window.print();
            document.getElementById('tr_Print').style.display = 'inline';
            try {
                if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
                    var ieversion = new Number(RegExp.$1) // capture x.x portion and store as a number
                    if (ieversion >= 8)
                        window.close();
                }
            }
            catch (ex) { }
        }
        function PrintReceipt() {
            if ('<%= Request.QueryString("isexport") %>' == '0')
                PrintReceiptExport();
        }
        function OnEscape() {
            if ((event.keyCode == 27) || (event.keyCode == 99) || (event.keyCode == 120))
                window.close();
        }
    </script>

</head>
<body onload="PrintReceipt();" onkeypress="OnEscape()" onkeydown="OnEscape()">
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="sm" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="80%">
            <tr valign="top" id="tr_Print">
                <td align="right">
                    <asp:UpdatePanel ID="UP1" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMessage" Text="" Font-Size="Small" ForeColor="red" runat="server"></asp:Label>
                            <asp:ImageButton ID="btnEmail" runat="server" AlternateText="Email" ToolTip="Email Receipt"
                                ImageUrl="~/Images/Misc/email.gif" />
                            <img src="../Images/Misc/print.gif" onclick="PrintReceiptExport();" alt="Print" style="cursor: hand" />
                            <asp:HiddenField ID="hf_FCLID" runat="server" />
                            <asp:HiddenField ID="hf_RecNo" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr valign="top">
                <td colspan="2" align="center" style="border-bottom: #000095 2pt solid">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" rowspan="4" width="10%">
                                <asp:Image ID="imgLogo" runat="server" />
                            </td>
                            <td width="90%" style="vertical-align: middle">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="matters_heading" align="center" style="height: 16px; padding-bottom: 2px; padding-top: 2px;">
                                            <asp:Label ID="lblSchool" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="trAddMain" runat="server">
                                        <td class="matters_print" align="center" style="height: 16px; padding-bottom: 2px; padding-top: 2px;">
                                            <asp:Label ID="lblAddressMain" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="matters_print" align="center" style="height: 16px; padding-bottom: 2px; padding-top: 2px;">
                                            <asp:Label ID="lblHeader1" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="matters_print" align="center" style="height: 16px; padding-bottom: 2px; padding-top: 2px;">
                                            <asp:Label ID="lblHeader2" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="matters_print" align="center" style="height: 16px; padding-bottom: 2px; padding-top: 2px;">
                                            <asp:Label ID="lblHeader3" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="center" colspan="1" style="height: 90px"></td>
            </tr>
            <tr>
                <td height="20px">&nbsp;
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                    <table align="center" border="0" cellspacing="0" cellpadding="2">
                        <tr>
                            <td align="center" class="ReceiptCaption">Receipt
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <!-- box starts here -->
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td width="16" valign="top">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="../Images/Misc/rounded1.gif" Width="16px"
                                                Height="16px" />
                                        </td>
                                        <td>
                                            <asp:Image ID="Image4" runat="server" ImageUrl="../Images/Misc/back1.gif" Width="100%"
                                                Height="16px" />
                                        </td>
                                        <td width="16" valign="top">
                                            <asp:Image ID="Image2" runat="server" ImageUrl="../Images/Misc/rounded2.gif" Width="16px"
                                                Height="16px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="left" style="height: 70px">
                                            <asp:Image ID="imgLogo1" runat="server" ImageUrl="../Images/Misc/back4.gif" Width="16px"
                                                Height="70px" />
                                        </td>
                                        <td>
                                            <!-- content starts here -->
                                            <table cellpadding="3" width="100%" border="0">
                                                <tr>
                                                    <td align="left" class="matters_normal" width="110">Receipt No
                                                    </td>
                                                    <td align="center" class="matters_print" style="width: 1px;">:
                                                    </td>
                                                    <td align="left" class="matters_print">
                                                        <asp:Label ID="lblRecno" runat="server"></asp:Label>
                                                        <asp:Label ID="lblCopy" runat="server" Font-Size="9pt" ForeColor="Red" Visible="False">[COPY]</asp:Label>
                                                    </td>
                                                    <td align="left" class="style1">Date
                                                    </td>
                                                    <td align="left" class="matters_print" style="width: 1px;">:
                                                    </td>
                                                    <td align="left" class="matters_normal" width="70">
                                                        <asp:Label ID="lblDate" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="matters_normal">
                                                        <asp:Label ID="lblStudentCaption" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="center" class="matters_print" style="width: 1px;">:
                                                    </td>
                                                    <td align="left" class="matters_print">
                                                        <asp:Label ID="lblStudentNo" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="left" class="style1"><asp:Label ID="lblGradeTitle" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="left" class="matters_print" style="width: 1px;">:
                                                    </td>
                                                    <td align="left" class="matters_normal">
                                                        <asp:Label ID="lblGrade" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="matters_normal">Name
                                                    </td>
                                                    <td align="center" class="matters_print" style="width: 1px;">:
                                                    </td>
                                                    <td align="left" class="matters_print">
                                                        <asp:Label ID="lblStudentName" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="left" class="style1">
                                                        <asp:Label ID="lblCurrencyLabel" runat="server" Visible="False">Currency</asp:Label>
                                                    </td>
                                                    <td align="left" class="matters_print" style="width: 1px;">
                                                        <asp:Label ID="lblCurrColon" runat="server" Visible="False">:</asp:Label>
                                                    </td>
                                                    <td align="left" class="matters_normal">
                                                        <asp:Label ID="lblCurrency" runat="server" Visible="False"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- content ends here -->
                                        </td>
                                        <td style="height: 70px" valign="top">
                                            <asp:Image ID="Image3" runat="server" ImageUrl="../Images/Misc/back2.gif" Width="16px"
                                                Height="70px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Image ID="Image5" runat="server" ImageUrl="../Images/Misc/rounded3.gif" Width="16px"
                                                Height="16px" />
                                        </td>
                                        <td>
                                            <asp:Image ID="Image7" runat="server" ImageUrl="../Images/Misc/back3.gif" Width="100%"
                                                Height="16px" />
                                        </td>
                                        <td>
                                            <asp:Image ID="Image6" runat="server" ImageUrl="../Images/Misc/rounded4.gif" Width="16px"
                                                Height="16px" />
                                        </td>
                                    </tr>
                                </table>
                                <!-- box ends here -->
                            </td>
                        </tr>
                        <tr class="ReceiptCaption">
                            <td style="height: 19px" align="left">Fee Details
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="matters_grid" valign="top" >
                                <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" BorderColor="#000095"
                                    BorderWidth="1pt" CellPadding="3" Width="99%" DataKeyNames="FCS_FEE_ID,FCA_bPDC_CANCELLED,FCA_PDC_CANCEL_AMOUNT,AMOUNT_DIFFERENCE">
                                    <RowStyle CssClass="rowheight" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Fee Head">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFEEDESCR" runat="server" Text='<%# Bind("FEE_DESCR") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="60%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFCSAmt" runat="server" Text='<%# Bind("FCS_AMOUNT", "{0:###,###,###,##0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" Width="20%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="FCS_CURRENCY_AMOUNT" DataFormatString="{0:###,###,###,##0.00}"
                                            HeaderText="Amount(FC)" Visible="False">
                                            <ItemStyle HorizontalAlign="Right" Width="20%" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="matters_print">
                                <asp:Label ID="lblBalance" runat="server"></asp:Label>&nbsp;
                            </td>
                        </tr>
                        <tr class="ReceiptCaption">
                            <td align="left" class="matters_print">Payment Details
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 10px;" class="matters_normal">
                                <asp:GridView ID="gvPayments" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                    BorderWidth="1pt" Width="99%" ShowFooter="True" DataKeyNames="FCD_bPDC_CANCELLED,FCD_PDC_CANCELLED_DATE,AMOUNT_DIFFERENCE">
                                    <Columns>
                                        <asp:BoundField DataField="CLT_DESCR" HeaderText="Payment Mode">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" CssClass="Firstcolumnwidth" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CRI_DESCR" HeaderText="Bank/Credit Card">
                                            <ItemStyle HorizontalAlign="Left" CssClass="columnwidth" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FCD_REFNO" HeaderText="ChqNo./AuthCode/Vouch.No">
                                            <ItemStyle HorizontalAlign="Left" CssClass="columnwidth" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FCD_DATE" HeaderText="Cheque Date">
                                            <ItemStyle HorizontalAlign="Left" CssClass="columnwidth" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FCD_AMOUNT" DataFormatString="{0:###,###,###,##0.00}"
                                            HeaderText="Amount">
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" CssClass="columnwidth" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FCD_CURRENCY_AMOUNT" DataFormatString="{0:###,###,###,##0.00}"
                                            HeaderText="Amount(FC)" Visible="False">
                                            <ItemStyle HorizontalAlign="Right" CssClass="columnwidth" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 10px;" class="matters_normal">
                                <asp:Label ID="lblNarration" runat="server"></asp:Label><br />
                                <br />
                                <br />

                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 10px;" class="matters_normal">
                                <asp:Label ID="lblSchoolName" runat="server"></asp:Label><br />
                                <br />
                                <br />
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 30px;" class="matters_normal">------------------------------
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 60px; height: 20px" class="matters_normal">Cashier/Accountant
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 10px; height: 25px" class="matters_normal">Payment by Cheque(s) are subject to realization.
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 10px; height: 25px" class="matters_normal">
                                <asp:Label ID="lblSchoolNameCheque" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 9px; height: 25px" class="matters_normal">Please retain this receipt for refund if any.<br />
                                <br />
                                In case of any refunds, the official refund policy will be applied and the discount
                                already granted will be adjusted accordingly. In case of any fee refunds for payments
                                made through Credit card(s), applicable bank charges will be deducted while processing
                                such claims.
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" style="text-indent: 10px; height: 25px" class="matters_normal">
                                <br />
                                <br />
                                <br />
                                <br />
                                Thank you for choosing &nbsp;<asp:Label ID="lblSchoolnameFavour" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" style="text-indent: 10px; height: 25px" class="matters_normal">We value your patronage
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" style="text-indent: 10px; height: 25px" class="matters_normal">
                                <asp:Label ID="lblDiscount" runat="server" Text="The discounted fee structure is applicable on the existing fee structure of the School.&lt;br /&gt;Any  revision in fee for the period if approved by the regulatory authority later, the difference shall be payable by the parent"
                                    Visible="false"> </asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" class="Printbg" valign="middle">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%">
                        <tr>
                            <td align="center" class="PrintSource" valign="top">Source: GEMS PHOENIX
                            </td>
                            <td valign="top" align="right">&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" valign="bottom" class="matters_normal">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-top: #003A63 1px solid; width: 100%; height: 100%">
                        <tr>
                            <td align="left" class="PrintSource" valign="top">
                                <asp:Label ID="lbluserloggedin" runat="server" CssClass="matters_small"></asp:Label>
                            </td>
                            <td valign="top" align="right" class="PrintSource">
                                <asp:Label ID="lblPrintTime" runat="server" CssClass="matters_small"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="h_print" runat="server" />

    </form>
</body>
</html>
