<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeConcession.aspx.vb" Inherits="Fees_FeeConcession" title="Untitled Page" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>  
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language ="javascript">      
function gettxtFEE_CONCESSION_ACT_ID()
{
    <%--popUpAccount('460','400','CONCESSION','<%=txtDebitAccName.ClientId %>')
 }--%>
    var ShowType = "CONCESSION";
    var url = "PopUp.aspx?id=EN&ShowType=" + ShowType;
    var oWnd = radopen(url, "pop_gettaxfee");

}

function OnClientClose1(oWnd, args) {
    //get the transferred arguments

    var arg = args.get_argument();

    if (arg) {

        NameandCode = arg.NameandCode.split('||');
        document.getElementById('<%=h_ACC_ID.ClientID %>').value = NameandCode[1];
        document.getElementById('<%=txtDebitAccName.ClientID%>').value = NameandCode[0];
        __doPostBack('<%=txtDebitAccName.ClientID%>', 'TextChanged');
            }
        }



    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_gettaxfee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> <asp:Label id="lblHeader" runat="server" Text="Fee Concession Master"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <%--<asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
        SkinID="Error"></asp:Label>--%>
    <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="FEE_CONC" />
    <table align="center" width="100%">
        
        <tr>
            <td align="left" width="20%">
                <span class="field-label">Description</span></td>
           
            <td align="left" width="30%">
                <asp:TextBox id="txtConcDescr" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Description required" ControlToValidate="txtConcDescr" ValidationGroup="FEE_CONC">*</asp:RequiredFieldValidator></td>
        
            <td align="left" width="20%">
                <span class="field-label">Concession Type</span></td>
            
            <td align="left" width="30%">
                <asp:DropDownList id="ddlConcessionType" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Account(D.R)</span></td>
            
            <td align="left">
                <asp:TextBox id="txtDebitAccName" runat="server"></asp:TextBox>
                <asp:ImageButton id="imgAccount" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="gettxtFEE_CONCESSION_ACT_ID(); return false;">
                </asp:ImageButton>
             <ajaxToolkit:AutoCompleteExtender ID="acFEE_DEBIT_ACC" runat="server" BehaviorID="AutoCompleteEx1"
                 CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement"
                 CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" CompletionListItemCssClass="autocomplete_listItem"
                 CompletionSetCount="19" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                 ServiceMethod="GetCompletionList" ServicePath="~/WebServices/AutoComplete.asmx"
                 TargetControlID="txtDebitAccName">
                 <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
             </ajaxToolkit:AutoCompleteExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDebitAccName"
                    ErrorMessage="Please Select an Account" ValidationGroup="FEE_CONC">*</asp:RequiredFieldValidator>
            </td>
        
            <td align="left">
                <span class="field-label">Business Unit(D.R)</span></td>
            
            <td align="left" >
                <asp:DropDownList id="ddlBusinessUnit" runat="server">
                </asp:DropDownList></td>
            
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" OnClick="btnAdd_Click" />
                <asp:Button id="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" OnClick="btnEdit_Click" />
                <asp:Button id="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" ValidationGroup="FEE_CONC" />
                <asp:Button id="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" />
                <asp:Button id="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" OnClick="btnCancel_Click" /></td>
        </tr>
    </table>
    <asp:HiddenField id="h_ACC_ID" runat="server">
    </asp:HiddenField>
    <asp:HiddenField id="h_FCM_ID" runat="server">
    </asp:HiddenField>

                </div>
            </div>
        </div>

</asp:Content>

