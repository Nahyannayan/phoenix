<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeCounterView.aspx.vb" Inherits="FeeProcessFlowView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Fee Counter ....
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr valign="top">
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="center" valign="top" width="100%">
                            <asp:GridView ID="gvJournal" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30">
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="COUNTER_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFCM_ID" runat="server" Text='<%# Bind("FCM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Unit">
                                        <HeaderTemplate>
                                            Business Unit<br />
                                            <asp:TextBox ID="txtBSUNAME" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                                        
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Counter_Descr">
                                        <HeaderTemplate>
                                            Counter<br />
                                            <asp:TextBox ID="txtCounterDescr" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("FCM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User name">
                                        <HeaderTemplate>
                                           User<br />
                                           <asp:TextBox ID="txtUser" runat="server" Width="75%"></asp:TextBox>
                                           <asp:ImageButton ID="btnNarration" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("USR_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>
