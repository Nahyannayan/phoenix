Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_rptDayenddocuments
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        h_DocNo.Value = Request.QueryString("DOCNO")
        h_DocDate.Value = Request.QueryString("DOCDATE")

        If Session("sUsr_name") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If
        Select Case h_DocNo.Value.Substring(2, 2).ToString()
            Case "JV"
                Session("ReportSource") = VoucherReports.JournalVouchers(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "JV", h_DocNo.Value, Session("HideCC"))
                Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
            Case "PP"
                Session("ReportSource") = VoucherReports.PrePaymentVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "PP", h_DocNo.Value, Request.QueryString("GUID"), Session("HideCC"))
                Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
            Case "IJ"
                Session("ReportSource") = VoucherReports.InterUnitVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "IJV", h_DocNo.Value, True, Session("HideCC"))
                Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
            Case "BP"
                Session("ReportSource") = VoucherReports.BankPaymentVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BP", h_DocNo.Value, False, Session("HideCC"))
                Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
            Case "BR"
                Session("ReportSource") = VoucherReports.BankReceiptVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "BR", h_DocNo.Value, Session("HideCC"))
                Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
            Case "CC"
                Session("ReportSource") = AccountsReports.CreditCard(h_DocNo.Value, h_DocDate.Value, Session("sBsuid"), Session("F_YEAR"))
                Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
            Case "CR"
                Session("ReportSource") = VoucherReports.CashReceiptVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "CR", h_DocNo.Value, Session("HideCC"))
                Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
            Case "CP"
                Session("ReportSource") = VoucherReports.CashPaymentVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "CP", h_DocNo.Value, Session("HideCC"))
                Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
            Case "DN"
                Session("ReportSource") = VoucherReports.DebitCreditNoteVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), h_DocNo.Value.Substring(2, 2).ToString(), h_DocNo.Value, Session("HideCC"))
                Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
            Case "IC"
                Session("ReportSource") = VoucherReports.InternetCollectionVoucher(Session("sBsuid"), Session("F_YEAR"), Session("SUB_ID"), "IC", h_DocNo.Value, Session("HideCC"))
                Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
                'Session("ReportSource") = AccountsReports.JournalVouchers(h_DocNo.Value, h_DocDate.Value, Session("sBsuID"), Session("F_YEAR"), "", "PP")
                'Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
            Case "LE"
                'Dim Fdate As String = Request.QueryString("FDATE")
                'Dim Tdate As String = Request.QueryString("TDATE")
                'Dim UserName As String = Request.QueryString("USERNAME")
                'Dim RoleId As String = "0" 'Request.QueryString("ROLEID")
                'Session("ReportSource") = AccountsReports.UserLoggedDetails(RoleId, UserName, Fdate, Tdate)
                'Response.Redirect("../Reports/ASPX Report/RptViewerModalview.aspx")
            Case Else
                ' Session("ReportSource") = AccountsReports.BankPayment(h_DocNo.Value, h_DocDate.Value, Session("sBsuID"), Session("F_YEAR"))
                Response.Redirect("../Reports/ASPX Report/RptViewerModal.aspx")
        End Select
    End Sub
    
End Class
