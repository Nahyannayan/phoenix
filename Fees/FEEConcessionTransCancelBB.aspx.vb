Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FEEConcessionTransCancelBB
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            txtStud_Name.Attributes.Add("ReadOnly", "ReadOnly")
            txtTotalFee.Attributes.Add("ReadOnly", "ReadOnly")
            gvFeeDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvMonthly.Attributes.Add("bordercolor", "#1b80b6")
            txtRefIDsub.Attributes.Add("ReadOnly", "ReadOnly")
            txtRefHEAD.Attributes.Add("ReadOnly", "ReadOnly")
            txtConcession_Head.Attributes.Add("ReadOnly", "ReadOnly")
            txtConcession_Det.Attributes.Add("ReadOnly", "ReadOnly")

            gvFeeDetails.DataBind()
            gvMonthly.DataBind()
            ddlBusinessunit.DataBind()
            If Not ddlBusinessunit.Items.FindByValue(Session("PROVIDER_BSU_ID")) Is Nothing Then
                ddlBusinessunit.ClearSelection()
                ddlBusinessunit.Items.FindByValue(Session("PROVIDER_BSU_ID")).Selected = True
            End If
            FillACD()
            Dim CurBsUnit As String = ddlBusinessunit.SelectedItem.Value
            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_CANCEL And _
            ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION_CANCEL_APPROVAL) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                radAmount.Checked = True
                ddlFeeType.DataBind()
                setAcademicyearDate()
            End If
            If ViewState("datamode") = "view" Then
                Dim FCH_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FCH_ID").Replace(" ", "+"))
                'h_FCH_ID.Value = FCH_ID
                If Request.QueryString("bsu") <> "" Then
                    ddlBusinessunit.SelectedIndex = -1
                    ddlBusinessunit.Items.FindByValue(Request.QueryString("bsu")).Selected = True
                End If
                ViewState("FCH_ID") = FCH_ID
                set_ViewData(FCH_ID, False)
                h_FCH_ID.Value = FCH_ID
            Else
                ViewState("FCH_ID") = Nothing
                Session("FEE_CONS_TRAN") = Nothing
            End If
            lblAlert.Text = getErrorMessage("642")
        End If
    End Sub

    Private Sub DissableControls(ByVal dissable As Boolean)
        txtDate.Enabled = Not dissable
        txtFromDT.Enabled = Not dissable
        txtToDT.Enabled = Not dissable
        txtRefHEAD.ReadOnly = dissable
        txtRemarks.ReadOnly = dissable
        txtAmount.ReadOnly = dissable
        txtRefIDsub.ReadOnly = dissable

        imgCompany.Enabled = Not dissable
        Image1.Enabled = Not dissable
        imgRefSub.Enabled = Not dissable
        imgRefHead.Enabled = Not dissable
        imgToDT.Enabled = Not dissable
        ImagefromDate.Enabled = Not dissable

        ddlAcademicYear.Enabled = Not dissable
        ddlFeeType.Enabled = Not dissable
        radAmount.Enabled = Not dissable
        radPercentage.Enabled = Not dissable
        'gvFeeDetails.Columns(6).Visible = Not dissable

        btnDetAdd.Enabled = Not dissable
        btnDetCancel.Enabled = Not dissable

        ddlBusinessunit.Enabled = Not dissable
        txtStdNo.Enabled = Not dissable
        txtConcession.Enabled = Not dissable
        txtLocation.Enabled = Not dissable
    End Sub

    Protected Sub btnDetAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetAdd.Click
        AddUpdateDetails()
    End Sub

    Sub AddUpdateDetails()
        If btnDetAdd.Text = "Add" Then
            Exit Sub
        End If
        If h_FCT_ID_DET.Value = "" Then
            '   lblError.Text = "Please select concession type!!!"
            usrMessageBar.ShowNotification("Please select concession type!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        'Select Case h_FCT_ID_DET.Value
        '    Case 1, 2
        '        If H_REFID_SUB.Value = "" Then
        '            lblError.Text = "Please specify the Reference No"
        '            Return
        '        ElseIf Not FEEConcessionTransactionBB.ValidateReference(h_FCT_ID_DET.Value, H_REFID_SUB.Value, ddlBusinessunit.SelectedItem.Value) Then
        '            lblError.Text = "The Reference No is not Valid "
        '            Return
        '        Else
        '            lblError.Text = ""
        '        End If
        'End Select
        Dim vFEE_CON As FEEConcessionTransactionBB
        vFEE_CON = Session("FEE_CONS_TRAN")
        If vFEE_CON Is Nothing Then
            vFEE_CON = New FEEConcessionTransactionBB
        End If
        Dim str_Stu_Join_Date As String = FeeCommon.GetStudentJoinDate(ddlBusinessunit.SelectedItem.Value, h_STUD_ID.Value, True)
        Dim dt_Join_Date, dt_From_Date As DateTime

        If IsDate(txtDate.Text) Then
            dt_From_Date = CDate(txtDate.Text)
        Else
            'lblError.Text = "Invalid document date!!!"
            usrMessageBar.ShowNotification("Invalid document date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        If IsDate(str_Stu_Join_Date) Then
            dt_Join_Date = CDate(str_Stu_Join_Date)
            If dt_From_Date < dt_Join_Date Then
                '  lblError.Text = "Document date is less than Student join date!!!"
                usrMessageBar.ShowNotification("Document date is less than Student join date!!!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
        Else
            ' lblError.Text = "Invalid Student Join Date!!!"
            usrMessageBar.ShowNotification("Invalid Student Join Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        For Each gvr As GridViewRow In gvMonthly.Rows
            'Get a programmatic reference to the CheckBox control
            Dim lblId As Label = CType(gvr.FindControl("lblId"), Label)
            Dim txtChargeDate As TextBox = CType(gvr.FindControl("txtChargeDate"), TextBox)
            Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)
            Dim dblActualAmount As Decimal = gvr.Cells(3).Text
            If Not txtAmount Is Nothing Then
                If Not IsNumeric(txtAmount.Text) Or Not IsDate(txtChargeDate.Text) Then
                    '   lblError.Text = "Invalid Amount/Date!!!"
                    usrMessageBar.ShowNotification("Invalid Amount/Date!!!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                Else
                    If dblActualAmount < Convert.ToDecimal(txtAmount.Text) Then
                        'lblError.Text = "Concession exceeds actual amount !!!"
                        usrMessageBar.ShowNotification("Concession exceeds actual amount !!!", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                End If
            End If
        Next
        If Not FEEConcessionTransactionBB.DuplicateFeeType(vFEE_CON.SUB_DETAILS, ddlFeeType.SelectedValue, Session("FEE_CON_EID")) Then
            If btnDetAdd.Text = "Update" Then
                vFEE_CON.SUB_DETAILS = EditSubDetails(Session("FEE_CON_EID"), vFEE_CON.SUB_DETAILS)
                btnDetAdd.Text = "Add"
                Session("FEE_CONS_TRAN") = vFEE_CON
                Session("FEE_CON_EID") = Nothing
            Else
                AddDetails()
            End If
            ClearSubDetails()
        Else
            '  lblError.Text = "Cannot repeat Fee Concession Type"
            usrMessageBar.ShowNotification("Cannot repeat Fee Concession Type", UserControls_usrMessageBar.WarningType.Danger)
            Return
        End If
        GridBind()
    End Sub

    Private Function AddUpdateDetailsInSave() As String
        If btnDetAdd.Text = "Add" Then
            Return "Add Button!!!"
        End If
        If h_FCT_ID_DET.Value = "" Then
            '   lblError.Text = "Please select concession type!!!"
            'usrMessageBar.ShowNotification("Please select concession type!!!", UserControls_usrMessageBar.WarningType.Danger)
            Return "Please select concession type!!!"
        End If
        'Select Case h_FCT_ID_DET.Value
        '    Case 1, 2
        '        If H_REFID_SUB.Value = "" Then
        '            lblError.Text = "Please specify the Reference No"
        '            Return
        '        ElseIf Not FEEConcessionTransactionBB.ValidateReference(h_FCT_ID_DET.Value, H_REFID_SUB.Value, ddlBusinessunit.SelectedItem.Value) Then
        '            lblError.Text = "The Reference No is not Valid "
        '            Return
        '        Else
        '            lblError.Text = ""
        '        End If
        'End Select
        Dim vFEE_CON As FEEConcessionTransactionBB
        vFEE_CON = Session("FEE_CONS_TRAN")
        If vFEE_CON Is Nothing Then
            vFEE_CON = New FEEConcessionTransactionBB
        End If
        Dim str_Stu_Join_Date As String = FeeCommon.GetStudentJoinDate(ddlBusinessunit.SelectedItem.Value, h_STUD_ID.Value, True)
        Dim dt_Join_Date, dt_From_Date As DateTime

        If IsDate(txtDate.Text) Then
            dt_From_Date = CDate(txtDate.Text)
        Else
            'lblError.Text = "Invalid document date!!!"
            'usrMessageBar.ShowNotification("Invalid document date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Return "Invalid document date!!!"
        End If

        If IsDate(str_Stu_Join_Date) Then
            dt_Join_Date = CDate(str_Stu_Join_Date)
            If dt_From_Date < dt_Join_Date Then
                '  lblError.Text = "Document date is less than Student join date!!!"
                'usrMessageBar.ShowNotification("Document date is less than Student join date!!!", UserControls_usrMessageBar.WarningType.Danger)
                Return "Document date is less than Student join date!!!"
            End If
        Else
            ' lblError.Text = "Invalid Student Join Date!!!"
            'usrMessageBar.ShowNotification("Invalid Student Join Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Return "Invalid Student Join Date!!!"
        End If
        For Each gvr As GridViewRow In gvMonthly.Rows
            'Get a programmatic reference to the CheckBox control
            Dim lblId As Label = CType(gvr.FindControl("lblId"), Label)
            Dim txtChargeDate As TextBox = CType(gvr.FindControl("txtChargeDate"), TextBox)
            Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)
            Dim dblActualAmount As Decimal = gvr.Cells(3).Text
            If Not txtAmount Is Nothing Then
                If Not IsNumeric(txtAmount.Text) Or Not IsDate(txtChargeDate.Text) Then
                    '   lblError.Text = "Invalid Amount/Date!!!"
                    'usrMessageBar.ShowNotification("Invalid Amount/Date!!!", UserControls_usrMessageBar.WarningType.Danger)
                    Return "Invalid Amount/Date!!!"
                Else
                    If dblActualAmount < Convert.ToDecimal(txtAmount.Text) Then
                        'lblError.Text = "Concession exceeds actual amount !!!"
                        'usrMessageBar.ShowNotification("Concession exceeds actual amount !!!", UserControls_usrMessageBar.WarningType.Danger)
                        Return "Concession exceeds actual amount !!!"
                    End If
                End If
            End If
        Next
        If Not FEEConcessionTransactionBB.DuplicateFeeType(vFEE_CON.SUB_DETAILS, ddlFeeType.SelectedValue, Session("FEE_CON_EID")) Then
            If btnDetAdd.Text = "Update" Then
                vFEE_CON.SUB_DETAILS = EditSubDetails(Session("FEE_CON_EID"), vFEE_CON.SUB_DETAILS)
                btnDetAdd.Text = "Add"
                Session("FEE_CONS_TRAN") = vFEE_CON
                Session("FEE_CON_EID") = Nothing
            Else
                AddDetails()
            End If
            ClearSubDetails()
        Else
            '  lblError.Text = "Cannot repeat Fee Concession Type"
            'usrMessageBar.ShowNotification("Cannot repeat Fee Concession Type", UserControls_usrMessageBar.WarningType.Danger)
            Return "Cannot repeat Fee Concession Type"
        End If
        GridBind()
        Return ""
    End Function

    Private Function EditSubDetails(ByVal EID As Integer, ByVal arrSUBList As ArrayList) As ArrayList
        If arrSUBList Is Nothing Then
            Return New ArrayList
        End If
        Dim vFEE_CON_SUB As FEE_CONC_TRANC_SUB_BB
        For i As Integer = 0 To arrSUBList.Count - 1
            vFEE_CON_SUB = arrSUBList(i)
            If vFEE_CON_SUB.FCD_ID = EID Then
                EditSubDetails_Monthly(vFEE_CON_SUB.SubList_Monthly)
                arrSUBList.Remove(vFEE_CON_SUB)
                vFEE_CON_SUB.FCD_AMOUNT = CDbl(txtAmount.Text)
                If radAmount.Checked Then
                    vFEE_CON_SUB.FCD_AMTTYPE = 1
                ElseIf radPercentage.Checked Then
                    vFEE_CON_SUB.FCD_AMTTYPE = 2
                End If
                vFEE_CON_SUB.FCD_ID = EID
                vFEE_CON_SUB.FCD_FCM_ID = h_FCM_ID_DET.Value
                vFEE_CON_SUB.FCD_FEE_ID = ddlFeeType.SelectedValue
                If txtRefIDsub.Text <> "" Then vFEE_CON_SUB.FCD_REF_ID = CInt(H_REFID_SUB.Value) _
                Else vFEE_CON_SUB.FCD_REF_ID = 0
                vFEE_CON_SUB.FEE_DESCR = ddlFeeType.SelectedItem.Text
                vFEE_CON_SUB.FCM_DESCR = txtConcession_Det.Text
                arrSUBList.Add(vFEE_CON_SUB)

                EditSubDetails_Monthly(vFEE_CON_SUB.SubList_Monthly)
                Exit For
            End If
        Next
        Return arrSUBList
    End Function

    Private Function EditSubDetails_Monthly(ByVal arrSUBListMonthly As ArrayList) As ArrayList
        If arrSUBListMonthly Is Nothing Then
            Return New ArrayList
        End If
        Dim vFEE_CON_SUB_Monthly As FEE_CONC_TRANC_SUB_MONTHLY_BB
        For Each gvr As GridViewRow In gvMonthly.Rows
            'Get a programmatic reference to the CheckBox control
            Dim lblId As Label = CType(gvr.FindControl("lblId"), Label)
            Dim txtChargeDate As TextBox = CType(gvr.FindControl("txtChargeDate"), TextBox)
            Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)

            If Not txtAmount Is Nothing Then
                If Not IsNumeric(txtAmount.Text) Or Not IsDate(txtChargeDate.Text) Then
                    ' lblError.Text = "Invalid Amount/Date!!!"
                    usrMessageBar.ShowNotification("Invalid Amount/Date!!!", UserControls_usrMessageBar.WarningType.Danger)
                    Return arrSUBListMonthly
                End If
                For i As Integer = 0 To arrSUBListMonthly.Count - 1
                    vFEE_CON_SUB_Monthly = arrSUBListMonthly(i)
                    If lblId.Text = vFEE_CON_SUB_Monthly.FMD_ID Then
                        vFEE_CON_SUB_Monthly.FMD_AMOUNT = CDbl(txtAmount.Text)
                        vFEE_CON_SUB_Monthly.FMD_ORG_AMOUNT = CDbl(gvr.Cells(3).Text)
                        vFEE_CON_SUB_Monthly.FMD_DATE = txtChargeDate.Text
                        Exit For
                    End If
                Next
            End If
        Next
        Return arrSUBListMonthly
    End Function

    Private Function AlertForRefRepetition(ByVal REF_TYP As ConcessionType, ByVal REF_ID As Integer) As Boolean
        Return FEEConcessionTransactionBB.RefRepetition(REF_TYP, REF_ID)
    End Function

    Private Sub GridBind()
        Dim vFEE_CON As FEEConcessionTransactionBB = Session("FEE_CONS_TRAN")
        Dim dttab As New DataTable
        If Not vFEE_CON Is Nothing Then
            dttab = GetDataTableFromObjects(vFEE_CON.SUB_DETAILS)
        End If
        gvFeeDetails.DataSource = dttab
        gvFeeDetails.DataBind()
    End Sub

    Private Function GetDataTableFromObjects(ByVal arrList As ArrayList) As DataTable
        If arrList Is Nothing Then Return Nothing
        Dim dtDt As DataTable = CreateDataTableForFeeDetails()
        For i As Integer = 0 To arrList.Count - 1
            Dim vFEE_CON As FEE_CONC_TRANC_SUB_BB = arrList(i)
            Dim dr As DataRow = dtDt.NewRow()
            dr("FCD_ID") = vFEE_CON.FCD_ID
            dr("FCM_DESCR") = vFEE_CON.FCM_DESCR
            dr("FEE_DESCR") = vFEE_CON.FEE_DESCR
            dr("AMT_TYPE") = IIf(vFEE_CON.FCD_AMTTYPE = 1, "Amount", "Percentage")
            dr("AMOUNT") = AccountFunctions.Round2(vFEE_CON.FCD_AMOUNT, 2)
            dtDt.Rows.Add(dr)
        Next
        Return dtDt
    End Function

    Private Function CreateDataTableForFeeDetails() As DataTable
        Dim dtDt As New DataTable()
        Dim dcFCD_ID As New DataColumn("FCD_ID", System.Type.[GetType]("System.Int32"))
        Dim dcFCM_DESCR As New DataColumn("FCM_DESCR", System.Type.[GetType]("System.String"))
        Dim dcFEE_DESCR As New DataColumn("FEE_DESCR", System.Type.[GetType]("System.String"))
        Dim dcAMT_TYPE As New DataColumn("AMT_TYPE", System.Type.[GetType]("System.String"))
        Dim dcAMOUNT As New DataColumn("AMOUNT", System.Type.[GetType]("System.Decimal"))

        dtDt.Columns.Add(dcFCD_ID)
        dtDt.Columns.Add(dcFCM_DESCR)
        dtDt.Columns.Add(dcFEE_DESCR)
        dtDt.Columns.Add(dcAMT_TYPE)
        dtDt.Columns.Add(dcAMOUNT)
        Return dtDt
    End Function

    Private Sub AddDetails()
        If Session("FEE_CONS_TRAN") Is Nothing Then
            Session("FEE_CONS_TRAN") = New FEEConcessionTransactionBB
        End If
        Dim vFEE_CON As FEEConcessionTransactionBB = Session("FEE_CONS_TRAN")
        Dim vFEE_CON_SUB As New FEE_CONC_TRANC_SUB_BB
        vFEE_CON_SUB.FCD_AMOUNT = CDbl(txtAmount.Text)

        If radAmount.Checked Then
            vFEE_CON_SUB.FCD_AMTTYPE = 1
        ElseIf radPercentage.Checked Then
            vFEE_CON_SUB.FCD_AMTTYPE = 2
        End If
        vFEE_CON_SUB.FCD_ID = GETNEXTFCD_ID(vFEE_CON.SUB_DETAILS)
        vFEE_CON_SUB.FCD_FCM_ID = h_FCM_ID_DET.Value
        vFEE_CON_SUB.FCD_FEE_ID = ddlFeeType.SelectedValue
        'vFEE_CON_SUB.FCD_SCH_ID = ViewState("SCH_ID")
        If CInt(h_FCT_ID_DET.Value) = ConcessionType.Staff Then
            vFEE_CON_SUB.FCD_REF_BSU_ID = FEEConcessionTransactionBB.GetEmployeeBSU(H_REFID_SUB.Value, H_REFID_HEAD.Value)
        End If

        If txtRefIDsub.Text <> "" Then vFEE_CON_SUB.FCD_REF_ID = CInt(H_REFID_SUB.Value) _
        Else vFEE_CON_SUB.FCD_REF_ID = 0
        vFEE_CON_SUB.FEE_DESCR = ddlFeeType.SelectedItem.Text
        vFEE_CON_SUB.FCM_DESCR = txtConcession_Det.Text
        If (vFEE_CON.SUB_DETAILS Is Nothing) Then
            vFEE_CON.SUB_DETAILS = New ArrayList
        End If
        vFEE_CON_SUB = AddDetails_Monthly(vFEE_CON_SUB, vFEE_CON_SUB.FCD_ID)
        vFEE_CON.SUB_DETAILS.Add(vFEE_CON_SUB)
        Session("FEE_CONS_TRAN") = vFEE_CON
    End Sub

    Private Function AddDetails_Monthly(ByVal vFEE_CON_SUB As FEE_CONC_TRANC_SUB_BB, ByVal vFMD_FCD_ID As Integer) As FEE_CONC_TRANC_SUB_BB
        For Each gvr As GridViewRow In gvMonthly.Rows
            Dim vFEE_CON_Monthly As New FEE_CONC_TRANC_SUB_MONTHLY_BB
            'Get a programmatic reference to the CheckBox control
            Dim lblId As Label = CType(gvr.FindControl("lblId"), Label)
            Dim txtChargeDate As TextBox = CType(gvr.FindControl("txtChargeDate"), TextBox)
            Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)
            If Not txtAmount Is Nothing Then
                vFEE_CON_Monthly.FMD_AMOUNT = Convert.ToDecimal(txtAmount.Text)
                vFEE_CON_Monthly.FMD_ORG_AMOUNT = CDbl(gvr.Cells(3).Text)
                vFEE_CON_Monthly.FMD_DATE = CDate(txtChargeDate.Text)
                vFEE_CON_Monthly.FMD_FCD_ID = vFMD_FCD_ID
                vFEE_CON_Monthly.FMD_REF_ID = lblId.Text
            End If
            If vFEE_CON_SUB.SubList_Monthly Is Nothing Then
                vFEE_CON_SUB.SubList_Monthly = New ArrayList
            End If
            vFEE_CON_SUB.SubList_Monthly.Add(vFEE_CON_Monthly)
        Next
        Return vFEE_CON_SUB
    End Function

    Private Function GETNEXTFCD_ID(ByVal arrSUBList As ArrayList) As Integer
        Dim vFEE_CON_SUB As New FEE_CONC_TRANC_SUB_BB
        Dim FCD_MAX_ID As Integer = 0
        If arrSUBList Is Nothing OrElse arrSUBList.Count <= 0 Then
            Return 1
        End If
        For i As Integer = 0 To arrSUBList.Count - 1
            vFEE_CON_SUB = arrSUBList(i)
            If vFEE_CON_SUB.FCD_ID > FCD_MAX_ID Then
                FCD_MAX_ID = vFEE_CON_SUB.FCD_ID
            End If
        Next
        Return FCD_MAX_ID + 1
    End Function

    Private Function GETNEXTFMD_ID(ByVal arrSUBList As ArrayList) As Integer
        Dim vFEE_CON_SUB_MONTHLY As New FEE_CONC_TRANC_SUB_MONTHLY_BB
        Dim FCD_MAX_ID As Integer = 0
        If arrSUBList Is Nothing OrElse arrSUBList.Count <= 0 Then
            Return 1
        End If
        For i As Integer = 0 To arrSUBList.Count - 1
            vFEE_CON_SUB_MONTHLY = arrSUBList(i)
            If vFEE_CON_SUB_MONTHLY.FMD_ID > FCD_MAX_ID Then
                FCD_MAX_ID = vFEE_CON_SUB_MONTHLY.FMD_ID
            End If
        Next
        Return FCD_MAX_ID + 1
    End Function

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblFCD_ID As New Label
        lblFCD_ID = TryCast(sender.parent.FindControl("lblFCD_ID"), Label)
        SetMonthlyDataEdit(lblFCD_ID.Text)
    End Sub

    Sub SetMonthlyDataEdit(ByVal FCD_ID As String)
        Dim vFEE_CON As FEEConcessionTransactionBB = Session("FEE_CONS_TRAN")
        Dim vFEE_CON_SUB As FEE_CONC_TRANC_SUB_BB
        Dim arrSUBDET As ArrayList = vFEE_CON.SUB_DETAILS
        For i As Integer = 0 To arrSUBDET.Count
            vFEE_CON_SUB = arrSUBDET(i)
            If vFEE_CON_SUB.FCD_ID = FCD_ID Then
                GetMonthlyList(vFEE_CON_SUB.SubList_Monthly)
                Session("FEE_CON_EID") = FCD_ID
                ddlFeeType.SelectedIndex = -1
                ddlFeeType.Items.FindByValue(vFEE_CON_SUB.FCD_FEE_ID).Selected = True
                txtAmount.Text = vFEE_CON_SUB.FCD_AMOUNT
                H_REFID_SUB.Value = vFEE_CON_SUB.FCD_REF_ID
                txtConcession_Det.Text = UtilityObj.GetDataFromSQL("SELECT FCM_DESCR FROM " _
                & " FEES.FEE_CONCESSION_M WHERE FCM_ID =" & h_FCM_ID_HEAD.Value, WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
                h_FCT_ID_DET.Value = UtilityObj.GetDataFromSQL("SELECT FCM_FCT_ID FROM " _
                & " FEES.FEE_CONCESSION_M WHERE FCM_ID =" & h_FCM_ID_HEAD.Value, WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
                h_FCM_ID_DET.Value = vFEE_CON_SUB.FCD_FCM_ID
                txtRefIDsub.Text = GetReference(h_FCT_ID_DET.Value, vFEE_CON_SUB.FCD_REF_ID, ddlBusinessunit.SelectedItem.Value)
                Select Case vFEE_CON_SUB.FCD_AMTTYPE
                    Case 1
                        radAmount.Checked = True
                        radPercentage.Checked = False
                    Case 2
                        radAmount.Checked = False
                        radPercentage.Checked = True
                End Select
                btnDetAdd.Text = "Update"
                Exit For
            End If
        Next
    End Sub

    Private Sub GetMonthlyList(ByVal arrSubList_Monthly As ArrayList)
        Dim dt As New DataTable
        dt = FEE_CONC_TRANC_SUB_MONTHLY_BB.CreateDataTableFeeConcessionMonthly
        For Each vSubMonthly As FEE_CONC_TRANC_SUB_MONTHLY_BB In arrSubList_Monthly
            Dim dr As DataRow
            dr = dt.NewRow
            dr("id") = vSubMonthly.FMD_ID
            dr("DESCR") = vSubMonthly.FMD_REF_NAME
            dr("REF_ID") = vSubMonthly.FMD_REF_ID
            dr("FDD_DATE") = vSubMonthly.FMD_DATE
            dr("CUR_AMOUNT") = vSubMonthly.FMD_AMOUNT
            dr("FDD_AMOUNT") = vSubMonthly.FMD_ORG_AMOUNT

            'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT
            dt.Rows.Add(dr)
        Next
        Session("tempMonthly") = dt
        gvMonthly.DataSource = dt
        gvMonthly.DataBind()
    End Sub

    Protected Sub btnDetCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetCancel.Click
        ClearSubDetails()
    End Sub

    Private Sub ClearSubDetails()
        txtRefIDsub.Text = ""
        txtAmount.Text = ""
        btnDetAdd.Text = "Add"
        Session("FEE_CON_EID") = Nothing
        gvMonthly.DataBind()
    End Sub

    Protected Sub lnkBtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblFCD_ID As New Label
        lblFCD_ID = TryCast(sender.parent.FindControl("lblFCD_ID"), Label)

        Dim vFEE_CON As FEEConcessionTransactionBB = Session("FEE_CONS_TRAN")
        Dim vFEE_CON_SUB As FEE_CONC_TRANC_SUB_BB
        Dim arrSUBDET As ArrayList = vFEE_CON.SUB_DETAILS
        For i As Integer = 0 To arrSUBDET.Count
            vFEE_CON_SUB = arrSUBDET(i)
            If vFEE_CON_SUB.FCD_ID = lblFCD_ID.Text Then
                'vFEE_CON_SUB.bDelete = True
                vFEE_CON.SUB_DETAILS.Remove(vFEE_CON_SUB)
                Exit For
            End If
        Next
        GridBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            '   lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim Str_ErrorReturn As String = AddUpdateDetailsInSave()
        If Str_ErrorReturn <> "" Then
            usrMessageBar.ShowNotification(Str_ErrorReturn, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim str_error As String = CheckErrors()
        Dim dtDocDate, dtFromDT, dtToDT As DateTime
        dtDocDate = CDate(txtDate.Text)
        dtFromDT = CDate(txtFromDT.Text)
        dtToDT = CDate(txtToDT.Text)
        If dtToDT < dtFromDT Then
            str_error = str_error & "From date is greater than to date<br />"
        End If
        If h_FCH_ID.Value = "" Then
            '  lblError.Text = "Please select the concession to cancel <br>" & str_error
            usrMessageBar.ShowNotification("Please select the concession to cancel <br>" & str_error, UserControls_usrMessageBar.WarningType.Danger)
        End If
        If str_error <> "" Then
            '   lblError.Text = "Please check the Following : <br>" & str_error
            usrMessageBar.ShowNotification("Please check the Following : <br>" & str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim FCH_ID As Integer = h_FCH_ID.Value
        If Not ViewState("FCH_ID") Is Nothing Then
            FCH_ID = ViewState("FCH_ID")
        End If
        Dim vFEE_CONS_TRAN As New FEEConcessionTransactionBB
        If Not Session("FEE_CONS_TRAN") Is Nothing Then
            vFEE_CONS_TRAN = Session("FEE_CONS_TRAN")
        End If
        vFEE_CONS_TRAN.FCH_ACD_ID = ddlAcademicYear.SelectedValue
        vFEE_CONS_TRAN.FCH_BSU_ID = Session("sBsuid")
        vFEE_CONS_TRAN.FCH_STU_BSU_ID = ddlBusinessunit.SelectedItem.Value
        vFEE_CONS_TRAN.FCH_DT = dtDocDate
        vFEE_CONS_TRAN.FCH_DTFROM = dtFromDT
        vFEE_CONS_TRAN.FCH_DTTO = dtToDT
        vFEE_CONS_TRAN.FCH_FCM_ID = h_FCM_ID_HEAD.Value
        If txtRefHEAD.Text <> "" Then vFEE_CONS_TRAN.FCH_REF_ID = CInt(H_REFID_HEAD.Value) _
      Else vFEE_CONS_TRAN.FCH_REF_ID = 0
        ' vFEE_CONS_TRAN.FCH_REF_ID = txtRefID.Text
        vFEE_CONS_TRAN.FCH_REMARKS = txtRemarks.Text
        vFEE_CONS_TRAN.FCH_STU_ID = h_STUD_ID.Value
        vFEE_CONS_TRAN.STU_NAME = txtStud_Name.Text
        vFEE_CONS_TRAN.FCH_DRCR = "DR"
        vFEE_CONS_TRAN.FCH_FCH_ID = FCH_ID
        If Not ViewState("FCH_ID") Is Nothing Then
            vFEE_CONS_TRAN.bEdit = True
            vFEE_CONS_TRAN.FCH_FCH_ID = H_FCH_FCH_ID.Value
            vFEE_CONS_TRAN.FCH_ID = FCH_ID
        End If
        Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
            Dim trans As SqlTransaction
            trans = conn.BeginTransaction("FEE_CONCESSION_TRANS")
            Dim NEW_FCH_ID As String = ""
            Dim retVal As Integer = FEEConcessionTransactionBB.F_SaveFEE_CONCESSION_H(vFEE_CONS_TRAN, NEW_FCH_ID, _
            conn, trans)

            If retVal <> 0 Then
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(retVal)
                usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                trans.Commit()
                ViewState("FCH_ID") = NEW_FCH_ID
                ' lblError.Text = "Data updated Successfully"
                usrMessageBar.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                Session("FEE_CONS_TRAN") = Nothing
                ClearDetails()
                ViewState("FCH_ID") = NEW_FCH_ID
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, NEW_FCH_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page, txtRemarks.Text)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If

        End Using
    End Sub

    Private Function CheckErrors() As String
        If Not FEEConcessionTransactionBB.PeriodBelongstoAcademicYear(CDate(txtFromDT.Text), CDate(txtToDT.Text), ddlAcademicYear.SelectedValue, ddlBusinessunit.SelectedItem.Value) Then
            Return "The date period doesnot belongs to current Academic Year"
        End If
        Return ""
    End Function

    Private Sub ClearDetails()
        setAcademicyearDate()
        txtRefHEAD.Text = ""
        txtRemarks.Text = ""
        txtAmount.Text = ""
        txtRefIDsub.Text = ""
        txtStud_Name.Text = ""
        h_STUD_ID.Value = ""
        ViewState("FCH_ID") = "0"
        h_FCM_ID_DET.Value = ""
        h_FCM_ID_HEAD.Value = ""
        h_FCT_ID_DET.Value = ""
        h_FCT_ID_HEAD.Value = ""

        H_REFID_HEAD.Value = ""
        txtRefHEAD.Text = ""
        txtRefIDsub.Text = ""
        H_REFID_SUB.Value = ""
        txtStdNo.Text = ""
        radAmount.Checked = True
        radPercentage.Checked = False
        Session("FEE_CONS_TRAN") = Nothing
        GridBind()
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        ClearDetails()
        ClearSubDetails()
        DissableControls(False)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        'ClearDetails()
        'ClearSubDetails()
        DissableControls(True)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    'Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
    '    ViewState("datamode") = "edit"
    '    Dim FEE_CON As FEEConcessionTransactionbb = Session("FEE_CONS_TRAN")
    '    FEE_CON.bEdit = True
    '    SetAllEditable(FEE_CON.SUB_DETAILS, True)
    '    Session("FEE_CONS_TRAN") = FEE_CON
    '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    '    DissableControls(False)
    '    imgCompany.Enabled = False
    '    UtilityObj.beforeLoopingControls(Me.Page)
    'End Sub 

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub imgRefSub_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgRefSub.Click
        If H_REFID_SUB.Value <> "" Then
            If AlertForRefRepetition(h_FCM_ID_DET.Value, H_REFID_SUB.Value) Then
                ViewState("CLICK_TYPE") = "SUB"
                'Me.programmaticModalPopup.Show()
            End If
        End If
    End Sub

    Protected Sub imgRefHead_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgRefHead.Click
        If H_REFID_HEAD.Value <> "" Then
            If AlertForRefRepetition(h_FCM_ID_DET.Value, H_REFID_HEAD.Value) Then
                ViewState("CLICK_TYPE") = "HEAD"
                'Me.programmaticModalPopup.Show()
            End If
        End If
    End Sub

    Protected Sub ddlFeeType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFeeType.SelectedIndexChanged
        Set_Collection()
    End Sub

    Sub Set_Collection()
        Dim iMonths, iSCH_ID As Integer
        Dim dtTermorMonths As New DataTable
        Dim TOTALAMOUNT As Decimal = 0
        Dim STR_SCH_ID As String = FeeCommon.GetFeeSCH_ID_ForConcession(h_STUD_ID.Value, ddlFeeType.SelectedItem.Value, _
        ddlAcademicYear.SelectedItem.Value, ddlBusinessunit.SelectedItem.Value, TOTALAMOUNT)
        ViewState("SCH_ID") = STR_SCH_ID
        txtTotalFee.Text = TOTALAMOUNT
        If IsNumeric(STR_SCH_ID) Then
            iSCH_ID = CInt(STR_SCH_ID)
            If iSCH_ID = 2 Then
                dtTermorMonths = FeeCommon.GetFee_MonthorTerm(ddlFeeType.SelectedItem.Value, _
                ddlAcademicYear.SelectedItem.Value, ddlBusinessunit.SelectedItem.Value, True, h_STUD_ID.Value, txtFromDT.Text)
            Else
                dtTermorMonths = FeeCommon.GetFee_MonthorTerm(ddlFeeType.SelectedItem.Value, _
                ddlAcademicYear.SelectedItem.Value, ddlBusinessunit.SelectedItem.Value, False, h_STUD_ID.Value, txtFromDT.Text)
            End If
            Select Case iSCH_ID
                Case 0 '0 Monthly 
                    iMonths = 10
                Case 1 '1 Bi-Monthly
                    iMonths = 5
                Case 2 '2 Quarterly 
                    iMonths = 3
                Case 3 '3 Half Year
                    iMonths = 2
                Case 4, 5 '4 Annual 
                    iMonths = 1
                Case Else '5 Once Only 
                    iMonths = 0
            End Select
        End If
        Dim dt As DataTable
        dt = FEE_CONC_TRANC_SUB_MONTHLY_BB.CreateDataTableFeeConcessionMonthly
        Select Case iMonths
            Case 10, 3, 1
                For i As Integer = 0 To iMonths - 1
                    If i < dtTermorMonths.Rows.Count Then
                        Dim dr As DataRow
                        dr = dt.NewRow
                        dr("id") = dtTermorMonths.Rows(i)("ID")
                        dr("Descr") = dtTermorMonths.Rows(i)("descr")
                        dr("FDD_DATE") = dtTermorMonths.Rows(i)("FDD_DATE")
                        dr("FDD_AMOUNT") = dtTermorMonths.Rows(i)("FDD_AMOUNT")
                        'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT
                        dt.Rows.Add(dr)
                    End If
                Next
            Case 5, 2
                For i As Integer = 0 To iMonths - 1
                    If i < dtTermorMonths.Rows.Count Then
                        Dim dr As DataRow
                        dr = dt.NewRow
                        dr("id") = dtTermorMonths.Rows(i * 10 / iMonths)("ID")
                        dr("Descr") = dtTermorMonths.Rows(i)("descr")
                        dr("FDD_DATE") = dtTermorMonths.Rows(i * 10 / iMonths)("FDD_DATE")
                        dr("FDD_AMOUNT") = dtTermorMonths.Rows(i)("FDD_AMOUNT")
                        'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT,FDD_AMOUNT
                        dt.Rows.Add(dr)
                    End If
                Next
        End Select
        Session("tempMonthly") = dt
        gvMonthly.DataSource = dt
        gvMonthly.DataBind()
    End Sub

    Protected Sub imgCompany_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCompany.Click
        Set_Collection()
    End Sub

    Protected Sub lnkFill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFill.Click
        If IsNumeric(txtTotalFee.Text) And IsNumeric(txtAmount.Text) Then
            Dim decTotalAmountorPerc As Decimal = Convert.ToDecimal(txtAmount.Text)
            Dim decTotalFee As Decimal = Convert.ToDecimal(txtTotalFee.Text)
            Dim decWeightagePercentage As Decimal
            If radAmount.Checked Then
                decWeightagePercentage = decTotalAmountorPerc / decTotalFee * 100
            Else
                decWeightagePercentage = decTotalAmountorPerc
            End If
            If Not Session("tempMonthly") Is Nothing Then
                Dim iTotalSplitup As Integer = Session("tempMonthly").Rows.Count
                Dim decSplitAmount As Decimal = 0
                If radPercentage.Checked Then
                    If decTotalAmountorPerc > 100 Then
                        '   lblError.Text = "% exceeds 100"
                        usrMessageBar.ShowNotification("% exceeds 100", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    decSplitAmount = decTotalFee / iTotalSplitup * decTotalAmountorPerc / 100
                Else
                    If decTotalAmountorPerc > decTotalFee Then
                        '   lblError.Text = "Amount exceeds total amount"
                        usrMessageBar.ShowNotification("Amount exceeds total amount", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    decSplitAmount = decTotalAmountorPerc / iTotalSplitup
                End If
                For i As Integer = 0 To iTotalSplitup - 1
                    'Session("tempMonthly").rows(i)("CUR_AMOUNT") = decSplitAmount
                    Session("tempMonthly").rows(i)("CUR_AMOUNT") = Session("tempMonthly").rows(i)("FDD_AMOUNT") * decWeightagePercentage / 100
                Next
                gvMonthly.DataSource = Session("tempMonthly")
                gvMonthly.DataBind()
            End If
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        setAcademicyearDate()
    End Sub

    Sub setAcademicyearDate()
        Dim DTFROM As String = ""
        Dim DTTO As String = ""
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, ddlAcademicYear.SelectedItem.Value, ddlBusinessunit.SelectedItem.Value)
        txtFromDT.Text = DTFROM
        txtToDT.Text = DTTO
        txtDate.Text = DTFROM
    End Sub

    Protected Sub imgSelConcession_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSelConcession.Click
        If h_FCH_ID.Value <> "" Then
            set_ViewData(h_FCH_ID.Value, True)
        End If
    End Sub

    Sub set_ViewData(ByVal FCH_ID As Integer, ByVal bCancel As Boolean)
        Dim vFEE_CON As FEEConcessionTransactionBB
        vFEE_CON = FEEConcessionTransactionBB.GetDetails(FCH_ID, 2, _
        Session("sBsuid"), ddlBusinessunit.SelectedItem.Value, bCancel, ViewState("datamode"))
        Session("FEE_CONS_TRAN") = vFEE_CON
        txtDate.Text = Format(vFEE_CON.FCH_DT, OASISConstants.DateFormat)
        txtFromDT.Text = Format(vFEE_CON.FCH_DTFROM, OASISConstants.DateFormat)
        txtToDT.Text = Format(vFEE_CON.FCH_DTTO, OASISConstants.DateFormat)
        h_FCM_ID_HEAD.Value = vFEE_CON.FCH_FCM_ID
        txtRefHEAD.Text = vFEE_CON.FCH_REF_NAME
        H_REFID_HEAD.Value = vFEE_CON.FCH_REF_ID
        h_FCM_ID_HEAD.Value = vFEE_CON.FCH_FCM_ID
        txtConcession_Head.Text = UtilityObj.GetDataFromSQL("SELECT FCM_DESCR FROM " _
        & " FEES.FEE_CONCESSION_M WHERE FCM_ID =" & h_FCM_ID_HEAD.Value, WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        h_FCT_ID_HEAD.Value = UtilityObj.GetDataFromSQL("SELECT FCM_FCT_ID FROM " _
        & " FEES.FEE_CONCESSION_M WHERE FCM_ID =" & h_FCM_ID_HEAD.Value, WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)

        txtRefHEAD.Text = vFEE_CON.FCH_REF_NAME
        H_REFID_HEAD.Value = vFEE_CON.FCH_REF_ID
        txtRefHEAD.Text = GetReference(h_FCT_ID_HEAD.Value, vFEE_CON.FCH_REF_ID, ddlBusinessunit.SelectedItem.Value)
        txtLocation.Text = vFEE_CON.FCH_SBL_DESCR
        H_Location.Value = vFEE_CON.FCH_SBL_ID
        txtRemarks.Text = vFEE_CON.FCH_REMARKS
        h_STUD_ID.Value = vFEE_CON.FCH_STU_ID
        txtStud_Name.Text = vFEE_CON.STU_NAME
        txtStdNo.Text = FeeCommon.GetStudentNo(vFEE_CON.FCH_STU_ID, True)
        ddlAcademicYear.SelectedValue = vFEE_CON.FCH_ACD_ID
        txtStud_Name.Text = vFEE_CON.STU_NAME
        If Not ViewState("FCH_ID") Is Nothing Then
            H_FCH_FCH_ID.Value = vFEE_CON.FCH_FCH_ID
            txtConcession.Text = vFEE_CON.FCH_REMARKS
        End If

        For Each vSUB_DET As FEE_CONC_TRANC_SUB_BB In vFEE_CON.SUB_DETAILS
            SetMonthlyDataEdit(vSUB_DET.FCD_ID)
        Next
        GridBind()
        DissableControls(Not bCancel)
    End Sub

    Private Function GetReference(ByVal refType As ConcessionType, ByVal ID As String, ByVal BSU_ID As String) As String
        Dim sql_query As String
        Dim objName As Object
        Select Case refType
            Case ConcessionType.Sibling
                sql_query = "SELECT ISNULL(STU_FIRSTNAME, '')+' ' + ISNULL(STU_MIDNAME, '')+' ' + ISNULL(STU_LASTNAME, '') AS STU_NAME FROM STUDENT_M WHERE (STU_ID='" & ID & "' ) "
                objName = SqlHelper.ExecuteScalar(WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString, _
                CommandType.Text, sql_query)
            Case ConcessionType.Staff
                sql_query = "SELECT ISNULL(EMP_FNAME, '')+' ' + ISNULL(EMP_MNAME, '')+' ' + ISNULL(EMP_LNAME, '') AS EMP_NAME FROM EMPLOYEE_M WHERE (EMP_ID='" & ID & "' ) "
                objName = SqlHelper.ExecuteScalar(WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, CommandType.Text, sql_query)
            Case Else
                Return ""
        End Select
        If objName Is Nothing Then
            Return ""
        End If
        Return objName.ToString
    End Function

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        Session("PROVIDER_BSU_ID") = ddlBusinessunit.SelectedItem.Value
        txtStdNo.Text = ""
        txtStud_Name.Text = ""
        h_STUD_ID.Value = ""
        FillACD()
        ddlFeeType.DataBind()
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBusinessunit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Sub PrintConcession(ByVal IntFCH_ID As Integer)
        Session("ReportSource") = FEEConcessionTransactionBB.PrintConcessionCancel(IntFCH_ID, Session("sBsuId"), Session("sUsr_name"))
        h_print.Value = "print"
    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        PrintConcession(ViewState("FCH_ID"))
        h_print.Value = "print"
    End Sub

    Protected Sub txtConcession_TextChanged(sender As Object, e As EventArgs)
        If h_FCH_ID.Value <> "" Then
            set_ViewData(h_FCH_ID.Value, True)
        End If
    End Sub
End Class
