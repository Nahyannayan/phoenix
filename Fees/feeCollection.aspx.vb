Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Net
Imports Newtonsoft.Json.Linq
Imports System.Drawing
Imports System.Threading
Imports System.Drawing.Imaging
Imports RestSharp


Partial Class Fees_feeCollection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public Shared dtDiscount As DataTable
    Public Const TAX_FEE_ID As Integer = 152
    Private Property bGSTEnabled() As Boolean
        Get
            Return ViewState("bGSTEnabled")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bGSTEnabled") = value
        End Set
    End Property
    Private Property STU_ID() As Long
        Get
            Return ViewState("STU_ID")
        End Get
        Set(ByVal value As Long)
            ViewState("STU_ID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If h_Chequeid.Value <> "" Then
            SetChequeDetails(h_Chequeid.Value)
        End If
        If Page.IsPostBack = False Then
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            If Session("sUsr_name") = "" Or Session("sBSuid") = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_COLLECTION Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBSuid"), ViewState("MainMnu_code"))
                'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                '-----The below function will overwrite the user rights for buttons-------
                InitializeRibbonControl()
                '--------------Setting user access rights for the buttons
                usrRibbonControlPanel.SetButtons(ViewState("menu_rights"), ViewState("datamode"))
            End If
            'Dim str_disableSavebutton As String = ("javascript:" & btnSave.ClientID & ".disabled=true;") + ClientScript.GetPostBackEventReference(btnSave, "").ToString() & ";"
            Dim str_scriptWhethercobrand As String = "if ( (parseFloat(document.getElementById('" & txtCCTotal.ClientID & "').value)>0 ) && document.getElementById('" & ddCreditcard.ClientID & "').options[document.getElementById('" & ddCreditcard.ClientID & "').selectedIndex].text.search(/Co-Brand/)>0)"

            'Dim str_savescript As String = "javascript:" & str_scriptWhethercobrand & "{ if (confirm('NBAD Co-brand card Selected. Do you want to continue?')==1) { " & str_disableSavebutton & " } else return false; } else  { " & str_disableSavebutton & " }"
            'btnSave.Attributes.Add("onclick", str_savescript)
            BindBusinessunits()
            Page.Title = OASISConstants.Gemstitle
            ViewState("datamode") = "add"
            InitialiseCompnents()
            gvFeeCollection.DataBind()
            
            If Not Request.QueryString("SID") Is Nothing Then
                Dim STUID = Encr_decrData.Decrypt(Request.QueryString("SID").Replace(" ", "+"))
                uscStudentPicker.LoadStudentsByID(STUID)
            End If
        End If

    End Sub
    Private Sub BindBusinessunits()
        ddlBusinessunit.Items.Clear()
        ddlBusinessunit.DataSource = clsFeeCollection.GetBusinessUnitsForCollection()
        ddlBusinessunit.DataBind()
        If ddlBusinessunit.Items.Count > 0 Then
            ddlBusinessunit.Items.FindItemByValue(Session("sBsuId")).Selected = True
        End If
    End Sub
    Private Sub bind_Currency() 'bind the currency combo according to selected date
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            ddCurrency.Items.Clear()
            ddCurrency.DataSource = MasterFunctions.GetExchangeRates(Now.Date.ToString("dd/MMM/yyyy"), ddlBusinessunit.SelectedValue, Session("BSU_CURRENCY"))
            ddCurrency.DataTextField = "EXG_CUR_ID"

            ddCurrency.DataValueField = "RATES"
            ddCurrency.DataBind()
            If ddCurrency.Items.Count > 0 Then
                Dim lstDrp As New ListItem
                lstDrp = ddCurrency.Items.FindByText(Session("BSU_CURRENCY"))
                If Not lstDrp Is Nothing Then
                    ddCurrency.SelectedValue = lstDrp.Value
                End If
                lblExgRate.Text = ddCurrency.SelectedItem.Value.Split("__")(0).Trim
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub DDCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddCurrency.SelectedIndexChanged
        lblExgRate.Text = ddCurrency.SelectedItem.Value.Split("__")(0).Trim
        gvFeeCollection.Columns(13).HeaderText = "Paying Now (" & ddCurrency.SelectedItem.Text & ")"
        Gridbind_Feedetails()
        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") Then
            gvFeeCollection.Columns(13).Visible = True
            lblOutstandingFC.Visible = True
            lblDueFC.Visible = True
        Else
            gvFeeCollection.Columns(13).Visible = False
            lblOutstandingFC.Visible = False
            lblDueFC.Visible = False
        End If
    End Sub
    Sub BindEmirate(ByVal dtemirates As DataTable, ByVal DefaultCity As String)
        Try
            ddlEmirate1.DataSource = dtemirates
            ddlEmirate1.DataBind()
            ddlEmirate2.DataSource = dtemirates
            ddlEmirate2.DataBind()
            ddlEmirate3.DataSource = dtemirates
            ddlEmirate3.DataBind()
            If DefaultCity <> "--" Then
                ddlEmirate1.Items.FindByValue(DefaultCity).Selected = True
                ddlEmirate2.Items.FindByValue(DefaultCity).Selected = True
                ddlEmirate3.Items.FindByValue(DefaultCity).Selected = True
            End If
        Catch ex As Exception
            Errorlog("Feecollection-BindEmirate()" & ex.Message, "PHOENIX")
        End Try
    End Sub
    Sub BindCardTypes(ByVal dtCrCards As DataTable)
        ddCreditcard.Items.Clear()
        ddCreditcard2.Items.Clear()
        ddCreditcard3.Items.Clear()
        
        ddCreditcard.DataSource = dtCrCards
        ddCreditcard2.DataSource = dtCrCards
        ddCreditcard3.DataSource = dtCrCards
        ddCreditcard.DataBind()
        ddCreditcard2.DataBind()
        ddCreditcard3.DataBind()
    End Sub

    Sub InitialiseCompnents()
        uscStudentPicker.STU_BSU_ID = ddlBusinessunit.SelectedValue
        Dim objclsFeecoll As New clsFeeCollection
        bind_Currency()
        objclsFeecoll.GetCobrandvalue()
        'Dim qry As String = "SELECT CONVERT(VARCHAR(3),CREDITCARD_S.CRR_ID)+'='+CONVERT(VARCHAR(10),isnull(dbo.CREDITCARD_S.CRR_CLIENT_RATE,0))+'|' FROM dbo.CREDITCARD_S WITH(NOLOCK) INNER JOIN " & _
        '                        " dbo.CREDITCARD_M WITH(NOLOCK) ON CREDITCARD_S.CRR_CRI_ID = CREDITCARD_M.CRI_ID INNER JOIN " & _
        '                        " dbo.CREDITCARD_PROVD_M WITH(NOLOCK) ON CREDITCARD_S.CRR_CPM_ID = CREDITCARD_PROVD_M.CPM_ID " & _
        '                        " WHERE (CREDITCARD_S.CRR_bOnline = 0) FOR XML PATH('')"
        Me.hfCobrand.Value = objclsFeecoll.hfCobrandValue 'SqlHelper.ExecuteScalar(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, qry)
        Set_COBrand_Payment_controls()

        objclsFeecoll.GetBusinessunitParameters(ddlBusinessunit.SelectedValue)
        pnlCurrency.Visible = objclsFeecoll.bFeesMulticurrency
        chkChargableCollection.Visible = objclsFeecoll.bOTH_DISC
        bGSTEnabled = objclsFeecoll.bGSTEnabled
        hfTaxable.Value = IIf(bGSTEnabled, "1", "0")
        Me.gvFeeCollection.Columns(11).Visible = bGSTEnabled
        Me.spanTax.Visible = bGSTEnabled

        txtCrCTotal1.Attributes.Add("onBlur", "UpdateSum('" & Me.ddCreditcard.ClientID & "','" & txtCrCTotal1.ClientID & "','" & txtCrCardP1.ClientID & "','" & txtCrCardCharge1.ClientID & "');")
        txtCrCTotal2.Attributes.Add("onBlur", "UpdateSum('" & Me.ddCreditcard2.ClientID & "','" & txtCrCTotal2.ClientID & "','" & txtCrCardP2.ClientID & "','" & txtCrCardCharge2.ClientID & "');")
        txtCrCTotal3.Attributes.Add("onBlur", "UpdateSum('" & Me.ddCreditcard3.ClientID & "','" & txtCrCTotal3.ClientID & "','" & txtCrCardP3.ClientID & "','" & txtCrCardCharge3.ClientID & "');")

        Me.ddCreditcard.Attributes.Add("onChange", "return CheckRate('" & Me.ddCreditcard.ClientID & "','" & txtCrCTotal1.ClientID & "','" & txtCrCardP1.ClientID & "','" & txtCrCardCharge1.ClientID & "');")
        Me.ddCreditcard2.Attributes.Add("onChange", "return CheckRate('" & Me.ddCreditcard2.ClientID & "','" & txtCrCTotal2.ClientID & "','" & txtCrCardP2.ClientID & "','" & txtCrCardCharge2.ClientID & "');")
        Me.ddCreditcard3.Attributes.Add("onChange", "return CheckRate('" & Me.ddCreditcard3.ClientID & "','" & txtCrCTotal3.ClientID & "','" & txtCrCardP3.ClientID & "','" & txtCrCardCharge3.ClientID & "');")

        txtTotal.Attributes.Add("readonly", "readonly")
        txtReceivedTotal.Attributes.Add("readonly", "readonly")
        txtBalance.Attributes.Add("readonly", "readonly")
        txtDue.Attributes.Add("readonly", "readonly")
        txtOutstanding.Attributes.Add("readonly", "readonly")
        Me.txtCrCardP1.Attributes.Add("readonly", "readonly")
        Me.txtCrCardP2.Attributes.Add("readonly", "readonly")
        Me.txtCrCardP3.Attributes.Add("readonly", "readonly")
        Me.txtCrCardCharge1.Attributes.Add("readonly", "readonly")
        Me.txtCrCardCharge2.Attributes.Add("readonly", "readonly")
        Me.txtCrCardCharge3.Attributes.Add("readonly", "readonly")
        Me.txtRewardsAmount.Attributes.Add("readonly", "true")
        'gvFeeCollection.Attributes.Add("bordercolor", "#1b80b6")
        gvDiscount.Attributes.Add("bordercolor", "#fc7f03")
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtChqDate1.Text = txtFrom.Text
        txtChqDate2.Text = txtFrom.Text
        txtChqDate3.Text = txtFrom.Text
        Dim formatstring As String = "#,##0.00"
        formatstring = formatstring.Replace("0.", "###,###,###,##0.")
        For columnIndex As Integer = 3 To 8
            DirectCast(gvFeeCollection.Columns(columnIndex), BoundField).DataFormatString = "{0:" & formatstring & "}"
        Next

        'FillACD()
        FillFeeType()
        'SetNarration()
        'SetCollectionBank()
        ddlVoucherType.DataBind()
        'lnkTransportfee.OnClientClick = "PayTransportFee('" & Request.Url.ToString.Replace("feeCollection.aspx", "feeTranspotCollectionfromFee.aspx") & "&stuno=" & uscStudentPicker.STU_NO & "&acdid=" & uscStudentPicker.STU_ACD_ID & "',1);return false;"
        'lbNotes.OnClientClick = "PayTransportFee('" & Request.Url.ToString.Replace("feeCollection.aspx", "FeeStudentNotes.aspx") & "&stuno=" & uscStudentPicker.STU_NO & "&acdid=" & uscStudentPicker.STU_ACD_ID & "',2);return false;"
        'Set_Provider()
        BindEmirate(objclsFeecoll.GetEmiratesOrStates(), objclsFeecoll.DefaultCity)

        BindCardTypes(objclsFeecoll.GetCreditCardListForCollection(ddlBusinessunit.SelectedValue, "FEES"))
    End Sub
    Sub InitializeRibbonControl()
        usrRibbonControlPanel.MenuCode = ViewState("MainMnu_code")
        '--"none" will hide the button, "block" will display the button
        usrRibbonControlPanel.DISPLAY_APPROVE = "none"
        usrRibbonControlPanel.DISPLAY_REJECT = "none"
        usrRibbonControlPanel.DISPLAY_DELETE = "none"
        usrRibbonControlPanel.DISPLAY_EDIT = "none"
        usrRibbonControlPanel.DISPLAY_EXPORT = "none"
        usrRibbonControlPanel.DISPLAY_HELP = "block"
        usrRibbonControlPanel.DISPLAY_HISTORY = "block"
        usrRibbonControlPanel.DISPLAY_PRINT = "none"
        usrRibbonControlPanel.DISPLAY_SEARCH = "none"
    End Sub
    Sub SetCollectionBank()
        Dim qry As String = "SELECT ISNULL(BSU_COLLECTBANK_ACT_ID,'')AS BSU_COLLECTBANK_ACT_ID FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & ddlBusinessunit.SelectedValue & "'"
        Dim BSU_COLLECTBANK_ACT_ID As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        Me.txtBankAct1.Text = BSU_COLLECTBANK_ACT_ID
        Me.txtBankAct2.Text = BSU_COLLECTBANK_ACT_ID
        Me.txtBankAct3.Text = BSU_COLLECTBANK_ACT_ID
        Me.hfBankAct1.Value = BSU_COLLECTBANK_ACT_ID
        Me.hfBankAct2.Value = BSU_COLLECTBANK_ACT_ID
        Me.hfBankAct3.Value = BSU_COLLECTBANK_ACT_ID
    End Sub
    Sub Set_Provider()
        Dim str_PROVIDER_BSU_ID As String = UtilityObj.GetDataFromSQL("SELECT SVB_PROVIDER_BSU_ID FROM OASIS.dbo.SERVICES_BSU_M WITH(NOLOCK) " & _
        " WHERE SVB_BSU_ID = '" & ddlBusinessunit.SelectedValue & "' AND SVB_ACD_ID = '" & uscStudentPicker.STU_ACD_ID & "' AND SVB_SVC_ID = 1", _
        ConnectionManger.GetOASISConnectionString)
        If str_PROVIDER_BSU_ID <> "--" Then
            Session("PROVIDER_BSU_ID") = str_PROVIDER_BSU_ID
        Else
            Session("PROVIDER_BSU_ID") = ""
        End If
    End Sub

    Sub FillACD()
        'Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        'ddlAcademicYear.DataSource = dtACD
        'ddlAcademicYear.DataTextField = "ACY_DESCR"
        'ddlAcademicYear.DataValueField = "ACD_ID"
        'ddlAcademicYear.DataBind()
        'For Each rowACD As DataRow In dtACD.Rows
        '    If rowACD("ACD_CURRENT") Then
        '        ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
        '        Exit For
        '    End If
        'Next
    End Sub

    Sub FillFeeType()
        Dim dtFeeType As DataTable = FeeCommon.getFeeTypes(ddlBusinessunit.SelectedValue, Int32.Parse(uscStudentPicker.STU_ACD_ID))
        ddFeetype.DataSource = dtFeeType
        ddFeetype.DataTextField = "FEE_DESCR"
        ddFeetype.DataValueField = "FEE_ID"
        ddFeetype.DataBind()
    End Sub

    Sub clear_All()
        txtCCTotal.Text = "0.00"
        txtChequeTotal1.Text = "0.00"
        txtCashTotal.Text = "0.00"
        txtTotal.Text = "0.00"
        txtVoucherAmount1.Text = "0.00"
        txtVoucherTotal.Text = "0.00"
        txtRewardsAmount.Text = "0.00"
        rblVAT.SelectedValue = Nothing
        txtReceivedTotal.Text = ""
        h_hideBank.Value = ""
        h_CrCard.Value = ""
        txtBankTotal.Text = ""
        'labGrade.Text = ""
        If Not chkRemember.Checked Then
            txtCreditno.Text = ""
            txtBank1.Text = ""
            txtBank2.Text = ""
            txtBank3.Text = ""
            h_Bank1.Value = ""
            h_Bank2.Value = ""
            h_Bank3.Value = ""
            txtBankAct1.Text = ""
            txtBankAct2.Text = ""
            txtBankAct3.Text = ""
            hfBankAct1.Value = ""
            hfBankAct2.Value = ""
            hfBankAct3.Value = ""
            txtChqno1.Text = ""
            txtChqno2.Text = ""
            txtChqno3.Text = ""
            txtChqDate1.Text = txtFrom.Text
            txtChqDate2.Text = txtFrom.Text
            txtChqDate3.Text = txtFrom.Text

            txtCreditno.Text = ""
            txtCreditno2.Text = ""
            txtCreditno3.Text = ""
            txtCCTotal.Text = 0
            txtCrCardCharges.Text = 0
        End If
        txtChqno1.Attributes.Remove("readonly")
        h_Chequeid.Value = ""
        txtBalance.Text = ""
        Me.txtCrCTotal1.Text = ""
        Me.txtCrCTotal2.Text = ""
        Me.txtCrCTotal3.Text = ""
        txtCrCardCharges.Text = 0
        txtCrCardCharge1.Text = 0
        txtCrCardP1.Text = 0
        txtCrCardCharge2.Text = 0
        txtCrCardP2.Text = 0
        txtCrCardCharge3.Text = 0
        txtCrCardP3.Text = 0
        txtCCTotal.Text = 0
        txtChequeTotal1.Text = ""
        txtChequeTotal2.Text = ""
        txtChequeTotal3.Text = ""
        txtRemarks.Text = ""
        txtAmountAdd.Text = "0.00"
        txtVATAmount.Text = "0.00"
        txtAmount.Text = "0.00"
        rblVAT.SelectedValue = Nothing

        txtDue.Text = ""
        txtOutstanding.Text = ""
        lbl_OS.Text = "0.00"
        lbl_PayingNow.Text = "0.00"
        lbl_TAX.Text = "0.00"
        lbl_Discnt.Text = "0.00"
        lbl_Due.Text = "0.00"

        ' txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        ViewState("DiscntXML") = ""
        SetNarration()
        chkRemember.Checked = False
        ClearStudentData()

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ForeignCurrency As Boolean = False
        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
            ForeignCurrency = True
        End If

        If Not Master.IsSessionMatchesForSave() Then
            ' ShowMessage(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH) 'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Set_GridTotal(True)
        Set_Total()
        Dim str_error As String = check_errors_details()
        If Not IsDate(txtFrom.Text) Then
            str_error = str_error & "Invalid from date <br />"
        End If
        If uscStudentPicker.STU_NO = "" Then
            str_error = str_error & "Please select student <br />"
        End If
        'If dtFrom > Now.Date Then
        '    str_error = str_error &"Invalid date(Future Date)!!!" 
        'End If
        If Not IsNumeric(txtBalance.Text) Then
            txtBalance.Text = "0"
        End If
        If gvFeeCollection.Rows.Count = 0 Then
            str_error = str_error & "Please add fee details<br />"
        End If

        If IsNumeric(txtCCTotal.Text) AndAlso FeeCollection.GetDoubleVal(txtCCTotal.Text) > 0 Then
            Dim str_CRR_ID As String = UtilityObj.GetDataFromSQL("SELECT SYS_COBRAND_CRR_ID FROM SYSINFO_S", ConnectionManger.GetOASISFINConnectionString)
            If ddCreditcard.SelectedItem.Value = str_CRR_ID And Not chkCoBrand.Checked Then
                str_error = str_error & "Please select co-brand check box...<br />"
            End If
        End If
        If FeeCollection.GetDoubleVal(Me.txtCCTotal.Text) <> (FeeCollection.GetDoubleVal(Me.txtCrCTotal1.Text) + FeeCollection.GetDoubleVal(Me.txtCrCTotal2.Text) + FeeCollection.GetDoubleVal(Me.txtCrCTotal3.Text)) Then
            str_error = str_error & "Credit card totals are not tallying <br />"
        End If
        If FeeCollection.GetDoubleVal(txtVoucherAmount1.Text) > 0 Then
            Dim objfcln As New FeeCollection
            objfcln.VOUCHER_NO = txtVoucherNo1.Text.Trim
            Dim Retmessage As String = objfcln.VALIDATE_VOUCHER_NUMBER
            If Retmessage <> "" Then
                str_error = str_error & Retmessage & "<br />"
            End If
        End If
        Dim dblReceivedAmount, dblTotal, dblCash, dblBalance, dblCreditCardCharge, dblVoucherTotal, dblRewardsTotal As Decimal
        Dim dblTAX As Double = 0
        dblReceivedAmount = FeeCollection.GetDoubleVal(txtReceivedTotal.Text)
        dblTotal = FeeCollection.GetDoubleVal(txtTotal.Text)
        dblCash = FeeCollection.GetDoubleVal(txtCashTotal.Text)
        dblTAX = IIf(IsNumeric(txtTax.Text), FeeCollection.GetDoubleVal(txtTax.Text), 0)
        dblCreditCardCharge = FeeCollection.GetDoubleVal(Me.txtCrCardCharges.Text)
        dblVoucherTotal = FeeCollection.GetDoubleVal(txtVoucherTotal.Text)
        dblRewardsTotal = FeeCollection.GetDoubleVal(txtRewardsAmount.Text)
        If (dblReceivedAmount - dblCreditCardCharge) <> (dblTotal) Then
            If dblCash > 0 Then
                If (dblTotal) < dblReceivedAmount Then
                    dblBalance = dblReceivedAmount - (dblTotal)
                    If dblBalance > dblCash Then
                        str_error = str_error & "Cannot Refund!!!<br />"
                        txtReceivedTotal.Text = 0
                    Else
                        txtBalance.Text = Format(dblBalance, "#,##0.00")
                    End If
                Else
                    str_error = str_error & "Totals not tallying<br />"
                End If
            Else
                txtBalance.Text = Format(FeeCollection.GetDoubleVal(txtBalance.Text), "#,##0.00")
                str_error = str_error & "Totals not tallying<br />"
            End If
        End If
        If dblTotal <= 0 Then
            str_error = str_error & "Invalid Amount!!!<br />"
        End If
        dblBalance = FeeCollection.GetDoubleVal(txtBalance.Text)
        'If txtRemarks.Text = "" Then
        '    str_error = str_error & "Please enter remarks<br />"
        'End If
        If str_error <> "" Then
            '  ShowMessage(str_error) 'lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)

            Exit Sub
        End If
        Dim dtFrom As DateTime
        dtFrom = CDate(txtFrom.Text)

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        Dim str_new_FCL_ID As Long
        Dim str_NEW_FCL_RECNO As String = ""
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            Dim STR_TYPE As Char = uscStudentPicker.STU_TYPE
            'If rbEnquiry.Checked Then
            '    STR_TYPE = "E"
            'End If
            Dim ExchRate As Decimal = IIf(ForeignCurrency = True, FeeCollection.GetDoubleVal(lblExgRate.Text), 1.0)
            retval = FeeCommon.CheckFeeclose(uscStudentPicker.STU_BSU_ID, txtFrom.Text, stTrans)

            If retval = "0" Then
                Dim Total As Decimal
                Dim CreditCardCharge As Double
                If IsNumeric(Me.txtCrCardCharges.Text) AndAlso FeeCollection.GetDoubleVal(Me.txtCrCardCharges.Text) > 0 Then
                    CreditCardCharge = FeeCollection.GetDoubleVal(Me.txtCrCardCharges.Text)
                Else
                    CreditCardCharge = 0
                End If
                Dim Currency As String = IIf(ForeignCurrency = True, ddCurrency.SelectedItem.Text, Session("BSU_CURRENCY"))
                Total = (FeeCollection.GetDoubleVal(txtTotal.Text) + CreditCardCharge) * ExchRate

                retval = FeeCollection.F_SaveFEECOLLECTION_H_Normal(0, "Counter", txtFrom.Text, _
                txtRemarks.Text, uscStudentPicker.STU_ACD_ID, STU_ID, Total, Session("sUsr_name"), _
                False, str_new_FCL_ID, uscStudentPicker.STU_BSU_ID, txtRemarks.Text, "CR", str_NEW_FCL_RECNO, 0, _
                 uscStudentPicker.STU_BSU_ID, STR_TYPE, txtDue.Text, Currency, ExchRate, stTrans, nxtAcaFee.Checked, Session("sBsuId"))

            End If
            If retval = "0" Then
                Dim FeeAutoChg As Boolean
                For Each gvr As GridViewRow In gvFeeCollection.Rows
                    FeeAutoChg = False
                    Dim ChkPost As CheckBox = CType(gvr.FindControl("ChkPost"), CheckBox)
                    Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
                    Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)
                    Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
                    Dim lblDiscount As Label = CType(gvr.FindControl("lblDiscount"), Label)
                    Dim txtDiscount As TextBox = CType(gvr.FindControl("txtDiscount"), TextBox)
                    Dim lblTAX As Label = CType(gvr.FindControl("lblTAX"), Label)
                    Dim PAY_TYPE_ID As String = "", FCS_PAY_TYPE As String = ""
                    If Not Me.gvFeeCollection.DataKeys(gvr.RowIndex)("PAY_TYPE_ID") Is Nothing Then
                        PAY_TYPE_ID = Convert.ToString(Me.gvFeeCollection.DataKeys(gvr.RowIndex)("PAY_TYPE_ID"))
                    End If
                    If Not Me.gvFeeCollection.DataKeys(gvr.RowIndex)("FCS_PAY_TYPE") Is Nothing Then
                        FCS_PAY_TYPE = Convert.ToString(Me.gvFeeCollection.DataKeys(gvr.RowIndex)("FCS_PAY_TYPE"))
                    End If
                    Dim CurrencyAmount As Decimal
                    If ForeignCurrency Then
                        Dim txtAmountToPayFC As TextBox = CType(gvr.FindControl("txtAmountToPayFC"), TextBox)
                        CurrencyAmount = FeeCollection.GetDoubleVal(txtAmountToPayFC.Text)
                    Else
                        CurrencyAmount = FeeCollection.GetDoubleVal(txtAmountToPay.Text)
                    End If
                    If Not txtAmountToPay Is Nothing Then
                        If FeeCollection.GetDoubleVal(txtAmountToPay.Text > 0) Then
                            If ChkPost.Checked Then
                                FeeAutoChg = True
                            End If
                            Dim dblTAX_FEE As Double = 0.0
                            If IsNumeric(lblTAX.Text) AndAlso FeeCollection.GetDoubleVal(lblTAX.Text) <> 0 Then
                                dblTAX_FEE = FeeCollection.GetDoubleVal(lblTAX.Text)
                            End If
                            Dim dcDiscount As Decimal
                            If gvFeeCollection.Columns(10).Visible Then 'Manual discount
                                dcDiscount = CDec(txtDiscount.Text)
                            Else
                                dcDiscount = CDec(lblDiscount.Text)
                            End If
                            retval = FeeCollection.F_SaveFEECOLLSUB_Normal(0, str_new_FCL_ID, lblFSR_FEE_ID.Text, _
                            txtAmountToPay.Text, -1, lblAmount.Text, dcDiscount, CurrencyAmount, stTrans, FeeAutoChg, _
                            dblTAX_FEE, PAY_TYPE_ID, FCS_PAY_TYPE)
                            If retval <> 0 Then
                                Exit For
                            End If
                        End If
                    End If
                Next
            End If

            '-------------Added by Jacob on 21/oct/2013, for credit card charges------------------------
            If retval = "0" Then
                If IsNumeric(Me.txtCrCardCharges.Text) AndAlso FeeCollection.GetDoubleVal(Me.txtCrCardCharges.Text) > 0 Then
                    retval = FeeCollection.F_SaveFEECOLLSUB_Normal(0, str_new_FCL_ID, 162, _
                                FeeCollection.GetDoubleVal(txtCrCardCharges.Text), -1, FeeCollection.GetDoubleVal(txtCrCardCharges.Text), 0, FeeCollection.GetDoubleVal(txtCrCardCharges.Text) * ExchRate, stTrans, True)
                End If
            End If
            '-------------Added by Jacob on 15/dec/2013, for tax collection------------------------
            'If retval = "0" Then
            '    If dblTAX <> 0 Then
            '        retval = FeeCollection.F_SaveFEECOLLSUB_Normal(0, str_new_FCL_ID, TAX_FEE_ID, _
            '                    dblTAX, -1, dblTAX, 0, dblTAX * ExchRate, stTrans, True, 0)
            '    End If
            'End If

            If retval = "0" Then
                Dim BaseCurrTotal As Decimal
                If FeeCollection.GetDoubleVal(txtCrCTotal1.Text) > 0 And retval = "0" Then 'credit card1 here
                    BaseCurrTotal = FeeCollection.GetDoubleVal(txtCrCTotal1.Text) + FeeCollection.GetDoubleVal(txtCrCardP1.Text) * ExchRate

                    retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID, COLLECTIONTYPE.CREDIT_CARD, _
                    BaseCurrTotal, FeeCollection.GetDoubleVal(txtCrCTotal1.Text) + FeeCollection.GetDoubleVal(txtCrCardP1.Text), txtCreditno.Text, txtFrom.Text, 0, "", ddCreditcard.SelectedItem.Value, _
                    "", stTrans, "", FeeCollection.GetDoubleVal(Me.txtCrCardP1.Text))
                End If
                If FeeCollection.GetDoubleVal(txtCrCTotal2.Text) > 0 And retval = "0" Then 'credit card2 here
                    BaseCurrTotal = FeeCollection.GetDoubleVal(txtCrCTotal2.Text) + FeeCollection.GetDoubleVal(txtCrCardP2.Text) * ExchRate

                    retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID, COLLECTIONTYPE.CREDIT_CARD, _
                    BaseCurrTotal, FeeCollection.GetDoubleVal(txtCrCTotal2.Text) + FeeCollection.GetDoubleVal(txtCrCardP2.Text), txtCreditno2.Text, txtFrom.Text, 0, "", ddCreditcard2.SelectedItem.Value, _
                    "", stTrans, "", FeeCollection.GetDoubleVal(Me.txtCrCardP2.Text))
                End If
                If FeeCollection.GetDoubleVal(txtCrCTotal3.Text) > 0 And retval = "0" Then 'credit card3 here
                    BaseCurrTotal = FeeCollection.GetDoubleVal(txtCrCTotal3.Text) + FeeCollection.GetDoubleVal(txtCrCardP3.Text) * ExchRate

                    retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID, COLLECTIONTYPE.CREDIT_CARD, _
                    BaseCurrTotal, FeeCollection.GetDoubleVal(txtCrCTotal3.Text) + FeeCollection.GetDoubleVal(txtCrCardP3.Text), txtCreditno3.Text, txtFrom.Text, 0, "", ddCreditcard3.SelectedItem.Value, _
                    "", stTrans, "", FeeCollection.GetDoubleVal(Me.txtCrCardP3.Text))
                End If

                If dblCash > dblBalance Then
                    BaseCurrTotal = FeeCollection.GetDoubleVal(dblCash - dblBalance) * ExchRate
                    'BaseCurrTotal = FeeCollection.GetDoubleVal(dblCash) * ExchRate
                    If FeeCollection.GetDoubleVal(txtCashTotal.Text) > 0 And retval = "0" Then 'cash  here
                        retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID, COLLECTIONTYPE.CASH, _
                       BaseCurrTotal, dblCash - dblBalance, "", txtFrom.Text, 0, "", "", "", stTrans)
                    End If
                End If

                If FeeCollection.GetDoubleVal(txtChequeTotal1.Text) > 0 And retval = "0" Then 'cheque 1 here
                    BaseCurrTotal = FeeCollection.GetDoubleVal(txtChequeTotal1.Text) * ExchRate

                    'If h_Bank1.Value.ToUpper <> "65" OrElse txtBank1.Text.ToUpper <> "BANK TRANSFER" Then
                    '    hfBankAct1.Value = ""
                    'End If

                    retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID, _
                    COLLECTIONTYPE.CHEQUES, BaseCurrTotal, FeeCollection.GetDoubleVal(txtChequeTotal1.Text), txtChqno1.Text, _
                    txtChqDate1.Text, 0, "", h_Bank1.Value, ddlEmirate1.SelectedItem.Value, _
                    stTrans, h_Chequeid.Value, 0, hfBankAct1.Value)
                End If

                If FeeCollection.GetDoubleVal(txtChequeTotal2.Text) > 0 And retval = "0" Then 'cheque 2 here
                    BaseCurrTotal = FeeCollection.GetDoubleVal(txtChequeTotal2.Text) * ExchRate

                    'If h_Bank2.Value.ToUpper <> "65" OrElse txtBank2.Text.ToUpper <> "BANK TRANSFER" Then
                    '    hfBankAct2.Value = ""
                    'End If

                    retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID, _
                    COLLECTIONTYPE.CHEQUES, BaseCurrTotal, FeeCollection.GetDoubleVal(txtChequeTotal2.Text), txtChqno2.Text, _
                    txtChqDate2.Text, 0, "", h_Bank2.Value, ddlEmirate2.SelectedItem.Value, _
                    stTrans, "", 0, hfBankAct2.Value)
                End If
                If FeeCollection.GetDoubleVal(txtChequeTotal3.Text) > 0 And retval = "0" Then 'cheque 3 here
                    BaseCurrTotal = FeeCollection.GetDoubleVal(txtChequeTotal3.Text) * ExchRate

                    'If h_Bank3.Value.ToUpper <> "65" OrElse txtBank3.Text.ToUpper <> "BANK TRANSFER" Then
                    '    hfBankAct3.Value = ""
                    'End If

                    retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID, _
                    COLLECTIONTYPE.CHEQUES, BaseCurrTotal, FeeCollection.GetDoubleVal(txtChequeTotal3.Text), txtChqno3.Text, _
                    txtChqDate3.Text, 0, "", h_Bank3.Value, ddlEmirate3.SelectedItem.Value, _
                    stTrans, "", 0, hfBankAct3.Value)
                End If
                If FeeCollection.GetDoubleVal(txtVoucherAmount1.Text) > 0 And retval = "0" Then 'Voucher 1 here
                    BaseCurrTotal = FeeCollection.GetDoubleVal(txtVoucherAmount1.Text) * ExchRate

                    retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID, _
                    COLLECTIONTYPE.VOUCHER_REDEMPTION, BaseCurrTotal, FeeCollection.GetDoubleVal(txtVoucherAmount1.Text), txtVoucherNo1.Text, _
                    Format(Date.Now, OASISConstants.DateFormat), 0, "", ddlVoucherType.SelectedValue, 0, _
                    stTrans, "", 0, "", ddlVoucherType.SelectedValue, txtVoucherRefNo1.Text.Trim, txtVoucherCardHolder1.Text.Trim, txtVoucherIssueDate1.SelectedDate, txtVoucherExpiry1.SelectedDate)
                End If
                If FeeCollection.GetDoubleVal(txtRewardsAmount.Text) > 0 And retval = "0" Then 'Rewards 1 here
                    BaseCurrTotal = FeeCollection.GetDoubleVal(txtRewardsAmount.Text) * ExchRate

                    retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID, COLLECTIONTYPE.REWARDS_REDEMPTION, _
                       BaseCurrTotal, BaseCurrTotal, "", txtFrom.Text, 0, "", "", "", stTrans)
                End If


                If retval = "0" Then
                    retval = FeeCollection.F_SaveFEECOLLALLOCATION_D_Normal(uscStudentPicker.STU_BSU_ID, _
                    str_new_FCL_ID, stTrans)
                End If
                If retval = "0" Then
                    Dim bEnquiry As Boolean = IIf(uscStudentPicker.STU_TYPE = "S", False, True)
                    retval = FeeCollection.F_SettleFee_Normal(uscStudentPicker.STU_BSU_ID, txtFrom.Text, _
                    STU_ID, bEnquiry, stTrans)
                End If
                If retval = "0" Then
                    retval = FeeCollection.F_SAVEFEEDISCOUNTTOADJUSTMENT_Normal(str_new_FCL_ID, stTrans)
                End If

                'Added by jacob on 09/Apr/2014
                'ViewState("DiscntXML")
                Try
                    If retval = "0" And Not ViewState("DiscntXML") Is Nothing AndAlso ViewState("DiscntXML").ToString <> "" Then
                        Dim Disc_XML As String = ViewState("DiscntXML").ToString
                        FeeCollection.F_SAVEFEE_DISCOUNT_DETAIL(str_new_FCL_ID, Disc_XML, stTrans)
                    End If
                Catch ex As Exception
                    'retval = "0"
                End Try

                If Not chkRemember.Checked And retval = "0" Then
                    If ViewState("FCL_IDS") Is Nothing Then
                        retval = FeeCollection.F_SAVECHEQUEDATA_OASIS(uscStudentPicker.STU_BSU_ID, _
                        str_new_FCL_ID, Session("sUsr_name"), stTrans)
                    Else
                        retval = FeeCollection.F_SAVECHEQUEDATA_OASIS(uscStudentPicker.STU_BSU_ID, _
                        ViewState("FCL_IDS").ToString & "|" & str_new_FCL_ID, Session("sUsr_name"), stTrans)
                    End If
                End If
                '----------------------------GEMS Rewards redemption here------------------------------
                If FeeCollection.GetDoubleVal(txtRewardsAmount.Text) > 0 And retval = "0" Then
                    BaseCurrTotal = FeeCollection.GetDoubleVal(txtRewardsAmount.Text) * ExchRate
                    retval = SAVE_COLLECTION_INITIATED(stTrans, str_new_FCL_ID)
                End If

                If retval = "0" Then
                    If chkRemember.Checked Then
                        If ViewState("FCL_IDS") Is Nothing Then
                            ViewState("FCL_IDS") = str_new_FCL_ID
                        Else
                            ViewState("FCL_IDS") = ViewState("FCL_IDS").ToString & "|" & str_new_FCL_ID
                        End If
                    Else
                        ViewState("FCL_IDS") = Nothing
                    End If
                    stTrans.Commit()
                    ViewState("recno") = str_NEW_FCL_RECNO
                    Session("FeeCollection").rows.clear()
                    SetVersionforReceipt(str_NEW_FCL_RECNO)
                    h_print.Value = "?type=REC&id=" + Encr_decrData.Encrypt(str_NEW_FCL_RECNO) & _
                   "&bsu_id=" & Encr_decrData.Encrypt(ddlBusinessunit.SelectedValue) & _
                   "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & _
                   "&isenq=" & Encr_decrData.Encrypt(IIf(uscStudentPicker.STU_TYPE = "S", False, True)) & _
                   "&iscolln=" & Encr_decrData.Encrypt(True) & "&isexport=0"
                    gvFeeCollection.DataBind()
                    SetAdvanceFeeBreakupInControls()
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_NEW_FCL_RECNO, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                    '   Session("ReportSource") = FeeCollection.PrintReceipt(str_NEW_FCL_RECNO, Session("sBsuid"), rbEnquiry.Checked, Session("sUsr_name"), True)
                    'lblError.Text = getErrorMessage("0")
                    '   ShowMessage(getErrorMessage(0), False)
                    usrMessageBar.ShowNotification(getErrorMessage(0), UserControls_usrMessageBar.WarningType.Success)
                    clear_All()
                    '  SetCollectionBank()
                    Gridbind_Feedetails()
                    Dim bbClearSelection As Boolean = chkCoBrand.Checked
                    chkCoBrand.Checked = False
                    chkChargableCollection.Checked = False
                    Set_COBrand_Payment_controls(bbClearSelection)
                    If ddlBusinessunit.Items.Count > 0 Then
                        If ddlBusinessunit.SelectedValue <> Session("sBsuId") Then
                            ddlBusinessunit.Items.FindItemByValue(Session("sBsuId")).Selected = True
                            InitialiseCompnents()
                        End If
                    End If

                    ViewState("datamode") = "add"
                    'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    usrRibbonControlPanel.SetButtons(ViewState("menu_rights"), ViewState("datamode"))
                Else
                    stTrans.Rollback()
                    If usrMessageBar.InnerHtml <> "" Then
                        ' ShowMessage(getErrorMessage(retval) & "<br />" & usrMessageBar.InnerHtml)
                        usrMessageBar.ShowNotification(getErrorMessage(retval) & "<br />" & usrMessageBar.InnerHtml, UserControls_usrMessageBar.WarningType.Danger)
                    Else
                        '  ShowMessage(getErrorMessage(retval))
                        usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                    End If
                    'lblError.Text = getErrorMessage(retval)
                End If
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                If usrMessageBar.InnerHtml <> "" Then
                    ' ShowMessage(getErrorMessage(retval) & "<br />" & usrMessageBar.InnerHtml)
                    usrMessageBar.ShowNotification(getErrorMessage(retval) & "<br />" & usrMessageBar.InnerHtml, UserControls_usrMessageBar.WarningType.Danger)
                Else
                    ' ShowMessage(getErrorMessage(retval))
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'lblError.Text = getErrorMessage(1000)
            If usrMessageBar.InnerHtml <> "" Then
                ' ShowMessage(getErrorMessage(1000) & "<br />" & usrMessageBar.InnerHtml)
                usrMessageBar.ShowNotification(getErrorMessage(1000) & "<br />" & usrMessageBar.InnerHtml, UserControls_usrMessageBar.WarningType.Danger)
            Else
                'ShowMessage(getErrorMessage(1000))
                usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
            End If
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Sub SetVersionforReceipt(ByVal RecNo As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FCL_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = ddlBusinessunit.SelectedValue
        pParms(1) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 20)
        pParms(1).Value = RecNo
        pParms(2) = New SqlClient.SqlParameter("@bENQUIRY", SqlDbType.Bit)
        pParms(2).Value = IIf(uscStudentPicker.STU_TYPE = "S", False, True) 'rbEnquiry.Checked  'Not used
        pParms(3) = New SqlClient.SqlParameter("@bCOLLECTION", SqlDbType.Bit)
        pParms(3).Value = 0
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.GetFeeReceipt", pParms)

        Dim BSU_CURRENCY As String = ""
        Dim BSU_bBSU_Version_Enabled As Boolean
        BSU_bBSU_Version_Enabled = False
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Columns.Contains("BSU_bBSU_Version_Enabled") Then
                BSU_bBSU_Version_Enabled = ds.Tables(0).Rows(0)("BSU_bBSU_Version_Enabled")
                If BSU_bBSU_Version_Enabled Then
                    Dim BSU_LOGO_VER_NO As String, BSU_VER_NO As String
                    BSU_LOGO_VER_NO = "0"
                    BSU_VER_NO = "0"
                    BSU_LOGO_VER_NO = CInt(ds.Tables(0).Rows(0)("BSU_LOGO_VER_NO").ToString)
                    BSU_VER_NO = CInt(ds.Tables(0).Rows(0)("BSU_VER_NO").ToString)
                    Dim sqlParam(1) As SqlParameter
                    sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", ddlBusinessunit.SelectedValue, SqlDbType.VarChar)
                    sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_VER_NO", BSU_VER_NO, SqlDbType.Int)
                    Dim dtBSUDetail As New DataTable
                    dtBSUDetail = Mainclass.getDataTable("SP_GET_BUSINESSUNIT_DETAILS", sqlParam, ConnectionManger.GetOASISConnectionString)
                    If dtBSUDetail.Rows(0)("BUS_bFEE_TAXABLE").ToString() = "True" Then
                        hfTaxable.Value = "1"
                    Else
                        hfTaxable.Value = "0"
                    End If
                End If
            End If
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            chkCoBrand.Checked = False
            chkChargableCollection.Checked = False
            Set_COBrand_Payment_controls()
            clear_All()
            ViewState("datamode") = "view"
            'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        clear_All()
        'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Sub setStudntData()
        Gridbind_Feedetails()
    End Sub

    Sub Gridbind_Feedetails()
        gvFeeCollection.DataSource = Nothing
        gvFeeCollection.DataBind()

        If IsDate(txtFrom.Text) And STU_ID <> 0 Then
            Dim dt As New DataTable
            lbNotes.OnClientClick = "PayTransportFee('" & Request.Url.ToString.Replace("feeCollection.aspx", "FeeStudentNotes.aspx") & "&stuno=" & uscStudentPicker.STU_NO & "&acdid=" & uscStudentPicker.STU_ACD_ID & "',2);return false;"
            dt = FeeCollection.F_GetFeeDetailsFOrCollection(txtFrom.Text, _
            STU_ID, uscStudentPicker.STU_TYPE, uscStudentPicker.STU_BSU_ID, uscStudentPicker.STU_ACD_ID, nxtAcaFee.Checked)

            If Not dt.Columns.Contains("PAY_TYPE_ID") Then
                Dim dcAPD As DataColumn = New DataColumn("PAY_TYPE_ID", System.Type.GetType("System.String"))
                dt.Columns.Add(dcAPD)
            End If
            If Not dt.Columns.Contains("FCS_PAY_TYPE") Then
                Dim dcFCS_PAY_TYPE As DataColumn = New DataColumn("FCS_PAY_TYPE", System.Type.GetType("System.String"))
                dt.Columns.Add(dcFCS_PAY_TYPE)
            End If

            gvFeeCollection.DataSource = dt
            Session("FeeCollection") = dt
            gvFeeCollection.DataBind()
            SetAdvanceFeeBreakupInControls()
        End If
        Set_GridTotal(False)

    End Sub
    Sub ShowNextAcademic()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim StoreProcedureQry As String = "dbo.GET_NEXTACADEMICPAYMENT_LIST @BSU_ID = '" & uscStudentPicker.STU_BSU_ID & "',@STU_ID = '" & uscStudentPicker.STU_ID & "',@STU_TYPE = '" & uscStudentPicker.STU_TYPE & "',@STU_ACD_ID = '" & uscStudentPicker.STU_ACD_ID & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(StoreProcedureQry, connection)
        command.CommandType = CommandType.Text

        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        While reader.Read
            If reader("SHOWCHECK") = 1 Then
                nxtAcaFee.Visible = True
                lblNacdFeetext.Text = reader("DISPLAYMESSAGE")
                lblNacdFeetext.Visible = False
            Else
                nxtAcaFee.Visible = False
                lblNacdFeetext.Text = reader("DISPLAYMESSAGE")
                lblNacdFeetext.Visible = True
            End If
        End While
        reader.Close()
    End Sub
    Sub Set_FCGridAmount()

    End Sub

    Protected Sub gvFeeCollection_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFeeCollection.RowDataBound
        Try
            'Dim pce As AjaxControlToolkit.PopupControlExtender = TryCast(e.Row.FindControl("PopupControlExtender1"), AjaxControlToolkit.PopupControlExtender)
            'pce.DynamicControlID = "pnlDiscntDtl"
            'Dim lbDiscout As ImageButton = DirectCast(e.Row.FindControl("lbDiscout"), ImageButton)
            'Dim behaviorID As String = "pce_" & e.Row.RowIndex 'Convert.ToString(e.Row.RowIndex)
            'pce.BehaviorID = behaviorID
            'Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup(); return false;", behaviorID)
            'lbDiscout.Attributes.Add("onClick", OnMouseOverScript)
            'pce.Enabled = True

            Dim AMOUNT As Double = 0
            Dim lblTAX As Label = CType(e.Row.FindControl("lblTAX"), Label)
            'Dim lblFeeTotal As Label = CType(e.Row.FindControl("lblFeeTotal"), Label)
            Dim txtAmountToPay As TextBox = CType(e.Row.FindControl("txtAmountToPay"), TextBox)
            Dim txtAmountToPayFC As TextBox = CType(e.Row.FindControl("txtAmountToPayFC"), TextBox)

            If txtAmountToPay IsNot Nothing And txtAmountToPayFC IsNot Nothing Then

                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    txtAmountToPayFC.Text = FeeCollection.GetDoubleVal(txtAmountToPay.Text / lblExgRate.Text)
                    AMOUNT = FeeCollection.GetDoubleVal(txtAmountToPay.Text / lblExgRate.Text)
                Else
                    txtAmountToPayFC.Text = FeeCollection.GetDoubleVal(txtAmountToPay.Text)
                    AMOUNT = FeeCollection.GetDoubleVal(txtAmountToPay.Text)
                End If

            End If

            If e.Row.RowType = DataControlRowType.DataRow And bGSTEnabled = True Then
                Dim FEE_ID As Int32 = Me.gvFeeCollection.DataKeys(e.Row.RowIndex)("FEE_ID")
                Dim ACD_ID As Int32 = uscStudentPicker.STU_ACD_ID
                Dim STU_ID As Int32 = STU_ID
                lblTAX.Text = FormatNumber(GetTAXAmount(FEE_ID, ACD_ID, STU_ID, FeeCollection.GetDoubleVal(txtAmountToPayFC.Text)), 2, , Microsoft.VisualBasic.TriState.UseDefault)
                'lblFeeTotal.Text = FeeCollection.GetDoubleVal(lblTAX.Text) + FeeCollection.GetDoubleVal(txtAmountToPayFC.Text)
                'lblFeeTotal.Text = FormatNumber(lblFeeTotal.Text, 2, , , TriState.True)
            End If


        Catch ex As Exception

        End Try
    End Sub
    Private Function GetTAXAmount(ByVal FEE_ID As Int32, ByVal ACD_ID As Int32, ByVal STU_ID As Int32, ByVal AMOUNT As Double) As Double
        GetTAXAmount = 0
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        Dim dsSummary As New DataSet
        Dim sqlParam(9) As SqlParameter
        Dim retval As String = ""
        Dim TAX As Double = 0
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            sqlParam(0) = New SqlParameter("@ReturnValue", SqlDbType.VarChar)
            sqlParam(0).Direction = ParameterDirection.ReturnValue

            sqlParam(1) = New SqlParameter("@STU_ID", SqlDbType.Int)
            sqlParam(1).Value = STU_ID
            sqlParam(2) = New SqlParameter("@BSU_ID", SqlDbType.VarChar)
            sqlParam(2).Value = ddlBusinessunit.SelectedValue
            sqlParam(3) = New SqlParameter("@ACD_ID", SqlDbType.Int)
            sqlParam(3).Value = ACD_ID
            sqlParam(4) = New SqlParameter("@FEE_ID", SqlDbType.Int)
            sqlParam(4).Value = FEE_ID
            sqlParam(5) = New SqlParameter("@DATE", SqlDbType.DateTime)
            sqlParam(5).Value = Me.txtFrom.Text
            sqlParam(6) = New SqlParameter("@AMOUNT", SqlDbType.Decimal)
            sqlParam(6).Value = AMOUNT
            sqlParam(7) = New SqlParameter("@TAX", SqlDbType.Float)
            sqlParam(7).Direction = ParameterDirection.Output
            sqlParam(8) = New SqlParameter("@STU_TYPE", SqlDbType.VarChar)
            sqlParam(8).Value = uscStudentPicker.STU_TYPE 'IIf(rbEnrollment.Checked, "S", "E")

            Mainclass.ExecuteParamQRY(objConn, stTrans, "FEES.GET_TAX_FEECOLLECTION", sqlParam)
            retval = sqlParam(0).Value
            GetTAXAmount = sqlParam(7).Value

        Catch ex As Exception
            GetTAXAmount = 0
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


    End Function
    Public Sub AddCreditCardCharges()
        Dim dr As DataRow
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If IsNumeric(txtCrCardCharges.Text) Then
            If FeeCollection.GetDoubleVal(txtCrCardCharges.Text) > 0 Then
                'FEE_ID, FEE_DESCR, Opening, CurrentCharge, Net,Amount
                'FSH_ID, FSH_DATE, FEE_ID, FEE_DESCR, AMOUNT, FSH_naRRATION
                For Each gvr As GridViewRow In gvFeeCollection.Rows
                    Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)

                    If Not lblFSR_FEE_ID Is Nothing Then
                        If lblFSR_FEE_ID.Text = 162 Then
                            'lblError.Text = "Fee repeating!!!"
                            '   ShowMessage("Fee repeating!!!")
                            usrMessageBar.ShowNotification("Fee repeating!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                    End If
                Next
                dr = Session("FeeCollection").NewRow
                dr("FEE_ID") = 162
                dr("FEE_DESCR") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT FEE_DESCR FROM FEES.FEES_M WITH(NOLOCK) WHERE FEE_ID=162")
                dr("CLOSING") = 0
                dr("Amount") = txtCrCardCharges.Text
                dr("AUTOYN") = "0"
                dr("Discount") = "0"
                If ChkChgPost.Checked Then
                    dr("AUTOYN") = "1"
                    dr("CLOSING") = txtCrCardCharges.Text
                End If
                dr("FEE_bBlockPaynow") = 0
                Session("FeeCollection").rows.add(dr)
                gvFeeCollection.DataSource = Session("FeeCollection")
                'txtCrCardCharges.Text = ""

                gvFeeCollection.DataBind()
                SetAdvanceFeeBreakupInControls()
                Set_GridTotal(False)
                smScriptManager.SetFocus(txtCashTotal)
            Else
                ' ShowMessage("Invalid Amount") 'lblError.Text = "Invalid Amount" 
                usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
                smScriptManager.SetFocus(txtCrCardCharges)
            End If
        Else
            ' ShowMessage("Invalid Amount") 'lblError.Text = "Invalid Amount"
            smScriptManager.SetFocus(txtCrCardCharges)
        End If

    End Sub
    Sub Set_GridTotal(ByVal DonotCalulateDisc As Boolean)
        If chkDisableDiscount.Checked Then '---donot calculate discount if true, it overwrites the value of valiable as true. Added by Jacob on 02/Jun/2020.
            DonotCalulateDisc = True
        End If


        Dim dAmount, dFCAmount As Decimal
        dAmount = 0 : dFCAmount = 0
        Dim dDueAmount As Decimal = 0
        Dim dDiscountAmount As Decimal = 0
        pceDiscountDetail.Enabled = False
        Dim ForeignCurr As Boolean = False
        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
            ForeignCurr = True
        End If
        ''--------check and add credit card charges to grid, added by Jacob on 20/oct/2013
        'If IsNumeric(txtCrCardCharges.Text) Then
        '    If FeeCollection.GetDoubleVal(txtCrCardCharges.Text) > 0 Then
        '        AddCreditCardCharges()
        '    End If
        'End If
        '--------checking for advance enrolment or admission paid
        Dim EnrollFee, AdmissionFee, OtherCharge As Double
        EnrollFee = 0 : AdmissionFee = 0 : OtherCharge = 0
        If uscStudentPicker.STU_TYPE = "S" Then ' rbEnrollment.Checked Then
            If chkCoBrand.Checked Then
                For Each gvro As GridViewRow In gvFeeCollection.Rows
                    Dim FSR_FEE_ID As Label = CType(gvro.FindControl("lblFSR_FEE_ID"), Label)
                    Dim tAmountToPay As TextBox = CType(gvro.FindControl("txtAmountToPay"), TextBox)
                    Dim tAmountToPayFC As TextBox = CType(gvro.FindControl("txtAmountToPayFC"), TextBox)
                    If Not FSR_FEE_ID.Text Is Nothing AndAlso (FSR_FEE_ID.Text = "145") Then
                        AdmissionFee += FeeCollection.GetDoubleVal(IIf(ForeignCurr = True, tAmountToPayFC.Text, tAmountToPay.Text))
                    End If
                    If Not FSR_FEE_ID.Text Is Nothing AndAlso (FSR_FEE_ID.Text = "145" Or FSR_FEE_ID.Text = "146" Or FSR_FEE_ID.Text = "300" Or FSR_FEE_ID.Text = "309") Then
                        EnrollFee += FeeCollection.GetDoubleVal(IIf(ForeignCurr = True, tAmountToPayFC.Text, tAmountToPay.Text))
                    End If
                Next
            End If
        End If
        '----------------------------------------------------------
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
            Dim txtAmountToPayFC As TextBox = CType(gvr.FindControl("txtAmountToPayFC"), TextBox)
            Dim ChkPost As CheckBox = CType(gvr.FindControl("ChkPost"), CheckBox)
            Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
            Dim lblDiscount As Label = CType(gvr.FindControl("lblDiscount"), Label)
            Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)
            Dim lbCancel As LinkButton = CType(gvr.FindControl("lbCancel"), LinkButton)
            Dim lbDiscDetail As ImageButton = CType(gvr.FindControl("lbDiscout"), ImageButton)
            Dim txtDiscount As TextBox = CType(gvr.FindControl("txtDiscount"), TextBox)
            Dim h_OtherCharge As HiddenField = CType(gvr.FindControl("h_OtherCharge"), HiddenField)
            Dim bBlockPayNow As Boolean = False
            If Not Me.gvFeeCollection.DataKeys(gvr.RowIndex)("FEE_bBlockPaynow") Is Nothing Then
                bBlockPayNow = Convert.ToBoolean(Me.gvFeeCollection.DataKeys(gvr.RowIndex)("FEE_bBlockPaynow"))
            End If


            If Not txtAmountToPay Is Nothing Then
                If ForeignCurr = True Then
                    txtAmountToPay.Enabled = False
                    txtAmountToPayFC.Enabled = True
                Else
                    txtAmountToPay.Enabled = True
                    txtAmountToPayFC.Enabled = False
                End If

                txtAmountToPay.Enabled = Not bBlockPayNow
                txtAmountToPayFC.Enabled = Not bBlockPayNow

                If IsNumeric(lblAmount.Text) Then
                    dDueAmount = dDueAmount + FeeCollection.GetDoubleVal(lblAmount.Text)
                End If
                If IsNumeric(txtAmountToPay.Text) Then
                    dAmount = dAmount + FeeCollection.GetDoubleVal(txtAmountToPay.Text)
                Else
                    txtAmountToPay.Text = "#,##0.00"
                End If

                If IsNumeric(txtAmountToPayFC.Text) Then
                    dFCAmount = dFCAmount + FeeCollection.GetDoubleVal(txtAmountToPayFC.Text)
                End If

                If gvr.RowIndex < Session("FeeCollection").rows.count Then
                    Session("FeeCollection").rows(gvr.RowIndex)(("Amount")) = txtAmountToPay.Text
                    If gvFeeCollection.Columns(10).Visible Then 'Manual discount
                        If IsNumeric(txtDiscount.Text) Then
                            Session("FeeCollection").rows(gvr.RowIndex)(("Discount")) = txtDiscount.Text
                        Else
                            txtDiscount.Text = "0"
                            Session("FeeCollection").rows(gvr.RowIndex)(("Discount")) = 0
                        End If
                    Else
                        Session("FeeCollection").rows(gvr.RowIndex)(("Discount")) = lblDiscount.Text
                    End If
                End If
                If Session("FeeCollection").rows(gvr.RowIndex)(("AUTOYN")) = 1 Then
                    ChkPost.Checked = True
                End If

                If gvFeeCollection.Columns(9).Visible And (lblFSR_FEE_ID.Text.Trim = "5") And lnkDiscount.Visible And Not lbCancel.Visible Then
                    If DonotCalulateDisc = False Then
                        Dim decDiscountamt As Decimal = 0
                        'If rbEnrollment.Checked Then
                        '    FeeCollection.DIS_FindDiscount(STU_ID, "S", txtFrom.Text, Session("sBsuid"), "", txtAmountToPay.Text, decDiscountamt)
                        'Else
                        FeeCollection.DIS_FindDiscount(STU_ID, uscStudentPicker.STU_TYPE, txtFrom.Text, uscStudentPicker.STU_BSU_ID, "", txtAmountToPay.Text, decDiscountamt)
                        'End If
                        lblDiscount.Text = Format(decDiscountamt, "#,##0.00")
                        'txtAmountToPay.Text = FeeCollection.GetDoubleVal(txtAmountToPay.Text) - FeeCollection.GetDoubleVal(lblDiscount.Text)
                    End If
                End If
                If gvFeeCollection.Columns(9).Visible And (lblFSR_FEE_ID.Text.Trim = "5") Then 'And (Not lbCancel.Visible Or EnrollorAdmsnFee > 0) Then
                    Dim p_Amount As Double = 0

                    p_Amount = FeeCollection.GetDoubleVal(IIf(ForeignCurr = True, IIf(IsNumeric(txtAmountToPayFC.Text), FeeCollection.GetDoubleVal(txtAmountToPayFC.Text), 0), IIf(IsNumeric(txtAmountToPay.Text), FeeCollection.GetDoubleVal(txtAmountToPay.Text), 0))) + EnrollFee + IIf(IsNumeric(lblDiscount.Text), FeeCollection.GetDoubleVal(lblDiscount.Text), 0) - IIf(IsNumeric(h_OtherCharge.Value), FeeCollection.GetDoubleVal(h_OtherCharge.Value), 0)

                    If DonotCalulateDisc = False Then
                        Dim decDiscountamt As Decimal = 0
                        If uscStudentPicker.STU_TYPE = "S" Then ' rbEnrollment.Checked Then
                            Dim objclsFeecoll As New clsFeeCollection
                            objclsFeecoll.STU_ID = STU_ID
                            objclsFeecoll.STU_TYPE = "S"
                            objclsFeecoll.CRR_ID = ddCreditcard.SelectedItem.Value
                            objclsFeecoll.FEE_ID = lblFSR_FEE_ID.Text
                            objclsFeecoll.AMOUNT = p_Amount
                            objclsFeecoll.ADMISSION_FEE = AdmissionFee
                            If chkCoBrand.Checked Then
                                objclsFeecoll.CPM_ID = 3
                                objclsFeecoll.CLT_ID = COLLECTIONTYPE.CREDIT_CARD
                            Else
                                objclsFeecoll.CPM_ID = 0
                                objclsFeecoll.CLT_ID = IIf(chkChargableCollection.Checked, COLLECTIONTYPE.CREDIT_CARD, 1)
                            End If
                            objclsFeecoll.GetDiscountedFeesPayable() '-------Calculating discount here
                            '-----------setting out put values-------
                            decDiscountamt = objclsFeecoll.DISCOUNT
                            OtherCharge = objclsFeecoll.OTHER_CHARGE
                            ViewState("DiscntXML") = objclsFeecoll.DISCOUNT_XML
                            dtDiscount = objclsFeecoll.DISCOUNT_DT
                        End If
                        If decDiscountamt <> 0 Then
                            txtAmountToPay.Attributes.Add("readonly", "readonly")
                            lbCancel.Visible = True
                            BindDiscountDetail()
                            pceDiscountDetail.Enabled = True
                        Else
                            Me.gvDiscDt.DataSource = ""
                            Me.gvDiscDt.DataBind()
                        End If

                        If ForeignCurr = True Then
                            txtAmountToPayFC.Text = FeeCollection.GetDoubleVal(txtAmountToPayFC.Text) - decDiscountamt + IIf(IsNumeric(lblDiscount.Text), FeeCollection.GetDoubleVal(lblDiscount.Text), 0) + OtherCharge - IIf(IsNumeric(h_OtherCharge.Value), FeeCollection.GetDoubleVal(h_OtherCharge.Value), 0)
                            If txtAmountToPayFC.Text < 0 Then
                                txtAmountToPayFC.Text = 0
                                decDiscountamt = 0
                                ViewState("DiscntXML") = ""
                            Else
                                dFCAmount -= decDiscountamt
                            End If
                        Else
                            txtAmountToPay.Text = Format(FeeCollection.GetDoubleVal(txtAmountToPay.Text) - decDiscountamt + IIf(IsNumeric(lblDiscount.Text), FeeCollection.GetDoubleVal(lblDiscount.Text), 0), "#,##0.00") + OtherCharge - IIf(IsNumeric(h_OtherCharge.Value), FeeCollection.GetDoubleVal(h_OtherCharge.Value), 0)
                            If txtAmountToPay.Text < 0 Then
                                txtAmountToPay.Text = 0
                                decDiscountamt = 0
                                ViewState("DiscntXML") = ""
                            Else
                                dAmount -= decDiscountamt
                            End If

                        End If
                        h_OtherCharge.Value = OtherCharge
                        lblDiscount.Text = Format(decDiscountamt, "#,##0.00")
                    End If
                End If
            End If
            If gvFeeCollection.Columns(9).Visible And IsNumeric(lblDiscount.Text) Then
                dDiscountAmount = dDiscountAmount + FeeCollection.GetDoubleVal(lblDiscount.Text)
            Else
                lblDiscount.Text = "#,##0.00"
            End If
            If gvFeeCollection.Columns(10).Visible And IsNumeric(txtDiscount.Text) Then
                dDiscountAmount = dDiscountAmount + FeeCollection.GetDoubleVal(txtDiscount.Text)
            Else
                txtDiscount.Text = "#,##0.00"
            End If
        Next
        '--------------re-calculating grid totals, added by jacob on 16/may2013------------
        dAmount = 0
        dFCAmount = 0
        Dim TAX As Double = 0
        Dim AMOUNT As New Double
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            AMOUNT = 0
            Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
            Dim txtAmountToPayFC As TextBox = CType(gvr.FindControl("txtAmountToPayFC"), TextBox)
            Dim lblTAX As Label = CType(gvr.FindControl("lblTAX"), Label)

            If IsNumeric(txtAmountToPay.Text) Then
                dAmount = dAmount + FeeCollection.GetDoubleVal(txtAmountToPay.Text)
            Else
                txtAmountToPay.Text = "#,##0.00"
            End If
            If ForeignCurr = True Then
                AMOUNT = FeeCollection.GetDoubleVal(txtAmountToPayFC.Text)
            Else
                AMOUNT = FeeCollection.GetDoubleVal(txtAmountToPay.Text)
            End If

            If bGSTEnabled = True Then
                Dim FEE_ID As Int32 = Me.gvFeeCollection.DataKeys(gvr.RowIndex)("FEE_ID")
                Dim ACD_ID As Int32 = uscStudentPicker.STU_ACD_ID
                Dim STU_ID As Int32 = uscStudentPicker.STU_ID
                lblTAX.Text = FormatNumber(GetTAXAmount(FEE_ID, ACD_ID, STU_ID, AMOUNT), 2, , Microsoft.VisualBasic.TriState.UseDefault)
                'lblFeeTotal.Text = FormatNumber(FeeCollection.GetDoubleVal(lblTAX.Text) + FeeCollection.GetDoubleVal(txtAmountToPay.Text), 2, , TriState.True)
                TAX += FeeCollection.GetDoubleVal(lblTAX.Text)
            End If

            If IsNumeric(txtAmountToPayFC.Text) Then
                dFCAmount = dFCAmount + FeeCollection.GetDoubleVal(txtAmountToPayFC.Text)
            End If

        Next
        Me.txtTax.Text = FormatNumber(TAX, 2, , Microsoft.VisualBasic.TriState.False, Microsoft.VisualBasic.TriState.UseDefault)

        txtDue.Text = Format(dDueAmount - dAmount, "#,##0.00")
        txtOutstanding.Text = Format(dDueAmount, "#,##0.00")
        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
            txtTotal.Text = Format(dFCAmount, "#,##0.00")
            lblDueFC.Text = Format(txtDue.Text / lblExgRate.Text, "#,##0.00").ToString & ddCurrency.SelectedItem.Text
            lblOutstandingFC.Text = Format(txtOutstanding.Text / lblExgRate.Text, "#,##0.00").ToString & "(" & ddCurrency.SelectedItem.Text & ")"
            txtOutstanding.Text &= "(" & Session("BSU_CURRENCY") & ") "
        Else
            txtTotal.Text = Format(dAmount, "#,##0.00")
            lblDueFC.Text = ""
        End If

        If chkCoBrand.Checked Then
            txtCCTotal.Text = txtTotal.Text
        End If
        lblTotalDiscount.Text = "Discount : " & Format(dDiscountAmount, "#,##0.00")

        lbl_OS.Text = txtOutstanding.Text
        lbl_PayingNow.Text = txtTotal.Text
        lbl_TAX.Text = txtTax.Text
        lbl_Discnt.Text = Format(dDiscountAmount, "#,##0.00")
        lbl_Due.Text = txtDue.Text
        txtReceivedTotal.Text = FeeCollection.GetDoubleVal(txtCCTotal.Text) + FeeCollection.GetDoubleVal(txtChequeTotal1.Text) + FeeCollection.GetDoubleVal(txtChequeTotal2.Text) + FeeCollection.GetDoubleVal(txtChequeTotal3.Text) + FeeCollection.GetDoubleVal(txtCashTotal.Text) + FeeCollection.GetDoubleVal(txtVoucherAmount1.Text) + FeeCollection.GetDoubleVal(txtRewardsAmount.Text)
        Dim dTotalReceived As Double = 0, dBalance As Double = 0
        If txtReceivedTotal.Text <> "" AndAlso IsNumeric(txtReceivedTotal.Text) AndAlso FeeCollection.GetDoubleVal(txtReceivedTotal.Text) > 0 Then
            dTotalReceived = FeeCollection.GetDoubleVal(txtReceivedTotal.Text)
        End If
        If IsNumeric(txtTotal.Text) Then
            dBalance = Math.Abs(dTotalReceived - FeeCollection.GetDoubleVal(txtTotal.Text))
        End If
        txtBalance.Text = Format(dBalance, "#,##0.00")
    End Sub
    Private Sub BindDiscountDetail()
        Me.gvDiscDt.DataSource = dtDiscount
        Me.gvDiscDt.DataBind()
    End Sub
    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetDynamicContentnew() As String
        Try
            Dim b As New StringBuilder()
            Dim itemDiv As HtmlControls.HtmlGenericControl
            itemDiv = HttpContext.Current.Session("divDetail")
            Dim sw As New StringWriter()
            Dim pnl As Panel
            pnl = itemDiv.FindControl("pnlDiscntDtl")
            'BindAssetDetails(pnl, contextKey, False)
            Dim w As New HtmlTextWriter(sw)
            itemDiv.RenderControl(w)
            Dim s As String = sw.GetStringBuilder().ToString()
            b.Append("<table style='background-color:#f3f3f3; ")
            b.Append("width:1000px; ' >")
            b.Append("<tr><td >")
            b.Append(s)
            b.Append("</td></tr>")
            b.Append("</table>")
            Return b.ToString
        Catch ex As Exception

        End Try
        GetDynamicContentnew = ""
    End Function

    Function check_errors_details() As String
        Dim str_error As String = ""
        If txtCCTotal.Text = "" Then
            txtCCTotal.Text = 0
        End If
        If txtCashTotal.Text = "" Then
            txtCashTotal.Text = 0
        End If
        If txtChequeTotal1.Text = "" Then
            txtChequeTotal1.Text = 0
        End If
        If txtChequeTotal2.Text = "" Then
            txtChequeTotal2.Text = 0
        End If
        If txtChequeTotal3.Text = "" Then
            txtChequeTotal3.Text = 0
        End If
        If txtVoucherAmount1.Text = "" OrElse Not IsNumeric(txtVoucherAmount1.Text) Then
            txtVoucherAmount1.Text = "0.00"
        End If
        If Not IsNumeric(txtCCTotal.Text) Then
            str_error = str_error & "Invalid Credit Card amount <br />"
        Else
            If FeeCollection.GetDoubleVal(txtCCTotal.Text) > 0 And txtCreditno.Text = "" Then
                str_error = str_error & "Invalid Authorisation Code <br />"
            End If
        End If
        If Not IsNumeric(txtCashTotal.Text) Then
            str_error = str_error & "Invalid Cash amount <br />"
        End If
        If Not IsNumeric(txtChequeTotal1.Text) Then
            str_error = str_error & "Invalid Cheque amount 1  <br />"
        Else
            If FeeCollection.GetDoubleVal(txtChequeTotal1.Text) > 0 Then
                If Not IsDate(txtChqDate1.Text) Then
                    str_error = str_error & "Invalid Cheque date 1  <br />"
                End If
                If txtChqno1.Text = "" Then
                    str_error = str_error & "Invalid Cheque no # 1  <br />"
                End If
                If h_Bank1.Value = "" Or txtBank1.Text = "" Then
                    str_error = str_error & "Invalid Bank for Cheque # 1  <br />"
                End If
                If txtBank1.Text = "Bank Transfer" AndAlso hfBankAct1.Value = "" Then
                    str_error = str_error & "Please select a Bank Account for Bank Transfers  <br />"
                End If
                If txtBank1.Text = "Bank Transfer" AndAlso IsDate(txtChqDate1.Text) AndAlso CDate(txtChqDate1.Text) > CDate(DateTime.Now.ToShortDateString) Then
                    str_error = str_error & "Post dated cheques are not allowed for Bank Transfers<br />"
                End If
                'If h_Emirate1.Value = "" Or txtEmirate1.Text = "" Then
                '    str_error = str_error & "Invalid emirate for Cheque # 1  <br />"
                'End If
            End If
        End If
        If Not IsNumeric(txtChequeTotal2.Text) Then
            str_error = str_error & "Invalid Cheque amount 2  <br />"
        Else
            If FeeCollection.GetDoubleVal(txtChequeTotal2.Text) > 0 Then
                If Not IsDate(txtChqDate2.Text) Then
                    str_error = str_error & "Invalid Cheque date 2  <br />"
                End If
                If txtChqno2.Text = "" Then
                    str_error = str_error & "Invalid Cheque no # 2  <br />"
                End If
                If h_Bank2.Value = "" Or txtBank2.Text = "" Then
                    str_error = str_error & "Invalid Bank for Cheque # 2  <br />"
                End If
                If txtBank2.Text = "Bank Transfer" AndAlso hfBankAct2.Value = "" Then
                    str_error = str_error & "Please select a Bank Account  <br />"
                End If
                If txtBank2.Text = "Bank Transfer" AndAlso IsDate(txtChqDate2.Text) AndAlso CDate(txtChqDate2.Text) > CDate(DateTime.Now.ToShortDateString) Then
                    str_error = str_error & "Post dated cheques are not allowed for Bank Transfers<br />"
                End If
                'If h_Emirate2.Value = "" Or txtEmirate2.Text = "" Then
                '    str_error = str_error & "Invalid emirate for Cheque # 2  <br />"
                'End If
            End If
        End If
        If Not IsNumeric(txtChequeTotal3.Text) Then
            str_error = str_error & "Invalid Cheque amount 3  <br />"
        Else
            If FeeCollection.GetDoubleVal(txtChequeTotal3.Text) > 0 Then
                If Not IsDate(txtChqDate3.Text) Then
                    str_error = str_error & "Invalid Cheque date 3  <br />"
                End If
                If txtChqno3.Text = "" Then
                    str_error = str_error & "Invalid Cheque no # 3  <br />"
                End If
                If h_Bank3.Value = "" Or txtBank3.Text = "" Then
                    str_error = str_error & "Invalid Bank for Cheque # 3  <br />"
                End If
                If txtBank3.Text = "Bank Transfer" AndAlso hfBankAct3.Value = "" Then
                    str_error = str_error & "Please select a Bank Account  <br />"
                End If
                If txtBank3.Text = "Bank Transfer" AndAlso IsDate(txtChqDate3.Text) AndAlso CDate(txtChqDate3.Text) > CDate(DateTime.Now.ToShortDateString) Then
                    str_error = str_error & "Post dated cheques are not allowed for Bank Transfers<br />"
                End If
                'If h_Emirate3.Value = "" Or txtEmirate3.Text = "" Then
                '    str_error = str_error & "Invalid emirate for Cheque # 3  <br />"
                'End If
            End If

        End If
        If FeeCollection.GetDoubleVal(txtVoucherAmount1.Text) < 0 Then
            str_error = str_error & "Amount for Voucher 1 is invalid <br />"
        Else
            If FeeCollection.GetDoubleVal(txtVoucherAmount1.Text) > 0 Then
                If Not IsDate(txtVoucherExpiry1.SelectedDate) Then
                    str_error = str_error & "Expiry Date for Voucher 1 is invalid  <br />"
                Else
                    If txtVoucherExpiry1.SelectedDate < CDate(Format(Date.Now, OASISConstants.DataBaseDateFormat)) Then
                        str_error = str_error & "Voucher 1 already expired  <br />"
                    End If
                End If
                If Not IsDate(txtVoucherIssueDate1.SelectedDate) Then
                    str_error = str_error & "Issue Date for Voucher 1 is invalid  <br />"
                End If
                If IsDate(txtVoucherExpiry1.SelectedDate) AndAlso IsDate(txtVoucherIssueDate1.SelectedDate) AndAlso txtVoucherExpiry1.SelectedDate < txtVoucherIssueDate1.SelectedDate Then
                    str_error = str_error & "Expiry date cannot be before Issue date for Voucher 1  <br />"
                End If
                If txtVoucherNo1.Text.Trim = "" Then
                    str_error = str_error & "Voucher No. cannot be empty for Voucher 1  <br />"
                End If
                If txtVoucherCardHolder1.Text = "" Then
                    str_error = str_error & "Please enter the Card Holder Name for Voucher 1  <br />"
                End If
                If txtVoucherRefNo1.Text.Trim = "" Then
                    str_error = str_error & "Please enter the Reference No. for Voucher 1  <br />"
                End If
            End If
        End If
        Return str_error
    End Function

    Sub Set_Total()
        Dim str_error As String = check_errors_details()
        If str_error = "" Then
            Dim dblReceivedAmount, dblTotal, dblCash, dblCreditCardCharge As Decimal
            Dim dblTAX As Double = 0
            If IsNumeric(Me.txtCrCardCharges.Text) AndAlso FeeCollection.GetDoubleVal(Me.txtCrCardCharges.Text) > 0 Then
                dblCreditCardCharge = FeeCollection.GetDoubleVal(Me.txtCrCardCharges.Text)
            Else
                dblCreditCardCharge = 0
            End If
            If IsNumeric(Me.txtTax.Text) AndAlso FeeCollection.GetDoubleVal(Me.txtTax.Text) > 0 Then
                dblTAX = FeeCollection.GetDoubleVal(Me.txtTax.Text)
            End If
            If Not IsNumeric(txtVoucherAmount1.Text) Then
                txtVoucherAmount1.Text = 0
            End If
            If Not IsNumeric(txtVoucherTotal.Text) Then
                txtVoucherTotal.Text = 0
            End If
            If Not IsNumeric(txtRewardsAmount.Text) Then
                txtRewardsAmount.Text = "0"
            End If
            dblReceivedAmount = FeeCollection.GetDoubleVal(txtCCTotal.Text) + FeeCollection.GetDoubleVal(txtCashTotal.Text) + FeeCollection.GetDoubleVal(txtChequeTotal1.Text) _
                + FeeCollection.GetDoubleVal(txtChequeTotal2.Text) + FeeCollection.GetDoubleVal(txtChequeTotal3.Text) + dblCreditCardCharge + FeeCollection.GetDoubleVal(txtVoucherTotal.Text) _
                + FeeCollection.GetDoubleVal(txtRewardsAmount.Text)

            dblTotal = FeeCollection.GetDoubleVal(txtTotal.Text)
            txtReceivedTotal.Text = dblReceivedAmount
            txtReceivedTotal.Text = Format(dblReceivedAmount, "#,##0.00")
            txtCCTotal.Text = Format(FeeCollection.GetDoubleVal(txtCCTotal.Text), "#,##0.00")
            dblCash = FeeCollection.GetDoubleVal(txtCashTotal.Text)
            txtCashTotal.Text = Format(dblCash, "#,##0.00")
            txtChequeTotal1.Text = Format(FeeCollection.GetDoubleVal(txtChequeTotal1.Text), "#,##0.00")
            txtChequeTotal2.Text = Format(FeeCollection.GetDoubleVal(txtChequeTotal2.Text), "#,##0.00")
            txtChequeTotal3.Text = Format(FeeCollection.GetDoubleVal(txtChequeTotal3.Text), "#,##0.00")
            txtVoucherTotal.Text = Format(FeeCollection.GetDoubleVal(txtVoucherTotal.Text), "#,##0.00")
            txtRewardsAmount.Text = Format(FeeCollection.GetDoubleVal(txtRewardsAmount.Text), "#,##0.00")
            txtBalance.Text = 0
            If dblCash > 0 Then
                If dblTotal < dblReceivedAmount Then
                    Dim dblBalance As Decimal = dblReceivedAmount - dblTotal - dblCreditCardCharge - dblTAX
                    If dblBalance > dblCash Then
                        ' ShowMessage("Cannot Refund!!!(Excess amount)") 'lblError.Text = "Cannot Refund!!!(Excess amount)"
                        usrMessageBar.ShowNotification("Cannot Refund!!!(Excess amount)", UserControls_usrMessageBar.WarningType.Danger)
                        txtReceivedTotal.Text = 0
                    Else
                        txtBalance.Text = Format(dblBalance, "#,##0.00")
                    End If
                End If
            Else
                txtBalance.Text = Format(FeeCollection.GetDoubleVal(txtBalance.Text), "#,##0.00")
            End If
        Else
            '  ShowMessage(str_error) 'lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            txtReceivedTotal.Text = 0
        End If
    End Sub

    Protected Sub txtAmountToPay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal(False)
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.SetFocus(sender)
        If sender.Parent.parent.rowindex() = gvFeeCollection.Rows.Count - 1 Then
            smScriptManager.SetFocus(txtTotal)
        Else
            smScriptManager.SetFocus(gvFeeCollection.Rows(sender.Parent.parent.rowindex + 1).FindControl("txtAmountToPay"))
        End If
    End Sub
    Protected Sub txtAmountToPayFC_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal(False)
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.SetFocus(sender)
        If sender.Parent.parent.rowindex() = gvFeeCollection.Rows.Count - 1 Then
            smScriptManager.SetFocus(txtTotal)
        Else
            smScriptManager.SetFocus(gvFeeCollection.Rows(sender.Parent.parent.rowindex + 1).FindControl("txtAmountToPayFC"))
        End If
        If sender.parent.parent IsNot Nothing Then
            Dim txtAmountToPay As TextBox
            txtAmountToPay = sender.parent.parent.FindControl("txtAmountToPay")
            If IsNumeric(sender.Text) And txtAmountToPay IsNot Nothing Then
                txtAmountToPay.Text = Format(sender.Text * lblExgRate.Text, "#,##0.00")
            End If
        End If
    End Sub

    Protected Sub rbEnrollment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles rbEnrollment.CheckedChanged
        'ClearStudentData()
    End Sub

    Protected Sub rbEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles rbEnquiry.CheckedChanged
        ClearStudentData()
    End Sub

    Sub ClearStudentData(Optional ByVal bClearStudentUserControl As Boolean = True)
        h_Student_no.Value = ""

        If bClearStudentUserControl Then
            uscStudentPicker.ClearDetails()
        End If
        uscStudentPicker.STU_BSU_ID = ddlBusinessunit.SelectedValue
        STU_ID = 0
        btnRedeem.Attributes.Remove("onClick")
        Gridbind_Feedetails()
        SetDiscountData_Student()
        ddlActivity.Items.Clear()
        lblActFeeType.Text = ""
        lblActAmount.Text = "0.00"
        lblNacdFeetext.Text = ""
        'lbLDAdate.Text = ""
        'lblStuStatus.Text = ""
        'lbStuDOJ.Text = ""
        'lbTCdate.Text = ""
        'labGrade.Text = ""

    End Sub

    Protected Sub btnAddDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDetails.Click
        If Not IsDate(txtFrom.Text) Then
            ' ShowMessage("Invalid Date!!! The date should be in dd/MMM/yyyy format") 'lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!! The date should be in dd/MMM/yyyy format", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If



        Dim dr As DataRow
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If IsNumeric(txtAmountAdd.Text) Then
            If FeeCollection.GetDoubleVal(txtAmountAdd.Text) > 0 Then
                'FEE_ID, FEE_DESCR, Opening, CurrentCharge, Net,Amount
                'FSH_ID, FSH_DATE, FEE_ID, FEE_DESCR, AMOUNT, FSH_naRRATION
                For Each gvr As GridViewRow In gvFeeCollection.Rows
                    Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)

                    If Not lblFSR_FEE_ID Is Nothing Then
                        If lblFSR_FEE_ID.Text = ddFeetype.SelectedItem.Value Then
                            ' ShowMessage("Fee repeating!!!") 'lblError.Text = "Fee repeating!!!"
                            usrMessageBar.ShowNotification("Fee repeating!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                    End If
                Next
                dr = Session("FeeCollection").NewRow
                dr("FEE_ID") = ddFeetype.SelectedItem.Value
                dr("FEE_DESCR") = ddFeetype.SelectedItem.Text
                dr("CLOSING") = 0
                dr("Amount") = txtAmountAdd.Text
                dr("AUTOYN") = "0"
                dr("Discount") = "0"
                If ChkChgPost.Checked Then
                    dr("AUTOYN") = "1"
                    dr("CLOSING") = txtAmountAdd.Text
                End If
                dr("FEE_bBlockPaynow") = 0
                Session("FeeCollection").rows.add(dr)
                gvFeeCollection.DataSource = Session("FeeCollection")
                txtAmountAdd.Text = "0.00"
                txtAmount.Text = "0.00"
                txtVATAmount.Text = "0.00"
                rblVAT.SelectedValue = Nothing

                gvFeeCollection.DataBind()
                SetAdvanceFeeBreakupInControls()
                Set_GridTotal(False)
                smScriptManager.SetFocus(txtCashTotal)
                tr_AddFee.Visible = Not tr_AddFee.Visible
                tr_AddFee1.Visible = Not tr_AddFee1.Visible
            Else
                ' ShowMessage("Invalid Amount") 'lblError.Text = "Invalid Amount"
                usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
                smScriptManager.SetFocus(txtAmountAdd)
            End If
        Else
            ' ShowMessage("Invalid Amount") 'lblError.Text = "Invalid Amount"
            usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
            smScriptManager.SetFocus(txtAmountAdd)
        End If
    End Sub

    Protected Sub UsrSelStudent1_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles UsrSelStudent1.StudentNoChanged
        'Try
        '    chkCoBrand.Checked = False
        '    chkChargableCollection.Checked = False
        '    Set_COBrand_Payment_controls()

        '    Dim GRM_DISPLAY As String = ""
        '    If (FeeCollection.GetDoubleVal(STU_ID.ToString)) > 0 Then
        '        GRM_DISPLAY = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT ISNULL(GRM_DISPLAY,'') + ' - ' + ISNULL(SCT_DESCR,'') FROM vw_OSO_STUDENT_ENQUIRY WHERE STU_ID=" & STU_ID & " and STU_TYPE='" & IIf(rbEnquiry.Checked, "E", "S") & "'")
        '    End If
        '    h_Student_no.Value = STU_ID
        '    btnRedeem.Attributes.Add("onClick", "return ShowSubWindowWithClose('popRewardsInfo.aspx?id=" & Encr_decrData.Encrypt(h_Student_no.Value) & "', '', '30%', '35%');")
        '    Dim objcls As New clsRewards
        '    objcls.FETCH_REWARDS_INFO(STU_ID)
        '    ViewState("PERSISTENT_ID") = objcls.PERSISTENT_ID
        '    labGrade.Text = "Grade : " & GRM_DISPLAY
        '    lblStuStatus.Visible = False
        '    lbLDAdate.Text = ""
        '    lbTCdate.Text = ""
        '    lbStuDOJ.Text = ""
        '    If UsrSelStudent1.STUDOJ <> "" Then
        '        lbStuDOJ.Text = "Join Date : " & UsrSelStudent1.STUDOJ
        '    End If
        '    If UsrSelStudent1.LDADATE <> "" Then
        '        lbLDAdate.Text = "LDA Date : " & UsrSelStudent1.LDADATE
        '    End If
        '    If UsrSelStudent1.TCDATE <> "" Then
        '        lbTCdate.Text = "Cancel Date : " & UsrSelStudent1.TCDATE
        '    End If

        '    Select Case UsrSelStudent1.STU_STATUS
        '        Case "TF"
        '            lblStuStatus.Visible = True
        '            lblStuStatus.Text = "Status : <span style='color:Green;'>" & UsrSelStudent1.STU_STATUS & "</span>"
        '            UsrSelStudent1.ChangeFontColor(Drawing.Color.Green)
        '        Case "EN"
        '            UsrSelStudent1.ChangeFontColor(Drawing.Color.Black)
        '        Case Else
        '            lblStuStatus.Visible = True
        '            lblStuStatus.Text = "Status : <span style='color:Red;'>" & UsrSelStudent1.STU_STATUS & "</span>"
        '            UsrSelStudent1.ChangeFontColor(Drawing.Color.Red)
        '    End Select
        '    'If UsrSelStudent1.STU_STATUS <> "EN" Then
        '    '    lblStuStatus.Visible = True
        '    '    lblStuStatus.Text = "Status : " & UsrSelStudent1.STU_STATUS
        '    '    UsrSelStudent1.ChangeFontColor(Drawing.Color.Red)
        '    'Else
        '    '    UsrSelStudent1.ChangeFontColor(Drawing.Color.Black)
        '    'End If
        '    Gridbind_Feedetails()
        '    lnkTransportfee.OnClientClick = "PayTransportFee('" & Request.Url.ToString.Replace("feeCollection.aspx", "feeTranspotCollectionfromFee.aspx") & "&stuno=" & UsrSelStudent1.STUDENT_NO & "&acdid=" & ddlAcademicYear.SelectedItem.Value & "',1);return false;"

        '    If SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "select count(*) from PosData.dbo.CUSTOMER  where CUS_NO='" & UsrSelStudent1.STUDENT_NO & "'") > 0 Then
        '        lnkCatering.OnClientClick = "PayTransportFee('" & Request.Url.ToString.Replace("feeCollection.aspx", "feeCatering.aspx") & "&stuno=" & UsrSelStudent1.STUDENT_NO & "&acdid=" & ddlAcademicYear.SelectedItem.Value & "',1);return false;"
        '        'lnkCatering.Visible = True
        '        lnkCatering.Visible = False
        '    Else
        '        lnkCatering.Visible = False
        '    End If
        '    If Not ddlAcademicYear.Items.FindByValue(UsrSelStudent1.ACD_ID) Is Nothing Then
        '        ddlAcademicYear.SelectedIndex = -1
        '        ddlAcademicYear.Items.FindByValue(UsrSelStudent1.ACD_ID).Selected = True
        '    End If
        '    SetDiscountData_Student()
        'Catch ex As Exception
        '    Errorlog(ex.Message)
        'End Try
    End Sub

    Protected Sub imgFrom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgFrom.Click
        Gridbind_Feedetails()
        SetNarration()
    End Sub

    Protected Sub txtFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFrom.TextChanged
        Gridbind_Feedetails()
        SetNarration()
    End Sub

    Protected Sub lbViewTotal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbViewTotal.Click
        Set_Total()
    End Sub

    Protected Sub txtBank1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBank1.TextChanged
        Dim str_bankid As String = ""
        Dim str_bankho As String = ""
        txtBank1.Text = FeeCommon.GetBankName(txtBank1.Text, str_bankid, str_bankho)
        h_Bank1.Value = str_bankid
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If txtBank1.Text.Trim <> "" Then
            smScriptManager.SetFocus(ddlEmirate1)
            If str_bankho <> "" Then
                ddlEmirate1.SelectedIndex = -1
                ddlEmirate1.Items.FindByValue(str_bankho).Selected = True
            End If
        Else
            smScriptManager.SetFocus(txtBank1)
            lblBankInvalid.Text = "Invalid Bank Selected in Cheque # 1"
        End If
    End Sub

    Protected Sub txtBank2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBank2.TextChanged
        Dim str_bankid As String = ""
        Dim str_bankho As String = ""
        txtBank2.Text = FeeCommon.GetBankName(txtBank2.Text, str_bankid, str_bankho)
        h_Bank2.Value = str_bankid
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If txtBank2.Text.Trim <> "" Then
            smScriptManager.SetFocus(ddlEmirate2)
            If str_bankho <> "" Then
                ddlEmirate2.SelectedIndex = -1
                ddlEmirate2.Items.FindByValue(str_bankho).Selected = True
            End If
        Else
            smScriptManager.SetFocus(txtBank2)
            lblBankInvalid.Text = "Invalid Bank Selected in Cheque # 2"
        End If
    End Sub

    Protected Sub txtBank3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBank3.TextChanged
        Dim str_bankid As String = ""
        Dim str_bankho As String = ""
        txtBank3.Text = FeeCommon.GetBankName(txtBank3.Text, str_bankid, str_bankho)
        h_Bank3.Value = str_bankid
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If txtBank3.Text.Trim <> "" Then
            smScriptManager.SetFocus(ddlEmirate3)
            If str_bankho <> "" Then
                ddlEmirate3.SelectedIndex = -1
                ddlEmirate3.Items.FindByValue(str_bankho).Selected = True
            End If
        Else
            smScriptManager.SetFocus(txtBank3)
            lblBankInvalid.Text = "Invalid Bank Selected in Cheque # 3"
        End If
    End Sub

    Sub SetNarration()
        If IsDate(txtFrom.Text) Then
            txtRemarks.Text = FeeCollection.F_GetFeeNarration(txtFrom.Text, uscStudentPicker.STU_ACD_ID, ddlBusinessunit.SelectedValue)
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles ddlAcademicYear.SelectedIndexChanged
        'SetNarration()
        ''uscStudentPicker.ClearDetails()
        'ddFeetype.DataBind()
        'Set_Provider()
        'Gridbind_Feedetails()
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        If (Not Session("counterId") Is Nothing) Then
            Dim NEXT_TOKEN_NO As String = Oasis_Counter.Counter.GetNextStudentInQueue(Session("counterId"), ddlBusinessunit.SelectedValue, Session("sUsr_name"))
            'uscStudentPicker.LoadStudentsByNo(stu_no)
            lblToken.Text = NEXT_TOKEN_NO
            Gridbind_Feedetails()
        Else
            usrMessageBar.ShowNotification("Counter number is not set for the User " & Session("sUsr_name").ToString, UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub

    Protected Sub imgCheque_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCheque.Click
        SetChequeDetails(h_Chequeid.Value)
    End Sub

    Sub SetChequeDetails(ByVal CHQ_ID As String)
        If IsNumeric(CHQ_ID) Then
            Dim ds As New DataSet
            Dim sql_query As String = " SELECT     BNK.BNK_DESCRIPTION,   FCD.FCD_REFNO, FCD.FCD_EMR_ID, FCD.FCD_REF_ID, " & _
            " REPLACE(CONVERT(VARCHAR(11), FCD.FCD_DATE, 113), ' ', '/') FCD_DATE" & _
            " FROM FEES.FEECOLLSUB_D AS FCD INNER JOIN" & _
            " FEES.FEECOLLECTION_H AS FCL ON FCD.FCD_FCL_ID = FCL.FCL_ID INNER JOIN" & _
            " VW_OSO_STUDENT_M AS STU ON FCL.FCL_STU_ID = STU.STU_ID INNER JOIN" & _
            " BANK_M AS BNK ON FCD.FCD_REF_ID = BNK.BNK_ID" & _
            " WHERE     (FCD.FCD_CLT_ID = 2) AND (FCL.FCL_BSU_ID = '" & ddlBusinessunit.SelectedValue & "') " & _
            " AND (FCD.FCD_UNIQCHQ_ID = " & CHQ_ID & " )"
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, sql_query)
            If ds.Tables.Count > 0 Then
                txtBank1.Text = ds.Tables(0).Rows(0)("BNK_DESCRIPTION")
                h_Bank1.Value = ds.Tables(0).Rows(0)("FCD_REF_ID")
                txtChqno1.Text = ds.Tables(0).Rows(0)("FCD_REFNO")
                txtChqDate1.Text = ds.Tables(0).Rows(0)("FCD_DATE")
                If Not ddlEmirate1.Items.FindByValue(ds.Tables(0).Rows(0)("FCD_EMR_ID")) Is Nothing Then
                    ddlEmirate1.SelectedIndex = -1
                    ddlEmirate1.Items.FindByValue(ds.Tables(0).Rows(0)("FCD_EMR_ID")).Selected = True
                End If
            End If
        End If
    End Sub

    Protected Sub lbApply_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        gvDiscount.SelectedIndex = sender.Parent.Parent.RowIndex
        Dim dtDiscountGrid As DataTable
        'If rbEnrollment.Checked Then
        'dtDiscountGrid = FeeCommon.DIS_SelectDiscount(txtFrom.Text, STU_ID, uscStudentPicker.STU_TYPE, Session("sBsuid"), "", False)
        'Else
        dtDiscountGrid = FeeCommon.DIS_SelectDiscount(txtFrom.Text, STU_ID, uscStudentPicker.STU_TYPE, uscStudentPicker.STU_BSU_ID, "", False)
        'End If
        gvDiscount.DataSource = dtDiscountGrid
        gvDiscount.DataBind()
        Dim lblFee_ID As Label = sender.Parent.Parent.Findcontrol("lblFee_ID")
        Dim lblFds_ID As Label = sender.Parent.Parent.Findcontrol("lblFds_ID")

        Dim dtDiscountData As DataTable
        'If rbEnrollment.Checked Then
        '    dtDiscountData = FeeCollection.DIS_GetDiscount(lblFds_ID.Text, STU_ID, "S", txtFrom.Text)
        'Else
        dtDiscountData = FeeCollection.DIS_GetDiscount(lblFds_ID.Text, STU_ID, uscStudentPicker.STU_TYPE, txtFrom.Text)
        'End If
        gvFeeCollection.DataSource = Session("FeeCollection")
        gvFeeCollection.DataBind()
        SetAdvanceFeeBreakupInControls()
        txtRemarks.Text = dtDiscountData.Rows(0)("Narration")
        If Not dtDiscountData Is Nothing AndAlso dtDiscountData.Rows.Count > 0 Then
            For Each gvr As GridViewRow In gvFeeCollection.Rows
                Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
                Dim lblDiscount As Label = CType(gvr.FindControl("lblDiscount"), Label)
                Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)
                Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
                Dim lbCancel As LinkButton = CType(gvr.FindControl("lbCancel"), LinkButton)
                If Not txtAmountToPay Is Nothing AndAlso lblFSR_FEE_ID.Text.Trim = lblFee_ID.Text.Trim Then
                    'txtAmountToPay.Text = Format(Math.Round(dtDiscountData.Rows(0)("Net"), 0) + Convert.ToDecimal(lblAmount.Text), "#,##0.00")
                    txtAmountToPay.Text = Format(Math.Round(dtDiscountData.Rows(0)("Net"), 0), "#,##0.00")
                    If txtAmountToPay.Text < 0 Then
                        txtAmountToPay.Text = "#,##0.00"
                    End If
                    txtAmountToPay.Attributes.Add("readonly", "readonly")
                    lblDiscount.Text = Format(Math.Round(dtDiscountData.Rows(0)("FDS_DISCAMOUNT"), 0), "#,##0.00")
                    lblAmount.Text = Format(Math.Round(dtDiscountData.Rows(0)("Total"), 0), "#,##0.00")
                    lbCancel.Visible = True
                End If
            Next
        End If
        Set_GridTotal(True)
    End Sub

    Sub SetDiscountData_BSU()
        Dim str_discount_sql As String = "select isnull(BSU_bAPPLYDISCOUNTSCHEME,0) from businessunit_m WITH(NOLOCK) where bsu_id='" & ddlBusinessunit.SelectedValue & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_discount_sql)
        lnkDiscount.Visible = False
        gvFeeCollection.Columns(9).Visible = False
        gvFeeCollection.Columns(10).Visible = True
        pnlDiscount.Visible = False
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            If Convert.ToBoolean(ds.Tables(0).Rows(0)(0)) Then
                lnkDiscount.Visible = True
                gvFeeCollection.Columns(9).Visible = True
                gvFeeCollection.Columns(10).Visible = False
                pnlDiscount.Visible = True
            End If
        End If
    End Sub

    Sub SetDiscountData_Student()
        If lnkDiscount.Visible And h_Student_no.Value <> "" Then
            lnkDiscount.Visible = True
            Dim dtDiscount As DataTable
            'If rbEnrollment.Checked Then
            '    dtDiscount = FeeCommon.DIS_SelectDiscount(txtFrom.Text, STU_ID, "S", Session("sBsuid"), "", False)
            'Else
            dtDiscount = FeeCommon.DIS_SelectDiscount(txtFrom.Text, STU_ID, uscStudentPicker.STU_TYPE, uscStudentPicker.STU_BSU_ID, "", False)
            'End If
            gvDiscount.DataSource = dtDiscount
            gvDiscount.DataBind()
        Else
            gvDiscount.SelectedIndex = -1
            gvDiscount.DataSource = Nothing
            gvDiscount.DataBind()
        End If
    End Sub

    Protected Sub lbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtDiscount As DataTable
        gvDiscount.SelectedIndex = -1
        'If rbEnrollment.Checked Then
        '    dtDiscount = FeeCommon.DIS_SelectDiscount(txtFrom.Text, STU_ID, "S", Session("sBsuid"), "", False)
        'Else
        dtDiscount = FeeCommon.DIS_SelectDiscount(txtFrom.Text, STU_ID, uscStudentPicker.STU_TYPE, uscStudentPicker.STU_BSU_ID, "", False)
        'End If
        gvDiscount.DataSource = dtDiscount
        gvDiscount.DataBind()
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
            Dim lbCancel As LinkButton = CType(gvr.FindControl("lbCancel"), LinkButton)
            If Not txtAmountToPay Is Nothing Then
                txtAmountToPay.Attributes.Remove("readonly")
                lbCancel.Visible = False
            End If
        Next
        For iIndex As Integer = 0 To Session("FeeCollection").rows.count - 1
            Session("FeeCollection").rows(iIndex)(("Amount")) = IIf(Session("FeeCollection").rows(iIndex)("Closing") > 0, Session("FeeCollection").rows(iIndex)("Closing"), 0)
            Session("FeeCollection").rows(iIndex)(("Discount")) = 0
        Next
        gvFeeCollection.DataSource = Session("FeeCollection")
        gvFeeCollection.DataBind()
        SetAdvanceFeeBreakupInControls()
        SetNarration()
        Set_GridTotal(True)
        Me.gvDiscDt.DataSource = ""
        Me.gvDiscDt.DataBind()
    End Sub

    Protected Sub lbStudentAudit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbStudentAudit.Click

        Dim param As New Hashtable

        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("studName", uscStudentPicker.STU_NAME)
        param.Add("Grade", uscStudentPicker.STU_GRD_ID)
        param.Add("Section", uscStudentPicker.STU_GRD_ID)
        param.Add("StuNo", uscStudentPicker.STU_NO)
        param.Add("status", uscStudentPicker.STU_CURRSTATUS)
        param.Add("stu_id", STU_ID)
        Dim rptClass As New rptClass

        With rptClass
            .crDatabase = "Oasis"
            .reportPath = Server.MapPath("../Students/Reports/RPT/rptstudAudit.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        h_print.Value = "audit"
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub

    Protected Sub chkCoBrand_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkCoBrand.Checked Then
            chkChargableCollection.Checked = False
        Else
            ddCreditcard.SelectedIndex = 0
        End If
        Set_COBrand_Payment_controls()
    End Sub

    Protected Sub chkChargableCollection_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkChargableCollection.CheckedChanged
        If chkChargableCollection.Checked Then
            chkCoBrand.Checked = False
            ddCreditcard.SelectedIndex = 0
        End If
        Set_OtherCC_Payment_controls()
    End Sub
    Sub Set_COBrand_Payment_controls(Optional ByVal bClearSelection As Boolean = True)
        If chkCoBrand.Checked Then
            txtCashTotal.Attributes.Add("readonly", "readonly")
            txtChequeTotal1.Attributes.Add("readonly", "readonly")
            txtChequeTotal2.Attributes.Add("readonly", "readonly")
            txtChequeTotal3.Attributes.Add("readonly", "readonly")
            txtCCTotal.Attributes.Add("readonly", "readonly")
            txtVoucherAmount1.Attributes.Add("readonly", "readonly")
            txtRewardsAmount.Attributes.Add("readonly", "readonly")

            txtCashTotal.Text = "0.00"
            txtChequeTotal1.Text = "0.00"
            txtChequeTotal2.Text = "0.00"
            txtChequeTotal3.Text = "0.00"
            txtBankTotal.Text = "0.00"
            txtVoucherAmount1.Text = "0.00"
            txtVoucherTotal.Text = "0.00"
            txtRewardsAmount.Text = "0.00"

            ddCreditcard.Enabled = False
            gvFeeCollection.Columns(9).Visible = True
            gvFeeCollection.Columns(10).Visible = False
            'txtRemarks.Text = "NBAD-GEMS co-brand discount on advance Tuition fees. " & txtRemarks.Text
            Dim str_CRR_ID As String = UtilityObj.GetDataFromSQL("SELECT SYS_COBRAND_CRR_ID FROM SYSINFO_S WITH(NOLOCK)", ConnectionManger.GetOASISFINConnectionString)
            If Not ddCreditcard.Items.FindByValue(str_CRR_ID) Is Nothing Then
                ddCreditcard.ClearSelection()
                ddCreditcard.Items.FindByValue(str_CRR_ID).Selected = True
            End If
        Else
            ddCreditcard.Enabled = True
            txtCashTotal.Attributes.Remove("readonly")
            txtChequeTotal1.Attributes.Remove("readonly")
            txtChequeTotal2.Attributes.Remove("readonly")
            txtChequeTotal3.Attributes.Remove("readonly")
            txtCCTotal.Attributes.Remove("readonly")
            txtVoucherAmount1.Attributes.Remove("readonly")

            gvFeeCollection.Columns(9).Visible = True 'changed to show discount for ever
            gvFeeCollection.Columns(10).Visible = False
            If bClearSelection AndAlso ddCreditcard.Items.Count > 0 And chkRemember.Checked = False Then
                'ddCreditcard.SelectedIndex = 0
            End If
        End If
        If chkCoBrand.Checked Then
            Set_GridTotal(False)
        Else
            Gridbind_Feedetails()
            txtCCTotal.Text = "0"
        End If

    End Sub
    Sub Set_OtherCC_Payment_controls(Optional ByVal bClearSelection As Boolean = True)
        If chkChargableCollection.Checked Then
            txtCashTotal.Attributes.Add("readonly", "readonly")
            txtChequeTotal1.Attributes.Add("readonly", "readonly")
            txtChequeTotal2.Attributes.Add("readonly", "readonly")
            txtChequeTotal3.Attributes.Add("readonly", "readonly")
            txtCCTotal.Attributes.Add("readonly", "readonly")
            txtVoucherAmount1.Attributes.Add("readonly", "readonly")
            txtRewardsAmount.Attributes.Add("readonly", "readonly")

            txtCashTotal.Text = "0.00"
            txtChequeTotal1.Text = "0.00"
            txtChequeTotal2.Text = "0.00"
            txtChequeTotal3.Text = "0.00"
            txtBankTotal.Text = "0.00"
            txtVoucherAmount1.Text = "0.00"
            txtVoucherTotal.Text = "0.00"
            txtRewardsAmount.Text = "0.00"

            ddCreditcard.Enabled = False
            gvFeeCollection.Columns(9).Visible = True
            gvFeeCollection.Columns(10).Visible = False
            'txtRemarks.Text = "NBAD-GEMS co-brand discount on advance Tuition fees. " & txtRemarks.Text
            Dim str_CRR_ID As String = UtilityObj.GetDataFromSQL("SELECT SYS_COBRAND_CRR_ID FROM SYSINFO_S", ConnectionManger.GetOASISFINConnectionString)
            If Not ddCreditcard.Items.FindByValue(str_CRR_ID) Is Nothing Then
                ddCreditcard.ClearSelection()
                ddCreditcard.Items.FindByValue(str_CRR_ID).Selected = True
            End If
        Else
            ddCreditcard.Enabled = True
            txtCashTotal.Attributes.Remove("readonly")
            txtChequeTotal1.Attributes.Remove("readonly")
            txtChequeTotal2.Attributes.Remove("readonly")
            txtChequeTotal3.Attributes.Remove("readonly")
            txtCCTotal.Attributes.Remove("readonly")
            txtVoucherAmount1.Attributes.Remove("readonly")

            gvFeeCollection.Columns(9).Visible = True 'changed to show discount for ever
            gvFeeCollection.Columns(10).Visible = False
            If bClearSelection And chkRemember.Checked = False Then
                'ddCreditcard.SelectedIndex = 0
            End If
        End If
        If chkChargableCollection.Checked Then
            Set_GridTotal(False)
        Else
            Gridbind_Feedetails()
            txtCCTotal.Text = "0.00"
        End If

    End Sub
    Public Function GetDiscountedFeesPayable_COBRAND(ByVal p_STU_ID As String, ByVal p_STU_TYPE As String, _
            ByVal p_CRR_ID As String, ByVal p_CLT_ID As String, ByVal p_FEE_ID As String, _
            ByVal p_AMOUNT As Decimal, ByRef p_Discount As Decimal, Optional ByVal AdmissionFee As Decimal = 0, Optional ByRef OtherCharge As Decimal = 0) As String

        Dim pParms(14) As SqlClient.SqlParameter
        Dim dtDiscnt As New DataSet

        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt) '
        pParms(0).Value = p_STU_ID
        pParms(1) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(1).Value = p_STU_TYPE
        pParms(2) = New SqlClient.SqlParameter("@CRR_ID", SqlDbType.Int)
        pParms(2).Value = p_CRR_ID
        pParms(3) = New SqlClient.SqlParameter("@CLT_ID", SqlDbType.Int)
        pParms(3).Value = p_CLT_ID
        pParms(4) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.BigInt)
        pParms(4).Value = p_FEE_ID
        pParms(5) = New SqlClient.SqlParameter("@AMOUNT", SqlDbType.Decimal, 21)
        pParms(5).Value = p_AMOUNT
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        pParms(7) = New SqlClient.SqlParameter("@Discount", SqlDbType.Decimal, 21)
        pParms(7).Direction = ParameterDirection.Output
        pParms(8) = New SqlClient.SqlParameter("@NEW_ADMISSION_FEE", SqlDbType.Decimal, 21)
        pParms(8).Value = AdmissionFee
        pParms(9) = New SqlClient.SqlParameter("@SPLITUP_XML", SqlDbType.Xml, 100000)
        pParms(9).Direction = ParameterDirection.Output
        pParms(10) = New SqlClient.SqlParameter("@OTHER_CHARGE", SqlDbType.Decimal, 21)
        pParms(10).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "GetDiscountedFeesPayable_COBRAND_NEW", pParms)
        If pParms(6).Value = 0 Then
            If Not pParms(7).Value Is Nothing And pParms(7).Value Is System.DBNull.Value Then
                p_Discount = 0
            Else
                p_Discount = pParms(7).Value
            End If

            If Not pParms(10).Value Is Nothing And pParms(10).Value Is System.DBNull.Value Then
                OtherCharge = 0
            Else
                OtherCharge = pParms(10).Value
            End If

        End If
        If Not pParms(9).Value Is DBNull.Value Then
            Dim readXML As StringReader = New StringReader(HttpUtility.HtmlDecode(pParms(9).Value))
            ViewState("DiscntXML") = pParms(9).Value
            dtDiscnt.ReadXml(readXML)
            dtDiscount = dtDiscnt.Tables(0).Copy
        End If
        GetDiscountedFeesPayable_COBRAND = pParms(6).Value
    End Function
    'Protected Sub txtReceivedTotal_textchanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtReceivedTotal.TextChanged
    '    Dim Amt As Double


    'End Sub
    'Protected Sub PCE_textchanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pceCreditCard.Disposed
    '    Dim Amt As Double


    'End Sub
    Protected Sub servicereport_calltype(ByVal sender As Object, ByVal e As System.EventArgs)


    End Sub

    Protected Sub rblVAT_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblVAT.SelectedIndexChanged
        txtAmount.Text = "0.00"
        txtVATAmount.Text = "0.00"
        txtAmountAdd.Text = "0.00"
    End Sub

    Protected Sub ddFeetype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddFeetype.SelectedIndexChanged
        If txtAmount.Text = "" Then
            CalculateVAT(ddFeetype.SelectedValue, 0, Format(CDate(txtFrom.Text), "dd/MMM/yyyy"))
        Else
            CalculateVAT(ddFeetype.SelectedValue, LTrim(RTrim(txtAmount.Text)), Format(CDate(txtFrom.Text), "dd/MMM/yyyy"))
        End If
    End Sub
    Private Sub CalculateVAT(ByVal FEEID As Integer, ByVal Amount As Double, ByVal txDate As String)

        If rblVAT.SelectedValue <> "" Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXCodeAndAmount_EXT('FEES','" & ddlBusinessunit.SelectedValue & "','FEE','" & FEEID & "','" & txDate & "'," & Amount & ",''," & IIf(rblVAT.SelectedValue = 1, 1, 0) & " )")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                txtAmount.Text = ds.Tables(0).Rows(0)("INV_AMOUNT").ToString
                txtVATAmount.Text = ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString
                txtAmountAdd.Text = ds.Tables(0).Rows(0)("NET_AMOUNT").ToString
            End If
        Else
            If txtAmountAdd.Text <> "" Or txtAmount.Text <> "" Then
                'ShowMessage("Please Select Including or Excluding TAX....!") 'lblError.Text = "Please Select Including or Excluding TAX....!"
                usrMessageBar.ShowNotification("Please Select Including or Excluding TAX....!", UserControls_usrMessageBar.WarningType.Danger)

            End If
        End If
    End Sub
    Protected Sub txtAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmount.TextChanged
        CalculateVAT(ddFeetype.SelectedValue, txtAmount.Text, Format(CDate(txtFrom.Text), "dd/MMM/yyyy"))
    End Sub

    Protected Sub lbtnAddMore_Click(sender As Object, e As EventArgs) Handles lbtnAddMore.Click
        If tr_AddFee.Visible Then
            tr_AddFee.Visible = False
        Else
            tr_AddFee.Visible = True
        End If
        tr_AddFee1.Visible = Not tr_AddFee1.Visible
    End Sub

    Private Function SAVE_COLLECTION_INITIATED(ByRef stTrans As SqlTransaction, ByVal FCL_ID As Long) As Integer
        SAVE_COLLECTION_INITIATED = 1000
        Dim PRO_DATE As String = Format(DateTime.Now, OASISConstants.DataBaseDateFormat)
        Dim NEW_PRO_ID As Long = 0

        Try
            Dim PRO_PRO_ID As Int64 = 0
            'Dim retval As String = "0"
            If FeeCollection.GetDoubleVal(txtRewardsAmount.Text) > 0 Then
                Dim objclsrewards As New clsRewards
                objclsrewards.PRO_ID = 0
                objclsrewards.PRO_PRO_ID = PRO_PRO_ID
                objclsrewards.PRO_BSU_ID = uscStudentPicker.STU_BSU_ID
                objclsrewards.PRO_ACD_ID = uscStudentPicker.STU_ACD_ID
                objclsrewards.PRO_DATE = Format(DateTime.Now.Date, OASISConstants.DateFormat)
                objclsrewards.PRO_SOURCE = "COUNTER"
                objclsrewards.PRO_STU_TYPE = "S"
                objclsrewards.PRO_STU_ID = STU_ID
                objclsrewards.PRO_DRCR = "CR"
                objclsrewards.PRO_AMOUNT = FeeCollection.GetDoubleVal(txtRewardsAmount.Text)
                objclsrewards.PRO_NARRATION = "GEMS Rewards Points redemption for " & uscStudentPicker.STU_NO
                objclsrewards.PRO_STATUS = "INITIATED"
                objclsrewards.PRO_LOG_USERNAME = Session("sUsr_name")
                objclsrewards.PRO_API_DATA_REDEEMABLE_POINTS = 0
                objclsrewards.PRO_API_DATA_AVAILABLE_POINTS = 0
                objclsrewards.PRO_API_DATA_REDEEMABLE_AMOUNT = 0
                objclsrewards.PERSISTENT_ID = uscStudentPicker.REWARDS_CUSTOMER_ID.ToString
                objclsrewards.API_CMD = "BURN"
                If objclsrewards.SAVE_POINTS_REDEMPTION_ONLINE(stTrans) Then
                    Return REDEEM_POINTS_CALL(objclsrewards.PRO_ID, objclsrewards.PRO_AMOUNT, stTrans, FCL_ID)
                End If
            End If
        Catch ex As Exception
            SAVE_COLLECTION_INITIATED = 1000
            'ShowMessage(getErrorMessage(1000), True)
            Errorlog("SAVE_COLLECTION_INITIATED - " & ex.Message, "OASIS")
        End Try
    End Function
    Private Function REDEEM_POINTS_CALL(ByVal GEMSTransactionID As Long, ByVal RedeemingAmount As Double, ByRef stTrans As SqlTransaction, ByVal FCL_ID As Long) As Integer
        REDEEM_POINTS_CALL = 1
        Dim ACL_ID As Long = 0, GENERATED_TOKEN As String = ""
        If Convert.ToInt64(STU_ID) > 0 Then
            Dim objcls As New clsRewards
            objcls.FETCH_REWARDS_INFO(Convert.ToInt64(STU_ID))
            '--------------------------------------Vernost API call--------------------------------------------
            If objcls.CUSTOMER_ID.Trim <> "" And objcls.CUSTOMER_ID.Trim <> "0" Then
                Try
                    If objcls.GENERATE_ACCESS_TOKEN(objcls, GENERATED_TOKEN) Then '-------------generate temperory token from API to access main APIs
                        objcls.GET_API_CALL_PARAMETERS("BURNPOINTS") 'api to burn points
                        Dim postString As String = String.Format("customer_id={0}&type={1}&activity={2}&description={3}&burn_points={4}", objcls.CUSTOMER_ID, "parent", "4", "burning points with refId." & FCL_ID.ToString & "", (RedeemingAmount * 10))
                        Const ContType As String = "application/x-www-form-urlencoded"
                        Dim str = clsRewards.SAVE_ENTERTAINER_API_CALL_LOG(ACL_ID, uscStudentPicker.STU_BSU_ID, DateTime.Now, "BURNPOINTS", objcls.API_URI, objcls.API_METHOD, objcls.CUSTOMER_ID, Session("username"))

                        Dim webRequest As HttpWebRequest = TryCast(Net.WebRequest.Create(objcls.API_URI), HttpWebRequest)
                        webRequest.Method = objcls.API_METHOD
                        webRequest.ContentType = ContType
                        webRequest.ContentLength = postString.Length
                        webRequest.Headers.Add("TP_APPLICATION_KEY", objcls.API_TOKEN)
                        webRequest.Headers.Add("CC_TOKEN", GENERATED_TOKEN)
                        Dim requestWriter As New StreamWriter(webRequest.GetRequestStream())
                        requestWriter.Write(postString)
                        requestWriter.Close()
                        Dim responseReader As StreamReader
                        responseReader = Nothing
                        responseReader = New StreamReader(webRequest.GetResponse().GetResponseStream())
                        Dim responseData As String = responseReader.ReadToEnd()


                        'Dim client = New RestClient(New Uri(objcls.API_URI).AbsoluteUri)
                        'Dim APIRequest = New RestRequest(clsRewards.GetAPIMethod(objcls.API_METHOD))
                        'APIRequest.AddHeader("TP_APPLICATION_KEY", objcls.API_TOKEN)
                        'APIRequest.AddHeader("CC_TOKEN", GENERATED_TOKEN)
                        'APIRequest.AddHeader("Accept", "*/*")
                        'APIRequest.AddHeader("Content-Type", ContType)
                        'APIRequest.AddHeader("Content-Length", postString.Length)
                        'APIRequest.AddParameter("undefined", postString, ParameterType.RequestBody)
                        'APIRequest.AddHeader("Accept-Encoding", "gzip, deflate")
                        'Dim Response As IRestResponse = client.Execute(APIRequest)
                        'responseData = Response.Content

                        Dim bool = clsRewards.UPDATE_ENTERTAINER_API_CALL_LOG(ACL_ID, Left(responseData, 2000))
                        Dim jsonBurnResponse = JObject.Parse(responseData)
                        Dim transaction_id As String = "", status_code As String = "", message As String = ""
                        If Not jsonBurnResponse("status") Is Nothing AndAlso jsonBurnResponse("status").ToObject(Of String)().ToLower = "true" Then
                            REDEEM_POINTS_CALL = 0
                            transaction_id = "" : status_code = "" : message = ""
                            If Not jsonBurnResponse("status_code") Is Nothing Then
                                status_code = jsonBurnResponse("status_code").ToObject(Of String)()
                            End If
                            If Not jsonBurnResponse("message") Is Nothing Then
                                message = jsonBurnResponse("message").ToObject(Of String)()
                            End If
                            If Not jsonBurnResponse("values") Is Nothing Then
                                Dim jsonBurnResponse_values = JObject.Parse(jsonBurnResponse("values").ToString())
                                transaction_id = FeeCollection.ValidateValue(jsonBurnResponse_values.Property("transaction_id"))
                            End If
                            UPDATE_COLLECTION_INITIATED(GEMSTransactionID, objcls.API_URI, "true", "200OK", status_code, status_code, message, transaction_id, "success", stTrans, FCL_ID) 'updating the api call response
                            Return SAVE_COLLECTION_SUCCESS(GEMSTransactionID, stTrans)
                        ElseIf Not jsonBurnResponse("status") Is Nothing AndAlso jsonBurnResponse("status").ToObject(Of String)().ToLower = "false" Then
                            transaction_id = "" : status_code = "" : message = ""
                            If Not jsonBurnResponse("message") Is Nothing Then
                                message = jsonBurnResponse("message").ToObject(Of String)()
                                usrMessageBar.ShowNotification("Points redemption failed because of " & message, UserControls_usrMessageBar.WarningType.Danger)
                            ElseIf Not jsonBurnResponse("errors") Is Nothing Then
                                Dim jsonBurnResponse_errors = JObject.Parse(jsonBurnResponse("errors").ToString())
                                Dim propCollection As System.Collections.Generic.IEnumerable(Of JProperty) = jsonBurnResponse_errors.Properties
                                If Not propCollection Is Nothing Then
                                    Dim ERRmSG As String = ""
                                    For Each errItem As JProperty In DirectCast(jsonBurnResponse_errors, JToken)
                                        ERRmSG = ERRmSG & IIf(ERRmSG <> "", "<BR />", "") & FeeCollection.ValidateValue(errItem)
                                    Next
                                    usrMessageBar.ShowNotification("Points redemption failed because of " & ERRmSG, UserControls_usrMessageBar.WarningType.Danger)
                                End If
                            End If
                            UPDATE_COLLECTION_INITIATED(GEMSTransactionID, objcls.API_URI, "false", "200OK", status_code, status_code, message, transaction_id, "failed", stTrans, FCL_ID) 'updating the api call response

                        End If
                    Else
                        '--------------error message returned from generate token
                        usrMessageBar.ShowNotification(GENERATED_TOKEN, UserControls_usrMessageBar.WarningType.Danger)
                    End If

                Catch ex As Exception

                End Try
            End If
        Else
            usrMessageBar.ShowNotification("Your session expired! Unable to fetch the GEMS Rewards data.", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Function
   
    Private Sub UPDATE_COLLECTION_INITIATED(ByVal PRO_ID As Long, ByVal API_CMD As String, ByVal API_SUCCESS As String, ByVal API_HTTP_RESPONSE As String, _
                                          ByVal API_CODE As String, ByVal API_DATA_CODE As String, ByVal API_DATA_MESSAGE As String, _
                                          ByVal API_DATA_TRANSACTION_ID As String, ByVal API_DATA_STATUS As String, ByRef stTrans As SqlTransaction, _
                                          ByVal FCL_ID As Long, _
                                          Optional ByVal bPointUpdate As Boolean = False, Optional ByVal REDEEMABLE_AMOUNT As Double = 0, _
                                          Optional ByVal REDEEMABLE_POINTS As Double = 0, Optional ByVal AVAILABLE_POINTS As Double = 0)
        Dim objclsrewards As New clsRewards
        With objclsrewards
            .PRO_ID = PRO_ID
            .API_CMD = API_CMD
            .PRO_API_SUCCESS = API_SUCCESS
            .PRO_API_HTTP_RESPONSE = API_HTTP_RESPONSE
            .PRO_API_CODE = API_CODE
            .PRO_API_DATA_CODE = API_DATA_CODE
            .PRO_API_DATA_MESSAGE = API_DATA_MESSAGE
            .PRO_API_DATA_TRANSACTION_ID = API_DATA_TRANSACTION_ID
            .PRO_API_DATA_STATUS = API_DATA_STATUS
            .bUpdate_Points = bPointUpdate
            .PRO_API_DATA_REDEEMABLE_AMOUNT = REDEEMABLE_AMOUNT
            .PRO_API_DATA_REDEEMABLE_POINTS = REDEEMABLE_POINTS
            .PRO_API_DATA_AVAILABLE_POINTS = AVAILABLE_POINTS
            .PRO_FCL_ID = FCL_ID
            .UPDATE_POINTS_REDEMPTION_ONLINE(stTrans) 'function to update the web api call response fields
        End With
    End Sub
    Private Function SAVE_COLLECTION_SUCCESS(ByVal PRO_ID As Long, ByRef stTrans As SqlTransaction) As Integer
        SAVE_COLLECTION_SUCCESS = 1
        Dim StuNos As String = ""

        Try
            Dim objcls As New clsRewards
            objcls.PRO_ID = PRO_ID
            If objcls.SAVE_FEECOLLECTION_H(stTrans) Then
                usrMessageBar.ShowNotification(getErrorMessage(360), UserControls_usrMessageBar.WarningType.Success)
                SAVE_COLLECTION_SUCCESS = 0
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            usrMessageBar.ShowNotification(getErrorMessage(359), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        End Try
    End Function

    Protected Sub usrRibbonControlPanel_btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles usrRibbonControlPanel.btnAdd_Click
        ViewState("datamode") = "add"
        clear_All()
        usrRibbonControlPanel.SetButtons(ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Protected Sub usrRibbonControlPanel_btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles usrRibbonControlPanel.btnClear_Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            chkCoBrand.Checked = False
            chkChargableCollection.Checked = False
            Set_COBrand_Payment_controls()
            clear_All()
            ViewState("datamode") = "view"
            usrRibbonControlPanel.SetButtons(ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Protected Sub usrRibbonControlPanel_btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles usrRibbonControlPanel.btnSave_Click
        btnSave_Click(sender, e)
    End Sub
    Protected Sub usrRibbonControlPanel_btnQuickFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles usrRibbonControlPanel.btnQuickFind_Click
        Response.Redirect("feeCollectionView.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("none") & "&mode=" & Encr_decrData.Encrypt("search"))
    End Sub
    Protected Sub uscStudentPicker_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uscStudentPicker.StudentNoChanged
        Try
            STU_ID = 0
            chkCoBrand.Checked = False
            chkChargableCollection.Checked = False
            Set_COBrand_Payment_controls()
            If (FeeCollection.GetDoubleVal(uscStudentPicker.STU_ID.ToString)) > 0 Then
                STU_ID = uscStudentPicker.STU_ID
            End If
            h_Student_no.Value = STU_ID
            btnRedeem.Attributes.Add("onClick", "Popup('popRewardsInfo.aspx?id=" & Encr_decrData.Encrypt(uscStudentPicker.STU_ID) & "');")
            SetNarration()
            ddFeetype.DataBind()
            Set_Provider()
            Gridbind_Feedetails()
            lnkTransportfee.OnClientClick = "PayTransportFee('" & Request.Url.ToString.Replace("feeCollection.aspx", "feeTranspotCollectionfromFee.aspx") & "&stuno=" & uscStudentPicker.STU_NO & "&acdid=" & uscStudentPicker.STU_ACD_ID & "',1);return false;"
            lnkCatering.Visible = False
            SetDiscountData_Student()
            If Not Session("BSU_bFPSEnabled") Is Nothing AndAlso Convert.ToBoolean(Session("BSU_bFPSEnabled")) = True Then
                ShowNextAcademic()
            End If
            GET_ACTIVITY_DETAILS()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub uscStudentPicker_StudentCleared(ByVal sender As Object, ByVal e As System.EventArgs) Handles uscStudentPicker.StudentCleared
        'ClearStudentData()
        chkCoBrand.Checked = False
        chkChargableCollection.Checked = False
        Set_COBrand_Payment_controls()
        ClearStudentData(False)
    End Sub
    Protected Sub RadSlider_Ticks_ValueChanged(sender As Object, e As EventArgs)
        Try
            SetNarration()
            Dim dt As DataTable = Session("VSgridSlider")
            Dim foundRows(), duerows() As Data.DataRow
            If Not dt Is Nothing Then
                Dim Startindex As Integer
                Dim obj As Object = sender.parent
                Dim RadSlider_Ticks As Telerik.Web.UI.RadSlider = CType(obj.FindControl("RadSlider_Ticks"), Telerik.Web.UI.RadSlider)
                Dim hid_sli_stu_id As HiddenField = CType(obj.FindControl("hid_sli_stu_id"), HiddenField)
                Dim dr As DataRow() = dt.Select("STU_ID =" & hid_sli_stu_id.Value)
                foundRows = dt.Select("SOURCE = 'CURRENT' AND STU_ID =" & hid_sli_stu_id.Value)
                duerows = dt.Select("SOURCE = 'OUTSTANDING' AND STU_ID =" & hid_sli_stu_id.Value)
                Dim txtAmountToPay As TextBox = CType(obj.FindControl("txtAmountToPay"), TextBox)
                Dim txtNetAmt As Label = CType(obj.FindControl("txtNetAmt"), Label)
                Dim lblmessage As Label = CType(obj.FindControl("lblmessage"), Label)
                Dim str As String = txtAmountToPay.Text
                Dim feeamount_total As Double = 0, current_due As Double = 0
                Dim index As Integer = RadSlider_Ticks.Value
                If Not duerows Is Nothing AndAlso duerows.Length > 0 AndAlso Not duerows(0)("FEE_AMOUNT") Is Nothing Then
                    feeamount_total = FeeCollection.GetDoubleVal(duerows(0)("FEE_AMOUNT"))
                End If
                If Not foundRows(0)("ID") Is Nothing Then
                    Startindex = Convert.ToInt32(foundRows(0)("ID"))
                Else
                    Startindex = 0
                End If
                If (index = 0) Then
                    txtNetAmt.Text = feeamount_total.ToString("#,##0.00")
                    txtAmountToPay.Text = "0.00"
                Else
                    If Startindex > index And 1 = 2 Then
                        lblmessage.Visible = True
                        lblmessage.Text = "You Have Already Paid For This Month"
                        txtNetAmt.Text = "0.00"
                        txtAmountToPay.Text = "0.00"
                    Else
                        For Value As Integer = Startindex To index
                            feeamount_total = feeamount_total + dr(Value)("FEE_AMOUNT")
                            txtRemarks.Text += ", " + dr(Value)("F_MONTH")
                        Next
                        lblmessage.Visible = False
                        txtAmountToPay.Text = feeamount_total.ToString()
                        txtNetAmt.Text = feeamount_total.ToString("#,##0.00") + "  "
                    End If
                End If
                txtAmountToPay_TextChanged(sender, e)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub SetAdvanceFeeBreakupInControls()
        Dim hfSTU_ID As New HiddenField
        Dim myRow As GridViewRow
        Dim lblFSR_FEE_ID As Label
        Dim btnSlider As New LinkButton
        Dim divslider As New HtmlGenericControl
        Dim hid_sli_stu_id As HiddenField
        hfSTU_ID.Value = uscStudentPicker.STU_ID
        If Not gvFeeCollection Is Nothing Then
            For Each myRow In gvFeeCollection.Rows
                lblFSR_FEE_ID = CType(myRow.FindControl("lblFSR_FEE_ID"), Label)
                divslider = CType(myRow.FindControl("divslider"), HtmlGenericControl)
                btnSlider = CType(myRow.FindControl("btnSlider"), LinkButton)
                hid_sli_stu_id = CType(myRow.FindControl("hid_sli_stu_id"), HiddenField)
                hid_sli_stu_id.Value = hfSTU_ID.Value
                If (lblFSR_FEE_ID.Text = "5") Then
                    'Creating the controls                       
                    Dim txtNetAmt As Label = CType(myRow.FindControl("txtNetAmt"), Label)
                    'Calling SP to fetch Details
                    Dim ds As DataSet = gridbind_slider(hfSTU_ID.Value)
                    If Not ds Is Nothing And Not ds.Tables(0) Is Nothing Then
                        Dim dt As DataTable = ds.Tables(0)
                        If dt.Rows.Count > 0 Then
                            dt.Columns.Add("STU_ID", GetType(Int64))
                            For Each drow In dt.Rows
                                drow("STU_ID") = hfSTU_ID.Value
                            Next
                            If Session("VSgridSlider") Is Nothing Then
                                Session("VSgridSlider") = dt
                            Else
                                Session("VSgridSlider").Merge(dt)
                            End If
                        End If
                    End If
                    'Creating slider and binding with dataset
                    Dim RadSlider_Ticks As Telerik.Web.UI.RadSlider = CType(myRow.FindControl("RadSlider_Ticks"), Telerik.Web.UI.RadSlider)
                    RadSlider_Ticks.DataSource = ds
                    RadSlider_Ticks.DataTextField = "F_MONTH"
                    RadSlider_Ticks.DataValueField = "ID"
                    RadSlider_Ticks.DataBind()
                    'finding max id 
                    Dim max_id As Integer
                    max_id = ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1)("ID")
                    'index for locating current position
                    Dim index As Integer = 0
                    Try
                        Dim foundRows(), dueRows() As Data.DataRow
                        foundRows = ds.Tables(0).Select("SOURCE = 'CURRENT'")
                        dueRows = ds.Tables(0).Select("SOURCE = 'OUTSTANDING'")
                        If foundRows.Length > 0 Then
                            btnSlider.Visible = True
                            index = Convert.ToInt32(foundRows(0)("ID"))
                            RadSlider_Ticks.SelectedIndex = index - 1
                            RadSlider_Ticks.MinimumValue = index - 1
                            RadSlider_Ticks.SelectionStart = index - 1
                            'txtNetAmt.Text = "0.00"
                        Else
                            divslider.Visible = False
                            btnSlider.Visible = False
                        End If
                        Dim CurrentDue As Double = 0
                        If Not dueRows Is Nothing AndAlso dueRows.Length > 0 Then
                            CurrentDue = FeeCollection.GetDoubleVal(dueRows(0)("FEE_AMOUNT"))
                        End If
                        txtNetAmt.Text = Format(CurrentDue, "#,##0.00")
                    Catch ex As ArgumentOutOfRangeException
                        divslider.Visible = False
                        btnSlider.Visible = False
                    Catch ex2 As Exception
                    End Try
                    ' Populating value on load
                Else
                    btnSlider.Visible = False
                    divslider.Visible = False
                End If
            Next
        End If
    End Sub
    Protected Function gridbind_slider(ByVal hfSTU_ID As Integer) As DataSet
        Dim Iscobrandcard As Boolean = IIf(chkCoBrand.Checked, True, False)
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_iD", SqlDbType.VarChar, 50)
        pParms(0).Value = hfSTU_ID
        If Iscobrandcard Then
            pParms(1) = New SqlClient.SqlParameter("@CPM_ID", SqlDbType.VarChar, 50)
            pParms(1).Value = 3
        Else
            pParms(1) = New SqlClient.SqlParameter("@CPM_ID", SqlDbType.VarChar, 50)
            pParms(1).Value = 2
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.GET_ADVANCE_PAYMENT_DETAILS", pParms)
        Return ds
    End Function

    Protected Sub nxtAcaFee_CheckedChanged(sender As Object, e As EventArgs) Handles nxtAcaFee.CheckedChanged
        Gridbind_Feedetails()
    End Sub

    Protected Sub btnRewards_Click(sender As Object, e As EventArgs) Handles btnRewards.Click
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        txtRewardsAmount.Text = 10
        Dim retval = SAVE_COLLECTION_INITIATED(stTrans, 0)
        stTrans.Rollback()
        objConn.Close()
    End Sub

    Private Sub GET_ACTIVITY_DETAILS()
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(0).Value = uscStudentPicker.STU_ID
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = ddlBusinessunit.SelectedValue

        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "OASIS.GET_ACTIVITY_LIST_FOR_PAYMENT", pParms)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            ddlActivity.DataValueField = "ID"
            ddlActivity.DataTextField = "DESCR"
            ddlActivity.DataSource = ds.Tables(0)
            ddlActivity.DataBind()
        End If

    End Sub

    Protected Sub btnAddActivity_Click(sender As Object, e As EventArgs) Handles btnAddActivity.Click
        If ddlActivity.Items.Count <= 0 OrElse ddlActivity.SelectedValue = "0" OrElse ddlActivity.SelectedIndex = 0 Then
            usrMessageBar.ShowNotification("Please select a other payment item", True)
            Exit Sub
        End If
        Dim pActIndex As Integer = ddlActivity.SelectedIndex
        Dim pArray As String() = ddlActivity.SelectedValue.Split("|")
        Dim pArrayLength As Integer = pArray.Length
        If pArrayLength > 0 Then
            Dim vPAY_TYPE_ID As String = Convert.ToString(pArray(0))
            Dim vFee_descr As String = IIf(pArrayLength > 1, pArray(1).ToString(), "")
            Dim vAmount As Double = IIf(pArrayLength > 2, FeeCollection.GetDoubleVal(pArray(2)), 0)
            'Added by vikranth on 5th Jan 2020
            'Dim vFEE_ID As Integer = IIf(pArrayLength > 3, Convert.ToInt32(pArray(3)), 0)
            Dim vFEE_ID As Integer = 0
            Dim pSubArray As String() = IIf(pArrayLength > 3, pArray(3).Split("&"), 0)
            Dim pSubArrayLength As Integer = pSubArray.Length
            If pSubArrayLength > 1 Then
                vFEE_ID = Convert.ToInt32(pSubArray(0))
                Dim ALD_ID As Integer = IIf(pSubArrayLength > 1, Convert.ToInt32(pSubArray(1)), 0)
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim strans As SqlTransaction = Nothing
                Dim param(8) As SqlParameter
                Dim ReturnVal As String = Nothing
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                strans = objConn.BeginTransaction
                param(0) = New SqlParameter("@VIEWID", SqlDbType.BigInt)
                param(0).Value = ALD_ID
                param(1) = New SqlParameter("@STU_ID", SqlDbType.VarChar, 100)
                param(1).Value = uscStudentPicker.STU_ID
                param(2) = New SqlParameter("@STU_NAME", SqlDbType.VarChar, 100)
                param(2).Value = uscStudentPicker.STU_NAME
                param(3) = New SqlParameter("@STU_COUNT", SqlDbType.BigInt)
                param(3).Value = 1
                param(4) = New SqlParameter("@TOTAL_AMOUNT", SqlDbType.Decimal, 21)
                param(4).Value = Convert.ToDecimal(vAmount)
                param(5) = New SqlParameter("@SCHEDULE", SqlDbType.VarChar)
                param(5).Value = ""
                param(6) = New SqlParameter("@SCH_ID", SqlDbType.BigInt)
                param(6).Value = Nothing
                param(7) = New SqlParameter("@GRP_ID", SqlDbType.BigInt)
                param(7).Value = Nothing
                param(8) = New SqlParameter("@APD_ID", SqlDbType.VarChar, 50)
                param(8).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "[OASIS].[UPDATE_ACTIVITY_DETAILS_TO_PARENTPORTAL]", param)
                'SqlHelper.ExecuteNonQuery(strans, "[OASIS].[UPDATE_ACTIVITY_DETAILS_TO_PARENTPORTAL]", param)
                If Not param(8).Value Is Nothing AndAlso param(8).Value.ToString.Trim <> "" Then
                    vPAY_TYPE_ID = param(8).Value
                    strans.Commit()
                Else : strans.Rollback()
                End If

            Else : vFEE_ID = IIf(pArrayLength > 3, Convert.ToInt32(pArray(3)), 0)
            End If
            'End Vikranth


            Dim vPAY_TYPE As String = IIf(pArrayLength > 4, pArray(4).ToString, 0)
            If vPAY_TYPE_ID <> "" And vAmount > 0 And vFEE_ID > 0 Then
                Dim dt As DataTable = DirectCast(Session("FeeCollection"), DataTable)
                If Not dt Is Nothing Then
                    If dt.Columns.Contains("PAY_TYPE_ID") Then
                        Dim dr = dt.Select("FEE_ID = '" & vFEE_ID & "'")
                        If Not dr Is Nothing AndAlso dr.Length > 0 Then
                            dt.Select("FEE_ID = '" & vFEE_ID & "'")(0)("PAY_TYPE_ID") &= "$" & vPAY_TYPE_ID
                            vAmount = vAmount + FeeCollection.GetDoubleVal(dt.Select("FEE_ID = '" & vFEE_ID & "'")(0)("Amount"))
                            dt.Select("FEE_ID = '" & vFEE_ID & "'")(0)("Amount") = vAmount
                            dt.Select("FEE_ID = '" & vFEE_ID & "'")(0)("FEE_bBlockPaynow") = True
                            dt.Select("FEE_ID = '" & vFEE_ID & "'")(0)("FCS_PAY_TYPE") = vPAY_TYPE
                            dt.AcceptChanges()
                        Else
                            Dim drnew As DataRow = dt.NewRow
                            drnew("FEE_ID") = vFEE_ID
                            drnew("PAY_TYPE_ID") = vPAY_TYPE_ID
                            drnew("FEE_DESCR") = vFee_descr
                            drnew("CLOSING") = 0
                            drnew("Amount") = vAmount
                            drnew("AUTOYN") = "0"
                            drnew("Discount") = "0"
                            drnew("FCS_PAY_TYPE") = vPAY_TYPE
                            drnew("FEE_bBlockPaynow") = True
                            dt.Rows.Add(drnew)
                            dt.AcceptChanges()
                        End If
                    Else
                        Dim dcAPD As DataColumn = New DataColumn("PAY_TYPE_ID", System.Type.GetType("System.String"))
                        dt.Columns.Add(dcAPD)
                        Dim dr = dt.Select("FEE_ID = '" & vFEE_ID & "'")
                        If Not dr Is Nothing AndAlso dr.Length > 0 Then
                            dt.Select("FEE_ID = '" & vFEE_ID & "'")(0)("PAY_TYPE_ID") = vPAY_TYPE_ID
                            vAmount = vAmount + FeeCollection.GetDoubleVal(dt.Select("FEE_ID = '" & vFEE_ID & "'")(0)("Amount"))
                            dt.Select("FEE_ID = '" & vFEE_ID & "'")(0)("Amount") = vAmount
                            dt.Select("FEE_ID = '" & vFEE_ID & "'")(0)("FEE_bBlockPaynow") = True
                            dt.Select("FEE_ID = '" & vFEE_ID & "'")(0)("FCS_PAY_TYPE") = vPAY_TYPE
                            dt.AcceptChanges()
                        Else
                            Dim drnew As DataRow = dt.NewRow
                            drnew("FEE_ID") = vFEE_ID
                            drnew("PAY_TYPE_ID") = vPAY_TYPE_ID
                            drnew("FEE_DESCR") = vFee_descr
                            drnew("CLOSING") = 0
                            drnew("Amount") = vAmount
                            drnew("AUTOYN") = "0"
                            drnew("Discount") = "0"
                            drnew("FCS_PAY_TYPE") = vPAY_TYPE
                            drnew("FEE_bBlockPaynow") = True
                            dt.Rows.Add(drnew)
                            dt.AcceptChanges()
                        End If
                    End If
                    Session("FeeCollection") = dt
                    gvFeeCollection.DataSource = Session("FeeCollection")
                    gvFeeCollection.DataBind()
                    SetAdvanceFeeBreakupInControls()
                    Set_GridTotal(False)
                Else '----------if the grid is empty, add new line activity fee type
                    usrMessageBar.ShowNotification("Unable to add payment item", True)
                    Exit Sub
                End If
                txtRemarks.Text &= ", " & ddlActivity.SelectedItem.Text
            End If
            If pActIndex > 0 Then
                ddlActivity.Items.RemoveAt(pActIndex)
            End If
            ddlActivity.SelectedIndex = 0
            ddlActivity_SelectedIndexChanged(Nothing, Nothing)
            Me.trActPay.Visible = Not trActPay.Visible
        End If
    End Sub

    Protected Sub lbtnActivityPayment_Click(sender As Object, e As EventArgs) Handles lbtnActivityPayment.Click
        Me.trActPay.Visible = Not trActPay.Visible
    End Sub

    Protected Sub ddlActivity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlActivity.SelectedIndexChanged
        lblActFeeType.Text = ""
        lblActAmount.Text = "0.00"
        Dim pArray As String() = ddlActivity.SelectedValue.Split("|")
        If pArray.Length > 1 Then
            lblActFeeType.Text = pArray(1).ToString
            lblActAmount.Text = Format(FeeCollection.GetDoubleVal(pArray(2)), "#,##0.00")
        End If
    End Sub
    Protected Sub chkDisableDiscount_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkDisableDiscount.Checked Then
            lbCancel_Click(Nothing, Nothing)
        Else
            Set_GridTotal(False)
        End If
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        uscStudentPicker.ClearDetails()
        'usrRibbonControlPanel_btnClear_Click(sender, Nothing)

        'InitializeRibbonControl()
        usrRibbonControlPanel.SetButtons(ViewState("menu_rights"), ViewState("datamode"))
        InitialiseCompnents()
    End Sub
End Class

