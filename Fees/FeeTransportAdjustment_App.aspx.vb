Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Sql
Imports Microsoft.ApplicationBlocks.Data

Partial Class FeeTransportAdjustment_App
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ADJUSTMENTS_APP_TRANSPORT Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            hlAddNew.NavigateUrl = "FeeTransportAdjustment_REQ.aspx" & "?MainMnu_code=" & Encr_decrData.Encrypt(OASISConstants.MNU_FEE_ADJUSTMENTS_REQ_TRANSPORT) & "&datamode=" & Encr_decrData.Encrypt("add")
            gvFeeDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvSerUsageHistory.Attributes.Add("bordercolor", "#1b80b6")
            txtReqDate.Attributes.Add("ReadOnly", "ReadOnly")
            TrserviceDet.Visible = False
            'radAdjType.Items(0).Attributes.Add("onclick", "ChangeTextEnteringCaption(" & radAdjType.Items(0).Text & ")")
            Session("sFEE_ADJUSTMENT") = Nothing

            If ViewState("datamode") = "view" Then
                Dim FAR_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FAR_ID").Replace(" ", "+"))
                Dim vFEE_ADJ As FEEADJUSTMENTREQTransport = FEEADJUSTMENTREQTransport.GetFeeAdjustments(FAR_ID)

                Session("sTotalFeePaid") = FEEADJUSTMENTREQTransport.GetTotalFeePaid(vFEE_ADJ.FAH_STU_ID)
                'Session("sFEEPaidCurAcdYr") = FEEADJUSTMENTREQTransport.GetFeePaidForCurrentAcademicYear(vFEE_ADJ.FAH_STU_ID, vFEE_ADJ.FAH_ACD_ID)
                Dim dtservice As DataTable = FEEADJUSTMENTREQTransport.GetServiceHistory(vFEE_ADJ.FAH_STU_ID)
                gvSerUsageHistory.DataSource = dtservice
                gvSerUsageHistory.DataBind()

                txtReqDate.Text = Format(vFEE_ADJ.FAH_DATE, OASISConstants.DateFormat)
                txtHeaderRemarks.Text = vFEE_ADJ.FAH_REMARKS
                txtApprRemarks.Text = txtHeaderRemarks.Text
                lblStudName.Text = vFEE_ADJ.FAR_STU_NAME
                lblArea.Text = vFEE_ADJ.SUB_LOCATION
                h_STUD_ID.Value = vFEE_ADJ.FAH_STU_ID
                h_BSU_ID.Value = vFEE_ADJ.FAR_STU_BSU_ID
                lblAcademicYear.Text = vFEE_ADJ.vFAR_ACD_DISPLAY
                lblStudBSUName.Text = vFEE_ADJ.FAR_STU_BSU_NAME
                lblSerTakenDate.Text = Format(vFEE_ADJ.vSER_START_DATE, OASISConstants.DateFormat)

                Dim str_sql As String = " SELECT  ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,   " & _
                " CASE WHEN (STU_LEAVEDATE IS NULL OR CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()))" & _
                " THEN CASE STU_CURRSTATUS WHEN 'EN' THEN 'Active' WHEN 'CN' THEN 'Cancelled Admission' WHEN 'TF' THEN 'Transfer' END  " & _
                " ELSE CASE ISNULL(TCM_TCSO,'TC') WHEN 'TC' THEN 'TC' WHEN 'SO' THEN 'Strike Off' END END AS STATUS " & _
                " FROM STUDENT_M AS A INNER JOIN VV_GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID " & _
                " INNER JOIN VV_SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID  " & _
                " LEFT OUTER JOIN OASIS..TCM_M AS D ON A.STU_ID=D.TCM_STU_ID AND TCM_CANCELDATE IS NULL " & _
                " where STU_ID=  '" & h_STUD_ID.Value & "'"
                Dim dsStudentGrade As DataSet
                dsStudentGrade = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_sql)
                If Not dsStudentGrade Is Nothing AndAlso dsStudentGrade.Tables.Count > 0 AndAlso dsStudentGrade.Tables(0).Rows.Count > 0 Then
                    h_STU_SCT_DESCR.Value = dsStudentGrade.Tables(0).Rows(0)("SCT_DESCR")
                    h_STU_STATUS.Value = IIf(Not dsStudentGrade.Tables(0).Rows(0)("STATUS") Is System.DBNull.Value, dsStudentGrade.Tables(0).Rows(0)("STATUS"), "")
                    lblGrade.Text = dsStudentGrade.Tables(0).Rows(0)("GRM_DISPLAY")
                End If

                If vFEE_ADJ.vSER_DISCONT_DATE = CDate("1/1/1900") Then
                    lblDiscontdate.Text = "Till Date"
                Else
                    lblDiscontdate.Text = Format(vFEE_ADJ.vSER_DISCONT_DATE, OASISConstants.DateFormat)
                End If

                If vFEE_ADJ.vJOIN_DATE = CDate("1/1/1900") Then
                    trJoinDet.Visible = False
                Else
                    lblJDate.Text = Format(vFEE_ADJ.vJOIN_DATE, OASISConstants.DateFormat)
                End If
                lblStudentNO.Text = FeeCommon.GetStudentNo(vFEE_ADJ.FAH_STU_ID, True)
                lblGrade.Text = vFEE_ADJ.FAH_GRD_ID
                lblReason.Text = getAdjustmentReason(vFEE_ADJ.FAR_EVENT)
                txtAppDate.Text = Format(Date.Now, OASISConstants.DateFormat)
                If vFEE_ADJ.APPR_STATUS <> "N" Then
                    btnApprove.Enabled = False
                    btnReject.Enabled = False

                    Dim ds As New DataSet
                    Dim sqlstr As String = ""
                    sqlstr = "SELECT FEES.FEEADJUSTMENT_S.FAD_FEE_ID FEE_ID,OASIS.FEES.FEES_M.FEE_DESCR FEE_TYPE, FEES.FEEADJUSTMENT_S.FAD_ID,FAD_AMOUNT,FAD_TAX_CODE FEE_TAX_CODE,FAD_TAX_AMOUNT FEE_TAX_AMOUNT,FAD_NET_AMOUNT FEE_NET_AMOUNT"
                    sqlstr = sqlstr & ",FEES.FEEADJUSTMENT_S.FAD_FEE_ID "
                    sqlstr = sqlstr & ",FEES.FEEADJUSTMENT_S.FAD_REMARKS FEE_REMARKS,FAD_NET_AMOUNT CHARGEDAMOUNT,FAD_NET_AMOUNT PAIDAMOUNT,'' DURATION,FAD_AMOUNT FEE_AMOUNT"
                    sqlstr = sqlstr & " FROM FEES.FEEADJUSTMENT_S "
                    sqlstr = sqlstr & "INNER JOIN FEES.FEEADJUSTMENT_H ON FAH_ID=FAD_FAH_ID "
                    sqlstr = sqlstr & "LEFT OUTER  JOIN OASIS.FEES.FEES_M ON OASIS.FEES.FEES_M.FEE_ID = FEES.FEEADJUSTMENT_S.FAD_FEE_ID "
                    sqlstr = sqlstr & "INNER JOIN FEES.FEEADJREQUEST_H ON FAR_ID=Fah_FAR_ID "
                    sqlstr = sqlstr & " where FAR_ID='" & FAR_ID & "'"

                    Try
                        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, sqlstr)
                        gvFeeDetails.DataSource = ds
                        gvFeeDetails.DataBind()

                        For Each gvr As GridViewRow In gvFeeDetails.Rows
                            gvr.Cells(6).Enabled = False

                        Next



                    Catch ex As Exception
                        'lblError.Text = ex.Message
                        usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                    End Try


                Else
                    Session("sFEE_ADJUSTMENT") = vFEE_ADJ
                    GridBindAdjustments()
                End If
                Dim FromDt As String = Format(Date.Now.AddMonths(-12), OASISConstants.DataBaseDateFormat)
                Dim QueryString = "&STUID=" & Encr_decrData.Encrypt(vFEE_ADJ.FAH_STU_ID) & "&STYPE=" & Encr_decrData.Encrypt("S") & "&SBSU=" & Encr_decrData.Encrypt(vFEE_ADJ.FAR_STU_BSU_ID) & "&FDT=" & Encr_decrData.Encrypt(FromDt) & ""
                lbLedger.Attributes.Add("onClick", "return ShowWindowWithClose('FeeReportInterpose.aspx?TYPE=" & Encr_decrData.Encrypt("LEDGER") & QueryString & "', 'STUDENT LEDGER', '60%', '75%');")
                lbTransport.Attributes.Add("onClick", "return ShowWindowWithClose('StudentTransportAudit.aspx?ID=" & Encr_decrData.Encrypt(vFEE_ADJ.FAH_STU_ID) & "', 'STUDENT TRANSPORT AUDIT', '60%', '75%');")
            End If
        End If
    End Sub

    Private Function getAdjustmentReason(ByVal ReasonId As Integer) As String
        Dim strSql As String = "SELECT ARM_DESCR FROM FEES.ADJ_REASON_M WHERE ARM_ID  =" & ReasonId
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, strSql)
    End Function

    Private Sub GridBindAdjustments()
        Dim vFEE_ADJ As FEEADJUSTMENTREQTransport
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
            gvFeeDetails.DataSource = FEEADJUSTMENTREQTransport.GetSubDetailsAsDataTable(vFEE_ADJ)
            gvFeeDetails.DataBind()
        Else
            gvFeeDetails.DataSource = Nothing
            gvFeeDetails.DataBind()
        End If
    End Sub

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()

        If HttpContext.Current.Session("sTotalFeePaid") Is Nothing Then
            Return String.Empty
        End If
        Dim dt As DataTable = HttpContext.Current.Session("sTotalFeePaid")
        Dim STUD_ID As Integer = contextKey
        If dt Is Nothing OrElse dt.Rows.Count < 0 Then
            'sTemp.Append("</table>")
            Return sTemp.ToString()
        End If
        Dim dv As New DataView(dt)
        Dim dgvRow As DataRowView
        dv.RowFilter = " FSH_FEE_ID = " & contextKey
        Dim i As Integer
        While (i < dv.Count)
            dgvRow = dv.Item(i)
            If i = 0 Then
                sTemp.Append("<table class='popdetails'>")
                sTemp.Append("<tr>")
                sTemp.Append("<td colspan=4><b>FEE Details For " & dgvRow("FEE_DESCR") & " </b></td>")
                sTemp.Append("</tr>")
                sTemp.Append("<tr>")
                'sTemp.Append("<td><b>FEE ID</b></td>")
                sTemp.Append("<td><b>DATE</b></td>")
                sTemp.Append("<td><b>ACD. YEAR</b></td>")
                sTemp.Append("<td><b>GRADE</b></td>")
                sTemp.Append("<td><b>CHARG. AMT</b></td>")
                sTemp.Append("<td><b>PAID AMT</b></td>")
                sTemp.Append("</tr>")
            End If
            sTemp.Append("<tr>")
            'sTemp.Append("<td>" & "1" & "</td>")
            sTemp.Append("<td>" & Format(dgvRow("FSH_DATE"), OASISConstants.DateFormat) & "</td>")
            sTemp.Append("<td>" & dgvRow("ACY_DESCR") & "</td>")
            sTemp.Append("<td>" & dgvRow("GRD_DISPLAY") & "</td>")
            sTemp.Append("<td>" & dgvRow("FSH_AMOUNT") & "</td>")
            sTemp.Append("<td>" & dgvRow("FSH_SETTLEAMT") & "</td>")
            'sTemp.Append("<td>" & FEE_SUB_DET.FPD_AMOUNT.ToString & "</td>")
            sTemp.Append("</tr>")
            i += 1
        End While
        sTemp.Append("</table>")

        Return sTemp.ToString()
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub gvFeeDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFeeDetails.RowDataBound
        Dim lblAmt As Label = e.Row.FindControl("lblAmount")
        If Not lblAmt Is Nothing Then
            Dim txtApprAmt As TextBox = e.Row.FindControl("txtApprAmt")
            txtApprAmt.Text = lblAmt.Text

        End If
    End Sub

    Private Function IsValidApprovalDate() As Boolean
        If CDate(txtAppDate.Text) < CDate(txtReqDate.Text) Then
            'lblError.Text = "Approval Date should not be less than Requested Date"
            usrMessageBar2.ShowNotification("Approval Date should not be less than Requested Date", UserControls_usrMessageBar.WarningType.Danger)
            Return False
        End If
        Return True
    End Function

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar2.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Not IsValidApprovalDate() Then
            Exit Sub
        End If
        Dim vFEE_ADJ_APPR As FEEADJUSTMENTREQTransport
        Dim conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
        Dim trans As SqlTransaction
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            trans = conn.BeginTransaction("sFEE_ADJUSTMENTAPPROVAL")
            Try
                vFEE_ADJ_APPR = SaveApprovedAmounts(Session("sFEE_ADJUSTMENT"))
                If vFEE_ADJ_APPR Is Nothing Then
                    trans.Rollback()
                    Exit Sub
                End If
                vFEE_ADJ_APPR.APPR_DATE = CDate(txtAppDate.Text)
                vFEE_ADJ_APPR.APPROVAL_REMARKS = txtApprRemarks.Text
                Dim FAH_ID As String = String.Empty
                Dim retVal As Integer = FEEADJUSTMENTREQTransport.F_SAVEFEEADJAPPROVAL(vFEE_ADJ_APPR, True, FAH_ID, Session("sUsr_name"), conn, trans)

                If retVal > 0 Then
                    trans.Rollback()
                    'lblError.Text = UtilityObj.getErrorMessage(retVal)
                    usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
                Else
                    trans.Commit()

                    'lblError.Text = "The Adjustment Approved..."
                    usrMessageBar2.ShowNotification("The Adjustment Approved...", UserControls_usrMessageBar.WarningType.Success)
                    ViewState("datamode") = "none"
                    ClearAll()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_KEY As String = "FEE ADJUSTMENT APPROVAL"
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, vFEE_ADJ_APPR.FAH_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    Session("ReportSource") = FEEADJUSTMENTTransport.PrintAdjustmentVoucher(FAH_ID, vFEE_ADJ_APPR.FAR_STU_BSU_ID, _
                    Session("sBsuid"), Session("sUsr_name"))
                    h_print.Value = "voucher"
                End If
            Catch ex As Exception
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(1000)
                usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Private Sub ClearAll()
        txtReqDate.Text = ""
        txtHeaderRemarks.Text = ""
        lblStudName.Text = ""
        lblAcademicYear.Text = ""
        lblJDate.Text = ""
        lblStudentNO.Text = ""
        lblGrade.Text = ""
        lblReason.Text = ""
        txtAppDate.Text = ""
        txtApprRemarks.Text = ""
        gvFeeDetails.DataSource = Nothing
        gvFeeDetails.DataBind()
        gvSerUsageHistory.DataSource = Nothing
        gvSerUsageHistory.DataBind()
    End Sub
    Private Function SaveApprovedAmounts(ByVal vFEE_ADJ_APPR As FEEADJUSTMENTREQTransport) As FEEADJUSTMENTREQTransport
        If vFEE_ADJ_APPR Is Nothing Then Return Nothing
        Dim txtApprAmount As TextBox
        Dim vSUB_DET As FeeAdjustmentRequestTransport_S
        Dim lblFEE_ID As Label
        Dim vFeeID As Integer
        Try
            If gvFeeDetails.Rows.Count > 0 Then
                For Each drGVRow As GridViewRow In gvFeeDetails.Rows
                    txtApprAmount = drGVRow.FindControl("txtApprAmt")
                    lblFEE_ID = drGVRow.FindControl("lblFEE_ID")
                    If (Not txtApprAmount Is Nothing) AndAlso (Not lblFEE_ID Is Nothing) Then
                        vFeeID = lblFEE_ID.Text
                        vSUB_DET = vFEE_ADJ_APPR.FEE_ADJ_DET(vFeeID)
                        If Math.Abs(CDbl(txtApprAmount.Text)) > Math.Abs(vSUB_DET.FAD_AMOUNT) Then
                            'lblError.Text = "Approved Amount should not be more than Requested.."
                            usrMessageBar2.ShowNotification("Approved Amount should not be more than Requested..", UserControls_usrMessageBar.WarningType.Danger)
                            Return Nothing
                        End If
                        vSUB_DET.APPROVEDAMOUNT = CDbl(txtApprAmount.Text)
                    End If
                Next

            End If
        Catch ex As Exception
            Return Nothing
        End Try
        Return vFEE_ADJ_APPR
    End Function
    Private Function SaveTaxAmounts(ByVal vFEE_ADJ_APPR As FEEADJUSTMENTREQTransport) As FEEADJUSTMENTREQTransport
        If vFEE_ADJ_APPR Is Nothing Then Return Nothing
        Dim txtApprAmount As TextBox
        Dim lblTaxAmount As Label
        Dim lblNetAmount As Label
        Dim lblOrginalAmount As Label
        Dim lblTaxCode As Label

        Dim vSUB_DET As FeeAdjustmentRequestTransport_S
        Dim lblFEE_ID As Label
        Dim vFeeID As Integer
        SaveTaxAmounts = Nothing
        Try
            If gvFeeDetails.Rows.Count > 0 Then
                For Each drGVRow As GridViewRow In gvFeeDetails.Rows
                    txtApprAmount = drGVRow.FindControl("txtApprAmt")
                    lblTaxAmount = drGVRow.FindControl("lblTaxAmount")
                    lblNetAmount = drGVRow.FindControl("lblNetAmount")
                    lblOrginalAmount = drGVRow.FindControl("lblAmount")
                    lblTaxCode = drGVRow.FindControl("lblTaxCode")

                    lblFEE_ID = drGVRow.FindControl("lblFEE_ID")
                    If (Not txtApprAmount Is Nothing) AndAlso (Not lblFEE_ID Is Nothing) Then
                        vFeeID = lblFEE_ID.Text
                        vSUB_DET = vFEE_ADJ_APPR.FEE_ADJ_DET(vFeeID)
                        If Math.Abs(CDbl(txtApprAmount.Text)) > Math.Abs(vSUB_DET.FAD_AMOUNT) Then
                            'lblError.Text = "Approved Amount should not be more than Requested.."
                            usrMessageBar2.ShowNotification("Approved Amount should not be more than Requested..", UserControls_usrMessageBar.WarningType.Danger)
                            txtApprAmount.Text = lblOrginalAmount.Text
                            Exit Function
                        End If
                        If IsNumeric(txtApprAmount.Text) Then
                            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount(" & txtApprAmount.Text & ",'" & lblTaxCode.Text & "')")
                            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                                lblTaxAmount.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
                                lblNetAmount.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
                            End If
                        End If
                        vSUB_DET.APPROVEDAMOUNT = CDbl(txtApprAmount.Text)
                        vSUB_DET.FAD_TAX_AMOUNT = CDbl(lblTaxAmount.Text)
                        vSUB_DET.FAD_NET_AMOUNT = CDbl(lblNetAmount.Text)


                    End If
                Next
            End If
        Catch ex As Exception
            Return Nothing
        End Try
        Return vFEE_ADJ_APPR
    End Function

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar2.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim vFEE_ADJ_APPR As FEEADJUSTMENTREQTransport
        Dim conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
        Dim trans As SqlTransaction
        Dim FAH_ID As String = String.Empty
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ_APPR = Session("sFEE_ADJUSTMENT")
            vFEE_ADJ_APPR.APPR_DATE = CDate(txtAppDate.Text)
            vFEE_ADJ_APPR.APPROVAL_REMARKS = txtApprRemarks.Text
            trans = conn.BeginTransaction("sFEE_ADJUSTMENTREJECT")
            Dim retVal As Integer = FEEADJUSTMENTREQTransport.F_SAVEFEEADJAPPROVAL(vFEE_ADJ_APPR, False, FAH_ID, Session("sUsr_name"), conn, trans)
            If retVal <> 0 Then
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(retVal)
                usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                trans.Commit()
                'lblError.Text = "The Adjustment Rejected..."
                usrMessageBar2.ShowNotification("The Adjustment Rejected...", UserControls_usrMessageBar.WarningType.Success)
                ClearAll()
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim str_KEY As String = "FEE ADJUSTMENT REJECT"
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
            End If
        End If
    End Sub

    Protected Sub lbLedger_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbLedger.Click
        'F_rptStudentLedger_Transport()
    End Sub

    Private Sub F_rptStudentLedger_Transport()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim FromDate, ToDate As String
        ToDate = Format(Now.Date, OASISConstants.DateFormat)
        FromDate = UtilityObj.GetDataFromSQL("SELECT BSU_TRANSPORT_STARTDT FROM BUSINESSUNIT_M WHERE BSU_ID='" & h_BSU_ID.Value & "'", str_conn)
        If Not IsDate(FromDate) Then
            FromDate = "1/Sep/2007"
        Else
            Format(CDate(FromDate), OASISConstants.DateFormat)
        End If

        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        Dim cmdSub1 As New SqlCommand("[FEES].[F_rptStudentLedgerSummary]")
        Dim cmdSub2 As New SqlCommand("[FEES].[F_rptStudentPDCData]")
        cmd.CommandType = CommandType.StoredProcedure
        cmdSub1.CommandType = CommandType.StoredProcedure
        cmdSub2.CommandType = CommandType.StoredProcedure
        'ALTER procedure rptStudentLedger
        ' @BSU_ID VARCHAR(20)='125016',
        '@STU_IDS VARCHAR(2000)='',
        '@STU_TYPE VARCHAR(5)='S',
        '@FromDT DATETIME ='1-JUN-2008',
        '@ToDT DATETIME='1-JUN-2008'

        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(h_STUD_ID.Value, XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
        cmdSub1.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)
        cmdSub2.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)
        cmdSub1.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
        cmdSub2.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSTU_BSU_ID.Value = h_BSU_ID.Value
        cmd.Parameters.Add(sqlpSTU_BSU_ID)
        cmdSub1.Parameters.AddWithValue("@STU_BSU_ID", h_BSU_ID.Value)
        cmdSub2.Parameters.AddWithValue("@STU_BSU_ID", h_BSU_ID.Value)

        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        sqlpSTU_TYPE.Value = "S"
        cmd.Parameters.Add(sqlpSTU_TYPE)
        cmdSub1.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)
        cmdSub2.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)


        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(FromDate)
        cmd.Parameters.Add(sqlpFromDT)
        cmdSub1.Parameters.AddWithValue("@FromDT", CDate(FromDate))
        cmdSub2.Parameters.AddWithValue("@FromDT", CDate(FromDate))

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(ToDate)
        cmd.Parameters.Add(sqlpTODT)
        cmdSub1.Parameters.AddWithValue("@ToDT", CDate(ToDate))
        cmdSub2.Parameters.AddWithValue("@ToDT", CDate(ToDate))

        cmd.Connection = New SqlConnection(str_conn)
        cmdSub1.Connection = New SqlConnection(str_conn)
        cmdSub2.Connection = New SqlConnection(str_conn)
        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub1
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(1).Command = cmdSub2
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = FromDate
        params("ToDate") = ToDate
        repSource.SubReport = repSourceSubRep

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = Session("sBsuid")
        repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFee_Transport_StudentLedgerNext.rpt"
        Session("ReportSource") = repSource
        h_print.Value = "ledger"
        'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
    End Sub

    Protected Sub lbTransport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbTransport.Click
        'rptstudTransportAudit()
    End Sub

    Sub rptstudTransportAudit()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", h_BSU_ID.Value)
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("studName", lblStudName.Text)
        param.Add("Grade", lblGrade.Text)
        param.Add("Section", h_STU_SCT_DESCR.Value)
        param.Add("StuNo", lblStudentNO.Text)
        param.Add("status", h_STU_STATUS.Value)
        param.Add("stu_id", h_STUD_ID.Value)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../Transport/Reports/RPT/rptstudTransportAudit.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        h_print.Value = "audit"
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub

    Protected Sub txtApprAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim gvr As GridViewRow = DirectCast(DirectCast(sender, TextBox).Parent.Parent, GridViewRow)
        Dim RowIndex As Integer = gvr.RowIndex
        Dim lblTaxAmount As Label = DirectCast(gvr.FindControl("lblTaxAmount"), Label)
        Dim lblNetAmount As Label = DirectCast(gvr.FindControl("lblNetAmount"), Label)
        Dim lblTaxCode As Label = DirectCast(gvr.FindControl("lblTaxCode"), Label)
        'Dim txtApprAmt As TextBox = DirectCast(gvr.FindControl("txtApprAmt"), TextBox)
        If Not lblNetAmount Is Nothing Then
            CALCULATE_TAX(DirectCast(sender, TextBox).Text, lblTaxAmount, lblNetAmount, lblTaxCode.Text)
        End If
    End Sub
    Private Sub CALCULATE_TAX(ByVal Amount As Double, ByRef lblTaxAmt As Label, ByRef lblNETAmt As Label, ByVal TaxCode As String)
        lblTaxAmt.Text = "0.00"
        lblNETAmt.Text = "0.00"

        'If IsNumeric(Amount) Then
        '    Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount(" & Amount & ",'" & TaxCode & "')")
        '    If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
        '        lblTaxAmt.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
        '        lblNETAmt.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
        '    End If
        'End If

        Dim vFEE_ADJ_APPR As FEEADJUSTMENTREQTransport

        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ_APPR = SaveTaxAmounts(Session("sFEE_ADJUSTMENT"))
        End If
        'GridBindAdjustments()



    End Sub

End Class
