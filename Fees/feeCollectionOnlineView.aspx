<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="feeCollectionOnlineView.aspx.vb" Inherits="Fees_OnlineFeePayment_feeCollectionOnlineView"
    Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" lang="javascript">
        Sys.Application.add_load(
            function CheckForPrint() {
                if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                    document.getElementById('<%= h_print.ClientID %>').value = '';
                    //showModelessDialog ('../Reports/ASPX Report/RptViewerModal.aspx', '',"dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;"); 
                    Popup1('../Reports/ASPX Report/RptViewerModal.aspx');
                }
                var frmReceipt = "";
                frmReceipt = "FeeFCOReceipt.aspx";
                if (document.getElementById('<%= h_FCO_print.ClientID %>').value != '') {
                    if (isIE()) {
                        window.showModalDialog(frmReceipt + document.getElementById('<%= h_FCO_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                    } else {
                        Popup1(frmReceipt + document.getElementById('<%= h_FCO_print.ClientID %>').value);
                    }
                }
                if (document.getElementById('<%= h_popup.ClientID%>').value != '') {
                    document.getElementById('<%= h_popup.ClientID%>').value = '';
                    $.fancybox({
                        'autoScale': true,
                        'fitToView': true,
                        'autoSize': true,
                        'transitionIn': 'fade',
                        'transitionOut': 'fade',
                        'type': 'iframe',
                        'content': $('#<%= divOrder.ClientID%>').html(),
                        afterClose: function () { document.getElementById('<%= preOrder.ClientID%>').html = ''; }
                    });
                }
            });
            function isIE() {
                ua = navigator.userAgent;
                /* MSIE used to detect old browsers and Trident used to newer ones*/
                var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
                return is_ie;
            }
            function Receipt() {
                alert("Please note, it may take 5 to 10 minutes to gnerate the receipt. So please verify here or on the fee collection page after the said time frame.")
                $("#GenerateReceipt_Click").click();
            }

    </script>
    <script>
        function Popup1(url) {
            $.fancybox({
                'width': '80%',
                'height': '60%',
                'autoScale': false,
                'fitToView': false,
                'autoSize': false,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url
            });
        };
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr valign="top">
                        <td valign="top" align="left" colspan="2" class="matters">
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table style="width: 100%;">
                    <tr>
                        <td><span class="field-label">Enrollment</span></td>
                        <td>
                            <asp:RadioButtonList ID="rblEnrollment" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                <asp:ListItem Selected="True" Value="S">Enrolled</asp:ListItem>
                                <asp:ListItem Value="E">Enquiry</asp:ListItem>
                            </asp:RadioButtonList></td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="matters" width="20%"><span class="field-label">Select Academic Year</span></td>
                        <td align="left" class="matters" valign="middle" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="matters" align="right" width="20%"><span class="field-label">Status</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True"
                                DataSourceID="SqlStatus" DataTextField="FCO_STATUS" DataValueField="FCO_STATUS">
                            </asp:DropDownList><asp:SqlDataSource ID="SqlStatus" runat="server" ConnectionString="<%$ ConnectionStrings:OASIS_FEESConnectionString %>"
                                SelectCommand="SELECT DISTINCT [FCO_STATUS] FROM [FEES].[VW_OSO_FEES_RECEIPT_ONLINE] WHERE ([FCO_BSU_ID] = @FCO_BSU_ID)">
                                <SelectParameters>
                                    <asp:SessionParameter Name="FCO_BSU_ID" SessionField="sBsuid" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="4">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found"
                                Style="width: 100%; font-size: x-small !important;" AllowPaging="True" PageSize="25" CssClass="table table-row table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="Docno">
                                        <HeaderTemplate>
                                            Receipt No.<br />
                                            <asp:TextBox ID="txtReceiptno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnReceiptSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblReceipt" runat="server" Text='<%# Bind("FCO_FCL_RECNO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Transaction Date<br />
                                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("FCO_DATE", "{0:dd/MMM/yyyy HH:mm:ss}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CollDate">
                                        <HeaderTemplate>
                                            Rec. Date<br />
                                            <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnGradeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("FCL_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student#">
                                        <HeaderTemplate>
                                            Student ID<br />
                                            <asp:TextBox ID="txtStuno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnstunoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Name<br />
                                            <asp:TextBox ID="txtStuname" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnstunameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <HeaderTemplate>
                                            Amount<br />
                                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmountSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("FCO_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderTemplate>
                                            Description<br />
                                            <asp:TextBox ID="txtDesc" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDescSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("FCO_NARRATION") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Payment Gateway">
                                        <HeaderTemplate>
                                            Payment Gateway<br />
                                            <asp:TextBox ID="txtPaymentGateway" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnPGatewaySearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LblPaymentGateway" runat="server" Text='<%# Bind("CPM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Merchant ID" Visible="false">
                                        <HeaderTemplate>
                                            Merchant ID<br />
                                            <asp:TextBox ID="txtMerchantID" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnMerchantIDSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label9" runat="server" Text='<%# Bind("MERCHANTID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Source Portal" Visible="false">
                                        <HeaderTemplate>
                                            Source Portal<br />
                                            <asp:TextBox ID="txtSourcePortal" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSourcePortalSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LblSourcePortal" runat="server" Text='<%# Bind("SOURCE_PORTAL") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="GateWay Type">
                                        <HeaderTemplate>
                                            GateWay Type<br />
                                            <asp:TextBox ID="txtGateWayType" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnGateWayType" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGatewayType" runat="server" Text='<%# Bind("GATEWAY_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Try Count" Visible="false">
                                        <HeaderTemplate>
                                            Try Count<br />
                                            <asp:TextBox ID="txtTryCount" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btntxtTryCount" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label12" runat="server" Text='<%# Bind("FCO_TRYCOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Last Try Date" Visible="false">
                                        <HeaderTemplate>
                                            Last Try Date<br />
                                            <asp:TextBox ID="txtLastTryDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btntxtLastTryDate" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LblLastTry" runat="server" Text='<%# Bind("LASTTRYDATE", "{0:dd/MMM/yyyy HH:mm:ss}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Src Ref Type" Visible="false">
                                        <HeaderTemplate>
                                            Src Ref Type<br />
                                            <asp:TextBox ID="txtSrcRefType" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSrcRefType" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label14" runat="server" Text='<%# Bind("SRC_REF_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Payment RefId" Visible="false">
                                        <HeaderTemplate>
                                            FCO ID<br />
                                            <asp:TextBox ID="txtFcoFcoID" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnFcoFcoID" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFCO_FCO_ID" runat="server" Text='<%# Bind("FCO_FCO_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pay Mode" Visible="true">
                                        <HeaderTemplate>
                                            Pay Mode<br />
                                            <asp:TextBox ID="txtpaymode" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnpaymode" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Lblpaymode" runat="server" Text='<%# Bind("FCO_PAY_MODE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AuthCode" Visible="true">
                                        <HeaderTemplate>
                                            AuthCode<br />
                                            <asp:TextBox ID="txtauthcode" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnauthcode" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Lblauthcode" runat="server" Text='<%# Bind("FCO_VPC_BANKAUTID")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View Collection" Visible="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblViewCollection" OnClick="lblViewCollection_Click" runat="server">View Collection</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print Receipt" Visible="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbPrint" runat="server" OnClick="lbPrint_Click">Print Receipt</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Gateway Response">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbFetchOrder" runat="server" OnClick="lbFetchOrder_Click">Gateway Response</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Generate Receipt">
                                        <ItemTemplate>
                                            <asp:LinkButton OnClientClick="return Receipt();" ID="lbGenerateReceipt" runat="server" OnClick="GenerateReceipt_Click">Generate Receipt</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_selected_menu_3" runat="server" />
                <asp:HiddenField ID="h_selected_menu_4" runat="server" />
                <asp:HiddenField ID="h_selected_menu_5" runat="server" />
                <asp:HiddenField ID="h_selected_menu_6" runat="server" />
                <asp:HiddenField ID="h_selected_menu_7" runat="server" />
                <asp:HiddenField ID="h_selected_menu_8" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_FCO_print" runat="server" />
                <asp:HiddenField ID="h_popup" runat="server" />
            </div>
        </div>

        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
    <div id="divOrder" runat="server" style="display: none;">
        <pre id="preOrder" runat="server">

        </pre>
    </div>
</asp:Content>
