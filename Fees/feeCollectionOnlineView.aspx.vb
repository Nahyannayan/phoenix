Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Collections.Generic
Imports System.Linq
Imports RestSharp
Imports Newtonsoft.Json.Linq
Imports System.Web.Script.Serialization
Imports System.Security.Cryptography
Imports System.Xml
Imports System.Net
Imports com.ni.dp.util
Imports GEMSRSAEncryption
Imports Newtonsoft.Json

Partial Class Fees_OnlineFeePayment_feeCollectionOnlineView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_ONLINE_FEECOLLECTION_VIEW Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_ONLINE_FEECOLLECTION_VIEW
                            lblHead.Text = "Online Fee Collection"
                    End Select
                    ddlAcademicYear.Items.Clear()
                    ddlAcademicYear.DataSource = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
                    ddlAcademicYear.DataTextField = "ACY_DESCR"
                    ddlAcademicYear.DataValueField = "ACD_ID"
                    ddlAcademicYear.DataBind()
                    ddlAcademicYear.SelectedIndex = -1
                    ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                    ddlStatus.DataBind()
                    VerifyGateWayResponseAccess()
                    gridbind()
                    h_popup.Value = ""
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                ' lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim ds As New DataSet
            Dim lstrOpr As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim str_Filter As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim lstrCondn6 As String = String.Empty
            Dim lstrCondn7 As String = String.Empty
            Dim lstrCondn8 As String = String.Empty
            Dim lstrCondn9 As String = String.Empty
            Dim lstrCondn10 As String = String.Empty
            Dim lstrCondn11 As String = String.Empty
            Dim lstrCondn12 As String = String.Empty
            Dim lstrCondn13 As String = String.Empty
            Dim lstrCondn14 As String = String.Empty
            Dim lstrCondn15 As String = String.Empty
            Dim lstrCondn16 As String = String.Empty

            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtReceiptno FCL_RECNO
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCO_FCL_RECNO", lstrCondn1)

                '   -- 2  txtDate
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCO_DATE", lstrCondn2)

                '   -- 3  txtGrade
                larrSearchOpr = h_selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRD_DISPLAY", lstrCondn3)

                '   -- 4   txtStuno
                larrSearchOpr = h_selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn4)

                '   -- 5  txtStuname
                larrSearchOpr = h_selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn5)

                '   -- 6  txtAmount
                larrSearchOpr = h_selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
                lstrCondn6 = txtSearch.Text
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCO_AMOUNT", lstrCondn6)

                '   -- 7  txtDesc
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
                lstrCondn7 = txtSearch.Text
                If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCO_NARRATION", lstrCondn7)

                '   -- 8  txtPGateway
                larrSearchOpr = h_selected_menu_8.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtPaymentGateway")
                lstrCondn8 = txtSearch.Text
                If (lstrCondn8 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "CPM_DESCR", lstrCondn8)


                '   -- 9 txtMerchantID
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtMerchantID")
                lstrCondn9 = txtSearch.Text
                If (lstrCondn9 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MERCHANTID", lstrCondn9)




                '   -- 10 txtSourcePortal
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtSourcePortal")
                lstrCondn10 = txtSearch.Text
                If (lstrCondn10 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "SOURCE_PORTAL", lstrCondn10)


                '   -- 11 txtGateWayType
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGateWayType")
                lstrCondn11 = txtSearch.Text
                If (lstrCondn11 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GATEWAY_TYPE", lstrCondn11)



                '   -- 12 txtTryCount
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtTryCount")
                lstrCondn12 = txtSearch.Text
                If (lstrCondn12 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCO_TRYCOUNT", lstrCondn12)



                '   -- 13 txtLastTryDate
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtLastTryDate")
                lstrCondn13 = txtSearch.Text
                If (lstrCondn13 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "LASTTRYDATE", lstrCondn13)


                '   -- 14 txtSrcRefType
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtSrcRefType")
                lstrCondn14 = txtSearch.Text
                If (lstrCondn14 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "SRC_REF_TYPE", lstrCondn14)


                '   -- 14 txtFcoFcoID
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtFcoFcoID")
                lstrCondn15 = txtSearch.Text
                If (lstrCondn15 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCO_FCO_ID", lstrCondn15)

                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtpaymode")
                lstrCondn16 = txtSearch.Text
                If (lstrCondn16 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCO_PAY_MODE", lstrCondn16)


            End If
            'Dim STR_RECEIPT As String = "FEES.VW_OSO_FEES_RECEIPT_ONLINE"
            'If rbEnquiry.Checked Then
            '    STR_RECEIPT = "FEES.VW_OSO_FEES_RECEIPT_ENQUIRY"
            'End If
            str_Sql = "SELECT * FROM FEES.VW_ONLINE_FEECOLLECTION WHERE FCO_BSU_ID='" & Session("sBSUID") & "'" _
            & " AND FCO_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' AND FCO_STU_TYPE = '" & rblEnrollment.SelectedValue & "' AND FCO_STATUS='" & ddlStatus.SelectedItem.Value & "' " & str_Filter _
            & " ORDER BY FCO_DATE DESC, FCO_FCL_RECNO DESC"

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)

            Dim columns As List(Of DataControlField) = gvJournal.Columns.Cast(Of DataControlField)().ToList()

            If (ddlStatus.SelectedItem.Value = "INITIATED") Then
                columns.Find(Function(col) col.HeaderText = "Merchant ID").Visible = True
                columns.Find(Function(col) col.HeaderText = "Source Portal").Visible = True
                columns.Find(Function(col) col.HeaderText = "GateWay Type").Visible = True
                columns.Find(Function(col) col.HeaderText = "Try Count").Visible = True
                columns.Find(Function(col) col.HeaderText = "Last Try Date").Visible = True
                columns.Find(Function(col) col.HeaderText = "Src Ref Type").Visible = True
                columns.Find(Function(col) col.HeaderText = "Payment RefId").Visible = True
                columns.Find(Function(col) col.HeaderText = "View Collection").Visible = True
                columns.Find(Function(col) col.HeaderText = "Docno").Visible = False
                columns.Find(Function(col) col.HeaderText = "Print Receipt").Visible = False
                columns.Find(Function(col) col.HeaderText = "CollDate").Visible = False
                columns.Find(Function(col) col.HeaderText = "Generate Receipt").Visible = False

            Else
                columns.Find(Function(col) col.HeaderText = "CollDate").Visible = True
                columns.Find(Function(col) col.HeaderText = "Docno").Visible = True
                'columns.Find(Function(col) col.HeaderText = "Print Receipt").Visible = True
                columns.Find(Function(col) col.HeaderText = "Merchant ID").Visible = True
                columns.Find(Function(col) col.HeaderText = "Source Portal").Visible = True
                columns.Find(Function(col) col.HeaderText = "GateWay Type").Visible = True
                columns.Find(Function(col) col.HeaderText = "Try Count").Visible = False
                columns.Find(Function(col) col.HeaderText = "Last Try Date").Visible = False
                columns.Find(Function(col) col.HeaderText = "Src Ref Type").Visible = True
                columns.Find(Function(col) col.HeaderText = "Payment RefId").Visible = True
                columns.Find(Function(col) col.HeaderText = "View Collection").Visible = False
                columns.Find(Function(col) col.HeaderText = "Generate Receipt").Visible = False
                columns.Find(Function(col) col.HeaderText = "AuthCode").Visible = True
            End If
            columns.Find(Function(col) col.HeaderText = "Gateway Response").Visible = False
            If (Not Session("sBITSupport") Is Nothing AndAlso Convert.ToBoolean(Session("sBITSupport")) = True) Or
               (ViewState("AccessGateWayResponse") = True) Then
                columns.Find(Function(col) col.HeaderText = "Gateway Response").Visible = True
                columns.Find(Function(col) col.HeaderText = "Generate Receipt").Visible = True
                columns.Find(Function(col) col.HeaderText = "Try Count").Visible = True
                columns.Find(Function(col) col.HeaderText = "Last Try Date").Visible = True
            End If
            If (ddlStatus.SelectedItem.Value = "SUCCESS" And ViewState("AccessGateWayResponse") = True) Then
                columns.Find(Function(col) col.HeaderText = "Generate Receipt").Visible = False
            End If

            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If

            txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn5

            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = lstrCondn6

            txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
            txtSearch.Text = lstrCondn7

            txtSearch = gvJournal.HeaderRow.FindControl("txtPaymentGateway")
            txtSearch.Text = lstrCondn8

            txtSearch = gvJournal.HeaderRow.FindControl("txtMerchantID")
            txtSearch.Text = lstrCondn9

            txtSearch = gvJournal.HeaderRow.FindControl("txtSourcePortal")
            txtSearch.Text = lstrCondn10

            txtSearch = gvJournal.HeaderRow.FindControl("txtGateWayType")
            txtSearch.Text = lstrCondn11

            txtSearch = gvJournal.HeaderRow.FindControl("txtTryCount")
            txtSearch.Text = lstrCondn12

            txtSearch = gvJournal.HeaderRow.FindControl("txtLastTryDate")
            txtSearch.Text = lstrCondn13

            txtSearch = gvJournal.HeaderRow.FindControl("txtSrcRefType")
            txtSearch.Text = lstrCondn14

            txtSearch = gvJournal.HeaderRow.FindControl("txtFcoFcoID")
            txtSearch.Text = lstrCondn15

            txtSearch = gvJournal.HeaderRow.FindControl("txtpaymode")
            txtSearch.Text = lstrCondn16


        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub GenerateReceipt_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblFCO_ID As Label = sender.Parent.parent.findcontrol("lblFCO_FCO_ID")
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SOURCE", SqlDbType.VarChar, 20)
            pParms(0).Value = "SCHOOL"
            pParms(1) = New SqlClient.SqlParameter("@FCO_FCO_ID", SqlDbType.BigInt)
            pParms(1).Value = Convert.ToInt32(lblFCO_ID.Text)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "FEES.RESET_MPGS_TRY_COUNT", pParms)
            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        h_FCO_print.Value = ""
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try

            Dim lblReceipt As New Label
            Dim LblSourcePortal As New Label
            Dim LblPaymentGateway As New Label
            Dim LblLastTry As New Label

            lblReceipt = TryCast(e.Row.FindControl("lblReceipt"), Label)
            LblPaymentGateway = TryCast(e.Row.FindControl("LblPaymentGateway"), Label)
            LblSourcePortal = TryCast(e.Row.FindControl("LblSourcePortal"), Label)
            LblLastTry = TryCast(e.Row.FindControl("LblLastTry"), Label)

            Dim lbPrint As New LinkButton
            lbPrint = TryCast(e.Row.FindControl("lbPrint"), LinkButton)
            If lbPrint IsNot Nothing And lblReceipt IsNot Nothing Then
                If lblReceipt.Text.Replace("&nbsp;", "").Trim = "" Then
                    lbPrint.Visible = False
                End If
            End If
            If LblPaymentGateway IsNot Nothing AndAlso LblPaymentGateway.Text <> "" Then
                If LblPaymentGateway.Text.ToUpper = "NBAD" Then
                    LblPaymentGateway.Text = "Credit/Debit Card"
                End If
                If LblPaymentGateway.Text.ToUpper = "NBAD GEMS CO-BRANDED" Then
                    LblPaymentGateway.Text = "Co-Branded Card"
                End If
            End If
            If LblSourcePortal IsNot Nothing AndAlso LblSourcePortal.Text <> "" Then
                If LblSourcePortal.Text.ToUpper = "GEMS_MOBILE_APP" Then
                    LblSourcePortal.Text = "Gems Connect"
                ElseIf LblSourcePortal.Text.ToUpper.Contains("PARENT") Then
                    LblSourcePortal.Text = "Gems Parent Portal"
                End If
            End If
            If LblLastTry IsNot Nothing AndAlso LblLastTry.Text <> "" Then
                If LblLastTry.Text = "01/Jan/1900 00:00:00" Then
                    LblLastTry.Text = ""
                End If
            End If
            Dim lbFetchOrder As New LinkButton
            lbFetchOrder = TryCast(e.Row.FindControl("lbFetchOrder"), LinkButton)
            Dim lblFCO_FCO_ID As Label = TryCast(e.Row.FindControl("lblFCO_FCO_ID"), Label) 'sender.Parent.parent.findcontrol("lblFCO_FCO_ID")
            If Not lbFetchOrder Is Nothing Then
                'lbFetchOrder.Attributes.Add("onClick", "ShowOrder();")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        h_FCO_print.Value = ""
        gridbind()
    End Sub

    Protected Sub lbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
            'PrintReceipt(lblReceipt.Text)
            Session("ReportSource") = FeeCollection.PrintReceipt(lblReceipt.Text, Session("sBsuid"), False, Session("sUsr_name"), False)
            h_print.Value = "print"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        h_FCO_print.Value = ""
        gridbind()
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStatus.SelectedIndexChanged
        h_FCO_print.Value = ""
        gridbind()
    End Sub

    Protected Sub lblViewCollection_Click(sender As Object, e As EventArgs)
        Try
            Dim lblFCO_ID As Label = sender.Parent.parent.findcontrol("lblFCO_FCO_ID")
            'Session("ReportSource") = FeeCollection.PrintReceipt(lblReceipt.Text, Session("sBsuid"), rbEnquiry.Checked, Session("sUsr_name"), False)
            SetVersionforFCOReceipt(lblFCO_ID.Text)
            h_FCO_print.Value = "?type=REC_FCO&id=" + Encr_decrData.Encrypt(lblFCO_ID.Text) &
              "&bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) &
              "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) &
              "&isenq=" & Encr_decrData.Encrypt(True) &
              "&iscolln=" & Encr_decrData.Encrypt(False) & "&isexport=1"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub
    Private Sub SetVersionforReceipt(ByVal RecNo As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FCL_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = Session("sbsuid")
        pParms(1) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 20)
        pParms(1).Value = RecNo
        pParms(2) = New SqlClient.SqlParameter("@bENQUIRY", SqlDbType.Bit)
        pParms(2).Value = True  'Not used
        pParms(3) = New SqlClient.SqlParameter("@bCOLLECTION", SqlDbType.Bit)
        pParms(3).Value = 0
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.GetFeeReceipt", pParms)

        Dim BSU_CURRENCY As String = ""
        Dim BSU_bBSU_Version_Enabled As Boolean
        BSU_bBSU_Version_Enabled = False
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Columns.Contains("BSU_bBSU_Version_Enabled") Then
                BSU_bBSU_Version_Enabled = ds.Tables(0).Rows(0)("BSU_bBSU_Version_Enabled")
                If BSU_bBSU_Version_Enabled Then
                    Dim BSU_LOGO_VER_NO As String, BSU_VER_NO As String
                    BSU_LOGO_VER_NO = "0"
                    BSU_VER_NO = "0"
                    BSU_LOGO_VER_NO = CInt(ds.Tables(0).Rows(0)("BSU_LOGO_VER_NO").ToString)
                    BSU_VER_NO = CInt(ds.Tables(0).Rows(0)("BSU_VER_NO").ToString)
                    Dim sqlParam(1) As SqlParameter
                    sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
                    sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_VER_NO", BSU_VER_NO, SqlDbType.Int)
                    Dim dtBSUDetail As New DataTable
                    dtBSUDetail = Mainclass.getDataTable("SP_GET_BUSINESSUNIT_DETAILS", sqlParam, ConnectionManger.GetOASISConnectionString)

                End If
            End If
        End If
    End Sub

    Private Sub SetVersionforFCOReceipt(ByVal FCO_ID As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FCO_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = Session("sbsuid")
        pParms(1) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = FCO_ID

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.GET_FCO_FEERECEIPT", pParms)

        Dim BSU_CURRENCY As String = ""
        Dim BSU_bBSU_Version_Enabled As Boolean
        BSU_bBSU_Version_Enabled = False
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Columns.Contains("BSU_bBSU_Version_Enabled") Then
                BSU_bBSU_Version_Enabled = ds.Tables(0).Rows(0)("BSU_bBSU_Version_Enabled")
                If BSU_bBSU_Version_Enabled Then
                    Dim BSU_LOGO_VER_NO As String, BSU_VER_NO As String
                    BSU_LOGO_VER_NO = "0"
                    BSU_VER_NO = "0"
                    BSU_LOGO_VER_NO = CInt(ds.Tables(0).Rows(0)("BSU_LOGO_VER_NO").ToString)
                    BSU_VER_NO = CInt(ds.Tables(0).Rows(0)("BSU_VER_NO").ToString)
                    Dim sqlParam(1) As SqlParameter
                    sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
                    sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_VER_NO", BSU_VER_NO, SqlDbType.Int)
                    Dim dtBSUDetail As New DataTable
                    dtBSUDetail = Mainclass.getDataTable("SP_GET_BUSINESSUNIT_DETAILS", sqlParam, ConnectionManger.GetOASISConnectionString)

                End If
            End If
        End If
    End Sub

    Protected Sub lbFetchOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        preOrder.InnerHtml = "<p>No Data</p>"
        h_popup.Value = ""
        Dim lblFCO_FCO_ID As Label = sender.Parent.parent.findcontrol("lblFCO_FCO_ID")
        Dim lblGatewayType As Label = sender.Parent.parent.findcontrol("lblGatewayType")

        If Not lblFCO_FCO_ID Is Nothing AndAlso Not lblGatewayType Is Nothing Then
            Dim objcls As New ClsPayment
            objcls.GET_PAYMENT_REDIRECT("SCHOOL", lblFCO_FCO_ID.Text, Session("sBsuid"))
            If lblGatewayType.Text = "MPGS" Then

                Dim recno As String = "", msgServer As String = "", retval As String = "", FetchOrderURL As String = ""
                Try
                    FetchOrderURL = objcls.API_URL.Replace("session", "order/") & objcls.MERCHANT_TXN_REF_ID

                    Dim client = New RestClient(FetchOrderURL)
                    Dim Request = New RestRequest(Method.GET)
                    Request.AddHeader("cache-control", "no-cache")
                    Request.AddHeader("Accept-Encoding", "gzip, deflate")
                    Request.AddHeader("Host", "ap-gateway.mastercard.com")
                    Request.AddHeader("Cache-Control", "no-cache")
                    Request.AddHeader("Accept", "*/*")
                    Request.AddHeader("Authorization", "Basic " & Convert.ToBase64String(Encoding.ASCII.GetBytes("merchant." & objcls.MERCHANT_ID & ":" & objcls.API_PASSWORD)))
                    Request.AddHeader("Content-Type", "application/json")

                    Dim Response As IRestResponse = client.Execute(Request)
                    Dim jsonResponse = JObject.Parse(Response.Content)
                    Dim jsonReq = New JavaScriptSerializer().Serialize(jsonResponse)
                    preOrder.InnerHtml = jsonResponse.ToString()
                    h_popup.Value = "print"
                    'Dim ScriptName As String = "POP"
                    'Dim Script As New StringBuilder
                    'Script.Append("alert('here');")
                    'If Not ClientScript.IsStartupScriptRegistered(ScriptName) Then
                    '    ClientScript.RegisterClientScriptBlock(Me.GetType(), ScriptName, Script.ToString(), True)
                    'End If
                Catch ex As Exception

                End Try
            ElseIf lblGatewayType.Text = "EBPG" Then
                GetEBPGOrderDetails(lblFCO_FCO_ID.Text, objcls)
            ElseIf lblGatewayType.Text = "DIRECPAY" Then
                GetDirecPayOrderDetails(lblFCO_FCO_ID.Text, objcls)
            ElseIf lblGatewayType.Text = "QLESS" Then
                GetQLESSOrderDetails(lblFCO_FCO_ID.Text, objcls)
            ElseIf lblGatewayType.Text = "INDUS" Then
                GetIndusOrderDetails(lblFCO_FCO_ID.Text, objcls)
            Else
                GetMIGSOrderDetails(lblFCO_FCO_ID.Text, objcls)
            End If
        End If
    End Sub

    Private Sub GetMIGSOrderDetails(ByVal pFCO_FCO_ID As String, ByRef objcls As ClsPayment)
        Dim strResponse As String = "", User As String = "", Password As String = "", Accesscode As String = "", MerchantID As String = ""
        'Dim objcls As New ClsPayment
        objcls.GET_MIGS_QUERY_DR_INFO(pFCO_FCO_ID, Session("sBsuId"))
        Dim myWebClient As New System.Net.WebClient
        Dim ValueCollection As New System.Collections.Specialized.NameValueCollection
        ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_User"), System.Web.HttpUtility.UrlEncode(objcls.BSU_QUERYDRUSER))
        ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Password"), System.Web.HttpUtility.UrlEncode(objcls.BSU_QUERYDRPASSWORD))
        ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_AccessCode"), System.Web.HttpUtility.UrlEncode(objcls.BSU_MERCHANTCODE))
        ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Merchant"), System.Web.HttpUtility.UrlEncode(objcls.MERCHANT_ID))
        ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Command"), System.Web.HttpUtility.UrlEncode("queryDR"))
        ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_MerchTxnRef"), System.Web.HttpUtility.UrlEncode(pFCO_FCO_ID))
        ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Version"), System.Web.HttpUtility.UrlEncode("1"))

        Dim responseArray As Byte() = myWebClient.UploadValues("https://migs.mastercard.com.au/vpcdps", "POST", ValueCollection)
        strResponse = System.Text.Encoding.ASCII.GetString(responseArray)
        Dim params As New Hashtable
        splitResponse(strResponse, params)
        If params.Count > 0 Then
            preOrder.InnerHtml = "<html>"
            For Each entry As DictionaryEntry In params
                preOrder.InnerHtml &= "<br />" & entry.Key.ToString & " = " & entry.Value.ToString() & ""
            Next
            preOrder.InnerHtml &= "<br /><hr><br />Note: <span style='background:yellow;font-weight:bold'>If vpc_FoundMultipleDRs='N',vpc_DRExists='Y',vpc_AcqResponseCode='00',vpc_TxnResponseCode='0' means it is a valid payment</span>"
            preOrder.InnerHtml &= "</html>"
        End If
        h_popup.Value = "print"

    End Sub
    Public Shared Sub splitResponse(ByVal data As String, ByRef params As Hashtable)
        Dim pairs As String()
        Dim equalsIndex As Integer
        Dim name As String
        Dim value As String
        ' Check if there was a response
        If Len(data) > 0 Then
            ' Check if there are any paramenters in the response
            If InStr(data, "=") > 0 Then
                ' Get the parameters out of the response
                pairs = Split(data, "&")
                For Each pair As String In pairs
                    ' If there is a key/value pair in this item then store it
                    equalsIndex = InStr(pair, "=")
                    If equalsIndex > 1 And Len(pair) > equalsIndex Then
                        name = Left(pair, equalsIndex - 1)
                        value = Right(pair, Len(pair) - equalsIndex)
                        params.Add(name, System.Web.HttpUtility.UrlEncode(value))
                    End If
                Next
            Else ' There were no parameters so create an error
                params.Add("vpc_Message", "The data contained in the response was invalid or corrupt, the data is: <pre>" & data & "</pre>")
            End If
        Else ' There was no data so create an error
            params.Add("vpc_Message", "There was no data contained in the response")
        End If

    End Sub
    Private Shared Function null2unknown(ByVal req As Object) As String
        Try
            If req Is Nothing Then
                Return String.Empty
            Else
                Return System.Web.HttpUtility.UrlDecode(req.ToString().Trim)
            End If
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    Public Shared Function CreateSHA512Signature(ByVal RawData As String) As String
        '    <summary>Creates a SHA512 Signature</summary>
        '    <param name="RawData">The string used to create the signautre.</param>

        Dim hasher As SHA512 = SHA512.Create()
        Dim HashValue As Byte() = hasher.ComputeHash(Encoding.ASCII.GetBytes(RawData))

        Dim strHex As String = ""
        For Each b As Byte In HashValue
            strHex &= b.ToString("x2")
        Next

        Return strHex
    End Function
    Private Sub GetEBPGOrderDetails(ByVal pFCO_FCO_ID As String, ByRef objcls As ClsPayment)
        Try
            Dim qry As String = "SELECT TOP 1 ISNULL(OPA_DATA, '') OPA_DATA FROM OASISAUDIT.FEES.ONLINE_PAYMENT_AUDIT WITH(NOLOCK) WHERE OPA_FOC_ID = " & pFCO_FCO_ID & " ORDER BY OPA_LOG_TIME DESC"
            Dim ResponseXML As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISAuditConnectionString, CommandType.Text, qry)
            Dim ResponseJson As String = String.Empty

            If ResponseXML.Trim() <> "" Then
                Dim doc As New XmlDocument
                doc.LoadXml(ResponseXML)
                ResponseJson = JsonConvert.SerializeXmlNode(doc)
                Dim jsonResponse = JObject.Parse(ResponseJson)
                preOrder.InnerHtml = jsonResponse.ToString
            Else
                preOrder.InnerHtml = "<html><br/>"
                preOrder.InnerHtml &= "<br /><hr><br />Error: <span style='color:red;font-weight:bold'>Unable to find the query response</span>"
                preOrder.InnerHtml &= "</html>"
            End If

            h_popup.Value = "print"
        Catch ex As Exception
            preOrder.InnerHtml = "<html><br/>"
            preOrder.InnerHtml &= "<br /><hr><br />Error: <span style='color:red;font-weight:bold'>" & ex.Message & "</span>"
            preOrder.InnerHtml &= "</html>"
        End Try
    End Sub
    Private Sub GetDirecPayOrderDetails(ByVal pFCO_FCO_ID As String, ByVal objcls As ClsPayment)
        Dim aesEncrypt = New EncDec()
        objcls.GET_PAYMENT_REDIRECT("SCHOOL", pFCO_FCO_ID, Session("sBsuid"))

        Try

            Dim requestObj = New QueryRequest()
            requestObj.MerchantID = objcls.MERCHANT_ID
            requestObj.CollaboratorID = objcls.MERCHANT_CODE
            requestObj.TransactionDetails().ReferenceID = Nothing
            requestObj.TransactionDetails().MerchantOrderNumber = Convert.ToString(objcls.FCO_FCO_ID)
            requestObj.TransactionDetails().TransactionType = Nothing
            requestObj.TransactionDetails().AdditionalDetails = "Y"

            Dim RequestString As String = Convert.ToString(requestObj.MerchantID) + "||" + requestObj.CollaboratorID + "||" + aesEncrypt.Encrypt(objcls.API_PASSWORD, clsDirecPay.configureRequest(requestObj)) '"1||010|3346224|Y"

            Dim client = New RestClient(objcls.QUERYDR_QUERY_URL)
            client.Timeout = -1
            Dim request = New RestRequest(Method.POST)
            request.AddHeader("Content-Type", "application/xml")
            Dim doc As New XmlDocument
            doc.LoadXml("<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://uilogic.dp.toml.com/'><SOAP-ENV:Body>  <ns1:invokeQueryAPI>      <requestparameters>" + RequestString + "</requestparameters>    </ns1:invokeQueryAPI>  </SOAP-ENV:Body></SOAP-ENV:Envelope>")
            request.AddParameter("application/xml", doc.InnerXml, ParameterType.RequestBody)
            Dim Response As IRestResponse = client.Execute(request)
            Dim docR As New XmlDocument
            docR.LoadXml(Response.Content)
            Dim objResponse As QueryResponse = clsDirecPay.getQueryResponse(docR.InnerText)

            preOrder.InnerHtml = "<html><br/>"
            preOrder.InnerHtml &= JObject.FromObject(objResponse).ToString()
            preOrder.InnerHtml &= "<br /><hr><br />Note: <span style='background:yellow;font-weight:bold'>If StatusFlag:'SUCCESS',Querydescription:'PAYMENT SUCCESS' in [TransactionResponseStatus] means it is a valid payment</span>"
            preOrder.InnerHtml &= "</html>"


            h_popup.Value = "print"

        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetQLESSOrderDetails(ByVal pFCO_FCO_ID As String, ByVal objcls As ClsPayment)
        objcls.GET_PAYMENT_REDIRECT("SCHOOL", pFCO_FCO_ID, Session("sBsuid"))
        Dim token As String
        Dim ownerServID As String
        Try
            Dim client = New RestClient(objcls.API_URL + "/token")
            client.Timeout = -1
            Dim Request = New RestRequest(Method.POST)
            Request.AddHeader("Content-Type", "application/x-www-form-urlencoded")
            Request.AddParameter("grant_type", "password")
            Request.AddParameter("username", objcls.MERCHANT_ID)
            Request.AddParameter("password", objcls.MERCHANT_CODE)
            Request.AddParameter("app_secret", objcls.API_PASSWORD)
            Request.AddParameter("type", "external")
            Dim tokenResponse As IRestResponse = client.Execute(Request)

            If (tokenResponse.StatusCode.ToString = "OK") Then
                Dim data = JObject.Parse(tokenResponse.Content)
                token = ValidateValue(data.Property("access_token"))
                ownerServID = ValidateValue(data.Property("ownerServID"))

                client = New RestClient(objcls.API_URL & "/api/v1/external/order/info")
                Request = New RestRequest(Method.POST)
                client.Timeout = -1
                Request.AddHeader("Authorization", "Bearer " & token)
                Request.AddHeader("Content-Type", "application/json")
                Request.AddParameter("application/json", "{""merchantOrderId"":" + objcls.FCO_FCO_ID.ToString + ",""serviceID"":""" + ownerServID + """}", ParameterType.RequestBody)
                Dim Response As IRestResponse = client.Execute(Request)
                Dim jsonResponse = JObject.Parse(Response.Content)
                preOrder.InnerHtml = "<html><br/>"
                preOrder.InnerHtml &= jsonResponse.ToString()

                preOrder.InnerHtml &= "<br /><hr><br />Note: <span style='background:yellow;font-weight:bold'>If orderStatus:'SUCCESS' means it is a valid payment</span>"
                preOrder.InnerHtml &= "</html>"


                h_popup.Value = "print"
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GetIndusOrderDetails(ByVal pFCO_FCO_ID As String, ByRef objcls As ClsPayment)
        Try

            Dim objInvoice As New clsInvoice With {
            .inv_no = objcls.MERCHANT_TXN_REF_ID,
           .request_id = "GEMS-" & objcls.FCO_FCO_ID
       }
            Dim RSACS As New RSACryptoservice
            Dim json = JsonConvert.SerializeObject(objInvoice)
            Dim KeyPath As String = Server.MapPath("../Fees/Key/IndusPubKey.pem")
            RSACS.RSACryptoServiceProvider(objcls.GEMS_PRIVATE_KEY, KeyPath)
            Dim EncrCollData As String = RSACS.Encrypt(json)

            Dim FetchOrderURL As String = objcls.QUERYDR_QUERY_URL
            Dim t As New Dictionary(Of String, String) '= New Dictionary(Of String, String)() {"Request", EncrCollData})
            t.Add("Request", EncrCollData)

            Dim postString As String = JsonConvert.SerializeObject(t)
            Const ContType As String = "application/json"

            Dim webRequest As HttpWebRequest = TryCast(Net.WebRequest.Create(FetchOrderURL), HttpWebRequest)
            webRequest.Method = "POST"
            webRequest.ContentType = ContType
            webRequest.ContentLength = postString.Length
            webRequest.Headers.Add("x-ibm-client-id", objcls.BSU_QUERYDRUSER)
            webRequest.Headers.Add("x-ibm-client-secret", objcls.BSU_QUERYDRPASSWORD)

            Dim requestWriter As New StreamWriter(webRequest.GetRequestStream())
            requestWriter.Write(postString)
            requestWriter.Close()
            Dim responseReader As StreamReader
            responseReader = Nothing
            responseReader = New StreamReader(webRequest.GetResponse().GetResponseStream())
            Dim responseData As String = responseReader.ReadToEnd()
            If Not responseReader Is Nothing Then
                responseReader.Close()
                webRequest.GetResponse().Close()
            End If
            Dim DecryptedResponseData As String = ""
            If Not responseData Is Nothing AndAlso responseData.Trim() <> "" Then '---------------if response is not empty
                Try
                    DecryptedResponseData = RSACS.Decrypt(responseData)
                Catch ex As Exception
                    DecryptedResponseData = responseData.Trim()
                End Try
                Dim jsonResponse = JObject.Parse(DecryptedResponseData)
                preOrder.InnerHtml = jsonResponse.ToString()
                preOrder.InnerHtml &= "<br /><hr><br />Note: <span style='background:yellow;font-weight:bold'>If success='true' and status='PG SUCCESS' means it is a valid payment</span>"
            Else
                preOrder.InnerHtml = "<html><br/>"
                preOrder.InnerHtml &= "<hr><br />Error: <span style='color:red;font-weight:bold'>"
                preOrder.InnerHtml &= "API Response data is empty"
                preOrder.InnerHtml &= "</span></html>"
            End If
        Catch ex As Exception
            preOrder.InnerHtml = "<html><br/>"
            preOrder.InnerHtml &= "<hr><br />Exception caught, Error: <span style='color:red;font-weight:bold'>"
            preOrder.InnerHtml &= ex.Message.ToString
            preOrder.InnerHtml &= "</span></html>"
        Finally
            h_popup.Value = "print"
        End Try
    End Sub
    Private Function ToQueryString(ByVal nvc As NameValueCollection) As String
        Dim array = (From key In nvc.AllKeys From value In nvc.GetValues(key) Select String.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value))).ToArray()
        Return "?" & String.Join("&", array)
    End Function
    Public Shared Function ValidateValue(ByVal parameter As JProperty) As String
        ValidateValue = ""
        Try
            Dim msgProperty = parameter
            If msgProperty IsNot Nothing Then
                ValidateValue = msgProperty.Value
            End If
        Catch ex As Exception

        End Try
    End Function

    Protected Sub rblEnrollment_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblEnrollment.SelectedIndexChanged
        h_FCO_print.Value = ""
        gridbind()
    End Sub
    Private Sub VerifyGateWayResponseAccess()
        ViewState("AccessGateWayResponse") = False
        Dim qry As String = "SELECT COUNT(USR_NAME) FROM [dbo].[VW_UsersAllowedForOnlinePaymentAPIData] WHERE USR_NAME = '" & Session("sUsr_name") & "'"
        Dim RowsCount As Integer = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry)
        If RowsCount > 0 Then
            ViewState("AccessGateWayResponse") = True
        End If
    End Sub
End Class
