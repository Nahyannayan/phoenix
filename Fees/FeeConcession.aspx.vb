Imports System.Data
Imports System.Data.SqlClient

Partial Class Fees_FeeConcession
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Not IsPostBack Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CONCESSION Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim tblbUSuper As Boolean = Session("sBusper")
                If tblbUSuper = True Then
                    ddlBusinessUnit.DataSource = AccessRoleUser.GetBusinessUnits()
                    ddlBusinessUnit.DataTextField = "bsu_name"
                    ddlBusinessUnit.DataValueField = "bsu_id"
                Else
                    ddlBusinessUnit.DataSource = AccessRoleUser.GetTotalBUnit(Session("sUsr_id"))
                    ddlBusinessUnit.DataTextField = "bname"
                    ddlBusinessUnit.DataValueField = "B_unit"
                End If
                ddlBusinessUnit.DataBind()
                Dim lstItm As New ListItem("None", "-1")
                ddlBusinessUnit.Items.Add(lstItm)
                ddlBusinessUnit.SelectedIndex = -1
                ddlBusinessUnit.Items.FindByText("None").Selected = True

                ddlConcessionType.DataSource = FeeConcession.PopulateConcessionType
                ddlConcessionType.DataTextField = "FCT_DESCR"
                ddlConcessionType.DataValueField = "FCT_ID"
                ddlConcessionType.DataBind()
            End If
            If ViewState("datamode") = "add" Then
                Session("sFEE_CONCESSION") = Nothing
            End If
            If ViewState("datamode") = "view" Then
                Dim FCM_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FCM_ID").Replace(" ", "+"))
                Dim vViewFEEConc As FeeConcession = FeeConcession.GetFeeConcession(FCM_ID, ConnectionManger.GetOASIS_FEESConnection)
                ddlBusinessUnit.ClearSelection()
                If vViewFEEConc.FCM_BSU_ID = "" Then
                    ddlBusinessUnit.Items.FindByText("None").Selected = True
                Else
                    ddlBusinessUnit.SelectedValue = vViewFEEConc.FCM_BSU_ID
                End If
                ddlConcessionType.SelectedValue = vViewFEEConc.FCT_ID
                txtConcDescr.Text = vViewFEEConc.FCM_DESCR
                txtDebitAccName.Text = vViewFEEConc.FCM_DEBIT_ACT_ID & "-" & vViewFEEConc.FCM_DEBIT_ACT_NAME
                h_ACC_ID.Value = vViewFEEConc.FCM_DEBIT_ACT_ID
                h_FCM_ID.Value = vViewFEEConc.FCM_ID
                Session("sFEE_CONCESSION") = vViewFEEConc
                DissableControls(True)
            End If
        End If
    End Sub

    Private Sub DissableControls(ByVal bDissable As Boolean)
        txtConcDescr.ReadOnly = bDissable
        txtDebitAccName.ReadOnly = bDissable
        ddlBusinessUnit.Enabled = Not bDissable
        ddlConcessionType.Enabled = Not bDissable
        imgAccount.Enabled = Not bDissable
    End Sub

    Private Sub VerifyAccount()
        If txtDebitAccName.Text = "" Then Return
        Dim STR_ACCCODE As String = txtDebitAccName.Text.Split("-")(0)
        Dim STR_ACCNAME As String = AccountFunctions.Validate_Account(STR_ACCCODE, Session("sbsuid"), "CONCESSION")
        If STR_ACCNAME = "" Then
            'lblError.Text = "Invalid Discount Account"
            usrMessageBar.ShowNotification("Invalid Discount Account", UserControls_usrMessageBar.WarningType.Danger)
            txtDebitAccName.Focus()
        Else
            txtDebitAccName.Text = STR_ACCCODE & "-" & STR_ACCNAME
            h_ACC_ID.Value = STR_ACCCODE
        End If
    End Sub

    Function checkErrors() As String
        If txtDebitAccName.Text = "" Then Return ""
        Dim Str_Error As String = ""
        Dim STR_ACCCODE As String = txtDebitAccName.Text.Split("-")(0)
        Dim STR_ACCNAME As String = AccountFunctions.Validate_Account(STR_ACCCODE, Session("sbsuid"), "CONCESSION")
        If STR_ACCNAME = "" Then
            Str_Error = Str_Error & "Invalid Discount Account <Br>"
        End If
        checkErrors = Str_Error
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim str_error As String = checkErrors()
        If str_error <> "" Then
            'lblError.Text = "Please check the Following : <br>" & str_error
            usrMessageBar.ShowNotification("Please check the Following : <br>" & str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim vFeeConcession As New FeeConcession
        If Not Session("sFEE_CONCESSION") Is Nothing Then
            vFeeConcession = Session("sFEE_CONCESSION")
        End If
        vFeeConcession.FCM_BSU_ID = ddlBusinessUnit.SelectedValue
        vFeeConcession.FCM_DEBIT_ACT_ID = h_ACC_ID.Value
        vFeeConcession.FCM_DESCR = txtConcDescr.Text
        vFeeConcession.FCT_ID = ddlConcessionType.SelectedValue

        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction("FEE_CONCESSION")
        Dim retVal As Integer = FeeConcession.SaveFeeConcession(vFeeConcession, trans, conn, Page.User.Identity.Name.ToString())
        If retVal <> 0 Then
            trans.Rollback()
            'lblError.Text = UtilityObj.getErrorMessage(retVal)
            usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
        Else
            trans.Commit()
            'lblError.Text = "Data updated Successfully"
            usrMessageBar.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
            ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
            'If flagAudit <> 0 Then
            '    Throw New ArgumentException("Could not process your request")
            'End If
        End If

    End Sub

    Private Sub ClearDetails()
        txtDebitAccName.Text = ""
        txtConcDescr.Text = ""
        h_ACC_ID.Value = ""
        h_FCM_ID.Value = ""
        ddlBusinessUnit.ClearSelection()
        ddlConcessionType.ClearSelection()
        Session("sFEE_CONCESSION") = Nothing
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        DissableControls(False)
        UtilityObj.beforeLoopingControls(Me.Page)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearDetails()
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        imgAccount.Enabled = True
        ddlConcessionType.Enabled = True
    End Sub

    Protected Sub txtDebitAccName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDebitAccName.TextChanged
        VerifyAccount()
    End Sub
End Class
