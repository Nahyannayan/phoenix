Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Fees_FeeCalculateFeeTransport
    Inherits BasePage
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Gridbind_Feedetails()
            txtTotal.Attributes.Add("readonly", "readonly") 
        End If
    End Sub 


    Sub Gridbind_Feedetails()
        If Request.QueryString("SBL") <> "" And Request.QueryString("stuid") <> "" Then
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(0).Value = Request.QueryString("stuid")
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = Request.QueryString("BSU")
            pParms(2) = New SqlClient.SqlParameter("@bTerm", SqlDbType.VarChar, 200)
            pParms(2).Value = False
            pParms(3) = New SqlClient.SqlParameter("@SBL_ID", SqlDbType.VarChar, 20)
            pParms(3).Value = Request.QueryString("SBL")
            pParms(4) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.VarChar, 20)
            pParms(4).Value = Request.QueryString("acd")

            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
                Dim cmd As New SqlCommand("[TRANSPORT].[GetFeePayableSplitup]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(pParms)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)
                gvFeeCollection.DataSource = ds
                gvFeeCollection.DataBind()
            End Using
        End If
        Set_GridTotal()
    End Sub


    Sub Set_GridTotal(Optional ByVal SelectAll As Integer = 0)
        Dim dAmount As Decimal = 0
        Dim str_Narration As String = ""
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
            Dim chkSelected As CheckBox = CType(gvr.FindControl("chkSelected"), CheckBox)
            If Not txtAmountToPay Is Nothing And Not chkSelected Is Nothing Then
                If SelectAll = 1 Then
                    chkSelected.Checked = False
                ElseIf SelectAll = 2 Then
                    chkSelected.Checked = True
                End If
                If chkSelected.Checked Then
                    str_Narration = str_Narration & gvr.Cells(1).Text & ", "
                    If IsNumeric(txtAmountToPay.Text) Then
                        dAmount = dAmount + CDbl(txtAmountToPay.Text)
                    Else
                        txtAmountToPay.Text = "0"
                    End If
                End If
            End If
        Next
        If str_Narration.Length > 2 Then
            str_Narration = str_Narration.Remove(str_Narration.Length - 2, 2)
        End If
        h_Narration.Value = str_Narration.Replace("'", "\'")
        txtTotal.Text = Format(dAmount, "0.00")
    End Sub


    Protected Sub chkSelected_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal()
    End Sub
 

    Protected Sub txtAmountToPay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal()
    End Sub


    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If sender.Checked Then
            Set_GridTotal(2)
        Else
            Set_GridTotal(1)
        End If

    End Sub

    Protected Sub btnFinish_Click(sender As Object, e As EventArgs)

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.NameandCode = '" & txtTotal.Text & "||" & h_Narration.Value & "';")
        Response.Write("var oWnd = GetRadWindow('" & txtTotal.Text & "||" & h_Narration.Value & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")
    End Sub
End Class
