<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeOtherFeeCollectionView.aspx.vb" Inherits="Fees_feeOtherFeeCollectionView" Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>


<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
<%--    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    --%>
    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
   function CheckForPrint() {
       if (document.getElementById('<%= h_print.ClientID %>').value != '') {
           document.getElementById('<%= h_print.ClientID %>').value = '';
           //showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
           return ShowWindowWithClose('../Reports/ASPX Report/RptViewerModal.aspx', '', '45%', '85%');
           //window.open('../Reports/ASPX Report/RptViewerModal.aspx')
       }
   }
    );
   function ShowWindowWithClose(gotourl, pageTitle, w, h) {
       $.fancybox({
           type: 'iframe',
           //maxWidth: 300,
           href: gotourl,
           //maxHeight: 600,
           fitToView: true,
           padding: 6,
           width: w,
           height: h,
           autoSize: false,
           openEffect: 'none',
           showLoading: true,
           closeClick: true,
           closeEffect: 'fade',
           'closeBtn': true,
           afterLoad: function () {
               this.title = '';//ShowTitle(pageTitle);
           },
           helpers: {
               overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
               title: { type: 'inside' }
           },
           onComplete: function () {
               $("#fancybox-wrap").css({ 'top': '90px' });
           },
           onCleanup: function () {
               var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

               if (hfPostBack == "Y")
                   window.location.reload(true);
           }
       });

       return false;
   }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
           <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table   width="100%">
                    <tr valign="top">
                        <td valign="top" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError"></asp:Label>
                        </td>
                        <td valign="top" align="right" class="matters">
                            <asp:RadioButton ID="rbAll" runat="server" AutoPostBack="True" Checked="True" CssClass="field-label"
                                GroupName="deleted" Text="All" />
                            <asp:RadioButton ID="rdDeleted" runat="server" AutoPostBack="True"  CssClass="field-label"
                                GroupName="deleted" Text="Only Deleted" />
                        </td>
                    </tr>
                </table>
                <table  width="100%">
                    <tr>
                        <td align="center" valign="top" class="matters" colspan="2">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="FOC_ID"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="25"  CssClass="table table-row table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="Docno">
                                        <HeaderTemplate>
                                            Receipt#<br /> Print<br />
                                                                    <asp:TextBox ID="txtReceiptno" runat="server"  Width="52px"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnReceiptSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="ImageButton1_Click" />
                                                       
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%---<asp:Label ID="lblReceipt" runat="server" Text='<%# Bind("FOC_RECNO") %>'></asp:Label>--%>
                                            <asp:LinkButton ID="lblReceipt" runat="server" Text='<%# Bind("FOC_RECNO") %>' OnClick="lbVoucher_Click">Receipt</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date<br />
                                                                    <asp:TextBox ID="txtDate" runat="server"  Width="52px"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFOCDT" runat="server" Text='<%# Bind("FOC_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            Mode<br />
                                                                    <asp:TextBox ID="txtGrade" runat="server"  Width="52px"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnGradeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("CLT_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student#">
                                        <HeaderTemplate>
                                            Account<br />
                                                                    <asp:TextBox ID="txtStuno" runat="server"  Width="52px"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnstunoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("ACT_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Name<br />
                                                                    <asp:TextBox ID="txtStuname" runat="server"  Width="52px"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnstunameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("COL_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount(Incl.Tax)">
                                        <HeaderTemplate>
                                            Amount(Incl.Tax)<br />
                                                                    <asp:TextBox ID="txtAmount" runat="server"  Width="52px"></asp:TextBox> 
                                                                    <asp:ImageButton ID="btnAmountSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="ImageButton1_Click" /> 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("FOC_NET_AMOUNT", "{0:0.00}")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderTemplate>
                                            Description<br />
                                                                    <asp:TextBox ID="txtDesc" runat="server"  Width="52px"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnDescSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("FOC_NARRATION") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoice No">
                                        <HeaderTemplate>
                                            Invoice#<br /> Print<br />
                                                                    <asp:TextBox ID="txtInvno" runat="server"  Width="52px"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnInvnoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%--<asp:Label ID="lblInvNo" Visible ="false"  runat="server" Text='<%# Bind("FOC_INVOICE_NO")%>'></asp:Label>--%>
                                            <asp:LinkButton ID="lblInvNo" runat="server" Text='<%# Bind("FOC_INVOICE_NO")%>'></asp:LinkButton>



                                        </ItemTemplate>
                                       <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Deposit Slip Print">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LbBatchReceipt" runat="server" OnClick="lbBatchReceipt_Click">Deposit Slip </asp:LinkButton>
                                            <asp:Label ID="lblBatchNo" runat="server" Visible="false" Text='<%# Bind("FOC_BATCH_NO")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"   />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_selected_menu_3" runat="server" />
                <asp:HiddenField ID="h_selected_menu_4" runat="server" />
                <asp:HiddenField ID="h_selected_menu_5" runat="server" />
                <asp:HiddenField ID="h_selected_menu_6" runat="server" />
                <asp:HiddenField ID="h_selected_menu_7" runat="server" />
                <asp:HiddenField ID="h_selected_menu_8" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
            </div>
        </div>
         <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>

