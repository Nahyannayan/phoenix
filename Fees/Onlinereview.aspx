<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Onlinereview.aspx.vb" Inherits="Fees_Onlinereview" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style type="text/css">
        .radioButtonList label {
            display: inline;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("ChkSelAll").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server" Text="Online Fee Reconciliation"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <input id="h_NextLine" runat="server" type="hidden" /></td>
                    </tr>

                    <tr>
                        <td align="left">
                            <asp:RadioButton ID="rbnmpgs" Checked="true" AutoPostBack="true"  OnCheckedChanged="rbnmpgs_CheckedChanged" runat="server" Text="MPGS" GroupName="grp1" />
                            <asp:RadioButton ID="rbnmigs" runat="server" AutoPostBack="true" OnCheckedChanged="rbnmigs_CheckedChanged" Text="MIGS" GroupName="grp1" />
                            <asp:RadioButton ID="rbnindus" runat="server" AutoPostBack="true" OnCheckedChanged="rbnindus_CheckedChanged" Text="INDUS" GroupName="grp1" />
                            <asp:RadioButton ID="rbnebpg" runat="server" AutoPostBack="true" OnCheckedChanged="rbnindus_CheckedChanged" Text="EBPG" GroupName="grp1" />
                            <asp:RadioButton ID="rbnqless" runat="server" AutoPostBack="true" OnCheckedChanged="rbnindus_CheckedChanged" Text="QLESS" GroupName="grp1" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">File Name</span></td>
                                    <td align="left" width="30%">
                                        <asp:FileUpload ID="uploadFile" runat="server"></asp:FileUpload></td>
                                    <td align="left" colspan="2">
                                        <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Upload File" CausesValidation="False"
                                            TabIndex="30" OnClientClick="getFile()" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" class="title-bg-lite">Reconciliation Details</td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:GridView ID="gvExcel" runat="server" Width="100%" EmptyDataText="No Accounts are Listing"
                                            AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" ShowFooter="True" OnRowDataBound="gvExcel_RowDataBound">
                                            <RowStyle CssClass="griditem"></RowStyle>
                                            <EmptyDataRowStyle Wrap="True"></EmptyDataRowStyle>
                                            <Columns>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <input id="ChkSelAll" name="ChkSelAll" onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <input id="ChkSelect" type="checkbox" runat="server" value='<%# Bind("FCO_ID") %>' />
                                                        <asp:HiddenField ID="hdnAmount" runat="server" Value='<%# Bind("FCO_AMOUNT")%>' />
                                                        <asp:HiddenField ID="hdnFCO_FCO_ID" runat="server" Value='<%# Bind("FCO_FCO_ID")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="FCO_DATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date" SortExpression="FCO_DATE"></asp:BoundField>
                                                <asp:BoundField DataField="STU_NO" HeaderText="StudentID"></asp:BoundField>
                                                <asp:BoundField DataField="NAME" HeaderText="Student Name"></asp:BoundField>
                                                <asp:BoundField DataField="STU_GRD_ID" HeaderText="Grade"></asp:BoundField>
                                                <asp:BoundField DataField="FCO_AMOUNT" HeaderText="Amount">
                                                    <ItemStyle HorizontalAlign="right" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="STS_FFIRSTNAME" HeaderText="Father Name"></asp:BoundField>
                                                <asp:BoundField DataField="STS_FMOBILE" HeaderText="Father Mobile"></asp:BoundField>
                                                <asp:BoundField DataField="STS_MFIRSTNAME" HeaderText="Mother Name"></asp:BoundField>
                                                <asp:BoundField DataField="STS_MMOBILE" HeaderText="Mother Name"></asp:BoundField>
                                                <asp:BoundField DataField="FCO_ID" HeaderText="Ref ID"></asp:BoundField>
                                            </Columns>
                                            <FooterStyle Height="20px" CssClass="griditem_alternative" />
                                            <SelectedRowStyle BackColor="Aqua"></SelectedRowStyle>
                                            <HeaderStyle CssClass="gridheader_new" Height="20px"></HeaderStyle>
                                            <AlternatingRowStyle CssClass="gridheader_new"></AlternatingRowStyle>

                                        </asp:GridView>

                                        <asp:GridView ID="gvMPGS" runat="server" Width="100%" EmptyDataText=""
                                            AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" ShowFooter="False" >
                                            <RowStyle CssClass="griditem"></RowStyle>
                                            <EmptyDataRowStyle Wrap="True"></EmptyDataRowStyle>
                                            <Columns>
                                                <asp:BoundField DataField="AuthorizationCode" HeaderText="Authorization Code">  <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle></asp:BoundField>
                                                <asp:BoundField DataField="MerchantID" HeaderText="Merchant ID">  <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle></asp:BoundField>
                                                <asp:BoundField DataField="OrderID" HeaderText="Order ID">  <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle></asp:BoundField>
                                                <asp:BoundField DataField="OrderStatus" HeaderText="Order Status">  <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CapturedAmount(amountonly)" HeaderText="Captured Amount (amount only)">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                            
                                            </Columns>
                                         
                                            <SelectedRowStyle BackColor="Aqua"></SelectedRowStyle>
                                            <HeaderStyle CssClass="gridheader_new" Height="20px"></HeaderStyle>
                                            <AlternatingRowStyle CssClass="gridheader_new"></AlternatingRowStyle>

                                        </asp:GridView>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table id="Table1" align="center" cellpadding="0" cellspacing="0" width="100%">
                                <tr id="tr_SaveButtons" runat="server">
                                    <td align="center">
                                        <asp:Button CssClass="button" ID="btnSave" runat="server" TabIndex="26" Text="Save" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="HidUpload" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

