﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="DebtfollowupPayplanUpload.aspx.vb" Inherits="Fees_DebtfollowupPayplanUpload" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script lang="javascript" type="text/javascript">


        function CommitButton() {

            var intervalHandle = setInterval(function () {
                if ($("#<%=h_Processeing.ClientID %>").val() == '1') {

                    var dt = $('#ctl00_cphMasterpage_hdBATCHNO').val() + "|" + '<%= hdrblAdjFor.Value%>';
                    var $ajaxImage = $("#ajaxImage");
                    var $progressbar = $("#progressbar");
                    var $fsProgress = $("#fsProgress");
                    var $statusDiv = $("#statusDiv");
                    var $startProcessButton = $("#<%= btnCommitImprt.ClientID%>");
                    var $PostbackButton = $("#<%= btnPostback.ClientID%>");
                    var $lblError = $("#<%= lblError.ClientID%>");
                    var $h_status = $("#<%= h_STATUS.ClientID%>");
                    var $h_errormessage = $("#<%= h_errormessage.ClientID%>");
                    var $h_IsSuccess = $("#<%= h_IsSuccess.ClientID%>");
                    var postData = {
                        BATCHNO: $('#ctl00_cphMasterpage_hdBATCHNO').val(),
                        SOURCE: 'DFUP',
                        PAGE: 'DFUP'
                    }
                    var xhr = $.ajax({
                        url: "../GetBulkProcessProgress.asmx/GetStatus",
                        data: JSON.stringify(postData),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",

                        success: function (response, status, xhr) {

                            if (response.d.ISERROR) {
                                $h_status.val("")
                                $startProcessButton.removeAttr("disabled");
                                $ajaxImage.hide(500);
                                $progressbar.hide(500);
                                $fsProgress.hide(500);
                                clearInterval(intervalHandle);
                                $startProcessButton.removeAttr("disabled");
                                $h_errormessage.val(response.d.MESSAGE);
                                $("#ctl00_cphMasterpage_h_Processeing").val('');
                                $h_IsSuccess.val("0");
                                $statusDiv.hide();
                                $('#error-message').html(response.d.MESSAGE);
                                $('#alert-error-popup').show();
                                xhr.abort();

                            }
                            else {

                                $startProcessButton.attr("disabled", "disabled");
                                if ($fsProgress.is(':visible') == false)
                                    $fsProgress.show();
                                if ($ajaxImage.is(':visible') == false)
                                    $ajaxImage.show();
                                if ($progressbar.is(':visible') == false)
                                    $progressbar.show();

                                $statusDiv.html(response.d.PERCENTAGE + "% " + response.d.WORKDESCRIPTION);
                                $progressbar.progressbar({
                                    value: parseInt(response.d.PERCENTAGE)
                                });


                                if (response.d.ISCOMPLETE) {
                                    clearInterval(intervalHandle);
                                    $startProcessButton.removeAttr("disabled");
                                    $h_errormessage.val(response.d.MESSAGE);
                                    $h_IsSuccess.val("1");
                                    $("#ctl00_cphMasterpage_h_Processeing").val('');
                                    $h_status.val("0")
                                    xhr.abort();
                                    $ajaxImage.hide(500);
                                    $progressbar.hide(500);
                                    $fsProgress.hide(500);
                                    $PostbackButton.click();
                                }
                                else
                                    $h_status.val("");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $h_status.val("")
                            $startProcessButton.removeAttr("disabled");
                            $ajaxImage.hide(500);
                            $progressbar.hide(500);
                            $fsProgress.hide(500);
                            clearInterval(intervalHandle);
                            $startProcessButton.removeAttr("disabled");
                            $("#ctl00_cphMasterpage_h_Processeing").val('');
                            xhr.abort();
                        },
                    });
                }
            }, 2000);
        }

        // A $(document).ready() block.
        $(document).ready(function () {
            var $ajaxImage = $("#ajaxImage");
            var $progressbar = $("#progressbar");
            var $fsProgress = $("#fsProgress");
            // Hide the AJAX spinner image and the progress bar, by default.
            $ajaxImage.hide(500);
            $progressbar.hide(500);
            $fsProgress.hide(500);
        });
        function ConfirmFormatFileDownload()
        {
            alert('Note: Please follow the same data formatting as in the downloaded format file.');
            return true;
        }

    </script>
    <style type="text/css">
        #alert-error-popup {
            position: fixed;
            top: 100px;
            left: 45%;
            z-index: 1000;
            min-width: 200px;
        }

        .field-rb-label > label {
            font-weight: bold !important;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <div id="alert-error-popup" style="display: none" class="sticky-top alert alert-danger alert-dismissable" role="alert"><i class="fa fa-info"></i><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><span id="error-message">  </span></div>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Debt followup payplan upload
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False" CssClass="error"></asp:Label>
                            <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
                        </td>
                    </tr>
                </table>
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td><span class="field-label">Select File</span></td>
                            <td>
                                <asp:FileUpload ID="FileUpload1" runat="server" Width="255px"></asp:FileUpload>&nbsp;<asp:Button ID="btnImport" runat="server" Text="Load" CssClass="button"></asp:Button><br />
                                <asp:HyperLink
                                    ID="lnkXcelFormat" runat="server">Click here to get the formatted Excel file</asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <fieldset id="fsProgress" style="width: 50%; display: none">
                                    <legend>Processing Status</legend>
                                    <div id="ajaxImage" style="display: inline;">
                                        <img alt="" src="../Images/Misc/AjaxLoading.gif" />
                                    </div>
                                    <div id="statusDiv"></div>
                                    <div id="progressbar" style="height: 20px; width: 100%"></div>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table border="0" style="width: 100%;">
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="button" Visible="False"></asp:Button></td>
                                        <td align="center">
                                            <asp:Button
                                                ID="btnProceedImpt" runat="server" Text="Proceed" CssClass="button" Visible="False"></asp:Button>
                                            <asp:Button ID="btnCommitImprt" runat="server" Text="Commit" CssClass="button"
                                                OnClientClick="CommitButton()" Visible="False"></asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trGvImport" runat="server">
                            <td id="Td1" runat="server" colspan="2">
                                <asp:GridView ID="gvExcelImport" runat="server" OnPageIndexChanging="gvExcelImport_PageIndexChanging"
                                    AllowPaging="True" EmptyDataText="No Data" Width="100%" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                    ShowFooter="True" DataKeyNames="bVALID">
                                    <Columns>
                                        <asp:BoundField DataField="SLNO" HeaderText="Serial Number">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ERROR_MSG" HeaderText="Message" HtmlEncode="False">
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="STUDENT_ID" HeaderText="Student Id">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="COMMENTS" HeaderText="Comments">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PAY_PLAN_TYPE" HeaderText="Plan Type">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DATE1" HeaderText="Date 1" DataFormatString = "{0:dd/MMM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AMOUNT1" HeaderText="Amount 1" DataFormatString="{0:n2}">
                                            <FooterStyle HorizontalAlign="Right" BorderStyle="None" Height="25px"></FooterStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DATE2" HeaderText="Date 2" DataFormatString = "{0:dd/MMM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AMOUNT2" HeaderText="Amount 2" DataFormatString="{0:n2}">
                                            <FooterStyle HorizontalAlign="Right" BorderStyle="None" Height="25px"></FooterStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DATE3" HeaderText="Date 3" DataFormatString = "{0:dd/MMM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AMOUNT3" HeaderText="Amount 3" DataFormatString="{0:n2}">
                                            <FooterStyle HorizontalAlign="Right" BorderStyle="None" Height="25px"></FooterStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DATE4" HeaderText="Date 4" DataFormatString = "{0:dd/MMM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AMOUNT4" HeaderText="Amount 4" DataFormatString="{0:n2}">
                                            <FooterStyle HorizontalAlign="Right" BorderStyle="None" Height="25px"></FooterStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DATE5" HeaderText="Date 5" DataFormatString = "{0:dd/MMM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AMOUNT5" HeaderText="Amount 5" DataFormatString="{0:n2}">
                                            <FooterStyle HorizontalAlign="Right" BorderStyle="None" Height="25px"></FooterStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DATE6" HeaderText="Date 6" DataFormatString = "{0:dd/MMM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AMOUNT6" HeaderText="Amount 6" DataFormatString="{0:n2}">
                                            <FooterStyle HorizontalAlign="Right" BorderStyle="None" Height="25px"></FooterStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DATE7" HeaderText="Date 7" DataFormatString = "{0:dd/MMM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AMOUNT7" HeaderText="Amount 7" DataFormatString="{0:n2}">
                                            <FooterStyle HorizontalAlign="Right" BorderStyle="None" Height="25px"></FooterStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DATE8" HeaderText="Date 8" DataFormatString = "{0:dd/MMM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AMOUNT8" HeaderText="Amount 8" DataFormatString="{0:n2}">
                                            <FooterStyle HorizontalAlign="Right" BorderStyle="None" Height="25px"></FooterStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DATE9" HeaderText="Date 9" DataFormatString = "{0:dd/MMM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AMOUNT9" HeaderText="Amount 9" DataFormatString="{0:n2}">
                                            <FooterStyle HorizontalAlign="Right" BorderStyle="None" Height="25px"></FooterStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DATE10" HeaderText="Date 10" DataFormatString = "{0:dd/MMM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AMOUNT10" HeaderText="Amount 10" DataFormatString="{0:n2}">
                                            <FooterStyle HorizontalAlign="Right" BorderStyle="None" Height="25px"></FooterStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="OUTSTANDING" HeaderText="Outstanding" DataFormatString="{0:n2}">
                                            <FooterStyle HorizontalAlign="Right" BorderStyle="None" Height="25px"></FooterStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="USER" HeaderText="User">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                    </Columns>
                                    <FooterStyle BackColor="#99CCFF" Font-Size="Small"></FooterStyle>
                                </asp:GridView>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <asp:HiddenField ID="hdrblAdjFor" runat="server" />
                <asp:HiddenField ID="hdBATCHNO" runat="server" />
                <asp:HiddenField ID="h_IsSuccess" runat="server" />
                <asp:HiddenField ID="h_STATUS" runat="server" />
                <asp:HiddenField ID="h_errormessage" runat="server" />
                <asp:HiddenField ID="h_Processeing" runat="server" />
                <asp:Button ID="btnPostback" runat="server" CssClass="button" Style="display: none;" CausesValidation="False" Height="1px" TabIndex="5000" Width="1px" OnClick="btnPostback_Click" />
            </div>
        </div>
    </div>
</asp:Content>

