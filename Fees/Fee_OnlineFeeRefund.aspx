﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Fee_OnlineFeeRefund.aspx.vb" Inherits="Fees_Fee_OnlineFeeRefund" %>

<%@ Register Src="~/UserControls/uscStudentPicker.ascx" TagPrefix="uc1" TagName="uscStudentPicker2" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            position: fixed;
            width: 70%;
        }
    </style>
    <script type="text/javascript" language="javascript">

        function get_Bank() {

            var NameandCode;
            var result;

            result = radopen("../accounts/PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value, "pop_up5");

            <%--if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
            document.getElementById('<%= txtChqBook.ClientId %>').value = '';
            document.getElementById('<%= hCheqBook.ClientId %>').value = '';
            return false;--%>
        }

        function OnClientClose5(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
               <%-- document.getElementById('<%= txtChqBook.ClientId %>').value = '';
                document.getElementById('<%= hCheqBook.ClientId %>').value = '';--%>
                __doPostBack('<%= txtAdjustment.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                Width="1000px" Height="620px" OnClientAutoSizeEnd="autoSizeWithCalendar">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up5" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose5"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Online Fee Refund
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tblAddLedger" runat="server" width="100%">
                    <tr valign="bottom">
                        <td align="left" colspan="2">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>
                <table runat="server" style="width: 100%">
                    <tr>
                        <td align="left"><span class="field-label">Select Student</span></td>
                        <td colspan="3">
                            <uc1:uscStudentPicker2 runat="server" ID="uscStudentPicker" />
                        </td>
                    </tr>

                </table>

                <table runat="server" style="width: 100%">
                    <tr id="tr_list" runat="server" visible="false">
                        <td align="left"><span class="field-label">Fee Receipt List</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:GridView ID="grdFee" runat="server" AutoGenerateColumns="False" PageSize="5" Width="100%" AllowPaging="true" EmptyDataText="No receipts for refund"
                                CaptionAlign="Top" class="table table-bordered table-row" DataKeyNames="SNO" OnPageIndexChanging="grdFee_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField Visible="True" HeaderText="S.No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_SNO" runat="server" Text='<%# Bind("SNO")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="5%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="Receipt No.">
                                        <HeaderTemplate>
                                            Receipt No.<br />
                                            <asp:TextBox ID="txtReceiptNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnReceiptSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../../Images/forum_search.gif"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ReceiptNo" runat="server" Text='<%# Bind("[Receipt No]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="20%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Date" runat="server" Text='<%# Bind("Date")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="15%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="FCL_FCL_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FCL_FCL_ID" runat="server" Text='<%# Bind("FCL_FCL_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="15%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Amount" runat="server" Text='<%# Bind("Amount")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="15%" HorizontalAlign="Right" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="True" HeaderText="Refunded Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Refunded_Amount" runat="server" Text='<%# Bind("[Refunded Amount]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="15%" HorizontalAlign="Right" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="True" HeaderText="Refundable Amount" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Refundable_Amount" runat="server" Text='<%# Bind("[Refundable Amount]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="15%" HorizontalAlign="Right" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="False" HeaderText="Student Type" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_StudentType" runat="server" Text='<%# Bind("[Student Type]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>

                                    <%--<asp:TemplateField HeaderText="Select">
                                        <HeaderTemplate>
                                            Select
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <asp:LinkButton ID="hlview" runat="server">SelectReceipt</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>--%>
                                    <asp:ButtonField CommandName="Select" HeaderText="Select" Text="Select">
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle CssClass="txt-bold1" Width="15%" />
                                    </asp:ButtonField>

                                </Columns>
                                <%--<PagerStyle CssClass="pagination" />
                                <FooterStyle CssClass="pagination" />--%>
                            </asp:GridView>
                        </td>
                    </tr>

                </table>

                <table runat="server" style="width: 100%" id="tbl_online" visible="false">
                    <tr id="tr_onlinehdr" runat="server" visible="true">
                        <td align="left"><span class="field-label">Online Transaction Details</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:GridView ID="grdOnline" runat="server" AutoGenerateColumns="False" PageSize="5" Width="100%" ShowFooter="false" AllowPaging="false"
                                CaptionAlign="Top" class="table table-bordered table-row" OnPageIndexChanging="grdOnline_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField Visible="True" HeaderText="School Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_SchoolName" runat="server" Text='<%# Bind("[School Name]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="20%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="Provider">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Provider" runat="server" Text='<%# Bind("[Provider]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="5%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="Provider Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ProviderType" runat="server" Text='<%# Bind("[Provider Type]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="10%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="Card Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CardType" runat="server" Text='<%# Bind("[Card Type]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="5%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="True" HeaderText="Transaction Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Transaction_Date" runat="server" Text='<%# Bind("[Transaction Date]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="10%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="True" HeaderText="Amount Paid" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Captured_Amount" runat="server" Text='<%# Bind("[Captured Amount]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="10%" HorizontalAlign="Right" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="False" HeaderText="Batch No" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_BatchNo" runat="server" Text='<%# Bind("[Batch No]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="10%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="Transaction Reference" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_TransactionReference" runat="server" Text='<%# Bind("[Transaction Reference]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="10%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="AUTH Code" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_AuthorizeCode" runat="server" Text='<%# Bind("[Authorize Code]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="10%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="Receipt Nos" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ReceiptNos" runat="server" Text='<%# Bind("[Receipt Nos]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="10%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="Payment Initiated From" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_PaymentInitiatedFrom" runat="server" Text='<%# Bind("[Payment Initiated From]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="10%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>

                                </Columns>
                                <PagerStyle CssClass="pagination" />
                                <FooterStyle CssClass="pagination" />
                            </asp:GridView>
                        </td>
                    </tr>

                </table>



                <table runat="server" style="width: 100%" id="tbl_refund" visible="false">
                    <tr id="tr_refundhdr" runat="server" visible="true">
                        <td align="left" colspan="4"><span class="field-label">Refund Details</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:GridView ID="grdRefund" runat="server" AutoGenerateColumns="False" PageSize="5" Width="100%" ShowFooter="false" AllowPaging="false" EmptyDataText="No Refund Details"
                                CaptionAlign="Top" class="table table-bordered table-row" OnPageIndexChanging="grdRefund_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField Visible="True" HeaderText="Receipt No">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ReceiptNo" runat="server" Text='<%# Bind("[Receipt No]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="10%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="Student Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_STUDENT_NAME" runat="server" Text='<%# Bind("[Student Name]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="fixed_row_header" />
                                        <ItemStyle CssClass="txt-bold1" Width="15%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_STU_GRADE" runat="server" Text='<%# Bind("[Grade/Section]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="Student ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_STU_ID" runat="server" Text='<%# Bind("[STU_ID]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="False" HeaderText="Fee ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FEE_ID" runat="server" Text='<%# Bind("[FEE_ID]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="Fee Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FEE_DESCR" runat="server" Text='<%# Bind("[FEE_DESCR]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="20%" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="Adv Inv Balance">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_AdvanceInvoiceBalance" runat="server" Text='<%# Bind("[Advance Invoice Balance]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="10%" HorizontalAlign="Right" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="True" HeaderText="Ledger Balance">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_NetBalance" runat="server" Text='<%# Bind("[Net Balance]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="10%" HorizontalAlign="Right" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="True" HeaderText="Collection Amount" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CollectionAmount" runat="server" Text='<%# Bind("[Collection Amount]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="8%" HorizontalAlign="Right" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="True" HeaderText="Refunded Amt" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_RefundedAmount" runat="server" Text='<%# Bind("[Refunded Amount]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="6%" HorizontalAlign="Right" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" HeaderText="Refundable Amt" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_RefundableAmount" runat="server" Text='<%# Bind("[Refundable Amount]")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="6%" HorizontalAlign="Right" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="True" HeaderText="Refund Amount" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:TextBox ID="lbl_RefundAmount" runat="server" Text='<%# Bind("[Refund Amount]")%>' AutoPostBack="true" OnTextChanged="lbl_RefundAmount_TextChanged"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmount" runat="server" FilterType="Numbers, Custom"
                                                ValidChars="." TargetControlID="lbl_RefundAmount" />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="txt-bold1" Width="15%" HorizontalAlign="Right" />
                                        <HeaderStyle CssClass="fixed_row_header" />
                                    </asp:TemplateField>

                                </Columns>
                                <PagerStyle CssClass="pagination" />
                                <FooterStyle CssClass="pagination" />
                            </asp:GridView>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select Bank </span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtBankCode" runat="server" AutoPostBack="True" Visible="true" Enabled="false"></asp:TextBox>
                            <asp:ImageButton ID="imgBank" runat="server" Enabled="false" ImageUrl="~/Images/cal.gif" OnClientClick="get_Bank(); return false;" />
                        </td>
                        <td align="left" width="20%"><span class="field-label">Bank Name </span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtBankDescr" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Bank charge </span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtBankCharge" runat="server" Text="0.00" Enabled="false" AutoPostBack="true" OnTextChanged="txtBankCharge_TextChanged"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtBankCharge" runat="server" FilterType="Numbers, Custom"
                                ValidChars="." TargetControlID="txtBankCharge" />
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="txtFrom" runat="server" class="field-value"></asp:Label>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Total Refund Amount</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="txt_Total_Refund_Amt" runat="server" class="field-value" Text="0.00"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="left"><span class="field-label" width="20%">Narration</span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtAdjustment" runat="server" Visible="false"></asp:TextBox>
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"
                                MaxLength="300"></asp:TextBox>
                        </td>
                    </tr>

                    <tr runat="server" visible="true">
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <%--<asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button" Visible="false" Text="Print Receipt" />--%>
                            <%--  <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />--%>
                        </td>
                    </tr>
                </table>


                <asp:Panel ID="divAge" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">
                        <div>
                            <asp:Button ID="btClose" type="button" runat="server"
                                Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                                ForeColor="White" Text="X"></asp:Button>
                            <div>
                                <div align="CENTER">
                                    <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                </div>

                                <table align="center" width="100%" cellpadding="2" cellspacing="0" width="100%">

                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnUpdate" Text="Update" CssClass="button" runat="server" />
                                            <asp:Button ID="btnUClose" Text="Close" CssClass="button" runat="server" />
                                        </td>
                                    </tr>

                                </table>
                            </div>

                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
            </div>
        </div>
    </div>
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
</asp:Content>
