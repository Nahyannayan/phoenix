Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class FEEConcessionTransView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            gvFEEConcessionDet.Attributes.Add("bordercolor", "#1b80b6")
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CONCESSION_TRANS _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CONCESSION_APPROVAL _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CONCESSION_CANCEL _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CONCESSION_CANCEL_APPROVAL) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_CONCESSION_TRANS

                        lblHead.Text = "Fee Concessions Transaction"
                        hlAddNew.NavigateUrl = "FeeConcessionTrans.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                        rbAll.Visible = True
                        rbPosted.Visible = True
                        rbUnposted.Visible = True
                        rbtRejected.Visible = True
                    Case OASISConstants.MNU_FEE_CONCESSION_APPROVAL
                        lblHead.Text = "Fee Concessions Approval"
                        gvFEEConcessionDet.Columns(0).Visible = True
                        btnPost.Visible = True
                        btnReject.Visible = True
                        chkPrint.Visible = True
                        hlAddNew.Visible = False
                        lblDate.Visible = True
                        txtFrom.Visible = True
                        imgFrom.Visible = True
                        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
                        rbAll.Visible = False
                        rbPosted.Visible = False
                        rbUnposted.Visible = False
                        rbtRejected.Visible = False
                    Case OASISConstants.MNU_FEE_CONCESSION_CANCEL
                        hlAddNew.NavigateUrl = "FeeConcessionCancellation.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                        lblHead.Text = "Fee Concessions Cancellation"
                        rbAll.Visible = True
                        rbPosted.Visible = True
                        rbUnposted.Visible = True
                        rbtRejected.Visible = True
                    Case OASISConstants.MNU_FEE_CONCESSION_CANCEL_APPROVAL
                        lblHead.Text = "Fee Concessions Cancellation Approval"
                        gvFEEConcessionDet.Columns(0).Visible = True
                        btnPost.Visible = True
                        btnReject.Visible = True
                        chkPrint.Visible = True
                        hlAddNew.Visible = False
                        lblDate.Visible = True
                        txtFrom.Visible = True
                        imgFrom.Visible = True
                        btnPost.Text = "Approve"
                        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
                        rbAll.Visible = False
                        rbPosted.Visible = False
                        rbUnposted.Visible = False
                        rbtRejected.Visible = False
                End Select
                ddlAcademicYear.Items.Clear()
                ddlAcademicYear.DataSource = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
                ddlAcademicYear.DataTextField = "ACY_DESCR"
                ddlAcademicYear.DataValueField = "ACD_ID"
                ddlAcademicYear.DataBind()
                ddlAcademicYear.SelectedIndex = -1
                ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEConcessionDet.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvFEEConcessionDet.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function  

  
    Sub GridBind()
        Try 
            Dim ds As New DataSet
            Dim str_Filter As String = String.Empty
            Dim lstrOpr As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim lstrCondn6 As String = String.Empty
            Dim lstrCondn7 As String = String.Empty
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            If gvFEEConcessionDet.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtStuNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuNo")
                lstrCondn1 = Trim(txtSearch.Text.Trim)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn1)
                '   -- 1  txtStuname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuname")
                lstrCondn2 = txtSearch.Text.Trim
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn2)
                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtConcession")
                lstrCondn3 = txtSearch.Text.Trim
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCM_DESCR", lstrCondn3)
                '   -- 3   txtTDate
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtFrom")
                lstrCondn4 = txtSearch.Text.Trim
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCH_DT", lstrCondn4)
                '   -- 5  city
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtPeriod")
                lstrCondn5 = txtSearch.Text.Trim
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "PERIOD", lstrCondn5)
                '   -- 5  city
                larrSearchOpr = h_Selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRemarks")
                lstrCondn6 = txtSearch.Text.Trim
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCH_REMARKS", lstrCondn6)
                '   --- Reference Details
                larrSearchOpr = h_Selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRefHEAD")
                lstrCondn7 = txtSearch.Text.Trim
                If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCH_REF_NAME", lstrCondn7)
            End If
            gvFEEConcessionDet.Columns(11).Visible = False 'cancelled doc number
            gvFEEConcessionDet.Columns(14).Visible = False 'status column
            If rbPosted.Checked Then
                str_Filter = str_Filter & " AND  ISNULL(FCH_bPosted,0)=1"
                If OASISConstants.MNU_FEE_CONCESSION_TRANS = ViewState("MainMnu_code").ToString Or ViewState("MainMnu_code").ToString = OASISConstants.MNU_FEE_CONCESSION_CANCEL Then
                    gvFEEConcessionDet.Columns(11).Visible = True
                End If
            ElseIf rbUnposted.Checked Then
                str_Filter = str_Filter & " AND  ISNULL(FCH_bPosted,0)=0 AND ISNULL(FCH_REJTDON,'01/jan/1900')='01/jan/1900' AND ISNULL(FCH_REJTDBY,'')=''"
            ElseIf rbtRejected.Checked Then
                str_Filter = str_Filter & " AND  ISNULL(FCH_REJTDON,'01/jan/1900')<>'01/jan/1900' AND ISNULL(FCH_REJTDBY,'')<>''"
            ElseIf rbAll.Checked Then
                gvFEEConcessionDet.Columns(11).Visible = True
                gvFEEConcessionDet.Columns(14).Visible = True
            End If
            str_Filter = str_Filter & " ORDER BY FCH_DT DESC"
            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.MNU_FEE_CONCESSION_TRANS
                    str_Sql = "SELECT * FROM FEES.vw_OSO_FEES_GETFEECONCESSIONTRANS" & _
                              " WHERE FCH_DRCR='CR' AND FCH_BSU_ID='" & Session("sBsuid") & "' AND FCH_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' " & str_Filter
                    gvFEEConcessionDet.Columns(11).HeaderText = "Cancelled"
                Case OASISConstants.MNU_FEE_CONCESSION_APPROVAL
                    str_Sql = "SELECT * FROM FEES.vw_OSO_FEES_GETFEECONCESSIONTRANS" & _
                              " WHERE FCH_DRCR='CR' AND ISNULL(FCH_bPosted,0)=0 AND FCH_BSU_ID='" & Session("sBsuid") & "'" _
                              & " AND FCH_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' " & str_Filter
                Case OASISConstants.MNU_FEE_CONCESSION_CANCEL
                    str_Sql = "SELECT * FROM FEES.vw_OSO_FEES_GETFEECONCESSIONTRANS" _
                              & " WHERE FCH_DRCR='DR' AND   ISNULL(FCH_FCH_ID, 0) <>0 " _
                              & " AND FCH_BSU_ID='" & Session("sBsuid") & "' AND FCH_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' " & str_Filter
                    gvFEEConcessionDet.Columns(11).HeaderText = "Conc.DocNo"
                Case OASISConstants.MNU_FEE_CONCESSION_CANCEL_APPROVAL
                    str_Sql = "SELECT * FROM FEES.vw_OSO_FEES_GETFEECONCESSIONTRANS" & _
                              " WHERE FCH_DRCR='DR' AND  ISNULL(FCH_bPosted,0)=0 AND FCH_BSU_ID='" & Session("sBsuid") & "'" _
                              & " AND FCH_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "' " & str_Filter
            End Select
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            gvFEEConcessionDet.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEConcessionDet.DataBind()
                Dim columnCount As Integer = gvFEEConcessionDet.Rows(0).Cells.Count
                gvFEEConcessionDet.Rows(0).Cells.Clear()
                gvFEEConcessionDet.Rows(0).Cells.Add(New TableCell)
                gvFEEConcessionDet.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEConcessionDet.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEConcessionDet.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEConcessionDet.DataBind()
            End If
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuNo")
            txtSearch.Text = lstrCondn1
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn2
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtConcession")
            txtSearch.Text = lstrCondn3
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtFrom")
            txtSearch.Text = lstrCondn4
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtPeriod")
            txtSearch.Text = lstrCondn5
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn6
            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRefHEAD")
            txtSearch.Text = lstrCondn7
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEConcessionDet.PageIndexChanging
        gvFEEConcessionDet.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFEEConcessionDet.RowDataBound
        Try
            Dim lblFCH_ID As New Label
            lblFCH_ID = TryCast(e.Row.FindControl("lblFCH_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblFCH_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_CONCESSION_TRANS, OASISConstants.MNU_FEE_CONCESSION_APPROVAL
                        hlEdit.NavigateUrl = "FeeConcessionTrans.aspx?FCH_ID=" & Encr_decrData.Encrypt(lblFCH_ID.Text) & _
                                       "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    Case OASISConstants.MNU_FEE_CONCESSION_CANCEL_APPROVAL
                        hlEdit.NavigateUrl = "FeeConcessionCancellation.aspx?FCH_ID=" & Encr_decrData.Encrypt(lblFCH_ID.Text) & _
                                  "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    Case OASISConstants.MNU_FEE_CONCESSION_CANCEL, OASISConstants.MNU_FEE_CONCESSION_CANCEL_APPROVAL
                        hlEdit.NavigateUrl = "FeeConcessionCancellation.aspx?FCH_ID=" & Encr_decrData.Encrypt(lblFCH_ID.Text) & _
                                   "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                End Select
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Dim str_ids As String = String.Empty
        Dim str_First_FCH_ID As String = String.Empty
        If Not IsDate(txtFrom.Text) Then
            'lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Try
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retval As Integer
                For Each gvr As GridViewRow In gvFEEConcessionDet.Rows
                    Dim lblFCH_ID As Label = CType(gvr.FindControl("lblFCH_ID"), Label)
                    Dim chkPost As CheckBox = CType(gvr.FindControl("chkPost"), CheckBox)
                    If Not lblFCH_ID Is Nothing Then
                        If IsNumeric(lblFCH_ID.Text) And chkPost.Checked Then
                            str_ids = str_ids & "," & lblFCH_ID.Text
                            If str_First_FCH_ID = "" Then
                                str_First_FCH_ID = lblFCH_ID.Text
                            End If
                            retval = CType(FeeDayendProcess.F_POSTCONCESSION(CType(Session("sBsuid"), String), txtFrom.Text, lblFCH_ID.Text, stTrans, CType(Session("sUsr_name"), String)), Integer)
                            If retval <> 0 Then
                                Exit For
                            End If
                        End If
                    End If
                Next
                If (retval = 0) And str_ids = "" Then
                    stTrans.Rollback()
                    'lblError.Text = "Please Select Atleast One Concession!!!"
                    usrMessageBar.ShowNotification("Please Select Atleast One Concession!!!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
                If (retval = 0) Then
                    stTrans.Commit()
                    'lblError.Text = getErrorMessage(0)
                    usrMessageBar.ShowNotification(getErrorMessage(0), UserControls_usrMessageBar.WarningType.Success)
                    GridBind()
                    If chkPrint.Checked And str_First_FCH_ID <> "" Then
                        PrintConcession(str_First_FCH_ID)
                    End If
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_ids, "Posting", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                Else
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(retval)
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Catch ex As Exception
                'lblError.Text = ex.Message  ' getErrorMessage("1000")
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub PrintConcession(ByVal First_FCH_ID As String)
        If ViewState("MainMnu_code") = OASISConstants.MNU_FEE_CONCESSION_TRANS _
                Or ViewState("MainMnu_code") = OASISConstants.MNU_FEE_CONCESSION_APPROVAL Then
            Session("ReportSource") = FEEConcessionTransaction.PrintConcession(First_FCH_ID, Session("sBsuid"), Session("sUsr_name"))
        ElseIf ViewState("MainMnu_code") = OASISConstants.MNU_FEE_CONCESSION_CANCEL _
                Or ViewState("MainMnu_code") = OASISConstants.MNU_FEE_CONCESSION_CANCEL_APPROVAL Then
            Session("ReportSource") = FEEConcessionTransaction.PrintConcessionCancel(First_FCH_ID, Session("sBsuid"), Session("sUsr_name"))
        End If
        h_print.Value = "print"
    End Sub
     
    Protected Sub rbUnposted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub rbPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_ids As String = String.Empty
        Dim str_First_FCH_ID As String = String.Empty
        If Not IsDate(txtFrom.Text) Then
            'lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Try
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retval As Integer
                Dim Qry As String = ""
                For Each gvr As GridViewRow In gvFEEConcessionDet.Rows
                    Dim lblFCH_ID As Label = CType(gvr.FindControl("lblFCH_ID"), Label)
                    Dim chkPost As CheckBox = CType(gvr.FindControl("chkPost"), CheckBox)
                    If Not lblFCH_ID Is Nothing Then
                        If IsNumeric(lblFCH_ID.Text) And chkPost.Checked Then
                            str_ids = str_ids & "," & lblFCH_ID.Text
                            If str_First_FCH_ID = "" Then
                                str_First_FCH_ID = lblFCH_ID.Text
                            End If
                            'Qry = "UPDATE FEES.FEE_CONCESSION_H SET FCH_REJTDON=GETDATE(),FCH_REJTDBY = " & CType(Session("sUsr_name"), String) & " " & _
                            '      "WHERE FCH_ID = " & lblFCH_ID.Text & " AND ISNULL(FCH_bdeleted, 0) = 0 AND ISNULL(FCH_bPOSTED,0)<>1 "
                            'retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, Qry)
                            retval = CType(FeeDayendProcess.REJECT_CONCESSION(lblFCH_ID.Text, stTrans, CType(Session("sUsr_name"), String)), Integer)
                            If retval <> 0 Then
                                Exit For
                            End If
                        End If
                    End If
                Next
                If (retval = 0) And str_ids = "" Then
                    stTrans.Rollback()
                    ' lblError.Text = "Please Select Atleast One Concession!!!"
                    usrMessageBar.ShowNotification("Please Select Atleast One Concession!!!", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
                If (retval = 0) Then
                    stTrans.Commit()
                    'lblError.Text = getErrorMessage(0)
                    usrMessageBar.ShowNotification(getErrorMessage(0), UserControls_usrMessageBar.WarningType.Success)
                    GridBind()
                    If chkPrint.Checked And str_First_FCH_ID <> "" Then
                        PrintConcession(str_First_FCH_ID)
                    End If
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_ids, "Posting", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                Else
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(retval)
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Catch ex As Exception
                'lblError.Text = ex.Message  ' getErrorMessage("1000")
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub rbtRejected_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub hlPrint_Click(sender As Object, e As EventArgs)
        Dim lblFCH_ID As Label = sender.Parent.findcontrol("lblFCH_ID")
        PrintConcession(lblFCH_ID.Text)
    End Sub
End Class
