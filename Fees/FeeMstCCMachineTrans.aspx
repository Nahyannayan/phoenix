<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeMstCCMachineTrans.aspx.vb" Inherits="fees_FeeTransportReminderTemplate" title="Untitled Page" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function getBank()
    { 
        var sFeatures;
        sFeatures="dialogWidth: 820px; ";
        sFeatures+="dialogHeight: 450px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var NameandCode;
        var result;
        //result = window.showModalDialog("../Accounts/accjvshowaccount.aspx?bankcash=b","", sFeatures)
//     popUp('460','400','BANK','<%=txtBankCode.ClientId %>','<%=txtBankDescr.ClientId %>')
        <%--if (result=='' || result==undefined)
            {    return false;      } 
        lstrVal=result.split('___');     
        document.getElementById('<%=txtBankCode.ClientId %>').value=lstrVal[0];
        document.getElementById('<%=txtBankDescr.ClientId %>').value=lstrVal[1];
    }--%>

            var url = "../Accounts/accjvshowaccount.aspx?bankcash=b";
            var oWnd = radopen(url, "pop_getbank");
                        
        } 

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBankCode.ClientId %>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientId %>').value = NameandCode[1]
                __doPostBack('<%= txtBankDescr.ClientID%>', 'TextChanged');
            }
        }


    function GetFEECounter()
       {     
            
            var NameandCode;
            
            var type= 'USERS';
        <%--result = window.showModalDialog("../Common/PopupForm.aspx?multiSelect=false&ID=" + type ,"", sFeatures)
            if(result != "" && result != "undefined")
            {
               NameandCode = result.split('___');
               document.getElementById('<%=hfFEECounter.ClientID %>').value=NameandCode[0]; 
               document.getElementById('<%=txtUserName.ClientID %>').value=NameandCode[1]; 
            }
            return false;
        }--%>
        var url = "../Common/PopupForm.aspx?multiSelect=false&ID=" + type;
            var oWnd = radopen(url, "pop_getfeecounter");
                        
        } 

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=hfFEECounter.ClientID %>').value=NameandCode[0]; 
               document.getElementById('<%=txtUserName.ClientID %>').value=NameandCode[1];
                __doPostBack('<%=txtUserName.ClientID%>', 'TextChanged');
            }
        }

    



function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getfeecounter" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getbank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Create Credit Card Machine
        </div>
        <div class="card-body">
            <div class="table-responsive">
   
   <%-- <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
        SkinID="Error"></asp:Label>--%>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="FEE_CONC" />
    <table align="center" width="100%">
       
        <tr>
            <td align="left" width="20%">
                <span class="field-label">Allocated
                Business Unit</span></td>
            <td align="left" width="30%">
                <asp:DropDownList id="ddlBusinessUnit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                    DataTextField="BSU_NAME" DataValueField="BSU_ID">
                </asp:DropDownList></td>
       
            <td align="left" width="15%">
                <span class="field-label">Bank</span></td>
            <td align="left" width="35%">
                <table width="100%">
                    <tr>
                        <td width="40%" align="left">
                            <asp:TextBox id="txtBankCode" runat="server" AutoPostBack="True" OnTextChanged="txtBankCode_TextChanged" ></asp:TextBox>
                <asp:ImageButton id="imgBank" runat="server" ImageUrl="~/Images/cal.gif"
                    OnClientClick="getBank();return false;"></asp:ImageButton>
                        </td>
                        <td width="60%" align="left">
                            <asp:TextBox id="txtBankDescr"
                        runat="server" CssClass="inputbox" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                
                    </td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">User</span></td>
            <td align="left">
                <asp:TextBox id="txtUserName" runat="server">
                </asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetFEECounter(); return false;">
                </asp:ImageButton></td>
            
            <td align="left" >
                <span class="field-label">Description</span></td>
            <td align="left" >
                <asp:TextBox ID="txtDescription" runat="server"  TextMode="MultiLine" width="100%"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" OnClick="btnAdd_Click" />
                <asp:Button id="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" OnClick="btnEdit_Click" />
                <asp:Button id="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" ValidationGroup="FEE_CONC" />
                <asp:Button id="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" />
                <asp:Button id="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" OnClick="btnCancel_Click" /></td>
        </tr>
    </table>
    <asp:ObjectDataSource id="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GETBSUFORUSER" TypeName="FeeCommon">
        <selectparameters>
<asp:SessionParameter SessionField="sUsr_name" Type="String" DefaultValue="" Name="USR_ID"></asp:SessionParameter>
</selectparameters>
    </asp:ObjectDataSource>
    <asp:HiddenField id="hfFEECounter" runat="server">
    </asp:HiddenField>

                </div>
            </div>
        </div>
</asp:Content>

