<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" EnableViewState="true" AutoEventWireup="false" CodeFile="ReverseTPTFeeCharge.aspx.vb" Inherits="ReverseTPTFeeCharge" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">
    <script language="javascript" type="text/javascript">

        function CommitButton() {

            var intervalHandle = setInterval(function () {

                if ($("#<%=h_Processeing.ClientID %>").val() == '1') {

                    var $ajaxImage = $("#ajaxImage");
                    var $progressbar = $("#progressbar");
                    var $fsProgress = $("#fsProgress");
                    var $statusDiv = $("#statusDiv");
                    var $startProcessButton = $("#<%= btnCommitImprt.ClientID%>");
                    var $PostbackButton = $("#<%= btnPostback.ClientID%>");
                    var $lblError = $("#<%= lblError.ClientID%>");
                    var $h_status = $("#<%= h_STATUS.ClientID%>");
                    var $h_errormessage = $("#<%= h_errormessage.ClientID%>");
                    var $h_IsSuccess = $("#<%= h_IsSuccess.ClientID%>");

                    var postData = {
                        BATCHNO: $('#ctl00_cphMasterpage_hdBATCHNO').val(),
                        SOURCE: 'FEES_REV',
                        PAGE: 'FEE_REV'
                    }
                    var xhr = $.ajax({
                        url: "../GetBulkProcessProgress.asmx/GetStatus",
                        data: JSON.stringify(postData),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response, status, xhr) {
                            if (response.d.ISERROR) {
                                $h_status.val("")
                                $startProcessButton.removeAttr("disabled");
                                $ajaxImage.hide(500);
                                $progressbar.hide(500);
                                $fsProgress.hide(500);
                                clearInterval(intervalHandle);
                                $startProcessButton.removeAttr("disabled");
                                $h_errormessage.val(response.d.MESSAGE);
                                $("#ctl00_cphMasterpage_h_Processeing").val('');
                                $h_IsSuccess.val("0");
                                $statusDiv.hide();
                                $('#error-message').html(response.d.MESSAGE);
                                $('#alert-error-popup').show();
                                xhr.abort();
                            }
                            else {

                                $startProcessButton.attr("disabled", "disabled");
                                if ($fsProgress.is(':visible') == false)
                                    $fsProgress.show();
                                if ($ajaxImage.is(':visible') == false)
                                    $ajaxImage.show();
                                if ($progressbar.is(':visible') == false)
                                    $progressbar.show();

                                $statusDiv.html(response.d.PERCENTAGE + "% " + response.d.WORKDESCRIPTION);
                                $progressbar.progressbar({
                                    value: parseInt(response.d.PERCENTAGE)
                                });

                                if (response.d.ISCOMPLETE) {
                                    clearInterval(intervalHandle);
                                    $startProcessButton.removeAttr("disabled");
                                    $h_errormessage.val(response.d.MESSAGE);
                                    $h_IsSuccess.val("1");
                                    $("#ctl00_cphMasterpage_h_Processeing").val('');
                                    $h_status.val("0")
                                    xhr.abort();
                                    $ajaxImage.hide(500);
                                    $progressbar.hide(500);
                                    $fsProgress.hide(500);
                                    $PostbackButton.click();
                                }
                                else
                                    $h_status.val("");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $h_status.val("")
                            $startProcessButton.removeAttr("disabled");
                            $ajaxImage.hide(500);
                            $progressbar.hide(500);
                            $fsProgress.hide(500);
                            clearInterval(intervalHandle);
                            $startProcessButton.removeAttr("disabled");
                            $("#ctl00_cphMasterpage_h_Processeing").val('');
                        },
                    });
                }
            }, 2000);
        }

        $(document).ready(function () {
            var $ajaxImage = $("#ajaxImage");
            var $progressbar = $("#progressbar");
            var $fsProgress = $("#fsProgress");
            $ajaxImage.hide(500);
            $progressbar.hide(500);
            $fsProgress.hide(500);
        });

    </script>

    <style type="text/css">
        #alert-error-popup {
            position: fixed;
            top: 100px;
            left: 45%;
            z-index: 1000;
            min-width: 200px;
        }

        .field-rb-label > label {
            font-weight: bold !important;
        }
    </style>
    <div id="alert-error-popup" style="display: none" class="sticky-top alert alert-danger alert-dismissable" role="alert"><i class="fa fa-info"></i><a href="#" class="close" data-dismiss="alert" aria-label="close">�</a><span id="error-message">  </span></div>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Transport Fee Charge Administration
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tbody>
                        <tr>
                            <td><span class="field-label">Charge Type</span></td>
                            <td colspan="3">
                                <asp:RadioButtonList ID="rblChargeType" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="CR">Reversal</asp:ListItem>
                                    <asp:ListItem Value="DR">Charge</asp:ListItem>
                                </asp:RadioButtonList></td>
                        </tr>
                        <tr>

                            <td width="20%"><span class="field-label">Date</span> </td>
                            <td width="30%">
                                <asp:TextBox ID="txtAdjDT" TabIndex="2" runat="server"></asp:TextBox><asp:ImageButton
                                    ID="imgAdjDT" TabIndex="4" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                <asp:CalendarExtender
                                    ID="CalendarExtender4" runat="server" CssClass="MyCalendar" TargetControlID="txtAdjDT"
                                    Enabled="True" PopupButtonID="imgAdjDT" Format="dd/MMM/yyyy">
                                </asp:CalendarExtender>

                            </td>
                            <td align="left"><span class="field-label">Select File</span> </td>
                            <td align="left">
                                <asp:FileUpload ID="FileUpload1" runat="server" Width="255px"></asp:FileUpload><br />
                                <asp:HyperLink
                                    ID="lnkXcelFormat" runat="server">Click here to get the formatted Excel file</asp:HyperLink>
                                <pre id="preMessage" runat="server" class="alert alert-info">The Amount in Excel sheet should be including Tax</pre>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <fieldset id="fsProgress" style="width: 50%; display: none">
                                    <legend>Processing Status</legend>
                                    <div id="ajaxImage" style="display: inline;">
                                        <img alt="" src="../Images/Misc/AjaxLoading.gif" />
                                    </div>
                                    <div id="statusDiv"></div>
                                    <div id="progressbar" style="height: 20px; width: 100%"></div>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table border="0" style="width: 100%;">
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnAddAdj" runat="server" Text="Reset" CssClass="button" Visible="False"></asp:Button></td>
                                        <td align="center">
                                            <asp:Button ID="btnImport" runat="server" Text="Load" CssClass="button"></asp:Button><asp:Button
                                                ID="btnProceedImpt" runat="server" Text="Proceed" CssClass="button" Visible="False"></asp:Button><asp:Button ID="btnCommitImprt" runat="server" Text="Commit" CssClass="button"
                                                    OnClientClick=" CommitButton()" Visible="False"></asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Label ID="lblError2" runat="server" CssClass="error"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Label ID="lblMessage" runat="server" CssClass="error"></asp:Label></td>
                        </tr>
                        <tr id="trGvImport" runat="server">
                            <td id="Td1" align="left" colspan="4" runat="server">
                                <asp:GridView ID="gvExcelImport" runat="server" OnPageIndexChanging="gvExcelImport_PageIndexChanging"
                                    AllowPaging="True" EmptyDataText="No Data" Width="100%" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                    ShowFooter="True" DataKeyNames="bVALID">
                                    <Columns>
                                        <asp:BoundField DataField="SLNO" HeaderText="SERIAL NUMBER">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="BSU" HeaderText="BSU NAME">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="STU_NO" HeaderText="STUDENT ID">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="STU_NAME" HeaderText="STUDENT NAME">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FEE_DESCR" HeaderText="FEE TYPE">
                                            <FooterStyle HorizontalAlign="Right" BorderStyle="None" Height="25px"></FooterStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="REVERSAL_DATE" HeaderText="REVERSAL DATE">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="REVERSAL_AMOUNT" HeaderText="REVERSAL AMOUNT">
                                            <FooterStyle HorizontalAlign="Right" BorderStyle="None" Height="25px"></FooterStyle>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NARRATION" HeaderText="NARRATION">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MONTHS_IN_QUARTER" HeaderText="MONTHS IN QUARTER">
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>

                                        <asp:BoundField DataField="TAX_INVOICE_NUMBER" HeaderText="TAX INVOICE NUMBER">
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>

                                        <asp:BoundField DataField="ERROR_MSG" HeaderText="MESSAGE" HtmlEncode="False">
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                    </Columns>
                                    <FooterStyle BackColor="#99CCFF" Font-Size="Small"></FooterStyle>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr id="trgvImportSmry" runat="server">
                            <td id="Td2" colspan="4" runat="server">
                                <asp:GridView ID="gvImportSmry" runat="server" CssClass="table table-row table-bordered" OnPageIndexChanging="gvExcelImport_PageIndexChanging"
                                    AllowPaging="True" EmptyDataText="No Data" Width="100%" AutoGenerateColumns="False"
                                    PageSize="20">
                                    <Columns>
                                        <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Description">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="REVERSAL_AMOUNT" DataFormatString="{0:n2}" HeaderText="REVERSAL AMOUNT">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnCancelAdj" runat="server" CssClass="button" Text="CANCEL" />
                            </td>
                        </tr>
                    </tbody>
                </table>

                <asp:HiddenField ID="hdBATCHNO" runat="server" />
                <asp:HiddenField ID="h_IsSuccess" runat="server" />
                <asp:HiddenField ID="h_STATUS" runat="server" />
                <asp:HiddenField ID="h_errormessage" runat="server" />
                <asp:HiddenField ID="h_Processeing" runat="server" />
                <asp:Button ID="btnPostback" runat="server" CssClass="button" Style="display: none;" CausesValidation="False" Height="1px" TabIndex="5000" Width="1px" OnClick="btnPostback_Click" />
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
