﻿Imports Telerik.Web.UI
Imports System
Imports System.Collections.Generic
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Services
Imports Microsoft.ApplicationBlocks.Data
Imports UtilityObj
Partial Class Fees_AddVendorforDAXView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F733043") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            hlAddNew.NavigateUrl = "AddVendorforDAX.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            BindBusinessUnit()
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            GridBind()
        End If
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub lblPrintReciept_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblINV_NO As Label = sender.Parent.parent.findcontrol("lblInvNo")
    End Sub

    Private Sub BindBusinessUnit()
        Try
            Dim dsBsu As DataSet = clsAddVendor.GetBusinessUnits(Session("sUsr_name"), True)
            If Not dsBsu Is Nothing AndAlso dsBsu.Tables(0).Rows.Count > 0 Then
                ddlBsu.DataSource = dsBsu
                ddlBsu.DataTextField = "BSU_NAMEwithShort"
                ddlBsu.DataValueField = "BSU_ID"
                ddlBsu.DataBind()
                ddlBsu.SelectedIndex = -1
                ddlBsu.SelectedValue = Session("sBsuid")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, ViewState("MainMnu_code"))
        End Try
    End Sub

    Private Sub GridBind()
        Try
            Dim str_Sql As String = ""
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            str_Filter = ""
            If gvVendor.Rows.Count > 0 Then

                '   --- FILTER CONDITIONS ---
                '   -- 1   Employee No
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvVendor.HeaderRow.FindControl("txtEmpNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "VDR_REF_NO", lstrCondn1)

                '   -- 2  Date
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvVendor.HeaderRow.FindControl("txtEmpName")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "EMP_NAME", lstrCondn2)

                '   -- 3  COMP_NAME
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvVendor.HeaderRow.FindControl("txtVendorcode")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then
                    str_Filter = str_Filter & SetCondn(lstrOpr, "VDR_CODE", lstrCondn3)
                End If
            End If
            Dim str_cond As String = String.Empty
            str_cond = " VDB_BSU_ID = '" & ddlBsu.SelectedValue & "' "
            str_Sql = "SELECT VDR_ID, BSU_SHORTNAME, VDR_REF_NO, EMP_NAME, DES_DESCR, VDR_CODE, VDB_BSU_ID, VDR_LOGDT, EMP_FNAME, EMP_ID FROM DAX.VW_VENDOR_M WHERE 1=1 AND "
            str_Sql = str_Sql & str_cond & str_Filter
            str_Sql = str_Sql & " ORDER BY EMP_FNAME "
            ds = SqlHelper.ExecuteDataset(ConnectionManger.OASIS_DAX_ConnectionString, CommandType.Text, str_Sql)
            gvVendor.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvVendor.DataBind()
                Dim columnCount As Integer = gvVendor.Rows(0).Cells.Count

                gvVendor.Rows(0).Cells.Clear()
                gvVendor.Rows(0).Cells.Add(New TableCell)
                gvVendor.Rows(0).Cells(0).ColumnSpan = columnCount
                gvVendor.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvVendor.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvVendor.DataBind()
            End If
            txtSearch = gvVendor.HeaderRow.FindControl("txtEmpNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvVendor.HeaderRow.FindControl("txtEmpName")
            txtSearch.Text = lstrCondn2

            txtSearch = gvVendor.HeaderRow.FindControl("txtVendorcode")
            txtSearch.Text = lstrCondn3


        Catch ex As Exception

        End Try
    End Sub
    
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvVendor.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvVendor.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub ddlBsu_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlBsu.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub gvVendor_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvVendor.RowDataBound
        Dim lbtnView As LinkButton = e.Row.FindControl("lbtnView")
        If Not lbtnView Is Nothing Then
            Dim VDR_ID = gvVendor.DataKeys(e.Row.RowIndex).Values(0)
            lbtnView.PostBackUrl = "AddVendorforDAX.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("view") & "&ID=" & Encr_decrData.Encrypt(VDR_ID) & ""
        End If
    End Sub
End Class
