<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeTransportMonthEndProcess.aspx.vb" Inherits="Fees_feeRevenueRecognition" title="Untitled Page" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
   
  <script language="javascript" type="text/javascript">     
      function GetStudent()
      {     
        var sFeatures;
        var sFeatures;
        sFeatures="dialogWidth: 875px; ";
        sFeatures+="dialogHeight: 600px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var NameandCode;
        var result;  
          <%--var url;  
               url = "ShowStudentMulti.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu="+document.getElementById('<%= ddlBUnit.ClientID %>').value;
                 result = window.showModalDialog(url,"", sFeatures);
              
            if(result != '' && result != undefined)
            {        
                document.getElementById('<%=txtStudName.ClientID %>').value='Multiple Students selected'; 
                document.getElementById('<%=h_STUD_ID.ClientID %>').value=result;
            }
            return true; 
      } --%>
          var url = "ShowStudentMulti.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu=" + document.getElementById('<%= ddlBUnit.ClientID %>').value;
          var oWnd = radopen(url, "pop_getstudent");

      }

      function OnClientClose1(oWnd, args) {
          //get the transferred arguments

          var arg = args.get_argument();

          if (arg) {

              NameandCode = arg.NameandCode.split('||');
              document.getElementById('<%=txtStudName.ClientID %>').value = 'Multiple Students selected';
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = arg.NameandCode;
                __doPostBack('<%=txtStudName.ClientID%>', 'TextChanged');
            }
        }
       
    function Showdata() 
     {      
        var sFeatures,url;
        sFeatures="dialogWidth:500px; ";
        sFeatures+="dialogHeight: 400px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var NameandCode;
        var result; 
        url= "../common/PopupShowData.aspx?id=VOUCHER&docid="+document.getElementById('<%= H_DOCNO.ClientID %>').value+"&bsu="+document.getElementById('<%= ddlBUnit.ClientID %>').value; 
        //result = window.showModalDialog(url,"", sFeatures); 
        var oWnd = radopen(url, "pop_showdata");
        return false;
    }       
    
function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>


     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_showdata" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getstudent" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table width="100%">
        <tr valign="bottom">
            <td align="left">
                <asp:Label ID="lblError" runat="server" SkinID="error"></asp:Label>              
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
            </td>
        </tr>         
    </table>
    <table Width="100%">
       
        <tr>
            <td align="left" width="20%">
                <span class="field-label">Business Unit</span></td>
            
            <td align="left"  width="30%">
                <asp:DropDownList ID="ddlBUnit" runat="server" AutoPostBack="True" DataSourceID="ODSGETBSUFORUSER" DataTextField="BSU_NAME" DataValueField="BSU_ID">
                </asp:DropDownList></td>
            <td align="left" width="20%"></td>
            <td align="left" width="30%"></td>
        </tr>
        <tr id ="trACD" runat="server">
            <td align="left" >
                <span class="field-label">Academic Year</span></td>
            
            <td align="left">
                <asp:DropDownList ID="ddlAcademic" runat="server" SkinID="DropDownListNormal" AutoPostBack="True">
                </asp:DropDownList></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">Date</span></td>
            
            <td align="left" >
                <asp:TextBox ID="txtDate" runat="server" ></asp:TextBox>
                <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"
                    TabIndex="4" /></td>
            <td></td>
            <td></td>
        </tr>
        <tr runat="server" id="tr_student_pick" visible="false" >
            <td align="left">
                <asp:CheckBox ID="chkStudentby" runat="server" Text="By Student" AutoPostBack="True" /></td>
            
            <td align="left" colspan="3">
                <asp:TextBox ID="txtStudName" runat="server" ReadOnly="True"></asp:TextBox>
                <asp:LinkButton ID="lblAddNewStudent" runat="server" CausesValidation="False" Enabled="False">Add</asp:LinkButton>
                <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetStudent(); return false;" /><br />
                <br />
                <asp:GridView ID="gvStudentDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    SkinID="GridViewNormal" Width="100%" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Student ID">
                            <ItemTemplate>
                                <asp:Label ID="lblStudID" runat="server" Text='<%# bind("STU_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student Name">
                            <ItemTemplate>
                                <asp:Label ID="lblStudentName" runat="server" Text='<%# bind("STU_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete" Visible="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr runat="server" id ="trHardClose">
            <td colspan="4" style="text-align: left">
                <asp:CheckBox id="chkHardClose" runat="server" CssClass="field-label" Text="Hard Close">
                </asp:CheckBox></td>
        </tr>
        <tr>
        <td colspan="4" align="center">
    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClientClick="show();" />
            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" Visible="False" />
            <asp:LinkButton ID="lblViewVoucher" runat="server" OnClientClick="Showdata() ;return false;" Visible="False">View Voucher</asp:LinkButton></td>
        </tr>
        </table>
         <table Width="100%" >
        <tr> 
            <td align="left"> 
            <asp:Panel ID="pnFOMonthendHeader" runat="server" Width="100%"> 
            <div  style="width:100%; padding:5px; cursor: pointer; vertical-align: top;" class="title-bg mb-2" align="left">
                <asp:ImageButton ID="imgFOMonthend" style="vertical-align: top;" runat="server" ImageUrl="~/images/password/expand.gif" AlternateText="(Show Details...)"/>
                FO Monthend Status </div> 
        </asp:Panel>
       <asp:Panel id="pnFOMonthendDetail" runat="server" Width="100%"> 
        <ajaxToolkit:CollapsiblePanelExtender ID="cpeQuick" runat="server" CollapseControlID="pnFOMonthendHeader"
        Collapsed="True" CollapsedImage="~/images/password/expand.gif" CollapsedText="(Show...)"
        ExpandControlID="pnFOMonthendHeader" ExpandedImage="~/images/password/collapse.gif" ExpandedText="(Hide...)"
        ImageControlID="imgFOMonthend" SuppressPostBack="true"
        TargetControlID="pnFOMonthendDetail">
        </ajaxToolkit:CollapsiblePanelExtender>
           <asp:GridView ID="gvFOMonthend" runat="server" CssClass="table table-bordered table-row" SkinID="GridViewNormal">
           </asp:GridView>
       </asp:Panel> 
        <asp:Panel id="pnlRevenueHeader" runat="server" Width="100%" >
        <div  style="width:100%; padding:5px; cursor: pointer; vertical-align: top;" class="title-bg mb-2" align="left">
          <asp:ImageButton ID="imgRevenue" style="vertical-align: top;" runat="server" ImageUrl="~/images/password/expand.gif" AlternateText="(Show Details...)"/>
          Revenue Recognition </div> 
        </asp:Panel>
       <asp:Panel id="pnlRevenueDetails" runat="server" Width="100%" >
         <ajaxToolkit:CollapsiblePanelExtender ID="cpeAlerts" runat="server" CollapseControlID="pnlRevenueHeader"
            Collapsed="True" CollapsedImage="~/images/password/expand.gif" CollapsedText="(Show...)"
            ExpandControlID="pnlRevenueHeader" ExpandedImage="~/images/password/collapse.gif" ExpandedText="(Hide...)"
            ImageControlID="imgRevenue" SuppressPostBack="true"
            TargetControlID="pnlRevenueDetails">
         </ajaxToolkit:CollapsiblePanelExtender>
          <asp:GridView ID="gvRevReco" runat="server" CssClass="table table-bordered table-row" SkinID="GridViewNormal">
          </asp:GridView>
         </asp:Panel>
      <asp:Panel ID="pnlChargingHeader" runat="server" Width="100%"> 
      <div class="title-bg mb-2" style="width:100%; padding:5px; cursor: pointer; vertical-align: top;" align="left">
          <asp:ImageButton ID="imgCharging" runat="server" AlternateText="(Show Details...)"
              ImageUrl="~/images/password/expand.gif" Style="vertical-align: top" />&nbsp;
          Monthly
          Fee Charging</div>
        </asp:Panel>
        <asp:Panel ID="pnlChargingDetail" runat="server" Width="100%">
        <ajaxToolkit:CollapsiblePanelExtender ID="cpePending" runat="Server" CollapseControlID="pnlChargingHeader"
            Collapsed="True" CollapsedImage="~/images/password/expand.gif" CollapsedText="(Show Details...)"
            ExpandControlID="pnlChargingHeader" ExpandedImage="~/images/password/collapse.gif" ExpandedText="(Hide Details...)"
            ImageControlID="imgCharging" SuppressPostBack="true"
            TargetControlID="pnlChargingDetail" >
         </ajaxToolkit:CollapsiblePanelExtender>
            <asp:GridView ID="gvCharging" runat="server" CssClass="table table-bordered table-row" SkinID="GridViewNormal">
            </asp:GridView>
            </asp:Panel>
          
        <asp:Panel ID="pnlPostingHeader" runat="server" Width="100%"> 
      <div class="title-bg mb-2" style="width:100%; padding:5px; cursor: pointer; vertical-align: top;" align="left">
          <asp:ImageButton ID="imgPosting" runat="server" AlternateText="(Show Details...)"
              ImageUrl="~/images/password/expand.gif" Style="vertical-align: top" />&nbsp;
          Fee Charge Posting</div>
        </asp:Panel>
        <asp:Panel ID="pnlPostingDetail" runat="server" Width="98%">
        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="Server" CollapseControlID="pnlPostingHeader"
            Collapsed="True" CollapsedImage="~/images/password/expand.gif" CollapsedText="(Show Details...)"
            ExpandControlID="pnlPostingHeader" ExpandedImage="~/images/password/collapse.gif" ExpandedText="(Hide Details...)"
            ImageControlID="imgPosting" SuppressPostBack="true"
            TargetControlID="pnlPostingDetail" >
         </ajaxToolkit:CollapsiblePanelExtender>
            <asp:GridView ID="gvPosting" runat="server" CssClass="table table-bordered table-row" SkinID="GridViewNormal">
            </asp:GridView>
            </asp:Panel>    
        </td>
        </tr>
        <script Language="javascript" type="text/javascript">
    function show()
        {  
        if (document.getElementById('<%=h_Save.ClientID %>').value =='true')      
        setTimeout('showGif()',500); 
        }
    function showGif()
        { 
        setTimeout('showGif()',500);
        if (document.getElementById('<%=h_Save.ClientID %>').value =='true')  
        PageMethods.HitPage();
        }
     </script>
    </table>
    <asp:ObjectDataSource ID="ODSGETBSUFORUSER" runat="server" SelectMethod="GETBSUFORUSER"
        TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
<asp:SessionParameter SessionField="sUsr_name" Type="String" Name="USR_ID"></asp:SessionParameter>
</SelectParameters>
    </asp:ObjectDataSource> 
    <asp:HiddenField ID="h_STUD_ID" runat="server" /><asp:HiddenField ID="H_DOCNO" runat="server" /><asp:HiddenField ID="h_Save" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFrom" CssClass="MyCalendar" 
        TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calDate2" runat="server" Format="dd/MMM/yyyy" CssClass="MyCalendar" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    
                </div>
            </div>
        </div>
</asp:Content>
