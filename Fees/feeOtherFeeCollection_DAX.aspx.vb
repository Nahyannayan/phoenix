Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports AjaxControlToolkit
Imports System.Linq
Imports System.Configuration
Imports Telerik.Web.UI

Partial Class Fees_feeOtherFeeCollection_DAX
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim SqlStr As String = ""
    Private Property bMUSIC() As Boolean
        Get
            Return ViewState("bMUSIC")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bMUSIC") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        If Page.IsPostBack = False Then
            Try
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                ViewState("datamode") = "add"

                'Changes on 12DEC2019
                ddEventtype.Visible = False
                lbleventtype.Visible = False
                lbleventid.Visible = False
                If Convert.ToBoolean(Session("BSU_bVATEnabled")) Then
                    tr_taxInv.Visible = True
                    tr1_header.Visible = False
                Else
                    tr_taxInv.Visible = False
                    tr1_header.Visible = False
                End If
                lblInvno.Attributes.Add("style", "visibility:hidden !important")
                txtInvno.Attributes.Add("style", "visibility:hidden !important")
                'Changes ends on 12DEC2019

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBSuid")
                Dim USR_NAME As String = Session("sUsr_name")
                bMUSIC = False
                bind_Currency()
                LOAD_TAX_CODES()
                'Changes on 12DEC2019
                BIND_CUSTOMERS()
                'Changes ends on 12DEC2019
                InitialiseCompnents()
                BindEmirate()
                FillacademicYear()
                FillGrade()
                'Changes on 12DEC2019
                txtInvno.Enabled = False
                'Changes ends on 12DEC2019
                '------------------------
                Dim sqlStr As String
                sqlStr = "SELECT isnull(BSU_bFeesMulticurrency,0) FROM BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & Session("sbsuid") & "'"
                If Mainclass.getDataValue(sqlStr, "OASISConnectionString") Then
                    pnlCurrency.Visible = True
                    trCurrency.Visible = True
                Else
                    pnlCurrency.Visible = False
                    trCurrency.Visible = False
                End If
                '------------------------
                Dim formatstring As String = Session("BSU_DataFormatString")
                formatstring = formatstring.Replace("0.", "###,###,###,##0.")
                DirectCast(gvFeeCollection.Columns(3), BoundField).DataFormatString = "{0:" & formatstring & "}"
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code").ToString <> OASISConstants.MNU_FEE_COLLECTION_OTHER _
                                And ViewState("MainMnu_code").ToString <> "F300255") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If

                Dim qry As String = "SELECT CONVERT(VARCHAR(3),CREDITCARD_S.CRR_ID)+'='+CONVERT(VARCHAR(10),isnull(dbo.CREDITCARD_S.CRR_CLIENT_RATE,0))+'|' FROM CREDITCARD_S INNER JOIN " & _
                                    " CREDITCARD_M ON CREDITCARD_S.CRR_CRI_ID = CREDITCARD_M.CRI_ID INNER JOIN " & _
                                    " CREDITCARD_PROVD_M ON CREDITCARD_S.CRR_CPM_ID = CREDITCARD_PROVD_M.CPM_ID " & _
                                    " WHERE     (CREDITCARD_S.CRR_bOnline = 0) FOR XML PATH('')"
                Me.hfCobrand.Value = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, qry)
                'BIND_CUSTOMERS() 'Changes on 12DEC2019 - commented

                SET_VAT_DROPDOWN_RIGHT()
                DISABLE_TAX_FIELDS()
                'lblAlert.Text = UtilityObj.getErrorMessage("642")
                lblAlert.CssClass = ""
                txtAmount_TextChanged(Nothing, Nothing)
                SET_MUSIC_CONTROLS()
            Catch ex As Exception
                Errorlog(ex.Message, "pageload")
            End Try
        End If
    End Sub
    Sub SET_VAT_DROPDOWN_RIGHT()
        Dim bHasRight As Boolean = Convert.ToBoolean(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CASE WHEN COUNT(USER_NAME)>0 THEN 1 ELSE 0 END AS bEXIST FROM OASIS.TAX.VW_VAT_ENABLE_USERS WHERE USER_NAME='" & Session("sUsr_name").ToString & "' AND USER_SOURCE='FEES'"))
        If Not bHasRight Then
            ddlTAXCode.Enabled = False
        End If
    End Sub
    Sub BindEmirate()
        Try
            Dim str_default_emirate As String = GetDataFromSQL("SELECT BSU_CITY FROM BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & Session("sBsuid") & "'", ConnectionManger.GetOASISConnectionString)
            If str_default_emirate <> "--" Then
                ddlEmirate.SelectedIndex = -1
                ddlEmirate.Items.FindByValue(str_default_emirate).Selected = True
            End If
        Catch ex As Exception
            Errorlog(ex.Message, "emirate")
        End Try
    End Sub
    Private Sub bind_Currency() 'bind the currency combo according to selected date
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            ddCurrency.Items.Clear()
            ddCurrency.DataSource = MasterFunctions.GetExchangeRates(Now.Date.ToString("dd/MMM/yyyy"), Session("sBsuid"), Session("BSU_CURRENCY"))
            ddCurrency.DataTextField = "EXG_CUR_ID"

            ddCurrency.DataValueField = "RATES"
            ddCurrency.DataBind()
            If ddCurrency.Items.Count > 0 Then
                Dim lstDrp As New ListItem
                lstDrp = ddCurrency.Items.FindByText(Session("BSU_CURRENCY"))
                If Not lstDrp Is Nothing Then
                    ddCurrency.SelectedValue = lstDrp.Value
                End If
                lblExgRate.Text = ddCurrency.SelectedItem.Value.Split("__")(0).Trim
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub LOAD_TAX_CODES()
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT TAX_ID,TAX_CODE,TAX_DESCR FROM TAX.VW_TAX_CODES WHERE ISNULL(TAX_bDeleted,0)=0 ORDER BY TAX_ID")
        Dim dtACD As DataTable = GetTAXCode()
        ddlTAXCode.DataSource = dtACD
        ddlTAXCode.DataTextField = "TAX_DESCR"
        ddlTAXCode.DataValueField = "TAX_CODE"
        ddlTAXCode.DataBind()
    End Sub
    Function GetTAXCode() As DataTable
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        Dim dsData As New DataSet
        dsData = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.[GET_TAX_CODES]", pParms)


        'Dim sql_query As String = "select TAX_CODE ID,TAX_DESCR DESCR FROM OASIS.TAX.VW_TAX_CODES ORDER BY TAX_ID "
        'Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        'CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If

    End Function
    Private Sub DISABLE_TAX_FIELDS() 'hide tax related fields if the unit is not TAX enabled
        Dim bShow As Boolean = False
        If Not Session("BSU_bVATEnabled") Is Nothing AndAlso IsNumeric(Session("BSU_bVATEnabled")) Then
            bShow = Convert.ToBoolean(Session("BSU_bVATEnabled"))
        End If
        trCustomer.Visible = bShow
        trTAXAmt1.Visible = bShow
        trTAXAmt2.Visible = bShow
        trTAXcode1.Visible = bShow
        trTAXcode2.Visible = bShow
        trNetAmt1.Visible = bShow
        trNetAmt2.Visible = bShow
        trAlertmessage.Visible = bShow
        gvFeeCollection.Columns(8).Visible = bShow
        gvFeeCollection.Columns(9).Visible = bShow
        gvFeeCollection.Columns(10).Visible = bShow
        trVATTotal.Visible = bShow
    End Sub

    'Changes on 12DEC2019

    Protected Sub txtInvno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles txtInvno.TextChanged
        If txtInvno.Text <> "" Then
            txtInvno.Attributes.Add("style", "border-color:#dee2da")
        Else
            txtInvno.Attributes.Add("style", "border-color:red !important")
        End If

    End Sub
    Protected Sub CheckedChangedNo(ByVal sender As Object, ByVal e As System.EventArgs)
        txtInvno.Text = ""
        txtInvno.Enabled = True
        If Chk_InvNo.Checked = True Then
            Chk_InvYes.Checked = False
            chkBulkUpload.Checked = False
            chkBulkUpload.Enabled = False
            'txtInvno.Attributes.Add("style", "border-color:red !important")
            lblInvno.Attributes.Add("style", "visibility:visible !important")
            txtInvno.Attributes.Add("style", "visibility:visible !important;border-color:red !important")
            tr1_header.Visible = True
            chkBulkUpload_CheckedChanged(Nothing, Nothing)
        Else
            chkBulkUpload.Checked = False
            chkBulkUpload.Enabled = True
            txtInvno.Attributes.Add("style", "border-color:#dee2da")
            lblInvno.Attributes.Add("style", "visibility:hidden !important")
            txtInvno.Attributes.Add("style", "visibility:hidden !important")
            tr1_header.Visible = False
            chkBulkUpload_CheckedChanged(Nothing, Nothing)
        End If


        'new change for filling the fee type based on generate invoice
        ddFeetype.Items.Clear()
        ddFeetype.DataSource = Nothing
        ddFeetype.DataBind()


        If ddEventtype.Visible Then
            ddEventtype.Items.Clear()
            ddEventtype.DataSource = Nothing
            ddEventtype.DataBind()
            ddEventtype.Visible = False
            lbleventtype.Visible = False
            lbleventid.Visible = False
        End If


        txtDAccountCode.Text = ""
        txtDAccountName.Text = ""
        If ddCollection.SelectedValue = "8" Then
            ddlEvents.SelectedValue = ""
        End If
        If ddCollection.SelectedValue = "4" Then
            ddlTAXCode.SelectedValue = "NA"
        End If
        ddlCustomers.SelectedValue = "0"
        If Chk_InvNo.Checked Then
            ddFeetype.Enabled = False
        Else
            ddFeetype.Enabled = True
        End If
        ddCollection_SelectedIndexChanged(Nothing, Nothing)
        'new change for filling the fee type based on generate invoice ends here

    End Sub
    Protected Sub CheckedChangedYes(ByVal sender As Object, ByVal e As System.EventArgs)
        txtInvno.Text = ""
        txtInvno.Enabled = False
        chkBulkUpload.Checked = False
        chkBulkUpload.Enabled = True
        If Chk_InvYes.Checked = True Then
            Chk_InvNo.Checked = False
            txtInvno.Attributes.Add("style", "border-color:#dee2da")
            tr1_header.Visible = False
        Else
            txtInvno.Attributes.Add("style", "border-color:#dee2da")
            tr1_header.Visible = False
        End If
        lblInvno.Attributes.Add("style", "visibility:hidden !important")
        txtInvno.Attributes.Add("style", "visibility:hidden !important")
        chkBulkUpload_CheckedChanged(Nothing, Nothing)

        'new change for filling the fee type based on generate invoice
        ddFeetype.Items.Clear()
        ddFeetype.DataSource = Nothing
        ddFeetype.DataBind()

        If ddEventtype.Visible Then
            ddEventtype.Items.Clear()
            ddEventtype.DataSource = Nothing
            ddEventtype.DataBind()
            ddEventtype.Visible = False
            lbleventtype.Visible = False
            lbleventid.Visible = False
        End If


        txtDAccountCode.Text = ""
        txtDAccountName.Text = ""
        If ddCollection.SelectedValue = "8" Then
            ddlEvents.SelectedValue = ""
        End If
        If ddCollection.SelectedValue = "4" Then
            ddlTAXCode.SelectedValue = "NA"
        End If
        ddlCustomers.SelectedValue = "0"
        If Chk_InvNo.Checked Then
            ddFeetype.Enabled = False
        Else
            ddFeetype.Enabled = True
        End If

        'new change for filling the fee type based on generate invoice ends here
        ddCollection_SelectedIndexChanged(Nothing, Nothing)
    End Sub
    'Changes ends on 12DEC2019

    Protected Sub DDCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddCurrency.SelectedIndexChanged
        lblExgRate.Text = ddCurrency.SelectedItem.Value.Split("__")(0).Trim
        'Gridbind_Feedetails()
        Session("FeeCollection") = FeeCollectionOther.CreateFeeCollectionOther()
        Me.gvFeeCollection.DataSource = ""
        Me.gvFeeCollection.DataBind()
        'Me.txtReceivedTotal.Text = "0"
        Me.lblTotalNETAmount.Text = "0.00"
        lblSubTotal.Text = "0.00"
        lblTotalVATAmount.Text = "0.00"
        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") Then
            gvFeeCollection.Columns(5).Visible = True

            gvFeeCollection.Columns(5).HeaderText = "Paying Now (" & ddCurrency.SelectedItem.Text & ")"
            'lblOutstandingFC.Visible = True
            'lblDueFC.Visible = True
        Else
            gvFeeCollection.Columns(5).Visible = False
            'lblOutstandingFC.Visible = False
            'lblDueFC.Visible = False
        End If

    End Sub
    Sub InitialiseCompnents()
        txtReceivedTotal.Attributes.Add("readonly", "readonly")
        txtCrCharge.Attributes.Add("readonly", "readonly")
        h_BSU_ID.Value = Session("sBsuId")

        'txtCashTotal.Attributes.Add("onKeyPress", "allownumber();")
        ddCollection.DataBind()
        If Not ddCollection.Items.FindByValue("5") Is Nothing Then
            ddCollection.SelectedIndex = -1
            ddCollection.Items.FindByValue("5").Selected = True
        End If
        ddCollection_SelectedIndexChanged(Nothing, Nothing)
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtChqDate.Text = txtFrom.Text
        ddlEmirate.DataBind()
        Session("FeeCollection") = FeeCollectionOther.CreateFeeCollectionOther()
        BindCollectionType()
        Set_CollectionControls()
        gvFeeCollection.DataBind()
        txtRemarks.Text = ddCollection.SelectedItem.Text.ToString() & "  For : " & txtFrom.Text.ToString()
        txtAmount.Text = "0.00"
        txtAmount_TextChanged(Nothing, Nothing)
    End Sub

    Sub BindCollectionType()
        'If ddlPaymentMode.Items.Count = 0 Then
        '    ddlPaymentMode.Items.Add(New ListItem("Cash", COLLECTIONTYPE.CASH))
        '    ddlPaymentMode.Items.Add(New ListItem("Cheque", COLLECTIONTYPE.CHEQUES))
        '    ddlPaymentMode.Items.Add(New ListItem("Credit Card", COLLECTIONTYPE.CREDIT_CARD))
        'End If
        If rblPaymentModes.Items.Count = 0 Then
            rblPaymentModes.Items.Add(New ListItem("CASH", COLLECTIONTYPE.CASH))
            rblPaymentModes.Items.Add(New ListItem("CHEQUE", COLLECTIONTYPE.CHEQUES))
            rblPaymentModes.Items.Add(New ListItem("CREDIT CARD", COLLECTIONTYPE.CREDIT_CARD))
            rblPaymentModes.Items(0).Selected = True
        End If
    End Sub

    Sub BIND_CUSTOMERS()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "EXEC FEES.GET_INVOICE_CUSTOMERS @BSU_ID='" & Session("sbsuid") & "'")
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ddlCustomers.Items.Clear()
            ddlCustomers.DataSource = ds.Tables(0)
            ddlCustomers.DataTextField = "CUS_ACT_NAME"
            ddlCustomers.DataValueField = "CUS_ACT_ID"
            ddlCustomers.DataBind()
        End If
    End Sub

    'Changes on 12DEC2019
    Protected Sub ddlCustomers_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)

        Dim cust_val As String = ddlCustomers.SelectedItem.Value
        If Not cust_val Is Nothing And cust_val = "0" Then
            ddFeetype.Items.Clear()
            ddFeetype.DataSource = Nothing
            ddFeetype.DataBind()

            If ddEventtype.Visible Then
                ddEventtype.Items.Clear()
                ddEventtype.DataSource = Nothing
                ddEventtype.DataBind()
                ddEventtype.Visible = False
                lbleventtype.Visible = False
                lbleventid.Visible = False
            End If


            txtDAccountCode.Text = ""
            txtDAccountName.Text = ""
            If ddCollection.SelectedValue = "8" Then
                ddlEvents.SelectedValue = ""
            End If
            If ddCollection.SelectedValue = "4" Then
                ddlTAXCode.SelectedValue = "NA"
            End If

        Else
            If Chk_InvYes.Checked Or Chk_InvNo.Checked Then
                FillFeeType()
                ddFeetype_SelectedIndexChanged(Nothing, Nothing)
            End If

            If Chk_InvNo.Checked Then
                ddFeetype.Enabled = False
            Else
                ddFeetype.Enabled = True
            End If

        End If

    End Sub
    Protected Sub ddFeetype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles ddFeetype.SelectedIndexChanged
        Dim fee_val As String = ddFeetype.SelectedItem.Value
        If Not fee_val Is Nothing And fee_val = "0" Then
            txtDAccountCode.Text = ""
            txtDAccountName.Text = ""
            If ddCollection.SelectedValue = "8" Then
                ddlEvents.SelectedValue = ""
            End If
            If ddCollection.SelectedValue = "9" Then

            End If
            If ddCollection.SelectedValue = "4" Then
                ddlTAXCode.SelectedValue = "NA"
            End If
            If ddFeetype.SelectedValue = "0" Then
                lbleventtype.Visible = False
                lbleventid.Visible = False

                ddEventtype.Items.Clear()
                ddEventtype.DataSource = Nothing
                ddEventtype.DataBind()
                ddEventtype.Visible = False
           
            End If
        Else
            If Convert.ToBoolean(Session("BSU_bVATEnabled")) Then
                If Chk_InvYes.Checked Or Chk_InvNo.Checked Then
                    GetFeeDetails()
                End If
            Else
                GetFeeDetails()
            End If

        End If

    End Sub

    Protected Sub GetFeeDetails()
        Try

            'If Chk_InvYes.Checked Or Chk_InvNo.Checked Then
            Dim dt As DataTable = getFeeTypes(2, ddFeetype.SelectedValue, Session("sBSUID"), ddlCustomers.SelectedItem.Value, ddCollection.SelectedValue, IIf(Chk_InvYes.Checked, 1, 0))
            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                txtDAccountCode.Text = dt.Rows(0)("ACCOUNT_CODE").ToString
                txtDAccountName.Text = dt.Rows(0)("ACCOUNT_NAME").ToString
                If ddCollection.SelectedValue = "8" Then
                    ddlEvents.SelectedValue = dt.Rows(0)("EVENT_ID").ToString
                End If
                ddlTAXCode.SelectedValue = dt.Rows(0)("TAX_TYPE").ToString
                ddlTAXCode_SelectedIndexChanged(Nothing, Nothing)

                If (dt.Rows(0)("SHOW_EVENTTYPE_LIST").ToString = "1") Then
                    ddEventtype.Visible = True
                    If ddFeetype.SelectedValue = "0" Then
                        lbleventtype.Visible = False
                        lbleventid.Visible = False
                    Else
                        lbleventtype.Visible = True
                        lbleventid.Visible = True
                    End If
               
                    lbleventtype.Text = "Type (" & ddFeetype.SelectedItem.Text & ")"
                    FillEventType()
                Else
                    If ddEventtype.Visible Then
                        ddEventtype.Items.Clear()
                        ddEventtype.DataSource = Nothing
                        ddEventtype.DataBind()
                        ddEventtype.Visible = False
                        lbleventtype.Visible = False
                        lbleventid.Visible = False
                    End If
                End If

            End If
            'End If
        Catch ex As Exception

        End Try
    End Sub

    Sub FillFeeType()
        If Convert.ToBoolean(Session("BSU_bVATEnabled")) Then
            If Chk_InvYes.Checked Or Chk_InvNo.Checked Then
                Dim dtFeeType As DataTable = getFeeTypes(1, 0, Session("sBSUID"), ddlCustomers.SelectedItem.Value, ddCollection.SelectedValue, IIf(Chk_InvYes.Checked, 1, 0))
                ddFeetype.DataSource = dtFeeType
                ddFeetype.DataTextField = "FEE_DESCR"
                ddFeetype.DataValueField = "FEE_ID"
                ddFeetype.DataBind()
            End If
        Else
            Dim dtFeeType As DataTable = getFeeTypes(1, 0, Session("sBSUID"), ddlCustomers.SelectedItem.Value, ddCollection.SelectedValue, IIf(Chk_InvYes.Checked, 1, 0))
            ddFeetype.DataSource = dtFeeType
            ddFeetype.DataTextField = "FEE_DESCR"
            ddFeetype.DataValueField = "FEE_ID"
            ddFeetype.DataBind()
        End If

    End Sub
    Sub FillEventType()
        Dim dtEventType As DataTable = getFeeTypes(3, ddFeetype.SelectedItem.Value, Session("sBSUID"), ddlCustomers.SelectedItem.Value, ddCollection.SelectedValue, IIf(Chk_InvYes.Checked, 1, 0))
        ddEventtype.DataSource = dtEventType
        ddEventtype.DataTextField = "DESCR"
        ddEventtype.DataValueField = "ID"
        ddEventtype.DataBind()
    End Sub

    Public Shared Function getFeeTypes(ByVal OPTIONS As Integer, ByVal FEE_ID As String, ByVal BSU_ID As String, ByVal CustomerID As String, ByVal CollectionID As String, ByVal IsGenerateInvoice As Boolean) As DataTable
        Dim pParms(6) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = FEE_ID
        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = BSU_ID

        pParms(3) = New SqlClient.SqlParameter("@CUSTOMER_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = CustomerID
        pParms(4) = New SqlClient.SqlParameter("@COLLECTION_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = CollectionID

        pParms(5) = New SqlClient.SqlParameter("@ISGENERATE_INVOICE", SqlDbType.Bit)
        pParms(5).Value = IsGenerateInvoice

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "[FEES].[FEE_GETFEESFOR_OTHCOLLECTION]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    'Changes ends on 12DEC2019

    Private Sub FillEvents(Optional ByVal EVT_ID As String = "")

        Me.trEvents.Visible = True
        Dim dtEvents As DataTable = FeeCommon.Get_Events_and_Activities(Session("sBSUID"), ddCollection.SelectedValue, txtDAccountCode.Text, Me.txtFrom.Text)
        ddlEvents.DataSource = dtEvents
        ddlEvents.DataTextField = "EVT_DESCR"
        ddlEvents.DataValueField = "EVT_ID"
        ddlEvents.DataBind()
        'For Each rowACD As DataRow In dtEvents.Rows
        '    If rowACD("ACD_CURRENT") Then
        '        ddlEvents.Items.FindByValue(rowACD("ACD_ID")).Selected = True
        '        Exit For
        '    End If
        'Next
        If EVT_ID <> "" Then
            ddlEvents.Items.FindByValue(EVT_ID).Selected = True
        End If

    End Sub
    Private Sub FillCashFlow(Optional ByVal EVT_ID As String = "")
        Me.trCashFlow.Visible = True
        Dim dtEvents As DataTable = FeeCommon.Get_CashFlowList(Session("sBSUID"), ddCollection.SelectedValue, txtDAccountCode.Text, Me.txtFrom.Text)
        ddlCashFlow.DataSource = dtEvents
        ddlCashFlow.DataTextField = "EVT_DESCR"
        ddlCashFlow.DataValueField = "EVT_ID"
        ddlCashFlow.DataBind()
        If EVT_ID <> "" Then
            ddlCashFlow.Items.FindByValue(EVT_ID).Selected = True
        End If
    End Sub


    Sub Clear_All()
        txtCCTotal.Text = "0.00"
        txtCrCharge.Text = "0.00"
        Me.hfCobrand.Value = ""
        txtChequeTotal.Text = ""
        txtCashTotal.Text = ""
        txtReceivedTotal.Text = ""
        lblTotalNETAmount.Text = "0.00"
        lblSubTotal.Text = "0.00"
        lblTotalVATAmount.Text = "0.00"
        txtDefaultAmount.Text = "0.00"
        txtRefno.Text = ""
        txtCreditno.Text = ""
        txtBank.Text = ""
        h_Bank.Value = ""
        txtBankAct1.Text = ""
        hfBankAct1.Value = ""
        txtChqno.Text = ""
        txtChqDate.Text = txtFrom.Text
        txtDAccountCode.Text = ""
        txtDAccountName.Text = ""
        txtChequeTotal.Text = ""
        txtRemarks.Text = ""
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        trEvents.Visible = False
        trCashFlow.Visible = False
        hf_OFM_ID.Value = ""
        hf_VDR_CODE.Value = ""
        txtVendorCode.Text = ""
        txtVendorDescr.Text = ""
        If Not ddCollection.Items.FindByValue("5") Is Nothing Then
            ddCollection.SelectedIndex = -1
            ddCollection.Items.FindByValue("5").Selected = True
            ddCollection_SelectedIndexChanged(Nothing, Nothing)
        End If
        txtAmount.Text = "0.00"
        txtAmount_TextChanged(Nothing, Nothing)
        chkBulkUpload.Checked = False
        chkBulkUpload_CheckedChanged(Nothing, Nothing)

        Chk_InvNo.Checked = False
        Chk_InvYes.Checked = False
        txtInvno.Text = ""
        CheckedChangedNo(Nothing, Nothing)
    End Sub

    Sub Clear_Details()
        txtCCTotal.Text = "0.00"
        txtCrCharge.Text = "0.00"
        txtChargeTotal.Text = "0.00"
        txtChequeTotal.Text = "0.00"
        txtCashTotal.Text = "0.00"
        txtAmount.Text = "0.00"
        txtDefaultAmount.Text = "0.00"
        txtCreditno.Text = ""
        txtBank.Text = ""
        h_Bank.Value = ""
        txtBankAct1.Text = ""
        hfBankAct1.Value = ""
        txtChqno.Text = ""
        txtChqDate.Text = txtFrom.Text
        txtChequeTotal.Text = ""
        txtAmount_TextChanged(Nothing, Nothing)
    End Sub
    Private Function GetAccountDescr(ByVal ACT_ID As String) As String
        GetAccountDescr = ""
        Try
            If ACT_ID <> "" Then
                GetAccountDescr = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "SELECT ACT_NAME FROM dbo.ACCOUNTS_M WITH(NOLOCK) WHERE ACT_ID='" & ACT_ID & "'")
            End If
        Catch ex As Exception
            GetAccountDescr = ""
        End Try
    End Function

    Private Sub SET_MUSIC_CONTROLS()
        If Session("sbsuid") = "500601" Or Session("sbsuid") = "500610" Or Session("sbsuid") = "500611" Then 'IF PLT or ELL
            bMUSIC = True
            If Not ddCollection.Items.FindByValue("8") Is Nothing Then
                ddCollection.SelectedValue = "8"
                ddCollection_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If

    End Sub


    Sub Set_GridTotal()
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
            Dim txtAmountToPayFC As TextBox = CType(gvr.FindControl("txtAmountToPayFC"), TextBox)
            If Not txtAmountToPay Is Nothing Then
                If Not IsNumeric(txtAmountToPay.Text) Then
                    txtAmountToPay.Text = "0"
                End If
                If gvr.RowIndex < Session("FeeCollection").rows.count Then
                    Session("FeeCollection").rows(gvr.RowIndex)(("Amount")) = txtAmountToPay.Text
                End If
            End If
            If Not txtAmountToPayFC Is Nothing Then
                If Not IsNumeric(txtAmountToPayFC.Text) Then
                    txtAmountToPayFC.Text = "0"
                End If
                If gvr.RowIndex < Session("FeeCollection").rows.count Then
                    Session("FeeCollection").rows(gvr.RowIndex)(("FCAmount")) = txtAmountToPayFC.Text
                End If
            End If
        Next
        lblSubTotal.Text = "0.00"
        lblTotalVATAmount.Text = "0.00"
        lblTotalNETAmount.Text = "0.00"

        Dim dAmount As Decimal = 0, dFCAmount As Decimal = 0, dVATAmount As Decimal = 0, dNETAmount As Decimal = 0
        For i As Integer = 0 To Session("FeeCollection").Rows.Count - 1
            dAmount = dAmount + Session("FeeCollection").Rows(i)("Amount")
            dFCAmount = dFCAmount + Session("FeeCollection").Rows(i)("FCAmount")
            dVATAmount = dVATAmount + Session("FeeCollection").Rows(i)("TAX_AMOUNT")
            dNETAmount = dNETAmount + Session("FeeCollection").Rows(i)("TAX_NET_AMOUNT")
        Next
        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
            'txtReceivedTotal.Text = Format(dFCAmount, Session("BSU_DataFormatString"))
            lblTotalNETAmount.Text = Format(dFCAmount, Session("BSU_DataFormatString"))
        Else
            'txtReceivedTotal.Text = Format(dAmount, Session("BSU_DataFormatString"))
            lblTotalNETAmount.Text = Format(dAmount, Session("BSU_DataFormatString"))
        End If
        lblTotalVATAmount.Text = Format(dVATAmount, Session("BSU_DataFormatString"))
        lblSubTotal.Text = Format(dAmount - dVATAmount, Session("BSU_DataFormatString"))
        gvFeeCollection.DataSource = Session("FeeCollection")
        gvFeeCollection.DataBind()
    End Sub

    Function check_errors_details() As String
        Dim str_error As String = ""

        If txtRemarks.Text.Trim = "" Then
            str_error = str_error & "Enter Remarks <br />"
        End If

        'If txtDAccountCode.Text.Trim = "" Then
        '    str_error = str_error & "Invalid Account Selected <br />"
        'End If

        'If Chk_InvYes.Checked Then
        '    txtDAccountName.Text = AccountFunctions.Validate_Account(txtDAccountCode.Text, Session("sbsuid"), "NOTCC")
        '    If txtDAccountName.Text = "" And ddCollection.SelectedValue <> "9" Then
        '        str_error = str_error & "Invalid Account Selected in Details<br />"
        '    End If
        'End If
        txtDAccountName.Text = ValidateAccount()
        If txtDAccountName.Text = "" And ddCollection.SelectedValue <> "9" Then
            str_error = str_error & "Invalid Account Selected in Details<br />"
        End If

        Return str_error
    End Function
    Function ValidateAccount() As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String = "SELECT ACT_ID,ACT_NAME,ACT_BANKCASH From OASISFIN..ACCOUNTS_M " _
                                 & " WHERE (ACT_Bctrlac = 'FALSE') AND ACT_BACTIVE='TRUE' AND ACT_ID = '" & txtDAccountCode.Text & "' "
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)("ACT_NAME")
        Else
            Return ""
        End If
    End Function

    Protected Sub PrintReceipt(ByVal p_Receiptno As String)
        Dim FC As Boolean = False
        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
            FC = True
        End If
        Session("ReportSource") = FeeCollectionOther.PrintReceipt_DAX(p_Receiptno, Session("sUsr_name"), Session("sBsuid"), _
        ConnectionManger.GetOASIS_FEESConnectionString, FC)
    End Sub

    Protected Sub txtBank_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBank.TextChanged
        Dim str_bankid As String = ""
        Dim str_bankho As String = ""
        txtBank.Text = FeeCommon.GetBankName(txtBank.Text, str_bankid, str_bankho)
        h_Bank.Value = str_bankid
        If h_Bank.Value = "65" Or txtBank.Text = "Bank Transfer" Then
            hf_BNK_TYPE.Value = "BANKTRF"
        Else
            hf_BNK_TYPE.Value = ""
        End If
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If txtBank.Text.Trim <> "" Then
            smScriptManager.SetFocus(ddlEmirate)
            If str_bankho <> "" Then
                ddlEmirate.SelectedIndex = -1
                ddlEmirate.Items.FindByValue(str_bankho).Selected = True
            End If
        Else
            smScriptManager.SetFocus(txtBank)
            '  lblError.Text = "Invalid Bank Selected"
            usrMessageBar.ShowNotification("Invalid Bank Selected", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub

    'Protected Sub ddlPaymentMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Set_CollectionControls()
    'End Sub

    Sub Set_CollectionControls()
        If Not Session("FeeCollection") Is Nothing Then
            Session("FeeCollection").rows.clear()

            Set_GridTotal()
            BindCollectionType()
            txtBank.Text = ""
            hf_BNK_TYPE.Value = ""
            txtBankAct1.Text = ""
            hfBankAct1.Value = ""

            txtCashTotal.Text = "0.00"
            txtChequeTotal.Text = "0.00"
            txtCCTotal.Text = "0.00"
            txtChargeTotal.Text = "0.00"
            'gvFeeCollection.DataBind()
            Select Case rblPaymentModes.SelectedItem.Value
                Case COLLECTIONTYPE.CASH
                    tr_Cash.Visible = True
                    tr_Cheque1.Visible = False
                    tr_Cheque2.Visible = False
                    tr_CreditCard.Visible = False
                    gvFeeCollection.Columns(0).Visible = False
                    gvFeeCollection.Columns(1).Visible = False
                    gvFeeCollection.Columns(3).Visible = False
                    gvFeeCollection.Columns(4).Visible = False
                    gvFeeCollection.Columns(5).Visible = False
                Case COLLECTIONTYPE.CHEQUES
                    tr_Cash.Visible = False
                    tr_Cheque1.Visible = True
                    tr_Cheque2.Visible = True
                    tr_CreditCard.Visible = False
                    gvFeeCollection.Columns(0).Visible = True
                    gvFeeCollection.Columns(1).Visible = True
                    gvFeeCollection.Columns(3).Visible = False
                    gvFeeCollection.Columns(4).Visible = False
                    gvFeeCollection.Columns(5).Visible = False
                Case COLLECTIONTYPE.CREDIT_CARD
                    tr_Cash.Visible = False
                    tr_Cheque1.Visible = False
                    tr_Cheque2.Visible = False
                    tr_CreditCard.Visible = True
                    gvFeeCollection.Columns(0).Visible = True
                    gvFeeCollection.Columns(1).Visible = True
                    gvFeeCollection.Columns(3).Visible = True
                    gvFeeCollection.Columns(4).Visible = True
                    gvFeeCollection.Columns(5).Visible = True
            End Select
            SET_PAYMENT_MODE_AMOUNT()
        End If
    End Sub

    Private Sub GET_TAX_DETAIL(ByVal Amount As Double, ByRef TAX_CODE As String, ByRef TAX_AMOUNT As Double, ByRef NET_AMOUNT As Double, Optional ByVal bIncludingVAT As Boolean = False, Optional ByRef AMOUNT_EX_TAX As Double = 0)
        Dim ACT_ID As String = SqlHelper.ExecuteScalar(ConnectionManger.OASIS_DAX_ConnectionString, CommandType.Text, "SELECT ISNULL(DOA_DAX_ACT_CODE ,'') FROM DAX.DAX_OASIS_ACCOUNTS_MAPPING WITH(NOLOCK) WHERE DOA_OASIS_ACT_ID='" & txtDAccountCode.Text.Trim & "'")
        ACT_ID = ddFeetype.SelectedValue
        If IsNumeric(Amount) AndAlso Not ACT_ID Is Nothing Then
            'SELECT * FROM OASIS.TAX.GetTAXCodeAndAmount('FEES','500600','FEE','1','30/OCT/2017',500,'')
            Dim REF_TYPE As String = ""
            If ddCollection.SelectedValue = "8" Then 'EVENTS/ACTIVITIES
                ACT_ID = ddlEvents.SelectedValue
                REF_TYPE = "FEE" '"EVENT"
            Else
                REF_TYPE = "FEE" '"ACCOUNT"
            End If
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXCodeAndAmount_EXT('FEES','" & Session("sBsuId") & "','" & REF_TYPE & "','" & ACT_ID & "','" & txtFrom.Text.Trim & "'," & Amount & ",''," & IIf(bIncludingVAT, 1, 0) & ")")
            'SELECT * FROM TAX.GetTAXCodeAndAmount_EXT('FEES','" & Session("sBsuId") & "','FEE','" & FEEID & "','" & txDate & "'," & Amount & ",''," & IIf(rblVAT.SelectedValue = 1, 1, 0) & " )
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                TAX_CODE = ds.Tables(0).Rows(0)("TAX_CODE").ToString
                TAX_AMOUNT = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
                NET_AMOUNT = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
                AMOUNT_EX_TAX = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("INV_AMOUNT").ToString), "#,##0.00")
            End If
        Else
            TAX_CODE = ddlTAXCode.SelectedValue
            TAX_AMOUNT = 0
            NET_AMOUNT = IIf(IsNumeric(Amount), Amount, 0)
            AMOUNT_EX_TAX = 0
        End If
    End Sub
    Protected Sub BtnCashAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCash.Click
        lblError.Text = ""

        If Not IsDate(txtFrom.Text) Then
            ' lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dr As DataRow
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If IsNumeric(txtCashTotal.Text) Then
            If CDbl(txtCashTotal.Text) > 0 Then
                If chkBulkUpload.Checked Then
                    If gvError.Rows.Count > 0 Then
                        '  lblError.Text = "Please check and clear the error on uploaded student(s) data"
                        usrMessageBar.ShowNotification("Please check and clear the error on uploaded student(s) data", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    If Not IsNumeric(lblTotalBulkAmount.Text) OrElse CDbl(lblTotalBulkAmount.Text) <= 0 Then
                        ' lblError.Text = "Please upload student(s) data"
                        usrMessageBar.ShowNotification("Please upload student(s) data", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    If ViewState("BulkStudata") Is Nothing OrElse DirectCast(ViewState("BulkStudata"), DataTable).Rows.Count <= 0 Then
                        ' lblError.Text = "Please upload student(s) data"
                        usrMessageBar.ShowNotification("Please upload student(s) data", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    If h_Emp_No.Value = "" Or h_Emp_No.Value = "0" Or txtEmpNo.Text.Trim = "" Then
                        'lblError.Text = "Please choose the staff and continue"
                        usrMessageBar.ShowNotification("Please choose the staff and continue", UserControls_usrMessageBar.WarningType.Danger)
                        txtEmpNo.Focus()
                        Exit Sub
                    End If
                    Dim dtexcel As DataTable = DirectCast(ViewState("BulkStudata"), DataTable)
                    Dim dtc1 As New DataColumn("PayMode", GetType(System.String))
                    Dim dtc2 As New DataColumn("REF_ID", GetType(System.String))
                    Dim dtc3 As New DataColumn("REF_NO", GetType(System.String))
                    Dim dtc4 As New DataColumn("REF_DESCR", GetType(System.String))
                    Dim dtc5 As New DataColumn("EMR_ID", GetType(System.String))
                    Dim dtc6 As New DataColumn("BNK_ACT", GetType(System.String))
                    Dim dtc7 As New DataColumn("Date", GetType(System.String))
                    Dim dtc8 As New DataColumn("FCAmount", GetType(System.Decimal))
                    Dim dtc9 As New DataColumn("CrCharge", GetType(System.Decimal))
                    Dim dtc10 As New DataColumn("TAX_CODE", GetType(System.String))
                    Dim dtc11 As New DataColumn("TAX_AMOUNT", GetType(System.Decimal))
                    Dim dtc12 As New DataColumn("TAX_NET_AMOUNT", GetType(System.Decimal))
                    If Not dtexcel.Columns.Contains(dtc1.ColumnName) Then dtexcel.Columns.Add(dtc1)
                    If Not dtexcel.Columns.Contains(dtc2.ColumnName) Then dtexcel.Columns.Add(dtc2)
                    If Not dtexcel.Columns.Contains(dtc3.ColumnName) Then dtexcel.Columns.Add(dtc3)
                    If Not dtexcel.Columns.Contains(dtc4.ColumnName) Then dtexcel.Columns.Add(dtc4)
                    If Not dtexcel.Columns.Contains(dtc5.ColumnName) Then dtexcel.Columns.Add(dtc5)
                    If Not dtexcel.Columns.Contains(dtc6.ColumnName) Then dtexcel.Columns.Add(dtc6)
                    If Not dtexcel.Columns.Contains(dtc7.ColumnName) Then dtexcel.Columns.Add(dtc7)
                    If Not dtexcel.Columns.Contains(dtc8.ColumnName) Then dtexcel.Columns.Add(dtc8)
                    If Not dtexcel.Columns.Contains(dtc9.ColumnName) Then dtexcel.Columns.Add(dtc9)
                    If Not dtexcel.Columns.Contains(dtc10.ColumnName) Then dtexcel.Columns.Add(dtc10)
                    If Not dtexcel.Columns.Contains(dtc11.ColumnName) Then dtexcel.Columns.Add(dtc11)
                    If Not dtexcel.Columns.Contains(dtc12.ColumnName) Then dtexcel.Columns.Add(dtc12)
                    Dim TAX_CODE As String = ddlTAXCode.SelectedValue, TAX_AMOUNT As Double = 0, TAX_NET_AMOUNT As Double = 0, AMOUNT_EX_TAX As Double = 0, ACTUAL_AMOUNT As Double = 0
                    For Each drr As DataRow In dtexcel.Rows
                        If drr("STU_NO").ToString <> "" AndAlso drr("AMOUNT").ToString <> "" Then
                            ACTUAL_AMOUNT = IIf(Not IsDBNull(drr("TAX_NET_AMOUNT")), drr("TAX_NET_AMOUNT"), drr("AMOUNT"))
                            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount_Ext(" & ACTUAL_AMOUNT & ",'" & TAX_CODE & "',1)")
                            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                                TAX_AMOUNT = Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString)
                                TAX_NET_AMOUNT = Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString)
                                AMOUNT_EX_TAX = TAX_NET_AMOUNT - TAX_AMOUNT
                            End If
                            drr("PayMode") = rblPaymentModes.SelectedValue
                            drr("REF_ID") = ""
                            drr("REF_NO") = ""
                            drr("REF_DESCR") = ""
                            drr("EMR_ID") = ""
                            drr("BNK_ACT") = ""
                            drr("Date") = ""
                            drr("FCAmount") = 0
                            drr("CrCharge") = 0
                            drr("TAX_CODE") = ddlTAXCode.SelectedValue
                            drr("AMOUNT") = AMOUNT_EX_TAX
                            drr("TAX_AMOUNT") = TAX_AMOUNT
                            drr("TAX_NET_AMOUNT") = TAX_NET_AMOUNT
                        End If
                    Next
                    ViewState("BulkStudata") = dtexcel
                End If


                If ViewState("ID") Is Nothing Then
                    ViewState("ID") = 1
                Else
                    ViewState("ID") = ViewState("ID") + 1
                End If

                'If trTAXAmt1.Visible = True AndAlso ddlTAXCode.SelectedValue = "NA" Then
                '    lblError.Text = "Please select a TAX code and continue"
                '    Exit Sub
                'End If

                dr = Session("FeeCollection").NewRow
                dr("ID") = ViewState("ID")
                dr("PayMode") = rblPaymentModes.SelectedValue
                dr("REF_ID") = ""
                dr("REF_NO") = ""
                dr("REF_DESCR") = ""
                dr("EMR_ID") = ""
                dr("BNK_ACT") = ""
                dr("Date") = ""
                dr("Amount") = txtCashTotal.Text
                dr("FCAmount") = 0
                dr("CrCharge") = 0
                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    dr("FCAmount") = txtCashTotal.Text
                    dr("Amount") = Format(txtCashTotal.Text * lblExgRate.Text, Session("BSU_DataFormatString"))
                End If
                dr("TAX_CODE") = ddlTAXCode.SelectedValue
                dr("TAX_AMOUNT") = lblTaxAmount.Text
                dr("TAX_NET_AMOUNT") = lblNetAmount.Text
                DirectCast(Session("FeeCollection"), DataTable).Rows.Clear()
                DirectCast(Session("FeeCollection"), DataTable).AcceptChanges()
                Session("FeeCollection").rows.add(dr)

                Clear_Details()
                Set_GridTotal()
                smScriptManager.SetFocus(txtCashTotal)
            Else
                '  lblError.Text = "Invalid Amount"
                usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
                smScriptManager.SetFocus(txtCashTotal)
            End If
        Else
            ' lblError.Text = "Invalid Amount"
            usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
            smScriptManager.SetFocus(txtCashTotal)
        End If
    End Sub

    Protected Sub btnAddCreditcard_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCreditcard.Click
        lblError.Text = ""

        If Not IsDate(txtFrom.Text) Then
            ' lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If txtCreditno.Text.Trim = "" Then
            'lblError.Text = "Invalid Auth. Code!!!"
            usrMessageBar.ShowNotification("Invalid Auth. Code!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dr As DataRow
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If IsNumeric(txtCCTotal.Text) Then
            If CDbl(txtCCTotal.Text) > 0 Then
                If chkBulkUpload.Checked Then
                    If gvError.Rows.Count > 0 Then
                        '  lblError.Text = "Please check and clear the error on uploaded student(s) data"
                        usrMessageBar.ShowNotification("Please check and clear the error on uploaded student(s) data", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    If Not IsNumeric(lblTotalBulkAmount.Text) OrElse CDbl(lblTotalBulkAmount.Text) <= 0 Then
                        ' lblError.Text = "Please upload student(s) data"
                        usrMessageBar.ShowNotification("Please upload student(s) data", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    If ViewState("BulkStudata") Is Nothing OrElse DirectCast(ViewState("BulkStudata"), DataTable).Rows.Count <= 0 Then
                        'lblError.Text = "Please upload student(s) data"
                        usrMessageBar.ShowNotification("Please upload student(s) data", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    Dim dtexcel As DataTable = DirectCast(ViewState("BulkStudata"), DataTable)
                    Dim dtc1 As New DataColumn("PayMode", GetType(System.String))
                    Dim dtc2 As New DataColumn("REF_ID", GetType(System.String))
                    Dim dtc3 As New DataColumn("REF_NO", GetType(System.String))
                    Dim dtc4 As New DataColumn("REF_DESCR", GetType(System.String))
                    Dim dtc5 As New DataColumn("EMR_ID", GetType(System.String))
                    Dim dtc6 As New DataColumn("BNK_ACT", GetType(System.String))
                    Dim dtc7 As New DataColumn("Date", GetType(System.String))
                    Dim dtc8 As New DataColumn("FCAmount", GetType(System.Decimal))
                    Dim dtc9 As New DataColumn("CrCharge", GetType(System.Decimal))
                    Dim dtc10 As New DataColumn("TAX_CODE", GetType(System.String))
                    Dim dtc11 As New DataColumn("TAX_AMOUNT", GetType(System.Decimal))
                    Dim dtc12 As New DataColumn("TAX_NET_AMOUNT", GetType(System.Decimal))
                    If Not dtexcel.Columns.Contains(dtc1.ColumnName) Then dtexcel.Columns.Add(dtc1)
                    If Not dtexcel.Columns.Contains(dtc2.ColumnName) Then dtexcel.Columns.Add(dtc2)
                    If Not dtexcel.Columns.Contains(dtc3.ColumnName) Then dtexcel.Columns.Add(dtc3)
                    If Not dtexcel.Columns.Contains(dtc4.ColumnName) Then dtexcel.Columns.Add(dtc4)
                    If Not dtexcel.Columns.Contains(dtc5.ColumnName) Then dtexcel.Columns.Add(dtc5)
                    If Not dtexcel.Columns.Contains(dtc6.ColumnName) Then dtexcel.Columns.Add(dtc6)
                    If Not dtexcel.Columns.Contains(dtc7.ColumnName) Then dtexcel.Columns.Add(dtc7)
                    If Not dtexcel.Columns.Contains(dtc8.ColumnName) Then dtexcel.Columns.Add(dtc8)
                    If Not dtexcel.Columns.Contains(dtc9.ColumnName) Then dtexcel.Columns.Add(dtc9)
                    If Not dtexcel.Columns.Contains(dtc10.ColumnName) Then dtexcel.Columns.Add(dtc10)
                    If Not dtexcel.Columns.Contains(dtc11.ColumnName) Then dtexcel.Columns.Add(dtc11)
                    If Not dtexcel.Columns.Contains(dtc12.ColumnName) Then dtexcel.Columns.Add(dtc12)
                    Dim TAX_CODE As String = ddlTAXCode.SelectedValue, TAX_AMOUNT As Double = 0, TAX_NET_AMOUNT As Double = 0, AMOUNT_EX_TAX As Double = 0, ACTUAL_AMOUNT As Double = 0
                    For Each drr As DataRow In dtexcel.Rows
                        If drr("STU_NO").ToString <> "" AndAlso drr("AMOUNT").ToString <> "" Then
                            ACTUAL_AMOUNT = IIf(Not IsDBNull(drr("TAX_NET_AMOUNT")), drr("TAX_NET_AMOUNT"), drr("AMOUNT"))
                            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount_Ext(" & ACTUAL_AMOUNT & ",'" & TAX_CODE & "',1)")
                            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                                TAX_AMOUNT = Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString)
                                TAX_NET_AMOUNT = Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString)
                                AMOUNT_EX_TAX = TAX_NET_AMOUNT - TAX_AMOUNT
                            End If

                            drr("PayMode") = rblPaymentModes.SelectedValue
                            drr("REF_ID") = ddCreditcard.SelectedItem.Value
                            drr("REF_NO") = txtCreditno.Text
                            drr("REF_DESCR") = ddCreditcard.SelectedItem.Text
                            drr("EMR_ID") = ""
                            drr("BNK_ACT") = ""
                            drr("Date") = ""
                            drr("FCAmount") = 0
                            drr("CrCharge") = 0
                            drr("TAX_CODE") = ddlTAXCode.SelectedValue
                            drr("TAX_AMOUNT") = TAX_AMOUNT
                            drr("TAX_NET_AMOUNT") = TAX_NET_AMOUNT
                        End If

                    Next
                    ViewState("BulkStudata") = dtexcel
                End If
                If ViewState("ID") Is Nothing Then
                    ViewState("ID") = 1
                Else
                    ViewState("ID") = ViewState("ID") + 1
                End If
                'If trTAXAmt1.Visible = True AndAlso ddlTAXCode.SelectedValue = "NA" Then
                '    lblError.Text = "Please select a TAX code and continue"
                '    Exit Sub
                'End If
                dr = Session("FeeCollection").NewRow
                dr("ID") = ViewState("ID")
                dr("PayMode") = rblPaymentModes.SelectedValue
                dr("REF_ID") = ddCreditcard.SelectedItem.Value
                dr("REF_NO") = txtCreditno.Text
                dr("REF_DESCR") = ddCreditcard.SelectedItem.Text
                dr("EMR_ID") = ""
                dr("BNK_ACT") = ""
                dr("Date") = ""
                dr("CrAmount") = CDbl(Me.txtCCTotal.Text)
                dr("CrCharge") = CDbl(Me.txtCrCharge.Text)
                dr("CrTotal") = CDbl(Me.txtChargeTotal.Text)
                dr("Amount") = CDbl(Me.txtChargeTotal.Text)
                dr("FCAmount") = 0
                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    dr("FCAmount") = CDbl(Me.txtChargeTotal.Text)
                    dr("Amount") = Format(CDbl(Me.txtChargeTotal.Text) * CDbl(lblExgRate.Text), Session("BSU_DataFormatString"))
                End If
                dr("TAX_CODE") = ddlTAXCode.SelectedValue
                dr("TAX_AMOUNT") = lblTaxAmount.Text
                dr("TAX_NET_AMOUNT") = lblNetAmount.Text
                Session("FeeCollection").rows.add(dr)
                Clear_Details()
                Set_GridTotal()
                smScriptManager.SetFocus(txtCCTotal)
            Else
                ' lblError.Text = "Invalid Amount"
                usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
                smScriptManager.SetFocus(txtCCTotal)
            End If
        Else
            ' lblError.Text = "Invalid Amount"
            usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
            smScriptManager.SetFocus(txtCashTotal)
        End If
    End Sub

    Protected Sub btnAddCheque_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        If Not IsDate(txtFrom.Text) Then
            '  lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If txtChqno.Text.Trim = "" Then
            '  lblError.Text = "Invalid Cheque No!!!"
            usrMessageBar.ShowNotification("Invalid Cheque No!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If txtBank.Text.Trim = "" Or h_Bank.Value.Trim = "" Then
            ' lblError.Text = "Invalid Bank!!!"
            usrMessageBar.ShowNotification("Invalid Bank!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If hf_BNK_TYPE.Value = "BANKTRF" AndAlso hfBankAct1.Value = "" Then
            '  lblError.Text = "Please select a Bank Account for Bank Transfers"
            usrMessageBar.ShowNotification("Please select a Bank Account for Bank Transfers", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Not IsDate(txtChqDate.Text) Then
            ' lblError.Text = "Invalid Cheque Date!!!"
            usrMessageBar.ShowNotification("Invalid Cheque Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If hf_BNK_TYPE.Value = "BANKTRF" AndAlso CDate(txtChqDate.Text) > CDate(Date.Now.ToShortDateString) Then
            '  lblError.Text = "Post dated cheques are not allowed for Bank Transfers"
            usrMessageBar.ShowNotification("Post dated cheques are not allowed for Bank Transfers", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dr As DataRow
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If IsNumeric(txtChequeTotal.Text) Then
            If CDbl(txtChequeTotal.Text) > 0 Then
                If chkBulkUpload.Checked Then
                    If gvError.Rows.Count > 0 Then
                        '  lblError.Text = "Please check and clear the error on uploaded student(s) data"
                        usrMessageBar.ShowNotification("Please check and clear the error on uploaded student(s) data", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    If Not IsNumeric(lblTotalBulkAmount.Text) OrElse CDbl(lblTotalBulkAmount.Text) <= 0 Then
                        ' lblError.Text = "Please upload student(s) data"
                        usrMessageBar.ShowNotification("Please upload student(s) data", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    If ViewState("BulkStudata") Is Nothing OrElse DirectCast(ViewState("BulkStudata"), DataTable).Rows.Count <= 0 Then
                        '   lblError.Text = "Please upload student(s) data"
                        usrMessageBar.ShowNotification("Please upload student(s) data", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    Dim dtexcel As DataTable = DirectCast(ViewState("BulkStudata"), DataTable)
                    Dim dtc1 As New DataColumn("PayMode", GetType(System.String))
                    Dim dtc2 As New DataColumn("REF_ID", GetType(System.String))
                    Dim dtc3 As New DataColumn("REF_NO", GetType(System.String))
                    Dim dtc4 As New DataColumn("REF_DESCR", GetType(System.String))
                    Dim dtc5 As New DataColumn("EMR_ID", GetType(System.String))
                    Dim dtc6 As New DataColumn("BNK_ACT", GetType(System.String))
                    Dim dtc7 As New DataColumn("Date", GetType(System.String))
                    Dim dtc8 As New DataColumn("FCAmount", GetType(System.Decimal))
                    Dim dtc9 As New DataColumn("CrCharge", GetType(System.Decimal))
                    Dim dtc10 As New DataColumn("TAX_CODE", GetType(System.String))
                    If Not dtexcel.Columns.Contains(dtc1.ColumnName) Then dtexcel.Columns.Add(dtc1)
                    If Not dtexcel.Columns.Contains(dtc2.ColumnName) Then dtexcel.Columns.Add(dtc2)
                    If Not dtexcel.Columns.Contains(dtc3.ColumnName) Then dtexcel.Columns.Add(dtc3)
                    If Not dtexcel.Columns.Contains(dtc4.ColumnName) Then dtexcel.Columns.Add(dtc4)
                    If Not dtexcel.Columns.Contains(dtc5.ColumnName) Then dtexcel.Columns.Add(dtc5)
                    If Not dtexcel.Columns.Contains(dtc6.ColumnName) Then dtexcel.Columns.Add(dtc6)
                    If Not dtexcel.Columns.Contains(dtc7.ColumnName) Then dtexcel.Columns.Add(dtc7)
                    If Not dtexcel.Columns.Contains(dtc8.ColumnName) Then dtexcel.Columns.Add(dtc8)
                    If Not dtexcel.Columns.Contains(dtc9.ColumnName) Then dtexcel.Columns.Add(dtc9)
                    If Not dtexcel.Columns.Contains(dtc10.ColumnName) Then dtexcel.Columns.Add(dtc10)
                    For Each drr As DataRow In dtexcel.Rows
                        drr("PayMode") = rblPaymentModes.SelectedValue
                        drr("REF_ID") = h_Bank.Value
                        drr("REF_NO") = txtChqno.Text
                        drr("REF_DESCR") = txtBank.Text
                        drr("EMR_ID") = ddlEmirate.SelectedItem.Value
                        drr("BNK_ACT") = hfBankAct1.Value
                        drr("Date") = txtChqDate.Text
                        drr("FCAmount") = 0
                        drr("CrCharge") = 0
                        drr("TAX_CODE") = ddlTAXCode.SelectedValue
                    Next
                    ViewState("BulkStudata") = dtexcel
                End If

                If ViewState("ID") Is Nothing Then
                    ViewState("ID") = 1
                Else
                    ViewState("ID") = ViewState("ID") + 1
                End If
                'If trTAXAmt1.Visible = True AndAlso ddlTAXCode.SelectedValue = "NA" Then
                '    lblError.Text = "Please select a TAX code and continue"
                '    Exit Sub
                'End If
                dr = Session("FeeCollection").NewRow
                dr("ID") = ViewState("ID")
                dr("PayMode") = rblPaymentModes.SelectedValue
                dr("REF_ID") = h_Bank.Value
                dr("REF_NO") = txtChqno.Text
                dr("REF_DESCR") = txtBank.Text
                dr("BNK_ACT") = hfBankAct1.Value
                dr("EMR_ID") = ddlEmirate.SelectedItem.Value
                dr("Date") = txtChqDate.Text
                dr("Amount") = txtChequeTotal.Text
                dr("FCAmount") = 0
                dr("CrCharge") = 0
                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    dr("FCAmount") = txtChequeTotal.Text
                    dr("Amount") = Format(txtChequeTotal.Text * lblExgRate.Text, Session("BSU_DataFormatString"))
                End If
                dr("TAX_CODE") = ddlTAXCode.SelectedValue
                dr("TAX_AMOUNT") = lblTaxAmount.Text
                dr("TAX_NET_AMOUNT") = lblNetAmount.Text
                Session("FeeCollection").rows.add(dr)
                Clear_Details()
                Set_GridTotal()
                smScriptManager.SetFocus(txtChequeTotal)
            Else
                '  lblError.Text = "Invalid Amount"
                usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
                smScriptManager.SetFocus(txtChequeTotal)
            End If
        Else
            '  lblError.Text = "Invalid Amount"
            usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
            smScriptManager.SetFocus(txtChequeTotal)
        End If
    End Sub
    Protected Sub txtAmountToPay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal()
    End Sub
    Protected Sub txtAmountToPayFC_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal()
    End Sub
    Protected Sub gvFeeCollection_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvFeeCollection.RowCancelingEdit
        gvFeeCollection.EditIndex = -1
        gvFeeCollection.DataSource = Session("FeeCollection")
        gvFeeCollection.DataBind()
        Select Case rblPaymentModes.SelectedItem.Value
            Case COLLECTIONTYPE.CASH
                btnAddCash.Visible = True
            Case COLLECTIONTYPE.CHEQUES
                btnAddCheque.Visible = True
            Case COLLECTIONTYPE.CREDIT_CARD
                btnAddCreditcard.Visible = True
        End Select
    End Sub

    Protected Sub gvFeeCollection_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvFeeCollection.RowEditing
        gvFeeCollection.EditIndex = e.NewEditIndex
        Dim row As GridViewRow = gvFeeCollection.Rows(e.NewEditIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("FeeCollection").Rows.Count - 1
            If iIndex = Session("FeeCollection").Rows(iEdit)(0) Then
                '"  "REF_ID" "REF_NO", "EMR_ID" "Date", "Amount",
                Select Case rblPaymentModes.SelectedItem.Value
                    Case COLLECTIONTYPE.CASH
                        btnAddCash.Visible = False
                        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                            txtCashTotal.Text = Session("FeeCollection").Rows(iEdit)("FCAmount")
                        Else
                            txtCashTotal.Text = Session("FeeCollection").Rows(iEdit)("Amount")
                        End If

                    Case COLLECTIONTYPE.CHEQUES
                        btnAddCheque.Visible = False
                        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                            txtChequeTotal.Text = Session("FeeCollection").Rows(iEdit)("FCAmount")
                        Else
                            txtChequeTotal.Text = Session("FeeCollection").Rows(iEdit)("Amount")
                        End If

                        h_Bank.Value = Session("FeeCollection").Rows(iEdit)("REF_ID")
                        txtBank.Text = Session("FeeCollection").Rows(iEdit)("REF_DESCR")
                        txtChqDate.Text = Session("FeeCollection").Rows(iEdit)("Date")
                        txtChqno.Text = Session("FeeCollection").Rows(iEdit)("REF_NO")
                        hfBankAct1.Value = Session("FeeCollection").Rows(iEdit)("BNK_ACT")
                        txtBankAct1.Text = hfBankAct1.Value 'GetAccountDescr(hfBankAct1.Value)
                        If h_Bank.Value = "65" Or txtBank.Text = "Bank Transfer" Then
                            hf_BNK_TYPE.Value = "BANKTRF"
                        Else
                            hf_BNK_TYPE.Value = ""
                        End If
                        ddlEmirate.SelectedIndex = -1
                        ddlEmirate.Items.FindByValue(Session("FeeCollection").Rows(iEdit)("EMR_ID")).Selected = True
                    Case COLLECTIONTYPE.CREDIT_CARD
                        btnAddCreditcard.Visible = False
                        If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                            txtCCTotal.Text = Session("FeeCollection").Rows(iEdit)("FCAmount")

                        Else
                            txtChargeTotal.Text = Session("FeeCollection").Rows(iEdit)("Amount")
                            txtCCTotal.Text = Session("FeeCollection").Rows(iEdit)("CrAmount")
                            txtCrCharge.Text = Session("FeeCollection").Rows(iEdit)("CrCharge")

                        End If
                        ddCreditcard.SelectedIndex = -1
                        ddCreditcard.Items.FindByValue(Session("FeeCollection").Rows(iEdit)("REF_ID")).Selected = True
                        txtCreditno.Text = Session("FeeCollection").Rows(iEdit)("REF_NO")
                End Select
                Exit For
            End If
        Next
        gvFeeCollection.DataSource = Session("FeeCollection")
        gvFeeCollection.DataBind()
    End Sub

    Protected Sub gvFeeCollection_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvFeeCollection.RowUpdating
        Dim row As GridViewRow = gvFeeCollection.Rows(gvFeeCollection.EditIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("FeeCollection").Rows.Count - 1
            If iIndex = Session("FeeCollection").Rows(iEdit)(0) Then
                '"  "REF_ID" "REF_NO", "EMR_ID" "Date", "Amount",
                Select Case rblPaymentModes.SelectedItem.Value
                    Case COLLECTIONTYPE.CASH
                        If IsNumeric(txtCashTotal.Text) AndAlso CDbl(txtCashTotal.Text) > 0 Then
                            btnAddCash.Visible = True
                            If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                                Session("FeeCollection").Rows(iEdit)("FCAmount") = txtCashTotal.Text
                                Session("FeeCollection").Rows(iEdit)("Amount") = Format(txtCashTotal.Text * lblExgRate.Text, Session("BSU_DataFormatString"))
                            Else
                                Session("FeeCollection").Rows(iEdit)("FCAmount") = 0
                                Session("FeeCollection").Rows(iEdit)("Amount") = txtCashTotal.Text
                            End If
                        Else
                            ' lblError.Text = "Invalid Amount!!!"
                            usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                    Case COLLECTIONTYPE.CHEQUES
                        If txtChqno.Text.Trim = "" Then
                            ' lblError.Text = "Invalid Cheque No!!!"
                            usrMessageBar.ShowNotification("Invalid Cheque No!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                        If txtBank.Text.Trim = "" Or h_Bank.Value.Trim = "" Then
                            '  lblError.Text = "Invalid Bank!!!"
                            usrMessageBar.ShowNotification("Invalid Bank!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                        If Not IsDate(txtChqDate.Text) Then
                            '  lblError.Text = "Invalid Cheque Date!!!"
                            usrMessageBar.ShowNotification("Invalid Cheque Date!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                        If IsNumeric(txtChequeTotal.Text) AndAlso CDbl(txtChequeTotal.Text) > 0 Then
                            btnAddCheque.Visible = True
                            If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                                Session("FeeCollection").Rows(iEdit)("FCAmount") = txtChequeTotal.Text
                                Session("FeeCollection").Rows(iEdit)("Amount") = Format(txtChequeTotal.Text * lblExgRate.Text, Session("BSU_DataFormatString"))
                            Else
                                Session("FeeCollection").Rows(iEdit)("FCAmount") = 0
                                Session("FeeCollection").Rows(iEdit)("Amount") = txtChequeTotal.Text
                            End If
                            'Session("FeeCollection").Rows(iEdit)("Amount") = txtChequeTotal.Text
                            Session("FeeCollection").Rows(iEdit)("REF_ID") = h_Bank.Value
                            Session("FeeCollection").Rows(iEdit)("REF_DESCR") = txtBank.Text
                            Session("FeeCollection").Rows(iEdit)("BNK_ACT") = hfBankAct1.Value
                            Session("FeeCollection").Rows(iEdit)("Date") = txtChqDate.Text
                            Session("FeeCollection").Rows(iEdit)("REF_NO") = txtChqno.Text
                            Session("FeeCollection").Rows(iEdit)("EMR_ID") = ddlEmirate.SelectedItem.Value
                        Else
                            ' lblError.Text = "Invalid Amount!!!"
                            usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                    Case COLLECTIONTYPE.CREDIT_CARD
                        If txtCreditno.Text.Trim = "" Then
                            '  lblError.Text = "Invalid Auth. Code!!!"
                            usrMessageBar.ShowNotification("Invalid Auth. Code!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                        If IsNumeric(txtCCTotal.Text) AndAlso CDbl(txtCCTotal.Text) > 0 Then
                            btnAddCreditcard.Visible = True
                            If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                                Session("FeeCollection").Rows(iEdit)("FCAmount") = txtCCTotal.Text
                                Session("FeeCollection").Rows(iEdit)("Amount") = Format(txtCCTotal.Text * lblExgRate.Text, Session("BSU_DataFormatString"))
                            Else
                                Session("FeeCollection").Rows(iEdit)("FCAmount") = 0
                                Session("FeeCollection").Rows(iEdit)("Amount") = txtCCTotal.Text
                            End If
                            'Session("FeeCollection").Rows(iEdit)("Amount") = txtCCTotal.Text
                            Session("FeeCollection").Rows(iEdit)("REF_ID") = ddCreditcard.SelectedItem.Value
                            Session("FeeCollection").Rows(iEdit)("REF_NO") = txtCreditno.Text
                        Else
                            '  lblError.Text = "Invalid Amount!!!"
                            usrMessageBar.ShowNotification("Invalid Amount!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                End Select
                Exit For
            End If
        Next
        Clear_Details()
        gvFeeCollection.EditIndex = -1
        gvFeeCollection.DataSource = Session("FeeCollection")
        gvFeeCollection.DataBind()
    End Sub

    Protected Sub txtDAccountCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        chk_DetailsAccount()
        Set_CollectionControls()
        If Not IsNumeric(txtAmount.Text) Then
            txtAmount.Text = "0.00"
        End If
        If IsNumeric(txtAmount.Text) Then
            Dim TAX_CODE As String = "", TAX_AMOUNT As Double = 0, TAX_NET_AMOUNT As Double = 0
            GET_TAX_DETAIL(txtAmount.Text, TAX_CODE, TAX_AMOUNT, TAX_NET_AMOUNT)
            ddlTAXCode.SelectedValue = TAX_CODE
            ddlTAXCode_SelectedIndexChanged(Nothing, Nothing)
            'lblTaxAmount.Text = Format(TAX_AMOUNT, "#,##0.00")
            'lblNetAmount.Text = Format(TAX_NET_AMOUNT, "#,##0.00")
            SET_PAYMENT_MODE_AMOUNT()
        End If
    End Sub

    Sub chk_DetailsAccount()
        txtDAccountName.Text = FeeCollectionOther.Validate_Account(txtDAccountCode.Text, Session("sbsuid"), ddCollection.SelectedValue)
        If txtDAccountName.Text = "" Then
            '   lblError.Text = "Invalid Account Selected in Details"
            usrMessageBar.ShowNotification("Invalid Account Selected in Details", UserControls_usrMessageBar.WarningType.Danger)
            txtDAccountCode.Focus()
        Else
            ShowEventAndCashFlow()
        End If
    End Sub
    Sub ShowEventAndCashFlow()


        Dim Qry As String = "SELECT ISNULL(OFM_bEventRequired,0)OFM_bEventRequired,ISNULL(OFM_bCashFlowRequired,0)OFM_bCashFlowRequired, " & _
            "ISNULL(OFM_DefaultEvent,'')OFM_DefaultEvent,ISNULL(OFM_DefaultCashFlow,'')OFM_DefaultCashFlow " & _
            "FROM DAX.OTH_FEECOLLECTION_MAPPING WITH(NOLOCK) WHERE OFM_ID=" & hf_OFM_ID.Value & ""
        If ddCollection.SelectedValue = "9" Then 'Vendor
            Qry = "SELECT TOP 1 ISNULL(OFM_bEventRequired,0)OFM_bEventRequired,ISNULL(OFM_bCashFlowRequired,0)OFM_bCashFlowRequired, " & _
            "ISNULL(OFM_DefaultEvent,'')OFM_DefaultEvent,ISNULL(OFM_DefaultCashFlow,'')OFM_DefaultCashFlow " & _
            "FROM DAX.OTH_FEECOLLECTION_MAPPING WITH(NOLOCK) WHERE OFM_COL_ID=" & ddCollection.SelectedValue & " ORDER BY OFM_ID DESC"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, Qry)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            If CBool(ds.Tables(0).Rows(0)("OFM_bEventRequired")) Then
                FillEvents(ds.Tables(0).Rows(0)("OFM_DefaultEvent").ToString)
            Else
                trEvents.Visible = False
            End If
            If CBool(ds.Tables(0).Rows(0)("OFM_bCashFlowRequired")) Then
                FillCashFlow(ds.Tables(0).Rows(0)("OFM_DefaultCashFlow").ToString)
            Else
                trCashFlow.Visible = False
            End If
        End If
    End Sub
    Protected Sub ddCollection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddCollection.SelectedIndexChanged
        'Changes on 12DEC2019
      
        If Convert.ToBoolean(Session("BSU_bVATEnabled")) Then
            txtDAccountCode.Text = ""
            txtDAccountName.Text = ""
            If Chk_InvYes.Checked Or Chk_InvNo.Checked Then
                If Chk_InvNo.Checked Then
                    ddFeetype.Enabled = False
                Else
                    ddFeetype.Enabled = True
                End If


                If (ddlCustomers.SelectedItem.Value <> "" And ddlCustomers.SelectedItem.Value <> "0") Or ddCollection.SelectedValue = "9" Then
                    FillFeeType()
                    ddFeetype.Enabled = True
                    ddFeetype_SelectedIndexChanged(Nothing, Nothing)
                Else
                    ddFeetype.Items.Clear()
                    ddFeetype.DataSource = Nothing
                    ddFeetype.DataBind()

                    If ddEventtype.Visible Then
                        ddEventtype.Items.Clear()
                        ddEventtype.DataSource = Nothing
                        ddEventtype.DataBind()
                        ddEventtype.Visible = False
                        lbleventtype.Visible = False
                        lbleventid.Visible = False
                    End If
                End If
            End If
        Else
            FillFeeType()
            ddFeetype_SelectedIndexChanged(Nothing, Nothing)
        End If
        'Changes ends on 12DEC2019
        If ddCollection.SelectedValue = "9" Then 'Employee
            trCustomer.Visible = False
            trVendor.Visible = True
            trAccount.Visible = False
            trEvents.Visible = False
            trCashFlow.Visible = False
            hf_OFM_ID.Value = ""
            txtDAccountCode.Text = ""
            txtDAccountName.Text = ""
            txtRemarks.Text = ddCollection.SelectedItem.Text.ToString() & " fee collection For : " & txtFrom.Text.ToString()
            ShowEventAndCashFlow()
        Else
            hf_VDR_CODE.Value = "0"
            trCustomer.Visible = Convert.ToBoolean(Session("BSU_bVATEnabled")) 'enable only if it is a VAT enabled unit
            txtVendorCode.Text = ""
            txtVendorDescr.Text = ""
            trVendor.Visible = False
            trAccount.Visible = True
            Dim str_collection As String = FeeCollectionOther.get_CollectionAccount(ddCollection.SelectedItem.Value, Session("sBsuid"))
            Try
                If Convert.ToBoolean(Session("BSU_bVATEnabled")) Then
                    txtDAccountCode.Text = str_collection.Split("|")(0)
                    txtDAccountName.Text = str_collection.Split("|")(1)
                End If
                hf_OFM_ID.Value = str_collection.Split("|")(2)
                txtRemarks.Text = ddCollection.SelectedItem.Text.ToString() & "  For : " & txtFrom.Text.ToString()
                txtDAccountCode_TextChanged(Nothing, Nothing)
            Catch ex As Exception
            End Try
            If ddCollection.SelectedValue = "4" Then 'if TEXT BKS & STY. COLLECTION enable TAX code selection
                ddlTAXCode.Enabled = True
            Else
                SET_VAT_DROPDOWN_RIGHT()
            End If
        End If

        'Changes on 12DEC2019
        If Convert.ToBoolean(Session("BSU_bVATEnabled")) Then
            txtDAccountCode.Text = ""
            txtDAccountName.Text = ""
        End If
        'If ddlCustomers.SelectedItem.Value <> "" And ddlCustomers.SelectedItem.Value <> "0" Then
        '    FillFeeType()
        '    ddFeetype_SelectedIndexChanged(Nothing, Nothing)
        'Else
        '    ddFeetype.Items.Clear()
        '    ddFeetype.DataSource = Nothing
        '    ddFeetype.DataBind()
        'End If
        'Changes ends on 12DEC2019

        Set_CollectionControls()
    End Sub

    Protected Sub gvFeeCollection_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFeeCollection.RowDataBound
        Try
            Dim txtAmountToPay As TextBox = CType(e.Row.FindControl("txtAmountToPay"), TextBox)
            Dim txtAmountToPayFC As TextBox = CType(e.Row.FindControl("txtAmountToPayFC"), TextBox)
            If txtAmountToPay IsNot Nothing And txtAmountToPayFC IsNot Nothing Then

                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    txtAmountToPay.Text = Format(txtAmountToPayFC.Text * lblExgRate.Text, Session("BSU_DataFormatString"))
                    txtAmountToPay.Enabled = False
                Else
                    txtAmountToPay.Enabled = True
                    'txtAmountToPayFC.Text = Format(txtAmountToPay.Text, Session("BSU_DataFormatString"))
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim gvr As GridViewRow = DirectCast(DirectCast(sender, LinkButton).Parent.Parent, GridViewRow)
        Dim RowIndex As Integer = gvr.RowIndex
        Dim PayMode As Int16 = gvFeeCollection.DataKeys(RowIndex).Item("PayMode")
        Dim dtFeeCollection As DataTable = DirectCast(Session("FeeCollection"), DataTable)
        Select Case PayMode
            Case COLLECTIONTYPE.CASH
                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    txtCashTotal.Text = Format(dtFeeCollection.Rows(RowIndex)("FCAmount"), Session("BSU_DataFormatString"))
                Else
                    txtCashTotal.Text = Format(dtFeeCollection.Rows(RowIndex)("Amount"), Session("BSU_DataFormatString"))
                End If
                ddlTAXCode.SelectedValue = dtFeeCollection.Rows(RowIndex)("TAX_CODE").ToString
                lblTaxAmount.Text = Format(dtFeeCollection.Rows(RowIndex)("TAX_AMOUNT"), Session("BSU_DataFormatString"))
                lblNetAmount.Text = Format(dtFeeCollection.Rows(RowIndex)("TAX_NET_AMOUNT"), Session("BSU_DataFormatString"))
                txtAmount.Text = Format(Convert.ToDouble(lblNetAmount.Text) - Convert.ToDouble(lblTaxAmount.Text), Session("BSU_DataFormatString"))
            Case COLLECTIONTYPE.CHEQUES
                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    txtChequeTotal.Text = Format(dtFeeCollection.Rows(RowIndex)("FCAmount"), Session("BSU_DataFormatString"))
                Else
                    txtChequeTotal.Text = Format(dtFeeCollection.Rows(RowIndex)("Amount"), Session("BSU_DataFormatString"))
                End If

                h_Bank.Value = dtFeeCollection.Rows(RowIndex)("REF_ID")
                hfBankAct1.Value = dtFeeCollection.Rows(RowIndex)("BNK_ACT")
                txtBankAct1.Text = hfBankAct1.Value 'GetAccountDescr(hfBankAct1.Value)
                txtBank.Text = dtFeeCollection.Rows(RowIndex)("REF_DESCR")
                txtChqDate.Text = dtFeeCollection.Rows(RowIndex)("Date")
                txtChqno.Text = dtFeeCollection.Rows(RowIndex)("REF_NO")
                ddlEmirate.SelectedIndex = -1
                ddlEmirate.Items.FindByValue(dtFeeCollection.Rows(RowIndex)("EMR_ID")).Selected = True
                ddlTAXCode.SelectedValue = dtFeeCollection.Rows(RowIndex)("TAX_CODE").ToString
                lblTaxAmount.Text = Format(dtFeeCollection.Rows(RowIndex)("TAX_AMOUNT"), Session("BSU_DataFormatString"))
                lblNetAmount.Text = Format(dtFeeCollection.Rows(RowIndex)("TAX_NET_AMOUNT"), Session("BSU_DataFormatString"))
                txtAmount.Text = Format(Convert.ToDouble(lblNetAmount.Text) - Convert.ToDouble(lblTaxAmount.Text), Session("BSU_DataFormatString"))
            Case COLLECTIONTYPE.CREDIT_CARD
                If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                    txtChargeTotal.Text = Format(dtFeeCollection.Rows(RowIndex)("FCAmount"), Session("BSU_DataFormatString"))
                    txtCCTotal.Text = Format(dtFeeCollection.Rows(RowIndex)("CrAmount"), Session("BSU_DataFormatString"))
                    txtCrCharge.Text = Format(dtFeeCollection.Rows(RowIndex)("CrCharge"), Session("BSU_DataFormatString"))
                    'txtReceivedTotal.Text = txtChargeTotal.Text
                    lblTotalNETAmount.Text = txtChargeTotal.Text
                Else
                    txtChargeTotal.Text = Format(dtFeeCollection.Rows(RowIndex)("Amount"), Session("BSU_DataFormatString"))
                    txtCCTotal.Text = Format(dtFeeCollection.Rows(RowIndex)("CrAmount"), Session("BSU_DataFormatString"))
                    txtCrCharge.Text = Format(dtFeeCollection.Rows(RowIndex)("CrCharge"), Session("BSU_DataFormatString"))
                    'txtReceivedTotal.Text = txtChargeTotal.Text
                    lblTotalNETAmount.Text = txtChargeTotal.Text
                End If
                ddCreditcard.SelectedIndex = -1
                ddCreditcard.Items.FindByValue(dtFeeCollection.Rows(RowIndex)("REF_ID")).Selected = True
                txtCreditno.Text = dtFeeCollection.Rows(RowIndex)("REF_NO")
                ddlTAXCode.SelectedValue = dtFeeCollection.Rows(RowIndex)("TAX_CODE").ToString
                lblTaxAmount.Text = Format(dtFeeCollection.Rows(RowIndex)("TAX_AMOUNT"), Session("BSU_DataFormatString"))
                lblNetAmount.Text = Format(dtFeeCollection.Rows(RowIndex)("TAX_NET_AMOUNT"), Session("BSU_DataFormatString"))
                txtAmount.Text = Format(Convert.ToDouble(lblNetAmount.Text) - Convert.ToDouble(lblTaxAmount.Text), Session("BSU_DataFormatString"))
        End Select
        DirectCast(dtFeeCollection, DataTable).Rows(RowIndex).Delete()
        DirectCast(dtFeeCollection, DataTable).AcceptChanges()
        Session("FeeCollection") = dtFeeCollection
        Set_GridTotal()

    End Sub

    Protected Sub txtEmpNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_CollectionControls()
    End Sub

    Protected Sub ddlEvents_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlEvents.SelectedIndexChanged
        Set_CollectionControls()
        Dim TAX_CODE As String = "", TAX_AMOUNT As Double = 0, TAX_NET_AMOUNT As Double = 0
        GET_TAX_DETAIL(txtAmount.Text, TAX_CODE, TAX_AMOUNT, TAX_NET_AMOUNT)
        ddlTAXCode.SelectedValue = TAX_CODE
        ddlTAXCode_SelectedIndexChanged(Nothing, Nothing)
        'lblTaxAmount.Text = Format(TAX_AMOUNT, "#,##0.00")
        'lblNetAmount.Text = Format(TAX_NET_AMOUNT, "#,##0.00")
        SET_PAYMENT_MODE_AMOUNT()
        If bMUSIC Then
            txtRemarks.Text = "StudentName: " & Environment.NewLine & "|StudentAge: " & Environment.NewLine & "|StudentGrade: " & Environment.NewLine & "|ProgrammeName: " & Environment.NewLine & "|Provider: " & Environment.NewLine & "|SchoolName: " & Environment.NewLine & "|ContactName: " & Environment.NewLine & "|EmailId: " & Environment.NewLine & "|" & ddCollection.SelectedItem.Text & " For : " & txtFrom.Text
        Else
            txtRemarks.Text = ddCollection.SelectedItem.Text & " For : " & txtFrom.Text
        End If

    End Sub

    Protected Sub ddlCashFlow_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCashFlow.SelectedIndexChanged
        Set_CollectionControls()
    End Sub

    Protected Sub txtAmount_TextChanged(sender As Object, e As EventArgs) Handles txtAmount.TextChanged
        If IsNumeric(txtAmount.Text) Then
            Dim TAX_CODE As String = "", TAX_AMOUNT As Double = 0, TAX_NET_AMOUNT As Double = 0
            'GET_TAX_DETAIL(txtAmount.Text, TAX_CODE, TAX_AMOUNT, TAX_NET_AMOUNT, rblTaxCalculation.SelectedValue = "I")
            'ddlTAXCode.SelectedValue = TAX_CODE
            'ddlTAXCode_SelectedIndexChanged(Nothing, Nothing)
            'lblTaxAmount.Text = Format(TAX_AMOUNT, "#,##0.00")
            'lblNetAmount.Text = Format(TAX_NET_AMOUNT, "#,##0.00")
            CALCULATE_TAX(txtAmount.Text, rblTaxCalculation.SelectedValue = "I")
            txtAmount.Text = CDbl(lblNetAmount.Text) - CDbl(lblTaxAmount.Text)
            SET_PAYMENT_MODE_AMOUNT()
        End If
    End Sub

    Protected Sub ddlTAXCode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTAXCode.SelectedIndexChanged
        'If chkBulkUpload.Checked Then
        '    Dim TAX_CODE As String = ddlTAXCode.SelectedValue, TAX_AMOUNT As Double = 0, TAX_NET_AMOUNT As Double = 0, AMOUNT_EX_TAX As Double = 0

        '    GET_TAX_DETAIL(lblTotalBulkAmount.Text, TAX_CODE, TAX_AMOUNT, TAX_NET_AMOUNT, True, AMOUNT_EX_TAX)
        '    If TAX_CODE <> ddlTAXCode.SelectedValue Then 'tax code will be different in cases when as per calculation and user selection are different
        '        CALCULATE_TAX(TAX_NET_AMOUNT, True)
        '        txtAmount.Text = CDbl(lblNetAmount.Text) - CDbl(lblTaxAmount.Text)
        '    Else
        '        txtAmount.Text = AMOUNT_EX_TAX
        '    End If
        '    'If TAX_CODE = "VAT5" Then
        '    'Else
        '    '    txtAmount.Text = lblTotalBulkAmount.Text
        '    'End If
        'End If
        'CALCULATE_TAX(txtAmount.Text)
        ddlTAXSelChanged()
    End Sub

    Private Sub ddlTAXSelChanged()
        If chkBulkUpload.Checked Then
            'Dim TAX_CODE As String = ddlTAXCode.SelectedValue, TAX_AMOUNT As Double = 0, TAX_NET_AMOUNT As Double = 0, AMOUNT_EX_TAX As Double = 0
            'lblTotalBulkAmount.Text = FeeCollection.GetDoubleVal(lblTotalBulkAmount.Text)
            'GET_TAX_DETAIL(lblTotalBulkAmount.Text, TAX_CODE, TAX_AMOUNT, TAX_NET_AMOUNT, True, AMOUNT_EX_TAX)
            'If TAX_CODE <> ddlTAXCode.SelectedValue Then 'tax code will be different in cases when as per calculation and user selection are different
            '    CALCULATE_TAX(TAX_NET_AMOUNT, True)
            '    txtAmount.Text = CDbl(lblNetAmount.Text) - CDbl(lblTaxAmount.Text)
            'Else
            '    txtAmount.Text = AMOUNT_EX_TAX
            'End If
            ' ''If TAX_CODE = "VAT5" Then
            ' ''Else
            ' ''    txtAmount.Text = lblTotalBulkAmount.Text
            ' ''End If
        End If
        CALCULATE_TAX(txtAmount.Text)
    End Sub
    Private Sub CALCULATE_TAX(ByVal Amount As Double, Optional ByVal bIncludingTax As Boolean = False)
        lblTaxAmount.Text = "0.00"
        lblNetAmount.Text = "0.00"
        If IsNumeric(Amount) Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount_Ext(" & Amount & ",'" & ddlTAXCode.SelectedValue & "'," & IIf(bIncludingTax, 1, 0) & ")")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblTaxAmount.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
                lblNetAmount.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
                If bIncludingTax Then
                    txtAmount.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00") - Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")

                End If
                SET_PAYMENT_MODE_AMOUNT()
            End If
        End If
    End Sub
    Private Sub SET_PAYMENT_MODE_AMOUNT()
        If rblPaymentModes.SelectedValue = COLLECTIONTYPE.CASH Then
            txtCashTotal.Text = lblNetAmount.Text
        ElseIf rblPaymentModes.SelectedValue = COLLECTIONTYPE.CHEQUES Then
            txtChequeTotal.Text = lblNetAmount.Text
        ElseIf rblPaymentModes.SelectedValue = COLLECTIONTYPE.CREDIT_CARD Then
            txtCCTotal.Text = lblNetAmount.Text
            txtChargeTotal.Text = Format(Convert.ToDouble(txtCrCharge.Text) + Convert.ToDouble(lblNetAmount.Text), "#,##0.00")
        End If
    End Sub

    Protected Sub rblPaymentModes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblPaymentModes.SelectedIndexChanged
        Set_CollectionControls()
    End Sub
    Protected Sub FileUploadComplete(ByVal sender As Object, ByVal e As EventArgs)
        Dim filename As String = System.IO.Path.GetFileName(AsyncFileUpload1.FileName)
        If (AsyncFileUpload1.HasFile) Then
            Dim strFileType As String = System.IO.Path.GetExtension(AsyncFileUpload1.FileName).ToString().ToLower()
            If strFileType <> ".xls" Then
                Exit Sub
            End If
            filename = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString().Replace(".", "") & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & filename
            AsyncFileUpload1.SaveAs(filename)
            Session("filename") = filename
        End If
    End Sub


    Protected Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Dim dtXcel As New DataTable, bHasErrors As Boolean = False
        gvError.Visible = True
        gvError.DataSource = Nothing
        gvError.DataBind()
        lblNoofStudents.Text = 0
        lblTotalBulkAmount.Text = 0
        lblError.Text = ""
        txtAmount.Text = "0.00"
        ViewState("BulkStudata") = Nothing
        If Not Session("filename") Is Nothing AndAlso System.IO.Path.GetFileName(Session("filename")) <> "" Then
            dtXcel = Mainclass.FetchFromExcelIntoDataTable(Session("filename"), 1, 1, 4)
            Dim dv As DataView = New DataView(dtXcel)
            dv.RowFilter = "STU_NO <> ''"
            dtXcel = dv.ToTable
            If Not dtXcel Is Nothing AndAlso dtXcel.Rows.Count > 0 Then
                Dim Columns As DataColumnCollection
                Columns = dtXcel.Columns
                If Not Columns.Contains("STU_NO") OrElse Not Columns.Contains("STU_NAME") OrElse Not Columns.Contains("GRADE") OrElse Not Columns.Contains("AMOUNT") Then
                    usrMessageBar.ShowNotification("The column name(s) for the uploaded file is not as per the format[STU_NO,STU_NAME,GRADE,AMOUNT]", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
                If dtXcel.Rows.Count > 200 Then
                    '   lblError.Text = "Only 200 students can be uploaded in a single transaction"
                    usrMessageBar.ShowNotification("Only 200 students can be uploaded in a single transaction", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
                Dim dtc1 As New DataColumn("ERR_MSG", GetType(System.String))
                Dim dtc2 As New DataColumn("STU_ID", GetType(System.Int64))
                dtXcel.Columns.Add(dtc1)
                dtXcel.Columns.Add(dtc2)

                lblNoofStudents.Text = dtXcel.Rows.Count
                Dim sumObject As Double = 0
                For Each dr As DataRow In dtXcel.Rows
                    If dr("STU_NO").ToString <> "" AndAlso dr("AMOUNT").ToString <> "" Then
                        dr("ERR_MSG") = ""
                        dr("STU_ID") = "0"
                        If VALIDATE_EXCEL_DATA(dr("STU_NO").ToString, dr("AMOUNT"), dr("ERR_MSG"), dr("STU_ID")) Then
                            sumObject = sumObject + dr("AMOUNT")
                        Else
                            bHasErrors = True
                        End If
                    End If
                Next
                If bHasErrors Then
                    Dim dtvw As DataView = dtXcel.DefaultView
                    dtvw.RowFilter = "ERR_MSG <> ''"
                    gvError.DataSource = dtvw
                    gvError.DataBind()
                    lblNoofStudents.Text = 0
                    sumObject = 0
                Else
                    ViewState("BulkStudata") = dtXcel
                    gvError.Visible = False
                End If
                lblTotalBulkAmount.Text = Format(IIf(IsNumeric(sumObject), sumObject, 0), "#,##0.00")
                Dim TAX_CODE As String = "", TAX_AMOUNT As Double = 0, TAX_NET_AMOUNT As Double = 0, AMOUNT_EX_TAX As Double = 0
                GET_TAX_DETAIL(lblTotalBulkAmount.Text, TAX_CODE, TAX_AMOUNT, TAX_NET_AMOUNT, True, AMOUNT_EX_TAX)
                txtAmount.Text = lblTotalBulkAmount.Text
                CALCULATE_TAX(lblTotalBulkAmount.Text, True)
                'txtAmount.Text = AMOUNT_EX_TAX
                'lblTaxAmount.Text = Format(TAX_AMOUNT, "#,##0.00")
                'lblNetAmount.Text = Format(TAX_NET_AMOUNT, "#,##0.00")
                SET_PAYMENT_MODE_AMOUNT()
                'txtAmount_TextChanged(Nothing, Nothing)
                Session("filename") = Nothing
                txtEmpNo.Focus()
                imgEmployee.Attributes.Add("onClick", "GetEMPName();return false;")
            End If
        End If
    End Sub
    Private Function VALIDATE_EXCEL_DATA(ByVal STU_NO As String, ByVal AMOUNT As Decimal, ByRef ERR_MSG As String, ByRef STU_ID As Long) As Boolean
        VALIDATE_EXCEL_DATA = False
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("FEES.VALIDATE_OTHFEECOLL_EXCEL_DATA")

            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuId")
            pParms(1) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 20)
            pParms(1).Value = STU_NO
            pParms(2) = New SqlClient.SqlParameter("@AMOUNT", SqlDbType.Decimal)
            pParms(2).Value = AMOUNT
            pParms(3) = New SqlClient.SqlParameter("@ERR_MSG", SqlDbType.VarChar, 200)
            pParms(3).Direction = ParameterDirection.Output
            pParms(4) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(4).Direction = ParameterDirection.Output
            pParms(5) = New SqlClient.SqlParameter("@RET_VAL", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, Qry.ToString, pParms)
            If pParms(5).Value = 0 Then
                ERR_MSG = pParms(3).Value
                STU_ID = pParms(4).Value
                VALIDATE_EXCEL_DATA = True
            Else
                STU_ID = 0
                ERR_MSG = pParms(3).Value
            End If
        Catch ex As Exception
            ERR_MSG = Left(ex.Message, 200)
        End Try
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            ' lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Set_GridTotal()
        Dim str_error As String = check_errors_details()
        If Not IsDate(txtFrom.Text) Then
            str_error = str_error & "Invalid from date <br />"
        End If

        Dim dblActualAmount As Decimal
        dblActualAmount = CDbl(lblSubTotal.Text)

        If Session("FeeCollection").Rows.Count = 0 Then
            str_error = str_error & "Please enter details<br />"
        End If

        'Changes on 12DEC2019
        If Chk_InvNo.Checked And txtInvno.Text = "" Then

            str_error = str_error & "Please enter Invoice No<br />"
        End If

        If Convert.ToBoolean(Session("BSU_bVATEnabled")) Then
            If Not Chk_InvNo.Checked And Not Chk_InvYes.Checked Then
                str_error = str_error & "Please select Generate Invoice Checkbox option <br />"
            End If
        End If

        If ddEventtype.Visible Then
            If ddEventtype.SelectedValue = "0" Then
                str_error = str_error & "Please select Fee Event Type <br />"
            End If
        End If
        'Athira changes
        'Dim Max_Amount As Double = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnection, CommandType.StoredProcedure, "FEES.GET_LIMIT_OTHERFEE_AUTOINVOICEGENERATE")
        'If Not Chk_InvNo.Checked Then
        '    If (dblActualAmount > Max_Amount) Then
        '        usrMessageBar.ShowNotification("Amount entered is higher that maximum limit, Please enter less amount to generate invoice.", UserControls_usrMessageBar.WarningType.Danger)
        '        Exit Sub
        '    End If
        'End If
        'Calling maximum limit of amount ( from stored procedure )with current amount , if exceeding not allowing to generate the tax invoice.
        'Changes ends on 12DEC2019

        If str_error <> "" Then
            ' lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dtFrom As DateTime
        dtFrom = CDate(txtFrom.Text)

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim str_new_FOC_ID As Integer
        Dim str_NEW_FOC_RECNO As String = ""
        Dim BatchId As Integer = 0, FileRowNo As Integer = 2
        Dim Batch_remarks As String = ""
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            Dim STR_TYPE As Char = "S"
            retval = FeeCommon.CheckFeeclose(Session("sBsuid"), txtFrom.Text, stTrans)
            Dim XRate As Decimal
            Dim Currency As String, FOC_EVT_ID As String = "", FOC_CFA_ID As String = "", FOC_REF_VDR_CODE As String = "", FOC_CUSTOMER_ACT_ID As String = ""
            Dim FOC_CUSTOMER_ADDRESS As String = "", FOC_CUSTOMER_TAX_REG_NO As String = "", FOC_TAX_CODE As String = "", STAFF_EMP_ID As Integer = 0
            If ddlEvents.Items.Count > 0 And trEvents.Visible = True Then
                FOC_EVT_ID = ddlEvents.SelectedValue
                If FOC_EVT_ID = "" Then
                    ' lblError.Text = "Please select an Event"
                    usrMessageBar.ShowNotification("Please select an Event", UserControls_usrMessageBar.WarningType.Danger)
                    ddlEvents.Focus()
                    Exit Sub
                End If
            Else
                FOC_EVT_ID = ""
            End If
            'Getting tax code for header table
            FOC_TAX_CODE = Session("FeeCollection").Rows(0)("TAX_CODE").ToString 'will contain only one row always
            If trCustomer.Visible = True Then
                FOC_CUSTOMER_ACT_ID = ddlCustomers.SelectedValue
                FOC_CUSTOMER_TAX_REG_NO = txtCustTAXRegNo.Text.Trim
                FOC_CUSTOMER_ADDRESS = txtCustAddress.Text.Trim
            End If
            If ddlCashFlow.Items.Count > 0 And trCashFlow.Visible = True Then
                FOC_CFA_ID = ddlCashFlow.SelectedValue
                If FOC_CFA_ID = "" Then
                    ' lblError.Text = "Please select a Cashflow"
                    usrMessageBar.ShowNotification("Please select a Cashflow", UserControls_usrMessageBar.WarningType.Danger)
                    ddlCashFlow.Focus()
                    Exit Sub
                End If
            End If
            If ddCollection.SelectedValue = "9" AndAlso (hf_VDR_CODE.Value = "" Or hf_VDR_CODE.Value = "0") Then
                '    lblError.Text = "Please select a Vendor"
                usrMessageBar.ShowNotification("Please select a Vendor", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            If hf_VDR_CODE.Value <> "" Then
                FOC_REF_VDR_CODE = hf_VDR_CODE.Value
            End If
            If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                XRate = Me.lblExgRate.Text
                Currency = ddCurrency.SelectedItem.Text
            Else
                XRate = 1
                Currency = Session("BSU_CURRENCY")
            End If

            If retval = 0 Then
                If chkBulkUpload.Checked Then 'if bulk upload, different procedure
                    If ViewState("BulkStudata") Is Nothing Then
                        ' lblError.Text = "Please upload the bulk students data and continue"
                        usrMessageBar.ShowNotification("Please upload the bulk students data and continue", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    If h_Emp_No.Value = "" Or h_Emp_No.Value = "0" Or txtEmpNo.Text.Trim = "" Then
                        ' lblError.Text = "Please choose the staff and continue"
                        usrMessageBar.ShowNotification("Please choose the staff and continue", UserControls_usrMessageBar.WarningType.Danger)
                        txtEmpNo.Focus()
                        Exit Sub
                    Else
                        STAFF_EMP_ID = h_Emp_No.Value
                    End If
                    Dim STU_ID As Long = 0, FCD_AMount As Double = 0, FOD_AMOUNT As Double = 0, STU_NAME As String = "", STU_NO As String = "", NARRATION As String = ""
                    'Dim TAX_CODE As String = "", TAX_AMOUNT As Double = 0, TAX_NET_AMOUNT As Double = 0, AMOUNT_EX_TAX As Double = 0

                    'Dim DTBulk As DataTable
                    'If rbUpload.SelectedValue = 1 Then
                    '    DTBulk = DirectCast(ViewState("BulkStudata"), DataTable)
                    'Else
                    '    DTBulk = StudentOtherColection
                    'End If

                    If chkBulkUpload.Checked = True Then
                        BatchId = GET_NEXT_BATCHNO(Session("sBsuid"), stTrans)
                        'BatchId = Mainclass.getDataValue("select MAX(FOC_BATCH_NO)+1 from FEES.FEEOTHCOLLECTION_H WHERE FOC_BSU_ID='" + Session("sBsuid") + "'", "OASIS_FEESConnectionString")
                    Else
                        BatchId = 0
                    End If




                    Dim dtBulk As DataTable = DirectCast(ViewState("BulkStudata"), DataTable)
                    For Each drr As DataRow In dtBulk.Rows
                        If retval <> 0 Then
                            Exit For
                        End If
                        If FeeCollection.GetDoubleVal(drr("STU_NO")) = 0 OrElse FeeCollection.GetDoubleVal(drr("STU_ID")) = 0 OrElse drr("STU_NAME").ToString = "" OrElse FeeCollection.GetDoubleVal(drr("AMOUNT")) = 0 Then
                            retval = "112"
                            Exit For
                        End If
                        str_NEW_FOC_RECNO = ""
                        str_new_FOC_ID = 0
                        STU_NO = drr("STU_NO").ToString
                        STU_NAME = drr("STU_NAME").ToString
                        STU_ID = drr("STU_ID").ToString
                        NARRATION = Left(txtRemarks.Text.Trim & "[" & STU_NO & " - " & STU_NAME & "]", 500)
                        Batch_remarks = txtRemarks.Text.Trim
                        dblActualAmount = CDbl(drr("AMOUNT"))

                        Dim Evt_typ_id As String
                        If (ddEventtype.Items.Count > 0) Then
                            Evt_typ_id = ddEventtype.SelectedItem.Value
                        Else
                            Evt_typ_id = "0"
                        End If

                        'Saving Header 
                        retval = FeeCollectionOther.F_SaveFEEOTHCOLLECTION_H(
                     "0", txtRefno.Text, txtFrom.Text, _
                       Session("sBsuid"), dblActualAmount, Session("sUsr_name"), rblPaymentModes.SelectedItem.Value, False, NARRATION, "CR", False, ddCollection.SelectedItem.Value, _
                       txtDAccountCode.Text, _
                       str_NEW_FOC_RECNO, str_new_FOC_ID _
                    , stTrans, objConn, Currency, XRate, FOC_EVT_ID, FOC_CFA_ID, FOC_REF_VDR_CODE, FOC_CUSTOMER_ACT_ID, FOC_CUSTOMER_TAX_REG_NO, FOC_CUSTOMER_ADDRESS, FOC_TAX_CODE, STU_ID, STAFF_EMP_ID, BatchId _
                      , Batch_remarks, "", IIf(Chk_InvNo.Checked, 0, 1), IIf(Chk_InvNo.Checked, txtInvno.Text, ""),
                      IIf(ddFeetype.SelectedItem.Value = "", 0, CInt(ddFeetype.SelectedItem.Value)), IIf(Evt_typ_id = "", 0, CInt(Evt_typ_id)), "", "") 'IIf(ddEventtype.Visible, ddEventtype.SelectedItem.Value, "0")
                        'Changes on 12DEC2019 - Adding Tax Invoice Reference No and IsGenerateTaxInvoice

                        'Saving Detail
                        If retval = 0 Then
                            If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                                FCD_AMount = drr("FCAmount") + CDbl(drr("TAX_AMOUNT"))
                            Else
                                FCD_AMount = dblActualAmount + CDbl(drr("TAX_AMOUNT"))
                            End If
                            FOD_AMOUNT = dblActualAmount + CDbl(drr("TAX_AMOUNT"))
                            retval = FeeCollectionOther.F_SaveFEEOTHCOLLSUB_D(0, str_new_FOC_ID, drr("REF_ID"), drr("EMR_ID"), FOD_AMOUNT, drr("REF_NO"), _
                                                                              drr("Date"), 0, "", False, stTrans, objConn, FCD_AMount, drr("CrCharge"), _
                                                                              drr("BNK_ACT"), drr("TAX_CODE"), drr("TAX_AMOUNT"), drr("TAX_NET_AMOUNT"))
                        End If

                        If retval <> "0" Then
                            Exit For
                        End If
                        FileRowNo += 1
                        Dim flagAudit As Integer = FeeCollectionOther.operOnAudiTable(Master.MenuName, str_NEW_FOC_RECNO, "Insert", stTrans, objConn, Page.User.Identity.Name.ToString, Me.Page, "")
                    Next
                Else 'If not bulk upload
                    'Save Header
                    Dim Evt_typ_id As String
                    If (ddEventtype.Items.Count > 0) Then
                        Evt_typ_id = ddEventtype.SelectedItem.Value
                    Else
                        Evt_typ_id = "0"
                    End If

                    retval = FeeCollectionOther.F_SaveFEEOTHCOLLECTION_H(0, txtRefno.Text, txtFrom.Text, _
                   Session("sBsuid"), dblActualAmount, Session("sUsr_name"), rblPaymentModes.SelectedItem.Value, _
                 False, txtRemarks.Text, "CR", False, ddCollection.SelectedItem.Value, txtDAccountCode.Text, _
                 str_NEW_FOC_RECNO, str_new_FOC_ID, stTrans, objConn, Currency, XRate, FOC_EVT_ID, FOC_CFA_ID, _
                 FOC_REF_VDR_CODE, FOC_CUSTOMER_ACT_ID, FOC_CUSTOMER_TAX_REG_NO, FOC_CUSTOMER_ADDRESS, FOC_TAX_CODE,
                 0, 0, 0, "", "", IIf(Chk_InvNo.Checked, 0, 1), IIf(Chk_InvNo.Checked, txtInvno.Text, ""), ddFeetype.SelectedItem.Value,
                 Evt_typ_id, "", "") '


                    'Save Detail
                    If retval = "0" Then
                        For I As Integer = 0 To Session("FeeCollection").Rows.Count - 1
                            Dim FCD_AMount As Double
                            If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
                                FCD_AMount = Session("FeeCollection").Rows(I)("FCAmount")
                            Else
                                FCD_AMount = Session("FeeCollection").Rows(I)("Amount")
                            End If
                            retval = FeeCollectionOther.F_SaveFEEOTHCOLLSUB_D(0, str_new_FOC_ID, Session("FeeCollection").Rows(I)("REF_ID"), _
                             Session("FeeCollection").Rows(I)("EMR_ID"), Session("FeeCollection").Rows(I)("Amount"), _
                             Session("FeeCollection").Rows(I)("REF_NO"), Session("FeeCollection").Rows(I)("Date"), _
                              0, "", False, stTrans, objConn, FCD_AMount, Session("FeeCollection").Rows(I)("CrCharge"), _
                              Session("FeeCollection").Rows(I)("BNK_ACT"), Session("FeeCollection").Rows(I)("TAX_CODE"), _
                              Session("FeeCollection").Rows(I)("TAX_AMOUNT"), Session("FeeCollection").Rows(I)("TAX_NET_AMOUNT"))
                            If retval <> "0" Then
                                Exit For
                            End If
                            I = I + 1
                        Next
                    End If
                End If
            End If

            If retval = "0" Then
                stTrans.Commit()
                Session("FeeCollection").rows.clear()
                If chkBulkUpload.Checked Then
                    ViewState("BulkStudata") = Nothing
                Else
                    h_print.Value = "print"
                    ViewState("recno") = str_NEW_FOC_RECNO
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_NEW_FOC_RECNO, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                    PrintReceipt(str_NEW_FOC_RECNO)
                End If
                Clear_All()
                gvFeeCollection.DataBind()
                trBulkUpload.Style.Item("display") = "none"
                '  lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
            Else
                stTrans.Rollback()
                ' lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval).Replace("#$$#", FileRowNo.ToString), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            ' lblError.Text = getErrorMessage(1000)
            usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()

            End If
        End Try
    End Sub

  
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        Clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub rblTaxCalculation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblTaxCalculation.SelectedIndexChanged
        txtAmount.Text = "0.00"
        lblTaxAmount.Text = "0.00"
        lblNetAmount.Text = "0.00"
        SET_PAYMENT_MODE_AMOUNT()
        If IsNumeric(txtAmount.Text) AndAlso CDbl(txtAmount.Text) > 0 Then
            Dim TAX_CODE As String = "", TAX_AMOUNT As Double = 0, TAX_NET_AMOUNT As Double = 0, AMOUNT_EX_TAX As Double = 0
            GET_TAX_DETAIL(txtAmount.Text, TAX_CODE, TAX_AMOUNT, TAX_NET_AMOUNT, rblTaxCalculation.SelectedValue = "I", AMOUNT_EX_TAX)
            txtAmount.Text = AMOUNT_EX_TAX
        End If
    End Sub
    Protected Sub chkBulkUpload_CheckedChanged(sender As Object, e As EventArgs) Handles chkBulkUpload.CheckedChanged
        'If chkBulkUpload.Checked Then
        '    trBulkUpload.Style.Item("display") = "block"
        '    txtAmount.Enabled = False
        '    rblTaxCalculation.SelectedValue = "I" 'Amount is Including Tax
        '    rblTaxCalculation.Enabled = False
        '    rblPaymentModes.SelectedValue = COLLECTIONTYPE.CASH
        '    rblPaymentModes.Enabled = False
        '    SET_PAYMENT_MODE_AMOUNT()
        '    imgEmployee.Attributes.Add("onClick", "GetEMPName();")
        'Else
        '    trBulkUpload.Style.Item("display") = "none"
        '    gvError.Visible = False
        '    gvError.DataSource = Nothing
        '    gvError.DataBind()
        '    lblNoofStudents.Text = 0
        '    lblTotalBulkAmount.Text = 0
        '    lblError.Text = ""
        '    txtAmount.Text = "0.00"
        '    h_Emp_No.Value = 0
        '    txtEmpNo.Text = ""
        '    Session("filename") = Nothing
        '    txtAmount.Enabled = True
        '    rblTaxCalculation.SelectedValue = "E" 'Amount is Excluding Tax
        '    rblTaxCalculation.Enabled = True
        '    ViewState("BulkStudata") = Nothing
        '    rblPaymentModes.SelectedValue = COLLECTIONTYPE.CASH
        '    rblPaymentModes.Enabled = True
        '    SET_PAYMENT_MODE_AMOUNT()
        'End If
        If chkBulkUpload.Checked Then

            trGridandStaff.Style.Item("display") = "block"
            trBulkUpload.Style.Item("display") = "block"
            rbUpload.SelectedValue = 1
            CheckUploadOptions()
            rblTaxCalculation.SelectedValue = "I" 'Amount is Including Tax
            rblTaxCalculation.Enabled = False
            rblPaymentModes.SelectedValue = COLLECTIONTYPE.CASH
            rblPaymentModes.Enabled = False
            SET_PAYMENT_MODE_AMOUNT()
            imgEmployee.Attributes.Add("onClick", "GetEMPName();return false;")
            trStudentSelection.Style.Item("display") = "none"
            StudentOtherColection = Nothing
            gvStudSelection.Visible = False
            gvStudSelection.DataSource = StudentOtherColection
            gvStudSelection.DataBind()
            rbUpload.Visible = True
        Else
            trBulkUpload.Style.Item("display") = "none"
            trGridandStaff.Style.Item("display") = "none"
            trStudentSelection.Style.Item("display") = "none"
            gvError.Visible = False
            gvError.DataSource = Nothing
            gvError.DataBind()
            StudentOtherColection = Nothing
            gvStudSelection.Visible = False
            gvStudSelection.DataSource = StudentOtherColection
            gvStudSelection.DataBind()
            lblNoofStudents.Text = 0
            lblTotalBulkAmount.Text = 0
            txtDefaultAmount.Text = 0
            lblError.Text = ""
            txtAmount.Text = "0.00"
            h_Emp_No.Value = 0
            txtEmpNo.Text = ""
            Session("filename") = Nothing
            txtAmount.Enabled = True
            rblTaxCalculation.SelectedValue = "E" 'Amount is Excluding Tax
            rblTaxCalculation.Enabled = True
            ViewState("BulkStudata") = Nothing
            rblPaymentModes.SelectedValue = COLLECTIONTYPE.CASH
            rblPaymentModes.Enabled = True
            SET_PAYMENT_MODE_AMOUNT()
            rbUpload.Visible = False
        End If

    End Sub
    Protected Sub rbUpload_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbUpload.SelectedIndexChanged
        CheckUploadOptions()
        h_OLD_STU_IDs.Value = ""
        txtAmount.Text = "0.0"
        lblTaxAmount.Text = "0.0"
        lblNetAmount.Text = "0.0"
    End Sub

    Private Sub CheckUploadOptions()
        If rbUpload.SelectedValue = 1 Then
            'trBulkUpload.Style.Item("display") = "block"
            'txtAmount.Enabled = False
            'trStudentSelection.Style.Item("display") = "none"
            'trGridandStaff.Style.Item("display") = "block"
            'gvError.Visible = False
            'gvError.DataSource = Nothing
            'gvError.DataBind()
            'lblNoofStudents.Text = 0
            'lblTotalBulkAmount.Text = 0
            'gvStudSelection.Visible = False
            'gvStudSelection.DataSource = Nothing
            'gvStudSelection.DataBind()


            trBulkUpload.Style.Item("display") = "table-row"
            trStudentSelection.Style.Item("display") = "none"
            trGridandStaff.Style.Item("display") = "table-row"
            txtAmount.Enabled = False
            rblTaxCalculation.SelectedValue = "I" 'Amount is Including Tax
            rblTaxCalculation.Enabled = False
            rblPaymentModes.SelectedValue = COLLECTIONTYPE.CASH
            rblPaymentModes.Enabled = False
            SET_PAYMENT_MODE_AMOUNT()
            imgEmployee.Attributes.Add("onClick", "GetEMPName();return false;")
            gvError.Visible = False
            gvError.DataSource = Nothing
            gvError.DataBind()
            lblNoofStudents.Text = 0
            lblTotalBulkAmount.Text = 0
            gvStudSelection.Visible = False
            gvStudSelection.DataSource = Nothing
            gvStudSelection.DataBind()
        Else
            trStudentSelection.Style.Item("display") = "table-row"
            txtAmount.Enabled = False
            trGridandStaff.Style.Item("display") = "table-row"
            trBulkUpload.Style.Item("display") = "none"
            gvError.Visible = False
            gvError.DataSource = Nothing
            gvError.DataBind()
            gvStudSelection.Visible = True
            gvStudSelection.DataSource = Nothing
            gvStudSelection.DataBind()
            lblNoofStudents.Text = 0
            lblTotalBulkAmount.Text = 0
            lblError.Text = ""
            txtAmount.Text = "0.00"
            h_Emp_No.Value = 0
            txtEmpNo.Text = ""
            Session("filename") = Nothing
            txtAmount.Enabled = True

            ViewState("BulkStudata") = Nothing
            rblPaymentModes.SelectedValue = COLLECTIONTYPE.CASH
            rblPaymentModes.Enabled = False
            SET_PAYMENT_MODE_AMOUNT()
            FillGrade(ddlAca_Year.SelectedValue)


        End If
    End Sub

    Private Sub FillacademicYear()
        Try
            Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
            ddlAca_Year.DataSource = dtACD
            ddlAca_Year.DataTextField = "ACY_DESCR"
            ddlAca_Year.DataValueField = "ACD_ID"
            ddlAca_Year.DataBind()
            For Each rowACD As DataRow In dtACD.Rows
                If rowACD("ACD_CURRENT") Then
                    ddlAca_Year.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                    Exit For
                End If
            Next
            FillGrade(ddlAca_Year.SelectedValue)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub FillGrade(Optional ByVal ACD_ID As String = "")
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim Strsql As String = "SELECT DISTINCT GRD_ID,GRD_DISPLAY,GRD_DISPLAYORDER FROM OASIS.dbo.GRADE_BSU_M WITH(NOLOCK) INNER JOIN OASIS.dbo.GRADE_M WITH(NOLOCK) ON GRM_GRD_ID=GRD_ID INNER JOIN OASIS.dbo.ACADEMICYEAR_D WITH(NOLOCK) ON ACD_ID=GRM_ACD_ID INNER JOIN OASIS.dbo.ACADEMICYEAR_M WITH(NOLOCK) ON ACD_ACY_ID=ACY_ID where GRM_BSU_ID='" & Session("sbsuid") & "' and ACD_ID='" & ACD_ID & "'  ORDER BY GRD_DISPLAYORDER "
            Using Grade_ACDReader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.Text, Strsql)
                ddlGrade.Items.Clear()
                If Grade_ACDReader.HasRows = True Then
                    ddlGrade.DataSource = Grade_ACDReader
                    ddlGrade.DataTextField = "GRD_DISPLAY"
                    ddlGrade.DataValueField = "GRD_ID"
                    ddlGrade.DataBind()
                End If
            End Using
            FillSection(ddlGrade.SelectedValue)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGrade.SelectedIndexChanged
        FillSection(ddlGrade.SelectedValue)
    End Sub
    Private Sub FillSection(Optional ByVal GRD_ID As String = "")
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim StrsqlSection As String = " SELECT SCT_ID,SCT_DESCR  FROM  OASIS.dbo.SECTION_M WITH(NOLOCK)  INNER JOIN OASIS.dbo.ACADEMICYEAR_D WITH(NOLOCK) ON ACD_ID=SCT_ACD_ID  INNER JOIN OASIS.dbo.ACADEMICYEAR_M WITH(NOLOCK) ON ACD_ACY_ID=ACY_ID where ACD_BSU_ID='" & Session("sbsuid") & "' and ACD_CURRENT=1 AND SCT_GRD_ID='" & GRD_ID & "'   ORDER BY ACY_ID"
            Using Section_ACDReader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.Text, StrsqlSection)
                ddlSection.Items.Clear()
                If Section_ACDReader.HasRows = True Then
                    ddlSection.DataSource = Section_ACDReader
                    ddlSection.DataTextField = "SCT_DESCR"
                    ddlSection.DataValueField = "SCT_ID"
                    ddlSection.DataBind()
                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            Dim str_sql As String = "SELECT DISTINCT STU_NO ,STU_ID, NAME STU_NAME,STU_GRD_ID GRADE,CONVERT(DECIMAL(16,2)," & txtDefaultAmount.Text & ") AMOUNT   FROM  OASIS..STUDENTS with (nolock) WHERE STU_ID IN ('" & h_STU_IDs.Value.Replace("___", "','") & "')"
            GridBindStudents(str_sql)
            h_STU_IDs.Value = ""
        End If

    End Sub
    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        If h_STU_IDs.Value <> "" Then
            Dim str_sql As String = "SELECT DISTINCT STU_NO ,STU_ID, NAME STU_NAME,STU_GRD_ID GRADE,CONVERT(DECIMAL(16,2)," & txtDefaultAmount.Text & ") AMOUNT   FROM  OASIS..STUDENTS with (nolock) WHERE STU_ID IN ('" & h_STU_IDs.Value.Replace("___", "','") & "')"
            GridBindStudents(str_sql)
            h_STU_IDs.Value = ""
        End If

    End Sub
    Private Sub GridBindStudents(ByVal Sqlstr As String)
        Try
            fillGridView(gvStudSelection, Sqlstr)
            UpdateAmounts()
            ddlTAXSelChanged()
        Catch ex As Exception
            Errorlog(ex.Message)
            ' lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Sub UpdateAmounts()
        Dim TAX_CODE As String = "", TAX_AMOUNT As Double = 0.0, TAX_NET_AMOUNT As Double = 0.0, AMOUNT_EX_TAX As Double = 0.0
        lblNoofStudents.Text = Convert.ToInt32(StudentOtherColection.Compute("count(STU_NO)", String.Empty))
        lblTotalBulkAmount.Text = Format(Convert.ToInt32(StudentOtherColection.Compute("SUM(AMOUNT)", String.Empty)), "#,##0.00")
        GET_TAX_DETAIL(lblTotalBulkAmount.Text, TAX_CODE, TAX_AMOUNT, TAX_NET_AMOUNT, True, AMOUNT_EX_TAX)
        txtAmount.Text = Format(AMOUNT_EX_TAX, "#,##0.00")
        lblTaxAmount.Text = Format(TAX_AMOUNT, "#,##0.00")
        lblNetAmount.Text = Format(TAX_NET_AMOUNT, "#,##0.00")
        ViewState("BulkStudata") = StudentOtherColection
        SET_PAYMENT_MODE_AMOUNT()
    End Sub
    Private Sub fillGridView(ByRef fillGrdView As GridView, ByVal fillSQL As String)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, CommandType.Text, fillSQL)
        If Not StudentOtherColection Is Nothing Then
            Dim dt As DataTable = StudentOtherColection
            If dt.Rows.Count >= 0 Then
                dt.Merge(ds.Tables(0))
                StudentOtherColection = dt
            End If
        Else
            StudentOtherColection = ds.Tables(0)
        End If
        fillGrdView.DataSource = StudentOtherColection
        fillGrdView.DataBind()
        showNoRecordsFound()

    End Sub
    Protected Sub ddlAca_Year_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        FillGrade(ddlAca_Year.SelectedValue)
    End Sub
    Private Property StudentOtherColection() As DataTable
        Get
            Return ViewState("StudentOtherColection")
        End Get
        Set(ByVal value As DataTable)
            ViewState("StudentOtherColection") = value
        End Set
    End Property
    Private Sub showNoRecordsFound()
        If StudentOtherColection.Rows(0)(1) = "-1" Then
            Dim TotalColumns As Integer = gvStudSelection.Columns.Count - 2
            gvStudSelection.Rows(0).Cells.Clear()
            gvStudSelection.Rows(0).Cells.Add(New TableCell())
            gvStudSelection.Rows(0).Cells(0).ColumnSpan = TotalColumns
            gvStudSelection.Rows(0).Cells(0).Text = "No Record Found"
        End If
    End Sub

    Protected Sub gvStudSelection_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvStudSelection.PageIndexChanging
        gvStudSelection.PageIndex = e.NewPageIndex
        gvStudSelection.DataSource = StudentOtherColection
        gvStudSelection.DataBind()
    End Sub
    Protected Sub lbtnDelete_Click(sender As Object, e As EventArgs)
        'Dim lbtnDelete As LinkButton = DirectCast(sender, LinkButton)
        'Dim gvr As GridViewRow = DirectCast(lbtnDelete.NamingContainer, GridViewRow)
        'StudentOtherColection.Rows(gvr.RowIndex).Delete()
        'StudentOtherColection.AcceptChanges()
        'gvStudSelection.DataSource = StudentOtherColection
        'UpdateAmounts()
        'gvStudSelection.DataBind()
        'showNoRecordsFound()
        'ddlTAXSelChanged()

        Dim lbtnDelete As LinkButton = DirectCast(sender, LinkButton)
        Dim gvr As GridViewRow = DirectCast(lbtnDelete.NamingContainer, GridViewRow)
        Dim mRow() As DataRow = StudentOtherColection.Select("STU_NO=" & gvStudSelection.DataKeys(gvr.RowIndex).Values(0), "")
        If mRow.Length > 0 Then
            StudentOtherColection.Select("STU_NO=" & gvStudSelection.DataKeys(gvr.RowIndex).Values(0), "")(0).Delete()
            StudentOtherColection.AcceptChanges()
        End If
        gvStudSelection.DataSource = StudentOtherColection
        UpdateAmounts()
        gvStudSelection.DataBind()
        showNoRecordsFound()
        ddlTAXSelChanged()




    End Sub

    Protected Sub TextBox1_TextChanged(sender As Object, e As EventArgs)
        TextBox1.Text = ""
        If h_STU_IDs.Value <> "" Then
            Dim str_sql As String = "SELECT DISTINCT STU_NO ,STU_ID, NAME STU_NAME,STU_GRD_ID GRADE,CONVERT(DECIMAL(16,2)," & txtDefaultAmount.Text & ") AMOUNT   FROM  OASIS..STUDENTS with (nolock) WHERE STU_ID IN ('" & h_STU_IDs.Value.Replace("___", "','") & "')"
            GridBindStudents(str_sql)
            h_STU_IDs.Value = ""
        End If
    End Sub


    Public Shared Function GET_NEXT_BATCHNO(ByVal BSU_ID As String,
   ByVal p_stTrans As SqlTransaction) As Integer
        Try
            Dim pParms(3) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
            pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue

            Dim RetVal As Integer = 0

            SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "[Fees].[GET_NEXT_BATCHNO]", pParms)
            RetVal = pParms(2).Value
            'If RetVal = -1 Then
            '    Return -1
            'Else
            '    Return 1
            'End If
            Return RetVal
        Catch e As Exception
            Return -1
        End Try

    End Function
End Class