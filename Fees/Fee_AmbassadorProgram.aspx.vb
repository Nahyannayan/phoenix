﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web
Imports System.Web.UI
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Telerik.Web.UI
Imports System.Linq


Partial Class Fees_Fee_AmbassadorProgram
    Inherits BasePage
    Dim Encr_decrData As New Encryption64
    Const PERCENT_VALUE As Decimal = 4
    Const REWARD_VALUE As Decimal = 11
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            ViewState("USER_EMP_ID") = Encr_decrData.Decrypt(Request.QueryString("UID").Replace(" ", "+"))
            ViewState("APH_ID") = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
            ViewState("LEVEL") = Encr_decrData.Decrypt(Request.QueryString("LVL").Replace(" ", "+"))

            Dim LEVEL_STRING As String = ""
            Dim dt12 As DataTable = GetEmployeeDetails(Convert.ToString(ViewState("USER_EMP_ID")))
            For Each row2 As DataRow In dt12.Rows
                If ViewState("LEVEL") = "1" Then
                    LEVEL_STRING = "Welcome " & row2.Item("EMP_NAME") & " !!! - " & "Registrar Approval Required"
                ElseIf ViewState("LEVEL") = "2" Then
                    LEVEL_STRING = "Welcome " & row2.Item("EMP_NAME") & " !!! - " & "Finance Approval Required"
                ElseIf ViewState("LEVEL") = "3" Then
                    LEVEL_STRING = "Welcome " & row2.Item("EMP_NAME") & " !!! - " & "Principal Approval Required"
                End If
            Next

            lbl_PageTitle.Text = LEVEL_STRING

            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "Images/Home/no_image.gif"
            Dim STU_PATH As String = ""

            Try
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
                Dim dt As DataTable = ViewAmbassadorProgramDetails(1, ViewState("APH_ID"))

                For Each drow As DataRow In dt.Rows
                    StudentNo.Text = drow.Item("STU_NO")
                    Name.Text = drow.Item("STU_NAME")
                    Grade.Text = drow.Item("GRD_DISPLAY")
                    DOJ.Text = GetDateFormat(drow.Item("STU_DOJ"))
                    'School.Text = drow.Item("BSU_NAME")
                    lbl_SchoolName.Text = drow.Item("BSU_NAME")
                    TUITION_AMT.Text = drow.Item("FEE_AMOUNT")

                    ViewState("APH_SOURCE") = drow.Item("APH_SOURCE")

                    If drow.Item("APH_SOURCE") = "EMPLOYEE" Then
                        ref_parent.Visible = False
                        ref_employee.Visible = True
                    Else
                        ref_parent.Visible = True
                        ref_employee.Visible = False
                    End If
                    Try
                        'lbl_Note.Text = "GEMS Rewards: Amount(AED) - " & getPercentage(Convert.ToDecimal(drow.Item("FEE_AMOUNT")), PERCENT_VALUE) & " / Points - " & Convert.ToInt32(REWARD_VALUE * Convert.ToDecimal(getPercentage(Convert.ToDecimal(drow.Item("FEE_AMOUNT")), PERCENT_VALUE))) & ""
                        'ViewState("REWARD_AMOUNT") = getPercentage(Convert.ToDecimal(drow.Item("FEE_AMOUNT")), PERCENT_VALUE)
                        'ViewState("REWARD_POINTS") = Convert.ToInt32(REWARD_VALUE * Convert.ToDecimal(getPercentage(Convert.ToDecimal(drow.Item("FEE_AMOUNT")), PERCENT_VALUE)))

                        Dim dtr2 As DataTable = GET_REWARDS_CALCULATION(1, Convert.ToDecimal(drow.Item("FEE_AMOUNT")), drow.Item("BSU_ID"), ViewState("APH_ID"))
                        For Each row2 As DataRow In dtr2.Rows
                            ViewState("REWARD_AMOUNT") = row2.Item("REWARD_AMOUNT")
                            ViewState("REWARD_POINTS") = row2.Item("REWARD_POINTS")

                            'lbl_Note.Text = "GEMS Rewards: Amount(AED) - " & ViewState("REWARD_AMOUNT") & " / Points - " & ViewState("REWARD_POINTS") & ""
                            lbl_Note.Text = row2.Item("REWARD_TEXT")

                        Next


                        If drow.Item("APH_SOURCE") = "EMPLOYEE" Then
                            Dim dt6 As DataTable = GET_EMPLOYEE_DETAILS(4, drow.Item("BSU_ID"), drow.Item("APH_STU_PERSISTANT_ID"))
                            For Each demprow As DataRow In dt6.Rows
                                LBL_EMP_ID.Text = demprow.Item("EMP_ID")
                                LBL_EMP_NAME.Text = demprow.Item("EMPLOYEE_NAME")
                                LBL_EMP_DES.Text = demprow.Item("DESIGNATION")
                                LBL_EMP_EMAIL.Text = demprow.Item("EMAIL")
                                LBL_EMP_DOJ.Text = demprow.Item("JOIN_DATE")
                                'LBL_EMP_.Text = demprow.Item("MOBILE_NUMBER")
                            Next
                        End If


                    Catch ex As Exception
                        lblError.Text = ex.Message
                        Errorlog(ex.Message)
                    End Try

                    Dim dt2 As DataTable = GetStudentDetails(drow.Item("BSU_ID"), drow.Item("STU_NO"))
                    For Each row2 As DataRow In dt2.Rows
                        Label1.Value = connPath & row2.Item("STU_PHOTOPATH")
                    Next

                Next



                If ViewState("APH_SOURCE") = "PARENT" Then
                    Dim dt1 As DataTable = ViewAmbassadorProgramDetails(2, ViewState("APH_ID"))

                    gv_stu_details.DataSource = dt1
                    gv_stu_details.DataBind()
                End If

                If ViewState("LEVEL") = "1" Then
                    Dim dt3 As DataTable = ViewAmbassadorChecklist(4, 1)
                    gv_chklistdetails.DataSource = dt3
                    gv_chklistdetails.DataBind()
                ElseIf ViewState("LEVEL") = "2" Then
                    Dim dt3 As DataTable = ViewAmbassadorChecklist(1)
                    gv_chklistdetails.DataSource = dt3
                    gv_chklistdetails.DataBind()
                ElseIf ViewState("LEVEL") = "3" Then
                    Dim dt3 As DataTable = ViewAmbassadorChecklist(1)
                    gv_chklistdetails.DataSource = dt3
                    gv_chklistdetails.DataBind()
                End If

                If ViewState("APH_SOURCE") = "PARENT" Then
                    Dim dt4 As DataTable = ViewAmbassadorProgramDetails(3, ViewState("APH_ID"))

                    For Each drow As DataRow In dt4.Rows
                        Session("RadioCheckValue") = Convert.ToString(drow.Item("STU_NO"))
                        Try
                            For Each item As GridDataItem In gv_stu_details.MasterTableView.Items

                                Dim radioButton As RadioButton = CType(item.FindControl("rdSelect"), RadioButton)

                                If Convert.ToString(Session("RadioCheckValue")) = Convert.ToString(item("STU_NO").Text()) Then
                                    TryCast(item.FindControl("rdSelect"), RadioButton).Checked = True
                                    Session("CurrentRadioCheckValue") = Convert.ToString(item("STU_NO").Text())
                                    'item.BackColor = Drawing.Color.Orange   'No need to show selected row in highlighted color
                                    'We are commenting below 2 lines because Calculate GEMS Rewards Amount and Point from Referred Student Yearly Tuition Fee
                                    'lbl_Note.Text = "GEMS Rewards: Amount(AED) - " & getPercentage(Convert.ToDecimal(item("FEE_AMOUNT").Text()), PERCENT_VALUE) & " / Points - " & 11 * Convert.ToInt32(getPercentage(Convert.ToDecimal(item("FEE_AMOUNT").Text()), PERCENT_VALUE)) & ""
                                    'ViewState("REWARD_AMOUNT") = getPercentage(Convert.ToDecimal(item("FEE_AMOUNT").Text()), PERCENT_VALUE)
                                    If ViewState("LEVEL") = "1" Then
                                        TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False
                                    ElseIf ViewState("LEVEL") = "2" Then
                                        Dim dt11 As DataTable = ViewAmbassadorProgramDetails(5, ViewState("APH_ID"))
                                        For Each row2 As DataRow In dt11.Rows
                                            Dim status As String = row2.Item("APH_LEVEL2_STATUS")
                                            If status = "A" Or status = "R" Then
                                                TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False
                                            Else
                                                TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False 'we are disabling for LEVEL2 also
                                            End If
                                        Next
                                    ElseIf ViewState("LEVEL") = "3" Then
                                        TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False
                                    End If
                                ElseIf Convert.ToString(Session("RadioCheckValue")) <> Convert.ToString(item("STU_NO").Text()) Then
                                    TryCast(item.FindControl("rdSelect"), RadioButton).Checked = False
                                    item.BackColor = Drawing.Color.White
                                    If ViewState("LEVEL") = "1" Then
                                        TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False
                                    ElseIf ViewState("LEVEL") = "2" Then
                                        Dim dt11 As DataTable = ViewAmbassadorProgramDetails(5, ViewState("APH_ID"))
                                        For Each row2 As DataRow In dt11.Rows
                                            Dim status As String = row2.Item("APH_LEVEL2_STATUS")
                                            If status = "A" Or status = "R" Then
                                                TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False
                                            Else
                                                TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False 'we are disabling for LEVEL2 also
                                            End If
                                        Next
                                    ElseIf ViewState("LEVEL") = "3" Then
                                        TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False
                                    End If
                                End If

                            Next

                        Catch ex As Exception
                            lblError.Text = ex.Message

                        End Try


                        Dim dt5 As DataTable = GetParentDetails(1, drow.Item("STU_NO"))

                        For Each row2 As DataRow In dt5.Rows
                            P_FName.Text = row2.Item("FNAME")
                            P_FContact.Text = row2.Item("FMOBILE")
                            P_FCompany.Text = row2.Item("FCOMPANY")
                            P_MName.Text = row2.Item("MNAME")
                            P_MContact.Text = row2.Item("MMOBILE")
                            P_MCompany.Text = row2.Item("MCOMPANY")
                            P_PRI_Contact_F.Text = row2.Item("PRIMAY_CONTACT")
                            P_PRI_Contact_M.Text = row2.Item("PRIMAY_CONTACT")
                            lbl_Email.Text = row2.Item("PARENT_EMAIL")
                            lbl_UserName.Text = row2.Item("PARENT_USERNAME")
                        Next


                    Next
                End If

                If ViewState("LEVEL") = "2" Then
                    lbl_Remarks1.Enabled = True
                    hdr_Remarks1.Enabled = True
                    hdr_Remarks1.Text = "Registrar Comments: "
                    Dim APH_ACM_IDS As String = ""
                    Dim dt6 As DataTable = ViewAmbassadorProgramDetails(5, ViewState("APH_ID"))
                    For Each drow As DataRow In dt6.Rows
                        lbl_Remarks1.Text = drow.Item("APH_LEVEL1_COMMENT")
                        Dim status As String = drow.Item("APH_LEVEL2_STATUS")
                        APH_ACM_IDS = drow.Item("APH_ACM_IDS")
                        If status = "A" Or status = "R" Then
                            'gv_stu_details.Enabled = False
                            gv_chklistdetails.Enabled = False
                            txt_Remarks2.Disabled = True
                            btn_Process.Visible = False
                            btn_Reject.Visible = False
                        End If
                        If status = "A" Then
                            txt_Remarks2.InnerText = drow.Item("APH_LEVEL2_COMMENT")
                            'lblMessage.Text = "&#10004; Approved the Ambassador Program Discount"
                            ShowMessage("&#10004; Approved and forwarded to Principal", 0)
                            divNote.Visible = True
                            divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                            lblError.Text = "<div>&#10004; Approved and forwarded to Principal </div>"
                        End If
                        If status = "R" Then
                            txt_Remarks2.InnerText = drow.Item("APH_LEVEL2_COMMENT")
                            'lblMessage.Text = "&#10060; Finance Rejected"
                            ShowMessage("&#10060; Rejected by Finance", 1)
                            divNote.Visible = True
                            divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                            lblError.Text = "<div>&#10060; Rejected by Finance</div>"
                        End If


                    Next

                    Dim exists As Boolean = False

                    Dim dt9 As DataTable = ViewAmbassadorChecklist(4, 1)
                    For Each item As GridDataItem In gv_chklistdetails.MasterTableView.Items

                        Dim StrArr As String() = getPipedData(APH_ACM_IDS)
                        Dim strFound = Array.Find(StrArr, Function(str) str.Equals(Convert.ToString(item("ACM_ID").Text())))
                        exists = dt9.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = strFound)
                        If exists Then
                            TryCast(item.FindControl("chk_select"), CheckBox).Checked = True
                            item.BackColor = Drawing.Color.LightYellow
                        End If

                    Next

                    Dim dt7 As DataTable = ViewAmbassadorChecklist(4, 2)
                    For Each item As GridDataItem In gv_chklistdetails.MasterTableView.Items

                        Dim StrArr As String() = getPipedData(APH_ACM_IDS)
                        Dim strFound = Array.Find(StrArr, Function(str) str.Equals(Convert.ToString(item("ACM_ID").Text())))
                        exists = dt7.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = strFound)
                        If exists Then
                            TryCast(item.FindControl("chk_select"), CheckBox).Checked = True
                            'item.BackColor = Drawing.Color.LightYellow
                        End If

                        exists = dt7.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = Convert.ToString(item("ACM_ID").Text()))
                        'exists = FuncIsSubjExistInDataTable(Convert.ToString(item("ACM_ID").Text()), dt7)

                        If Not exists Then
                            TryCast(item.FindControl("chk_select"), CheckBox).Enabled = False
                        End If

                    Next
                ElseIf ViewState("LEVEL") = "3" Then
                    lbl_Remarks1.Enabled = True
                    hdr_Remarks1.Enabled = True
                    hdr_Remarks1.Text = "Registrar Comments: "
                    lbl_Remarks2.Enabled = True
                    hdr_Remarks2.Enabled = True
                    hdr_Remarks2.Text = "Finance Comments: "
                    Dim APH_ACM_IDS As String = ""
                    Dim dt6 As DataTable = ViewAmbassadorProgramDetails(5, ViewState("APH_ID"))
                    For Each drow As DataRow In dt6.Rows
                        lbl_Remarks1.Text = drow.Item("APH_LEVEL1_COMMENT")
                        lbl_Remarks2.Text = drow.Item("APH_LEVEL2_COMMENT")
                        Dim status As String = drow.Item("APH_LEVEL3_STATUS")
                        APH_ACM_IDS = drow.Item("APH_ACM_IDS")
                        If status = "A" Or status = "R" Then
                            'gv_stu_details.Enabled = False
                            gv_chklistdetails.Enabled = False
                            txt_Remarks2.Disabled = True
                            btn_Process.Visible = False
                            btn_Reject.Visible = False
                        End If
                        If status = "A" Then
                            txt_Remarks2.InnerText = drow.Item("APH_LEVEL3_COMMENT")
                            'lblMessage.Text = "&#10004; Approved the Ambassador Program Discount"
                            ShowMessage("&#10004; Approved by Principal", 0)
                            divNote.Visible = True
                            divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                            lblError.Text = "<div>&#10004; Approved by Principal </div>"
                        End If
                        If status = "R" Then
                            txt_Remarks2.InnerText = drow.Item("APH_LEVEL3_COMMENT")
                            'lblMessage.Text = "&#10060; Finance Rejected"
                            ShowMessage("&#10060; Rejected by Principal", 1)
                            divNote.Visible = True
                            divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                            lblError.Text = "<div>&#10060; Rejected by Principal</div>"
                        End If


                    Next

                    Dim exists As Boolean = False

                    Dim dt9 As DataTable = ViewAmbassadorChecklist(4, 1)
                    For Each item As GridDataItem In gv_chklistdetails.MasterTableView.Items

                        Dim StrArr As String() = getPipedData(APH_ACM_IDS)
                        Dim strFound = Array.Find(StrArr, Function(str) str.Equals(Convert.ToString(item("ACM_ID").Text())))
                        exists = dt9.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = strFound)
                        If exists Then
                            TryCast(item.FindControl("chk_select"), CheckBox).Checked = True
                            item.BackColor = Drawing.Color.LightYellow
                        End If

                        exists = dt9.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = Convert.ToString(item("ACM_ID").Text()))
                        'exists = FuncIsSubjExistInDataTable(Convert.ToString(item("ACM_ID").Text()), dt7)

                        If exists Then
                            TryCast(item.FindControl("chk_select"), CheckBox).Enabled = False
                        End If

                    Next

                    Dim dt7 As DataTable = ViewAmbassadorChecklist(4, 2)
                    For Each item As GridDataItem In gv_chklistdetails.MasterTableView.Items

                        Dim StrArr As String() = getPipedData(APH_ACM_IDS)
                        Dim strFound = Array.Find(StrArr, Function(str) str.Equals(Convert.ToString(item("ACM_ID").Text())))
                        exists = dt7.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = strFound)
                        If exists Then
                            TryCast(item.FindControl("chk_select"), CheckBox).Checked = True
                            item.BackColor = Drawing.Color.LightYellow
                        End If

                        exists = dt7.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = Convert.ToString(item("ACM_ID").Text()))
                        'exists = FuncIsSubjExistInDataTable(Convert.ToString(item("ACM_ID").Text()), dt7)

                        If exists Then
                            TryCast(item.FindControl("chk_select"), CheckBox).Enabled = False
                        End If

                    Next

                Else
                    Dim dt6 As DataTable = ViewAmbassadorProgramDetails(5, ViewState("APH_ID"))
                    Dim APH_ACM_IDS As String = ""

                    For Each drow As DataRow In dt6.Rows
                        txt_Remarks2.InnerText = drow.Item("APH_LEVEL1_COMMENT")
                        Dim status As String = drow.Item("APH_LEVEL1_STATUS")
                        APH_ACM_IDS = drow.Item("APH_ACM_IDS")
                        If status = "A" Or status = "R" Then
                            'gv_stu_details.Enabled = False
                            gv_chklistdetails.Enabled = False
                            txt_Remarks2.Disabled = True
                            btn_Process.Visible = False
                            btn_Reject.Visible = False
                        End If
                        If status = "A" Then
                            'lblMessage.Text = "&#10004; Approved and forwarded to Finance"
                            ShowMessage("&#10004; Approved and forwarded to Finance", 0)
                            divNote.Visible = True
                            divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                            lblError.Text = "<div>&#10004; Approved and forwarded to Finance</div>"
                        End If
                        If status = "R" Then
                            'lblMessage.Text = "&#10060; Registrar Rejected"
                            ShowMessage("&#10060; Rejected by Registrar", 1)
                            divNote.Visible = True
                            divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                            lblError.Text = "<div>&#10060; Rejected by Registrar</div>"
                        End If
                    Next

                    'gv_stu_details.Enabled = False
                    lbl_Remarks1.Enabled = False
                    hdr_Remarks1.Enabled = False
                    hdr_Remarks1.Text = ""



                    Dim exists As Boolean = False
                    Dim dt7 As DataTable = ViewAmbassadorChecklist(4, 1)
                    For Each item As GridDataItem In gv_chklistdetails.MasterTableView.Items

                        Dim StrArr As String() = getPipedData(APH_ACM_IDS)
                        Dim strFound = Array.Find(StrArr, Function(str) str.Equals(Convert.ToString(item("ACM_ID").Text())))
                        exists = dt7.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = strFound)
                        If exists Then
                            TryCast(item.FindControl("chk_select"), CheckBox).Checked = True
                            item.BackColor = Drawing.Color.LightYellow
                        End If

                        exists = dt7.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = Convert.ToString(item("ACM_ID").Text()))
                        'exists = FuncIsSubjExistInDataTable(Convert.ToString(item("ACM_ID").Text()), dt7)

                        If Not exists Then
                            TryCast(item.FindControl("chk_select"), CheckBox).Enabled = False
                        End If


                    Next


                End If

                If Integer.Parse(Convert.ToString(ViewState("LEVEL"))) = 1 Then

                    btn_Process.Text = "Forward to Finance"
                End If

                If Integer.Parse(Convert.ToString(ViewState("LEVEL"))) = 2 Then

                    btn_Process.Text = "Forward to Principal"
                End If

            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Public Shared Function ViewAmbassadorProgramDetails(ByVal OPTIONS As Integer, ByVal APH_ID As Integer, Optional ByVal STU_NO As String = "") As DataTable
        Dim pParms(3) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@APH_ID", SqlDbType.Int)
        pParms(1).Value = APH_ID
        pParms(2) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 50)
        pParms(2).Value = STU_NO


        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GET_AMBASSADOR_PGM_STUDENT_DETAILS", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Public Shared Function ViewAmbassadorChecklist(ByVal OPTIONS As Integer, Optional ByVal LEVEL As Integer = 0) As DataTable
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@LEVEL", SqlDbType.Int)
        pParms(1).Value = LEVEL


        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GET_AMBASSADOR_CHECKLIST", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Protected Sub btn_Process_Click(sender As Object, e As EventArgs) Handles btn_Process.Click

        divNote.Visible = False
        lblError.Text = ""

        Dim count As Integer = -1
        Dim COUNT_CHECK_LIST As ArrayList = New ArrayList()
        Dim strKey As String = ""
        Dim BSU_ID As String = ""
        For Each item As GridDataItem In gv_chklistdetails.MasterTableView.Items

            Dim CheckBox1 As CheckBox = CType(item.FindControl("chk_select"), CheckBox)
            If CheckBox1.Checked Then
                COUNT_CHECK_LIST.Add(item("ACM_DESCR").Text())
                If strKey <> "" Then
                    strKey = strKey & "|"
                End If
                strKey = strKey & Convert.ToString(item("ACM_ID").Text())  ' item.GetDataKeyValue("ACM_ID").ToString()
            End If
        Next



        If ViewState("LEVEL") = "1" Then

            Dim dt3 As DataTable = ViewAmbassadorChecklist(3, 1)
            For Each row As DataRow In dt3.Rows
                count = row.Item("COUNT")
            Next
        ElseIf ViewState("LEVEL") = "2" Then

            Dim dt3 As DataTable = ViewAmbassadorChecklist(2)
            For Each row As DataRow In dt3.Rows
                count = row.Item("COUNT")
            Next
        ElseIf ViewState("LEVEL") = "3" Then

            Dim dt3 As DataTable = ViewAmbassadorChecklist(2)
            For Each row As DataRow In dt3.Rows
                count = row.Item("COUNT")
            Next
        End If
        If COUNT_CHECK_LIST.Count = count Then


            Dim dt As DataTable = ViewAmbassadorProgramDetails(1, ViewState("APH_ID"))

            For Each row As DataRow In dt.Rows

                BSU_ID = row.Item("BSU_ID")
                Dim dt2 As DataTable = GetStudentDetails(row.Item("BSU_ID"), row.Item("STU_NO"))
                For Each row2 As DataRow In dt2.Rows
                    ViewState("STU_ID") = row2.Item("STU_ID")
                Next

            Next

            'For avoiding email resending while working with 2 windows
            Dim statusValue As String = ""
            If Integer.Parse(Convert.ToString(ViewState("LEVEL"))) = 1 Then
                statusValue = GET_LEVEL_STATUS(ViewState("APH_ID"), 1)
            ElseIf Integer.Parse(Convert.ToString(ViewState("LEVEL"))) = 2 Then
                statusValue = GET_LEVEL_STATUS(ViewState("APH_ID"), 2)
            ElseIf Integer.Parse(Convert.ToString(ViewState("LEVEL"))) = 3 Then
                statusValue = GET_LEVEL_STATUS(ViewState("APH_ID"), 3)
            End If

            If statusValue <> "F" Then
                Dim returnValue As Integer = UPDATE_AMBASSADOR_PROGRAM_H(Integer.Parse(Convert.ToString(ViewState("LEVEL"))), Integer.Parse(ViewState("APH_ID")), "A", ViewState("USER_EMP_ID"), txt_Remarks2.InnerText, Integer.Parse(ViewState("STU_ID")), strKey, Decimal.Parse(ViewState("REWARD_AMOUNT")), Decimal.Parse(ViewState("REWARD_POINTS")))
                If returnValue = 0 Then
                    If Integer.Parse(Convert.ToString(ViewState("LEVEL"))) = 1 Then
                        'lblMessage.Text = "&#10004; Approved and forwarded to Finance"
                        ShowMessage("&#10004; Approved and forwarded to Finance", 0)
                        divNote.Visible = True
                        divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                        lblError.Text = "<div>&#10004; Approved and forwarded to Finance </div>"
                        SendEmailToFinance(BSU_ID)
                        btn_Process.Visible = False
                        btn_Reject.Visible = False
                        reloadData() 'usually LEVEL 1 is enabled=false for student and checklist grid, so no need to reload
                    End If
                    If Integer.Parse(Convert.ToString(ViewState("LEVEL"))) = 2 Then
                        'lblMessage.Text = "&#10004; Approved the Ambassador Program Discount"
                        ShowMessage("&#10004; Approved and forwarded to Principal", 0)
                        divNote.Visible = True
                        divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                        lblError.Text = "<div>&#10004; Approved and forwarded to Principal </div>"
                        SendEmailToPrincipal(BSU_ID)
                        btn_Process.Visible = False
                        btn_Reject.Visible = False
                        reloadData()
                    End If
                    If Integer.Parse(Convert.ToString(ViewState("LEVEL"))) = 3 Then
                        'lblMessage.Text = "&#10004; Approved the Ambassador Program Discount"
                        ShowMessage("&#10004; Approved by Principal", 0)
                        divNote.Visible = True
                        divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                        lblError.Text = "<div>&#10004; Approved by Principal </div>"
                        btn_Process.Visible = False
                        btn_Reject.Visible = False
                        reloadData()
                    End If
                End If
            Else
                reloadData()
            End If

        Else
            'lblError.Text = "Please select all checklist for confirmation!!!"
            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoError"
            lblError.Text = "<div>Please select all checklist for confirmation!!!</div>"
        End If




    End Sub

    Protected Sub btn_Reject_Click(sender As Object, e As EventArgs) Handles btn_Reject.Click

        divNote.Visible = False
        lblError.Text = ""

        Dim count As Integer = -1
        Dim COUNT_CHECK_LIST As ArrayList = New ArrayList()
        Dim strKey As String = ""
        For Each item As GridDataItem In gv_chklistdetails.MasterTableView.Items

            Dim CheckBox1 As CheckBox = CType(item.FindControl("chk_select"), CheckBox)
            If CheckBox1.Checked Then
                COUNT_CHECK_LIST.Add(item("ACM_DESCR").Text())
                If strKey <> "" Then
                    strKey = strKey & "|"
                End If
                strKey = strKey & Convert.ToString(item("ACM_ID").Text())
            End If
        Next

        Dim dt3 As DataTable = ViewAmbassadorChecklist(2)

        For Each row As DataRow In dt3.Rows
            count = row.Item("COUNT")
        Next

        ''Not required for reject cases
        'If COUNT_CHECK_LIST.Count = count Then


        Dim dt As DataTable = ViewAmbassadorProgramDetails(1, ViewState("APH_ID"))

        For Each row As DataRow In dt.Rows

            Dim dt2 As DataTable = GetStudentDetails(row.Item("BSU_ID"), row.Item("STU_NO"))
            For Each row2 As DataRow In dt2.Rows
                ViewState("STU_ID") = row2.Item("STU_ID")
            Next

        Next

        Dim returnValue As Integer = UPDATE_AMBASSADOR_PROGRAM_H(Integer.Parse(Convert.ToString(ViewState("LEVEL"))), Integer.Parse(ViewState("APH_ID")), "R", ViewState("USER_EMP_ID"), txt_Remarks2.InnerText, Integer.Parse(ViewState("STU_ID")), strKey, Decimal.Parse(ViewState("REWARD_AMOUNT")), Decimal.Parse(ViewState("REWARD_POINTS")))

        If returnValue = 0 Then
            If Integer.Parse(Convert.ToString(ViewState("LEVEL"))) = 1 Then
                'lblMessage.Text = "&#10060; Registrar Rejected"
                ShowMessage("&#10060; Rejected by Registrar", 1)
                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                lblError.Text = "<div>&#10060; Rejected by Registrar </div>"
                btn_Process.Visible = False
                btn_Reject.Visible = False
                'reloadData() 'usually LEVEL 1 is enabled=false for student and checklist grid, so no need to reload
            End If
            If Integer.Parse(Convert.ToString(ViewState("LEVEL"))) = 2 Then
                'lblMessage.Text = "&#10060; Finance Rejected"
                ShowMessage("&#10060; Rejected by Finance", 1)
                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                lblError.Text = "<div>&#10060; Rejected by Finance </div>"
                btn_Process.Visible = False
                btn_Reject.Visible = False
                reloadData()
            End If
            If Integer.Parse(Convert.ToString(ViewState("LEVEL"))) = 3 Then
                'lblMessage.Text = "&#10060; Finance Rejected"
                ShowMessage("&#10060; Rejected by Principal", 1)
                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                lblError.Text = "<div>&#10060; Rejected by Principal </div>"
                btn_Process.Visible = False
                btn_Reject.Visible = False
                reloadData()
            End If
        Else
            'lblError.Text = "Error while processing the request !!!"
            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoError"
            lblError.Text = "<div>Error while processing the request !!! </div>"


        End If
        'Else
        '    lblError.Text = "Please select all checklist for confirmation!!!"
        'End If

    End Sub
    Public Shared Function UPDATE_AMBASSADOR_PROGRAM_H(ByVal LEVEL As Integer, ByVal APH_ID As Integer, ByVal APH_LEVEL_STATUS As String,
                                                       ByVal APH_LEVEL_USER_EMP_ID As String, ByVal APH_LEVEL_COMMENT As String,
                                                       ByVal APH_REFERRER_STU_ID As String, ByVal APH_ACM_IDS As String, ByVal APH_REWARD_AMOUNT As Decimal, ByVal APH_REWARD_POINTS As Decimal) As Integer


        Dim pParms(10) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@LEVEL", SqlDbType.Int)
        pParms(0).Value = LEVEL
        pParms(1) = New SqlClient.SqlParameter("@APH_ID", SqlDbType.Int)
        pParms(1).Value = APH_ID
        pParms(2) = New SqlClient.SqlParameter("@APH_LEVEL_STATUS", SqlDbType.VarChar, 50)
        pParms(2).Value = APH_LEVEL_STATUS
        pParms(3) = New SqlClient.SqlParameter("@APH_LEVEL_USER_EMP_ID", SqlDbType.VarChar, 50)
        pParms(3).Value = APH_LEVEL_USER_EMP_ID
        pParms(4) = New SqlClient.SqlParameter("@APH_LEVEL_COMMENT", SqlDbType.VarChar, 500)
        pParms(4).Value = APH_LEVEL_COMMENT
        pParms(5) = New SqlClient.SqlParameter("@APH_REFERRER_STU_ID", SqlDbType.VarChar, 50)
        pParms(5).Value = APH_REFERRER_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@APH_ACM_IDS", SqlDbType.VarChar, 100)
        pParms(6).Value = APH_ACM_IDS
        pParms(7) = New SqlClient.SqlParameter("@APH_REWARD_AMOUNT", SqlDbType.Decimal)
        pParms(7).Value = APH_REWARD_AMOUNT
        pParms(8) = New SqlClient.SqlParameter("@APH_REWARD_POINTS", SqlDbType.Decimal)
        pParms(8).Value = APH_REWARD_POINTS
        pParms(9) = New SqlClient.SqlParameter("@OUT_VALUE", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.Output
        pParms(10) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.ReturnValue

        Dim ReturnFlag As Integer = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnection, _
          CommandType.StoredProcedure, "FEES.UPDATE_AMBASSADOR_PROGRAM_H", pParms)
        Dim ReturnValue As Integer = pParms(10).Value
        Return ReturnValue

    End Function

    Public Shared Function GetStudentDetails(ByVal sBsuid As String, ByVal stu_no As String) As DataTable

        Dim str_Sql As String = "select STU_PHOTOPATH,STU_ID from [OASIS_FEES].[dbo].[STUDENT_M] where stu_no='" & stu_no & "' AND STU_BSU_ID='" & sBsuid & "'"

        Dim ds As New DataSet


        If str_Sql <> "" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
                                          CommandType.Text, str_Sql)
        End If
        Return ds.Tables(0)
    End Function

    Public Shared Function GetEmployeeDetails(ByVal EMP_ID As String) As DataTable

        Dim str_Sql As String = "SELECT CONCAT(EMP_SALUTE,EMP_FNAME,' ',EMP_MNAME,' ',EMP_LNAME) AS EMP_NAME FROM [OASIS].[DBO].EMPLOYEE_M WHERE  EMP_ID ='" & EMP_ID & "'"

        Dim ds As New DataSet


        If str_Sql <> "" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
                                          CommandType.Text, str_Sql)
        End If
        Return ds.Tables(0)
    End Function

    Public Shared Function GetParentDetails(ByVal OPTIONS As Integer, ByVal STU_NO As String) As DataTable
        Dim pParms(2) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 50)
        pParms(1).Value = STU_NO

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
          CommandType.StoredProcedure, "DBO.GET_STUDENT_PARENT_DETAILS", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GET_REWARDS_CALCULATION(ByVal OPTIONS As Integer, ByVal AMOUNT As Decimal, ByVal BSU_ID As String, ByVal APH_ID As Integer) As DataTable
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@AMOUNT", SqlDbType.Decimal)
        pParms(1).Value = AMOUNT
        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
        pParms(2).Value = BSU_ID
        pParms(3) = New SqlClient.SqlParameter("@APH_ID", SqlDbType.Int)
        pParms(3).Value = APH_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "[FEES].GET_REWARDS_CALCULATION", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Protected Sub rdSelect_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

        Try
            For Each item As GridDataItem In gv_stu_details.MasterTableView.Items

                Dim radioButton As RadioButton = CType(item.FindControl("rdSelect"), RadioButton)

                If TryCast(item.FindControl("rdSelect"), RadioButton).Checked Then
                    If Convert.ToString(Session("RadioCheckValue")) <> Convert.ToString(item("STU_NO").Text()) Then
                        Session("CurrentRadioCheckValue") = Convert.ToString(item("STU_NO").Text())
                        'item.BackColor = Drawing.Color.Orange   'No need to show selected row in highlighted color
                        'We are commenting below 2 lines because Calculate GEMS Rewards Amount and Point from Referred Student Yearly Tuition Fee
                        'lbl_Note.Text = "GEMS Rewards Point Amount is " & getPercentage(Convert.ToDecimal(item("FEE_AMOUNT").Text()), PERCENT_VALUE)
                        'ViewState("REWARD_AMOUNT") = getPercentage(Convert.ToDecimal(item("FEE_AMOUNT").Text()), PERCENT_VALUE)
                    ElseIf Convert.ToString(Session("RadioCheckValue")) = Convert.ToString(item("STU_NO").Text()) Then
                        TryCast(item.FindControl("rdSelect"), RadioButton).Checked = False
                        item.BackColor = Drawing.Color.White
                    End If

                End If
            Next

            Session("RadioCheckValue") = Convert.ToString(Session("CurrentRadioCheckValue"))
            Dim dt4 As DataTable = ViewAmbassadorProgramDetails(4, ViewState("APH_ID"), Convert.ToString(Session("CurrentRadioCheckValue")))

            For Each row As DataRow In dt4.Rows

                Dim dt5 As DataTable = GetParentDetails(1, row.Item("STU_NO"))

                For Each row2 As DataRow In dt5.Rows
                    P_FName.Text = row2.Item("FNAME")
                    P_FContact.Text = row2.Item("FMOBILE")
                    P_FCompany.Text = row2.Item("FCOMPANY")
                    P_MName.Text = row2.Item("MNAME")
                    P_MContact.Text = row2.Item("MMOBILE")
                    P_MCompany.Text = row2.Item("MCOMPANY")
                    P_PRI_Contact_F.Text = row2.Item("PRIMAY_CONTACT")
                    P_PRI_Contact_M.Text = row2.Item("PRIMAY_CONTACT")
                    lbl_Email.Text = row2.Item("PARENT_EMAIL")
                    lbl_UserName.Text = row2.Item("PARENT_USERNAME")
                Next
            Next
        Catch ex As Exception
            lblError.Text = ex.Message

        End Try

    End Sub
    Public Shared Function GetDateFormat(ByVal date_f As DateTime) As String
        Dim format As String = "dd/MMM/yyyy"
        Return date_f.ToString(format)
    End Function

    Public Function FuncIsSubjExistInDataTable(ByVal subjID As String, ByVal dt As DataTable) As Boolean
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)(0).ToString() = subjID Then Return True
        Next

        Return False
    End Function


    Public Function getPipedData(ByVal strPipe As String) As String()
        Dim strArr() As String
        strArr = strPipe.Split("|")
        Return strArr
    End Function

    Public Function getPercentage(ByVal TotalValue As Decimal, ByVal PercentValue As Decimal) As Decimal
        Dim Value As Decimal
        Value = (TotalValue * PercentValue) / 100
        Return Value
    End Function


    Protected Sub SendEmailToFinance(ByVal BSU_ID As String)
        Dim Mailstatus As String = ""
        Dim APH_SOURCE As String = "0"
        Try

            Dim dt6 As DataTable = GET_EMPLOYEE_DETAILS(1, BSU_ID)

            For Each drow As DataRow In dt6.Rows

                Dim ToEmailId As String = ""
                Dim ToEMPName As String = ""
                ToEmailId = drow.Item("EMP_EMAIL")
                'ToEmailId = "shakeel.shakkeer@gemseducation.com"
                If ToEmailId = "" Then
                    ToEmailId = "shakeel.shakkeer@gemseducation.com"
                End If
                Dim dt4 As DataTable = ViewAmbassadorProgramDetails(5, ViewState("APH_ID"))
                For Each row As DataRow In dt4.Rows
                    APH_SOURCE = row.Item("APH_SOURCE")
                Next

                Dim Subject As String = "GEMS AMBASSADOR PROGRAM - FINANCE CONFIRMATION"
               

                Dim Email_Text As String = getEmailText(1)
                Email_Text = Email_Text.Replace(" $$$$$$$$$$ ", drow.Item("EMP_NAME"))

                If APH_SOURCE = "EMPLOYEE" Then
                    Subject = "GEMS AMBASSADOR PROGRAM - FINANCE CONFIRMATION (Referred by Staff)"
                    Email_Text = Email_Text.Replace(" FFFF_REFERRING_FFFF ", "(Referred by Staff)")
                ElseIf APH_SOURCE = "PARENT" Then
                    Subject = "GEMS AMBASSADOR PROGRAM - FINANCE CONFIRMATION (Referred by Parent)"
                    Email_Text = Email_Text.Replace(" FFFF_REFERRING_FFFF ", "(Referred by Parent)")
                End If

                Email_Text = Email_Text.Replace(" AAAA_EMP_ID_AAAA ", Encr_decrData.Encrypt(drow.Item("EMP_ID")))
                Email_Text = Email_Text.Replace(" BBBB_APH_ID_BBBB ", Encr_decrData.Encrypt(ViewState("APH_ID")))
                Email_Text = Email_Text.Replace(" CCCC_LEVEL_CCCC ", Encr_decrData.Encrypt("2"))

                Dim dt As DataTable = ViewAmbassadorProgramDetails(1, ViewState("APH_ID"))
                For Each drow22 As DataRow In dt.Rows
                    Email_Text = Email_Text.Replace(" DDDD_REFERRED_DDDD ", "Name: " & drow22.Item("STU_NAME"))
                    Email_Text = Email_Text.Replace(" EEEE_REFERRED_EEEE ", "Grade: " & drow22.Item("GRD_DISPLAY"))
                Next

                'Dim ds2 As New DataSet
                'ds2 = GetCommunicationSettings(BSU_ID)

                'Dim username = ""
                'Dim password = ""
                'Dim port = ""
                'Dim host = ""
                'Dim fromemailid = ""
                'If ds2.Tables(0).Rows.Count > 0 Then
                '    fromemailid = ds2.Tables(0).Rows(0).Item("BSC_FROMEMAIL").ToString()
                '    username = ds2.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
                '    password = ds2.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
                '    port = ds2.Tables(0).Rows(0).Item("BSC_PORT").ToString()
                '    host = ds2.Tables(0).Rows(0).Item("BSC_HOST").ToString()
                'End If
                Try
                    'Mailstatus = EmailService.email.SendPlainTextEmails(fromemailid, ToEmailId, Subject, Email_Text, username, password, host, port, 0, False)
                    Dim ReturnValue As Integer = InsertIntoEmailSendSchedule(BSU_ID, "AMBASSADOR_PROGRAM", "SYSTEM", ToEmailId, Subject, Email_Text)
                Catch ex As Exception
                    lblError.Text = ex.Message.ToString
                Finally

                End Try

            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub SendEmailToPrincipal(ByVal BSU_ID As String)
        Dim Mailstatus As String = ""
        Dim APH_SOURCE As String = "0"
        Try

            Dim dt6 As DataTable = GET_EMPLOYEE_DETAILS(3, BSU_ID)

            For Each drow As DataRow In dt6.Rows

                Dim ToEmailId As String = ""
                Dim ToEMPName As String = ""
                ToEmailId = drow.Item("EMP_EMAIL")
                'ToEmailId = "shakeel.shakkeer@gemseducation.com"
                If ToEmailId = "" Then
                    ToEmailId = "shakeel.shakkeer@gemseducation.com"
                End If
                Dim dt4 As DataTable = ViewAmbassadorProgramDetails(5, ViewState("APH_ID"))
                For Each row As DataRow In dt4.Rows
                    APH_SOURCE = row.Item("APH_SOURCE")
                Next


                Dim Subject As String = "GEMS AMBASSADOR PROGRAM - PRINCIPAL CONFIRMATION"
                Dim Email_Text As String = getEmailText(2)
                Email_Text = Email_Text.Replace(" $$$$$$$$$$ ", drow.Item("EMP_NAME"))
                If APH_SOURCE = "EMPLOYEE" Then
                    Subject = "GEMS AMBASSADOR PROGRAM - PRINCIPAL CONFIRMATION (Referred by Staff)"
                    Email_Text = Email_Text.Replace(" FFFF_REFERRING_FFFF ", "(Referred by Staff)")
                ElseIf APH_SOURCE = "PARENT" Then
                    Subject = "GEMS AMBASSADOR PROGRAM - PRINCIPAL CONFIRMATION (Referred by Parent)"
                    Email_Text = Email_Text.Replace(" FFFF_REFERRING_FFFF ", "(Referred by Parent)")
                End If

                Email_Text = Email_Text.Replace(" AAAA_EMP_ID_AAAA ", Encr_decrData.Encrypt(drow.Item("EMP_ID")))
                Email_Text = Email_Text.Replace(" BBBB_APH_ID_BBBB ", Encr_decrData.Encrypt(ViewState("APH_ID")))
                Email_Text = Email_Text.Replace(" CCCC_LEVEL_CCCC ", Encr_decrData.Encrypt("3"))

                Dim dt As DataTable = ViewAmbassadorProgramDetails(1, ViewState("APH_ID"))
                For Each drow22 As DataRow In dt.Rows
                    Email_Text = Email_Text.Replace(" DDDD_REFERRED_DDDD ", "Referred Student Name:" & drow22.Item("STU_NAME"))
                    Email_Text = Email_Text.Replace(" EEEE_REFERRED_EEEE ", "Referred Student Grade: " & drow22.Item("GRD_DISPLAY"))
                Next


                Try
                    Dim ReturnValue As Integer = InsertIntoEmailSendSchedule(BSU_ID, "AMBASSADOR_PROGRAM", "SYSTEM", ToEmailId, Subject, Email_Text)
                Catch ex As Exception
                    lblError.Text = ex.Message.ToString
                Finally

                End Try

            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Function GetCommunicationSettings(ByVal BSU_ID As String) As DataSet
        Dim ds As DataSet
        Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Dim Sql_Query = "select * from [OASIS].[DBO].BSU_COMMUNICATION_M where BSC_BSU_ID = '" & BSU_ID & "' and BSC_TYPE ='COM' "
        ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
        Return ds
    End Function

    Public Sub reloadData()

        If ViewState("APH_SOURCE") = "PARENT" Then
            Dim dt4 As DataTable = ViewAmbassadorProgramDetails(3, ViewState("APH_ID"))

            For Each drow As DataRow In dt4.Rows
                Session("RadioCheckValue") = Convert.ToString(drow.Item("STU_NO"))
                Try
                    For Each item As GridDataItem In gv_stu_details.MasterTableView.Items

                        Dim radioButton As RadioButton = CType(item.FindControl("rdSelect"), RadioButton)

                        If Convert.ToString(Session("RadioCheckValue")) = Convert.ToString(item("STU_NO").Text()) Then
                            TryCast(item.FindControl("rdSelect"), RadioButton).Checked = True
                            Session("CurrentRadioCheckValue") = Convert.ToString(item("STU_NO").Text())
                            'item.BackColor = Drawing.Color.Orange 'No need to show selected row in highlighted color
                            'We are commenting below 2 lines because Calculate GEMS Rewards Amount and Point from Referred Student Yearly Tuition Fee
                            'lbl_Note.Text = "GEMS Rewards: Amount(AED) - " & getPercentage(Convert.ToDecimal(item("FEE_AMOUNT").Text()), PERCENT_VALUE) & " / Points - " & 11 * Convert.ToInt32(getPercentage(Convert.ToDecimal(item("FEE_AMOUNT").Text()), PERCENT_VALUE)) & ""
                            'ViewState("REWARD_AMOUNT") = getPercentage(Convert.ToDecimal(item("FEE_AMOUNT").Text()), PERCENT_VALUE)
                            If ViewState("LEVEL") = "1" Then
                                TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False
                            ElseIf ViewState("LEVEL") = "2" Then
                                Dim dt11 As DataTable = ViewAmbassadorProgramDetails(5, ViewState("APH_ID"))
                                For Each row2 As DataRow In dt11.Rows
                                    Dim status As String = row2.Item("APH_LEVEL2_STATUS")
                                    If status = "A" Or status = "R" Then
                                        TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False
                                    Else
                                        TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False 'we are disabling for LEVEL2 also
                                    End If
                                Next
                            ElseIf ViewState("LEVEL") = "3" Then
                                TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False
                            End If
                        ElseIf Convert.ToString(Session("RadioCheckValue")) <> Convert.ToString(item("STU_NO").Text()) Then
                            TryCast(item.FindControl("rdSelect"), RadioButton).Checked = False
                            item.BackColor = Drawing.Color.White
                            If ViewState("LEVEL") = "1" Then
                                TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False
                            ElseIf ViewState("LEVEL") = "2" Then
                                Dim dt11 As DataTable = ViewAmbassadorProgramDetails(5, ViewState("APH_ID"))
                                For Each row2 As DataRow In dt11.Rows
                                    Dim status As String = row2.Item("APH_LEVEL2_STATUS")
                                    If status = "A" Or status = "R" Then
                                        TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False
                                    Else
                                        TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False 'we are disabling for LEVEL2 also
                                    End If
                                Next
                            ElseIf ViewState("LEVEL") = "3" Then
                                TryCast(item.FindControl("rdSelect"), RadioButton).Enabled = False
                            End If
                        End If

                    Next

                Catch ex As Exception
                    lblError.Text = ex.Message

                End Try

                Dim dt5 As DataTable = GetParentDetails(1, drow.Item("STU_NO"))

                For Each row2 As DataRow In dt5.Rows
                    P_FName.Text = row2.Item("FNAME")
                    P_FContact.Text = row2.Item("FMOBILE")
                    P_FCompany.Text = row2.Item("FCOMPANY")
                    P_MName.Text = row2.Item("MNAME")
                    P_MContact.Text = row2.Item("MMOBILE")
                    P_MCompany.Text = row2.Item("MCOMPANY")
                    P_PRI_Contact_F.Text = row2.Item("PRIMAY_CONTACT")
                    P_PRI_Contact_M.Text = row2.Item("PRIMAY_CONTACT")
                    lbl_Email.Text = row2.Item("PARENT_EMAIL")
                    lbl_UserName.Text = row2.Item("PARENT_USERNAME")
                Next
            Next
        End If

        If ViewState("LEVEL") = "2" Then
            lbl_Remarks1.Enabled = True
            hdr_Remarks1.Enabled = True
            hdr_Remarks1.Text = "Registrar Comments: "
            Dim APH_ACM_IDS As String = ""
            Dim dt6 As DataTable = ViewAmbassadorProgramDetails(5, ViewState("APH_ID"))
            For Each drow As DataRow In dt6.Rows
                lbl_Remarks1.Text = drow.Item("APH_LEVEL1_COMMENT")
                Dim status As String = drow.Item("APH_LEVEL2_STATUS")
                APH_ACM_IDS = drow.Item("APH_ACM_IDS")
                If status = "A" Or status = "R" Then
                    'gv_stu_details.Enabled = False
                    gv_chklistdetails.Enabled = False
                    txt_Remarks2.Disabled = True
                    btn_Process.Visible = False
                    btn_Reject.Visible = False
                End If
                If status = "A" Then
                    txt_Remarks2.InnerText = drow.Item("APH_LEVEL2_COMMENT")
                    lblMessage.Text = " &#10004; Approved and forwarded to Principal"
                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                    lblError.Text = "<div>&#10004; Approved and forwarded to Principal </div>"
                End If
                If status = "R" Then
                    txt_Remarks2.InnerText = drow.Item("APH_LEVEL2_COMMENT")
                    lblMessage.Text = "&#10060; Rejected by Finance"
                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                    lblError.Text = "<div>&#10060; Rejected by Finance</div>"
                End If


            Next

            Dim exists As Boolean = False

            Dim dt9 As DataTable = ViewAmbassadorChecklist(4, 1)
            For Each item As GridDataItem In gv_chklistdetails.MasterTableView.Items

                Dim StrArr As String() = getPipedData(APH_ACM_IDS)
                Dim strFound = Array.Find(StrArr, Function(str) str.Equals(Convert.ToString(item("ACM_ID").Text())))
                exists = dt9.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = strFound)
                If exists Then
                    TryCast(item.FindControl("chk_select"), CheckBox).Checked = True
                    item.BackColor = Drawing.Color.LightYellow
                End If

            Next

            Dim dt7 As DataTable = ViewAmbassadorChecklist(4, 2)
            For Each item As GridDataItem In gv_chklistdetails.MasterTableView.Items

                Dim StrArr As String() = getPipedData(APH_ACM_IDS)
                Dim strFound = Array.Find(StrArr, Function(str) str.Equals(Convert.ToString(item("ACM_ID").Text())))
                exists = dt7.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = strFound)
                If exists Then
                    TryCast(item.FindControl("chk_select"), CheckBox).Checked = True
                    'item.BackColor = Drawing.Color.LightYellow
                End If

                exists = dt7.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = Convert.ToString(item("ACM_ID").Text()))
                'exists = FuncIsSubjExistInDataTable(Convert.ToString(item("ACM_ID").Text()), dt7)

                If Not exists Then
                    TryCast(item.FindControl("chk_select"), CheckBox).Enabled = False
                End If

            Next
        ElseIf ViewState("LEVEL") = "3" Then
            lbl_Remarks1.Enabled = True
            hdr_Remarks1.Enabled = True
            hdr_Remarks1.Text = "Registrar Comments: "
            lbl_Remarks2.Enabled = True
            hdr_Remarks2.Enabled = True
            hdr_Remarks2.Text = "Finance Comments: "
            Dim APH_ACM_IDS As String = ""
            Dim dt6 As DataTable = ViewAmbassadorProgramDetails(5, ViewState("APH_ID"))
            For Each drow As DataRow In dt6.Rows
                lbl_Remarks1.Text = drow.Item("APH_LEVEL1_COMMENT")
                lbl_Remarks2.Text = drow.Item("APH_LEVEL2_COMMENT")
                Dim status As String = drow.Item("APH_LEVEL3_STATUS")
                APH_ACM_IDS = drow.Item("APH_ACM_IDS")
                If status = "A" Or status = "R" Then
                    'gv_stu_details.Enabled = False
                    gv_chklistdetails.Enabled = False
                    txt_Remarks2.Disabled = True
                    btn_Process.Visible = False
                    btn_Reject.Visible = False
                End If
                If status = "A" Then
                    txt_Remarks2.InnerText = drow.Item("APH_LEVEL3_COMMENT")
                    lblMessage.Text = " &#10004; Approved by Principal"
                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                    lblError.Text = "<div>&#10004; Approved by Principal </div>"
                End If
                If status = "R" Then
                    txt_Remarks2.InnerText = drow.Item("APH_LEVEL3_COMMENT")
                    lblMessage.Text = "&#10060; Rejected by Principal"
                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                    lblError.Text = "<div>&#10060; Rejected by Principal</div>"
                End If


            Next

            Dim exists As Boolean = False

            Dim dt9 As DataTable = ViewAmbassadorChecklist(4, 1)
            For Each item As GridDataItem In gv_chklistdetails.MasterTableView.Items

                Dim StrArr As String() = getPipedData(APH_ACM_IDS)
                Dim strFound = Array.Find(StrArr, Function(str) str.Equals(Convert.ToString(item("ACM_ID").Text())))
                exists = dt9.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = strFound)
                If exists Then
                    TryCast(item.FindControl("chk_select"), CheckBox).Checked = True
                    item.BackColor = Drawing.Color.LightYellow
                End If

                exists = dt9.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = Convert.ToString(item("ACM_ID").Text()))
                'exists = FuncIsSubjExistInDataTable(Convert.ToString(item("ACM_ID").Text()), dt7)

                If exists Then
                    TryCast(item.FindControl("chk_select"), CheckBox).Enabled = False
                End If

            Next

            Dim dt7 As DataTable = ViewAmbassadorChecklist(4, 2)
            For Each item As GridDataItem In gv_chklistdetails.MasterTableView.Items

                Dim StrArr As String() = getPipedData(APH_ACM_IDS)
                Dim strFound = Array.Find(StrArr, Function(str) str.Equals(Convert.ToString(item("ACM_ID").Text())))
                exists = dt7.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = strFound)
                If exists Then
                    TryCast(item.FindControl("chk_select"), CheckBox).Checked = True
                    item.BackColor = Drawing.Color.LightYellow
                End If

                exists = dt7.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = Convert.ToString(item("ACM_ID").Text()))
                'exists = FuncIsSubjExistInDataTable(Convert.ToString(item("ACM_ID").Text()), dt7)

                If exists Then
                    TryCast(item.FindControl("chk_select"), CheckBox).Enabled = False
                End If

            Next

        Else
            Dim dt6 As DataTable = ViewAmbassadorProgramDetails(5, ViewState("APH_ID"))
            Dim APH_ACM_IDS As String = ""

            For Each drow As DataRow In dt6.Rows
                txt_Remarks2.InnerText = drow.Item("APH_LEVEL1_COMMENT")
                Dim status As String = drow.Item("APH_LEVEL1_STATUS")
                APH_ACM_IDS = drow.Item("APH_ACM_IDS")
                If status = "A" Or status = "R" Then
                    'gv_stu_details.Enabled = False
                    gv_chklistdetails.Enabled = False
                    txt_Remarks2.Disabled = True
                    btn_Process.Visible = False
                    btn_Reject.Visible = False
                End If
                If status = "A" Then
                    'lblMessage.Text = "&#10004; Approved and forwarded to Finance"
                    ShowMessage("&#10004; Approved and forwarded to Finance", 0)
                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                    lblError.Text = "<div>&#10004; Approved and forwarded to Finance </div>"
                End If
                If status = "R" Then
                    'lblMessage.Text = "&#10060; Registrar Rejected"
                    ShowMessage("&#10060; Rejected by Registrar", 1)
                    divNote.Visible = True
                    divNote.Attributes("class") = "msgInfoBox msgInfoWarning"
                    lblError.Text = "<div>&#10060; Rejected by Registrar</div>"
                End If
            Next

            'gv_stu_details.Enabled = False
            lbl_Remarks1.Enabled = False
            hdr_Remarks1.Enabled = False
            hdr_Remarks1.Text = ""



            Dim exists As Boolean = False
            Dim dt7 As DataTable = ViewAmbassadorChecklist(4, 1)
            For Each item As GridDataItem In gv_chklistdetails.MasterTableView.Items

                Dim StrArr As String() = getPipedData(APH_ACM_IDS)
                Dim strFound = Array.Find(StrArr, Function(str) str.Equals(Convert.ToString(item("ACM_ID").Text())))
                exists = dt7.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = strFound)
                If exists Then
                    TryCast(item.FindControl("chk_select"), CheckBox).Checked = True
                    item.BackColor = Drawing.Color.LightYellow
                End If

                exists = dt7.[Select]().ToList().Exists(Function(row) row("ACM_ID").ToString() = Convert.ToString(item("ACM_ID").Text()))
                'exists = FuncIsSubjExistInDataTable(Convert.ToString(item("ACM_ID").Text()), dt7)

                If exists Then
                    TryCast(item.FindControl("chk_select"), CheckBox).Enabled = False
                End If


            Next


        End If
    End Sub

    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        If Message <> "" Then
            If bError Then
                lblMessage.CssClass = "diverrorPopUp"
            Else
                lblMessage.CssClass = "divvalidPopUp"
            End If
        Else
            lblMessage.CssClass = ""
        End If
        lblMessage.Text = Message
    End Sub
    Public Shared Function getEmailText(ByVal OPTIONS As Integer) As String
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS



        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GET_AMBASSADOR_EMAIL", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0).Rows(0).Item("EMAIL_TXT")
        Else
            Return ""
        End If
    End Function

    Public Shared Function GET_EMPLOYEE_DETAILS(ByVal OPTIONS As Integer, ByVal BSU_ID As String, Optional ByVal EMP_ID As String = "") As DataTable
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
        pParms(1).Value = BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.VarChar, 50)
        pParms(2).Value = EMP_ID


        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, _
          CommandType.StoredProcedure, "[OASIS].DBO.GET_EMPLOYEE_DETAILS", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function InsertIntoEmailSendSchedule(ByVal EML_BSU_ID As String, ByVal EML_TYPE As String,
                                                     ByVal EML_PROFILE_ID As String, ByVal EML_TOEMAIL As String, ByVal EML_SUBJECT As String, ByVal EML_MESSAGE As String) As Integer


        Dim pParms(10) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@EML_BSU_ID", SqlDbType.VarChar, 50)
        pParms(0).Value = EML_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@EML_TYPE", SqlDbType.VarChar, 50)
        pParms(1).Value = EML_TYPE
        pParms(2) = New SqlClient.SqlParameter("@EML_PROFILE_ID", SqlDbType.VarChar, 500)
        pParms(2).Value = EML_PROFILE_ID
        pParms(3) = New SqlClient.SqlParameter("@EML_TOEMAIL", SqlDbType.VarChar, 500)
        pParms(3).Value = EML_TOEMAIL
        pParms(4) = New SqlClient.SqlParameter("@EML_SUBJECT", SqlDbType.VarChar, 500)
        pParms(4).Value = EML_SUBJECT
        pParms(5) = New SqlClient.SqlParameter("@EML_MESSAGE", SqlDbType.VarChar)
        pParms(5).Value = EML_MESSAGE
        pParms(6) = New SqlClient.SqlParameter("@EML_ID", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.Output

        Dim ReturnFlag As Integer = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnection, _
          CommandType.StoredProcedure, "OASIS.dbo.InsertIntoEmailSendSchedule", pParms)
        Dim ReturnValue As Integer = pParms(6).Value
        Return ReturnValue

    End Function
    Public Shared Function GET_LEVEL_STATUS(ByVal APH_ID As String, ByVal LEVEL As Integer) As String

        Dim returnValue As String = ""
        Dim dt11 As DataTable = ViewAmbassadorProgramDetails(5, APH_ID)
        For Each row2 As DataRow In dt11.Rows
            If LEVEL = 1 Then
                Dim status As String = row2.Item("APH_LEVEL1_STATUS")
                If status = "A" Or status = "R" Then
                    returnValue = "F"
                End If
            End If
            If LEVEL = 2 Then
                Dim status As String = row2.Item("APH_LEVEL2_STATUS")
                If status = "A" Or status = "R" Then
                    returnValue = "F"
                End If
            End If
            If LEVEL = 3 Then
                Dim status As String = row2.Item("APH_LEVEL3_STATUS")
                If status = "A" Or status = "R" Then
                    returnValue = "F"
                End If
            End If
        Next
        Return returnValue
    End Function
    Protected Sub btn_Click(sender As Object, e As EventArgs) Handles btn.Click
        divNote.Visible = False
        lblError.Text = ""
    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

    End Sub

End Class


