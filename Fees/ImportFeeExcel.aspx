<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ImportFeeExcel.aspx.vb" Inherits="Fees_ImportFeeExcel"
    Title="Untitled Page" %> 

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
     

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server" Text="Import Fee"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">                 
              <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                <br />
                 <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tbody>
                                                             
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">Select File</span>
                                                                </td>

                                                                <td align="left" width="30%">
                                                                    <asp:FileUpload ID="FileUpload1" runat="server" accept=".xls"></asp:FileUpload>
                                                                </td>
                                                                <td align="left" colspan="2">
                                                                    <%--<asp:HyperLink ID="lnkXcelFormat" runat="server">Click here to get the formatted Excel file</asp:HyperLink>--%></td>
                                                            </tr>
                                                              
                                                            <tr>
                                                                <td align="center" colspan="4">
                                                                    <asp:Button ID="btnImport" runat="server" Text="Load" CssClass="button"></asp:Button>
                                                                    <asp:Button ID="btnProceedImpt" runat="server" Text="Proceed" CssClass="button" Visible="False"></asp:Button>
                                                                    <asp:Button ID="btnCommitImprt" runat="server" Text="Commit" CssClass="button" Visible="False"></asp:Button>
                                                                </td>
                                                            </tr> 
                                                            <tr>
                                                                <td align="center" colspan="4">
                                                                    <asp:Label ID="lblMessage" runat="server" CssClass="error"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trGvImport" runat="server">
                                                                <td id="Td1" align="left" colspan="4" runat="server">
                                                                    <center>
                                                            <asp:GridView ID="gvExcelImport" runat="server" CssClass="table table-bordered table-row"
                                                                AllowPaging="True" EmptyDataText="No Data" Width="100%" AutoGenerateColumns="false"
                                                                ShowFooter="True" OnPageIndexChanging="gvExcelImport_PageIndexChanging"> 
                                                                <Columns>
                                                                    <asp:BoundField DataField="IRE_School_Name" HeaderText="School Name">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>			
                                                                     <asp:BoundField DataField="IRE_Student_ID" HeaderText="STU NO.">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                     <asp:BoundField DataField="IRE_National_ID" HeaderText="National ID">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                     <asp:BoundField DataField="IRE_STUDENT_NAME" HeaderText="Student Name">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                     <asp:BoundField DataField="IRE_Fee_date1" HeaderText="Fee Date" DataFormatString = "{0:dd/MMM/yyyy}">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                     <asp:BoundField DataField="IRE_FEE_TYPE" HeaderText="Fee Type">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="IRE_Fee_Amount" DataFormatString="{0:n2}" HeaderText="Amount">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                    </asp:BoundField>
                                                                     <asp:BoundField DataField="IRE_Mode_of_payment" HeaderText="Mode Of Payment">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>

                                                                     <asp:BoundField DataField="IRE_Transaction_ref_no" HeaderText="Transaction Ref No">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>

                                                                     <asp:BoundField DataField="IRE_Narration" HeaderText="Narration">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>

                                                                     <asp:BoundField DataField="IRE_Bank" HeaderText="Bank">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="IRE_Receipt_no" HeaderText="Receipt No.">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Error_desc" HeaderText="Error Desc.">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>

<%--  	 						batchno	STU_ID	ACD_ID	GRD_ID	FEE_ID	CLT_ID	REF_ID	BSU_ID	CTY_ID	FCL_ID	Error_desc	Has_Error--%>


                                                        </center>
                                                                </td>
                                                            </tr>
                                                           <tr id="trgvImportSmry" runat="server">
                                                                <td id="Td2" colspan="4" runat="server">
                                                                    <center>
                                                            <asp:GridView ID="gvImportSmry" runat="server" CssClass="table table-bordered table-row"
                                                                AllowPaging="False" EmptyDataText="No Data" Width="100%" AutoGenerateColumns="False"
                                                                PageSize="20" >
                                                                <Columns>
                                                                    <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Description">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="amount" DataFormatString="{0:n2}" HeaderText="Amount">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    
                                                                </Columns>
                                                            </asp:GridView>
                                                        </center>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

            </div>
        </div>
    </div>
</asp:Content>
