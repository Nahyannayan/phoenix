Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class FeeVoidCreditCardTransaction
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            trstudDet.Visible = False
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            txtDate.Text = Format(Now.Date, OASISConstants.DateFormat)
            ddBusinessunit.DataBind()
            ddBusinessunit.SelectedIndex = -1
            ddBusinessunit.Items.FindByValue(Session("sBsuid")).Selected = True
            ViewState("ID") = 1
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "F300228" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        End If
    End Sub

    Function ValidateRecieptNo(ByVal recNo As String, ByVal vBSUID As String) As Boolean
        Dim CommandText As String = "select count(*) from [FEES].[FEECOLLECTION_H] where FCL_RECNO='" & _
        recNo & "' and FCL_STU_BSU_ID = '" & vBSUID & "'"
        Dim vCount As Integer = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, CommandText)
        If vCount > 0 Then
            Return True
        Else
            Return False
        End If
        Return False
    End Function

    Function UpdateStudentDetails(ByVal recNo As String, ByVal vBSUID As String) As Boolean
        trstudDet.Visible = False
        Dim CommandText As String
        If rbEnrollment.Checked Then
            CommandText = "SELECT     VW_OSO_STUDENT_M.STU_NAME, FEES.FEECOLLECTION_H.FCL_AMOUNT " & _
            " FROM FEES.FEECOLLECTION_H LEFT OUTER JOIN  VW_OSO_STUDENT_M ON " & _
            " FEES.FEECOLLECTION_H.FCL_STU_ID = VW_OSO_STUDENT_M.STU_ID where FCL_RECNO='" & _
            recNo & "' and FCL_STU_BSU_ID = '" & vBSUID & "'"
        Else
            CommandText = "SELECT     FEES.vw_OSO_ENQUIRY_COMP.STU_NAME, FEES.FEECOLLECTION_H.FCL_AMOUNT " & _
              " FROM FEES.FEECOLLECTION_H LEFT OUTER JOIN  FEES.vw_OSO_ENQUIRY_COMP ON " & _
              " FEES.FEECOLLECTION_H.FCL_STU_ID = FEES.vw_OSO_ENQUIRY_COMP.STU_ID where FCL_RECNO='" & _
              recNo & "' and FCL_STU_BSU_ID = '" & vBSUID & "'"
        End If

        Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, CommandText)
        While (drReader.Read())
            lblStudentName.Text = drReader("STU_NAME")
            lblAmount.Text = Format(drReader("FCL_AMOUNT"), "0.00")
            trstudDet.Visible = True
        End While

    End Function

    Private Function ChechErrors() As Boolean
        Try
            If CDate(txtDate.Text) > Date.Now Then
                'lblError.Text = "Future Date is not allowed"
                usrMessageBar.ShowNotification("Future Date is not allowed", UserControls_usrMessageBar.WarningType.Danger)
                Return False
            End If
        Catch
            'lblError.Text = "Date is not valid"
            usrMessageBar.ShowNotification("Date is not valid", UserControls_usrMessageBar.WarningType.Danger)
            Return False
        End Try
        Return True
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Not ChechErrors() Then Return
        If Not ValidateRecieptNo(txtRecieptNo.Text, ddBusinessunit.SelectedValue) Then
            'lblError.Text = "Receipt No is not Valid..."
            usrMessageBar.ShowNotification("Receipt No is not Valid...", UserControls_usrMessageBar.WarningType.Danger)
            Return
        End If
        If h_FCO_ID.Value.ToString = "" Or h_FCO_ID.Value.ToString = "0" Then
            'lblError.Text = "Invalid Reference Detail, Please Contact System Administrator..."
            usrMessageBar.ShowNotification("Invalid Reference Detail, Please Contact System Administrator...", UserControls_usrMessageBar.WarningType.Danger)
            Return
        End If

        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Try
            Dim User, Password, Accesscode, MerchantID, MerchTxnRef, MerchDetail, TranNo As String
            Dim DS As New DataSet
            DS = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, _
            "EXEC FEES.GetDataforVoidCCCollection @FCO_ID=" & h_FCO_ID.Value.ToString)
            If DS.Tables.Count > 0 Then
                If DS.Tables(0).Rows.Count > 0 Then
                    User = DS.Tables(0).Rows(0)("BSU_QUERYDRUSER")
                    Password = DS.Tables(0).Rows(0)("BSU_QUERYDRPASSWORD")
                    Accesscode = DS.Tables(0).Rows(0)("BSU_MERCHANTCODE")
                    MerchantID = DS.Tables(0).Rows(0)("BSU_MERCHANTID")
                    MerchTxnRef = DS.Tables(0).Rows(0)("FCO_ID")
                    MerchDetail = DS.Tables(0).Rows(0)("MERCH_DETAIL")
                    TranNo = DS.Tables(0).Rows(0)("FCO_VPC_TRANNO")
                    Dim strResponse As String = ""
                    Dim myWebClient As New System.Net.WebClient
                    Dim ValueCollection As New System.Collections.Specialized.NameValueCollection
                    'ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Command"), System.Web.HttpUtility.UrlEncode("queryDR"))

                    ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Version"), System.Web.HttpUtility.UrlEncode("1"))
                    ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_AccessCode"), System.Web.HttpUtility.UrlEncode(Accesscode))
                    ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_MerchTxnRef"), System.Web.HttpUtility.UrlEncode(MerchTxnRef))
                    ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Merchant"), System.Web.HttpUtility.UrlEncode(MerchantID))
                    ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_TransNo"), System.Web.HttpUtility.UrlEncode(TranNo))
                    ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_User"), System.Web.HttpUtility.UrlEncode(User))
                    ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Password"), System.Web.HttpUtility.UrlEncode(Password))
                    ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Command"), System.Web.HttpUtility.UrlEncode("voidPurchase"))
                    ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Currency"), System.Web.HttpUtility.UrlEncode("QAR"))
                    'ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Amount"), System.Web.HttpUtility.UrlEncode("100"))

                    Dim responseArray As Byte() = myWebClient.UploadValues("https://migs.mastercard.com.au/vpcdps", "POST", ValueCollection)
                    strResponse = System.Text.Encoding.ASCII.GetString(responseArray)
                    Dim params As New Hashtable
                    splitResponse(strResponse, params)
                    Dim recno As String = "", msgServer As String = ""
                    '' Do It Here
                    'If strResponse.ToLower.Contains("password") Then
                    '    EmailService.email.SendQueryDRLogEmail("999998", "QUERYDR", 0, "shakeel.shakkeer@gemseducation.com", "VOID Password Mismatch for " & MerchDetail, "Password Mismatch for " & MerchantID.ToString & " ( " & MerchDetail & " ). </br>  </br>  Message : " & strResponse)
                    '    EmailService.email.SendQueryDRLogEmail("999998", "QUERYDR", 0, "kapil.thomas@gemseducation.com", "VOID Password Mismatch for " & MerchDetail, "Password Mismatch for " & MerchantID.ToString & " ( " & MerchDetail & " ). </br>  </br>  Message : " & strResponse)
                    'End If
                    If null2unknown(params("vpc_TxnResponseCode")).ToString.Trim <> "0" Then
                        'lblError.Text = "Transaction Declined - Contact Issuing Bank"
                        'lblError.Text = null2unknown(params("vpc_Message")).ToString.Trim
                        usrMessageBar.ShowNotification(null2unknown(params("vpc_Message")).ToString.Trim, UserControls_usrMessageBar.WarningType.Danger)
                    Else
                        If chkDeleteReceipt.Checked Then
                            If h_ReceiptType.Value = "FEE COLLECTION" Then
                                CancelFeeReceipt()
                            End If
                        End If
                        'lblError.Text = "Void Purchase done successfully."
                        usrMessageBar.ShowNotification("Void Purchase done successfully.", UserControls_usrMessageBar.WarningType.Success)
                    End If
                End If
            Else
                'lblError.Text = "Invalid Data "
                usrMessageBar.ShowNotification("Invalid Data ", UserControls_usrMessageBar.WarningType.Danger)
            End If

        Catch ex As Exception
            'lblError.Text = "Error occured while processing void -  " & ex.Message
            usrMessageBar.ShowNotification("Error occured while processing void -  " & ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Sub

    Private Sub CancelFeeReceipt()
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim trans As SqlTransaction
        Try
            conn.Open()
            trans = conn.BeginTransaction("DeleteReceipt_TRANS")

            Dim cmd As New SqlCommand("FEES.DeleteReceipt_OASIS", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpAUD_BSU_ID As New SqlParameter("@AUD_BSU_ID", SqlDbType.VarChar, 20)
            sqlpAUD_BSU_ID.Value = ddBusinessunit.SelectedValue
            cmd.Parameters.Add(sqlpAUD_BSU_ID)

            Dim sqlpFCL_RECNO As New SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 20)
            sqlpFCL_RECNO.Value = txtRecieptNo.Text
            cmd.Parameters.Add(sqlpFCL_RECNO)


            Dim sqlpAUD_USER As New SqlParameter("@AUD_USER", SqlDbType.VarChar, 50)
            sqlpAUD_USER.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpAUD_USER)

            Dim sqlpAUD_REMARKS As New SqlParameter("@AUD_REMARKS", SqlDbType.VarChar, 100)
            sqlpAUD_REMARKS.Value = txtRemarks.Text
            cmd.Parameters.Add(sqlpAUD_REMARKS)

            Dim sqlpFromDT As New SqlParameter("@DT", SqlDbType.DateTime)
            sqlpFromDT.Value = CDate(txtDate.Text)
            cmd.Parameters.Add(sqlpFromDT)

            Dim sqlpVOIDONLINERECEIPT As New SqlParameter("@VOID_ONLINE_RECEIPT", SqlDbType.Bit)
            sqlpVOIDONLINERECEIPT.Value = True
            cmd.Parameters.Add(sqlpVOIDONLINERECEIPT)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            Dim iReturnvalue As Integer = retSValParam.Value
            If iReturnvalue <> 0 Then
                trans.Rollback()
                'lblError.Text = "Error occured while deleting : " & UtilityObj.getErrorMessage(iReturnvalue) & " . Please Cancel the receipt manually"
                usrMessageBar.ShowNotification("Error occured while deleting : " & UtilityObj.getErrorMessage(iReturnvalue) & " . Please Cancel the receipt manually", UserControls_usrMessageBar.WarningType.Danger)
            Else
                trans.Commit()
                'lblError.Text = "Receipt deleted Successfully.."
                usrMessageBar.ShowNotification("Receipt deleted Successfully..", UserControls_usrMessageBar.WarningType.Success)
                ClearAll()
            End If
        Catch ex As Exception
            'lblError.Text = "Error occured while deleting,Please Cancel the receipt manually  " & ex.Message
            usrMessageBar.ShowNotification("Error occured while deleting,Please Cancel the receipt manually  " & ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            trans.Rollback()
        Finally
            conn.Close()
        End Try

    End Sub

    Public Shared Sub splitResponse(ByVal data As String, ByRef params As Hashtable)
        Dim pairs As String()
        Dim equalsIndex As Integer
        Dim name As String
        Dim value As String
        ' Check if there was a response
        If Len(data) > 0 Then
            ' Check if there are any paramenters in the response
            If InStr(data, "=") > 0 Then
                ' Get the parameters out of the response
                pairs = Split(data, "&")
                For Each pair As String In pairs
                    ' If there is a key/value pair in this item then store it
                    equalsIndex = InStr(pair, "=")
                    If equalsIndex > 1 And Len(pair) > equalsIndex Then
                        name = Left(pair, equalsIndex - 1)
                        value = Right(pair, Len(pair) - equalsIndex)
                        params.Add(name, System.Web.HttpUtility.UrlEncode(value))
                    End If
                Next
            Else ' There were no parameters so create an error
                params.Add("vpc_Message", "The data contained in the response was invalid or corrupt, the data is: <pre>" & data & "</pre>")
            End If
        Else ' There was no data so create an error
            params.Add("vpc_Message", "There was no data contained in the response")
        End If

    End Sub

    Private Sub ClearAll()
        lblAmount.Text = ""
        lblStudentName.Text = ""
        txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
        txtRecieptNo.Text = ""
        txtRemarks.Text = ""
    End Sub
    Private Shared Function null2unknown(ByVal req As Object) As String
        Try
            If req Is Nothing Then
                Return String.Empty
            Else
                Return System.Web.HttpUtility.UrlDecode(req.ToString().Trim)
            End If
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub txtRecieptNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRecieptNo.TextChanged
        UpdateStudentDetails(txtRecieptNo.Text, ddBusinessunit.SelectedValue)
    End Sub

    Protected Sub imgDocno_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UpdateStudentDetails(txtRecieptNo.Text, ddBusinessunit.SelectedValue)
    End Sub

End Class
