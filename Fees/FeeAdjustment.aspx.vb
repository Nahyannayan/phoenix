Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration


Partial Class Fees_FeeAdjustment
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Not IsPostBack Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ADJUSTMENTS Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            btnEdit.Visible = False
            btnDelete.Visible = False
            gvFeeDetails.Attributes.Add("bordercolor", "#1b80b6")
            FillACD()
            UsrSelStudent1.ACD_ID = Session("current_ACD_ID")
            UsrSelStudent1.IsStudent = True

            h_BSUID.Value = Session("sbsuid")
            txtHeaderRemarks.Attributes.Add("OnBlur", "FillDetailRemarks()")
            Session("sFEE_ADJUSTMENT") = Nothing
            txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)

            If ViewState("datamode") = "view" Then
                Dim FAH_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FAH_ID").Replace(" ", "+"))
                Dim vFEE_ADJ As FEEADJUSTMENT = FEEADJUSTMENT.GetFeeAdjustments(FAH_ID)
                txtDate.Text = Format(vFEE_ADJ.FAH_DATE, OASISConstants.DateFormat)
                txtHeaderRemarks.Text = vFEE_ADJ.FAH_REMARKS
                h_STUD_ID.Value = vFEE_ADJ.FAH_STU_ID
                UsrSelStudent1.IsStudent = IIf(vFEE_ADJ.FAH_STU_TYPE = "E", False, True)
                UsrSelStudent1.SetStudentDetails(vFEE_ADJ.FAH_STU_ID)
                UsrSelStudent1.STUDENT_GRADE_ID = vFEE_ADJ.vFAH_GRD_DISPLAY
                PopulateFeeType(vFEE_ADJ.FAH_GRD_ID)
                If vFEE_ADJ.FAH_STU_TYPE = "S" Then
                    radStud.Checked = True
                Else
                    radEnq.Checked = True
                End If
                Session("sFEE_ADJUSTMENT") = vFEE_ADJ
                GridBindAdjustments()
                DissableAllControls(True)
                If Not vFEE_ADJ.bAllowEdit Then
                    btnEdit.Visible = False
                    btnDelete.Visible = False
                    btnPrint.Visible = True
                Else
                    'btnPrint.Visible = False
                End If
                'txtStudName.Text = PROC_DESCR
                'imgProcess.Enabled = False
                'gvFeeDetails.Columns(3).Visible = False
            End If
            UsrSelStudent1.SelectedDate = CDate(txtDate.Text)
        End If
    End Sub

    Private Sub PopulateFeeType(ByVal vGRD_ID As String)
        ddlFeeType.DataSource = FEEADJUSTMENT.PopulateFeeMaster(h_BSUID.Value, ddlAcademicYear.SelectedValue, vGRD_ID)
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()
    End Sub

    Private Sub DissableAllControls(ByVal dissble As Boolean)
        txtDate.ReadOnly = dissble
        txtHeaderRemarks.ReadOnly = dissble
        txtDetRemarks.ReadOnly = dissble
        txtDetAmount.ReadOnly = dissble
        ddlFeeType.Enabled = Not dissble
        btnDetAdd.Enabled = Not dissble
        imgDate.Enabled = Not dissble
        UsrSelStudent1.IsReadOnly = dissble
        CalendarExtender1.Enabled = Not dissble
        calendarButtonExtender.Enabled = Not dissble
        gvFeeDetails.Columns(5).Visible = Not dissble
    End Sub


    Private Sub GridBindAdjustments()
        Dim vFEE_ADJ As FEEADJUSTMENT
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
            gvFeeDetails.DataSource = FEEADJUSTMENT.GetSubDetailsAsDataTable(vFEE_ADJ.FEE_ADJ_DET)
            gvFeeDetails.DataBind()
        Else
            gvFeeDetails.DataSource = Nothing
            gvFeeDetails.DataBind()
        End If
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim vFEE_ADJ As New FEEADJUSTMENT
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
        End If
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        If Not lblFEE_ID Is Nothing Then
            vFEE_ADJ.FEE_ADJ_DET.Remove(CInt(lblFEE_ID.Text))
        End If
        Session("sFEE_ADJUSTMENT") = vFEE_ADJ
        GridBindAdjustments()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = "Sessions not matching for Save...."
            usrMessageBar2.ShowNotification("Sessions not matching for Save....", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim vFEE_ADJ As FEEADJUSTMENT
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim trans As SqlTransaction
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
            vFEE_ADJ.FAH_ACD_ID = ddlAcademicYear.SelectedValue
            vFEE_ADJ.FAH_BSU_ID = Session("sBSUID")
            vFEE_ADJ.FAH_STU_ID = h_STUD_ID.Value
            vFEE_ADJ.FAH_GRD_ID = h_GRD_ID.Value
            If radEnq.Checked Then
                vFEE_ADJ.FAH_STU_TYPE = "E"
            ElseIf radStud.Checked Then
                vFEE_ADJ.FAH_STU_TYPE = "S"
            End If
            If radPositive.Checked Then
                vFEE_ADJ.ADJUSTMENTTYPE = 1
            ElseIf radNegtive.Checked Then
                vFEE_ADJ.ADJUSTMENTTYPE = -1
            End If
            vFEE_ADJ.FAH_DATE = CDate(txtDate.Text)
            vFEE_ADJ.FAH_REMARKS = txtHeaderRemarks.Text
            trans = conn.BeginTransaction("sFEE_ADJUSTMENT")
            Dim newretFAH_ID As String = String.Empty
            Dim retVal As Integer = FEEADJUSTMENT.SaveDetails(vFEE_ADJ, newretFAH_ID, conn, trans)
            If retVal > 0 Then
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(retVal)
                usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                Dim str_KEY As String = "INSERT"
                If ViewState("datamode") <> "edit" Then
                    str_KEY = "EDIT"
                End If
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    UtilityObj.Errorlog("Cannot write into Audit trial")
                    ' Throw New ArgumentException("Could not process your request")
                End If
                trans.Commit()
                'lblError.Text = "Data updated Successfully"
                usrMessageBar2.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                ClearDetails()
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                If chkPrintAfterSave.Checked Then PrintReceipt(newretFAH_ID)
            End If
        End If
    End Sub

    Private Sub ClearDetails()
        h_BSUID.Value = Session("sBSUID")
        h_STUD_ID.Value = ""
        UsrSelStudent1.ClearDetails()
        txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
        txtHeaderRemarks.Text = ""
        Session("sFEE_ADJUSTMENT") = Nothing
        ClearSubDetails()
        ddlFeeType.DataSource = Nothing
        ddlFeeType.DataBind()
        ddlFeeType.ClearSelection()
        GridBindAdjustments()
    End Sub

    Private Sub ClearSubDetails()
        txtDetAmount.Text = ""
        txtDetRemarks.Text = ""
        ddlFeeType.SelectedIndex = -1
        btnDetAdd.Text = "Add"
    End Sub

    'Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
    '    ViewState("datamode") = "edit"
    '    gvFeeDetails.Columns(3).Visible = True
    '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    '    UtilityObj.beforeLoopingControls(Me.Page)
    'End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearDetails()
        DissableAllControls(False)
        gvFeeDetails.Columns(3).Visible = True
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        UsrSelStudent1.IsReadOnly = False
    End Sub

    Protected Sub btnDetAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetAdd.Click
        Dim vFEE_ADJ As New FEEADJUSTMENT
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
        End If
        vFEE_ADJ.FEE_ADJ_DET = AddDetails(vFEE_ADJ.FEE_ADJ_DET)
        Session("sFEE_ADJUSTMENT") = vFEE_ADJ
        GridBindAdjustments()
    End Sub

    Private Function AddDetails(ByVal htFEE_DET As Hashtable) As Hashtable
        Dim vFEE_DET As New FEEADJUSTMENT_S
        Dim eId As Integer = -1
        If btnDetAdd.Text = "Save" Then
            vFEE_DET = htFEE_DET(ViewState("Eid"))
            htFEE_DET.Remove(ViewState("Eid"))
            eId = ViewState("Eid")
        End If
        If Not htFEE_DET(CInt(ddlFeeType.SelectedValue)) Is Nothing Then
            'lblError.Text = "The Fee type is repeating..."
            usrMessageBar2.ShowNotification("The Fee type is repeating...", UserControls_usrMessageBar.WarningType.Danger)
            If eId <> -1 Then htFEE_DET(eId) = vFEE_DET
        Else
            'lblError.Text = ""
            If radPositive.Checked Then
                vFEE_DET.AdjustmentType = 1
            ElseIf radNegtive.Checked Then
                vFEE_DET.AdjustmentType = -1
            End If
            vFEE_DET.FAD_AMOUNT = CDbl(txtDetAmount.Text) * vFEE_DET.AdjustmentType
            vFEE_DET.FEE_TYPE = ddlFeeType.SelectedItem.Text
            vFEE_DET.FAD_FEE_ID = ddlFeeType.SelectedValue
            vFEE_DET.FAD_REMARKS = txtDetRemarks.Text
            htFEE_DET(vFEE_DET.FAD_FEE_ID) = vFEE_DET
            ClearSubDetails()
        End If

        Return htFEE_DET
    End Function

    Protected Sub btnSubCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubCancel.Click
        ClearSubDetails()
    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim vFEE_ADJ As New FEEADJUSTMENT
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
        End If
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        If Not lblFEE_ID Is Nothing Then
            Dim vFEE_DET As FEEADJUSTMENT_S = vFEE_ADJ.FEE_ADJ_DET(CInt(lblFEE_ID.Text))
            If Not vFEE_DET Is Nothing Then
                ddlFeeType.SelectedIndex = -1
                If Not ddlFeeType.Items.FindByValue(vFEE_DET.FAD_FEE_ID) Is Nothing Then
                    ddlFeeType.Items.FindByValue(vFEE_DET.FAD_FEE_ID).Selected = True
                End If
                txtDetAmount.Text = vFEE_DET.FAD_AMOUNT
                txtDetRemarks.Text = vFEE_DET.FAD_REMARKS
                If vFEE_DET.AdjustmentType = 1 Then
                    radPositive.Checked = True
                ElseIf vFEE_DET.AdjustmentType = -1 Then
                    radNegtive.Checked = True
                End If
                ViewState("Eid") = CInt(lblFEE_ID.Text)
                btnDetAdd.Text = "Save"
            End If
        End If
    End Sub

    'Protected Sub imgProcess_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgProcess.Click
    '    If h_STUD_ID.Value <> "" Then
    '        Dim STUD_ID As Integer = h_STUD_ID.Value.Split("||")(0)
    '        FillGradeDetails(STUD_ID)
    '        ClearEnteredDatas()
    '        PopulateFeeType(h_GRD_ID.Value)
    '    End If
    'End Sub

    'Private Sub FillGradeDetails(ByVal STU_ID As Integer)
    '    Dim STU_TYP As STUDENTTYPE = STUDENTTYPE.STUDENT
    '    If radEnq.Checked Then
    '        STU_TYP = STUDENTTYPE.ENQUIRY
    '    ElseIf radStud.Checked Then
    '        STU_TYP = STUDENTTYPE.STUDENT
    '    End If
    '    Dim vGrade As String = FEEPERFORMAINVOICE.GetGradeDetails(STU_ID, False, STU_TYP)
    '    h_GRD_ID.Value = FEEADJUSTMENT.GetGradeDetails(STU_ID, UsrSelStudent1.STUDENT_GRADE_ID)
    '    'ddlGrade.DataSource = FEEPERFORMAINVOICE.GetGradeDetails(STU_ID, STU_TYP)
    '    'ddlGrade.DataTextField = "GRM_DISPLAY"
    '    'ddlGrade.DataValueField = "GRD_ID"
    '    'ddlGrade.DataBind()
    'End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ClearDetails()
        UsrSelStudent1.ACD_ID = ddlAcademicYear.SelectedValue
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        DissableAllControls(False)
        MakeEditable()
        gvFeeDetails.Columns(3).Visible = True
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Private Sub MakeEditable()
        If Session("sFEE_ADJUSTMENT") Is Nothing Then Return
        Dim vFEE_ADJ As New FEEADJUSTMENT
        vFEE_ADJ = Session("sFEE_ADJUSTMENT")
        vFEE_ADJ.bEdit = True
        'For Each vFEE_DET As FEEADJUSTMENT_S In vFEE_ADJ.FEE_ADJ_DET.Values
        '    vFEE_DET.bEdit = True
        'Next
        Session("sFEE_ADJUSTMENT") = vFEE_ADJ
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Session("sFEE_ADJUSTMENT") Is Nothing Then Return
        Dim vFEE_ADJ As FEEADJUSTMENT = Session("sFEE_ADJUSTMENT")
        vFEE_ADJ.bDelete = True
        vFEE_ADJ.bEdit = True
        For Each vFEE_DET As FEEADJUSTMENT_S In vFEE_ADJ.FEE_ADJ_DET.Values
            vFEE_DET.bDelete = True
            'vFEE_DET.bEdit = True
        Next
        Session("sFEE_ADJUSTMENT") = vFEE_ADJ
        btnSave_Click(sender, e)
    End Sub

    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        Dim vDate As String = txtDate.Text
        Try
            If txtDate.Text <> "" Then
                UsrSelStudent1.SelectedDate = CDate(txtDate.Text)
            End If
        Catch
            'lblError.Text = "Invalid Date"
            usrMessageBar2.ShowNotification("Invalid Date", UserControls_usrMessageBar.WarningType.Danger)
        End Try
        ClearDetails()
        txtDate.Text = vDate
    End Sub

    Protected Sub radStud_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStud.CheckedChanged
        UsrSelStudent1.IsStudent = True
        ClearDetails()
    End Sub

    Protected Sub radEnq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnq.CheckedChanged
        UsrSelStudent1.IsStudent = False
        ClearDetails()
    End Sub

    Private Sub ClearEnteredDatas()
        ClearSubDetails()
        Session("sFEE_ADJUSTMENT") = Nothing
        GridBindAdjustments()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim vFEE_ADJ As FEEADJUSTMENT = Session("sFEE_ADJUSTMENT")
        If Not vFEE_ADJ Is Nothing Then
            PrintReceipt(vFEE_ADJ.FAH_ID)
        End If
    End Sub

    Protected Sub PrintReceipt(ByVal vFAH_ID As Integer)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_GETFEEADJUSTMENTFORVOUCHER]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAR_ID As New SqlParameter("@FAH_ID", SqlDbType.Int)
        sqlpFAR_ID.Value = vFAH_ID
        cmd.Parameters.Add(sqlpFAR_ID)

        'Dim ds As New DataSet
        'SqlHelper.FillDataset(str_conn, CommandType.StoredProcedure, "[FEES].[F_GETFEEADJUSTMENTREQUESTFORVOUCHER]", ds, Nothing, cmd.Parameters)
        'If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable

        params("RPT_CAPTION") = "FEE ADJUSTMENT VOUCHER"

        params("UserName") = Session("sUsr_name")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../fees/Reports/RPT/rptFEEAdjustmentVoucher.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
        'End If
        h_print.Value = "print"
    End Sub

    Protected Sub UsrSelStudent1_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles UsrSelStudent1.StudentNoChanged
        h_STUD_ID.Value = UsrSelStudent1.STUDENT_ID
        h_GRD_ID.Value = UsrSelStudent1.STUDENT_GRADE_ID
        If h_STUD_ID.Value <> "" Then
            Dim STUD_ID As Integer = h_STUD_ID.Value.Split("||")(0)
            ClearEnteredDatas()
            PopulateFeeType(h_GRD_ID.Value)
        End If
    End Sub
    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBsuid"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next

    End Sub
End Class
