<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowStudentTransportServicedetails.aspx.vb" Inherits="ShowStudentTransportServicedetails" Theme="General" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <base target="_self" />
    <script>
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />   --%>
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
</head>
<body class="matter" onload="listen_window();" leftmargin="0" topmargin="0" bottommargin="0" rightmargin="0">
    <form id="form1" runat="server">

        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center">
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row" EmptyDataText="NO DATA">
                        <Columns>
                            <asp:TemplateField HeaderText="Service Start Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblservicestartdate" runat="server" Text='<%# Eval("SSV_FROMDATE") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    Service Start Date
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Service End Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblserviceenddate" runat="server" Text='<%# Eval("SSV_TODATE") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                   Service End Date
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Location From ">
                                <ItemTemplate>
                                    <asp:Label ID="lbllocfrom" runat="server" Text='<%# Eval("PICKUPAREA") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    Pickup Area
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Location To">
                                <ItemTemplate>
                                    <asp:Label ID="lbllocationto" runat="server" Text='<%# Eval("DROPOFFAREA") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    Dropoff Area
                                           
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Service Id">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnselect" runat="server" OnClick="lbtnselect_Click">Select</asp:LinkButton>
                                    <asp:HiddenField ID="HfSSV_ID" runat="server" Value='<%# Eval("SSV_ID") %>' />
                                    <asp:HiddenField ID="HF_roundtrip" runat="server" />
                                </ItemTemplate>
                                <HeaderTemplate>
                                    Select
                                </HeaderTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="height: 24px">
                    <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                    <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                </td>
            </tr>
        </table>

    </form>
</body>
</html>
