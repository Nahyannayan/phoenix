Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeTranspotCollectionView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Dim sbSelectAll As Boolean
    Public Property bSelectAll() As Boolean
        Get
            Return sbSelectAll
        End Get
        Set(ByVal value As Boolean)
            sbSelectAll = value
        End Set
    End Property
    Dim sbReceiptNo As String
    Public Property ReceiptNos() As String
        Get
            Return sbReceiptNo
        End Get
        Set(ByVal value As String)
            sbReceiptNo = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "feeTranspotCollectionNew.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_FEE_COLLECTION Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_FEE_TRANSPORT_FEE_COLLECTION
                            lblHead.Text = "Transport Fee Collection"
                    End Select
                    ddlBusinessunit.DataBind()
                    GET_COOKIE()
                    gridbind()
                End If
                'btnOkay.Attributes.Add("onClick", "return disableBtn('" & btnOkay.ClientID & "');")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim lstrOpr, str_Filter, lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7, FCL_bEMAILED As String
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtReceiptno FCL_RECNO
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCL_RECNO", lstrCondn1)
                '   -- 2  txtDate
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCL_DATE", lstrCondn2)
                '   -- 3  txtGrade
                larrSearchOpr = h_selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRD_DISPLAY", lstrCondn3)
                '   -- 4   txtStuno
                larrSearchOpr = h_selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn4)
                '   -- 5  txtStuname
                larrSearchOpr = h_selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn5)
                '   -- 6  txtAmount
                larrSearchOpr = h_selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
                lstrCondn6 = txtSearch.Text
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCL_AMOUNT", lstrCondn6)
                '   -- 7  txtDesc
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
                lstrCondn7 = txtSearch.Text
                If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCL_NARRATION", lstrCondn7)
            End If
           If rblEmailStatus.SelectedValue = "1" Then 'Emailed
                FCL_bEMAILED = "1"
            ElseIf rblEmailStatus.SelectedValue = "2" Then 'Failed
                FCL_bEMAILED = "0"
            Else 'All
                FCL_bEMAILED = "ISNULL(FCL_bEMAILED,0)"
            End If
            Dim str_topFilter As String = ""
            If ddlTopFilter.SelectedItem.Value <> "All" Then
                str_topFilter = " top " & ddlTopFilter.SelectedItem.Value
            End If
            str_Sql = "SELECT " & str_topFilter & " *,CASE WHEN ISNULL(FCL_bEMAILED,0)=0 THEN 'No' ELSE 'Yes' END AS bEMAILED FROM FEES.[VW_OSO_FEES_RECEIPTS_LISTS] WHERE FCL_BSU_ID='" & Session("sBSUID") & "'" _
            & " AND isnull(FCL_bDELETED,0)=0 AND ISNULL(FCL_bEMAILED,0)=" & FCL_bEMAILED & " AND FCL_STU_BSU_ID='" & ddlBusinessunit.SelectedItem.Value & "'" & str_Filter _
            & " ORDER BY FCL_DATE DESC ,FCL_RECNO DESC,STU_NAME"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
            txtSearch.Text = lstrCondn1
            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2
            txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn3
            txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
            txtSearch.Text = lstrCondn4
            txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn5
            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = lstrCondn6
            txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
            txtSearch.Text = lstrCondn7
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbVoucher_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
            h_print.Value = "?type=REC&id=" + Encr_decrData.Encrypt(lblReceipt.Text) & _
            "&bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) & _
            "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & _
            "&stu_bsu_id=" & Encr_decrData.Encrypt(ddlBusinessunit.SelectedItem.Value) & _
            "&isexport=0" & _
            "&isDuplicate=1"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        SET_COOKIE()
        gridbind()
    End Sub

    Protected Sub ddlTopFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTopFilter.SelectedIndexChanged
        SET_COOKIE()
        gridbind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub

    Protected Sub lbExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
            h_print.Value = "?type=REC&id=" + Encr_decrData.Encrypt(lblReceipt.Text) & _
            "&bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) & _
            "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & _
            "&stu_bsu_id=" & Encr_decrData.Encrypt(ddlBusinessunit.SelectedItem.Value) & _
            "&isexport=1" & _
            "&isDuplicate=1"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub chkSelectH_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSelectH As CheckBox = sender.Parent.parent.findcontrol("chkSelectH")
        Try
            bSelectAll = chkSelectH.Checked
            For Each gvr As GridViewRow In gvJournal.Rows
                If gvr.RowType = DataControlRowType.DataRow Then
                    Dim chkSelect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
                    chkSelect.Checked = bSelectAll
                End If
            Next
            If bSelectAll Then
                'btnEmail.Focus()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnPDF_Click(sender As Object, e As ImageClickEventArgs)
        Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
        DownloadPDF(lblReceipt.Text)
    End Sub
    Protected Sub btngvEmail_Click(sender As Object, e As ImageClickEventArgs)
        Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
        Dim bsuccess As Boolean = False
        Dim retmessage = SendEmail(lblReceipt.Text, 0, bsuccess)

    End Sub
    Private Sub DownloadPDF(ByVal RecNo As String)
        Dim RptFile As String
        Dim str As String = "SELECT FCL_ID FROM FEES.FEECOLLECTION_H WITH ( NOLOCK ) WHERE FCL_BSU_ID='" & Session("sBsuid") & "' AND FCL_RECNO='" & RecNo & "'"
        Dim FCL_ID As Long = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim param As New Hashtable
        Dim rptClass As New rptClass
        param.Add("UserName", "SYSTEM")
        param.Add("@FCL_ID", FCL_ID)
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        RptFile = Server.MapPath("~/Fees/Reports/RPT_EMAIL/rptFeeReceipt_Transport.rpt")

        rptClass.reportPath = RptFile
        rptClass.reportParameters = param
        rptClass.crDatabase = ConnectionManger.GetOASISTransportConnection.Database
        Dim rptDownload As New EmailFeeReceipt
        rptDownload.LogoPath = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_BSU_GROUP_LOGO,'https://oasis.gemseducation.com/Images/Misc/TransparentLOGO.gif')GROUP_LOGO FROM dbo.BUSINESSUNIT_SUB  WITH ( NOLOCK ) WHERE BUS_BSU_ID='" & Session("sBsuid") & "'")
        rptDownload.BSU_ID = Session("sBsuId")
        rptDownload.FCL_ID = FCL_ID
        rptDownload.Action = "DOWNLOAD"
        'UtilityObj.Errorlog("Calling Loadreports", "BBT_TEST")
        rptDownload.bEmailSuccess = False
        rptDownload.LoadReports(rptClass)
    End Sub
    Private Function SendEmail(ByVal RecNo As String, ByVal FCO_ID As Long, ByRef bEmailSuccess As Boolean) As String
        SendEmail = ""
        Dim RptFile As String
        Dim str As String = "SELECT FCL_ID FROM FEES.FEECOLLECTION_H WITH ( NOLOCK ) WHERE FCL_BSU_ID='" & Session("sBsuid") & "' AND FCL_RECNO='" & RecNo & "'"
        Dim FCL_ID As Long = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim param As New Hashtable
        Dim rptClass As New rptClass
        param.Add("UserName", "SYSTEM")
        param.Add("@FCL_ID", FCL_ID)
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        RptFile = Server.MapPath("~/Fees/Reports/RPT_EMAIL/rptFeeReceipt_Transport.rpt")

        rptClass.reportPath = RptFile
        rptClass.reportParameters = param
        rptClass.crDatabase = ConnectionManger.GetOASISTransportConnection.Database
        Dim rptDownload As New EmailFeeReceipt
        rptDownload.LogoPath = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_BSU_GROUP_LOGO,'https://oasis.gemseducation.com/Images/Misc/TransparentLOGO.gif')GROUP_LOGO FROM dbo.BUSINESSUNIT_SUB  WITH ( NOLOCK ) WHERE BUS_BSU_ID='" & Session("sBsuid") & "'")
        rptDownload.BSU_ID = Session("sBsuId")
        rptDownload.FCL_ID = FCL_ID
        rptDownload.FCO_ID = FCO_ID
        'UtilityObj.Errorlog("Calling Loadreports", "BBT_TEST")
        rptDownload.bEmailSuccess = False
        rptDownload.LoadReports(rptClass)
        SendEmail = rptDownload.EmailStatusMsg
        bEmailSuccess = rptDownload.bEmailSuccess
        rptDownload = Nothing
        If bEmailSuccess Then
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.Text, "EXEC FEES.SAVE_EMAIL_RECEIPT_LOG '" & Session("sBsuId") & "','" & Session("sUsr_name") & "','" & RecNo & "','" & FCL_ID & "','" & SendEmail & "' ")
        End If
    End Function
    Sub InitializeControls()
        btnCancel.Text = "Cancel"
        btnOkay.Text = "Yes, Send to all"
        ViewState("EmailList") = Nothing
        gvEmailStatus.DataSource = Nothing
        gvEmailStatus.DataBind()
        gvEmailStatus.Visible = False
        btnOkay.Visible = True
        btnEmailUnSent.Visible = False
        ReceiptNos = ""
        lblMsg.Text = ""
    End Sub
    Sub BindEmailStatus()
        gvEmailStatus.DataSource = DirectCast(ViewState("EmailList"), DataTable)
        gvEmailStatus.DataBind()
        gvEmailStatus.Visible = True
        btnEmailUnSent.Visible = False
        btnOkay.Visible = False
        Me.testpopup.Style.Item("display") = "block"
    End Sub

    Protected Sub btnEmail_Click(sender As Object, e As EventArgs) Handles btnEmail.Click
        Dim dt As New DataTable
        dt.Columns.Add("REC_NO", GetType([String]))
        dt.Columns.Add("STU_NO", GetType([String]))
        dt.Columns.Add("FCL_bEMAILED", GetType(Boolean))
        dt.Columns.Add("RET_MESSAGE", GetType([String]))
        dt.AcceptChanges()
        InitializeControls()
        For Each gvr As GridViewRow In gvJournal.Rows
            Dim chkselect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
            If chkselect.Checked Then
                Dim lblReceipt As Label = DirectCast(gvr.FindControl("lblReceipt"), Label)
                Dim hf_STU_NO As HiddenField = DirectCast(gvr.FindControl("hf_STU_NO"), HiddenField)
                Dim lblGrade As Label = DirectCast(gvr.FindControl("lblGrade"), Label)
                Dim lblStuName As Label = DirectCast(gvr.FindControl("lblStuName"), Label)
                Dim lblAmount As Label = DirectCast(gvr.FindControl("lblAmount"), Label)
                Dim hf_Emailed As HiddenField = DirectCast(gvr.FindControl("hf_Emailed"), HiddenField)
                Dim dr As DataRow = dt.NewRow
                dr(0) = lblReceipt.Text
                dr(1) = hf_STU_NO.Value
                dr(2) = hf_Emailed.Value
                dt.Rows.Add(dr)
                dt.AcceptChanges()
                If hf_Emailed.Value = "True" Then
                    ReceiptNos = ReceiptNos & IIf(ReceiptNos = "", "", ",") & lblReceipt.Text
                End If
            End If
        Next
        If dt Is Nothing Or dt.Rows.Count = 0 Then
            'lblError.Text = "Please choose the Receipt(s) to be Emailed"
            usrMessageBar.ShowNotification("Please choose the Receipt(s) to be Emailed", UserControls_usrMessageBar.WarningType.Danger)
            Return
        End If
        ViewState("EmailList") = dt
        If ReceiptNos <> "" Then
            'Dim lblMessage As Label = pnlEmail.FindControl("lblMessage")
            'lblMessage.Text = "Please note, Email has already been sent for Receipt(s) " & ReceiptNos & ".<br />Are you sure you want to send again?"
            btnEmailUnSent.Visible = True
            'Me.MPE1.Show()
            Me.testpopup.Style.Item("display") = "block"
            lblMsg.Text = "Please note, Email has already been sent for Receipt(s) " & ReceiptNos & ".<br />Are you sure you want to send again?"
            'lblMsg2.Text = ""
        Else
            'Dim lblMessage As Label = pnlEmail.FindControl("lblMessage")
            'lblMessage.Text = "Are you sure you want to send selected Receipt(s) as Email?"
            btnOkay.Text = "Yes"
            Me.testpopup.Style.Item("display") = "block"
            lblMsg.Text = "Are you sure you want to send selected Receipt(s) as Email?"
            'lblMsg2.Text = ""
            'Me.MPE1.Show()
        End If
    End Sub

    Protected Sub btnOkay_Click(sender As Object, e As EventArgs) Handles btnOkay.Click
        Try
            Dim bFailed As Boolean = False
            btnCancel.Text = "OK"
            If Not ViewState("EmailList") Is Nothing AndAlso DirectCast(ViewState("EmailList"), DataTable).Rows.Count > 0 Then
                For i As Integer = 0 To DirectCast(ViewState("EmailList"), DataTable).Rows.Count - 1
                    Dim ReceiptNo As String = DirectCast(ViewState("EmailList"), DataTable).Rows(i)("REC_NO").ToString
                    Dim bsuccess As Boolean = False
                    Dim retmessage = SendEmail(ReceiptNo, 0, bsuccess)
                    DirectCast(ViewState("EmailList"), DataTable).Rows(i)("FCL_bEMAILED") = bsuccess
                    DirectCast(ViewState("EmailList"), DataTable).Rows(i)("RET_MESSAGE") = retmessage
                    If bsuccess = False Then
                        bFailed = True
                    End If
                Next
                If bFailed = True Then
                    lblMsg.Text = "Email sent successfully except the below"
                Else
                    lblMsg.Text = "Email sent successfully"
                End If
                BindEmailStatus()
                gridbind()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnEmailUnSent_Click(sender As Object, e As EventArgs) Handles btnEmailUnSent.Click
        Try
            Dim bFailed As Boolean = False
            btnCancel.Text = "OK"
            If Not ViewState("EmailList") Is Nothing AndAlso DirectCast(ViewState("EmailList"), DataTable).Rows.Count > 0 Then
                For i As Integer = 0 To DirectCast(ViewState("EmailList"), DataTable).Rows.Count - 1
                    If DirectCast(DirectCast(ViewState("EmailList"), DataTable).Rows(i)("FCL_bEMAILED"), Boolean) = False Then
                        Dim ReceiptNo As String = DirectCast(ViewState("EmailList"), DataTable).Rows(i)("REC_NO").ToString
                        Dim bsuccess As Boolean = False
                        Dim retmessage = SendEmail(ReceiptNo, 0, bsuccess)
                        DirectCast(ViewState("EmailList"), DataTable).Rows(i)("FCL_bEMAILED") = bsuccess
                        DirectCast(ViewState("EmailList"), DataTable).Rows(i)("RET_MESSAGE") = retmessage
                        If bsuccess = False Then
                            bFailed = True
                        End If
                    End If
                Next
                If bFailed = True Then
                    lblMsg.Text = "Email sent successfully except the below"
                Else
                    lblMsg.Text = "Email sent successfully"
                End If
                BindEmailStatus()
                gridbind()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.testpopup.Style.Item("display") = "none"
    End Sub

    Protected Sub rblEmailStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblEmailStatus.SelectedIndexChanged
        gridbind()
    End Sub

    Private Sub SET_COOKIE()
        Dim OasisSettingsCookie As HttpCookie
        If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
            OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
        Else
            OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
        End If
        OasisSettingsCookie.Values("TPTBSUID") = ddlBusinessunit.SelectedValue
        OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
        Response.Cookies.Add(OasisSettingsCookie)
    End Sub
    Private Sub GET_COOKIE()
        If Not Request.Cookies("OASIS_SETTINGS") Is Nothing Then
            ddlBusinessunit.SelectedValue = Request.Cookies("OASIS_SETTINGS")("TPTBSUID")
        End If
    End Sub

    Protected Sub gvJournal_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvJournal.RowDataBound

    End Sub
End Class
