﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeChangePaymentPlanView.aspx.vb" Inherits="Fees_feeChangePaymentPlanView" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/chkPrint/) > 0)) {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr valign="top">
                        <td valign="top" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError"></asp:Label>--%>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" width="100%">


                    <tr>
                        <td align="left" width="15%">
                            <span class="field-label">Academic Year</span></td>
                        <td align="left" width="25%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList></td>
                        <td align="right" width="10%">
                            <asp:Label ID="lblDate" runat="server" Visible="False"><span class="field-label">Date</span></asp:Label></td>
                        <td align="left" width="20%">
                            <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True" Visible="False"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" Visible="False" />
                        </td>
                        <td align="right" width="30%">
                            <asp:RadioButton ID="rbApproved" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Text="Approved"
                                OnCheckedChanged="rbAll_CheckedChanged" />
                            <asp:RadioButton ID="rbRejected" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Text="Rejected"
                                OnCheckedChanged="rbAll_CheckedChanged" />
                            <asp:RadioButton ID="rbAll" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Text="All"
                                OnCheckedChanged="rbAll_CheckedChanged" Checked="True" />&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center"  width="100%" colspan="6">
                            <asp:GridView ID="gvFEEConcessionDet" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30">
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="STP_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTP_ID" runat="server" Text='<%# Bind("STP_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student#">
                                        <HeaderTemplate>
                                            Student ID<br />
                                            <asp:TextBox ID="txtStuno" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchno" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Student Name<br />
                                            <asp:TextBox ID="txtStuname" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Concession">
                                        <HeaderTemplate>
                                            Plan<br />
                                            <asp:TextBox ID="txtConcession" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnConcession" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("SCH_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            From Date<br />
                                            <asp:TextBox ID="txtFrom" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnFrom" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("STP_FROMDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Period">
                                        <HeaderTemplate>
                                            To Date<br />
                                            <asp:TextBox ID="txtPeriod" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                           
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPeriod" runat="server" Text='<%# Bind("STP_TODT")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderTemplate>
                                            Remarks<br />
                                            <asp:TextBox ID="txtRemarks" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                                
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("STP_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="STP_STATUS" HeaderText="Status" />
                                    <asp:TemplateField HeaderText="Post" Visible="False">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkPost" runat="server" />
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Post
                                   <input id="chkSelectall" type="checkbox" onclick="fnSelectAll(this)" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                            <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" Visible="False" />
                            <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject"
                                Visible="False" />
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>


                        </td>
                    </tr>
                </table>
                
                </div>
            </div>
        </div>
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
</asp:Content>
