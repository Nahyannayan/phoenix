Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Threading
Partial Class Fees_feeTranspotMonthlyCharging
    Inherits PageLongProcess
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            InitialiseCompnents()
            ddBusinessunit.DataBind()
            FillACD()
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "F351025" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        End If
    End Sub

    Sub InitialiseCompnents()
        gvStudentDetails.Attributes.Add("bordercolor", "#1b80b6")
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
    End Sub

    Sub Clear_All() 
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtStudName.Text = ""
        ClearStudentData()
        h_STUD_ID.Value = ""
        Session("vStud_Det") = Nothing
        gvStudentDetails.DataBind()
    End Sub

    Function CheckForErrors() As String
        Dim str_error As String = ""
        Return str_error
    End Function

    'Private Sub LongRunningMethod(ByVal name As String)
    '    'This will run for 5 mins
    '    Thread.Sleep(60000 * 1)
    'End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Session("result") = Nothing
        Session("message") = Nothing
        h_Processeing.Value = "1"
        'Me.lblError.Text = SaveAll("")
        usrMessageBar.ShowNotification(SaveAll(""), UserControls_usrMessageBar.WarningType.Information)
        'Dim LongProcess As New LongRun(AddressOf SaveAll)
        'Dim ar As IAsyncResult = LongProcess.BeginInvoke("CHECK", New AsyncCallback(AddressOf CallBackMethod), Nothing)
        'Session("result") = ar
    End Sub

    Function SaveAll(ByVal name As String) As String
        If Not Master.IsSessionMatchesForSave() Then
            Return OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            Exit Function
        End If
        Dim str_error As String = CheckForErrors()
        Dim STR_BSU_Provider As String = feeGenerateTransportFeeCharge.Get_BSU_Provider(ddBusinessunit.SelectedItem.Value, ddlAcademicYear.SelectedItem.Value)

        If STR_BSU_Provider = "" Then
            Return "Invalid Service provider / business unit !!!"
            Exit Function
        End If
        If str_error <> "" Then
            Return str_error
            Exit Function
        End If
        
        Try
            Dim retval, str_STUD_IDs As String
            If chkStudentwise.Checked Then
                str_STUD_IDs = h_STUD_ID.Value.Replace("||", "|")
            Else
                str_STUD_IDs = ""
            End If

            Dim objTFC As New feeGenerateTransportFeeCharge
            objTFC.GENERATE_TRANSPORT_FEE_CHARGE_FOR_BSU(STR_BSU_Provider, _
            ddlAcademicYear.SelectedItem.Value, txtFrom.Text, ddBusinessunit.SelectedItem.Value, _
            str_STUD_IDs)

            'retval = feeGenerateTransportFeeCharge.F_GenerateTransportFeeCharge_For_BSU(STR_BSU_Provider, _
            'ddlAcademicYear.SelectedItem.Value, txtFrom.Text, ddBusinessunit.SelectedItem.Value, _
            'str_STUD_IDs, objConn, stTrans)

            'If retval = "0" Then
            '    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
            '    ddlAcademicYear.SelectedItem.Value & " - " & txtFrom.Text & " - " & ddBusinessunit.SelectedItem.Value, _
            '    "Insert", Page.User.Identity.Name.ToString, Me.Page)
            '    stTrans.Commit()
            '    Return getErrorMessage("0")
            '    Clear_All()
            'Else
            '    stTrans.Rollback()
            '    Return getErrorMessage(retval)
            'End If
        Catch ex As Exception
            'stTrans.Rollback()
            Errorlog(ex.Message)
            Return getErrorMessage("1000")
        End Try
    End Function

    Private Function FillSTUNames(ByVal STU_IDs As String) As Boolean
        Dim IDs As String() = STU_IDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = " SELECT STU_NO , STU_NAME FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvStudentDetails.DataSource = ds
        gvStudentDetails.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub txtAmountToPay_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtAmount As New TextBox
        txtAmount = sender
        txtAmount.Attributes.Add("onFocus", "this.select();")
        txtAmount.Style.Add("text-align", "right")
    End Sub

    'Protected Sub rbEnrollment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnrollment.CheckedChanged
    '    ClearStudentData()
    'End Sub


    'Protected Sub rbEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnquiry.CheckedChanged
    '    ClearStudentData()
    'End Sub

    Sub ClearStudentData()
        h_STUD_ID.Value = ""
        'txtStdNo.Text = ""
        'txtStudentname.Text = "" 
    End Sub

    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBusinessunit.SelectedIndexChanged
        clear_All()
        FillACD() 
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddBusinessunit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Protected Sub lblAddNewStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddNewStudent.Click
        If txtStudName.Text <> "" Then
            Dim str_stuid As String = FeeCommon.GetTransportStudentID(txtStudName.Text, ddBusinessunit.SelectedItem.Value, False, True)
            If str_stuid <> "" Then
                If h_STUD_ID.Value.EndsWith("||") Then
                    h_STUD_ID.Value = h_STUD_ID.Value & str_stuid
                Else
                    h_STUD_ID.Value = h_STUD_ID.Value & "||" & str_stuid
                End If
                txtStudName.Text = ""
                FillSTUNames(h_STUD_ID.Value)
            End If
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetMsg() As String
        Dim str_message As String = IIf(HttpContext.Current.Session("message") Is Nothing, "", HttpContext.Current.Session("message"))
        HttpContext.Current.Session("message") = Nothing
        Return str_message
    End Function

    Protected Sub btnPostback_Click(sender As Object, e As EventArgs)
        If Me.h_STATUS.Value = "0" Then
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                ddlAcademicYear.SelectedItem.Value & " - " & txtFrom.Text & " - " & ddBusinessunit.SelectedItem.Value, _
                "Insert", Page.User.Identity.Name.ToString, Me.Page)
            Dim errormsg As String = h_errormessage.Value
            Clear_All()
            'Me.lblError.Text = errormsg
            usrMessageBar.ShowNotification(errormsg, UserControls_usrMessageBar.WarningType.Information)
            h_Processeing.Value = ""
        End If
    End Sub

    Protected Sub txtStudName_TextChanged(sender As Object, e As EventArgs)
        txtStudName.Text = ""
        FillSTUNames(h_STUD_ID.Value)
    End Sub
End Class
