<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeRevenueRecognition.aspx.vb" Inherits="Fees_feeRevenueRecognition" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link href="../Scripts/JQuery-ui-1.10.3.css" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>

    <script language="javascript" type="text/javascript">
        $().ready(function () {
            // Go get handles to the UI elements that we need.
            var $ajaxImage = $("#ajaxImage");
            var $progressbar = $("#progressbar");
            var $statusDiv = $("#statusDiv");
            var $startProcessButton = $("#<%= btnSave.ClientID%>");
            // Hide the AJAX spinner image and the progress bar, by default.
            $ajaxImage.hide();
            $progressbar.hide();
            // Turn off AJAX caching. When we make an AJAX call, we always want the latest.
            $.ajaxSetup({ cache: false });

            // Simple error handling. If an AJAX error occurs, it wil show in our statusDiv.
            $(document).ajaxError(function (e, jqxhr, settings, exception) {
                $statusDiv.html("Error from AJAX call: " + jqxhr.statusText);
                $startProcessButton.removeAttr("disabled");
            });

            // Event for the when the startProcessButton is clicked
            //$startProcessButton.click(function () {
            //    // Disable the button (it is re-enabled later)
            //    $startProcessButton.attr("disabled", "disabled");
            //    // Show "Loading..." for a status
            //    $statusDiv.html("Loading...");

            //    // Call the web service to kick off the process
            //    $.ajax({
            //        type: "GET",
            //        dataType: "json",
            //        url: "../AjaxServices.asmx/StartProcess",
            //        contentType: "application/json; charset=utf-8"
            //    });

            //    // Set up a recurring timer to keep running this code every second
            //    // We use intervalHandle later via clearInterval to turn off this timer

            //});

           <%-- var intervalHandle = setInterval(function () {
                //// Go hit the web service to get the latest status
                if ($("#<%=h_Processeing.ClientID %>").val() == '1') {
                        $.ajax({
                            type: "GET",
                            dataType: "json",
                            url: "../AjaxServices.asmx/GetStatus",
                            contentType: "application/json; charset=utf-8",
                            success: function (response, status, xhr) {
                                if (response.d == null) {
                                    // The background process isn't running or there was a problem - reset.
                                    $statusDiv.html("");

                                    // Stop this setInterval loop that we're in.
                                    //clearInterval(intervalHandle);
                                }
                                else {
                                    // We're running and have status - so:

                                    // Show the AJAX spinner image and progress bar
                                    $ajaxImage.show();
                                    $progressbar.show();

                                    // Set the status text and the progress back value
                                    $statusDiv.html(response.d.PercentComplete + "% " + response.d.WorkDescription);
                                    $progressbar.progressbar({
                                        value: response.d.PercentComplete
                                    });

                                    // Process is complete, start to reset the UI (the response.d == null above, will do the rest).
                                    if (response.d.IsComplete) {
                                        $startProcessButton.removeAttr("disabled");
                                        $ajaxImage.hide();
                                        $progressbar.hide();
                                    }
                                }
                            }
                        });
                    }
                }, 1000);--%>
        });
        Sys.Application.add_load(function () {
            var $ajaxImage = $("#ajaxImage");
            var $progressbar = $("#progressbar");
            var $fsProgress = $("#fsProgress");
            // Hide the AJAX spinner image and the progress bar, by default.
            $ajaxImage.hide();
            $progressbar.hide();
            $fsProgress.hide();
            var intervalHandle = setInterval(function () {

                var $statusDiv = $("#statusDiv");
                var $startProcessButton = $("#<%= btnSave.ClientID%>");
                var $lblError = $("#<%= lblError.ClientID%>");
                //// Go hit the web service to get the latest status
                if ($("#<%=h_Processeing.ClientID %>").val() == '1') {
                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: "../AjaxServices.asmx/GetStatus",
                        contentType: "application/json; charset=utf-8",
                        success: function (response, status, xhr) {
                            if (response.d == null) {
                                $ajaxImage.hide();
                                $progressbar.hide();
                                $fsProgress.hide();
                                // Stop this setInterval loop that we're in.
                                clearInterval(intervalHandle);
                            }
                            else {
                                // Show the AJAX spinner image and progress bar
                                $fsProgress.show();
                                $ajaxImage.show();
                                $progressbar.show();

                                $startProcessButton.attr("disabled", "disabled");
                                // Set the status text and the progress back value
                                $statusDiv.html(response.d.PercentComplete + "% " + response.d.WorkDescription);
                                $progressbar.progressbar({
                                    value: response.d.PercentComplete
                                });
                                $lblError.html(response.d.WorkDescription);
                                // Process is complete, start to reset the UI (the response.d == null above, will do the rest).
                                if (response.d.IsComplete) {
                                    $startProcessButton.removeAttr("disabled");
                                    $ajaxImage.hide();
                                    $progressbar.hide();
                                    $fsProgress.hide();
                                    $lblError.html(response.d.WorkDescription);
                                }
                            }
                        }
                    });
                }
            }, 1000);
        }
        );
        <%--Sys.Application.add_load(
          function CallServer() {
              if ($("#<%=h_Processeing.ClientID %>").val() == '1') {
                  document.querySelector('[id$="divLoading"]').style.display = 'block';
                  $("#<%=lblError.ClientID %>").html("Please Wait..");
                  <%= Page.ClientScript.GetCallbackEventReference(Page, "", "ShowResult", Nothing)%>;
              }
          }
        );

        function ShowResult(eventArgument, context) {
            alert(eventArgument);
            if (eventArgument == "SUCCESS") {
                document.querySelector('[id$="divLoading"]').style.display = "none";
                $("#<%=h_Processeing.ClientID %>").val('');
                PageMethods.GetMsg(OnSuccess1, OnFailure1);
            }
            else setTimeout("CallServer()", 5000);//Priodic call to server.
        }
        function OnSuccess1(result) {
            $("#<%=lblError.ClientID %>").html(result);
        }
        function OnFailure1(error) {
            alert(error);
        }--%>
        function GetStudent() {
            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 875px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            <%--var url;
            url = "ShowStudentMulti.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu=" + document.getElementById('<%= ddlBUnit.ClientID %>').value;
            result = window.showModalDialog(url, "", sFeatures);

            if (result != '' && result != undefined) {
                document.getElementById('<%=txtStudName.ClientID %>').value = 'Multiple Students selected';
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = result;
            }
            return true;
        }--%>


            var url = "ShowStudentMulti.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu=" + document.getElementById('<%= ddlBUnit.ClientID %>').value;
            var oWnd = radopen(url, "pop_getstudent");
                        
        } 

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtStudName.ClientID %>').value = 'Multiple Students selected';
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = arg.NameandCode;
                __doPostBack('<%=txtStudName.ClientID%>', 'TextChanged');
            }
        } 
        function Showdata() {
            var sFeatures, url;
            sFeatures = "dialogWidth:500px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            url = "../common/PopupShowData.aspx?id=VOUCHER&docid=" + document.getElementById('<%= H_DOCNO.ClientID %>').value + "&bsu=" + document.getElementById('<%= ddlBUnit.ClientID %>').value;
            // result = window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_showdata");
            return false;
        }
    
    

function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_showdata" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getstudent" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table cellspacing="0" width="60%">
        <tr valign="bottom">
            <td align="left" colspan="3" valign="bottom">
                <asp:Label ID="lblError" runat="server" SkinID="error" EnableViewState="False"></asp:Label>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
            </td>
        </tr>
    </table>
    <table width="100%">
       
        <tr>
            <td align="left" width="20%">
                <span class="field-label">Business Unit</span>
            </td>
            
            <td align="left" width="30%">
                <asp:DropDownList ID="ddlBUnit" runat="server" AutoPostBack="True" DataSourceID="ODSGETBSUFORUSER" DataTextField="BSU_NAME" DataValueField="BSU_ID">
                </asp:DropDownList></td>
        
            <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
            
            <td align="left" width="30%">
                <asp:DropDownList ID="ddlAcademic" runat="server" SkinID="DropDownListNormal" AutoPostBack="True">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left"><span class="field-label">Date</span></td>
            
            <td align="left">
                <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"
                    TabIndex="4" /></td>
            <td></td>
            <td></td>
        </tr>
        <tr runat="server" id="tr_student_pick" visible="false">
            <td align="left">
                <asp:CheckBox ID="chkStudentby" runat="server" CssClass="field-label" Text="By Student" AutoPostBack="True" /></td>
            
            <td align="left" colspan="3">
                <asp:TextBox ID="txtStudName" runat="server" Width="50%" autopostback="true" OnTextChanged="txtStudName_TextChanged"></asp:TextBox>
                <asp:LinkButton ID="lblAddNewStudent" runat="server" CausesValidation="False" Enabled="False">Add</asp:LinkButton>
                <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetStudent(); return false;" /><br />
                <br />
                <asp:GridView ID="gvStudentDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    cssclass="table table-bordered table-row" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Student ID">
                            <ItemTemplate>
                                <asp:Label ID="lblStudID" runat="server" Text='<%# bind("STU_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student Name">
                            <ItemTemplate>
                                <asp:Label ID="lblStudentName" runat="server" Text='<%# bind("STU_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete" Visible="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="3" align="center">
                <asp:CheckBox ID="chkHardClose" runat="server" Text="Hard Close"
                     Checked="True"></asp:CheckBox>
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />

                <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" Visible="False" />
                <asp:LinkButton ID="lblViewVoucher" runat="server" OnClientClick="Showdata() ;return false;" Visible="False">View Voucher</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <fieldset id="fsProgress">
                    <legend>Processing Status</legend>
                    <div id="ajaxImage" style="display: inline;">
                        <img alt="" src="../Images/Misc/AjaxLoading.gif" />
                    </div>
                    <div id="statusDiv"></div>
                    <div id="progressbar" style="height: 20px; width: 200px;"></div>
                </fieldset>
            </td>
            
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td align="left">
                <asp:Panel ID="pnFOMonthendHeader" runat="server" Width="100%">
                    <div style="width: 100%; padding: 5px; cursor: pointer; vertical-align: top;" class="title-bg mb-2" align="left">
                        <asp:ImageButton ID="imgFOMonthend" Style="vertical-align: top;" runat="server" ImageUrl="~/images/password/expand.gif" AlternateText="(Show Details...)" />
                        FO Monthend Status
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnFOMonthendDetail" runat="server" Width="100%">
                    <ajaxToolkit:CollapsiblePanelExtender ID="cpeQuick" runat="server" CollapseControlID="pnFOMonthendHeader"
                        Collapsed="True" CollapsedImage="~/images/password/expand.gif" CollapsedText="(Show...)"
                        ExpandControlID="pnFOMonthendHeader" ExpandedImage="~/images/password/collapse.gif" ExpandedText="(Hide...)"
                        ImageControlID="imgFOMonthend" SuppressPostBack="true"
                        TargetControlID="pnFOMonthendDetail">
                    </ajaxToolkit:CollapsiblePanelExtender>
                    <asp:GridView ID="gvFOMonthend" runat="server" cssclass="table table-bordered table-row" SkinID="GridViewNormal">
                    </asp:GridView>
                </asp:Panel>
                <asp:Panel ID="pnlRevenueHeader" runat="server" Width="100%">
                    <div style="width: 100%; padding: 5px; cursor: pointer; vertical-align: top;" class="title-bg mb-2" align="left">
                        <asp:ImageButton ID="imgRevenue" Style="vertical-align: top;" runat="server" ImageUrl="~/images/password/expand.gif" AlternateText="(Show Details...)" />
                        Revenue Recognition
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlRevenueDetails" runat="server" Width="100%">
                    <ajaxToolkit:CollapsiblePanelExtender ID="cpeAlerts" runat="server" CollapseControlID="pnlRevenueHeader"
                        Collapsed="True" CollapsedImage="~/images/password/expand.gif" CollapsedText="(Show...)"
                        ExpandControlID="pnlRevenueHeader" ExpandedImage="~/images/password/collapse.gif" ExpandedText="(Hide...)"
                        ImageControlID="imgRevenue" SuppressPostBack="true"
                        TargetControlID="pnlRevenueDetails">
                    </ajaxToolkit:CollapsiblePanelExtender>
                    <asp:GridView ID="gvRevReco" runat="server" cssclass="table table-bordered table-row" SkinID="GridViewNormal">
                    </asp:GridView>
                </asp:Panel>
                <asp:Panel ID="pnlChargingHeader" runat="server" Width="100%">
                    <div class="title-bg mb-2" style="width: 100%; padding: 5px; cursor: pointer; vertical-align: top;" align="left">
                        <asp:ImageButton ID="imgCharging" runat="server" AlternateText="(Show Details...)"
                            ImageUrl="~/images/password/expand.gif" Style="vertical-align: top" />&nbsp;
          Monthly
          Fee Charging
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlChargingDetail" runat="server" Width="100%">
                    <ajaxToolkit:CollapsiblePanelExtender ID="cpePending" runat="Server" CollapseControlID="pnlChargingHeader"
                        Collapsed="True" CollapsedImage="~/images/password/expand.gif" CollapsedText="(Show Details...)"
                        ExpandControlID="pnlChargingHeader" ExpandedImage="~/images/password/collapse.gif" ExpandedText="(Hide Details...)"
                        ImageControlID="imgCharging" SuppressPostBack="true"
                        TargetControlID="pnlChargingDetail">
                    </ajaxToolkit:CollapsiblePanelExtender>
                    <asp:GridView ID="gvCharging" runat="server" cssclass="table table-bordered table-row" SkinID="GridViewNormal">
                    </asp:GridView>
                </asp:Panel>

                <asp:Panel ID="pnlPostingHeader" runat="server" Width="100%">
                    <div class="title-bg mb-2" style="width: 100%; padding: 5px; cursor: pointer; vertical-align: top;" align="left">
                        <asp:ImageButton ID="imgPosting" runat="server" AlternateText="(Show Details...)"
                            ImageUrl="~/images/password/expand.gif" Style="vertical-align: top" />&nbsp;
          Fee Charge Posting
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlPostingDetail" runat="server" Width="100%">
                    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="Server" CollapseControlID="pnlPostingHeader"
                        Collapsed="True" CollapsedImage="~/images/password/expand.gif" CollapsedText="(Show Details...)"
                        ExpandControlID="pnlPostingHeader" ExpandedImage="~/images/password/collapse.gif" ExpandedText="(Hide Details...)"
                        ImageControlID="imgPosting" SuppressPostBack="true"
                        TargetControlID="pnlPostingDetail">
                    </ajaxToolkit:CollapsiblePanelExtender>
                    <asp:GridView ID="gvPosting" runat="server" cssclass="table table-bordered table-row" SkinID="GridViewNormal">
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
        <script language="javascript" type="text/javascript">
     
        </script>
    </table>
    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    <asp:ObjectDataSource ID="ODSGETBSUFORUSER" runat="server" SelectMethod="GETBSUFORUSER"
        TypeName="FeeCommon">
        <SelectParameters>
            <asp:SessionParameter Name="USR_ID" SessionField="sUsr_name" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:HiddenField ID="h_Processeing" runat="server" />
    <asp:HiddenField ID="h_STUD_ID" runat="server" />
    <asp:HiddenField ID="H_DOCNO" runat="server" />
    <div id="divLoading" class="screenCenter" style="display: none;">
        <br />
        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading1.gif" /><br />
        Please Wait....
    </div>
                </div>
            </div>
        </div>

</asp:Content>
