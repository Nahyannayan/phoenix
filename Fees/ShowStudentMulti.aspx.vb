Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports Microsoft.VisualBasic
Imports System.Web.Services
Imports System.Collections.Generic

Partial Class ShowStudent
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            If Session("sUsr_name") & "" = "" Then
                Response.Redirect("~/login.aspx")
            End If

            Try
                gvGroup.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"

                Dim mnu_code As String = IIf(Request.QueryString("MainMnu_code") Is Nothing, "", Request.QueryString("MainMnu_code"))
                mnu_code = Encr_decrData.Decrypt(mnu_code.Replace(" ", "+"))
                If mnu_code = "F100130" Or mnu_code = "F300136" Then
                    enrlbl_id.Visible = True
                    enrdrp_id.Visible = True
                    chkSelAll.Visible = False
                Else
                    enrlbl_id.Visible = False
                    enrdrp_id.Visible = False
                    chkSelAll.Visible = True
                End If

                Dim dtFiltr As DataTable = Nothing

                If mnu_code = "F100130" Then
                    dtFiltr = GetPopupFilter()
                    ddlType.DataSource = dtFiltr
                    ddlType.DataTextField = "DESCR"
                    ddlType.DataValueField = "ID"
                    ddlType.DataBind()
                End If
                'GetPopupStatusFilter
                If mnu_code = "F300136" Then
                    dtFiltr = GetPopupStatusFilter()
                    ddlType.DataSource = dtFiltr
                    ddlType.DataTextField = "DESCR"
                    ddlType.DataValueField = "ID"
                    ddlType.DataBind()
                End If



                Session("liUserList") = New ArrayList

                Bind_All()
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        set_Menu_Img()
    End Sub
    Public Shared Function GetPopupFilter() As DataTable
        Dim sql_query As String = " SELECT * FROM DBO.VW_STU_POPUP_FILTER"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Public Shared Function GetPopupStatusFilter() As DataTable
        Dim sql_query As String = " SELECT * FROM DBO.VW_STU_POPUP_STATUS_FILTER"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid4(str_Sid_img(2))
    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub Gridbind(Optional ByVal STUD_ID As String = "")
        Try
            Dim str_filter_acctype, str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtControl As String

            Dim i_dd_acctype As Integer = 0

            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""
            Dim str_filter_debit As String = String.Empty
            str_txtCode = ""
            str_txtName = ""
            str_txtControl = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D

            Dim str_filter_pname As String = String.Empty
            Dim str_filter_pmobile As String = String.Empty
            Dim str_txtpname As String = String.Empty
            Dim str_txtpmobile As String = String.Empty

            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
                str_txtControl = txtSearch.Text
                str_filter_control = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtpname = txtSearch.Text
                str_filter_pname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtpmobile = txtSearch.Text
                str_filter_pmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))

            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, " _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive,PARENT_NAME, PARENT_MOBILE" _
            & " FROM VW_OSO_STUDENT_M " _
            & " WHERE STU_bActive=1  AND STU_BSU_ID='" & Session("sBsuid") & "' "
            If STUD_ID <> "" Then
                str_Sql += " AND STU_SIBLING_ID <> STU_ID AND STU_SIBLING_ID=" & STUD_ID
            End If
            Dim str_orderby As String = " ORDER BY " & IIf(str_filter_control <> "", "GRD_DISPLAY", "STU_NAME") & "  "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_debit & str_filter_acctype & _
            str_filter_control & str_filter_code & str_filter_name & str_filter_pmobile & str_filter_pname & str_orderby)
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
            txtSearch.Text = str_txtControl
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtpname
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtpmobile

            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindEnquiryCompany(ByVal COMP_ID As Integer, ByVal ACD_ID As Integer)
        Try
            Dim str_filter_acctype, str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtControl As String

            Dim i_dd_acctype As Integer = 0

            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""
            Dim str_filter_debit As String = String.Empty
            str_txtCode = ""
            str_txtName = ""
            str_txtControl = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D

            Dim str_filter_pname As String = String.Empty
            Dim str_filter_pmobile As String = String.Empty
            Dim str_txtpname As String = String.Empty
            Dim str_txtpmobile As String = String.Empty

            Dim ddacctype As New DropDownList
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                'Try
                '    Dim s As HtmlControls.HtmlImage = gvGroup.HeaderRow.FindControl("mnu_2_img")
                '    ddacctype = gvGroup.HeaderRow.FindControl("DDAccountType")
                'Catch ex As Exception
                'End Try

                'If ddacctype.SelectedItem.Value <> "All" Then
                '    str_filter_acctype = " AND STU_GENDER='" & ddacctype.SelectedItem.Value & "'"
                'End If
                str_mode = Request.QueryString("ShowType") 'ShowType
                i_dd_acctype = ddacctype.SelectedIndex
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
                str_txtControl = txtSearch.Text
                str_filter_control = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtpname = txtSearch.Text
                str_filter_pname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtpmobile = txtSearch.Text
                str_filter_pmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))

            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = " Select * from FEES.vw_OSO_ENQUIRY_COMP "
            str_Sql += " WHERE  STU_BSU_ID='" & Session("sBSUID") & "'"
            If ACD_ID <> "-1" Then
                str_Sql += " AND  STU_ACD_ID = " & ACD_ID
            End If
            If COMP_ID <> "-1" Then
                str_Sql += " AND  COMP_ID = " & COMP_ID
            End If
            str_Sql &= " AND STU_STATUS<>'DEL' "

            If Request.QueryString("EXL_TYPE") IsNot Nothing Then
                Dim EXL_TYPE As String = Request.QueryString("EXL_TYPE")
                str_Sql &= " AND STU_ID not in (select EXL_STU_ID from OASIS_FEES.[FEES].[STUDENT_EXCLUDE_LIST] WHERE EXL_TYPE='" & EXL_TYPE & "'  and EXL_BSU_ID='" & Session("sBSUID") & "') "
                If EXL_TYPE = "PI" Then
                    str_Sql &= " AND STU_ID not in (select TCM_STU_ID from OASIS..TCM_M where isnull( TCM_Bcancelled,0)=0  and TCM_BSU_ID='" & Session("sBSUID") & "' and ISNULL(TCM_LASTATTDATE,GETDATE()+1)<GETDATE()) "
                End If
            End If

            Dim ds As New DataSet
            Dim str_orderby As String = " ORDER BY STU_NAME "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_debit & str_filter_acctype & _
            str_filter_control & str_filter_code & str_filter_name & str_filter_pmobile & str_filter_pname & str_orderby)
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            '  gvGroup.Columns(0).Visible = False
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
            txtSearch.Text = str_txtControl
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtpname
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtpmobile

            set_Menu_Img()
            SetChk(Me.Page)

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindEnquiryForBSU(ByVal BSU_ID As String)
        Try
            If BSU_ID = "" Then
                BSU_ID = Session("sBSUID")
            End If

            Dim str_filter_acctype, str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtControl As String

            Dim i_dd_acctype As Integer = 0

            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""
            Dim str_filter_debit As String = String.Empty
            str_txtCode = ""
            str_txtName = ""
            str_txtControl = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D

            Dim str_filter_pname As String = String.Empty
            Dim str_filter_pmobile As String = String.Empty
            Dim str_txtpname As String = String.Empty
            Dim str_txtpmobile As String = String.Empty

            Dim ddacctype As New DropDownList
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                'Try
                '    Dim s As HtmlControls.HtmlImage = gvGroup.HeaderRow.FindControl("mnu_2_img")
                '    ddacctype = gvGroup.HeaderRow.FindControl("DDAccountType")
                'Catch ex As Exception
                'End Try

                'If ddacctype.SelectedItem.Value <> "All" Then
                '    str_filter_acctype = " AND STU_GENDER='" & ddacctype.SelectedItem.Value & "'"
                'End If
                str_mode = Request.QueryString("ShowType") 'ShowType
                i_dd_acctype = ddacctype.SelectedIndex
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
                str_txtControl = txtSearch.Text
                str_filter_control = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtpname = txtSearch.Text
                str_filter_pname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtpmobile = txtSearch.Text
                str_filter_pmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))

            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = " Select * from FEES.vw_OSO_ENQUIRY_COMP "
            str_Sql += " WHERE  STU_BSU_ID='" & BSU_ID & "'"
            str_Sql &= " AND STU_STATUS<>'DEL' "
            Dim ds As New DataSet
            Dim str_orderby As String = " ORDER BY STU_NAME "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_debit & str_filter_acctype & _
            str_filter_control & str_filter_code & str_filter_name & str_orderby)
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            '  gvGroup.Columns(0).Visible = False
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
            txtSearch.Text = str_txtControl
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtpname
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtpmobile

            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindStudentCompany(ByVal COMP_ID As Integer)
        Try
            Dim str_filter_acctype, str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtControl As String

            Dim str_filter_pname As String = String.Empty
            Dim str_filter_pmobile As String = String.Empty
            Dim str_txtpname As String = String.Empty
            Dim str_txtpmobile As String = String.Empty

            Dim i_dd_acctype As Integer = 0

            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""
            Dim str_filter_debit As String = String.Empty
            str_txtCode = ""
            str_txtName = ""
            str_txtControl = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D

            Dim ddacctype As New DropDownList
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                i_dd_acctype = ddacctype.SelectedIndex
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtpname = txtSearch.Text
                str_filter_pname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtpmobile = txtSearch.Text
                str_filter_pmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
                str_txtControl = txtSearch.Text
                str_filter_control = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = " select * from ( SELECT STU_ID, STU_NO, " & _
            " STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_NAME, PARENT_MOBILE, " & _
            " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, " & _
            " CASE isnull(oasis.dbo.STUDENT_D.STS_FEESPONSOR,0) WHEN 4 " & _
            " THEN IsNULL(oasis.dbo.STUDENT_D.STS_F_COMP_ID, '')   " & _
            " WHEN 5 THEN IsNULL(oasis.dbo.STUDENT_D.STS_M_COMP_ID, '')   " & _
            " WHEN 6 THEN IsNULL(oasis.dbo.STUDENT_D.STS_G_COMP_ID, '')  END AS COMP_ID " & _
            " FROM VW_OSO_STUDENT_M  INNER JOIN" & _
            " dbo.STUDENT_D WITH(NOLOCK) ON dbo.VW_OSO_STUDENT_M .STU_SIBLING_ID = dbo.STUDENT_D.STS_STU_ID" & _
            " AND  STU_ACD_ID IN (SELECT ACD_ID FROM ACADEMICYEAR_D WITH(NOLOCK) WHERE (ACD_CURRENT=1 or ACD_ENDDT > GETDATE()) AND  ACD_BSU_ID = '" & Session("sBSUID") & "' )  " & _
            " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS not in('CN','TF')"

            If Request.QueryString("NewStudent") IsNot Nothing And Request.QueryString("ACD_ID") IsNot Nothing Then
                Dim NewStudent As String = Request.QueryString("NewStudent")
                Dim ACD_ID As String = Request.QueryString("ACD_ID")
                If NewStudent Then
                    str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) >= (SELECT ACD_STARTDT FROM ACADEMICYEAR_D WITH(NOLOCK) WHERE ACD_ID= " & Val(ACD_ID) & ")"
                    str_Sql &= " AND STU_ACD_ID=" & ACD_ID.ToString
                    str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) > dateadd(month,-2,getdate()) "
                Else
                    str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) < dateadd(month,-2,getdate()) "
                End If
            End If

            str_Sql &= " ) a "

            If Request.QueryString("IsService") IsNot Nothing Then
                Dim IsService As String = Request.QueryString("IsService")
                If IsService Then
                    str_Sql &= " INNER JOIN (SELECT DISTINCT SSV_STU_ID FROM OASIS_FEES.FEES.VW_PROFORMA_STUD_SERVICES WHERE   isnull(SSV_TODATE,getdate())>=getdate() and SSV_BSU_ID='" & Session("sBSUID") & "' "
                    str_Sql &= " AND  SSV_ACD_ID IN (SELECT ACD_ID FROM ACADEMICYEAR_D WITH(NOLOCK) WHERE (ACD_CURRENT=1 or ACD_ENDDT > GETDATE()) AND  ACD_BSU_ID = '" & Session("sBSUID") & "' )"
                    If Request.QueryString("FeeTypes") IsNot Nothing Then
                        Dim FeeTypes As String = Request.QueryString("FeeTypes")
                        str_Sql &= " AND SSV_FEE_ID IN (select * from dbo.fnSplitMe('" & FeeTypes & "','|'))"
                    End If
                    str_Sql &= ")AS B ON a.STU_ID=B.SSV_STU_ID  "
                End If
            End If

            str_Sql &= "WHERE a.STU_BSU_ID='" & Session("sBSUID") & "'"

            If COMP_ID <> "-1" Then
                str_Sql += " AND a.COMP_ID = " & COMP_ID
            End If

            If Request.QueryString("EXL_TYPE") IsNot Nothing Then
                Dim EXL_TYPE As String = Request.QueryString("EXL_TYPE")
                str_Sql &= " AND STU_ID not in (select EXL_STU_ID from OASIS_FEES.[FEES].[STUDENT_EXCLUDE_LIST] WITH(NOLOCK) WHERE EXL_TYPE='" & EXL_TYPE & "'  and EXL_BSU_ID='" & Session("sBSUID") & "') "
                If EXL_TYPE = "PI" Then
                    If Request.QueryString("EXCL_TC") IsNot Nothing AndAlso Request.QueryString("EXCL_TC") = "1" Then 'Exclude future TC students, added condition by Jacob on 08/Jun/2016
                        str_Sql &= " AND STU_ID NOT IN (SELECT TCM_STU_ID from OASIS..TCM_M WITH(NOLOCK) where isnull( TCM_Bcancelled,0)=0 AND ISNULL(TCM_bPREAPPROVED,0)<>0 and TCM_BSU_ID='" & Session("sBSUID") & "') "
                    Else
                        str_Sql &= " AND STU_ID NOT IN (SELECT TCM_STU_ID from OASIS..TCM_M WITH(NOLOCK) where isnull( TCM_Bcancelled,0)=0 AND ISNULL(TCM_bPREAPPROVED,0)<>0 and TCM_BSU_ID='" & Session("sBSUID") & "' and ISNULL(TCM_LASTATTDATE,GETDATE()+1)<GETDATE()) "
                    End If

                End If
            End If

            Dim ds As New DataSet
            Dim str_orderby As String = " ORDER BY " & IIf(str_filter_control <> "", "LTRIM(RTRIM(GRD_DISPLAY))", "LTRIM(RTRIM(STU_NAME))") & " "
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            'str_Sql & str_filter_debit & str_filter_acctype & _
            'str_filter_control & str_filter_code & str_filter_name & str_filter_pname & str_filter_pmobile & str_orderby)



            Dim NewStudent_fltr As String = ""
            Dim ACD_ID_fltr As String = ""
            Dim IsService_fltr As String = ""
            Dim FeeTypes_fltr As String = ""
            Dim EXL_TYPE_fltr As String = ""
            Dim EXCL_TC_fltr As String = ""
            Dim Enroll_type_fltr As String = ""
            If Request.QueryString("NewStudent") IsNot Nothing Then
                NewStudent_fltr = Request.QueryString("NewStudent")
            End If

            If Request.QueryString("ACD_ID") IsNot Nothing Then
                ACD_ID_fltr = Request.QueryString("ACD_ID")
            End If
            If Request.QueryString("IsService") IsNot Nothing Then
                IsService_fltr = Request.QueryString("IsService")
            End If

            If Request.QueryString("FeeTypes") IsNot Nothing Then
                FeeTypes_fltr = Request.QueryString("FeeTypes")
            End If

            If Request.QueryString("EXL_TYPE") IsNot Nothing Then
                EXL_TYPE_fltr = Request.QueryString("EXL_TYPE")
            End If

            If Request.QueryString("EXCL_TC") IsNot Nothing Then
                EXCL_TC_fltr = Request.QueryString("EXCL_TC")
            End If

            'If ddlType.SelectedValue = "1" Then
            '    Enroll_type_fltr = "ALL"
            'Else
            '    Enroll_type_fltr = "ENROLL"
            'End If
            Enroll_type_fltr = ddlType.SelectedValue

            Dim pParms(13) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
            pParms(0).Value = 1
            pParms(1) = New SqlClient.SqlParameter("@BSU_CODE", SqlDbType.VarChar)
            pParms(1).Value = Session("sBSUID")
            pParms(2) = New SqlClient.SqlParameter("@NewStudent", SqlDbType.VarChar)
            pParms(2).Value = NewStudent_fltr
            pParms(3) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.VarChar)
            pParms(3).Value = ACD_ID_fltr
            pParms(4) = New SqlClient.SqlParameter("@POP_ACD_ID", SqlDbType.VarChar)
            pParms(4).Value = ACD_ID_fltr
            pParms(5) = New SqlClient.SqlParameter("@IsService", SqlDbType.VarChar)
            pParms(5).Value = IsService_fltr
            pParms(6) = New SqlClient.SqlParameter("@FEE_TYPE", SqlDbType.VarChar)
            pParms(6).Value = FeeTypes_fltr
            pParms(7) = New SqlClient.SqlParameter("@EXL_TYPE", SqlDbType.VarChar)
            pParms(7).Value = EXL_TYPE_fltr
            pParms(8) = New SqlClient.SqlParameter("@EXCL_TC", SqlDbType.VarChar)
            pParms(8).Value = EXCL_TC_fltr
            pParms(9) = New SqlClient.SqlParameter("@COMP_ID", SqlDbType.VarChar)
            pParms(9).Value = COMP_ID
            pParms(10) = New SqlClient.SqlParameter("@STU_NO_FLTR", SqlDbType.VarChar)
            pParms(10).Value = str_txtCode
            pParms(11) = New SqlClient.SqlParameter("@STU_NAME_FLTR", SqlDbType.VarChar)
            pParms(11).Value = str_txtName
            pParms(12) = New SqlClient.SqlParameter("@STU_GRD_FLTR", SqlDbType.VarChar)
            pParms(12).Value = str_txtControl
            pParms(13) = New SqlClient.SqlParameter("@ENROLL_TYPE_FLTR", SqlDbType.VarChar)
            pParms(13).Value = Enroll_type_fltr
            str_Sql = "[OASIS].[DBO].[GET_STU_NEW_POPUP_LIST]"
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, str_Sql, pParms)



            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            '  gvGroup.Columns(0).Visible = False

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
            txtSearch.Text = str_txtControl
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtpname
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtpmobile

            set_Menu_Img()
            SetChk(Me.Page)

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindStudent_For_BSU(ByVal BSU_ID As String, Optional ByVal ACD_ID As String = "")
        Try
            Dim str_filter_acctype, str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtControl As String

            Dim i_dd_acctype As Integer = 0

            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""
            Dim str_filter_debit As String = String.Empty
            str_txtCode = ""
            str_txtName = ""
            str_txtControl = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim str_filter_pname As String = String.Empty
            Dim str_filter_pmobile As String = String.Empty
            Dim str_txtpname As String = String.Empty
            Dim str_txtpmobile As String = String.Empty


            Dim ddacctype As New DropDownList
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                'Try
                '    Dim s As HtmlControls.HtmlImage = gvGroup.HeaderRow.FindControl("mnu_2_img")
                '    ddacctype = gvGroup.HeaderRow.FindControl("DDAccountType")
                'Catch ex As Exception
                'End Try

                'If ddacctype.SelectedItem.Value <> "All" Then
                '    str_filter_acctype = " AND STU_GENDER='" & ddacctype.SelectedItem.Value & "'"
                'End If
                str_mode = Request.QueryString("ShowType") 'ShowType
                i_dd_acctype = ddacctype.SelectedIndex
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
                str_txtControl = txtSearch.Text
                str_filter_control = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtpname = txtSearch.Text
                str_filter_pname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtpmobile = txtSearch.Text
                str_filter_pmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))

            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = " select * from ( SELECT STU_ID, STU_NO, " & _
            " STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_NAME, PARENT_MOBILE, " & _
            " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, " & _
            " CASE dbo.VW_OSO_STUDENT_M .STU_PRIMARYCONTACT WHEN 'F' " & _
            " THEN IsNULL(dbo.STUDENT_D.STS_F_COMP_ID, '') " & _
            " WHEN 'M' THEN IsNULL(dbo.STUDENT_D.STS_M_COMP_ID, '') " & _
            " WHEN 'G' THEN IsNULL(dbo.STUDENT_D.STS_G_COMP_ID, '') END AS COMP_ID, STU_CURRSTATUS" & _
            " FROM VW_OSO_STUDENT_M  INNER JOIN" & _
            " dbo.STUDENT_D WITH (NOLOCK) ON dbo.VW_OSO_STUDENT_M .STU_SIBLING_ID = dbo.STUDENT_D.STS_STU_ID) a" & _
            " WHERE a.STU_BSU_ID='" & BSU_ID & "'"
            If ACD_ID <> "" Then
                str_Sql += "AND STU_ACD_ID = " & ACD_ID & " "
            End If

            If ddlType.SelectedValue = "1" Then
                str_Sql += "AND STU_CURRSTATUS = 'EN'"
            ElseIf ddlType.SelectedValue = "0" Then
                str_Sql += "AND STU_CURRSTATUS <> 'EN'"
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_debit & str_filter_acctype & _
            str_filter_control & str_filter_code & str_filter_pmobile & str_filter_pname & str_filter_name & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            '  gvGroup.Columns(0).Visible = False
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
            txtSearch.Text = str_txtControl

            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtpname
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtpmobile

            set_Menu_Img()
            SetChk(Me.Page)

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub GridbindStudent_For_BSU_Service(ByVal BSU_ID As String, Optional ByVal ACD_ID As String = "")
        Try
            Dim str_filter_acctype, str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtControl As String

            Dim i_dd_acctype As Integer = 0

            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""
            Dim str_filter_debit As String = String.Empty
            str_txtCode = ""
            str_txtName = ""
            str_txtControl = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim str_filter_pname As String = String.Empty
            Dim str_filter_pmobile As String = String.Empty
            Dim str_txtpname As String = String.Empty
            Dim str_txtpmobile As String = String.Empty


            Dim ddacctype As New DropDownList
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                'Try
                '    Dim s As HtmlControls.HtmlImage = gvGroup.HeaderRow.FindControl("mnu_2_img")
                '    ddacctype = gvGroup.HeaderRow.FindControl("DDAccountType")
                'Catch ex As Exception
                'End Try

                'If ddacctype.SelectedItem.Value <> "All" Then
                '    str_filter_acctype = " AND STU_GENDER='" & ddacctype.SelectedItem.Value & "'"
                'End If
                str_mode = Request.QueryString("ShowType") 'ShowType
                i_dd_acctype = ddacctype.SelectedIndex
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
                str_txtControl = txtSearch.Text
                str_filter_control = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtpname = txtSearch.Text
                str_filter_pname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtpmobile = txtSearch.Text
                str_filter_pmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))

            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = " select * from ( SELECT STU_ID, STU_NO, " & _
            " STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_NAME, PARENT_MOBILE, " & _
            " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, " & _
            " CASE dbo.VW_OSO_STUDENT_M .STU_PRIMARYCONTACT WHEN 'F' " & _
            " THEN IsNULL(dbo.STUDENT_D.STS_F_COMP_ID, '') " & _
            " WHEN 'M' THEN IsNULL(dbo.STUDENT_D.STS_M_COMP_ID, '') " & _
            " WHEN 'G' THEN IsNULL(dbo.STUDENT_D.STS_G_COMP_ID, '') END AS COMP_ID" & _
            " FROM VW_OSO_STUDENT_M  INNER JOIN" & _
            " dbo.STUDENT_D ON dbo.VW_OSO_STUDENT_M .STU_SIBLING_ID = dbo.STUDENT_D.STS_STU_ID " & _
            " ) a" & _
            " WHERE a.STU_BSU_ID='" & BSU_ID & "' AND a.STU_ID NOT IN (SELECT SSV_STU_ID FROM OASIS.dbo.STUDENT_SERVICES_D WHERE SSV_SVC_ID <> 1 AND SSV_TODATE IS NULL)  "
            If ACD_ID <> "" Then
                str_Sql += " AND STU_ACD_ID = " & ACD_ID & " "
            End If
            'INNER JOIN OASIS.dbo.STUDENT_SERVICES_D ON STU_ID<>SSV_STU_ID 
            'str_Sql += " AND SSV_SVC_ID <> 1 "

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_debit & str_filter_acctype & _
            str_filter_control & str_filter_code & str_filter_pmobile & str_filter_pname & str_filter_name & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            '  gvGroup.Columns(0).Visible = False
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
            txtSearch.Text = str_txtControl

            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtpname
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtpmobile

            set_Menu_Img()
            SetChk(Me.Page)

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindStudent_For_BSU_Service_NEW(ByVal BSU_ID As String, Optional ByVal ACD_ID As String = "", Optional ByVal SVC_ID As String = "1")
        Try
            Dim str_filter_acctype, str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtControl As String

            Dim i_dd_acctype As Integer = 0

            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""
            Dim str_filter_debit As String = String.Empty
            str_txtCode = ""
            str_txtName = ""
            str_txtControl = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim str_filter_pname As String = String.Empty
            Dim str_filter_pmobile As String = String.Empty
            Dim str_txtpname As String = String.Empty
            Dim str_txtpmobile As String = String.Empty


            Dim ddacctype As New DropDownList
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                'Try
                '    Dim s As HtmlControls.HtmlImage = gvGroup.HeaderRow.FindControl("mnu_2_img")
                '    ddacctype = gvGroup.HeaderRow.FindControl("DDAccountType")
                'Catch ex As Exception
                'End Try

                'If ddacctype.SelectedItem.Value <> "All" Then
                '    str_filter_acctype = " AND STU_GENDER='" & ddacctype.SelectedItem.Value & "'"
                'End If
                str_mode = Request.QueryString("ShowType") 'ShowType
                i_dd_acctype = ddacctype.SelectedIndex
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
                str_txtControl = txtSearch.Text
                str_filter_control = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtpname = txtSearch.Text
                str_filter_pname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtpmobile = txtSearch.Text
                str_filter_pmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))

            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = " select * from ( SELECT STU_ID, STU_NO, " & _
            " STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_NAME, PARENT_MOBILE, " & _
            " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, " & _
            " CASE dbo.VW_OSO_STUDENT_M .STU_PRIMARYCONTACT WHEN 'F' " & _
            " THEN IsNULL(dbo.STUDENT_D.STS_F_COMP_ID, '') " & _
            " WHEN 'M' THEN IsNULL(dbo.STUDENT_D.STS_M_COMP_ID, '') " & _
            " WHEN 'G' THEN IsNULL(dbo.STUDENT_D.STS_G_COMP_ID, '') END AS COMP_ID" & _
            " FROM VW_OSO_STUDENT_M  INNER JOIN" & _
            " dbo.STUDENT_D ON dbo.VW_OSO_STUDENT_M .STU_SIBLING_ID = dbo.STUDENT_D.STS_STU_ID " & _
            " ) a" & _
            " WHERE a.STU_BSU_ID='" & BSU_ID & "' AND a.STU_ID NOT IN (SELECT SSV_STU_ID FROM OASIS.dbo.STUDENT_SERVICES_D WHERE SSV_SVC_ID='" & SVC_ID & "' AND SSV_SVC_ID <> 1 AND SSV_TODATE IS NULL)  "
            If ACD_ID <> "" Then
                str_Sql += " AND STU_ACD_ID = " & ACD_ID & " "
            End If
            'INNER JOIN OASIS.dbo.STUDENT_SERVICES_D ON STU_ID<>SSV_STU_ID 
            'str_Sql += " AND SSV_SVC_ID <> 1 "

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_debit & str_filter_acctype & _
            str_filter_control & str_filter_code & str_filter_pmobile & str_filter_pname & str_filter_name & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            '  gvGroup.Columns(0).Visible = False
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
            txtSearch.Text = str_txtControl

            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtpname
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtpmobile

            set_Menu_Img()
            SetChk(Me.Page)

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        SetChk(Me.Page)
        Bind_All()
    End Sub

    Protected Sub DDAccountType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Bind_All()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSTU_ID As New Label
        Dim lbClose As New LinkButton
        lbClose = sender
        lblSTU_ID = sender.Parent.FindControl("lblSTU_ID")
        Dim lblCode As New Label
        lblCode = sender.Parent.FindControl("lblCode")
        If (Not lblSTU_ID Is Nothing) Then
            '   Response.Write(lblcode.Text)
            '   Response.Write("<script language='javascript'> function listen_window(){")
            '   Response.Write("window.returnValue = '" & lblSTU_ID.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblCode.Text & "';")
            '   Response.Write("window.close();")
            '   Response.Write("} </script>")

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & lblSTU_ID.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblCode.Text & "';")
            Response.Write("var oWnd = GetRadWindow('" & lblSTU_ID.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblCode.Text & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvGroup.PageIndex = 0
        Bind_All()
    End Sub

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Protected Sub chkSelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelAll.CheckedChanged
        Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
        Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
        Dim BSU_ID As String = Request.QueryString("bsu")
        If chkSelAll.Checked Then
            Dim str_Filter As String = GetFilter()
            Dim BUnitreaderSuper As SqlDataReader
            Select Case type
                Case "STUD_COMP"
                    BUnitreaderSuper = GetAllStudentsListActive(COMP_ID, str_Filter)
                Case "ENQ_COMP"
                    BUnitreaderSuper = GetAllEnquiryList(COMP_ID, str_Filter)
                Case "ENQUIRY"
                    BUnitreaderSuper = GetAllEnquiryListForBSU(BSU_ID, str_Filter)
                Case "STUD_BSU"
                    BUnitreaderSuper = GetAllStudentsList(COMP_ID, str_Filter)
                Case "SERVICE_COMP"
                    BUnitreaderSuper = GetAllServiceStudents(COMP_ID, str_Filter)
            End Select
            If BUnitreaderSuper.HasRows = True Then
                While (BUnitreaderSuper.Read())
                    Session("liUserList").Remove(BUnitreaderSuper(0).ToString)
                    Session("liUserList").Add(BUnitreaderSuper(0).ToString)
                End While
            End If
        Else
            Session("liUserList").Clear()
        End If
        Bind_All()
        SetChk(Me.Page)
    End Sub

    Private Function GetFilter() As String
        Dim str_filter_acctype, str_mode As String
        Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
        Dim str_txtCode, str_txtName, str_txtControl, str_filter_pname, str_filter_pmobile As String
        Dim i_dd_acctype As Integer = 0
        str_filter_acctype = ""
        str_filter_code = ""
        str_filter_name = ""
        str_filter_control = ""
        Dim str_filter_debit As String = String.Empty
        str_txtCode = ""
        str_txtName = ""
        str_txtControl = ""
        str_mode = Request.QueryString("ShowType") 'PARTY_D
        Dim txtSearch As New TextBox
        Try
            Dim s As HtmlControls.HtmlImage = gvGroup.HeaderRow.FindControl("mnu_2_img")
        Catch ex As Exception
        End Try

        str_mode = Request.QueryString("ShowType") 'ShowType
        ''code
        Dim str_Sid_search() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_search = h_selected_menu_1.Value.Split("__")
        str_search = str_Sid_search(0)
        txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
        str_txtCode = txtSearch.Text
        str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
        ''name
        str_Sid_search = h_Selected_menu_2.Value.Split("__")
        str_search = str_Sid_search(0)
        txtSearch = gvGroup.HeaderRow.FindControl("txtName")
        str_txtName = txtSearch.Text
        str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
        ''control
        str_Sid_search = h_Selected_menu_3.Value.Split("__")
        str_search = str_Sid_search(0)
        txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
        str_filter_control = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))

        str_Sid_search = h_Selected_menu_4.Value.Split("__")
        str_search = str_Sid_search(0)
        txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
        str_filter_pname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))

        ''control
        str_Sid_search = h_Selected_menu_5.Value.Split("__")
        str_search = str_Sid_search(0)
        txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
        str_filter_pmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))

        Return str_filter_debit & str_filter_acctype & str_filter_control & str_filter_code & str_filter_name & str_filter_pname & str_filter_pmobile
    End Function

    Private Function GetAllEnquiryList(ByVal COMP_ID As String, ByVal str_Filter As String) As SqlDataReader
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = " Select * from FEES.vw_OSO_ENQUIRY_COMP "
        str_Sql += " WHERE  STU_BSU_ID='" & Session("sBSUID") & "'"
        Dim ACD_ID As String = IIf(Request.QueryString("ACD_ID") = "", -1, Request.QueryString("ACD_ID"))
        If ACD_ID <> "-1" Then
            str_Sql += " AND  STU_ACD_ID = " & ACD_ID
        End If
        If COMP_ID <> "-1" Then
            str_Sql += " AND  COMP_ID = " & COMP_ID
        End If
        str_Sql &= " AND STU_STATUS<>'DEL' "
        str_Sql += str_Filter
        If Request.QueryString("EXL_TYPE") IsNot Nothing Then
            Dim EXL_TYPE As String = Request.QueryString("EXL_TYPE")
            str_Sql &= " AND STU_ID not in (select EXL_STU_ID from OASIS_FEES.[FEES].[STUDENT_EXCLUDE_LIST] WHERE EXL_TYPE='" & EXL_TYPE & "'  and EXL_BSU_ID='" & Session("sBSUID") & "') "
            If EXL_TYPE = "PI" Then
                str_Sql &= " AND STU_ID not in (select TCM_STU_ID from OASIS..TCM_M where isnull( TCM_Bcancelled,0)=0  and TCM_BSU_ID='" & Session("sBSUID") & "' and ISNULL(TCM_LASTATTDATE,GETDATE()+1)<GETDATE()) "
            End If
        End If
        Return SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
    End Function

    Private Function GetAllEnquiryListForBSU(ByVal BSU_ID As String, ByVal str_Filter As String) As SqlDataReader
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = " Select * from FEES.vw_OSO_ENQUIRY_COMP "
        str_Sql += " WHERE  STU_BSU_ID='" & BSU_ID & "'" & str_Filter
        str_Sql &= " AND STU_STATUS<>'DEL' "
        Return SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
    End Function

    Private Function GetAllStudentsList(ByVal COMP_ID As String, ByVal str_Filter As String) As SqlDataReader
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = " select STU_ID,STU_BSU_ID,  STU_bActive from " & _
        "( SELECT STU_ID, STU_BSU_ID, STU_bActive ," & _
        " CASE dbo.VW_OSO_STUDENT_M .STU_PRIMARYCONTACT WHEN 'F' " & _
        " THEN IsNULL(dbo.STUDENT_D.STS_F_COMP_ID, '') " & _
        " WHEN 'M' THEN IsNULL(dbo.STUDENT_D.STS_M_COMP_ID, '') " & _
        " WHEN 'G' THEN IsNULL(dbo.STUDENT_D.STS_G_COMP_ID, '') END AS COMP_ID" & _
        " FROM VW_OSO_STUDENT_M  INNER JOIN" & _
        " dbo.STUDENT_D ON dbo.VW_OSO_STUDENT_M .STU_ID = dbo.STUDENT_D.STS_STU_ID WHERE 1=1 " & _
        str_Filter & ") a" & _
        " WHERE a.STU_BSU_ID='" & Session("sBSUID") & "' AND STU_bActive =1 "
        If COMP_ID <> "-1" Then
            str_Sql += " AND a.COMP_ID = " & COMP_ID
        End If
        Return SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
    End Function

    Private Function GetAllStudentsListActive(ByVal COMP_ID As String, ByVal str_Filter As String) As SqlDataReader
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = " select STU_ID,STU_BSU_ID,  STU_bActive from " & _
        "( SELECT STU_ID, STU_NO, " & _
                    " STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_NAME, PARENT_MOBILE, " & _
                    " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, " & _
                    " CASE isnull(oasis.dbo.STUDENT_D.STS_FEESPONSOR,0) WHEN 4 " & _
                    " THEN IsNULL(oasis.dbo.STUDENT_D.STS_F_COMP_ID, '')   " & _
                    " WHEN 5 THEN IsNULL(oasis.dbo.STUDENT_D.STS_M_COMP_ID, '')   " & _
                    " WHEN 6 THEN IsNULL(oasis.dbo.STUDENT_D.STS_G_COMP_ID, '')  END AS COMP_ID " & _
                    " FROM VW_OSO_STUDENT_M  INNER JOIN" & _
                    " dbo.STUDENT_D ON dbo.VW_OSO_STUDENT_M .STU_SIBLING_ID = dbo.STUDENT_D.STS_STU_ID" & _
                    " AND  STU_ACD_ID IN (SELECT ACD_ID FROM ACADEMICYEAR_D WHERE (ACD_CURRENT=1 or ACD_ENDDT > GETDATE()) AND  ACD_BSU_ID = '" & Session("sBSUID") & "' )  " & _
                    " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS not in('CN','TF')  " & str_Filter

        If Request.QueryString("NewStudent") IsNot Nothing And Request.QueryString("ACD_ID") IsNot Nothing Then
            Dim NewStudent As String = Request.QueryString("NewStudent")
            Dim ACD_ID As String = Request.QueryString("ACD_ID")
            If NewStudent Then
                str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) >= (SELECT ACD_STARTDT FROM ACADEMICYEAR_D WHERE ACD_ID= " & Val(ACD_ID) & ")"
                str_Sql &= " AND STU_ACD_ID=" & ACD_ID.ToString
                str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) > dateadd(month,-2,getdate()) "
            Else
                str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) < dateadd(month,-2,getdate()) "
            End If
        End If

        str_Sql &= " ) a WHERE a.STU_BSU_ID='" & Session("sBSUID") & "' AND STU_bActive =1 "

        If COMP_ID <> "-1" Then
            str_Sql += " AND a.COMP_ID = " & COMP_ID
        End If
        If Request.QueryString("EXL_TYPE") IsNot Nothing Then
            Dim EXL_TYPE As String = Request.QueryString("EXL_TYPE")
            str_Sql &= " AND STU_ID not in (select EXL_STU_ID from OASIS_FEES.[FEES].[STUDENT_EXCLUDE_LIST] WHERE EXL_TYPE='" & EXL_TYPE & "'  and EXL_BSU_ID='" & Session("sBSUID") & "') "
            If EXL_TYPE = "PI" Then
                If Request.QueryString("EXCL_TC") IsNot Nothing AndAlso Request.QueryString("EXCL_TC") = "1" Then 'Exclude future TC students, added condition by Jacob on 08/Jun/2016
                    str_Sql &= " AND STU_ID not in (select TCM_STU_ID from OASIS..TCM_M where isnull( TCM_Bcancelled,0)=0  and TCM_BSU_ID='" & Session("sBSUID") & "') "
                Else
                    str_Sql &= " AND STU_ID not in (select TCM_STU_ID from OASIS..TCM_M where isnull( TCM_Bcancelled,0)=0  and TCM_BSU_ID='" & Session("sBSUID") & "' and ISNULL(TCM_LASTATTDATE,GETDATE()+1)<GETDATE()) "
                End If

            End If
        End If

        If Request.QueryString("IsService") IsNot Nothing Then
            Dim IsService As String = Request.QueryString("IsService")
            If IsService Then
                str_Sql &= " AND STU_ID IN (SELECT  SSV_STU_ID FROM OASIS_FEES.FEES.VW_PROFORMA_STUD_SERVICES WHERE   isnull(SSV_TODATE,getdate())>=getdate() and SSV_BSU_ID='" & Session("sBSUID") & "' "
                str_Sql &= " AND  SSV_ACD_ID IN (SELECT ACD_ID FROM ACADEMICYEAR_D WHERE (ACD_CURRENT=1 or ACD_ENDDT > GETDATE()) AND  ACD_BSU_ID = '" & Session("sBSUID") & "' )"
                If Request.QueryString("FeeTypes") IsNot Nothing Then
                    Dim FeeTypes As String = Request.QueryString("FeeTypes")
                    str_Sql &= " AND  SSV_FEE_ID IN (select * from dbo.fnSplitMe('" & FeeTypes & "','|'))"
                End If
                str_Sql &= ") "
            End If
        End If


        Return SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
    End Function

    Protected Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        SetChk(Me.Page)
        h_SelectedId.Value = ""
        For i As Integer = 0 To Session("liUserList").Count - 1
            If h_SelectedId.Value <> "" Then
                h_SelectedId.Value += "||"
            End If
            h_SelectedId.Value += Session("liUserList")(i).ToString
        Next
        '  Response.Write("<script language='javascript'> function listen_window(){")
        '   Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        '  Response.Write("window.close();")
        '  Response.Write("} </script>")

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.NameandCode ='" & h_SelectedId.Value & "';")
        Response.Write("var oWnd = GetRadWindow('" & h_SelectedId.Value & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")
    End Sub

    Sub Bind_All()
        Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
        Select Case type
            Case "STUD_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GridbindStudentCompany(COMP_ID)
            Case "ENQ_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                Dim ACD_ID As String = IIf(Request.QueryString("ACD_ID") = "", -1, Request.QueryString("ACD_ID"))
                GridbindEnquiryCompany(COMP_ID, ACD_ID)
            Case "STUD_BSU"
                Dim BSU_ID As String = IIf(Request.QueryString("bsu") = "", Session("sBSUID"), Request.QueryString("bsu"))
                Dim ACD_ID As String = IIf(Request.QueryString("ACD_ID") = "", "", Request.QueryString("ACD_ID"))
                GridbindStudent_For_BSU(BSU_ID, ACD_ID)
            Case "STUD_BSU_SRV"
                Dim BSU_ID As String = IIf(Request.QueryString("bsu") = "", Session("sBSUID"), Request.QueryString("bsu"))
                Dim ACD_ID As String = IIf(Request.QueryString("ACD_ID") = "", "", Request.QueryString("ACD_ID"))
                GridbindStudent_For_BSU_Service(BSU_ID, ACD_ID)
            Case "STUD_BSU_SRV_NEW"
                Dim BSU_ID As String = IIf(Request.QueryString("bsu") = "", Session("sBSUID"), Request.QueryString("bsu"))
                Dim ACD_ID As String = IIf(Request.QueryString("ACD_ID") = "", "", Request.QueryString("ACD_ID"))
                Dim SVC_ID As String = IIf(Request.QueryString("SVC_ID") = "", "", Request.QueryString("SVC_ID"))
                GridbindStudent_For_BSU_Service_NEW(BSU_ID, ACD_ID, SVC_ID)
            Case "REFER"
                Dim STU_ID As String = IIf(Request.QueryString("STU_ID") = "", -1, Request.QueryString("STU_ID"))
                Gridbind(STU_ID)
            Case "ENQUIRY"
                Dim BSU_ID As String = Request.QueryString("bsu")
                GridbindEnquiryForBSU(BSU_ID)
            Case "SERVICE_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GetServiceStudents(COMP_ID)
            Case Else
                Gridbind()
        End Select
    End Sub

    Private Sub GetServiceStudents(ByVal COMP_ID As Integer)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade As String
            Dim str_txtCode, str_txtName, str_txtGrade As String
            str_filter_code = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_mode = Request.QueryString("ShowType")
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType 
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtControl")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
            End If
            Dim BSU As String = ""
            If Request.QueryString("BSU") IsNot Nothing Then
                BSU = Request.QueryString("BSU")
            End If

            Dim str_Sql As String = " SELECT STU_ID,STU_NO,STU_BSU_ID,STU_NAME,STU_GENDER, " & _
                                    "PARENT_MOBILE,PARENT_NAME,GRD_DISPLAY,STU_ACD_ID , " & _
                                    "ACY_DESCR, STU_GRD_ID, STU_bActive " & _
                                    "FROM OASIS_SERVICES.dbo.VW_OSO_STUDENT_M " & _
                                    "WHERE STU_BSU_ID = '" & BSU & "' AND " & _
                                    "STU_ID IN ( SELECT DISTINCT SSR_STU_ID FROM STUDENT_SERVICES_REQUEST WITH(NOLOCK) WHERE SSR_BSU_ID = '500610' ) "

            If Request.QueryString("STUID") IsNot Nothing Then
                Dim STUID As String = Request.QueryString("STUID")
                str_Sql = str_Sql & " AND STU_SIBLING_ID <> STU_ID AND STU_SIBLING_ID=" & STUID & ""
            End If
            If COMP_ID <> "-1" Then
                str_Sql = str_Sql & " AND ISNULL(COMP_ID,0) = " & COMP_ID
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & " ORDER BY  LTRIM(RTRIM(STU_NAME)) ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function GetAllServiceStudents(ByVal COMP_ID As Integer, ByVal str_filter As String) As SqlDataReader
        Try
            Dim str_mode As String

            str_mode = Request.QueryString("ShowType")

            Dim BSU As String = ""
            If Request.QueryString("BSU") IsNot Nothing Then
                BSU = Request.QueryString("BSU")
            End If

            Dim str_Sql As String = " select * from ( SELECT STU_ID, STU_NO, " & _
            " STU_BSU_ID, STU_NAME, GRM_DESCR,SCT_DESCR,STU_GENDER,PARENT_NAME,PARENT_MOBILE, " & _
            " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, " & _
            " CASE isnull(oasis.dbo.STUDENT_D.STS_FEESPONSOR,0) WHEN 4 " & _
            " THEN IsNULL(oasis.dbo.STUDENT_D.STS_F_COMP_ID, '')   " & _
            " WHEN 5 THEN IsNULL(oasis.dbo.STUDENT_D.STS_M_COMP_ID, '')   " & _
            " WHEN 6 THEN IsNULL(oasis.dbo.STUDENT_D.STS_G_COMP_ID, '')  END AS COMP_ID " & _
            " FROM VW_OSO_STUDENT_M  INNER JOIN" & _
            " dbo.STUDENT_D ON ISNULL(dbo.VW_OSO_STUDENT_M.STU_SIBLING_ID, STU_ID) = STS_STU_ID" & _
            " AND  STU_ACD_ID IN (SELECT ACD_ID FROM ACADEMICYEAR_D WHERE (ACD_CURRENT=1 or ACD_ENDDT > GETDATE()) AND  ACD_BSU_ID = '" & BSU & "' )  " & _
            " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS not in('CN','TF') "

            If Request.QueryString("NewStudent") IsNot Nothing And Request.QueryString("ACD_ID") IsNot Nothing Then
                Dim NewStudent As String = Request.QueryString("NewStudent")
                Dim ACD_ID As String = Request.QueryString("ACD_ID")
                If NewStudent Then
                    str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) >= (SELECT ACD_STARTDT FROM ACADEMICYEAR_D WHERE ACD_ID= " & Val(ACD_ID) & ")"
                    str_Sql &= " AND STU_ACD_ID=" & ACD_ID.ToString
                    str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) > dateadd(month,-2,getdate()) "
                Else
                    'str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01'))  < (SELECT ACD_STARTDT FROM ACADEMICYEAR_D WHERE ACD_ID= " & Val(ACD_ID) & ")"
                    str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) < dateadd(month,-2,getdate()) "
                End If
            End If

            str_Sql &= " ) a WHERE a.STU_BSU_ID='" & BSU & "'"

            If Request.QueryString("EXL_TYPE") IsNot Nothing Then
                Dim EXL_TYPE As String = Request.QueryString("EXL_TYPE")
                str_Sql &= " AND a.STU_ID not in (select EXL_STU_ID from OASIS_FEES.[FEES].[STUDENT_EXCLUDE_LIST] WHERE EXL_TYPE='" & EXL_TYPE & "'  and EXL_BSU_ID='" & BSU & "') "
                If EXL_TYPE = "PI" Then
                    str_Sql &= " AND a.STU_ID not in (select TCM_STU_ID from OASIS..TCM_M where isnull( TCM_Bcancelled,0)=0 AND ISNULL(TCM_bPREAPPROVED,0)<>0  and TCM_BSU_ID='" & BSU & "' and ISNULL(TCM_LASTATTDATE,GETDATE()+1)<GETDATE()) "
                End If
            End If

            If Request.QueryString("IsService") IsNot Nothing Then
                Dim IsService As String = Request.QueryString("IsService")
                If IsService Then
                    str_Sql &= " AND a.STU_ID IN (SELECT  SSV_STU_ID FROM OASIS_FEES.FEES.VW_PROFORMA_STUD_SERVICES WHERE isnull(SSV_TODATE,getdate())>=getdate() and  SSV_BSU_ID='" & BSU & "' "
                    str_Sql &= " AND  SSV_ACD_ID IN (SELECT ACD_ID FROM ACADEMICYEAR_D WHERE (ACD_CURRENT=1 or ACD_ENDDT > GETDATE()) AND  ACD_BSU_ID = '" & BSU & "' )"
                    If Request.QueryString("FeeTypes") IsNot Nothing Then
                        Dim FeeTypes As String = Request.QueryString("FeeTypes")
                        str_Sql &= " AND  SSV_FEE_ID IN (select * from dbo.fnSplitMe('" & FeeTypes & "','|'))"
                    End If
                    str_Sql &= ") "
                End If
            End If

            If COMP_ID <> "-1" Then
                str_Sql += " AND a.COMP_ID = " & COMP_ID
            End If

            Return SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql & str_filter)
        Catch ex As Exception
            Return SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnectionString, CommandType.Text, " PRINT GETDATE() ")
            Errorlog(ex.Message)
        End Try

    End Function
    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        Bind_All()
    End Sub

#Region "Web Methods"


    <WebMethod()> _
    Public Shared Function GetStudents(ByVal type As String, ByVal COMP_ID As String, ByVal bNewStudent As String, _
                                       ByVal ACD_ID As String, ByVal IsService As String, ByVal EXL_TYPE As String, _
                                       ByVal EXCL_TC As String, ByVal FeeTypes As String, ByVal SearchFor As String, ByVal prefix As String) As String()

        If Not type Is Nothing Then
            Select Case type
                Case "STUD_COMP"
                    Return GetStudentComp(COMP_ID, bNewStudent, ACD_ID, IsService, EXL_TYPE, EXCL_TC, FeeTypes, SearchFor, prefix)
                Case "ENQ_COMP"
                    Return GetEnquiryComp(COMP_ID, bNewStudent, ACD_ID, IsService, EXL_TYPE, EXCL_TC, FeeTypes, SearchFor, prefix)
            End Select
        End If
        Return Nothing
    End Function

    Public Shared Function GetStudentComp(ByVal COMP_ID As String, ByVal bNewStudent As String, _
                                       ByVal ACD_ID As String, ByVal IsService As String, ByVal EXL_TYPE As String, _
                                       ByVal EXCL_TC As String, ByVal FeeTypes As String, ByVal SearchFor As String, ByVal prefix As String) As String()
        GetStudentComp = Nothing
        Dim Student As New List(Of String)()

        Dim str_Sql As String = " SELECT TOP 20 * FROM ( SELECT STU_ID, STU_NO, " & _
            " STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_NAME, PARENT_MOBILE, " & _
            " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, " & _
            " CASE isnull(oasis.dbo.STUDENT_D.STS_FEESPONSOR,0) WHEN 4 " & _
            " THEN IsNULL(oasis.dbo.STUDENT_D.STS_F_COMP_ID, '')   " & _
            " WHEN 5 THEN IsNULL(oasis.dbo.STUDENT_D.STS_M_COMP_ID, '')   " & _
            " WHEN 6 THEN IsNULL(oasis.dbo.STUDENT_D.STS_G_COMP_ID, '')  END AS COMP_ID " & _
            " FROM VW_OSO_STUDENT_M  INNER JOIN" & _
            " dbo.STUDENT_D WITH(NOLOCK) ON dbo.VW_OSO_STUDENT_M .STU_SIBLING_ID = dbo.STUDENT_D.STS_STU_ID" & _
            " AND  STU_ACD_ID IN (SELECT ACD_ID FROM ACADEMICYEAR_D WITH(NOLOCK) WHERE (ACD_CURRENT=1 or ACD_ENDDT > GETDATE()) AND  ACD_BSU_ID = '" & HttpContext.Current.Session("sBSUID") & "' )  " & _
            " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS not in('CN','TF')"

        If bNewStudent IsNot Nothing And ACD_ID IsNot Nothing Then
            If CBool(bNewStudent) Then
                str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) >= (SELECT ACD_STARTDT FROM ACADEMICYEAR_D WITH(NOLOCK) WHERE ACD_ID= " & Val(ACD_ID) & ")"
                str_Sql &= " AND STU_ACD_ID=" & ACD_ID.ToString
                str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) > dateadd(month,-2,getdate()) "
            Else
                str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) < dateadd(month,-2,getdate()) "
            End If
        End If

        str_Sql &= " ) a "

        If IsService IsNot Nothing Then
            If CBool(IsService) Then
                str_Sql &= " INNER JOIN (SELECT DISTINCT SSV_STU_ID FROM OASIS_FEES.FEES.VW_PROFORMA_STUD_SERVICES WHERE   isnull(SSV_TODATE,getdate())>=getdate() and SSV_BSU_ID='" & HttpContext.Current.Session("sBSUID") & "' "
                str_Sql &= " AND  SSV_ACD_ID IN (SELECT ACD_ID FROM ACADEMICYEAR_D WITH(NOLOCK) WHERE (ACD_CURRENT=1 or ACD_ENDDT > GETDATE()) AND  ACD_BSU_ID = '" & HttpContext.Current.Session("sBSUID") & "' )"
                If FeeTypes IsNot Nothing Then
                    str_Sql &= " AND SSV_FEE_ID IN (select * from dbo.fnSplitMe('" & FeeTypes & "','|'))"
                End If
                str_Sql &= ")AS B ON a.STU_ID=B.SSV_STU_ID  "
            End If
        End If

        str_Sql &= "WHERE a.STU_BSU_ID='" & HttpContext.Current.Session("sBSUID") & "'"

        If COMP_ID <> "-1" Then
            str_Sql += " AND a.COMP_ID = " & COMP_ID
        End If

        If EXL_TYPE IsNot Nothing Then
            str_Sql &= " AND STU_ID not in (select EXL_STU_ID from OASIS_FEES.[FEES].[STUDENT_EXCLUDE_LIST] WITH(NOLOCK) WHERE EXL_TYPE='" & EXL_TYPE & "'  and EXL_BSU_ID='" & HttpContext.Current.Session("sBSUID") & "') "
            If EXL_TYPE = "PI" Then
                If EXCL_TC IsNot Nothing AndAlso EXCL_TC = "1" Then 'Exclude future TC students, added condition by Jacob on 08/Jun/2016
                    str_Sql &= " AND STU_ID NOT IN (SELECT TCM_STU_ID from OASIS..TCM_M WITH(NOLOCK) where isnull( TCM_Bcancelled,0)=0 AND ISNULL(TCM_bPREAPPROVED,0)<>0 and TCM_BSU_ID='" & HttpContext.Current.Session("sBSUID") & "') "
                Else
                    str_Sql &= " AND STU_ID NOT IN (SELECT TCM_STU_ID from OASIS..TCM_M WITH(NOLOCK) where isnull( TCM_Bcancelled,0)=0 AND ISNULL(TCM_bPREAPPROVED,0)<>0 and TCM_BSU_ID='" & HttpContext.Current.Session("sBSUID") & "' and ISNULL(TCM_LASTATTDATE,GETDATE()+1)<GETDATE()) "
                End If

            End If
        End If
        Dim str_orderby As String = ""
        If SearchFor IsNot Nothing Then
            If SearchFor = "STU_NO" Then
                str_Sql &= " AND a.STU_NO LIKE '%" & prefix & "%' "
                str_orderby = " ORDER BY a.STU_NO "
            ElseIf SearchFor = "STU_NAME" Then
                str_Sql &= " AND a.STU_NAME LIKE '%" & prefix & "%' "
                str_orderby = " ORDER BY LTRIM(RTRIM(a.STU_NAME)) "
            End If

        End If



        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASISConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = str_Sql & str_orderby
                'cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        If SearchFor = "STU_NO" Then
                            Student.Add(String.Format("{0}|{1}|{2}|{3}|{4}", sdr("STU_NO"), sdr("STU_ID"), sdr("STU_NAME"), sdr("GRD_DISPLAY"), sdr("STU_GRD_ID")))
                        ElseIf SearchFor = "STU_NAME" Then
                            Student.Add(String.Format("{0}|{1}|{2}|{3}|{4}", sdr("STU_NAME"), sdr("STU_ID"), sdr("STU_NO"), sdr("GRD_DISPLAY"), sdr("STU_GRD_ID")))
                        End If
                    End While
                End Using
                conn.Close()
            End Using
            Return Student.ToArray()
        End Using
    End Function

    Public Shared Function GetEnquiryComp(ByVal COMP_ID As String, ByVal bNewStudent As String, _
                                       ByVal ACD_ID As String, ByVal IsService As String, ByVal EXL_TYPE As String, _
                                       ByVal EXCL_TC As String, ByVal FeeTypes As String, ByVal SearchFor As String, ByVal prefix As String) As String()
        GetEnquiryComp = Nothing
        Dim Student As New List(Of String)()
        Dim BSU_ID = HttpContext.Current.Session("sBsuId")
        Dim str_Sql As String = " SELECT TOP 20 * FROM FEES.vw_OSO_ENQUIRY_COMP "
        str_Sql += " WHERE  STU_BSU_ID='" & BSU_ID & "'"
        If ACD_ID <> "-1" Then
            str_Sql += " AND  STU_ACD_ID = " & ACD_ID
        End If
        If COMP_ID <> "-1" Then
            str_Sql += " AND  COMP_ID = " & COMP_ID
        End If
        str_Sql &= " AND STU_STATUS<>'DEL' "

        If EXL_TYPE IsNot Nothing Then
            str_Sql &= " AND STU_ID NOT IN (SELECT EXL_STU_ID FROM OASIS_FEES.[FEES].[STUDENT_EXCLUDE_LIST] WHERE EXL_TYPE='" & EXL_TYPE & "' AND EXL_BSU_ID='" & BSU_ID & "') "
            If EXL_TYPE = "PI" Then
                str_Sql &= " AND STU_ID NOT IN (SELECT TCM_STU_ID FROM OASIS..TCM_M WHERE isnull( TCM_Bcancelled,0)=0  AND TCM_BSU_ID='" & BSU_ID & "' AND ISNULL(TCM_LASTATTDATE,GETDATE()+1)<GETDATE()) "
            End If
        End If

        Dim str_orderby As String = ""
        If SearchFor IsNot Nothing Then
            If SearchFor = "STU_NO" Then
                str_Sql &= " AND STU_NO LIKE '%" & prefix & "%' "
                str_orderby = " ORDER BY STU_NO "
            ElseIf SearchFor = "STU_NAME" Then
                str_Sql &= " AND STU_NAME LIKE '%" & prefix & "%' "
                str_orderby = " ORDER BY LTRIM(RTRIM(STU_NAME)) "
            End If
        End If

        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASISConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = str_Sql & str_orderby
                'cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        If SearchFor = "STU_NO" Then
                            Student.Add(String.Format("{0}|{1}|{2}|{3}|{4}", sdr("STU_NO"), sdr("STU_ID"), sdr("STU_NAME"), sdr("GRD_DISPLAY"), sdr("STU_GRD_ID")))
                        ElseIf SearchFor = "STU_NAME" Then
                            Student.Add(String.Format("{0}|{1}|{2}|{3}|{4}", sdr("STU_NAME"), sdr("STU_ID"), sdr("STU_NO"), sdr("GRD_DISPLAY"), sdr("STU_GRD_ID")))
                        End If
                    End While
                End Using
                conn.Close()
            End Using
            Return Student.ToArray()
        End Using

    End Function
#End Region
End Class
