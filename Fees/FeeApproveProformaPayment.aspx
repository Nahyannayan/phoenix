<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FeeApproveProformaPayment.aspx.vb" Inherits="Fees_FeeApproveProformaPayment" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">
        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("chkAL").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                        if (document.forms[0].elements[i].type == 'checkbox') {
                            document.forms[0].elements[i].checked = chk_state;
                        }
                    }
                }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr valign="top">
                        <td valign="bottom" width="50%" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                        </td>
                        <td align="right" >
                            <table style="margin-left: 0px; height: 1px;" width="0px">
                                <tr>
                                    <td width="0" height="0px">
                                        <asp:CheckBox ID="chkSelection1" runat="server" CssClass="field-label" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td width="0" height="0px">
                                        <asp:CheckBox ID="chkSelection2" runat="server" CssClass="field-label" AutoPostBack="True" />
                                    </td>
                                    <td height="0px"></td>
                                    <td width="0" height="0px">
                                        <asp:RadioButton ID="rad1" runat="server" GroupName="Rad" CssClass="field-label" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rad2" runat="server" GroupName="Rad" CssClass="field-label" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td width="0">
                                        <asp:RadioButton ID="rad3" runat="server" GroupName="Rad" CssClass="field-label" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td width="0">
                                        <asp:RadioButtonList ID="rblFilter" runat="server" CssClass="field-label" AutoPostBack="True"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="NA">UnApproved</asp:ListItem>
                                            <asp:ListItem Value="AP">Approved</asp:ListItem>
                                            <asp:ListItem Value="CP">All</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table width="100%">
                    <tr  valign="top">
                        <th align="right" valign="middle" width="70%"></th>
                        <th align="right" valign="middle" style="text-align: right" width="30%">
                            <asp:LinkButton ID="btnPrint" runat="server" Visible="False">Print</asp:LinkButton>

                            <asp:HyperLink ID="btnExport" runat="server">Export</asp:HyperLink>
                        </th>
                    </tr>
                    <tr>
                        <td align="center" valign="top" width="100%" colspan="2">
                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" Width="100%"
                                CssClass="table table-row table-bordered" AllowPaging="True" PageSize="20">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>
                                            <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkControl" runat="server" type="checkbox" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col1" SortExpression="Col1">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol1" runat="server" Text='<%# Bind("Col1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol1" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col1"></asp:Label><br />
                                            <asp:TextBox ID="txtCol1" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch1" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col2" SortExpression="Col2">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol2" runat="server" Text='<%# Bind("Col2") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol2" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col2"></asp:Label><br />
                                            <asp:TextBox ID="txtCol2" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col3" SortExpression="Col3">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol3" runat="server" Text='<%# Bind("Col3") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol3" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col3"></asp:Label><br />
                                            <asp:TextBox ID="txtCol3" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch3" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col4" SortExpression="Col4">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol4" runat="server" Text='<%# Bind("Col4") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol4" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col4"></asp:Label><br />
                                            <asp:TextBox ID="txtCol4" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch4" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col5" SortExpression="Col5">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol5" runat="server" Text='<%# Bind("Col5") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol5" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col5"></asp:Label><br />
                                            <asp:TextBox ID="txtCol5" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch5" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col6" SortExpression="Col6">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol6" runat="server" Text='<%# Bind("Col6") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol6" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col6"></asp:Label><br />
                                            <asp:TextBox ID="txtCol6" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch6" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col7" SortExpression="Col7">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol7" runat="server" Text='<%# Bind("Col7") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol7" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col7"></asp:Label><br />
                                            <asp:TextBox ID="txtCol7" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch7" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col8" SortExpression="Col8">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol8" runat="server" Text='<%# Bind("Col8") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol8" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col8"></asp:Label><br />
                                            <asp:TextBox ID="txtCol8" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch8" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col9" SortExpression="Col9">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol9" runat="server" Text='<%# Bind("Col9") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol9" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col9"></asp:Label><br />
                                            <asp:TextBox ID="txtCol9" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch9" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col10" SortExpression="Col10">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol10" runat="server" Text='<%# Bind("Col10") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol10" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col10"></asp:Label><br />
                                            <asp:TextBox ID="txtCol10" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch10" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <table style="width: 100%;">
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True"
                                            Text="Select All" Visible="False" />
                                    </td>
                                    <td align="center" width="60%">
                                        <asp:Button ID="ListButton1" runat="server" CssClass="button" CausesValidation="False"
                                            Width="142px" Visible="False"></asp:Button>
                                        <asp:Button ID="ListButton2" runat="server" CssClass="button" CausesValidation="False"
                                            Width="142px" OnClick="ListButton2_Click" Visible="False"></asp:Button>
                                    </td>
                                    <td width="20%">
                                        <asp:HiddenField ID="h_print" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                </table>



                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_7" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_8" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_9" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_10" runat="server" type="hidden" value="=" />
                <input id="h_SelectedId" runat="server" type="hidden" value="0" />
            </div>
        </div>

        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
