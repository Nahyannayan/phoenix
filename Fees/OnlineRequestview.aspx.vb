Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Partial Class Fees_OnlineRequestview
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddHandler UsrTopFilter1.FilterChanged, AddressOf UsrTopFilter1_FilterChanged
        If Page.IsPostBack = False Then
            h_Grid.Value = "top"
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"



            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "F300177") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            gridbind()

            Dim url As String
            ViewState("datamode") = "add"
            Dim Queryusername As String = Session("sUsr_name")
            Dim dataModeAdd As String = Encr_decrData.Encrypt("add")
            url = "AccTreasuryTransfer.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & dataModeAdd

        End If
    End Sub

    Protected Sub UsrTopFilter1_FilterChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub


    Public Function returnpath(ByVal p_posted As Object) As String
        Try
            Dim b_posted As Boolean = Convert.ToBoolean(p_posted)
            If p_posted Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return "~/Images/cross.gif"
        End Try

    End Function

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim hlview As New HyperLink
            e.Row.Cells(7).Style("display") = "none"
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlview.NavigateUrl = "OnlineUserCreation.aspx?viewid=" & Encr_decrData.Encrypt(e.Row.Cells(7).Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")

            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = pField & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = pField & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = pField & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = pField & " NOT LIKE '%" & pVal & "'"
        End If
        lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function


    Private Sub gridbind(Optional ByVal p_selected_id As Integer = -1)
        Try
            Dim SqlQuery As String = String.Empty
            Dim str_Sql As String = String.Empty

            Dim ChkSchool As String = String.Empty
            Dim ChkStudentId As String = String.Empty
            Dim ChkStuName As String = String.Empty

            Dim lstrOpr As String = String.Empty

            Dim FillSchool As String = String.Empty
            Dim FillStudentId As String = String.Empty
            Dim FillStuName As String = String.Empty


            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox


            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 3333   refno
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtSchool")
                ChkSchool = Trim(txtSearch.Text)
                If (ChkSchool <> "") Then FillSchool = SetCondn(lstrOpr, "BSU_NAME", ChkSchool)

                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStudentId")
                ChkStudentId = Trim(txtSearch.Text)
                If (ChkStudentId <> "") Then FillStudentId = SetCondn(lstrOpr, "OUR_STU_NO", ChkStudentId)

                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuName")
                ChkStuName = Trim(txtSearch.Text)
                If (ChkStuName <> "") Then FillStuName = SetCondn(lstrOpr, "OUR_STU_NAME", ChkStuName)




            End If
            Dim str_Filter As String = " WHERE ISNULL(OUR_OLU_ID,0) = 0 "
            Dim str_Topfilter As String = ""
            If UsrTopFilter1.FilterCondition <> "All" Then
                str_Topfilter = " top " & UsrTopFilter1.FilterCondition
            End If

            str_Sql = " SELECT  " & str_Topfilter & "   OUR_ID,replace(convert(char,OUR_DT,106),' ','/')OUR_DT,VW_BUSINESSUNIT_M.BSU_NAME, " _
                    & " ONLINE.ONLINE_USERREQUEST.OUR_STU_NAME, ONLINE.ONLINE_USERREQUEST.OUR_STU_NO,  " _
                    & " ONLINE.ONLINE_USERREQUEST.OUR_REQUESTEDBY, ONLINE.ONLINE_USERREQUEST.OUR_EMAIL,  " _
                    & " ONLINE.ONLINE_USERREQUEST.OUR_PHONE, ONLINE.ONLINE_USERREQUEST.OUR_Remarks, ONLINE.ONLINE_USERREQUEST.OUR_USER " _
                    & " FROM         ONLINE.ONLINE_USERREQUEST INNER JOIN " _
                    & " VW_BUSINESSUNIT_M ON ONLINE.ONLINE_USERREQUEST.OUR_BSU_ID = VW_BUSINESSUNIT_M.BSU_ID  " _
            & str_Filter & FillSchool & FillStudentId & FillStuName _
            & " ORDER BY OUR_ID DESC  "

            Dim ds As New DataTable
            Dim MainObj As Mainclass = New Mainclass()

            ds = MainObj.ListRecords(str_Sql, "OASIS_TRANSPORTConnectionString")
            gvJournal.DataSource = ds
            If ds.Rows.Count = 0 Then
                ds.Rows.Add()
                'ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If

            txtSearch = gvJournal.HeaderRow.FindControl("txtSchool")
            txtSearch.Text = ChkSchool


            txtSearch = gvJournal.HeaderRow.FindControl("txtStudentId")
            txtSearch.Text = ChkStudentId

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuName")
            txtSearch.Text = ChkStuName
            gvJournal.SelectedIndex = p_selected_id
        Catch ex As Exception
            ' lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub gvJournal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)



    End Sub
End Class

