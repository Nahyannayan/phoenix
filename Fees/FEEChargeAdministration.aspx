<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FEEChargeAdministration.aspx.vb" Inherits="Fees_FEEChargeAdministration"
    Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ Register Src="~/UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc2" %>
<%--<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc2" %>--%>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>--%>
<%@ Register Src="~/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script src="../Scripts/jquery.mtz.monthpicker.js" type="text/javascript"></script>
    <style>
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(function () {
            var $ajaxImage = $("#ajaxImage");
            var $progressbar = $("#progressbar");
            var $fsProgress = $("#fsProgress");
            // Hide the AJAX spinner image and the progress bar, by default.
            $ajaxImage.hide();
            $progressbar.hide();
            $fsProgress.hide();
            var intervalHandle = setInterval(function () {

                var $statusDiv = $("#statusDiv");
                var $startProcessButton = $("#<%= btnCommitImprt.ClientID%>");
                var $PostbackButton = $("#<%= btnPostback.ClientID%>");
                var $lblError = $("#<%= lblError.ClientID%>");
                var $h_status = $("#<%= h_STATUS.ClientID%>");
                var $h_errormessage = $("#<%= h_errormessage.ClientID%>");
                //// Go hit the web service to get the latest status
                if ($("#<%=h_Processeing.ClientID %>").val() == '1') {
                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: "../UpdateProgressService.asmx/GetStatus",
                        contentType: "application/json; charset=utf-8",
                        success: function (response, status, xhr) {
                            if (response.d == null) {
                                //alert('1_null');
                                $ajaxImage.hide();
                                $progressbar.hide();
                                $fsProgress.hide();
                                // Stop this setInterval loop that we're in.
                                clearInterval(intervalHandle);
                            }
                            else {
                                // Show the AJAX spinner image and progress bar
                                //alert('not null');
                                $fsProgress.show(500);
                                $ajaxImage.show(500);
                                $progressbar.show(500);
                                $startProcessButton.attr("disabled", "disabled");
                                // Set the status text and the progress back value
                                $statusDiv.html(response.d.PercentComplete + "% " + response.d.WorkDescription);
                                //alert(response.d.PercentComplete + "% " + response.d.WorkDescription);
                                $progressbar.progressbar({
                                    value: response.d.PercentComplete
                                });
                                if (response.d.PercentComplete >= 100)
                                    $lblError.html(response.d.WorkDescription);
                                // Process is complete, start to reset the UI (the response.d == null above, will do the rest).
                                if (response.d.IsComplete) {
                                    $startProcessButton.removeAttr("disabled");
                                    $ajaxImage.hide(500);
                                    $progressbar.hide(500);
                                    $fsProgress.hide(500);
                                    $lblError.html(response.d.Message);
                                    alert('Message:' + response.d.Message);
                                    $h_errormessage.val(response.d.WorkDescription);
                                    if (response.d.IsSuccess) {
                                        //alert('IsSuccess: ' + response.d.IsSuccess);
                                        $h_status.val("0")
                                        $("#<%=h_Processeing.ClientID %>").val('');
                                        $PostbackButton.click();
                                        $PostbackButton.unbind('click');
                                        $h_status.val("")
                                    }
                                    else
                                        $h_status.val("1");
                                }
                            }
                        }
                    });
                }
            }, 1000);
        });

        <%--  function GetUserIds() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            type = 'USERS';
            result = window.showModalDialog("../Common/PopupForm.aspx?multiSelect=false&ID=" + type, "", sFeatures)
            if (result != "" && result != "undefined") {
                NameandCode = result.split('___');
                document.getElementById('<%=txtUserID.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=hdnUserID.ClientID %>').value = NameandCode[1];
            }
            return false;
        }--%>





    </script>

    <script>
        function get_Bank() {
            var oWnd = radopen("..\/accounts\/PopUp.aspx?ShowType=RRY&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value, "pop_get_Bank");

        }
        function OnClientClose3(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=hfACT_CODE.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
            }
        }

        function get_JV() {

            var oWnd = radopen("..\/Common\/PopUpselect.aspx?id=REGENERATEREVENUE&BSUID=" + $('#<%= DDL_RR_BSU.ClientID%>').val() + "&codeorname=" + document.getElementById('<%=txtJVNO.ClientID%>').value + "&date=" + $("#<%=monthpicker.ClientID%>").val(), "pop_get_JV");

                    }

                    function OnClientClose2(oWnd, args) {

                        //get the transferred arguments
                        var arg = args.get_argument();
                        if (arg) {

                            NameandCode = arg.NameandCode.split('||');

                            document.getElementById('<%=txtJVNO.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=hfJVNO.ClientID%>').value = NameandCode[0];
            }
        }


        function GetUserIds() {
            var type;
            type = 'USERS';
            var oWnd = radopen("../Common/PopupForm.aspx?multiSelect=false&ID=" + type, "pop_GetUserIds");
        }

        function OnClientClose1(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                document.getElementById('<%=txtUserID.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=hdnUserID.ClientID %>').value = NameandCode[1];
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_GetUserIds" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_get_JV" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_get_Bank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
       
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Student Fee Administration
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="FEE_COUNTER" />
                <asp:HiddenField ID="hfPostback" runat="server" OnValueChanged="hfPostback_ValueChanged" />
                <table id="Table1" width="100%">
                    <tr>
                        <td>
                            <table align="left" width="100%">
                                <tr>
                                    <td align="left">
                                        <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="2" TabIndex="7">
                                            <asp:TabPanel ID="TabPanel1" HeaderText="Student Charges" runat="server">
                                                <HeaderTemplate>Student Charges</HeaderTemplate>
                                                <ContentTemplate>
                                                    <table width="100%">
                                                        <tr id="TRbsu" runat="server">
                                                            <td runat="server" width="20%"><span class="field-label">Business Unit</span> </td>
                                                            <td align="left" colspan="3" runat="server">
                                                                <asp:DropDownList ID="ddlBSU" DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"
                                                                    runat="server" AutoPostBack="True">
                                                                </asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%"><span class="field-label">Academic Year</span> </td>
                                                            <td align="left" width="30%">
                                                                <asp:DropDownList ID="ddlAcdY" runat="server" SkinID="DropDownListNormal" AutoPostBack="True"></asp:DropDownList></td>
                                                            <td align="left" width="20%"><span class="field-label">Fee Type</span> </td>
                                                            <td align="left" width="30%">
                                                                <asp:DropDownList ID="ddlFeeType" runat="server"></asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left"><span class="field-label">From Date</span> </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtFromDT" runat="server" AutoPostBack="True" TabIndex="2"></asp:TextBox><asp:ImageButton
                                                                    ID="imgFDT" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4"></asp:ImageButton><asp:CalendarExtender
                                                                        ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Enabled="True" Format="dd/MMM/yyyy"
                                                                        PopupButtonID="imgFDT" TargetControlID="txtFromDT">
                                                                    </asp:CalendarExtender>
                                                            </td>
                                                            <td align="left"><span class="field-label">To Date</span> </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtToDT" runat="server" AutoPostBack="True" TabIndex="2"></asp:TextBox><asp:ImageButton
                                                                    ID="imgTDT" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /><asp:CalendarExtender
                                                                        ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Enabled="True" Format="dd/MMM/yyyy"
                                                                        PopupButtonID="imgTDT" TargetControlID="txtToDT">
                                                                    </asp:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="4">
                                                                <asp:Button ID="btnLoadChgs" runat="server" CssClass="button" Text="Load Students"></asp:Button></td>
                                                        </tr>
                                                        <tr id="trgvStudentCharges" runat="server">
                                                            <td align="center" colspan="4"></td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:TabPanel>
                                            <asp:TabPanel ID="TabPanel2" HeaderText="Student Ledger" runat="server">
                                                <HeaderTemplate>Student Ledger</HeaderTemplate>
                                                <ContentTemplate>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" width="20%"><span class="field-label">Student Type</span> </td>
                                                            <td align="left" colspan="3">
                                                                <asp:RadioButton ID="rbLEnrollment" TabIndex="1" runat="server" Text="Enrollment#" CssClass="field-label"
                                                                    AutoPostBack="True" Checked="True" GroupName="Ledger"></asp:RadioButton>
                                                                <asp:RadioButton ID="rbLEnquiry" runat="server" Text="Enquiry#" CssClass="field-label" AutoPostBack="True" GroupName="Ledger"></asp:RadioButton></td>

                                                            <tr>
                                                                <td align="left"><span class="field-label">Student</span> </td>
                                                                <td align="left" colspan="3">
                                                                    <uc2:usrSelStudent ID="UsrSelStudent1" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">Academic Year</span> </td>
                                                                <td align="left" colspan="3">
                                                                    <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal"></asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">From Date</span> </td>
                                                                <td align="left" width="30%">
                                                                    <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True" TabIndex="2"></asp:TextBox><asp:ImageButton
                                                                        ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /><asp:CalendarExtender
                                                                            ID="calendarButtonExtender" runat="server" CssClass="MyCalendar" Enabled="True"
                                                                            Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                                                                        </asp:CalendarExtender>
                                                                </td>
                                                                <td align="left" width="20%"><span class="field-label">To Date</span> </td>
                                                                <td align="left" width="30%">
                                                                    <asp:TextBox ID="txtTo" runat="server" AutoPostBack="True" TabIndex="2"></asp:TextBox><asp:ImageButton
                                                                        ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /><asp:CalendarExtender
                                                                            ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Enabled="True" Format="dd/MMM/yyyy"
                                                                            PopupButtonID="imgTo" TargetControlID="txtTo">
                                                                        </asp:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left"><span class="field-label">Student</span> </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblName" runat="server" CssClass="field-value"></asp:Label></td>
                                                                <td align="left"><span class="field-label">DOJ</span> </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblDOJ" runat="server" CssClass="field-value"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left"><span class="field-label">Grade</span> </td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblGrade" runat="server" CssClass="field-value"></asp:Label></td>
                                                                <td colspan="2">
                                                                    <asp:Button ID="btnLedger" runat="server" CssClass="button" Text="Show Ledger" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" colspan="4">
                                                                    <asp:Panel ID="pHeader" runat="server" CssClass="panel-cover">
                                                                        <asp:Image ID="Image1" runat="server" /><asp:Label ID="lblText" runat="server" />
                                                                    </asp:Panel>
                                                                    <asp:Panel ID="pBody" runat="server">
                                                                        <table>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Button ID="btnFCharges" runat="server" Text="Delete future Charges" CssClass="button" />
                                                                                    <asp:Button ID="btnEFR" runat="server" CssClass="button" Text="Enable Fee Refund" />
                                                                                    <asp:Button ID="btnResettlement" runat="server" CssClass="button" Text="Re Settlement" />
                                                                                    <asp:Button ID="btnEnrollStudent" runat="server" CssClass="button" Text="Enroll Student" />
                                                                                    <asp:Button ID="btnResetEnrollment" runat="server" CssClass="button" Text="Reset Reenrollment" />
                                                                                    <asp:Button ID="btnNotReenrolling" runat="server" CssClass="button" Text="Not Reenrolling" /></td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:Panel>
                                                                    <asp:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="pBody"
                                                                        CollapseControlID="pHeader" ExpandControlID="pHeader" Collapsed="True" TextLabelID="lblText"
                                                                        CollapsedText="Other Options" ExpandedText="Hide Options" ImageControlID="Image1"
                                                                        ExpandedImage="~/Images/collapse.jpg" CollapsedImage="~/Images/expand.jpg" CollapsedSize="0"
                                                                        Enabled="True">
                                                                    </asp:CollapsiblePanelExtender>
                                                                </td>
                                                            </tr>
                                                            <tr id="trLedger" runat="server">
                                                                <td runat="server" align="center" colspan="4">
                                                                    <asp:GridView ID="gvStLedger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                        EmptyDataText="No Data" ShowFooter="True" CssClass="table table-row table-bordered" Width="100%" EnableModelValidation="True">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="DOCDATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Doc.Date">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Description">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Grade">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblGrade" runat="server" Text='<%#Bind("GRD_DISPLAY") %>'>&#39;&gt;</asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="RECNO" HeaderText="RefNo">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="COLLECTIONTYPE" HeaderText="Collection Type">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="DEBIT" DataFormatString="{0:n2}" HeaderText="Debit">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="CREDIT" DataFormatString="{0:n2}" HeaderText="Credit">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="Balance" DataFormatString="{0:n2}" HeaderText="Balance">
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                        <FooterStyle Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" colspan="4">
                                                                    <asp:Button ID="btnCharge" runat="server" CssClass="button" Text="Charge" Enabled="False" /><asp:Button
                                                                        ID="btnCommit" runat="server" CssClass="button" Text="Commit Charge" Visible="False"
                                                                        Enabled="False" /><asp:Button ID="btnRollback" runat="server" CssClass="button" Text="Rollback Charge"
                                                                            Visible="False" /></td>
                                                            </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:TabPanel>
                                            <asp:TabPanel ID="TabPanel3" HeaderText="Adjustment Import" runat="server">
                                                <ContentTemplate>
                                                    <table width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                                                                <td align="left" width="30%">
                                                                    <telerik:RadComboBox ID="ddlBUnit" RenderMode="Lightweight" runat="server" Filter="Contains" AutoPostBack="True" ToolTip="Type in unit name or short code" Width="100%" ZIndex="2000">
                                                                    </telerik:RadComboBox>
                                                                    <asp:HiddenField ID="h_STATUS" runat="server" />
                                                                    <asp:HiddenField ID="h_errormessage" runat="server" />
                                                                    <asp:HiddenField ID="h_Processeing" runat="server" />
                                                                    <asp:Button ID="btnPostback" runat="server" CssClass="button" Style="display: none;" CausesValidation="False" Height="1px" TabIndex="5000" Width="1px" OnClick="btnPostback_Click" />

                                                                </td>
                                                                <td width="20%"><span class="field-label">Date</span> </td>
                                                                <td width="30%">
                                                                    <asp:TextBox ID="txtAdjDT" TabIndex="2" runat="server" AutoPostBack="True"></asp:TextBox><asp:ImageButton
                                                                        ID="imgAdjDT" TabIndex="4" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:CalendarExtender
                                                                            ID="CalendarExtender4" runat="server" CssClass="MyCalendar" TargetControlID="txtAdjDT"
                                                                            Enabled="True" PopupButtonID="imgAdjDT" Format="dd/MMM/yyyy">
                                                                        </asp:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><span class="field-label">Adjustment For</span></td>
                                                                <td align="left">
                                                                    <asp:RadioButtonList ID="rblAdjFor" runat="server" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Selected="True" Value="F">&lt;span class=&quot;field-label&quot;&gt;Fee&lt;/span&gt;</asp:ListItem>
                                                                        <asp:ListItem Value="S">&lt;span class=&quot;field-label&quot;&gt;Service&lt;/span&gt;</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                                <td align="left"><span class="field-label">Adjustment Type</span> </td>
                                                                <td align="left">
                                                                    <asp:DropDownList ID="ddlAdjType" runat="server"></asp:DropDownList><br />
                                                                    <asp:CheckBox ID="chkDefaultTaxcode" runat="server" CssClass="field-label" Text="Set default TAX code for Fee Type(s)." />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left"><span class="field-label">Student Type</span> </td>
                                                                <td align="left">
                                                                    <asp:RadioButton ID="rbEnrollment" TabIndex="1" runat="server" Text="Enrollment#" CssClass="field-label"
                                                                        AutoPostBack="True" Checked="True" GroupName="mode"></asp:RadioButton><asp:RadioButton CssClass="field-label"
                                                                            ID="rbEnquiry" runat="server" Text="Enquiry#" AutoPostBack="True" GroupName="mode"></asp:RadioButton></td>

                                                                <td align="left"><span class="field-label">Select File</span> </td>
                                                                <td align="left">
                                                                    <asp:FileUpload ID="FileUpload1" runat="server" Width="255px"></asp:FileUpload><br />
                                                                    <asp:HyperLink
                                                                        ID="lnkXcelFormat" runat="server">Click here to get the formatted Excel file</asp:HyperLink>
                                                                    <pre id="preMessage" runat="server" class="alert alert-info">The Amount in Excel sheet should be including Tax</pre>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left"><span class="field-label">Narration</span> </td>
                                                                <td align="left" colspan="3">
                                                                    <asp:TextBox ID="txtNarration" runat="server" Style="width: 50%;" Height="100px" TextMode="MultiLine" SkinID="MultiText"
                                                                        MaxLength="300"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" align="center">
                                                                    <fieldset id="fsProgress" style="width: 50%;">
                                                                        <legend>Processing Status</legend>
                                                                        <div id="ajaxImage" style="display: inline;">
                                                                            <img alt="" src="../Images/Misc/AjaxLoading.gif" />
                                                                        </div>
                                                                        <div id="statusDiv"></div>
                                                                        <div id="progressbar" style="height: 20px; width: 80%;"></div>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <table border="0" style="width: 100%;">
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="btnAddAdj" runat="server" Text="Reset" CssClass="button" Visible="False"></asp:Button></td>
                                                                            <td align="center">
                                                                                <asp:Button ID="btnImport" runat="server" Text="Load" CssClass="button"></asp:Button><asp:Button
                                                                                    ID="btnProceedImpt" runat="server" Text="Proceed" CssClass="button" Visible="False"></asp:Button><asp:Button ID="btnCommitImprt" runat="server" Text="Commit" CssClass="button"
                                                                                        Visible="False"></asp:Button>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" colspan="4">
                                                                    <asp:Label ID="lblError2" runat="server" CssClass="error"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" colspan="4">
                                                                    <asp:Label ID="lblMessage" runat="server" CssClass="error"></asp:Label></td>
                                                            </tr>
                                                            <tr id="trGvImport" runat="server">
                                                                <td id="Td1" align="left" colspan="4" runat="server">
                                                                    <center>
                                                            <asp:GridView ID="gvExcelImport" runat="server"  OnPageIndexChanging="gvExcelImport_PageIndexChanging"
                                                                AllowPaging="True" EmptyDataText="No Data" Width="100%" AutoGenerateColumns="False" cssclass="table table-row table-bordered"
                                                                ShowFooter="True" DataKeyNames="bVALID">
                                                                <Columns>
                                                                    <asp:BoundField DataField="STU_NO" HeaderText="Student Id">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="STU_NAME" HeaderText="Name">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Description">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DRCR" HeaderText="DRCR">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="AMOUNT" HeaderText="Amount">
                                                                        <FooterStyle HorizontalAlign="Right"  BorderStyle="None" Height="25px"></FooterStyle>
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="TAX_CODE" HeaderText="Tax Code">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="NARRATION" HeaderText="Narration">
                                                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="ERROR_MSG" HeaderText="Message" HtmlEncode="False">
                                                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <FooterStyle BackColor="#99CCFF" Font-Size="Small"></FooterStyle>
                                                            </asp:GridView>
                                                        </center>
                                                                </td>
                                                            </tr>
                                                            <tr id="trgvImportSmry" runat="server">
                                                                <td id="Td2" colspan="4" runat="server">
                                                                    <center>
                                                            <asp:GridView ID="gvImportSmry" runat="server" cssclass="table table-row table-bordered" OnPageIndexChanging="gvExcelImport_PageIndexChanging"
                                                                AllowPaging="True" EmptyDataText="No Data" Width="100%" AutoGenerateColumns="False"
                                                                PageSize="20">
                                                                <Columns>
                                                                    <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Description">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DRAMOUNT" DataFormatString="{0:n2}" HeaderText="Debit">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="CRAMOUNT" DataFormatString="{0:n2}" HeaderText="Credit">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </center>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" align="center">
                                                                    <asp:Button ID="btnCancelAdj" runat="server" CssClass="button"
                                                                        Text="CANCEL" />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:TabPanel>
                                            <asp:TabPanel ID="TabPanel4" HeaderText="Transport Rate Import" runat="server" ActiveTabIndex="0">
                                                <ContentTemplate>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" width="20%"><span class="field-label">Business Unit</span> </td>
                                                            <td align="left" width="30%">
                                                                <asp:DropDownList ID="ddlTBSU" DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"
                                                                    runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTBSU_SelectedIndexChanged">
                                                                </asp:DropDownList></td>
                                                            <td align="left" width="20%"><span class="field-label">Academic Year</span> </td>

                                                            <td align="left" width="30%">
                                                                <asp:DropDownList ID="ddlACDYear" runat="server"></asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left"><span class="field-label">Select File</span> </td>

                                                            <td align="left" colspan="3">
                                                                <asp:FileUpload ID="FileUpload2" runat="server" Width="255px" /><asp:HyperLink
                                                                    ID="lnkTrRateFormat" runat="server">Click here to get the formatted Excel file</asp:HyperLink></td>
                                                        </tr>
                                                        <tr>

                                                            <td align="center" colspan="4">
                                                                <asp:Button ID="btnImpTrRate" runat="server" CssClass="button" OnClick="btnImpTrRate_Click"
                                                                    Text="Import" /><asp:Button ID="btntrProceed" runat="server" CssClass="button"
                                                                        OnClick="btntrProceed_Click" Text="Proceed" />
                                                                <asp:Button ID="btnTrCommit" runat="server" CssClass="button" OnClick="btnTrCommit_Click"
                                                                    Text="Commit" Visible="False" /><asp:Button ID="btnCancelTrImport" runat="server"
                                                                        CssClass="button" Text="Cancel" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" colspan="4">
                                                                <center>
                                                        <asp:Label ID="lblError3" runat="server" CssClass="error"></asp:Label></center>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="4">
                                                                <asp:GridView runat="server" AllowPaging="True" AutoGenerateColumns="False" EmptyDataText="No Data"
                                                                    CssClass="table table-row table-bordered" Width="100%" ID="gvTrRateImport" OnPageIndexChanging="gvTrRateImport_PageIndexChanging"
                                                                    ShowFooter="True" OnRowDataBound="gvTrRateImport_RowDataBound" EnableModelValidation="True">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="SlNo" HeaderText="SlNo">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Loc" HeaderText="Loc">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField HeaderText="Area" DataField="Area">
                                                                            <FooterStyle BackColor="#99CCFF" BorderStyle="None" Height="25px" HorizontalAlign="Right" />
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="1Normal" HeaderText="1Normal" DataFormatString="{0:n2}">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="1OneWay" HeaderText="1OneWay" DataFormatString="{0:n2}">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="2Normal" HeaderText="2Normal" DataFormatString="{0:n2}">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="2OneWay" HeaderText="2OneWay" DataFormatString="{0:n2}">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="3Normal" HeaderText="3Normal" DataFormatString="{0:n2}">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="3OneWay" HeaderText="3OneWay" DataFormatString="{0:n2}">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <FooterStyle />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="4">
                                                                <center>
                                                        <asp:Label ID="lblXLNormal" runat="server" CssClass="error"></asp:Label>
                                                        <asp:Label ID="lblXLOneway" runat="server" CssClass="error"></asp:Label></center>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="4">
                                                                <asp:GridView ID="gvTrRateImportSummary" runat="server" AutoGenerateColumns="False"
                                                                    EmptyDataText="No Data" ShowFooter="True" CssClass="table table-row table-bordered" Width="100%" EnableModelValidation="True">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Normal" HeaderText="Normal Amount"></asp:BoundField>
                                                                        <asp:BoundField DataField="OneWay" HeaderText="OneWay Amount"></asp:BoundField>
                                                                    </Columns>
                                                                    <FooterStyle />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:TabPanel>
                                            <asp:TabPanel ID="TabPanel5" HeaderText="Fee Dayend" runat="server">
                                                <ContentTemplate>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <center>
                                                        <div style="height: 250px; overflow: auto; width: 100%;">
                                                            <asp:GridView ID="gvFeeDE" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"   DataKeyNames="BSU_ID" EmptyDataText="No Pending Dayends">
                                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkAll" runat="server" onclick="SelectAll(this.id)" />
                                                                        </HeaderTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                        </EditItemTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center"  />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="BSU_NAME" HeaderText="Business Unit">
                                                                        <ItemStyle HorizontalAlign="Left"  />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="FPD_DATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="FPD Date">
                                                                        <ItemStyle HorizontalAlign="Center"  />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="FPD_LOGDATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Log Date">
                                                                        <ItemStyle HorizontalAlign="Center"  />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="FPD_USER" HeaderText="Log User">
                                                                        <ItemStyle HorizontalAlign="Left"  />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                                                <RowStyle CssClass="griditem" Height="25px" />
                                                            </asp:GridView>
                                                        </div>
                                                    </center>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Button ID="btnDayend" runat="server" Text="Do Day End" CssClass="button" /></td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:TabPanel>
                                            <asp:TabPanel ID="TabPanel6" HeaderText="Reports" runat="server">
                                                <HeaderTemplate>Reports</HeaderTemplate>
                                                <ContentTemplate>
                                                    <table width="100%">
                                                        <tr id="tr1" runat="server">
                                                            <td runat="server"><span class="field-label">Report Type</span> </td>
                                                            <td align="left" runat="server">
                                                                <telerik:RadComboBox ID="radReportType" runat="server" EmptyMessage="Type here to search..." RenderMode="Lightweight"
                                                                    EnableScreenBoundaryDetection="False" Width="100%" CssClass="cellMatters" AutoPostBack="True">
                                                                </telerik:RadComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr id="trBSUIDs" runat="server">
                                                            <td runat="server"><span class="field-label">Business Unit</span>  </td>
                                                            <td align="left" runat="server">
                                                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr id="trUserID" runat="server">
                                                            <td id="Td5" runat="server"><span class="field-label">User Name</span></td>
                                                            <td id="Td6" align="left" runat="server">
                                                                <asp:TextBox ID="txtUserID" runat="server" CssClass="inputbox"></asp:TextBox><asp:HiddenField
                                                                    ID="hdnUserID" runat="server" />
                                                                <asp:ImageButton ID="imgUserID" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                    OnClientClick="GetUserIds();return false"></asp:ImageButton></td>
                                                        </tr>
                                                        <tr id="trStudents" runat="server">
                                                            <td id="Td7" runat="server"><span class="field-label">Student Name</span></td>
                                                            <td id="Td8" align="left" runat="server"></td>
                                                        </tr>
                                                        <tr id="trDateRange" runat="server">
                                                            <td id="Td3" runat="server"><span class="field-label">Date Range</span> </td>
                                                            <td id="Td4" align="left" runat="server">
                                                                <asp:TextBox ID="txtFromDate" TabIndex="2" runat="server"></asp:TextBox><asp:ImageButton
                                                                    ID="imgFromDate" TabIndex="4" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:CalendarExtender
                                                                        ID="CalendarExtender6" runat="server" CssClass="MyCalendar" TargetControlID="txtFromDate"
                                                                        Enabled="True" PopupButtonID="imgFromDate" Format="dd/MMM/yyyy">
                                                                    </asp:CalendarExtender>
                                                                To
                                                    <asp:TextBox ID="txtToDate" TabIndex="2" runat="server"></asp:TextBox><asp:ImageButton
                                                        ID="imgToDate" TabIndex="4" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:CalendarExtender
                                                            ID="CalendarExtender7" runat="server" CssClass="MyCalendar" TargetControlID="txtToDate"
                                                            Enabled="True" PopupButtonID="imgToDate" Format="dd/MMM/yyyy">
                                                        </asp:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                        <tr id="trAsOnDate" runat="server">
                                                            <td runat="server"><span class="field-label">Date</span>  </td>
                                                            <td align="left" runat="server">
                                                                <asp:TextBox ID="txtDate" TabIndex="2" runat="server"></asp:TextBox><asp:ImageButton
                                                                    ID="imgDate" TabIndex="4" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:CalendarExtender
                                                                        ID="CalendarExtender5" runat="server" CssClass="MyCalendar" TargetControlID="txtDate"
                                                                        Enabled="True" PopupButtonID="imgDate" Format="dd/MMM/yyyy">
                                                                    </asp:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                        <tr id="trRESULTTYPE" runat="server">
                                                            <td style="width: 20%;" runat="server"></td>
                                                            <td align="left" runat="server">
                                                                <asp:RadioButtonList ID="rblFilter" RepeatDirection="Horizontal" runat="server">
                                                                    <asp:ListItem Value="0"><span class="field-label">With Difference</span></asp:ListItem>
                                                                    <asp:ListItem Value="1"><span class="field-label">Without Difference</span></asp:ListItem>
                                                                    <asp:ListItem Value="2"><span class="field-label">All</span></asp:ListItem>
                                                                </asp:RadioButtonList></td>
                                                        </tr>
                                                        <tr id="trSearchStr" runat="server">
                                                            <td id="Td9" style="width: 20%;" runat="server"><span class="field-label">Search String</span>  </td>
                                                            <td id="Td10" align="left" runat="server">
                                                                <asp:TextBox ID="txtSearchStr" runat="server"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <asp:Button ID="btnView" runat="server" Text="View" CssClass="button" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <asp:LinkButton ID="lnkExportToExcel" runat="server" CommandName="ExportToExcel">Export To Excel</asp:LinkButton>|
                                                    <asp:LinkButton ID="lnkExportToPDF" runat="server" CommandName="ExportToExcel">Export To PDF</asp:LinkButton></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <center>
                                                        <telerik:RadGrid ID="RadGridReport" runat="server" AllowSorting="True" ShowStatusBar="True" CssClass="table table-row table-bordered"
                                                            ShowGroupPanel="True" Width="100%" GridLines="None" AllowPaging="True" PageSize="100"
                                                            EnableTheming="False" CellSpacing="0">
                                                            <ClientSettings EnableRowHoverStyle="True" AllowDragToGroup="True"></ClientSettings>
                                                            <AlternatingItemStyle CssClass="RadGridAltRow" HorizontalAlign="Left" />
                                                            <MasterTableView GridLines="Both">
                                                                <EditFormSettings></EditFormSettings>
                                                                <ItemStyle CssClass="RadGridRow" HorizontalAlign="Left" />
                                                                <HeaderStyle   HorizontalAlign="Center" />
                                                            </MasterTableView>
                                                        </telerik:RadGrid></center>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:TabPanel>
                                            <asp:TabPanel ID="TabPanel7" HeaderText="Transport Fee Dayend" runat="server">
                                                <ContentTemplate>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <center>
                                                        <asp:Label ID="lblErrorTr" runat="server" CssClass="error"></asp:Label>
                                                                    <div style="height: 250px; overflow: auto; width: 100%;">
                                                            <asp:GridView ID="gvTrFeeDE" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                                                 DataKeyNames="BSU_ID,PRO_BSU_ID" EmptyDataText="No Pending Dayends">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkAll" runat="server" onclick="SelectAll(this.id)" />
                                                                        </HeaderTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                        </EditItemTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="BSU_NAME" HeaderText="Business Unit">
                                                                        <ItemStyle HorizontalAlign="Left" Width="300px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="FPD_DATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="FPD Date">
                                                                        <ItemStyle HorizontalAlign="Center"  />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="FPD_LOGDATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Log Date">
                                                                        <ItemStyle HorizontalAlign="Center"  />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="FPD_USER" HeaderText="Log User">
                                                                        <ItemStyle HorizontalAlign="Left"  />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                                                <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                                                <RowStyle CssClass="griditem" Height="25px" />
                                                            </asp:GridView>
                                                        </div>
                                                    </center>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Button ID="btnTptDayEnd" runat="server" Text="Do Day End" CssClass="button" /></td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:TabPanel>
                                            <asp:TabPanel ID="TabPanel8" HeaderText="Revenue Recognition" runat="server">
                                                <HeaderTemplate>Revenue Recognition</HeaderTemplate>
                                                <ContentTemplate>
                                                    <table width="100%">
                                                        <tr>
                                                            <td colspan="4">
                                                                <center>
                                                        <asp:Label ID="lblErrorRR" runat="server" SkinID="LabelError" EnableViewState="False" CssClass="error"></asp:Label></center>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%"><span class="field-label">Business Unit</span>  </td>
                                                            <td align="left" width="30%">
                                                                <asp:DropDownList ID="DDL_RR_BSU" runat="server" DataTextField="BSU_NAME" DataValueField="BSU_ID" AutoPostBack="True"></asp:DropDownList></td>

                                                            <td width="20%"><span class="field-label">Academic Year</span></td>
                                                            <td align="left" width="30%">
                                                                <asp:DropDownList ID="DDL_RR_ACD" runat="server"></asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="field-label">Month/Year</span></td>
                                                            <td align="left">
                                                                <input type="text" id="monthpicker" runat="server" class="monthpicker" style="width: 79px" /></td>
                                                            <td colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="field-label">Account Code</span> </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtBankCode" runat="server" AutoPostBack="True"  ></asp:TextBox>
                                                                <asp:ImageButton ID="imgBank" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_Bank();return false;" />
                                                                </td>
                                                            <td><span class="field-label">Account Name</span> </td>
                                                            <td>
                                                                <asp:TextBox ID="txtBankDescr" runat="server" ></asp:TextBox>
                                                                <asp:HiddenField ID="hfACT_CODE" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="field-label">JV</span></td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtJVNO" runat="server" AutoPostBack="True" Width="20%"></asp:TextBox>
                                                                <asp:ImageButton ID="imgJV" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="get_JV();return false;" />
                                                                <asp:HiddenField ID="hfJVNO" runat="server" /><br />
                                                                <asp:CheckBox ID="chkNewJV" runat="server" Text="New JV" CssClass="field-label" /></td>
                                                            <td colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td id="Td13"></td>
                                                            <td id="Td14" align="left" colspan-="3">
                                                                <telerik:RadGrid ID="RadGV" runat="server">
                                                                    <HeaderStyle Width="20px"></HeaderStyle>
                                                                    <HeaderStyle Width="20px"></HeaderStyle>
                                                                </telerik:RadGrid></td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td align="center">
                                                                <asp:Button ID="btnRRProceed" runat="server" CausesValidation="False" CssClass="button"
                                                                    Text="Proceed" /><asp:Button ID="btnRRCommit" runat="server" CausesValidation="False" CssClass="button" Visible="False"
                                                                        Text="Commit" /><asp:Button ID="btnRRCancel" runat="server" CausesValidation="False" CssClass="button"
                                                                            Text="Cancel" /></td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:TabPanel>

                                            <asp:TabPanel ID="TabPanel9" HeaderText="Fee Type Access" runat="server">
                                                <HeaderTemplate>Fee Type Access</HeaderTemplate>
                                                <ContentTemplate>
                                                    <table width="100%">
                                                        <tr id="TR2" runat="server">
                                                            <td align="left" runat="server" width="20%"><span class="field-label">Source Screen</span> </td>
                                                            <td align="left" runat="server" width="30%">
                                                                <asp:DropDownList ID="ddlSource_" DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"
                                                                    runat="server" AutoPostBack="True">
                                                                </asp:DropDownList></td>
                                                            <td align="left" width="20%"><span class="field-label">Fee Type</span> </td>
                                                            <td align="left" width="30%">
                                                                <asp:DropDownList ID="ddlFeeType_" runat="server" SkinID="DropDownListNormal" AutoPostBack="True"></asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top"><span class="field-label">Business Unit(s)</span> </td>
                                                            <td align="left" colspan="2">
                                                                <div class="checkbox-list-full-height">

                                                                    <uc1:usrBSUnits ID="tvBsu" runat="server" />
                                                                </div>
                                                            </td>
                                                            <td align="center">
                                                                <asp:Button ID="btnSaveAccessSettings" runat="server" CssClass="button" Text="Save Access Settings"></asp:Button></td>
                                                        </tr>
                                                        <tr id="tr3" runat="server">
                                                            <td align="center" colspan="2" runat="server"></td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:TabPanel>

                                        </asp:TabContainer>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:GridView ID="gvStudentCharges" runat="server" AutoGenerateColumns="False" Width="100%"
                                            EmptyDataText="No Data" AllowPaging="True" OnPageIndexChanging="gvStudentCharges_PageIndexChanging"
                                            CssClass="table table-row table-bordered" DataKeyNames="STU_ID">
                                            <Columns>
                                                <asp:BoundField DataField="STU_NO" HeaderText="Student.No" />
                                                <asp:BoundField DataField="STU_NAME" HeaderText="Name" />
                                                <asp:BoundField DataField="STU_GRD_ID" HeaderText="Grade">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkBLedger" runat="server" CommandName="LEDGER" OnClick="lnkBLedger_Click">View Ledger</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            OnClick="btnCancel_Click" Text="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <script type="text/javascript">
                    function pageLoad() {

                        $('.monthpicker').monthpicker();
                    };

                </script>
                <script type="text/javascript" language="javascript">
                    <%--function get_Bank() {
                        var sFeatures;
                        sFeatures = "dialogWidth: 509px; ";
                        sFeatures += "dialogHeight: 434px; ";
                        sFeatures += "help: no; ";
                        sFeatures += "resizable: no; ";
                        sFeatures += "scroll: yes; ";
                        sFeatures += "status: no; ";
                        sFeatures += "unadorned: no; ";
                        var NameandCode;
                        var result;

                        result = window.showModalDialog("..\/accounts\/PopUp.aspx?ShowType=RRY&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value, "", sFeatures);

                        if (result == '' || result == undefined) {
                            return false;
                        }
                        NameandCode = result.split('||');
                        document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%=hfACT_CODE.ClientID%>').value = NameandCode[0];
                        document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
                        return false;
                    }

                    function get_JV() {
                        var sFeatures;
                        sFeatures = "dialogWidth: 509px; ";
                        sFeatures += "dialogHeight: 434px; ";
                        sFeatures += "help: no; ";
                        sFeatures += "resizable: no; ";
                        sFeatures += "scroll: yes; ";
                        sFeatures += "status: no; ";
                        sFeatures += "unadorned: no; ";
                        var NameandCode;
                        var result;

                        result = window.showModalDialog("..\/Common\/PopUpselect.aspx?id=REGENERATEREVENUE&BSUID=" + $('#<%= DDL_RR_BSU.ClientID%>').val() + "&codeorname=" + document.getElementById('<%=txtJVNO.ClientID%>').value + "&date=" + $("#<%=monthpicker.ClientID%>").val(), "", sFeatures);

                        if (result == '' || result == undefined) {
                            return false;
                        }
                        NameandCode = result.split('___');
                        document.getElementById('<%=txtJVNO.ClientID%>').value = NameandCode[0];
            document.getElementById('<%=hfJVNO.ClientID%>').value = NameandCode[0];
                        return false;
                    }--%>
                    function DisableJV() {

                        if ($("#<%=chkNewJV.ClientID%>").prop('checked')) {

                            $("#<%=imgJV.ClientID%>").prop("disabled", true);
                            $("#<%=txtJVNO.ClientID%>").prop("disabled", true);
                            $("#<%=txtJVNO.ClientID%>").val("");
                            $("#<%=hfJVNO.ClientID%>").val("");
                        }
                        else {
                            $("#<%=imgJV.ClientID%>").prop("disabled", false);
                            $("#<%=txtJVNO.ClientID%>").prop("disabled", false);

                        }

                    }

                </script>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
