﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="popFeeReminder.aspx.vb" Inherits="Fees_popFeeReminder" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/cssfiles/sb-admin.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 card-header letter-space">
                        Fee Reminder
                    </div>
                </div>
                <div class="row title-bg-lite">
                    Email
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <span class="field-label">To: </span>
                    </div>
                    <div class="col-md-9">
                        <asp:Label runat="server" ID="lblToEmail"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs">
                        <asp:Literal runat="server" ID="ltrlHtml"></asp:Literal>
                    </div>
                </div>
                <div class="row title-bg-lite">
                    SMS
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <span class="field-label">To: </span>
                    </div>
                    <div class="col-md-9">
                        <asp:Label runat="server" ID="lblToMobile"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs" style="margin: 5pt;">
                        <asp:Literal runat="server" ID="ltrlMobile"></asp:Literal>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <asp:Button ID="btnPrevious" runat="server" CssClass="button" Style="width: 150px !important;" Text="Previous" />
                    </div>
                    <div class="col-md-4">
                        <asp:Label runat="server" ID="lblIndex"></asp:Label>
                    </div>
                    <div class="col-md-4" style="float: right;">
                        <asp:Button ID="btnNext" runat="server" CssClass="button" Style="width: 150px !important;" Text="Next" />
                    </div>
                </div>
            </div>
        </div>

    </form>
</body>
</html>
