Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeTranspotReminder
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEConcessionDet.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvFEEConcessionDet.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            gvFEEConcessionDet.Attributes.Add("bordercolor", "#1b80b6")
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_FEE_REMINDER And ViewState("MainMnu_code") <> "F300199") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                Dim urlName As String = "feeTranspotReminderAddDetail.aspx"
                If ViewState("MainMnu_code") = "F300199" Then
                    urlName = "feeTranspotReminderAddDetailNew.aspx"
                End If
                hlAddNew.NavigateUrl = urlName & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                ddlBSUnit.DataBind()
                GET_COOKIE()
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)

            End Try
        End If
    End Sub

    Sub GridBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim ds As New DataSet
            Dim str_Filter, lstrOpr, str_Sql, lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6 As String
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            str_Filter = ""
            If gvFEEConcessionDet.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtStuNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuNo")
                lstrCondn1 = Trim(txtSearch.Text.Trim)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ACY_DESCR", lstrCondn1)

                '   -- 1  txtStuname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuname")
                lstrCondn2 = txtSearch.Text.Trim
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FRH_ASONDATE", lstrCondn2)

                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtConcession")
                lstrCondn3 = txtSearch.Text.Trim
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FRH_Level", lstrCondn3)

                '   -- 3   txtTDate
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtFrom")
                lstrCondn4 = txtSearch.Text.Trim
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FRH_DT", lstrCondn4)

                '   -- 5  city
                'larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                'lstrOpr = larrSearchOpr(0)
                'txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtPeriod")
                'lstrCondn5 = txtSearch.Text.Trim
                'If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "PERIOD", lstrCondn5)

                '   -- 5  city
                'larrSearchOpr = h_Selected_menu_6.Value.Split("__")
                'lstrOpr = larrSearchOpr(0)
                'txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRemarks")
                'lstrCondn6 = txtSearch.Text.Trim
                'If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCH_REMARKS", lstrCondn6)
            End If
            ''''FCH_ID,FCH_STU_ID, FCH_DT,FCH_REMARKS,PERIOD,STU_NAME,STU_NO,FCM_DESCR, AMOUNT
            Dim str_cond As String = String.Empty
            str_Sql = "SELECT ACY_DESCR, FRH_DT, " & _
            " FRH_ASONDATE, FRH_ID, " & _
            " FRH_Level, FRH_REMARKS " & _
            " FROM [TRANSPORT].[FEE_REMINDER_LIST] " & _
            " WHERE FRH_BSU_ID = '" & Session("sBsuId") & "' AND FRH_STU_BSU_ID='" & ddlBSUnit.SelectedValue & "' " & str_Filter

            Dim str_orderby As String = " ORDER BY ACD_ACY_ID DESC, FRH_ID DESC "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_orderby)
            gvFEEConcessionDet.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEConcessionDet.DataBind()
                Dim columnCount As Integer = gvFEEConcessionDet.Rows(0).Cells.Count
                gvFEEConcessionDet.Rows(0).Cells.Clear()
                gvFEEConcessionDet.Rows(0).Cells.Add(New TableCell)
                gvFEEConcessionDet.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEConcessionDet.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEConcessionDet.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEConcessionDet.DataBind()
            End If

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn2

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtConcession")
            txtSearch.Text = lstrCondn3

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtFrom")
            txtSearch.Text = lstrCondn4

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtPeriod")
            txtSearch.Text = lstrCondn5

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn6
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SET_COOKIE()
        GridBind()
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEConcessionDet.PageIndexChanging
        gvFEEConcessionDet.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFEEConcessionDet.RowDataBound
        Try
            Dim lblFRH_ID As New Label
            lblFRH_ID = TryCast(e.Row.FindControl("lblFRH_ID"), Label)
            'Dim lblLevel As New Label
            'lblLevel = TryCast(e.Row.FindControl("lblLevel"), Label)
            'If lblLevel IsNot Nothing Then
            '    Select Case lblLevel.Text
            '        Case "1"
            '            lblLevel.Text = "First Reminder"
            '        Case "2"
            '            lblLevel.Text = "Second Reminder"
            '        Case "3"
            '            lblLevel.Text = "Third Reminder"
            '        Case Else
            '            lblLevel.Text = ""
            '    End Select
            'End If
            Dim lnkView As New HyperLink
            lnkView = TryCast(e.Row.FindControl("lnkView"), HyperLink)
            If lnkView IsNot Nothing And lblFRH_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                lnkView.NavigateUrl = "feeTranspotReminderAddDetailNew.aspx?FRH_ID=" & Encr_decrData.Encrypt(lblFRH_ID.Text) & _
                "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lnkPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblFRHID As New Label
        lblFRHID = TryCast(sender.parent.FindControl("lblFRH_ID"), Label)
        If Not lblFRHID Is Nothing Then
            Session("ReportSource") = FEEReminderTransport.PrintReminder(CInt(lblFRHID.Text), Session("sUsr_name"), Session("sBsuid"), "")
            h_print.Value = "print"
        End If
    End Sub
    Private Sub SET_COOKIE()
        Dim OasisSettingsCookie As HttpCookie
        If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
            OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
        Else
            OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
        End If
        OasisSettingsCookie.Values("TPTBSUID") = ddlBSUnit.SelectedValue
        OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
        Response.Cookies.Add(OasisSettingsCookie)
    End Sub
    Private Sub GET_COOKIE()
        If Not Request.Cookies("OASIS_SETTINGS") Is Nothing Then
            ddlBSUnit.SelectedValue = Request.Cookies("OASIS_SETTINGS")("TPTBSUID")
        End If
    End Sub

End Class
