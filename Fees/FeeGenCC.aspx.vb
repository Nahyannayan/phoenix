Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FeeGenCC
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim ldblPerc As Decimal
    Dim ldblActDepAmount As Decimal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            InitialiseCompnents()
            If Session("BSU_IsOnDAX") = 1 Then
                Response.Redirect("FeeGenCC_DAX.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add"))
            End If
            ViewState("datamode") = "add"
            Session("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Session("gdtSub") = CreateDataTable()
            Session("dtColln") = CreateCollnTable()
            ClearVal()
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            If USR_NAME = "" Or CurBsUnit = "" Or (Session("MainMnu_code") <> OASISConstants.MNU_FEE_CREDITCARD_DEPOSIT_TRANSPORT) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Session("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, Session("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), Session("datamode"))
            End If
            Generatenewid()
        End If
    End Sub

    Sub InitialiseCompnents()
        ddlBusinessunit.DataBind()
        gvDTL.Attributes.Add("bordercolor", "#1b80b6")
        txtdocNo.Attributes.Add("readonly", "readonly")
        txtdocDate.Attributes.Add("readonly", "readonly")
        txtBankCode.Attributes.Add("readonly", "readonly")
        txtBankDescr.Attributes.Add("readonly", "readonly")
        txtNetAmount.Attributes.Add("readonly", "readonly")
        txtCashFlow.Text = "364"
        txtCardType.Attributes.Add("readonly", "readonly")
        txtComsn.Attributes.Add("readonly", "readonly")
        txtCashFlow.Attributes.Add("readonly", "readonly")
        txtdepAmount.Attributes.Add("readonly", "readonly")
    End Sub

    Protected Sub FillValues()
        '   --- Initialize The Grid With The Data From The Detail Table
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds2 As New DataSet
        Dim lstrSQL2 As String = "SELECT *,Replace(Convert(VarChar,VHH_DocDt,106),' ','/') as DocDate  FROM vw_OSA_CCDEPOSIT " _
        & " WHERE VHH_BSU_Id='" & Session("sBSUID") & "' AND VHH_DOCDT<='" & txtdocDate.Text & "' " _
        & " AND isnull(VHH_OWNER_BSU_ID,'')= '" & ddlBusinessunit.SelectedItem.Value & "' ORDER BY VHH_DOCDT desc "
        ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)
        gvDTL.DataSource = ds2
        gvDTL.DataBind()
    End Sub


    Sub Generatenewid()
        Try
            txtdocNo.Text = AccountFunctions.GetNextDocId("BR", Session("sBsuid"), CType(txtdocDate.Text, Date).Month, CType(txtdocDate.Text, Date).Year)
            FillValues()
            If Session("sTranDt") = "" Then
                'lblError.Text = "Please Select The Card"
                usrMessageBar.ShowNotification("Please Select The Card", UserControls_usrMessageBar.WarningType.Danger)
                btnPost.Enabled = False
            ElseIf (DateDiff(DateInterval.Day, Convert.ToDateTime(txtdocDate.Text), Convert.ToDateTime(Session("sTranDt"))) >= 1) Then

                'lblError.Text = "Cannot post transaction for a previous date !!!"
                usrMessageBar.ShowNotification("Cannot post transaction for a previous date !!!", UserControls_usrMessageBar.WarningType.Danger)
                btnPost.Enabled = False
            End If
            If txtdocNo.Text = "" Then
                'lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
                usrMessageBar.ShowNotification("Voucher Series not set. Cannot Add Data!!!", UserControls_usrMessageBar.WarningType.Danger)
                btnPost.Enabled = False
            Else
                btnPost.Enabled = True
            End If

            Dim lstrConn As String = ConnectionManger.GetOASISFINConnectionString
            Dim strSQL As String = "SELECT ISNULL(BSU_CCARD_BANK_ACT_ID,'') ACT_ID," & _
            " ISNULL(ACT_NAME,'') ACT_NAME FROM OASIS_TRANSPORT..BUSINESSUNIT_M " & _
            " INNER JOIN ACCOUNTS_M ON BSU_CCARD_BANK_ACT_ID = ACT_ID  WHERE BSU_ID ='" & Session("sBSUID") & "'"
            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(lstrConn, CommandType.Text, strSQL)
            While (drReader.Read())
                txtBankCode.Text = drReader("ACT_ID").ToString
                txtBankDescr.Text = drReader("ACT_NAME").ToString
                Exit While
            End While

        Catch ex As Exception
            'lblError.Text = "Voucher Series not set. Cannot Add Data!!!"
            usrMessageBar.ShowNotification("Voucher Series not set. Cannot Add Data!!!", UserControls_usrMessageBar.WarningType.Danger)
            btnPost.Enabled = False
        End Try
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRowId As New Label
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim lstrSQL2 As String
        Dim ldblBankCharges As Decimal
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        ViewState("datamode") = "add"
        lblRowId = TryCast(sender.parent.FindControl("lblTranDate"), Label)
        Dim str_docno As String = sender.parent.parent.cells(1).text
        Dim h_CreditCardRate As HiddenField
        h_CreditCardRate = TryCast(sender.parent.FindControl("h_CreditCardRate"), HiddenField)
        Session("sTranDt") = Left(lblRowId.Text, 11)
        ViewState("sDocno") = str_docno
        lblRowId = TryCast(sender.parent.FindControl("lblCRRID"), Label)
        ViewState("sCardType") = lblRowId.Text
        Session("BankTran") = "BR"

        lstrSQL2 = "SELECT CRR_CRI_ID,CRR_RATE,CRR_ACT_ID FROM CREDITCARD_S A " _
                          & " WHERE CRR_ID='" & ViewState("sCardType") & "'  AND '" & Session("sTranDt") & "'   between CRR_DTFROM AND '" & Session("sTranDt") & "'"
        ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)
        txtCardType.Text = ds2.Tables(0).Rows(0)("CRR_CRI_ID")
        'txtComsn.Text = AccountFunctions.Round(ds2.Tables(0).Rows(0)("CRR_RATE"))
        txtComsn.Text = AccountFunctions.Round(h_CreditCardRate.Value)
        hCardAccount.Value = ds2.Tables(0).Rows(0)("CRR_ACT_ID")

        Session("cCardComsn") = h_CreditCardRate.Value 'ds2.Tables(0).Rows(0)("CRR_RATE")
        'txtDocDate.Text = String.Format("{0:dd/MMM/yyyy}", Session("EntryDate"))
        txtdocNo.Text = Master.GetNextDocNo("BR", Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
        lblRowId = TryCast(sender.parent.FindControl("lblAmount"), Label)
        txtdepAmount.Text = AccountFunctions.Round(lblRowId.Text)
        ldblBankCharges = Convert.ToDecimal(txtdepAmount.Text) * (Session("cCardComsn") / 100)
        txtBankCom.Text = AccountFunctions.Round(ldblBankCharges)

        txtNetAmount.Text = AccountFunctions.Round(Convert.ToDecimal(txtdepAmount.Text) - ldblBankCharges)
        Session("dtColln") = New DataTable
        Session("dtColln").rows.clear()

        Dim str_Sql As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        str_Sql = "SELECT Distinct A.VHH_DOCDT,A.VHH_DOCNO,SUM(B.VHD_AMOUNT) as VHH_AMOUNT, " _
        & " MAx(A.VHH_NARRATION) as Narration, '' as Status FROM [VOUCHER_H] A " _
        & " INNER JOIN VOUCHER_D B ON A.VHH_BSU_ID=B.VHD_BSU_ID AND A.VHH_DOCNO=B.VHD_DOCNo " _
        & " WHERE VHH_BSU_Id='" & Session("sBSUID") & "' AND VHH_DOCTYPE='CC' " _
        & " AND VHH_DOCDT='" & Session("sTranDt") & "' AND VHH_bPOsted=1 AND VHH_DOCNO='" & str_docno & "' " _
        & " AND VHH_CRR_ID='" & ViewState("sCardType") & "' AND isNull(VHD_bBankReceipt,0)=0 " _
        & " GROUP BY A.VHH_DOCDT,A.VHH_DOCNO"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Session("gdtSub") = ds.Tables(0)
    End Sub

    Private Function CreateDataTable() As DataTable
        Try
            Dim cDocNo As New DataColumn("VHH_DOCNO", System.Type.GetType("System.String"))
            Dim cDocDt As New DataColumn("VHH_DOCDT", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("VHH_AMOUNT", System.Type.GetType("System.String"))
            Dim cNarrn As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))

            Session("gdtSub").Columns.Add(cDocNo)
            Session("gdtSub").Columns.Add(cDocDt)
            Session("gdtSub").Columns.Add(cAmount)
            Session("gdtSub").Columns.Add(cNarrn)
            Session("gdtSub").Columns.Add(cStatus)
            Return Session("gdtSub")
        Catch ex As Exception
            Return Session("gdtSub")
        End Try
    End Function

    Private Function CreateCollnTable() As DataTable
        Try
            Dim cLineId As New DataColumn("LineId", System.Type.GetType("System.String"))
            Dim cCollnType As New DataColumn("CollnType", System.Type.GetType("System.String"))
            Dim cCollnDescr As New DataColumn("CollnDescr", System.Type.GetType("System.String"))
            Dim cCollnAmount As New DataColumn("CollnAmount", System.Type.GetType("System.Decimal"))

            Session("dtColln").Columns.Add(cLineId)
            Session("dtColln").Columns.Add(cCollnType)
            Session("dtColln").Columns.Add(cCollnDescr)
            Session("dtColln").Columns.Add(cCollnAmount)
            Return Session("dtColln")
        Catch ex As Exception
            Return Session("dtColln")
        End Try
    End Function

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        '   --- Save It ---
        If txtNarration.Text.Trim = "" Then
            'lblError.Text = "Please enter narration!!!"
            usrMessageBar.ShowNotification("Please enter narration!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim lstrErrMsg As String = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim ldblAmount As Decimal
        Dim lintRetVal As Integer
        Dim lstrNewDocNo As String
        Dim ds As New DataSet

        Dim lintIndex As Integer
        Dim lstrExclude As String = ""
        If Trim(txtdocNo.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocNo " & "<br>"
        End If

        If Trim(txtdocDate.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocDate " & "<br>"
        End If

        If Trim(txtBankCode.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid Bank " & "<br>"
        End If

        If Trim(txtCashFlow.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid Cash Flow " & "<br>"
        End If

        If (IsNumeric(txtBankCom.Text) = False) Then
            lstrErrMsg = lstrErrMsg & "Bank Charges Should Be A Numeric Value" & "<br>"
        End If

        If (txtBankCom.Text <> "") Then
            If (Convert.ToDecimal(txtBankCom.Text) > Convert.ToDecimal(txtdepAmount.Text)) Then
                lstrErrMsg = lstrErrMsg & "Bank Charges Should Be Less Than Actual Deposit Amount" & "<br>"
            End If
        End If

        If (Session("gdtSub").Rows.Count = 0) Then
            lstrErrMsg = lstrErrMsg & "No transaction exist" & "<br>"
        End If

        If (lstrErrMsg <> "") Then
            lstrErrMsg = "Please check the following errors" & "<br>" & lstrErrMsg
            'lblError.Text = lstrErrMsg
            usrMessageBar.ShowNotification(lstrErrMsg, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        If CDate(txtdocDate.Text) > Now.Date Then
            lstrErrMsg = lstrErrMsg & "Invalid DocDate(Future date) " & "<br>"
        End If
        Session("dtColln").Rows.Clear()
        Dim str_Sql As String
        str_Sql = "SELECT COL_ID as CollnType,COL_DESCR as CollnDescr,0 as CollnAmount FROM COLLECTION_M "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Session("dtColln") = ds.Tables(0)

        For lintIndex = 0 To Session("gdtSub").Rows.Count - 1
            If Session("gdtSub").Rows(lintIndex)("Status") & "" = "DELETED" Then
                lstrExclude = lstrExclude & ",'" & Session("gdtSub").Rows(lintIndex)("VHH_DOCNO") & "'"
            End If
        Next
        lstrExclude = Mid(lstrExclude, 2)
        If lstrExclude = "" Then
            lstrExclude = Nothing
        End If
        If Not lstrExclude Is Nothing Then
            str_Sql = "SELECT Id=count(*),VHD_COL_ID as CollnType, Max(COL_DESCR) as CollnDescr, " _
                       & " SUM(VHD_AMOUNT) as CollnAmount FROM vw_OSA_CCTOB  WHERE VHH_BSU_ID='" & Session("sBsuId") & "' AND " _
                       & " VHH_DOCNO NOT IN (" & lstrExclude & ") AND VHH_CRR_ID='" & ViewState("sCardType") & "' AND VHH_DOCDT='" & Session("sTranDt") & "' " _
                       & " AND VHH_DOCNO='" & ViewState("sDocno") & "' Group BY VHD_COL_ID"
        Else
            str_Sql = "SELECT Id=count(*),VHD_COL_ID as CollnType, Max(COL_DESCR) as CollnDescr, " _
                       & " SUM(VHD_AMOUNT) as CollnAmount FROM vw_OSA_CCTOB  WHERE VHH_BSU_ID='" & Session("sBsuId") & "' AND " _
                       & " VHH_CRR_ID='" & ViewState("sCardType") & "' AND VHH_DOCDT='" & Session("sTranDt") & "' " _
                       & " AND VHH_DOCNO='" & ViewState("sDocno") & "' Group BY VHD_COL_ID"
        End If

        Dim ds2 As New DataSet
        ds2 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        hColln.Value = ds2.Tables(0).Rows(0)("CollnType")

        Dim ds3 As New DataSet
        ds3 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Session("dtColln") = ds3.Tables(0)

        ldblAmount = Math.Round(Convert.ToDecimal(txtdepAmount.Text) - Convert.ToDecimal(txtBankCom.Text), 2)        '
        ldblActDepAmount = ldblAmount
        ldblPerc = ((Convert.ToDecimal(txtBankCom.Text) / Convert.ToDecimal(txtdepAmount.Text)) * 100)

        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim SqlCmd As New SqlCommand("SaveVOUCHER_H", objConn, stTrans)
            SqlCmd.CommandType = CommandType.StoredProcedure
            Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
            sqlpGUID.Value = System.DBNull.Value
            SqlCmd.Parameters.Add(sqlpGUID)
            SqlCmd.Parameters.AddWithValue("@VHH_SUB_ID", Session("SUB_ID"))
            SqlCmd.Parameters.AddWithValue("@VHH_BSU_ID", Session("sBsuid"))
            SqlCmd.Parameters.AddWithValue("@VHH_FYEAR", Session("F_YEAR"))
            SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", "BR")
            SqlCmd.Parameters.AddWithValue("@VHH_DOCNO", txtdocNo.Text)
            SqlCmd.Parameters.AddWithValue("@VHH_TYPE", "R")
            SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_DOCDT", Trim(txtdocDate.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_CHQDT", Trim(txtdocDate.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtBankCode.Text)
            SqlCmd.Parameters.AddWithValue("@VHH_NOOFINST", 1)
            SqlCmd.Parameters.AddWithValue("@VHH_MONTHINTERVEL", 1)
            SqlCmd.Parameters.AddWithValue("@VHH_PARTY_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_INSTAMT", 0)
            SqlCmd.Parameters.AddWithValue("@VHH_INTPERCT", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_bINTEREST", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bAuto", True)
            SqlCmd.Parameters.AddWithValue("@VHH_CALCTYP", System.DBNull.Value)

            SqlCmd.Parameters.AddWithValue("@VHH_INT_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_ACRU_INT_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_CHQ_pdc_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_PROV_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_COL_ACT_ID", System.DBNull.Value)

            SqlCmd.Parameters.AddWithValue("@VHH_CUR_ID", Session("BSU_CURRENCY"))
            SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE1", 1)
            SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE2", 1)
            SqlCmd.Parameters.AddWithValue("@VHH_NARRATION", txtNarration.Text)
            ' "CREDIT CARD COLLECTION DEPOSIT -" & txtCardType.Text & _
            '           " (FOR " & Session("sTranDt") & ")")
            SqlCmd.Parameters.AddWithValue("@VHH_COL_ID", hColln.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_AMOUNT", ldblAmount)

            SqlCmd.Parameters.AddWithValue("@VHH_BANKCHARGE", Convert.ToDecimal(txtBankCom.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_bDELETED", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bPOSTED", False)
            SqlCmd.Parameters.AddWithValue("@bGenerateNewNo", True)
            Dim sqlpJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
            sqlpJHD_TIMESTAMP.Value = System.DBNull.Value
            SqlCmd.Parameters.Add(sqlpJHD_TIMESTAMP)
            SqlCmd.Parameters.AddWithValue("@VHH_SESSIONID", Session.SessionID)
            SqlCmd.Parameters.AddWithValue("@VHH_LOCK", Session("sUsr_name"))
            SqlCmd.Parameters.Add("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
            SqlCmd.Parameters("@VHH_NEWDOCNO").Direction = ParameterDirection.Output
            SqlCmd.Parameters.AddWithValue("@bEdit", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bPDC", False)
            SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            SqlCmd.ExecuteNonQuery()
            lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
            If lintRetVal <> 0 Then
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(lintRetVal)
                usrMessageBar.ShowNotification(getErrorMessage(lintRetVal), UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            lstrNewDocNo = CStr(SqlCmd.Parameters("@VHH_NEWDOCNO").Value)
            SqlCmd.Parameters.Clear()

            If (lintRetVal = 0) Then
                lintRetVal = DoTransactions(objConn, stTrans, lstrNewDocNo)
                If lintRetVal = "0" Then
                    ' --- Proceed to POST THE TRANSACTION
                    Dim cmd As New SqlCommand("POSTVOUCHER", objConn, stTrans)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                    sqlpJHD_SUB_ID.Value = Session("SUB_ID")
                    cmd.Parameters.Add(sqlpJHD_SUB_ID)

                    Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                    sqlpsqlpJHD_BSU_ID.Value = Session("sBSUID")
                    cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = Session("F_YEAR")
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCTYPE.Value = "BR"
                    cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                    Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                    sqlpJHD_DOCNO.Value = lstrNewDocNo
                    cmd.Parameters.Add(sqlpJHD_DOCNO)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)
                    cmd.ExecuteNonQuery()
                    lintRetVal = retValParam.Value
                    cmd.Parameters.Clear()
                    If (lintRetVal = 0) Then
                        '   --- END OF POST
                        Dim iIndex As Integer
                        Dim str_refdocnos As String = ""
                        If (Session("gdtSub").Rows.Count > 0) Then
                            For iIndex = 0 To Session("gdtSub").Rows.Count - 1
                                If Session("gdtSub").Rows(iIndex)("Status") & "" <> "DELETED" Then
                                    Dim cmd2 As New SqlCommand("UPDATEVOUCHER", objConn, stTrans)
                                    cmd2.CommandType = CommandType.StoredProcedure
                                    cmd2.Parameters.AddWithValue("@DocType", "CC")
                                    cmd2.Parameters.AddWithValue("@DOC_ID", Session("gdtSub").Rows(iIndex)("VHH_DOCNO"))
                                    str_refdocnos = Session("gdtSub").Rows(iIndex)("VHH_DOCNO") & "," & str_refdocnos
                                    cmd2.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
                                    cmd2.Parameters.AddWithValue("@RefDocNo", lstrNewDocNo)
                                    cmd2.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                                    cmd2.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                                    cmd2.ExecuteNonQuery()
                                    lintRetVal = CInt(cmd2.Parameters("@ReturnValue").Value)
                                    If lintRetVal <> 0 Then
                                        'lblError.Text = getErrorMessage(lintRetVal)
                                        usrMessageBar.ShowNotification(getErrorMessage(lintRetVal), UserControls_usrMessageBar.WarningType.Danger)
                                        stTrans.Rollback()
                                        Exit Sub
                                    End If
                                    cmd2.Parameters.Clear()
                                End If
                            Next
                        Else
                        End If
                        ''''tally
                        lintRetVal = AccountFunctions.CheckBAnkClearanceData("CC", Session("sBSUID"), lstrNewDocNo, stTrans)
                        'If lintRetVal = 0 Then
                        '    lintRetVal = AccountFunctions.SaveDayendJournalReverse(objConn, stTrans, ViewState("sDocno"), "CC", _
                        '    Session("SUB_ID"), Session("sBsuid"), Session("F_YEAR"), txtdocDate.Text, ddlBusinessunit.SelectedItem.Value)
                        'End If

                        If lintRetVal <> 0 Then
                            'lblError.Text = getErrorMessage(lintRetVal)
                            usrMessageBar.ShowNotification(getErrorMessage(lintRetVal), UserControls_usrMessageBar.WarningType.Danger)
                            stTrans.Rollback()
                            Exit Sub
                        End If
                        ''''
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lstrNewDocNo, _
                        "Credit Card Deposit", Page.User.Identity.Name.ToString, Me.Page, str_refdocnos)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        stTrans.Commit()
                        gvDTL.EditIndex = -1
                        FillValues()
                        gvDTL.Columns(4).Visible = True
                        Session("gdtSub").rows.clear()
                        ClearVal()
                        'lblError.Text = "Data Successfully Posted..."
                        usrMessageBar.ShowNotification("Data Successfully Posted...", UserControls_usrMessageBar.WarningType.Success)
                    Else
                        stTrans.Rollback()
                        'lblError.Text = getErrorMessage(lintRetVal)
                        usrMessageBar.ShowNotification(getErrorMessage(lintRetVal), UserControls_usrMessageBar.WarningType.Danger)
                    End If
                Else
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(lintRetVal)
                    usrMessageBar.ShowNotification(getErrorMessage(lintRetVal), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(lintRetVal)
                usrMessageBar.ShowNotification(getErrorMessage(lintRetVal), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Function DoTransactions(ByVal objConn As SqlConnection, _
            ByVal stTrans As SqlTransaction, ByVal p_docno As String) As String
        Dim iReturnvalue As Integer
        Dim ldblAmount, ldblLineAmount, ldblTrackAmount As Decimal
        ldblAmount = Convert.ToDecimal(txtdepAmount.Text) - Convert.ToDecimal(txtBankCom.Text)
        'Adding transaction info
        Dim cmd As New SqlCommand
        Dim str_err As String = ""
        Dim dTotal As Double = 0
        Dim iIndex As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        ldblTrackAmount = 0
        For iIndex = 0 To Session("dtColln").Rows.Count - 1
            cmd.Dispose()
            ldblLineAmount = Math.Round(Session("dtColln").Rows(iIndex)("CollnAmount") - (Session("dtColln").Rows(iIndex)("CollnAmount") * (ldblPerc / 100)), 2)
            ldblTrackAmount = Math.Round(ldblTrackAmount + ldblLineAmount, 2)
            If iIndex = Session("dtColln").Rows.Count - 1 Then
                If ldblTrackAmount <> ldblActDepAmount Then
                    ldblLineAmount = Math.Round(ldblLineAmount + (ldblTrackAmount - ldblActDepAmount), 2)
                End If
            End If
            cmd = New SqlCommand("SaveVOUCHER_D", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@GUID", System.DBNull.Value)
            cmd.Parameters.AddWithValue("@VHD_SUB_ID", Session("SUB_ID"))
            cmd.Parameters.AddWithValue("@VHD_BSU_ID", Session("sBsuid"))
            cmd.Parameters.AddWithValue("@VHD_FYEAR", Session("F_YEAR"))
            cmd.Parameters.AddWithValue("@VHD_DOCTYPE", "BR")
            cmd.Parameters.AddWithValue("@VHD_DOCNO", Trim(p_docno))
            cmd.Parameters.AddWithValue("@VHD_LINEID", iIndex + 1)
            cmd.Parameters.AddWithValue("@VHD_ACT_ID", hCardAccount.Value)
            cmd.Parameters.AddWithValue("@VHD_AMOUNT", ldblLineAmount)
            cmd.Parameters.AddWithValue("@VHD_NARRATION", txtNarration.Text)
            '"CREDIT CARD COLLECTION DEPOSIT -" & Session("dtColln").Rows(iIndex)("CollnDescr") & _
            '" (FOR " & Session("sTranDt") & ")")

            cmd.Parameters.AddWithValue("@VHD_CHQID", System.DBNull.Value)
            cmd.Parameters.AddWithValue("@VHD_CHQNO", System.DBNull.Value)
            cmd.Parameters.AddWithValue("@VHD_CHQDT", Trim(txtdocDate.Text))
            cmd.Parameters.AddWithValue("@VHD_RSS_ID", txtCashFlow.Text)
            cmd.Parameters.AddWithValue("@VHD_OPBAL", 0)
            cmd.Parameters.AddWithValue("@VHD_INTEREST", 0)
            cmd.Parameters.AddWithValue("@VHD_bBOUNCED", False)
            cmd.Parameters.AddWithValue("@VHD_bCANCELLED", False)
            cmd.Parameters.AddWithValue("@VHD_bDISCONTED", False)
            cmd.Parameters.AddWithValue("@VHD_COL_ID", Session("dtColln").Rows(iIndex)("CollnType"))
            cmd.Parameters.AddWithValue("@bEdit", False)

            cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            cmd.ExecuteNonQuery()

            iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
            Dim success_msg As String = ""

            cmd.Parameters.Clear()
        Next
        Return iReturnvalue
    End Function

    Protected Sub ClearVal()
        txtdocNo.Text = ""
        txtdocDate.Text = GetDiplayDate()
        txtBankCode.Text = ""
        txtBankDescr.Text = ""
        hColln.Value = ""
        Session("sTranDt") = ""
        ViewState("sCardType") = ""
        ViewState("sDocno") = ""
        txtCashFlow.Text = "364"
        txtCardType.Text = ""
        txtBankCom.Text = ""
        txtdepAmount.Text = ""
        txtComsn.Text = ""
        txtNetAmount.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        gvDTL.EditIndex = -1
        FillValues()
        gvDTL.Columns(4).Visible = True
        If ViewState("datamode") = "add" Then
            ViewState("datamode") = "none"
            ClearVal()
            'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub gvDTL_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvDTL.RowCancelingEdit
        gvDTL.EditIndex = -1
        FillValues()
    End Sub

    Protected Sub gvDTL_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvDTL.RowEditing
        gvDTL.Columns(4).Visible = False
        gvDTL.EditIndex = e.NewEditIndex
        FillValues()
    End Sub

    Protected Sub txtdocDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdocDate.TextChanged
        Generatenewid()
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gvDTL.EditIndex = -1
        FillValues()
    End Sub
End Class
