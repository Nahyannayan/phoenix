﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Sql
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Web.Configuration

Partial Class Fees_Fee_Rec_Adj_IU
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public Property IsProviderInitiated() As Boolean
        Get
            Return ViewState("IsProviderInitiated")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsProviderInitiated") = value
        End Set
    End Property
    Public Property IsProviderReceiving() As Boolean
        Get
            Return ViewState("IsProviderReceiving")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsProviderReceiving") = value
        End Set
    End Property

#Region "PublicVariables"
    Public Property FAI_ID() As Int32
        Get
            Return ViewState("FAH_ID")
        End Get
        Set(ByVal value As Int32)
            ViewState("FAH_ID") = value
        End Set
    End Property

    Public Property FeeTotal() As Double
        Get
            Return ViewState("FeeTotal")
        End Get
        Set(ByVal value As Double)
            ViewState("FeeTotal") = value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnUpload)
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F300151" And ViewState("MainMnu_code") <> "F300153" And ViewState("MainMnu_code") <> "F300183" And ViewState("MainMnu_code") <> "F300181") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Select Case ViewState("MainMnu_code")
                    Case "F300151", "F300183"
                        Dim CurBsUnit As String = Session("sBsuid")
                        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                        If ViewState("datamode") = "view" Or ViewState("datamode") = "approve" Then
                            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        Else
                            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        End If
                    Case "F300153", "F300181"
                        Dim CurBsUnit As String = Session("sBsuid")
                        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                        'If ViewState("datamode") = "view" Or ViewState("datamode") = "approve" Then
                        '    'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), "0", "add")

                        'Else
                        '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        'End If
                End Select
            End If

            If ViewState("MainMnu_code") = "F300183" Or ViewState("MainMnu_code") = "F300181" Then
                IsProviderReceiving = True
                trStudentBSU.Style("display") = "contents"
            Else
                IsProviderReceiving = False
                trStudentBSU.Style("display") = "none"
            End If

            cleardetails()
            h_BSUID.Value = Session("sBsuid")
            FEE_ADJ_INTERUNIT.PopulateBSU(ddlStudentBSU, "", False)


            h_print.Value = ""
            If ViewState("datamode") = "view" Or ViewState("datamode") = "approve" Then
                Dim objFAI As New FEE_ADJ_INTERUNIT
                objFAI.FAI_ID = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                Me.FAI_ID = objFAI.FAI_ID
                objFAI.LoadSavedAdjustmentH()
                Me.lblAdjCreatedDate.Text = objFAI.FAI_DATE
                Me.lblFrmBSU.Text = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT BSU_NAME FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & objFAI.FAI_BSU_ID & "'")
                Me.lblRemarks.Text = objFAI.FAI_STU_DETAILS
                Me.txtHeaderRemarks.Text = objFAI.FAI_CR_REMARKS
                Me.trSuppDocs.Visible = True
                If objFAI.FAI_CR_STU_BSU_ID <> "" Then
                    ddlStudentBSU.SelectedValue = objFAI.FAI_CR_STU_BSU_ID
                Else
                    ddlStudentBSU.SelectedValue = objFAI.FAI_CR_BSU_ID
                End If
                h_STUD_ID.Value = objFAI.FAI_CR_STU_ID
                SetDRStudentDetails(objFAI.FAI_STU_ID)
                Me.lblDRAmount.Text = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT ISNULL(SUM(FAID_AMOUNT),0)DRAMOUNT FROM FEES.FEE_ADJ_INTERUNIT_D WHERE FAID_DRCR='DR' AND FAID_FAI_ID=" & objFAI.FAI_ID & "")
                BindToDoxGridDR()
                LoadDoxGridCR()
                If objFAI.FAI_CR_STU_ID <> 0 Then 'interunit adjustment is received/saved
                    SetStudentDetails(objFAI.FAI_BSU_ID, objFAI.FAI_STU_TYPE, objFAI.FAI_CR_STU_ID)
                    ViewState("gvFeeDetails") = objFAI.LoadSavedAdjustmentS("CR")
                    BindCRFeeDetails()
                    'Me.lblBalanceAmt.Text = "0.00"
                    PopulateFeeType()
                    Dissablecontrols(True)
                    ViewState("datamode") = "none"
                    If objFAI.FAI_CR_APPR_STATUS.ToString.Trim = "N" Then 'Inter unit adjustment is Aproved or Rejected
                        If ViewState("MainMnu_code") = "F300153" Or ViewState("MainMnu_code") = "F300181" Then
                            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), "2", ViewState("datamode"))
                            Me.btnApprove.Visible = True
                            Me.btnReject.Visible = True
                        End If
                    Else
                        Me.btnApprove.Visible = False
                        Me.btnReject.Visible = False
                        Me.btnUpload.Enabled = False
                        Me.UploadDocPhoto.Enabled = False
                        Me.gvSuppDocsCR.Columns(4).Visible = False
                        'Me.lblError.Text = "Adjustment has been " & IIf(objFAI.FAI_CR_APPR_STATUS.Trim = "A", "approved", IIf(objFAI.FAI_CR_APPR_STATUS.Trim = "R", "rejected", ""))
                        usrMessageBar2.ShowNotification("Adjustment has been " & IIf(objFAI.FAI_CR_APPR_STATUS.Trim = "A", "approved", IIf(objFAI.FAI_CR_APPR_STATUS.Trim = "R", "rejected", "")), UserControls_usrMessageBar.WarningType.Danger)
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), "2", ViewState("datamode"))
                    End If

                End If
                SetBalanceAmt()
                'ViewState("gvDRFeeDetails") = objFAI.LoadSavedAdjustmentS()
            End If
        End If
    End Sub
    Public Sub SetStudentDetails(ByVal ProviderBSUID As String, ByVal STU_TYPE As String, ByVal STU_ID As String)
        Dim str_sql As String = String.Empty
        Dim ds As New DataTable
        Dim sqlParam(2) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@ProviderBsu_id", ProviderBSUID, SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@STU_TYPE", STU_TYPE, SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@STU_ID", STU_ID, SqlDbType.VarChar)
        ds = Mainclass.getDataTable("GetAllStudentDetails", sqlParam, ConnectionManger.GetOASIS_FEESConnectionString)
        If ds.Rows.Count > 0 Then
            txtStdNo.Text = ds.Rows(0).Item("STU_NO")
            txtStudentname.Text = ds.Rows(0).Item("STU_NAME")
        End If
    End Sub
    Sub cleardetails(Optional ByVal OnlyReceivedPart As Boolean = False)
        If Not OnlyReceivedPart Then
            lblAdjCreatedDate.Text = ""
            Me.txtHeaderRemarks.Text = ""
            FAI_ID = 0
            FeeTotal = 0
            lblFrmBSU.Text = ""
            Me.lblDRAmount.Text = "0.00"
            Me.lblRemarks.Text = ""
            Me.txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
            SetDRStudentDetails(0)
        End If

        h_STUD_ID.Value = ""
        txtStdNo.Text = ""
        txtStudentname.Text = ""
        ViewState("gvFeeDetails") = Nothing
        BindCRFeeDetails()
        SetBalanceAmt()
        ClearSubDetails()
    End Sub
    Sub SetDRStudentDetails(ByVal STUID As Integer)
        Dim qry = "SELECT STU_NO,STU_NAME,GRD_DISPLAY,PARENT_NAME FROM  dbo.VW_OSO_STUDENT_M WHERE STU_ID=" & STUID & ""
        Dim ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Me.lblStuNo.Text = ds.Tables(0).Rows(0)("STU_NO").ToString
            Me.lblStuName.Text = ds.Tables(0).Rows(0)("STU_NAME").ToString
            Me.lblGrade.Text = ds.Tables(0).Rows(0)("GRD_DISPLAY").ToString
            Me.lblParent.Text = ds.Tables(0).Rows(0)("PARENT_NAME").ToString
        Else
            Me.lblStuNo.Text = ""
            Me.lblStuName.Text = ""
            Me.lblGrade.Text = ""
            Me.lblParent.Text = ""
        End If
    End Sub
    Private Sub BindToDoxGridDR()
        Try
            Dim qry As String = "SELECT FAS_ID,FAS_CONTENT_TYPE,FAS_DR_FAI_ID,FAS_ORG_FILENAME,FAS_LOG_DATE,FAS_USER,FAS_FILENAME,ISNULL(FAS_bShared,0)FAS_bShared FROM FEES.FEE_ADJ_INTERUNIT_S WHERE ISNULL(FAS_bShared,0)=1 AND ISNULL(FAS_CR_FAI_ID,0)=0 AND ISNULL(FAS_DR_FAI_ID,0)=" & Me.FAI_ID & ""
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry)
            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Me.gvSuppDocsDR.DataSource = ds
                Me.gvSuppDocsDR.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub gvSuppDocsDR_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSuppDocsDR.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hf_ContentType As HiddenField = DirectCast(e.Row.FindControl("hf_ContentType"), HiddenField)
            Dim hf_filename As HiddenField = DirectCast(e.Row.FindControl("hf_filename"), HiddenField)
            Dim lnkFileName As LinkButton = DirectCast(e.Row.FindControl("lnkFileName"), LinkButton)
            lnkFileName.Attributes.Add("onClick", "return showDocument('" & (hf_ContentType.Value) & "','" & (hf_filename.Value) & "');")
        End If
    End Sub
    Private Sub PopulateFeeType()
        Dim httabFPM_ID As New Hashtable
        If h_STUD_ID.Value.ToString <> "" Then
            ddlFeeType.DataSource = FEE_ADJ_INTERUNIT.PopulateFeeMaster(h_BSUID.Value, h_STUD_ID.Value, txtDate.Text, IIf(radStud.Checked, "S", "E"), 0, 1)
        End If
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()
    End Sub

    Protected Sub btnDetAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetAdd.Click
        lblErrorFooter.Text = ""
        If ValidateADD() Then
            If IsNumeric(txtAdjAmount.Text) AndAlso CDbl(txtAdjAmount.Text) > 0 Then

                If ViewState("gvFeeDetails") Is Nothing Then
                    ViewState("gvFeeDetails") = FEE_ADJ_INTERUNIT.CreateGvFeeDetailSchema
                End If
                Dim dr As DataRow = DirectCast(ViewState("gvFeeDetails"), DataTable).NewRow
                dr("FEE_ID") = ddlFeeType.SelectedValue
                dr("FEE_TYPE") = ddlFeeType.SelectedItem.Text
                dr("DURATION") = 0
                dr("FEE_AMOUNT") = CDbl(txtAdjAmount.Text)
                dr("FEE_REMARKS") = txtDetRemarks.Text.Trim
                DirectCast(ViewState("gvFeeDetails"), DataTable).Rows.Add(dr)
                DirectCast(ViewState("gvFeeDetails"), DataTable).AcceptChanges()
                BindCRFeeDetails()
                ClearSubDetails()
                SetBalanceAmt()
            Else
                lblErrorFooter.Text = "Invalid Amount!!!"
                Exit Sub
            End If
        End If
    End Sub

    Private Sub GridBindAdjustments()
        Dim vFEE_ADJ As FEEADJUSTMENTREQ
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
            ViewState("gvFeeDetails") = FEEADJUSTMENTREQ.GetSubDetailsAsDataTable(vFEE_ADJ)
        Else
            ViewState("gvFeeDetails") = Nothing
        End If
        BindCRFeeDetails()
    End Sub
    Sub SetBalanceAmt()
        lblBalanceAmt.Text = FormatNumber(Val(lblDRAmount.Text) - FeeTotal, 2, TriState.True)
    End Sub

    Private Function AddDetails(ByVal htFEE_DET As Hashtable) As Hashtable
        Dim vFEE_DET As New FEEADJUSTMENT_S
        Dim eId As Integer = -1
        If btnDetAdd.Text = "Save" Then
            vFEE_DET = htFEE_DET(ViewState("Eid"))
            htFEE_DET.Remove(ViewState("Eid"))
            eId = ViewState("Eid")
            'If ViewState("datamode") = "edit" Then
            '    vFEE_DET.bEdit = True
            'End If
        End If
        If ddlFeeType.SelectedValue = "" Then
            'lblError.Text = "Invalid Fee type..! "
            usrMessageBar2.ShowNotification("Invalid Fee type..! ", UserControls_usrMessageBar.WarningType.Danger)
            Return Nothing
        End If

        Dim selFeeTyp As Integer = ddlFeeType.SelectedValue
        If Not htFEE_DET(CInt(selFeeTyp)) Is Nothing Then
            'lblError.Text = "The Fee type is repeating..."
            usrMessageBar2.ShowNotification("The Fee type is repeating...", UserControls_usrMessageBar.WarningType.Danger)
            Return htFEE_DET
            If eId <> -1 Then htFEE_DET(eId) = vFEE_DET
            'ElseIf FEEADJUSTMENTREQ.IsRepeated(vFEE_DET.FAD_FAH_ID, h_STUD_ID.Value, _
            'Session("Current_ACD_ID"), usrSelStudent1.STUDENT_GRADE_ID, Session("sBsuid")) Then

            '    lblError.Text = "There is already approval pending for " & ddlFeeType.SelectedItem.Text
            '    Return htFEE_DET
            '    If eId <> -1 Then htFEE_DET(eId) = vFEE_DET
        Else
            vFEE_DET.FAD_AMOUNT = CDbl(txtAdjAmount.Text)
        End If
        If vFEE_DET.FAD_AMOUNT = 0 Then
            If eId <> -1 Then htFEE_DET(eId) = vFEE_DET
            Return htFEE_DET
        End If

        vFEE_DET.FEE_TYPE = ddlFeeType.SelectedItem.Text
        vFEE_DET.FAD_FEE_ID = selFeeTyp
        vFEE_DET.FRS_Description = "0"

        vFEE_DET.FAD_REMARKS = txtDetRemarks.Text
        htFEE_DET(vFEE_DET.FAD_FEE_ID) = vFEE_DET
        ClearSubDetails()
        'End If
        Return htFEE_DET
    End Function
    Private Function ValidateADD() As Boolean
        ValidateADD = True
        If CheckDuplicate(ddlFeeType.SelectedValue) And btnDetAdd.Text <> "Save" Then
            lblErrorFooter.Text = "Fee Type already exists!!!"
            ValidateADD = False
            Exit Function
        End If
        If Not IsNumeric(txtAdjAmount.Text) Then
            lblErrorFooter.Text = "Invalid Amount!!!"
            ValidateADD = False
            Exit Function
        ElseIf CDbl(txtAdjAmount.Text) = 0 Then
            lblErrorFooter.Text = "Invalid Amount!!!"
            ValidateADD = False
            Exit Function
        ElseIf Val(txtAdjAmount.Text) > Val(lblDRAmount.Text) Then
            lblErrorFooter.Text = "Invalid Amount!!!"
            ValidateADD = False
            Exit Function
        ElseIf CDbl(lblBalanceAmt.Text) <> 0 And Val(txtAdjAmount.Text) > CDbl(lblBalanceAmt.Text) Then
            lblErrorFooter.Text = "Invalid Amount!!!"
            ValidateADD = False
            Exit Function
        End If
        If CDbl(lblBalanceAmt.Text) <= 0 And gvFeeDetails.Rows.Count > 0 And btnDetAdd.Text <> "Save" Then
            lblErrorFooter.Text = "Invalid Amount!!!"
            ValidateADD = False
            Exit Function
        End If
    End Function
    Private Sub BindCRFeeDetails()
        FeeTotal = 0
        gvFeeDetails.DataSource = ViewState("gvFeeDetails")
        gvFeeDetails.DataBind()

    End Sub
    Private Function CheckDuplicate(ByVal FEEID As String) As Boolean
        CheckDuplicate = False
        If Not ViewState("gvFeeDetails") Is Nothing Then
            For Each dr As DataRow In DirectCast(ViewState("gvFeeDetails"), DataTable).Rows
                If dr("FEE_ID").ToString.Trim = FEEID Then
                    CheckDuplicate = True
                    Exit For
                End If
            Next
        End If
    End Function

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        Dim lblAmt As New Label
        lblAmt = TryCast(sender.parent.FindControl("lblAmount"), Label)
        Dim lblRemarks As New Label
        lblRemarks = TryCast(sender.parent.FindControl("lblRemarks"), Label)

        Dim gvr As GridViewRow = TryCast(lblFEE_ID.NamingContainer, GridViewRow)
        If Not lblFEE_ID Is Nothing AndAlso Not lblAmt Is Nothing AndAlso Not lblRemarks Is Nothing Then
            ddlFeeType.SelectedValue = lblFEE_ID.Text
            txtAdjAmount.Text = lblAmt.Text
            txtDetRemarks.Text = lblRemarks.Text
            DirectCast(ViewState("gvFeeDetails"), DataTable).Rows.RemoveAt(gvr.RowIndex)
            DirectCast(ViewState("gvFeeDetails"), DataTable).AcceptChanges()
            BindCRFeeDetails()
        End If
        SetBalanceAmt()
    End Sub
    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        Dim gvr As GridViewRow = TryCast(lblFEE_ID.NamingContainer, GridViewRow)
        DirectCast(ViewState("gvFeeDetails"), DataTable).Rows.RemoveAt(gvr.RowIndex)
        DirectCast(ViewState("gvFeeDetails"), DataTable).AcceptChanges()
        BindCRFeeDetails()
        SetBalanceAmt()
    End Sub

    Private Sub ClearSubDetails()
        txtAdjAmount.Text = "0.00"
        txtDetRemarks.Text = ""
        ddlFeeType.SelectedIndex = -1
        btnDetAdd.Text = "Add"
        'lblBalanceAmt.Text = ""
        lbDescription.Text = ""
        lblErrorFooter.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call cleardetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Protected Sub gvFeeDetails_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvFeeDetails.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            FeeTotal += Val(DirectCast(ViewState("gvFeeDetails"), DataTable).Rows(e.Row.RowIndex)("FEE_AMOUNT"))
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        'If Not Master.IsSessionMatchesForSave() Then
        '    lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
        '    Exit Sub
        'Else
        If Not ValidateSave() Then
            Exit Sub
        End If

        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim trans As SqlTransaction
        Dim retVal As Integer = 1000
        trans = conn.BeginTransaction("sFEE_ADJUSTMENT")

        Dim objFAI As New FEE_ADJ_INTERUNIT

        Try
            objFAI.FAI_ID = FAI_ID
            objFAI.FAI_DATE = IIf(IsDate(Me.txtDate.Text.Trim), Me.txtDate.Text.Trim, Format(DateTime.Now, OASISConstants.DataBaseDateFormat))
            objFAI.FAI_STU_TYPE = ""
            If IsProviderReceiving Then
                objFAI.FAI_CR_STU_ACD_ID = 0
            Else
                objFAI.FAI_CR_STU_ACD_ID = Session("Current_ACD_ID")
            End If

            objFAI.FAI_CR_STU_BSU_ID = ddlStudentBSU.SelectedValue
            objFAI.FAI_BSU_ID = ""
            objFAI.FAI_CR_STU_ID = h_STUD_ID.Value
            objFAI.FAI_CR_STU_TYPE = IIf(radStud.Checked, "S", "E")
            objFAI.FAI_CR_BSU_ID = Session("sBsuid")
            objFAI.FAI_STU_DETAILS = ""
            objFAI.FAI_CR_REMARKS = Me.txtHeaderRemarks.Text.Trim
            objFAI.User = Session("sUsr_name")
            objFAI.PROCESS = "RECEIVE"

            retVal = objFAI.SaveAdjustmentHeader(conn, trans)
            If retVal = 0 Then
                For Each dr As DataRow In DirectCast(ViewState("gvFeeDetails"), DataTable).Rows
                    If retVal = 0 Then
                        objFAI.FAID_FAI_ID = objFAI.FAI_ID
                        objFAI.FAID_FEE_ID = dr("FEE_ID").ToString
                        objFAI.FAID_DRCR = "CR"
                        objFAI.FAID_AMOUNT = IIf(IsNumeric(dr("FEE_AMOUNT")), Convert.ToDouble(dr("FEE_AMOUNT")), 0) 'Val(dr("FEE_AMOUNT").ToString)
                        objFAI.FAID_REMARKS = dr("FEE_REMARKS").ToString

                        retVal = objFAI.SaveAdjustment_Detail(conn, trans)
                    Else
                        'ValidateAndSave = retValD
                        Exit For
                    End If
                Next

            End If

        Catch ex As Exception
            trans.Rollback()
            'If lblError.Text = "" Then
            '    lblError.Text = UtilityObj.getErrorMessage(retVal)
            'End If
            usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End Try

        If retVal <> 0 Then
            trans.Rollback()
            'If lblError.Text = "" Then
            '    lblError.Text = UtilityObj.getErrorMessage(retVal)
            'End If
            usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
        Else
            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
            'If chkPrint.Checked Then
            '    PrintReceiptAdjustmentRequest(vNewFAR_ID)
            'End If
            trans.Commit()
            'lblError.Text = UtilityObj.getErrorMessage(retVal)
            usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Success)
            If chkPrint.Checked Then
                h_print.Value = "print"
                PrintReceiptAdjustmentRequest(FAI_ID)
            End If
            cleardetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
        End If
    End Sub

    Private Function ValidateSave() As Boolean
        ValidateSave = True
        If h_STUD_ID.Value.ToString = "" Or h_STUD_ID.Value.ToString = "0" Then
            'Me.lblError.Text = "select a student"
            usrMessageBar2.ShowNotification("select a student", UserControls_usrMessageBar.WarningType.Danger)
            ValidateSave = False
            Exit Function
        ElseIf Me.txtHeaderRemarks.Text = "" Then
            'Me.lblError.Text = "Remarks should not be empty"
            usrMessageBar2.ShowNotification("Remarks should not be empty", UserControls_usrMessageBar.WarningType.Danger)
            Me.txtHeaderRemarks.Focus()
            ValidateSave = False
            Exit Function
        ElseIf gvFeeDetails.Rows.Count <= 0 Then
            'Me.lblError.Text = "Add the adjustment details"
            usrMessageBar2.ShowNotification("Add the adjustment details", UserControls_usrMessageBar.WarningType.Danger)
            ValidateSave = False
            Exit Function
        End If
        Dim GridTotal As Double = 0
        For Each dr As DataRow In DirectCast(ViewState("gvFeeDetails"), DataTable).Rows
            GridTotal += Val(dr("FEE_AMOUNT").ToString)
        Next
        If GridTotal <> Val(lblDRAmount.Text) Then
            'Me.lblError.Text = "Amount not tallying"
            usrMessageBar2.ShowNotification("Amount not tallying", UserControls_usrMessageBar.WarningType.Danger)
            ValidateSave = False
            Exit Function
        End If

    End Function
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"

        Dissablecontrols(False)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Private Sub Dissablecontrols(ByVal dissble As Boolean)
        radStud.Enabled = Not dissble
        radEnq.Enabled = Not dissble
        txtDate.ReadOnly = dissble
        imgDate.Enabled = Not dissble
        CalendarExtender1.Enabled = Not dissble
        calendarButtonExtender.Enabled = Not dissble
        txtStdNo.ReadOnly = dissble
        txtStudentname.ReadOnly = dissble
        imgStudent.Enabled = Not dissble
        ddlFeeType.Enabled = Not dissble
        Me.txtHeaderRemarks.Enabled = Not dissble
        btnDetAdd.Enabled = Not dissble
        gvFeeDetails.Enabled = Not dissble
        ddlStudentBSU.Enabled = Not dissble
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnApprove.Click
        'If Not Master.IsSessionMatchesForSave() Then
        '    lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
        '    Exit Sub
        'Else
        If Not ValidateSave() Then
            Exit Sub
        End If

        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim trans As SqlTransaction
        Dim retVal As Integer = 1000
        trans = conn.BeginTransaction("FeeAdjIU")

        Dim objFAI As New FEE_ADJ_INTERUNIT

        Try
            objFAI.FAI_ID = FAI_ID
            objFAI.FAI_DATE = txtDate.Text
            objFAI.FAI_STU_TYPE = ""
            objFAI.FAI_BSU_ID = ""
            objFAI.FAI_STU_DETAILS = ""
            objFAI.FAI_REMARKS = Me.lblRemarks.Text
            objFAI.FAI_CR_REMARKS = Me.txtHeaderRemarks.Text.Trim
            objFAI.FAI_CR_STU_ACD_ID = Session("Current_ACD_ID")
            objFAI.FAI_CR_STU_ID = h_STUD_ID.Value
            objFAI.FAI_CR_STU_TYPE = IIf(radStud.Checked, "S", "E")
            objFAI.FAI_CR_BSU_ID = Session("sBsuid")

            objFAI.User = Session("sUsr_name")
            objFAI.PROCESS = "APPROVE_CR"

            retVal = objFAI.SaveAdjustmentHeader(conn, trans)
            If retVal = "0" And objFAI.PROCESS = "APPROVE_CR" Then
                Dim lblFEE_ID As Label, lblAmount As Label, lblRemarks As Label
                For Each gvr As GridViewRow In gvFeeDetails.Rows 'Saving Adjustment request detail
                    If retVal = 0 Then
                        lblFEE_ID = DirectCast(gvr.FindControl("lblFEE_ID"), Label)
                        lblAmount = DirectCast(gvr.FindControl("lblAmount"), Label)
                        lblRemarks = DirectCast(gvr.FindControl("lblRemarks"), Label)
                        objFAI.FAID_FAI_ID = objFAI.FAI_ID
                        objFAI.FAID_AMOUNT = IIf(IsNumeric(lblAmount.Text.Trim), Convert.ToDouble(lblAmount.Text.Trim), 0)
                        objFAI.FAID_FEE_ID = lblFEE_ID.Text
                        objFAI.FAID_DRCR = "CR"
                        objFAI.FAID_REMARKS = lblRemarks.Text

                        retVal = objFAI.SaveAdjustment_Detail(conn, trans)
                    Else
                        'ValidateAndSa = retVal
                        Exit For
                    End If
                Next
                If retVal = "0" And objFAI.PROCESS = "APPROVE_CR" Then 'Saving adjustment request approve
                    objFAI.APPROVE_STAGE = "APPROVE_RECEIVE"
                    retVal = objFAI.ApproveAdjustment_IU(conn, trans)
                End If
                If retVal <> 0 Then
                    trans.Rollback()
                    'If lblError.Text = "" Then
                    '    lblError.Text = UtilityObj.getErrorMessage(retVal)
                    'End If
                    usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
                Else
                    trans.Commit()
                    'lblError.Text = "Adjustment receipt has been Approved"
                    usrMessageBar2.ShowNotification("Adjustment receipt has been Approved", UserControls_usrMessageBar.WarningType.Success)
                    cleardetails()
                    btnApprove.Visible = False
                    btnReject.Visible = False
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), "2", ViewState("datamode"))
                End If

            End If
        Catch ex As Exception
            trans.Rollback()
            'If lblError.Text = "" Then
            '    lblError.Text = ex.Message
            'End If
            usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReject.Click
        'If Not Master.IsSessionMatchesForSave() Then
        '    lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
        '    Exit Sub
        'End If

        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim trans As SqlTransaction
        Dim retVal As Integer = 1000
        trans = conn.BeginTransaction("FeeAdjIU")

        Dim objFAI As New FEE_ADJ_INTERUNIT

        Try
            objFAI.FAI_ID = FAI_ID
            objFAI.FAI_DATE = txtDate.Text
            objFAI.FAI_STU_TYPE = ""
            objFAI.FAI_BSU_ID = ""
            objFAI.FAI_STU_DETAILS = ""
            objFAI.FAI_REMARKS = Me.lblRemarks.Text
            objFAI.FAI_CR_STU_ACD_ID = Session("Current_ACD_ID")
            objFAI.FAI_CR_STU_ID = h_STUD_ID.Value
            objFAI.FAI_CR_STU_TYPE = IIf(radStud.Checked, "S", "E")
            objFAI.FAI_CR_BSU_ID = Session("sBsuid")

            objFAI.User = Session("sUsr_name")
            objFAI.PROCESS = "REJECT_CR"

            retVal = objFAI.SaveAdjustmentHeader(conn, trans)

            If retVal <> 0 Then
                trans.Rollback()
                'If lblError.Text = "" Then
                '    lblError.Text = UtilityObj.getErrorMessage(retVal)
                'End If
                usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                trans.Commit()
                'lblError.Text = "Adjustment receipt has been Rejected"
                usrMessageBar2.ShowNotification("Adjustment receipt has been Rejected", UserControls_usrMessageBar.WarningType.Success)
                cleardetails()
                btnApprove.Visible = False
                btnReject.Visible = False
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), "2", ViewState("datamode"))
            End If
        Catch ex As Exception
            trans.Rollback()
            'If lblError.Text = "" Then
            '    lblError.Text = ex.Message
            'End If
            usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrint.Click
        h_print.Value = "print"
        PrintReceiptAdjustmentRequest(FAI_ID)
    End Sub

    Protected Sub PrintReceiptAdjustmentRequest(ByVal vFAI_ID As Integer)
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("[FEES].[GET_FEE_ADJUSTMENTREQUEST_IU]")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpFAI_ID As New SqlParameter("@FAI_ID", SqlDbType.Int)
        sqlpFAI_ID.Value = vFAI_ID
        cmd.Parameters.Add(sqlpFAI_ID)

        Dim sqlpFAID_DRCR As New SqlParameter("@FAID_DRCR", SqlDbType.VarChar)
        sqlpFAID_DRCR.Value = "CR"
        cmd.Parameters.Add(sqlpFAID_DRCR)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("RPT_CAPTION") = "FEE ADJUSTMENT RECEIVED (IU)"
        params("UserName") = Session("sUsr_name")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../fees/Reports/RPT/rptFEEAdjustmentReceipt_IU.rpt"

        Session("ReportSource") = repSource
        'h_print.Value = "print"
    End Sub
    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        SetStudentDetails(ddlStudentBSU.SelectedValue, IIf(radEnq.Checked, "E", "S"), h_STUD_ID.Value)
        PopulateFeeType()
    End Sub

    Protected Sub ddlStudentBSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStudentBSU.SelectedIndexChanged
        cleardetails(True)
    End Sub

    Protected Sub txtStdNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStdNo.TextChanged
        Try
            Dim str_data, str_sql As String
            txtStdNo.Text = txtStdNo.Text.Trim
            Dim iStdnolength As Integer = txtStdNo.Text.Length
            If radStud.Checked Then
                If iStdnolength < 9 Then
                    If txtStdNo.Text.Trim.Length < 8 Then
                        For i As Integer = iStdnolength + 1 To 8
                            txtStdNo.Text = "0" & txtStdNo.Text
                        Next
                    End If
                    txtStdNo.Text = ddlStudentBSU.SelectedValue & txtStdNo.Text
                End If
                str_sql = "SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM VW_OSO_STUDENT_M" _
                 & " WHERE     (STU_bActive = 1) AND (STU_BSU_ID = '" & ddlStudentBSU.SelectedValue & "') AND  STU_NO='" & txtStdNo.Text & "'"
                str_data = Mainclass.getDataValue(str_sql, "OASIS_FEESConnectionString")
            Else
                str_sql = "SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM FEES.vw_OSO_ENQUIRY_COMP" _
                & " WHERE     (STU_BSU_ID = '" & ddlStudentBSU.SelectedValue & "') AND  STU_NO='" & txtStdNo.Text & "'"

                str_data = Mainclass.getDataValue(str_sql, "OASIS_FEESConnectionString")
            End If
            If str_data <> "--" Then
                h_STUD_ID.Value = str_data.Split("|")(0)
                SetStudentDetails(ddlStudentBSU.SelectedValue, IIf(radEnq.Checked, "E", "S"), h_STUD_ID.Value)
                PopulateFeeType()
            Else
                h_STUD_ID.Value = "0"
                txtStudentname.Text = ""
                'lblError.Text = "Student # Entered is not valid  !!!"
                usrMessageBar2.ShowNotification("Student # Entered is not valid  !!!", UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If UploadDocPhoto.FileName <> "" Then
            Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Fees\"
            Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName
            Dim strFilepath As String = str_img & str_tempfilename

            Try
                UploadDocPhoto.PostedFile.SaveAs(strFilepath)
            Catch ex As Exception
                Exit Sub
            End Try
            'If hdnAssetImagePath.Value <> "" Then
            If Not IO.File.Exists(strFilepath) Then
                'lblError.Text = "Invalid FilePath!!!!"
                usrMessageBar2.ShowNotification("Invalid FilePath!!!!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            Else
                Dim ContentType As String = getContentType(strFilepath)
                Dim FileExt As String = str_tempfilename.Substring(str_tempfilename.Length - 4)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 5)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 6)
                Try
                    Dim RandVal As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CAST(RAND() * 1000000 AS INT) AS RANDVAL")
                    Dim NewFileName As String = RandVal & "_" & Session("sBsuid") & "_CR_" & Me.FAI_ID & FileExt
                    IO.File.Move(strFilepath, str_img & NewFileName)

                    Dim retVal As Integer = 1000
                    Dim conn = ConnectionManger.GetOASIS_FEESConnection
                    Dim trans As SqlTransaction = conn.BeginTransaction()
                    Dim objFAI As New FEE_ADJ_INTERUNIT
                    objFAI.FAS_ID = 0
                    objFAI.FAS_CONTENT_TYPE = ContentType
                    objFAI.FAS_CR_FAI_ID = Me.FAI_ID
                    objFAI.FAS_ORG_FILENAME = UploadDocPhoto.FileName
                    objFAI.FAS_USER = Session("sUsr_name")
                    objFAI.FAS_FILENAME = NewFileName
                    objFAI.FAS_bShared = False
                    retVal = objFAI.SAVE_ADJ_INTERUNIT_S(conn, trans)
                    If retVal = "0" Then
                        trans.Commit()
                        If Not ViewState("gvSuppDocsCR") Is Nothing And Me.FAI_ID = 0 Then
                            Dim dr As DataRow = TryCast(ViewState("gvSuppDocsCR"), DataTable).NewRow
                            dr("FAS_ID") = objFAI.FAS_ID
                            dr("FAS_CONTENT_TYPE") = objFAI.FAS_CONTENT_TYPE
                            dr("FAS_ORG_FILENAME") = objFAI.FAS_ORG_FILENAME
                            dr("FAS_FILENAME") = objFAI.FAS_FILENAME
                            dr("FAS_bShared") = objFAI.FAS_bShared
                            dr("FAS_USER") = objFAI.FAS_USER
                            TryCast(ViewState("gvSuppDocsCR"), DataTable).Rows.Add(dr)
                            TryCast(ViewState("gvSuppDocsCR"), DataTable).AcceptChanges()
                            BindDoxGvCR()
                        ElseIf Me.FAI_ID <> 0 Then
                            LoadDoxGridCR()
                        End If

                    Else
                        trans.Rollback()
                        IO.File.Delete(str_img & NewFileName)
                    End If
                    'Dim QryInsert As String = "INSERT INTO FEES.FEE_ADJ_INTERUNIT_S(FAS_CONTENT_TYPE,FAS_CR_FAI_ID,FAS_ORG_FILENAME,FAS_LOG_DATE,FAS_USER,FAS_FILENAME,FAS_bShared)" & _
                    '"VALUES('" & ContentType & "'," & Me.FAI_ID & ",'" & UploadDocPhoto.FileName & "',GETDATE(),'" & Session("sUsr_name") & "','" & NewFileName & "',0)"
                    'SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, QryInsert)
                    'LoadDoxGridCR()
                Catch ex As Exception

                End Try
            End If
        ElseIf Not ViewState("imgAsset") Is Nothing Then

        Else
            ViewState("imgAsset") = Nothing
        End If
    End Sub
    Private Function getContentType(ByVal FilePath As String) As String
        Dim filename As String = Path.GetFileName(FilePath)
        Dim ext As String = Path.GetExtension(filename)
        Select Case ext
            Case ".doc"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".xlsx"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                getContentType = "image/jpg"
                Exit Select
            Case ".png"
                getContentType = "image/png"
                Exit Select
            Case ".gif"
                getContentType = "image/gif"
                Exit Select
            Case ".pdf"
                getContentType = "application/pdf"
                Exit Select
            Case Else
                getContentType = "text/html"
        End Select
    End Function
    Private Sub LoadDoxGridCR()
        Try
            Dim qry As String = "SELECT FAS_ID,FAS_CONTENT_TYPE,FAS_CR_FAI_ID,FAS_ORG_FILENAME,FAS_LOG_DATE,FAS_USER,FAS_FILENAME,ISNULL(FAS_bShared,0)FAS_bShared FROM FEES.FEE_ADJ_INTERUNIT_S WHERE ISNULL(FAS_DR_FAI_ID,0)=0 AND ISNULL(FAS_CR_FAI_ID,0)=" & Me.FAI_ID & ""
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry)
            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                ViewState("gvSuppDocsCR") = ds.Tables(0)
            Else
                ViewState("gvSuppDocsCR") = Nothing
            End If
            BindDoxGvCR()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindDoxGvCR()
        Me.gvSuppDocsCR.DataSource = ViewState("gvSuppDocsCR")
        Me.gvSuppDocsCR.DataBind()
    End Sub
    Protected Sub gvSuppDocsCR_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSuppDocsCR.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hf_ContentType As HiddenField = DirectCast(e.Row.FindControl("hf_ContentTypeCR"), HiddenField)
            Dim hf_filename As HiddenField = DirectCast(e.Row.FindControl("hf_filenameCR"), HiddenField)
            Dim lnkFileName As LinkButton = DirectCast(e.Row.FindControl("lnkFileNameCR"), LinkButton)
            lnkFileName.Attributes.Add("onClick", "return showDocument('" & (hf_ContentType.Value) & "','" & (hf_filename.Value) & "');")
        End If
    End Sub
    Protected Sub lnkDeleteCR_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnkDelete As New LinkButton
        lnkDelete = TryCast(sender.parent.FindControl("lnkDeleteCR"), LinkButton)
        Dim hf_FASID As HiddenField = DirectCast(sender.parent.FindControl("hf_FASIDCR"), HiddenField)
        Dim hf_filename As HiddenField = DirectCast(sender.parent.FindControl("hf_filenameCR"), HiddenField)

        Dim gvr As GridViewRow = TryCast(hf_FASID.NamingContainer, GridViewRow)
        If Not hf_FASID Is Nothing Then
            Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Fees\"
            Dim strFilepath As String = str_img & hf_filename.Value
            If IO.File.Exists(strFilepath) Then
                Try
                    IO.File.Delete(strFilepath)
                Catch ex As Exception

                End Try
            End If
            Dim QryInsert As String = "DELETE FEES.FEE_ADJ_INTERUNIT_S WHERE FAS_ID=" & hf_FASID.Value & ""
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, QryInsert)
            TryCast(ViewState("gvSuppDocsCR"), DataTable).Rows(gvr.RowIndex).Delete()
            TryCast(ViewState("gvSuppDocsCR"), DataTable).AcceptChanges()
            BindDoxGvCR()
        End If
    End Sub

    Protected Sub chkSharedOBCR_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSharedOBCR As CheckBox = DirectCast(sender.parent.Findcontrol("chkSharedOBCR"), CheckBox)
        Dim hf_FASIDCR As HiddenField = DirectCast(sender.parent.FindControl("hf_FASIDCR"), HiddenField)
        Dim gvr As GridViewRow = TryCast(hf_FASIDCR.NamingContainer, GridViewRow)
        If Not chkSharedOBCR Is Nothing AndAlso Not hf_FASIDCR Is Nothing Then
            Dim bShared As Int16 = IIf(chkSharedOBCR.Checked, 1, 0)
            Dim QryInsert As String = "UPDATE FEES.FEE_ADJ_INTERUNIT_S SET FAS_bShared=" & bShared & " WHERE FAS_ID=" & hf_FASIDCR.Value & ""
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, QryInsert)
            TryCast(ViewState("gvSuppDocsCR"), DataTable).Rows(gvr.RowIndex)("FAS_bShared") = bShared
            TryCast(ViewState("gvSuppDocsCR"), DataTable).AcceptChanges()
            BindDoxGvCR()
        End If
    End Sub
End Class
