﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Tax_Code_Configuration_Add_Edit.aspx.vb" Inherits="Accounts_Tax_Code_Configuration_Add_Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Tax Code Configuration Add
        </div>

        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <span style="float: left; display: block;">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" Font-Size="10px" />
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                        ValidationGroup="AttGroup" DisplayMode="List" Font-Size="10px" ForeColor="" />
                                </div>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="matters" valign="top">
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Country <font color="red">*</font></span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry"
                                            Display="Dynamic" ErrorMessage="Country required" InitialValue="0" ValidationGroup="AttGroup" ForeColor="">*</asp:RequiredFieldValidator>

                                    </td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Fee Type <font color="red">*</font></span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="0">-SELECT-</asp:ListItem>
                                            <asp:ListItem Value="FEE">FEE</asp:ListItem>
                                            <asp:ListItem Value="ACCOUNT">ACCOUNT</asp:ListItem>
                                            <asp:ListItem Value="EVENT">EVENT</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Ref. Type <font color="red">*</font></span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlRefType" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvRefType" runat="server" ControlToValidate="ddlRefType"
                                            Display="Dynamic" ErrorMessage="Ref. type required" InitialValue="0" ValidationGroup="AttGroup" ForeColor="">*</asp:RequiredFieldValidator>

                                    </td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Tax Code <font color="red">*</font></span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlTaxCode" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvTaxCode" runat="server" ControlToValidate="ddlTaxCode"
                                            Display="Dynamic" ErrorMessage="Tax code required" InitialValue="0" ValidationGroup="AttGroup" ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">From Date <font color="red">*</font></span></td>
                                    <td align="left" class="matters" width="20%">
                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgCal1" runat="server"
                                            ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFromDate"
                                            Display="Dynamic" ErrorMessage="From Date required" ValidationGroup="AttGroup" ForeColor="">*</asp:RequiredFieldValidator></td>
                                    <td align="left" class="matters" width="20%"></td>
                                    <td align="left" class="matters" width="30%"></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" OnClick="btnAdd_Click" ValidationGroup="AttGroup" />                                       
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button"
                                            Text="Back" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="matters" valign="bottom">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">
                            <div>
                                <asp:Label ID="lblError_grid" runat="server"
                                    EnableViewState="False"></asp:Label>
                                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                    DataKeyNames="TXM_ID"
                                    EmptyDataText="No Record(s) Found."
                                    Width="100%">
                                    <RowStyle CssClass="griditem" Height="23px" />
                                    <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                    <Columns>
                               
                                        <asp:TemplateField HeaderText="Country" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCTY_DESCR" runat="server" Text='<%#Bind("CTY_DESCR") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fee Type" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTXM_REF_TYPE" runat="server" Text='<%#Bind("TXM_REF_TYPE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ref. Type" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTXM_DESCR" runat="server" Text='<%#Bind("TXM_DESCR") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="From Date" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTXM_FROMDT" runat="server" Text='<%# Bind("TXM_FROMDT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Date" ItemStyle-Width="20%">
                                            <ItemTemplate>                                           
                                                <asp:Label ID="lblTXM_TODT" runat="server" Text='<%#Bind("TXM_TODT") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle BackColor="Khaki" />
                                    <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="cal1" runat="server" TargetControlID="txtFromDate" Format="dd/MMM/yyyy" PopupButtonID="imgcal1">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

