﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FEEFGBOnlineCollection.aspx.vb" Inherits="Fees_FEEFGBOnlineCollection" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
        function CheckForPrint() {
            if (document.getElementById('<%= h_print.ClientID %>').value != '') {
            document.getElementById('<%= h_print.ClientID %>').value = '';
               // showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                Popup('../Reports/ASPX Report/RptViewerModal.aspx');
       }
    }
    );
   function getFile() {
       var filepath = document.getElementById('<%=uploadFile.ClientID %>').value;
        document.getElementById('<%=HidUpload.ClientID %>').value = filepath;
    }
        <%--  function getBank()
    { 
        var sFeatures;
        var sFeatures;
        sFeatures="dialogWidth: 460px; ";
        sFeatures+="dialogHeight: 475px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var NameandCode;
        var result;
        result = window.showModalDialog("../ACCOUNTS/PopUp.aspx?ShowType=BANK&codeorname="+document.getElementById('<%=txtBankCode.ClientID %>').value,"", sFeatures);
        if (result=='' || result==undefined)
        {    return false;      } 
         lstrVal=result.split('||');     
         document.getElementById('<%=txtBankCode.ClientID %>').value=lstrVal[0];
         document.getElementById('<%=txtBankDescr.ClientID %>').value=lstrVal[1];
    }--%>
           
    </script>
    <script>
        function getBank() {
            var oWnd = radopen("../ACCOUNTS/PopUp.aspx?ShowType=BANK&codeorname=" + document.getElementById('<%=txtBankCode.ClientID %>').value, "pop_getBank");

                }
                function OnClientClose1(oWnd, args) {


                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg) {

                        NameandCode = arg.NameandCode.split('||');

                        document.getElementById('<%=txtBankCode.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientID %>').value = NameandCode[1];
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>
    <telerik:radwindowmanager id="RadWindowManager1" showcontentduringload="false" visiblestatusbar="false"
        reloadonshow="true" runat="server" enableshadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getBank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Internet Fee Collection-FGB
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">File Name</span></td>
                                    <td   align="left" width="30%">
                                        <asp:FileUpload ID="uploadFile" runat="server" Width="80%" />
                                    </td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr class="matters">
                                    <td align="left"><span class="field-label">Bank Code</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtBankCode" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server"
                                            ImageUrl="~/Images/cal.gif" OnClientClick="getBank();return false" /></td>
                                    <td align="left"><span class="field-label">Bank Name</span></td>
                                    <td>
                                        <asp:TextBox ID="txtBankDescr" runat="server" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="matters">
                                    <td align="left"  ><span class="field-label">Date</span></td>
                                    <td align="left"  >
                                        <asp:TextBox ID="txtHDocdate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle" /><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtHDocdate"
                                            ErrorMessage="Invalid Date" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                                    <td  >
                                        <asp:RadioButton ID="RdExcel" runat="server" Text="Excel" Checked="True" CssClass="field-label" GroupName="excel" />
                                        <asp:RadioButton ID="RdDbf" runat="server" Text="DBF" GroupName="excel" CssClass="field-label" />
                                    </td>
                                    <td   align="left">
                                        <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Upload File" CausesValidation="False"
                                            TabIndex="30" OnClick="btnFind_Click"   OnClientClick="getFile()" />
                                        <asp:RadioButton ID="RdNotprocessing" runat="server" OnCheckedChanged="RdNotprocessing_CheckedChanged" CssClass="field-label"
                                                Text="View Unprocessed Records" AutoPostBack="True"></asp:RadioButton></td>
                                </tr>
                                <tr class="title-bg">
                                    <td align="left" colspan="4">Details</td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="gvExcel" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered" Width="100%">
                                            <Columns>
                                                <asp:BoundField DataField="STU_NO" HeaderText="STUDENT ID" />
                                                <asp:BoundField DataField="STU_NAME" HeaderText="NAME" />
                                                <asp:BoundField DataField="DOCDT" DataFormatString="{0:dd/MMM/yyyy}"
                                                    HeaderText="DATE" HtmlEncode="False" />
                                                <asp:BoundField DataField="FEE_DESCR" HeaderText="FEE" />
                                                <asp:BoundField DataField="NARRATION" HeaderText="NARRATION" />
                                                <asp:TemplateField HeaderText="FEE_ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("fee_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="AMOUNT" HeaderText="AMOUNT">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="FCT_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFCT_ID" runat="server" Text='<%# Bind("FCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr class="matters">
                                    <td align="right" colspan="4">
                                        <asp:Label ID="Amount" runat="server" Text="Label"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="matters">
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="26"   />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_Editid" runat="server" Value="0" />
                <asp:HiddenField ID="HidUpload" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />

    </div>
</asp:Content>

