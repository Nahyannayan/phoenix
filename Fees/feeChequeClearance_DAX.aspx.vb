Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Xml
Imports GridViewHelper

Partial Class Fees_feeChequeClearance_DAX
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Session("BANKTRAN") = "QR"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                '   --- For Checking Rights And Initilize The Edit Variables --
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                InitialiseControls()
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CHEQUE_COLLECTION And _
                ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CHEQUE_COLLECTION_TRANSPORT) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    txtdocDate.Text = GetDiplayDate()
                    If ViewState("datamode") = "add" Then
                        txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
                    End If
                End If
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_CHEQUE_COLLECTION
                        tr_Businessunit.Visible = False
                    Case OASISConstants.MNU_FEE_CHEQUE_COLLECTION_TRANSPORT
                        tr_Businessunit.Visible = True
                        Set_Bank()
                End Select
                bind_Currency()
                GridView1.Attributes.Add("bordercolor", "#1b80b6")
                txtNarrn.Text = "Cheque collection for the day " & txtdocDate.Text
                gridbind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Private Sub InitialiseControls()
        ViewState("datamode") = "add"
        txtBankCode.Text = Session("CollectBank")
        txtBankDescr.Text = Session("Collect_name")
        ddCollection.DataBind()
        ddlBusinessunit.DataBind()
        h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
        h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
        h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
        h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
        h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
        h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
        txtdocNo.Attributes.Add("readonly", "readonly")
        txtBankDescr.Attributes.Add("readonly", "readonly")
        txtExchRate.Attributes.Add("readonly", "readonly")
        txtLocalRate.Attributes.Add("readonly", "readonly")
        btnPrint.Visible = False
    End Sub

    Private Sub bind_Currency()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            cmbCurrency.Items.Clear()
            cmbCurrency.DataSource = MasterFunctions.GetExchangeRates(txtdocDate.Text, Session("sBsuid"), Session("BSU_CURRENCY"))
            cmbCurrency.DataTextField = "EXG_CUR_ID"
            cmbCurrency.DataValueField = "RATES"
            cmbCurrency.DataBind()
            If cmbCurrency.Items.Count > 0 Then
                If set_default_currency() <> True Then
                    cmbCurrency.SelectedIndex = 0
                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub gridbind()
        Dim iAddDays As Integer = 0
        If Not IsNumeric(txtDaysInclude.Text) Then
            txtDaysInclude.Text = 0
        Else
            iAddDays = CInt(txtDaysInclude.Text)
        End If
        If Not IsDate(txtdocDate.Text) Then
            'lblError.Text = "Invalid Date"
            usrMessageBar.ShowNotification("Invalid Date", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim lstrDocNo, lstrBank, lstrPartyAC, lstrCreditor, lstrChqNo, lstrCurrency, lstrAmount As String
        Dim lstrOpr, lstrFiltDocNo, lstrFiltBank, lstrFiltCreditor, lstrFiltChqNo, lstrFiltCurrency As String
        Dim larrSearchOpr() As String
        Dim txtSearch As New TextBox
        lstrFiltDocNo = ""
        lstrFiltBank = ""
        lstrFiltCreditor = ""
        lstrFiltChqNo = ""
        lstrFiltCurrency = ""
        lstrCurrency = ""
        lstrChqNo = ""
        lstrDocNo = ""
        lstrCreditor = ""
        lstrBank = ""
        If GridView1.Rows.Count > 0 Then
            ' --- Initialize The Variables
            lstrDocNo = ""
            lstrBank = ""
            lstrPartyAC = ""
            lstrCreditor = ""
            lstrChqNo = ""
            lstrCurrency = ""
            lstrAmount = ""
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)

            '   --- FILTER CONDITIONS ---
            '   -- 1   DocNo
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = GridView1.HeaderRow.FindControl("txtDocNo")
            If txtSearch IsNot Nothing Then
                lstrDocNo = Trim(txtSearch.Text)
                If (lstrDocNo <> "") Then lstrFiltDocNo = SetCondn(lstrOpr, "VCD_DOCNO", lstrDocNo)
            End If
            '   -- 2  DocDate
            larrSearchOpr = h_Selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = GridView1.HeaderRow.FindControl("txtChqNo")
            If txtSearch IsNot Nothing Then
                lstrBank = txtSearch.Text
                If (lstrBank <> "") Then lstrFiltBank = SetCondn(lstrOpr, "VCD_CHQNO", lstrBank)
            End If
            '   -- 3  Bank AC
            larrSearchOpr = h_Selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = GridView1.HeaderRow.FindControl("txtRecNo")
            If txtSearch IsNot Nothing Then
                lstrCreditor = txtSearch.Text
                If (lstrCreditor <> "") Then lstrFiltCreditor = SetCondn(lstrOpr, "VCD_FCL_RECNO", lstrCreditor)
            End If

            '   -- 5  Narration
            larrSearchOpr = h_Selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = GridView1.HeaderRow.FindControl("txtBank")
            If txtSearch IsNot Nothing Then
                lstrChqNo = txtSearch.Text
                If (lstrChqNo <> "") Then lstrFiltChqNo = SetCondn(lstrOpr, "BNK_SHORT", lstrChqNo)
            End If
            '   -- 7  Currency
            larrSearchOpr = h_Selected_menu_7.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = GridView1.HeaderRow.FindControl("txtEmirate")
            If txtSearch IsNot Nothing Then
                lstrCurrency = txtSearch.Text
                If (lstrCurrency <> "") Then lstrFiltCurrency = SetCondn(lstrOpr, "EMR_DESCR", lstrCurrency)
            End If
        End If
        Dim str_Filter As String = lstrFiltDocNo & lstrFiltBank & lstrFiltCreditor & lstrFiltChqNo & lstrFiltCurrency

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString
        Dim str_Sql As String = ""
        Select Case ViewState("MainMnu_code").ToString
            Case OASISConstants.MNU_FEE_CHEQUE_COLLECTION
                str_Sql = "SELECT     VCD.VCD_DOCNO, VCD.VCD_FYEAR, SUM(VCD.VCD_AMOUNT) AS VCD_AMOUNT, VCD.VCD_CHQNO, " _
                    & " REPLACE(CONVERT(VARCHAR(11), VCD.VCD_CHQDT, 113), ' ', '/') AS VCD_CHQDT, VCD.VCD_FCL_RECNO, " _
                    & " VCD.VCD_BNK_ID, BNK.BNK_SHORT,ISNULL(VCD.VCD_BANK_ACT_ID,'')  DEP_BANK, VCD.VCD_EMR_ID, EMR.EMR_DESCR, MAX(VCD.VCD_NARRATION) AS VCD_NARRATION, " _
                    & " VCD.VCD_CHQID, VCH.VCH_CHQOWNER_BSU_ID FROM         FIN.VOUCHER_CHQ_D AS VCD INNER JOIN" _
                    & " FIN.VOUCHER_CHQ_H AS VCH ON VCD.VCD_SUB_ID = VCH.VCH_SUB_ID AND VCD.VCD_BSU_ID = VCH.VCH_BSU_ID AND " _
                    & " VCD.VCD_FYEAR = VCH.VCH_FYEAR AND VCD.VCD_DOCTYPE = VCH.VCH_DOCTYPE AND VCD.VCD_DOCNO = VCH.VCH_DOCNO " _
                    & " LEFT OUTER JOIN OASISFIN..VW_OSO_BANK_M AS BNK ON VCD.VCD_BNK_ID = BNK.BNK_ID LEFT OUTER JOIN" _
                    & " OASISFIN..VW_OSO_EMIRATE_M AS EMR ON VCD.VCD_EMR_ID = EMR.EMR_CODE" _
                    & " WHERE ISNULL(VCH.VCH_CHQOWNER_BSU_ID,'')='' AND  (VCD.VCD_DOCTYPE = 'QC') AND (VCD.VCD_SUB_ID = '" & Session("Sub_ID") & "') AND (VCD.VCD_BSU_ID = '" & Session("sBSUID") & "') " _
                    & " AND (VCD.VCD_CHQDT <= '" & CDate(txtdocDate.Text).AddDays(iAddDays) & "') AND  (ISNULL(VCD.VCD_bBANKRECNO, '') = '')" & str_Filter _
                    & " GROUP BY VCD.VCD_DOCNO, VCD.VCD_FYEAR, VCD.VCD_CHQNO, REPLACE(CONVERT(VARCHAR(11), VCD.VCD_CHQDT, 113), ' ', '/'), " _
                    & " VCD.VCD_FCL_RECNO,  VCD.VCD_BNK_ID, BNK.BNK_SHORT, VCD.VCD_EMR_ID, EMR.EMR_DESCR, VCD.VCD_CHQID, VCH.VCH_CHQOWNER_BSU_ID,VCD.VCD_BANK_ACT_ID"
            Case OASISConstants.MNU_FEE_CHEQUE_COLLECTION_TRANSPORT
                str_Sql = "SELECT     VCD.VCD_DOCNO, VCD.VCD_FYEAR, SUM(VCD.VCD_AMOUNT) AS VCD_AMOUNT, VCD.VCD_CHQNO, " _
                   & " REPLACE(CONVERT(VARCHAR(11), VCD.VCD_CHQDT, 113), ' ', '/') AS VCD_CHQDT, VCD.VCD_FCL_RECNO, " _
                   & " VCD.VCD_BNK_ID, BNK.BNK_SHORT,'' DEP_BANK, VCD.VCD_EMR_ID, EMR.EMR_DESCR, MAX(VCD.VCD_NARRATION) AS VCD_NARRATION, " _
                   & " VCD.VCD_CHQID, VCH.VCH_CHQOWNER_BSU_ID FROM         FIN.VOUCHER_CHQ_D AS VCD INNER JOIN" _
                   & " FIN.VOUCHER_CHQ_H AS VCH ON VCD.VCD_SUB_ID = VCH.VCH_SUB_ID AND VCD.VCD_BSU_ID = VCH.VCH_BSU_ID AND " _
                   & " VCD.VCD_FYEAR = VCH.VCH_FYEAR AND VCD.VCD_DOCTYPE = VCH.VCH_DOCTYPE AND VCD.VCD_DOCNO = VCH.VCH_DOCNO " _
                   & " LEFT OUTER JOIN OASISFIN..VW_OSO_BANK_M AS BNK ON VCD.VCD_BNK_ID = BNK.BNK_ID LEFT OUTER JOIN" _
                   & " OASISFIN..VW_OSO_EMIRATE_M AS EMR ON VCD.VCD_EMR_ID = EMR.EMR_CODE" _
                   & " WHERE   ISNULL(VCH.VCH_CHQOWNER_BSU_ID,'')='" & ddlBusinessunit.SelectedItem.Value & "' AND  (VCD.VCD_DOCTYPE = 'QC') AND (VCD.VCD_SUB_ID = '" & Session("Sub_ID") & "') AND (VCD.VCD_BSU_ID = '" & Session("sBSUID") & "') " _
                   & " AND (VCD.VCD_CHQDT <= '" & CDate(txtdocDate.Text).AddDays(iAddDays) & "') AND  (ISNULL(VCD.VCD_bBANKRECNO, '') = '')" & str_Filter _
                   & " GROUP BY VCD.VCD_DOCNO, VCD.VCD_FYEAR, VCD.VCD_CHQNO, REPLACE(CONVERT(VARCHAR(11), VCD.VCD_CHQDT, 113), ' ', '/'), " _
                   & " VCD.VCD_FCL_RECNO,  VCD.VCD_BNK_ID, BNK.BNK_SHORT, VCD.VCD_EMR_ID, EMR.EMR_DESCR, VCD.VCD_CHQID, VCH.VCH_CHQOWNER_BSU_ID"
        End Select
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        GridView1.DataSource = ds
        Dim helper As GridViewHelper
        helper = New GridViewHelper(GridView1, True)
        helper.RegisterSummary("VCD_AMOUNT", SummaryOperation.Sum)
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            GridView1.DataBind()
            Dim columnCount As Integer = GridView1.Rows(0).Cells.Count
            GridView1.Rows(0).Cells.Clear()
            GridView1.Rows(0).Cells.Add(New TableCell)
            GridView1.Rows(0).Cells(0).ColumnSpan = columnCount
            GridView1.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            GridView1.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            GridView1.DataBind()
        End If

        If GridView1.Rows.Count > 0 Then

            txtSearch = GridView1.HeaderRow.FindControl("txtDocNo")
            If txtSearch IsNot Nothing Then
                txtSearch.Text = lstrDocNo
            End If
            txtSearch = GridView1.HeaderRow.FindControl("txtChqNo")
            If txtSearch IsNot Nothing Then txtSearch.Text = lstrBank

            txtSearch = GridView1.HeaderRow.FindControl("txtRecNo")
            If txtSearch IsNot Nothing Then txtSearch.Text = lstrCreditor

            txtSearch = GridView1.HeaderRow.FindControl("txtBank")
            If txtSearch IsNot Nothing Then txtSearch.Text = lstrChqNo

            txtSearch = GridView1.HeaderRow.FindControl("txtEmirate")
            If txtSearch IsNot Nothing Then txtSearch.Text = lstrCurrency
        End If
    End Sub

    Private Function set_default_currency() As Boolean
        Try
            For Each item As ListItem In cmbCurrency.Items
                If item.Text.ToUpper = Session("BSU_CURRENCY").ToString.ToUpper Then
                    item.Selected = True
                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
                    Return True
                    Exit For
                End If
            Next
            Return False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return False
        End Try
    End Function

    Private Sub Clear_Header()
        txtdocNo.Text = ""
        txtdocDate.Text = GetDiplayDate()
        txtNarrn.Text = ""
        bind_Currency()
        txtBankCode.Text = Session("CollectBank")
        txtBankDescr.Text = Session("Collect_name")
    End Sub

    Protected Function GetNavigateUrl(ByVal pId As String, ByVal pAmount As String, ByVal pPly As String) As String
        Return String.Format("javascript:var popup = window.showModalDialog('TestAlloc.aspx?Id={0}&pAmount={1}&pPly={2}', '','dialogHeight:600px;dialogWidth:800px;scroll:yes;resizable:no;');", pId, pAmount, pPly)
    End Function

    Protected Function SaveValidate() As String
        Dim lstrErrMsg As String
        lstrErrMsg = ""
        If Trim(txtdocNo.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocNo " & "<br>"
        End If
        If Trim(txtdocDate.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid DocDate " & "<br>"
        End If
        If Trim(txtNarrn.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Enter Narration " & "<br>"
        End If
        If Trim(txtBankCode.Text = "") Then
            lstrErrMsg = lstrErrMsg & "Invalid Bank " & "<br>"
        End If
        'If (IsNumeric(txtAmount.Text) = False) Then
        '    lstrErrMsg = lstrErrMsg & "Amount Should Be A Numeric Value" & "<br>"
        'End If 
        Return lstrErrMsg
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim lintRetVal As Integer
        Dim lstrNewDocNo As String = ""

        Dim decSumAmount As Decimal = 0
        '   ----------------- VALIDATIONS --------------------
        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            'lblError.Text = "Invalid Bank Selected"
            usrMessageBar.ShowNotification("Invalid Bank Selected", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        Else
            'lblError.Text = ""
        End If
        Dim str_err As String = SaveValidate()
        Dim strfDate As String = txtdocDate.Text.Trim

        str_err = str_err & DateFunctions.checkdate(strfDate)
        Dim str_FYear As String = UtilityObj.GetDataFromSQL("SELECT FYR_ID FROM FINANCIALYEAR_S " _
         & " WHERE '" & txtdocDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT AND ISNULL(FYR_bCLOSE,0)=0", _
         WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)

        ''''''''''''''''
        Dim xmlDoc As New XmlDocument
        Dim XMLVCD_DET As XmlElement
        Dim XMLVCD_DOCNO As XmlElement
        Dim XMLVCD_FYEAR As XmlElement
        Dim XMLVCD_CHQNO As XmlElement
        Dim XMLVCD_CHQDT As XmlElement
        Dim XMLVCD_BNK_ID As XmlElement
        Dim XMLVCD_EMR_ID As XmlElement
        Dim XMLVCD_CHQID As XmlElement
        Dim XMLRoot As XmlElement
        XMLRoot = xmlDoc.CreateElement("Root")
        xmlDoc.AppendChild(XMLRoot)
        For Each gvr As GridViewRow In GridView1.Rows
            Dim chkVCD_ID As HtmlInputCheckBox = CType(gvr.FindControl("chkVCD_ID"), HtmlInputCheckBox)
            Dim lblVCD_BNK_ID As Label = CType(gvr.FindControl("lblVCD_BNK_ID"), Label)
            Dim lblVCD_EMR_ID As Label = CType(gvr.FindControl("lblVCD_EMR_ID"), Label)
            Dim lblVCD_CHQID As Label = CType(gvr.FindControl("lblVCD_CHQID"), Label)

            Dim lblDocno As Label = CType(gvr.FindControl("lblDocno"), Label)
            Dim lblChqNo As Label = CType(gvr.FindControl("lblChqNo"), Label)
            If Not chkVCD_ID Is Nothing And Not lblVCD_BNK_ID Is Nothing And Not lblVCD_EMR_ID Is Nothing Then
                If chkVCD_ID.Checked Then
                    decSumAmount = decSumAmount + CDbl(gvr.Cells(1).Text)
                    'str_VCD_IDS = str_VCD_IDS & chkVCD_ID.Value & "|"
                    'VCD_DOCNO, VCD_FYEAR, VCD_CHQNO, VCD_CHQDT, VCD_BNK_ID, VCD_EMR_ID
                    XMLVCD_DET = xmlDoc.CreateElement("VCD_DET")
                    XMLVCD_DOCNO = xmlDoc.CreateElement("VCD_DOCNO")
                    XMLVCD_DOCNO.InnerText = lblDocno.Text
                    XMLVCD_DET.AppendChild(XMLVCD_DOCNO)

                    XMLVCD_FYEAR = xmlDoc.CreateElement("VCD_FYEAR")
                    XMLVCD_FYEAR.InnerText = str_FYear
                    XMLVCD_DET.AppendChild(XMLVCD_FYEAR)

                    XMLVCD_CHQNO = xmlDoc.CreateElement("VCD_CHQNO")
                    XMLVCD_CHQNO.InnerText = lblChqNo.Text
                    XMLVCD_DET.AppendChild(XMLVCD_CHQNO)

                    XMLVCD_CHQDT = xmlDoc.CreateElement("VCD_CHQDT")
                    XMLVCD_CHQDT.InnerText = gvr.Cells(4).Text
                    XMLVCD_DET.AppendChild(XMLVCD_CHQDT)

                    XMLVCD_BNK_ID = xmlDoc.CreateElement("VCD_BNK_ID")
                    XMLVCD_BNK_ID.InnerText = lblVCD_BNK_ID.Text
                    XMLVCD_DET.AppendChild(XMLVCD_BNK_ID)

                    XMLVCD_EMR_ID = xmlDoc.CreateElement("VCD_EMR_ID")
                    XMLVCD_EMR_ID.InnerText = lblVCD_EMR_ID.Text
                    XMLVCD_DET.AppendChild(XMLVCD_EMR_ID)

                    XMLVCD_CHQID = xmlDoc.CreateElement("VCD_CHQID")
                    XMLVCD_CHQID.InnerText = lblVCD_CHQID.Text
                    XMLVCD_DET.AppendChild(XMLVCD_CHQID)
                    xmlDoc.DocumentElement.InsertBefore(XMLVCD_DET, xmlDoc.DocumentElement.LastChild)
                End If
            End If
        Next

        'If xmlDoc.OuterXml.ToString = "" Then
        If decSumAmount = 0 Then
            str_err = str_err & "<br />" & "Please select at least one details"
        End If


        If str_FYear = "" Or str_FYear = "--" Then
            str_err = str_err & "<br />" & "Check Financial Year"
        End If
        If str_err <> "" Then
            'lblError.Text = str_err
            usrMessageBar.ShowNotification(str_err, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        Else
            txtdocDate.Text = strfDate
        End If
        '''''''''''''
        '   ----------------- END OG VALIDATE -----------------        
        Session("iDeleteCount") = 0
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim SqlCmd As New SqlCommand("FIN.SaveVOUCHER_H", objConn, stTrans)
            SqlCmd.CommandType = CommandType.StoredProcedure
            Dim sqlpGUID As New SqlParameter("@GUID", SqlDbType.UniqueIdentifier)
            sqlpGUID.Value = System.DBNull.Value
            SqlCmd.Parameters.Add(sqlpGUID)
            SqlCmd.Parameters.AddWithValue("@VHH_SUB_ID", Session("SUB_ID"))
            SqlCmd.Parameters.AddWithValue("@VHH_BSU_ID", Session("sBsuid"))
            SqlCmd.Parameters.AddWithValue("@VHH_FYEAR", str_FYear)
            SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", Session("BANKTRAN"))
            SqlCmd.Parameters.AddWithValue("@VHH_DOCNO", Trim(txtdocNo.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_REFNO", "")
            SqlCmd.Parameters.AddWithValue("@VHH_TYPE", "R")
            SqlCmd.Parameters.AddWithValue("@VHH_CHB_ID", "")
            SqlCmd.Parameters.AddWithValue("@VHH_DOCDT", Trim(txtdocDate.Text))
            SqlCmd.Parameters.AddWithValue("@VHH_CHQDT", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtBankCode.Text)
            SqlCmd.Parameters.AddWithValue("@VHH_NOOFINST", 0)
            SqlCmd.Parameters.AddWithValue("@VHH_MONTHINTERVEL", 0)
            SqlCmd.Parameters.AddWithValue("@VHH_PARTY_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_INSTAMT", decSumAmount)
            SqlCmd.Parameters.AddWithValue("@VHH_INTPERCT", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_bINTEREST", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bAuto", False)
            SqlCmd.Parameters.AddWithValue("@VHH_CALCTYP", System.DBNull.Value)

            SqlCmd.Parameters.AddWithValue("@VHH_INT_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_ACRU_INT_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_CHQ_pdc_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_PROV_ACT_ID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_COL_ACT_ID", System.DBNull.Value)

            SqlCmd.Parameters.AddWithValue("@VHH_CUR_ID", cmbCurrency.SelectedItem.Text)
            SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE1", txtExchRate.Text)
            SqlCmd.Parameters.AddWithValue("@VHH_EXGRATE2", txtLocalRate.Text)
            SqlCmd.Parameters.AddWithValue("@VHH_NARRATION", txtNarrn.Text)
            SqlCmd.Parameters.AddWithValue("@VHH_COL_ID", ddCollection.SelectedItem.Value)
            SqlCmd.Parameters.AddWithValue("@VHH_AMOUNT", decSumAmount)
            SqlCmd.Parameters.AddWithValue("@VHH_bDELETED", False)
            SqlCmd.Parameters.AddWithValue("@VHH_bPOSTED", False)
            SqlCmd.Parameters.AddWithValue("@bGenerateNewNo", True)
   
            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.MNU_FEE_CHEQUE_COLLECTION
                Case OASISConstants.MNU_FEE_CHEQUE_COLLECTION_TRANSPORT
                    SqlCmd.Parameters.AddWithValue("@VHH_OWNER_BSU_ID", ddlBusinessunit.SelectedItem.Value)
            End Select
            Dim sqlpJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
            If ViewState("datamode") <> "edit" Then
                sqlpJHD_TIMESTAMP.Value = System.DBNull.Value
            Else
                sqlpJHD_TIMESTAMP.Value = Session("str_timestamp")
            End If
            SqlCmd.Parameters.Add(sqlpJHD_TIMESTAMP)
            SqlCmd.Parameters.AddWithValue("@VHH_SESSIONID", Session.SessionID)
            SqlCmd.Parameters.AddWithValue("@VHH_LOCK", Session("sUsr_name"))
            SqlCmd.Parameters.Add("@VHH_NEWDOCNO", SqlDbType.VarChar, 20)
            SqlCmd.Parameters("@VHH_NEWDOCNO").Direction = ParameterDirection.Output
            If (ViewState("datamode") = "edit") Then
                SqlCmd.Parameters.AddWithValue("@bEdit", True)
            Else
                SqlCmd.Parameters.AddWithValue("@bEdit", False)
            End If
            SqlCmd.Parameters.AddWithValue("@VHH_bPDC", False)
            SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue

            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.MNU_FEE_CHEQUE_COLLECTION
                    SqlCmd.Parameters.AddWithValue("@VHH_DAX_REF_DOCTYPE", "FEE CHEQUE COLN")
                Case OASISConstants.MNU_FEE_CHEQUE_COLLECTION_TRANSPORT
                    SqlCmd.Parameters.AddWithValue("@VHH_DAX_REF_DOCTYPE", "TPT CHEQUE COLN")
            End Select

            SqlCmd.ExecuteNonQuery()
            lintRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
            If lintRetVal = 0 Then
                If (ViewState("datamode") = "edit") Then
                    lstrNewDocNo = txtdocNo.Text
                Else
                    lstrNewDocNo = CStr(SqlCmd.Parameters("@VHH_NEWDOCNO").Value)
                End If
            End If
            'Adding header info
            ViewState("VHD_DOCNO") = lstrNewDocNo
            ViewState("FYear") = str_FYear

            SqlCmd.Parameters.Clear()
            'Adding transaction info 
            If (lintRetVal = 0) Then
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_CHEQUE_COLLECTION
                        str_err = FeeDayendProcess_DAX.GenerateQRDetails(Session("sBsuid"), _
                        txtdocDate.Text, Session("SUB_ID"), Session("BANKTRAN"), xmlDoc.OuterXml, _
                        lstrNewDocNo, Session("sUsr_name"), str_FYear, stTrans)
                    Case OASISConstants.MNU_FEE_CHEQUE_COLLECTION_TRANSPORT
                        str_err = FeeDayendProcess_DAX.GenerateQRDetails_Transport(Session("sBsuid"), _
                        txtdocDate.Text, Session("SUB_ID"), Session("BANKTRAN"), xmlDoc.OuterXml, _
                        lstrNewDocNo, Session("sUsr_name"), str_FYear, ddlBusinessunit.SelectedItem.Value, stTrans)
                End Select

                If str_err = "0" Then
                    str_err = FeeDayendProcess_DAX.POSTVOUCHER_QR(Session("SUB_ID"), _
                        Session("sBsuid"), str_FYear, "QR", lstrNewDocNo, stTrans)
                End If
                If str_err = "0" Then
                    str_err = FeeDayendProcess_DAX.POSTVOUCHER_CHQ_REVERSE(Session("SUB_ID"), _
                        Session("sBsuid"), str_FYear, "QR", lstrNewDocNo, txtdocDate.Text, stTrans)
                End If
                Dim RETURN_STATUS, RETURN_MESSAGE As String
                RETURN_STATUS = ""
                RETURN_MESSAGE = ""

                If str_err = "0" Then
                    str_err = FeeDayendProcess_DAX.CopyVouchersToDAXStaging(Session("sBsuid"), IIf(ViewState("MainMnu_code").ToString = OASISConstants.MNU_FEE_CHEQUE_COLLECTION, "Fees", "TPT"), "", "PENDING", "", lstrNewDocNo, stTrans, RETURN_STATUS, RETURN_MESSAGE)
                End If

                If RETURN_STATUS = "FAILED" Then
                    'lblError.Text = RETURN_MESSAGE
                    usrMessageBar.ShowNotification(RETURN_MESSAGE, UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    objConn.Close()
                    Exit Sub
                End If

                If str_err = "0" Then
                    stTrans.Commit()
                    'stTrans.Rollback()
                    Call Clear_Header()
                    gridbind()

                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    If ViewState("datamode") = "add" Then
                        txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
                    End If
                    If ViewState("datamode") <> "edit" Then
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, lstrNewDocNo, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                    Else
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, txtdocNo.Text, "EDIT", Page.User.Identity.Name.ToString, Me.Page)
                    End If
                    'lblError.Text = "Data Successfully Saved..."
                    usrMessageBar.ShowNotification("Data Successfully Saved...", UserControls_usrMessageBar.WarningType.Success)
                    btnPrint.Visible = True
                Else
                    'lblError.Text = getErrorMessage(str_err)
                    usrMessageBar.ShowNotification(getErrorMessage(str_err), UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                End If
            Else
                'lblError.Text = getErrorMessage(lintRetVal & "")
                usrMessageBar.ShowNotification(getErrorMessage(lintRetVal & ""), UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            stTrans.Rollback()
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
        Finally
            objConn.Close() 'Finally, close the connection
        End Try
    End Sub


   
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        '   --- Remove ReadOnly From The Form 
        txtBankCode.Text = Session("CollectBank")
        txtBankDescr.Text = Session("Collect_name")
        'set_collection()
        Call Clear_Header()
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        If ViewState("datamode") = "add" Then
            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
        End If
        imgDate.Enabled = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'PrintReceipt()
        'Exit Sub


        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub cmbCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCurrency.SelectedIndexChanged
        txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub

    Sub DoDateChange()
        Dim strfDate As String = txtdocDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            'lblError.Text = str_err
            usrMessageBar.ShowNotification(str_err, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        Else
            txtdocDate.Text = strfDate
        End If
        bind_Currency()
        gridbind()
        If ViewState("datamode") = "add" Then
            txtdocNo.Text = Master.GetNextDocNo(Session("BANKTRAN"), Month(Convert.ToDateTime(txtdocDate.Text)), Year(Convert.ToDateTime(txtdocDate.Text))).ToString
        End If
        txtNarrn.Text = "Cheque collection for the day " & txtdocDate.Text
    End Sub

    Protected Sub txtDocDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdocDate.TextChanged
        DoDateChange()
    End Sub

    Protected Sub imgDate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDate.Click
        DoDateChange()
    End Sub

    Protected Sub imgBank_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBank.Click
        chk_bankcode()
    End Sub

    Protected Sub txtBankCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBankCode.TextChanged
        chk_bankcode()
    End Sub

    Sub chk_bankcode()
        txtBankDescr.Text = AccountFunctions.Validate_Account(txtBankCode.Text, Session("sbsuid"), "BANK")
        If txtBankDescr.Text = "" Then
            'lblError.Text = "Invalid bank selected"
            usrMessageBar.ShowNotification("Invalid bank selected", UserControls_usrMessageBar.WarningType.Danger)
            txtBankCode.Focus()
        Else
            'lblError.Text = ""
            cmbCurrency.Focus()
        End If
    End Sub

    Protected Sub txtDaysInclude_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDaysInclude.TextChanged
        DoDateChange()
    End Sub

    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Session("ReportSource") = FeeDayendProcess.PrintQR(ViewState("VHD_DOCNO"), txtdocDate.Text, Session("sBsuID"), Session("F_YEAR"), "QR")
        Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_Bank()
        gridbind()
    End Sub

    Sub Set_Bank()
        Dim str_bankact_name As String = UtilityObj.GetDataFromSQL("SELECT BSU.BSU_COLLECTBANK_ACT_ID+'|'+ACT.ACT_NAME " _
       & " FROM BUSINESSUNIT_M AS BSU INNER JOIN" _
       & " OASISFIN..ACCOUNTS_M AS ACT ON BSU.BSU_COLLECTBANK_ACT_ID = ACT.ACT_ID" _
       & " WHERE (BSU.BSU_ID = '" & ddlBusinessunit.SelectedItem.Value & "')", ConnectionManger.GetOASISTRANSPORTConnectionString)
        If str_bankact_name <> "" Then
            txtBankCode.Text = str_bankact_name.Split("|")(0)
            txtBankDescr.Text = str_bankact_name.Split("|")(1)
        End If
    End Sub

End Class
