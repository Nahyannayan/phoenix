Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Net
Imports Newtonsoft.Json.Linq

Partial Class Fees_feeTranspotCollectionNew
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                Select Case Session("sBsuid").ToString
                    Case OASISConstants.TransportBSU_BrightBus
                        lnkSetArea.Visible = False
                    Case OASISConstants.TransportBSU_Sts
                        lnkSetArea.Visible = True
                End Select
                ViewState("datamode") = "add"
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                InitialiseCompnents()
                ddlBusinessunit.DataBind()
                GET_COOKIE()

                ddlEmirateMain.DataBind()
                BindEmirate()
                FillACD()
                'PopulateMonths()
                'SetNarration()
                If USR_NAME = "" Or CurBsUnit = "" Or _
                (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_FEE_COLLECTION_NEW And _
                ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_FEE_COLLECTION) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If

                Dim qry As String = "SELECT CONVERT(VARCHAR(3),CREDITCARD_S.CRR_ID)+'='+CONVERT(VARCHAR(10),isnull(dbo.CREDITCARD_S.CRR_CLIENT_RATE,0))+'|' FROM CREDITCARD_S INNER JOIN " & _
                                " CREDITCARD_M ON CREDITCARD_S.CRR_CRI_ID = CREDITCARD_M.CRI_ID INNER JOIN " & _
                                " CREDITCARD_PROVD_M ON CREDITCARD_S.CRR_CPM_ID = CREDITCARD_PROVD_M.CPM_ID " & _
                                " WHERE     (CREDITCARD_S.CRR_bOnline = 0) FOR XML PATH('')"
                Me.hfCrCharges.Value = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, qry)
                'ddCreditcard.Attributes.Add("onChange", "CalculateRate('" + Val(txtCCTotal.Text) + "');")
                gvFeeCollection.DataBind()
                SetTermMonth()
                uscStudentPicker.STU_BSU_ID = ddlBusinessunit.SelectedValue
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Sub LockYN(ByVal LoYN As Boolean)
        uscStudentPicker.IsReadonly = Not LoYN
        'imgStudent.Enabled = LoYN
        txtLocation.Enabled = LoYN
        'txtStdNo.Enabled = LoYN
        'txtStudentname.Enabled = LoYN
        txtBankTotal.Enabled = LoYN
        txtCCTotal.Enabled = LoYN
        txtCashTotal.Enabled = LoYN
    End Sub

    Sub SetTermMonth()
        Dim TrMon As Boolean = BrightBusCommon.TermOrMonth(ddlBusinessunit.SelectedItem.Value.ToString())
        rbMonthly.Checked = False
        rbTermly.Checked = False
        If TrMon = True Then
            rbMonthly.Checked = True
        Else
            rbTermly.Checked = True
        End If
    End Sub

    Sub BindEmirate()
        Try
            Dim str_default_emirate As String = GetDataFromSQL("SELECT BSU_CITY FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & ddlBusinessunit.SelectedItem.Value & "'", ConnectionManger.GetOASISConnectionString)
            If str_default_emirate <> "--" Then
                ddlEmirateMain.SelectedIndex = -1
                ddlEmirateMain.Items.FindByValue(str_default_emirate).Selected = True
            End If
        Catch ex As Exception
            Errorlog(ex.Message, "emirate")
        End Try
    End Sub

    Sub BindFeeType()
        Dim dtFeeType As New DataTable
        dtFeeType = FeeCommon.FEE_GETFEESFORCOLLECTION(ddlBusinessunit.SelectedValue, uscStudentPicker.STU_ACD_ID)
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataSource = dtFeeType
        ddlFeeType.DataBind()
    End Sub

    Sub InitialiseCompnents()
        txtTotal.Attributes.Add("readonly", "readonly")
        txtReceivedTotal.Attributes.Add("readonly", "readonly")
        txtBalance.Attributes.Add("readonly", "readonly")
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtChqdate.Text = Format(Date.Now, "dd/MMM/yyyy")
        gvFeeCollection.Attributes.Add("bordercolor", "#1b80b6")
        txtLocation.Attributes.Add("readonly", "readonly")
        LinkButton10.Attributes.Add("onClick", "return ValidateAddCheque();")
        'Me.txtRewardsAmount.Attributes.Add("readonly", "true")
        'BindFeeType()
        'ViewState("PERSISTENT_ID") = 0
    End Sub

    Sub clear_All(Optional ByVal bClearStudent As Boolean = True)
        gvFeeCollection.DataBind()
        txtCCTotal.Text = "0.00"
        txtCrCharge.Text = "0.00"

        txtCashTotal.Text = "0.00"
        txtTotal.Text = "0.00"
        txtReceivedTotal.Text = "0.00"
        gvChequeDetails.DataSource = Nothing
        gvChequeDetails.DataBind()
        If Not chkRemember.Checked Then
            txtCreditno.Text = ""
            txtBank.Text = ""
            h_BankId.Value = ""
            txtChequeNo.Text = ""
            txtChqdate.Text = ""
        End If
        ChkChgPost.Checked = True
        txtBalance.Text = "0.00"
        txtBankTotal.Text = "0.00"
        txtAmountAdd.Text = "0.00"
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        SetNarration()
        chkRemember.Checked = False
        ClearStudentData(bClearStudent)
    End Sub

    Private Function getChequeAmt() As Double
        Dim ChqAmount As Double = 0
        For Each gvr As GridViewRow In gvChequeDetails.Rows
            Dim txtChequeTotal As TextBox = CType(gvr.FindControl("txtChequeTotal"), TextBox)
            If Not txtChequeTotal Is Nothing Then
                ChqAmount = ChqAmount + CDbl(txtChequeTotal.Text)
            End If
        Next
        Return ChqAmount
    End Function

    Private Function doSaveChqDetails(ByVal NewFclId As String, ByVal stTrans As SqlTransaction) As String
        Dim retval As String = "0"
        Dim ChqAmount As Double = 0
        For Each gvr As GridViewRow In gvChequeDetails.Rows
            Dim txtChequeTotal As TextBox = CType(gvr.FindControl("txtChequeTotal"), TextBox)
            Dim txtChqno As TextBox = CType(gvr.FindControl("txtChqno"), TextBox)
            Dim txtChqDate As TextBox = CType(gvr.FindControl("txtChqDate"), TextBox)
            Dim h_Bank As HiddenField = CType(gvr.FindControl("h_Bank"), HiddenField)
            Dim hf_DBNK_TYPE As HiddenField = CType(gvr.FindControl("hf_DBNK_TYPE"), HiddenField)
            Dim hf_DBankAct As HiddenField = CType(gvr.FindControl("hf_DBankAct"), HiddenField)
            Dim ddlEmirate As DropDownList = CType(gvr.FindControl("ddlEmirate"), DropDownList)
            If Not txtChequeTotal Is Nothing Then
                If CDbl(txtChequeTotal.Text) > 0 Then
                    If h_Bank.Value.Trim = "" Then
                        Return "931" 'Invalid Bank
                    End If
                    If txtChqno.Text.Trim = "" Then
                        Return "930" 'Cheque no missing
                    End If
                    If Not IsDate(txtChqDate.Text) Then
                        Return "932" 'Invalid Date
                    End If
                    If Not hf_DBNK_TYPE Is Nothing AndAlso hf_DBNK_TYPE.Value = "BANKTRF" Then
                        If Not hf_DBankAct Is Nothing AndAlso hf_DBankAct.Value = "" Then
                            Return "947" 'Account missing when Bank Transfer
                        End If
                    End If
                    If hf_DBNK_TYPE.Value = "BANKTRF" AndAlso CDate(txtChqDate.Text) > CDate(DateTime.Now.ToShortDateString) Then
                        Return "948" 'PDC not allowed when Bank Transfer
                    End If
                    retval = FeeCollectionTransport.F_SaveFEECOLLSUB_D_TRANSPORT(0, NewFclId, COLLECTIONTYPE.CHEQUES, _
                  CDbl(txtChequeTotal.Text), txtChqno.Text, txtChqDate.Text, 0, "", h_Bank.Value, _
                  ddlEmirate.SelectedItem.Value, 0, stTrans, 0, hf_DBankAct.Value)
                End If
                If retval <> "0" Then
                    Return retval
                    Exit Function
                End If
            End If
        Next
        Return retval
    End Function

    Private Function getTotal(ByVal withCheque As Boolean) As Double
        txtCCTotal.Text = FeeCollection.GetDoubleVal(txtCCTotal.Text)
        txtCashTotal.Text = FeeCollection.GetDoubleVal(txtCashTotal.Text)
        txtRewardsAmount.Text = FeeCollection.GetDoubleVal(txtRewardsAmount.Text)
        Dim TotalAmt As Double = 0
        If withCheque = True Then
            TotalAmt = getChequeAmt()
        End If
        txtBankTotal.Text = TotalAmt.ToString("###.00")
        TotalAmt += FeeCollection.GetDoubleVal(txtCCTotal.Text) + FeeCollection.GetDoubleVal(txtCashTotal.Text) + FeeCollection.GetDoubleVal(txtRewardsAmount.Text)
        If FeeCollection.GetDoubleVal(txtCCTotal.Text) <> FeeCollection.GetDoubleVal(txtReceivedTotal.Text) And FeeCollection.GetDoubleVal(txtCrCharge.Text) <> 0 Then
            TotalAmt += FeeCollection.GetDoubleVal(txtCrCharge.Text)
        End If

        txtReceivedTotal.Text = TotalAmt.ToString("###.00")
        Return TotalAmt
    End Function

    Sub SetChqamount(ByVal lastColumn As Integer)
        Dim TotalAmount As Double = IIf(IsNumeric(txtTotal.Text), CDbl(txtTotal.Text), 0)
        Dim ChqAmount As Double = getTotal(False)
        Dim ChkAmt As Boolean = False
        Dim x As Integer = 0
        For Each gvr As GridViewRow In gvChequeDetails.Rows
            Dim ddlEmirate As DropDownList = TryCast(gvr.FindControl("ddlEmirate"), DropDownList)
            ddlEmirate.SelectedIndex = -1
            ddlEmirate.Items.FindByValue(ddlEmirateMain.SelectedItem.Value).Selected = True
            Dim txtChequeTotal As TextBox = CType(gvr.FindControl("txtChequeTotal"), TextBox)
            If Not txtChequeTotal Is Nothing Then

                ChqAmount = ChqAmount + CDbl(txtChequeTotal.Text)
                If ChkAmt = False Then
                    If ChqAmount = TotalAmount Then
                        ChkAmt = True
                    End If
                    If ChqAmount > TotalAmount Then
                        ChkAmt = True
                        'Dim txtCheqTotal As TextBox = CType(GridView1.Rows(x - 1).FindControl("txtChequeTotal"), TextBox)
                        txtChequeTotal.Text = Format((TotalAmount - (ChqAmount - CDbl(txtChequeTotal.Text))), "0.00")
                        txtChequeTotal.Text = FeeCollection.GetDoubleVal(txtChequeTotal.Text) + FeeCollection.GetDoubleVal(txtCrCharge.Text)
                        'txtChequeTotal.Text = "0.00"
                    End If
                Else
                    txtChequeTotal.Text = "0.00"
                End If
            End If
            x = x + 1
        Next
        If ChkAmt = True Then
            lastColumn = x
        End If
        If TotalAmount > ChqAmount Then
            Dim txtChqTotal As TextBox = CType(gvChequeDetails.Rows(lastColumn - 1).FindControl("txtChequeTotal"), TextBox)
            txtChqTotal.Text = Format(((TotalAmount - ChqAmount) + CDbl(txtChqTotal.Text)), "0.00")
        End If
        getTotal(True)
    End Sub

    Function check_errors_details() As String
        Dim str_error As String = ""
        'If txtCCTotal.Text = "" Then
        '    txtCCTotal.Text = 0
        'End If
        'If txtCashTotal.Text = "" Then
        '    txtCashTotal.Text = 0
        'End If
        'txtBankTotal.Text = getChequeAmt().ToString("###.00")

        'txtReceivedTotal.Text = CDbl(txtCCTotal.Text) + CDbl(txtCashTotal.Text) + CDbl(txtBankTotal.Text)
        Dim dblCrCharges As Double = 0
        If FeeCollection.GetDoubleVal(txtCCTotal.Text) <> FeeCollection.GetDoubleVal(txtReceivedTotal.Text) And FeeCollection.GetDoubleVal(txtCrCharge.Text) <> 0 Then
            dblCrCharges = FeeCollection.GetDoubleVal(txtCrCharge.Text)
        End If


        txtReceivedTotal.Text = getTotal(True).ToString("###.00")
        txtBalance.Text = CDbl(txtReceivedTotal.Text) - (CDbl(txtTotal.Text) + dblCrCharges)


        If Not IsDate(txtFrom.Text) Then
            str_error = str_error & "Invalid from date <br />"
        End If
        If uscStudentPicker.STU_NO = "" Then
            str_error = str_error & "Please select student <br />"
        End If
        'If dtFrom > Now.Date Then
        '    str_error = str_error &"Invalid date(Future Date)!!!" 
        'End If
        'If H_Location.Value = "" Then
        '    str_error = str_error & "Please set the area <br />"
        'End If
        If gvFeeCollection.Rows.Count = 0 Then
            str_error = str_error & "Please add fee details<br />"
        End If

        If Not IsNumeric(txtCCTotal.Text) Then
            str_error = str_error & "Invalid Credit Card amount <br />"
        Else
            If CDbl(txtCCTotal.Text) > 0 Then
                If txtCreditno.Text.Trim = "" Then
                    str_error = str_error & "Invalid Credit Card transaction # <br />"
                End If
            End If
        End If
        If Not IsNumeric(txtCashTotal.Text) Then
            str_error = str_error & "Invalid Cash amount <br />"
        End If

        Return str_error
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        setFeeCollection()
        Set_GridTotal()
        getTotal(True)
        Dim str_error As String = check_errors_details()
        Dim dblReceivedAmount, dblTotal, dblCash, dblBalance, dblCrCharge As Decimal
        dblReceivedAmount = CDbl(txtReceivedTotal.Text)
        dblTotal = CDbl(txtTotal.Text)
        dblCash = CDbl(txtCashTotal.Text)
        dblCrCharge = FeeCollection.GetDoubleVal(txtCrCharge.Text)
        If dblReceivedAmount <> (dblTotal + dblCrCharge) Then
            If dblCash > 0 Then
                If dblTotal < dblReceivedAmount Then
                    dblBalance = dblReceivedAmount - dblTotal
                    If dblBalance > dblCash Then
                        str_error = str_error & "Cannot Refund!!!(Excess amount)<br />"
                        txtReceivedTotal.Text = 0
                    Else
                        txtBalance.Text = Format(dblBalance, "0.00")
                    End If
                Else
                    str_error = str_error & "Totals not tallying<br />"
                End If
            Else
                txtBalance.Text = Format(CDbl(txtBalance.Text), "0.00")
                str_error = str_error & "Totals not tallying<br />"
            End If
        End If
        dblBalance = CDbl(txtBalance.Text)

        If str_error <> "" Then
            'lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim str_new_FCL_ID As Long
        Dim str_NEW_FCL_RECNO As String = ""
        Dim str_type As String = "S"
        'If rbEnquiry.Checked Then
        '    str_type = "E"
        'End If
        str_type = uscStudentPicker.STU_TYPE

        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            retval = FeeCollectionTransport.F_SaveFEECOLLECTION_H_TRANSPORT(0, "Counter", txtFrom.Text, _
                              txtRemarks.Text, uscStudentPicker.STU_ACD_ID, _
                              uscStudentPicker.STU_ID, _
                              CDec(txtReceivedTotal.Text) - dblBalance, Session("sUsr_name"), _
                              False, str_new_FCL_ID, Session("sBsuid"), txtRemarks.Text, "CR", str_NEW_FCL_RECNO, _
                              0, ddlBusinessunit.SelectedItem.Value, _
                              H_Location.Value, str_type, stTrans)

            For I As Integer = 0 To Session("FeeCollection").ROWS.COUNT - 1
                If retval = "0" Then
                    Dim bChargeandPost As Boolean = False
                    If Session("FeeCollection").ROWS(I)("bChargeandPost") = "1" Then
                        bChargeandPost = True
                    End If
                    retval = FeeCollectionTransport.F_SaveFEECOLLSUB_TRANSPORT(0, str_new_FCL_ID, Session("FeeCollection").ROWS(I)("FEE_ID"), _
                    Session("FeeCollection").ROWS(I)("amount"), bChargeandPost, stTrans)

                    If retval <> 0 Then
                        Exit For
                    End If
                End If
            Next
            '---------------------Added by Jacob on 27/OCT/2013
            If retval = "0" Then
                If FeeCollection.GetDoubleVal(txtCrCharge.Text) > 0 Then
                    retval = FeeCollectionTransport.F_SaveFEECOLLSUB_TRANSPORT(0, str_new_FCL_ID, 162, FeeCollection.GetDoubleVal(txtCrCharge.Text), True, stTrans)
                End If
            End If


            If retval = "0" Then
                If CDbl(txtCCTotal.Text) > 0 And retval = "0" Then
                    retval = FeeCollectionTransport.F_SaveFEECOLLSUB_D_TRANSPORT(0, str_new_FCL_ID, COLLECTIONTYPE.CREDIT_CARD, _
                    FeeCollection.GetDoubleVal(txtCCTotal.Text) + FeeCollection.GetDoubleVal(Me.txtCrCharge.Text), txtCreditno.Text, txtFrom.Text, 0, "", ddCreditcard.SelectedItem.Value, _
                    "", ddMachineList.SelectedItem.Value, stTrans, FeeCollection.GetDoubleVal(Me.txtCrCharge.Text))
                End If
                If CDbl(txtCashTotal.Text) > 0 And retval = "0" Then
                    retval = FeeCollectionTransport.F_SaveFEECOLLSUB_D_TRANSPORT(0, str_new_FCL_ID, COLLECTIONTYPE.CASH, _
                   CDbl(txtCashTotal.Text), "", txtFrom.Text, 0, "", "", "", 0, stTrans)
                End If
                If retval = "0" Then
                    retval = doSaveChqDetails(str_new_FCL_ID, stTrans)
                End If
                If FeeCollection.GetDoubleVal(txtRewardsAmount.Text) > 0 And retval = "0" Then 'Rewards 1 here
                    retval = FeeCollectionTransport.F_SaveFEECOLLSUB_D_TRANSPORT(0, str_new_FCL_ID, COLLECTIONTYPE.REWARDS_REDEMPTION, _
                       FeeCollection.GetDoubleVal(Me.txtRewardsAmount.Text), "", txtFrom.Text, 0, "", "", "", 0, stTrans)
                End If
                If retval = "0" Then
                    retval = FeeCollectionTransport.F_SaveFEECOLLALLOCATION_D_TRANSPORT(ddlBusinessunit.SelectedItem.Value, _
                    str_new_FCL_ID, stTrans)
                End If
                If retval = "0" And 1 = 2 Then ' Commented by Shakeel on 17May2015 as it creates mismatch on past dates
                    retval = FeeCollectionTransport.F_SettleFee_Transport(Session("sBsuid"), txtFrom.Text, uscStudentPicker.STU_ID, _
                    ddlBusinessunit.SelectedItem.Value, stTrans)
                End If
                If Not chkRemember.Checked And retval = "0" Then
                    If ViewState("FCL_IDS") Is Nothing Then
                        retval = FeeCollectionTransport.F_SAVECHEQUEDATA_Transport(Session("sBsuid"), _
                        str_new_FCL_ID, ddlBusinessunit.SelectedItem.Value, Session("sUsr_name"), stTrans)
                    Else
                        retval = FeeCollectionTransport.F_SAVECHEQUEDATA_Transport(Session("sBsuid"), _
                        ViewState("FCL_IDS").ToString & "|" & str_new_FCL_ID, ddlBusinessunit.SelectedItem.Value, Session("sUsr_name"), stTrans)
                    End If
                End If
                '----------------------------GEMS Rewards redemption here------------------------------
                If FeeCollection.GetDoubleVal(txtRewardsAmount.Text) > 0 And retval = "0" Then
                    'BaseCurrTotal = FeeCollection.GetDoubleVal(txtRewardsAmount.Text) * ExchRate
                    retval = SAVE_COLLECTION_INITIATED(stTrans, str_new_FCL_ID)
                End If
                If retval = "0" Then
                    If chkRemember.Checked Then
                        If ViewState("FCL_IDS") Is Nothing Then
                            ViewState("FCL_IDS") = str_new_FCL_ID
                        Else
                            ViewState("FCL_IDS") = ViewState("FCL_IDS").ToString & "|" & str_new_FCL_ID
                        End If
                    Else
                        ViewState("FCL_IDS") = Nothing
                    End If

                    ViewState("recno") = str_NEW_FCL_RECNO
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_NEW_FCL_RECNO, _
                    "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    stTrans.Commit()
                    Session("FeeCollection").rows.clear()
                    'h_print.Value = "print"
                    ' Session("ReportSource") = FeeCollectionTransport.PrintReceipt(str_NEW_FCL_RECNO, Session("sUsr_name"), ddlBusinessunit.SelectedItem.Value, Session("sBsuid"))
                    h_print.Value = "?type=REC&id=" + Encr_decrData.Encrypt(str_NEW_FCL_RECNO) & _
                    "&bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) & _
                    "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & _
                    "&stu_bsu_id=" & Encr_decrData.Encrypt(ddlBusinessunit.SelectedItem.Value) & _
                    "&isexport=0"
                    'lblError.Text = getErrorMessage("0")
                    usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                    'clear_All()
                    uscStudentPicker.ClearDetails()
                    'PopulateMonths()
                    Dim httab As New Hashtable
                    If Not Session("sDEFAULTCCMACHINE") Is Nothing Then
                        httab = Session("sDEFAULTCCMACHINE")
                    End If
                    httab(ddlBusinessunit.SelectedValue) = ddMachineList.SelectedValue
                    Session("sDEFAULTCCMACHINE") = httab
                Else
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(retval)
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If

        Catch ex As Exception
            stTrans.Rollback()
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Sub setEditdata()
        txtFrom.Attributes.Remove("readonly")
        imgFrom.Enabled = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            clear_All()
            ViewState("datamode") = "view"
            LockYN(False)
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        clear_All()
        LockYN(True)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    'Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
    '    Gridbind_Feedetails()
    'End Sub

    Sub ClearChqDetails()
        gvChequeDetails.DataSource = Nothing
        gvChequeDetails.DataBind()
        getTotal(False)
    End Sub

    Sub Gridbind_Feedetails()
        'lbTCdate.Text = ""
        'lbStuDOJ.Text = ""
        'lblStuStatus.Text = ""
        'lbLDAdate.Text = ""
        'lblGrade.Text = ""
        'txtStdNo.ForeColor = Drawing.Color.Black
        'txtStudentname.ForeColor = Drawing.Color.Black

        If txtRemarks.Text.Trim = String.Empty Then
            SetNarration()
        End If
        If uscStudentPicker.STU_NO <> "" Then
            Dim bNoError As Boolean = True
            Try
                ClearChqDetails()
                'txtStdNo.Text = txtStdNo.Text.Trim
                'Dim iStdnolength As Integer = txtStdNo.Text.Length
                'If iStdnolength < 9 Then
                '    If txtStdNo.Text.Trim.Length < 8 Then
                '        For i As Integer = iStdnolength + 1 To 8
                '            txtStdNo.Text = "0" & txtStdNo.Text
                '        Next
                '    End If
                '    txtStdNo.Text = ddlBusinessunit.SelectedItem.Value & txtStdNo.Text
                'End If
                If Not FeeCommon.GetStudentWithLocation_Transport(uscStudentPicker.STU_ID, uscStudentPicker.STU_NO, uscStudentPicker.STU_NAME, _
                ddlBusinessunit.SelectedItem.Value, True, H_Location.Value, txtLocation.Text, txtFrom.Text, uscStudentPicker.STU_ACD_ID) Then
                    lblNoStudent.Text = "Invalid Student #/Area Not Found!!!"
                    bNoError = False
                    gvFeeCollection.DataBind()
                End If
            Catch ex As Exception
                bNoError = True
            End Try
            If IsDate(txtFrom.Text) And bNoError Then
                'Gridbind_CollectionHistory()
                Dim dt As New DataTable
                'If rbEnquiry.Checked Then
                '    dt = FeeCollectionTransport.F_GetFeeDetailsForCollectionTransport(txtFrom.Text, uscStudentPicker.STU_ID, "E", ddlBusinessunit.SelectedItem.Value, ddlAcademicYear.SelectedValue)
                'Else
                '    dt = FeeCollectionTransport.F_GetFeeDetailsForCollectionTransport(txtFrom.Text, uscStudentPicker.STU_ID, "S", ddlBusinessunit.SelectedItem.Value, ddlAcademicYear.SelectedValue)
                'End If
                dt = FeeCollectionTransport.F_GetFeeDetailsForCollectionTransport(txtFrom.Text, uscStudentPicker.STU_ID, uscStudentPicker.STU_TYPE, ddlBusinessunit.SelectedItem.Value, uscStudentPicker.STU_ACD_ID, "COUNTER")
                lblChqBalance.Text = GetChequeData(uscStudentPicker.STU_ID, txtFrom.Text, 1)
                gvFeeCollection.DataSource = dt
                'If dt.Rows.Count = 0 Then
                '    tr_AddFee.Visible = True
                '    'pnlMonthterm.Visible = True
                'Else
                '    tr_AddFee.Visible = False
                '    'pnlMonthterm.Visible = False
                'End If
                Session("FeeCollection") = dt
                gvFeeCollection.DataBind()
                Set_GridTotal()
            Else
                gvFeeCollection.DataBind()
                Set_GridTotal()
            End If
            If FeeCollection.GetDoubleVal(uscStudentPicker.STU_ID) <> 0 Then
                Dim QueryString = "&STUID=" & Encr_decrData.Encrypt(uscStudentPicker.STU_ID) & "&STYPE=" & Encr_decrData.Encrypt(uscStudentPicker.STU_TYPE) & "&SBSU=" & Encr_decrData.Encrypt(ddlBusinessunit.SelectedValue) & "&FDT=" & Encr_decrData.Encrypt(txtFrom.Text) & ""
                lbtnLedger.Attributes.Add("onClick", "return ShowWindowWithClose('FeeReportInterpose.aspx?TYPE=" & Encr_decrData.Encrypt("LEDGER") & QueryString & "', 'STUDENT LEDGER', '60%', '75%');")
                'lbtnAudit.Attributes.Add("onClick", "return ShowWindowWithClose('FeeReportInterpose.aspx?TYPE=" & Encr_decrData.Encrypt("AUDIT") & QueryString & "', 'STUDENT TRANSPORT AUDIT', '60%', '75%');")
                lbtnAudit.Attributes.Add("onClick", "return ShowWindowWithClose('StudentTransportAudit.aspx?ID=" & Encr_decrData.Encrypt(uscStudentPicker.STU_ID) & "', 'STUDENT TRANSPORT AUDIT', '60%', '75%');")

                'Dim objTC As New FeeCollectionTransport
                'objTC.BSU_ID = Session("sBsuId")
                'objTC.USER_NAME = Session("sUsr_name")
                'objTC.STU_ID = uscStudentPicker.STU_ID

                'objTC.GET_STUDENT_AUDIT_DETAIL()
                'If objTC.GRADE <> "" Then
                '    lblGrade.Text = "Grade : " & objTC.GRADE
                'End If
                'If objTC.STU_LASTATTDATE <> "" Then
                '    lbLDAdate.Text = " | LDA : " & objTC.STU_LASTATTDATE
                'End If
                'If objTC.STU_CANCELDATE <> "" Then
                '    lbTCdate.Text = " | TC Date : " & objTC.STU_CANCELDATE
                'End If
                'If objTC.STU_DOJ <> "" Then
                '    lbStuDOJ.Text = " | DOJ : " & objTC.STU_DOJ
                'End If
                'Select Case objTC.STU_CURRSTATUS
                '    Case "TF"
                '        lblStuStatus.Visible = True
                '        lblStuStatus.Text = " | Status : <span style='color:Green;'>" & objTC.STU_CURRSTATUS & "</span>"
                '        txtStdNo.ForeColor = Drawing.Color.Green
                '        txtStudentname.ForeColor = Drawing.Color.Green
                '    Case "EN"
                '        txtStdNo.ForeColor = Drawing.Color.Black
                '        txtStudentname.ForeColor = Drawing.Color.Black
                '    Case Else
                '        lblStuStatus.Visible = True
                '        lblStuStatus.Text = " | Status : <span style='color:Red;'>" & objTC.STU_CURRSTATUS & "</span>"
                '        txtStdNo.ForeColor = Drawing.Color.Red
                '        txtStudentname.ForeColor = Drawing.Color.Red
                'End Select
            End If
        Else
            lbtnLedger.Attributes.Remove("onClick")
            lbtnAudit.Attributes.Remove("onClick")
            lbtnLedger.Attributes.Add("onClick", "return false;")
            lbtnAudit.Attributes.Add("onClick", "return false;")
        End If
    End Sub

    Public Function GetChequeData(ByVal STU_ID As String, ByVal ONDATE As String, ByVal SUMMARY As Integer) As String
        Try
            Dim ds As New DataSet
            Dim str_sql As String = "exec TRANSPORT.GetChequeData " & STU_ID & ", '" & ONDATE & "' , " & SUMMARY
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
            CommandType.Text, str_sql)
            If ds.Tables(0).Rows.Count > 0 Then
                If Not ds.Tables(0).Rows(0)(0) Is System.DBNull.Value Then
                    Return "Cheques in hand :" & ds.Tables(0).Rows(0)(0) & ""
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ""
        End Try
    End Function

    Sub Set_GridTotal()
        Dim dAmount As Decimal = 0
        Dim dueAmount As Decimal = 0
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            dueAmount = dAmount + CDbl(gvr.Cells(7).Text)
            Dim ChkPost As CheckBox = CType(gvr.FindControl("ChkPost"), CheckBox)
            Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
            If Not txtAmountToPay Is Nothing Then
                dAmount = dAmount + CDbl(txtAmountToPay.Text)
            End If
            If Session("FeeCollection").rows(gvr.RowIndex)(("bChargeandPost")) = 1 Then
                ChkPost.Checked = True
            End If
            Dim lblFSR_FEE_ID As Label = TryCast(gvr.FindControl("lblFSR_FEE_ID"), Label)
            If lblFSR_FEE_ID.Text = OASISConstants.TransportFEEID Then
                ChkPost.Enabled = False
            Else
                ChkPost.Enabled = True
            End If
        Next
        txtTotal.Text = Format(dAmount, "0.00")
        h_du.Value = dueAmount.ToString()
    End Sub

    'Protected Sub rbEnrollment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnrollment.CheckedChanged
    '    ClearStudentData()
    'End Sub

    'Protected Sub rbEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnquiry.CheckedChanged
    '    ClearStudentData()
    'End Sub

    Sub ClearStudentData(Optional ByVal bClearStudent As Boolean = True)
        txtLocation.Text = ""
        H_Location.Value = ""
        h_Student_no.Value = "0"
        'txtStdNo.Text = ""
        'txtStudentname.Text = ""
        If bClearStudent Then
            uscStudentPicker.ClearDetails()
            btnRedeem.Attributes.Remove("onClick")
        End If
        'gvColHistory.DataBind()
        Gridbind_Feedetails()
        'ViewState("PERSISTENT_ID") = 0
    End Sub

    Protected Sub btnAionddDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDetails.Click
        setFeeCollection()
        If uscStudentPicker.STU_NO = "" Then
            'lblError.Text = "Select student!!!"
            usrMessageBar.ShowNotification("Select student!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        '''''''''''''
        If Not IsDate(txtFrom.Text) Then
            'lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If rblVAT.SelectedValue = "" Then
            'lblError.Text = "Please Select Including or Excluding TAX....!"
            usrMessageBar.ShowNotification("Please Select Including or Excluding TAX....!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        Dim dr As DataRow
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If IsNumeric(txtAmountAdd.Text) Then
            If CDbl(txtAmountAdd.Text) > 0 Then
                'FEE_ID, FEE_DESCR, Opening, CurrentCharge, Net,Amount
                'FSH_ID, FSH_DATE, FEE_ID, FEE_DESCR, AMOUNT, FSH_naRRATION
                For Each gvr As GridViewRow In gvFeeCollection.Rows
                    Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)

                    If Not lblFSR_FEE_ID Is Nothing Then
                        If lblFSR_FEE_ID.Text = ddlFeeType.SelectedItem.Value Then
                            'lblError.Text = "Fee repeating!!!"
                            usrMessageBar.ShowNotification("Fee repeating!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                    End If
                Next
                dr = Session("FeeCollection").NewRow
                dr("FEE_ID") = ddlFeeType.SelectedItem.Value
                dr("FEE_DESCR") = ddlFeeType.SelectedItem.Text
                dr("CLOSING") = 0
                dr("Amount") = txtAmountAdd.Text
                dr("bChargeandPost") = "0"
                dr("PAID_AMOUNT") = "0"
                If ChkChgPost.Checked Then
                    dr("bChargeandPost") = "1"
                    dr("CLOSING") = txtAmountAdd.Text
                End If
                Session("FeeCollection").rows.add(dr)
                gvFeeCollection.DataSource = Session("FeeCollection")
                txtAmountAdd.Text = "0.00"
                txtAmount.Text = "0.00"
                txtVATAmount.Text = "0.00"
                rblVAT.SelectedValue = Nothing
                gvFeeCollection.DataBind()
                Set_GridTotal()
                smScriptManager.SetFocus(txtCashTotal)
                tr_AddFee.Visible = Not tr_AddFee.Visible
                tr_AddFee1.Visible = Not tr_AddFee1.Visible
            Else
                'lblError.Text = "Invalid Amount"
                usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
                smScriptManager.SetFocus(txtAmountAdd)
            End If
        Else
            'lblError.Text = "Invalid Amount"
            usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
            smScriptManager.SetFocus(txtAmountAdd)
        End If

    End Sub
    Sub ClearDetail()
        txtAmountAdd.Text = "0.00"
        txtVATAmount.Text = "0.00"
        txtAmount.Text = "0.00"
        rblVAT.SelectedValue = Nothing
    End Sub

    'Protected Sub txtStdNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStdNo.TextChanged
    '    Gridbind_Feedetails()
    'End Sub

    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        clear_All()
        FillACD()
        'PopulateMonths()
        ddMachineList.DataBind()
        If Not Session("sDEFAULTCCMACHINE") Is Nothing Then
            Dim httab As Hashtable = Session("sDEFAULTCCMACHINE")
            ddMachineList.SelectedValue = httab(ddlBusinessunit.SelectedValue)
        End If
        BindEmirate()
        'SetTermMonth()
        SET_COOKIE()
        uscStudentPicker.STU_BSU_ID = ddlBusinessunit.SelectedValue
    End Sub

    Sub FillACD()
        'Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBusinessunit.SelectedItem.Value)
        'ddlAcademicYear.DataSource = dtACD
        'ddlAcademicYear.DataTextField = "ACY_DESCR"
        'ddlAcademicYear.DataValueField = "ACD_ID"
        'ddlAcademicYear.DataBind()
        'For Each rowACD As DataRow In dtACD.Rows
        '    If rowACD("ACD_CURRENT") Then
        '        ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
        '        Exit For
        '    End If
        'Next
        Set_Previous_Acd()
    End Sub

    Sub setFeeCollection()
        Dim iEdit, iRowcount As Integer
        iRowcount = 0
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            Dim lblFSR_FEE_ID As Label = TryCast(gvr.FindControl("lblFSR_FEE_ID"), Label)
            Dim ChkPost As CheckBox = CType(gvr.FindControl("ChkPost"), CheckBox)
            If Not lblFSR_FEE_ID Is Nothing Then
                If lblFSR_FEE_ID.Text = OASISConstants.TransportFEEID Then
                    ChkPost.Enabled = False
                Else
                    ChkPost.Enabled = True
                End If
                iRowcount = iRowcount + 1
                For iEdit = 0 To Session("FeeCollection").Rows.Count - 1
                    If lblFSR_FEE_ID.Text = Session("FeeCollection").Rows(iEdit)("FEE_ID") Then
                        Exit For
                    End If
                Next
            End If
            Dim str_amount As String
            str_amount = gvr.Cells(7).Text
            str_amount = TryCast(gvr.FindControl("txtAmountToPay"), TextBox).Text
            Session("FeeCollection").Rows(iEdit)("Amount") = str_amount
            Session("FeeCollection").Rows(iEdit)("bChargeandPost") = ChkPost.Checked
        Next
    End Sub

    Protected Sub txtAmountToPay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal()
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.SetFocus(sender)
        If sender.Parent.parent.rowindex() = gvFeeCollection.Rows.Count - 1 Then
            smScriptManager.SetFocus(txtCashTotal)
        Else
            smScriptManager.SetFocus(gvFeeCollection.Rows(sender.Parent.parent.rowindex + 1).FindControl("txtAmountToPay"))
        End If
    End Sub

    Sub SetNarration()
        If IsDate(txtFrom.Text) Then
            txtRemarks.Text = FeeCollectionTransport.F_GetFeeNarration(txtFrom.Text, uscStudentPicker.STU_ACD_ID, ddlBusinessunit.SelectedItem.Value)
        End If
    End Sub

    Protected Sub txtFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFrom.TextChanged
        SetNarration()
    End Sub

    Protected Sub imgFrom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgFrom.Click
        SetNarration()
    End Sub

    Protected Sub lnkSetArea_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Gridbind_Feedetails()
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim stu_no As String = Oasis_Counter.Counter.GetNextStudentInQueue(Session("counterId"), ddlBusinessunit.SelectedItem.Value, Session("sUsr_name"))
        'txtStdNo.Text = stu_no
        uscStudentPicker.LoadStudentsByNo(stu_no)
        Gridbind_Feedetails()
    End Sub

    Protected Sub imgNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim stu_no As String = Oasis_Counter.Counter.GetNextStudentInQueue(Session("counterId"), ddlBusinessunit.SelectedItem.Value, Session("sUsr_name"))
        'txtStdNo.Text = stu_no
        uscStudentPicker.LoadStudentsByNo(stu_no)
        Gridbind_Feedetails()
    End Sub

    Private Sub PopulateMonths()
        Dim dtTable As DataTable = FeeTranspotInvoice.PopulateMonthsInAcademicYearID(uscStudentPicker.STU_ACD_ID)
        ' PROCESS Filter
        Dim dvTRM_DESCRIPTION As New DataView(dtTable, "", "TRM_DESCRIPTION", DataViewRowState.OriginalRows)

        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim ienumTRM_DESCRIPTION As IEnumerator = dvTRM_DESCRIPTION.GetEnumerator
        Dim drTRM_DESCRIPTION As DataRowView
        While (ienumTRM_DESCRIPTION.MoveNext())
            'Processes List
            drTRM_DESCRIPTION = ienumTRM_DESCRIPTION.Current

            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drTRM_DESCRIPTION("TRM_DESCRIPTION") Then
                    contains = True
                End If
            End While
            Dim trNodeTRM_DESCRIPTION As New TreeNode(drTRM_DESCRIPTION("TRM_DESCRIPTION"), drTRM_DESCRIPTION("TRM_ID"))
            If contains Then
                Continue While
            End If
            'If trSelectAll.ChildNodes.Contains(trNodeTRM_DESCRIPTION) Then
            '    Continue While
            'End If
            Dim strAMS_MONTH As String = "TRM_DESCRIPTION = '" & _
            drTRM_DESCRIPTION("TRM_DESCRIPTION") & "'"
            Dim dvAMS_MONTH As New DataView(dtTable, strAMS_MONTH, "AMS_ID", DataViewRowState.OriginalRows)
            Dim ienumAMS_MONTH As IEnumerator = dvAMS_MONTH.GetEnumerator
            While (ienumAMS_MONTH.MoveNext())
                Dim drMONTH_DESCR As DataRowView = ienumAMS_MONTH.Current
                Dim trNodeMONTH_DESCR As New TreeNode(drMONTH_DESCR("MONTH_DESCR"), drMONTH_DESCR("AMS_ID"))
                trNodeTRM_DESCRIPTION.ChildNodes.Add(trNodeMONTH_DESCR)
            End While
            trSelectAll.ChildNodes.Add(trNodeTRM_DESCRIPTION)
        End While
        trMonths.Nodes.Clear()
        trMonths.Nodes.Add(trSelectAll)
        trMonths.DataBind()
    End Sub

    Protected Sub btnNarrInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNarrInsert.Click
        If rbMonthly.Checked Then
            txtRemarks.Text = FillMonthsSelected()
        Else
            txtRemarks.Text = FillTermsSelected()
        End If
        SetLocation()
        ' btnAionddDetails_Click(sender, e)
    End Sub

    Private Function FillMonthsSelected() As String
        Try
            Dim str_Selected As String = ""
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If str_Selected = "" Then
                    str_Selected = "For the month(s) " & node.Text
                Else
                    str_Selected = str_Selected & ", " & node.Text
                End If
            Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Private Function FillTermsSelected() As String
        Try
            Dim str_Selected As String = ""
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    If node.Value <> "ALL" Then
                        If str_Selected = "" Then
                            str_Selected = "For the Term(s) " & node.Text
                        Else
                            str_Selected = str_Selected & ", " & node.Text
                        End If
                    End If
                Else
                    Continue For
                End If
            Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Private Function GetTermsSelected() As String
        Try
            Dim str_Selected As String = ""
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    If node.Value <> "ALL" Then
                        If str_Selected = "" Then
                            str_Selected = node.Value & "|"
                        Else
                            str_Selected = str_Selected & node.Value & "|"
                        End If
                    End If
                Else
                    Continue For
                End If
            Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Private Function GetMonthsSelected() As String
        Try
            Dim str_Selected As String = ""
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If str_Selected = "" Then
                    str_Selected = node.Value & "|"
                Else
                    str_Selected = str_Selected & node.Value & "|"
                End If
            Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Sub SetLocation()
        Dim sch_id As Integer
        Dim str_monthorTerm As String
        If rbTermly.Checked Then
            str_monthorTerm = GetTermsSelected()
            sch_id = 2
        Else
            str_monthorTerm = GetMonthsSelected()
            sch_id = 0
        End If
        If H_Location.Value = "" Or str_monthorTerm = "" Then
            If str_monthorTerm = "" Then
                lblNoStudent.Text = "Please select Term/Month"
            End If
        Else
            Dim str_amount As String = FeeCollection.GETFEEPAYABLE(uscStudentPicker.STU_ACD_ID, _
            ddlBusinessunit.SelectedItem.Value, str_monthorTerm, H_Location.Value, sch_id, _
            ConnectionManger.GetOASISTRANSPORTConnectionString)
            'txtAmountAdd.Text = str_amount

            '''''''''
            setFeeCollection()

            Dim dr As DataRow
            Dim smScriptManager As New ScriptManager
            smScriptManager = Master.FindControl("ScriptManager1")
            Dim bAmountUpdated As Boolean = False
            If CDbl(str_amount) > 0 Then
                For I As Integer = 0 To Session("FeeCollection").ROWS.COUNT - 1
                    If Session("FeeCollection").ROWS(I)("FEE_ID") = OASISConstants.TransportFEEID Then
                        Session("FeeCollection").ROWS(I)("amount") = str_amount
                        bAmountUpdated = True
                    End If
                Next

                If Not bAmountUpdated Then
                    dr = Session("FeeCollection").NewRow
                    dr("FEE_ID") = OASISConstants.TransportFEEID
                    dr("FEE_DESCR") = "Transport Fee"
                    dr("CLOSING") = 0
                    dr("Amount") = str_amount
                    dr("bChargeandPost") = "0"
                    dr("PAID_AMOUNT") = "0"

                    Session("FeeCollection").rows.add(dr)
                End If
                gvFeeCollection.DataSource = Session("FeeCollection")
                gvFeeCollection.DataBind()
                Set_GridTotal()
                smScriptManager.SetFocus(txtCashTotal)
            End If
            '''''''''
        End If
    End Sub

    'Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    clear_All()
    '    Set_Previous_Acd()
    '    PopulateMonths()
    '    Gridbind_Feedetails()
    'End Sub

    Sub Set_Previous_Acd()
        h_PREV_ACD.Value = FeeCollection.GetPreviousACD(uscStudentPicker.STU_ACD_ID, ddlBusinessunit.SelectedItem.Value, _
        ConnectionManger.GetOASISTRANSPORTConnectionString)
    End Sub

    Protected Sub LinkButton10_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton10.Click
        bind_cheques()
    End Sub

    Sub bind_cheques()
        ClearChqDetails()
        Dim lastColumn As Integer = 0
        Dim dt As DataTable
        Dim sch_id As Integer
        Dim str_monthorTerm As String
        Dim FirstMonthAmount As Double = 0

        If rbTermly.Checked Then
            str_monthorTerm = GetTermsSelected()
            sch_id = 2
        Else
            str_monthorTerm = GetMonthsSelected()
            sch_id = 0
        End If
        If str_monthorTerm = "" Then
            str_monthorTerm = "N"
        End If

        If Not IsDate(txtChqdate.Text) Then
            txtChqdate.Text = Format(Date.Now, "dd/MMM/yyyy")
        End If
        If IsNumeric(H_Location.Value) Or ddlFeeType.SelectedItem.Value <> 6 Then
            If IsNumeric(H_Location.Value) Then
                dt = GETFEEPAYABLE_Detail_Next(uscStudentPicker.STU_ACD_ID, _
                ddlBusinessunit.SelectedItem.Value, str_monthorTerm, H_Location.Value, sch_id, txtChqdate.Text, _
                ConnectionManger.GetOASISTRANSPORTConnectionString, Int32.Parse(ddlTotchqs.SelectedValue.ToString))
            Else
                dt = GETFEEPAYABLE_Detail_Next(uscStudentPicker.STU_ACD_ID, _
                                ddlBusinessunit.SelectedItem.Value, str_monthorTerm, 0, sch_id, txtChqdate.Text, _
                                ConnectionManger.GetOASISTRANSPORTConnectionString, Int32.Parse(ddlTotchqs.SelectedValue.ToString))
            End If

            If dt.Rows.Count > 0 Then
                FirstMonthAmount = (FeeCollection.GetDoubleVal(h_du.Value) - getTotal(False))
                If FirstMonthAmount < 0 Then
                    FirstMonthAmount = 0
                End If
                lastColumn = dt.Rows.Count

                Dim NoChq As Integer = Int32.Parse(ddlTotchqs.SelectedValue.ToString) - (dt.Rows.Count + 1)
                'If rbTermly.Checked = True Then
                NoChq = NoChq + 1
                'End If
                For x As Integer = 0 To NoChq - 1
                    dt.Rows.Add(dt.NewRow)
                    dt.Rows(dt.Rows.Count - 1)("Id") = dt.Rows.Count + x
                Next
                If txtChqdate.Text <> dt.Rows(0)("DATE").ToString() Then
                    'dt.Rows.Add(dt.NewRow)
                    dt.Rows(dt.Rows.Count - 1)("AMOUNT") = Format(FirstMonthAmount, "0.00")
                    dt.Rows(dt.Rows.Count - 1)("DATE") = txtChqdate.Text.ToString()
                    dt.Rows(dt.Rows.Count - 1)("Id") = "0"
                ElseIf FirstMonthAmount > 0 Then
                    dt.Rows(0)("AMOUNT") = Format(FirstMonthAmount, "0.00")
                End If

                Dim dv As New DataView(dt)
                dv.Sort = "Id"
                gvChequeDetails.DataSource = dv
                gvChequeDetails.DataBind()
                SetChqamount(lastColumn)
            End If
        End If
    End Sub

    Public Function GETFEEPAYABLE_Detail_Next(ByVal p_ACD_ID As Integer, ByVal p_BSU_ID As String, _
    ByVal p_Term As String, ByVal p_SBL_ID As Integer, ByVal p_FSM_Collection_SCH_ID As Integer, _
    ByVal p_DATE As String, ByVal p_str_conn As String, Optional ByVal ChqCount As Integer = 0) As DataTable
        Dim pParms(7) As SqlClient.SqlParameter
        'EXEC	@return_value = [TRANSPORT].[GETFEEPAYABLE_Detail]
        '@ACD_ID = 79,    
        '@BSU_ID = N'125016',
        '@Term = N'41|42',
        '@SBL_ID = 1
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(0).Value = p_ACD_ID
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@Term", SqlDbType.VarChar, 200)
        pParms(2).Value = p_Term
        pParms(3) = New SqlClient.SqlParameter("@SBL_ID", SqlDbType.Int)
        pParms(3).Value = p_SBL_ID
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@FSM_Collection_SCH_ID", SqlDbType.Int)
        pParms(5).Value = p_FSM_Collection_SCH_ID
        pParms(6) = New SqlClient.SqlParameter("@DATE", SqlDbType.DateTime)
        pParms(6).Value = p_DATE
        pParms(7) = New SqlClient.SqlParameter("@ChqueCount", SqlDbType.Int)
        pParms(7).Value = ChqCount
        Dim DS As DataSet
        DS = SqlHelper.ExecuteDataset(p_str_conn, CommandType.StoredProcedure, "TRANSPORT.GETFEEPAYABLE_Detail_Next", pParms)
        Return DS.Tables(0)
    End Function

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim x As Integer = 0
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim imgBank1 As New ImageButton, imgDBankAct As New ImageButton
            Dim txtDBank As New TextBox
            Dim h_Bank As New HiddenField
            Dim hf_DBankAct As New HiddenField, hf_DBNK_TYPE As New HiddenField
            Dim txtChqno As New TextBox
            Dim txtChequeTotal As New TextBox
            Dim txtDBankAct As New TextBox
            imgDBankAct = e.Row.FindControl("imgDBankAct")
            imgBank1 = e.Row.FindControl("imgBank1")
            txtDBank = e.Row.FindControl("txtDBank")
            h_Bank = e.Row.FindControl("h_Bank")
            txtChqno = e.Row.FindControl("txtChqno")
            txtDBankAct = e.Row.FindControl("txtDBankAct")
            hf_DBankAct = e.Row.FindControl("hf_DBankAct")
            hf_DBNK_TYPE = e.Row.FindControl("hf_DBNK_TYPE")
            imgBank1.Attributes.Add("onclick", "  getBankOrEmirate('1','" & txtDBank.ClientID & "','" & h_Bank.ClientID & "','" & hf_DBNK_TYPE.ClientID & "' );return false;")
            imgDBankAct.Attributes.Add("onclick", " getBankAct('1','" & txtDBankAct.ClientID & "','" & hf_DBankAct.ClientID & "' );return false;")
            If h_BankId.Value.Trim <> "" And txtBank.Text.Trim <> "" Then
                txtDBank.Text = txtBank.Text
                h_Bank.Value = h_BankId.Value
            End If
            If hfBankAct1.Value.Trim <> "" And txtBankAct.Text.Trim <> "" Then
                txtDBankAct.Text = txtBankAct.Text
                hf_DBankAct.Value = hfBankAct1.Value
                hf_DBNK_TYPE.Value = hf_BNK_TYPE.Value
            Else
                If x = 0 Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ShowGridColumn", "ShowGridColumn(false);", True)
                End If

            End If
            If IsNumeric(txtChequeNo.Text) Then
                txtChqno.Text = Convert.ToInt64(txtChequeNo.Text.ToString()) + x
                x = x + 1
            End If
        End If
    End Sub

    Protected Sub txtBank_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBank.TextChanged
        Dim str_bankid As String = ""
        Dim str_bankho As String = ""
        txtBank.Text = FeeCommon.GetBankName(txtBank.Text, str_bankid, str_bankho)
        h_BankId.Value = str_bankid
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If txtBank.Text.Trim <> "" Then
            smScriptManager.SetFocus(ddlEmirateMain)
            If str_bankho <> "" Then
                ddlEmirateMain.SelectedIndex = -1
                ddlEmirateMain.Items.FindByValue(str_bankho).Selected = True
            End If
        Else
            h_BankId.Value = ""
            smScriptManager.SetFocus(txtBank)
            lblBankInvalid.Text = "Invalid Bank Selected"
        End If
    End Sub

    Protected Sub txtDBank_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtAmount As New TextBox
        txtAmount = sender
        txtAmount.Attributes.Add("readonly", "readonly")
    End Sub

    Public Shared Function getFeeTypes(ByVal BSU_ID As String, Optional ByVal ACD_ID As Integer = 0) As DataTable
        Dim pParms(2) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@SVB_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID

        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(1).Value = ACD_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "[FEES].[FEE_GETFEESFORCOLLECTION]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Protected Sub txtAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmount.TextChanged
        CalculateVAT(ddlFeeType.SelectedValue, txtAmount.Text, Format(CDate(txtFrom.Text), "dd/MMM/yyyy"))
    End Sub
    Private Sub CalculateVAT(ByVal FEEID As Integer, ByVal Amount As Double, ByVal txDate As String)

        If rblVAT.SelectedValue <> "" Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXCodeAndAmount_EXT('FEES','" & Session("sBsuId") & "','FEE','" & FEEID & "','" & txDate & "'," & Amount & ",''," & IIf(rblVAT.SelectedValue = 1, 1, 0) & " )")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                txtAmount.Text = ds.Tables(0).Rows(0)("INV_AMOUNT").ToString
                txtVATAmount.Text = ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString
                txtAmountAdd.Text = ds.Tables(0).Rows(0)("NET_AMOUNT").ToString
            End If
        Else
            If txtAmountAdd.Text <> "" Or txtAmount.Text <> "" Then
                'lblError.Text = "Please Select Including or Excluding TAX....!"
                usrMessageBar.ShowNotification("Please Select Including or Excluding TAX....!", UserControls_usrMessageBar.WarningType.Danger)
            End If
        End If
    End Sub
    Protected Sub chkVAT_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblVAT.SelectedIndexChanged
        txtAmount.Text = "0.0"
        txtVATAmount.Text = "0.0"
        txtAmountAdd.Text = "0.0"
        'CalculateVAT(ddlFeeType.SelectedValue, txtAmount.Text, txtFrom.text)
    End Sub
    Protected Sub ddlFeeType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFeeType.SelectedIndexChanged
        If txtAmount.Text = "" Then
            CalculateVAT(ddlFeeType.SelectedValue, 0, Format(CDate(txtFrom.Text), "dd/MMM/yyyy"))
        Else
            CalculateVAT(ddlFeeType.SelectedValue, LTrim(RTrim(txtAmount.Text)), Format(CDate(txtFrom.Text), "dd/MMM/yyyy"))
        End If
    End Sub
    Protected Sub lbtnAddMore_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtnAddMore.Click
        If tr_AddFee.Visible Then
            tr_AddFee.Visible = False
            tr_AddFee1.Visible = False
        Else
            tr_AddFee.Visible = True
            tr_AddFee1.Visible = True
        End If
    End Sub
    Private Sub SET_COOKIE()
        Dim OasisSettingsCookie As HttpCookie
        If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
            OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
        Else
            OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
        End If
        OasisSettingsCookie.Values("TPTBSUID") = ddlBusinessunit.SelectedValue
        OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
        Response.Cookies.Add(OasisSettingsCookie)
    End Sub
    Private Sub GET_COOKIE()
        If Not Request.Cookies("OASIS_SETTINGS") Is Nothing Then
            ddlBusinessunit.SelectedValue = Request.Cookies("OASIS_SETTINGS")("TPTBSUID")
        End If
    End Sub

    Protected Sub uscStudentPicker_StudentCleared(sender As Object, e As EventArgs) Handles uscStudentPicker.StudentCleared
        clear_All(False)
        FillACD()

        ddMachineList.DataBind()
        If Not Session("sDEFAULTCCMACHINE") Is Nothing Then
            Dim httab As Hashtable = Session("sDEFAULTCCMACHINE")
            ddMachineList.SelectedValue = httab(ddlBusinessunit.SelectedValue)
        End If
        BindEmirate()
        SetTermMonth()
        SET_COOKIE()
        uscStudentPicker.STU_BSU_ID = ddlBusinessunit.SelectedValue
        txtLocation.Text = ""
        H_Location.Value = ""
        h_Student_no.Value = "0"
    End Sub

    Protected Sub uscStudentPicker_StudentNoChanged(sender As Object, e As EventArgs) Handles uscStudentPicker.StudentNoChanged
        BindFeeType()
        PopulateMonths()
        Gridbind_Feedetails()
        h_Student_no.Value = uscStudentPicker.STU_ID
        SetNarration()
        'Dim objcls As New clsRewards
        'objcls.FETCH_REWARDS_INFO(uscStudentPicker.STU_ID)
        'ViewState("PERSISTENT_ID") = uscStudentPicker.REWARDS_CUSTOMER_ID.ToString 'objcls.PERSISTENT_ID
        btnRedeem.Attributes.Add("onClick", "Popup('popRewardsInfo.aspx?id=" & Encr_decrData.Encrypt(uscStudentPicker.STU_ID) & "&MOD=" & Encr_decrData.Encrypt("TRANSPORT") & "');")
    End Sub

    Private Function SAVE_COLLECTION_INITIATED(ByRef stTrans As SqlTransaction, ByVal FCL_ID As Long) As Integer
        SAVE_COLLECTION_INITIATED = 1000
        Dim PRO_DATE As String = Format(DateTime.Now, OASISConstants.DataBaseDateFormat)
        Dim NEW_PRO_ID As Long = 0

        Try
            Dim PRO_PRO_ID As Int64 = 0
            If FeeCollection.GetDoubleVal(txtRewardsAmount.Text) > 0 Then
                Dim objclsrewards As New clsRewardsTransport
                objclsrewards.PRO_ID = 0
                objclsrewards.PRO_PRO_ID = PRO_PRO_ID
                objclsrewards.PRO_BSU_ID = Session("sBsuId")
                objclsrewards.PRO_ACD_ID = uscStudentPicker.STU_ACD_ID
                objclsrewards.PRO_DATE = Format(DateTime.Now.Date, OASISConstants.DateFormat)
                objclsrewards.PRO_SOURCE = "COUNTER"
                objclsrewards.PRO_STU_TYPE = "S"
                objclsrewards.PRO_STU_ID = uscStudentPicker.STU_ID
                objclsrewards.PRO_DRCR = "CR"
                objclsrewards.PRO_AMOUNT = FeeCollection.GetDoubleVal(txtRewardsAmount.Text)
                objclsrewards.PRO_NARRATION = "GEMS Rewards Points redemption for " & uscStudentPicker.STU_NO
                objclsrewards.PRO_STATUS = "INITIATED"
                objclsrewards.PRO_LOG_USERNAME = Session("sUsr_name")
                objclsrewards.PRO_API_DATA_REDEEMABLE_POINTS = 0
                objclsrewards.PRO_API_DATA_AVAILABLE_POINTS = 0
                objclsrewards.PRO_API_DATA_REDEEMABLE_AMOUNT = 0
                objclsrewards.PERSISTENT_ID = uscStudentPicker.REWARDS_CUSTOMER_ID.ToString
                objclsrewards.API_CMD = ""
                objclsrewards.PRO_STU_BSU_ID = uscStudentPicker.STU_BSU_ID
                objclsrewards.PRO_SBL_ID = H_Location.Value
                If objclsrewards.SAVE_POINTS_REDEMPTION_ONLINE(stTrans) Then
                    Return REDEEM_POINTS_CALL(objclsrewards.PRO_ID, objclsrewards.PRO_AMOUNT, stTrans, FCL_ID)
                End If
            End If
        Catch ex As Exception
            stTrans.Rollback()
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        End Try
    End Function
    Private Function REDEEM_POINTS_CALL(ByVal GEMSTransactionID As Long, ByVal RedeemingAmount As Double, ByRef stTrans As SqlTransaction, ByVal FCL_ID As Long) As Integer
        REDEEM_POINTS_CALL = 1
        Dim ACL_ID As Long = 0, GENERATED_TOKEN As String = ""
        If uscStudentPicker.STU_ID > 0 Then
            Dim objcls As New clsRewardsTransport
            objcls.FETCH_REWARDS_INFO(uscStudentPicker.STU_ID)

            If objcls.CUSTOMER_ID.Trim <> "" And objcls.CUSTOMER_ID.Trim <> "0" Then
                Try
                    If objcls.GENERATE_ACCESS_TOKEN(objcls, GENERATED_TOKEN) Then '-------------generate temperory token from API to access main APIs
                        objcls.GET_API_CALL_PARAMETERS("BURNPOINTS") 'api to burn points
                        Dim postString As String = String.Format("customer_id={0}&type={1}&activity={2}&description={3}&burn_points={4}", objcls.CUSTOMER_ID, "parent", "4", "burning points with refId." & FCL_ID.ToString & "", (RedeemingAmount * 10))
                        Const ContType As String = "application/x-www-form-urlencoded"
                        Dim str = clsRewards.SAVE_ENTERTAINER_API_CALL_LOG(ACL_ID, Session("sBsuid"), DateTime.Now, "BURNPOINTS", objcls.API_URI, objcls.API_METHOD, objcls.CUSTOMER_ID, Session("username"))

                        Dim webRequest As HttpWebRequest = TryCast(Net.WebRequest.Create(objcls.API_URI), HttpWebRequest)
                        webRequest.Method = objcls.API_METHOD
                        webRequest.ContentType = ContType
                        webRequest.ContentLength = postString.Length
                        webRequest.Headers.Add("TP_APPLICATION_KEY", objcls.API_TOKEN)
                        webRequest.Headers.Add("CC_TOKEN", GENERATED_TOKEN)
                        Dim requestWriter As New StreamWriter(webRequest.GetRequestStream())
                        requestWriter.Write(postString)
                        requestWriter.Close()
                        Dim responseReader As StreamReader
                        responseReader = Nothing
                        responseReader = New StreamReader(webRequest.GetResponse().GetResponseStream())
                        Dim responseData As String = responseReader.ReadToEnd()


                        'Dim client = New RestClient(New Uri(objcls.API_URI).AbsoluteUri)
                        'Dim APIRequest = New RestRequest(clsRewards.GetAPIMethod(objcls.API_METHOD))
                        'APIRequest.AddHeader("TP_APPLICATION_KEY", objcls.API_TOKEN)
                        'APIRequest.AddHeader("CC_TOKEN", GENERATED_TOKEN)
                        'APIRequest.AddHeader("Accept", "*/*")
                        'APIRequest.AddHeader("Content-Type", ContType)
                        'APIRequest.AddHeader("Content-Length", postString.Length)
                        'APIRequest.AddParameter("undefined", postString, ParameterType.RequestBody)
                        'APIRequest.AddHeader("Accept-Encoding", "gzip, deflate")
                        'Dim Response As IRestResponse = client.Execute(APIRequest)
                        'responseData = Response.Content

                        Dim bool = clsRewardsTransport.UPDATE_ENTERTAINER_API_CALL_LOG(ACL_ID, Left(responseData, 2000))
                        Dim jsonBurnResponse = JObject.Parse(responseData)
                        Dim transaction_id As String = "", status_code As String = "", message As String = ""
                        If Not jsonBurnResponse("status") Is Nothing AndAlso jsonBurnResponse("status").ToObject(Of String)().ToLower = "true" Then
                            REDEEM_POINTS_CALL = 0
                            transaction_id = "" : status_code = "" : message = ""
                            If Not jsonBurnResponse("status_code") Is Nothing Then
                                status_code = jsonBurnResponse("status_code").ToObject(Of String)()
                            End If
                            If Not jsonBurnResponse("message") Is Nothing Then
                                message = jsonBurnResponse("message").ToObject(Of String)()
                            End If
                            If Not jsonBurnResponse("values") Is Nothing Then
                                Dim jsonBurnResponse_values = JObject.Parse(jsonBurnResponse("values").ToString())
                                transaction_id = FeeCollection.ValidateValue(jsonBurnResponse_values.Property("transaction_id"))
                            End If
                            UPDATE_COLLECTION_INITIATED(GEMSTransactionID, objcls.API_URI, "true", "200OK", status_code, status_code, message, transaction_id, "success", stTrans, FCL_ID) 'updating the api call response
                            Return SAVE_COLLECTION_SUCCESS(GEMSTransactionID, stTrans)
                        ElseIf Not jsonBurnResponse("status") Is Nothing AndAlso jsonBurnResponse("status").ToObject(Of String)().ToLower = "false" Then
                            transaction_id = "" : status_code = "" : message = ""
                            If Not jsonBurnResponse("message") Is Nothing Then
                                message = jsonBurnResponse("message").ToObject(Of String)()
                                usrMessageBar.ShowNotification("Points redemption failed because of " & message, UserControls_usrMessageBar.WarningType.Danger)
                            ElseIf Not jsonBurnResponse("errors") Is Nothing Then
                                Dim jsonBurnResponse_errors = JObject.Parse(jsonBurnResponse("errors").ToString())
                                Dim propCollection As System.Collections.Generic.IEnumerable(Of JProperty) = jsonBurnResponse_errors.Properties
                                If Not propCollection Is Nothing Then
                                    Dim ERRmSG As String = ""
                                    For Each errItem As JProperty In DirectCast(jsonBurnResponse_errors, JToken)
                                        ERRmSG = ERRmSG & IIf(ERRmSG <> "", "<BR />", "") & FeeCollection.ValidateValue(errItem)
                                    Next
                                    usrMessageBar.ShowNotification("Points redemption failed because of " & ERRmSG, UserControls_usrMessageBar.WarningType.Danger)
                                End If
                            End If
                            UPDATE_COLLECTION_INITIATED(GEMSTransactionID, objcls.API_URI, "false", "200OK", status_code, status_code, message, transaction_id, "failed", stTrans, FCL_ID) 'updating the api call response
                        End If
                    Else
                        '--------------error message returned from generate token
                        usrMessageBar.ShowNotification(GENERATED_TOKEN, UserControls_usrMessageBar.WarningType.Danger)
                    End If
                Catch ex As Exception
                    Errorlog("FeeTranspotCollectionNew.aspx.REDEEM_POINTS_CALL, Error:" & ex.Message, "PHOENIX")
                End Try
            End If

            'If objcls.PERSISTENT_ID > 0 Then
            '    Try
            '        Dim ACL_ID As Long = 0
            '        objcls.GET_API_CALL_PARAMETERS("POINTS_REDEEM") 'function gets the web api call parameters
            '        Dim postString As String = String.Format("persistent_id={0}&token={1}&amount={2}&GEMSTransactionID={3}", objcls.PERSISTENT_ID, objcls.API_TOKEN, RedeemingAmount, GEMSTransactionID)
            '        Const contentType As String = "application/x-www-form-urlencoded"
            '        System.Net.ServicePointManager.Expect100Continue = False

            '        Dim webRequest As HttpWebRequest = TryCast(Net.WebRequest.Create(objcls.API_URI), HttpWebRequest)
            '        webRequest.Method = objcls.API_METHOD
            '        webRequest.Headers.Add(HttpRequestHeader.Authorization, "Basic " & Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(Encr_decrData.Decrypt(objcls.API_USERNAME) & ":" & Encr_decrData.Decrypt(objcls.API_PASSWORD))))
            '        webRequest.ContentType = contentType
            '        webRequest.ContentLength = postString.Length

            '        'Function to log the api call
            '        Dim str = clsRewardsTransport.SAVE_ENTERTAINER_API_CALL_LOG(ACL_ID, Session("sBsuid"), DateTime.Now, "REDEEM", objcls.API_URI, objcls.API_METHOD, objcls.PERSISTENT_ID, Session("username"))

            '        Dim requestWriter As New StreamWriter(webRequest.GetRequestStream())
            '        requestWriter.Write(postString)
            '        requestWriter.Close()
            '        Dim responseReader As StreamReader
            '        responseReader = Nothing
            '        Try
            '            responseReader = New StreamReader(webRequest.GetResponse().GetResponseStream())
            '            Dim responseData As String = responseReader.ReadToEnd()
            '            Dim bool = clsRewardsTransport.UPDATE_ENTERTAINER_API_CALL_LOG(ACL_ID, Left(responseData, 2000))
            '            Dim json1 = JObject.Parse(responseData)
            '            If Not json1("success") Is Nothing AndAlso json1("success").ToObject(Of String)().ToLower = "true" Then 'if web api return with success=true message
            '                'ShowMessage("Points redeemed successfully for the requested amount", False)
            '                REDEEM_POINTS_CALL = 0
            '                Dim http_response As String = FeeCollection.ValidateValue(json1.Property("http_response"))
            '                Dim code As String = FeeCollection.ValidateValue(json1.Property("code"))
            '                Dim json2 = JObject.Parse(json1("data").ToString())

            '                Dim data_code = FeeCollection.ValidateValue(json2.Property("code"))
            '                Dim data_message = FeeCollection.ValidateValue(json2.Property("message"))
            '                Dim data_transaction_id = FeeCollection.ValidateValue(json2.Property("transaction_id"))

            '                Dim data_status = FeeCollection.ValidateValue(json2.Property("status"))
            '                UPDATE_COLLECTION_INITIATED(GEMSTransactionID, "/web/v1/gems/points_summary", "true", http_response, code, data_code, data_message, data_transaction_id, data_status, stTrans, FCL_ID) 'updating the api call response
            '            ElseIf Not json1("success") Is Nothing AndAlso json1("success").ToObject(Of String)().ToLower = "false" Then
            '                Dim http_response As String = FeeCollection.ValidateValue(json1.Property("http_response"))
            '                Dim code = FeeCollection.ValidateValue(json1.Property("code"))
            '                Dim json2 = JObject.Parse(json1("data").ToString())

            '                Dim data_code = FeeCollection.ValidateValue(json2.Property("code"))
            '                Dim data_message = FeeCollection.ValidateValue(json2.Property("message"))
            '                Dim data_transaction_id = FeeCollection.ValidateValue(json2.Property("transaction_id"))
            '                UPDATE_COLLECTION_INITIATED(GEMSTransactionID, "/web/v1/gems/points_summary", "false", http_response, code, data_code, data_message, data_transaction_id, "", stTrans, FCL_ID)
            '            End If
            '        Catch ex As WebException
            '            Errorlog(ex.Message & "Rewards - Transport PointRedeem - WebApi call", "PHOENIX")
            '            Dim errResp As WebResponse = ex.Response
            '            Using respStream As Stream = errResp.GetResponseStream()
            '                Dim reader As New StreamReader(respStream)
            '                Dim errorMessage As String = reader.ReadToEnd()
            '                If errorMessage <> "" Then
            '                    Dim bool = clsRewardsTransport.UPDATE_ENTERTAINER_API_CALL_LOG(ACL_ID, Left(errorMessage, 2000))
            '                    Dim json1 = JObject.Parse(errorMessage)
            '                    If Not json1("success") Is Nothing AndAlso json1("success").ToObject(Of String)().ToLower = "false" Then 'if web api return with success=false message
            '                        Dim json2 = JObject.Parse(json1("data").ToString())
            '                        Dim data_code = FeeCollection.ValidateValue(json2.Property("code")) 'IIf(json2("code") Is Nothing, "", json2("code").ToObject(Of String)())
            '                        Dim data_message = FeeCollection.ValidateValue(json2.Property("message")) 'IIf(json2("message") Is Nothing, "", json2("message").ToObject(Of String)())
            '                        Select Case data_code
            '                            Case "41" 'Invalid Authentication
            '                                '  ShowMessage(getErrorMessage("341"))
            '                                usrMessageBar.ShowNotification(getErrorMessage("341"), UserControls_usrMessageBar.WarningType.Danger)
            '                            Case "42" 'Invalid token
            '                                '  ShowMessage(getErrorMessage("342"))
            '                                usrMessageBar.ShowNotification(getErrorMessage("342"), UserControls_usrMessageBar.WarningType.Danger)
            '                            Case "43" 'Invalid persistent User ID
            '                                ' ShowMessage(getErrorMessage("343"))
            '                                usrMessageBar.ShowNotification(getErrorMessage("343"), UserControls_usrMessageBar.WarningType.Danger)
            '                            Case "51" 'Invalid Authentication
            '                                ' ShowMessage(getErrorMessage("351"))
            '                                usrMessageBar.ShowNotification(getErrorMessage("351"), UserControls_usrMessageBar.WarningType.Danger)
            '                            Case "52" 'Invalid token
            '                                '  ShowMessage(getErrorMessage("352"))
            '                                usrMessageBar.ShowNotification(getErrorMessage("352"), UserControls_usrMessageBar.WarningType.Danger)
            '                            Case "54" 'Invalid persistent User ID
            '                                ' ShowMessage(getErrorMessage("354"))
            '                                usrMessageBar.ShowNotification(getErrorMessage("354"), UserControls_usrMessageBar.WarningType.Danger)
            '                            Case "55" 'Invalid Authentication
            '                                ' ShowMessage(getErrorMessage("355"))
            '                                usrMessageBar.ShowNotification(getErrorMessage("355"), UserControls_usrMessageBar.WarningType.Danger)
            '                            Case "61" 'Error encountered while processing transaction!
            '                                ' ShowMessage(getErrorMessage("361"))
            '                                usrMessageBar.ShowNotification(getErrorMessage("361"), UserControls_usrMessageBar.WarningType.Danger)
            '                        End Select
            '                        UPDATE_COLLECTION_INITIATED(GEMSTransactionID, "/web/v1/gems/points_summary", "false", "", "", data_code, data_message, GEMSTransactionID, "", stTrans, FCL_ID)
            '                        Errorlog(data_code & "- " & data_message & " - /web/v1/gems/points_summary", "PHOENIX")
            '                    End If
            '                End If
            '            End Using
            '        Finally
            '            If Not responseReader Is Nothing Then
            '                responseReader.Close()
            '                webRequest.GetResponse().Close()
            '            End If
            '        End Try
            '    Catch ex As Exception
            '        ' ShowMessage(getErrorMessage(1000), True)
            '        usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
            '        Errorlog(ex.Message)
            '    End Try

            'Else 'If persistent id not found
            '    'ShowMessage(getErrorMessage("343"))
            '    usrMessageBar.ShowNotification(getErrorMessage("343"), UserControls_usrMessageBar.WarningType.Danger)
            'End If
        Else
            'ShowMessage("Your session expired! Unable to fetch the GEMS Rewards data.")
            usrMessageBar.ShowNotification("Your session expired! Unable to fetch the GEMS Rewards data.", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Function
    'Sub ShowMessage(ByVal Message As String, Optional ByVal bError As Boolean = True)
    '    'usrMessageBar.ShowNotification(Message, IIf(bError = True, UserControls_usrMessageBar.WarningType.Danger, UserControls_usrMessageBar.WarningType.Success)) '.ShowMessage(Message, bError)
    '    usrMessageBar.ShowMessage(Message, bError)

    'End Sub
    Private Sub UPDATE_COLLECTION_INITIATED(ByVal PRO_ID As Long, ByVal API_CMD As String, ByVal API_SUCCESS As String, ByVal API_HTTP_RESPONSE As String, _
                                          ByVal API_CODE As String, ByVal API_DATA_CODE As String, ByVal API_DATA_MESSAGE As String, _
                                          ByVal API_DATA_TRANSACTION_ID As String, ByVal API_DATA_STATUS As String, ByRef stTrans As SqlTransaction, _
                                          ByVal FCL_ID As Long, _
                                          Optional ByVal bPointUpdate As Boolean = False, Optional ByVal REDEEMABLE_AMOUNT As Double = 0, _
                                          Optional ByVal REDEEMABLE_POINTS As Double = 0, Optional ByVal AVAILABLE_POINTS As Double = 0)
        Dim objclsrewards As New clsRewardsTransport
        With objclsrewards
            .PRO_ID = PRO_ID
            .API_CMD = API_CMD
            .PRO_API_SUCCESS = API_SUCCESS
            .PRO_API_HTTP_RESPONSE = API_HTTP_RESPONSE
            .PRO_API_CODE = API_CODE
            .PRO_API_DATA_CODE = API_DATA_CODE
            .PRO_API_DATA_MESSAGE = API_DATA_MESSAGE
            .PRO_API_DATA_TRANSACTION_ID = API_DATA_TRANSACTION_ID
            .PRO_API_DATA_STATUS = API_DATA_STATUS
            .bUpdate_Points = bPointUpdate
            .PRO_API_DATA_REDEEMABLE_AMOUNT = REDEEMABLE_AMOUNT
            .PRO_API_DATA_REDEEMABLE_POINTS = REDEEMABLE_POINTS
            .PRO_API_DATA_AVAILABLE_POINTS = AVAILABLE_POINTS
            .PRO_FCL_ID = FCL_ID
            .UPDATE_POINTS_REDEMPTION_ONLINE(stTrans) 'function to update the web api call response fields
        End With
    End Sub
    Private Function SAVE_COLLECTION_SUCCESS(ByVal PRO_ID As Long, ByRef stTrans As SqlTransaction) As Integer
        SAVE_COLLECTION_SUCCESS = 1
        Dim StuNos As String = ""

        Try
            Dim objcls As New clsRewardsTransport
            objcls.PRO_ID = PRO_ID
            If objcls.SAVE_FEECOLLECTION_H(stTrans) Then
                ' ShowMessage(getErrorMessage(360), False)
                usrMessageBar.ShowNotification(getErrorMessage(363), UserControls_usrMessageBar.WarningType.Success)
                SAVE_COLLECTION_SUCCESS = 0
                'Gridbind_Feedetails()
                'GET_POINTS_SUMMARY_CALL()
                'updating latest points after successfull points redemption
                'UPDATE_COLLECTION_INITIATED(PRO_ID, "", "", "", "", "", "", "", "", True, FeeCollection.GetDoubleVal(lblRedeemableAmt.Text), FeeCollection.GetDoubleVal(lblRedeemablePoints.Text), FeeCollection.GetDoubleVal(lblAvailablePoints.Text))
                'Me.testpopup.Style.Item("display") = "none"
                'Me.btnRedeem.Visible = True
                'bPaymentConfirmMsgShown = False
                'Redirecting the page to result page with receipt details
                'Response.Redirect("RewardsRedemptionResult.aspx?ID=" & Encr_decrData.Encrypt(PRO_ID))
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            ' ShowMessage(getErrorMessage(359), True)
            usrMessageBar.ShowNotification(getErrorMessage(359), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        End Try
    End Function
End Class
