﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="Emp_TuitionFeeConcessionFinList.aspx.vb" Inherits="Fees_Emp_TuitionFeeConcessionFinList" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css?1=2" rel="stylesheet" />
    <script type="text/javascript" language="javascript">


        function isIE() {
            ua = navigator.userAgent;
            /* MSIE used to detect old browsers and Trident used to newer ones*/
            var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

            return is_ie;
        }

        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
        function CloseFrame() {
            jQuery.fancybox.close();
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Employee Tuition Fee Concession List
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tblAddLedger" runat="server" width="100%">
                    <tr valign="bottom">
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>


                <table runat="server" width="100%">
                    <tr valign="bottom">
                        <td align="right">
                            <asp:RadioButton ID="rad1" runat="server" GroupName="Rad" Text="All" AutoPostBack="True" Visible="True" OnCheckedChanged="gridbind" />

                            <asp:RadioButton ID="rad2" runat="server" GroupName="Rad" Text="Approved" AutoPostBack="True" Visible="True" OnCheckedChanged="gridbind" />

                            <asp:RadioButton ID="rad3" runat="server" GroupName="Rad" Text="Declined" AutoPostBack="True" Visible="True" OnCheckedChanged="gridbind" />

                            <asp:RadioButton ID="rad4" runat="server"  Checked="true" GroupName="Rad" Text="Pending" AutoPostBack="True" Visible="True" OnCheckedChanged="gridbind" />

                        </td>
                    </tr>
                    <tr>
                        <td align="left">

                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found"
                                Width="100%" AllowPaging="True" PageSize="25" class="table table-row table-bordered">
                                <Columns>

                                    <asp:TemplateField HeaderText="ID" Visible="false">
                                        <HeaderTemplate>
                                            ID<br />
                                            <asp:TextBox ID="txtEmployeeId" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnEMPIDSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../../Images/forum_search.gif"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmployeeId" runat="server" Text='<%# Bind("EMP_ID")%>'></asp:Label>

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee ID">
                                        <HeaderTemplate>
                                            Employee No<br />
                                            <asp:TextBox ID="txtEmployeeNo" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnEMPNOSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmployeeNo" runat="server" Text='<%# Bind("EMPNO")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <HeaderTemplate>
                                            Employee Name<br />
                                            <asp:TextBox ID="txtEmployeeName" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnEMPNAMESearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmployeeName" runat="server" Text='<%# Bind("EMPLOYEE_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Group Join Date<br />
                                            <asp:TextBox ID="txtDate" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../../Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>


                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("GRP_JOIN_DATE", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Designation">
                                        <HeaderTemplate>
                                            Designation<br />
                                            <asp:TextBox ID="txtDesignation" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDESIGSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />


                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Bind("DESIGNATION")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Department">
                                        <HeaderTemplate>
                                            Department<br />
                                            <asp:TextBox ID="txtDepartment" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDEPTSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Bind("DPT_DESCR")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mobile Number">
                                        <HeaderTemplate>
                                            Mobile Number<br />
                                            <asp:TextBox ID="txtMobileNumber" runat="server" Width="70%"></asp:TextBox>
                                            <asp:ImageButton ID="btnMOBILESearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMobileNumber" runat="server" Text='<%# Bind("MOBILE_NUMBER")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="View">
                                        <HeaderTemplate>
                                            View
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <asp:LinkButton ID="hlview" runat="server" OnClick="hlview_Click">View</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>



                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_selected_menu_3" runat="server" />
                <asp:HiddenField ID="h_selected_menu_4" runat="server" />
                <asp:HiddenField ID="h_selected_menu_5" runat="server" />
                <asp:HiddenField ID="h_selected_menu_6" runat="server" />
                <asp:HiddenField ID="h_selected_menu_7" runat="server" />
                <asp:HiddenField ID="h_selected_menu_8" runat="server" />
            </div>
        </div>
    </div>



</asp:Content>
