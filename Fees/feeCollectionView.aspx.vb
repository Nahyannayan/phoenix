Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Threading
Imports System.Threading.Tasks
'ts
Partial Class Fees_feeCollectionView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'as
    Dim sbReceiptNo As String
    Public Property ReceiptNos() As String
        Get
            Return sbReceiptNo
        End Get
        Set(ByVal value As String)
            sbReceiptNo = value
        End Set
    End Property
    Dim sbSelectAll As Boolean
    Public Property bSelectAll() As Boolean
        Get
            Return sbSelectAll
        End Get
        Set(ByVal value As Boolean)
            sbSelectAll = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "feeCollection.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
            'gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+")))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+")))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_COLLECTION Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_FEE_COLLECTION
                            lblHead.Text = "Fee Collection"
                            ViewState("trantype") = "A"
                    End Select
                    ddlAcademicYear.Items.Clear()
                    FillACD()
                    gridbind()
                    hfTaxable.Value = IIf(DirectCast(Session("BSU_bGSTEnabled"), Boolean) = True, "1", "0")
                    btnOkay.Attributes.Add("onclick", "this.disabled=true;this.value='Wait..';" + ClientScript.GetPostBackEventReference(btnOkay, "").ToString())
                    btnEmailUnSent.Attributes.Add("onclick", "this.disabled=true;this.value='Wait..';" + ClientScript.GetPostBackEventReference(btnEmailUnSent, "").ToString())
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        Else
            h_print.Value = ""
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = "", FCL_bDeleted As String = "", FCL_ACD_ID As Integer = 0

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7, lstrCondn8 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String, FCL_bEMAILED As String
            Dim txtSearch As New TextBox
            Dim CurBsUnit As String = Session("sBsuid")
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            lstrCondn8 = ""
            str_Filter = ""
            FCL_ACD_ID = ddlAcademicYear.SelectedValue
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtReceiptno FCL_RECNO
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
                lstrCondn1 = Trim(Mainclass.cleanString(txtSearch.Text))
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCL_RECNO", lstrCondn1)
                '   -- 8   txtSchoolcode FCL_RECNO
                larrSearchOpr = h_selected_menu_8.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtSchoolcode")
                lstrCondn8 = Trim(Mainclass.cleanString(txtSearch.Text))
                If (lstrCondn8 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BSU_SHORTNAME", lstrCondn8)

                '   -- 2  txtDate
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(Mainclass.cleanString(txtSearch.Text))
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCL_DATE", lstrCondn2)

                '   -- 3  txtGrade
                larrSearchOpr = h_selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
                lstrCondn3 = Mainclass.cleanString(txtSearch.Text)
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRD_DISPLAY", lstrCondn3)

                '   -- 4   txtStuno
                larrSearchOpr = h_selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
                lstrCondn4 = Mainclass.cleanString(txtSearch.Text)
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn4)

                '   -- 5  txtStuname
                larrSearchOpr = h_selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
                lstrCondn5 = Mainclass.cleanString(txtSearch.Text)
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn5)

                '   -- 6  txtAmount
                larrSearchOpr = h_selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
                lstrCondn6 = Mainclass.cleanString(txtSearch.Text)
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCL_AMOUNT", lstrCondn6)

                '   -- 7  txtDesc
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
                lstrCondn7 = Mainclass.cleanString(txtSearch.Text)
                If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCL_NARRATION", lstrCondn7)
            End If
            Dim STR_RECEIPT As String = "FEES.VW_FEES_RECEIPT_LIST"
            If rbEnquiry.Checked Then
                STR_RECEIPT = "FEES.VW_OSO_FEES_RECEIPT_ENQUIRY"
            End If
            If rdDeleted.Checked Then
                FCL_bDeleted = " ISNULL(FCL_bDELETED, 0) = 1 AND "
            Else
                FCL_bDeleted = " ISNULL(FCL_bDELETED, 0) = 0 AND "
            End If
            Dim str_topFilter As String = ""
            If ddlTopFilter.SelectedItem.Value <> "All" Then
                str_topFilter = " top " & ddlTopFilter.SelectedItem.Value
            End If

            If ddlEmailStatus.SelectedValue = "NE" Then 'Not Emailed
                FCL_bEMAILED = "0"
            ElseIf ddlEmailStatus.SelectedValue = "E" Then
                FCL_bEMAILED = "1"
            Else
                FCL_bEMAILED = "FCL_bEMAILED"
            End If

            str_Sql = "SELECT " & str_topFilter & " FCL_ID, CAST(0 AS BIT) AS CHECKED, FCL_BSU_ID, FCL_PAYMENT_BSU_ID, " _
                & "BSU_SHORTNAME,FCL_RECNO,ISNULL(GRD_DISPLAY, GRM_DESCR)GRD_DISPLAY,FCL_bEMAILED,FCL_DATE,STU_NO,STU_NAME, " _
                & "FCL_AMOUNT, FCL_NARRATION, BSU_bBSU_Version_Enabled, BSU_VER_NO FROM " & STR_RECEIPT & " WHERE " & FCL_bDeleted & " (FCL_BSU_ID='" & CurBsUnit & "' OR FCL_PAYMENT_BSU_ID='" & CurBsUnit & "')" _
            & " AND (FCL_ACD_ID=" & FCL_ACD_ID & " OR FCL_BSU_ID<>FCL_PAYMENT_BSU_ID) AND FCL_bEMAILED=" & FCL_bEMAILED & " " & str_Filter & " ORDER BY FCL_DATE DESC ,FCL_RECNO DESC"


            Dim cmd As New SqlCommand
            Dim adap As New SqlDataAdapter
            cmd.Connection = ConnectionManger.GetOASIS_FEESConnection
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 0
            cmd.CommandText = str_Sql
            adap.SelectCommand = cmd
            adap.Fill(ds)


            'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ds.Tables(0).Rows(0)("CHECKED") = False
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                BindGrid(ds.Tables(0))
            End If

            txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn5

            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = lstrCondn6

            txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
            txtSearch.Text = lstrCondn7

            txtSearch = gvJournal.HeaderRow.FindControl("txtSchoolcode")
            txtSearch.Text = lstrCondn8

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid(ByVal dt As DataTable)
        gvJournal.DataSource = dt
        gvJournal.DataBind()
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbVoucher_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        h_print.Value = ""
        Try
            Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
            'Session("ReportSource") = FeeCollection.PrintReceipt(lblReceipt.Text, Session("sBsuid"), rbEnquiry.Checked, Session("sUsr_name"), False)

            'SetVersionforReceipt(lblReceipt.Text)

            h_print.Value = "?type=REC&id=" + Encr_decrData.Encrypt(lblReceipt.Text) & _
            "&bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) & _
            "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & _
            "&isenq=" & Encr_decrData.Encrypt(rbEnquiry.Checked) & _
            "&iscolln=" & Encr_decrData.Encrypt(False) & "&isexport=0"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub SetVersionforReceipt(ByVal RecNo As String, ByVal pBsuId As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FCL_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = pBsuId
        pParms(1) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 20)
        pParms(1).Value = RecNo
        pParms(2) = New SqlClient.SqlParameter("@bENQUIRY", SqlDbType.Bit)
        pParms(2).Value = rbEnquiry.Checked  'Not used
        pParms(3) = New SqlClient.SqlParameter("@bCOLLECTION", SqlDbType.Bit)
        pParms(3).Value = 0
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.GetFeeReceipt", pParms)

        Dim BSU_CURRENCY As String = ""
        Dim BSU_bBSU_Version_Enabled As Boolean
        BSU_bBSU_Version_Enabled = False
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Columns.Contains("BSU_bBSU_Version_Enabled") Then
                BSU_bBSU_Version_Enabled = ds.Tables(0).Rows(0)("BSU_bBSU_Version_Enabled")
                If BSU_bBSU_Version_Enabled Then
                    Dim BSU_LOGO_VER_NO As String, BSU_VER_NO As String
                    BSU_LOGO_VER_NO = "0"
                    BSU_VER_NO = "0"
                    BSU_LOGO_VER_NO = CInt(ds.Tables(0).Rows(0)("BSU_LOGO_VER_NO").ToString)
                    BSU_VER_NO = CInt(ds.Tables(0).Rows(0)("BSU_VER_NO").ToString)
                    Dim sqlParam(1) As SqlParameter
                    sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", pBsuId, SqlDbType.VarChar)
                    sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_VER_NO", BSU_VER_NO, SqlDbType.Int)
                    Dim dtBSUDetail As New DataTable
                    dtBSUDetail = Mainclass.getDataTable("SP_GET_BUSINESSUNIT_DETAILS", sqlParam, ConnectionManger.GetOASISConnectionString)
                    If dtBSUDetail.Rows(0)("BUS_bFEE_TAXABLE").ToString() = "True" Then
                        hfTaxable.Value = "1"
                    Else
                        hfTaxable.Value = "0"
                    End If
                End If
            End If
        End If
    End Sub
    Private Sub SetVersion(ByVal VerNo As String, ByVal pBsuId As String)
        Dim sqlParam(1) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", pBsuId, SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_VER_NO", VerNo, SqlDbType.Int)
        Dim dtBSUDetail As New DataTable
        dtBSUDetail = Mainclass.getDataTable("SP_GET_BUSINESSUNIT_DETAILS", sqlParam, ConnectionManger.GetOASISConnectionString)
        If dtBSUDetail.Rows(0)("BUS_bFEE_TAXABLE").ToString() = "True" Then
            hfTaxable.Value = "1"
        Else
            hfTaxable.Value = "0"
        End If
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub rbEnrollment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnrollment.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnquiry.CheckedChanged
        gridbind()
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Protected Sub lbExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
            'Session("ReportSource") = FeeCollection.PrintReceipt(lblReceipt.Text, Session("sBsuid"), rbEnquiry.Checked, Session("sUsr_name"), False)
            SetVersionforReceipt(lblReceipt.Text, Session("sbsuid"))
            h_print.Value = "?type=REC&id=" + Encr_decrData.Encrypt(lblReceipt.Text) & _
              "&bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) & _
              "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & _
              "&isenq=" & Encr_decrData.Encrypt(rbEnquiry.Checked) & _
              "&iscolln=" & Encr_decrData.Encrypt(False) & "&isexport=1"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        h_print.Value = ""
        gridbind()
    End Sub

    Protected Sub rdDeleted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdDeleted.CheckedChanged
        h_print.Value = ""
        gridbind()
    End Sub

    Protected Sub ddlCount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTopFilter.SelectedIndexChanged
        h_print.Value = ""
        gridbind()
    End Sub

    Protected Sub lbtnEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Dim RetMessage As String
            'Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
            'Dim hf_FCLID As HiddenField = sender.Parent.parent.findcontrol("hf_FCLID")
            'RetMessage = DownloadEmailReceipt.DownloadOrEMailReceipt("EMAIL", lblReceipt.Text, Session("sBsuId"), True)
            'FeeCollection.SAVE_EMAIL_LOG(Session("sBsuId"), Session("sUsr_name"), lblReceipt.Text, hf_FCLID.Value, RetMessage)
            'lblError.Text = RetMessage
            'usrMessageBar.ShowNotification(RetMessage, UserControls_usrMessageBar.WarningType.Danger)
        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub btnEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmail.Click
        Try
            'Dim dt As DataTable = DirectCast(ViewState("gvJournal"), DataTable)
            Dim cntSelected As Integer = 0
            Dim dt As New DataTable
            dt.Columns.Add("REC_NO", GetType([String]))
            dt.Columns.Add("STU_NO", GetType([String]))
            dt.Columns.Add("STU_NAME", GetType([String]))
            dt.Columns.Add("STU_GRADE", GetType([String]))
            dt.Columns.Add("FCL_AMOUNT", GetType(Decimal))
            dt.AcceptChanges()
            Dim dr As DataRow '= dt.NewRow
            For Each gvr As GridViewRow In gvJournal.Rows
                Dim chkselect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
                If chkselect.Checked Then
                    '        Dim RetMessage As String
                    Dim lblReceipt As Label = DirectCast(gvr.FindControl("lblReceipt"), Label)
                    Dim lblStuNo As Label = DirectCast(gvr.FindControl("lblStuNo"), Label)
                    Dim lblGrade As Label = DirectCast(gvr.FindControl("lblGrade"), Label)
                    Dim lblStuName As Label = DirectCast(gvr.FindControl("lblStuName"), Label)
                    Dim lblAmount As Label = DirectCast(gvr.FindControl("lblAmount"), Label)
                    Dim hf_Emailed As HiddenField = DirectCast(gvr.FindControl("hf_Emailed"), HiddenField)
                    If hf_Emailed.Value = "True" Then
                        dr = dt.NewRow
                        dr(0) = lblReceipt.Text
                        dr(1) = lblStuNo.Text
                        dr(2) = lblStuName.Text
                        dr(3) = lblGrade.Text
                        dr(4) = lblAmount.Text
                        dt.Rows.Add(dr)
                        dt.AcceptChanges()
                    End If
                    '        RetMessage = DownloadEmailReceipt.DownloadOrEMailReceipt("EMAIL", lblReceipt.Text, Session("sBsuId"), True)
                    '        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.Text, "EXEC FEES.SAVE_EMAIL_RECEIPT_LOG '" & Session("sBsuId") & "','" & Session("sUsr_name") & "','" & lblReceipt.Text & "','" & hf_FCLID.Value & "','" & RetMessage & "' ")
                    '        ReceiptNos &= ("|" & lblReceipt.Text)
                    cntSelected += 1
                End If
            Next
            If dt.Rows.Count > 0 Then
                'Dim gvEmailStatus As GridView = pnlEmail.FindControl("gvEmailStatus")
                Dim lblMessage As Label = pnlEmail.FindControl("lblMessage")
                lblMessage.Text = "Please note, Email has already been sent for the below Receipt No(s).<br />Are you sure you want to send again?"
                ViewState("gvEmailStatus") = dt
                BindStatusGrid()
                If cntSelected <> dt.Rows.Count Then
                    btnEmailUnSent.Visible = True
                Else
                    btnEmailUnSent.Visible = False
                End If
            Else
                lblMessage.Text = "Are you sure you want to send selected Receipt(s) as Email?"
                btnEmailUnSent.Visible = False
            End If
            '
            If cntSelected > 0 Then
                HideButtons(False)
                Me.MPE1.Show()
            Else
                'lblError.Text = "Please select atleast one receipt to send"
                usrMessageBar.ShowNotification("Please select atleast one receipt to send", UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hfEmailed As HiddenField = DirectCast(e.Row.FindControl("hf_Emailed"), HiddenField)
            Dim lbtnEmail As LinkButton = DirectCast(e.Row.FindControl("lbtnEmail"), LinkButton)
            Dim lbVoucher As LinkButton = DirectCast(e.Row.FindControl("lbVoucher"), LinkButton)
            Dim lbExport As LinkButton = DirectCast(e.Row.FindControl("lbExport"), LinkButton)
            Dim lblReceipt As Label = DirectCast(e.Row.FindControl("lblReceipt"), Label)
            If hfEmailed.Value = True Then
                lbtnEmail.Text = "Send as Email Again"
            End If
            Dim StuBsuId As String = gvJournal.DataKeys(e.Row.RowIndex)("FCL_BSU_ID")
            Dim Version_Enabled As Boolean = CBool(gvJournal.DataKeys(e.Row.RowIndex)("BSU_bBSU_Version_Enabled"))
            Dim Version_No As String = gvJournal.DataKeys(e.Row.RowIndex)("BSU_VER_NO")
            Dim ReceiptNo As String = "", receiptPage As String = "FeeReceipt.aspx", receiptUrl As String = ""
            If Not lblReceipt Is Nothing Then
                ReceiptNo = lblReceipt.Text
                'SetVersionforReceipt(ReceiptNo, StuBsuId)
                If Version_Enabled Then
                    SetVersion(Version_No, StuBsuId)
                End If
                receiptPage = IIf(hfTaxable.Value = "1", "FeeReceipt_TAX.aspx", "FeeReceipt.aspx")
                receiptUrl = receiptPage & "?type=REC&id=" + Encr_decrData.Encrypt(ReceiptNo) & _
                "&bsu_id=" & Encr_decrData.Encrypt(StuBsuId) & _
                "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & _
                "&isenq=" & Encr_decrData.Encrypt(rbEnquiry.Checked) & _
                "&iscolln=" & Encr_decrData.Encrypt(False)
            End If
            If Not lbVoucher Is Nothing Then
                receiptUrl &= "&isexport=0"
                lbVoucher.Attributes.Add("onClick", "return ShowSubWindowWithClose('" & receiptUrl & "', '', '60%', '80%');")
            End If
            If Not lbExport Is Nothing Then
                receiptUrl &= "&isexport=1"
                lbExport.Attributes.Add("onClick", "return ShowSubWindowWithClose('" & receiptUrl & "', '', '60%', '80%');")
            End If
        ElseIf e.Row.RowType = DataControlRowType.Header Then
            Dim chkSelectH As CheckBox = DirectCast(e.Row.FindControl("chkSelectH"), CheckBox)
            chkSelectH.Checked = bSelectAll
        End If
    End Sub

    Protected Sub chkSelectH_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSelectH As CheckBox = sender.Parent.parent.findcontrol("chkSelectH")
        Try
            bSelectAll = chkSelectH.Checked
            For Each gvr As GridViewRow In gvJournal.Rows
                If gvr.RowType = DataControlRowType.DataRow Then
                    Dim chkSelect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
                    chkSelect.Checked = bSelectAll
                End If
            Next
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlEmailStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Protected Sub btnOkay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOkay.Click
        Try
            Dim dt As New DataTable
            dt.Columns.Add("REC_NO", GetType([String]))
            dt.Columns.Add("STU_NO", GetType([String]))
            dt.Columns.Add("STU_NAME", GetType([String]))
            dt.Columns.Add("STU_GRADE", GetType([String]))
            dt.Columns.Add("FCL_AMOUNT", GetType(Decimal))
            dt.AcceptChanges()
            Dim cntSelected As Integer = 0
            Dim dr As DataRow '= dt.NewRow
            'Dim gvEmailStatus As GridView = pnlEmail.FindControl("gvEmailStatus")
            'Dim lblMessage As Label = pnlEmail.FindControl("lblMessage")
            lblMessage.Text = "Email Successfuly sent to the Receipts below"
            ViewState("gvEmailStatus") = Nothing
            BindStatusGrid()

            For Each gvr As GridViewRow In gvJournal.Rows
                Dim chkselect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
                If chkselect.Checked Then
                    Dim lblStuNo As Label = DirectCast(gvr.FindControl("lblStuNo"), Label)
                    Dim RetMessage As String
                    Dim lblReceipt As Label = DirectCast(gvr.FindControl("lblReceipt"), Label)
                    Dim hf_FCLID As HiddenField = DirectCast(gvr.FindControl("hf_FCLID"), HiddenField)
                    Dim lblGrade As Label = DirectCast(gvr.FindControl("lblGrade"), Label)
                    Dim lblStuName As Label = DirectCast(gvr.FindControl("lblStuName"), Label)
                    Dim lblAmount As Label = DirectCast(gvr.FindControl("lblAmount"), Label)
                    Dim StuBsuId As String = gvJournal.DataKeys(gvr.RowIndex)("FCL_BSU_ID")
                    Try
                        Dim qry As String = "UPDATE FEES.FEECOLLECTION_H SET FCL_bSCHEDULE_EMAIL = 1,FCL_bEMAILED=0,FCL_bFETCHED=0,FCL_EMAILSTATUS='' WHERE FCL_BSU_ID='" & StuBsuId & "' AND FCL_RECNO = '" & lblReceipt.Text & "'"
                        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry)
                        RetMessage = "Receipt successfully scheduled for emailing"
                    Catch ex As Exception
                        RetMessage = "unable to schedule the receipt for emailing"
                    End Try
                    'RetMessage = DownloadEmailReceipt.DownloadOrEMailReceipt("EMAIL", lblReceipt.Text, StuBsuId, True)
                    'FeeCollection.SAVE_EMAIL_LOG(Session("sBsuId"), Session("sUsr_name"), lblReceipt.Text, hf_FCLID.Value, RetMessage)
                    If RetMessage.Contains("Receipt successfully scheduled") Then
                        'lblMessage.Text &= " | " & lblReceipt.Text
                        dr = dt.NewRow
                        dr(0) = lblReceipt.Text
                        dr(1) = lblStuNo.Text
                        dr(2) = lblStuName.Text
                        dr(3) = lblGrade.Text
                        dr(4) = lblAmount.Text
                        dt.Rows.Add(dr)
                        dt.AcceptChanges()
                    End If
                    cntSelected += 1
                End If
            Next
            If dt.Rows.Count > 0 Then
                ViewState("gvEmailStatus") = dt
                gridbind()
                BindStatusGrid()
                Me.MPE1.Show()
                Me.btnOkay.Visible = False
            Else
                If cntSelected < 1 Then
                    'lblError.Text = "No receipts found."
                    usrMessageBar.ShowNotification("No receipts found.", UserControls_usrMessageBar.WarningType.Danger)
                End If
                'lblError.Text = "Failed to send Email(s)"
                usrMessageBar.ShowNotification("Failed to send Email(s)", UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception

        Finally
            Me.btnOkay.Enabled = True
        End Try
    End Sub

    Sub BindStatusGrid()
        gvEmailStatus.DataSource = ViewState("gvEmailStatus")
        gvEmailStatus.DataBind()
    End Sub

    Protected Sub btnEmailUnSent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailUnSent.Click
        Try
            Dim dt As New DataTable
            dt.Columns.Add("REC_NO", GetType([String]))
            dt.Columns.Add("STU_NO", GetType([String]))
            dt.Columns.Add("STU_NAME", GetType([String]))
            dt.Columns.Add("STU_GRADE", GetType([String]))
            dt.Columns.Add("FCL_AMOUNT", GetType(Decimal))
            dt.AcceptChanges()
            Dim cntSelected As Integer = 0
            Dim dr As DataRow '= dt.NewRow
            'Dim gvEmailStatus As GridView = pnlEmail.FindControl("gvEmailStatus")
            'Dim lblMessage As Label = pnlEmail.FindControl("lblMessage")
            lblMessage.Text = "Email Successfuly sent to the Receipts below."
            ViewState("gvEmailStatus") = dt
            BindStatusGrid()

            For Each gvr As GridViewRow In gvJournal.Rows
                Dim chkselect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
                Dim hf_Emailed As HiddenField = DirectCast(gvr.FindControl("hf_Emailed"), HiddenField)
                If chkselect.Checked AndAlso hf_Emailed.Value = "False" Then
                    Dim lblStuNo As Label = DirectCast(gvr.FindControl("lblStuNo"), Label)
                    Dim RetMessage As String
                    Dim lblReceipt As Label = DirectCast(gvr.FindControl("lblReceipt"), Label)
                    Dim lblGrade As Label = DirectCast(gvr.FindControl("lblGrade"), Label)
                    Dim lblStuName As Label = DirectCast(gvr.FindControl("lblStuName"), Label)
                    Dim lblAmount As Label = DirectCast(gvr.FindControl("lblAmount"), Label)
                    Dim hf_FCLID As HiddenField = DirectCast(gvr.FindControl("hf_FCLID"), HiddenField)
                    Dim StuBsuId As String = gvJournal.DataKeys(gvr.RowIndex)("FCL_BSU_ID")
                    RetMessage = DownloadEmailReceipt.DownloadOrEMailReceipt("EMAIL", lblReceipt.Text, StuBsuId, True)
                    'FeeCollection.SAVE_EMAIL_LOG(Session("sBsuId"), Session("sUsr_name"), lblReceipt.Text, hf_FCLID.Value, RetMessage)
                    If RetMessage.Contains("successfully emailed") Then
                        'lblMessage.Text &= " | " & lblReceipt.Text
                        dr = dt.NewRow
                        dr(0) = lblReceipt.Text
                        dr(1) = lblStuNo.Text
                        dr(2) = lblStuName.Text
                        dr(3) = lblGrade.Text
                        dr(4) = lblAmount.Text
                        dt.Rows.Add(dr)
                        dt.AcceptChanges()
                    End If
                    cntSelected += 1
                End If
            Next
            If dt.Rows.Count > 0 Then
                ViewState("gvEmailStatus") = dt
                BindStatusGrid()
                gridbind()
                Me.MPE1.Show()
            Else
                If cntSelected < 1 Then
                    'lblError.Text = "No receipts found."
                    usrMessageBar.ShowNotification("No receipts found.", UserControls_usrMessageBar.WarningType.Danger)
                End If
                'lblError.Text &= "Failed to send Email(s)"
                usrMessageBar.ShowNotification("Failed to send Email(s)", UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
        Finally
            Me.btnEmailUnSent.Enabled = True
        End Try
    End Sub

    Protected Sub gvEmailStatus_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmailStatus.PageIndexChanging
        gvEmailStatus.PageIndex = e.NewPageIndex
        BindStatusGrid()
        Me.MPE1.Show()
    End Sub
    Private Sub HideButtons(ByVal bHide As Boolean)
        Me.btnOkay.Visible = Not bHide
        Me.btnCancel.Visible = Not bHide
    End Sub
End Class
