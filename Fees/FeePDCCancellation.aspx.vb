﻿Imports System.Web.Services
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic

Partial Class Fees_FeePDCCancellation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Property FPC_ID() As Integer
        Get
            Return ViewState("FPC_ID")
        End Get
        Set(ByVal value As Integer)
            ViewState("FPC_ID") = value
        End Set
    End Property
    Private Property FCL_ID() As Integer
        Get
            Return ViewState("FCL_ID")
        End Get
        Set(ByVal value As Integer)
            ViewState("FCL_ID") = value
        End Set
    End Property
    Private Property FPC_STATUS() As String
        Get
            Return ViewState("FPC_STATUS")
        End Get
        Set(ByVal value As String)
            ViewState("FPC_STATUS") = value
        End Set
    End Property
    Private Shared PageSize As Integer = 10
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Not Request.QueryString("MainMnu_code") Is Nothing AndAlso Request.QueryString("MainMnu_code") <> "" Then
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                ViewState("MainMnu_code") = "F300137"
            End If
            If Not Request.QueryString("datamode") Is Nothing AndAlso Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = "add"
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Session("sUsr_name") = "" Or Session("sBSuid") = "" Or (ViewState("MainMnu_code") <> "F300137" And ViewState("MainMnu_code") <> "F300138") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBSuid"), ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If ViewState("datamode") = "view" Or ViewState("datamode") = "approve" Then
                Me.pnlPage.Enabled = False
            End If
            If ViewState("datamode") = "approve" Then
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "view")
                Me.btnApprove.Visible = True
                btnEdit.Visible = False
                btnSave.Visible = False
                btnDelete.Visible = True
            End If

            txtDocDate.Text = Format(Date.Now, OASISConstants.DataBaseDateFormat)
            ddlAcdYear.DataBind()
            ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            usrSelStudent1.IsStudent = rbtnEnrollment.Checked
            usrSelStudent1.ACD_ID = ddlAcdYear.SelectedValue
            FPC_ID = 0
            FCL_ID = 0
            If Not Request.QueryString("viewid") Is Nothing AndAlso Request.QueryString("viewid") <> "" Then
                FPC_ID = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                If FPC_ID <> 0 Then
                    GET_SAVED_DATA()
                End If
            End If
            If FPC_STATUS = "A" Then 'if pdc cancellation already approved
                Me.pnlPage.Enabled = False
                'ShowMessage("The PDC cancellation is approved and no more modifications are allowed")
                usrMessageBar.ShowNotification("The PDC cancellation is approved and no more modifications are allowed", UserControls_usrMessageBar.WarningType.Danger)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), 1, "view")
            End If
        End If
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnSave)
        smScriptManager.RegisterPostBackControl(btnOkay)
    End Sub

    Sub CLEAR_PAGE()
        ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
        rbtnEnrollment.Checked = True
        rbtnEnrollment_CheckedChanged(Nothing, Nothing)
        txtComments.Text = ""
        FPC_STATUS = ""
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    'Sub ShowMessage(ByVal Message As String, Optional ByVal bError As Boolean = True)
    '    If Message <> "" Then
    '        If bError Then
    '            lblError.CssClass = "diverrorPopUp"
    '        Else
    '            lblError.CssClass = "divvalidPopUp"
    '        End If
    '    Else
    '        lblError.CssClass = ""
    '    End If
    '    lblError.Text = Message
    'End Sub
    Private Sub GET_SAVED_DATA()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuId"))
        pParms(1) = New SqlClient.SqlParameter("@FPC_ID", FPC_ID)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.GET_SAVED_PDC_CANCELLATION", pParms)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ddlAcdYear.SelectedValue = ds.Tables(0).Rows(0)("FPC_ACD_ID").ToString
            usrSelStudent1.SetStudentDetails(ds.Tables(0).Rows(0)("FPC_STU_ID").ToString)
            txtDocDate.Text = Format(CDate(ds.Tables(0).Rows(0)("FPC_DATE").ToString), OASISConstants.DataBaseDateFormat)
            txtComments.Text = ds.Tables(0).Rows(0)("FPC_NARRATION").ToString
            If usrSelStudent1.IsStudent Then
                rbtnEnrollment.Checked = True
            Else
                rbtnEnquiry.Checked = True
            End If
            FPC_STATUS = ds.Tables(0).Rows(0)("FPC_STATUS").ToString
            usrSelStudent1_StudentNoChanged(Nothing, Nothing)
            FCL_ID = ds.Tables(0).Rows(0)("FPC_FCL_ID").ToString
            For Each gvr As GridViewRow In gvReceipts.Rows
                Dim lblFCLID As Label = DirectCast(gvr.FindControl("lblFCLID"), Label)
                If Not lblFCLID Is Nothing AndAlso lblFCLID.Text.Trim = FCL_ID Then
                    gvReceipts.SelectedIndex = gvr.RowIndex
                End If
            Next
            GET_PDC()
            Dim FCQ_IDs As String() = ds.Tables(0).Rows(0)("FCQIDs").ToString.Split("|")
            If Not FCQ_IDs Is Nothing AndAlso FCQ_IDs.Length > 0 Then
                Dim listfcqids As New List(Of String)(FCQ_IDs)
                For Each gvr As GridViewRow In gvPDC.Rows
                    Dim lblFCQID As Label = gvr.FindControl("lblFCQID")
                    Dim chkSelectPDC As CheckBox = gvr.FindControl("chkSelectPDC")
                    If listfcqids.Contains(lblFCQID.Text) Then
                        chkSelectPDC.Checked = True
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub GET_RECEIPTS()
        Try
            ViewState("gvReceipts") = Nothing
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuId"))
            pParms(1) = New SqlClient.SqlParameter("@STU_ID", usrSelStudent1.STUDENT_ID)
            pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", IIf(rbtnEnrollment.Checked, "S", "E"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.GET_PDC_RECEIPTS", pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                ViewState("gvReceipts") = ds
                BIND_GRID_RECEIPTS()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvReceipts.DataSource = ds.Tables(0)
                Try
                    gvReceipts.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvReceipts.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvReceipts.Rows(0).Cells.Clear()
                gvReceipts.Rows(0).Cells.Add(New TableCell)
                gvReceipts.Rows(0).Cells(0).ColumnSpan = columnCount
                gvReceipts.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvReceipts.Rows(0).Cells(0).Text = "No Receipts found"
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub BIND_GRID_RECEIPTS()
        gvReceipts.DataSource = ViewState("gvReceipts")
        gvReceipts.DataBind()
    End Sub
    Private Sub GET_PDC()
        Try
            ViewState("PDC_WITH_MULTI_RECEIPT") = ""
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FCL_ID", FCL_ID)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuId"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.GET_FEECOLLECTION_PDC", pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                gvPDC.DataSource = ds
                gvPDC.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvPDC.DataSource = ds.Tables(0)
                Try
                    gvPDC.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvPDC.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvPDC.Rows(0).Cells.Clear()
                gvPDC.Rows(0).Cells.Add(New TableCell)
                gvPDC.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPDC.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPDC.Rows(0).Cells(0).Text = "No PDC(s) found"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Private Sub GET_PDC_SPLITUP(ByVal FCQ_ID As Int64, ByRef gvPDCSplit As GridView)
        Try
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FCQ_ID", FCQ_ID)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuId"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.GET_FEECOLLECTION_PDC_SPLITUP", pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                gvPDCSplit.DataSource = ds
                gvPDCSplit.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub ClearStudentDetails()
        usrSelStudent1.ClearDetails()
        usrSelStudent1.IsStudent = rbtnEnrollment.Checked
        usrSelStudent1_StudentNoChanged(Nothing, Nothing)
    End Sub

    Private Function VALIDATE_SAVE() As Boolean
        VALIDATE_SAVE = True
        'ShowMessage("")
        ViewState("FCQIDs") = ""
        ViewState("CHQNOs") = ""
        Try
            If Val(usrSelStudent1.STUDENT_ID) <= 0 Then
                'ShowMessage("Please select a student")
                usrMessageBar.ShowNotification("Please select a student", UserControls_usrMessageBar.WarningType.Danger)
                VALIDATE_SAVE = False
                Exit Function
            End If
            If txtDocDate.Text.Trim = "" OrElse Not IsDate(txtDocDate.Text) Then
                'ShowMessage("Please enter the document date in dd/MMM/yyyy format")
                usrMessageBar.ShowNotification("Please enter the document date in dd/MMM/yyyy format", UserControls_usrMessageBar.WarningType.Danger)
                VALIDATE_SAVE = False
                Exit Function
            End If
            For Each gvr As GridViewRow In gvPDC.Rows
                Dim lblFCQID As Label = gvr.FindControl("lblFCQID")
                Dim chkSelectPDC As CheckBox = gvr.FindControl("chkSelectPDC")
                Dim lblChqNo As Label = gvr.FindControl("lblChqNo")
                If Not chkSelectPDC Is Nothing Then
                    If chkSelectPDC.Checked Then
                        If CHECK_ALREADY_EXISTS(lblFCQID.Text.Trim) Then
                            'ShowMessage("The Cheque No " & lblChqNo.Text & " already exists in another PDC cancellation")
                            usrMessageBar.ShowNotification("The Cheque No " & lblChqNo.Text & " already exists in another PDC cancellation", UserControls_usrMessageBar.WarningType.Danger)
                            VALIDATE_SAVE = False
                            Exit Function
                        End If
                        ViewState("FCQIDs") = ViewState("FCQIDs") & IIf(ViewState("FCQIDs") = "", lblFCQID.Text, "|" & lblFCQID.Text)
                        If ViewState("PDC_WITH_MULTI_RECEIPT").ToString().Contains(lblChqNo.Text.Trim) Then
                            ViewState("CHQNOs") = ViewState("CHQNOs") & IIf(ViewState("CHQNOs") = "", lblChqNo.Text, ", " & lblChqNo.Text)
                        End If
                    End If
                End If
            Next
            If ViewState("FCQIDs") = "" Then
                'ShowMessage("Please select PDC(s) to continue")
                usrMessageBar.ShowNotification("Please select PDC(s) to continue", UserControls_usrMessageBar.WarningType.Danger)
                VALIDATE_SAVE = False
                Exit Function
            End If
            If txtComments.Text.Trim = "" Then
                'ShowMessage("Please give Narration to continue")
                usrMessageBar.ShowNotification("Please give Narration to continue", UserControls_usrMessageBar.WarningType.Danger)
                VALIDATE_SAVE = False
                Exit Function
            End If
        Catch ex As Exception
            VALIDATE_SAVE = False
        End Try
    End Function

    Private Function CHECK_ALREADY_EXISTS(ByVal FCQ_ID As Integer) As Boolean
        CHECK_ALREADY_EXISTS = False
        Try
            Dim Qry = "SELECT ISNULL(A.FPCD_ID,0)ID FROM FEES.FEE_COLL_PDC_CANCEL_D AS A WITH(NOLOCK) INNER JOIN FEES.FEE_COLL_PDC_CANCEL_H AS B WITH(NOLOCK) " & _
                "ON A.FPCD_FPC_ID=B.FPC_ID WHERE ISNULL(B.FPC_bDELETED,0)=0 AND A.FPCD_FPC_ID <> " & FPC_ID & " AND A.FPCD_FCQ_ID=" & FCQ_ID & ""
            If SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, Qry) > 0 Then
                CHECK_ALREADY_EXISTS = True
            End If
        Catch ex As Exception

        End Try
    End Function
    Private Sub SAVE_PDC_CANCELLATION()
        '  ShowMessage("")
        Dim connection As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()

        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction()
        Try
            Dim param(12) As SqlParameter
            param(0) = New SqlParameter("@FPC_ID", SqlDbType.Int)
            param(0).Value = FPC_ID
            param(0).Direction = ParameterDirection.InputOutput
            param(1) = New SqlParameter("@FPC_BSU_ID", Session("sBsuId"))
            param(2) = New SqlParameter("@FPC_ACD_ID", ddlAcdYear.SelectedValue)
            param(3) = New SqlParameter("@FPC_DATE", txtDocDate.Text.Trim)
            param(4) = New SqlParameter("@FPC_STU_ID", usrSelStudent1.STUDENT_ID)
            param(5) = New SqlParameter("@FPC_FCL_ID", FCL_ID)
            param(6) = New SqlParameter("@FPC_STATUS", "N")
            param(7) = New SqlParameter("@FPC_NARRATION", txtComments.Text.Trim)
            param(8) = New SqlParameter("@USER_NAME", Session("sUsr_name"))
            'Dim computer_name() As String
            'computer_name = Split(System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, ".")
            param(9) = New SqlParameter("@FPC_CREATED_IP", "")
            param(10) = New SqlParameter("@FCQ_IDs", ViewState("FCQIDs"))

            param(11) = New SqlParameter("@RET_VALUE", SqlDbType.Int)
            param(11).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "FEES.SAVE_FEECOLL_PDC_CANCELLATION", param)
            If param(11).Value = 0 And param(0).Value > 0 Then
                sqltran.Commit()
                Dim flagAudit As Integer
                If FPC_ID = 0 Then
                    flagAudit = UtilityObj.operOnAudiTable(Master.MenuName, param(0).Value, "Insert", Page.User.Identity.Name.ToString, Me.Page, "pdc cancellation created")
                Else
                    flagAudit = UtilityObj.operOnAudiTable(Master.MenuName, FPC_ID, "Update", Page.User.Identity.Name.ToString, Me.Page, "pdc cancellation updated")
                End If
                FPC_ID = param(0).Value
                'ShowMessage("PDC Cancellation has been saved successfully and forwarded for approval", False)
                usrMessageBar.ShowNotification("PDC Cancellation has been saved successfully and forwarded for approval", UserControls_usrMessageBar.WarningType.Success)
                btnClose_Click(Nothing, Nothing)
                ViewState("datamode") = "view"
                pnlPage.Enabled = False
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                FPC_ID = 0
                sqltran.Rollback()
                btnClose_Click(Nothing, Nothing)

                'ShowMessage("Unable to Save/Update PDC Cancellation - " & UtilityObj.getErrorMessage(param(11).Value))
                usrMessageBar.ShowNotification("Unable to Save/Update PDC Cancellation - " & UtilityObj.getErrorMessage(param(11).Value), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            'ShowMessage("Unable to Save/Update PDC Cancellation - " & ex.InnerException.Message)
            usrMessageBar.ShowNotification("Unable to Save/Update PDC Cancellation - " & ex.InnerException.Message, UserControls_usrMessageBar.WarningType.Danger)
            sqltran.Rollback()
        Finally
            If con.State = ConnectionState.Open Then
                con.Close()
            End If
        End Try
    End Sub
    Private Sub DELETE_PDC_CANCELLATION()
        'ShowMessage("")
        Dim connection As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()

        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction()
        Try
            Dim param(12) As SqlParameter
            param(0) = New SqlParameter("@FPC_ID", SqlDbType.Int)
            param(0).Value = FPC_ID
            param(0).Direction = ParameterDirection.InputOutput
            param(1) = New SqlParameter("@FPC_BSU_ID", Session("sBsuId"))
            param(2) = New SqlParameter("@FPC_ACD_ID", 0)
            param(3) = New SqlParameter("@FPC_DATE", txtDocDate.Text.Trim)
            param(4) = New SqlParameter("@FPC_STU_ID", 0)
            param(5) = New SqlParameter("@FPC_FCL_ID", 0)
            param(6) = New SqlParameter("@FPC_STATUS", "D")
            param(7) = New SqlParameter("@FPC_NARRATION", "")
            param(8) = New SqlParameter("@USER_NAME", Session("sUsr_name"))
            'Dim computer_name() As String
            'computer_name = Split(System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, ".")
            param(9) = New SqlParameter("@FPC_CREATED_IP", "")
            param(10) = New SqlParameter("@bDELETE", True)

            param(11) = New SqlParameter("@RET_VALUE", SqlDbType.Int)
            param(11).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "FEES.SAVE_FEECOLL_PDC_CANCELLATION", param)
            If param(11).Value = 0 And param(0).Value > 0 Then
                FPC_ID = param(0).Value
                sqltran.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, FPC_ID, "Delete", Page.User.Identity.Name.ToString, Me.Page, "pdc cancellation deleted")
                CLEAR_PAGE()
                'ShowMessage("PDC Cancellation has been deleted successfully", False)
                usrMessageBar.ShowNotification("PDC Cancellation has been deleted successfully", UserControls_usrMessageBar.WarningType.Success)
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                FPC_ID = 0
                sqltran.Rollback()
                'ShowMessage("Unable to Delete PDC Cancellation - " & UtilityObj.getErrorMessage(param(11).Value))
                usrMessageBar.ShowNotification("Unable to Delete PDC Cancellation - " & UtilityObj.getErrorMessage(param(11).Value), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            'ShowMessage("Unable to Delete PDC Cancellation - " & ex.InnerException.Message)
            usrMessageBar.ShowNotification("Unable to Delete PDC Cancellation - " & ex.InnerException.Message, UserControls_usrMessageBar.WarningType.Danger)
            sqltran.Rollback()
        Finally
            If con.State = ConnectionState.Open Then
                con.Close()
            End If
        End Try
    End Sub
    Private Sub APPROVE_PDC_CANCELLATION()
        'ShowMessage("")
        Dim connection As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()

        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction()
        Try
            Dim param(13) As SqlParameter
            param(0) = New SqlParameter("@FPC_ID", SqlDbType.Int)
            param(0).Value = FPC_ID
            param(0).Direction = ParameterDirection.InputOutput
            param(1) = New SqlParameter("@FPC_BSU_ID", Session("sBsuId"))
            param(2) = New SqlParameter("@FPC_ACD_ID", 0)
            param(3) = New SqlParameter("@FPC_DATE", txtDocDate.Text.Trim)
            param(4) = New SqlParameter("@FPC_STU_ID", 0)
            param(5) = New SqlParameter("@FPC_FCL_ID", 0)
            param(6) = New SqlParameter("@FPC_STATUS", "A")
            param(7) = New SqlParameter("@FPC_NARRATION", "")
            param(8) = New SqlParameter("@USER_NAME", Session("sUsr_name"))
            'Dim computer_name() As String
            'computer_name = Split(System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, ".")
            param(9) = New SqlParameter("@FPC_CREATED_IP", "")
            param(10) = New SqlParameter("@bDELETE", False)
            param(11) = New SqlParameter("@bApprove", True)
            param(12) = New SqlParameter("@RET_VALUE", SqlDbType.Int)
            param(12).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "FEES.SAVE_FEECOLL_PDC_CANCELLATION", param)
            If param(12).Value = 0 And param(0).Value > 0 Then
                FPC_ID = param(0).Value
                sqltran.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, FPC_ID, "Approve", Page.User.Identity.Name.ToString, Me.Page, "pdc cancellation approved")
                CLEAR_PAGE()
                'ShowMessage("PDC Cancellation has been approved", False)
                usrMessageBar.ShowNotification("PDC Cancellation has been approved", UserControls_usrMessageBar.WarningType.Success)
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                FPC_ID = 0
                sqltran.Rollback()
                'ShowMessage("Unable to Approve PDC Cancellation - " & UtilityObj.getErrorMessage(param(12).Value))
                usrMessageBar.ShowNotification("Unable to Approve PDC Cancellation - " & UtilityObj.getErrorMessage(param(12).Value), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            'ShowMessage("Unable to Approve PDC Cancellation - " & ex.InnerException.Message)
            usrMessageBar.ShowNotification("Unable to Approve PDC Cancellation - " & ex.InnerException.Message, UserControls_usrMessageBar.WarningType.Danger)
            sqltran.Rollback()
        Finally
            If con.State = ConnectionState.Open Then
                con.Close()
            End If
        End Try
    End Sub

    Protected Sub usrSelStudent1_StudentNoChanged(sender As Object, e As EventArgs) Handles usrSelStudent1.StudentNoChanged
        FCL_ID = 0 'clear the selected receipt
        ViewState("FCQIDs") = "" 'clear the selected cheque ids
        ViewState("CHQNOs") = "" 'clear the cheque nos
        'ShowMessage("")
        gvReceipts.DataSource = Nothing
        gvReceipts.DataBind()
        gvPDC.DataSource = Nothing
        gvPDC.DataBind()
        If Val(usrSelStudent1.STUDENT_ID) >= 0 Then
            GET_RECEIPTS()
        End If
    End Sub
    Protected Sub ddlAcdYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAcdYear.SelectedIndexChanged
        usrSelStudent1.ACD_ID = ddlAcdYear.SelectedValue
        ClearStudentDetails()
    End Sub

    Protected Sub gvReceipts_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvReceipts.PageIndexChanging
        gvReceipts.PageIndex = e.NewPageIndex
        BIND_GRID_RECEIPTS()
    End Sub

    Protected Sub gvReceipts_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvReceipts.RowCommand
        If e.CommandName = "Select" Then
            'ShowMessage("")
            Dim lblFCLID As Label = DirectCast(gvReceipts.Rows(e.CommandArgument).FindControl("lblFCLID"), Label)
            If Not lblFCLID Is Nothing Then
                FCL_ID = lblFCLID.Text
                GET_PDC()
            End If
        End If
    End Sub

    Protected Sub rbtnEnrollment_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnEnrollment.CheckedChanged
        ClearStudentDetails()
    End Sub

    Protected Sub rbtnEnquiry_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnEnquiry.CheckedChanged
        ClearStudentDetails()
    End Sub

    Protected Sub gvPDC_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvPDC.RowDataBound
        Dim lblFCQID As Label = DirectCast(e.Row.FindControl("lblFCQID"), Label)
        Dim lblChqNo As Label = DirectCast(e.Row.FindControl("lblChqNo"), Label)

        Dim gvPDCSplit As GridView = DirectCast(e.Row.FindControl("gvPDCSplit"), GridView)
        If Not lblFCQID Is Nothing AndAlso Not gvPDCSplit Is Nothing Then
            GET_PDC_SPLITUP(lblFCQID.Text, gvPDCSplit)
            If gvPDCSplit.Rows.Count > 1 Then
                ViewState("PDC_WITH_MULTI_RECEIPT") = ViewState("PDC_WITH_MULTI_RECEIPT") & IIf(ViewState("PDC_WITH_MULTI_RECEIPT").ToString = "", lblChqNo.Text, "," & lblChqNo.Text)
            End If
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim chkSelectPDC As CheckBox = DirectCast(e.Row.FindControl("chkSelectPDC"), CheckBox)
            Dim bDISABLE As Boolean = CBool(gvPDC.DataKeys(e.Row.RowIndex).Value)
            If Not chkSelectPDC Is Nothing Then
                If bDISABLE Then
                    'chkSelectPDC.Enabled = False
                    chkSelectPDC.Attributes.Add("onclick", "return unselect('" & chkSelectPDC.ClientID & "','true');")
                    Dim span As System.Web.UI.WebControls.DataControlFieldCell = chkSelectPDC.Parent
                    'span.Attributes.Add("onclick", "alert('Cannot cancel the PDC, date already past the current date.');")
                    chkSelectPDC.ToolTip = "Cannot cancel the PDC, date already past the current date."
                    span.Style.Item("cursor") = "pointer"
                End If

            End If
        End If

    End Sub

    Protected Sub gvReceipts_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvReceipts.RowDataBound
        Dim lblFCLID As Label = DirectCast(e.Row.FindControl("lblFCLID"), Label)
        Dim lbtnRecNo As LinkButton = DirectCast(e.Row.FindControl("lbtnRecNo"), LinkButton)
        If Not lblFCLID Is Nothing AndAlso Not lbtnRecNo Is Nothing Then
            Dim QueryString = "&ID=" & Encr_decrData.Encrypt(lblFCLID.Text) & "&RECNO=" & Encr_decrData.Encrypt(lbtnRecNo.Text) & ""
            lbtnRecNo.Attributes.Add("onClick", "return ShowWindowWithClose('FeeReportInterpose.aspx?TYPE=" & Encr_decrData.Encrypt("RECIEPT") & QueryString & "', '', '60%', '75%');")
        End If
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If VALIDATE_SAVE() Then
            If ViewState("CHQNOs") <> "" Then
                Me.lblMsg.Text = "For your Information cheque(s) " & ViewState("CHQNOs") & " have more than one receipts. Select 'CANCEL' and Expand Cheque No to view the receipts or select 'OK' to continue to Save."
                Me.alertpopup.Style.Item("display") = "block"
            Else
                SAVE_PDC_CANCELLATION()
            End If
        End If
    End Sub

    Protected Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.lblMsg.Text = ""
        Me.alertpopup.Style.Item("display") = "none"
    End Sub

    Protected Sub btnOkay_Click(sender As Object, e As EventArgs) Handles btnOkay.Click
        SAVE_PDC_CANCELLATION()
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            CLEAR_PAGE()
            ViewState("datamode") = "view"
            pnlPage.Enabled = False
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect("../Common/ListReportView.aspx?MainMnu_code=u0HkTnbq0/A=&datamode=Zo4HhpVNpXc=")
        End If
    End Sub

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        pnlPage.Enabled = True
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If FPC_ID <> 0 Then
            DELETE_PDC_CANCELLATION()
        End If
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        If FPC_ID <> 0 Then
            APPROVE_PDC_CANCELLATION()
        End If
    End Sub
End Class
