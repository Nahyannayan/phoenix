﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="feeTransportAttendanceView.aspx.vb" Inherits="Fees_feeTransportAttendanceView" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <base target="_self" />
    <script language="javascript" type="text/javascript"> 
    </script>
    <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" /> 
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" /> --%>

    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

    <!-- Bootstrap header files ends here -->
</head>

<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
    <form id="form1" runat="server">

        <div class="card mb-3">
            <div class="card-header letter-space">
                <i class="fa fa-money mr-3"></i>
                <asp:Label ID="lblHeader" runat="server" Text="Transport Attendance Log"></asp:Label>
            </div>
            <div class="card-body">
                <div class="table-responsive m-auto">
                    <table cellspacing="0" width="100%" align="center" border="0" cellpadding="0">
                        <tr>
                            <td colspan="4" align="left">
                                <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" SkinID="error"></asp:Label>--%>
                                <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                                <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
                                </ajaxToolkit:ToolkitScriptManager>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">From Date</span></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtFromDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /></td>
                            <td align="left" width="20%"><span class="field-label">To Date</span></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtToDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif"
                                    TabIndex="4" /></td>
                        </tr>
                        <tr>
                            <td align="left" class="title-bg-lite" colspan="4">Details
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:GridView ID="gvFeeCollection" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                    CssClass="table table-bordered table-row" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="LOG_DATE" HeaderText="Date">
                                            <ItemStyle />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LOG_INTIME" HeaderText="In">
                                            <ItemStyle />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LOG_OUTTIME" HeaderText="Out">
                                            <ItemStyle />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LOG_PICKUP" HeaderText="Pick Up" />
                                        <asp:BoundField DataField="LOG_DROPOFF" HeaderText="Drop Off" />
                                        <asp:BoundField DataField="STU_NAME" HeaderText="Name" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="btnFinish" runat="server" CssClass="button" Text="Close" OnClientClick="javascript:window.close();" />
                                <asp:Button ID="btnPrint" runat="server" CssClass="button" OnClientClick="javascript:window.print();" Text="Print"></asp:Button></td>
                        </tr>
                    </table>

                    <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                        PopupButtonID="imgFrom" TargetControlID="txtFromDate">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                        PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                        PopupButtonID="imgTo" TargetControlID="txtToDate">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                        PopupButtonID="txtToDate" TargetControlID="txtToDate">
                    </ajaxToolkit:CalendarExtender>
                </div>
            </div>
        </div>
    </form>
</body>


</html>
