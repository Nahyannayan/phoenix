Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Imports System.Xml
Imports System.Xml.Xsl
Imports GemBox.Spreadsheet
Imports Lesnikowski.Barcode
Imports CrystalDecisions.CrystalReports.Engine

Partial Class Fees_FeeGroupProformaInvoicePayment
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property ConnectionString() As String
        Get
            Return ViewState("ConnectionString")
        End Get
        Set(ByVal value As String)
            ViewState("ConnectionString") = value
        End Set
    End Property
    Private Property FloatColIndex() As String
        Get
            Return ViewState("FloatColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("FloatColIndex") = value
        End Set
    End Property
    Private Property FilterString() As String
        Get
            Return ViewState("FilterString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterString") = value
        End Set
    End Property
    'Private Property PINos() As String
    '    Get
    '        Return ViewState("PINos")
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("PINos") = value
    '    End Set
    'End Property
    Private Property InvoicedAmt() As Double
        Get
            Return ViewState("InvoicedAmt")
        End Get
        Set(ByVal value As Double)
            ViewState("InvoicedAmt") = value
        End Set
    End Property
    Private Property FPHInvNos() As String
        Get
            Return ViewState("FPHInvNos")
        End Get
        Set(ByVal value As String)
            ViewState("FPHInvNos") = value
        End Set
    End Property
    Private Property InvTotals() As Double
        Get
            Return ViewState("InvTotals")
        End Get
        Set(ByVal value As Double)
            ViewState("InvTotals") = value
        End Set
    End Property
    Private Property DateColIndex() As String
        Get
            Return ViewState("DateColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("DateColIndex") = value
        End Set
    End Property
    Private Property SQLSelect() As String
        Get
            Return ViewState("SQLSelect")
        End Get
        Set(ByVal value As String)
            ViewState("SQLSelect") = value
        End Set
    End Property
    Private Property SortColString() As String
        Get
            Return ViewState("SortColString")
        End Get
        Set(ByVal value As String)
            ViewState("SortColString") = value
        End Set
    End Property
    Private Property FilterTextboxString() As String
        Get
            Return ViewState("FilterTextboxString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterTextboxString") = value
        End Set
    End Property
    Private Property EditPagePath() As String
        Get
            Return ViewState("EditPagePath")
        End Get
        Set(ByVal value As String)
            ViewState("EditPagePath") = value
        End Set
    End Property
    Private Property MainMenuCode() As String
        Get
            Return ViewState("MainMenuCode")
        End Get
        Set(ByVal value As String)
            ViewState("MainMenuCode") = value
        End Set
    End Property
    Private Property Datamode() As String
        Get
            Return ViewState("Datamode")
        End Get
        Set(ByVal value As String)
            ViewState("Datamode") = value
        End Set
    End Property
    Private Property HeaderTitle() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            Page.Title = value
            lblTitle.Text = value
        End Set
    End Property
    Private Property ShowGridCheckBox() As Boolean
        Get
            Return ViewState("ShowGridCheckBox")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ShowGridCheckBox") = value
        End Set
    End Property
    Private Property ShowLastColumn() As Boolean
        Get
            Return ViewState("ShowLastColumn")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ShowLastColumn") = value
        End Set
    End Property
    Private Property Show2ndLastColumn() As Boolean
        Get
            Return ViewState("Show2ndLastColumn")
        End Get
        Set(ByVal value As Boolean)
            ViewState("Show2ndLastColumn") = value
        End Set
    End Property
    Private Property Show3rdLastColumn() As Boolean
        Get
            Return ViewState("Show3rdLastColumn")
        End Get
        Set(ByVal value As Boolean)
            ViewState("Show3rdLastColumn") = value
        End Set
    End Property
    Private Property NoAddingAllowed() As Boolean
        Get
            Return ViewState("NoAddingAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoAddingAllowed") = value
        End Set
    End Property
    Private Property NoViewAllowed() As Boolean
        Get
            Return ViewState("NoViewAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoViewAllowed") = value
        End Set
    End Property
    Private Property HideExportButton() As Boolean
        Get
            Return ViewState("HideExportButton")
        End Get
        Set(ByVal value As Boolean)
            ViewState("HideExportButton") = value
        End Set
    End Property
    Private Property IsStoredProcedure() As Boolean
        Get
            Return ViewState("IsStoredProcedure")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsStoredProcedure") = value
        End Set
    End Property
    Private Property SaveSuccess() As Boolean
        Get
            Return ViewState("SaveSuccess")
        End Get
        Set(ByVal value As Boolean)
            ViewState("SaveSuccess") = value
        End Set
    End Property
    Private Property DTgvDetails() As DataTable
        Get
            Return ViewState("gvDetails")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvDetails") = value
        End Set
    End Property

    Private Property SPParam() As SqlParameter()
        Get
            Return Session("SPParam")
        End Get
        Set(ByVal value As SqlParameter())
            Session("SPParam") = value
        End Set
    End Property

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If Session("sModule") = "SS" Then
                Me.MasterPageFile = "../mainMasterPageSS.master"
            Else
                Me.MasterPageFile = "../mainMasterPage.master"
            End If
        Catch ex As Exception

        End Try
    End Sub
    ' Dim SPParam(10) As SqlParameter
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                gvDetails.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
                MainMenuCode = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    Datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                Dim msgText As String
                If Not Request.QueryString("msgText") Is Nothing And Request.QueryString("msgText") <> "" Then
                    msgText = Encr_decrData.Decrypt(Request.QueryString("msgText").Replace(" ", "+"))
                    '  lblError.Text = msgText
                    usrMessageBar.ShowNotification(msgText, UserControls_usrMessageBar.WarningType.Danger)
                End If
                Me.lblTitle.Text = "Group ProForma Invoice"
                ShowGridCheckBox = False
                NoAddingAllowed = False
                NoViewAllowed = False
                HideExportButton = False
                IsStoredProcedure = False
                SaveSuccess = False
                Me.trChqDetail.Visible = False
                Me.trChqSave.Visible = False
                Show2ndLastColumn = True
                Show3rdLastColumn = True
                ShowLastColumn = False
                h_Company_ID.Value = "1"
                BuildListView()
                gridbind(False)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(1, str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(2, str_Sid_img(2))
        str_Sid_img = h_selected_menu_3.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(3, str_Sid_img(2))
        str_Sid_img = h_selected_menu_4.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(4, str_Sid_img(2))
        str_Sid_img = h_selected_menu_5.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(5, str_Sid_img(2))
        str_Sid_img = h_selected_menu_6.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(6, str_Sid_img(2))
    End Sub
    Public Sub setID(ByVal ColumnNo As Int16, Optional ByVal p_imgsrc As String = "")
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvDetails.HeaderRow.FindControl("mnu_" & ColumnNo.ToString & "_img")
                If s Is Nothing Then Exit Sub
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvDetails.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function SetDTFilterCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function getColumnFilterString(ByVal FilterTypeString As String, ByVal GridViewHeaderRow As GridViewRow, ByVal ControlName As String, ByVal ColumnName As String) As String
        Try
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            txtSearch = GridViewHeaderRow.FindControl(ControlName)
            Dim str_Filter As String = ""
            larrSearchOpr = FilterTypeString.Split("__")
            lstrOpr = larrSearchOpr(0)
            str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            If Not IsStoredProcedure Then
                str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            Else
                str_Filter = SetDTFilterCondn(lstrOpr, ColumnName, txtSearch.Text)
            End If
            getColumnFilterString = str_Filter
            FilterTextboxString &= ControlName & "=" & txtSearch.Text & "||"
        Catch ex As Exception
            getColumnFilterString = ""
        End Try
    End Function
    Private Sub SetHeaderFilterString()
        Try
            Dim ColStrFilter(), ColValue(), iVal As String
            ColStrFilter = FilterTextboxString.Split("||")
            Dim iTxtBox As TextBox = Nothing
            For Each iVal In ColStrFilter
                ColValue = iVal.Split("=")
                If ColValue.Length > 0 Then
                    If gvDetails.HeaderRow.FindControl(ColValue(0)) IsNot Nothing Then
                        iTxtBox = CType(gvDetails.HeaderRow.FindControl(ColValue(0)), TextBox)
                    End If
                    If ColValue.Length > 1 And iTxtBox IsNot Nothing Then
                        iTxtBox.Text = ColValue(1)
                    End If
                End If

            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub gridbind(ByVal NoReset As Boolean)
        Try
            Dim GridData As New DataTable
            Dim lblID As New Label
            Dim lblName As New Label
            Dim str_Filter As String = ""
            Dim i As Int16
            Dim ColName As String
            FilterTextboxString = ""
            If gvDetails.Rows.Count > 0 And NoReset = False Then
                If ViewState("GridTable").Columns.count > 0 Then str_Filter &= getColumnFilterString(h_selected_menu_1.Value, gvDetails.HeaderRow, "txtCol1", ViewState("GridTable").Columns(0).ColumnName)
                If ViewState("GridTable").Columns.count > 1 Then str_Filter &= getColumnFilterString(h_Selected_menu_2.Value, gvDetails.HeaderRow, "txtCol2", ViewState("GridTable").Columns(1).ColumnName)
                If ViewState("GridTable").Columns.count > 2 Then str_Filter &= getColumnFilterString(h_selected_menu_3.Value, gvDetails.HeaderRow, "txtCol3", ViewState("GridTable").Columns(2).ColumnName)
                If ViewState("GridTable").Columns.count > 3 Then str_Filter &= getColumnFilterString(h_selected_menu_4.Value, gvDetails.HeaderRow, "txtCol4", ViewState("GridTable").Columns(3).ColumnName)
                If ViewState("GridTable").Columns.count > 4 Then str_Filter &= getColumnFilterString(h_selected_menu_5.Value, gvDetails.HeaderRow, "txtCol5", ViewState("GridTable").Columns(4).ColumnName)
                If ViewState("GridTable").Columns.count > 5 Then str_Filter &= getColumnFilterString(h_selected_menu_6.Value, gvDetails.HeaderRow, "txtCol6", ViewState("GridTable").Columns(5).ColumnName)
            End If
            Dim OrderByStr As String = ""
            If SortColString <> "" Then
                OrderByStr = "order by " & SortColString
            Else
                OrderByStr = "order by  1"
            End If
            If Not IsStoredProcedure Then
                GridData = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQLSelect & str_Filter & OrderByStr).Tables(0)
            Else
                GridData = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, SQLSelect, CType(SPParam, SqlParameter())).Tables(0)
                str_Filter = " 1 = 1 " & str_Filter
                GridData.Select(str_Filter)
                Dim mySelectDT As New DataTable
                Dim mRow As DataRow
                mySelectDT = GridData.Copy
                mySelectDT.Rows.Clear()
                For Each mRow In GridData.Select(str_Filter)
                    mySelectDT.ImportRow(mRow)
                Next
                GridData = mySelectDT.Copy()
            End If

            Dim bindDT, myData As New DataTable
            bindDT = GridData.Copy
            myData = GridData.Copy
            If myData.Columns.Count > 0 Then
                myData.Columns.RemoveAt(0)
            End If
            ViewState("GridTable") = GridData.Copy
            Session("myData") = myData
            GridData.Rows.Clear()
            Dim HeaderLabel As New Label
            Dim DColIndex, FColIndex As String
            FColIndex = "" : DColIndex = ""
            For i = 1 To GridData.Columns.Count
                ColName = "Col" & i.ToString
                bindDT.Columns(i - 1).ColumnName = ColName
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Int32") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Decimal") Then
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Decimal") Then
                        FColIndex &= "|" & (i).ToString
                    End If
                Else
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Left
                End If
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.DateTime") Then
                    DColIndex &= "|" & (i).ToString
                End If
            Next
            FloatColIndex = FColIndex
            DateColIndex = DColIndex
            If bindDT.Columns.Count < 6 Or gvDetails.Columns.Count <> bindDT.Columns.Count Then
                For i = 0 To bindDT.Columns.Count
                    gvDetails.Columns(i).Visible = True
                Next
                For i = bindDT.Columns.Count + 1 To 6
                    ColName = "Col" & i.ToString
                    bindDT.Columns.Add(ColName, System.Type.GetType("System.String"))
                    gvDetails.Columns(i).Visible = False
                Next
            End If
            Dim Columns As Int16 = gvDetails.Columns.Count
            gvDetails.Columns(0).Visible = ShowGridCheckBox
            gvDetails.Columns(Columns - 1).Visible = ShowLastColumn
            gvDetails.Columns(Columns - 2).Visible = Show2ndLastColumn
            gvDetails.Columns(Columns - 3).Visible = Show3rdLastColumn
            gvDetails.Columns(1).Visible = False
            gvDetails.DataSource = bindDT
            If bindDT.Rows.Count = 0 Then
                bindDT.Rows.Add(bindDT.NewRow())
                gvDetails.DataBind()
                Dim columnCount As Integer = gvDetails.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvDetails.Rows(0).Cells.Clear()
                gvDetails.Rows(0).Cells.Add(New TableCell)
                gvDetails.Rows(0).Cells(0).ColumnSpan = columnCount
                gvDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvDetails.DataBind()
            End If

            For i = 1 To GridData.Columns.Count
                HeaderLabel = gvDetails.HeaderRow.FindControl("lblHeaderCol" & i)
                If Not HeaderLabel Is Nothing Then
                    HeaderLabel.Text = GridData.Columns(i - 1).ColumnName
                End If
            Next
            SetHeaderFilterString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            Dim vCOMP_ID As Integer
            Dim str_ENQ As String
            Dim FloatColIndexArr(), i, ControlName As String
            If e.Row.RowType = DataControlRowType.Header Then
                If rblFilter.SelectedValue <> "P" Then
                    gvDetails.Columns(0).Visible = False
                End If
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                Dim chk As HtmlInputCheckBox = DirectCast(e.Row.FindControl("chkControl"), HtmlInputCheckBox)
                Dim lblID As Label = DirectCast(e.Row.FindControl("lblItemCol1"), Label)
                Dim lblInvNo As Label = DirectCast(e.Row.FindControl("lblItemCol4"), Label)
                'Dim lblCOMP_ID As Label = sender.Parent.parent.findcontrol("lblCOMP_ID")
                If rblFilter.SelectedValue = "PD" And lblID.Text.Trim <> "" Then
                    chk.Disabled = True
                End If
                Dim hlEdit As New HyperLink
                hlEdit = TryCast(e.Row.FindControl("hlView"), HyperLink)
                If hlEdit IsNot Nothing AndAlso lblInvNo IsNot Nothing Then
                    vCOMP_ID = h_Company_ID.Value
                    str_ENQ = "STUD"
                    ViewState("datamode") = Encr_decrData.Encrypt("view")
                    hlEdit.NavigateUrl = "FEEPerformaInvoice.aspx?inv_no=" & Encr_decrData.Encrypt(lblInvNo.Text) & _
                    "&COMP_ID=" & Encr_decrData.Encrypt(vCOMP_ID) & _
                    "&MainMnu_code=" & Request.QueryString("MainMnu_code") & _
                    "&datamode=" & ViewState("datamode") & "&TYPE=" & Encr_decrData.Encrypt(str_ENQ)
                End If
                FloatColIndexArr = FloatColIndex.Split("|")
                Dim lblItemCol As Label
                For Each i In FloatColIndexArr
                    If IsNumeric(i) Then
                        ControlName = "lblItemCol" & i.ToString
                        lblItemCol = TryCast(e.Row.FindControl(ControlName), Label)
                        If Not lblItemCol Is Nothing Then
                            If IsNumeric(lblItemCol.Text) Then
                                lblItemCol.Text = Convert.ToDouble(lblItemCol.Text).ToString("#,##0.00")
                            End If
                        End If
                    End If
                Next
                Dim DateColIndexArr() As String
                DateColIndexArr = DateColIndex.Split("|")
                For Each i In DateColIndexArr
                    If IsNumeric(i) Then
                        ControlName = "lblItemCol" & i.ToString
                        lblItemCol = TryCast(e.Row.FindControl(ControlName), Label)
                        If Not lblItemCol Is Nothing Then
                            If IsDate(lblItemCol.Text) Then
                                lblItemCol.Text = CDate(lblItemCol.Text).ToString("dd/MMM/yyyy")
                            End If
                        End If
                    End If
                Next
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind(False)
    End Sub
    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        gvDetails.PageIndex = e.NewPageIndex
        gridbind(True)
    End Sub

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            Dim chkControl As New HtmlInputCheckBox
            For Each grow As GridViewRow In gvDetails.Rows
                chkControl = grow.FindControl("chkControl")
                chkControl.Checked = chkSelectAll.Checked
            Next
        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Sub

    Private Sub BuildListView()
        Try
            Dim strConn, StrSQL, StrSortCol As String
            strConn = "" : StrSQL = "" : StrSortCol = ""
            strConn = ConnectionManger.GetOASISConnection.ConnectionString
            Dim SearchMode As String = Request.QueryString("id")
            Select Case MainMenuCode
                Case "F100174"
                    StrSQL &= "FEES.GroupProformaInvoiceList"
                    ReDim SPParam(3)
                    NoAddingAllowed = True
                    SPParam(0) = Mainclass.CreateSqlParameter("@CompanyID", h_Company_ID.Value, SqlDbType.Int)
                    SPParam(1) = Mainclass.CreateSqlParameter("@Filter", Me.rblFilter.SelectedValue, SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    EditPagePath = "../Inventory/Priority.aspx"
                    strConn = ConnectionManger.GetOASIS_FEESConnection.ConnectionString
                    StrSortCol = "FPH_DT"
                    HeaderTitle = "Group Proforma Invoice"
                    ShowGridCheckBox = True
                    IsStoredProcedure = True
            End Select
            SQLSelect = StrSQL
            ConnectionString = strConn
            SortColString = StrSortCol
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub imgCompany_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCompany.Click
        Me.rblFilter.SelectedValue = "P"

        rblFilter_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.txtCompanyDescr.Text = ""
        Me.h_Company_ID.Value = ""
        ViewState("GridTable") = ""
        Me.rblFilter.SelectedValue = "P"
        Me.gvDetails.DataSource = Mainclass.getDataTable("select *from FEES.FEE_GroupProformaInv where fgp_id=-1", ConnectionManger.GetOASIS_FEESConnectionString)
        Me.gvDetails.DataBind()
    End Sub
    Function ValidateGrid() As Boolean
        ValidateGrid = False
        FPHInvNos = String.Empty
        'Dim dtgvDetails As DataTable = DirectCast(ViewState("GridTable"), DataTable)
        For Each gvr As GridViewRow In Me.gvDetails.Rows
            Dim chk As HtmlInputCheckBox = DirectCast(gvr.FindControl("chkControl"), HtmlInputCheckBox)
            Dim lblBSUID As Label = DirectCast(gvr.FindControl("lblItemCol1"), Label)
            Dim lblInvNo As Label = DirectCast(gvr.FindControl("lblItemCol4"), Label)
            If chk.Checked Then
                FPHInvNos = IIf(FPHInvNos = String.Empty, (lblInvNo.Text & "@" & lblBSUID.Text), FPHInvNos + "|" + (lblInvNo.Text & "@" & lblBSUID.Text))
            End If
        Next
        ValidateGrid = IIf(FPHInvNos <> String.Empty, True, False)
    End Function
    Private Function SelectedInvoicesforPayment() As Boolean
        SelectedInvoicesforPayment = False
        FPHInvNos = String.Empty
        InvoicedAmt = 0
        'Dim dtgvDetails As DataTable = DirectCast(ViewState("GridTable"), DataTable)
        For Each gvr As GridViewRow In Me.gvDetails.Rows
            Dim chk As HtmlInputCheckBox = DirectCast(gvr.FindControl("chkControl"), HtmlInputCheckBox)
            Dim lblInvNo As Label = DirectCast(gvr.FindControl("lblItemCol3"), Label)
            Dim lblInvAmt As Label = DirectCast(gvr.FindControl("lblItemCol5"), Label)
            If chk.Checked Then
                InvoicedAmt = InvoicedAmt + Convert.ToDouble(lblInvAmt.Text)
                FPHInvNos = IIf(FPHInvNos = String.Empty, lblInvNo.Text, FPHInvNos + "|" + lblInvNo.Text)
            End If
        Next
        SelectedInvoicesforPayment = IIf(FPHInvNos <> String.Empty, True, False)
    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '  Me.lblError.Text = ""
       If Me.btnSave.CommandName = "PAY" Then
            If SelectedInvoicesforPayment() Then
                Me.trChqDetail.Visible = True
                Me.trChqSave.Visible = True
                txtChequeTotal1.Text = InvoicedAmt
                Me.gvDetails.Enabled = False
            Else
                Me.gvDetails.Enabled = True
                '     Me.lblError.Text = "Please select the invoices or check the cheque amount"
                usrMessageBar.ShowNotification("Please select the invoices or check the cheque amount", UserControls_usrMessageBar.WarningType.Danger)
            End If

        End If

    End Sub
 
    Protected Sub rblFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblFilter.SelectedIndexChanged
        Me.btnSave.Visible = False
        Me.trChqDetail.Visible = False
        Me.trChqSave.Visible = False
        If rblFilter.SelectedValue = "P" Then 'pending payment
            Me.btnSave.Visible = True
            Me.btnSave.CommandName = "PAY"
            Me.btnSave.Text = "Make Payment"
            If Me.txtChequeTotal1.Text.Trim <> "" Then
                Me.trChqDetail.Visible = True
                Me.trChqSave.Visible = True
            End If
        ElseIf rblFilter.SelectedValue = "PD" Then 'paid
            'gvDetails.Columns(0).Visible = False
        Else
            Me.btnSave.Visible = False
        End If
        BuildListView()
        gridbind(True)
        Me.gvDetails.Enabled = True
    End Sub
    Protected Sub lblPrintReciept_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblINV_NO As Label = sender.Parent.parent.findcontrol("lblItemCol4")
        Dim lblBSU As Label = sender.Parent.parent.findcontrol("lblItemCol1")
        Session("ReportSource") = FEEPERFORMAINVOICE.PrintReceipt("'" & lblINV_NO.Text & "'", True, lblBSU.Text, False, Session("sUsr_name"))
        h_print.Value = "print"
    End Sub

    Private Function PrintReceipt(ByVal vINV_NO As String, ByVal USR_NAME As String) As MyReportClass
        Dim repSource As New MyReportClass
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim RptNameComp As String = "../../fees/Reports/RPT/rptFeeGroupProformaInvoice.rpt"
        Dim str_Sql, strFilter As String
        strFilter = "  FPH_INOICENO in ('" & vINV_NO & "')"
        str_Sql = "select * from FEES.VW_OSO_FEES_GROUPPROFORMA_REPORT WHERE " & strFilter


        Dim params As New Hashtable
        params("BSU_ID") = Session("sBsuid")
        params("UserName") = USR_NAME
        repSource.Parameter = params
        repSource.IncludeBSUImage = True

        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.Connection = New SqlConnection(str_conn)
        cmd.CommandType = CommandType.Text
        repSource.Command = cmd

        Dim repSourceSubRep(0) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        Dim cmdSubStudentSummary As New SqlCommand
        cmdSubStudentSummary.CommandText = "[FEES].[GroupProformaInvoiceRPT]"
        cmdSubStudentSummary.CommandType = Data.CommandType.StoredProcedure
        Dim param(1) As SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@InvoiceNo", vINV_NO.Trim, SqlDbType.VarChar)
        cmdSubStudentSummary.Connection = New SqlConnection(str_conn)
        cmdSubStudentSummary.Parameters.Add(param(0))
        cmdSubStudentSummary.Connection = New SqlConnection(str_conn)
        repSourceSubRep(0).Command = cmdSubStudentSummary
        repSource.SubReport = repSourceSubRep

        repSource.ResourceName = RptNameComp

        Return repSource
    End Function

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblINV_NO As Label = sender.Parent.parent.findcontrol("lblItemCol3")
        Session("ReportSource") = PrintReceipt(lblINV_NO.Text, Session("sUsr_name"))
        h_print.Value = "print"
    End Sub

    'Private Function GetSelected() As Boolean
    '    GetSelected = False
    '    FPHInvNos = String.Empty
    '    InvTotals = 0
    '    'Dim dtgvDetails As DataTable = DirectCast(ViewState("GridTable"), DataTable)
    '    For Each gvr As GridViewRow In Me.gvDetails.Rows
    '        Dim chk As HtmlInputCheckBox = DirectCast(gvr.FindControl("chkControl"), HtmlInputCheckBox)
    '        Dim lblAmount As Label = DirectCast(gvr.FindControl("lblItemCol5"), Label)
    '        Dim lblInvNo As Label = DirectCast(gvr.FindControl("lblItemCol3"), Label)
    '        If chk.Checked Then
    '            InvTotals = InvTotals + CDbl(lblAmount.Text)
    '            FPHInvNos = IIf(FPHInvNos = String.Empty, (lblInvNo.Text), FPHInvNos + "|" + (lblInvNo.Text))
    '        End If
    '    Next
    '    GetSelected = IIf((FPHInvNos <> String.Empty) And (Val(txtChequeTotal1.Text) = InvTotals), True, False)
    'End Function

    Protected Sub btnSavePay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavePay.Click
        '  Me.lblError.Text = ""
        If Val(Me.txtChequeTotal1.Text) = 0 Then
            '  Me.lblError.Text = "Please enter the cheque amount"
            usrMessageBar.ShowNotification("Please enter the cheque amount", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        ElseIf Me.txtChqno1.Text.Trim = "" Then
            '   Me.lblError.Text = "Please enter the cheque no."
            usrMessageBar.ShowNotification("Please enter the cheque no.", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        ElseIf h_Bank1.Value = "" Then
            ' Me.lblError.Text = "Please select the bank"
            usrMessageBar.ShowNotification("Please select the bank", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        ElseIf Me.txtChqDate1.Text.Trim = "" Then
            'Me.lblError.Text = "Please select the cheque date"
            usrMessageBar.ShowNotification("Please select the cheque date", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        ElseIf Val(Me.txtChequeTotal1.Text) = InvoicedAmt Then
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retval As Int16 = MakePayments(stTrans)
                If retval = 1 Then
                    stTrans.Commit()
                    'Me.lblError.Text = "Payment details updated successfully"
                    usrMessageBar.ShowNotification("Payment details updated successfully", UserControls_usrMessageBar.WarningType.Danger)
                    ClearPaymentDetail()
                    rblFilter.SelectedValue = "PD"
                    rblFilter_SelectedIndexChanged(sender, e)
                    Me.gvDetails.Enabled = True
                Else
                    stTrans.Rollback()
                    'Me.lblError.Text = "Unable to Save Payment details"
                    usrMessageBar.ShowNotification("Unable to Save Payment details", UserControls_usrMessageBar.WarningType.Danger)
                End If
            Catch ex As Exception
                ' Me.lblError.Text = ex.Message
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try
        Else
            '  Me.lblError.Text = "Check the selected invoices amount and Cheque amount"
            usrMessageBar.ShowNotification("Check the selected invoices amount and Cheque amount", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub
    Private Function MakePayments(ByRef Trans As SqlTransaction) As Int16
        Dim pParms(9) As SqlClient.SqlParameter
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.TinyInt)
        pParms(1).Direction = ParameterDirection.ReturnValue
        pParms(0) = New SqlClient.SqlParameter("@InvoiceNos", SqlDbType.VarChar)
        pParms(0).Value = FPHInvNos
        pParms(2) = New SqlClient.SqlParameter("@FPP_Bank_ID", SqlDbType.Int)
        pParms(2).Value = h_Bank1.Value
        pParms(3) = New SqlClient.SqlParameter("@FPP_Emirate_ID", SqlDbType.VarChar)
        pParms(3).Value = Me.ddlEmirate1.SelectedValue
        pParms(4) = New SqlClient.SqlParameter("@FPP_CHQ_No", SqlDbType.VarChar, 20)
        pParms(4).Value = Me.txtChqno1.Text.Trim

        pParms(5) = New SqlClient.SqlParameter("@FPP_CHQ_Amount", SqlDbType.Decimal)
        pParms(5).Value = Val(txtChequeTotal1.Text)
        pParms(6) = New SqlClient.SqlParameter("@FPP_CHQ_Date", SqlDbType.VarChar)
        pParms(6).Value = Me.txtChqDate1.Text
        pParms(7) = New SqlClient.SqlParameter("@FPP_LOG_User", SqlDbType.VarChar, 20)
        pParms(7).Value = Session("sUsr_name")
        pParms(8) = New SqlClient.SqlParameter("@FPP_IsGroupInvoice", SqlDbType.Bit)
        pParms(8).Value = True

        Dim retval As Int16
        retval = Convert.ToInt16(SqlHelper.ExecuteNonQuery(Trans, CommandType.StoredProcedure, "FEES.SaveGroupProformaPayment", pParms))
        MakePayments = pParms(1).Value
    End Function

    Sub ClearPaymentDetail()
        Me.txtBank1.Text = ""
        h_Bank1.Value = ""
        Me.txtChequeTotal1.Text = ""
        Me.txtChqDate1.Text = ""
        Me.txtChqno1.Text = ""
        h_Chequeid.Value = ""
        'Me.trChqDetail.Visible = False
        'Me.trChqSave.Visible = False
    End Sub

    Protected Sub txtCompanyDescr_TextChanged(sender As Object, e As EventArgs)
        Me.rblFilter.SelectedValue = "P"
        rblFilter_SelectedIndexChanged(sender, e)
    End Sub
End Class

