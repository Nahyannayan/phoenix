Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class FEEAdjustmentREQ_APPView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                gvFEEAdjustments.Attributes.Add("bordercolor", "#1b80b6")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ADJUSTMENTS_REQ AndAlso _
                ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ADJUSTMENTS_APP AndAlso _
                ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ADJUSTMENTS_REQ_NEW) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If

                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS_REQ
                        hlAddNew.NavigateUrl = "FeeAdjustment_REQ.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                        lblHeader.Text = "Fee Adjustment Request"
                        hlAddNew.Visible = True
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS_APP
                        hlAddNew.Visible = False
                        lblHeader.Text = "Fee Adjustment Approval"
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS_REQ_NEW
                        hlAddNew.NavigateUrl = "FeeAdjustment_REQNew.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                        lblHeader.Text = "Fee Adjustment Request"
                        hlAddNew.Visible = True
                End Select
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEAdjustments.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvFEEAdjustments.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim ds As New DataSet
            Dim str_Filter As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim lstrCondn6 As String = String.Empty
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            If gvFEEAdjustments.Rows.Count > 0 Then
                '   --- FILTER CONDITIONS ---
                '   -- 1   STU_NAME
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtstudname")
                lstrCondn1 = Trim(Mainclass.cleanString(txtSearch.Text))
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn1)

                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtstudno")
                lstrCondn6 = Trim(Mainclass.cleanString(txtSearch.Text))
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn6)


                '   -- 1  FAR_DATE
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(Mainclass.cleanString(txtSearch.Text))
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FAR_DATE", lstrCondn2)

                '   -- 2  ACY_DESCR
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtAcademicYear")
                lstrCondn3 = Trim(Mainclass.cleanString(txtSearch.Text))
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FAR_DOCNO", lstrCondn3)

                '   -- 3   remarks
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtRemarks")
                lstrCondn4 = Trim(Mainclass.cleanString(txtSearch.Text))
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FAH_REMARKS", lstrCondn4)

                '   -- 3  reason
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtReason")
                lstrCondn5 = Trim(Mainclass.cleanString(txtSearch.Text))
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ARM_DESCR", lstrCondn5)
            End If

            Dim str_cond As String = String.Empty
            Dim vStatus As String = String.Empty
            If radApproved.Checked Then
                vStatus = "A"
            ElseIf radReject.Checked Then
                vStatus = "R"
            ElseIf radRequested.Checked Then
                vStatus = "N"
            End If
            'FEES.FEEADJREQUEST_H.FAR_REMARKS FAH_REMARKS,
            Dim selFilter As String = " FAR_APPRSTATUS = '" & vStatus & "' AND  ISNULL(FAR_bDeleted,0)=0" _
            & " AND FAR_BSU_ID ='" & Session("sBSUID") & "' "

            Dim strGroupBy As String = " GROUP BY FAR_ID, FAR_REMARKS, FAR_DATE, STU_NAME, ACY_DESCR, FAR_BSU_ID,FAR_DOCNO, ARM_DESCR,STU_NO, " & _
            " FAR_EVENT_REF_ID, FAR_EVENT_SOURCE "
            If radStud.Checked Then
                str_Sql = " SELECT * FROM (" & _
                " SELECT FEES.FEEADJREQUEST_H.FAR_ID FAH_ID,  " & _
                " (FEES.FEEADJREQUEST_H.FAR_REMARKS +  " & _
                " (CASE WHEN FEES.FEEADJREQUEST_H.FAR_EVENT_SOURCE = 'TCSO' THEN  '.. Last Attend Date :' + " & _
                " ( SELECT replace(convert(varchar(11),TCM_LASTATTDATE,106),' ','/')TCM_LASTATTDATE " & _
                " FROM OASIS.DBO.TCM_M WHERE TCM_ID  = FEES.FEEADJREQUEST_H.FAR_EVENT_REF_ID )  ELSE '' END )) AS FAH_REMARKS, " & _
                " FEES.FEEADJREQUEST_H.FAR_DATE FAH_DATE, VW_OSO_STUDENT_M.STU_NAME, " & _
                " VW_OSO_STUDENT_M.ACY_DESCR, FEES.FEEADJREQUEST_H.FAR_BSU_ID FAH_BSU_ID,VW_OSO_STUDENT_M.STU_NO, " & _
                " SUM(FEES.FEEADJREQUEST_S.FRS_AMOUNT) AMOUNT ,FEES.FEEADJREQUEST_H.FAR_DOCNO, ARM.ARM_DESCR" & _
                " FROM FEES.FEEADJREQUEST_H INNER JOIN VW_OSO_STUDENT_M  " & _
                " ON FEES.FEEADJREQUEST_H.FAR_STU_ID = VW_OSO_STUDENT_M.STU_ID " & _
                " INNER JOIN FEES.FEEADJREQUEST_S ON FAR_ID = FRS_FAR_ID INNER JOIN" & _
                " FEES.ADJ_REASON_M AS ARM ON FEES.FEEADJREQUEST_H.FAR_EVENT = ARM.ARM_ID" & _
                " WHERE isnull(FAR_STU_TYP,'S') = 'S' AND " & selFilter & strGroupBy & _
                ")A WHERE 1=1 " & str_Filter
            ElseIf radEnq.Checked Then
                str_Sql = " SELECT * FROM (" & _
                " SELECT FEES.FEEADJREQUEST_H.FAR_ID FAH_ID,  " & _
                " (FEES.FEEADJREQUEST_H.FAR_REMARKS +  " & _
                " (CASE WHEN FEES.FEEADJREQUEST_H.FAR_EVENT_SOURCE = 'TCSO' THEN  '.. Last Attend Date :' + " & _
                " ( SELECT replace(convert(varchar(11),TCM_LASTATTDATE,106),' ','/')TCM_LASTATTDATE " & _
                " FROM OASIS.DBO.TCM_M WHERE TCM_ID  = FEES.FEEADJREQUEST_H.FAR_EVENT_REF_ID )  ELSE '' END )) AS FAH_REMARKS, " & _
                " FEES.FEEADJREQUEST_H.FAR_DATE FAH_DATE, FEES.vw_OSO_ENQUIRY_COMP.STU_NAME, " & _
                " FEES.vw_OSO_ENQUIRY_COMP.ACY_DESCR, FEES.FEEADJREQUEST_H.FAR_BSU_ID FAH_BSU_ID, FEES.vw_OSO_ENQUIRY_COMP.STU_NO," & _
                " SUM(FEES.FEEADJREQUEST_S.FRS_AMOUNT) AMOUNT,FEES.FEEADJREQUEST_H.FAR_DOCNO,ARM.ARM_DESCR " & _
                " FROM FEES.FEEADJREQUEST_H INNER JOIN FEES.vw_OSO_ENQUIRY_COMP  " & _
                " ON FEES.FEEADJREQUEST_H.FAR_STU_ID = FEES.vw_OSO_ENQUIRY_COMP.STU_ID " & _
                " INNER JOIN FEES.FEEADJREQUEST_S ON FAR_ID = FRS_FAR_ID INNER JOIN" & _
                " FEES.ADJ_REASON_M AS ARM ON FEES.FEEADJREQUEST_H.FAR_EVENT = ARM.ARM_ID" & _
                " WHERE FAR_STU_TYP = 'E' AND " & selFilter & strGroupBy & _
                ")A WHERE 1=1 " & str_Filter
            End If
            Dim str_orderby As String = " ORDER BY FAR_DOCNO DESC, FAH_DATE DESC, STU_NAME "

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
            CommandType.Text, str_Sql & str_orderby)
            gvFEEAdjustments.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEAdjustments.DataBind()
                Dim columnCount As Integer = gvFEEAdjustments.Rows(0).Cells.Count

                gvFEEAdjustments.Rows(0).Cells.Clear()
                gvFEEAdjustments.Rows(0).Cells.Add(New TableCell)
                gvFEEAdjustments.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEAdjustments.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEAdjustments.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEAdjustments.DataBind()
            End If
            'gvJournal.DataBind()
            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtstudname")
            txtSearch.Text = lstrCondn1

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtAcademicYear")
            txtSearch.Text = lstrCondn3

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn4

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtReason")
            txtSearch.Text = lstrCondn5

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEAdjustments.PageIndexChanging
        gvFEEAdjustments.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFEEAdjustments.RowDataBound
        Try
            Dim lblFAH_ID As New Label
            lblFAH_ID = TryCast(e.Row.FindControl("lblFAH_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblFAH_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS_REQ
                        hlEdit.NavigateUrl = "FeeAdjustment_REQ.aspx?FAH_ID=" & Encr_decrData.Encrypt(lblFAH_ID.Text) & _
                        "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS_APP
                        hlEdit.NavigateUrl = "FeeAdjustment_App.aspx?FAH_ID=" & Encr_decrData.Encrypt(lblFAH_ID.Text) & _
                        "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS_REQ_NEW
                        hlEdit.NavigateUrl = "FeeAdjustment_REQNew.aspx?FAH_ID=" & Encr_decrData.Encrypt(lblFAH_ID.Text) & _
                        "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                End Select
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub radStud_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStud.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radEnq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnq.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radRequested_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radRequested.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radApproved_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radApproved.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radReject_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radReject.CheckedChanged
        GridBind()
    End Sub
    
End Class
