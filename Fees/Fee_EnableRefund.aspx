﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="Fee_EnableRefund.aspx.vb" Inherits="Fees_Fee_EnableRefund" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" language="javascript">
        function Confirmation() {
            var response = Confirm("Do you want to Enable Fee Refund for the Student?");
            if (!response)
                return false;
            else
                return true;
        }
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Enable Fee Refund"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td align="left" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student Type</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:RadioButton ID="rbLEnrollment" TabIndex="1" runat="server" Text="Enrollment"
                                AutoPostBack="True" Checked="True" GroupName="Ledger" CssClass="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="rbLEnquiry" runat="server" Text="Enquiry" AutoPostBack="True"
                                GroupName="Ledger" CssClass="field-label"></asp:RadioButton>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student</span>
                        </td>
                        <td align="left" colspan="3">
                            <uc1:usrSelStudent ID="UsrSelStudent1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">From Date</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True" TabIndex="2">
                            </asp:TextBox><asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="4" /><asp:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                                    Enabled="True" Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                                </asp:CalendarExtender>
                        </td>
                        <td align="left"><span class="field-label">To Date</span>
                        </td>

                        <td align="left">
                            <asp:TextBox ID="txtTo" runat="server" AutoPostBack="True" TabIndex="2">
                            </asp:TextBox><asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="4" /><asp:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                    Enabled="True" Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                                </asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Student</span>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblName" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" class="matters"><span class="field-label">DOJ</span>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblDOJ" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Grade</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:Label ID="lblGrade" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnEFR" runat="server" CssClass="button"
                                Text="Enable Fee Refund" OnClientClick='return confirm("Are you sure you want to Enable Refund?");' />
                            <asp:Button ID="btnLedger" runat="server" CssClass="button" Text="Show Ledger" />
                        </td>
                    </tr>
                    <tr id="trLedger" runat="server">
                        <td id="Td1" runat="server" align="center" colspan="4">
                            <asp:GridView ID="gvStLedger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                EmptyDataText="No Data" ShowFooter="True" Width="100%" CssClass="table table-bordered table-row"
                                EnableModelValidation="True">
                                <Columns>
                                    <asp:BoundField DataField="DOCDATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Doc.Date">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Description">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%#Bind("GRD_DISPLAY") %>'>&#39;&gt;</asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="RECNO" HeaderText="RefNo">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="COLLECTIONTYPE" HeaderText="Collection Type">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DEBIT" DataFormatString="{0:n2}" HeaderText="Debit">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CREDIT" DataFormatString="{0:n2}" HeaderText="Credit">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Balance" DataFormatString="{0:n2}" HeaderText="Balance">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle HorizontalAlign="Right" VerticalAlign="Bottom" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
