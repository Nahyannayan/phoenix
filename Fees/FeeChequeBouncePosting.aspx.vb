Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj

Partial Class FEEAdjustmentPosting
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = OASISConstants.Gemstitle
        If Page.IsPostBack = False Then
            If Request.QueryString("editerror") <> "" Then
                'lblError.Text = "Record already posted/Locked"
                usrMessageBar.ShowNotification("Record already posted/Locked", UserControls_usrMessageBar.WarningType.Danger)
            Else
                'lblError.Text = ""
            End If
            Dim MainMnu_code As String
            Dim menu_rights As Integer
            Dim datamode As String = "none"
            'collect the url of the file to be redirected in view state
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                datamode = ""
            End If
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If Session("sUsr_name") = "" Or MainMnu_code <> OASISConstants.MNU_FEE_CHEQUE_BOUNCE_POST Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Response.CacheControl = "no-cache"
                'calling pageright class to get the access rights
                menu_rights = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, datamode)
                txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                chkEmail.Checked = True
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                gvFeeAdjustmentDet.Attributes.Add("bordercolor", "#1b80b6")
                gridbind()
            End If
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFeeAdjustmentDet.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvFeeAdjustmentDet.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid("mnu_1_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid("mnu_2_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid("mnu_3_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid("mnu_5_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid("mnu_6_img", str_Sid_img(2))

        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid("mnu_7_img", str_Sid_img(2))

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFeeAdjustmentDet.RowDataBound
        Try
            Dim lblFCR_ID As New Label
            lblFCR_ID = TryCast(e.Row.FindControl("lblFCR_ID"), Label)
            Dim hlview As New HyperLink
            Dim datamode As String = Encr_decrData.Encrypt("view")
            Dim MainMnu_code As String
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If (lblFCR_ID IsNot Nothing) Then
                MainMnu_code = Encr_decrData.Encrypt(OASISConstants.MNU_FEE_CHEQUE_BOUNCE)
                hlview.NavigateUrl = "FEECHEQUEBOUNCEDetailedView.aspx?FCR_ID=" & Encr_decrData.Encrypt(lblFCR_ID.Text) & _
               "&MainMnu_code=" & MainMnu_code & "&datamode=" & datamode
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function PostAllSelected(ByVal Page As Control, ByVal conn As SqlConnection, ByVal trans As SqlTransaction) As Integer
        Dim retval As Integer
        Dim nochk As Boolean = False
        Session("arrPostedFEECHQBOUNCE") = New ArrayList
        For Each gvr As GridViewRow In gvFeeAdjustmentDet.Rows
            Dim lblFCR_ID As Label = CType(gvr.FindControl("lblFCR_ID"), Label)
            Dim chkPost As CheckBox = CType(gvr.FindControl("chkPost"), CheckBox)
            If Not lblFCR_ID Is Nothing Then
                If IsNumeric(lblFCR_ID.Text) And chkPost.Checked Then
                    nochk = True
                    retval = F_POSTFEECHEQUERETURN(lblFCR_ID.Text, conn, trans)
                    If retval <> 0 Then
                        Exit For
                    End If
                End If
            End If
        Next
        If Not nochk Then
            Return -100
        End If
        Return retval
    End Function

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim stTrans As SqlTransaction
        Try
            objConn.Open()
            stTrans = objConn.BeginTransaction
            Try
                Dim iReturnvalue As Integer = PostAllSelected(Me.Page, objConn, stTrans)

                If (iReturnvalue = 0) Then
                    'stTrans.Rollback()
                    stTrans.Commit()
                    'lblError.Text = "Cheque Return Successfully Posted"
                    usrMessageBar.ShowNotification("Cheque Return Successfully Posted", UserControls_usrMessageBar.WarningType.Success)
                    Exit Sub
                    btnPrintReciept.Visible = True
                    'If chkPrint.Checked Then
                    '    PrintPostedVouchers()
                    'End If
                    gridbind()
                Else
                    stTrans.Rollback()
                    If iReturnvalue = -100 Then
                        'lblError.Text = "Please select atleast 1 for Posting..."
                        usrMessageBar.ShowNotification("Please select atleast 1 for Posting...", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    Else
                        'lblError.Text = getErrorMessage(iReturnvalue)
                        usrMessageBar.ShowNotification(getErrorMessage(iReturnvalue), UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                End If
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                'lblError.Text = getErrorMessage("1000")
                usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            End Try
        Catch ex As Exception
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            objConn.Close() 'Finally, close the connection
        End Try
        gridbind()
    End Sub

    Private Sub PrintPostedVouchers()
        Dim arrList As ArrayList = Session("arrPostedFEECHQBOUNCE")
        Dim ienum As IEnumerator = arrList.GetEnumerator
        Dim vDocNo As String = String.Empty
        Dim comma As String = String.Empty
        While ienum.MoveNext
            vDocNo = vDocNo & comma & "'" & ienum.Current & "'"
            comma = ","
        End While
        'Session("ReportSource") = AccountsReports.JournalVouchers(vDocNo, "", Session("sBSUID"), "", vDocNo, "JV")
        'Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
    End Sub

    Private Function F_POSTFEECHEQUERETURN(ByVal vFAH_ID As Integer, ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, Optional ByVal SmsYN As Boolean = False) As Integer
        Dim cmd As New SqlCommand("[FEES].[F_POSTFEECHEQUERETURN]", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFAH_ID As New SqlParameter("@FCR_ID", SqlDbType.Int)
        sqlpFAH_ID.Value = vFAH_ID
        cmd.Parameters.Add(sqlpFAH_ID)

        Dim sqlpDate As New SqlParameter("@Date", SqlDbType.DateTime)
        sqlpDate.Value = CDate(txtDate.Text)
        cmd.Parameters.Add(sqlpDate)

        Dim sqlpSMSYN As New SqlParameter("@bSendSMSYN", SqlDbType.Bit)
        sqlpSMSYN.Value = ChkSMS.Checked
        cmd.Parameters.Add(sqlpSMSYN)

        Dim sqlpEmailYN As New SqlParameter("@bSendEmail", SqlDbType.Bit)
        sqlpEmailYN.Value = chkEmail.Checked
        cmd.Parameters.Add(sqlpEmailYN)

        Dim sqlpNewDOCNo As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
        sqlpNewDOCNo.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlpNewDOCNo)

        Dim iReturnvalue As Integer
        Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
        retValParam.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(retValParam)

        cmd.ExecuteNonQuery()

        iReturnvalue = retValParam.Value
        If iReturnvalue = 0 Then
            Session("arrPostedFEECHQBOUNCE").add(sqlpNewDOCNo.Value)
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, sqlpNewDOCNo.Value, "FEE Adjustment Posting", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
        End If
        Return iReturnvalue
    End Function

    Private Sub gridbind()
        Try
            Dim str_Filter As String = String.Empty
            Dim lstrStuid As String = String.Empty
            Dim lstrStuName As String = String.Empty
            Dim lstrDate As String = String.Empty
            Dim lstrChqNo As String = String.Empty
            Dim lstrBank As String = String.Empty

            Dim lstrOpr As String = String.Empty
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox

            If gvFeeAdjustmentDet.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   Stuid
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtSTU_NO")
                If txtSearch IsNot Nothing Then
                    lstrStuid = Trim(txtSearch.Text)
                    If (lstrStuid <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrStuid)
                End If
                '   -- 2  DocDate
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtSTUNAME")
                If txtSearch IsNot Nothing Then
                    lstrStuName = txtSearch.Text
                    If (lstrStuName <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrStuName)
                End If
                '   -- 3  Bank AC
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtDate")
                If txtSearch IsNot Nothing Then
                    lstrDate = txtSearch.Text
                    If (lstrDate <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCR_DATE", lstrDate)
                End If
                '   -- 5  Narration
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtCHQNO")
                If txtSearch IsNot Nothing Then
                    lstrChqNo = txtSearch.Text
                    If (lstrChqNo <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCQ_CHQNO", lstrChqNo)
                End If
                '   -- 7  Currency
                larrSearchOpr = h_Selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtBank")
                If txtSearch IsNot Nothing Then
                    lstrBank = txtSearch.Text
                    If (lstrBank <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "BNK_DESCRIPTION", lstrBank)
                End If
            End If
            'Dim str_Sql As String
            'Dim str_orderBy As String = " ORDER BY FCR_DATE DESC"
            'str_Sql = "select * from FEES.FeeChequeListBounced  " & _
            '" WHERE  isnull( FCR_Bposted, 0) = 0  AND (FCR_BSU_ID = '" & Session("sBSUID") & "') " & str_Filter
            Dim pParms(6) As SqlClient.SqlParameter
            Dim ds As New DataSet
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuId")
            pParms(1) = New SqlClient.SqlParameter("@bPOSTED", SqlDbType.Bit)
            pParms(1).Value = False
            pParms(2) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 20)
            pParms(2).Value = lstrStuid
            pParms(3) = New SqlClient.SqlParameter("@STU_NAME", SqlDbType.VarChar, 100)
            pParms(3).Value = lstrStuName
            pParms(4) = New SqlClient.SqlParameter("@FCR_DATE", SqlDbType.VarChar, 20)
            pParms(4).Value = lstrDate
            pParms(5) = New SqlClient.SqlParameter("@FCQ_CHQNO", SqlDbType.VarChar, 20)
            pParms(5).Value = lstrChqNo
            pParms(6) = New SqlClient.SqlParameter("@BNK_DESCRIPTION", SqlDbType.VarChar, 100)
            pParms(6).Value = lstrBank

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, "FEES.GET_CHEQUE_LIST_BOUNCED", pParms)

            'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql & str_orderBy)
            gvFeeAdjustmentDet.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFeeAdjustmentDet.DataBind()
                Dim columnCount As Integer = gvFeeAdjustmentDet.Rows(0).Cells.Count
                gvFeeAdjustmentDet.Rows(0).Cells.Clear()
                gvFeeAdjustmentDet.Rows(0).Cells.Add(New TableCell)
                gvFeeAdjustmentDet.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFeeAdjustmentDet.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFeeAdjustmentDet.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFeeAdjustmentDet.DataBind()
            End If
            txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtSTU_NO")
            txtSearch.Text = lstrStuid

            txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtSTUNAME")
            txtSearch.Text = lstrStuName

            txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrDate

            txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtCHQNO")
            txtSearch.Text = lstrChqNo

            txtSearch = gvFeeAdjustmentDet.HeaderRow.FindControl("txtBank")
            txtSearch.Text = lstrBank
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFeeAdjustmentDet.PageIndexChanging
        gvFeeAdjustmentDet.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub btnPrintReciept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintReciept.Click
        PrintPostedVouchers()
    End Sub

End Class
