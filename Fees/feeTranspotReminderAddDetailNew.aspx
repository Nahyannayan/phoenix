<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="feeTranspotReminderAddDetailNew.aspx.vb" Inherits="Fees_feeTranspotReminderAddDetailNew" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
function CheckForPrint() {
    if (document.getElementById('<%= h_print.ClientID %>').value != '') {
        document.getElementById('<%= h_print.ClientID %>').value = '';
        //showModelessDialog('../Reports/ASPX Report/RptViewerModalView.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
        return ShowWindowWithClose('../Reports/ASPX Report/RptViewerModalView.aspx', 'search', '55%', '85%')
        return false;
    }
}
    );
function ChangeCheckBoxState(id, checkState) {
    var cb = document.getElementById(id);
    if (cb != null)
        cb.checked = checkState;
}

function ChangeAllCheckBoxStates(checkState) {
    var chk_state = document.getElementById("ChkSelAll").checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
            if (document.forms[0].elements[i].type == 'checkbox') {
                document.forms[0].elements[i].checked = chk_state;
            }
    }
}
function CheckBoxListSelect(cbControl, state) {
    var chkBoxList = document.getElementById(cbControl);
    var chkBoxCount = chkBoxList.getElementsByTagName("input");
    for (var i = 0; i < chkBoxCount.length; i++) {
        chkBoxCount[i].checked = state;
    }
    document.getElementById('<%= h_checkAll.ClientID %>').value = state;

    return false;
}
    </script>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server" Text="Transport Fee Reminder Detail New..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" CssClass="error"></asp:Label>--%>
                <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select Business Unit</span>
                        </td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"
                                TabIndex="5" OnSelectedIndexChanged="ddlBusinessunit_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Reminder Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtAsOnDate" runat="server" AutoPostBack="True">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAsOnDate"
                                ErrorMessage="Please Select Date" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Reminder</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReminder" runat="server">
                                <asp:ListItem Value="1">First Reminder</asp:ListItem>
                                <asp:ListItem Value="2">Second Reminder</asp:ListItem>
                                <asp:ListItem Value="3">Third Reminder</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td id="trFind" runat="server" align="center" colspan="4">
                            <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Find" />
                        </td>
                    </tr>
                    <tr id="trFilter" runat="server" visible="false">
                        <td align="center" colspan="4">
                            <asp:RadioButton ID="radAll" runat="server" GroupName="Email" Text="All" AutoPostBack="True" CssClass="field-label"
                                Checked="True" />
                            <asp:RadioButton ID="radSent" runat="server" GroupName="Email" Text="Emailed" AutoPostBack="True" CssClass="field-label" />
                            <asp:RadioButton ID="radNotSent" runat="server" GroupName="Email" Text="Not Emailed"
                                AutoPostBack="True" CssClass="field-label" />
                            <asp:RadioButton ID="radEmailFailed" runat="server" GroupName="Email" Text="Failed"
                                AutoPostBack="True" CssClass="field-label" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <center>
                    <table width="100%">
                        <tr>
                            <td align="left" width="15%">
                               <span class="field-label"> Grade</span>
                            </td>
                            <td align="left" width="15%">
                               <span class="field-label"> Bus Nos.</span>
                            </td>
                             <td align="left" width="15%">
                               <span class="field-label">  StudentID</span>
                            </td>
                            <td align="left" width="15%">
                               <span class="field-label"> Student Name</span>
                            </td>
                            <td align="left" width="15%">
                               <span class="field-label"> Minimum NetOS</span>
                            </td>
                            <td align="left" width="15%">
                               <span class="field-label"> As On</span></td>
                             <td align="left" width="10%">
                              
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="15%">
                                <telerik:RadComboBox ID="cblGrade" runat="server" RenderMode="Lightweight" EmptyMessage="<--Select-->" DataTextField="GRM_DESCR"
                                    DataValueField="GRM_ID" >
                                    <ItemTemplate>
                                        <div>
                                            <asp:CheckBox runat="server" ID="chkComboItem" />
                                            <asp:Label runat="server" ID="lblComboItem" AssociatedControlID="chkComboItem" Text='<%# Eval("GRM_DESCR") %>'>
                                            </asp:Label>
                                            <asp:HiddenField ID="hdnComboItem" Visible="true" runat="server" Value='<%# Eval("GRM_ID") %>' />
                                        </div>
                                    </ItemTemplate>
                                    <%-- <HeaderTemplate>
                                                <asp:CheckBox ID="chkAll" runat="server" AutoPostBack="True" 
                                                    oncheckedchanged="chkAll_CheckedChanged" Text="Select All" />
                                            </HeaderTemplate>--%>
                                </telerik:RadComboBox>
                            </td>
                            <td align="left" width="15%">
                                <telerik:RadComboBox ID="cblBusNos" runat="server" RenderMode="Lightweight" EmptyMessage="<--Select-->" DataTextField="BNO_DESCR"
                                    DataValueField="BNO_ID" >
                                    <ItemTemplate>
                                        <div>
                                            <asp:CheckBox runat="server" ID="chkComboItem" />
                                            <asp:Label runat="server" ID="lblComboItem" AssociatedControlID="chkComboItem" Text='<%# Eval("BNO_DESCR") %>'>
                                            </asp:Label>
                                            <asp:HiddenField ID="hdnComboItem" runat="server" Value='<%# Eval("BNO_ID") %>' />
                                        </div>
                                    </ItemTemplate>
                                </telerik:RadComboBox>
                            </td>
                            <td align="left" width="15%">
                                <asp:TextBox ID="txtStudNo" runat="server" >
                                </asp:TextBox>
                            </td>
                            <td align="left" width="15%">
                                <asp:TextBox ID="txtStudName" runat="server" >
                                </asp:TextBox>
                            </td>
                            <td align="left" width="15%">
                                <asp:TextBox ID="txtMinNetOS" Style="text-align: right;" runat="server" >0.00</asp:TextBox>
                            </td>
                            <td align="left" width="15%">
                                <asp:TextBox ID="txtDate" runat="server" >
                                </asp:TextBox>
                                <asp:ImageButton ID="ImgDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                    OnClientClick="return false;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDate"
                                    ErrorMessage="Please Select Date" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" width="10%">
                                <asp:Button ID="btnView" runat="server" CssClass="button" Text="Enter" ValidationGroup="MAINERROR" />
                            </td>
                        </tr>
                    </table>
                </center>
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:GridView ID="gvExcel" runat="server" Width="100%" EmptyDataText="No Accounts are Listing"
                                AutoGenerateColumns="False" CssClass="table table-bordered table-row" DataKeyNames="FRD_bSMS">
                                <RowStyle CssClass="griditem"></RowStyle>
                                <EmptyDataRowStyle Wrap="True"></EmptyDataRowStyle>
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <input id="ChkSelAll" name="ChkSelAll" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="ChkSelect" type="checkbox" runat="server" value='<%# Bind("STU_ID") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="STU_NO" HeaderText="StudentID"></asp:BoundField>
                                    <asp:BoundField DataField="STU_NAME" HeaderText="Student Name"></asp:BoundField>
                                    <asp:BoundField DataField="GRM_DESCR" HeaderText="Grade"></asp:BoundField>
                                    <asp:BoundField DataField="BUS_NO" HeaderText="BusNo"></asp:BoundField>
                                    <asp:BoundField DataField="AMOUNT" HeaderText="Total Due" DataFormatString="{0:0,000.00}">
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FRD_EmailStatus" HeaderText="EmailStatus" Visible="False"></asp:BoundField>
                                </Columns>
                                <FooterStyle />
                                <SelectedRowStyle></SelectedRowStyle>
                                <HeaderStyle></HeaderStyle>
                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="Trbuttons" runat="server" visible="false">
                        <td align="center">
                            <asp:CheckBox ID="ChkPrint" runat="server" Text="Print After Save" CssClass="field-label" />

                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                    <tr id="TrEmail" runat="server" visible="false">
                        <td align="center">
                            <asp:Button ID="btnEmail" runat="server" CssClass="button" Text="Send EMail" />&nbsp;
                <asp:Button ID="btnSMS" runat="server" CssClass="button" Text="Send SMS" Visible="false" />
                            &nbsp;
                <asp:Button ID="btnCancel2" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server"
                        type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden"
                            value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_Selected_Value" runat="server" type="hidden" value="=" />
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="ImgDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_checkAll" runat="server" Value="1" />
                &nbsp;<asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_Confirm" runat="server" Value="0" />

                <script type="text/javascript" lang="javascript">
                    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
                        $.fancybox({
                            type: 'iframe',
                            //maxWidth: 300,
                            href: gotourl,
                            //maxHeight: 600,
                            fitToView: true,
                            padding: 6,
                            width: w,
                            height: h,
                            autoSize: false,
                            openEffect: 'none',
                            showLoading: true,
                            closeClick: true,
                            closeEffect: 'fade',
                            'closeBtn': true,
                            afterLoad: function () {
                                this.title = '';//ShowTitle(pageTitle);
                            },
                            helpers: {
                                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                                title: { type: 'inside' }
                            },
                            onComplete: function () {
                                $("#fancybox-wrap").css({ 'top': '90px' });

                            },
                            onCleanup: function () {
                                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                                if (hfPostBack == "Y")
                                    window.location.reload(true);
                            }
                        });

                        return false;
                    }
                </script>
            </div>
        </div>
    </div>
</asp:Content>
