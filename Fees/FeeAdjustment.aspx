<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeAdjustment.aspx.vb" Inherits="Fees_FeeAdjustment" Theme="General" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <script language="javascript">
        Sys.Application.add_load(
        function CheckForPrint() {
            if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                document.getElementById('<%= h_print.ClientID %>').value = '';
                //showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                return ShowWindowWithClose('../Reports/ASPX Report/RptViewerModal.aspx', 'search', '55%', '85%')
                return false;
            }
        }
    );

        function FillDetailRemarks() {
            var HeaderRemarks;
            HeaderRemarks = document.getElementById('<%=txtHeaderRemarks.ClientID %>').value;
            if (HeaderRemarks != '') {
                document.getElementById('<%=txtDetRemarks.ClientID %>').value = HeaderRemarks;
            }
            return false;
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fee Adjustments"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr runat="server">
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" EnableViewState="False" SkinID="LabelError"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="SUBERROR"></asp:ValidationSummary>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="MAINERROR"></asp:ValidationSummary>
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="4"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Date  required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtDate">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:RadioButton ID="radStud" runat="server" AutoPostBack="true" Checked="True" GroupName="ENQ_STUD"
                                Text="Student" />
                            <asp:RadioButton ID="radEnq" runat="server" AutoPostBack="true"
                                GroupName="ENQ_STUD" Text="Enquiry" />
                            <asp:RadioButton ID="radNegtive" runat="server" Text="-Ve" GroupName="ADJTYPE" Visible="False" />
                            <asp:RadioButton ID="radPositive" runat="server" Text="+Ve" Checked="True" GroupName="ADJTYPE" Visible="False" /></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student</span></td>

                        <td align="left" colspan="3">
                            <uc1:usrSelStudent ID="UsrSelStudent1" runat="server" EnableViewState="true" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtHeaderRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Header remarks required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtHeaderRemarks">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg-lite" colspan="4">Fee Adjustments Details... </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Fee Type</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlFeeType" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Amount</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDetAmount" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Amount required"
                                ValidationGroup="SUBERROR" ControlToValidate="txtDetAmount">*</asp:RequiredFieldValidator>(-ve
                for CR entry)</td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtDetRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Detailed Remarks required"
                                ValidationGroup="SUBERROR" ControlToValidate="txtDetRemarks">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="SUBERROR" />
                            <asp:Button ID="btnSubCancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr id="tr_Deatails" runat="server">
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="FeeId">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fee Type">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFeeType" runat="server" Text='<%# bind("FEE_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# bind("FEE_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# bind("FEE_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left">
                            <asp:CheckBox ID="chkPrintAfterSave" runat="server" Text="Print After Save" CssClass="field-label"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" OnClick="btnAdd_Click" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>


                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <asp:HiddenField ID="h_GRD_ID" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>

                <script type="text/javascript" lang="javascript">
                    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
                        $.fancybox({
                            type: 'iframe',
                            //maxWidth: 300,
                            href: gotourl,
                            //maxHeight: 600,
                            fitToView: true,
                            padding: 6,
                            width: w,
                            height: h,
                            autoSize: false,
                            openEffect: 'none',
                            showLoading: true,
                            closeClick: true,
                            closeEffect: 'fade',
                            'closeBtn': true,
                            afterLoad: function () {
                                this.title = '';//ShowTitle(pageTitle);
                            },
                            helpers: {
                                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                                title: { type: 'inside' }
                            },
                            onComplete: function () {
                                $("#fancybox-wrap").css({ 'top': '90px' });

                            },
                            onCleanup: function () {
                                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                                if (hfPostBack == "Y")
                                    window.location.reload(true);
                            }
                        });

                        return false;
                    }
                </script>

            </div>
        </div>
    </div>
</asp:Content>

