﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Threading.Tasks
Imports System.Web.Configuration
Imports GemBox.Spreadsheet

Partial Class Fees_DebtFollowupUpload
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property VSgvExcelImport() As DataTable
        Get
            Return ViewState("gvExcelImport")
        End Get
        Set(ByVal value As DataTable)
            ViewState("gvExcelImport") = value
        End Set
    End Property
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F733048") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnLoadData)
    End Sub
    Protected Sub btnLoadData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoadData.Click
        ImportExcelSheet()
    End Sub
    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        System.Threading.Thread.Sleep(1000)
        h_Processeing.Value = "1"
        Dim transaction As SqlTransaction

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim intBatchNo As Int16 = Session("BATCH_NO")
        Dim task As Task
        task = System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                Try
                                                                    Dim sqlParam(3) As SqlParameter
                                                                    objConn.Open()
                                                                    transaction = objConn.BeginTransaction("SampleTransaction")
                                                                    Dim cmd As New SqlCommand
                                                                    cmd.Dispose()
                                                                    cmd = New SqlCommand("[DataImport].[DEBT_FOLLOW_UP_UPLOAD_TRAN]", objConn, transaction)
                                                                    cmd.CommandType = CommandType.StoredProcedure
                                                                    cmd.CommandTimeout = 0
                                                                    Dim retmsg As String = ""
                                                                    sqlParam(0) = Mainclass.CreateSqlParameter("@StrMode", "SAVE_COMMENT", SqlDbType.VarChar)
                                                                    cmd.Parameters.Add(sqlParam(0))
                                                                    sqlParam(1) = Mainclass.CreateSqlParameter("@BATCH_NO", intBatchNo, SqlDbType.VarChar)
                                                                    cmd.Parameters.Add(sqlParam(1))
                                                                    sqlParam(2) = Mainclass.CreateSqlParameter("@ReturnMessage", retmsg, SqlDbType.VarChar, True, 200)
                                                                    cmd.Parameters.Add(sqlParam(2))
                                                                    cmd.ExecuteNonQuery()
                                                                    retmsg = sqlParam(2).Value
                                                                    If retmsg = "" Then
                                                                        transaction.Commit()
                                                                        System.Threading.Thread.Sleep(1000)
                                                                        h_Processeing.Value = ""
                                                                    Else
                                                                        transaction.Rollback()
                                                                        UtilityObj.Errorlog("Rollback - " & retmsg, "PHOENIX")
                                                                        usrMessageBar1.ShowNotification("Rollback - " & retmsg, UserControls_usrMessageBar.WarningType.Danger)
                                                                        h_Processeing.Value = ""
                                                                        Exit Sub
                                                                    End If
                                                                Catch ex As Exception
                                                                    System.Threading.Thread.Sleep(1500)
                                                                    h_Processeing.Value = ""
                                                                    UtilityObj.Errorlog("Save debt followUp upload comment : " + ex.Message, "OASIS ACTIVITY SERVICES")
                                                                Finally
                                                                    If objConn.State = ConnectionState.Open Then
                                                                        objConn.Close()
                                                                    End If
                                                                End Try
                                                            End Sub)
    End Sub
    Protected Sub btnCommitImprt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCommitImprt.Click
        ViewState("BATCH_NO") = 0
        Session("BATCH_NO") = Nothing
        hdBATCHNO.Value = 0
        Using con As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            con.Open()
            Dim stTrans As SqlTransaction = con.BeginTransaction
            Try
                Using cmd As New SqlCommand("[DataImport].[VELIDATE_DEBT_FOLLOW_UP_UPLOAD_TRAN]", con, stTrans)
                    cmd.CommandTimeout = 0
                    cmd.CommandType = CommandType.StoredProcedure
                    Dim sqlParam(9) As SqlParameter
                    Dim dtParam As DataTable = CType(ViewState("ExcelData"), DataTable)
                    If dtParam.Columns.Contains("ERROR_MSG") Then
                        dtParam.Columns.Remove("ERROR_MSG")
                    End If

                    dtParam.AcceptChanges()
                    sqlParam(0) = New SqlParameter("@DT_XL", dtParam)
                    cmd.Parameters.Add(sqlParam(0))
                    sqlParam(1) = New SqlParameter("@BSU_ID", Session("sBsuId"))
                    cmd.Parameters.Add(sqlParam(1))
                    sqlParam(2) = New SqlParameter("@USER_ID", Session("sUsr_name"))
                    cmd.Parameters.Add(sqlParam(2))
                    sqlParam(3) = New SqlParameter("@BATCH_NO", 0)
                    sqlParam(3).Direction = ParameterDirection.Output
                    cmd.Parameters.Add(sqlParam(3))

                    Using dr As SqlDataReader = cmd.ExecuteReader()
                        Dim dttb As New DataTable
                        dttb.Load(dr)
                        If sqlParam(3).Value Is Nothing OrElse sqlParam(3).Value = 0 Then
                            stTrans.Rollback()
                        Else
                            stTrans.Commit()
                            ViewState("BATCH_NO") = sqlParam(3).Value
                            Session("BATCH_NO") = sqlParam(3).Value
                            hdBATCHNO.Value = sqlParam(3).Value
                            btnCommitImprt.Visible = False
                            btnProcess.Visible = True
                        End If
                        If Not dttb Is Nothing AndAlso dttb.Rows.Count > 0 Then
                            'Grid_StudentDetails.Columns(4).Visible = True 'shoes the error message column
                            ViewState("ExcelData") = dttb
                            VSgvExcelImport = dttb
                            BindExcel()
                        End If
                    End Using
                    con.Close()
                End Using
            Catch ex As Exception
                usrMessageBar1.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            Finally
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
            End Try
        End Using
    End Sub
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        ClearData()
    End Sub
    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ClearData()
        usrMessageBar1.ShowNotification("Upload completed successfully", UserControls_usrMessageBar.WarningType.Success)
    End Sub
    Protected Sub Grid_StudentDetails_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        Me.Grid_StudentDetails.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
    Private Sub ImportExcelSheet()
        Try
            ClearData()
            Dim FileName As String = ""
            If (FileUpload1.HasFile) Then
                Dim strFileType As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToString().ToLower()
                FileName = WebConfigurationManager.AppSettings("UploadExcelFile").ToString() & "\" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & FileUpload1.FileName.ToString()
                If strFileType = ".xls" Or strFileType = ".xlsx" Then
                    If File.Exists(FileName) Then
                        File.Delete(FileName)
                    End If
                    Me.FileUpload1.SaveAs(FileName)
                Else
                    usrMessageBar1.ShowNotification("Only Excel files are allowed", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Try
                End If
            Else
                usrMessageBar1.ShowNotification("Please select file.", UserControls_usrMessageBar.WarningType.Information)
            End If
            If File.Exists(FileName) Then
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim workbook = ExcelFile.Load(FileName)
                Dim worksheet = workbook.Worksheets.ActiveWorksheet
                Dim dtXL As New DataTable
                If worksheet.Rows.Count > 1 Then
                    dtXL = worksheet.CreateDataTable(New CreateDataTableOptions() With
                            {
                             .ColumnHeaders = True,
                             .StartRow = 0,
                             .NumberOfColumns = worksheet.Rows(0).AllocatedCells.Count,
                             .NumberOfRows = worksheet.Rows.Count,
                             .Resolution = ColumnTypeResolution.AutoPreferStringCurrentCulture
                            })
                End If
                ' File delete first 
                File.Delete(FileName)
                If Not dtXL Is Nothing AndAlso dtXL.Rows.Count > 0 Then
                    Try
                        If dtXL.Rows.Count > 0 Then
                            Dim column As DataColumn
                            column = New DataColumn
                            With column
                                .ColumnName = "ERROR_MSG"
                                .DataType = System.Type.GetType("System.String")
                                .DefaultValue = ""
                            End With
                            dtXL.Columns.Add(column)

                            ViewState("ExcelData") = dtXL
                            BindGrid()
                            'Grid_StudentDetails.Columns(4).Visible = False
                        End If
                    Catch ex As Exception
                        UtilityObj.Errorlog("Read Excel : " + ex.Message, "PHOENIX")
                        usrMessageBar1.ShowNotification("Read Excel Error : " & ex.Message.ToString(), UserControls_usrMessageBar.WarningType.Danger)
                    End Try
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("Unable to load datatable from file -  " + ex.Message, "PHOENIX")
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim dt As DataTable = CType(ViewState("ExcelData"), DataTable)
            VSgvExcelImport = dt
            If dt.Rows.Count > 0 Then
                Grid_StudentDetails.Visible = True
                Grid_StudentDetails.DataSource = dt
                Grid_StudentDetails.DataBind()
                btnReset.Visible = True
                btnCommitImprt.Visible = True
            Else
                ClearData()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("Bind grid" + ex.Message, "OASIS ACTIVITY SERVICES")
            usrMessageBar1.ShowNotification(ex.Message.ToString(), UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Sub BindExcel()
        Me.Grid_StudentDetails.DataSource = VSgvExcelImport
        Me.Grid_StudentDetails.DataBind()
    End Sub
    Private Sub ClearData()
        Grid_StudentDetails.DataSource = Nothing
        Grid_StudentDetails.DataBind()
        Grid_StudentDetails.Visible = False
        btnProcess.Visible = False
        btnReset.Visible = False
        ViewState("BATCH_NO") = ""
        Session("BATCH_NO") = ""
        hdBATCHNO.Value = 0
    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function GetLiveStatus(ByVal BATCH_NO As String) As String
        Dim Percent As Decimal = 0
        If Not BATCH_NO = "" Then
            Try
                Dim dt As DataTable
                Using con As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
                    Using cmd As New SqlCommand("[DataImport].[DEBT_FOLLOW_UP_UPLOAD_TRAN]", con)
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.AddWithValue("@StrMode", "GET_STATUS")
                        cmd.Parameters.AddWithValue("@BATCH_NO", BATCH_NO)
                        con.Open()
                        Using Adpt As New SqlDataAdapter(cmd)
                            dt = New DataTable()
                            Adpt.Fill(dt)
                            If dt.Rows.Count > 0 Then
                                Percent = dt.Rows(0)(0).ToString()
                            Else
                                Percent = 0
                            End If
                        End Using
                        con.Close()
                    End Using
                End Using
            Catch ex As Exception
                UtilityObj.Errorlog("Service Call : " + ex.Message, "OASIS ACTIVITY SERVICES")
            End Try
        End If

        Return Percent
    End Function
End Class
