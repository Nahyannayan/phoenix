<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ImportTransportRate.aspx.vb" Inherits="Fees_ImportTransportRate"
    Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <asp:Label ID="lblHeader" runat="server" Text="Import Transport Rate"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="FEE_COUNTER" />
                <br />
                <table id="Table1" align="center" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td align="left">
                            <table style="width: 100%;">
                                <tr>
                                    <td align="left">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <tr>
                                                <td align="left" style="width: 20%;"><span class="field-label">Business Unit</span></td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlTBSU" DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"
                                                        runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTBSU_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" style="width: 20%;"><span class="field-label">Academic Year</span>
                                                </td>
                                                <td align="left" style="width: 30%;">
                                                    <asp:DropDownList ID="ddlACDYear" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"><span class="field-label">Charge & Collection schedule</span>
                                                </td>
                                                <td align="left">
                                                    <asp:CheckBox ID="chkDefault" Text="Set default" AutoPostBack="true" runat="server" />
                                                </td>
                                                <td align="left"><span class="field-label">Pro-Rata(Join)</span></td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlJoinPR" runat="server"></asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"><span class="field-label">Pro-Rata(Discontinue)</span></td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlDiscontinuePR" runat="server"></asp:DropDownList>
                                                </td>
                                                <td align="left"><span class="field-label">Collection Schedule</span></td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlCollSchedule" runat="server"></asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"><span class="field-label">Select File</span></td>
                                                <td align="left">
                                                    <asp:FileUpload ID="FileUpload2" runat="server" /></td>
                                                <td align="left" colspan="2">
                                                    <asp:HyperLink ID="lnkTrRateFormat" runat="server">Get the format file here</asp:HyperLink></td>

                                            </tr>
                                            <tr>
                                                <td align="center" colspan="4">
                                                    <asp:Button ID="btnImpTrRate" runat="server" CssClass="button" OnClick="btnImpTrRate_Click"
                                                        Text="Import" />

                                                    <asp:Button ID="btntrProceed" runat="server" CssClass="button" OnClick="btntrProceed_Click"
                                                        Text="Proceed" />
                                                    <asp:Button ID="btnTrCommit" runat="server" CssClass="button" OnClick="btnTrCommit_Click"
                                                        Text="Commit" Visible="False" />
                                                    <asp:Button ID="btnCancelTrImport" Visible="false" runat="server" CssClass="button" Text="Cancel" /></td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="4">
                                                    <asp:Label ID="lblError3" runat="server" CssClass="error"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="4">
                                                    <asp:GridView runat="server" AllowPaging="True" AutoGenerateColumns="False" EmptyDataText="No Data"
                                                        CssClass="table table-bordered table-row" Width="100%" ID="gvTrRateImport" OnPageIndexChanging="gvTrRateImport_PageIndexChanging"
                                                        ShowFooter="True" OnRowDataBound="gvTrRateImport_RowDataBound">
                                                        <Columns>
                                                            <asp:BoundField DataField="SlNo" HeaderText="SlNo">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Loc" HeaderText="Loc">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField HeaderText="Area" DataField="Area">
                                                                <FooterStyle BackColor="#99CCFF" BorderStyle="None" HorizontalAlign="Right" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="T1NORMAL_RATE" HeaderText="Term1 Normal" DataFormatString="{0:0.00}">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="T1ONEWAY_RATE" HeaderText="Term1 OneWay" DataFormatString="{0:0.00}">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="T2NORMAL_RATE" HeaderText="Term2 Normal" DataFormatString="{0:0.00}">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="T2ONEWAY_RATE" HeaderText="Term2 OneWay" DataFormatString="{0:0.00}">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="T3NORMAL_RATE" HeaderText="Term3 Normal" DataFormatString="{0:0.00}">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="T3ONEWAY_RATE" HeaderText="Term3 OneWay" DataFormatString="{0:0.00}">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="New Rate">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("NEW_TOTAL")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Existing Rate">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("EXISTING_TOTAL")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Difference">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("DIFF_TOTAL")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle BackColor="#99CCFF" HorizontalAlign="Right" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="4">

                                                    <asp:Label ID="lblXLNormal" runat="server" CssClass="error"></asp:Label>

                                                    <asp:Label ID="lblXLOneway" runat="server" CssClass="error"></asp:Label>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="4">
                                                    <asp:GridView ID="gvTrRateImportSummary" runat="server" AutoGenerateColumns="False"
                                                        EmptyDataText="No Data" ShowFooter="True" CssClass="table table-bordered table-row" Width="100%">
                                                        <Columns>
                                                            <asp:BoundField DataField="Normal" HeaderText="Normal Amount"></asp:BoundField>
                                                            <asp:BoundField DataField="OneWay" HeaderText="OneWay Amount"></asp:BoundField>
                                                        </Columns>
                                                        <FooterStyle BackColor="#99CCFF" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
