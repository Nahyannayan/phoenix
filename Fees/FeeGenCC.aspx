<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeGenCC.aspx.vb" Inherits="Fees_FeeGenCC" title="Untitled Page" %>
<%@ MasterType  virtualPath="~/mainMasterPage.master"%>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function CheckAmount(e)
    {var amt;
     amt=parseFloat(e.value);
     if (isNaN(amt))
           amt= 0;
          e.value= amt.toFixed(2);
          UpdateSum();
     return true;
    } 
 function UpdateSum()
 {    
   var txtBankCom, txtNetAmount, txtdepAmount;   
        txtBankCom=parseFloat(document.getElementById('<%=txtBankCom.ClientID %>').value);
        if (isNaN(txtBankCom) )
            txtBankCom=0;
        txtdepAmount=parseFloat(document.getElementById('<%=txtdepAmount.ClientID %>').value);
        if (isNaN(txtdepAmount) )
            txtdepAmount=0;
        txtNetAmount=txtdepAmount-txtBankCom;             
        document.getElementById('<%=txtNetAmount.ClientID %>').value=txtNetAmount.toFixed(2);   
 }

    function OnClientClose(oWnd, args) {
        //get the transferred arguments

        var arg = args.get_argument();
        var ctrl = document.getElementById('<%=h_ctrl.ClientID%>').value;
        var ctrl1 = document.getElementById('<%=h_ctrl1.ClientID%>').value;
        var ctrl2 = document.getElementById('<%=h_ctrl2.ClientID%>').value;
        var pMode = document.getElementById('<%=h_pMode.ClientID%>').value;

        if (arg) {
            NameandCode = arg.NameandCode.split('||');

            if (pMode == 'CASHFLOW_BP' || pMode == 'CASHFLOW_BR') {
                document.getElementById(ctrl).value = NameandCode[0];
                __doPostBack(ctrl, 'TextChanged');
            }
            else if (pMode == 'EDITDEP') {
                document.getElementById(ctrl1).value = NameandCode[0];
                document.getElementById(ctrl2).value = NameandCode[1];
                __doPostBack(ctrl1, 'TextChanged');
            }
        }
    }

            function popUp(pWidth, pHeight, pMode, ctrl, ctrl1, ctrl2, ctrl3, ctrl4, ctrl5, acctype) {

                var sFeatures;
                var lstrVal;
                var lintScrVal;

                document.getElementById('<%=h_ctrl.ClientID%>').value = ctrl;
                document.getElementById('<%=h_ctrl1.ClientID%>').value = ctrl1;
                document.getElementById('<%=h_ctrl2.ClientID%>').value = ctrl2;
                document.getElementById('<%=h_pMode.ClientID%>').value = pMode;
           
                sFeatures = "dialogWidth: " + pWidth + "px; ";
                sFeatures += "dialogHeight: " + pHeight + "px; ";
                sFeatures += "help: no; ";
                sFeatures += "resizable: no; ";
                sFeatures += "scroll: yes; ";
                sFeatures += "status: no; ";
                sFeatures += "unadorned: no; ";
                var NameandCode;
                var result;
                if (pMode == 'SUBGRP') {

                    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];
                    document.getElementById(ctrl2).value = lstrVal[0];
                }
                else if (pMode == 'RFS') {

                    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];

                }
                else if (pMode == 'CASHFLOW_BP' || pMode == 'CASHFLOW_BR') {
                    QRYSTR = '';
                    if (pMode == 'CASHFLOW_BP')
                        QRYSTR = "?rss=0";
                    if (pMode == 'CASHFLOW_BR')
                        QRYSTR = "?rss=1";

                    result = radopen("../Accounts/acccpShowCashflow.aspx" + QRYSTR, "pop_up");
                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //lstrVal = result.split('__');
                    //document.getElementById(ctrl).value = lstrVal[0];


                }
                else if (pMode == 'DOCTYPE') {

                    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];


                }
                else if (pMode == 'EMPS') {

                    result = window.showModalDialog("ShowEmp.aspx?ShowType=" + pMode + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];

                }

                else if (pMode == 'DEPT') {

                    result = window.showModalDialog("ShowDept.aspx?ShowType=" + pMode + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];

                }

                else if (pMode == 'BSU') {

                    result = window.showModalDialog("ShowBSU.aspx?ShowType=" + pMode + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];

                }

                else if (pMode == 'COLLN') {

                    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];
                    document.getElementById(ctrl2).value = lstrVal[0];
                    document.getElementById(ctrl3).value = lstrVal[1];

                }


                else if (pMode == 'RSS') {

                    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];


                }
                else if (pMode == 'CCM') {

                    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];

                }
                else if (pMode == 'CCS') {

                    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];

                }
                else if (pMode == 'CTY') {

                    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];

                }

                else if (pMode == 'CONTROLACC') {
                    var ddsel = '';
                    var selObj = document.getElementById(acctype);
                    ddsel = selObj.options[selObj.selectedIndex].value;

                    result = window.showModalDialog("ShowControl.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value + "&acctype=" + ddsel, "", sFeatures);

                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];
                    document.getElementById(ctrl4).value = lstrVal[2];
                    if (document.getElementById(ctrl).value == '06101001')
                        //{ document.getElementById(ctrl4).value='063'; }
                        //document.getElementById(ctrl5).value=lstrVal[0].substring(3,lstrVal[0].length); 
                        document.getElementById(ctrl5).value = lstrVal[4];


                    var L = selObj.options.length;
                    for (var i = 0; i <= L - 1; i++) {
                        if (lstrVal[5] == selObj.options[i].value) { selObj.options.selectedIndex = i; }

                    }
                }
                else if (pMode == 'ALLACC' || pMode == 'BANKONLY' || pMode == 'CASHONLY' || pMode == 'CUSTSUPP' || pMode == 'CUSTSUPPnIJV') {

                    result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);

                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];
                }
                else if (pMode == 'EDITDEP') {
                
                    result =radopen("accgenbrcc.aspx?ShowType=" + pMode + "", "pop_up");

                    //if (result == '' || result == undefined)
                    //{ return false; }
                    //lstrVal = result.split('||');
                    //document.getElementById(ctrl1).value = lstrVal[0];
                    //document.getElementById(ctrl2).value = lstrVal[1];
                }
                else if (pMode == 'ALLOC') {

                    result = window.showModalDialog("TestAlloc.aspx?ShowType=" + pMode + "", "", sFeatures);

                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];
                }
                else if (pMode == 'ALLOCATE') {

                    result = window.showModalDialog("TestAlloc.aspx?Id=" + ctrl + "&pAmount=" + document.getElementById(ctrl1).value + "&pPly=" + document.getElementById(ctrl2).value + "", "", sFeatures);

                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');

                }
                else if (pMode == 'SHOWRSS') {

                    result = window.showModalDialog("ShowRSS.aspx?ShowType=" + pMode + "", "", sFeatures);

                    if (result == '' || result == undefined)
                    { return false; }

                    document.getElementById(ctrl).value = replaceSubstring(result, "|", " ");



                }
                else if (pMode == 'BANK') {
                    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];

                }

                else if (pMode == 'INTRAC') {

                    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];

                }

                else if (pMode == 'ACRDAC') {

                    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];

                }

                else if (pMode == 'PREPDAC') {


                    result = window.showModalDialog("ShowPrepaid.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];
                    if (ctrl2 != '')
                        if (document.getElementById(ctrl2).value == '') {
                            document.getElementById(ctrl2).value = lstrVal[2];
                            document.getElementById(ctrl3).value = lstrVal[3];
                        }
                }

                else if (pMode == 'CHQISSAC') {

                    result = window.showModalDialog("PopUp.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];

                }
                else if (pMode == 'CHQISSAC_PDC') {

                    result = window.showModalDialog("ShowPrepaid.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];
                    if (document.getElementById(ctrl2).value == '') {
                        document.getElementById(ctrl2).value = lstrVal[2];
                        document.getElementById(ctrl3).value = lstrVal[3];
                    }
                }

                else if (pMode == 'CHQBOOK') {
                    if (document.getElementById(ctrl3).value == "") {
                        alert("Please Select The Bank");
                        return false;
                    }

                    result = window.showModalDialog("ShowChqs.aspx?ShowType=" + pMode + "&BankCode=" + document.getElementById(ctrl3).value + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');

                    document.getElementById(ctrl).value = lstrVal[1];
                    document.getElementById(ctrl1).value = lstrVal[0];
                    document.getElementById(ctrl2).value = lstrVal[2];

                    document.getElementById(ctrl).focus();

                }
                else if (pMode == 'CHQBOOK_PDC') {
                    if (document.getElementById(ctrl3).value == "") {
                        alert("Please Select The Bank");
                        return false;
                    }

                    result = window.showModalDialog("ShowChqs.aspx?ShowType=" + pMode + "&BankCode=" + document.getElementById(ctrl3).value + "&docno=" + document.getElementById(ctrl4).value + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');

                    document.getElementById(ctrl).value = lstrVal[1];
                    document.getElementById(ctrl1).value = lstrVal[0];
                    document.getElementById(ctrl2).value = lstrVal[2];
                    document.getElementById(ctrl).focus();

                }



                else if (pMode == 'PDCCHQBOOK') {
                    if (document.getElementById(ctrl2).value == "") {
                        alert("Please Select The Bank");
                        return false;
                    }
                    else if (document.getElementById(ctrl3).value == "") {
                        alert("Please Enter The Installment Months");
                        return false;
                    }
                    result = window.showModalDialog("ShowChqsPDC.aspx?ShowType=" + pMode + "&BankCode=" + document.getElementById(ctrl2).value + "&MonthsReq=" + document.getElementById(ctrl3).value + "&docno=" + document.getElementById(ctrl4).value + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');

                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];

                    document.getElementById(ctrl).focus();


                }
                else if (pMode == 'PARTY') {

                    result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];

                    document.getElementById(ctrl2).value = lstrVal[2];
                    document.getElementById(ctrl3).value = lstrVal[3];

                }
                    //EDITED BY GURU - FILTER BY PARTY, SHOW DEBIT ACC//
                else if (pMode == 'DEBIT_D' || pMode == 'PARTY1' || pMode == 'DEBIT' || pMode == 'PARTY2') {

                    result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);

                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];
                    if (pMode == 'DEBIT' && (document.getElementById(ctrl2).value == '')) {
                        document.getElementById(ctrl2).value = lstrVal[0];
                        document.getElementById(ctrl3).value = lstrVal[1];

                        document.getElementById(ctrl4).value = lstrVal[2];
                        document.getElementById(ctrl5).value = lstrVal[3];
                    }
                    if (pMode == 'DEBIT_D') {
                        document.getElementById(ctrl2).value = lstrVal[2];
                        document.getElementById(ctrl3).value = lstrVal[3];
                    }
                }
                else if (pMode == 'ALL') {

                    result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];
                    document.getElementById(ctrl2).value = lstrVal[2];
                    document.getElementById(ctrl3).value = lstrVal[3];

                }
                else if (pMode == 'NORMAL') {

                    result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];
                    //document.getElementById(ctrl2).value=lstrVal[2];
                    //document.getElementById(ctrl3).value=lstrVal[3];

                }
                else if (pMode == 'NOTCC') {
                    if (ctrl2 == '' || ctrl2 == undefined) {
                        result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&codeorname=" + document.getElementById(ctrl).value, "", sFeatures);
                    } else {
                        result = window.showModalDialog("ShowAccount.aspx?ShowType=" + pMode + "&colid=" + document.getElementById(ctrl2).value, "", sFeatures);
                    }
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl1).value = lstrVal[1];
                    //document.getElementById(ctrl2).value=lstrVal[2];
                    // document.getElementById(ctrl3).value=lstrVal[3];

                }
                else if (pMode == 'MSTCHQBOOK') {
                    result = window.showModalDialog("accShowChequebook.aspx?BankCode=" + document.getElementById(ctrl1).value + "", "", sFeatures);
                    if (result == '' || result == undefined)
                    { return false; }
                    lstrVal = result.split('||');
                    document.getElementById(ctrl).value = lstrVal[0];
                    document.getElementById(ctrl2).value = lstrVal[1];
                }
                //EDITED BY GURU//         
            }
  
   function getBank()
    { 
        var sFeatures;
        sFeatures="dialogWidth: 820px; ";
        sFeatures+="dialogHeight: 450px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var NameandCode;
        var result;
       <%--result = window.showModalDialog("../Accounts/accjvshowaccount.aspx?bankcash=b","", sFeatures)
        if (result=='' || result==undefined)
            {    return false;      } 
        lstrVal=result.split('___');     
        document.getElementById('<%=txtBankCode.ClientId %>').value=lstrVal[0];
        document.getElementById('<%=txtBankDescr.ClientId %>').value=lstrVal[1];--%>
       var url = "../Accounts/accjvshowaccount.aspx?bankcash=b";
       var oWnd = radopen(url, "pop_getbank");
    }     
 function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBankCode.ClientId %>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientId %>').value = NameandCode[1];
                __doPostBack('<%=txtBankDescr.ClientID%>', 'TextChanged');
            }
        }




function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getbank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Credit Card Clearance
        </div>
        <div class="card-body">
            <div class="table-responsive">

        <table width="100%" align="center">    
           
      <tr><td width="100%">      
        <table align="center" width="100%">   
        <tr>
                <TD align="left" width="20%">
                   <span class="field-label"> Business Unit</span>
                </td>
                <td align="left" colspan="2">
                    &nbsp;<asp:DropDownList id="ddlBusinessunit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                        DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" SkinID="DropDownListNormal"
                        tabIndex="5" OnSelectedIndexChanged="ddlBusinessunit_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>            
            <tr valign="top">
                <td align="left" colspan="4" width="100%">
                    <asp:GridView ID="gvDTL" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" EmptyDataText="No transaction details added yet." Width="100%" SkinID="GridViewView">
                        <Columns>
                        <asp:TemplateField HeaderText="Doc Date">                                
                                <ItemTemplate>
                                    <asp:Label ID="lbltranDate" runat="server" Text='<%# Bind("DocDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="10%" HorizontalAlign="Center"/>
                            </asp:TemplateField>
                            <asp:BoundField DataField="VHH_DOCNO" HeaderText="Doc No" ReadOnly="True" />
                            
                             <asp:TemplateField HeaderText="CRR_ID" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCRRID" runat="server" Text='<%# Bind("CRR_ID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="10%" HorizontalAlign="Center"/>
                            </asp:TemplateField>                            
                             <asp:BoundField DataField="CRR_CRI_ID" HeaderText="Card Type" ReadOnly="True">
                                <ItemStyle Width="10%" />
                            </asp:BoundField>                            
                                  <asp:TemplateField HeaderText="Total transaction">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("tranAmount") %>'></asp:Label>
                                        <asp:HiddenField ID="h_CreditCardRate" Value ='<%# Bind("VHH_CRR_RATE") %>' runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="10%" HorizontalAlign="Right"/>
                            </asp:TemplateField>                           
                            <asp:TemplateField ShowHeader="False">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edit"
                                        OnClick="LinkButton1_Click" Text="Deposit"></asp:LinkButton>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    Deposit
                                </HeaderTemplate>
                            </asp:TemplateField>                           
                        </Columns>
                    </asp:GridView>
                <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                    <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                </td>
            </tr>
        </table>
       <table width="100%" align="center">
            <tr>
                <td width="20%" align="left">
                   <span class="field-label">Doc No</span></td>
                <td width="30%" align="left">
                    <asp:TextBox ID="txtdocNo" runat="server"></asp:TextBox></td>
                <td width="20%" align="left">
                  <span class="field-label"> Doc Date</span></td>
               
                 <td width="30%" align="left">
                    <asp:TextBox ID="txtdocDate" runat="server" AutoPostBack="True"></asp:TextBox>
                     <asp:ImageButton ID="imgDocDate" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
            </tr>
          <tr>
                <td width="15%" align="left">
                   <span class="field-label">Bank Account</span></td>
                <td align="left" colspan="3">
                    <table width="100%">
                        <tr>
                            <td align="left" width="38%" class="p-0">
                                <asp:TextBox ID="txtBankCode" runat="server" Style="left: -6px; 
                      top: 0px; ; " ></asp:TextBox>
                    <a href="#" onclick="getBank();return false;">
                          <img border="0" src="../Images/cal.gif" id="IMG3" /></a>
                            </td>
                            <td align="left" width="28%" class="p-0"><span class="field-label">Bank Name</span></td>
                            <td align="left" width="38%" class="p-0"><asp:TextBox ID="txtBankDescr" runat="server"></asp:TextBox></td>
                        </tr>
                    </table>
                        
                                       
                </td>               
            </tr>
           <tr>
                <td width="20%" align="left">
                   <span class="field-label"> Cash Flow</span></td>
                <td align="left">
                   <asp:TextBox ID="txtCashFlow" runat="server">
                   </asp:TextBox>
                 <a href="#" onclick="popUp('460','400','CASHFLOW_BR','<%=txtCashFlow.ClientId %>'); return false;">
                          <img border="0" src="../Images/cal.gif" id="IMG1"  /></a> 
                              </td>
            </tr>
           <tr>
               <td align="left" width="15%">
                   <span class="field-label">Narration</span></td>
               <td align="left" colspan="4">
                   <asp:TextBox ID="txtNarration" runat="server" CssClass="inputbox_multi" SkinID="MultiText"
                       TabIndex="16" TextMode="MultiLine">CREDIT CARD COLLECTION DEPOSIT</asp:TextBox></td>
           </tr>
            <tr>
                <td width="15%" align="left">
                   <span class="field-label">Card Type</span></td>
                <td align="left">
                    <asp:TextBox ID="txtCardType" runat="server" ></asp:TextBox></td>
                <td width="15%" align="left">
                  <span class="field-label">Deposit Amount</span></td>
              
                 <td width="25%" align="left">
                   <asp:TextBox ID="txtdepAmount" runat="server" ></asp:TextBox>
                     
                      <a href="#" onclick="popUp('600','400','EDItdEP','1','<%=txtdepAmount.ClientId %>','<%=txtBankCom.ClientId %>'); return false;">
                          <img border="0" src="../Images/cal.gif" id="IMG8" language="javascript"  /></a> 
                     </td>
            </tr>
           <tr>
                <td align="left">
                  <span class="field-label">Commission</span></td>
                <td align="left">
                    <asp:TextBox ID="txtComsn" runat="server"></asp:TextBox></td>
                <td align="left">
                   <span class="field-label">Bank Charges</span></td>
               
                 <td align="left">
                    <asp:TextBox ID="txtBankCom" runat="server" onBlur="CheckAmount(this);"></asp:TextBox>
                      </td>
            </tr>   
           <tr>
               <td align="left" colspan="2">
               </td>
               <td align="left">
                   <span class="field-label">Net</span>
               </td>
               
               <td align="left">
                   <asp:TextBox ID="txtNetAmount" runat="server"></asp:TextBox></td>
           </tr>
            <tr>
                <td colspan="4" align="center">
              <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" />
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                        TabIndex="30" Text="Cancel" /></td></tr>              
          </table> 
          
        
          </td>
          </tr>
          </table> 

                </div>
            </div>
          <asp:HiddenField id="h_ctrl" runat="server" />
                <asp:HiddenField id="h_ctrl1" runat="server" />
                <asp:HiddenField id="h_ctrl2" runat="server" />
                <asp:HiddenField ID="h_pMode" runat="server" /> 
        </div>

    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgDocDate" TargetControlID="txtdocDate">
    </ajaxToolkit:CalendarExtender>
    <asp:ObjectDataSource id="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
        <selectparameters>
            <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
        </selectparameters>
    </asp:ObjectDataSource>
    <asp:HiddenField ID="hColln" runat="server" />
    <asp:HiddenField ID="hCardAccount" runat="server" />
</asp:Content>
