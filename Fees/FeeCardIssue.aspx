<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FeeCardIssue.aspx.vb" Inherits="Fees_FeeCardIssue" Title="Loyalty Card Query" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" language="javascript">
        var isShift = false;
        function isNumeric(keyCode) {
            if (keyCode == 16) isShift = true;

            return ((keyCode >= 48 && keyCode <= 57 || keyCode == 8 || keyCode == 39 || keyCode == 46 || keyCode == 37 ||
            (keyCode >= 96 && keyCode <= 105)) && isShift == false);
        }

        function keyUP(keyCode) {
            if (keyCode == 16)
                isShift = false;

        }

        //function ajaxToolkit_CalendarExtenderClientShowing(e) {
        //    if (!e.get_selectedDate() || !e.get_element().value)
        //        e._selectedDate = (new Date()).getDateOnly();

        //}

        function checkDate(sender, args) {
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Issue Loyalty Card
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tblAddLedger" runat="server" width="100%">
                    <tr valign="bottom">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="center">
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">School Name<span style="color: #c00000">*</span></span>
                                    </td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:DropDownList ID="ddlSchool" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="20%"><span class="field-label">School Code</td>
                                    <td width="30%">
                                        <asp:TextBox ID="txtBSUId" ReadOnly="true" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>

                                    <td align="left" class="matters"><span class="field-label">Student Id<span style="color: #c00000">*</span></span>
                                    </td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtStudentId" runat="server" MaxLength="8" AutoPostBack="True"></asp:TextBox>
                                    </td>
                                    <td class="matters" align="left"><span class="field-label">Student Name<span style="color: #c00000">*</span></span>
                                    </td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtStudentName" runat="server" MaxLength="100"
                                            ReadOnly="True"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters" align="left"><span class="field-label">Parent Name</span>
                                    </td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtParentName" runat="server" MaxLength="100"
                                            ReadOnly="True"></asp:TextBox>
                                    </td>
                                    <td class="matters" align="left"><span class="field-label">Status</span></td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtStatus" runat="server" MaxLength="100"
                                            ReadOnly="True"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Issued Date</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtIssuedDate" runat="server"
                                            AutoPostBack="True"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgIssuedDate" TargetControlID="txtIssuedDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ID="imgIssuedDate" runat="server"
                                            ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                        &nbsp;<asp:RequiredFieldValidator runat="server" ID="rfvIssuedDate" ControlToValidate="txtIssuedDate"
                                            ErrorMessage="Transaction Date" ValidationGroup="Datas" Display="None">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td class="matters" align="left"><span class="field-label">Remarks</span>
                                    </td>
                                    <td class="matters" align="left">
                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"
                                            EnableTheming="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">

                                        <asp:Button ID="btnItemAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" /><asp:Button ID="btnItemCancel" runat="server" CausesValidation="False"
                                                CssClass="button" Text="Cancel" />
                                    </td>
                                </tr>
                                <tr id="trGridStudent" runat="server">
                                    <td align="center" class="matters" colspan="4">
                                        <asp:GridView ID="gvStudent" runat="server" AutoGenerateColumns="False"
                                            CaptionAlign="Top" PageSize="15"
                                            CssClass="table table-row table-bordered" UseAccessibleHeader="False">
                                            <Columns>
                                                <asp:BoundField DataField="TRS_TRH_ID" HeaderText="SCI_ID" Visible="False" />
                                                <asp:TemplateField HeaderText="SCI_STU_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("SCI_STU_ID") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("SCI_STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="STUDENT ID" DataField="STU_NO">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="STUDENT NAME" DataField="STU_NAME">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="PARENT NAME" DataField="PARENTNAME">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SCI_ISSUEDT" HeaderText="ISSUE DATE"
                                                    DataFormatString="{0:dd/MMM/yyyy}" />
                                                <asp:BoundField DataField="SCI_REMARKS" HeaderText="REMARKS" />
                                                <asp:CommandField ShowDeleteButton="True" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" /><asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                                Text="Edit" /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                                    ValidationGroup="groupM1" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                                CssClass="button" Text="Cancel" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="4">
                                        <asp:HiddenField ID="h_STU_ID" runat="server" Value="0" />
                                        <asp:HiddenField ID="h_SCI_ID" runat="server" Value="0" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
         <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
