﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Accounts_Tax_Code_Configuration_Add_Edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ''get the data mode from the query string to check if in add or edit mode 
                'ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ''get the menucode to confirm the user is accessing the valid page
                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059043") Then
                '    If Not Request.UrlReferrer Is Nothing Then
                '        Response.Redirect(Request.UrlReferrer.ToString())
                '    Else

                '        Response.Redirect("~\noAccess.aspx")
                '    End If
                'Else
                '    'txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                '    'txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)



                '    'ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                '    'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                'End If

                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                Call BindCountry(CInt(Session("BSU_COUNTRY_ID")))
                Call BindTaxCode(CInt(Session("BSU_COUNTRY_ID")))
                Call BindRefType(ddlType.SelectedValue.ToString)
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
    End Sub
    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        Call BindRefType(ddlType.SelectedValue.ToString)
        Call BindGrid()
    End Sub
    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        Call BindTaxCode(ddlCountry.SelectedValue.ToString)
        Call BindGrid()
    End Sub
    Protected Sub ddlRefType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRefType.SelectedIndexChanged
        Call BindGrid()
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "ADD"
        SaveEntry()
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Call clearContent()
            ViewState("viewid") = 0
            If ViewState("datamode") = "ADD" Or ViewState("datamode") = "EDIT" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub BindCountry(ByVal intCountryID As Int16)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String

            Dim ds As New DataSet
            str_Sql = " SELECT CTY_ID, CASE CTY_ID WHEN '5' THEN 'Not Available' ELSE CTY_NATIONALITY END CTY_NATIONALITY FROM COUNTRY_M WITH (NOLOCK) WHERE CTY_NATIONALITY <> '' ORDER BY CTY_NATIONALITY ;"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlCountry.Items.Clear()
            ddlCountry.DataSource = ds.Tables(0)
            ddlCountry.DataTextField = "CTY_NATIONALITY"
            ddlCountry.DataValueField = "CTY_ID"
            ddlCountry.DataBind()
            ddlCountry.ClearSelection()

            Dim li As New ListItem
            li.Text = "-SELECT-"
            li.Value = "0"
            ddlCountry.Items.Insert(0, li)

            ddlCountry.SelectedValue = intCountryID
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub BindTaxCode(ByVal intCountryID As Int16)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String

            Dim ds As New DataSet
            str_Sql = " SELECT TAX_CODE, TAX_DESCR  FROM oasis.TAX.TAX_CODES_M WHERE TAX_CTY_ID = " & intCountryID & " ORDER BY TAX_DESCR ASC ;"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlTaxCode.Items.Clear()
            ddlTaxCode.DataSource = ds.Tables(0)
            ddlTaxCode.DataTextField = "TAX_DESCR"
            ddlTaxCode.DataValueField = "TAX_CODE"
            ddlTaxCode.DataBind()
            ddlTaxCode.ClearSelection()

            Dim li As New ListItem
            li.Text = "-SELECT-"
            li.Value = "0"
            ddlTaxCode.Items.Insert(0, li)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub BindRefType(Optional ByVal strType As String = "FEE")
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String

            Dim ds As New DataSet
            If strType = "FEE" Then
                str_Sql = " SELECT FEE_ID AS REF_ID, FEE_DESCR AS REF_DESC FROM OASIS_FEES.FEES.FEES_M ORDER BY FEE_DESCR ASC;"
            ElseIf strType = "ACCOUNT" Then
                str_Sql = " SELECT ACT_ID AS REF_ID, ACT_NAME AS REF_DESC FROM OASISFIN..ACCOUNTS_M WHERE ACT_TYPE IN('INCOME','EXPENSE') ORDER BY ACT_NAME ASC ;"
            ElseIf strType = "EVENT" Then
                str_Sql = " SELECT DIM_CODE AS REF_ID, DIM_DESC AS REF_DESC FROM OASIS_DAX.DAX.DIMENSIONS_M WHERE DIM_DTM_ID = 4 ORDER BY DIM_DESC ASC ;"
            End If

            If Not strType = "0" Then
                If Not ds Is Nothing Then
                    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    If ds.Tables(0) Is Nothing OrElse ds.Tables(0).Rows.Count > 0 Then
                        ddlRefType.Items.Clear()
                        ddlRefType.DataSource = ds.Tables(0)
                        ddlRefType.DataTextField = "REF_DESC"
                        ddlRefType.DataValueField = "REF_ID"
                        ddlRefType.DataBind()
                        ddlRefType.ClearSelection()
                    End If
                End If
            End If

            Dim li As New ListItem
            li.Text = "-SELECT-"
            li.Value = "0"
            ddlRefType.Items.Insert(0, li)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub SaveEntry()
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim pParms(9) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TXM_SOURCE", "FEES")
                pParms(1) = New SqlClient.SqlParameter("@TXM_COUNTRY_ID", ddlCountry.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@TXM_REF_TYPE", ddlType.SelectedItem.Text)
                pParms(3) = New SqlClient.SqlParameter("@TXM_REF_CODE", ddlRefType.SelectedValue)
                pParms(4) = New SqlClient.SqlParameter("@TXM_DESCR", ddlRefType.SelectedItem.Text)
                pParms(5) = New SqlClient.SqlParameter("@TXM_FROMDT", txtFromDate.Text.ToString.Trim)
                pParms(6) = New SqlClient.SqlParameter("@TXM_TAX_CODE", ddlTaxCode.SelectedValue)
                pParms(7) = New SqlClient.SqlParameter("@TXM_USR_ID", Session("sUsr_name"))
                pParms(8) = New SqlClient.SqlParameter("@DataMode", ViewState("datamode"))
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[TAX].[SAVE_TAX_SETTINGS_MASTER]", pParms)
                transaction.Commit()
                lblError.Text = "Save successfully."
                Call clearContent()
            Catch ex As Exception
                lblError.Text = "Record could not be processed."
                transaction.Rollback()
            End Try
        End Using
        ViewState("datamode") = "none"
    End Sub
    Sub BindGrid()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As New DataSet

            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@TXM_REF_TYPE", ddlType.SelectedItem.Text.ToString)
            pParms(1) = New SqlClient.SqlParameter("@TXM_COUNTRY_ID", ddlCountry.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@TXM_DESCR", ddlRefType.SelectedItem.Text)
            pParms(3) = New SqlClient.SqlParameter("@DataMode", "Get_Filter_Data")

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[TAX].[GET_TAX_SETTINGS_MASTER_DATA]", pParms)
            If Not ds Is Nothing Then
                If ds.Tables(0) Is Nothing OrElse ds.Tables(0).Rows.Count > 0 Then
                    gvData.DataSource = ds.Tables(0)
                    gvData.DataBind()
                Else
                    gvData.DataSource = Nothing
                    gvData.DataBind()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub clearContent()
        ddlCountry.SelectedValue = CInt(Session("BSU_COUNTRY_ID"))
        ddlType.SelectedIndex = 0
        ddlRefType.SelectedIndex = 0
        ddlTaxCode.SelectedIndex = 0
        txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        Call BindGrid()
    End Sub
End Class
