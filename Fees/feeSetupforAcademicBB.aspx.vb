Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Fees_feeSetupforAcademicBB
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            'btnUpdate.Visible = False
            'btnChildCancel.Visible = False
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "F100040" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            InitialiseComponents()
            Session("FeeSetup") = FeeMaster.CreateDataTableFeeSetup()
            txtLinkToStage.Attributes.Add("ReadOnly", "ReadOnly")

            If Request.QueryString("viewid") <> "" Then
                setViewData()
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                tr_DetailViewDetailHeader.Visible = True
                tr_DetailViewDetails.Visible = True
                'Dim dt As DataTable
                'dt = FeeMaster.CreateDataTableFeeSetupMonthly
                'gvMonthly.DataSource = dt
                'gvMonthly.DataBind()
                gvAttendance.Columns(7).Visible = False
            Else
                ViewState("datamode") = "add"
                tr_DetailViewDetailHeader.Visible = False
                tr_DetailViewDetails.Visible = False
                ResetViewData()
            End If
            ViewState("iID") = 0

            gvAttendance.DataBind()
        End If
    End Sub

    Sub InitialiseComponents()
        txtLocation.Attributes.Add("readonly", "readonly")
        gvMonthly.Attributes.Add("bordercolor", "#1b80b6")
        ddlCollectionSchedule.DataBind()
        ddlFeetype.DataBind()
        'ddlCollectionSchedule.ClearSelection 
        'ddlCollectionSchedule.SelectedIndex = 1
        setProRata()
        ddBusinessunit.DataBind()
        If Request.QueryString("bsu") <> "" Then
            ddBusinessunit.SelectedIndex = -1
            ddBusinessunit.Items.FindByValue(Request.QueryString("bsu").Replace(" ", "+")).Selected = True
        End If
        ddlJoinprorata.DataBind()
        ddlDiscontinueprorata.DataBind()
        FillACD()
        txtFrom.Text = FeeCommon.GetACDStartDate(ddBusinessunit.SelectedItem.Value)
        Set_Collection()
    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ""
            If Request.QueryString("acdid") <> "" Then
                ACD_ID = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                ddlAcademicYear.SelectedIndex = -1
                ddlAcademicYear.Items.FindByValue(ACD_ID).Selected = True
            End If
            str_Sql = "SELECT     FSP.FSP_ID AS Id, ACY.ACY_DESCR AS Acd_year, FSP.FSP_ACD_ID AS Acd_id, " _
                & " FSP.FSP_FEE_ID AS Fee_id, FEE.FEE_DESCR AS Fee_descr, FSP.FSP_GRD_ID AS Grd_id, " _
                & " FSP.FSP_AMOUNT AS Amount, FSP.FSP_bActive AS Active, FSP.FSP_FROMDT AS FDate, " _
                & " FSP.FSP_TODT AS TDate, FSP.FSP_JOIN_FPM_ID AS 'Join', FSP.FSP_DISCONTINUE_FPM_ID AS Discontinue, " _
                & " FSP.FSP_LATEAMTTYPE AS Percentage, FSP.FSP_LATEAMT AS LateAmount, FSP.FSP_LATEDAYS AS LateDays, " _
                & " FSP.FSP_Collection_SCH_ID AS 'Collection', 'Normal' AS Status,  FSP_STM_ID as Stream, " _
                & " FSP.FSP_bWEEKLY_CHARGE as weekly, FSP.FSP_SBL_ID AS Locationid, SBL.SBL_DESCRIPTION AS Location, " _
                & " PRO_ID, PRO_DESCRIPTION, FSP.FSP_BRefundable,isnull(Fsp_ONEWAYAMT,0)ONEWAYAMT FROM ACADEMICYEAR_M AS ACY " _
                & " INNER JOIN ACADEMICYEAR_D AS ACD ON ACY.ACY_ID = ACD.ACD_ACY_ID AND ACY.ACY_ID = ACD.ACD_ACY_ID " _
                & " INNER JOIN FEES.FEESETUP_S AS FSP INNER JOIN FEES.FEES_M AS FEE ON FSP.FSP_FEE_ID = FEE.FEE_ID " _
                & " ON ACD.ACD_ID = FSP.FSP_ACD_ID INNER JOIN VV_TRANSPORT_SUBLOCATION AS SBL ON FSP.FSP_SBL_ID = SBL.SBL_ID  AND SBL.BSU_ID= '" & ddBusinessunit.SelectedItem.Value & "'   " _
                & " INNER JOIN PROCESSFO_SYS_M ON FSP_PRO_ID = PROCESSFO_SYS_M.PRO_ID " _
                & " WHERE     (FSP.FSP_ACD_ID = '" & ACD_ID & "') AND (FSP.FSP_ID = '" & p_Modifyid & "') AND (FSP.FSP_BSU_ID = '" & ddBusinessunit.SelectedItem.Value & "')"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            Session("FeeSetup") = ds.Tables(0)

            Dim iEdit As Integer = 0
            ddlCollectionSchedule.DataBind()
            ddlAcademicYear.SelectedIndex = -1
            ddlAcademicYear.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Acd_id").ToString).Selected = True
            ddlFeetype.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Fee_id").ToString).Selected = True
            ddlCollectionSchedule.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Collection").ToString).Selected = True
            txtFrom.Text = Format(CDate(Session("FeeSetup").Rows(iEdit)("FDate")), "dd/MMM/yyyy")

            ddlJoinprorata.SelectedIndex = -1
            ddlJoinprorata.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Join")).Selected = True

            ddlDiscontinueprorata.SelectedIndex = -1
            ddlDiscontinueprorata.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Discontinue")).Selected = True

            chkActive.Checked = Session("FeeSetup").Rows(iEdit)("Active")
            txtHAmount.Text = Format(Session("FeeSetup").Rows(iEdit)("Amount"), "0.00")
            txtOneWayAmt.Text = Format(Session("FeeSetup").Rows(iEdit)("ONEWAYAMT"), "0.00")
            txtLateAmount.Text = Session("FeeSetup").Rows(iEdit)("LateAmount")
            txtLatefeedays.Text = Session("FeeSetup").Rows(iEdit)("LateDays")

            txtLocation.Text = Session("FeeSetup").Rows(iEdit)("Location").ToString
            H_Location.Value = Session("FeeSetup").Rows(iEdit)("Locationid").ToString

            hf_PRO_ID.Value = Session("FeeSetup").Rows(iEdit)("PRO_ID")
            txtLinkToStage.Text = Session("FeeSetup").Rows(iEdit)("PRO_DESCRIPTION")
            chkRefundable.Checked = Session("FeeSetup").Rows(iEdit)("FSP_BRefundable")
            rbAmount.Checked = False
            rbPercentage.Checked = False
            setProRata("1")
            If Session("FeeSetup").Rows(iEdit)("Percentage") = 1 Then
                rbAmount.Checked = True
            ElseIf Session("FeeSetup").Rows(iEdit)("Percentage") = 2 Then
                rbPercentage.Checked = True
            End If
            setMonthlyDetails(p_Modifyid)
          

            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub setViewData()
        gvAttendance.Columns(7).Visible = False
        txtFrom.Attributes.Add("readonly", "readonly")
        ddlCollectionSchedule.Enabled = False
        ddlAcademicYear.Enabled = False
        'ddlFeetype.Enabled = False
        'ddlGrade.Enabled = False
        'ddlStream.Enabled = False
        imgFrom.Enabled = False
        ImageButton1.Enabled = False
        chkRefundable.Enabled = False
    End Sub

    Sub ResetViewData()
        txtFrom.Attributes.Remove("readonly")
        gvAttendance.Columns(7).Visible = False
        ddlCollectionSchedule.Enabled = True
        ddlAcademicYear.Enabled = True
        'ddlFeetype.Enabled = True
        'ddlGrade.Enabled = True
        'ddlStream.Enabled = True
        imgFrom.Enabled = True
        ImageButton1.Enabled = True
        chkRefundable.Enabled = True
        If ViewState("datamode") = "add" Then
            FeeMaster.SetDefaultLinkToState(txtLinkToStage, hf_PRO_ID)
        End If
    End Sub

    Sub Clear_All()
        txtLocation.Text = ""
        H_Location.Value = ""
        txtHAmount.Text = ""
        txtOneWayAmt.Text = "0"
        txtLateAmount.Text = "0"
        txtLatefeedays.Text = "0"
        gvAttendance.DataBind()
        Session("FeeSetup").Rows.Clear()
        gvMonthly.DataBind()
        Set_Collection()
    End Sub

    Sub gridbind()
        gvAttendance.DataSource = Session("FeeSetup")
        gvAttendance.DataBind()
    End Sub

    Private Function ValidateGrid() As Boolean
        If Trim(Me.txtOneWayAmt.Text) <> "" Or Trim(Me.txtOneWayAmt.Text) <> "0" Then
            Dim sum As Decimal = 0
            For Each grd As GridViewRow In Me.gvMonthly.Rows
                Dim txt1way As TextBox = DirectCast(grd.FindControl("txt1Way"), TextBox)
                sum = sum + Convert.ToDecimal(txt1way.Text)
            Next
            If sum = Convert.ToDecimal(Me.txtOneWayAmt.Text) Then
                Return True
            Else
                Return False
            End If
        Else
            Return True
        End If

    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If gvMonthly.Rows.Count = 0 Then
            'lblError.Text = "Please add fee details"
            usrMessageBar.ShowNotification("Please add fee details", UserControls_usrMessageBar.WarningType.Information)
            Exit Sub
        End If
        If tr_Transport.Visible And H_Location.Value = "" Then
            'lblError.Text = "Please Select location..."
            usrMessageBar.ShowNotification("Please Select location...", UserControls_usrMessageBar.WarningType.Information)
            Exit Sub
        End If
        If ValidateGrid() = False Then
            'lblError.Text = "Please check the one way amount..."
            usrMessageBar.ShowNotification("Please check the one way amount...", UserControls_usrMessageBar.WarningType.Information)
            Exit Sub
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim str_update_id, NEW_FSP_ID, NEW_FSM_ID, NEW_RTM_ID As String
        str_update_id = ""
        NEW_FSP_ID = ""
        NEW_FSM_ID = ""
        NEW_RTM_ID = ""
        Dim dblTotal As Decimal = 0
        Dim dbl1way As Decimal = 0
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            If ViewState("datamode") = "edit" Then
                If Session("FeeSetup").Rows.Count = 0 And ViewState("datamode") <> "edit" Then
                    'lblError.Text = "There is no data to save"
                    usrMessageBar.ShowNotification("There is no data to save", UserControls_usrMessageBar.WarningType.Information)
                    Exit Sub
                End If
                For i As Integer = 0 To Session("FeeSetup").Rows.Count - 1
                    Dim iLateAmttype As Integer = 1
                    If Session("FeeSetup").Rows(i)("Percentage") Then
                        iLateAmttype = 2
                    End If
                    If Session("FeeSetup").Rows(i)("Status").ToString = "updated" Or Session("FeeSetup").Rows(i)("Status").ToString = "Inserted" Then
                        If Session("FeeSetup").Rows(i)("Status").ToString = "Inserted" Then
                            str_update_id = 0
                        Else
                            str_update_id = Session("FeeSetup").Rows(i)("id")
                        End If
                        retval = FeeMaster.F_SaveFEESETUP_S(str_update_id, _
                           ddBusinessunit.SelectedItem.Value, Session("FeeSetup").Rows(i)("Acd_id"), Session("FeeSetup").Rows(i)("Fee_id"), _
                           Session("FeeSetup").Rows(i)("Grd_id"), Session("FeeSetup").Rows(i)("Amount"), Session("FeeSetup").Rows(i)("ONEWAYAMT"), _
                           Session("FeeSetup").Rows(i)("Active"), Session("FeeSetup").Rows(i)("FDate"), _
                           Session("FeeSetup").Rows(i)("TDate").ToString, Session("FeeSetup").Rows(i)("Join"), _
                           Session("FeeSetup").Rows(i)("Discontinue"), iLateAmttype, _
                           Session("FeeSetup").Rows(i)("LateAmount"), Session("FeeSetup").Rows(i)("LateDays"), _
                           Session("FeeSetup").Rows(i)("Collection"), Session("FeeSetup").Rows(i)("Stream"), _
                           Session("FeeSetup").Rows(i)("Weekly"), ddlJoinprorata.SelectedItem.Value, _
                           ddlDiscontinueprorata.SelectedItem.Value, H_Location.Value, "", NEW_FSP_ID, _
                           NEW_RTM_ID, hf_PRO_ID.Value, chkRefundable.Checked, 0, 0, stTrans)
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                        Session("FeeSetup").Rows(i)("Fee_id"), _
                        "Insert", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                    End If
                Next
            Else
                'Id ,Acd_year, Acd_id,  Fee_id,  Fee_descr,  Grd_id, FDate,  TDate, Amount ,Status
                '  , Join, Discontinue, Active , Percentage,LateAmount,LateDays 
                Dim iLateAmttype As Integer = 1
                If rbPercentage.Checked Then
                    iLateAmttype = 2
                End If
                If Not IsDate(txtFrom.Text) Then
                    'lblError.Text = "Invalid date"
                    usrMessageBar.ShowNotification("Invalid date", UserControls_usrMessageBar.WarningType.Danger)
                    stTrans.Rollback()
                    Exit Sub
                End If
                If Not IsNumeric(txtHAmount.Text) Or Not IsNumeric(txtLateAmount.Text) Or Not IsNumeric(txtLatefeedays.Text) Then
                    stTrans.Rollback()
                    'lblError.Text = "Invalid Amount/No. of Days"
                    usrMessageBar.ShowNotification("Invalid Amount/No. of Days", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
                Dim boolJoin As Boolean = True
                Dim boolDiscontinue As Boolean = True

                If ddlJoinprorata.SelectedIndex = 0 Then
                    boolJoin = False
                End If
                If ddlDiscontinueprorata.SelectedIndex = 0 Then
                    boolDiscontinue = False
                End If

                If tr_Transport.Visible Then
                    retval = FeeMaster.F_SaveFEESETUP_M(0, ddBusinessunit.SelectedItem.Value, ddlAcademicYear.SelectedItem.Value, _
                       ddlFeetype.SelectedItem.Value, ViewState("FEE_ChargedFor_SCH_ID"), _
                       ddlCollectionSchedule.SelectedItem.Value, NEW_FSM_ID, stTrans, "-1", False)
                End If
                If retval = "0" Then
                    retval = FeeMaster.F_SaveFEESETUP_S(0, ddBusinessunit.SelectedItem.Value, ddlAcademicYear.SelectedItem.Value, _
                                  ddlFeetype.SelectedItem.Value, "", txtHAmount.Text, txtOneWayAmt.Text, chkActive.Checked, txtFrom.Text, "", boolJoin, _
                                  boolDiscontinue, iLateAmttype, txtLateAmount.Text, txtLatefeedays.Text, ddlCollectionSchedule.SelectedItem.Value, _
                                   "", False, ddlJoinprorata.SelectedItem.Value, ddlDiscontinueprorata.SelectedItem.Value, H_Location.Value, _
                                   NEW_FSM_ID, NEW_FSP_ID, NEW_RTM_ID, Val(hf_PRO_ID.Value), chkRefundable.Checked, NEW_FSM_ID, 0, stTrans)
                End If
                If retval = "0" Then
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                                  NEW_FSP_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If retval = "0" Then
                    For Each gvr As GridViewRow In gvMonthly.Rows
                        'Get a programmatic reference to the CheckBox control
                        Dim lblId As Label = CType(gvr.FindControl("lblId"), Label)
                        Dim txtChargeDate As TextBox = CType(gvr.FindControl("txtChargeDate"), TextBox)
                        Dim txtFirstMemoDate As TextBox = CType(gvr.FindControl("txtFirstMemoDate"), TextBox)
                        Dim txtSecondMemoDate As TextBox = CType(gvr.FindControl("txtSecondMemoDate"), TextBox)
                        Dim txtThirdMemoDate As TextBox = CType(gvr.FindControl("txtThirdMemoDate"), TextBox)
                        Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)
                        Dim txtOneWay As TextBox = CType(gvr.FindControl("txt1Way"), TextBox)
                        If Not txtAmount Is Nothing Then
                            If Not IsNumeric(txtAmount.Text) Then
                                retval = "510" 'invalid amount
                                Exit For
                            End If
                            retval = FeeMaster.F_SaveFEESETUPMONTHLY_D(0, NEW_FSP_ID, lblId.Text, txtChargeDate.Text, _
                            txtAmount.Text, txtOneWay.Text, txtFirstMemoDate.Text, txtSecondMemoDate.Text, txtThirdMemoDate.Text, _
                            ddBusinessunit.SelectedItem.Value, ddlAcademicYear.SelectedItem.Value, NEW_RTM_ID, _
                            Session("sUsr_name"), txtChargeDate.Text, 0, stTrans)
                            dblTotal = dblTotal + CDbl(txtAmount.Text)
                            dbl1way = dbl1way + CDbl(txtOneWay.Text)
                        End If
                        If retval <> "0" Then
                            Exit For
                        End If
                    Next
                End If
            End If
            If retval = "0" And (dblTotal <> CDbl(txtHAmount.Text) Or dbl1way <> CDbl(txtOneWayAmt.Text)) Then
                retval = "413" 'totals not tallying
            End If
            If retval = "0" Then
                stTrans.Commit()
                'lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Danger)
                Clear_All()
                ViewState("datamode") = "add"
                gridbind()
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub gvAttendance_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvAttendance.RowDeleting
        Try
            Dim row As GridViewRow = gvAttendance.Rows(e.RowIndex)
            Dim idRow As New Label
            idRow = TryCast(row.FindControl("lblId"), Label)
            Dim iRemove As Integer = 0
            Dim iIndex As Integer = 0
            iIndex = CInt(idRow.Text)
            'loop through the data table row  for the selected rowindex item in the grid view
            For iRemove = 0 To Session("FeeSetup").Rows.Count - 1
                If iIndex = Session("FeeSetup").Rows(iRemove)(0) Then
                    Session("FeeSetup").Rows(iRemove).Delete()
                    Exit For
                End If
            Next
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'lblError.Text = "Record cannot be Deleted"
            usrMessageBar.ShowNotification("Record cannot be Deleted", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        setEditdata()
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Sub setEditdata()
        txtFrom.Attributes.Remove("readonly")
        imgFrom.Enabled = True
        gvAttendance.Columns(7).Visible = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            setViewData()
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        Clear_All()
        gridbind()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
    End Sub

    Protected Sub ddFeetype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFeetype.SelectedIndexChanged
        setProRata()
        Set_Collection()
    End Sub

    Sub setProRata(Optional ByVal p_Collection As String = "")
        Dim str_Sql As String = "SELECT FEE.FEE_bJOINPRORATA, FEE.FEE_bDiscontinuePRORATA, " _
        & " CHG.SCH_DESCR AS CHG_DESCR, FEE.FEE_Collection_SCH_ID,FEE.FEE_ChargedFor_SCH_ID, " _
        & " FEE.FEE_bSETUPBYGRADE FROM FEES.FEES_M AS FEE INNER JOIN FEES.SCHEDULE_M AS CHG  " _
        & " ON FEE.FEE_ChargedFor_SCH_ID = CHG.SCH_ID" _
        & " where FEE_ID='" & ddlFeetype.SelectedItem.Value & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
        CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            lblChargeSchedule.Text = "(" & ds.Tables(0).Rows(0)("CHG_DESCR") & ")"
            ViewState("FEE_ChargedFor_SCH_ID") = ds.Tables(0).Rows(0)("FEE_ChargedFor_SCH_ID")

            'ddlCollectionSchedule.SelectedIndex = -1
            'ddlCollectionSchedule.Items.FindByValue(ds.Tables(0).Rows(0)("FEE_Collection_SCH_ID")).Selected = True
            If ds.Tables(0).Rows(0)("FEE_bSETUPBYGRADE") Then
                'ddlJoinprorata.SelectedIndex = -1
                'ddlDiscontinueprorata.SelectedIndex = -1
                'ddlJoinprorata.Items.FindByValue(ds.Tables(0).Rows(0)("FEE_bJOINPRORATA")).Selected = True
                'ddlDiscontinueprorata.Items.FindByValue(ds.Tables(0).Rows(0)("FEE_bDiscontinuePRORATA")).Selected = True
            End If
        End If
        SetTransport()
    End Sub

    Sub SetTransport()
        Dim sb_sql As New StringBuilder()
        sb_sql.Append("SELECT FEE_SVC_ID FROM FEES.FEES_M" _
        & "  where (FEE_ID= '" & ddlFeetype.SelectedItem.Value & "') ")
        Dim str_svc_id As String = UtilityObj.GetDataFromSQL(sb_sql.ToString, WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        If str_svc_id <> "--" Then
            If str_svc_id = "1" Then
                tr_Transport.Visible = True
            Else
                tr_Transport.Visible = False
                txtLocation.Text = ""
                H_Location.Value = ""
            End If
        End If
    End Sub

    Protected Sub gvAttendance_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvAttendance.RowEditing
        gvAttendance.EditIndex = e.NewEditIndex
        Dim row As GridViewRow = gvAttendance.Rows(e.NewEditIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("FeeSetup").Rows.Count - 1
            If iIndex = Session("FeeSetup").Rows(iEdit)(0) Then
                'txtAmount.Text = Session("FeeSetup").Rows(iEdit)("Amount")
                'ID, Acd_year, Acd_id, Fee_id, Fee_descr, Grd_id, FDate, TDate, Amount, Status
                '  , Join, Discontinue, Active ,Percentage,LateAmount,LateDays ,Collection
                ddlCollectionSchedule.DataBind()
                ddlAcademicYear.SelectedIndex = -1
                ddlAcademicYear.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Acd_id").ToString).Selected = True
                ddlFeetype.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Fee_id").ToString).Selected = True
                ddlCollectionSchedule.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Collection").ToString).Selected = True
                txtFrom.Text = Format(CDate(Session("FeeSetup").Rows(iEdit)("FDate")), "dd/MMM/yyyy")

                ddlJoinprorata.SelectedIndex = -1
                ddlJoinprorata.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Join")).Selected = True

                ddlDiscontinueprorata.SelectedIndex = -1
                ddlDiscontinueprorata.Items.FindByValue(Session("FeeSetup").Rows(iEdit)("Discontinue")).Selected = True

                chkActive.Checked = Session("FeeSetup").Rows(iEdit)("Active")
                txtHAmount.Text = Format(Session("FeeSetup").Rows(iEdit)("Amount"), "0.00")
                txtLateAmount.Text = Session("FeeSetup").Rows(iEdit)("LateAmount")
                txtLatefeedays.Text = Session("FeeSetup").Rows(iEdit)("LateDays")

                txtLocation.Text = Session("FeeSetup").Rows(iEdit)("Location").ToString
                H_Location.Value = Session("FeeSetup").Rows(iEdit)("Locationid").ToString
                rbAmount.Checked = False
                rbPercentage.Checked = False
                setProRata("1")
                If Session("FeeSetup").Rows(iEdit)("Percentage") = 1 Then
                    rbAmount.Checked = True
                ElseIf Session("FeeSetup").Rows(iEdit)("Percentage") = 2 Then
                    rbPercentage.Checked = True
                End If
                setMonthlyDetails(iIndex)
                Exit For
            End If
        Next
        gvAttendance.Columns(6).Visible = False
        gridbind()
    End Sub

    Private Sub setMonthlyDetails(ByVal p_Monthlyid As String) 'setting header data on view/edit
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT FDD_ID, FDD_FSP_ID, FDD_REF_ID, " _
            & " REPLACE(CONVERT(VARCHAR(11), FDD_DATE, 113), ' ', '/')AS  FDD_DATE, FDD_AMOUNT, " _
            & " REPLACE(CONVERT(VARCHAR(11), FDD_FIRSTMEMODT, 113), ' ', '/')AS FDD_FIRSTMEMODT, " _
            & " REPLACE(CONVERT(VARCHAR(11), FDD_SECONDMEMODT, 113), ' ', '/')AS FDD_SECONDMEMODT, " _
            & " REPLACE(CONVERT(VARCHAR(11), FDD_THIRDMEMODT, 113), ' ', '/')AS FDD_THIRDMEMODT, " _
            & " DESCR,ONEWAYAMT FROM FEES.VW_OSO_FEEMONTHLY " _
            & " WHERE FDD_FSP_ID = '" & p_Monthlyid & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            Dim dt As DataTable
            dt = FeeMaster.CreateDataTableFeeSetupMonthly
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim dr As DataRow
                dr = dt.NewRow
                dr("id") = ds.Tables(0).Rows(i)("FDD_ID")
                dr("Descr") = ds.Tables(0).Rows(i)("descr")
                dr("FDD_DATE") = ds.Tables(0).Rows(i)("FDD_DATE")
                dr("FDD_FIRSTMEMODT") = ds.Tables(0).Rows(i)("FDD_FIRSTMEMODT")
                dr("FDD_SECONDMEMODT") = ds.Tables(0).Rows(i)("FDD_SECONDMEMODT")
                dr("FDD_THIRDMEMODT") = ds.Tables(0).Rows(i)("FDD_THIRDMEMODT")
                dr("FDD_REF_ID") = ds.Tables(0).Rows(i)("FDD_REF_ID")
                dr("FDD_AMOUNT") = ds.Tables(0).Rows(i)("FDD_AMOUNT")
                dr("ONEWAYAMT") = ds.Tables(0).Rows(i)("ONEWAYAMT")
                'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT, FDD_REF_ID
                dt.Rows.Add(dr)
            Next
            gvMonthly.DataSource = dt
            gvMonthly.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddCollectionSchedule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCollectionSchedule.SelectedIndexChanged
        Set_Collection()
    End Sub

    Sub Set_Collection()
        Dim iMonths As Integer
        Dim dtTermorMonths As New DataTable
        Select Case ddlCollectionSchedule.SelectedItem.Value
            Case 0 '0 Monthly 
                iMonths = 10
                dtTermorMonths = FeeCommon.GetACADEMIC_MonthorTerm(ddlAcademicYear.SelectedItem.Value, False)
            Case 1 '1 Bi-Monthly
                iMonths = 5
                dtTermorMonths = FeeCommon.GetACADEMIC_MonthorTerm(ddlAcademicYear.SelectedItem.Value, False)
            Case 2 '2 Quarterly 
                iMonths = 3
                dtTermorMonths = FeeCommon.GetACADEMIC_MonthorTerm(ddlAcademicYear.SelectedItem.Value, True)

            Case 3 '3 Half Year
                iMonths = 2
                dtTermorMonths = FeeCommon.GetACADEMIC_MonthorTerm(ddlAcademicYear.SelectedItem.Value, False)
            Case 4, 5 '4 Annual 
                iMonths = 1
                dtTermorMonths = FeeCommon.GetACADEMIC_MonthorTerm(ddlAcademicYear.SelectedItem.Value, False)
            Case Else '5 Once Only 
                iMonths = 0
        End Select
        Dim dt As DataTable
        dt = FeeMaster.CreateDataTableFeeSetupMonthly
        If dtTermorMonths.Rows.Count > 0 Then
            Select Case iMonths
                Case 10, 3, 1
                    If iMonths = 3 Then
                        iMonths = dtTermorMonths.Rows.Count
                    End If
                    For i As Integer = 0 To iMonths - 1
                        Dim dr As DataRow
                        dr = dt.NewRow
                        dr("id") = dtTermorMonths.Rows(i)("ID")
                        dr("Descr") = dtTermorMonths.Rows(i)("descr")
                        dr("FDD_AMOUNT") = "0.00"
                        dr("ONEWAYAMT") = "0.00"
                        dr("FDD_DATE") = dtTermorMonths.Rows(i)("FDD_DATE")
                        dr("FDD_FIRSTMEMODT") = dtTermorMonths.Rows(i)("FDD_FIRSTMEMODT")
                        dr("FDD_SECONDMEMODT") = dtTermorMonths.Rows(i)("FDD_SECONDMEMODT")
                        dr("FDD_THIRDMEMODT") = dtTermorMonths.Rows(i)("FDD_THIRDMEMODT")
                        'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT
                        dt.Rows.Add(dr)
                    Next
                Case 5, 2
                    For i As Integer = 0 To iMonths - 1
                        Dim dr As DataRow
                        dr = dt.NewRow
                        dr("id") = dtTermorMonths.Rows(i * 10 / iMonths)("ID")
                        dr("Descr") = dtTermorMonths.Rows(i * 10 / iMonths)("descr")
                        dr("FDD_AMOUNT") = "0.00"
                        dr("ONEWAYAMT") = "0.00"
                        dr("FDD_DATE") = dtTermorMonths.Rows(i * 10 / iMonths)("FDD_DATE")
                        dr("FDD_FIRSTMEMODT") = dtTermorMonths.Rows(i * 10 / iMonths)("FDD_FIRSTMEMODT")
                        dr("FDD_SECONDMEMODT") = dtTermorMonths.Rows(i * 10 / iMonths)("FDD_SECONDMEMODT")
                        dr("FDD_THIRDMEMODT") = dtTermorMonths.Rows(i * 10 / iMonths)("FDD_THIRDMEMODT")
                        'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT
                        dt.Rows.Add(dr)
                    Next
            End Select
        End If

        gvMonthly.DataSource = dt
        gvMonthly.DataBind()
    End Sub

    Protected Sub gvAttendance_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvAttendance.RowUpdating

    End Sub

    Protected Sub gvAttendance_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvAttendance.RowCancelingEdit

    End Sub

    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBusinessunit.SelectedIndexChanged
        Clear_All()
        FillACD() 
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddBusinessunit.SelectedItem.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        'ddlGrade.DataBind()
        'ddlStream.DataBind()
        ddlAcademicYear.SelectedIndex = -1
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID").ToString).Selected = True
                Exit For
            End If
        Next
        Set_Collection()
    End Sub

End Class
 