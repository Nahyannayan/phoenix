﻿Imports System.Data
Imports UtilityObj
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration


Partial Class Fees_FeeTaxInvoice
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Shared reportHeader As String

    Private Property sqlConnectionString() As String
        Get
            Return ViewState("sqlconn")
        End Get
        Set(ByVal value As String)
            ViewState("sqlconn") = value
        End Set
    End Property
    Private Property INV_TYPE() As String
        Get
            Return ViewState("INV_TYPE")
        End Get
        Set(ByVal value As String)
            ViewState("INV_TYPE") = value
        End Set
    End Property
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F100134" And ViewState("MainMnu_code") <> "F300164") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            Initialize()
            gvInvoices.Attributes.Add("bordercolor", "#1b80b6")
            GridBind()
            'btnPrint.Attributes.Add("onClick", "return LoopGrid();")
            hf_INVTYPE.Value = Encr_decrData.Encrypt("TAXINV")
        End If

    End Sub
    Private Sub Initialize()
        Select Case ViewState("MainMnu_code")
            Case "F100134" 'Fees
                sqlConnectionString = ConnectionManger.GetOASIS_FEESConnectionString
                INV_TYPE = "TAXINV"
                trBsu.Visible = False
            Case "F300164" 'Transport
                sqlConnectionString = ConnectionManger.GetOASISTRANSPORTConnectionString
                INV_TYPE = "TRTAXINV"
                trBsu.Visible = True
        End Select
        ddlBusinessunit.DataBind()
        If ddlBusinessunit.Items.Count > 0 Then
            ddlBusinessunit.Items.Insert(0, New ListItem("<---ALL UNITS--->", "0"))
            ddlBusinessunit.SelectedIndex = 0
            ddlBusinessunit_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvInvoices.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvInvoices.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim ds As New DataSet
            Dim str_Filter As String = String.Empty
            Dim lstrOpr As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim lstrCondn6 As String = String.Empty
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            If gvInvoices.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtAcd_year
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvInvoices.HeaderRow.FindControl("txtInvoiceNo")
                lstrCondn1 = Trim(txtSearch.Text.Trim)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FSH_INVOICE_NO", lstrCondn1)

                '   -- 1  txtAsOnDate
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvInvoices.HeaderRow.FindControl("txtInvDate")
                lstrCondn2 = txtSearch.Text.Trim
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FSH_DATE", lstrCondn2)

                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvInvoices.HeaderRow.FindControl("txtStuNo")
                lstrCondn3 = txtSearch.Text.Trim
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn3)

                '   -- 3   txtTDate
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvInvoices.HeaderRow.FindControl("txtStuName")
                lstrCondn4 = txtSearch.Text.Trim
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "NAME", lstrCondn4)

                '   -- 5  grade
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvInvoices.HeaderRow.FindControl("txtGrade")
                lstrCondn5 = txtSearch.Text.Trim
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FSH_GRD_ID", lstrCondn5)

                '   -- 6  bsu
                larrSearchOpr = h_Selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvInvoices.HeaderRow.FindControl("txtStuBsuShort")
                lstrCondn6 = txtSearch.Text.Trim
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_BSU_SHORT", lstrCondn6)
            End If

            Dim str_conn As String = sqlConnectionString
            Dim pParms(5) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuId")
            pParms(1) = New SqlClient.SqlParameter("@INVOICE_TYPE", SqlDbType.VarChar, 50)
            pParms(1).Value = rblInvType.SelectedValue
            pParms(2) = New SqlClient.SqlParameter("@TOP_FILTER", SqlDbType.VarChar, 20)
            pParms(2).Value = IIf(Convert.ToString(ddlTopFilter.SelectedValue).ToUpper = "ALL", "", " TOP " & ddlTopFilter.SelectedValue & " ")
            pParms(3) = New SqlClient.SqlParameter("@FILTER", SqlDbType.VarChar, 1000)
            pParms(3).Value = str_Filter
            pParms(4) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(4).Value = ddlBusinessunit.SelectedValue

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.[GET_FEE_TAX_INVOICES_LIST]", pParms)
            ViewState("gvInvoices") = ds.Tables(0)
            BindGrid()
            'If ds.Tables(0).Rows.Count = 0 Then
            '    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            '    'gvInvoices.DataBind()
            '    Dim columnCount As Integer = gvInvoices.Rows(0).Cells.Count
            '    gvInvoices.Rows(0).Cells.Clear()
            '    gvInvoices.Rows(0).Cells.Add(New TableCell)
            '    gvInvoices.Rows(0).Cells(0).ColumnSpan = columnCount
            '    gvInvoices.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            '    gvInvoices.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            'Else
            '    gvInvoices.DataBind()
            'End If

            txtSearch = gvInvoices.HeaderRow.FindControl("txtInvoiceNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvInvoices.HeaderRow.FindControl("txtInvDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvInvoices.HeaderRow.FindControl("txtStuNo")
            txtSearch.Text = lstrCondn3

            txtSearch = gvInvoices.HeaderRow.FindControl("txtStuName")
            txtSearch.Text = lstrCondn4

            txtSearch = gvInvoices.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn5

            txtSearch = gvInvoices.HeaderRow.FindControl("txtStuBsuShort")
            txtSearch.Text = lstrCondn6


        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        gvInvoices.DataSource = ViewState("gvInvoices")
        gvInvoices.DataBind()
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub ddlTopFilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTopFilter.SelectedIndexChanged
        GridBind()
    End Sub
    Protected Sub rblInvType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblInvType.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub gvInvoices_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvInvoices.PageIndexChanging
        gvInvoices.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub chkSelectAll_CheckedChanged(sender As Object, e As EventArgs)
        Dim chkAll As CheckBox = DirectCast(sender, CheckBox)
        For Each gvr As GridViewRow In gvInvoices.Rows
            Dim chkFSH_ID As CheckBox = DirectCast(gvr.FindControl("chkFSH_ID"), CheckBox)
            If Not chkFSH_ID Is Nothing Then
                chkFSH_ID.Checked = chkAll.Checked
            End If
        Next
    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) 'Handles btnPrint.Click
        Dim FSH_IDs As String = ""
        For Each gvr As GridViewRow In gvInvoices.Rows
            Dim chkFSH_ID As CheckBox = DirectCast(gvr.FindControl("chkFSH_ID"), CheckBox)
            Dim lblFSH_ID As Label = DirectCast(gvr.FindControl("lblFSH_ID"), Label)
            If Not chkFSH_ID Is Nothing And Not lblFSH_ID Is Nothing AndAlso chkFSH_ID.Checked Then
                FSH_IDs = IIf(FSH_IDs = "", "", "|") & lblFSH_ID.Text
            End If
        Next
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Dim chkSelectAll As CheckBox = DirectCast(gvInvoices.HeaderRow.FindControl("chkSelectAll"), CheckBox)
        If Not chkSelectAll Is Nothing Then
            chkSelectAll.Checked = False
            chkSelectAll_CheckedChanged(Nothing, Nothing)
        End If
        For Each gvr As GridViewRow In gvInvoices.Rows

        Next
    End Sub

    Protected Sub gvInvoices_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvInvoices.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lbtnView As LinkButton = DirectCast(e.Row.FindControl("lbtnView"), LinkButton)
            Dim lbtnDownload As LinkButton = DirectCast(e.Row.FindControl("lbtnDownload"), LinkButton)
            Dim lblFSH_ID As Label = DirectCast(e.Row.FindControl("lblFSH_ID"), Label)
            Dim lblInvNo As Label = DirectCast(e.Row.FindControl("lblInvNo"), Label)
            Dim hf_bInvoiceeSet As HiddenField = DirectCast(e.Row.FindControl("hf_bInvoiceeSet"), HiddenField)
            Dim hf_FSH_STU_ID As HiddenField = DirectCast(e.Row.FindControl("hf_FSH_STU_ID"), HiddenField) 'lblInvNo
            Dim rowView As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
            Dim STU_BSU_ID As String = rowView("STU_BSU_ID").ToString
            If Not lbtnView Is Nothing AndAlso Not hf_bInvoiceeSet Is Nothing AndAlso hf_bInvoiceeSet.Value = 1 Then
                lbtnDownload.Enabled = True
                Dim QueryString = "&ID=" & Encr_decrData.Encrypt(lblFSH_ID.Text) & "&SBSU=" & Encr_decrData.Encrypt(STU_BSU_ID) & ""
                lbtnView.Attributes.Add("onClick", "return ShowWindowWithClose('FeeReportInterpose.aspx?TYPE=" & Encr_decrData.Encrypt(INV_TYPE) & QueryString & "','INVOICE','55%','85%');")
            End If
            If Not lbtnView Is Nothing AndAlso Not hf_bInvoiceeSet Is Nothing AndAlso hf_bInvoiceeSet.Value = 0 Then
                lbtnDownload.Enabled = False
                Dim QueryString = "&ID=" & Encr_decrData.Encrypt(lblFSH_ID.Text) & "&STUID=" & Encr_decrData.Encrypt(hf_FSH_STU_ID.Value) & "&INVNO=" & Encr_decrData.Encrypt(lblInvNo.Text) & "&SBSU=" & Encr_decrData.Encrypt(STU_BSU_ID) & ""
                lbtnView.Attributes.Add("onClick", "return ShowWindowWithClose('FeeChooseTaxInvoicee.aspx?TYPE=" & Encr_decrData.Encrypt(INV_TYPE) & QueryString & "','INVOICE','55%','85%');")
            End If
        End If
    End Sub

    Protected Sub lbtnDownload_Click(sender As Object, e As EventArgs)
        Dim gvr As GridViewRow = DirectCast(DirectCast(sender, LinkButton).Parent.Parent, GridViewRow)
        Dim RowIndex As Integer = gvr.RowIndex
        Dim lblFSH_ID As Label = DirectCast(gvr.FindControl("lblFSH_ID"), Label)
        If Not lblFSH_ID Is Nothing Then
            DownloadPDF(lblFSH_ID.Text)
        End If
    End Sub
    Private Sub DownloadPDF(ByVal FSH_ID As Int64)
        Dim RptFile As String

        Dim param As New Hashtable
        Dim rptClass As New rptClass
        param.Add("UserName", "SYSTEM")
        param.Add("@BSU_ID", Session("sBsuid"))
        param.Add("@FSH_ID", FSH_ID)
        'param.Add("@IMG_BSU_ID", Session("sBsuid"))
        'param.Add("IMG_TYPE", "LOGO")
        RptFile = Server.MapPath("~/Fees/Reports/RPT/rptFeeTAXInvoicePrint.rpt")

        rptClass.reportPath = RptFile
        rptClass.reportParameters = param
        rptClass.crDatabase = ConnectionManger.GetOASIS_FEESConnection.Database
        Dim rptDownload As New DownloadTAXInvoice
        rptDownload.LogoPath = "" 'SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_BSU_GROUP_LOGO,'https://oasis.gemseducation.com/Images/Misc/TransparentLOGO.gif')GROUP_LOGO FROM dbo.BUSINESSUNIT_SUB  WITH ( NOLOCK ) WHERE BUS_BSU_ID='" & Session("sBsuid") & "'")
        rptDownload.BSU_ID = Session("sBsuId")
        rptDownload.FSH_ID = FSH_ID
        rptDownload.Action = "DOWNLOAD"
        'UtilityObj.Errorlog("Calling Loadreports,DOWNLOAD", "OASIS_TPT")
        rptDownload.bEmailSuccess = False
        rptDownload.LoadReports(rptClass)
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        GridBind()
    End Sub
End Class
