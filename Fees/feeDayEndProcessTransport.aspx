<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeDayEndProcessTransport.aspx.vb" Inherits="Fees_feeDayEndProcessTransport" title="Untitled Page" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script Language="javascript" type="text/javascript">
 
 function getPrint(docNo,docDate)
   {     
        var sFeatures;
        sFeatures="dialogWidth: 800px; ";
        sFeatures+="dialogHeight: 700px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        if(docNo.substring(2,4) == "QC" )
        {
        alert('No voucher to print..');
        return ;
        }
       // result = window.showModalDialog("rptDayenddocuments.aspx?DOCNO="+docNo+"&DOCDATE=" + docDate,"", sFeatures)
     //return false;
        var url = "rptDayenddocuments.aspx?DOCNO=" + docNo + "&DOCDATE=" + docDate;
        var oWnd = radopen(url, "pop_getprint");
    }      
  

function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getprint" runat="server" Behaviors="Close,Move" 
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
       
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table align="center" width="100%">
        <tr valign="bottom">
            <td align="left">
               <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>--%>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
            </td>
        </tr>
         
    </table>
    <table allign="center" Width="100%">
        
        <tr >
            <td align="left" width="20%" >
                <span class="field-label">Date</span></td>
            
            <td align="left" width="30%">
                <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" OnClientClick="return getDate(2);"
                    TabIndex="4" />
                <asp:TextBox ID="txtDaysToInclude" runat="server" Visible="False">0</asp:TextBox>
                <asp:Panel id="pnlLink" runat="server">
                    <asp:LinkButton id="lnkBtnSelectmonth" runat="server" OnClientClick="return false;"
                        tabIndex="15">Dayend Details</asp:LinkButton>
                </asp:Panel>
                <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover"
                    PopDelay="25" PopupControlID="PanelTree" PopupPosition="Center" TargetControlID="pnlLink">
                </ajaxToolkit:HoverMenuExtender>
            </td>
       
            <td align="left" width="20%">
                <span class="field-label">Business Unit</span></td>
            
            <td align="left" width="30%">
                <asp:DropDownList ID="ddlBUnit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                    DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" SkinID="DropDownListNormal"
                    TabIndex="5">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" colspan="4">
                <asp:CheckBox id="chkHardClose" runat="server" CssClass="field-label" Text="Hard Close" Checked="True">
                </asp:CheckBox></td>
        </tr>
        <tr>
        <td colspan="4" align="center">
    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" /></td>
        </tr>
    </table>
      <table Width="100%">
        <tr>
            <td class="title-bg" align="left" colspan="4">
                Day End History</td>
        </tr>
          <tr>
              <td align="center" colspan="4">
    <asp:GridView ID="gvHistory" runat="server" CssClass="table table-bordered table-row" Width="100%">
    </asp:GridView>
              </td>
          </tr>
        </table>
    <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:Panel id="PanelTree" runat="server" CssClass="panel-cover" style="display: block">
        <table width="100%">
            <tr>
                <td align="left">
                    <span class="text-dark">Dayend Status</span><asp:GridView id="gvCloseStatus" runat="server" AutoGenerateColumns="True" CssClass="table table-bordered table-row" width="100%"></asp:GridView>
                    <span class="text-dark">Collection Details</span>&nbsp;<br />
                    <asp:GridView id="gvDayend" runat="server" AutoGenerateColumns="True" CssClass="table table-bordered table-row">
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Timer id="Timer1" runat="server" Interval="50000">
    </asp:Timer>
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgFrom" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
            
  </div>
            </div>
        </div>
</asp:Content>
