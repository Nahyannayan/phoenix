Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeFeeReceiptCancellation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            trstudDet.Visible = False
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            txtDate.Text = Format(Now.Date, OASISConstants.DateFormat)
            ddBusinessunit.DataBind()
            ddBusinessunit.SelectedIndex = -1
            ddBusinessunit.Items.FindByValue(Session("sBsuid")).Selected = True
            ViewState("ID") = 1
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_RECEIPT_CANCELLATION Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        End If
    End Sub

    Function ValidateRecieptNo(ByVal FCL_ID As String, ByVal vBSUID As String) As Boolean
        Dim CommandText As String = "SELECT COUNT(FCL_ID) FROM [FEES].[FEECOLLECTION_H] WHERE (FCL_STU_BSU_ID = '" & vBSUID & "' OR ISNULL(FCL_PAYMENT_BSU_ID, '') = '" & vBSUID & "') AND FCL_ID = " & FCL_ID & ""
        Dim vCount As Integer = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, CommandText)
        If vCount > 0 Then
            Return True
        Else
            Return False
        End If
        Return False
    End Function

    Sub UpdateStudentDetails(ByVal FCL_ID As Long, ByVal vBSUID As String)
        trstudDet.Visible = False
        Dim CommandText As New StringBuilder
        CommandText.Append("")
        If rbEnrollment.Checked Then
            CommandText.Append("SELECT B.STU_NO, STU_NAME, OASIS.dbo.fnFormatDateToDisplay(A.FCL_DATE) AS FCL_DATE, FCL_AMOUNT, FCL_STU_BSU_ID, BSU_SHORTNAME ")
            CommandText.Append("FROM FEES.FEECOLLECTION_H AS A WITH (NOLOCK) LEFT OUTER JOIN dbo.VW_OSO_STUDENT_ENQUIRY AS B ")
            CommandText.Append("ON A.FCL_BSU_ID = B.STU_BSU_ID AND A.FCL_STU_ID = B.STU_ID INNER JOIN OASIS.dbo.BUSINESSUNIT_M AS BU WITH(NOLOCK) ")
            CommandText.Append("ON A.FCL_BSU_ID = BU.BSU_ID WHERE (FCL_STU_BSU_ID = '" & vBSUID & "' OR ISNULL(FCL_PAYMENT_BSU_ID, '') = '" & vBSUID & "') ")
            CommandText.Append("AND B.STU_TYPE='S' AND FCL_ID = " & FCL_ID & "")
        Else
            CommandText.Append("SELECT B.STU_NO, STU_NAME, OASIS.dbo.fnFormatDateToDisplay(A.FCL_DATE) AS FCL_DATE, FCL_AMOUNT, FCL_STU_BSU_ID, BSU_SHORTNAME ")
            CommandText.Append("FROM FEES.FEECOLLECTION_H AS A WITH (NOLOCK) LEFT OUTER JOIN dbo.VW_OSO_STUDENT_ENQUIRY AS B ")
            CommandText.Append("ON A.FCL_BSU_ID = B.STU_BSU_ID AND A.FCL_STU_ID = B.STU_ID INNER JOIN OASIS.dbo.BUSINESSUNIT_M AS BU WITH(NOLOCK) ")
            CommandText.Append("WHERE (FCL_STU_BSU_ID = '" & vBSUID & "' OR ISNULL(FCL_PAYMENT_BSU_ID, '') = '" & vBSUID & "') ")
            CommandText.Append("AND B.STU_TYPE='E' AND FCL_ID = '" & FCL_ID & "'")
        End If

        Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, CommandText.ToString)
        While (drReader.Read())
            lblBsuShort.Text = drReader("BSU_SHORTNAME").ToString
            lblStuNo.Text = drReader("STU_NO").ToString
            lblStudentName.Text = drReader("STU_NAME").ToString
            lblRecDate.Text = drReader("FCL_DATE").ToString
            lblAmount.Text = Format(drReader("FCL_AMOUNT"), "0.00")
            ViewState("FCL_STU_BSU_ID") = drReader("FCL_STU_BSU_ID").ToString
            trstudDet.Visible = True
        End While
    End Sub

    Private Function ChechErrors() As Boolean
        Try
            If CDate(txtDate.Text) > Date.Now Then
                'lblError.Text = "Future Date is not allowed"
                ' ShowMessage("Future Date is not allowed", True)
                usrMessageBar.ShowNotification("Future Date is not allowed", UserControls_usrMessageBar.WarningType.Danger)
                Return False
            End If
        Catch
            '  ShowMessage("Date is not valid", True)
            'lblError.Text = "Date is not valid"
            usrMessageBar.ShowNotification("Date is not valid", UserControls_usrMessageBar.WarningType.Danger)
            Return False
        End Try
        Return True
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If Not Master.IsSessionMatchesForSave() Then
            '  lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Not ChechErrors() Then Return
        Dim FCLID As String = hfFCLID.Value
        If Not ValidateRecieptNo(FCLID, ddBusinessunit.SelectedValue) Then
            'lblError.Text = "Receipt No is not Valid..."
            '  ShowMessage("Receipt No is not Valid...", True)
            usrMessageBar.ShowNotification("Receipt No is not Valid...", UserControls_usrMessageBar.WarningType.Danger)

            Return
        End If

        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim trans As SqlTransaction
        Try
            'conn.Open()
            'Dim cmd As New SqlCommand("FEES.DeleteReceipt_OASIS", conn)
            'cmd.CommandType = CommandType.StoredProcedure

            'Dim sqlpAUD_BSU_ID As New SqlParameter("@AUD_BSU_ID", SqlDbType.VarChar, 20)
            'sqlpAUD_BSU_ID.Value = ddBusinessunit.SelectedValue
            'cmd.Parameters.Add(sqlpAUD_BSU_ID)

            'Dim sqlpFCL_RECNO As New SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 20)
            'sqlpFCL_RECNO.Value = txtRecieptNo.Text
            'cmd.Parameters.Add(sqlpFCL_RECNO)


            'Dim sqlpAUD_USER As New SqlParameter("@AUD_USER", SqlDbType.VarChar, 50)
            'sqlpAUD_USER.Value = Session("sUsr_name")
            'cmd.Parameters.Add(sqlpAUD_USER)

            'Dim sqlpAUD_REMARKS As New SqlParameter("@AUD_REMARKS", SqlDbType.VarChar, 100)
            'sqlpAUD_REMARKS.Value = txtRemarks.Text
            'cmd.Parameters.Add(sqlpAUD_REMARKS)

            'Dim sqlpFromDT As New SqlParameter("@DT", SqlDbType.DateTime)
            'sqlpFromDT.Value = CDate(txtDate.Text)
            'cmd.Parameters.Add(sqlpFromDT)

            'Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            'retSValParam.Direction = ParameterDirection.ReturnValue
            'cmd.Parameters.Add(retSValParam)

            'cmd.ExecuteNonQuery()
            'Dim iReturnvalue As Integer = retSValParam.Value
            'If iReturnvalue <> 0 Then

            '    lblError.Text = "Error occured while deleting : " & UtilityObj.getErrorMessage(iReturnvalue)
            'Else

            '    lblError.Text = "Receipt deleted Successfully.."
            '    ClearAll()
            'End If
            SentForApproval()

        Catch ex As Exception
            'lblError.Text = "Error while sending for Approval  " & ex.Message
            ' ShowMessage("Error while sending for Approval  " & ex.Message, True)
            usrMessageBar.ShowNotification("Error while sending for Approval  " & ex.Message, UserControls_usrMessageBar.WarningType.Danger)

        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

    End Sub
    Private Sub SentForApproval()
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim trans As SqlTransaction
        Try
            conn.Open()
            'trans = conn.BeginTransaction("DeleteReceipt_TRANS")

            Dim cmd As New SqlCommand("FEES.DeleteReceipt_OASIS_Approval", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpAUD_BSU_ID As New SqlParameter("@AUD_BSU_ID", SqlDbType.VarChar, 20)
            sqlpAUD_BSU_ID.Value = ddBusinessunit.SelectedValue
            cmd.Parameters.Add(sqlpAUD_BSU_ID)

            Dim sqlpFCL_RECNO As New SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 20)
            sqlpFCL_RECNO.Value = txtRecieptNo.Text
            cmd.Parameters.Add(sqlpFCL_RECNO)


            Dim sqlpAUD_USER As New SqlParameter("@AUD_USER", SqlDbType.VarChar, 50)
            sqlpAUD_USER.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpAUD_USER)

            Dim sqlpAUD_REMARKS As New SqlParameter("@AUD_REMARKS", SqlDbType.VarChar, 100)
            sqlpAUD_REMARKS.Value = txtRemarks.Text
            cmd.Parameters.Add(sqlpAUD_REMARKS)

            Dim sqlpAUD_TYPE As New SqlParameter("@FRCA_TYPE", SqlDbType.VarChar, 10)
            sqlpAUD_TYPE.Value = "FEE"
            cmd.Parameters.Add(sqlpAUD_TYPE)

            Dim sqlpFromDT As New SqlParameter("@DT", SqlDbType.DateTime)
            sqlpFromDT.Value = CDate(txtDate.Text)
            cmd.Parameters.Add(sqlpFromDT)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            Dim sqlpFCLID As New SqlParameter("@FCL_ID", SqlDbType.BigInt)
            sqlpFCLID.Value = hfFCLID.Value
            cmd.Parameters.Add(sqlpFCLID)

            Dim sqlpFCL_STU_BSUID As New SqlParameter("@FCL_STU_BSU_ID", SqlDbType.VarChar, 20)
            sqlpFCL_STU_BSUID.Value = ViewState("FCL_STU_BSU_ID")
            cmd.Parameters.Add(sqlpFCL_STU_BSUID)

            cmd.ExecuteNonQuery()
            Dim iReturnvalue As Integer = retSValParam.Value
            If iReturnvalue <> 0 Then
                'trans.Rollback()
                'lblError.Text = "Error occured while sending for Approval : " & UtilityObj.getErrorMessage(iReturnvalue)
                ' ShowMessage("Error occured while sending for Approval : " & UtilityObj.getErrorMessage(iReturnvalue), True)
                usrMessageBar.ShowNotification("Error occured while sending for Approval : " & UtilityObj.getErrorMessage(iReturnvalue), UserControls_usrMessageBar.WarningType.Danger)
            Else
                'trans.Commit()
                'lblError.Text = "Receipt cancellation request has been saved successfully and forwarded for Approval, and it is valid only for today."
                'ShowMessage("Request sent for approval and is valid only for today.", False)
                usrMessageBar.ShowNotification("Request sent for approval and is valid only for today.", UserControls_usrMessageBar.WarningType.Success)
                ClearAll()
                btnSave.Enabled = False
            End If
        Catch ex As Exception
            ' ShowMessage("Error occured while sending for Approval.........." & ex.Message, True)
            usrMessageBar.ShowNotification("Error occured while sending for Approval..." & ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            'lblError.Text = "Error occured while sending for Approval.........." & ex.Message
            'trans.Rollback()
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

    End Sub

    Private Sub ClearAll()
        lblAmount.Text = ""
        lblStuNo.Text = ""
        lblStudentName.Text = ""
        txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
        txtRecieptNo.Text = ""
        lblRecDate.Text = ""
        txtRemarks.Text = ""
        lblBsuShort.Text = ""
        hfFCLID.Value = 0
        ViewState("FCL_STU_BSU_ID") = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub txtRecieptNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRecieptNo.TextChanged
        'UpdateStudentDetails(txtRecieptNo.Text, ddBusinessunit.SelectedValue)
    End Sub

    Protected Sub imgDocno_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'UpdateStudentDetails(txtRecieptNo.Text, ddBusinessunit.SelectedValue)
    End Sub
    Protected Sub hfFCLID_ValueChanged(sender As Object, e As EventArgs) Handles hfFCLID.ValueChanged
        Dim FCLID As String = DirectCast(sender, HiddenField).Value
        UpdateStudentDetails(FCLID, ddBusinessunit.SelectedValue)
    End Sub
End Class
