﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowStudentInfo.aspx.vb" Inherits="Fees_ShowStudentInfo" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <base target="_self" />
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
   <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
</script>
 <script language="javascript" type="text/javascript">

     function GetRadWindow() {
         var oWindow = null;
         if (window.radWindow) oWindow = window.radWindow;
         else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
         return oWindow;
     }
     function divIMG(pId, val, ctrl1, pImg) {
         var path;
         if (val == 'LI') {
             path = '../Images/operations/like.gif';
         } else if (val == 'NLI') {
             path = '../Images/operations/notlike.gif';
         } else if (val == 'SW') {
             path = '../Images/operations/startswith.gif';
         } else if (val == 'NSW') {
             path = '../Images/operations/notstartwith.gif';
         } else if (val == 'EW') {
             path = '../Images/operations/endswith.gif';
         } else if (val == 'NEW') {
             path = '../Images/operations/notendswith.gif';
         }
         if (pId == 1) {
             document.getElementById("<%=getid("mnu_1_img") %>").src = path;
                }
                else if (pId == 2) {
                    document.getElementById("<%=getid("mnu_2_img") %>").src = path;
               }
               else if (pId == 3) {
                   document.getElementById("<%=getid("mnu_3_img") %>").src = path;
               }
               else if (pId == 4) {
                   document.getElementById("<%=getid("mnu_4_img") %>").src = path;
               }

               else if (pId == 5) {
                   document.getElementById("<%=getid("mnu_5_img") %>").src = path;
               }
               else if (pId == 6) {
                   document.getElementById("<%=getid("mnu_6_img") %>").src = path;
               }
               else if (pId == 7) {
                   document.getElementById("<%=getid("mnu_7_img") %>").src = path;
               }
    document.getElementById(ctrl1).value = val + '__' + path;
}
    </script>
</head>
<body  class="matter" onload="listen_window();" leftmargin="0" topmargin="0" bottommargin="0" rightmargin="0">
    <form id="form1" runat="server">
  <!--1st drop down menu -->                                                   
<div id="dropmenu1" class="dropmenudiv" style="width: 110px;display:none;">
<a href="javascript:divIMG(1,'LI','<%=h_selected_menu_1.ClientID %>','mnu_1_img');"><img class="img_left" alt="Any where" src= "../Images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:divIMG(1,'NLI','<%=h_selected_menu_1.ClientID %>','mnu_1_img');"><img class="img_left" alt="Not In" src= "../Images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:divIMG(1,'SW','<%=h_selected_menu_1.ClientID %>','mnu_1_img');"><img class="img_left" alt="Starts With" src= "../Images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:divIMG(1,'NSW','<%=h_selected_menu_1.ClientID %>','mnu_1_img');"><img class="img_left" alt="Like" src= "../Images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:divIMG(1,'EW','<%=h_selected_menu_1.ClientID %>','mnu_1_img');"><img class="img_left" alt="Like" src= "../Images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:divIMG(1,'NEW','<%=h_selected_menu_1.ClientID %>','mnu_1_img');"><img class="img_left" alt="Like" src= "../Images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>

<!--2nd drop down menu -->                                                
<div id="dropmenu2" class="dropmenudiv" style="width: 110px;display:none;">
<a href="javascript:divIMG(2,'LI','<%=h_selected_menu_2.ClientID %>','mnu_2_img');"><img class="img_left" alt="Any where" src= "../Images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:divIMG(2,'NLI','<%=h_selected_menu_2.ClientID %>','mnu_2_img');"><img class="img_left" alt="Not In" src= "../Images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:divIMG(2,'SW','<%=h_selected_menu_2.ClientID %>','mnu_2_img');"><img class="img_left" alt="Starts With" src= "../Images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:divIMG(2,'NSW','<%=h_selected_menu_2.ClientID %>','mnu_2_img');"><img class="img_left" alt="Like" src= "../Images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:divIMG(2,'EW','<%=h_selected_menu_2.ClientID %>','mnu_2_img');"><img class="img_left" alt="Like" src= "../Images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:divIMG(2,'NEW','<%=h_selected_menu_2.ClientID %>','mnu_2_img');"><img class="img_left" alt="Like" src= "../Images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>

<!--3rd drop down menu -->                                                   
<div id="dropmenu3" class="dropmenudiv" style="width: 150px;display:none;">
<a href="javascript:divIMG(3,'LI','<%=h_selected_menu_3.ClientID %>','mnu_3_img');"><img class="img_left" alt="Any where" src= "../Images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:divIMG(3,'NLI','<%=h_selected_menu_3.ClientID %>','mnu_3_img');"><img class="img_left" alt="Not In" src= "../Images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:divIMG(3,'SW','<%=h_selected_menu_3.ClientID %>','mnu_3_img');"><img class="img_left" alt="Starts With" src= "../Images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:divIMG(3,'NSW','<%=h_selected_menu_3.ClientID %>','mnu_3_img');"><img class="img_left" alt="Like" src= "../Images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:divIMG(3,'EW','<%=h_selected_menu_3.ClientID %>','mnu_3_img');"><img class="img_left" alt="Like" src= "../Images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:divIMG(3,'NEW','<%=h_selected_menu_3.ClientID %>','mnu_3_img');"><img class="img_left" alt="Like" src= "../Images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>

<!--3rd drop down menu -->                                                   
<div id="dropmenu4" class="dropmenudiv" style="width: 150px;display:none;">
<a href="javascript:divIMG(4,'LI','<%=h_selected_menu_4.ClientID %>','mnu_4_img');"><img class="img_left" alt="Any where" src= "../Images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:divIMG(4,'NLI','<%=h_selected_menu_4.ClientID %>','mnu_4_img');"><img class="img_left" alt="Not In" src= "../Images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:divIMG(4,'SW','<%=h_selected_menu_4.ClientID %>','mnu_4_img');"><img class="img_left" alt="Starts With" src= "../Images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:divIMG(4,'NSW','<%=h_selected_menu_4.ClientID %>','mnu_4_img');"><img class="img_left" alt="Like" src= "../Images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:divIMG(4,'EW','<%=h_selected_menu_4.ClientID %>','mnu_4_img');"><img class="img_left" alt="Like" src= "../Images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:divIMG(4,'NEW','<%=h_selected_menu_4.ClientID %>','mnu_4_img');"><img class="img_left" alt="Like" src= "../Images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>

<!--4th drop down menu -->                                                   
<div id="dropmenu5" class="dropmenudiv" style="width: 150px;display:none;">
<a href="javascript:divIMG(5,'LI','<%=h_selected_menu_5.ClientID %>','mnu_5_img');"><img class="img_left" alt="Any where" src= "../Images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:divIMG(5,'NLI','<%=h_selected_menu_5.ClientID %>','mnu_5_img');"><img class="img_left" alt="Not In" src= "../Images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:divIMG(5,'SW','<%=h_selected_menu_5.ClientID %>','mnu_5_img');"><img class="img_left" alt="Starts With" src= "../Images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:divIMG(5,'NSW','<%=h_selected_menu_5.ClientID %>','mnu_5_img');"><img class="img_left" alt="Like" src= "../Images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:divIMG(5,'EW','<%=h_selected_menu_5.ClientID %>','mnu_5_img');"><img class="img_left" alt="Like" src= "../Images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:divIMG(5,'NEW','<%=h_selected_menu_5.ClientID %>','mnu_5_img');"><img class="img_left" alt="Like" src= "../Images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>
    <table width="100%" align="center" >
                <tr>
                    <td align="center">
                        <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"  Width="100%" EmptyDataText="No Data" CssClass="table table-bordered table-row" AllowPaging="True" PageSize="20" SkinID="GridViewView">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       
                                                    Student#
                                                                <div id="chromemenu1" class="chromestyle" style="display:none;">
                                                                    <ul>
                                                                        <li><a href="#" rel="dropmenu1">
                                                                            <img id="mnu_1_img" alt="Menu" runat="server" align="middle"  border="0" src="../Images/operations/like.gif" /> 
                                                                                 </a> </li>
                                                                    </ul>
                                                                </div>
                                                           <br />
                                                                <asp:TextBox ID="txtCode" runat="server" Width="72px"></asp:TextBox>
                                                                <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />&nbsp;
                                                       
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCode" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       
                                                    Student Name
                                                                 <div id="chromemenu" class="chromestyle" style="display:none;">
                                                                    <ul>
                                                                        <li><a href="#" rel="dropmenu2">
                                                                            <img id="mnu_2_img" alt="Menu" runat="server" align="middle" border="0" src="../Images/operations/like.gif" /><span
                                                                                style="font-size: 12pt; color: #0000ff;text-decoration: none">&nbsp;</span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                          <br />
                                                                <asp:TextBox ID="txtName" runat="server" Width="80px"></asp:TextBox>
                                                                <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />&nbsp;
                                                        
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        &nbsp;<asp:LinkButton ID="lbCodeSubmit" runat="server" OnClick="LinkButton1_Click" Text='<%# Bind("STU_NAME") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       
                                                    Grade
                                          
                                                               <div id="chromemenu2" class="chromestyle" style="display:none;">
                                                                    <ul>
                                                                        <li><a href="#" rel="dropmenu3">
                                                                            <img id="mnu_3_img" alt="Menu" runat="server" align="middle" border="0" src="../Images/operations/like.gif" /><span
                                                                                style="font-size: 12pt; color: #0000ff;text-decoration: none">&nbsp;</span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                         
                                                                <asp:TextBox ID="txtGrade" runat="server" Width="54px"></asp:TextBox>
                                                                <asp:ImageButton ID="btnSearchControl" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />&nbsp;
                                                        
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("GRD_DISPLAY") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField >
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("PARENT_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                      
                                                    Parent Name
                                                                <div id="chromemenu4" class="chromestyle" style="display:none;">
                                                                    <ul>
                                                                        <li><a href="#" rel="dropmenu3">
                                                                            <img id="mnu_4_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" /> 
                                                                                 </a> </li>
                                                                    </ul>
                                                                </div>
                                                         
                                                                <asp:TextBox ID="txtPName" runat="server" SkinID="Gridtxt" Width="48px"></asp:TextBox>
                                                         
                                                                <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                    OnClick="ImageButton1_Click" />&nbsp;
                                                        
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Parent Mobile">
                                    <HeaderTemplate>
                                       
                                                    Parent Mobile
                                         
                                                                <div id="chromemenu5" class="chromestyle" style="display:none;">
                                                                    <ul>
                                                                        <li><a href="#" rel="dropmenu5">
                                                                            <img id="mnu_5_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" /><span
                                                                                style="font-size: 12pt; color: #0000ff; text-decoration: none">&nbsp;</span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                          
                                                                <asp:TextBox ID="txtMobile" runat="server" SkinID="Gridtxt" Width="58px"></asp:TextBox>
                                                          
                                                                <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                    OnClick="btnSearchName_Click" />&nbsp;
                                                      
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("PARENT_MOBILE") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>                              
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>                
                <tr>
                    <td>
                        <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />                        
                        <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                        </td>                    
                </tr>
            </table>    
 <script type="text/javascript">
     cssdropdown.startchrome("chromemenu");
     cssdropdown.startchrome("chromemenu1");
     cssdropdown.startchrome("chromemenu2");
     cssdropdown.startchrome("chromemenu4");
     cssdropdown.startchrome("chromemenu5");
 </script>
</form>
</body>
</html>

