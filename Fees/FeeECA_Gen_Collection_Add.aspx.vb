﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class StudentServices_FeeECA_Gen_Collection_Add
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property ShowNextStudentForCollection() As Boolean
        Get
            Return ViewState("ShowNextStudentForCollection")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ShowNextStudentForCollection") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                ClientScript.RegisterStartupScript(Me.GetType(), _
                        "script", "<script language='javascript'>  CheckForPrint(); </script>")
                Dim RegisteredStuID As String = ""
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("RegisteredStuID") = ""
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    If Request.QueryString("STUID") <> "" Then
                        ViewState("RegisteredStuID") = Encr_decrData.Decrypt(Request.QueryString("STUID").Replace(" ", "+"))
                    End If

                End If
                bindBusinessUnits()
                If Session("sBsuid") = "500605" Then
                    ddlBusinessunit.Items.FindByValue("500605").Selected = True
                End If
                FillACD()
                bindFeeTypes()
                txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
                tr_AddFee.Visible = True
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or _
              (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ECA_FEE_COLLECTION And ViewState("MainMnu_code") <> "ER01011") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Session("sBsuid") = "500605" And ViewState("RegisteredStuID").ToString <> "" Then
                    SetNextStudentForCollection(ViewState("RegisteredStuID"))
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        'If ShowNextStudentForCollection = True And Session("sBsuid") = "500605" Then
        '    ShowNextStudentForCollection = False
        '    SetNextStudentForCollection(ViewState("RegisteredStuID"))
        'End If
    End Sub
    Sub SetNextStudentForCollection(RegisteredStuID As String)
        Dim str_sql, str_conn As String
        str_conn = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim regStuIds As String(), i As Integer
        i = 0
        regStuIds = RegisteredStuID.Split("|")
        If regStuIds.Length > 0 Then
            Dim StuId As String
            While i <= regStuIds.Length - 1
                StuId = regStuIds(i)
                If StuId.ToString.Replace("|", "").Replace(" ", "") <> "" Then
                    str_sql = "SELECT STU_NO  FROM ENR.REG_STUDENT_M WHERE STU_ID=" & StuId
                    txtStdNo.Text = GetDataFromSQL(str_sql, str_conn)
                    str_sql = "SELECT ISNULL(STU_FIRST_NAME, '') + ' ' + ISNULL(STU_MIDDLE_NAME, '') + ' ' + ISNULL(STU_LAST_NAME, '') STU_NAME  FROM ENR.REG_STUDENT_M" _
                            & " WHERE STU_NO='" & txtStdNo.Text & "'"
                    txtStudentname.Text = GetDataFromSQL(str_sql, str_conn)
                    Gridbind_Feedetails()
                    i = regStuIds.Length + 1
                End If
                i = i + 1
            End While



        End If
    End Sub

    'Sub clear_All()
    '    txtCCTotal.Text = ""
    '    txtCashTotal.Text = ""
    '    txtTotal.Text = ""
    '    txtReceivedTotal.Text = ""
    '    txtBankTotal.Text = ""
    '    If Not chkRemember.Checked Then
    '        txtCreditno.Text = ""
    '        h_Bank1.Value = ""
    '        h_Bank2.Value = ""
    '        h_Bank3.Value = ""
    '        txtBankAct1.Text = ""
    '        hfBankAct1.Value = ""
    '        txtCreditno.Text = ""
    '        txtCCTotal.Text = 0
    '    End If
    '    txtBalance.Text = ""
    '    txtCCTotal.Text = 0

    '    txtRemarks.Text = ""
    '    txtAmountAdd.Text = ""
    '    SetNarration()
    '    chkRemember.Checked = False
    'End Sub
    Sub BindEmirate()
        Try
            If ddlBusinessunit.SelectedItem IsNot Nothing Then
                Dim str_default_emirate As String = GetDataFromSQL("SELECT BSU_CITY FROM BUSINESSUNIT_M WHERE BSU_ID='" & ddlBusinessunit.SelectedItem.Value & "'", ConnectionManger.GetOASISConnectionString)
                If str_default_emirate <> "--" Then
                    ddlEmirateMain.SelectedIndex = -1
                    ddlEmirateMain.Items.FindByValue(str_default_emirate).Selected = True
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message, "emirate")
        End Try
    End Sub
    Protected Sub btnAionddDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDetails.Click
        ''setFeeCollection()
        If txtStdNo.Text = "" Or txtStudentname.Text = "" Then
            'lblError.Text = "Select student!!!"
            usrMessageBar.ShowNotification("Select student!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        '''''''''''''
        If Not IsDate(txtFrom.Text) Then
            'lblError.Text = "Invalid Date!!!"
            usrMessageBar.ShowNotification("Invalid Date!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dr As DataRow
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If IsNumeric(txtAmountAdd.Text) Then
            If CDbl(txtAmountAdd.Text) > 0 Then
                'FEE_ID, FEE_DESCR, Opening, CurrentCharge, Net,Amount
                'FSH_ID, FSH_DATE, FEE_ID, FEE_DESCR, AMOUNT, FSH_naRRATION
                For Each gvr As GridViewRow In gvFeeCollection.Rows
                    Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)

                    If Not lblFSR_FEE_ID Is Nothing Then
                        If lblFSR_FEE_ID.Text = ddlFeeType.SelectedItem.Value Then
                            'lblError.Text = "Fee repeating!!!"
                            usrMessageBar.ShowNotification("Fee repeating!!!", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                    End If
                Next
                dr = Session("FeeCollection").NewRow
                dr("FEE_ID") = ddlFeeType.SelectedItem.Value
                dr("FEE_DESCR") = ddlFeeType.SelectedItem.Text
                dr("CLOSING") = 0
                dr("Amount") = txtAmountAdd.Text
                dr("bChargeandPost") = "0"
                dr("PAID_AMOUNT") = "0"
                If ChkChgPost.Checked Then
                    dr("bChargeandPost") = "1"
                    dr("CLOSING") = txtAmountAdd.Text
                End If
                Session("FeeCollection").rows.add(dr)
                gvFeeCollection.DataSource = Session("FeeCollection")
                txtAmountAdd.Text = ""
                gvFeeCollection.DataBind()
                Set_GridTotal()
                smScriptManager.SetFocus(txtCashTotal)
            Else
                'lblError.Text = "Invalid Amount"
                usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
                smScriptManager.SetFocus(txtAmountAdd)
            End If
        Else
            'lblError.Text = "Invalid Amount"
            usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
            smScriptManager.SetFocus(txtAmountAdd)
        End If

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
        '    '' clear_All()
        '    ViewState("datamode") = "view"
        '    ''LockYN(False)
        '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        'Else
        Response.Redirect(ViewState("ReferrerUrl"))
        'End If
    End Sub
    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        '' clear_All()
        FillACD()
        BindEmirate()
        'PopulateMonths()
        'ddMachineList.DataBind()
        'If Not Session("sDEFAULTCCMACHINE") Is Nothing Then
        '    Dim httab As Hashtable = Session("sDEFAULTCCMACHINE")
        '    ddMachineList.SelectedValue = httab(ddlBusinessunit.SelectedValue)
        'End If
        'BindEmirate()
        'SetTermMonth()
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim x As Integer = 0
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim imgBank1 As New ImageButton
            Dim txtDBank As New TextBox
            Dim h_Bank As New HiddenField
            Dim txtChqno As New TextBox
            Dim txtChequeTotal As New TextBox
            imgBank1 = e.Row.FindControl("imgBank1")
            txtDBank = e.Row.FindControl("txtDBank")
            h_Bank = e.Row.FindControl("h_Bank")
            txtChqno = e.Row.FindControl("txtChqno")
            imgBank1.Attributes.Add("onclick", " return getBankOrEmirate('1','" & txtDBank.ClientID & "','" & h_Bank.ClientID & "' )")
            If h_BankId.Value.Trim <> "" And txtBank.Text.Trim <> "" Then
                txtDBank.Text = txtBank.Text
                h_Bank.Value = h_BankId.Value
            End If
            If IsNumeric(txtChequeNo.Text) Then
                txtChqno.Text = Convert.ToInt64(txtChequeNo.Text.ToString()) + x
                x = x + 1
            End If
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'clear_All()
        'Set_Previous_Acd()
        'PopulateMonths()
        Gridbind_Feedetails()
    End Sub

    Protected Sub txtAmountToPay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SetNarration()
    End Sub

    Protected Sub txtStdNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStdNo.TextChanged
        Gridbind_Feedetails()
    End Sub

    Protected Sub txtDBank_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtAmount As New TextBox
        txtAmount = sender
        txtAmount.Attributes.Add("readonly", "readonly")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'If Not Master.IsSessionMatchesForSave() Then
        '    lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
        '    Exit Sub
        'End If
        setFeeCollection()
        Set_GridTotal()
        getTotal(True)
        Dim str_error As String = check_errors_details()
        Dim dblReceivedAmount, dblTotal, dblCash, dblBalance As Decimal, dtFeeCollection As New DataTable
        dblReceivedAmount = CDbl(txtReceivedTotal.Text)
        dblTotal = CDbl(txtTotal.Text)
        dblCash = CDbl(txtCashTotal.Text)
        If dblReceivedAmount <> dblTotal Then
            If dblCash > 0 Then
                If dblTotal < dblReceivedAmount Then
                    dblBalance = dblReceivedAmount - dblTotal
                    If dblBalance > dblCash Then
                        str_error = str_error & "Cannot Refund!!!(Excess amount)<br />"
                        txtReceivedTotal.Text = 0
                    Else
                        txtBalance.Text = Format(dblBalance, "0.00")
                    End If
                Else
                    str_error = str_error & "Totals not tallying<br />"
                End If
            Else
                txtBalance.Text = Format(CDbl(txtBalance.Text), "0.00")
                str_error = str_error & "Totals not tallying<br />"
            End If
        End If
        dblBalance = CDbl(txtBalance.Text)

        If str_error <> "" Then
            'lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim str_new_FCL_ID As Long, str_NEW_FCL_RECNO As String = "", str_type As String = "S", CRH_ID As Long = 0
        If rbEnquiry.Checked Then
            str_type = "E"
        End If


        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            dtFeeCollection = DirectCast(Session("FeeCollection"), DataTable)
            If dtFeeCollection.Columns.Contains("CRH_ID") Then
                CRH_ID = Convert.ToInt64(dtFeeCollection.Rows(0)("CRH_ID"))
            End If
            Dim retval As String = "1000"
            retval = ECA_Fee_Management.F_SaveFEECOLLECTION_H_ECA(0, "Counter", txtFrom.Text, _
                              txtRemarks.Text, ddlAcademicYear.SelectedItem.Value, _
                              h_StuID.Value, _
                              CDec(txtReceivedTotal.Text) - dblBalance, Session("sUsr_name"), _
                              False, str_new_FCL_ID, Session("sBsuid"), txtRemarks.Text, "CR", str_NEW_FCL_RECNO, _
                              0, ddlBusinessunit.SelectedItem.Value, _
                              "H_Location.Value", str_type, CRH_ID, stTrans)

            For I As Integer = 0 To dtFeeCollection.Rows.Count - 1
                If retval = "0" Then
                    Dim bChargeandPost As Boolean = False, CRD_ID As Long = 0
                    If dtFeeCollection.Rows(I)("bChargeandPost") = "1" Then
                        bChargeandPost = True
                    End If
                    Dim FCS_AMOUNT As Decimal = 0, FCS_FEE_ID As Integer = 0
                    If IsNumeric(dtFeeCollection.Rows(I)("FEE_ID")) Then
                        FCS_FEE_ID = Convert.ToInt32(dtFeeCollection.Rows(I)("FEE_ID"))
                    End If
                    If IsNumeric(dtFeeCollection.Rows(I)("amount")) Then
                        FCS_AMOUNT = Convert.ToDecimal(dtFeeCollection.Rows(I)("amount"))
                    End If
                    If dtFeeCollection.Columns.Contains("CRD_ID") Then
                        CRD_ID = Convert.ToInt64(dtFeeCollection.Rows(I)("CRD_ID"))
                    End If

                    retval = ECA_Fee_Management.F_SaveFEECOLLSUB_ECA(0, str_new_FCL_ID, FCS_FEE_ID, FCS_AMOUNT, bChargeandPost, CRD_ID, stTrans)

                    If retval <> 0 Then
                        Exit For
                    End If
                End If
            Next
            If retval = "0" Then
                If CDbl(txtCCTotal.Text) > 0 And retval = "0" Then
                    retval = ECA_Fee_Management.F_SaveFEECOLLSUB_D_ECA(0, str_new_FCL_ID, COLLECTIONTYPE.CREDIT_CARD, _
                    CDbl(txtCCTotal.Text), txtCreditno.Text, txtFrom.Text, 0, "", ddCreditcard.SelectedItem.Value, _
                    "", ddMachineList.SelectedItem.Value, stTrans)
                End If
                If CDbl(txtCashTotal.Text) > 0 And retval = "0" Then
                    retval = ECA_Fee_Management.F_SaveFEECOLLSUB_D_ECA(0, str_new_FCL_ID, COLLECTIONTYPE.CASH, _
                   CDbl(txtCashTotal.Text), "", txtFrom.Text, 0, "", "", "", 0, stTrans)
                End If
                If CDbl(txtBankTotal.Text) > 0 And retval = "0" Then
                    If h_BankId.Value.ToUpper <> "65" OrElse txtBank.Text.ToUpper <> "BANK TRANSFER" Then
                        hfBankAct1.Value = ""
                    End If
                    retval = ECA_Fee_Management.F_SaveFEECOLLSUB_D_ECA(0, str_new_FCL_ID, COLLECTIONTYPE.CHEQUES, _
                            CDbl(txtBankTotal.Text), txtChequeNo.Text, txtChqdate.Text, 0, "", h_BankId.Value, _
                            ddlEmirateMain.SelectedItem.Value, 0, stTrans, hfBankAct1.Value)
                End If

                If retval = "0" Then
                    retval = ECA_Fee_Management.F_SaveFEECOLLALLOCATION_D_ECA(ddlBusinessunit.SelectedItem.Value, _
                    str_new_FCL_ID, stTrans)
                End If
                If retval = "0" Then
                    '' retval = FeeCollectionTransport.F_SettleFee_Transport(Session("sBsuid"), txtFrom.Text, h_Student_no.Value, _
                    ''ddlBusinessunit.SelectedItem.Value, stTrans)
                End If
                If Not chkRemember.Checked And retval = "0" Then
                    If ViewState("FCL_IDS") Is Nothing Then
                        retval = ECA_Fee_Management.F_SAVECHEQUEDATA_ECA(Session("sBsuid"), _
                        str_new_FCL_ID, ddlBusinessunit.SelectedItem.Value, Session("sUsr_name"), stTrans)
                    Else
                        retval = ECA_Fee_Management.F_SAVECHEQUEDATA_ECA(Session("sBsuid"), _
                        ViewState("FCL_IDS").ToString & "|" & str_new_FCL_ID, ddlBusinessunit.SelectedItem.Value, Session("sUsr_name"), stTrans)
                    End If
                End If
                If retval = "0" Then
                    If chkRemember.Checked Then
                        If ViewState("FCL_IDS") Is Nothing Then
                            ViewState("FCL_IDS") = str_new_FCL_ID
                        Else
                            ViewState("FCL_IDS") = ViewState("FCL_IDS").ToString & "|" & str_new_FCL_ID
                        End If
                    Else
                        ViewState("FCL_IDS") = Nothing
                    End If


                    ViewState("recno") = str_NEW_FCL_RECNO
                    'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_NEW_FCL_RECNO, _
                    '"Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If Session("sBsuid") = "500605" And ViewState("RegisteredStuID").ToString <> "" Then
                        ViewState("RegisteredStuID") = ViewState("RegisteredStuID").ToString.Replace(h_StuID.Value, "")
                    End If
                    stTrans.Commit()
                    Session("FeeCollection").rows.clear()
                    h_print.Value = "print"
                    Session("ReportSource") = FeeCollectionTransport.PrintReceipt(str_NEW_FCL_RECNO, Session("sUsr_name"), ddlBusinessunit.SelectedItem.Value, Session("sBsuid"))
                    h_print.Value = "?type=REC&id=" + Encr_decrData.Encrypt(str_NEW_FCL_RECNO) & _
                    "&bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) & _
                    "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & _
                    "&stu_bsu_id=" & Encr_decrData.Encrypt(ddlBusinessunit.SelectedItem.Value) & _
                    "&isexport=0"
                    'lblError.Text = getErrorMessage("0")
                    usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                    'clear_All()
                    'PopulateMonths()
                    Dim httab As New Hashtable
                    If Not Session("sDEFAULTCCMACHINE") Is Nothing Then
                        httab = Session("sDEFAULTCCMACHINE")
                    End If
                    httab(ddlBusinessunit.SelectedValue) = ddMachineList.SelectedValue
                    Session("sDEFAULTCCMACHINE") = httab
                    'If Session("sBsuid") = "500605" And ViewState("RegisteredStuID").ToString <> "" Then
                    '    'clear_All()
                    '    ShowNextStudentForCollection = True
                    'End If
                Else
                    stTrans.Rollback()
                    'lblError.Text = getErrorMessage(retval)
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If

        Catch ex As Exception
            stTrans.Rollback()
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub txtBank_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBank.TextChanged
        Dim str_bankid As String = ""
        Dim str_bankho As String = ""
        txtBank.Text = FeeCommon.GetBankName(txtBank.Text, str_bankid, str_bankho)
        h_BankId.Value = str_bankid
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        If txtBank.Text.Trim <> "" Then
            If txtBank.Text.Contains("Bank Transfer") Then
                smScriptManager.SetFocus(txtBankAct1)
            Else
                smScriptManager.SetFocus(ddlEmirateMain)
            End If
            If str_bankho <> "" Then
                ddlEmirateMain.SelectedIndex = -1
                ddlEmirateMain.Items.FindByValue(str_bankho).Selected = True
            End If
        Else
            h_BankId.Value = ""
            smScriptManager.SetFocus(txtBank)
            lblBankInvalid.Text = "Invalid Bank Selected"
        End If
    End Sub

    Private Function GetTermsSelected() As String
        Try
            '    Dim str_Selected As String = ""
            '    For Each node As TreeNode In trMonths.CheckedNodes
            '        If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
            '            If node.Value <> "ALL" Then
            '                If str_Selected = "" Then
            '                    str_Selected = node.Value & "|"
            '                Else
            '                    str_Selected = str_Selected & node.Value & "|"
            '                End If
            '            End If
            '        Else
            '            Continue For
            '        End If
            '    Next
            '    Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Private Function GetMonthsSelected() As String
        Try
            Dim str_Selected As String = ""
            'For Each node As TreeNode In trMonths.CheckedNodes
            '    If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
            '        Continue For
            '    End If
            '    If str_Selected = "" Then
            '        str_Selected = node.Value & "|"
            '    Else
            '        str_Selected = str_Selected & node.Value & "|"
            '    End If
            'Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Protected Sub LinkButton10_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton10.Click
        bind_cheques()
    End Sub
    Private Function getChequeAmt() As Double
        Dim ChqAmount As Double = 0
        If (txtBankTotal.Text <> "" And IsNumeric(txtBankTotal.Text.Trim)) Then
            ChqAmount = CDbl(txtBankTotal.Text)
        End If

        Return ChqAmount
    End Function
    Private Function getTotal(ByVal withCheque As Boolean) As Double
        If txtCCTotal.Text = "" Or Not IsNumeric(txtCCTotal.Text.Trim) Then
            txtCCTotal.Text = 0
        End If
        If txtCashTotal.Text = "" Or Not IsNumeric(txtCashTotal.Text.Trim) Then
            txtCashTotal.Text = 0
        End If
        Dim TotalAmt As Double = 0
        If withCheque = True Then
            TotalAmt = getChequeAmt()
        End If
        txtBankTotal.Text = TotalAmt.ToString("#,##0.00")
        TotalAmt += CDbl(txtCCTotal.Text) + CDbl(txtCashTotal.Text)
        txtReceivedTotal.Text = TotalAmt.ToString("#,##0.00")
        Return TotalAmt
    End Function
    Sub ClearChqDetails()
        gvChequeDetails.DataSource = Nothing
        gvChequeDetails.DataBind()
        getTotal(False)
    End Sub

    Sub bind_cheques()
        ClearChqDetails()

        Dim lastColumn As Integer = 0
        Dim dt As DataTable
        Dim sch_id As Integer
        Dim str_monthorTerm As String
        Dim FirstMonthAmount As Double = 0

        If rbTermly.Checked Then
            str_monthorTerm = GetTermsSelected()
            sch_id = 2
        Else
            str_monthorTerm = GetMonthsSelected()
            sch_id = 0
        End If
        If str_monthorTerm = "" Then
            str_monthorTerm = "N"
        End If

        If Not IsDate(txtChqdate.Text) Then
            txtChqdate.Text = Format(Date.Now, "dd/MMM/yyyy")
        End If

        'If ddlFeeType.SelectedItem.Value <> 6 Then
        '    dt = GETFEEPAYABLE_Detail_Next(ddlAcademicYear.SelectedItem.Value, _
        '                        ddlBusinessunit.SelectedItem.Value, str_monthorTerm, 0, sch_id, txtChqdate.Text, _
        '                        ConnectionManger.GetOASISTRANSPORTConnectionString, Int32.Parse(ddlTotchqs.SelectedValue.ToString))
        'End If

        'Dim DTS As DataTable
        'DTS.Columns.Add("id", GetType(Integer))
        'DTS.Columns.Add("DATE", GetType(Date))
        'DTS.Columns.Add("Amount", GetType(Double))

        SetChqamount(lastColumn)
    End Sub

    Function check_errors_details() As String
        Dim str_error As String = ""
        'If txtCCTotal.Text = "" Then
        '    txtCCTotal.Text = 0
        'End If
        'If txtCashTotal.Text = "" Then
        '    txtCashTotal.Text = 0
        'End If
        'txtBankTotal.Text = getChequeAmt().ToString("###.00")

        'txtReceivedTotal.Text = CDbl(txtCCTotal.Text) + CDbl(txtCashTotal.Text) + CDbl(txtBankTotal.Text)
        txtReceivedTotal.Text = getTotal(True).ToString("#,##0.00")
        txtBalance.Text = CDbl(txtReceivedTotal.Text) - CDbl(txtTotal.Text)

        If Not IsDate(txtFrom.Text) Then
            str_error = str_error & "Invalid from date <br />"
        End If
        If h_StuID.Value = "" Then
            str_error = str_error & "Please select student <br />"
        End If
        'If dtFrom > Now.Date Then
        '    str_error = str_error &"Invalid date(Future Date)!!!" 
        'End If
        'If H_Location.Value = "" Then
        '    str_error = str_error & "Please set the area <br />"
        'End If
        If gvFeeCollection.Rows.Count = 0 Then
            str_error = str_error & "Please add fee details<br />"
        End If

        If Not IsNumeric(txtCCTotal.Text) Then
            str_error = str_error & "Invalid Credit Card amount <br />"
        Else
            If CDbl(txtCCTotal.Text) > 0 Then
                If txtCreditno.Text.Trim = "" Then
                    str_error = str_error & "Invalid Credit Card transaction # <br />"
                End If
            End If
        End If
        If Not IsNumeric(txtCashTotal.Text) Then
            str_error = str_error & "Invalid Cash amount <br />"
        End If

        Return str_error
    End Function

    Sub Set_GridTotal()
        Dim dAmount As Decimal = 0
        Dim dueAmount As Decimal = 0
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            dueAmount = dAmount + CDbl(gvr.Cells(7).Text)
            Dim ChkPost As CheckBox = CType(gvr.FindControl("ChkPost"), CheckBox)
            Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
            If Not txtAmountToPay Is Nothing Then
                If txtAmountToPay.Text.Trim = "" OrElse Not IsNumeric(txtAmountToPay.Text) Then
                    txtAmountToPay.Text = "0.00"
                End If
                dAmount = dAmount + CDbl(txtAmountToPay.Text)
            End If
            If Session("FeeCollection").rows(gvr.RowIndex)(("bChargeandPost")) = 1 Then
                ChkPost.Checked = True
            End If
            Dim lblFSR_FEE_ID As Label = TryCast(gvr.FindControl("lblFSR_FEE_ID"), Label)
            If lblFSR_FEE_ID.Text = OASISConstants.TransportFEEID Then
                ChkPost.Enabled = False
            Else
                ChkPost.Enabled = True
            End If
        Next
        txtTotal.Text = Format(dAmount, "0.00")
        '' h_du.Value = dueAmount.ToString()
    End Sub

    Sub bindBusinessUnits()
        ddlBusinessunit.Items.Clear()



        Dim ds As New DataSet
        Dim PARAM(5) As SqlParameter


        Try
            PARAM(0) = New SqlParameter("@providerBsuId", Session("sbsuId"))
            PARAM(1) = New SqlParameter("@userId", Session("sUsr_id"))
            PARAM(2) = New SqlParameter("@menuCode", ViewState("MainMnu_code"))
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, _
            CommandType.StoredProcedure, "SM.Get_GMA_BusinessUnit", PARAM)

            ddlBusinessUnit.DataSource = ds.Tables(0)
            ddlBusinessUnit.DataTextField = "BSU_NAME"
            ddlBusinessUnit.DataValueField = "BSU_ID"
            ddlBusinessUnit.DataBind()
            ddlBusinessUnit.SelectedIndex = -1
            ddlBusinessUnit.Items.Insert(0, New ListItem("Please Select", "0"))
            '' ddlBusinessunit_SelectedIndexChanged(ddlBusinessUnit, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind pdc")
        End Try
    End Sub

    Sub bindFeeTypes()
        ddlFeeType.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SM.GetFeeTypes")



        ddlFeeType.DataSource = ds
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()


    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = ECA_Fee_Management.GetECABSUAcademicYear(Session("sBsuid"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        '' Set_Previous_Acd()
    End Sub

    Sub Gridbind_Feedetails()

        If txtStdNo.Text.Trim <> "" Then
            Dim bNoError As Boolean = True
            Try

                txtStdNo.Text = txtStdNo.Text.Trim
                Dim iStdnolength As Integer = txtStdNo.Text.Length
                If iStdnolength < 9 Then
                    If txtStdNo.Text.Trim.Length < 8 Then
                        For i As Integer = iStdnolength + 1 To 8
                            txtStdNo.Text = "0" & txtStdNo.Text
                        Next
                    End If
                    txtStdNo.Text = ddlBusinessunit.SelectedItem.Value & txtStdNo.Text
                End If

            Catch ex As Exception
                bNoError = True
            End Try
            Dim StuId As String = String.Empty
            If txtStdNo.Text <> "" AndAlso h_StuID.Value = "" Then
                If Session("sBsuid") = "500605" Then
                    Dim str_sql, str_conn As String
                    str_conn = ConnectionManger.GetOASIS_SERVICESConnectionString
                    str_sql = "SELECT STU_ID  FROM ENR.REG_STUDENT_M" _
                     & " WHERE STU_NO='" & txtStdNo.Text & "'"
                    h_StuID.Value = GetDataFromSQL(str_sql, str_conn)
                    str_sql = "SELECT ISNULL(STU_FIRST_NAME, '') + ' ' + ISNULL(STU_MIDDLE_NAME, '') + ' ' + ISNULL(STU_LAST_NAME, '') STU_NAME  FROM ENR.REG_STUDENT_M" _
                   & " WHERE STU_NO='" & txtStdNo.Text & "'"
                    txtStudentname.Text = GetDataFromSQL(str_sql, str_conn)
                Else
                    h_StuID.Value = ECA_Fee_Management.GetECAStudentID(txtStdNo.Text, ddlBusinessunit.SelectedValue, False)
                End If
            End If
            If IsDate(txtFrom.Text) And bNoError Then
                'Gridbind_CollectionHistory()
                Dim dt As New DataTable
                'If rbEnquiry.Checked Then
                ' dt = FeeCollectionTransport.F_GetFeeDetailsForCollectionTransport(txtFrom.Text, h_Student_no.Value, "E", ddlBusinessunit.SelectedItem.Value, ddlAcademicYear.SelectedValue)
                'Else
                dt = ECA_Fee_Management.F_GetFeeDetailsForCollectionECA(txtFrom.Text, h_StuID.Value, "S", Session("sBsuid"), ddlAcademicYear.SelectedValue)
                'End If
                'lblChqBalance.Text = GetChequeData(h_Student_no.Value, txtFrom.Text, 1)
                gvFeeCollection.DataSource = dt
                'If dt.Rows.Count = 0 Then
                '    tr_AddFee.Visible = True
                '    'pnlMonthterm.Visible = True
                'Else
                '    tr_AddFee.Visible = False
                '    'pnlMonthterm.Visible = False
                'End If
                Session("FeeCollection") = dt
                gvFeeCollection.DataBind()
                Set_GridTotal()
            Else
                gvFeeCollection.DataBind()
                Set_GridTotal()
            End If
            SetNarration()
        End If
    End Sub
    Sub SetNarration()
        Try
            If IsDate(txtFrom.Text) Then
                Dim lblFSR_FEE_ID As Label, StrFEEIDs As String
                StrFEEIDs = ""
                For Each gvr As GridViewRow In gvFeeCollection.Rows
                    Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
                    If Not txtAmountToPay Is Nothing Then
                        If IsNumeric(txtAmountToPay.Text) Then
                            If txtAmountToPay.Text > 0 Then
                                lblFSR_FEE_ID = TryCast(gvr.FindControl("lblFSR_FEE_ID"), Label)
                                If lblFSR_FEE_ID IsNot Nothing Then
                                    StrFEEIDs = StrFEEIDs + "|" & lblFSR_FEE_ID.Text
                                End If
                            End If
                        End If
                    End If
                Next
                txtRemarks.Text = F_GetFeeNarration(txtFrom.Text, ddlAcademicYear.SelectedItem.Value, Session("sBsuid"), h_StuID.Value, StrFEEIDs)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Shared Function F_GetFeeNarration(ByVal p_DOCDT As String, _
   ByVal p_ACD_ID As String, ByVal p_BSU_ID As String, ByVal p_STU_ID As String, ByVal p_StrFEEIDs As String) As String
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
        pParms(0).Value = p_DOCDT
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(3).Value = p_STU_ID
        pParms(4) = New SqlClient.SqlParameter("@StrFEEIDs", SqlDbType.VarChar, 20)
        pParms(4).Value = p_StrFEEIDs
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, _
          CommandType.StoredProcedure, "FEES.F_GetFeeNarration", pParms)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return dsData.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function
    Sub setFeeCollection()
        Dim iEdit, iRowcount As Integer
        iRowcount = 0
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            Dim lblFSR_FEE_ID As Label = TryCast(gvr.FindControl("lblFSR_FEE_ID"), Label)
            Dim ChkPost As CheckBox = CType(gvr.FindControl("ChkPost"), CheckBox)
            If Not lblFSR_FEE_ID Is Nothing Then
                If lblFSR_FEE_ID.Text = OASISConstants.TransportFEEID Then
                    ChkPost.Enabled = False
                Else
                    ChkPost.Enabled = True
                End If
                iRowcount = iRowcount + 1
                For iEdit = 0 To Session("FeeCollection").Rows.Count - 1
                    If lblFSR_FEE_ID.Text = Session("FeeCollection").Rows(iEdit)("FEE_ID") Then
                        Exit For
                    End If
                Next
            End If
            Dim str_amount As String = "0.00"
            If gvr.Cells(7).Text <> "" AndAlso IsNumeric(gvr.Cells(7).Text) Then
                str_amount = gvr.Cells(7).Text
            End If
            Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
            If Not txtAmountToPay Is Nothing Then
                If txtAmountToPay.Text.Trim = "" OrElse Not IsNumeric(txtAmountToPay.Text) Then
                    txtAmountToPay.Text = "0.00"
                End If
            End If
            str_amount = txtAmountToPay.Text
            Session("FeeCollection").Rows(iEdit)("Amount") = str_amount
            Session("FeeCollection").Rows(iEdit)("bChargeandPost") = ChkPost.Checked
        Next
    End Sub

    Sub SetChqamount(ByVal lastColumn As Integer)
        'Dim TotalAmount As Double = CDbl(txtTotal.Text)
        'Dim ChqAmount As Double = getTotal(False)
        'Dim ChkAmt As Boolean = False
        'Dim x As Integer = 0
        'For Each gvr As GridViewRow In gvChequeDetails.Rows
        '    Dim ddlEmirate As DropDownList = TryCast(gvr.FindControl("ddlEmirate"), DropDownList)
        '    ddlEmirate.SelectedIndex = -1
        '    ddlEmirate.Items.FindByValue(ddlEmirateMain.SelectedItem.Value).Selected = True
        '    Dim txtChequeTotal As TextBox = CType(gvr.FindControl("txtChequeTotal"), TextBox)
        '    If Not txtChequeTotal Is Nothing Then

        '        ChqAmount = ChqAmount + CDbl(txtChequeTotal.Text)
        '        If ChkAmt = False Then
        '            If ChqAmount = TotalAmount Then
        '                ChkAmt = True
        '            End If
        '            If ChqAmount > TotalAmount Then
        '                ChkAmt = True
        '                'Dim txtCheqTotal As TextBox = CType(GridView1.Rows(x - 1).FindControl("txtChequeTotal"), TextBox)
        '                txtChequeTotal.Text = Format((TotalAmount - (ChqAmount - CDbl(txtChequeTotal.Text))), "0.00")
        '                'txtChequeTotal.Text = "0.00"
        '            End If
        '        Else
        '            txtChequeTotal.Text = "0.00"
        '        End If
        '    End If
        '    x = x + 1
        'Next
        'If ChkAmt = True Then
        '    lastColumn = x
        'End If
        'If TotalAmount > ChqAmount Then
        '    Dim txtChqTotal As TextBox = CType(gvChequeDetails.Rows(lastColumn - 1).FindControl("txtChequeTotal"), TextBox)
        '    txtChqTotal.Text = Format(((TotalAmount - ChqAmount) + CDbl(txtChqTotal.Text)), "0.00")
        'End If
        getTotal(True)
    End Sub
End Class
