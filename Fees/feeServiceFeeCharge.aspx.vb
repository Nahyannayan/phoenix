Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeServiceFeeCharge
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "F300130" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            InitialiseComponents()
            If Request.QueryString("viewid") <> "" Then
                btnAdddetails.Visible = False
                setViewData()
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            Else
                ResetViewData()
            End If
            ViewState("iID") = 0
            Session("FeeSetup") = FeeMaster.CreateDataTableServiceCharges()
            gvAttendance.DataBind()
        End If
    End Sub

    Sub InitialiseComponents()
        txtFrom.Text = FeeCommon.GetACDStartDate(Session("Current_ACD_ID"))
        ddService.DataBind()
        SetTransport()
    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            Dim str_Sql As String
            str_Sql = "SELECT SBR_ID, SBR_BSU_ID, SBR_RATE, SBR_DTFROM, SBR_DTTO, " _
                & " SBR_PNT_ID, SBR_SVB_ID FROM SERVICES_BSU_RATE_S  " _
                & " WHERE (SBR_BSU_ID = '" & Session("sBSUID") & "') and SBR_ID='" & p_Modifyid & "'"
            Dim ds As New DataSet
            ViewState("canedit") = "no"
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddService.DataBind()
                ddPickPoint.DataBind()
                ddService.Items.FindByValue(ds.Tables(0).Rows(0)("SBR_SVB_ID").ToString).Selected = True
                txtFrom.Text = Format(CDate(ds.Tables(0).Rows(0)("SBR_DTFROM")), "dd/MMM/yyyy")
                txtAmount.Text = Format(ds.Tables(0).Rows(0)("SBR_RATE"), "0.00")
                ddPickPoint.Items.FindByValue(ds.Tables(0).Rows(0)("SBR_PNT_ID").ToString).Selected = True
            Else
                ViewState("canedit") = "no"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub setViewData()
        txtFrom.Attributes.Add("readonly", "readonly")
        btnAdddetails.Enabled = False
        tr_Deatailhead.Visible = False
        tr_Deatails.Visible = False
        imgFrom.Enabled = False
    End Sub

    Sub ResetViewData()
        txtFrom.Attributes.Remove("readonly")
        btnAdddetails.Enabled = True
        tr_Deatailhead.Visible = True
        tr_Deatails.Visible = True
        imgFrom.Enabled = True
        btnAdddetails.Visible = True
    End Sub

    Sub clear_All()
        txtFrom.Text = ""
        txtAmount.Text = ""
        Session("FeeSetup").Rows.Clear()
    End Sub

    Sub gridbind()
        gvAttendance.DataSource = Session("FeeSetup")
        gvAttendance.DataBind()
    End Sub

    Sub gridbind_add()
        Try
            Dim dtFrom As Date
            dtFrom = CDate(txtFrom.Text)
            If IsNumeric(txtAmount.Text) = False Then
                'lblError.Text = "Invalid Amount"
                usrMessageBar.ShowNotification("Invalid Amount", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            Dim dr As DataRow
            dr = Session("FeeSetup").NewRow
            dr("Id") = ViewState("iID")
            'Id, SVB_ID, PNT_ID,SVB_DESCR,PNT_DESCR,FDate,TDate,Amount,Status
            ViewState("iID") = ViewState("iID") + 1
            dr("SVB_ID") = ddService.SelectedItem.Value
            dr("SVB_DESCR") = ddService.SelectedItem.Text
            If ddService.SelectedItem.Value = "1" Then
                dr("PNT_ID") = ddPickPoint.SelectedItem.Value
                dr("PNT_DESCR") = ddPickPoint.SelectedItem.Text
            Else
                dr("PNT_ID") = 0
                dr("PNT_DESCR") = ""
            End If

            dr("FDate") = dtFrom
            dr("TDate") = ""
            dr("Grade") = ddGrade.SelectedItem.Value
            dr("Amount") = txtAmount.Text
            dr("Status") = "Normal"

            'dr("Permonth") = chkPermonth.Checked
            'dr("Join") = chkJoinpro.Checked
            'dr("Discontinue") = chkDiscontinuePro.Checked
            'dr("Active") = chkActive.Checked

            Session("FeeSetup").Rows.Add(dr)
            txtAmount.Text = ""
            gvAttendance.DataSource = Session("FeeSetup")
            gvAttendance.DataBind()
        Catch ex As Exception
            'lblError.Text = "Check Date"
            usrMessageBar.ShowNotification("Check Date", UserControls_usrMessageBar.WarningType.Warning)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Session("FeeSetup").Rows.Count = 0 And ViewState("datamode") <> "edit" Then
            'lblError.Text = "There is no data to save"
            usrMessageBar.ShowNotification("There is no data to save", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            Dim fep_id, str_grd_id As String
            If ViewState("datamode") = "edit" Then
                Dim dtFrom As Date
                '"Id", "Remarks" "EMP_ID",  "EMP_NAME" "Leavetype", "FDate", "TDate",   "Status"  
                If IsDate(txtFrom.Text) And IsNumeric(txtAmount.Text) Then
                    dtFrom = CDate(txtFrom.Text)
                    If ddService.SelectedItem.Value = "1" Then
                        str_grd_id = ""
                    Else
                        str_grd_id = ddGrade.SelectedItem.Value
                    End If
                    fep_id = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    retval = FeeMaster.F_SaveSERVICES_BSU_RATE_S(fep_id, Session("sbsuid"), ddService.SelectedItem.Value, _
                    ddPickPoint.SelectedItem.Value, txtAmount.Text, txtFrom.Text, "", str_grd_id, stTrans)
                Else
                    stTrans.Rollback()
                    'lblError.Text = "Cannot save. Check Date/Amount"
                    usrMessageBar.ShowNotification("Cannot save. Check Date/Amount", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            Else
                'Id, SVB_ID, PNT_ID,SVB_DESCR,PNT_DESCR,FDate,TDate,Amount,Status
                For i As Integer = 0 To Session("FeeSetup").Rows.Count - 1
                    If ddService.SelectedItem.Value = "1" Then
                        str_grd_id = ""
                    Else
                        str_grd_id = Session("FeeSetup").Rows(i)("grade")
                    End If
                    retval = FeeMaster.F_SaveSERVICES_BSU_RATE_S(0, Session("sbsuid"), Session("FeeSetup").Rows(i)("SVB_ID"), _
                    Session("FeeSetup").Rows(i)("PNT_ID"), Session("FeeSetup").Rows(i)("Amount"), _
                    Session("FeeSetup").Rows(i)("FDate"), Session("FeeSetup").Rows(i)("TDate"), str_grd_id, stTrans)

                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                    Session("FeeSetup").Rows(i)("SVB_ID"), _
                    "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                Next
            End If
            If retval = "0" Then
                stTrans.Commit()
                'lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                clear_All()
                gridbind()
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdddetails.Click
        gridbind_add()
    End Sub

    Protected Sub gvAttendance_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvAttendance.RowDeleting
        Try
            Dim row As GridViewRow = gvAttendance.Rows(e.RowIndex)
            Dim idRow As New Label
            idRow = TryCast(row.FindControl("lblId"), Label)
            Dim iRemove As Integer = 0
            Dim iIndex As Integer = 0
            iIndex = CInt(idRow.Text)
            'loop through the data table row  for the selected rowindex item in the grid view
            For iRemove = 0 To Session("FeeSetup").Rows.Count - 1
                If iIndex = Session("FeeSetup").Rows(iRemove)(0) Then
                    Session("FeeSetup").Rows(iRemove).Delete()
                    Exit For
                End If
            Next
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            'lblError.Text = "Record cannot be Deleted"
            usrMessageBar.ShowNotification("Record cannot be Deleted", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        setEditdata()
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Sub setEditdata()
        txtFrom.Attributes.Remove("readonly")
        imgFrom.Enabled = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            setViewData()
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
    End Sub

    Protected Sub ddService_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddService.SelectedIndexChanged
        SetTransport()
    End Sub

    Sub SetTransport()
        Dim sb_sql As New StringBuilder()
        sb_sql.Append("SELECT CHG.SCH_DESCR AS CHG_DESCR  FROM FEES.FEES_M AS FEE " _
        & " INNER JOIN FEES.SCHEDULE_M AS CHG ON FEE.FEE_ChargedFor_SCH_ID = CHG.SCH_ID " _
        & " WHERE(FEE.FEE_SVC_ID = '" & ddService.SelectedItem.Value & "') ")
        lblChargeSchedule.Text = UtilityObj.GetDataFromSQL(sb_sql.ToString, WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        If lblChargeSchedule.Text <> "--" Then
            lblChargeSchedule.Text = "(" & lblChargeSchedule.Text & ")"
        End If
        If ddService.SelectedItem.Value = "1" Then
            tr_Transport.Visible = True
        Else
            tr_Transport.Visible = False
        End If

    End Sub

    Protected Sub gvAttendance_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvAttendance.RowEditing

    End Sub

End Class
