﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.Net
Imports UtilityObj
Imports System.Xml
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Partial Class Fees_FeePaymentPlannerView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Dim sbReceiptNo As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
            Try
                ViewState("MainMnu_code") = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+")))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+")))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "F300271" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    'Select Case ViewState("MainMnu_code").ToString
                    '    Case OASISConstants.MNU_FEE_COLLECTION

                    '        ViewState("trantype") = "A"
                    'End Select
                    If rad1.Checked Then
                        id_princ_cmnts.Visible = True
                    End If

                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub
    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            Dim lblItemColID As Label, lblStatus As Label
            lblItemColID = TryCast(e.Row.FindControl("lblId"), Label)
            lblStatus = TryCast(e.Row.FindControl("lblStatus"), Label)


            Dim viewmode As String = ""
            Dim cmdCol As Integer = gvDetails.Columns.Count - 1
            Dim hlview As New LinkButton
            hlview = TryCast(e.Row.FindControl("hlView"), LinkButton)
            If hlview IsNot Nothing And lblItemColID IsNot Nothing Then
                Dim objFPP As New FeePaymentPlanner
                objFPP.FPP_ID = lblItemColID.Text
                If objFPP.bPLAN_APPROVED_OR_REJECTED() Then
                    viewmode = Encr_decrData.Encrypt("")
                    hlview.Attributes.Add("onclick", "ApprovalForm('" & Encr_decrData.Encrypt("view") & "','" & Encr_decrData.Encrypt(lblItemColID.Text) & "','F300271')")
                    hlview.Text = "View"
                Else
                    hlview.Attributes.Add("onclick", "ApprovalForm('" & Encr_decrData.Encrypt("approve") & "','" & Encr_decrData.Encrypt(lblItemColID.Text) & "','F300271')")
                    hlview.Text = "View"
                End If

            End If


            'Dim hlEmail As New LinkButton
            'hlEmail = TryCast(e.Row.FindControl("hlEmail"), LinkButton)
            'If hlEmail IsNot Nothing And lblItemColID IsNot Nothing Then
            '    Dim objFPP As New FeePaymentPlanner
            '    objFPP.FPP_ID = lblItemColID.Text
            '    If objFPP.bPLAN_APPROVED_OR_REJECTED() Then
            '        hlEmail.Attributes.Add("onclick", "ApprovalForm('" & Encr_decrData.Encrypt("view") & "','" & Encr_decrData.Encrypt(lblItemColID.Text) & "','F300271')")
            '        hlEmail.Text = "Email"
            '    End If

            'End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub lbEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblId As Label = sender.Parent.parent.findcontrol("lblId")
            Dim objFPP As New FeePaymentPlanner
            objFPP.FPP_ID = lblId.Text
            If objFPP.bPLAN_APPROVED_OR_REJECTED() Then
                Get_Email_Preview2(Session("sBsuid"), lblId.Text)
                pnlemailhistory.Visible = True
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            usrMessageBar2.ShowNotification("Error in fetching email ", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Sub Get_Email_Preview2(ByVal BSU_ID As String, ByVal FPP_ID As String)
        Try

            Dim appr As Boolean = rad4.Checked
            Dim rej As Boolean = rad3.Checked
            Dim EML_TYPE As String = ""
            If appr Then
                EML_TYPE = "PAYPLAN_A"
            ElseIf rej Then
                EML_TYPE = "PAYPLAN_R"
            End If
            Dim emailtext As String = FeePaymentPlanner.GET_EMAIL_PREVIEW(FPP_ID, BSU_ID, EML_TYPE)
            lbl_emailHistory.Text = emailtext
            pnlemailhistory.Visible = True

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub btn_EHclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_EHclose.Click
        pnlemailhistory.Visible = False
    End Sub
    Protected Sub btn_EHclose2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_EHclose2.Click
        pnlemailhistory.Visible = False
    End Sub
    Protected Sub gridbind()


        Dim str_Filter As String = "", FCL_bDeleted As String = "", FCL_ACD_ID As Integer = 0

        Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7, lstrCondn8 As String

        Dim larrSearchOpr() As String
        Dim lstrOpr As String
        Dim txtSearch As New TextBox
        Dim CurBsUnit As String = Session("sBsuid")
        lstrCondn1 = ""
        lstrCondn2 = ""
        lstrCondn3 = ""
        lstrCondn4 = ""
        lstrCondn5 = ""
        lstrCondn6 = ""
        lstrCondn7 = ""
        lstrCondn8 = ""
        str_Filter = ""

        If gvDetails.Rows.Count > 0 Then
            ' --- Initialize The Variables
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            '   --- FILTER CONDITIONS ---
            '   -- 1   txtReceiptno FCL_RECNO
            larrSearchOpr = h_selected_menu_1.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtId")
            lstrCondn1 = Trim(Mainclass.cleanString(txtSearch.Text))
            If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ID", lstrCondn1)

            '   -- 2  txtDate
            larrSearchOpr = h_selected_menu_2.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtStudentID")
            lstrCondn2 = Trim(Mainclass.cleanString(txtSearch.Text))
            If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "StudentID", lstrCondn2)

            '   -- 3  txtGrade
            larrSearchOpr = h_selected_menu_3.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtStudentName")
            lstrCondn3 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "StudentName", lstrCondn3)

            '   -- 4   txtStuno
            larrSearchOpr = h_selected_menu_4.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtDate")
            lstrCondn4 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "DocumentDate", lstrCondn4)

            '   -- 5  txtStuname
            larrSearchOpr = h_selected_menu_5.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtGrade")
            lstrCondn5 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "Grade", lstrCondn5)

            '   -- 6  txtAmount
            larrSearchOpr = h_selected_menu_6.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtParentName")
            lstrCondn6 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ParentName", lstrCondn6)

            '   -- 8  txtGrade
            larrSearchOpr = h_selected_menu_8.Value.Split("__")
            lstrOpr = larrSearchOpr(0)
            txtSearch = gvDetails.HeaderRow.FindControl("txtStatus")
            lstrCondn8 = Mainclass.cleanString(txtSearch.Text)
            If (lstrCondn8 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "Status", lstrCondn8)

            '   -- 7  txtDesc
            'larrSearchOpr = h_selected_menu_7.Value.Split("__")
            'lstrOpr = larrSearchOpr(0)
            'txtSearch = gvDetails.HeaderRow.FindControl("txtAmount")
            'lstrCondn7 = Mainclass.cleanString(txtSearch.Text)
            'If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "Amount", lstrCondn7)
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        'Dim StrSQL As String = "select BSAH_ID AS ID, BSAH_ID  ,BSAH_NO ,BSAH_SOURCE , BSAH_DATE ,BSAH_CUST_NO ,BSAH_CUST_NAME  ,(BSAH_GRADE+'-'+BSAH_SECTION) AS BSAH_GRADE ,BSAH_NET_AMOUNT, ISNULL(BSAH_INVOICE_NO,'') BSAH_INVOICE_NO from [dbo].[BOOK_SALE_H] where BSAH_TYPE='S' AND [BSAH_BSU_ID]='" & Session("sBsuid") & "'" & str_Filter & " ORDER BY  BSAH_ID DESC"
        Dim StrSQL As String = ""
        StrSQL = "SELECT ID,StudentID,StudentName,Grade,ParentName,DocumentDate,Amount, [Status] FROM "
        StrSQL &= "(SELECT FPP_ID AS ID,FPP_STU_ID AS STU_ID,STU_NO AS StudentID,StudentName,Grade,ParentName,FPP_DATE AS DocumentDate," & _
            "FPD_AMOUNT AS Amount, [PLAN_STATUS_DESCRIPTION] As [Status] FROM vwFEE_PAYMENTPLAN_H WHERE FPP_BSU_ID='" & Session("sBSUID") & "' "
        If rad1.Checked Then
            StrSQL &= " AND ISNULL(FPP_APPROVED,'')  IN ('FA','FR') AND ISNULL(FPP_LEVEL2_APPROVED,'') ='' "
        ElseIf rad2.Checked Then
            StrSQL &= " AND ISNULL(FPP_APPROVED,'')='FA' AND ISNULL(FPP_LEVEL2_APPROVED,'')='PO' "
        ElseIf rad3.Checked Then
            StrSQL &= " AND ISNULL(FPP_APPROVED,'')='FA' AND ISNULL(FPP_LEVEL2_APPROVED,'')='PR' "
        ElseIf rad4.Checked Then
            StrSQL &= " AND ISNULL(FPP_APPROVED,'') IN ('FA','FR') AND ISNULL(FPP_LEVEL2_APPROVED,'')='PA' "
        End If
        StrSQL &= ") a where 1=1 ORDER BY DocumentDate DESC,ID DESC"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)
        gvDetails.DataSource = ds
        gvDetails.DataBind()
        Dim appr As Boolean = rad4.Checked
        Dim rej As Boolean = rad3.Checked
        If (appr Or rej) AndAlso FeePaymentPlanner.GetROLEAccess(Session("sUsr_name"), Session("sBsuid")) = 1 Then
            gvDetails.Columns(10).Visible = True
        Else
            gvDetails.Columns(10).Visible = False
        End If


        If gvDetails.Rows.Count > 0 Then

            txtSearch = gvDetails.HeaderRow.FindControl("txtId")
            txtSearch.Text = lstrCondn1

            txtSearch = gvDetails.HeaderRow.FindControl("txtStudentID")
            txtSearch.Text = lstrCondn2

            txtSearch = gvDetails.HeaderRow.FindControl("txtStudentName")
            txtSearch.Text = lstrCondn3

            txtSearch = gvDetails.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn4

            txtSearch = gvDetails.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn5

            txtSearch = gvDetails.HeaderRow.FindControl("txtParentName")
            txtSearch.Text = lstrCondn6

            txtSearch = gvDetails.HeaderRow.FindControl("txtStatus")
            txtSearch.Text = lstrCondn8
        End If
    End Sub
    Protected Sub rad_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rad1.CheckedChanged, rad2.CheckedChanged, rad3.CheckedChanged, rad4.CheckedChanged
        Try
            If rad1.Checked Then
                btnApprove.Visible = True
                btnReject.Visible = True
                btnRevert.Visible = True
                id_princ_cmnts.Visible = True
            Else
                btnApprove.Visible = False
                btnReject.Visible = False
                btnRevert.Visible = False
                id_princ_cmnts.Visible = False
            End If
            gridbind()
        Catch ex As Exception
            usrMessageBar2.ShowNotification("Error in loading data to grid ", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Sub BindGrid(ByVal dt As DataTable)
        gvDetails.DataSource = dt
        gvDetails.DataBind()
    End Sub

    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        gvDetails.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub ckbheader_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkhead As CheckBox
        chkhead = TryCast(gvDetails.HeaderRow.FindControl("ckbheader"), CheckBox)
        For Each gvrow As GridViewRow In gvDetails.Rows
            Dim chk As CheckBox
            chk = TryCast(gvrow.FindControl("ckbSelect"), CheckBox)
            If chkhead.Checked = True Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
        Next
    End Sub

    Protected Sub ckbSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim chk As CheckBox
        chk = TryCast(sender.FindControl("ckbSelect"), CheckBox)
        Dim chkhead As CheckBox
        chkhead = TryCast(gvDetails.HeaderRow.FindControl("ckbheader"), CheckBox)
        If chk.Checked = False Then
            chkhead.Checked = False
        End If

    End Sub

    Protected Sub btnRevert_Click(sender As Object, e As EventArgs) Handles btnRevert.Click
        btnApprove.Enabled = False
        btnReject.Enabled = False
        btnRevert.Enabled = False

        Dim objFPP As New FeePaymentPlanner
        Dim RetFlag As Boolean = False
        Dim final_string As String = ""
        Dim sqlConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        sqlConn.Open()
        Dim sqlTrans As SqlTransaction = sqlConn.BeginTransaction

        For Each gvrow As GridViewRow In gvDetails.Rows
            Dim chk As CheckBox
            chk = TryCast(gvrow.FindControl("ckbSelect"), CheckBox)
            Dim lbl_ID As Label
            lbl_ID = TryCast(gvrow.FindControl("lblId"), Label)
            If chk.Checked Then
                'If final_string = "" Then
                '    final_string = final_string + lbl_ID.Text
                'Else
                '    final_string = final_string + "|" + lbl_ID.Text
                'End If
                If lbl_ID.Text <> "" Then
                    objFPP.FPP_ID = lbl_ID.Text
                    objFPP.User = Session("sUsr_name")
                    Try
                        objFPP.Comments = txt_princ_comments.Text
                        RetFlag = objFPP.APPROVE_FEE_PAYMENT_PLAN_H(3, sqlConn, sqlTrans)
                    Catch ex As Exception
                        sqlTrans.Rollback()
                        'Me.lblError.Text = "unable to approve fee payment plan"
                        'usrMessageBar2.ShowMessage("unable to revert fee payment plan", True)
                        usrMessageBar2.ShowNotification("unable to revert fee payment plan ", UserControls_usrMessageBar.WarningType.Danger)
                        btnApprove.Enabled = True
                        btnReject.Enabled = True
                        btnRevert.Enabled = True
                        If sqlConn.State = ConnectionState.Open Then
                            sqlConn.Close()
                        End If
                        Exit Sub
                    End Try
                End If

            End If
        Next
        Try
            If RetFlag Then
                sqlTrans.Commit()
                'clearAll()
                ' Me.lblError.Text = "Fee Payment plan has been approved successfully"
                'usrMessageBar2.ShowMessage("Fee Payment plan has been revert successfully", False)
                usrMessageBar2.ShowNotification("Fee Payment plan has been revert successfully", UserControls_usrMessageBar.WarningType.Success)
            Else
                sqlTrans.Rollback()
                'Me.lblError.Text = "unable to approve fee payment plan"
                'usrMessageBar2.ShowMessage("unable to revert fee payment plan", True)
                usrMessageBar2.ShowNotification("unable to revert fee payment plan", UserControls_usrMessageBar.WarningType.Danger)
            End If
            If sqlConn.State = ConnectionState.Open Then
                sqlConn.Close()
            End If
        Catch Ex As Exception
        End Try
        btnApprove.Enabled = True
        btnReject.Enabled = True
        btnRevert.Enabled = True
        txt_princ_comments.Text = ""
        id_princ_cmnts.Visible = True
        gridbind()
    End Sub
    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        btnApprove.Enabled = False
        btnReject.Enabled = False
        btnRevert.Enabled = False

        Dim objFPP As New FeePaymentPlanner
        Dim RetFlag As Boolean = False
        Dim final_string As String = ""
        Dim sqlConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        sqlConn.Open()
        Dim sqlTrans As SqlTransaction = sqlConn.BeginTransaction

        For Each gvrow As GridViewRow In gvDetails.Rows
            Dim chk As CheckBox
            chk = TryCast(gvrow.FindControl("ckbSelect"), CheckBox)
            Dim lbl_ID As Label
            lbl_ID = TryCast(gvrow.FindControl("lblId"), Label)
            If chk.Checked Then
                'If final_string = "" Then
                '    final_string = final_string + lbl_ID.Text
                'Else
                '    final_string = final_string + "|" + lbl_ID.Text
                'End If
                If lbl_ID.Text <> "" Then
                    objFPP.FPP_ID = lbl_ID.Text
                    objFPP.User = Session("sUsr_name")
                    objFPP.BSU_ID = Session("sBsuid")
                    Try
                        objFPP.Comments = txt_princ_comments.Text
                        RetFlag = objFPP.APPROVE_FEE_PAYMENT_PLAN_H(1, sqlConn, sqlTrans)
                    Catch ex As Exception
                        sqlTrans.Rollback()
                        'Me.lblError.Text = "unable to approve fee payment plan"
                        'usrMessageBar2.ShowMessage("unable to approve fee payment plan", True)
                        usrMessageBar2.ShowNotification("unable to approve fee payment plan", UserControls_usrMessageBar.WarningType.Danger)
                        btnApprove.Enabled = True
                        btnReject.Enabled = True
                        btnRevert.Enabled = True
                        If sqlConn.State = ConnectionState.Open Then
                            sqlConn.Close()
                        End If
                        Exit Sub
                    End Try
                End If
            End If
        Next
        Try
            If RetFlag Then
                sqlTrans.Commit()
                ' Me.lblError.Text = "Fee Payment plan has been approved successfully"
                'usrMessageBar2.ShowMessage("Fee Payment plan has been approved successfully", False)
                usrMessageBar2.ShowNotification("Fee Payment plan has been approved successfully", UserControls_usrMessageBar.WarningType.Success)
            Else
                sqlTrans.Rollback()
                'Me.lblError.Text = "unable to approve fee payment plan"
                'usrMessageBar2.ShowMessage("unable to approve fee payment plan", True)
                usrMessageBar2.ShowNotification("unable to approve fee payment plan", UserControls_usrMessageBar.WarningType.Danger)
            End If
            If sqlConn.State = ConnectionState.Open Then
                sqlConn.Close()
            End If
        Catch Ex As Exception
        End Try
        btnApprove.Enabled = True
        btnReject.Enabled = True
        btnRevert.Enabled = True
        txt_princ_comments.Text = ""
        id_princ_cmnts.Visible = True
        gridbind()
    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        btnApprove.Enabled = False
        btnReject.Enabled = False
        btnRevert.Enabled = False

        Dim objFPP As New FeePaymentPlanner
        Dim RetFlag As Boolean = False
        Dim final_string As String = ""
        Dim sqlConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        sqlConn.Open()
        Dim sqlTrans As SqlTransaction = sqlConn.BeginTransaction

        For Each gvrow As GridViewRow In gvDetails.Rows
            Dim chk As CheckBox
            chk = TryCast(gvrow.FindControl("ckbSelect"), CheckBox)
            Dim lbl_ID As Label
            lbl_ID = TryCast(gvrow.FindControl("lblId"), Label)
            If chk.Checked Then
                'If final_string = "" Then
                '    final_string = final_string + lbl_ID.Text
                'Else
                '    final_string = final_string + "|" + lbl_ID.Text
                'End If
                If lbl_ID.Text <> "" Then
                    objFPP.FPP_ID = lbl_ID.Text
                    objFPP.User = Session("sUsr_name")
                    objFPP.BSU_ID = Session("sBsuid")
                    Try
                        objFPP.Comments = txt_princ_comments.Text
                        RetFlag = objFPP.APPROVE_FEE_PAYMENT_PLAN_H(2, sqlConn, sqlTrans)
                    Catch ex As Exception
                        sqlTrans.Rollback()
                        'Me.lblError.Text = "unable to reject fee payment plan"
                        'usrMessageBar2.ShowMessage("unable to reject fee payment plan", True)
                        usrMessageBar2.ShowNotification("unable to reject fee payment plan", UserControls_usrMessageBar.WarningType.Danger)
                        btnApprove.Enabled = True
                        btnReject.Enabled = True
                        btnRevert.Enabled = True
                        If sqlConn.State = ConnectionState.Open Then
                            sqlConn.Close()
                        End If
                        Exit Sub
                    End Try

                End If
            End If
        Next

        Try
            If RetFlag Then
                sqlTrans.Commit()
                'Me.lblError.Text = "Fee Payment plan has been rejected"
                'usrMessageBar2.ShowMessage("Fee Payment plan has been rejected", False)
                usrMessageBar2.ShowNotification("Fee Payment plan has been rejected", UserControls_usrMessageBar.WarningType.Success)
            Else
                sqlTrans.Rollback()
                ' Me.lblError.Text = "unable to reject fee payment plan"
                'usrMessageBar2.ShowMessage("unable to reject fee payment plan", True)
                usrMessageBar2.ShowNotification("unable to reject fee payment plan", UserControls_usrMessageBar.WarningType.Danger)
            End If
            If sqlConn.State = ConnectionState.Open Then
                sqlConn.Close()
            End If
        Catch Ex As Exception
        End Try
        btnApprove.Enabled = True
        btnReject.Enabled = True
        btnRevert.Enabled = True
        txt_princ_comments.Text = ""
        id_princ_cmnts.Visible = True
        gridbind()
    End Sub

End Class
