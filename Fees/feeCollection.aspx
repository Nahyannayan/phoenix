<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="feeCollection.aspx.vb" Inherits="Fees_feeCollection" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Src="~/UserControls/usrRibbonControlPanel.ascx" TagPrefix="uc3" TagName="usrRibbonControlPanel" %>
<%@ Register Src="~/UserControls/uscStudentPicker.ascx" TagPrefix="uc1" TagName="uscStudentPicker" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">


    <style type="text/css">
        /*bootstrap class overrides starts here*/
        .card-body {
            padding: 0.25rem !important;
        }

        table th, table td {
            padding: 0.1rem !important;
        }

        .bg-dark div input.button, .bg-dark table td input.button, .bg-dark table td div input.button {
            padding: 6px !important;
            margin: 2px !important;
        }

        table td select, div select, table td input[type=text], table td input[type=date], table td input[type=password], table td textarea, table td select:disabled, div select:disabled, table td input[type=text]:disabled, table td textarea:disabled, table td input[type=password]:disabled {
            padding: 6px !important;
        }
        /*bootstrap class overrides ends here*/
        /*table class overrides starts here*/

        .table-fee {
            width: 100%;
            max-width: 100%;
            margin-bottom: 1rem;
        }

            .table-fee th,
            .table-fee td {
                padding: 0.07rem;
                vertical-align: middle;
                border-color: #8dc24c;
            }

        .bg-light .table-fee th,
        .bg-light .table-fee td {
            border-color: rgba(0,169,228,1) !important;
        }

        .table-fee-striped tbody tr:nth-of-type(odd) {
            background-color: rgba(0, 0, 0, 0.05);
        }
        /*table class overrides ends here*/


        .RadInput textarea.riEmpty, html body input.RadInput_Empty {
            font-style: normal;
        }

        html body .riSingle .riTextBox[type="text"] {
            width: 120% !important;
        }
    </style>
    <script>
        function OnClientLoaded(sender, args) {
            var slider = sender;
            if ($(window).width() < 979) {
                slider.set_orientation(1);
                slider.set_width(50);
                slider.set_height(600);
            }
        }
    </script>

    <script type="text/javascript" lang="javascript">
        Sys.Application.add_load(
        function CheckForPrint() {
            ShowHideBankAccount();
            var hf_print = document.getElementById('<%= h_print.ClientID %>');
            var frmReceipt = "";
            if ($("#<%=hfTaxable.ClientID%>").val() == "1")
                frmReceipt = "FeeReceipt_TAX.aspx";
            else
                frmReceipt = "FeeReceipt.aspx";

            if (hf_print.value.indexOf('?') == 0) {
                if (isIE()) {
                    window.showModalDialog(frmReceipt + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                } else {
                    Popup(frmReceipt + document.getElementById('<%= h_print.ClientID %>').value);
                }
            }
            else if (hf_print.value == 'audit')
                if (isIE()) {
                    window.showModalDialog('../Reports/ASPX Report/rptReportViewerModelView.aspx?paging=1', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                } else {
                    Popup('../Reports/ASPX Report/rptReportViewerModelView.aspx?paging=1');
                }
            hf_print.value = '';
            if ($("#<%=hfReportError.ClientID%>").val() == "1") {
                $("#<%=hfReportError.ClientID%>").val('0');
                if (isIE()) {
                    window.showModalDialog('../Common/EmailErrorpage.aspx?id=0', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                } else {
                    Popup('../Common/EmailErrorpage.aspx?id=0');
                }
            }
            var hid = document.getElementById('<%=h_hideBank.ClientID %>').value
            if (hid == 2) Hidebank('tr_chq2', 2);
            if (hid == 3) {
                Hidebank('tr_chq2', 2);
                Hidebank('tr_chq3', 3);
            }
            var hidcr = document.getElementById('<%=h_CrCard.ClientID %>').value;
            if (hidcr == 2)
                HideCard('trCrCard2', 2);
            if (hidcr == 3) {
                //                HideCard('trCrCard2', 2);
                HideCard('trCrCard3', 3);
            }<%----%>
        }

      );
        function isIE() {
            ua = navigator.userAgent;
            /* MSIE used to detect old browsers and Trident used to newer ones*/
            var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

            return is_ie;
        }
        function ShowHideBankAccount() {
            if (document.getElementById('<%= txtBank1.ClientID%>').value != 'Bank Transfer') {
                $("#tdhBA").hide();
                $("#tdBA1").hide();
            }
            else {
                $("#tdhBA").show();
                $("#tdBA1").show();
            }

            if (document.getElementById('<%= txtBank2.ClientID%>').value != 'Bank Transfer')
                $("#tdBA2").hide();
            else
                $("#tdBA2").show();
            if (document.getElementById('<%= txtBank3.ClientID%>').value != 'Bank Transfer')
                $("#tdBA3").hide();
            else
                $("#tdBA3").show();
        }
        function HideAll() {
            document.getElementById('<%= pnlCheque.ClientID %>').style.display = 'none';
            document.getElementById('<%= pnlCreditCard.ClientID %>').style.display = 'none';
            document.getElementById('<%= pnlVoucher.ClientID%>').style.display = 'none';
            return false;
        }

        function getRoundOff() {
            var roundOff = '<%=Session("BSU_ROUNDOFF") %>';
            var amt;
            amt = parseFloat(roundOff)
            if (isNaN(amt))
                amt = 2;
            return amt;
        }
        function CheckNumber(ctrl) {
            var temp;
            temp = parseFloat(ctrl.value);
            if (isNaN(temp))
                ctrl.value = 0;
            else
                ctrl.value = temp.toFixed(2);
        }
        function CheckAmount(e) {
            var amt;
            amt = parseFloat(e.value)
            if (isNaN(amt))
                amt = 0;
            e.value = amt.toFixed(getRoundOff());
            return true;
        }
        function UpdateSum(ddl, txt, txtrate, txtSum) {
            var txtCCTotal, txtChequeTotal1, txtChequeTotal2, txtChequeTotal3, txtCashTotal,
               TotalPayingNow, txtReturn, TotalReceived, Balance, BankTotal, txtCrCTotal1,
               txtCrCTotal2, txtCrCTotal3, txtTAX, VoucherAmount1, VoucherTotal, RewardsTotal;

            txtCCTotal = parseFloat(document.getElementById('<%=txtCCTotal.ClientID %>').value);
            if (isNaN(txtCCTotal))
                txtCCTotal = 0;

            txtChequeTotal1 = parseFloat(document.getElementById('<%=txtChequeTotal1.ClientID %>').value);
            if (isNaN(txtChequeTotal1))
                txtChequeTotal1 = 0;
            txtChequeTotal2 = parseFloat(document.getElementById('<%=txtChequeTotal2.ClientID %>').value);
            if (isNaN(txtChequeTotal2))
                txtChequeTotal2 = 0;
            txtChequeTotal3 = parseFloat(document.getElementById('<%=txtChequeTotal3.ClientID %>').value);
            if (isNaN(txtChequeTotal3))
                txtChequeTotal3 = 0;
            BankTotal = txtChequeTotal1 + txtChequeTotal2 + txtChequeTotal3;
            txtCrCTotal1 = parseFloat(document.getElementById('<%=txtCrCTotal1.ClientID %>').value);
            if (isNaN(txtCrCTotal1))
                txtCrCTotal1 = 0;
            txtCrCTotal2 = parseFloat(document.getElementById('<%=txtCrCTotal2.ClientID %>').value);
            if (isNaN(txtCrCTotal2))
                txtCrCTotal2 = 0;
            txtCrCTotal3 = parseFloat(document.getElementById('<%=txtCrCTotal3.ClientID %>').value);
            if (isNaN(txtCrCTotal3))
                txtCrCTotal3 = 0;
            txtCCTotal = txtCrCTotal1 + txtCrCTotal2 + txtCrCTotal3;
            if (isNaN(txtCCTotal))
                txtCCTotal = 0;
            txtCashTotal = parseFloat(document.getElementById('<%=txtCashTotal.ClientID %>').value);
            if (isNaN(txtCashTotal))
                txtCashTotal = 0;

            TotalPayingNow = parseFloat($("#<%=txtTotal.ClientID%>").text().replace(/,/g, ''));
            if (isNaN(TotalPayingNow))
                TotalPayingNow = 0;
            txtTAX = $("#<%=txtTax.ClientID%>").html();
            if (isNaN(txtTAX) || txtTAX == null)
                txtTAX = 0;
            VoucherAmount1 = parseFloat(document.getElementById('<%=txtVoucherAmount1.ClientID%>').value);
            if (isNaN(VoucherAmount1))
                VoucherAmount1 = 0;
            VoucherTotal = VoucherAmount1;

            RewardsTotal = parseFloat(document.getElementById('<%=txtRewardsAmount.ClientID%>').value);
            if (isNaN(RewardsTotal))
                RewardsTotal = 0;

            TotalReceived = txtCCTotal + txtChequeTotal1 + txtChequeTotal2 + txtChequeTotal3 + txtCashTotal + VoucherTotal + RewardsTotal;

            Balance = 0;
            if (txtCashTotal > 0 || VoucherTotal > 0 || RewardsTotal > 0) {
                if (TotalReceived > (TotalPayingNow)) {
                    Balance = TotalReceived - TotalPayingNow;
                    if (Balance > txtCashTotal)
                        Balance = 0;
                }
                else {
                    Balance = (parseFloat(TotalPayingNow)) - parseFloat(TotalReceived);
                }
            }

            $("#<%=txtReceivedTotal.ClientID%>").text(TotalReceived.toFixed(getRoundOff()));
            document.getElementById('<%=txtCCTotal.ClientID %>').value = txtCCTotal.toFixed(getRoundOff());
            document.getElementById('<%=txtBankTotal.ClientID %>').value = BankTotal.toFixed(getRoundOff());
            document.getElementById('<%=txtCashTotal.ClientID %>').value = txtCashTotal.toFixed(getRoundOff());
            document.getElementById('<%=txtChequeTotal1.ClientID %>').value = txtChequeTotal1.toFixed(getRoundOff());
            document.getElementById('<%=txtChequeTotal2.ClientID %>').value = txtChequeTotal2.toFixed(getRoundOff());
            document.getElementById('<%=txtChequeTotal3.ClientID %>').value = txtChequeTotal3.toFixed(getRoundOff());
            document.getElementById('<%=txtBalance.ClientID %>').innerHTML = Balance.toFixed(getRoundOff());
            document.getElementById('<%=txtVoucherTotal.ClientID%>').value = VoucherTotal.toFixed(getRoundOff());
            document.getElementById('<%=txtRewardsAmount.ClientID%>').value = RewardsTotal.toFixed(getRoundOff());
            if (txtCCTotal >= 0) {
                CheckRate(ddl, txt, txtrate, txtSum);
                txtCrCardCharges = parseFloat(document.getElementById('<%=txtCrCardCharges.ClientID %>').value);
                if (isNaN(txtCrCardCharges))
                    txtCrCardCharges = 0.00;
                TotalReceived = txtCCTotal + txtChequeTotal1 + txtChequeTotal2 + txtChequeTotal3 + txtCashTotal + txtCrCardCharges + VoucherTotal + RewardsTotal;
                BankTotal = txtChequeTotal1 + txtChequeTotal2 + txtChequeTotal3;

                if (txtCashTotal > 0) {
                    Balance = 0;
                    if (TotalReceived > (TotalPayingNow)) {
                        Balance = TotalReceived - TotalPayingNow - txtCrCardCharges;
                        if (Balance > txtCashTotal)
                            Balance = 0;
                    }
                    else {
                        Balance = (parseFloat(TotalPayingNow)) - parseFloat(TotalReceived);
                    }
                }

                $("#<%=txtReceivedTotal.ClientID%>").text(TotalReceived.toFixed(getRoundOff()));
            }
            document.getElementById('<%=txtBalance.ClientID %>').innerHTML = Balance.toFixed(getRoundOff());
        }
        function CheckRate(ddl, txt, txtrate, txtSum) {

            var temp = new Array();
            if (txt != "0") {
                var txtCr = parseFloat(document.getElementById(txt).value);
                if (isNaN(txtCr))
                    txtCr = 0.00;
            }
            else
                txtCr = 0.00;
            var CrCardCharge = parseFloat(document.getElementById('<%=txtCrCardCharges.ClientID %>').value);
            if (isNaN(CrCardCharge))
                CrCardCharge = 0.00;
            temp = document.getElementById('<%=hfCobrand.ClientID %>').value.split("|");
            var ddlist = document.getElementById(ddl);

            if (ddl != "0" && ddlist != null) {
                for (var i = 0; i < temp.length; i++) {
                    if (temp[i].split("=")[0] == ddlist.options[ddlist.selectedIndex].value) {
                        document.getElementById(txtrate).value = Math.ceil((txtCr * temp[i].split("=")[1] / 100.00).toFixed(getRoundOff()));
                        document.getElementById(txtSum).value = txtCr + Math.ceil((txtCr * (temp[i].split("=")[1] / 100.00)));
                    }
                }
            }

            var txtCr1 = parseFloat(document.getElementById('<%=txtCrCTotal1.ClientID %>').value);
            if (isNaN(txtCr1))
                txtCr1 = 0.00;
            var txtCr2 = parseFloat(document.getElementById('<%=txtCrCTotal2.ClientID %>').value);
            if (isNaN(txtCr2))
                txtCr2 = 0.00;
            var txtCr3 = parseFloat(document.getElementById('<%=txtCrCTotal3.ClientID %>').value);
            if (isNaN(txtCr3))
                txtCr3 = 0.00;
            var CardP1 = parseFloat(document.getElementById('<%=txtCrCardP1.ClientID %>').value);
            if (isNaN(CardP1))
                CardP1 = 0.00;
            var CardP2 = parseFloat(document.getElementById('<%=txtCrCardP2.ClientID %>').value);
            if (isNaN(CardP2))
                CardP2 = 0.00;
            var CardP3 = parseFloat(document.getElementById('<%=txtCrCardP3.ClientID %>').value);
            if (isNaN(CardP3))
                CardP3 = 0.00;

            CrCardCharge = (CardP1) + (CardP2) + (CardP3);
            document.getElementById('<%=txtCrCardCharge1.ClientID %>').value = (txtCr1 + CardP1).toFixed(getRoundOff());
            document.getElementById('<%=txtCrCardCharge2.ClientID %>').value = (txtCr2 + CardP2).toFixed(getRoundOff());
            document.getElementById('<%=txtCrCardCharge3.ClientID %>').value = (txtCr3 + CardP3).toFixed(getRoundOff());
            document.getElementById('<%=txtCrCardCharges.ClientID %>').value = CrCardCharge.toFixed(getRoundOff());

            //}return true;

        }

        function Hidebank(id, index) {
            $("#" + id).show(250);
            $('#<%=h_hideBank.ClientID %>').val(index);
            return false;
        }
        function HideCard(id, index) {
            var CoBrand = document.getElementById('<%=chkCoBrand.ClientID %>').checked;
            if (CoBrand == false) {
                document.getElementById(id).style.display = 'table-row';
                document.getElementById('<%=h_CrCard.ClientID %>').value = index;
                return false;
            }
            else if (CoBrand == true)
            { alert("Multiple credit cards are not allowed when 'co-brand payment' is selected"); }

        }
        function HideCardRow(id, index) {
            $("#" + id).hide(250);
            $('#<%=h_CrCard.ClientID%>').val(index);
            return false;
        }
        function HideBankRow(id, index) {
            $("#" + id).hide(250);
            $('#<%=h_hideBank.ClientID%>').val(index);
            return false;
        }

        function ClearCheque() {
            document.getElementById('<%= txtChqno1.ClientID %>').readOnly = '';
            document.getElementById('<%= h_Chequeid.ClientID %>').value = '';
            document.getElementById('<%= txtChqno1.ClientID %>').value = '';
            return false;
        }
    </script>

    <script>
        function PayTransportFee(url, mode) {
            Popup(url);

        }


        function getBankOrEmirate(mode, ctrl) {
            var url;
            document.getElementById('<%= hf_mode.ClientID%>').value = mode
            document.getElementById('<%= hf_ctrl.ClientID%>').value = ctrl
            if (mode == 1)
                url = "../common/PopupFormIDhidden.aspx?iD=BANK&MULTISELECT=FALSE";
            else
                url = "../common/PopupFormIDhidden.aspx?iD=EMIRATE&MULTISELECT=FALSE";
            var oWnd = radopen(url, "pop_getBankOrEmirate");

        }

        function OnClientClose1(oWnd, args) {

            var mode = document.getElementById('<%= hf_mode.ClientID%>').value
            var ctrl = document.getElementById('<%= hf_ctrl.ClientID%>').value
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameCode.split('||');

                if (mode == 1) {
                    if (ctrl == 1) {
                        document.getElementById('<%= h_Bank1.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%= txtBank1.ClientID %>').value = NameandCode[1];
                        if (NameandCode[2] == "BANKTRF") {
                            $("#tdhBA").show(250);
                            $("#tdBA1").show(250);
                        }
                        else {
                            $("#tdhBA").hide(250);
                            $("#tdBA1").hide(250);
                        }
                    }
                    else if (ctrl == 2) {
                        document.getElementById('<%= h_Bank2.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%= txtBank2.ClientID %>').value = NameandCode[1];
                        if (NameandCode[2] == "BANKTRF")
                            $("#tdBA2").show(250);
                        else
                            $("#tdBA2").hide(250);
                    }
                    else if (ctrl == 3) {
                        document.getElementById('<%= h_Bank3.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%= txtBank3.ClientID %>').value = NameandCode[1];
                        if (NameandCode[2] == "BANKTRF")
                            $("#tdBA3").show(250);
                        else
                            $("#tdBA3").hide(250);
                    }
        }
    }
}


function getBank(mode) {
    var url;
    document.getElementById('<%= hf_mode.ClientID%>').value = mode

    var txtBankCode;
    if (mode == 1)
        txtBankCode = document.getElementById('<%=txtBankAct1.ClientID%>').value;
    var oWnd = radopen("../ACCOUNTS/PopUp.aspx?ShowType=BANK&codeorname=" + txtBankCode, "pop_getBank");


}

function OnClientClose2(oWnd, args) {

    var mode = document.getElementById('<%= hf_mode.ClientID%>').value

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg) {

        NameandCode = arg.NameandCode.split('||');

        if (mode == 1) {
            document.getElementById('<%=hfBankAct1.ClientID%>').value = NameandCode[0];
            document.getElementById('<%=txtBankAct1.ClientID%>').value = NameandCode[0];
        }
        else if (mode == 2) {
            document.getElementById('<%=hfBankAct2.ClientID%>').value = NameandCode[0];
            document.getElementById('<%=txtBankAct2.ClientID%>').value = NameandCode[0];
        }
        else if (mode == 3) {
            document.getElementById('<%=hfBankAct3.ClientID%>').value = NameandCode[0];
            document.getElementById('<%=txtBankAct3.ClientID%>').value = NameandCode[0];
        }
}
}

function getCheque() {
    var oWnd = radopen("../common/PopupFormThreeIDHidden.aspx?iD=FEE_CHEQUE&MULTISELECT=FALSE", "pop_getCheque");


}

function OnClientClose3(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg) {

        NameandCode = arg.NameandCode.split('||');

        document.getElementById('<%= txtChqno1.ClientID %>').readOnly = true;
        document.getElementById('<%= h_Chequeid.ClientID %>').value = NameandCode[0];
        document.getElementById('<%= txtChqno1.ClientID %>').value = NameandCode[1];
    }
}

function Showdata(mode) {
    var url;
    var NameandCode;
    var STU_ID = document.getElementById('<%= h_Student_no.ClientID %>').value;

    var ACD_ID = '<%= uscStudentPicker.STU_ACD_ID %>';
    if (mode == 1) {
        url = "../common/PaymentHistory.aspx?id=RECEIPTHISTORY&stuid=" + STU_ID;
    }
    else if (mode == 2)
        url = "../common/PopupShowData.aspx?id=CHARGEHISTORY&stuid=" + STU_ID;
    else if (mode == 3)
        url = "../common/PopupShowData.aspx?id=SIBBLINGS&stuid=" + STU_ID;
    else if (mode == 4)
        url = "../common/PopupShowData.aspx?id=CONCESSION_FEES&stuid=" + STU_ID + "&acd=" + ACD_ID;
    else if (mode == 5)
        url = "../common/PopupShowData.aspx?id=ADJUSTMENT&stuid=" + STU_ID;
    else if (mode == 6)
        url = "../common/PopupShowData.aspx?id=CHQBOUNCE&stuid=" + STU_ID;
    else if (mode == 7)
        url = "../common/PopupShowData.aspx?id=ADVPAY&stuid=" + STU_ID;
    else if (mode == 8)
        url = "../common/PopupShowData.aspx?id=AGING&stuid=" + STU_ID;
    else if (mode == 9)
        url = "FeeCalculateFee.aspx";
    Popup(url);
}



function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>
    <style>
        @media (min-width:980px) {
            .PopUpMessageStyle {
                /*background-image: linear-gradient(to left, #eff1fd 0%, #dbe0ff 100%) !important;*/
                left: 15px !important;
                border: 1px solid #d5dbff !important;
                border-radius: 4px !important;
                padding: 5px !important;
                text-align: -webkit-center !important;
                box-shadow: 4px 6px 13px #cdc;
                background-color: #f1f1f1;
            }
        }

        @media (max-width:980px) {
            .PopUpMessageStyle {
                /*background-image: linear-gradient(to left, #eff1fd 0%, #dbe0ff 100%) !important;*/
                left: 15px !important;
                border: 1px solid #d5dbff !important;
                padding: 5px !important;
                text-align: -webkit-center !important;
                top: 100px !important;
                box-shadow: 4px 6px 13px #cdc;
                background-color: #f1f1f1;
            }
        }


        /*.RadSlider_Metro .rslHorizontal .rslItem {
            background-image:none!important;            
        }*/

        .RadSlider .rslHorizontal .rslItem, .RadSlider .rslHorizontal .rslLargeTick, .RadSlider .rslHorizontal .rslSmallTick {
            background-position: center !important;
        }

        .RadSlider_Silk {
            height: 80px !important;
        }

        .imgStyle {
            float: left;
        }
    </style>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getBankOrEmirate" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getBank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getCheque" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Fee Collection
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table border="0" cellpadding="0" onclick="return true;" id="tblErrortop" cellspacing="0"
                    align="center" width="100%" style="display: none;">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left"></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left"></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left"><span class="field-label"></span>
                        </td>
                        <td align="left"></td>
                        <td align="left"></td>
                        <td align="right"></td>
                    </tr>
                    <tr style="display: none;">
                        <td align="left"><span class="field-label">Academic Year</span>
                        </td>
                        <td align="left"></td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Businessunit</span></td>
                        <td>
                            <telerik:RadComboBox ID="ddlBusinessunit" runat="server" Filter="Contains" Width="100%" RenderMode="Lightweight" AutoPostBack="true" ZIndex="2000" ToolTip="Type in unit name or short code" DataTextField="BSU_NAME" DataValueField="BSU_ID"></telerik:RadComboBox>
                        </td>
                        <td align="left"><span class="field-label">Date</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True" TabIndex="2" Style="min-width: 25% !important;"></asp:TextBox><asp:ImageButton
                                ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server"
                                PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender><asp:HiddenField ID="hfTaxable" runat="server" Value="0" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Select Student</span></td>
                        <td colspan="3">
                            <uc1:uscStudentPicker runat="server" ID="uscStudentPicker" />
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td align="left"><span class="field-label"></span>
                        </td>
                        <td align="left" colspan="3">
                            <table width="100%">
                                <tr>
                                    <td width="70%">
                                        <asp:ImageButton ID="lnkDiscount" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Misc/discount.gif"
                                            TabIndex="8" OnClientClick="return false;"></asp:ImageButton>
                                        <asp:ImageButton ID="ImgMore" runat="server" ImageUrl="~/Images/Misc/moreinfo_blue.jpg"
                                            ImageAlign="AbsMiddle" OnClientClick="return false;" TabIndex="8" />
                                        <asp:ImageButton ID="lnkTransportfee" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Misc/bus.gif"
                                            TabIndex="8"></asp:ImageButton>
                                        <asp:ImageButton ID="lnkCatering" runat="server" Visible="false" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Images/Misc/Catering.gif" TabIndex="8"></asp:ImageButton>
                                        <asp:HiddenField ID="h_Student_no" runat="server" />
                                        <ajaxToolkit:PopupControlExtender ID="pceMoreDetails" runat="server" PopupControlID="pnelmoreinfo"
                                            Position="Bottom" TargetControlID="ImgMore">
                                        </ajaxToolkit:PopupControlExtender>
                                        <ajaxToolkit:PopupControlExtender ID="pceDiscount" runat="server" PopupControlID="pnlDiscount"
                                            Position="Left" TargetControlID="lnkDiscount">
                                        </ajaxToolkit:PopupControlExtender>
                                    </td>
                                    <td width="30%" style="vertical-align: top"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"></td>
                        <td align="left">
                            <asp:CheckBox ID="chkCoBrand" runat="server" OnCheckedChanged="chkCoBrand_CheckedChanged"
                                Text="Co-brand Payment" AutoPostBack="True" CssClass="field-label"></asp:CheckBox>
                            <asp:CheckBox ID="chkChargableCollection" runat="server" AutoPostBack="True" OnCheckedChanged="chkCoBrand_CheckedChanged"
                                Text="Other Credit Card Payment" CssClass="field-label" />
                            <asp:CheckBox ID="chkDisableDiscount" runat="server" AutoPostBack="True" OnCheckedChanged="chkDisableDiscount_CheckedChanged"
                                Text="Discount Not Applicable" CssClass="field-label" />
                            <asp:CheckBox ID="nxtAcaFee" runat="server" AutoPostBack="True"
                                Text="Next Academic Year Fee" CssClass="field-label" Visible="false" />
                            <asp:Label ID="lblNacdFeetext" runat="server" Visible="false" CssClass="text-danger"></asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:Panel ID="pnlCurrency" runat="server" Width="100%">
                                <table>
                                    <tr>
                                        <td><span class="field-label">Currency</span>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddCurrency" runat="server" AutoPostBack="True" Style="width: 100px;">
                                            </asp:DropDownList>
                                        </td>
                                        <td><span class="field-label">Exchange Rate</span>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblExgRate" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                        
                    </tr>
                    <tr class="title-bg-lite">
                        <td colspan="4" align="left">Fee Details
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="4">
                            <span style="font-family: Verdana; text-decoration: underline; font-size: 8pt"></span>
                            <asp:GridView ID="gvFeeCollection" runat="server" Width="100%" AutoGenerateColumns="False"
                                EmptyDataText="No Details Added" CssClass="table table-bordered table-row" EnableModelValidation="True" DataKeyNames="FEE_ID,FEE_bBlockPaynow,PAY_TYPE_ID,FCS_PAY_TYPE">
                                <Columns>
                                    <asp:TemplateField HeaderText="feeidandamount" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFSR_FEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("CLOSING") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="C.P">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ChkPost" runat="server" Enabled="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fee">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFeeDescr" runat="server" Text='<%# Bind("FEE_DESCR") %>'></asp:Label>
                                            <asp:LinkButton runat="server" ID="btnSlider" Visible="false" OnClientClick="return false;" ToolTip="Click here to select advance payment" Text="Click here to select advance payment" />
                                            <asp:Panel ID="RadSliders_Wrapper" runat="server" CssClass="sliderView horizontalSliderView">
                                                <div id="divslider" runat="server" class="PopUpMessageStyle">
                                                    <asp:Label ID="lblmessage" runat="server" CssClass="text-danger" />
                                                    <telerik:RadSlider RenderMode="Lightweight" ID="RadSlider_Ticks" runat="server" Height="80px" Width="700px" ItemType="Item"
                                                        AnimationDuration="400" ThumbsInteractionMode="Free" OnValueChanged="RadSlider_Ticks_ValueChanged" AutoPostBack="true" Skin="Silk" TrackPosition="Center" OnClientLoaded="OnClientLoaded">
                                                        <ItemBinding TextField="F_MONTH" ValueField="ID" />
                                                    </telerik:RadSlider>
                                                    <asp:HiddenField ID="hid_sli_stu_id" runat="server" />
                                                    <div class="text-right">
                                                        <asp:Label ID="lblonetamt" runat="server" Text="Total amount : " ForeColor="#333333" Font-Bold="true" />
                                                        <asp:Label ID="txtNetAmt" runat="server" ForeColor="#333333" Style="padding-right: 25px;" />
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <ajaxToolkit:PopupControlExtender ID="pcet_Lib_Iss" runat="server"
                                                OffsetX="-750" PopupControlID="divslider"
                                                Position="Bottom" TargetControlID="btnSlider">
                                            </ajaxToolkit:PopupControlExtender>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotals" runat="server" Text='TOTAL'></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="OPENING" HeaderText="Opening" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MONTHLY_AMOUNT" HeaderText="Charge" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CONC_AMOUNT" HeaderText="Concession" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ADJUSTMENT" HeaderText="Adjustment" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PAID_AMOUNT" HeaderText="Paid" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CLOSING" HeaderText="Net" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Discount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiscount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Discount")) %>'
                                                AutoPostBack="True" onFocus="this.select();" Style="text-align: right"
                                                OnTextChanged="txtAmountToPay_TextChanged"></asp:Label>
                                            <asp:HiddenField ID="h_OtherCharge" runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Discount">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDiscount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Discount")) %>'
                                                AutoPostBack="True" onFocus="this.select();" onblur="return CheckAmount(this);"
                                                Style="text-align: right" OnTextChanged="txtAmountToPay_TextChanged" TabIndex="25"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tax">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtTAX" runat="server" Text='<%# Bind("TAX") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTAX" runat="server" Text='<%# Bind("TAX", "{0:0.00}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paying Now">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmountToPay" runat="server"
                                                AutoPostBack="True" onFocus="this.select();" onblur="return CheckAmount(this);" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'
                                                Style="text-align: right;" OnTextChanged="txtAmountToPay_TextChanged" TabIndex="25"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmountToPay" runat="server" FilterType="Numbers, Custom"
                                                ValidChars="." TargetControlID="txtAmountToPay" />
                                            <asp:LinkButton ID="lbCancel" Visible="false" runat="server" OnClick="lbCancel_Click">Cancel</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paying Now(FC)" Visible="false">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmountToPayFC" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'
                                                AutoPostBack="True" onFocus="this.select();" onblur="return CheckAmount(this);"
                                                Style="text-align: right" OnTextChanged="txtAmountToPayFC_TextChanged" TabIndex="26"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmountToPayFC" runat="server" FilterType="Numbers, Custom"
                                                ValidChars="." TargetControlID="txtAmountToPayFC" />
                                            <asp:LinkButton ID="lbCancelFC" Visible="false" runat="server" OnClick="lbCancel_Click">Cancel</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="right">
                            <asp:LinkButton ID="lbtnActivityPayment" runat="server" Style="font-weight: bold;">Activities Payment</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="lbtnAddMore" runat="server" Style="font-weight: bold;">Add More Fee Head(s)</asp:LinkButton>
                        </td>
                    </tr>
                    <tr id="tr_AddFee1" runat="server" class="gridheader_pop" visible="false">
                        <td colspan="4" align="left">Add More Fee Head(s)</td>
                    </tr>
                    <tr id="tr_AddFee" runat="server" visible="false">
                        <td align="center" colspan="4">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <tr class="title-bg-lite">
                                    <td align="center">Fee Type</td>
                                    <td align="center">Charge &amp; Post</td>
                                    <td align="center">Amount Paid</td>
                                    <td align="center">Amount</td>
                                    <td align="center">Tax Amount</td>
                                    <td align="center">Net Amount</td>
                                    <td align="center"></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:DropDownList ID="ddFeetype" runat="server" TabIndex="9" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="center">
                                        <asp:CheckBox ID="ChkChgPost" runat="server" Text="" />
                                    </td>
                                    <td align="center">
                                        <asp:RadioButtonList ID="rblVAT" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                            <asp:ListItem Value="1">Incl.VAT</asp:ListItem>
                                            <asp:ListItem Value="2" Selected="True">Excl.VAT</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td align="center">
                                        <asp:TextBox ID="txtAmount" runat="server" Style="text-align: right" onblur="CheckNumber(this)" AutoCompleteType="Disabled" AutoPostBack="True" autocomplete="off"
                                            TabIndex="45" Text="0.00"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmount" runat="server" FilterType="Numbers, Custom"
                                            ValidChars="." TargetControlID="txtAmount" />
                                    </td>
                                    <td align="center">
                                        <asp:TextBox ID="txtVATAmount" Enabled="false" Style="text-align: right" runat="server" onblur="CheckNumber(this)" AutoCompleteType="Disabled" autocomplete="off"
                                            TabIndex="46" Text="0.00"></asp:TextBox>
                                    </td>
                                    <td align="center">
                                        <asp:TextBox ID="txtAmountAdd" runat="server" Enabled="false" Style="text-align: right" AutoCompleteType="Disabled" autocomplete="off" onblur="CheckNumber(this)"
                                            TabIndex="10" Text="0.00"></asp:TextBox>
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btnAddDetails" runat="server" CssClass="button" Text="Add" CausesValidation="False"
                                            TabIndex="12" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trActPay" runat="server" visible="false">
                        <td colspan="4">
                            <table style="width: 100%;">
                                <tr>
                                    <th class="title-bg-lite" colspan="7">Activities Payment</th>
                                </tr>
                                <tr>
                                    <td style="width: 10%;"><span class="field-label">Activity :</span></td>
                                    <td style="width: 25%;">
                                        <asp:DropDownList ID="ddlActivity" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                                    <td style="width: 10%;"><span class="field-label">Fee Type :</span></td>
                                    <td style="width: 25%;">
                                        <asp:Label ID="lblActFeeType" runat="server"></asp:Label></td>
                                    <td style="width: 10%;"><span class="field-label">Amount :</span></td>
                                    <td>
                                        <asp:Label ID="lblActAmount" runat="server" Text="0.00"></asp:Label></td>
                                    <td>
                                        <asp:Button ID="btnAddActivity" runat="server" OnClick="btnAddActivity_Click" Text="Add" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table style="border: 1px solid #1B80B6; padding: 3px; border-collapse: collapse; width: 80%; display: none;"
                                cellspacing="0">
                                <tr>
                                    <td align="center">OutStanding</td>
                                    <td align="center">Discount</td>
                                    <td align="center">Tax</td>
                                    <td align="center">Paying Now</td>
                                    <td align="center">Due</td>
                                </tr>
                                <tr>
                                    <td class="matters">
                                        <asp:Label ID="lbl_OS" Text="0.00" runat="server" Style="text-align: right"></asp:Label></td>
                                    <td class="matters">
                                        <asp:Label ID="lbl_Discnt" Text="0.00" runat="server" Style="text-align: right"></asp:Label></td>
                                    <td class="matters">
                                        <asp:Label ID="lbl_TAX" Text="0.00" runat="server" Style="text-align: right"></asp:Label></td>
                                    <td class="matters">
                                        <asp:Label ID="lbl_PayingNow" Text="0.00" runat="server" Style="text-align: right"></asp:Label></td>
                                    <td class="matters">
                                        <asp:Label ID="lbl_Due" Text="0.00" runat="server" Style="text-align: right"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="right">
                            <table border="0" style="padding: 3px; border-collapse: collapse;">
                                <tr>
                                    <td align="left"><span class="field-label">Total Outstanding</span>
                                    </td>
                                    <td align="left">
                                        <%--<asp:TextBox ID="txtReceivedTotal" Style="text-align: right" runat="server" TabIndex="205" Text="0.00"></asp:TextBox>--%>
                                        <asp:Label ID="txtOutstanding" runat="server" Style="text-align: right" Text="0.00"></asp:Label>
                                        <asp:Label ID="lblOutstandingFC" runat="server" Style="text-align: right"></asp:Label>
                                    </td>
                                    <td align="right"><span id="spanTax" runat="server">(Tax : 
                            <asp:Label ID="txtTax" runat="server" Style="text-align: right" class="field-label"></asp:Label>)&nbsp;&nbsp; </span>&nbsp;
                            (<asp:Label ID="lblTotalDiscount" runat="server" Style="text-align: right" onmouseover="this.style.cursor='hand'"
                                onmouseout="this.style.cursor='default'"></asp:Label>)
                            <ajaxToolkit:HoverMenuExtender ID="pceDiscountDetail" runat="server" PopupControlID="pnlDiscntDtl"
                                PopupPosition="Top" TargetControlID="lblTotalDiscount" OffsetX="-300">
                            </ajaxToolkit:HoverMenuExtender>
                                    </td>
                                    <td align="left"><span class="field-label">Total Paying Now</span></td>

                                    <td align="right" class="mattersBigger">

                                        <asp:Label ID="txtTotal" runat="server" Style="text-align: right" Text="0.00"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="title-bg-lite">
                        <td align="left" colspan="4">Payment Details
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table style="width: 100%;">
                                <tr class="title-bg-lite">
                                    <td align="center" class="matters">Cash</td>
                                    <td align="center" class="matters">Credit Card</td>
                                    <td align="center" class="matters">Cheque</td>
                                    <td align="center" class="matters">Voucher</td>
                                    <td align="center" class="matters">GEMS Points</td>
                                    <td align="center" class="matters">Total</td>
                                </tr>
                                <tr>
                                    <td>
                                        <!--CASH -->
                                        <asp:TextBox ID="txtCashTotal" runat="server" AutoCompleteType="Disabled" autocomplete="off" onFocus="this.select();"
                                            onBlur="UpdateSum('0','0','0','0');" Style="text-align: right" TabIndex="45" Text="0.00"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtCashTotal" runat="server" FilterType="Numbers, Custom"
                                            ValidChars="." TargetControlID="txtCashTotal" />
                                    </td>
                                    <td>
                                        <!--CARD -->
                                        <table style="width: 100%;">
                                            <tr>
                                                <td align="left">
                                                    <asp:TextBox ID="txtCCTotal" Style="text-align: right" runat="server" onFocus="this.select(); " autocomplete="off"
                                                        onBlur="UpdateSum('0','0','0','0');" TabIndex="50" AutoPostBack="True" Text="0.00"></asp:TextBox>
                                                    <ajaxToolkit:PopupControlExtender ID="pceCreditCard" runat="server" PopupControlID="pnlCreditCard"
                                                        Position="Bottom" TargetControlID="txtCCTotal">
                                                    </ajaxToolkit:PopupControlExtender>
                                                </td>
                                                <td align="left"><span class="field-label">Txn.Charge</span>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtCrCardCharges" runat="server" Text="0.00" Style="text-align: right; width: 60px;"
                                                        TabIndex="200" Enabled="False"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <!--CHEQUE -->
                                        <asp:TextBox ID="txtBankTotal" Style="text-align: right" runat="server" onFocus="this.select();"
                                            onBlur="UpdateSum('0','0','0','0');" AutoCompleteType="Disabled" autocomplete="off" TabIndex="165" Text="0.00"></asp:TextBox>
                                        <ajaxToolkit:PopupControlExtender ID="pceCheque" runat="server" PopupControlID="pnlCheque"
                                            Position="Bottom" TargetControlID="txtBankTotal">
                                        </ajaxToolkit:PopupControlExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtVoucherTotal" Style="text-align: right" onFocus="this.select();" onBlur="UpdateSum('0','0','0','0');" AutoCompleteType="Disabled" autocomplete="off" TabIndex="166" runat="server" Text="0.00"></asp:TextBox>
                                        <ajaxToolkit:PopupControlExtender ID="pceVoucher" runat="server" PopupControlID="pnlVoucher"
                                            Position="Bottom" TargetControlID="txtVoucherTotal">
                                        </ajaxToolkit:PopupControlExtender>
                                    </td>
                                    <td style="display: flex;">
                                        <asp:TextBox ID="txtRewardsAmount" Style="text-align: right;" runat="server" onFocus="this.select();" AutoCompleteType="Disabled" autocomplete="off">0.00</asp:TextBox>
                                        <span style="display: inline-block !important; margin-left: 2px;">
                                            <asp:Button ID="btnRedeem" runat="server" CssClass="button" Text="..." /></span>
                                    </td>
                                    <td align="center">
                                        <asp:Label ID="txtReceivedTotal" runat="server" Text="0.00"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" rowspan="2"><span class="field-label">Narration</span>
                        </td>
                        <td align="left" rowspan="2">
                            <asp:TextBox ID="txtRemarks" runat="server" Height="32px" Style="width: 100%;" TextMode="MultiLine"
                                TabIndex="160"></asp:TextBox>
                        </td>
                        <td align="left" rowspan="1"></td>
                        <td align="right">
                            <span class="field-label">Due</span>&nbsp;
                            <asp:Label ID="txtDue" runat="server" Text="0.00"></asp:Label>
                            <asp:Label ID="lblDueFC" runat="server"></asp:Label>
                            <br />
                            <span class="field-label">Balance</span>&nbsp;&nbsp;<asp:Label ID="txtBalance" runat="server" Text="0.00"></asp:Label>
                            <asp:LinkButton ID="lbViewTotal" runat="server" Visible="False">Preview Total</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                            <asp:CheckBox
                                ID="chkRemember" runat="server" Text="Remember Payment Info" CssClass="field-label" />
                            <asp:Button ID="btnNext" runat="server" CausesValidation="False" CssClass="button"
                                TabIndex="160" Text="Next Token" />
                            <asp:Button ID="btnRewards" runat="server" Text="test rewards" Visible="false" />
                        </td>
                        <td align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" Visible="false" /><asp:Button
                                ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="165" Visible="false" /><asp:Button
                                    ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" Visible="false" />
                            <asp:Label ID="lblToken" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
                <uc3:usrRibbonControlPanel runat="server" ID="usrRibbonControlPanel" />
                <asp:Panel ID="pnlCreditCard" Style="display: none" runat="server" CssClass="panel-cover" Width="50%">
                    <table border="1" align="center"
                        bgcolor="#ffffff">
                        <tr class="title-bg">
                            <th align="left" colspan="1" class="matters_Colln">Credit Card Type
                            </th>
                            <th align="left" colspan="1" class="matters_Colln">Authorisation code
                            </th>
                            <th align="left" colspan="1" class="matters_Colln">Amount
                            </th>
                            <th align="left" colspan="1" class="matters_Colln">Charge
                            </th>
                            <th align="left" colspan="1" class="matters_Colln">Total
                            </th>
                            <th align="center" colspan="1">
                                <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/close_red.gif"
                                    OnClientClick="return HideAll();" TabIndex="54" Style="text-align: right" />
                            </th>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:DropDownList ID="ddCreditcard" runat="server" DataTextField="CRI_DESCR"
                                    DataValueField="CRR_ID" TabIndex="55" Style="width: 80% !important">
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="font-size: 12pt">
                                <asp:TextBox ID="txtCreditno" runat="server" AutoCompleteType="Disabled" autocomplete="off"
                                    TabIndex="56" Style="width: 80% !important"></asp:TextBox>
                            </td>
                            <td align="left" style="font-size: 12pt">
                                <asp:TextBox ID="txtCrCTotal1" runat="server" AutoCompleteType="Disabled" autocomplete="off" onfocus="this.select();"
                                    Style="text-align: right; width: 80% !important" TabIndex="57"></asp:TextBox>
                            </td>
                            <td align="left" style="font-size: 12pt">
                                <asp:TextBox ID="txtCrCardP1" runat="server" Text="0.00" AutoCompleteType="Disabled" autocomplete="off"
                                    onfocus="this.select();" Style="text-align: right; width: 80% !important"
                                    TabIndex="58" Enabled="False"></asp:TextBox>
                            </td>
                            <td align="left" style="font-size: 12pt">
                                <asp:TextBox ID="txtCrCardCharge1" runat="server" Text="0.00" AutoCompleteType="Disabled" autocomplete="off"
                                    onfocus="this.select();" Style="text-align: right; width: 80% !important"
                                    TabIndex="59" Enabled="False"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:LinkButton ID="LinkButton11" runat="server" TabIndex="60" OnClientClick="HideCard('trCrCard2',2);return false;">Add</asp:LinkButton>
                                <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/Images/close_red.gif"
                                    OnClientClick="return HideAll();" TabIndex="61" />
                                <asp:HiddenField ID="hfCard1" runat="server" />
                            </td>
                        </tr>
                        <tr id="trCrCard2" style="display: none">
                            <td align="left" colspan="1">
                                <asp:DropDownList ID="ddCreditcard2" runat="server"
                                    DataTextField="CRI_DESCR" DataValueField="CRR_ID" SkinID="DropDownListNormal"
                                    TabIndex="62" Style="width: 80% !important">
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="font-size: 12pt">
                                <asp:TextBox ID="txtCreditno2" runat="server" AutoCompleteType="Disabled" autocomplete="off"
                                    TabIndex="63" Style="width: 80% !important"></asp:TextBox>
                            </td>
                            <td align="left" style="font-size: 12pt">
                                <asp:TextBox ID="txtCrCTotal2" runat="server" AutoCompleteType="Disabled" autocomplete="off" onfocus="this.select();"
                                    Style="text-align: right; width: 80% !important" TabIndex="64"></asp:TextBox>
                            </td>
                            <td align="left" style="font-size: 12pt">
                                <asp:TextBox ID="txtCrCardP2" runat="server" Text="0.00" AutoCompleteType="Disabled" autocomplete="off"
                                    onfocus="this.select();" Style="text-align: right; width: 80% !important"
                                    TabIndex="65" Enabled="False"></asp:TextBox>
                            </td>
                            <td align="left" style="font-size: 12pt">
                                <asp:TextBox ID="txtCrCardCharge2" runat="server" Text="0.00" AutoCompleteType="Disabled" autocomplete="off"
                                    onfocus="this.select();" Style="text-align: right; width: 80% !important"
                                    TabIndex="66" Enabled="False"></asp:TextBox>
                            </td>
                            <td align="left" style="font-size: 12pt; width: auto">
                                <asp:LinkButton ID="LinkButton12" runat="server" TabIndex="67" OnClientClick="HideCard('trCrCard3',3);return false;">Add</asp:LinkButton>
                                <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/close_red.gif"
                                    OnClientClick="HideCardRow('trCrCard2',1);return false;" TabIndex="68" />
                                <asp:HiddenField ID="hfCard2" runat="server" />
                            </td>
                        </tr>
                        <tr id="trCrCard3" style="display: none">
                            <td align="left">
                                <asp:DropDownList ID="ddCreditcard3" runat="server"
                                    DataTextField="CRI_DESCR" DataValueField="CRR_ID" SkinID="DropDownListNormal"
                                    TabIndex="69" Width="80%">
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="font-size: 12pt">
                                <asp:TextBox ID="txtCreditno3" runat="server" AutoCompleteType="Disabled" autocomplete="off"
                                    TabIndex="70" Width="80%"></asp:TextBox>
                            </td>
                            <td align="left" style="font-size: 12pt">
                                <asp:TextBox ID="txtCrCTotal3" runat="server" AutoCompleteType="Disabled" autocomplete="off" onfocus="this.select();"
                                    Style="text-align: right; width: 80% !important" TabIndex="71"></asp:TextBox>
                            </td>
                            <td align="left" style="font-size: 12pt">
                                <asp:TextBox ID="txtCrCardP3" runat="server" Text="0.00" AutoCompleteType="Disabled" autocomplete="off"
                                    onfocus="this.select();" Style="text-align: right; width: 80% !important"
                                    TabIndex="72" Enabled="False"></asp:TextBox>
                            </td>
                            <td align="left" style="font-size: 12pt">
                                <asp:TextBox ID="txtCrCardCharge3" runat="server" Text="0.00" AutoCompleteType="Disabled" autocomplete="off"
                                    onfocus="this.select();" Style="text-align: right; width: 80% !important"
                                    TabIndex="73" Enabled="False"></asp:TextBox>
                            </td>
                            <td align="left" style="font-size: 12pt">
                                <asp:ImageButton ID="ImageButton6" runat="server" TabIndex="74" ImageUrl="~/Images/close_red.gif"
                                    OnClientClick="HideCardRow('trCrCard3',1);return false;" />
                                <asp:HiddenField ID="hfCard3" runat="server" />
                            </td>
                        </tr>
                    </table>

                    <asp:HiddenField ID="hfCobrand" runat="server" />
                </asp:Panel>
                <asp:Panel ID="pnlDiscntDtl" Style="display: none" CssClass="panel-cover" runat="server">
                    <table border="0" align="center">
                        <tr>
                            <td class="matters">
                                <asp:Repeater ID="gvDiscDt" runat="server">
                                    <HeaderTemplate>
                                        <table border="1">
                                            <tr style="background-color: #b0c4de; border-color: #b0c4de; border: 1pt; font-weight: bold;">
                                                <th style="width: 150px">Month
                                                </th>
                                                <th style="width: 20%">Fee
                                                </th>
                                                <th style="width: 20%">Disc(%)
                                                </th>
                                                <th style="width: 20%">Discount
                                                </th>
                                                <th style="width: 20%">Net
                                                </th>
                                            </tr>
                                    </HeaderTemplate>
                                    <%--, , , , --%>
                                    <AlternatingItemTemplate>
                                        <tr style="background-color: #DFE0E5; border: 1pt;">
                                            <td align="left">
                                                <%#Container.DataItem("MONTH")%>
                                            </td>
                                            <td align="right">
                                                <%#AccountFunctions.Round(Container.DataItem("FEE_AMOUNT"))%>
                                            </td>
                                            <td align="right">
                                                <%#AccountFunctions.Round(Container.DataItem("DISC_PERC"))%>
                                            </td>
                                            <td align="right">
                                                <%#AccountFunctions.Round(Container.DataItem("DISC_AMOUNT"))%>
                                            </td>
                                            <td align="right">
                                                <%#AccountFunctions.Round(Container.DataItem("NET_AMOUNT"))%>
                                            </td>
                                        </tr>
                                    </AlternatingItemTemplate>
                                    <ItemTemplate>
                                        <tr style="border: 1pt;">
                                            <td align="left">
                                                <%#Container.DataItem("MONTH")%>
                                            </td>
                                            <td align="right">
                                                <%#AccountFunctions.Round(Container.DataItem("FEE_AMOUNT"))%>
                                            </td>
                                            <td align="right">
                                                <%#AccountFunctions.Round(Container.DataItem("DISC_PERC"))%>
                                            </td>
                                            <td align="right">
                                                <%#AccountFunctions.Round(Container.DataItem("DISC_AMOUNT"))%>
                                            </td>
                                            <td align="right">
                                                <%#AccountFunctions.Round(Container.DataItem("NET_AMOUNT"))%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnelmoreinfo" runat="server" Style="display: none;" CssClass="panel-cover">
                    <table cellpadding="3" cellspacing="0" style="background-color: #FFFFCC; border-right: #ff9e18 1pt solid; border-top: #ff9e18 1pt solid; border-left: #ff9e18 1pt solid; border-bottom: #ff9e18 1pt solid;">
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" align="absMiddle" />
                                <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="return Showdata(1);">View Payment History</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton3" runat="server" OnClientClick="return Showdata(2);">View Charge Details</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="lnkAdvReciept" runat="server" OnClientClick="return Showdata(7);">View Advance Receipt</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton4" runat="server" OnClientClick="return Showdata(3);">Sibling Information</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton5" runat="server" OnClientClick="return Showdata(8);">Ageing</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton7" runat="server" OnClientClick="return Showdata(4);">Concession Details</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton6" runat="server" OnClientClick="return Showdata(5);">Fee Adjustment</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="return Showdata(6);">Cheque History</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton8" runat="server" OnClientClick="return Showdata(9);">Calculate Fee</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" alt="" />
                                <asp:LinkButton ID="lbStudentAudit" runat="server">Student Audit</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" alt="" />
                                <asp:LinkButton ID="lbNotes" OnClientClick="return false;" runat="server">Notes</asp:LinkButton>
                                <%--<ajaxToolkit:PopupControlExtender ID="pceNotes" runat="server" PopupControlID="pnlNotes"
                                Position="Left" TargetControlID="lbNotes" >
                            </ajaxToolkit:PopupControlExtender>--%>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

                <asp:Panel ID="pnlCheque" Style="display: none" runat="server" CssClass="panel-cover" Width="50%">
                    <asp:Label ID="lblBankInvalid" runat="server" EnableViewState="False" SkinID="LabelError" Height="0px"></asp:Label>
                    <table border="1" cellpadding="0" cellspacing="0" class="BlueTable_Colln"
                        bgcolor="#ffffff">
                        <tr class="title-bg">
                            <th align="left">Bank
                            </th>
                            <th align="left" id="tdhBA">Bank Account
                            </th>
                            <th align="left">Emirate
                            </th>
                            <th align="left">Cheque #<asp:LinkButton ID="lnkClear" OnClientClick="return ClearCheque();" runat="server">(Clear)</asp:LinkButton>
                            </th>
                            <th align="left">Amount
                            </th>
                            <th align="left">Cheque Date
                            </th>
                            <th align="left">
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/close_red.gif"
                                    OnClientClick="return HideAll();" TabIndex="89" /></th>
                        </tr>
                        <tr id="tr_chq1" class="table-popup">
                            <td align="left">

                                <asp:TextBox ID="txtBank1" runat="server" AutoPostBack="True" TabIndex="70"
                                    Style="width: 80% !important"></asp:TextBox>
                                <asp:ImageButton ID="imgBank1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="getBankOrEmirate(1,1); return false" TabIndex="75" />
                            </td>
                            <td id="tdBA1" align="left">
                                <asp:TextBox ID="txtBankAct1" runat="server" onfocus="this.blur();" AutoPostBack="True" TabIndex="70"
                                    Style="width: 80% !important"></asp:TextBox>
                                <asp:ImageButton ID="imgBankAct1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="getBank(1); return false;" TabIndex="75" /><asp:HiddenField ID="hfBankAct1" runat="server" />
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlEmirate1" runat="server" DataTextField="EMR_DESCR"
                                    DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="78">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtChqno1" runat="server" TabIndex="80" AutoCompleteType="Disabled" autocomplete="off"
                                    Style="width: 80% !important"></asp:TextBox>
                                <asp:ImageButton ID="imgCheque" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="getCheque() ; return false;" TabIndex="75" />
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtChequeTotal1" onBlur="UpdateSum('1','0','0','0');" onFocus="this.select();"
                                    runat="server" Style="text-align: right" TabIndex="85" AutoCompleteType="Disabled" autocomplete="off"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtChqDate1" runat="server" TabIndex="87" Style="width: 80% !important"></asp:TextBox>
                                <asp:ImageButton ID="imgChequedate1" runat="server" ImageUrl="~/Images/calendar.gif"
                                    TabIndex="88" />
                            </td>
                            <td align="left">
                                <asp:LinkButton ID="LinkButton9" runat="server" OnClientClick="Hidebank('tr_chq2',2);return false;">Add</asp:LinkButton>
                            </td>
                        </tr>
                        <tr id="tr_chq2" style="display: none" class="table-popup">
                            <td align="left">
                                <asp:TextBox ID="txtBank2" runat="server" AutoPostBack="True" TabIndex="90"
                                    Style="width: 80% !important"></asp:TextBox>
                                <asp:ImageButton ID="imgBank2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="  getBankOrEmirate(1,2);return false" TabIndex="95" />
                            </td>
                            <td id="tdBA2" align="left">
                                <asp:TextBox ID="txtBankAct2" runat="server" onfocus="this.blur();" AutoPostBack="True" TabIndex="70"
                                    Style="width: 80% !important"></asp:TextBox>
                                <asp:ImageButton ID="imgBankAct2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="  getBank(2); return false;" TabIndex="75" /><asp:HiddenField ID="hfBankAct2" runat="server" />
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlEmirate2" runat="server" DataTextField="EMR_DESCR"
                                    DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="100">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtChqno2" runat="server" TabIndex="105" AutoCompleteType="Disabled " autocomplete="off"
                                    Style="width: 80% !important"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtChequeTotal2" onBlur="UpdateSum('2','0','0','0');" onFocus="this.select();"
                                    runat="server" Style="text-align: right" TabIndex="110" AutoCompleteType="Disabled" autocomplete="off"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtChqDate2" runat="server" TabIndex="115" Style="width: 80% !important"></asp:TextBox>
                                <asp:ImageButton ID="imgChequedate2" runat="server" ImageUrl="~/Images/calendar.gif"
                                    TabIndex="120" />
                            </td>
                            <td align="left">
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/close_red.gif"
                                    OnClientClick="return HideBankRow('tr_chq2',2);" TabIndex="121" />
                                <asp:LinkButton ID="LinkButton10" runat="server" OnClientClick="Hidebank('tr_chq3',3);return false;">Add</asp:LinkButton>
                            </td>
                        </tr>
                        <tr id="tr_chq3" style="display: none" class="table-popup">
                            <td align="left">
                                <asp:TextBox ID="txtBank3" runat="server" AutoPostBack="True" TabIndex="125"
                                    Style="width: 80% !important"></asp:TextBox>
                                <asp:ImageButton ID="imgBank3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="getBankOrEmirate(1,3); return false" TabIndex="130" />
                            </td>
                            <td id="tdBA3" align="left">
                                <asp:TextBox ID="txtBankAct3" runat="server" onfocus="this.blur();" AutoPostBack="True" TabIndex="70"
                                    Style="width: 80% !important"></asp:TextBox>
                                <asp:ImageButton ID="imgBankAct3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="  getBank(3);return false;" TabIndex="75" /><asp:HiddenField ID="hfBankAct3" runat="server" />
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlEmirate3" runat="server" DataTextField="EMR_DESCR"
                                    DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="135">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtChqno3" runat="server" TabIndex="140" AutoCompleteType="Disabled" autocomplete="off"
                                    Style="width: 80% !important"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtChequeTotal3" onBlur="UpdateSum('3','0','0','0');" onFocus="this.select();"
                                    Style="text-align: right" runat="server" TabIndex="145" AutoCompleteType="Disabled" autocomplete="off"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtChqDate3" runat="server" TabIndex="150" Style="width: 80% !important"></asp:TextBox>
                                <asp:ImageButton ID="imgChequedate3" runat="server" ImageUrl="~/Images/calendar.gif"
                                    TabIndex="156" />
                            </td>
                            <td align="left">
                                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/close_red.gif"
                                    OnClientClick="return HideBankRow('tr_chq3',3);" TabIndex="157" />
                            </td>
                        </tr>
                    </table>
                    <asp:HiddenField ID="h_Bank1" runat="server" />
                    <asp:HiddenField ID="h_Bank2" runat="server" />
                    <asp:HiddenField ID="h_Bank3" runat="server" />
                    <%--<asp:SqlDataSource ID="SqlEmirate_m" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                        SelectCommand="SELECT     EMR_CODE, EMR_DESCR, EMR_ENQ_GROUP, EMR_CTY_ID&#13;&#10;FROM  EMIRATE_M&#13;&#10;WHERE     (EMR_CTY_ID IN&#13;&#10;                          (SELECT     BSU_COUNTRY_ID&#13;&#10;                            FROM          BUSINESSUNIT_M&#13;&#10;                            WHERE      (BSU_ID =@BSU_ID)))">
                        <SelectParameters>
                            <asp:SessionParameter SessionField="sBsuid" Name="BSU_ID"></asp:SessionParameter>
                        </SelectParameters>
                    </asp:SqlDataSource>--%>
                    <ajaxToolkit:CalendarExtender ID="calCheque1" runat="server"
                        PopupButtonID="imgChequedate1" TargetControlID="txtChqDate1" Format="dd/MMM/yyyy"
                        Enabled="True" PopupPosition="TopLeft">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="calCheque2" runat="server"
                        PopupButtonID="imgChequedate2" TargetControlID="txtChqDate2" Format="dd/MMM/yyyy"
                        Enabled="True" PopupPosition="TopLeft">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="calCheque3" runat="server"
                        PopupButtonID="imgChequedate3" TargetControlID="txtChqDate3" Format="dd/MMM/yyyy"
                        Enabled="True" PopupPosition="TopLeft">
                    </ajaxToolkit:CalendarExtender>
                </asp:Panel>
                <asp:Panel ID="pnlDiscount" runat="server" CssClass="panel-cover">
                    <table cellpadding="3" cellspacing="0" style="background-color: #FFFFCC; border-right: #ff9e18 1pt solid; border-top: #ff9e18 1pt solid; border-left: #ff9e18 1pt solid; border-bottom: #ff9e18 1pt solid;">
                        <tr>
                            <td align="left">
                                <asp:GridView ID="gvDiscount" runat="server" SkinID="GridViewRed" Width="100%" EmptyDataText="No Data Found"
                                    AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:BoundField DataField="Scheme" HeaderText="Scheme" Visible="False">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Priod" HeaderText="Valid period">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Slab" HeaderText="Fee Terms">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Fees" HeaderText="Fees">
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PayNow" HeaderText="Pay Now">
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="YouSave" HeaderText="You Save">
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Select">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbApply" runat="server" OnClick="lbApply_Click" OnClientClick="tblErrortop.click();return true;">Select</asp:LinkButton>
                                                <asp:Label ID="lblFee_ID" runat="server" Text='<%# Bind("FEE_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblFds_ID" runat="server" Text='<%# Bind("FDS_ID") %>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlVoucher" Style="display: none;" runat="server" CssClass="panel-cover">
                    <table border="0" cellpadding="3" cellspacing="0" align="center"
                        width="100" bgcolor="#ffffff">
                        <tr>
                            <td align="left" class="gridheader_pop_orange" style="font-size: 7pt !important;"><span class="field-label">Type</span>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlVoucherType" runat="server" Width="200px" DataSourceID="sdsVouherType" DataTextField="VTM_DESCR" DataValueField="VTM_ID" SkinID="DropDownListNormal">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="gridheader_pop_orange" style="font-size: 7pt !important;"><span class="field-label">VoucherNo.</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtVoucherNo1" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="gridheader_pop_orange" style="font-size: 7pt !important;"><span class="field-label">ReferenceNo.</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtVoucherRefNo1" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="gridheader_pop_orange" style="font-size: 7pt !important;"><span class="field-label">Card Holder Name</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtVoucherCardHolder1" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="gridheader_pop_orange" style="font-size: 7pt !important;"><span class="field-label">Issue Date</span>
                            </td>
                            <td align="left">
                                <%--<asp:TextBox ID="txtVoucherExpiry" runat="server"></asp:TextBox>--%>
                                <telerik:RadDatePicker ID="txtVoucherIssueDate1" runat="server" DateInput-EmptyMessage="Iss.Date">
                                    <DateInput ID="DateInput2" EmptyMessage="Iss.Date" DateFormat="dd/MMM/yyyy" runat="server">
                                    </DateInput>
                                    <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                        ViewSelectorText="x" runat="server">
                                    </Calendar>
                                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                </telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="gridheader_pop_orange" style="font-size: 7pt !important;"><span class="field-label">Expiry Date</span>
                            </td>
                            <td align="left">
                                <%--<asp:TextBox ID="txtVoucherExpiry" runat="server"></asp:TextBox>--%>
                                <telerik:RadDatePicker ID="txtVoucherExpiry1" runat="server" DateInput-EmptyMessage="Exp.Date">
                                    <DateInput ID="DateInput1" EmptyMessage="Exp.Date" DateFormat="dd/MMM/yyyy" runat="server">
                                    </DateInput>
                                    <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                        ViewSelectorText="x" runat="server">
                                    </Calendar>
                                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                </telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="gridheader_pop_orange" style="font-size: 7pt !important;"><span class="field-label">Amount</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtVoucherAmount1" runat="server" Text="0.00" Style="text-align: right;" onBlur="UpdateSum('0','0','0','0');" AutoCompleteType="None" autocomplete="off"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, Custom"
                                    ValidChars="." TargetControlID="txtVoucherAmount1" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:ImageButton ID="ImageButton5" runat="server" CssClass="button"
                                    OnClientClick="return HideAll();" TabIndex="54" Style="text-align: right" /><%--ImageUrl="~/Images/close_red.gif"--%>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div id="divRewards" runat="server" style="display: none;">
                    GEMS REWARDS Points summary
                </div>
                <asp:SqlDataSource ID="sdsVouherType" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                    SelectCommand="SELECT VTM_ID,VTM_DESCR FROM FEES.VOUCHERTYPE_M WHERE ISNULL(VTM_bDELETED,0)=0 ORDER BY VTM_DESCR"></asp:SqlDataSource>
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_hideBank" runat="server" />
                <asp:HiddenField ID="h_CrCard" runat="server" />
                <asp:HiddenField ID="h_Chequeid" runat="server" />
                <asp:HiddenField ID="hf_mode" runat="server" />
                <asp:HiddenField ID="hf_ctrl" runat="server" />

                <script type="text/javascript">
                    function setRewardsValue(Amt) {
                        $("#<%=txtRewardsAmount.ClientID%>").val(Amt);
                        $.fancybox.close();
                        UpdateSum('0', '0', '0', '0');
                    }
                </script>
                <asp:HiddenField ID="hfReportError" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
