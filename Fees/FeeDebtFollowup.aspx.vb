﻿Imports Telerik.Web.UI
Imports System
Imports System.Collections.Generic
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Services
Imports Microsoft.ApplicationBlocks.Data
Partial Class Fees_FeeDebtFollowup
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            h_Sel_Bsu_Id.Value = Session("sBsuid")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F733044") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            InitializePage()
            BindBusinessUnits()
            BindSearchFilters()
            BindFollowUpList()
            BindAlphabets()
            BindDebtFollowupReasons()
        End If
    End Sub
    Sub InitializePage()
        ViewState("gvStudents") = Nothing
        BindStudentGrid(ViewState("gvStudents"))
        h_STU_ID.Value = 0
        ddlFollowups.SelectedIndex = 0
        ddlFilters.SelectedIndex = 0
        ClearSearchFilterControls(False)
    End Sub
    Private Sub BindSearchFilters()
        Dim ds As DataSet = clsDebtFollowup.GET_SEARCH_FILTERS()
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ddlFilters.DataSource = ds.Tables(0)
            ddlFilters.DataTextField = "DFF_DESC"
            ddlFilters.DataValueField = "DFF_ID"
            ddlFilters.DataBind()
            Dim AllItem As New Telerik.Web.UI.RadComboBoxItem
            AllItem.Text = "<--Select filter-->"
            AllItem.Value = "0"
            ddlFilters.Items.Insert(0, AllItem)
            ddlFilters.SelectedIndex = 0
        End If
    End Sub
    Private Sub BindFollowUpList()
        Dim ds As DataSet = clsDebtFollowup.GET_DEBT_FOLLOW_UP_LIST()
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ddlFollowups.DataSource = ds.Tables(0)
            ddlFollowups.DataTextField = "DFM_DESC"
            ddlFollowups.DataValueField = "DFM_DAYS"
            ddlFollowups.DataBind()
            Dim AllItem As New Telerik.Web.UI.RadComboBoxItem
            AllItem.Text = "<--Select follow up list-->"
            AllItem.Value = "0"
            ddlFollowups.Items.Insert(0, AllItem)
            ddlFollowups.SelectedIndex = 0
        End If
    End Sub
    Private Sub BindBusinessUnits()
        Try
            Dim dsBsu As New DataSet
            Dim SelectedValue As String = Session("sBsuId")
            '  dsBsu = clsTaxFunctions.GetBusinessUnits(Session("sUsr_name"), True)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Params(4) As SqlParameter
            Params(0) = Mainclass.CreateSqlParameter("@USRNAME", Session("sUsr_name"), SqlDbType.Char)
            Params(1) = Mainclass.CreateSqlParameter("@MENUCODE", ViewState("MainMnu_code"), SqlDbType.BigInt)
            Params(2) = Mainclass.CreateSqlParameter("@SHOWACTONLY", True, SqlDbType.Bit)
            Params(3) = Mainclass.CreateSqlParameter("@SELCTD_BSU", Session("sBsuId"), SqlDbType.VarChar)
            Params(4) = Mainclass.CreateSqlParameter("@BSU_TYPE", "OWN_SCHOOL", SqlDbType.VarChar)
            dsBsu = SqlHelper.ExecuteDataset(str_conn, "[dbo].[GETBUSINESSUNIT_ACCESSLIST]", Params)
            If Not dsBsu Is Nothing AndAlso dsBsu.Tables(0).Rows.Count > 0 Then
                ddlBUnit.DataSource = dsBsu
                ddlBUnit.DataTextField = "BSU_NAMEwithShort"
                ddlBUnit.DataValueField = "BSU_ID"
                ddlBUnit.DataBind()
                Dim AllItem As New Telerik.Web.UI.RadComboBoxItem
                AllItem.Text = "<--Select filter-->"
                AllItem.Value = "0"
                ddlBUnit.Items.Insert(0, AllItem)
                'ddlBUnit.SelectedIndex = 0
                ddlBUnit.FindItemByValue(0).Selected = False
                ddlBUnit.FindItemByValue(SelectedValue).Selected = True
                ddlBUnit_SelectedIndexChanged(Nothing, Nothing)
                'ddlAcdYear_SelectedIndexChanged(Nothing, Nothing)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("BindBusinessUnits()-" & ex.Message, ViewState("MainMnu_code"))
        End Try
    End Sub
    Private Sub LoadAcademicYear()
        ddlAcdYear.Items.Clear()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(ddlBUnit.SelectedValue)
        ddlAcdYear.DataSource = dtACD
        ddlAcdYear.DataTextField = "ACY_DESCR"
        ddlAcdYear.DataValueField = "ACD_ID"
        ddlAcdYear.DataBind()
        Dim AllItem As New Telerik.Web.UI.RadComboBoxItem
        AllItem.Text = "<--Select filter-->"
        AllItem.Value = "0"
        ddlAcdYear.Items.Insert(0, AllItem)
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                'ddlAcdYear.SelectedIndex = -1
                'ddlAcdYear.Items.FindItemByValue(rowACD("ACD_ID")).Text &= " - CURRENT"
                ddlAcdYear.Items.FindItemByValue(rowACD("ACD_ID")).Selected = True
                'Exit For
            End If
        Next
        'ddlAcdYear.SelectedValue = 0
        ddlAcdYear_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub PopulateGrade()
        Try
            If ddlAcdYear.Items.Count > 0 AndAlso ddlAcdYear.SelectedValue > 0 Then

            End If
            Dim dtTable As DataTable = PopulateGradeandSections(ddlAcdYear.SelectedValue, ddlBUnit.SelectedValue)
            ' PROCESS Filter
            Dim dvTRM_DESCRIPTION As New DataView(dtTable, "", "GRM_GRD_ID", DataViewRowState.OriginalRows)
            Dim trSelectAll As New TreeNode("Select All", "ALL")
            Dim ienumTRM_DESCRIPTION As IEnumerator = dvTRM_DESCRIPTION.GetEnumerator
            Dim drTRM_DESCRIPTION As DataRowView
            While (ienumTRM_DESCRIPTION.MoveNext())
                'Processes List
                drTRM_DESCRIPTION = ienumTRM_DESCRIPTION.Current

                Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
                Dim contains As Boolean = False
                While (ienumSelectAll.MoveNext())
                    If ienumSelectAll.Current.Text = drTRM_DESCRIPTION("GRM_GRD_ID") Then
                        contains = True
                    End If
                End While
                Dim trNodeTRM_DESCRIPTION As New TreeNode(drTRM_DESCRIPTION("GRM_GRD_ID"), drTRM_DESCRIPTION("GRM_GRD_ID"))
                If contains Then
                    Continue While
                End If
                Dim strAMS_GRD As String = "GRM_GRD_ID = '" & _
                drTRM_DESCRIPTION("GRM_GRD_ID") & "'"
                Dim dvAMS_GRD As New DataView(dtTable, strAMS_GRD, "GRM_GRD_ID", DataViewRowState.OriginalRows)
                Dim ienumAMS_GRD As IEnumerator = dvAMS_GRD.GetEnumerator
                While (ienumAMS_GRD.MoveNext())
                    Dim drGRD_DESCR As DataRowView = ienumAMS_GRD.Current
                    Dim trNodeGRD_DESCR As New TreeNode(drGRD_DESCR("SCT_DESCR"), (drGRD_DESCR("SCT_ID")))
                    trNodeTRM_DESCRIPTION.ChildNodes.Add(trNodeGRD_DESCR)
                End While
                trSelectAll.ChildNodes.Add(trNodeTRM_DESCRIPTION)
            End While
            trGrades.Nodes.Clear()
            trGrades.Nodes.Add(trSelectAll)
            trGrades.DataBind()
            trGrades.CollapseAll()
            trGrades.Nodes(0).Expand()
        Catch ex As Exception
            'Message = getErrorMessage("4000")
            'ShowMessage(Message, True)
            UtilityObj.Errorlog("Fee Debt follow up, PopulateGrade: " + ex.Message, "PHOENIX")
        End Try
    End Sub
    Private Function PopulateGradeandSections(ByVal ACD_ID As String, ByVal BSU_ID As String) As DataTable
        Try
            Dim dsData As DataSet = Nothing
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@ACD_ID", ACD_ID, SqlDbType.BigInt)
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim sql_query As String = "[OASIS].[GET_ALL_GRADES_INSCHOOL]"
                dsData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sql_query, Param)
            End Using
            If Not dsData Is Nothing Then
                Return dsData.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Return Nothing
            'Message = getErrorMessage("4000")
            'ShowMessage(Message, True)
            UtilityObj.Errorlog("From PopulateGradeandSections" + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Function
    Private Sub HideButtons(ByVal bHide As Boolean)
        Me.btnOkay.Visible = Not bHide
        Me.btnCancel.Visible = Not bHide
    End Sub
    Sub ShowMessage(ByVal Message As String, Optional ByVal bError As Boolean = True)
        usrMessageBar1.ShowMessage(Message, bError)
    End Sub
    Sub ClearSearchFilterControls(Optional ByVal bClearUnit As Boolean = True)
        If bClearUnit Then
            ddlBUnit.SelectedValue = 0
            'ddlBUnit_SelectedIndexChanged(Nothing, Nothing)
            ddlAcdYear.Items.Clear()
        End If
        trGrades.Nodes.Clear()
        trGrades.CollapseAll()
        txtMobile.Text = ""
        txtEmailId.Text = ""
        txtOutstanding.Text = 0
        txtDuedays.Text = 0
        h_STU_ID.Value = 0
        txtStudent.Text = ""
    End Sub
    Private Sub FillTreeViewGrade(ByVal arrListSelectedIDs As ArrayList)
        If arrListSelectedIDs Is Nothing OrElse arrListSelectedIDs.Count <= 0 Then Return
        Dim i As Integer = 0
        While (i < arrListSelectedIDs.Count)
            FillTreeWithValueGrade(arrListSelectedIDs(i), trGrades.Nodes)
            i += 1
        End While
    End Sub
    Private Sub FillTreeWithValueGrade(ByVal ID As String, ByRef Nodes As TreeNodeCollection)
        For Each Node As TreeNode In Nodes
            If (Node.Value.ToUpper <> "ALL") Then
                If Node.Value = ID Then Node.Checked = True
            Else
                FillTreeWithValueGrade(ID, Node.ChildNodes)
            End If
        Next
    End Sub
    Private Sub FillTreeView(ByVal arrListSelectedIDs As ArrayList)
        If arrListSelectedIDs Is Nothing OrElse arrListSelectedIDs.Count <= 0 Then Return
        Dim i As Integer = 0
        While (i < arrListSelectedIDs.Count)
            FillTreeWithValue(arrListSelectedIDs(i), trGrades.Nodes)
            i += 1
        End While
    End Sub
    Private Sub FillTreeWithValue(ByVal ID As String, ByRef Nodes As TreeNodeCollection)
        For Each Node As TreeNode In Nodes
            If (Node.ChildNodes Is Nothing OrElse Node.ChildNodes.Count <= 0) Then
                If Node.Value = ID Then Node.Checked = True
            Else
                FillTreeWithValue(ID, Node.ChildNodes)
            End If
        Next
    End Sub
    Sub GetSelectedGradeAndSections(ByRef GRDIDs As String, ByRef SCTIDs As String)
        Dim j As Int32 = 0
        For Each node As TreeNode In trGrades.CheckedNodes
            If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                Continue For
            End If
            If node.Value <> "ALL" Then
                SCTIDs &= IIf(SCTIDs = "", "", "|") & node.Value
                If GRDIDs <> "" Or j = 0 Then
                    If Not GRDIDs.Contains(node.Parent.Value) Then
                        GRDIDs &= IIf(GRDIDs = "", "", "|") & node.Parent.Value
                    End If
                End If
                j += 1
            End If
        Next
    End Sub

    Sub BindStudentGrid(ByVal dt As DataTable)
        gvStudents.DataSource = dt
        gvStudents.DataBind()
    End Sub
    Sub BindDebtFollowupReasons()
        Dim dt As New DataTable
        dt = clsDebtFollowup.GET_DEBT_FOLLOW_UP_REASONS(False)
        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
            ddlfollowupReasons.DataSource = dt
            ddlfollowupReasons.DataTextField = "DFR_REASON"
            ddlfollowupReasons.DataValueField = "DFR_ID"
            ddlfollowupReasons.DataBind()
            ddlfollowupReasons.SelectedValue = -1
        End If
    End Sub
    Sub BindAlphabets()
        Dim rad As RadComboBoxItem
        Dim alpha As Char() = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray()
        For x As Integer = 0 To alpha.Length - 1
            rad = New RadComboBoxItem
            rad.Text = alpha(x)
            rad.Value = alpha(x)
            ddlAlphabets.Items.Add(rad)
        Next
    End Sub
    Private Function GetSelectedAlphabets() As String
        Dim collection As IList(Of RadComboBoxItem) = ddlAlphabets.CheckedItems
        Dim Alphabets As String = "", pipe As String = ""
        For Each item As RadComboBoxItem In collection
            Alphabets &= pipe & item.Value
            pipe = "|"
        Next
        Return Alphabets
    End Function

    <WebMethod()> _
    Public Shared Function GetStudent(ByVal BSUID As String, ByVal STUTYPE As String, ByVal prefix As String) As String()

        Return clsTaxFunctions.GetStudent(BSUID, STUTYPE, prefix)

    End Function

    Sub lbtnAddComment_Click(sender As Object, e As EventArgs)
        Dim lbtnAddComment As LinkButton = DirectCast(sender, LinkButton)
        Dim gvr As GridViewRow = sender.parent.parent
        If Not gvr Is Nothing Then
            Dim STUID As Long = gvStudents.DataKeys(gvr.RowIndex)("STU_ID")
            Dim mpeAddComment As AjaxControlToolkit.ModalPopupExtender = gvr.FindControl("mpeAddComment")

            mpeAddComment.Show()
        End If
    End Sub
    Protected Sub lbtnLoadStuDetail_Click(sender As Object, e As EventArgs) Handles lbtnLoadStuDetail.Click
        Dim objclsTaxFunctions As New clsTaxFunctions
        'objclsTaxFunctions.PROVIDER_BSU_ID = Session("sBsuId")
        objclsTaxFunctions.STU_BSU_ID = ddlBUnit.SelectedValue
        objclsTaxFunctions.STU_ID = h_STU_ID.Value
        'objclsTaxFunctions.FEE_ID = ddlFeeType.SelectedValue
        If objclsTaxFunctions.GET_STUDENT_DETAILS("S") Then
            txtStudent.Text = objclsTaxFunctions.STU_NO & " - " & objclsTaxFunctions.STU_NAME
        Else
            h_STU_ID.Value = 0
        End If
    End Sub
    Protected Sub ddlBUnit_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlBUnit.SelectedIndexChanged
        LoadAcademicYear()
        'ddlAcdYear_SelectedIndexChanged(Nothing, Nothing)
    End Sub
    Protected Sub ddlAcdYear_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlAcdYear.SelectedIndexChanged
        InitializePage()
        PopulateGrade()
    End Sub
  
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If ddlFollowups.SelectedIndex > 0 Then
            ddlFollowups.SelectedIndex = 0
            ViewState("gvStudents") = Nothing
            BindStudentGrid(Nothing)
        End If
        Dim objcls As New clsDebtFollowup
        objcls.DFF_STU_TYPE = "S"
        objcls.DFF_STU_BSU_ID = ddlBUnit.SelectedValue
        objcls.DFF_STU_ACD_ID = ddlAcdYear.SelectedValue
        objcls.DFF_STU_SCT_IDs = ""
        objcls.DFF_STU_GRD_IDs = ""
        GetSelectedGradeAndSections(objcls.DFF_STU_GRD_IDs, objcls.DFF_STU_SCT_IDs)
        objcls.DFF_STU_MOBILENO = txtMobile.Text.Trim
        objcls.DFF_STU_EMAIL_ID = txtEmailId.Text.Trim
        objcls.DFF_STU_ID = FeeCollection.GetDoubleVal(h_STU_ID.Value)
        objcls.DFF_STU_OS = FeeCollection.GetDoubleVal(txtOutstanding.Text)
        objcls.DFF_STU_FEE_DUE_SINCE = FeeCollection.GetDoubleVal(txtDuedays.Text)
        objcls.FOLLW_UP_ID = ddlFollowups.SelectedValue
        objcls.DFF_ALPHABETS = GetSelectedAlphabets()
        objcls.DFF_LOG_USER = Session("sUsr_name")
        If rbALL.Checked = True Then
            objcls.DFF_COVID_CONCESSION = "A"
        ElseIf rbYes.Checked = True Then
            objcls.DFF_COVID_CONCESSION = "Y"
        ElseIf rbNo.Checked = True Then
            objcls.DFF_COVID_CONCESSION = "N"
        End If
        objcls.DFF_REASON = ddlfollowupReasons.SelectedValue

        Dim dtStudents As New DataTable
        Dim RetVal As Integer = 966
        dtStudents = objcls.SEARCH_STUDENTS_FOR_DEBT_FOLLOWUP(RetVal)
        If dtStudents Is Nothing Or RetVal <> 0 Then
            ShowMessage(UtilityObj.getErrorMessage(RetVal))
        End If
        ViewState("gvStudents") = dtStudents
        BindStudentGrid(dtStudents)
    End Sub
    Protected Sub btnSaveFilter_Click(sender As Object, e As EventArgs) Handles btnSaveFilter.Click
        If ddlFilters.Items.Count > 0 AndAlso ddlFilters.SelectedValue > 0 Then
            txtFilterName.Text = ddlFilters.SelectedItem.Text
        End If
        HideButtons(False)
        MPE1.Show()
    End Sub
    Protected Sub ddlFilters_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlFilters.SelectedIndexChanged
        ClearSearchFilterControls()
        Dim OBJCLS As New clsDebtFollowup

        OBJCLS.DFF_ID = ddlFilters.SelectedValue
        OBJCLS.GET_SEARCH_FILTER() 'function to get the saved filter data
        ddlBUnit.SelectedValue = OBJCLS.DFF_STU_BSU_ID
        ddlBUnit_SelectedIndexChanged(Nothing, Nothing)
        ddlAcdYear.SelectedValue = OBJCLS.DFF_STU_ACD_ID
        ddlAcdYear_SelectedIndexChanged(Nothing, Nothing)
        txtMobile.Text = OBJCLS.DFF_STU_MOBILENO
        txtEmailId.Text = OBJCLS.DFF_STU_EMAIL_ID
        txtOutstanding.Text = OBJCLS.DFF_STU_OS
        txtDuedays.Text = OBJCLS.DFF_STU_FEE_DUE_SINCE
        h_STU_ID.Value = OBJCLS.DFF_STU_ID
        Dim arrGRD_IDs As New ArrayList, arrSCT_IDs As New ArrayList
        If OBJCLS.DFF_STU_GRD_IDs <> "" Then
            Dim grdids As String() = OBJCLS.DFF_STU_GRD_IDs.Split("|")
            If grdids.Length > 0 Then
                For i As Int16 = 0 To grdids.Length - 1
                    arrGRD_IDs.Add(grdids(i))
                Next
            End If
        End If
        If OBJCLS.DFF_STU_SCT_IDs <> "" Then
            Dim sctids As String() = OBJCLS.DFF_STU_SCT_IDs.Split("|")
            If sctids.Length > 0 Then
                For i As Int16 = 0 To sctids.Length - 1
                    arrSCT_IDs.Add(sctids(i))
                Next
            End If
        End If
        FillTreeViewGrade(arrGRD_IDs)
        FillTreeView(arrSCT_IDs)

        lbtnLoadStuDetail_Click(Nothing, Nothing)
    End Sub
    Protected Sub btnOkay_Click(sender As Object, e As EventArgs) Handles btnOkay.Click
        If ddlFilters.Items.Count > 0 AndAlso ddlFilters.SelectedValue > 0 Then
            If txtFilterName.Text.Trim = ddlFilters.SelectedItem.Text Then
                usrMessageBar2.ShowMessage("You are about to update existing filter with the new search criteria", False)
            End If
        End If

        Dim objcls As New clsDebtFollowup
        objcls.DFF_ID = IIf(ddlFilters.Items.Count > 0, ddlFilters.SelectedValue, 0)
        objcls.DFF_BSU_ID = Session("sBsuId")
        objcls.DFF_DESC = txtFilterName.Text.Trim
        objcls.DFF_LOG_USER = Session("sUsr_name")
        objcls.DFF_STU_ACD_ID = IIf(ddlAcdYear.Items.Count > 0, ddlAcdYear.SelectedValue, 0)
        objcls.DFF_STU_BSU_ID = IIf(ddlBUnit.Items.Count > 0, ddlBUnit.SelectedValue, 0)
        objcls.DFF_STU_EMAIL_ID = txtEmailId.Text.Trim
        objcls.DFF_STU_FEE_DUE_SINCE = FeeCollection.GetDoubleVal(txtDuedays.Text)
        objcls.DFF_STU_ID = FeeCollection.GetDoubleVal(h_STU_ID.Value)
        objcls.DFF_STU_MOBILENO = txtMobile.Text.Trim
        objcls.DFF_STU_OS = FeeCollection.GetDoubleVal(txtOutstanding.Text)
        objcls.DFF_STU_TYPE = "S"
        objcls.DFF_STU_SCT_IDs = ""
        objcls.DFF_STU_GRD_IDs = ""
        GetSelectedGradeAndSections(objcls.DFF_STU_GRD_IDs, objcls.DFF_STU_SCT_IDs)

        Dim RetMessage As String = ""
        Dim RetVal As Integer = objcls.SAVE_DEBT_FOLLOWUP_FILTER_M(RetMessage)
        If RetVal = 0 And RetMessage = "" Then
            'lblMessage.Text = "Filter selection saved successfully"
            usrMessageBar2.ShowMessage("Filter selection saved successfully", False)
            BindSearchFilters()
        Else
            If RetMessage <> "" Then
                'lblMessage.Text = RetMessage
                usrMessageBar2.ShowMessage(RetMessage, True)
            Else
                'lblMessage.Text = UtilityObj.getErrorMessage(RetVal)
                usrMessageBar2.ShowMessage(UtilityObj.getErrorMessage(RetVal), True)
            End If
        End If
        MPE1.Show()
        Me.btnOkay.Visible = False
    End Sub
    Protected Sub gvStudents_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvStudents.PageIndexChanging
        gvStudents.PageIndex = e.NewPageIndex
        BindStudentGrid(ViewState("gvStudents"))
    End Sub
    Protected Sub lbtnClearStudent_Click(sender As Object, e As EventArgs) Handles lbtnClearStudent.Click
        h_STU_ID.Value = 0
        txtStudent.Text = ""
    End Sub

    Protected Sub gvStudents_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvStudents.RowDataBound
        Dim lbtnAddComment As LinkButton = e.Row.FindControl("lbtnAddComment")
        If Not lbtnAddComment Is Nothing Then
            Dim STU_ID As String = gvStudents.DataKeys(e.Row.RowIndex)("STU_ID")
            lbtnAddComment.Attributes.Add("onclick", "ShowWindowWithClose('FeeDebtAddComments.aspx?ID=" & Encr_decrData.Encrypt(STU_ID) & "&BU=" & Encr_decrData.Encrypt(ddlBUnit.SelectedValue) & "', 'ADD COMMENTS', '85%', '90%');")
        End If
    End Sub

    Protected Sub ddlFollowups_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlFollowups.SelectedIndexChanged
        ddlFilters.SelectedIndex = 0
        ClearSearchFilterControls(False)
        ViewState("gvStudents") = Nothing
        BindStudentGrid(Nothing)
        Dim objcls As New clsDebtFollowup
        objcls.DFF_STU_TYPE = "S"
        objcls.DFF_STU_BSU_ID = ddlBUnit.SelectedValue
        objcls.DFF_STU_ACD_ID = 0
        objcls.DFF_STU_SCT_IDs = ""
        objcls.DFF_STU_GRD_IDs = ""
        GetSelectedGradeAndSections(objcls.DFF_STU_GRD_IDs, objcls.DFF_STU_SCT_IDs)
        objcls.DFF_STU_MOBILENO = txtMobile.Text.Trim
        objcls.DFF_STU_EMAIL_ID = txtEmailId.Text.Trim
        objcls.DFF_STU_ID = FeeCollection.GetDoubleVal(h_STU_ID.Value)
        objcls.DFF_STU_OS = FeeCollection.GetDoubleVal(txtOutstanding.Text)
        objcls.DFF_STU_FEE_DUE_SINCE = FeeCollection.GetDoubleVal(txtDuedays.Text)
        objcls.FOLLW_UP_ID = ddlFollowups.SelectedValue
        Dim dtStudents As New DataTable
        dtStudents = objcls.SEARCH_STUDENTS_FOR_DEBT_FOLLOWUP()
        If Not dtStudents Is Nothing Then
            ViewState("gvStudents") = dtStudents
            BindStudentGrid(dtStudents)
        End If
    End Sub

    Protected Sub btnReloadFollowup_Click(sender As Object, e As ImageClickEventArgs) Handles btnReloadFollowup.Click
        ddlFilters_SelectedIndexChanged(Nothing, Nothing)
    End Sub
End Class
