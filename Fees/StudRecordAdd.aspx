<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="StudRecordAdd.aspx.vb" Inherits="Fee_StudRecordAdd" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script src="../Scripts/jquery-1.10.2.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        function getStudent() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var sFeatures;
            sFeatures = "dialogWidth: 875px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var ProviderBSU = $("#<%=ddlGEMSSchool.ClientID %>").val();
            var StuBsuId = $("#<%=ddlGEMSSchool.ClientID %>").val();
            var hMode = $("#<%=h_Mode.ClientID%>").val();
            var bCobrand;
            Stu_type = "S";
            if (hMode == "add") {
                pMode = "ALL_SOURCE_STUDENTS"
                url = "../Fees/ShowStudent.aspx?type=" + pMode + "&ProviderBSUID=" + ProviderBSU + "&Stu_Bsu_Id=" + StuBsuId + "&STU_TYPE=" + Stu_type;
                result = radopen(url, "pop_getstudent");
                <%--<%--if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('||');
                $("#<%=txtStudentname.ClientID %>").val(NameandCode[1]);
                $("#<%=h_STUD_ID.ClientID %>").val(NameandCode[0]);
                $("#<%=txtStdNo.ClientID %>").val(NameandCode[2]);
                return true;--%>
            }
            else {
                alert('Not allowed to change student on edit mode');
                return false;
            }


           

        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                $("#<%=txtStudentname.ClientID %>").val(NameandCode[1]);
                $("#<%=h_STUD_ID.ClientID %>").val(NameandCode[0]);
                $("#<%=txtStdNo.ClientID %>").val(NameandCode[2]);
                __doPostBack('<%=txtStudentname.ClientID%>', 'TextChanged');
            }
        }



        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">        
        <Windows>
            <telerik:RadWindow ID="pop_getstudent" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> <asp:Label ID="tr_Header" runat="server" Text="GEMS Student Selection"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table align="center" width="100%">
        <tr align="left">
            <td>
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                   HeaderText="Following condition required" ValidationGroup="dayBook" />
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
            </td>
        </tr>
        <tr align="left">
            <td>
                <table align="center" width="100%">
                   
                    <tr id="tr_1" runat="server">
                        <td align="left" width="20%"><span class="field-label">Select GEMS School</span>
                        </td>
                        
                        <td align="left" width="80%" colspan="3">
                            <asp:DropDownList ID="ddlGEMSSchool" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:LinkButton ID="hlClearGEMSStudentSel" runat="server">Clear</asp:LinkButton></td>
                    </tr>
                    <tr id="tr_ClientSchool" visible="false" runat="server">
                        <td align="left"><span class="field-label">Select Client</span></td>
                        
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlTptClient" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="tr_2" runat="server">
                        <td align="left"><span class="field-label">Student</span>
                        </td>
                        
                        <td align="left" colspan="3">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="40%">
                                        <asp:TextBox ID="txtStdNo" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" OnClientClick="getStudent();return false;"
                                ImageUrl="~/Images/cal.gif" />
                                    </td>
                                    <td align="left" width="60%">
                                        <asp:TextBox ID="txtStudentname" runat="server" OnTextChanged="txtStudentname_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            
                            </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" class="title-bg">                                                           
                                    <asp:Label ID="lblCaption" runat="server" Text="Academic Year/Grade/Section/Shift"></asp:Label>                          
                        </td>
                    </tr>
                    <tr id="tr_ACD_tr1" runat="server">
                        <td align="left" width="20%"><span class="field-label">Academic Year</span>
                        </td>
                        
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Grade</span>
                        </td>
                        
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="tr_ACD_tr2" runat="server">
                        <td align="left"><span class="field-label">Section</span>
                        </td>
                        
                        <td align="left" style="text-align: left">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left"><span class="field-label">Shift</span>
                        </td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlShift" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="tr_3" runat="server">
                        <td align="left" colspan="4" class="title-bg">                            
                                    <asp:Label ID="Label2" runat="server" Text="E-Sports Venue and Period"></asp:Label>                          
                        </td>
                    </tr>
                    <tr id="tr_4" runat="server">
                        <td align="left"><span class="field-label">Venue</span>
                        </td>
                        
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlVenue" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="tr_5" runat="server">
                        <td align="left"><span class="field-label">Period</span>
                        </td>                        
                        <td align="left"><span class="field-label">From</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtFDT" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgBtnFDT" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="ceFDT_img" CssClass="MyCalendar" runat="server"
                                PopupButtonID="imgBtnFDT" TargetControlID="txtFDT" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="ceFDT_txt" CssClass="MyCalendar" runat="server"
                                PopupButtonID="txtFDT" TargetControlID="txtFDT" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator93" runat="server"
                                ControlToValidate="txtFDT" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Start Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator>--%>
                            </td>
                        <td align="left">
                            <span class="field-label">To</span>
                            </td>
                        <td align="left">
                            <asp:TextBox ID="txtTDT" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgBtnTDT" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="ceTDT_img" CssClass="MyCalendar" runat="server"
                                PopupButtonID="imgBtnTDT" TargetControlID="txtTDT" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="ceTDT_txt" CssClass="MyCalendar" runat="server"
                                PopupButtonID="txtTDT" TargetControlID="txtTDT" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator95" runat="server"
                                ControlToValidate="txtTDT" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the End Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="5" class="title-bg">
                             <asp:Label ID="lblCaption2" runat="server" Text="Student Details"></asp:Label>
                        </td>
                    </tr>
                    <tr id="tr_SDtr1" runat="server">
                        <td align="left"><span class="field-label">Date of Join</span><font class="text-danger">*</font>
                        </td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtDoj" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgBtnDOJ" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="ceDOJ_img" CssClass="MyCalendar" runat="server"
                                PopupButtonID="imgBtnDOJ" TargetControlID="txtDoj" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="ceDOJ_txt" CssClass="MyCalendar" runat="server"
                                PopupButtonID="txtDoj" TargetControlID="txtDoj" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtDOj"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Last Rejoining  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtDOj"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Last Rejoining  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator><br />
                            (dd/mmm/yyyy)
                        </td>
                        <td align="left"><span class="field-label">Fee Id</span><font class="text-danger">*</font>
                        </td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtFeeId" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="tr_SDtr2" runat="server">
                        <td align="left"><span class="field-label">Full Name in English</span><br />
                            (First,Middle,Last)<font class="text-danger">*</font>
                        </td>
                        
                        <td align="left" colspan="4">
                             <table width="100%">
                                <tr>
                                    <td align="left" width="35%">
                                        <asp:TextBox ID="txtFname_E" runat="server" CssClass="m-0"></asp:TextBox>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtMname_E" runat="server" CssClass="m-0"></asp:TextBox>
                                    </td>
                                    <td align="left" width="35%">
                                        <asp:TextBox ID="txtLname_E" runat="server" CssClass="m-0"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            
                        </td>
                    </tr>
                    <tr id="tr_SDtr3" runat="server" >
                        <td align="left"><span class="field-label">Gender</span><span class="text-danger">*</span>
                        </td>
                       
                        <td align="left">
                            <asp:RadioButton ID="rdMale" runat="server" GroupName="Gender" Text="Male" Checked="true" />
                            <asp:RadioButton ID="rdFemale" runat="server" GroupName="Gender" Text="Female" />
                        </td>
                        <td align="left"><span class="field-label">Religion</span>
                        </td>
                       
                        <td align="left">
                            <asp:DropDownList ID="ddlReligion" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="tr_SDtr4" runat="server">
                        <td align="left"><span class="field-label">Date of Birth</span><span class="text-danger">*</span>
                        </td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtDob" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgBtnDOB" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="ceDOB_img" CssClass="MyCalendar" runat="server"
                                PopupButtonID="imgBtnDOB" TargetControlID="txtDob" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="ceDOB_txt" CssClass="MyCalendar" runat="server"
                                PopupButtonID="txtDob" TargetControlID="txtDob" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator72" runat="server"
                                ControlToValidate="txtDOB" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Last Rejoining  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator92" runat="server"
                                ControlToValidate="txtDOB" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Last Rejoining  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator><br />
                            (dd/mmm/yyyy)
                        </td>
                        <td align="left"><span class="field-label">Nationality</span>
                        </td>
                        
                        <td align="left">
                            <asp:DropDownList ID="ddlNationality_Birth" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="subheader_img">
                        <td align="left" colspan="5" class="title-bg">                            
                                    <asp:Label ID="lblCaption3" runat="server" Text="Parent Details"></asp:Label>                        </td>
                    </tr>
                    <tr id="tr_PDtr1" runat="server">
                        <td align="left"><span class="field-label">Full Name in English</span><br />
                            (First,Middle,Last)<span class="text-danger">*</span>
                        </td>
                        
                        <td align="left" colspan="4">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="35%">
                                        <asp:TextBox ID="txtFname_P" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left" width="30%">
                                         <asp:TextBox ID="txtMname_P" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left" width="35%">
                                        <asp:TextBox ID="txtLname_P" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            
                           
                            
                        </td>
                    </tr>
                    <tr id="tr_PDtr2" runat="server">
                        <td align="left"><span class="field-label">P.O BOX</span>
                        </td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtFCOMPOBOX" runat="server"></asp:TextBox>
                        </td>
                        <td align="left"><span class="field-label">Emirate</span>
                        </td>
                        
                        <td align="left">
                            <asp:DropDownList ID="ddlFCOMEmirate" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="tr_PDtr3" runat="server">
                        <td align="left"><span class="field-label">Mobile</span>
                        </td>
                       
                        <td align="left">
                            <asp:TextBox ID="txtFMobile_No" runat="server" MaxLength="14"></asp:TextBox><br />
                            (Country-Area-Number)
                        </td>
                        <td align="left"><span class="field-label">Email</span>
                        </td>
                       
                        <td align="left">
                            <asp:TextBox ID="txtFEmail" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="5" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="6" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                TabIndex="7" OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_Mode" runat="server" />
                <asp:HiddenField ID="h_STUD_ID" runat="server" />
            </td>
        </tr>
    </table>

                </div>
            </div>
        </div>

</asp:Content>
