<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeStudentDiscount.aspx.vb" Inherits="Fees_Reports_ASPX_FeeStudentDiscount" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function GetStudentSingle() {

            var NameandCode;
            var result;
            var bsu;
            bsu = document.getElementById('<%=ddBusinessunit.ClientID %>').value
            var url = "ShowStudent.aspx?bsu=" + bsu + "&type=NO";
            result = radopen(url, "pop_up");
            <%--if (result != '' && result != undefined) {
            NameandCode = result.split('||');
            document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtStudName.ClientID %>').value = NameandCode[1];
            return true;
        }
        else {
            return false;
        }--%>
        }


        function GetStudent() {
            var SingleYN = document.getElementById('<%=h_Single.ClientID %>').value
            if (SingleYN == "1") {
                GetStudentSingle();
            }
            return true;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStudName.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= h_STUD_ID.ClientID%>', 'ValueChanged');
            }
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="5" cellspacing="0"
                    style="width: 100%;">
                    <%--<tr class ="subheader_BlueTableView">
            <th align="left" colspan="2" style="height: 19px">
                <asp:Label ID="lblReportCaption" runat="server"></asp:Label></th>
        </tr>--%>
                    <tr>
                        <td align="left" width="20%" valign="top"><span class="field-label">Business Unit</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddBusinessunit" runat="server" AutoPostBack="True"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"
                                TabIndex="5" SkinID="DropDownListNormal">
                            </asp:DropDownList></td>
                        <td align="left" width="20%" valign="top"><span class="field-label">Student</span></td>
                        <td align="left" width="30%" valign="top">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtStudNo" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="GetStudent(); return false;" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStudName" runat="server" AutoPostBack="True"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" valign="top" width="20%"><span class="field-label">Grade</span></td>
                        <td align="left" width="30%" valign="top">
                            <asp:TextBox ID="txtGrade" runat="server" AutoPostBack="True"
                                ReadOnly="True"></asp:TextBox></td>
                        <td align="left" width="20%" valign="top"><span class="field-label">Date Of Join</span></td>
                        <td align="left" width="30%" valign="top">
                            <asp:TextBox ID="txtDOJ" runat="server" AutoPostBack="True"
                                ReadOnly="True"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td align="left" valign="top" width="20%"><span class="field-label">Outstanding Amount</span></td>
                        <td align="left" width="30%" valign="top">
                            <asp:TextBox ID="txtOutstandingAmount" runat="server"
                                AutoPostBack="True" ReadOnly="True"></asp:TextBox></td>
                        <td align="left" width="20%" valign="top"><span class="field-label">Enter the Amount</span></td>
                        <td align="left" valign="top"
                            width="30%">
                            <asp:TextBox ID="txtAmount" runat="server" AutoPostBack="True"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td align="left" valign="top" width="20%"><span class="field-label">Discount</span></td>
                        <td align="left" valign="top" width="30%">
                            <asp:TextBox ID="txtDiscount" runat="server" AutoPostBack="True"
                                ReadOnly="True"></asp:TextBox></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4"
                            valign="top">
                            <asp:Button ID="btnProcess" runat="server" CssClass="button" Text="Calculate"
                                ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_STUD_ID" runat="server" OnValueChanged="h_STUD_ID_ValueChanged" />
                <asp:HiddenField ID="h_Single" runat="server" Value="1" />
            </div>
        </div>
    </div>
</asp:Content>

