﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FeePaymentPlanApproval.aspx.vb" Inherits="Fees_FeePaymentPlanApproval" %>

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PHOENIX:: GEMS FEE PAYMENT PLAN</title>
<%--    <link rel="stylesheet" href="../cssfiles/bootstrapAmbassador.css" type="text/css" media="screen" />
    <link href="../cssfiles/Ambassador.css" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />--%>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700;800&display=swap" rel="stylesheet">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <style>
        body {
            font-family: 'Montserrat', sans-serif;
            color: #6b6b6b;
            background-color: #eff4f7;
        }

        h1 {
            font-size: 2.5rem;
            font-weight: 800;
            color: #5caad2;
        }

        h3 {
            font-family: 'MontserratBold', sans-serif;
            font-size: 1.75rem;
            padding-bottom: 8px;
            color: #5caad2;
            border-bottom: 1px solid #f5f5f5;
        }

        label.title {
            font-size: 1.5rem;
            color: #888888;
            font-weight: 500;
        }

        label.item-value {
            font-size: 1.5rem;
            font-weight: bold;
            color: #888888;
        }

        span.status {
            padding: 3px 8px;
            border-radius: 6px;
            font-size: 1.25rem;
        }

            span.status.approved {
                background: rgba(41, 241, 121, 0.3);
            }

            span.status.reject {
                background: rgba(241, 41, 41, .3);
            }

            span.status.pending {
                background: rgba(255, 210, 44, .3);
            }

        .bold-label, .bold-value {
            font-size: 1.5rem;
            font-weight: 600;
        }

        table tbody tr th {
            background-color: #dbf1fd !important;
            border:0 !important;
        }
        table tbody tr td{
            border:0 !important;
        }

        .bg-container {
            background-color: #fff;
            border-radius: 12px;
            border: 1px solid #efefef;
        }

        .pt-5 {
            padding-top: 50px;
        }

        .pb-5 {
            padding-bottom: 50px;
        }

        .p-2 {
            padding: 20px;
        }

        .mt-1 {
            margin-top: 10px;
        }

        .mb-1 {
            margin-bottom: 10px;
        }

        input[type="submit"] {
            background-color: #5caad2 !important;
            color: #fff;
            border-radius: 6px;
            border: 1px solid #3e8db5;
            padding: 6px 10px;
            min-width: 120px;
        }

            input[type="submit"]:hover {
                background-color: #3e8db5 !important;
                color: #fff;
                border: 1px solid #5caad2;
            }

            input[type="submit"].btn-reject {
                background-color: #d65d5d !important;
                border: 1px solid #b33f3f;
            }

                input[type="submit"].btn-reject:hover {
                    background-color: #b33f3f !important;
                    border: 1px solid #d65d5d;
                }
    </style>
    <style>
        .RadGrid_Default .rgRow td, .RadGrid_Default .rgAltRow td {
            border-color: #d0d7e5 !important;
        }

        .RadGrid_Default .rgRow td, .RadGrid_Default .rgAltRow td, .RadGrid_Default .rgEditRow td, .RadGrid_Default .rgFooter td {
            border-style: solid;
            border-width: 0 0 1px 1px !important;
        }

        .ambassador-rowheader {
            background: lightblue !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager runat="server" ID="ScriptManager1" AsyncPostBackTimeout="600">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" class="width-100">
            <ContentTemplate>
                <div class="container bg-container pb-5 mt-1 mb-1">
                    <div class="page-header">
                        <h1 class="text-center">
                            <asp:Label ID="lbl_SchoolName" runat="server" EnableViewState="True"></asp:Label></h1>
                        <div class="text-right">
                            <strong>Payment Plan Status -</strong> <span id="status_class" runat="server">
                                <asp:Label ID="lbl_Status" runat="server" EnableViewState="True" Text=""></asp:Label></span>
                        </div>
                    </div>
                    <div class="row" id="id_banner" runat="server" visible="false">
                        <div class="col-lg-12">
                            <img src="../Images/ambassdor-H.png" alt="logo" class="img-fluid" id="header_image" runat="server" />
                        </div>
                        <%--  <div class="col-lg-12 text-center">
                            <asp:Label ID="lbl_SchoolName2" CssClass="ambassador-mainheader" runat="server" EnableViewState="True"></asp:Label>
                        </div>--%>
                    </div>
                    <div class="container-fluid">
                        <%--<div class="row">
                            <div class="col-12 dropdown-divider">&nbsp; </div>

                        </div>--%>

                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <asp:Label ID="lblMessage" runat="server"  EnableViewState="True"></asp:Label>
                            </div>
                            <asp:HiddenField ID="LabelPhotoPath" runat="server" EnableViewState="False"></asp:HiddenField>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-12">
                                <h3>Student Details</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <label class="title">Academic Year</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <label class="item-value">
                                            <asp:Label ID="LabelACDYear" runat="server" /></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <label class="title">Name</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <label class="item-value">
                                            <asp:Label ID="Name" runat="server" /></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <label class="title">Date of Joining</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <label class="item-value">
                                            <asp:Label ID="DOJ" runat="server" /></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <label class="title">Fee Type</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <label class="item-value">
                                            <asp:CheckBoxList ID="cblFeeType" CssClass="field-label" RepeatDirection="Horizontal" RepeatColumns="5" runat="server" TabIndex="3" Enabled="false"></asp:CheckBoxList></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <label class="title">Student No.</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <label class="item-value">
                                            <asp:Label ID="StudentNo" runat="server" /></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <label class="title">Grade</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <label class="item-value">
                                            <asp:Label ID="Grade" runat="server" />
                                            <asp:Label ID="lblDate" runat="server" Visible="false" />
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <label class="title">Parent Details</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <label class="item-value">
                                            <asp:Label ID="ParentDetails" runat="server" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%-- <div class="col-4 col-lg-2 text-left" style="display: none;">
                                <img id="cphParent_urcPInfo_repInfo_imgEmpImage_0" title='<%= Name.Text%>' src='<%= LabelPhotoPath.Value%>' style="height: 142px; width: 130px;" />
                            </div>                           
                        --%>
                        <%--<div class="row">
                            <div class="col-12 ">&nbsp; </div>
                        </div>--%>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-12">
                                <h3>Outstanding Details</h3>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="bold-label">Outstanding Amount</div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="bold-value">
                                    <asp:Label ID="LabelOutstanding" runat="server" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-12">
                                <h3>Upcoming Term/Month</h3>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="bold-label">Upcoming Term Fees Amount</div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="bold-value">
                                    <asp:Label ID="LabelUPCOMINGTOTAL" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-12">
                                <h3>Payment Details</h3>
                            </div>
                        </div>
                        <asp:GridView ID="gvFeeDetail" Visible="false" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="True" EnableModelValidation="True" SkinID="GridViewNormal" ShowFooter="false" RowStyle-Wrap="true" TabIndex="4">
                            <Columns>
                                <asp:TemplateField HeaderText="Total" Visible="false">
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalUPCOMING" runat="server" Text="0.00"></asp:Label>
                                    </FooterTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelUPCOMING" runat="server" Text='<%# Bind("TOTAL", "{0:0.00}")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right" />
                                    <FooterStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <%--                        <div class="row border ambassador-header  ambassador-rowheader">
                            <div class="col-12 border">Outstanding Details: </div>

                        </div>
                        <div class="row table-bordered mb-2">
                            <div class="col-lg-4 col-md-4 col-4 border font-weight-bold p-3">Outstanding Amount:</div>
                            <div class="col-lg-8 col-md-8 col-8 text-center font-weight-bold p-3">
                               
                            </div>
                        </div>

                        <div class="row border ambassador-header  ambassador-rowheader">
                            <div class="col-12 border">Fee for upcoming Term/Month: </div>

                        </div>
                        <div class="row border ">
                            <div class="col-lg-12 col-md-12 col-12 table-responsive">
                              
                            </div>


                        </div>--%>

                        <%--                        <div class="row">
                            <div class="col-12 ">
                                <asp:Label ID="lblGrandTotal" CssClass="field-label" runat="server" Text=""></asp:Label>
                            </div>

                        </div>
                        <div class="row border ambassador-header  ambassador-rowheader">
                            <div class="col-12 border border-color: #d0d7e5 !important;">Payment Details: </div>

                        </div>--%>
                        <div class="row ">
                            <div class="col-lg-12 col-md-12 col-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvPayPlanDetail" runat="server" CssClass="table table-striped" BorderStyle="None"  DataKeyNames="CLT_ID,BANK_ID,CRR_ID" AutoGenerateColumns="False" EnableModelValidation="True" ShowFooter="False">
                                        <Columns>

                                            <asp:TemplateField HeaderText="Id" Visible="false">
                                                <HeaderTemplate >Id</HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="1%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CHQDT"  HeaderStyle-CssClass="text-left"  ItemStyle-CssClass="text-left" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date" />
                                            <asp:TemplateField HeaderText="Payment Mode">
                                                <HeaderTemplate>Payment Mode</HeaderTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PAYMODE") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("PAYMODE") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterStyle HorizontalAlign="Right" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount">
                                                   <HeaderTemplate >Amount</HeaderTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("AMOUNT") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPayAmt" runat="server" Text='<%# Bind("AMOUNT", "{0:0.00}")%>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterStyle HorizontalAlign="Right" />
                                                <HeaderStyle HorizontalAlign="Right" CssClass="text-right" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>

                                        </Columns>

                                    </asp:GridView>
                                </div>
                            </div>

                        </div>

                        <div class="row " id="id_finalComments" runat="server" visible="false">
                            <div class="col-12">
                                <asp:Label ID="lblFinalComments" runat="server" class="field-label"></asp:Label>
                            </div>
                        </div>

                        <div class="row " id="id_fin_cmnts" runat="server" visible="false">
                            <div class="col-4 border">Comments:</div>
                            <div class="col-8">
                                <asp:TextBox ID="txt_fin_comments" runat="server" TextMode="MultiLine" TabIndex="13"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row" id="id_princ_cmnts" runat="server" visible="false">
                            <div class="col-12">Comments: </div>
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <%--      <asp:TextBox ID="" runat="server" TextMode="MultiLine" TabIndex="13"></asp:TextBox>--%>
                                <textarea id="txt_princ_comments" rows="3" style="width: 100%;" runat="server" />

                            </div>
                        </div>
                        <div class="row" id="id_cashier" runat="server" visible="false">
                            <div class="col-4 border">Comments:</div>
                            <div class="col-8">
                                <asp:TextBox ID="txt_cashier_cmnts" runat="server" TextMode="MultiLine" TabIndex="13"></asp:TextBox>
                            </div>
                        </div>

                        <div class="spacer5"></div>
                        <div class="row mt-1">
                            <div class="col-lg-12 text-center">
                                <asp:Button ID="btnApprove" CssClass="button btn-approve" runat="server" Text="Approve" />
                                <asp:Button ID="btnReject" CssClass="button btn-reject" runat="server" Text="Reject" />
                                <asp:Button ID="btnRevert" CssClass="button btn-reject" runat="server" Text="Revert" Visible="false" />
                            </div>
                        </div>
                        <div class="spacer5"></div>
                    </div>
                </div>
                <div id="divNote" runat="server" title="Click on the message box to drag it up and down" visible="false" clientidmode="Static">
                    <span class="msgInfoclose">
                        <asp:Button ID="btn" type="button" ValidationGroup="none" runat="server" Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid white; border-radius: 10px 10px; background-color: white;"
                            ForeColor="White" Text="X"
                            CausesValidation="false"></asp:Button>
                    </span>
                    <asp:Label ID="lblError" runat="server" EnableViewState="false"></asp:Label>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>



        <asp:UpdateProgress ID="upProgGv" runat="server">
            <ProgressTemplate>
                <asp:Panel ID="pnlProgress" runat="server">
                    <br />
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading1.gif" /><br />
                    Please Wait....
                </asp:Panel>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="avcProgress" runat="server" HorizontalOffset="10"
                    HorizontalSide="Center" ScrollEffectDuration=".1" TargetControlID="pnlProgress"
                    VerticalOffset="10" VerticalSide="Middle">
                </ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>

    </form>
</body>
</html>
