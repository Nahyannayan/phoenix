Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Imports System.Xml
Imports System.Xml.Xsl
Imports GemBox.Spreadsheet
Imports Lesnikowski.Barcode
Partial Class Common_ListReportView_Fees
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property ConnectionString() As String
        Get
            Return ViewState("ConnectionString")
        End Get
        Set(ByVal value As String)
            ViewState("ConnectionString") = value
        End Set
    End Property
    Private Property FloatColIndex() As String
        Get
            Return ViewState("FloatColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("FloatColIndex") = value
        End Set
    End Property
    Private Property FilterString() As String
        Get
            Return ViewState("FilterString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterString") = value
        End Set
    End Property
    Private Property DateColIndex() As String
        Get
            Return ViewState("DateColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("DateColIndex") = value
        End Set
    End Property
    Private Property SQLSelect() As String
        Get
            Return ViewState("SQLSelect")
        End Get
        Set(ByVal value As String)
            ViewState("SQLSelect") = value
        End Set
    End Property
    Private Property SortColString() As String
        Get
            Return ViewState("SortColString")
        End Get
        Set(ByVal value As String)
            ViewState("SortColString") = value
        End Set
    End Property
    Private Property FilterTextboxString() As String
        Get
            Return ViewState("FilterTextboxString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterTextboxString") = value
        End Set
    End Property
    Private Property EditPagePath() As String
        Get
            Return ViewState("EditPagePath")
        End Get
        Set(ByVal value As String)
            ViewState("EditPagePath") = value
        End Set
    End Property
    Private Property MainMenuCode() As String
        Get
            Return ViewState("MainMenuCode")
        End Get
        Set(ByVal value As String)
            ViewState("MainMenuCode") = value
        End Set
    End Property
    Private Property Datamode() As String
        Get
            Return ViewState("Datamode")
        End Get
        Set(ByVal value As String)
            ViewState("Datamode") = value
        End Set
    End Property
    Private Property HeaderTitle() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            Page.Title = value
            lblTitle.Text = value
        End Set
    End Property
    Private Property ShowGridCheckBox() As Boolean
        Get
            Return ViewState("ShowGridCheckBox")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ShowGridCheckBox") = value
        End Set
    End Property
    Private Property NoAddingAllowed() As Boolean
        Get
            Return ViewState("NoAddingAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoAddingAllowed") = value
        End Set
    End Property
    Private Property NoViewAllowed() As Boolean
        Get
            Return ViewState("NoViewAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoViewAllowed") = value
        End Set
    End Property
    Private Property HideExportButton() As Boolean
        Get
            Return ViewState("HideExportButton")
        End Get
        Set(ByVal value As Boolean)
            ViewState("HideExportButton") = value
        End Set
    End Property
    Private Property IsStoredProcedure() As Boolean
        Get
            Return ViewState("IsStoredProcedure")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsStoredProcedure") = value
        End Set
    End Property
    Private Property SPParam() As SqlParameter()
        Get
            Return Session("SPParam")
        End Get
        Set(ByVal value As SqlParameter())
            Session("SPParam") = value
        End Set
    End Property
    Private Property AllRecordsIDs() As String
        Get
            Return ViewState("AllRecordsIDs")
        End Get
        Set(ByVal value As String)
            ViewState("AllRecordsIDs") = value
        End Set
    End Property
    Private Property FilterText1() As String
        Get
            Return chkSelection1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                chkSelection1.Text = value
                chkSelection1.Visible = True
            Else
                chkSelection1.Visible = False
                chkSelection1.Text = ""
            End If
        End Set
    End Property
    Private Property FilterText2() As String
        Get
            Return chkSelection2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                chkSelection2.Text = value
                chkSelection2.Visible = True
            Else
                chkSelection2.Visible = False
                chkSelection2.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox1Text() As String
        Get
            Return rad1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad1.Text = value
                rad1.Visible = True
            Else
                rad1.Visible = False
                rad1.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox2Text() As String
        Get
            Return rad2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad2.Text = value
                rad2.Visible = True
            Else
                rad2.Visible = False
                rad2.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox3Text() As String
        Get
            Return rad3.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad3.Text = value
                rad3.Visible = True
            Else
                rad3.Visible = False
                rad3.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox4Text() As String
        Get
            Return rad4.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad4.Text = value
                rad4.Visible = True
            Else
                rad4.Visible = False
                rad4.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox5Text() As String
        Get
            Return rad5.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad5.Text = value
                rad5.Visible = True
            Else
                rad5.Visible = False
                rad5.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox6Text() As String
        Get
            Return rad6.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad6.Text = value
                rad6.Visible = True
            Else
                rad6.Visible = False
                rad6.Text = ""
            End If
        End Set
    End Property
    Private Property ButtonText1() As String
        Get
            Return ListButton1.Value
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                ListButton1.Value = value
                ListButton1.Visible = True
            Else
                ListButton1.Visible = False
                ListButton1.Value = ""
            End If
        End Set
    End Property
    Private Property ButtonText2() As String
        Get
            Return ListButton2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                ListButton2.Text = value
                ListButton2.Visible = True
            Else
                ListButton2.Visible = False
                ListButton2.Text = ""
            End If
        End Set
    End Property

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If Session("sModule") = "SS" Then
                Me.MasterPageFile = "../mainMasterPageSS.master"
            Else
                Me.MasterPageFile = "../mainMasterPage.master"
            End If
        Catch ex As Exception

        End Try
    End Sub
    ' Dim SPParam(10) As SqlParameter
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                gvDetails.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_9.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_10.Value = "LI__../Images/operations/like.gif"

                MainMenuCode = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    Datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                Dim msgText As String
                If Not Request.QueryString("msgText") Is Nothing And Request.QueryString("msgText") <> "" Then
                    msgText = Encr_decrData.Decrypt(Request.QueryString("msgText").Replace(" ", "+"))
                    '   lblError.Text = msgText
                    usrMessageBar.ShowNotification(msgText, UserControls_usrMessageBar.WarningType.Danger)
                End If
                ShowGridCheckBox = False
                NoAddingAllowed = False
                NoViewAllowed = False
                HideExportButton = False
                IsStoredProcedure = False
                ButtonText1 = ""
                ButtonText2 = ""
                rad1.Checked = True
                CheckBox1Text = ""
                CheckBox2Text = ""
                CheckBox3Text = ""
                CheckBox4Text = ""
                CheckBox5Text = ""
                CheckBox6Text = ""
                FilterText1 = ""
                FilterText2 = ""
                BuildListView()
                gridbind(False)
                btnExport.Visible = Not HideExportButton
                If NoAddingAllowed Then
                    hlAddNew.Visible = False
                Else
                    hlAddNew.Visible = True
                    Dim url As String
                    url = EditPagePath & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    hlAddNew.NavigateUrl = url
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(1, str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(2, str_Sid_img(2))
        str_Sid_img = h_selected_menu_3.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(3, str_Sid_img(2))
        str_Sid_img = h_selected_menu_4.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(4, str_Sid_img(2))
        str_Sid_img = h_selected_menu_5.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(5, str_Sid_img(2))
        str_Sid_img = h_selected_menu_6.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(6, str_Sid_img(2))
        str_Sid_img = h_selected_menu_7.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(7, str_Sid_img(2))
        str_Sid_img = h_selected_menu_8.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(8, str_Sid_img(2))
        str_Sid_img = h_selected_menu_9.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(9, str_Sid_img(2))
        str_Sid_img = h_selected_menu_10.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(10, str_Sid_img(2))

    End Sub
    Public Sub setID(ByVal ColumnNo As Int16, Optional ByVal p_imgsrc As String = "")
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvDetails.HeaderRow.FindControl("mnu_" & ColumnNo.ToString & "_img")
                If s Is Nothing Then Exit Sub
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvDetails.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function SetDTFilterCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function getColumnFilterString(ByVal FilterTypeString As String, ByVal GridViewHeaderRow As GridViewRow, ByVal ControlName As String, ByVal ColumnName As String) As String
        Try
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            txtSearch = GridViewHeaderRow.FindControl(ControlName)
            Dim str_Filter As String = ""
            larrSearchOpr = FilterTypeString.Split("__")
            lstrOpr = larrSearchOpr(0)
            str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            If Not IsStoredProcedure Then
                str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            Else
                str_Filter = SetDTFilterCondn(lstrOpr, ColumnName, txtSearch.Text)
            End If
            getColumnFilterString = str_Filter
            FilterTextboxString &= ControlName & "=" & txtSearch.Text & "||"
        Catch ex As Exception
            getColumnFilterString = ""
        End Try
    End Function
    Private Sub SetHeaderFilterString()
        Try
            Dim ColStrFilter(), ColValue(), iVal As String
            ColStrFilter = FilterTextboxString.Split("||")
            Dim iTxtBox As TextBox = Nothing
            For Each iVal In ColStrFilter
                ColValue = iVal.Split("=")
                If ColValue.Length > 0 Then
                    If gvDetails.HeaderRow.FindControl(ColValue(0)) IsNot Nothing Then
                        iTxtBox = CType(gvDetails.HeaderRow.FindControl(ColValue(0)), TextBox)
                    End If
                    If ColValue.Length > 1 And iTxtBox IsNot Nothing Then
                        iTxtBox.Text = ColValue(1)
                    End If
                End If

            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub gridbind(ByVal NoReset As Boolean, Optional ByVal ForceResetColumns As Boolean = False)
        Try
            Dim GridData As New DataTable
            Dim lblID As New Label
            Dim lblName As New Label
            Dim str_Filter As String = ""
            Dim i As Int16
            Dim ColName As String
            FilterTextboxString = ""
            If gvDetails.Rows.Count > 0 And Not ForceResetColumns Then
                If ViewState("GridTable").Columns.count > 0 Then str_Filter &= getColumnFilterString(h_selected_menu_1.Value, gvDetails.HeaderRow, "txtCol1", ViewState("GridTable").Columns(0).ColumnName)
                If ViewState("GridTable").Columns.count > 1 Then str_Filter &= getColumnFilterString(h_Selected_menu_2.Value, gvDetails.HeaderRow, "txtCol2", ViewState("GridTable").Columns(1).ColumnName)
                If ViewState("GridTable").Columns.count > 2 Then str_Filter &= getColumnFilterString(h_selected_menu_3.Value, gvDetails.HeaderRow, "txtCol3", ViewState("GridTable").Columns(2).ColumnName)
                If ViewState("GridTable").Columns.count > 3 Then str_Filter &= getColumnFilterString(h_selected_menu_4.Value, gvDetails.HeaderRow, "txtCol4", ViewState("GridTable").Columns(3).ColumnName)
                If ViewState("GridTable").Columns.count > 4 Then str_Filter &= getColumnFilterString(h_selected_menu_5.Value, gvDetails.HeaderRow, "txtCol5", ViewState("GridTable").Columns(4).ColumnName)
                If ViewState("GridTable").Columns.count > 5 Then str_Filter &= getColumnFilterString(h_selected_menu_6.Value, gvDetails.HeaderRow, "txtCol6", ViewState("GridTable").Columns(5).ColumnName)
                If ViewState("GridTable").Columns.count > 6 Then str_Filter &= getColumnFilterString(h_selected_menu_7.Value, gvDetails.HeaderRow, "txtCol7", ViewState("GridTable").Columns(6).ColumnName)
                If ViewState("GridTable").Columns.count > 7 Then str_Filter &= getColumnFilterString(h_selected_menu_8.Value, gvDetails.HeaderRow, "txtCol8", ViewState("GridTable").Columns(7).ColumnName)
                If ViewState("GridTable").Columns.count > 8 Then str_Filter &= getColumnFilterString(h_selected_menu_9.Value, gvDetails.HeaderRow, "txtCol9", ViewState("GridTable").Columns(8).ColumnName)
                If ViewState("GridTable").Columns.count > 9 Then str_Filter &= getColumnFilterString(h_selected_menu_10.Value, gvDetails.HeaderRow, "txtCol10", ViewState("GridTable").Columns(9).ColumnName)
            End If
            Dim OrderByStr As String = ""
            If SortColString <> "" Then
                OrderByStr = "order by " & SortColString
            Else
                OrderByStr = "order by  1"
            End If
            If ddlFilter.Visible Then
                If ddlFilter.SelectedItem Is Nothing Then
                    SQLSelect = SQLSelect.Replace("~", " top 10 ")
                ElseIf ddlFilter.SelectedItem.Value = "All" Then
                    SQLSelect = SQLSelect.Replace("~", " top 99999999 ")
                Else
                    SQLSelect = SQLSelect.Replace("~", " top " & ddlFilter.SelectedItem.Value)
                End If
            End If
            If Not IsStoredProcedure Then
                GridData = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQLSelect & str_Filter & OrderByStr).Tables(0)
            Else
                GridData = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, SQLSelect, CType(SPParam, SqlParameter())).Tables(0)
                str_Filter = " 1 = 1 " & str_Filter
                GridData.Select(str_Filter)
                Dim mySelectDT As New DataTable
                Dim mRow As DataRow
                mySelectDT = GridData.Copy
                mySelectDT.Rows.Clear()
                For Each mRow In GridData.Select(str_Filter)
                    mySelectDT.ImportRow(mRow)
                Next
                GridData = mySelectDT.Copy()
            End If

            Dim bindDT, myData As New DataTable
            bindDT = GridData.Copy
            myData = GridData.Copy

            Try
                Dim mRow As DataRow
                AllRecordsIDs = ""
                If myData.Columns.Count > 0 Then
                    For Each mRow In myData.Rows
                        AllRecordsIDs &= IIf(AllRecordsIDs <> "", "|", "") & mRow(0).ToString
                    Next
                End If
            Catch ex As Exception

            End Try


            If myData.Columns.Count > 0 Then
                myData.Columns.RemoveAt(0)
            End If
            ViewState("GridTable") = GridData
            Session("myData") = myData
            SetExportFileLink()
            GridData.Rows.Clear()
            Dim HeaderLabel As New Label
            Dim DColIndex, FColIndex As String
            FColIndex = "" : DColIndex = ""
            For i = 1 To GridData.Columns.Count
                ColName = "Col" & i.ToString
                bindDT.Columns(i - 1).ColumnName = ColName
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Int32") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Decimal") Then
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Center
                    If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Then
                        FColIndex &= "|" & (i).ToString
                    End If
                Else
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Center
                End If
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.DateTime") Then
                    DColIndex &= "|" & (i).ToString
                End If
            Next
            FloatColIndex = FColIndex
            DateColIndex = DColIndex
            If bindDT.Columns.Count < 10 Then
                For i = 0 To bindDT.Columns.Count
                    gvDetails.Columns(i).Visible = True
                Next
                For i = bindDT.Columns.Count + 1 To 10
                    ColName = "Col" & i.ToString
                    bindDT.Columns.Add(ColName, System.Type.GetType("System.String"))
                    gvDetails.Columns(i).Visible = False
                Next
            End If
            gvDetails.Columns(0).Visible = ShowGridCheckBox
            gvDetails.Columns(11).Visible = Not NoAddingAllowed
            gvDetails.Columns(1).Visible = False
            gvDetails.Columns(11).Visible = Not NoViewAllowed
            gvDetails.DataSource = bindDT
            If bindDT.Rows.Count = 0 Then
                bindDT.Rows.Add(bindDT.NewRow())
                gvDetails.DataBind()
                Dim columnCount As Integer = gvDetails.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvDetails.Rows(0).Cells.Clear()
                gvDetails.Rows(0).Cells.Add(New TableCell)
                gvDetails.Rows(0).Cells(0).ColumnSpan = columnCount
                gvDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvDetails.DataBind()
            End If

            For i = 1 To GridData.Columns.Count
                HeaderLabel = gvDetails.HeaderRow.FindControl("lblHeaderCol" & i)
                If Not HeaderLabel Is Nothing Then
                    HeaderLabel.Text = GridData.Columns(i - 1).ColumnName
                End If
            Next
            SetHeaderFilterString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            Dim lblItemColID As Label
            lblItemColID = TryCast(e.Row.FindControl("lblItemCol1"), Label)
            Dim cmdCol As Integer = gvDetails.Columns.Count - 1
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblItemColID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                If (MainMenuCode = "PI02008" Or MainMenuCode = "PI02025" Or MainMenuCode = "T200142" Or MainMenuCode = "T200216" Or MainMenuCode = "T200219") And rad1.Checked Then '-----------------------------------
                    hlview.NavigateUrl = EditPagePath & "?viewid=" & Encr_decrData.Encrypt(lblItemColID.Text) & "&MainMnu_code=" & Encr_decrData.Encrypt(MainMenuCode) & "&datamode=" & Encr_decrData.Encrypt("add")
                ElseIf MainMenuCode = "F300138" Or MainMenuCode = "F300204" Then
                    hlview.NavigateUrl = EditPagePath & "?viewid=" & Encr_decrData.Encrypt(lblItemColID.Text) & "&MainMnu_code=" & Encr_decrData.Encrypt(MainMenuCode) & "&datamode=" & Encr_decrData.Encrypt("approve")
                Else
                    hlview.NavigateUrl = EditPagePath & "?viewid=" & Encr_decrData.Encrypt(lblItemColID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                End If
            End If
            Try
                Dim FloatColIndexArr(), i, ControlName As String
                FloatColIndexArr = FloatColIndex.Split("|")
                Dim lblItemCol As Label
                For Each i In FloatColIndexArr
                    If IsNumeric(i) Then
                        ControlName = "lblItemCol" & i.ToString
                        lblItemCol = TryCast(e.Row.FindControl(ControlName), Label)
                        If Not lblItemCol Is Nothing Then
                            If IsNumeric(lblItemCol.Text) Then
                                lblItemCol.Text = Convert.ToDouble(lblItemCol.Text).ToString("####0.00")
                            End If
                        End If
                    End If
                Next
                Dim DateColIndexArr() As String
                DateColIndexArr = DateColIndex.Split("|")
                For Each i In DateColIndexArr
                    If IsNumeric(i) Then
                        ControlName = "lblItemCol" & i.ToString
                        lblItemCol = TryCast(e.Row.FindControl(ControlName), Label)
                        If Not lblItemCol Is Nothing Then
                            If IsDate(lblItemCol.Text) Then
                                lblItemCol.Text = CDate(lblItemCol.Text).ToString("dd/MMM/yyyy")
                            End If
                        End If
                    End If
                Next

            Catch ex As Exception

            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind(False)
    End Sub
    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        RememberOldValues()
        gvDetails.PageIndex = e.NewPageIndex
        gridbind(True)
        SetChk(Me.Page)
        RePopulateValues()

    End Sub
    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt(lblTitle.Text)
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub rad_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rad1.CheckedChanged, rad2.CheckedChanged, rad3.CheckedChanged, rad4.CheckedChanged, rad5.CheckedChanged, rad6.CheckedChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            ' lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Protected Sub chkSelection_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelection1.CheckedChanged, chkSelection2.CheckedChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            ' lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try

        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            Dim MySelectedIDS() As String
            Dim itmIdList As New ArrayList()
            If chkSelectAll.Checked Then
                MySelectedIDS = AllRecordsIDs.Split("|")
                If MySelectedIDS.Length > 0 Then
                    Dim MySelectedID As String
                    For Each MySelectedID In MySelectedIDS
                        If Not itmIdList.Contains(MySelectedID) Then
                            itmIdList.Add(MySelectedID)
                        End If
                    Next
                End If
            End If
            ViewState("checked_items") = itmIdList
            Dim chkControl As New HtmlInputCheckBox
            For Each grow As GridViewRow In gvDetails.Rows
                chkControl = grow.FindControl("chkControl")
                chkControl.Checked = chkSelectAll.Checked
            Next
        Catch ex As Exception
            ' lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Sub

    Protected Sub ListButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkControl As New HtmlInputCheckBox
        Dim chkControl1 As New HtmlInputCheckBox
        Dim lblID As Label
        Dim IDs As String = ""

        Dim lblID1 As Label
        Dim IDs1 As String = ""

        Dim SelectedIds As String = ""
        For Each grow As GridViewRow In gvDetails.Rows
            chkControl1 = grow.FindControl("chkControl")
            If chkControl1 IsNot Nothing Then
                If chkControl1.Checked Then
                    lblID1 = TryCast(grow.FindControl("lblItemCol1"), Label)
                    IDs1 &= IIf(IDs <> "", "|", "") & lblID1.Text
                End If
            End If
        Next


        If IDs1 <> "" Then
            RememberOldValues()
        End If


        If ViewState("checked_items") IsNot Nothing Then
            For i As Integer = 0 To ViewState("checked_items").Count - 1
                If ViewState("checked_items")(i).ToString.Length > 0 Then SelectedIds &= IIf(SelectedIds <> "", "|", "") & ViewState("checked_items")(i).ToString
            Next
        End If

        For Each grow As GridViewRow In gvDetails.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl IsNot Nothing Then
                If chkControl.Checked Then
                    lblID = TryCast(grow.FindControl("lblItemCol1"), Label)
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                    If Not ("|" & SelectedIds.ToString & "|").ToString.Contains("|" & lblID.Text.ToString & "|") Then
                        SelectedIds &= IIf(SelectedIds <> "", "|", "") & lblID.Text
                    End If
                End If
            End If
        Next
        If IDs = "" And SelectedIds = "" Then
            'lblError.Text = "No Item Selected !!!"
            usrMessageBar.ShowNotification("No Item Selected !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMenuCode
            Case "F300172"
                'PrintBarcode(IDs)
            Case "F300172"
                'PrintBarcode(IDs)
            Case "F351090"
                ApproveRejectCancelReceiptRequests(SelectedIds, "FEE", "Approve")
            Case "F351091"
                ApproveRejectCancelReceiptRequests(SelectedIds, "OTH", "Approve")
        End Select
    End Sub
    Private Sub ApproveRejectCancelReceiptRequests(ByVal Ids As String, ByVal TType As String, ByVal Action As String)
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        Dim trans As SqlTransaction
        Try
            conn.Open()
            Dim cmd As New SqlCommand("FEES.APPROVE_RECEIPT_CANCELLATION", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpFRCA_IDs As New SqlParameter("@FRCA_IDs", SqlDbType.VarChar, 500)
            sqlpFRCA_IDs.Value = Ids
            cmd.Parameters.Add(sqlpFRCA_IDs)

            Dim sqlpUSER As New SqlParameter("@USER", SqlDbType.VarChar, 50)
            sqlpUSER.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpUSER)

            Dim sqlpLBsuID As New SqlParameter("@LogINBSU_ID", SqlDbType.VarChar, 10)
            sqlpLBsuID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpLBsuID)

            Dim sqlpTtype As New SqlParameter("@TType", SqlDbType.VarChar, 10)
            sqlpTtype.Value = TType
            cmd.Parameters.Add(sqlpTtype)

            Dim sqlpAction As New SqlParameter("@ACTION", SqlDbType.VarChar, 10)
            sqlpAction.Value = Action
            cmd.Parameters.Add(sqlpAction)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            Dim iReturnvalue As Integer = retSValParam.Value
            If iReturnvalue <> 0 Then

                '   lblError.Text = "Error occured while deleting : " & UtilityObj.getErrorMessage(iReturnvalue)
                usrMessageBar.ShowNotification("Error occured while deleting : " & UtilityObj.getErrorMessage(iReturnvalue), UserControls_usrMessageBar.WarningType.Danger)
            Else
                ' lblError.Text = "Receipt Cancellation approved Successfully.."
                usrMessageBar.ShowNotification("Receipt Cancellation approved Successfully..", UserControls_usrMessageBar.WarningType.Success)
                gridbind(True)

            End If

        Catch ex As Exception
            ' lblError.Text = "Error occured while deleting  " & ex.Message
            usrMessageBar.ShowNotification("Error occured while deleting  " & ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub


    Protected Sub ListButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListButton2.Click
        Dim chkControl As New HtmlInputCheckBox
        Dim lblID As Label
        Dim IDs As String = ""
        For Each grow As GridViewRow In gvDetails.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl.Checked Then
                lblID = TryCast(grow.FindControl("lblItemCol1"), Label)
                IDs &= IIf(IDs <> "", "|", "") & lblID.Text
            End If
        Next

        Dim SelectedIds As String = ""
        RememberOldValues()
        If ViewState("checked_items") IsNot Nothing Then
            For i As Integer = 0 To ViewState("checked_items").Count - 1
                If ViewState("checked_items")(i).ToString.Length > 0 Then SelectedIds &= IIf(SelectedIds <> "", "|", "") & ViewState("checked_items")(i).ToString
            Next
        End If


        If IDs = "" Then
            ' lblError.Text = "No Item Selected !!!"
            usrMessageBar.ShowNotification("No Item Selected !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMenuCode

            Case "P153067"
                IDs &= IIf(IDs <> "", "|", "") & SelectedIds
                'PrintPasswords(IDs)
            Case "F351090"
                ApproveRejectCancelReceiptRequests(SelectedIds, "FEE", "Reject")
            Case "F351091"
                ApproveRejectCancelReceiptRequests(SelectedIds, "OTH", "Reject")
        End Select
    End Sub


    Private Sub BuildListView()
        Try
            Dim strConn, StrSortCol As String
            Dim StrSQL As New StringBuilder
            strConn = "" : StrSortCol = ""
            StrSQL.Append("")
            strConn = ConnectionManger.GetOASISConnection.ConnectionString
            Dim SearchMode As String = Request.QueryString("id")
            Select Case MainMenuCode
                Case "F300172" 'Fees
                    EditPagePath = "FeeGenerateAdvanceTaxInvoice.aspx"
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    StrSortCol = ""
                    HeaderTitle = "Generate Advance Tax Invoice"
                    IsStoredProcedure = True
                    StrSQL.Append("FEES.GET_BULK_ADVANCE_TAX_INVOICE_LIST")
                    ReDim SPParam(1)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    NoViewAllowed = True
                Case "F300173" 'Transport
                    StrSQL.Append("")
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = ""
                    HeaderTitle = "Generate Advance Tax Invoice"
                    EditPagePath = "FeeGenerateAdvanceTaxInvoice.aspx"

                Case "F351090" 'Fee Receipt Cancellation Approval
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    lblError.Text = ""
                    If rad1.Checked Then
                        IsStoredProcedure = True
                        StrSQL.Append("FEES.GET_RECEIPT_CANCELLATION_LIST")
                        ReDim SPParam(2)
                        SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                        SPParam(1) = Mainclass.CreateSqlParameter("@FRCA_TYPE", "FEE", SqlDbType.VarChar)
                        SPParam(2) = Mainclass.CreateSqlParameter("@FRCA_STATUS", "N", SqlDbType.VarChar)
                        ButtonText1 = "Approve"
                        ButtonText2 = "Reject"
                        ShowGridCheckBox = True
                        NoAddingAllowed = True
                        HeaderTitle = "Pending Fee Receipt Cancellation List"
                    ElseIf rad2.Checked Then
                        IsStoredProcedure = True
                        StrSQL.Append("FEES.GET_RECEIPT_CANCELLATION_LIST")
                        ReDim SPParam(2)
                        SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                        SPParam(1) = Mainclass.CreateSqlParameter("@FRCA_TYPE", "FEE", SqlDbType.VarChar)
                        SPParam(2) = Mainclass.CreateSqlParameter("@FRCA_STATUS", "A", SqlDbType.VarChar)
                        NoAddingAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        ButtonText2 = ""
                        HeaderTitle = "Approved Fee Receipt Cancellation List"
                    Else
                        IsStoredProcedure = True
                        StrSQL.Append("FEES.GET_RECEIPT_CANCELLATION_LIST")
                        ReDim SPParam(2)
                        SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                        SPParam(1) = Mainclass.CreateSqlParameter("@FRCA_TYPE", "FEE", SqlDbType.VarChar)
                        SPParam(2) = Mainclass.CreateSqlParameter("@FRCA_STATUS", "R", SqlDbType.VarChar)
                        NoAddingAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        ButtonText2 = ""
                        HeaderTitle = "Rejected Fee Receipt Cancellation List"

                    End If
                    NoViewAllowed = True
                Case "F351091" 'Other Receipt Cancellation Approval
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    lblError.Text = ""
                    If rad1.Checked Then
                        IsStoredProcedure = True
                        StrSQL.Append("FEES.GET_RECEIPT_CANCELLATION_LIST")
                        ReDim SPParam(2)
                        SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                        SPParam(1) = Mainclass.CreateSqlParameter("@FRCA_TYPE", "OTH", SqlDbType.VarChar)
                        SPParam(2) = Mainclass.CreateSqlParameter("@FRCA_STATUS", "N", SqlDbType.VarChar)
                        ButtonText1 = "Approve"
                        ButtonText2 = "Reject"
                        ShowGridCheckBox = True
                        NoAddingAllowed = True
                        HeaderTitle = "Pending Other Receipt CollReceipt Cancellation List"
                    ElseIf rad2.Checked Then
                        IsStoredProcedure = True
                        StrSQL.Append("FEES.GET_RECEIPT_CANCELLATION_LIST")
                        ReDim SPParam(2)
                        SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                        SPParam(1) = Mainclass.CreateSqlParameter("@FRCA_TYPE", "OTH", SqlDbType.VarChar)
                        SPParam(2) = Mainclass.CreateSqlParameter("@FRCA_STATUS", "A", SqlDbType.VarChar)
                        NoAddingAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        ButtonText2 = ""
                        HeaderTitle = "Approved Other Receipt Cancellation List"
                    Else
                        IsStoredProcedure = True
                        StrSQL.Append("FEES.GET_RECEIPT_CANCELLATION_LIST")
                        ReDim SPParam(2)
                        SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                        SPParam(1) = Mainclass.CreateSqlParameter("@FRCA_TYPE", "OTH", SqlDbType.VarChar)
                        SPParam(2) = Mainclass.CreateSqlParameter("@FRCA_STATUS", "R", SqlDbType.VarChar)
                        NoAddingAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        ButtonText2 = ""
                        HeaderTitle = "Rejected Other Receipt Cancellation List"

                    End If
                    NoViewAllowed = True
                Case "F300137" 'PDC cancellation Fees
                    StrSQL.Append("SELECT ID, [Doc No],[Doc Date],[StudentId],[Name],[Receipt No],[Cheque Nos],[Status] FROM " & _
                        "(SELECT FPC_ID ID, FPC_ID [Doc No], FPC_DATE [Doc Date],ST.STU_NO [StudentId],ST.NAME [Name],FC.FCL_RECNO [Receipt No], " & _
                        "CASE ISNULL(H.FPC_STATUS ,'N') WHEN 'N' THEN 'PENDING' WHEN 'A' THEN 'APPROVED' WHEN 'D' THEN 'DELETED' END [Status] ," & _
                        "STUFF(( SELECT  ',' + FCQ_CHQNO FROM FEES.FEE_COLL_PDC_CANCEL_D AS CD WITH ( NOLOCK ) " & _
                        "INNER JOIN OASIS.FEES.FEECOLLCHQUES_D WITH ( NOLOCK ) ON FPC_BSU_ID = FCQ_BSU_ID AND CD.FPCD_FCQ_ID = FCQ_ID WHERE CD.FPCD_FPC_ID = H.FPC_ID " & _
                        "FOR XML PATH('')), 1, 1, '') AS [Cheque Nos] FROM FEES.FEE_COLL_PDC_CANCEL_H AS H WITH ( NOLOCK ) INNER JOIN dbo.STUDENTS AS ST ON H.FPC_STU_ID=ST.STU_ID " & _
                        "INNER JOIN FEES.FEECOLLECTION_H AS FC WITH(NOLOCK) ON FC.FCL_BSU_ID=H.FPC_BSU_ID AND FC.FCL_ID=H.FPC_FCL_ID WHERE ISNULL(H.FPC_bDELETED,0)=0 AND H.FPC_BSU_ID='" & Session("sBsuId") & "' ) AS BB WHERE 1=1 ")
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    StrSortCol = "[Doc Date]"
                    HeaderTitle = "Collection PDC cancellation"
                    EditPagePath = "../Fees/FeePDCCancellation.aspx"
                Case "F300138" 'PDC cancellation Fees - Approval
                    StrSQL.Append("SELECT ID, [Doc No],[Doc Date],[StudentId],[Name],[Receipt No],[Cheque Nos],[Status] FROM " & _
                        "(SELECT FPC_ID ID, FPC_ID [Doc No], FPC_DATE [Doc Date],ST.STU_NO [StudentId],ST.NAME [Name],FC.FCL_RECNO [Receipt No], " & _
                        "CASE ISNULL(H.FPC_STATUS ,'N') WHEN 'N' THEN 'PENDING' WHEN 'A' THEN 'APPROVED' WHEN 'D' THEN 'DELETED' END [Status] ," & _
                        "STUFF(( SELECT  ',' + FCQ_CHQNO FROM FEES.FEE_COLL_PDC_CANCEL_D AS CD WITH ( NOLOCK ) " & _
                        "INNER JOIN OASIS.FEES.FEECOLLCHQUES_D WITH ( NOLOCK ) ON FPC_BSU_ID = FCQ_BSU_ID AND CD.FPCD_FCQ_ID = FCQ_ID WHERE CD.FPCD_FPC_ID = H.FPC_ID " & _
                        "FOR XML PATH('')), 1, 1, '') AS [Cheque Nos] FROM FEES.FEE_COLL_PDC_CANCEL_H AS H WITH ( NOLOCK ) INNER JOIN dbo.STUDENTS AS ST ON H.FPC_STU_ID=ST.STU_ID " & _
                        "INNER JOIN FEES.FEECOLLECTION_H AS FC WITH(NOLOCK) ON FC.FCL_BSU_ID=H.FPC_BSU_ID AND FC.FCL_ID=H.FPC_FCL_ID WHERE ISNULL(H.FPC_bDELETED,0)=0 AND H.FPC_BSU_ID='" & Session("sBsuId") & "' AND ISNULL(H.FPC_STATUS ,'N')='N' ) AS BB WHERE 1=1 ")
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    StrSortCol = "[Doc Date]"
                    HeaderTitle = "Collection PDC cancellation"
                    EditPagePath = "../Fees/FeePDCCancellation.aspx"
                Case "F300203" 'PDC cancellation Transport
                    StrSQL.Append("SELECT ID, [Doc No],[Doc Date],[School],[StudentId],[Name],[Receipt No],[Cheque Nos],[Status] FROM " & _
                        "(SELECT FPC_ID ID, FPC_ID [Doc No], FPC_DATE [Doc Date],ST.STU_NO [StudentId],ST.NAME [Name],FC.FCL_RECNO [Receipt No], " & _
                        "CASE ISNULL(H.FPC_STATUS ,'N') WHEN 'N' THEN 'PENDING' WHEN 'A' THEN 'APPROVED' WHEN 'D' THEN 'DELETED' END [Status] ," & _
                        "STUFF(( SELECT  ',' + FCQ_CHQNO FROM FEES.FEE_COLL_PDC_CANCEL_D AS CD WITH ( NOLOCK ) " & _
                        "INNER JOIN OASIS.FEES.FEECOLLCHQUES_D WITH ( NOLOCK ) ON FPC_BSU_ID = FCQ_BSU_ID AND CD.FPCD_FCQ_ID = FCQ_ID WHERE CD.FPCD_FPC_ID = H.FPC_ID " & _
                        "FOR XML PATH('')), 1, 1, '') AS [Cheque Nos], BU.BSU_SHORTNAME AS [School] FROM FEES.FEE_COLL_PDC_CANCEL_H AS H WITH ( NOLOCK ) INNER JOIN dbo.STUDENTS AS ST WITH ( NOLOCK ) ON H.FPC_STU_ID=ST.STU_ID " & _
                        "INNER JOIN FEES.FEECOLLECTION_H AS FC WITH(NOLOCK) ON FC.FCL_BSU_ID=H.FPC_BSU_ID AND FC.FCL_ID=H.FPC_FCL_ID INNER JOIN dbo.BUSINESSUNIT_M AS BU WITH (NOLOCK) ON H.FPC_STU_BSU_ID = BU.BSU_ID " & _
                        "WHERE ISNULL(H.FPC_bDELETED,0)=0 AND H.FPC_BSU_ID='" & Session("sBsuId") & "' ) AS BB WHERE 1=1 ")
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[Doc Date]"
                    HeaderTitle = "Collection PDC cancellation"
                    EditPagePath = "../Fees/TransportPDCCancellation.aspx"
                Case "F300204" 'PDC cancellation Transport - Approval
                    StrSQL.Append("SELECT ID, [Doc No],[Doc Date],[School],[StudentId],[Name],[Receipt No],[Cheque Nos],[Status] FROM " & _
                        "(SELECT FPC_ID ID, FPC_ID [Doc No], FPC_DATE [Doc Date],ST.STU_NO [StudentId],ST.NAME [Name],FC.FCL_RECNO [Receipt No], " & _
                        "CASE ISNULL(H.FPC_STATUS ,'N') WHEN 'N' THEN 'PENDING' WHEN 'A' THEN 'APPROVED' WHEN 'D' THEN 'DELETED' END [Status] ," & _
                        "STUFF(( SELECT  ',' + FCQ_CHQNO FROM FEES.FEE_COLL_PDC_CANCEL_D AS CD WITH ( NOLOCK ) " & _
                        "INNER JOIN OASIS.FEES.FEECOLLCHQUES_D WITH ( NOLOCK ) ON FPC_BSU_ID = FCQ_BSU_ID AND CD.FPCD_FCQ_ID = FCQ_ID WHERE CD.FPCD_FPC_ID = H.FPC_ID " & _
                        "FOR XML PATH('')), 1, 1, '') AS [Cheque Nos], BU.BSU_SHORTNAME AS [School] FROM FEES.FEE_COLL_PDC_CANCEL_H AS H WITH ( NOLOCK ) INNER JOIN dbo.STUDENTS AS ST WITH ( NOLOCK ) ON H.FPC_STU_ID=ST.STU_ID " & _
                        "INNER JOIN FEES.FEECOLLECTION_H AS FC WITH(NOLOCK) ON FC.FCL_BSU_ID=H.FPC_BSU_ID AND FC.FCL_ID=H.FPC_FCL_ID INNER JOIN dbo.BUSINESSUNIT_M AS BU WITH (NOLOCK) ON H.FPC_STU_BSU_ID = BU.BSU_ID " & _
                        "WHERE ISNULL(H.FPC_bDELETED,0)=0 AND H.FPC_BSU_ID='" & Session("sBsuId") & "' AND ISNULL(H.FPC_STATUS ,'N')='N' ) AS BB WHERE 1=1 ")
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[Doc Date]"
                    HeaderTitle = "Collection PDC cancellation"
                    EditPagePath = "../Fees/TransportPDCCancellation.aspx"
            End Select
            SQLSelect = StrSQL.ToString
            ConnectionString = strConn
            SortColString = StrSortCol
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            ' lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            ' lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Private Sub RememberOldValues()
        Dim itmIdList As New ArrayList()
        Dim bindDt As DataTable
        bindDt = gvDetails.DataSource
        For Each row As GridViewRow In gvDetails.Rows
            Dim index As String = DirectCast(row.FindControl("lblItemCol1"), Label).Text

            Dim result As Boolean = DirectCast(row.FindControl("chkControl"), HtmlInputCheckBox).Checked
            If ViewState("checked_items") IsNot Nothing Then
                itmIdList = DirectCast(ViewState("checked_items"), ArrayList)
            End If
            If result Then
                If Not itmIdList.Contains(index) Then
                    itmIdList.Add(index)
                End If
            Else
                itmIdList.Remove(index)
            End If
        Next
        If itmIdList IsNot Nothing AndAlso itmIdList.Count > 0 Then
            ViewState("checked_items") = itmIdList
        End If
    End Sub

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk IsNot Nothing Then

                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Sub RePopulateValues()
        Dim itmIdList As ArrayList = DirectCast(ViewState("checked_items"), ArrayList)
        If itmIdList IsNot Nothing AndAlso itmIdList.Count > 0 Then
            For Each row As GridViewRow In gvDetails.Rows
                Dim index As String = DirectCast(row.FindControl("lblItemCol1"), Label).Text
                If itmIdList.Contains(index) Then
                    Dim myCheckBox As HtmlInputCheckBox = DirectCast(row.FindControl("chkControl"), HtmlInputCheckBox)
                    myCheckBox.Checked = True
                End If
            Next
        End If
    End Sub


End Class

