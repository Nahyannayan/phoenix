﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeChangePaymentPlan.aspx.vb" Inherits="Fees_feeChangePaymentPlan" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server" EnableViewState="false" >

<script type="text/javascript" language="javascript">

function CheckAmount(e)
    {
    var amt;
    amt=parseFloat(e.value)
    if (isNaN(amt) )
        amt= 0;
    e.value = amt.toFixed(2);
    return true;
    } 
   
</script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Set Payment Plan
        </div>
        <div class="card-body">
            <div class="table-responsive">

 <table onclick="return true;" id="tblErrortop" align="center" width="100%">
    <tr>
        <td align="left">
        <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>--%> 
            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
        </td>
    </tr>
</table>
<table align="center" width="100%">            
    
    <tr>
     <td align="left" width="20%">
         <span class="field-label">Enrollment/Enquiry</span></td>
     <td align="left" width="30%">
         <asp:RadioButton ID="rbEnrollment" runat="server" Checked="True" GroupName="mode" Text="Enrollment#" CssClass="field-label" AutoPostBack="True" TabIndex="1" />
         <asp:RadioButton ID="rbEnquiry" runat="server" CssClass="field-label" GroupName="mode" Text="Enquiry#" 
             AutoPostBack="True" Enabled="False" /></td>
         <td align="right"width="20%">
             <span class="field-label">Date</span> </td>
         <td align="left" width="30%">
         <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True" TabIndex="2" ></asp:TextBox><asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
         <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
             PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
         </ajaxToolkit:CalendarExtender>
        </td>
    </tr>    
    <tr>
        <td align="left">
             <span class="field-label">Select Student</span></td>
        <td align="left" colspan="3" >
            <uc1:usrSelStudent ID="UsrSelStudent1" runat="server" />
        </td>
    </tr>
    <tr>
         <td align="left">
             <span class="field-label">Academic year</span></td>
         <td align="left">
             <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
             </asp:DropDownList>
             <asp:Label ID="labGrade" runat="server"></asp:Label>             
             <asp:Label ID="lblStuStatus" runat="server"></asp:Label>             
             <asp:Label ID="lbLDAdate" runat="server"></asp:Label>             
             <asp:Label ID="lbTCdate" runat="server"></asp:Label>
             <asp:Label ID="lbStuDOJ" runat="server"></asp:Label></td>
        
        <td align="left">
             <span class="field-label">Payment Plan</span></td>
        <td align="left">
             <asp:DropDownList ID="ddlCollectionSchedule" runat="server" DataSourceID="odsGetSCHEDULE_M"
                 DataTextField="SCH_DESCR" DataValueField="SCH_ID" AutoPostBack="True" SkinID="DropDownListNormal">
             </asp:DropDownList>
        </td>
        
    </tr>
    <tr>
        <td align="left">
             <span class="field-label">History</span></td>
        <td align="left" colspan="3" >
            <asp:GridView ID="gvHistory" runat="server" CssClass="table table-bordered table-row" Width="100%">
            </asp:GridView>
        </td>
    </tr>
    <tr>
         <td colspan="4" align="left" class="title-bg" >
             Fee Details&nbsp;</td>
    </tr>           
    <tr>
        <td align="left">
            <span class="field-label">Narration</span></td>
          <td align="left" colspan="3">
            <asp:TextBox ID="txtRemarks" runat="server"  TextMode="MultiLine" SkinID="MultiText" Width="424px" TabIndex="160"></asp:TextBox>
          </td>
    </tr>          
    <tr>
        <td colspan="4" align="center">
            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="165" /><asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
             <asp:ObjectdataSource ID="odsGetSCHEDULE_M" runat="server" SelectMethod="GetSCHEDULE_M"
                 TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
                 <selectparameters>
                <asp:Parameter DefaultValue="false" Name="bisCollection" Type="Boolean"></asp:Parameter>
                </selectparameters>
             </asp:ObjectdataSource>
             </td>                
    </tr>
    </table>  
                </div>
            </div>
        </div>
    
    </asp:Content>

