<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudRecordView.aspx.vb" Inherits="StudRecordView" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Student Records
        </div>
        <div class="card-body">
            <div class="table-responsive">


    <table align="center" width="100%">
        <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
            </td>
        </tr>
       
        <tr>
            <td align="left" valign="top">
                <table id="tbl_test" runat="server" align="center" width="100%">
                    <tr>
                        <td align="left" colspan="2">
                 <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                 </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                <span class="field-label">Select Academic year</span> </td>
                        <td align="left" width="30%"><asp:DropDownList ID="ddlAca_Year" runat="server" CssClass="listbox" Width="132px" AutoPostBack="True">
                </asp:DropDownList></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                      <tr>
                        <td align="left" colspan="4">
                            <asp:GridView ID="gvStudentRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%">
                                <Columns>
                                
                                   <asp:TemplateField HeaderText="-" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("eqs_id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    
                                    <asp:TemplateField HeaderText="Join Date" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("DOJ", "{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblJOINDTH" runat="server" Text="Join Date"></asp:Label><br />
                                            <asp:TextBox ID="txtJOINDT" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchJOINDT" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchJOINDT_Click" />
                                                            
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                        <HeaderStyle Wrap="False" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblJOINTDT" runat="server" Text='<%# Bind("DOJ", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student No">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("DOJ", "{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                           <asp:Label ID="lblSTUD_IDH" runat="server" Text="Student ID"></asp:Label><br />
                                            <asp:TextBox ID="txtSTUD_ID" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchStud_ID" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchStud_ID_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTUD_ID" runat="server" Text='<%# Bind("STU_No") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("STU_No") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblStud_NameH" runat="server" Text="Student Name"></asp:Label><br />
                                            <asp:TextBox ID="txtStud_Name" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchStud_Name" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchStud_Name_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                        <HeaderStyle Wrap="False" />
                                        <ItemTemplate>
                                          <asp:Label ID="lblSTUD_Name" runat="server" Text='<%# Bind("SName") %>'></asp:Label>  
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("SName") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblGradeH" runat="server" Text="Grade"></asp:Label><br />
                                            <asp:TextBox ID="txtStud_Grade" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchGrade" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchGrade_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            &nbsp;
                                            <asp:Label ID="lblGradeH" runat="server" Text='<%# Bind("GRM_DIS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    
                                    <asp:TemplateField HeaderText="Pending Documents" Visible="False"><HeaderTemplate>
                                    Documents<br />
                                    <asp:DropDownList id="ddlgvDoc" runat="server" CssClass="listbox" Width="50px" AutoPostBack="True" OnSelectedIndexChanged="ddlgvDoc_SelectedIndexChanged">
                                    <asp:ListItem>ALL</asp:ListItem>
                                    <asp:ListItem>YES</asp:ListItem>
                                    <asp:ListItem>NO</asp:ListItem>
                                    </asp:DropDownList>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                    <asp:Image ID="imgDoc" runat="server" HorizontalAlign="Center" ImageUrl='<%# BIND("IMGDOC") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>

                                    
                                    
                                    <asp:TemplateField>
                                    
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblEditH" runat="server" Text="Edit"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lblEdit" runat="server" OnClick="lblEdit_Click">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                     <asp:TemplateField HeaderText="Services" Visible="False">
                                       <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lblService" runat="server" OnClick="lblService_Click">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    
                                    
                                    <asp:TemplateField HeaderText="Student ID" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("stu_id") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudentID" runat="server" Text='<%# Bind("stu_id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                   
                                    
                                </Columns>
                                <RowStyle CssClass="griditem" />                               
                                <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                               
                            </asp:GridView>
                           
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" /></td>
        </tr>
    </table>
    
                </div>
            </div>
        </div>

</asp:Content>

