﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_RewardsSearchSearch
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            'btnSave.Attributes.Add("onclick", "return UnpostOthFeecharge(" & h_UnpostFChg.ClientID & ") ")


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try

                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                ' gvHistory.Attributes.Add("bordercolor", "#1b80b6")

                If Session("sUsr_name") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))

                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_FEE_DAYEND_PROCESS
                            lblHead.Text = "Day End Process"
                    End Select
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ' txtDate.Text = Format(Now.Date.AddDays(1), OASISConstants.DateFormat)
                    ' getDate()
                    ' ddlBUnit.DataBind()

                    'ddlBUnit.Items.FindByValue(Session("sBsuid")).Selected = True
                    ' BindHistory()
                    Call BindBusinessUnit()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), CStr(Session("sBsuid")))
                    Call BindGrade()
                    Call BindSection()
                    '  btnprint.Visible = False
                    If rbStaff.Checked = True Then
                        Call disableControls()
                        'txtEmpno.Visible = True
                        'txtEmployeename.Visible = True
                        txtStudentName.Text = ""
                        txtStudentNo.Text = ""
                    Else
                        Call EnableControls()
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try

        End If
    End Sub

    Sub disableControls()
        ddlAcademicYear.Enabled = False
        ddlgrade.Enabled = False
        ddlSection.Enabled = False
        txtParentName.Enabled = False
        txtParentUsername.Enabled = False

    End Sub
    Sub EnableControls()
        ddlAcademicYear.Enabled = True
        ddlgrade.Enabled = True
        ddlSection.Enabled = True
        txtParentName.Enabled = True
        txtParentUsername.Enabled = True
    End Sub
    Sub BindBusinessUnit()
        Try

            Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString)

            Dim cmd As New SqlCommand("dbo.BusinessUnitsFeesSearch", sqlCon)
            cmd.CommandType = CommandType.StoredProcedure
            'cmd.Parameters.Add("@BSU_ID", SqlDbType.VarChar).Value = CStr(Session("sBsuid"))
            sqlCon.Open()
            Using dr As SqlDataReader = cmd.ExecuteReader()
                If dr.HasRows Then
                    ddlBUnit.DataSource = dr
                    ddlBUnit.DataTextField = "BSU_NAME"
                    ddlBUnit.DataValueField = "BSU_ID"

                    ddlBUnit.DataBind()
                    Dim li As New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlBUnit.Items.Insert(0, li)
                    ddlBUnit.SelectedValue = CStr(Session("sBsuid"))

                End If
            End Using
            sqlCon.Close()


        Catch ex As Exception
            Response.Write("Error:" + ex.Message.ToString())
        End Try
    End Sub

    Private Sub BindGrade()
        Try

            Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)

            Dim cmd As New SqlCommand("[OASIS].GET_ALL_GRADES_FROM_ACDID", sqlCon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@BSU_ID", SqlDbType.VarChar).Value = ddlBUnit.SelectedValue.ToString
            cmd.Parameters.Add("@ACD_ID", SqlDbType.Int).Value = ddlAcademicYear.SelectedValue.ToString
            sqlCon.Open()
            Using dr As SqlDataReader = cmd.ExecuteReader()
                If dr.HasRows Then
                    ddlgrade.DataSource = dr
                    ddlgrade.DataTextField = "GRM_GRD_ID"
                    ddlgrade.DataValueField = "GRM_GRD_ID"
                    ddlgrade.DataBind()
                    Dim li As New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlgrade.Items.Insert(0, li)
                End If
            End Using
            sqlCon.Close()


        Catch ex As Exception
            Response.Write("Error:" + ex.Message.ToString())
        End Try
    End Sub

    Private Sub BindSection()
        Try

            Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)

            Dim cmd As New SqlCommand("[OASIS].[GET_ALL_SECTION_FROM_GRADE]", sqlCon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@BSU_ID", SqlDbType.VarChar).Value = ddlBUnit.SelectedValue.ToString
            cmd.Parameters.Add("@ACD_ID", SqlDbType.Int).Value = ddlAcademicYear.SelectedValue.ToString
            cmd.Parameters.Add("@GRADE", SqlDbType.VarChar).Value = ddlgrade.SelectedValue.ToString
            sqlCon.Open()
            Using dr As SqlDataReader = cmd.ExecuteReader()
                If dr.HasRows Then
                    ddlSection.DataSource = dr
                    ddlSection.DataTextField = "SCT_DESCR"
                    ddlSection.DataValueField = "SCT_ID"
                    ddlSection.DataBind()
                    Dim li As New ListItem
                    li.Text = "ALL"
                    li.Value = ""
                    ddlSection.Items.Insert(0, li)
                End If
            End Using
            sqlCon.Close()


        Catch ex As Exception
            Response.Write("Error:" + ex.Message.ToString())
        End Try
    End Sub

    Protected Sub ddlBUnit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBUnit.SelectedIndexChanged
        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), CStr(Session("sBsuid")))
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Call BindGrade()
    End Sub

    Protected Sub ddlgrade_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlgrade.SelectedIndexChanged
        Call BindSection()
    End Sub
    Private Sub gridbind()
        Try
            Dim bsunit As Integer
            If ddlBUnit.SelectedItem.ToString <> "ALL" Then
                bsunit = ddlBUnit.SelectedItem.Value.ToString
            Else
                bsunit = 0
            End If

            Dim acdid As Integer


            acdid = ddlAcademicYear.SelectedItem.Value.ToString
            Dim grade As String = ""
            If ddlgrade.Items.Count > 0 Then
                '    ddlgrade.SelectedItem.Text = ""
                'Else
                If ddlgrade.SelectedItem.ToString <> "ALL" Then
                    grade = ddlgrade.SelectedItem.Value.ToString
                Else
                    grade = ""
                End If
            Else

                grade = ""
            End If
            Dim section As String
            If ddlSection.Items.Count > 0 Then
                '    ddlSection.SelectedItem.Text = ""
                'Else
                If ddlSection.SelectedItem.ToString <> "ALL" Then
                    section = ddlSection.SelectedItem.Value.ToString
                Else
                    section = Nothing
                End If
            Else
                section = Nothing
            End If

            If txtEmail.Text <> "" Then
            Else
                txtEmail.Text = ""
            End If

            If txtMobNo.Text <> "" Then
            Else
                txtMobNo.Text = ""
            End If
            Dim ESNO As String = ""
            Dim ESName As String = ""
            Dim mode As Integer
            If rbParent.Checked = True Then   'parent

                mode = 0
                If txtStudentName.Text <> "" Then
                    ESName = txtStudentName.Text
                Else
                    ESName = ""
                End If
                If txtStudentNo.Text <> "" Then
                    ESNO = txtStudentNo.Text
                Else
                    ESNO = ""
                End If

            Else   'staff
                mode = 1
                If txtStudentName.Text <> "" Then
                    ESName = txtStudentName.Text
                Else
                    ESName = ""
                End If
                If txtStudentNo.Text <> "" Then
                    ESNO = txtStudentNo.Text
                Else
                    ESNO = ""

                End If

                'staff case


            End If





            'If txtStudentName.Text <> "" Then
            'Else
            '    txtStudentName.Text = ""
            'End If
            'If txtStudentNo.Text <> "" Then 'if staff 'employeeNo' is searched
            'Else
            '    txtStudentNo.Text = ""
            'End If
            If txtParentName.Text <> "" Then
            Else
                txtParentName.Text = ""
            End If
            If txtParentUsername.Text <> "" Then
            Else
                txtParentUsername.Text = ""
            End If
            If txtCustomerID.Text <> "" Then
            Else
                txtCustomerID.Text = ""
            End If
            If txtPersistentId.Text <> Nothing Then
            Else
                txtPersistentId.Text = 0
            End If


            Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString)
            Dim Param(12) As SqlParameter

            Param(0) = Mainclass.CreateSqlParameter("@academicyr", ddlAcademicYear.SelectedItem.Value.ToString, SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@grade", grade, SqlDbType.VarChar)
            Param(2) = Mainclass.CreateSqlParameter("@section", section, SqlDbType.VarChar)
            Param(3) = Mainclass.CreateSqlParameter("@studentno", ESNO, SqlDbType.VarChar) 'studno /empno 
            Param(4) = Mainclass.CreateSqlParameter("@studentname", ESName, SqlDbType.VarChar) 'studname/empname
            Param(5) = Mainclass.CreateSqlParameter("@mobnum", txtMobNo.Text, SqlDbType.VarChar)
            Param(6) = Mainclass.CreateSqlParameter("@email", txtEmail.Text, SqlDbType.VarChar)
            Param(7) = Mainclass.CreateSqlParameter("@parentName", txtParentName.Text, SqlDbType.VarChar)
            Param(8) = Mainclass.CreateSqlParameter("@parentusername", txtParentUsername.Text, SqlDbType.VarChar)
            Param(9) = Mainclass.CreateSqlParameter("@customerid", txtCustomerID.Text, SqlDbType.VarChar)
            Param(10) = Mainclass.CreateSqlParameter("@persistentid", Convert.ToInt32(txtPersistentId.Text), SqlDbType.Int)
            Param(11) = Mainclass.CreateSqlParameter("@bsunitid", Convert.ToInt32(bsunit), SqlDbType.Int)
            Param(12) = Mainclass.CreateSqlParameter("@mode", mode, SqlDbType.Int)
            Dim DS As DataSet = SqlHelper.ExecuteDataset(sqlCon, "[dbo].StudentFeesSearch", Param)
            Dim dt As DataTable = DS.Tables(0)
            gvStudfeeSearch.DataSource = dt
            gvStudfeeSearch.DataBind()
            If txtPersistentId.Text = 0 Then
                txtPersistentId.Text = ""
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        gridbind()
    End Sub

    Protected Sub gvStudfeeSearch_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvStudfeeSearch.PageIndexChanging
        Me.gvStudfeeSearch.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvStudfeeSearch_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvStudfeeSearch.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblstudid As Label
            Dim lblview As LinkButton = e.Row.FindControl("lnkBtnView")

            lblstudid = e.Row.FindControl("lblstudid")
            Session("Stud_num") = lblstudid.Text
            'lblview.Attributes.Add("OnClick", "return ShowWindowWithClose('../CoCurricularActivities/FeesDetailSearch_View.aspx?', 'Student Details', '50%', '70%', 'View');")
            lblview.Attributes.Add("OnClick", "ShowWindowWithClose('../Fees/RewardSearch_View.aspx?ID=" & Encr_decrData.Encrypt(lblstudid.Text) & "', 'Student Details', '80%', '70%', 'View');")
            'lblview.Attributes.Add("OnClick", "ShowWindowWithClose('../Fees/FeesSearch_View.aspx?ID=" & Encr_decrData.Encrypt(lblstudid.Text) & "', 'Student Details', '100%', '100%', 'View');")

        End If

    End Sub

    Protected Sub rbStaff_CheckedChanged(sender As Object, e As EventArgs) Handles rbStaff.CheckedChanged
        Call disableControls()
    End Sub

    Protected Sub rbParent_CheckedChanged(sender As Object, e As EventArgs) Handles rbParent.CheckedChanged
        Call EnableControls()
    End Sub
End Class
