Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FEEConcessionTransCancel
    Inherits BasePage
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            InitialiseComponents()
            Dim CurBsUnit As String = Session("sBsuid")
            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "add")
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CONCESSION_CANCEL And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CONCESSION_APPROVAL And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_CONCESSION_CANCEL_APPROVAL) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                radAmount.Checked = True
                ddlAcademicYear.DataSource = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
                ddlAcademicYear.DataTextField = "ACY_DESCR"
                ddlAcademicYear.DataValueField = "ACD_ID"
                ddlAcademicYear.DataBind()
                ddlAcademicYear.SelectedIndex = -1
                If Not Session("Current_ACD_ID") Is Nothing Then
                    ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                End If

                BindFee()
                setAcademicyearDate()
                End If
                If ViewState("datamode") = "view" Then
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "view")
                    Dim FCH_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FCH_ID").Replace(" ", "+"))
                    set_ViewData(FCH_ID, False)

                Else
                    Session("FEE_CONS_TRAN") = Nothing
                End If
                lblAlert.Text = getErrorMessage("642")
        End If
    End Sub

    Sub BindFee()
        ddlFeeType.DataSource = FEEConcessionTransaction.GetConcessionFEEType(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value)
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()
    End Sub

    Sub BindConcessioin()
        ddlConcession.DataSource = FEEConcessionTransaction.StudentConcessions(Session("sBsuid"), h_STUD_ID.Value, ddlAcademicYear.SelectedItem.Value)
        ddlConcession.DataTextField = "FCH_RECNO"
        ddlConcession.DataValueField = "FCH_ID"
        ddlConcession.DataBind()
    End Sub

    Sub InitialiseComponents()
        txtStud_Name.Attributes.Add("ReadOnly", "ReadOnly")
        txtTotalFee.Attributes.Add("ReadOnly", "ReadOnly")
        gvFeeDetails.Attributes.Add("bordercolor", "#1b80b6")
        gvMonthly.Attributes.Add("bordercolor", "#1b80b6")
        txtRefIDsub.Attributes.Add("ReadOnly", "ReadOnly")
        txtRefHEAD.Attributes.Add("ReadOnly", "ReadOnly")
        txtConcession_Head.Attributes.Add("ReadOnly", "ReadOnly")
        txtConcession_Det.Attributes.Add("ReadOnly", "ReadOnly")
        txtAmount.Attributes.Add("ReadOnly", "ReadOnly")
        gvFeeDetails.DataBind()
        gvMonthly.DataBind()

        txtDate.Attributes.Add("ReadOnly", "ReadOnly")
        txtRefHEAD.Attributes.Add("ReadOnly", "ReadOnly")
        txtRemarks.Attributes.Add("ReadOnly", "ReadOnly")
        txtAmount.Attributes.Add("ReadOnly", "ReadOnly")
        txtRefIDsub.Attributes.Add("ReadOnly", "ReadOnly")
        imgCompany.Attributes.Add("onClick", "return GetStudent();")
        ' ddlAcademicYear.Enabled = False
        ddlFeeType.Enabled = False
        radAmount.Enabled = False
        radPercentage.Enabled = False
        EnabelYN(False)
    End Sub

    Private Sub DissableControls(ByVal dissable As Boolean) 
        gvFeeDetails.Columns(6).Visible = Not dissable
        btnDetAdd.Enabled = Not dissable
        btnDetCancel.Enabled = Not dissable
    End Sub

    Sub EnabelYN(ByVal EnYN As Boolean)
        txtFromDT.Enabled = EnYN
        Calendarextender1.Enabled = EnYN
        Calendarextender2.Enabled = EnYN
        txtToDT.Enabled = EnYN
    End Sub

    Protected Sub btnDetAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetAdd.Click
        If btnDetAdd.Text = "Add" Then
            Exit Sub
        End If
        If h_FCT_ID_DET.Value = "" Then
            lblError.Text = "Please select concession type!!!"
            Exit Sub
        End If
        Select Case h_FCT_ID_DET.Value
            Case 1, 2
                If H_REFID_SUB.Value = "" Then
                    lblError.Text = "Please specify the Reference No"
                    Return
                    'ElseIf Not FEEConcessionTransaction.ValidateReference(h_FCT_ID_DET.Value, H_REFID_SUB.Value, Session("sBSUID")) Then
                    '    lblError.Text = "The Reference No is not Valid "
                    '    Return
                Else
                    lblError.Text = ""
                End If
        End Select
        Dim vFEE_CON As FEEConcessionTransaction
        vFEE_CON = Session("FEE_CONS_TRAN")
        If vFEE_CON Is Nothing Then
            vFEE_CON = New FEEConcessionTransaction
        End If
        Dim str_Stu_Join_Date As String = FeeCommon.GetStudentJoinDate(Session("sBsuid"), h_STUD_ID.Value, False)
        Dim dt_Join_Date, dt_From_Date As DateTime

        If IsDate(txtDate.Text) Then
            dt_From_Date = CDate(txtDate.Text)
        Else
            lblError.Text = "Invalid document date!!!"
            Exit Sub
        End If

        If IsDate(str_Stu_Join_Date) Then
            dt_Join_Date = CDate(str_Stu_Join_Date)
            'If dt_From_Date < dt_Join_Date Then
            '    lblError.Text = "Document date is less than Student join date!!!"
            '    Exit Sub
            'End If
        Else
            lblError.Text = "Invalid Student Join Date!!!"
            Exit Sub
        End If

        For Each gvr As GridViewRow In gvMonthly.Rows
            'Get a programmatic reference to the CheckBox control
            Dim lblId As Label = CType(gvr.FindControl("lblId"), Label)
            Dim txtChargeDate As TextBox = CType(gvr.FindControl("txtChargeDate"), TextBox)
            Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)
            Dim dblActualAmount As Decimal = gvr.Cells(3).Text
            If Not txtAmount Is Nothing Then
                If Not IsNumeric(txtAmount.Text) Or Not IsDate(txtChargeDate.Text) Then
                    lblError.Text = "Invalid Amount/Date!!!"
                    Exit Sub
                Else
                    If dblActualAmount < Convert.ToDecimal(txtAmount.Text) Then
                        lblError.Text = "Concession amount exceeds actual Concession amount !!!"
                        Exit Sub
                    End If
                End If
            End If
        Next
        If Not FEEConcessionTransaction.DuplicateFeeType(vFEE_CON.SUB_DETAILS, ddlFeeType.SelectedValue, Session("FEE_CON_EID")) Then
            If btnDetAdd.Text = "Update" Then
                vFEE_CON.SUB_DETAILS = EditSubDetails(Session("FEE_CON_EID"), vFEE_CON.SUB_DETAILS)
                btnDetAdd.Text = "Add"
                Session("FEE_CONS_TRAN") = vFEE_CON
                Session("FEE_CON_EID") = Nothing
            Else
                AddDetails()
            End If

            ClearSubDetails()
        Else
            lblError.Text = "Cannot repeat Fee Concession Type"
            Return
        End If
        EnabelYN(False)
        GridBind()
    End Sub

    Private Function EditSubDetails(ByVal EID As Integer, ByVal arrSUBList As ArrayList) As ArrayList
        If arrSUBList Is Nothing Then
            Return New ArrayList
        End If
        Dim vFEE_CON_SUB As FEE_CONC_TRANC_SUB
        For i As Integer = 0 To arrSUBList.Count - 1
            vFEE_CON_SUB = arrSUBList(i)
            If vFEE_CON_SUB.FCD_ID = EID Then
                EditSubDetails_Monthly(vFEE_CON_SUB.SubList_Monthly)
                arrSUBList.Remove(vFEE_CON_SUB)
                vFEE_CON_SUB.FCD_AMOUNT = CDbl(txtAmount.Text)
                If radAmount.Checked Then
                    vFEE_CON_SUB.FCD_AMTTYPE = 1
                ElseIf radPercentage.Checked Then
                    vFEE_CON_SUB.FCD_AMTTYPE = 2
                End If
                vFEE_CON_SUB.FCD_ID = EID
                vFEE_CON_SUB.FCD_FCM_ID = h_FCM_ID_DET.Value
                vFEE_CON_SUB.FCD_FEE_ID = ddlFeeType.SelectedValue
                If txtRefIDsub.Text <> "" Then vFEE_CON_SUB.FCD_REF_ID = CInt(H_REFID_SUB.Value) _
                Else vFEE_CON_SUB.FCD_REF_ID = 0
                vFEE_CON_SUB.FEE_DESCR = ddlFeeType.SelectedItem.Text
                vFEE_CON_SUB.FCM_DESCR = txtConcession_Det.Text
                arrSUBList.Add(vFEE_CON_SUB)

                EditSubDetails_Monthly(vFEE_CON_SUB.SubList_Monthly)
                Exit For
            End If
        Next
        Return arrSUBList
    End Function

    Private Function EditSubDetails_Monthly(ByVal arrSUBListMonthly As ArrayList) As ArrayList
        If arrSUBListMonthly Is Nothing Then ''SADHU
            Return New ArrayList
        End If
        Dim vFEE_CON_SUB_Monthly As FEE_CONC_TRANC_SUB_MONTHLY
        For Each gvr As GridViewRow In gvMonthly.Rows
            'Get a programmatic reference to the CheckBox control
            Dim lblId As Label = CType(gvr.FindControl("lblId"), Label)
            Dim txtChargeDate As TextBox = CType(gvr.FindControl("txtChargeDate"), TextBox)
            Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)

            If Not txtAmount Is Nothing Then
                If Not IsNumeric(txtAmount.Text) Or Not IsDate(txtChargeDate.Text) Then
                    lblError.Text = "Invalid Amount/Date!!!"
                    Return arrSUBListMonthly
                End If
                For i As Integer = 0 To arrSUBListMonthly.Count - 1
                    vFEE_CON_SUB_Monthly = arrSUBListMonthly(i)
                    If lblId.Text = vFEE_CON_SUB_Monthly.FMD_ID Then
                        vFEE_CON_SUB_Monthly.FMD_AMOUNT = CDbl(txtAmount.Text)
                        vFEE_CON_SUB_Monthly.FMD_ORG_AMOUNT = CDbl(gvr.Cells(3).Text)
                        vFEE_CON_SUB_Monthly.FMD_DATE = txtChargeDate.Text
                        Exit For
                    End If
                Next
            End If
        Next
        Return arrSUBListMonthly
    End Function

    Private Function AlertForRefRepetition(ByVal REF_TYP As ConcessionType, ByVal REF_ID As Integer) As Boolean
        Return FEEConcessionTransaction.RefRepetition(REF_TYP, REF_ID)
    End Function

    Private Sub GridBind()
        Dim vFEE_CON As FEEConcessionTransaction = Session("FEE_CONS_TRAN")
        Dim dttab As New DataTable
        If Not vFEE_CON Is Nothing Then
            dttab = GetDataTableFromObjects(vFEE_CON.SUB_DETAILS)
        End If
        gvFeeDetails.DataSource = dttab
        gvFeeDetails.DataBind()
    End Sub

    Private Function GetDataTableFromObjects(ByVal arrList As ArrayList) As DataTable
        If arrList Is Nothing Then Return Nothing
        Dim dtDt As DataTable = CreateDataTableForFeeDetails()
        For i As Integer = 0 To arrList.Count - 1
            Dim vFEE_CON As FEE_CONC_TRANC_SUB = arrList(i)
            Dim dr As DataRow = dtDt.NewRow()
            dr("FCD_ID") = vFEE_CON.FCD_ID
            dr("FCM_DESCR") = vFEE_CON.FCM_DESCR
            dr("FEE_DESCR") = vFEE_CON.FEE_DESCR
            dr("AMT_TYPE") = IIf(vFEE_CON.FCD_AMTTYPE = 1, "Amount", "Percentage")
            dr("AMOUNT") = AccountFunctions.Round2(vFEE_CON.FCD_AMOUNT, 2)
            dtDt.Rows.Add(dr)
        Next
        Return dtDt
    End Function

    Private Function CreateDataTableForFeeDetails() As DataTable
        Dim dtDt As New DataTable()
        Dim dcFCD_ID As New DataColumn("FCD_ID", System.Type.[GetType]("System.Int32"))
        Dim dcFCM_DESCR As New DataColumn("FCM_DESCR", System.Type.[GetType]("System.String"))
        Dim dcFEE_DESCR As New DataColumn("FEE_DESCR", System.Type.[GetType]("System.String"))
        Dim dcAMT_TYPE As New DataColumn("AMT_TYPE", System.Type.[GetType]("System.String"))
        Dim dcAMOUNT As New DataColumn("AMOUNT", System.Type.[GetType]("System.Decimal"))

        dtDt.Columns.Add(dcFCD_ID)
        dtDt.Columns.Add(dcFCM_DESCR)
        dtDt.Columns.Add(dcFEE_DESCR)
        dtDt.Columns.Add(dcAMT_TYPE)
        dtDt.Columns.Add(dcAMOUNT)
        Return dtDt
    End Function

    Private Sub AddDetails()
        If Session("FEE_CONS_TRAN") Is Nothing Then
            Session("FEE_CONS_TRAN") = New FEEConcessionTransaction
        End If
        Dim vFEE_CON As FEEConcessionTransaction = Session("FEE_CONS_TRAN")
        Dim vFEE_CON_SUB As New FEE_CONC_TRANC_SUB
        vFEE_CON_SUB.FCD_AMOUNT = CDbl(txtAmount.Text)
        If radAmount.Checked Then
            vFEE_CON_SUB.FCD_AMTTYPE = 1
        ElseIf radPercentage.Checked Then
            vFEE_CON_SUB.FCD_AMTTYPE = 2
        End If
        vFEE_CON_SUB.FCD_ID = GETNEXTFCD_ID(vFEE_CON.SUB_DETAILS)
        vFEE_CON_SUB.FCD_FCM_ID = h_FCM_ID_DET.Value
        vFEE_CON_SUB.FCD_FEE_ID = ddlFeeType.SelectedValue
        'vFEE_CON_SUB.FCD_SCH_ID = ViewState("SCH_ID")
        If CInt(h_FCT_ID_DET.Value) = ConcessionType.Staff Then
            vFEE_CON_SUB.FCD_REF_BSU_ID = FEEConcessionTransaction.GetEmployeeBSU(H_REFID_SUB.Value, H_REFID_HEAD.Value)
        End If

        If txtRefIDsub.Text <> "" Then vFEE_CON_SUB.FCD_REF_ID = CInt(H_REFID_SUB.Value) _
        Else vFEE_CON_SUB.FCD_REF_ID = 0
        vFEE_CON_SUB.FEE_DESCR = ddlFeeType.SelectedItem.Text
        vFEE_CON_SUB.FCM_DESCR = txtConcession_Det.Text
        If (vFEE_CON.SUB_DETAILS Is Nothing) Then
            vFEE_CON.SUB_DETAILS = New ArrayList
        End If
        vFEE_CON_SUB = AddDetails_Monthly(vFEE_CON_SUB, vFEE_CON_SUB.FCD_ID)
        vFEE_CON.SUB_DETAILS.Add(vFEE_CON_SUB)
        Session("FEE_CONS_TRAN") = vFEE_CON
    End Sub

    Private Function AddDetails_Monthly(ByVal vFEE_CON_SUB As FEE_CONC_TRANC_SUB, ByVal vFMD_FCD_ID As Integer) As FEE_CONC_TRANC_SUB
        For Each gvr As GridViewRow In gvMonthly.Rows
            Dim vFEE_CON_Monthly As New FEE_CONC_TRANC_SUB_MONTHLY
            'Get a programmatic reference to the CheckBox control
            Dim lblId As Label = CType(gvr.FindControl("lblId"), Label)
            Dim txtChargeDate As TextBox = CType(gvr.FindControl("txtChargeDate"), TextBox)
            Dim txtAmount As TextBox = CType(gvr.FindControl("txtAmount"), TextBox)
            If Not txtAmount Is Nothing Then
                vFEE_CON_Monthly.FMD_AMOUNT = Convert.ToDecimal(txtAmount.Text)
                vFEE_CON_Monthly.FMD_ORG_AMOUNT = CDbl(gvr.Cells(3).Text)
                vFEE_CON_Monthly.FMD_DATE = CDate(txtChargeDate.Text)
                vFEE_CON_Monthly.FMD_FCD_ID = vFMD_FCD_ID
                vFEE_CON_Monthly.FMD_REF_ID = lblId.Text
            End If
            If vFEE_CON_SUB.SubList_Monthly Is Nothing Then
                vFEE_CON_SUB.SubList_Monthly = New ArrayList
            End If
            vFEE_CON_SUB.SubList_Monthly.Add(vFEE_CON_Monthly)
        Next
        Return vFEE_CON_SUB
    End Function

    Private Function GETNEXTFCD_ID(ByVal arrSUBList As ArrayList) As Integer
        Dim vFEE_CON_SUB As New FEE_CONC_TRANC_SUB
        Dim FCD_MAX_ID As Integer = 0
        If arrSUBList Is Nothing OrElse arrSUBList.Count <= 0 Then
            Return 1
        End If
        For i As Integer = 0 To arrSUBList.Count - 1
            vFEE_CON_SUB = arrSUBList(i)
            If vFEE_CON_SUB.FCD_ID > FCD_MAX_ID Then
                FCD_MAX_ID = vFEE_CON_SUB.FCD_ID
            End If
        Next
        Return FCD_MAX_ID + 1
    End Function

    Private Function GETNEXTFMD_ID(ByVal arrSUBList As ArrayList) As Integer
        Dim vFEE_CON_SUB_MONTHLY As New FEE_CONC_TRANC_SUB_MONTHLY
        Dim FCD_MAX_ID As Integer = 0
        If arrSUBList Is Nothing OrElse arrSUBList.Count <= 0 Then
            Return 1
        End If
        For i As Integer = 0 To arrSUBList.Count - 1
            vFEE_CON_SUB_MONTHLY = arrSUBList(i)
            If vFEE_CON_SUB_MONTHLY.FMD_ID > FCD_MAX_ID Then
                FCD_MAX_ID = vFEE_CON_SUB_MONTHLY.FMD_ID
            End If
        Next
        Return FCD_MAX_ID + 1
    End Function

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblFCD_ID As New Label
        lblFCD_ID = TryCast(sender.parent.FindControl("lblFCD_ID"), Label)
        Dim vFEE_CON As FEEConcessionTransaction = Session("FEE_CONS_TRAN")
        Dim vFEE_CON_SUB As FEE_CONC_TRANC_SUB
        Dim arrSUBDET As ArrayList = vFEE_CON.SUB_DETAILS
        For i As Integer = 0 To arrSUBDET.Count - 1
            vFEE_CON_SUB = arrSUBDET(i)
            If vFEE_CON_SUB.FCD_ID = lblFCD_ID.Text Then
                GetMonthlyList(vFEE_CON_SUB.SubList_Monthly)

                Session("FEE_CON_EID") = lblFCD_ID.Text
                ddlFeeType.SelectedIndex = -1
                ddlFeeType.Items.FindByValue(vFEE_CON_SUB.FCD_FEE_ID).Selected = True
                txtAmount.Text = vFEE_CON_SUB.FCD_AMOUNT
                txtTotalFee.Text = vFEE_CON_SUB.FCD_ACTUALAMT

                H_REFID_SUB.Value = vFEE_CON_SUB.FCD_REF_ID

                txtConcession_Det.Text = UtilityObj.GetDataFromSQL("SELECT FCM_DESCR FROM " _
                & " FEES.FEE_CONCESSION_M WHERE FCM_ID =" & h_FCM_ID_HEAD.Value, ConnectionManger.GetOASIS_FEESConnectionString)
                h_FCT_ID_DET.Value = UtilityObj.GetDataFromSQL("SELECT FCM_FCT_ID FROM " _
                          & " FEES.FEE_CONCESSION_M WHERE FCM_ID =" & h_FCM_ID_HEAD.Value, ConnectionManger.GetOASIS_FEESConnectionString)
                h_FCM_ID_DET.Value = vFEE_CON_SUB.FCD_FCM_ID
                txtRefIDsub.Text = FEEConcessionTransaction.GetReference(h_FCT_ID_DET.Value, vFEE_CON_SUB.FCD_REF_ID, Session("sBsuid"))
                Select Case vFEE_CON_SUB.FCD_AMTTYPE
                    Case 1
                        radAmount.Checked = True
                        radPercentage.Checked = False
                    Case 2
                        radAmount.Checked = False
                        radPercentage.Checked = True
                End Select
                btnDetAdd.Text = "Update"
                txtPercAmountCancel.Text = txtAmount.Text
                EnabelYN(True)
                'getMonthly()
                Exit For
            End If
        Next
    End Sub

    Private Sub GetMonthlyList(ByVal arrSubList_Monthly As ArrayList)
        Dim dt As New DataTable
        dt = FEE_CONC_TRANC_SUB_MONTHLY.CreateDataTableFeeConcessionMonthly
        For Each vSubMonthly As FEE_CONC_TRANC_SUB_MONTHLY In arrSubList_Monthly
            Dim dr As DataRow
            dr = dt.NewRow
            dr("id") = vSubMonthly.FMD_ID
            dr("DESCR") = vSubMonthly.FMD_REF_NAME
            dr("REF_ID") = vSubMonthly.FMD_REF_ID
            dr("FDD_DATE") = vSubMonthly.FMD_DATE
            dr("CUR_AMOUNT") = vSubMonthly.FMD_AMOUNT
            dr("FDD_AMOUNT") = vSubMonthly.FMD_ORG_AMOUNT

            'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT
            dt.Rows.Add(dr)
        Next
        Session("tempMonthly") = dt
        gvMonthly.DataSource = dt
        gvMonthly.DataBind()
    End Sub

    Protected Sub btnDetCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetCancel.Click
        ClearSubDetails()
    End Sub

    Private Sub ClearSubDetails()
        txtRefIDsub.Text = ""
        txtAmount.Text = ""
        txtPercAmountCancel.Text = ""
        btnDetAdd.Text = "Add"
        Session("FEE_CON_EID") = Nothing
        gvMonthly.DataBind()
    End Sub

    Protected Sub lnkBtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblFCD_ID As New Label
        lblFCD_ID = TryCast(sender.parent.FindControl("lblFCD_ID"), Label)

        Dim vFEE_CON As FEEConcessionTransaction = Session("FEE_CONS_TRAN")
        Dim vFEE_CON_SUB As FEE_CONC_TRANC_SUB
        Dim arrSUBDET As ArrayList = vFEE_CON.SUB_DETAILS
        For i As Integer = 0 To arrSUBDET.Count - 1
            vFEE_CON_SUB = arrSUBDET(i)
            If vFEE_CON_SUB.FCD_ID = lblFCD_ID.Text Then
                'vFEE_CON_SUB.bDelete = True
                vFEE_CON.SUB_DETAILS.Remove(vFEE_CON_SUB)
                Exit For
            End If
        Next
        GridBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'If Not Master.IsSessionMatchesForSave() Then
        '    lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
        '    Exit Sub
        'End If
        Dim str_error As String = CheckErrors()
        Dim dtDocDate, dtFromDT, dtToDT As DateTime
        dtDocDate = CDate(txtDate.Text)
        dtFromDT = CDate(txtFromDT.Text)
        dtToDT = CDate(txtToDT.Text)

        If btnDetAdd.Text = "Update" Then
            str_error = str_error & "Please Update Selected Details..!<br />"
        End If
        If DateValue(txtFromDT.Text) < DateValue(h_Fdate.Value) Then
            str_error = str_error & "Invalid From date..!<br />"
        End If
        If DateValue(txtToDT.Text) > DateValue(h_Tdate.Value) Then
            str_error = str_error & "Invalid To date..!<br />"
        End If
        If dtToDT < dtFromDT Then
            str_error = str_error & "From date is greater than to date<br />"
        End If
        If h_FCH_ID.Value = "" Then
            lblError.Text = "Please select the concession to cancel <br>" & str_error
        End If
        If str_error <> "" Then
            lblError.Text = "Please check the Following : <br>" & str_error
            Exit Sub
        End If
        Dim FCH_ID As Integer = h_FCH_ID.Value
        Dim vFEE_CONS_TRAN As New FEEConcessionTransaction
        If Not Session("FEE_CONS_TRAN") Is Nothing Then
            vFEE_CONS_TRAN = Session("FEE_CONS_TRAN")
        End If
        vFEE_CONS_TRAN.FCH_ACD_ID = ddlAcademicYear.SelectedValue
        vFEE_CONS_TRAN.FCH_BSU_ID = Session("sbsuid")
        vFEE_CONS_TRAN.FCH_DT = dtDocDate
        vFEE_CONS_TRAN.FCH_DTFROM = dtFromDT
        vFEE_CONS_TRAN.FCH_DTTO = dtToDT
        vFEE_CONS_TRAN.FCH_FCM_ID = h_FCM_ID_HEAD.Value
        If txtRefHEAD.Text <> "" Then vFEE_CONS_TRAN.FCH_REF_ID = CInt(H_REFID_HEAD.Value) _
      Else vFEE_CONS_TRAN.FCH_REF_ID = 0
        ' vFEE_CONS_TRAN.FCH_REF_ID = txtRefID.Text
        vFEE_CONS_TRAN.FCH_REMARKS = txtRemarks.Text
        vFEE_CONS_TRAN.FCH_STU_ID = h_STUD_ID.Value
        vFEE_CONS_TRAN.STU_NAME = txtStud_Name.Text
        vFEE_CONS_TRAN.FCH_DRCR = "DR"
        vFEE_CONS_TRAN.FCH_FCH_ID = FCH_ID
        Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
            Dim trans As SqlTransaction
            trans = conn.BeginTransaction("FEE_CONCESSION_TRANS")
            Dim NEW_FCH_ID As String = ""
            Dim retVal As Integer = FEEConcessionTransaction.F_SaveFEE_CONCESSION_H(vFEE_CONS_TRAN, NEW_FCH_ID, _
            h_FCT_ID_HEAD.Value, conn, trans)
            If retVal <> 0 Then
                trans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(retVal)
            Else
                'trans.Rollback()
                trans.Commit()
                ViewState("FCH_ID") = NEW_FCH_ID
                lblError.Text = "Data updated Successfully"
                Session("FEE_CONS_TRAN") = Nothing
                ClearDetails()
                ViewState("datamode") = "add"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim str_KEY As String = "INSERT"
                If ViewState("datamode") <> "edit" Then
                    str_KEY = "EDIT"
                End If
                Dim flagAudit As Integer '= UtilityObj.operOnAudiTable(Master.MenuName, NEW_FCH_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page, txtRemarks.Text)
                'If flagAudit <> 0 Then
                '    Throw New ArgumentException("Could not process your request")
                'End If
                'If ViewState("datamode") <> "view" Then
                '    PrintConcessionCancel(ViewState("FCH_ID"))
                'Else
                '    PrintConcessionCancel(Encr_decrData.Decrypt(Request.QueryString("FCH_ID").Replace(" ", "+")))
                'End If
            End If
        End Using
    End Sub

    Private Function CheckErrors() As String
        If Not FEEConcessionTransaction.PeriodBelongstoAcademicYear(CDate(txtFromDT.Text), CDate(txtToDT.Text), ddlAcademicYear.SelectedValue, Session("sBSUID")) Then
            Return "The date period doesnot belongs to current Academic Year"
        End If
        Return ""
    End Function

    Private Sub ClearDetails()
        setAcademicyearDate() 
        h_FCH_ID.Value = ""
        txtRefHEAD.Text = ""
        txtRemarks.Text = ""
        txtAmount.Text = ""
        txtRefIDsub.Text = ""
        txtStud_Name.Text = ""
        h_STUD_ID.Value = ""
        h_GRD_ID.Value = ""

        h_FCM_ID_DET.Value = ""
        h_FCM_ID_HEAD.Value = ""
        h_FCT_ID_DET.Value = ""
        h_FCT_ID_HEAD.Value = ""

        H_REFID_HEAD.Value = ""
        txtRefHEAD.Text = ""
        txtRefIDsub.Text = ""
        H_REFID_SUB.Value = ""
        txtStdNo.Text = ""
        radAmount.Checked = True
        radPercentage.Checked = False
        Session("FEE_CONS_TRAN") = Nothing
        GridBind()
        ClearSubDetails()
        ddlConcession.DataSource = Nothing
        ddlConcession.DataBind()
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        ClearDetails()
        ClearSubDetails()
        DissableControls(False)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub imgRefSub_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgRefSub.Click
        If H_REFID_SUB.Value <> "" Then
            If AlertForRefRepetition(h_FCM_ID_DET.Value, H_REFID_SUB.Value) Then
                ViewState("CLICK_TYPE") = "SUB"
                'Me.programmaticModalPopup.Show()
            End If
        End If
    End Sub

    Protected Sub imgRefHead_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgRefHead.Click
        If H_REFID_HEAD.Value <> "" Then
            If AlertForRefRepetition(h_FCM_ID_DET.Value, H_REFID_HEAD.Value) Then
                ViewState("CLICK_TYPE") = "HEAD"
                'Me.programmaticModalPopup.Show()
            End If
        End If
    End Sub

    Protected Sub ddlFeeType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFeeType.SelectedIndexChanged
        Set_Collection()
    End Sub

    Sub Set_Collection()
        If h_STUD_ID.Value <> "" Then
            Dim str_acd As String = UtilityObj.GetDataFromSQL("SELECT STU_ACD_ID FROM STUDENT_M WHERE STU_ID ='" & h_STUD_ID.Value & "'", ConnectionManger.GetOASISConnectionString)
            'If Not ddlAcademicYear.Items.FindByValue(str_acd) Is Nothing Then
            '    ddlAcademicYear.SelectedIndex = -1
            '    ddlAcademicYear.Items.FindByValue(str_acd).Selected = True
            'End If
        End If
        BindConcessioin()
        If ddlConcession.Items.Count > 0 Then
            set_ViewData(ddlConcession.SelectedValue, True)
        End If
        Dim iMonths, iSCH_ID As Integer
        Dim dtTermorMonths As New DataTable
        Dim TOTALAMOUNT As Decimal = 0
        Dim STR_SCH_ID As String = FeeCommon.GetFeeSCH_ID_ForConcession(h_STUD_ID.Value, ddlFeeType.SelectedItem.Value, _
        ddlAcademicYear.SelectedItem.Value, Session("sBSUID"), TOTALAMOUNT)
        ViewState("SCH_ID") = STR_SCH_ID
        txtTotalFee.Text = TOTALAMOUNT
        If IsNumeric(STR_SCH_ID) Then
            iSCH_ID = CInt(STR_SCH_ID)
            If iSCH_ID = 2 Then
                dtTermorMonths = FeeCommon.GetFee_MonthorTerm(ddlFeeType.SelectedItem.Value, _
                ddlAcademicYear.SelectedItem.Value, Session("sBSUID"), True, h_STUD_ID.Value, txtFromDT.Text)
            Else
                dtTermorMonths = FeeCommon.GetFee_MonthorTerm(ddlFeeType.SelectedItem.Value, _
                ddlAcademicYear.SelectedItem.Value, Session("sBSUID"), False, h_STUD_ID.Value, txtFromDT.Text)
            End If
            Select Case iSCH_ID
                Case 0 '0 Monthly 
                    iMonths = 10
                Case 1 '1 Bi-Monthly
                    iMonths = 5
                Case 2 '2 Quarterly 
                    iMonths = 3
                Case 3 '3 Half Year
                    iMonths = 2
                Case 4, 5 '4 Annual 
                    iMonths = 1
                Case Else '5 Once Only 
                    iMonths = 0
            End Select
        End If
        Dim dt As DataTable
        dt = FEE_CONC_TRANC_SUB_MONTHLY.CreateDataTableFeeConcessionMonthly
        Select Case iMonths
            Case 10, 3, 1
                For i As Integer = 0 To iMonths - 1
                    If i < dtTermorMonths.Rows.Count Then
                        Dim dr As DataRow
                        dr = dt.NewRow
                        dr("id") = dtTermorMonths.Rows(i)("ID")
                        dr("Descr") = dtTermorMonths.Rows(i)("descr")
                        dr("FDD_DATE") = dtTermorMonths.Rows(i)("FDD_DATE")
                        dr("FDD_AMOUNT") = dtTermorMonths.Rows(i)("FDD_AMOUNT")
                        'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT
                        dt.Rows.Add(dr)
                    End If
                Next
            Case 5, 2
                For i As Integer = 0 To iMonths - 1
                    If i < dtTermorMonths.Rows.Count Then
                        Dim dr As DataRow
                        dr = dt.NewRow
                        dr("id") = dtTermorMonths.Rows(i * 10 / iMonths)("ID")
                        dr("Descr") = dtTermorMonths.Rows(i)("descr")
                        dr("FDD_DATE") = dtTermorMonths.Rows(i * 10 / iMonths)("FDD_DATE")
                        dr("FDD_AMOUNT") = dtTermorMonths.Rows(i)("FDD_AMOUNT")
                        'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT,FDD_AMOUNT
                        dt.Rows.Add(dr)
                    End If
                Next
        End Select
        Session("tempMonthly") = dt
        gvMonthly.DataSource = dt
        gvMonthly.DataBind()
    End Sub

    Protected Sub imgCompany_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCompany.Click
        Set_Collection()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        setAcademicyearDate()
    End Sub

    Sub setAcademicyearDate()
        Dim DTFROM As String = ""
        Dim DTTO As String = ""
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, ddlAcademicYear.SelectedItem.Value, Session("sBsuid"))
        txtFromDT.Text = DTFROM
        txtToDT.Text = DTTO
        txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
    End Sub  

    Sub set_ViewData(ByVal FCH_ID As Integer, ByVal bCancel As Boolean)
        h_FCH_ID.Value = FCH_ID
        Dim vFEE_CON As FEEConcessionTransaction 'NO need to pass acd_id (acd id tracked from fch_id) 
        vFEE_CON = FEEConcessionTransaction.GetDetails(FCH_ID, 2, 0, Session("sBSUID"), bCancel)
        Session("FEE_CONS_TRAN") = vFEE_CON
        'txtDate.Text = Format(vFEE_CON.FCH_DT, OASISConstants.DateFormat)
        txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        txtFromDT.Text = Format(vFEE_CON.FCH_DTFROM, OASISConstants.DateFormat)
        txtToDT.Text = Format(vFEE_CON.FCH_DTTO, OASISConstants.DateFormat)
        h_Fdate.Value = txtFromDT.Text
        h_Tdate.Value = txtToDT.Text

        h_FCM_ID_HEAD.Value = vFEE_CON.FCH_FCM_ID
        txtConcession_Head.Text = UtilityObj.GetDataFromSQL("SELECT FCM_DESCR FROM " _
        & " FEES.FEE_CONCESSION_M WHERE FCM_ID =" & h_FCM_ID_HEAD.Value, ConnectionManger.GetOASIS_FEESConnectionString)
        h_FCT_ID_HEAD.Value = UtilityObj.GetDataFromSQL("SELECT FCM_FCT_ID FROM " _
        & " FEES.FEE_CONCESSION_M WHERE FCM_ID =" & h_FCM_ID_HEAD.Value, ConnectionManger.GetOASIS_FEESConnectionString)
        h_GRD_ID.Value = UtilityObj.GetDataFromSQL("SELECT STU_GRD_ID FROM " _
                & " STUDENT_M WHERE STU_ID =" & vFEE_CON.FCH_STU_ID, ConnectionManger.GetOASISConnectionString)

        txtRefHEAD.Text = vFEE_CON.FCH_REF_NAME
        H_REFID_HEAD.Value = vFEE_CON.FCH_REF_ID

        txtRefHEAD.Text = FEEConcessionTransaction.GetReference(h_FCT_ID_HEAD.Value, vFEE_CON.FCH_REF_ID, Session("sBsuid"))
        txtRemarks.Text = vFEE_CON.FCH_REMARKS
        h_STUD_ID.Value = vFEE_CON.FCH_STU_ID
        txtStud_Name.Text = FeeCommon.GetStudentName(vFEE_CON.FCH_STU_ID, False)
        txtStdNo.Text = FeeCommon.GetStudentNo(vFEE_CON.FCH_STU_ID, False)
        ddlAcademicYear.SelectedIndex = -1
        ddlAcademicYear.Items.FindByValue(vFEE_CON.FCH_ACD_ID).Selected = True
        BindFee()
        txtStud_Name.Text = vFEE_CON.STU_NAME
        GridBind()
        DissableControls(Not bCancel)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If ViewState("datamode") <> "view" Then
            PrintConcessionCancel(ViewState("FCH_ID"))
        Else
            PrintConcessionCancel(Encr_decrData.Decrypt(Request.QueryString("FCH_ID").Replace(" ", "+")))
        End If
    End Sub

    Private Sub PrintConcessionCancel(ByVal IntFCH_ID As Integer)
        Session("ReportSource") = FEEConcessionTransaction.PrintConcessionCancel(IntFCH_ID, Session("sBsuid"), Session("sUsr_name"))
        h_print.Value = "print"
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Status As Integer
        Try
            Dim IntFCH_ID As Integer
            If ViewState("datamode") <> "view" Then
                IntFCH_ID = ViewState("FCH_ID")
            Else
                IntFCH_ID = Encr_decrData.Decrypt(Request.QueryString("FCH_ID").Replace(" ", "+"))
            End If
            Status = FEEConcessionTransaction.DELETEFEE_CONCESSION_H(IntFCH_ID, Session("sBsuid"), Session("sUsr_name"))
            If Status <> 0 Then
                lblError.Text = (UtilityObj.getErrorMessage(Status))
                Exit Sub
            Else
                'Status = UtilityObj.operOnAudiTable(Master.MenuName, IntFCH_ID, "delete", Page.User.Identity.Name.ToString, Me.Page)
                'If Status <> 0 Then
                '    Throw New ArgumentException("Could not complete your request")
                'End If

                lblError.Text = "Record Deleted Successfully"
            End If
        Catch myex As ArgumentException
            lblError.Text = "Record could not be Deleted"
            UtilityObj.Errorlog(myex.Message, Page.Title)
        Catch ex As Exception
            lblError.Text = "Record could not be Deleted"
            Errorlog(ex.Message)
        End Try

        Session("datamode") = "none"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), Session("menu_rights"), "add")
    End Sub

    Sub Set_Concession()

        Dim _table As DataTable = getMonthly()
        If _table.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim vFEE_CON As FEEConcessionTransaction = Session("FEE_CONS_TRAN")
        Dim vFEE_CON_SUB As FEE_CONC_TRANC_SUB
        Dim arrSUBDET As ArrayList = vFEE_CON.SUB_DETAILS
        For i As Integer = 0 To arrSUBDET.Count - 1
            vFEE_CON_SUB = arrSUBDET(i) 'Got the current record
            Dim dPErcentage As Decimal
            Dim dTotal As Decimal = 0
            If vFEE_CON_SUB.FCD_AMTTYPE = 1 Then
                For j As Integer = 0 To vFEE_CON_SUB.SubList_Monthly.Count - 1
                    Dim vSubMonthly As New FEE_CONC_TRANC_SUB_MONTHLY
                    vSubMonthly = vFEE_CON_SUB.SubList_Monthly(i)
                    dTotal = vSubMonthly.FMD_ORG_AMOUNT + dTotal
                Next
                dPErcentage = dTotal / vFEE_CON_SUB.FCD_ACTUALAMT * 100
            Else
                If txtPercAmountCancel.Text = "" Then
                    dPErcentage = vFEE_CON_SUB.FCD_AMOUNT
                Else
                    dPErcentage = Convert.ToDouble(txtPercAmountCancel.Text)
                End If

            End If
            'ApplyProrata(vFEE_CON_SUB.FCD_FEE_ID, vFEE_CON_SUB.SubList_Monthly, dPErcentage, vFEE_CON_SUB.FCD_ID, True)
            ApplyProrata(vFEE_CON_SUB.SubList_Monthly, vFEE_CON_SUB.FCD_ID, _table)
        Next
        ''''''''''''''''''''

        'Dim FromDT As Date = txtFromDT.Text
        'Dim dtTermorMonths As New DataTable
        'Dim dt As DataTable

        'dt = FEE_CONC_TRANC_SUB_MONTHLY.CreateDataTableFeeConcessionMonthly
        'Dim STR_SCH_ID As Integer
        'Dim TOTALAMOUNT As Decimal = 0
        'dtTermorMonths = FeeCommon.GetFee_MonthorTerm_OASIS(ddlFeeType.SelectedItem.Value, _
        '                ddlAcademicYear.SelectedItem.Value, Session("sBSUID"), h_STUD_ID.Value, _
        '                txtFromDT.Text, txtToDT.Text, TOTALAMOUNT, STR_SCH_ID)
        'For i As Integer = 0 To dtTermorMonths.Rows.Count - 1
        '    Dim dr As DataRow
        '    dr = dt.NewRow
        '    dr("id") = dtTermorMonths.Rows(i)("ID")
        '    dr("Descr") = dtTermorMonths.Rows(i)("descr")
        '    dr("FDD_DATE") = dtTermorMonths.Rows(i)("FDD_DATE")
        '    dr("FDD_AMOUNT") = dtTermorMonths.Rows(i)("FDD_AMOUNT")
        '    dr("CUR_AMOUNT") = 0
        '    dr("PRO_AMOUNT") = dtTermorMonths.Rows(i)("FDD_PROAMOUNT")
        '    'FDD_DATE,FDD_FIRSTMEMODT,FDD_SECONDMEMODT,FDD_THIRDMEMODT,FDD_AMOUNT
        '    dt.Rows.Add(dr)
        'Next
        'txtTotalFee.Text = TOTALAMOUNT
        'ViewState("SCH_ID") = STR_SCH_ID

        'Session("tempMonthly") = dt
        'gvMonthly.DataSource = dt
        'gvMonthly.DataBind()
    End Sub

    Private Function getMonthly() As DataTable
        Dim dt As New DataTable
        If DateValue(txtFromDT.Text) < DateValue(h_Fdate.Value) Then
            lblError.Text = "From date should be grater than Concession From Date..!"
            txtFromDT.Text = h_Fdate.Value
            Return dt
        End If
        If DateValue(txtToDT.Text) > DateValue(h_Tdate.Value) Then
            lblError.Text = "To date should be less than Concession To Date..!"
            txtToDT.Text = h_Tdate.Value
            Return dt
        End If
        Dim FromDT As Date = txtFromDT.Text
        Dim dtTermorMonths As New DataTable

        Dim ConsAmount As Double = 0
        Dim CurAmount As Double = 0
        Dim StrMsg As String = "Percentage"

        If txtAmount.Text = "" Then
            txtAmount.Text = "0"
        End If
        Dim DisPrec As Double = Convert.ToDouble(txtAmount.Text)
        Dim CnDisPrec As Double = 0
        If txtPercAmountCancel.Text = "" Then
            txtPercAmountCancel.Text = txtAmount.Text
        End If
        CnDisPrec = Convert.ToDouble(txtPercAmountCancel.Text)

        If radAmount.Checked Then
            CnDisPrec = CnDisPrec / CDbl(txtTotalFee.Text) * 100
            DisPrec = DisPrec / CDbl(txtTotalFee.Text) * 100
            StrMsg = "Amount"
        End If

        If CnDisPrec > DisPrec Then
            lblError.Text = "Concession Cancel " & StrMsg & " Exceeds Actual " & StrMsg & " ..!"
            txtToDT.Text = h_Tdate.Value
            Return dt
        End If

        dt = FEE_CONC_TRANC_SUB_MONTHLY.CreateDataTableFeeConcessionMonthly
        Dim STR_SCH_ID As Integer
        Dim TOTALAMOUNT As Decimal = 0
        dtTermorMonths = FeeCommon.GetFee_MonthorTerm_OASIS(ddlFeeType.SelectedItem.Value, _
                        ddlAcademicYear.SelectedItem.Value, Session("sBSUID"), h_STUD_ID.Value, _
                        txtFromDT.Text, txtToDT.Text, TOTALAMOUNT, STR_SCH_ID)
        For i As Integer = 0 To dtTermorMonths.Rows.Count - 1
            ConsAmount = (Convert.ToDouble(dtTermorMonths.Rows(i)("FDD_AMOUNT")) * DisPrec) / 100
            CurAmount = (Convert.ToDouble(dtTermorMonths.Rows(i)("FDD_PROAMOUNT")) * CnDisPrec) / 100
            Dim dr As DataRow
            dr = dt.NewRow
            dr("REF_ID") = dtTermorMonths.Rows(i)("ID")
            dr("Descr") = dtTermorMonths.Rows(i)("descr")
            dr("FDD_DATE") = dtTermorMonths.Rows(i)("FDD_DATE")
            dr("FDD_AMOUNT") = Math.Floor(ConsAmount) 'dtTermorMonths.Rows(i)("FDD_AMOUNT")
            dr("CUR_AMOUNT") = Math.Floor(CurAmount) '0
            dr("PRO_AMOUNT") = dtTermorMonths.Rows(i)("FDD_PROAMOUNT")
            dt.Rows.Add(dr)
        Next
        txtTotalFee.Text = TOTALAMOUNT
        ViewState("SCH_ID") = STR_SCH_ID
        Session("tempMonthly") = dt
        Return dt
    End Function

    Private Sub ApplyProrata(ByRef arrSubList_Monthly As ArrayList, ByVal FCD_ID As String, ByVal _table As DataTable)
        Dim NotIn As Boolean = False
        For i As Integer = 0 To arrSubList_Monthly.Count - 1
            Dim vSubMonthly As New FEE_CONC_TRANC_SUB_MONTHLY
            vSubMonthly = arrSubList_Monthly(i)
            For x As Integer = 0 To _table.Rows.Count - 1
                If Month(vSubMonthly.FMD_DATE) = Month(_table.Rows(x)("FDD_DATE")) And Year(vSubMonthly.FMD_DATE) = Year(_table.Rows(x)("FDD_DATE")) Then
                    vSubMonthly.FMD_AMOUNT = _table.Rows(x)("CUR_AMOUNT")
                    vSubMonthly.FMD_DATE = _table.Rows(x)("FDD_DATE")
                    NotIn = True
                End If

            Next
            If NotIn = False Then
                vSubMonthly.FMD_AMOUNT = "0"
            End If
            NotIn = False
        Next
        If Session("FEE_CON_EID") = FCD_ID Then
            GetMonthlyList(arrSubList_Monthly)
        End If
    End Sub
    Private Sub ApplyProrata(ByVal Fee_Id As Integer, ByRef arrSubList_Monthly As ArrayList, _
    ByVal dPercentage As Decimal, ByVal FCD_ID As String, Optional ByVal ShouldApplyPerc As Boolean = False)
        dPercentage = dPercentage / 100
        Dim ChkStartMth As Boolean = True
        Dim ChkEndMth As Boolean = False
        Dim EndYN As Boolean = False
        Dim ChkChrgedt As String = ""
        Dim ChkFromedt As String = ""
        Dim ChkEndMonth As String = ""
        Dim CheckDate As String = ""
        Dim FromDate As String = ""
        Dim Reason As Integer = 2
        ChkFromedt = txtFromDT.Text.ToString()
        CheckDate = txtFromDT.Text.ToString()
        FromDate = txtFromDT.Text.ToString()
        ChkEndMonth = txtToDT.Text.ToString()
        ChkFromedt = DateFunctions.StartDateofMonth(ChkFromedt)
        ChkEndMonth = DateFunctions.StartDateofMonth(ChkEndMonth)
        For i As Integer = 0 To arrSubList_Monthly.Count - 1
            Dim vSubMonthly As New FEE_CONC_TRANC_SUB_MONTHLY
            vSubMonthly = arrSubList_Monthly(i)


            ChkChrgedt = Convert.ToDateTime(vSubMonthly.FMD_DATE).ToString("dd/MMM/yyyy")
            ChkChrgedt = DateFunctions.StartDateofMonth(ChkChrgedt)

            If Convert.ToDateTime(ChkChrgedt) < Convert.ToDateTime(ChkFromedt) Or (Convert.ToDateTime(ChkChrgedt) > Convert.ToDateTime(ChkEndMonth)) Then
                vSubMonthly.FMD_AMOUNT = 0
            Else

                If ChkStartMth = False Then
                    CheckDate = Convert.ToDateTime(vSubMonthly.FMD_DATE).ToString("dd/MMM/yyyy")
                End If
                Dim ProRateamount As Decimal = FEEConcessionTransaction.GetProrateChargeFee_CANCEL(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value, _
                                        h_STUD_ID.Value, Reason, FromDate, Fee_Id, vSubMonthly.FMD_PRO_AMOUNT, CheckDate)

                vSubMonthly.FMD_AMOUNT = ((ProRateamount * Convert.ToDecimal(txtPercAmountCancel.Text)) / 100)
                vSubMonthly.FMD_AMOUNT = Math.Round(vSubMonthly.FMD_AMOUNT)
            End If
        Next
        If Session("FEE_CON_EID") = FCD_ID Then
            GetMonthlyList(arrSubList_Monthly)
        End If
    End Sub

    Protected Sub txtFromDT_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_Concession()
    End Sub

    Protected Sub txtToDT_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_Concession()
    End Sub

    Protected Sub txtChargeDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_Concession()
    End Sub

    Protected Sub txtChargeDate_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtChargeDate As New TextBox
        txtChargeDate = sender
        txtChargeDate.Attributes.Add("ReadOnly", "ReadOnly")
    End Sub

    Protected Sub lnkFill_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_Concession()
    End Sub

    Private Sub Autofill_SubDetails(ByVal EID As Integer, ByVal arrSUBList As ArrayList, ByVal dPrecentage As Decimal)
        Dim vFEE_CON_SUB As FEE_CONC_TRANC_SUB
        For i As Integer = 0 To arrSUBList.Count - 1
            vFEE_CON_SUB = arrSUBList(i)
            If vFEE_CON_SUB.FCD_ID = EID Then
                EditSubDetails_Monthly(vFEE_CON_SUB.SubList_Monthly)
                ApplyProrata(vFEE_CON_SUB.FCD_FEE_ID, vFEE_CON_SUB.SubList_Monthly, dPrecentage, vFEE_CON_SUB.FCD_ID, True)
                Exit For
            End If
        Next
    End Sub

    Protected Sub ddlConcession_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlConcession.SelectedIndexChanged
        If ddlConcession.SelectedValue <> Nothing Then
            set_ViewData(ddlConcession.SelectedValue, True)
        End If
    End Sub

  
End Class
