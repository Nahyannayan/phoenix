<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="TranportPINVPrintTemplate.aspx.vb" Inherits="fees_TranportPINVPrintTemplate"
    Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %> 
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
  function CheckForPrint() {
      if (document.getElementById('<%= h_print.ClientID %>').value != '') {
        document.getElementById('<%= h_print.ClientID %>').value = '';
           radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up');
       }
}
    );

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>

     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="720px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="ProformaInvoice Template"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"--%>
                    <%--></asp:Label>--%>
                <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                <table align="center" cellpadding="5" cellspacing="0" width="100%"
                    style="border-collapse: collapse;">
                    <%--<tr class="subheader_img">
            <td align="left" colspan="2">
                <asp:Label ID="lblHeader" runat="server" Text="ProformaInvoice Template"></asp:Label></td>
        </tr>--%>
                    <tr>
                        <td align="left" width="20%" ><span class="field-label">Business Unit</span></td>
                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddBusinessunit" runat="server" AutoPostBack="True"
                                DataSourceID="odsSERVICES_BSU_M" DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"
                                TabIndex="5" OnSelectedIndexChanged="ddBusinessunit_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%" ><span class="field-label">Version.No</span></td>
                        <td align="left" width="30%" >
                            <asp:Label ID="lblVsnNo" runat="server" CssClass="field-value"> </asp:Label>
                        </td>
                    </tr>
                  
                    <tr>
                        <td align="left" ><span class="field-label">Payment Mode</span></td>
                        <td align="left" colspan="3" >
                            <telerik:RadEditor ID="txtPayMode" runat="server" StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                                 StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span"
                                 ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                            </telerik:RadEditor>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Bank Transfer detail</span></td>
                        <td align="left" colspan="3" >
                            <telerik:RadEditor ID="txtBankTfrDetail" runat="server" 
                                StripFormattingOptions="MSWordNoFonts, Css, Font, Span" 
                                StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span" ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                            </telerik:RadEditor>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Promotions</span></td>
                        <td align="left" colspan="3" >
                            <telerik:RadEditor ID="txtPromotion" runat="server" 
                                StripFormattingOptions="MSWordNoFonts, Css, Font, Span" 
                                StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span" ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                            </telerik:RadEditor>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Fee Ammendments</span></td>
                        <td align="left" colspan="3" >
                            <telerik:RadEditor ID="txtFeeDescr" runat="server"  StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                                 StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span"
                                ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                            </telerik:RadEditor>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Footer</span></td>
                        <td align="left" colspan="3" >
                            <telerik:RadEditor ID="txtFooter" runat="server"  StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                                 StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span"
                                ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                            </telerik:RadEditor>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Show Refer A Friend</span></td>
                        <td align="left" colspan="3" >
                            <asp:CheckBox ID="chkRAF" runat="server" />
                            <asp:Image ID="imgRAF" runat="server" ImageUrl="~/Images/ReferAFriend.png" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">ThankYou Message</span></td>
                        <td align="left" colspan="3" >
                            <telerik:RadEditor ID="txtThankU" runat="server"  StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                                 StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span"
                                ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                            </telerik:RadEditor>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" OnClick="btnEdit_Click" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" UseSubmitBehavior="false" />
                            <%--                <asp:Button id="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" />--%>
                            <asp:Button ID="btnPreview" runat="server" CausesValidation="False" CssClass="button"
                                Text="Preview" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" OnClick="btnCancel_Click" /></td>
                    </tr>
                </table>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:HiddenField ID="h_print" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
