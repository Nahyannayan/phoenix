<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FeeGroupProformaInvoice.aspx.vb" Inherits="Fee_FeeGroupProformaInvoice"
    Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %> 
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
    function CheckForPrint() {
        if (document.getElementById('<%= h_print.ClientID %>').value != '') {
        document.getElementById('<%= h_print.ClientID %>').value = '';
           showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
       }
}
    );
   function ChangeAllCheckBoxStates(checkState) {
       var chk_state = document.getElementById("chkAL").checked;
       for (i = 0; i < document.forms[0].elements.length; i++) {
           if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
               if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                   if (document.forms[0].elements[i].type == 'checkbox') {
                       document.forms[0].elements[i].checked = chk_state;
                   }
               }
           }
       }
   }

   function GetCompany() {

       var NameandCode;
       var result;
       var url = "../Common/popupForm.aspx?ID=COMPANY&multiSelect=false";
       result = radopen(url, "pop_up")
       <%-- if (result != '' && result != undefined) {
           NameandCode = result.split('___');
           document.getElementById('<%=h_Company_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtCompanyDescr.ClientID %>').value = NameandCode[1];
                return true;
            }
            else {
                return false;
            }--%>
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Company_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtCompanyDescr.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtCompanyDescr.ClientID%>', 'TextChanged');
            }
        }

    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <%-- <tr class="subheader_BlueTableView" valign="top">
            <th align="left" valign="middle" colspan="4">
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </th>
        </tr>--%>
                    <tr>
                        <td align="left" ><span class="field-label">Company</span>
                        </td>
                        <td align="left" >
                            <asp:TextBox ID="txtCompanyDescr" runat="server" AutoPostBack="true" OnTextChanged="txtCompanyDescr_TextChanged" ></asp:TextBox>
                            <asp:ImageButton ID="imgCompany" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetCompany(); return false;"  />
                        </td>
                        <td align="right">
                            <asp:RadioButtonList ID="rblFilter" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="NG">Not Generated</asp:ListItem>
                                <asp:ListItem Value="G">Generated</asp:ListItem>
                                <asp:ListItem Value="A">All</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" width="100%" colspan="4">
                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row"
                                 AllowPaging="True" PageSize="20">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>
                                            <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkControl" runat="server" type="checkbox" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col1" SortExpression="Col1">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol1" runat="server" Text='<%# Bind("Col1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol1" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col1"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol1" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch1" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col2" SortExpression="Col2">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol2" runat="server" Text='<%# Bind("Col2") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol2" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col2"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol2" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col3" SortExpression="Col3">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol3" runat="server" Text='<%# Bind("Col3") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol3" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col3"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol3" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch3" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col4" SortExpression="Col4">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol4" runat="server" Text='<%# Bind("Col4") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol4" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col4"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol4" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch4" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col5" SortExpression="Col5">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol5" runat="server" Text='<%# Bind("Col5") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol5" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col5"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol5" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch5" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col6" SortExpression="Col6">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol6" runat="server" Text='<%# Bind("Col6") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol6" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col6"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol6" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch6" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblPrintReciept" runat="server" OnClick="lblPrintReciept_Click">Print</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Print Invoice</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <table style="width: 100%;">
                                <tr>                                 
                                    <td align="center" >
                                         <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" Text="Select All"
                                            Visible="False" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Generate Invoice" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" />
                                         <asp:HiddenField ID="h_Company_ID" runat="server" />
                                        <asp:HiddenField ID="h_print" runat="server" />
                                    </td>                                    
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                </table>



                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_SelectedId" runat="server" type="hidden" value="0" />
            </div>
        </div>
    </div>
</asp:Content>
