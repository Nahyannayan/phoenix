Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data


Partial Class Fees_FeeProcessFlow
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvFeeDetails.Attributes.Add("bordercolor", "#1b80b6")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_PROCESS_FLOW Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            h_BSUID.Value = Session("sbsuid")
            txtProcessName.Attributes.Add("ReadOnly", "ReadOnly")

            FillACD()
            ddlFeeType.DataSource = feeProcessFlow.PopulateFeeMaster(h_BSUID.Value, ddlAcademicYear.SelectedValue)
            ddlFeeType.DataTextField = "FEE_DESCR"
            ddlFeeType.DataValueField = "FEE_ID"
            ddlFeeType.DataBind()

            If ViewState("datamode") = "view" Then
                Dim PROC_ID As Integer = IIf(IsNumeric(Encr_decrData.Decrypt(Request.QueryString("PROC_ID").Replace(" ", "+"))), _
                Encr_decrData.Decrypt(Request.QueryString("PROC_ID").Replace(" ", "+")), -1)
                Dim ACD_ID As Integer = IIf(IsNumeric(Encr_decrData.Decrypt(Request.QueryString("ACD_ID").Replace(" ", "+"))), _
                Encr_decrData.Decrypt(Request.QueryString("ACD_ID").Replace(" ", "+")), -1)
                Dim ACY_ID As Integer
                Dim PROC_DESCR As String = String.Empty
                h_PROCESSID.Value = PROC_ID
                Session("sFEE_PROC_FLOW") = feeProcessFlow.GetFeeProcess(PROC_ID, Session("sBSUID"), _
                ACD_ID, ConnectionManger.GetOASIS_FEESConnection, PROC_DESCR, ACY_ID, False)
                GridBindFeeProcess()
                txtProcessName.Text = PROC_DESCR
                ddlAcademicYear.ClearSelection()
                ddlAcademicYear.SelectedValue = ACD_ID
                ddlAcademicYear.Enabled = False
                imgProcess.Enabled = False
                gvFeeDetails.Columns(3).Visible = False
            End If
            UpdateSession()
        End If
        PopulateAcademicYearBulletList()
    End Sub


    Protected Sub lnkbtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnAdd.Click
        Dim httable As New Hashtable
        If Not Session("sFEE_PROC_FLOW") Is Nothing Then
            httable = Session("sFEE_PROC_FLOW")
        End If
        If Not feeProcessFlow.DuplicateEntry(httable, ddlFeeType.SelectedValue) Then
            Dim vProcessFlow As New feeProcessFlow
            vProcessFlow.FEE_ID = ddlFeeType.SelectedValue
            vProcessFlow.FEE_TYPE = ddlFeeType.SelectedItem.Text
            vProcessFlow.FPB_PRB_ID = h_PROCESSID.Value
            vProcessFlow.BSU_ID = h_BSUID.Value
            vProcessFlow.Required = chkmandatory.Checked
            vProcessFlow.bEdited = True
            vProcessFlow.Insert(httable)
        Else
            'lblError.Text = "This Fee Type is Duplicated..."
            usrMessageBar.ShowNotification("This Fee Type is Duplicated...", UserControls_usrMessageBar.WarningType.Information)
            Return
        End If
        Session("sFEE_PROC_FLOW") = httable
        GridBindFeeProcess()
    End Sub

    Private Sub GridBindFeeProcess()
        Dim httable As New Hashtable
        If Not Session("sFEE_PROC_FLOW") Is Nothing Then
            httable = Session("sFEE_PROC_FLOW")
        End If
        gvFeeDetails.DataSource = feeProcessFlow.GetDataTableFromObjects(httable)
        gvFeeDetails.DataBind()
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim httable As New Hashtable
        Dim vfeeP As feeProcessFlow
        If Not Session("sFEE_PROC_FLOW") Is Nothing Then
            httable = Session("sFEE_PROC_FLOW")
        End If
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        If Not lblFEE_ID Is Nothing Then
            vfeeP = httable(CInt(lblFEE_ID.Text))
            If Not vfeeP Is Nothing Then
                vfeeP.Delete()
                Session("sFEE_PROC_FLOW") = httable
                GridBindFeeProcess()
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim httable As New Hashtable
        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim trans As SqlTransaction
        If Not Session("sFEE_PROC_FLOW") Is Nothing Then
            httable = Session("sFEE_PROC_FLOW")
            trans = conn.BeginTransaction("FEE_PROCESS_FLOW")
            Dim retVal As Integer = feeProcessFlow.SaveProcessFlow(httable, trans, conn)
            If retVal > 0 Then
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(retVal)
                usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                trans.Commit()
                'lblError.Text = "Data updated Successfully"
                usrMessageBar.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                ClearDetails()
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim str_KEY As String = "INSERT"
                If ViewState("datamode") <> "edit" Then
                    str_KEY = "EDIT"
                End If
            End If
        End If
    End Sub

    Private Sub ClearDetails()
        h_BSUID.Value = Session("sBSUID")
        h_PROCESSID.Value = ""
        txtProcessName.Text = ""
        ddlAcademicYear.ClearSelection()
        FillACD()
        ddlFeeType.ClearSelection()
        Session("sFEE_PROC_FLOW") = Nothing
        GridBindFeeProcess()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        gvFeeDetails.Columns(3).Visible = True
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        UpdateSession()
        UtilityObj.beforeLoopingControls(Me.Page)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        gvFeeDetails.Columns(3).Visible = True
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        imgProcess.Enabled = True
        ddlAcademicYear.Enabled = True
        FillACD()
        UpdateSession()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateAcademicYearBulletList()
        UpdateSession()
        ddlFeeType.DataSource = feeProcessFlow.PopulateFeeMaster(h_BSUID.Value, ddlAcademicYear.SelectedValue)
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()
    End Sub

    Private Sub UpdateSession()
        Dim PROC_ID As Integer = IIf(h_PROCESSID.Value <> "", h_PROCESSID.Value, 0)
        Session("sFEE_PROC_FLOW") = feeProcessFlow.GetFeeProcess(PROC_ID, Session("sBSUID"), _
        ddlAcademicYear.SelectedValue, ConnectionManger.GetOASIS_FEESConnection, String.Empty, 0, True)
    End Sub

    Private Sub PopulateAcademicYearBulletList()
        trViewFeeCollections.Nodes.Clear()
        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim sql_query As String = "select * from dbo.vw_OSO_FEE_PROCESS_FLOW_TREE WHERE BSU_ID ='" & _
        Session("sBSUID") & "' AND PRB_ACD_ID = " & ddlAcademicYear.SelectedValue
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        If dsData Is Nothing Or dsData.Tables(0).Rows.Count <= 0 Then
            Return
        End If
        Dim dtTable As DataTable = dsData.Tables(0)
        ' PROCESS Filter
        Dim dvPRO_DESCRIPTION As New DataView(dtTable, "", "PRB_STG_ORDER", DataViewRowState.OriginalRows)
        Dim trNodeAcademicYear As New TreeNode("Academic Year - " & ddlAcademicYear.SelectedItem.Text)
        Dim ienumPROCESSES As IEnumerator = dvPRO_DESCRIPTION.GetEnumerator
        Dim drPROCESS As DataRowView
        While (ienumPROCESSES.MoveNext())
            'Processes List
            drPROCESS = ienumPROCESSES.Current

            Dim trNodePROCESSES As New TreeNode(drPROCESS("PRO_DESCRIPTION"))
            Dim ienumAcademicYear As IEnumerator = trNodeAcademicYear.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumAcademicYear.MoveNext())
                If ienumAcademicYear.Current.Text = drPROCESS("PRO_DESCRIPTION") Then
                    contains = True
                End If
            End While
            'If trNodeAcademicYear.ChildNodes.Contains(trNodePROCESSES) Then
            '    Continue While
            'End If
            If contains Then
                Continue While
            End If

            Dim strPROCESS_FEE As String = "PRO_DESCRIPTION = '" & _
            drPROCESS("PRO_DESCRIPTION") & "'"
            Dim dvFEE_DESCR As New DataView(dtTable, strPROCESS_FEE, "FEE_DESCR", DataViewRowState.OriginalRows)
            Dim ienumFEE_DESCR As IEnumerator = dvFEE_DESCR.GetEnumerator
            While (ienumFEE_DESCR.MoveNext())
                Dim drFEE_DESCR As DataRowView = ienumFEE_DESCR.Current
                Dim trNodeFEE_DESCR As New TreeNode(drFEE_DESCR("FEE_DESCR"))
                trNodePROCESSES.ChildNodes.Add(trNodeFEE_DESCR)
            End While
            trNodeAcademicYear.ChildNodes.Add(trNodePROCESSES)
        End While

        trViewFeeCollections.Nodes.Add(trNodeAcademicYear)
        trViewFeeCollections.DataBind()
    End Sub

    Protected Sub gvFeeDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFeeDetails.RowDataBound
        Dim lblbRequired As Label = e.Row.FindControl("lblbRequired")
        If Not lblbRequired Is Nothing Then
            If lblbRequired.Text.ToUpper = "TRUE" Then
                lblbRequired.Text = "YES"
            Else
                lblbRequired.Text = "NO"
            End If
        End If
    End Sub

    Protected Sub imgProcess_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgProcess.Click
        If h_PROCESSID.Value <> ViewState("PROCESS_ID") Then
            ViewState("PROCESS_ID") = h_PROCESSID.Value
            ClearProcessDetails()
            GetViewData(h_PROCESSID.Value)
        End If
    End Sub

    Private Sub GetViewData(ByVal PROC_ID As Integer)
        Session("sFEE_PROC_FLOW") = feeProcessFlow.GetFeeProcess(PROC_ID, Session("sBSUID"), _
        ddlAcademicYear.SelectedValue, ConnectionManger.GetOASIS_FEESConnection, String.Empty, 0, False)
        GridBindFeeProcess()
    End Sub

    Private Sub ClearProcessDetails()
        ddlFeeType.ClearSelection()
        Session("sFEE_PROC_FLOW") = Nothing
        GridBindFeeProcess()
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(h_BSUID.Value)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
End Class
