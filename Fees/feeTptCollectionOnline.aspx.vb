﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Collections.Generic
Imports System.Linq
Imports RestSharp
Imports Newtonsoft.Json.Linq
Imports System.Web.Script.Serialization
Imports System.Net

Partial Class Fees_feeTptCollectionOnline
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "F300206" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case "F300206"
                            lblHead.Text = "Transport Online Fee Collection"
                    End Select
                    ddlBusinessunit.DataBind()
                    GET_COOKIE()
                    gridbind()
                End If
                'btnOkay.Attributes.Add("onClick", "return disableBtn('" & btnOkay.ClientID & "');")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub
    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        h_FCO_print.Value = ""
        gridbind()
    End Sub
    Private Sub GET_COOKIE()
        If Not Request.Cookies("OASIS_SETTINGS") Is Nothing Then
            ddlBusinessunit.SelectedValue = Request.Cookies("OASIS_SETTINGS")("TPTBSUID")
        End If
    End Sub
    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        gridbind()
    End Sub
    Protected Sub lblViewCollection_Click(sender As Object, e As EventArgs)
        Try
            Dim lblFCO_ID As Label = sender.Parent.parent.findcontrol("lblFCO_FCO_ID")
            'Session("ReportSource") = FeeCollection.PrintReceipt(lblReceipt.Text, Session("sBsuid"), rbEnquiry.Checked, Session("sUsr_name"), False)
            SetVersionforFCOReceipt(lblFCO_ID.Text)
            h_FCO_print.Value = "?type=REC_FCO&id=" + Encr_decrData.Encrypt(lblFCO_ID.Text) &
              "&bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) &
              "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) &
              "&isenq=" & Encr_decrData.Encrypt(True) &
              "&iscolln=" & Encr_decrData.Encrypt(False) & "&isexport=1"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub
    Private Sub SetVersionforFCOReceipt(ByVal FCO_ID As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FCO_BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = Session("sbsuid")
        pParms(1) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = FCO_ID

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.GET_FCO_FEERECEIPT", pParms)

        Dim BSU_CURRENCY As String = ""
        Dim BSU_bBSU_Version_Enabled As Boolean
        BSU_bBSU_Version_Enabled = False
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Columns.Contains("BSU_bBSU_Version_Enabled") Then
                BSU_bBSU_Version_Enabled = ds.Tables(0).Rows(0)("BSU_bBSU_Version_Enabled")
                If BSU_bBSU_Version_Enabled Then
                    Dim BSU_LOGO_VER_NO As String, BSU_VER_NO As String
                    BSU_LOGO_VER_NO = "0"
                    BSU_VER_NO = "0"
                    BSU_LOGO_VER_NO = CInt(ds.Tables(0).Rows(0)("BSU_LOGO_VER_NO").ToString)
                    BSU_VER_NO = CInt(ds.Tables(0).Rows(0)("BSU_VER_NO").ToString)
                    Dim sqlParam(1) As SqlParameter
                    sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
                    sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_VER_NO", BSU_VER_NO, SqlDbType.Int)
                    Dim dtBSUDetail As New DataTable
                    dtBSUDetail = Mainclass.getDataTable("SP_GET_BUSINESSUNIT_DETAILS", sqlParam, ConnectionManger.GetOASISConnectionString)

                End If
            End If
        End If
    End Sub

    Protected Sub lbFetchOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        preOrder.InnerHtml = "<p>No Data</p>"
        h_popup.Value = ""
        Dim lblFCO_FCO_ID As Label = sender.Parent.parent.findcontrol("lblFCO_FCO_ID")
        Dim lblGatewayType As Label = sender.Parent.parent.findcontrol("lblGatewayType")

        If Not lblFCO_FCO_ID Is Nothing AndAlso Not lblGatewayType Is Nothing Then
            Dim objcls As New ClsPayment
            If lblGatewayType.Text = "MPGS" Then
                objcls.GET_PAYMENT_REDIRECT("TRANSPORT", lblFCO_FCO_ID.Text, Session("sBsuid"))
                Dim recno As String = "", msgServer As String = "", retval As String = "", FetchOrderURL As String = ""
                Try
                    FetchOrderURL = objcls.API_URL.Replace("session", "order/") & objcls.MERCHANT_TXN_REF_ID
                    Dim client = New RestClient(FetchOrderURL)
                    Dim Request = New RestRequest(Method.GET)
                    Request.AddHeader("cache-control", "no-cache")
                    Request.AddHeader("Accept-Encoding", "gzip, deflate")
                    Request.AddHeader("Host", "ap-gateway.mastercard.com")
                    Request.AddHeader("Cache-Control", "no-cache")
                    Request.AddHeader("Accept", "*/*")
                    Request.AddHeader("Authorization", "Basic " & Convert.ToBase64String(Encoding.ASCII.GetBytes("merchant." & objcls.MERCHANT_ID & ":" & objcls.API_PASSWORD)))
                    Request.AddHeader("Content-Type", "application/json")
                    ServicePointManager.Expect100Continue = True
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    Dim Response As IRestResponse = client.Execute(Request)
                    Dim jsonResponse = JObject.Parse(Response.Content)
                    Dim jsonReq = New JavaScriptSerializer().Serialize(jsonResponse)
                    preOrder.InnerHtml = jsonResponse.ToString()
                    h_popup.Value = "print"
                    'Dim ScriptName As String = "POP"
                    'Dim Script As New StringBuilder
                    'Script.Append("alert('here');")
                    'If Not ClientScript.IsStartupScriptRegistered(ScriptName) Then
                    '    ClientScript.RegisterClientScriptBlock(Me.GetType(), ScriptName, Script.ToString(), True)
                    'End If
                Catch ex As Exception

                End Try
            Else
                GetMIGSOrderDetails(lblFCO_FCO_ID.Text)
            End If
        End If
    End Sub

    Private Sub GetMIGSOrderDetails(ByVal pFCO_FCO_ID As String)
        Dim strResponse As String = "", User As String = "", Password As String = "", Accesscode As String = "", MerchantID As String = ""
        Dim objcls As New ClsPayment
        objcls.GET_MIGS_QUERY_DR_INFO(pFCO_FCO_ID, Session("sBsuId"))
        Dim myWebClient As New System.Net.WebClient
        Dim ValueCollection As New System.Collections.Specialized.NameValueCollection
        ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_User"), System.Web.HttpUtility.UrlEncode(objcls.BSU_QUERYDRUSER))
        ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Password"), System.Web.HttpUtility.UrlEncode(objcls.BSU_QUERYDRPASSWORD))
        ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_AccessCode"), System.Web.HttpUtility.UrlEncode(objcls.BSU_MERCHANTCODE))
        ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Merchant"), System.Web.HttpUtility.UrlEncode(objcls.MERCHANT_ID))
        ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Command"), System.Web.HttpUtility.UrlEncode("queryDR"))
        ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_MerchTxnRef"), System.Web.HttpUtility.UrlEncode(pFCO_FCO_ID))
        ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Version"), System.Web.HttpUtility.UrlEncode("1"))
        ServicePointManager.Expect100Continue = True
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
        Dim responseArray As Byte() = myWebClient.UploadValues("https://migs.mastercard.com.au/vpcdps", "POST", ValueCollection)
        strResponse = System.Text.Encoding.ASCII.GetString(responseArray)
        Dim params As New Hashtable
        splitResponse(strResponse, params)
        If params.Count > 0 Then
            preOrder.InnerHtml = "<html>"
            For Each entry As DictionaryEntry In params
                preOrder.InnerHtml &= "<br />" & entry.Key.ToString & " = " & entry.Value.ToString() & ""
            Next
            preOrder.InnerHtml &= "<br /><hr><br />Note: <span style='background:yellow;font-weight:bold'>If vpc_FoundMultipleDRs='N',vpc_DRExists='Y',vpc_AcqResponseCode='00',vpc_TxnResponseCode='0' means it is a valid payment</span>"
            preOrder.InnerHtml &= "</html>"
        End If
        h_popup.Value = "print"

    End Sub
    Public Shared Sub splitResponse(ByVal data As String, ByRef params As Hashtable)
        Dim pairs As String()
        Dim equalsIndex As Integer
        Dim name As String
        Dim value As String
        ' Check if there was a response
        If Len(data) > 0 Then
            ' Check if there are any paramenters in the response
            If InStr(data, "=") > 0 Then
                ' Get the parameters out of the response
                pairs = Split(data, "&")
                For Each pair As String In pairs
                    ' If there is a key/value pair in this item then store it
                    equalsIndex = InStr(pair, "=")
                    If equalsIndex > 1 And Len(pair) > equalsIndex Then
                        name = Left(pair, equalsIndex - 1)
                        value = Right(pair, Len(pair) - equalsIndex)
                        params.Add(name, System.Web.HttpUtility.UrlEncode(value))
                    End If
                Next
            Else ' There were no parameters so create an error
                params.Add("vpc_Message", "The data contained in the response was invalid or corrupt, the data is: <pre>" & data & "</pre>")
            End If
        Else ' There was no data so create an error
            params.Add("vpc_Message", "There was no data contained in the response")
        End If

    End Sub
    Private Shared Function null2unknown(ByVal req As Object) As String
        Try
            If req Is Nothing Then
                Return String.Empty
            Else
                Return System.Web.HttpUtility.UrlDecode(req.ToString().Trim)
            End If
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStatus.SelectedIndexChanged
        h_FCO_print.Value = ""
        gridbind()
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        h_FCO_print.Value = ""
        gridbind()
    End Sub
    Protected Sub GenerateReceipt_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblFCO_ID As Label = sender.Parent.parent.findcontrol("lblFCO_FCO_ID")
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SOURCE", SqlDbType.VarChar, 20)
            pParms(0).Value = "TRANSPORT"
            pParms(1) = New SqlClient.SqlParameter("@FCO_FCO_ID", SqlDbType.BigInt)
            pParms(1).Value = Convert.ToInt32(lblFCO_ID.Text)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "FEES.RESET_MPGS_TRY_COUNT", pParms)
            gridbind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub lbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
            h_print.Value = "?type=REC&id=" + Encr_decrData.Encrypt(lblReceipt.Text) &
            "&bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) &
            "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) &
            "&stu_bsu_id=" & Encr_decrData.Encrypt(ddlBusinessunit.SelectedItem.Value) &
            "&isexport=1" &
            "&isDuplicate=1"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub gridbind()
        Try
            Dim ds As New DataSet
            Dim lstrOpr As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim str_Filter As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim lstrCondn6 As String = String.Empty
            Dim lstrCondn7 As String = String.Empty
            Dim lstrCondn8 As String = String.Empty
            Dim lstrCondn9 As String = String.Empty
            Dim lstrCondn10 As String = String.Empty
            Dim lstrCondn11 As String = String.Empty
            Dim lstrCondn12 As String = String.Empty
            Dim lstrCondn13 As String = String.Empty
            Dim lstrCondn14 As String = String.Empty
            Dim lstrCondn15 As String = String.Empty
            Dim lstrCondn16 As String = String.Empty
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtReceiptno FCL_RECNO
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCO_FCL_RECNO", lstrCondn1)

                '   -- 2  txtDate
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCO_DATE", lstrCondn2)

                '   -- 3  txtGrade
                larrSearchOpr = h_selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRD_DISPLAY", lstrCondn3)

                '   -- 4   txtStuno
                larrSearchOpr = h_selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn4)

                '   -- 5  txtStuname
                larrSearchOpr = h_selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn5)

                '   -- 6  txtAmount
                larrSearchOpr = h_selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
                lstrCondn6 = txtSearch.Text
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCO_AMOUNT", lstrCondn6)

                '   -- 7  txtDesc
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
                lstrCondn7 = txtSearch.Text
                If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCO_NARRATION", lstrCondn7)

                '   -- 8  txtPGateway
                larrSearchOpr = h_selected_menu_8.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtPaymentGateway")
                lstrCondn8 = txtSearch.Text
                If (lstrCondn8 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "CPM_DESCR", lstrCondn8)


                '   -- 9 txtMerchantID
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtMerchantID")
                lstrCondn9 = txtSearch.Text
                If (lstrCondn9 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MERCHANTID", lstrCondn9)
                '   -- 10 txtSourcePortal
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtSourcePortal")
                lstrCondn10 = txtSearch.Text
                If (lstrCondn10 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "SOURCE_PORTAL", lstrCondn10)
                '   -- 11 txtGateWayType
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGateWayType")
                lstrCondn11 = txtSearch.Text
                If (lstrCondn11 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GATEWAY_TYPE", lstrCondn11)



                '   -- 12 txtTryCount
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtTryCount")
                lstrCondn12 = txtSearch.Text
                If (lstrCondn12 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCO_TRYCOUNT", lstrCondn12)



                '   -- 13 txtLastTryDate
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtLastTryDate")
                lstrCondn13 = txtSearch.Text
                If (lstrCondn13 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "LASTTRYDATE", lstrCondn13)


                '   -- 14 txtSrcRefType
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtSrcRefType")
                lstrCondn14 = txtSearch.Text
                If (lstrCondn14 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "SRC_REF_TYPE", lstrCondn14)


                '   -- 14 txtFcoFcoID
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtFcoFcoID")
                lstrCondn15 = txtSearch.Text
                If (lstrCondn15 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCO_FCO_ID", lstrCondn15)
                '   -- 16 txtpaymode

                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtpaymode")
                lstrCondn16 = txtSearch.Text
                If (lstrCondn16 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCO_PAY_MODE", lstrCondn16)


            End If
            Dim STR_RECEIPT As String = "TRANSPORT.VW_OSO_FEES_RECEIPT_ONLINE"
            'If rbEnquiry.Checked Then
            '    STR_RECEIPT = "FEES.VW_OSO_FEES_RECEIPT_ENQUIRY"
            'End If
            str_Sql = "SELECT * FROM " & STR_RECEIPT & " where  FCO_BSU_ID='" & Session("sBSUID") & "'" _
            & " AND isnull(FCO_bDELETED,0)=0 AND FCO_STATUS='" & ddlStatus.SelectedItem.Value & "' AND FCO_STU_BSU_ID='" & ddlBusinessunit.SelectedItem.Value & "'" & str_Filter _
            & " ORDER BY FCO_DATE DESC ,FCO_FCL_RECNO DESC,STU_NAME"

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_Sql)

            Dim columns As List(Of DataControlField) = gvJournal.Columns.Cast(Of DataControlField)().ToList()

            If (ddlStatus.SelectedItem.Value = "INITIATED") Then
                columns.Find(Function(col) col.HeaderText = "Merchant ID").Visible = True
                columns.Find(Function(col) col.HeaderText = "Source Portal").Visible = True
                columns.Find(Function(col) col.HeaderText = "GateWay Type").Visible = True
                columns.Find(Function(col) col.HeaderText = "Last Try Date").Visible = True
                columns.Find(Function(col) col.HeaderText = "Src Ref Type").Visible = True
                columns.Find(Function(col) col.HeaderText = "Payment RefId").Visible = True
                columns.Find(Function(col) col.HeaderText = "View Collection").Visible = False
                columns.Find(Function(col) col.HeaderText = "Docno").Visible = False
                columns.Find(Function(col) col.HeaderText = "Print Receipt").Visible = False
                columns.Find(Function(col) col.HeaderText = "CollDate").Visible = False
                columns.Find(Function(col) col.HeaderText = "Pay Mode").Visible = True
            Else
                columns.Find(Function(col) col.HeaderText = "CollDate").Visible = True
                columns.Find(Function(col) col.HeaderText = "Docno").Visible = True
                columns.Find(Function(col) col.HeaderText = "Print Receipt").Visible = False
                columns.Find(Function(col) col.HeaderText = "Merchant ID").Visible = False
                columns.Find(Function(col) col.HeaderText = "Source Portal").Visible = True
                columns.Find(Function(col) col.HeaderText = "GateWay Type").Visible = True
                columns.Find(Function(col) col.HeaderText = "Last Try Date").Visible = False
                columns.Find(Function(col) col.HeaderText = "Src Ref Type").Visible = False
                columns.Find(Function(col) col.HeaderText = "Payment RefId").Visible = True
                columns.Find(Function(col) col.HeaderText = "View Collection").Visible = False
                columns.Find(Function(col) col.HeaderText = "Pay Mode").Visible = True
            End If
            columns.Find(Function(col) col.HeaderText = "View OrderInfo").Visible = False
            If (Not Session("sBITSupport") Is Nothing AndAlso Convert.ToBoolean(Session("sBITSupport")) = True) Or
               (Not Session("sroleid") Is Nothing AndAlso (Session("sroleid")) = 204) Then
                columns.Find(Function(col) col.HeaderText = "View OrderInfo").Visible = True
                columns.Find(Function(col) col.HeaderText = "Generate Receipt").Visible = True
            End If
            If (ddlStatus.SelectedItem.Value = "SUCCESS") Then
                columns.Find(Function(col) col.HeaderText = "Generate Receipt").Visible = False
                columns.Find(Function(col) col.HeaderText = "Print Receipt").Visible = True
                columns.Find(Function(col) col.HeaderText = "Try Count").Visible = False
            End If

            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If

            txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn5

            txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = lstrCondn6

            txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
            txtSearch.Text = lstrCondn7

            txtSearch = gvJournal.HeaderRow.FindControl("txtPaymentGateway")
            txtSearch.Text = lstrCondn8

            txtSearch = gvJournal.HeaderRow.FindControl("txtMerchantID")
            txtSearch.Text = lstrCondn9

            txtSearch = gvJournal.HeaderRow.FindControl("txtSourcePortal")
            txtSearch.Text = lstrCondn10

            txtSearch = gvJournal.HeaderRow.FindControl("txtGateWayType")
            txtSearch.Text = lstrCondn11

            txtSearch = gvJournal.HeaderRow.FindControl("txtTryCount")
            txtSearch.Text = lstrCondn12

            txtSearch = gvJournal.HeaderRow.FindControl("txtLastTryDate")
            txtSearch.Text = lstrCondn13

            txtSearch = gvJournal.HeaderRow.FindControl("txtSrcRefType")
            txtSearch.Text = lstrCondn14

            txtSearch = gvJournal.HeaderRow.FindControl("txtFcoFcoID")
            txtSearch.Text = lstrCondn15

            txtSearch = gvJournal.HeaderRow.FindControl("txtpaymode")
            txtSearch.Text = lstrCondn16


        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
End Class
