Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class FEEReminderView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            gvFEEConcessionDet.Attributes.Add("bordercolor", "#1b80b6")
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif" 
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_REMINDER Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                hlAddNew.NavigateUrl = "FeeReminderWithAging.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEConcessionDet.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String 
            pControl = pImg
            Try
                s = gvFEEConcessionDet.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim ds As New DataSet
            Dim str_Filter As String = String.Empty
            Dim lstrOpr As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim larrSearchOpr() As String 
            Dim txtSearch As New TextBox  
            If gvFEEConcessionDet.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtAcd_year
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtAcd_year")
                lstrCondn1 = Trim(txtSearch.Text.Trim)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ACY_DESCR", lstrCondn1)

                '   -- 1  txtAsOnDate
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtAsOnDate")
                lstrCondn2 = txtSearch.Text.Trim
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FRH_ASONDATE", lstrCondn2)

                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRemarks")
                lstrCondn3 = txtSearch.Text.Trim
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FRH_REMARKS", lstrCondn3)

                '   -- 3   txtTDate
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtFrom")
                lstrCondn4 = txtSearch.Text.Trim
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FCH_DT", lstrCondn4)

                '   -- 5  city
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtLevel")
                lstrCondn5 = txtSearch.Text.Trim
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FRH_Level", lstrCondn5)
            End If
            Dim str_cond As String = String.Empty

            str_Sql = "SELECT     ACM.ACY_DESCR, FEH.FRH_DT, FEH.FRH_ASONDATE, FEH.FRH_ID, REM.RMD_DESCR AS FRH_Level, " & _
            " FEH.FRH_REMARKS, GRD.GRD_DISPLAY " & _
            " FROM  SECTION_M AS SEC INNER JOIN " & _
            " FEES.FEE_REMINDER_D AS FED ON SEC.SCT_ID = FED.FRD_SCT_ID INNER JOIN " & _
            " GRADE_M AS GRD ON SEC.SCT_GRD_ID = GRD.GRD_ID INNER JOIN " & _
            " OASIS.dbo.ACADEMICYEAR_D AS ADC INNER JOIN " & _
            " OASIS.dbo.ACADEMICYEAR_M AS ACM ON ADC.ACD_ACY_ID = ACM.ACY_ID INNER JOIN  " & _
            " FEES.FEE_REMINDER_H AS FEH ON ADC.ACD_ID = FEH.FRH_ACD_ID INNER JOIN  " & _
            " REMINDERTYPE_M AS REM ON REM.RMD_ID = FEH.FRH_Level ON FED.FRD_FRH_ID = FEH.FRH_ID  " & _
            " AND FED.FRD_ACD_ID = FEH.FRH_ACD_ID  " & _
            " INNER JOIN   OASIS..STUDENTS ON FRD_STU_ID=STU_ID " & _
            " WHERE FRH_BSU_ID='" & Session("sBsuid") & "' " & _
            " AND STU_NO LIKE '%" & txtStuNoSearch.Text & "%' AND NAME LIKE '%" & txtstuNameSearch.Text & "%'" & str_Filter & _
            " GROUP BY ACM.ACY_DESCR, FEH.FRH_DT, FEH.FRH_ASONDATE, FEH.FRH_ID, REM.RMD_DESCR, FEH.FRH_REMARKS, GRD.GRD_DISPLAY "
            Dim str_orderby As String = " ORDER BY FRH_ID DESC,FRH_ASONDATE DESC"

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql & str_orderby)
            gvFEEConcessionDet.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEConcessionDet.DataBind()
                Dim columnCount As Integer = gvFEEConcessionDet.Rows(0).Cells.Count
                gvFEEConcessionDet.Rows(0).Cells.Clear()
                gvFEEConcessionDet.Rows(0).Cells.Add(New TableCell)
                gvFEEConcessionDet.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEConcessionDet.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEConcessionDet.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEConcessionDet.DataBind()
            End If

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtAcd_year")
            txtSearch.Text = lstrCondn1

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtAsOnDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn3

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtFrom")
            txtSearch.Text = lstrCondn4

            txtSearch = gvFEEConcessionDet.HeaderRow.FindControl("txtLevel")
            txtSearch.Text = lstrCondn5
 
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEConcessionDet.PageIndexChanging
        gvFEEConcessionDet.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFEEConcessionDet.RowDataBound
        Try
            Dim lblFRH_ID As New Label
            lblFRH_ID = TryCast(e.Row.FindControl("lblFRH_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblFRH_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "FEEReminderViewDetails.aspx?FRH_ID=" & Encr_decrData.Encrypt(lblFRH_ID.Text) & _
                "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

     Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Master.DisableScriptManager()
    End Sub

    Protected Sub btnSearchReminders_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchReminders.Click
        GridBind()
    End Sub
End Class
