<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeSetupforAcademicViewBB.aspx.vb" Inherits="Fees_feeSetupforAcademicViewBB" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
</script> 

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Transport Fees Setup(For Outsourced Units Only)
        </div>
        <div class="card-body">
            <div class="table-responsive">
    
 
    <table align="center" width="100%">
            <tr valign="top" >          
                <td valign="top" align="left">
                    <asp:HyperLink ID="hlAddnew" runat="server">Add New</asp:HyperLink>
                   <%-- <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>--%>
                    <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                </td>
            </tr>
        </table>
    <table align="center" width="100%">
            
        <tr>
            <td align="left" width="20%">
                <span class="field-label">Business Unit</span></td>
            <td align="left" width="30%">
                <asp:DropDownList ID="ddBusinessunit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                    DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" TabIndex="5" Width="326px">
                </asp:DropDownList></td>
        
            <td align="left" width="20%">
                <span class="field-label">Academic Year</span></td>
            <td align="left">
                <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" TabIndex="10">
                </asp:DropDownList></td>
        </tr>
            <tr>
                <td align="center" valign="top" colspan="4">
                    <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                        EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30" >
                        <Columns>  
                            <asp:TemplateField>
                                <HeaderTemplate>   
                                    Location<br />
                                    <asp:TextBox ID="txtAgegroup" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnAmtSearcha" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>   
                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("SBL_DESCRIPTION") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="From Date">
                                <HeaderTemplate>
                                   From Dt<br />
                                   <asp:TextBox ID="txtFDate" runat="server" Width="75%"></asp:TextBox>
                                   <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("FSP_FROMDT","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="150px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To Date">
                                <HeaderTemplate>
                                   Fee<br />
                                   <asp:TextBox ID="txtTDate" runat="server" Width="75%"></asp:TextBox>
                                   <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("FEE_DESCR") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                            </asp:TemplateField> 
                            <asp:BoundField DataField="FSP_AMOUNT" HeaderText="Amount" />
                            <asp:TemplateField HeaderText="View">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FEE_ID" Visible="False">
                                <EditItemTemplate> 
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("FSP_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView> 
                </td>
               
            </tr>
        </table> 
     <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
     <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
     <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" /> 
    <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>

            </div>
        </div>
    </div>

</asp:Content>
