<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeTransportAdjustment_REQ.aspx.vb" Inherits="Fees_FeeTransportAdjustment_REQ" Theme="General" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <script lang="javascript" type="text/javascript">
        Sys.Application.add_load(
        function CheckForPrint() {
            if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                document.getElementById('<%= h_print.ClientID %>').value = '';
                //showModelessDialog('../Reports/ASPX Report/RptViewerModalview.aspx', '', "dialogWidth: 850px; dialogHeight: 900px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                return ShowWindowWithClose('../Reports/ASPX Report/RptViewerModal.aspx', 'search', '55%', '85%')
                return false;
            }
        }
    );

        function FillDetailRemarks() {
            var HeaderRemarks;
            HeaderRemarks = document.getElementById('<%=txtHeaderRemarks.ClientID %>').value;
            if (HeaderRemarks != '') {
                document.getElementById('<%=txtDetRemarks.ClientID %>').value = HeaderRemarks;
            }
            return false;
        }
        function GetStudent() {
            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 750px; ";
            sFeatures += "dialogHeight: 475px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var selType = document.getElementById('<%=ddlAdjReason.ClientID %>').value;
            var selACD_ID = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            var STUD_TYP = document.getElementById('<%=radEnq.ClientID %>').checked;
            var url;
            if (STUD_TYP == true)
                url = "ShowStudentTransport.aspx?TYPE=TCENQ&VAL=" + selType + "&ACD_ID=" + selACD_ID + "&bsu=" + document.getElementById('<%= ddBusinessunit.ClientID %>').value;
            else
                url = "ShowStudentTransport.aspx?TYPE=TC&VAL=" + selType + "&ACD_ID=" + selACD_ID + "&bsu=" + document.getElementById('<%= ddBusinessunit.ClientID %>').value;

            //var url = "ShowStudent.aspx?TYPE=TC&VAL=" + selType + "&ACD_ID=" + selACD_ID;
            //result = window.showModalDialog(url, "", sFeatures)
            var oWnd = radopen(url, "pop_fee");
            <%--if (result != '' && result != undefined) {
                NameandCode = result.split('||');
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStudName.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtStdNo.ClientID %>').value = NameandCode[2];
                return false;
            }
            else {
                return false;
            }--%>
        }
        function TransportAttendanceView() {
            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 800px; dialogHeight: 675px; help: no; ";
            sFeatures += "resizable: no; scroll: yes; status: no; unadorned: no; ";
            var NameandCode;
            var result;
            var url = "feeTransportAttendanceView.aspx?stu=" + document.getElementById('<%=h_STUD_ID.ClientID %>').value + "&bsu=" + document.getElementById('<%= ddBusinessunit.ClientID %>').value;
            //result = window.showModalDialog(url, "", sFeatures)
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(arg.NameandCode);
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStudName.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtStdNo.ClientID %>').value = NameandCode[2];
                __doPostBack('<%= txtStudName.ClientID%>', 'TextChanged');
            }
        }
    </script>
    <style type="text/css">
        .divinfo {
            display: block;
            border: 1px solid #ccc;
            text-align: left;
            font-family: segoe ui, verdana, arial, sans-serif;
            font-size: 11px;
            font-weight: normal;
            color: #E9330C;
            letter-spacing: 1px;
            font-weight: 600;
            width: 1100px;
            background-position: 5px center;
            background: #feffb3 url(../images/Common/PageBody/info_s.png) no-repeat 2px 3px;
            padding-left: 22px;
            padding-top: 3pt;
            padding-bottom: 3pt;
        }
    </style>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_fee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Transport Fee Adjustments Request..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" width="95%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" SkinID="Error" />--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="SUBERROR" CssClass="error" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="MAINERROR" CssClass="error" />
                        </td>
                    </tr>
                </table>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddBusinessunit" runat="server" AutoPostBack="True"
                                DataSourceID="odsSERVICES_BSU_M" DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"
                                TabIndex="5">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="4"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Date  required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtDate">*</asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDate"
                                ErrorMessage="Date Required" ValidationGroup="SUBERROR">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Reason</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAdjReason" runat="server" AutoPostBack="True">
                                <asp:ListItem Value="1">Late Join</asp:ListItem>
                                <asp:ListItem Value="2">Withdrawal</asp:ListItem>
                                <asp:ListItem Value="3">Refund</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student</span></td>
                        <td align="left" colspan="3">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="30%">
                                        <asp:RadioButton ID="radStud" AutoPostBack="true" runat="server" GroupName="ENQ_STUD" Text="Student" Checked="True" />
                                        <asp:RadioButton ID="radEnq" AutoPostBack="true" runat="server"
                                            GroupName="ENQ_STUD" Text="Enquiry" Enabled="False" />
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtStdNo" runat="server" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgProcess" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="GetStudent(); return false;" OnClick="imgProcess_Click"
                                            Style="width: 16px"></asp:ImageButton>
                                    </td>
                                    <td align="left" width="40%">
                                        <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Student Name required"
                                            ValidationGroup="MAINERROR" ControlToValidate="txtStudName">*</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtStudName"
                                            ErrorMessage="Please select a Student" ValidationGroup="SUBERROR">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="30%">
                                        <span class="field-label">Grade </span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="40%">
                                        <asp:LinkButton ID="lbLedger" runat="server">Student Ledger</asp:LinkButton>
                                        <asp:LinkButton ID="lblAttendance" runat="server" OnClientClick="TransportAttendanceView();return false;">Attendance</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>



                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Area</span></td>
                        <td align="left" colspan="3">
                            <asp:Label ID="lblArea" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtHeaderRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Header remarks required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtHeaderRemarks">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg-lite" colspan="4">Fee Adjustments Details</td>
                    </tr>
                    <tr>
                        <td align="left" width="15%"><span class="field-label">Fee Type</span></td>
                        <td align="left" width="55%">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="40%">
                                        <asp:DropDownList ID="ddlFeeType" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblTaxCode" runat="server" Text="Tax Code" CssClass="field-label"></asp:Label>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlTAX" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td align="left" width="15%">
                            <asp:Label ID="lblTextEntered" runat="server" Text="Amount" CssClass="field-label"></asp:Label></td>
                        <td align="left" width="20%">
                            <asp:TextBox ID="txtDetAmount" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Amount required"
                                ValidationGroup="SUBERROR" ControlToValidate="txtDetAmount">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr id="trVATInfo" runat="server">
                        <td align="center" colspan="4">
                            <asp:Label ID="lblalert" runat="server" CssClass="divinfo"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtDetRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Detailed Remarks required"
                                ValidationGroup="SUBERROR" ControlToValidate="txtDetRemarks">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="SUBERROR" />
                            <asp:Button ID="btnSubCancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                    <tr id="tr_Deatails" runat="server">
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" Width="100%" CssClass="table table-bordered table-row" CellPadding="4">
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="FeeId">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fee Type">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFeeType" runat="server" Text='<%# bind("FEE_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="Duration">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDuration" runat="server" Text='<%# bind("DURATION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# bind("FEE_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Tax Code">
                                        <ItemStyle HorizontalAlign="left"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVatCode" runat="server" Text='<%# bind("FEE_TAX_CODE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Tax Amount">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVATAmount" runat="server" Text='<%# bind("FEE_TAX_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Net Amount">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNetAmount" runat="server" Text='<%# bind("FEE_NET_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# bind("FEE_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left">
                            <asp:CheckBox ID="chkPrintAfterSave" runat="server" Text="Print After Save" CssClass="field-label"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:Panel ID="Panel1" runat="server">
                    <asp:GridView ID="gvFeePaidHistory" runat="server" SkinID="GridViewNormal" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="FEE_DESCR" HeaderText="FEE Type" />
                            <asp:BoundField DataField="DOCDATE" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False" HeaderText="Date" />
                            <asp:BoundField DataField="PAID_AMT" HeaderText="Paid Amount" />
                            <asp:BoundField DataField="CHARGED_AMT" HeaderText="Charged Amt" />
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>

                <script type="text/javascript" lang="javascript">
                    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
                        $.fancybox({
                            type: 'iframe',
                            //maxWidth: 300,
                            href: gotourl,
                            //maxHeight: 600,
                            fitToView: true,
                            padding: 6,
                            width: w,
                            height: h,
                            autoSize: false,
                            openEffect: 'none',
                            showLoading: true,
                            closeClick: true,
                            closeEffect: 'fade',
                            'closeBtn': true,
                            afterLoad: function () {
                                this.title = '';//ShowTitle(pageTitle);
                            },
                            helpers: {
                                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                                title: { type: 'inside' }
                            },
                            onComplete: function () {
                                $("#fancybox-wrap").css({ 'top': '90px' });

                            },
                            onCleanup: function () {
                                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                                if (hfPostBack == "Y")
                                    window.location.reload(true);
                            }
                        });

                        return false;
                    }
                </script>
            </div>
        </div>
    </div>
</asp:Content>

