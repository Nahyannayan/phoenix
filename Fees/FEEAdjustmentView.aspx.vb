Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class FEEAdjustmentView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            gvFEEAdjustments.Attributes.Add("bordercolor", "#1b80b6")
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" OrElse (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ADJUSTMENTS _
                AndAlso ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ADJUSTMENTS_HEADTOHEAD) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS
                        lblHeader.Text = "Fee Adjustment"
                        'hlAddNew.NavigateUrl = "FeeAdjustment.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS_HEADTOHEAD
                        'hlAddNew.NavigateUrl = "FeeAdjustment_HeadtoHead.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                        lblHeader.Text = "Fee Adjustment(Between Heads)"
                End Select
                FillACD()
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEAdjustments.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvFEEAdjustments.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim str_Sql As String = String.Empty
            Dim lstrOpr As String = String.Empty
            Dim str_Filter As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim lstrCondn6 As String = String.Empty
            Dim ds As New DataSet
            Dim larrSearchOpr() As String
            Dim txtSearch As New TextBox
            If gvFEEAdjustments.Rows.Count > 0 Then
                ' --- Initialize The Variables
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtEmpNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtDocNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FAH_DOCNO", lstrCondn1)
                '  -- 1  txtEmpname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtstudname")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn2)
                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtDate")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FAH_DATE", lstrCondn3)
                '   -- 3   txtTDate
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtAcademicYear")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "AMOUNT", lstrCondn4)
                '   -- 5  city
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtRemarks")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FAH_REMARKS", lstrCondn5)
                '   -- 5  city
                larrSearchOpr = h_Selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtStudNo")
                lstrCondn6 = txtSearch.Text
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn6)

            End If
            Dim str_cond As String = " AND FAH_BSU_ID ='" & Session("sBSUID") & "' AND FAH_ACD_ID = " & ddlAcademicYear.SelectedValue
            Dim strGroupby As String = " GROUP BY FEES.FEEADJUSTMENT_H.FAH_ID, FEES.FEEADJUSTMENT_H.FAH_REMARKS, " & _
            " FEES.FEEADJUSTMENT_H.FAH_DATE, VW_OSO_STUDENT_M.STU_NO, VW_OSO_STUDENT_M.STU_NAME, " & _
            " FAH_DOCNO, VW_OSO_STUDENT_M.ACY_DESCR, FEES.FEEADJUSTMENT_H.FAH_BSU_ID, " & _
            " FAH_bInter, FAH_ACD_ID, FAH_bPosted, FEES.FEEADJREQUEST_H.FAR_DOCNO "
            If radStud.Checked Then
                str_Sql = "SELECT * FROM ( " & _
                "SELECT FEES.FEEADJUSTMENT_H.FAH_ID, FEES.FEEADJUSTMENT_H.FAH_REMARKS, " & _
                " ISNULL( FAH_bInter,0) AS FAH_bInter, FAH_ACD_ID, FAH_bPosted ," & _
                " FEES.FEEADJUSTMENT_H.FAH_DATE, VW_OSO_STUDENT_M.STU_NO, VW_OSO_STUDENT_M.STU_NAME,FAH_DOCNO, " & _
                " VW_OSO_STUDENT_M.ACY_DESCR, FEES.FEEADJUSTMENT_H.FAH_BSU_ID, " & _
                " SUM(FEES.FEEADJUSTMENT_S.FAD_AMOUNT) AMOUNT , FEES.FEEADJREQUEST_H.FAR_DOCNO " & _
                " FROM FEES.FEEADJUSTMENT_H INNER JOIN VW_OSO_STUDENT_M " & _
                " ON FEES.FEEADJUSTMENT_H.FAH_STU_ID = VW_OSO_STUDENT_M.STU_ID" & _
                " INNER JOIN FEES.FEEADJUSTMENT_S ON  " & _
                " FEES.FEEADJUSTMENT_H.FAH_ID = FEES.FEEADJUSTMENT_S.FAD_FAH_ID " & _
                " LEFT OUTER JOIN FEES.FEEADJREQUEST_H ON FEES.FEEADJUSTMENT_H.FAH_FAR_ID = FEES.FEEADJREQUEST_H.FAR_ID " & _
                " WHERE FEES.FEEADJUSTMENT_H.FAH_bDeleted =0 AND FEES.FEEADJUSTMENT_H.FAH_STU_TYPE = 'S' " & _
                strGroupby & ")A WHERE 1=1 " & str_Filter
            ElseIf radEnq.Checked Then
                str_Sql = "SELECT * FROM ( " & _
                " SELECT FEES.FEEADJUSTMENT_H.FAH_ID, FEES.FEEADJUSTMENT_H.FAH_REMARKS, " & _
               " FEES.FEEADJUSTMENT_H.FAH_DATE, VW_OSO_STUDENT_M.STU_NO, VW_OSO_STUDENT_M.STU_NAME,FAH_DOCNO,  " & _
               " VW_OSO_STUDENT_M.ACY_DESCR, FEES.FEEADJUSTMENT_H.FAH_BSU_ID, " & _
               " ISNULL( FAH_bInter,0) AS FAH_bInter, FAH_ACD_ID, FAH_bPosted, " & _
               " SUM(FEES.FEEADJUSTMENT_S.FAD_AMOUNT) AMOUNT, FEES.FEEADJREQUEST_H.FAR_DOCNO " & _
               " FROM FEES.FEEADJUSTMENT_H INNER JOIN FEES.vw_OSO_ENQUIRY_COMP VW_OSO_STUDENT_M" & _
               " ON FEES.FEEADJUSTMENT_H.FAH_STU_ID =  STU_ID" & _
               " INNER JOIN FEES.FEEADJUSTMENT_S ON  " & _
               " FEES.FEEADJUSTMENT_H.FAH_ID = FEES.FEEADJUSTMENT_S.FAD_FAH_ID " & _
               " LEFT OUTER JOIN FEES.FEEADJREQUEST_H ON FEES.FEEADJUSTMENT_H.FAH_FAR_ID = FEES.FEEADJREQUEST_H.FAR_ID " & _
               " WHERE FEES.FEEADJUSTMENT_H.FAH_bDeleted =0 AND FEES.FEEADJUSTMENT_H.FAH_STU_TYPE = 'E' " & _
               strGroupby & ")A WHERE 1=1 " & str_Filter
            End If
            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.MNU_FEE_ADJUSTMENTS
                    ' str_cond += " AND isnull(FAH_bInter, 0) = 0"
                Case OASISConstants.MNU_FEE_ADJUSTMENTS_HEADTOHEAD
                    str_cond += " AND isnull(FAH_bInter,0) = 1"
            End Select

            If radOpen.Checked Then
                str_cond += " AND isnull(FAH_bPosted, 0) = 0"
            ElseIf radPosted.Checked Then
                str_cond += " AND isnull(FAH_bPosted, 0) = 1"
            End If

            Dim str_orderby As String = " ORDER BY  FAH_DOCNO DESC,FAH_DATE DESC "
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
            CommandType.Text, str_Sql & str_cond & str_orderby)
            gvFEEAdjustments.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ds.Tables(0).Rows(0)("FAH_ID") = 0
                ds.Tables(0).Rows(0)("FAH_bInter") = 0
                gvFEEAdjustments.DataBind()
                Dim columnCount As Integer = gvFEEAdjustments.Rows(0).Cells.Count

                gvFEEAdjustments.Rows(0).Cells.Clear()
                gvFEEAdjustments.Rows(0).Cells.Add(New TableCell)
                gvFEEAdjustments.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEAdjustments.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEAdjustments.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEAdjustments.DataBind()
            End If
            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtDocNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtstudname")
            txtSearch.Text = lstrCondn2

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn3

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtAcademicYear")
            txtSearch.Text = lstrCondn4

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn5

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtStudNo")
            txtSearch.Text = lstrCondn6
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEAdjustments.PageIndexChanging
        gvFEEAdjustments.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFEEAdjustments.RowDataBound
        Try
            Dim lblFAH_ID As New Label
            lblFAH_ID = TryCast(e.Row.FindControl("lblFAH_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblFAH_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS
                        hlEdit.NavigateUrl = "FeeAdjustment.aspx?FAH_ID=" & Encr_decrData.Encrypt(lblFAH_ID.Text) & _
                       "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    Case OASISConstants.MNU_FEE_ADJUSTMENTS_HEADTOHEAD
                        hlEdit.NavigateUrl = "FeeAdjustment_HeadtoHead.aspx?FAH_ID=" & Encr_decrData.Encrypt(lblFAH_ID.Text) & _
                       "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                End Select

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub radStud_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStud.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radEnq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnq.CheckedChanged
        GridBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub radOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOpen.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPosted.CheckedChanged
        GridBind()
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblFAH_ID As Label = sender.Parent.parent.findcontrol("lblFAH_ID")
            Dim lblFAH_bInter As Label = sender.Parent.parent.findcontrol("lblFAH_bInter")
            Dim isHaedtoHead As Boolean = True
            If lblFAH_bInter.Text.ToUpper = "FALSE" Then
                isHaedtoHead = False
            End If
            Session("ReportSource") = FEEADJUSTMENT.PrintFEEAdjustmentVoucher(lblFAH_ID.Text, Session("sUsr_name"), isHaedtoHead)
            h_print.Value = "print"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


End Class
