Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_Discountimport
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnFind)
        If Page.IsPostBack = False Then
            doClear()
            initialize_components()

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'MainMnu_code = "A200354"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "F300555") Then
                'And ViewState("MainMnu_code") <> "A200310" And ViewState("MainMnu_code") <> "A200351"
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                
            End If
            UtilityObj.beforeLoopingControls(Me.Page)
        End If
        gvExcel.Attributes.Add("bordercolor", "#1b80b6")
        btnSave.Visible = True
    End Sub
    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
    Public Sub getdata(ByVal filePath As String, ByVal Fname As String)
        Try
            Dim Query As String = ""
            'Query = " SELECT NO,DATE,STUDENTID,CASH,FEECODE,PAID,CHEQUE,CHKNO,CHKDATE,BANK  FROM  " & filePath & Fname
            Query = " SELECT * FROM  " & filePath & Fname
            Dim _table As DataTable
            _table = Mainclass.FecthFromDBFodbc(Query, filePath)
            If _table.Rows.Count.Equals(0) Then
                Throw New Exception("Could not process of you are request.date not match with excel..!")
            End If
            If h_Editid.Value.Equals("0") Then
                InsertTemtable(_table)
            End If
        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Public Sub getdataExcel(ByVal filePath As String)
        Try
            Dim excelQuery As String = ""
            'and year(DT) = " & Session("F_YEAR") & "   
            excelQuery = " SELECT * FROM  [TableName]"

            'excelQuery = "SELECT * from [Sheet1$]"
            Dim _table As DataTable
            _table = Mainclass.FetchFromExcel(excelQuery, filePath)
            If _table.Rows.Count.Equals(0) Then
                Throw New Exception("Could not process of you are request.date not match with excel..!")
            End If
            If h_Editid.Value.Equals("0") Then
                InsertTemtable(_table)
            End If

        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Sub initialize_components()
        FillACD()
        BindBusinessUnit()
        ddlBSUnit.Attributes.Add("ReadOnly", "ReadOnly")
        btnSave.Visible = True
    End Sub

    Private Sub doClear()
        gvExcel.DataSource = Nothing
        gvExcel.DataBind()
        h_Editid.Value = "0"
        btnFind.Enabled = True

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim RetVal As Integer = doInsert()
            If RetVal = 0 Then
                doClear()
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Sub BindBusinessUnit()
        ddlBSUnit.DataSource = FeeCommon.GETBSUFORUSER(Session("sUsr_name"))
        ddlBSUnit.DataTextField = "bsu_name"
        ddlBSUnit.DataValueField = "bsu_id"
        ddlBSUnit.DataBind()
        ddlBSUnit.Items.FindByValue(Session("sBsuid")).Selected = True
    End Sub
    Private Sub UpLoadDBF(ByVal flieType As String)
        If uploadFile.HasFile Then
            'Dim filePath As String = Server.MapPath("")
            Dim filePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            If Not Directory.Exists(filePath & "\temp") Then
                Directory.CreateDirectory(filePath & "\temp")
            End If
            Dim FolderPath As String = filePath & "\temp\"
            filePath = filePath & "\temp\" & uploadFile.FileName 'FileName
            If uploadFile.HasFile Then
                uploadFile.SaveAs(filePath)
                Try
                    getdataExcel(filePath)
                    If File.Exists(filePath) Then
                        File.Delete(filePath)
                    End If
                Catch ex As Exception
                    'lblError.Text = ex.Message
                    usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                End Try
            End If
        End If
    End Sub
    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        Dim fileType As String = "Excel"
        If (h_Editid.Value = "0") Then
            If Not uploadFile.HasFile Then
                'lblError.Text = "Select Particular File...!"
                usrMessageBar.ShowNotification("Select Particular File...!", UserControls_usrMessageBar.WarningType.Danger)
                gvExcel.DataSource = Nothing
                gvExcel.DataBind()
                Exit Sub
            End If
            If Not (uploadFile.FileName.EndsWith("xls", StringComparison.OrdinalIgnoreCase) Or uploadFile.FileName.EndsWith("xlsx", StringComparison.OrdinalIgnoreCase)) Then
                'lblError.Text = "Invalid file type.. Only Excel files are alowed.!"
                usrMessageBar.ShowNotification("Invalid file type.. Only Excel files are alowed.!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
            UpLoadDBF(fileType)
        End If
    End Sub
    Private Sub InsertTemtable(ByVal _table As DataTable)
        If _table.Rows.Count > 0 Then
            Try
               
                If _table.Rows.Count > 0 Then
                    Dim BsuId As String = Convert.ToInt32(_table.Rows(0)(0)).ToString()
                    ddlBSUnit.SelectedValue = BsuId
                    ddlAcademicYear.SelectedItem.Value = FeeCommon.GetCurrentAcademicYear(BsuId)
                    ddlBSUnit.Enabled = False
                    ddlAcademicYear.Enabled = False
                    labdetailHead.Text = labdetailHead.Text & " ( " & ddlBSUnit.SelectedItem.Text & " )"
                End If
                _table.Columns().RemoveAt(0)
                gvExcel.DataSource = _table
                gvExcel.DataBind()

            Catch ex As Exception
                Errorlog(ex.Message)
                'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
                usrMessageBar.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub
    Protected Sub gvExcel_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Convert.ToDateTime(e.Row.Cells(2).Text).ToString("dd/MMM/yyyy")
            e.Row.Cells(3).Text = Convert.ToDateTime(e.Row.Cells(3).Text).ToString("dd/MMM/yyyy")
            e.Row.Cells(10).Text = Math.Round(Convert.ToDouble(e.Row.Cells(10).Text), 2).ToString()
            e.Row.Cells(11).Text = Math.Round(Convert.ToDouble(e.Row.Cells(11).Text)).ToString()
            e.Row.Cells(12).Text = Math.Round(Convert.ToDouble(e.Row.Cells(12).Text)).ToString()

        End If
    End Sub

    Private Function doInsert() As Integer

        Dim FctId As String = ""
        Dim retval As Integer = 0
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim TrmFees As Double = 0
        Dim TrmDiscount As Double = 0
        Try
            For Each gvRow As GridViewRow In gvExcel.Rows

                Dim pParms(18) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@FDM_BSU_ID", SqlDbType.VarChar)
                pParms(0).Value = ddlBSUnit.SelectedItem.Value    'Session("sBsuid")
                pParms(1) = New SqlClient.SqlParameter("@FDM_ACD_ID", SqlDbType.VarChar)
                pParms(1).Value = ddlAcademicYear.SelectedItem.Value
                pParms(2) = New SqlClient.SqlParameter("@FDM_DESCR", SqlDbType.VarChar)
                pParms(2).Value = gvRow.Cells(0).Text.Trim()
                pParms(3) = New SqlClient.SqlParameter("@FDM_PROMOTIONTEXT", SqlDbType.VarChar)
                pParms(3).Value = gvRow.Cells(1).Text.Trim()
                pParms(4) = New SqlClient.SqlParameter("@FDM_FROMDT", SqlDbType.VarChar)
                pParms(4).Value = gvRow.Cells(2).Text.Trim()
                pParms(5) = New SqlClient.SqlParameter("@FDM_TODT", SqlDbType.VarChar)
                pParms(5).Value = gvRow.Cells(3).Text.Trim()
                pParms(6) = New SqlClient.SqlParameter("@FDM_INTEREST", SqlDbType.Decimal)
                pParms(6).Value = gvRow.Cells(4).Text.Trim()
                pParms(7) = New SqlClient.SqlParameter("@FDM_PROMOTIONURL", SqlDbType.VarChar)
                pParms(7).Value = ""
                pParms(8) = New SqlClient.SqlParameter("@FDS_GRD_ID", SqlDbType.VarChar)
                pParms(8).Value = gvRow.Cells(5).Text.Trim()
                pParms(9) = New SqlClient.SqlParameter("@STREAM", SqlDbType.VarChar)
                pParms(9).Value = gvRow.Cells(6).Text.Trim()
                pParms(10) = New SqlClient.SqlParameter("@FDS_SLABDESCR", SqlDbType.VarChar)
                pParms(10).Value = gvRow.Cells(8).Text.Trim().Replace("&amp;", " ")
                pParms(11) = New SqlClient.SqlParameter("@FDS_AMOUNT", SqlDbType.Decimal)
                pParms(11).Value = gvRow.Cells(9).Text.Trim()
                pParms(12) = New SqlClient.SqlParameter("@FDS_RATE", SqlDbType.Decimal)
                pParms(12).Value = gvRow.Cells(10).Text.Trim()
                pParms(13) = New SqlClient.SqlParameter("@FDS_DISCAMOUNT", SqlDbType.Decimal)
                pParms(13).Value = gvRow.Cells(11).Text.Trim()
                pParms(14) = New SqlClient.SqlParameter("@FDS_NETAMOUNT", SqlDbType.Decimal)
                pParms(14).Value = gvRow.Cells(12).Text.Trim()
                pParms(15) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                pParms(15).Direction = ParameterDirection.ReturnValue
                If gvRow.Cells.Count > 15 Then
                    TrmFees = Convert.ToDouble(gvRow.Cells(13).Text.Trim())
                End If
                If gvRow.Cells.Count > 16 Then
                    TrmDiscount = Convert.ToDouble(gvRow.Cells(14).Text.Trim())
                End If
                pParms(16) = New SqlClient.SqlParameter("@FDS_TRMFEES", SqlDbType.Decimal)
                pParms(16).Value = Math.Round(TrmFees)
                pParms(17) = New SqlClient.SqlParameter("@FDS_TRMDISCOUNT", SqlDbType.Decimal)
                pParms(17).Value = Math.Round(TrmDiscount)

                retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[FEES].[DIS_DiscountScheme_Save]", pParms)
                If pParms(15).Value = "0" Then
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, ddlBSUnit.SelectedItem.Value, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                Else
                    'lblError.Text = getErrorMessage(pParms(15).Value)
                    usrMessageBar.ShowNotification(getErrorMessage(pParms(15).Value), UserControls_usrMessageBar.WarningType.Danger)
                    retval = pParms(15).Value
                    stTrans.Rollback()
                    objConn.Close()
                    gvRow.BackColor = Drawing.Color.BurlyWood
                    Return retval
                End If

            Next
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            'lblError.Text = getErrorMessage(ex.Message)
            usrMessageBar.ShowNotification(getErrorMessage(ex.Message), UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            objConn.Close()
            Return 1000
        End Try
        stTrans.Commit()

        If objConn.State = ConnectionState.Open Then
            objConn.Close()
        End If
        'lblError.Text = getErrorMessage(0)
        usrMessageBar.ShowNotification(getErrorMessage(0), UserControls_usrMessageBar.WarningType.Danger)
        Return 0
    End Function
End Class

