<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="feeTransportPerformaInvoice.aspx.vb" Inherits="Fees_feeTransportPerformaInvoice"
    Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="true">

    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
                    function CheckForPrint() {
                        if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                            document.getElementById('<%= h_print.ClientID %>').value = '';
                            radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up4');
                        }
                    }
                    );
        function CheckNumber(ctrl) {
            var temp;
            temp = parseFloat(ctrl.value);
            if (isNaN(temp))
                ctrl.value = 0;
            else
                ctrl.value = temp.toFixed(2);
        }

        function CheckAlready() {
            if (document.getElementById('<%= h_setAlert.ClientID %>').value == '1')
                return confirm('Invoice already exists!!! Do you want to Continue ?')
            else
                return true
        }

        function getStudent() {
           
            var NameandCode;
            var result;
            var GroupSel = document.getElementById('<%=chkGroupInvoice.ClientID %>').checked;
            var COMP_ID = document.getElementById('<%=h_Company_ID.ClientID %>').value;
            var STUD_TYP = document.getElementById('<%=rbEnquiry.ClientID %>').checked;
            var ACD_ID = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            var BSUID = document.getElementById('<%=ddBusinessunit.ClientID %>').value;
            var chkCompanyFilter = document.getElementById('<%=chkCompanyFilter.ClientID %>').checked;

            var url;

            if (GroupSel == true) {
                if (chkCompanyFilter == true) {
                    url = "ShowStudentMultiTransport.aspx?TYPE=STUD_COMP&COMP_ID=" + COMP_ID + "&MULTI_SEL=" + GroupSel + "&ACD_ID=" + ACD_ID + "&bsu=" + BSUID;
                }
                else {
                    url = "ShowStudentMultiTransport.aspx?TYPE=STUD_COMP&MULTI_SEL=" + GroupSel + "&ACD_ID=" + ACD_ID + "&bsu=" + BSUID;
                }
            }
            else
                if (chkCompanyFilter == true) {
                    url = "ShowStudentTransport.aspx?type=STUD_COMP&COMP_ID=" + COMP_ID + "&bsu=" + BSUID;
                }
                else {
                    url = "ShowStudentTransport.aspx?type=STUD_COMP&bsu=" + BSUID;
                }

            {

            }

            result = radopen(url, "pop_up");

            <%--if (result == '' || result == undefined) {
                return false;
            }

            if (GroupSel == true) {
                document.getElementById('<%= txtStdNo.ClientID %>').value = "";
                document.getElementById('<%=txtStudentname.ClientID %>').value = 'Multiple Students selected';
                document.getElementById('<%=h_Student_no.ClientID %>').value = result;
                document.getElementById('<%= txtStdNo.ClientID %>').value = "1234";
            }
            else {
                NameandCode = result.split('||');
                document.getElementById('<%= h_Student_no.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= txtStudentname.ClientID %>').value = NameandCode[1];
                document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];
            }
            return true;--%>
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            var GroupSel = document.getElementById('<%=chkGroupInvoice.ClientID %>').checked;
            if (arg) {
                NameandCode = arg.NameandCode.split('||');                
                if (GroupSel == true) {
                    document.getElementById('<%= txtStdNo.ClientID %>').value = "";
                    document.getElementById('<%= txtStudentname.ClientID%>').value = 'Multiple Students Selected';
                    document.getElementById('<%= h_Student_no.ClientID%>').value = arg.NameandCode;
                    document.getElementById('<%= txtStdNo.ClientID %>').value = "1234";
                    __doPostBack('<%= h_Student_no.ClientID%>', 'ValueChanged');
                }
                else {                   
                    document.getElementById('<%= h_Student_no.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%= txtStudentname.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];
                    __doPostBack('<%= h_Student_no.ClientID%>', 'ValueChanged');
                }
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        

        function getLOCATION() {
            var chk;
            chk = 'notchecked';
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    if (document.forms[0].elements[i].checked == true)
                        chk = 'checked';

                }
            }

            if (chk == 'notchecked') {
                alert('Please select the term')
                return false;
            }
            var  url;
            
            var NameandCode;
            var result;

            url = "../common/PopupFormIDhidden.aspx?iD=LOCATION&bsu=" + document.getElementById('<%= ddBusinessunit.ClientID %>').value + "&MULTISELECT=FALSE";
            result = radopen(url, "pop_up2");
          <%--  if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%= H_Location.ClientID %>').value = NameandCode[0];
            document.getElementById('<%= txtLocation.ClientID %>').value = NameandCode[1];
            return true;--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                  document.getElementById('<%= H_Location.ClientID %>').value = NameandCode[0];
            document.getElementById('<%= txtLocation.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtLocation.ClientID%>', 'TextChanged');
            }
        }

        function GetCompany() {
            
            var NameandCode;
            var result;
            var url = "../Common/popupForm.aspx?ID=COMPANY&multiSelect=false";
            result = radopen(url, "pop_up3")
            <%--if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Company_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtCompanyDescr.ClientID %>').value = NameandCode[1];
                return true;
            }
            else {
                return false;
            }--%>
        }

         function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Company_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtCompanyDescr.ClientID %>').value = NameandCode[1];
                __doPostBack('<%= txtCompanyDescr.ClientID%>', 'TextChanged');
            }
         }

        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move"
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="720px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Transport Pro forma Invoice
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                            <%--<asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>--%>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <%--  <tr class="subheader_img">
            <td colspan="4" style="height: 19px" align="left">Transport Pro forma Invoice</td>
        </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span> </td>
                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddBusinessunit" runat="server" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" AutoPostBack="True" TabIndex="5">
                            </asp:DropDownList>
                            <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover"
                                PopupControlID="PanelTree" PopupPosition="Center" TargetControlID="pnlLink" PopDelay="25" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left"  width="20%"><span class="field-label">Company</span></td>
                        <td align="left"  width="30%">
                            <asp:TextBox ID="txtCompanyDescr" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgCompany" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetCompany(); return false;" />
                            <asp:LinkButton ID="lnkClearCompany" runat="server"
                                TabIndex="15">Clear </asp:LinkButton>

                        </td>
                        <td align="left"  colspan="2">
                            <asp:CheckBox ID="chkCompanyFilter" runat="server" Text="Company Filter" CssClass="field-label" />
                            <asp:CheckBox ID="chkExcludeAdv" runat="server" Text="Exclude Advance Fees" CssClass="field-label" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Academic Year</span> </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" TabIndex="10">
                            </asp:DropDownList></td>
                        <td align="left"><span class="field-label">Term/Months</span> </td>
                        <td align="left">
                            <asp:RadioButton ID="rbTermly" runat="server" Text="Termly" Checked="True" CssClass="field-label"
                                GroupName="trm" AutoPostBack="True" />
                            <asp:RadioButton ID="rbMonthly" runat="server" Text="Monthly" GroupName="trm" AutoPostBack="True" CssClass="field-label" />
                            <asp:Panel ID="pnlLink" runat="server">
                                <asp:LinkButton ID="lnkBtnSelectmonth" runat="server" OnClientClick="return false;"
                                    TabIndex="15">Select Month/Term </asp:LinkButton>
                            </asp:Panel>

                            <asp:Panel ID="PanelTree" runat="server" CssClass="Visibility_none" BackColor="#8dc24c"
                                Style="display: none">                               
                                    <asp:TreeView ID="trMonths" ShowCheckBoxes="all" onclick="client_OnTreeNodeChecked();"
                                        runat="server">
                                    </asp:TreeView>
                                    <div style="text-align: center;">
                                        <asp:Button ID="btnNarrInsert" runat="server" CssClass="button_small" Text="Fill Narration" />
                                    </div>                                
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Select Student </span></td>
                        <td align="left">
                            <asp:CheckBox ID="chkGroupInvoice" runat="server" Text="Group Invoice" AutoPostBack="True" CssClass="field-label"
                                OnCheckedChanged="chkGroupInvoice_CheckedChanged" />
                            <asp:RadioButton ID="rbEnrollment" runat="server" Checked="True" GroupName="mode" CssClass="field-label"
                                Text="Student#" AutoPostBack="True" />
                            <asp:RadioButton ID="rbEnquiry" runat="server" GroupName="mode" Text="Enquiry#" AutoPostBack="True" CssClass="field-label"
                                Enabled="False" /></td>
                        <td align="left"><span class="field-label">Date</span> </td>
                        <td align="left">
                            <asp:TextBox ID="txtFrom" runat="server" TabIndex="15"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="20" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Select Student</span> </td>
                        <td align="left">
                            <asp:TextBox ID="txtStdNo" runat="server" AutoPostBack="True" OnTextChanged="txtStdNo_TextChanged" Width="92%"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getStudent();return false;" />                          
                        </td>
                        <td>
                            <asp:TextBox ID="txtStudentname" runat="server"></asp:TextBox>
                            <asp:LinkButton ID="lblAddNewStudent" runat="server" CausesValidation="False" OnClick="lblAddNewStudent_Click">Add</asp:LinkButton>
                            <asp:Label ID="lblNoStudent" runat="server" EnableViewState="False" CssClass="error"
                                ForeColor="Red"></asp:Label><br />
                        </td>
                    </tr>
                    <tr id="TrArea" runat="server">
                        <td align="left"><span class="field-label">Area </span></td>
                        <td align="left" valign="middle">
                            <asp:TextBox ID="txtLocation" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgLocation" runat="server" ImageUrl="~/Images/Misc/Route.png"
                                OnClientClick="getLOCATION(); return false;" ImageAlign="AbsMiddle" TabIndex="30" />
                            <asp:Button ID="btnAddDetails" runat="server" CssClass="button" Text="Add"
                                CausesValidation="False" TabIndex="50" /></td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr id="TrGrid1" runat="server">
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvFeeTransport" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="No Details Added" SkinID="GridViewPickDetails">
                                <RowStyle CssClass="griditem" />
                                <EmptyDataRowStyle />
                                <Columns>
                                    <asp:BoundField DataField="Description" HeaderText="Description" />
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmountToPay" runat="server" Text='<%# Bind("Amount", "{0:0.00}") %>'
                                                AutoPostBack="True" OnPreRender="txtAmountToPay_PreRender" onblur="CheckNumber(this);UpdateSum();"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="REF_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblREF_ID" runat="server" Text='<%# Bind("REF_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <asp:Button ID="btnItemUpdate" runat="server" CssClass="button" Text="Update" TabIndex="105"
                                OnClick="btnItemUpdate_Click" />
                            <asp:Button ID="btnItemCancel" runat="server" CssClass="button"
                                Text="Cancel" CausesValidation="False" />
                        </td>
                    </tr>
                    <tr id="TrGrid2" runat="server">
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvStudentDetails" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                Width="100%" OnRowDataBound="gvStudentDetails_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Student ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudID" runat="server" Text='<%# bind("STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="STU_NO" HeaderText="Student No" />
                                    <asp:BoundField DataField="STU_NAME" HeaderText="Student Name" />
                                    <asp:BoundField DataField="TripType" HeaderText="Trip Type" />
                                    <asp:BoundField DataField="AMOUNT" HeaderText="Amount" />
                                    <asp:TemplateField HeaderText="Area" Visible="True">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSBL_DESCRIPTION" runat="server" Text='<%# bind("SBL_DESCRIPTION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SBL_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSBL_ID" runat="server" Text='<%# bind("SBL_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%" cellpadding="5">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span> </td>
                        <td width="30%">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"
                                TabIndex="100"></asp:TextBox>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" OnClientClick="return CheckAlready()" CssClass="button"
                                Text="Save" TabIndex="105" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="H_Location" runat="server" />
                <asp:HiddenField ID="h_Student_no" runat="server" OnValueChanged="h_Student_no_ValueChanged" />
                <asp:HiddenField ID="h_Edit_STU_ID" runat="server" />
                <asp:HiddenField ID="h_Company_ID" runat="server" />
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" SelectMethod="SERVICES_BSU_M"
                    TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                    PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_setAlert" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
