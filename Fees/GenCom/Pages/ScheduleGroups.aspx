﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="ScheduleGroups.aspx.vb" Inherits="Gencom_ScheduleGroups" %>

<%@ Register Src="../UserControls/ScheduleGroups.ascx" TagName="ScheduleGroups" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Email/SMS"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <uc1:ScheduleGroups ID="VAXScheduleGroups1" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>
