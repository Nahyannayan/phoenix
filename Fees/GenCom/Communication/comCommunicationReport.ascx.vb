﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO


Partial Class Transport_GPS_Tracking_Communication_comCommunicationReport
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenModule.Value = "GEN"
            BindBsu()
            BindGrid()
        End If
    End Sub

    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = ""
        If Session("sBsuid") = "900501" Then  '' STS LOGIN
            str_query = "SELECT  BSU_ID,BSU_NAME FROM BUSINESSUNIT_M AS A " _
                            & " INNER JOIN BSU_TRANSPORT AS B ON A.BSU_ID=B.BST_BSU_ID" _
                            & "  WHERE BST_BSU_OPRT_ID='" + Session("sbsuid") + "' ORDER BY BSU_NAME"

            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()

            Dim list As New ListItem
            list.Text = "Select Business Unit"
            list.Value = "-1"
            ddBsu.Items.Insert(0, list)
        Else
            str_query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "' order by BSU_NAME"
            Dim dsBsu As DataSet
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddBsu.DataSource = dsBsu
            ddBsu.DataTextField = "BSU_NAME"
            ddBsu.DataValueField = "BSU_ID"
            ddBsu.DataBind()
        End If

    End Sub
    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(14) As SqlClient.SqlParameter

        If ddBsu.SelectedValue <> "-1" Then
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddBsu.SelectedValue)
        End If

        If DropType.SelectedValue <> "-1" Then
            pParms(1) = New SqlClient.SqlParameter("@MESSAGE_TYPE", DropType.SelectedValue)
        End If

        pParms(2) = New SqlClient.SqlParameter("@MESSAGE_MODULE", HiddenModule.Value)
        pParms(3) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COM_V2_GET_TRAN_MESSAGE_SEND_REPORT", pParms)
        GridInfo.DataSource = ds
        GridInfo.DataBind()

    End Sub

    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging

        GridInfo.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub

    Protected Sub ddBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBsu.SelectedIndexChanged
        BindGrid()
    End Sub

    Protected Sub DropType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropType.SelectedIndexChanged
        BindGrid()
    End Sub
End Class
