﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="ListGroupsAddOnList.aspx.vb" Inherits="Transport_GPS_Tracking_SMS_gpsListGroupsAddOnList" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Group List &nbsp; 
            <asp:Label ID="lbltotal" runat="server" Text=""></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table border="0" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="100%">
                    <%-- <tr>
                        <td class="subheader_img">Group List &nbsp;
          
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="left">
                            <asp:GridView ID="GridInfo" runat="server" ShowFooter="true"
                                AutoGenerateColumns="false"
                                Width="100%" AllowPaging="True" PageSize="20" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            User ID
                              
                              
                                                  
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                <%# Eval("USR_ID")%>
      <asp:HiddenField ID="HiddenUserid" Value='<%# Eval("USR_ID")%>' runat="server" />
                                </center>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <center>
                        <asp:Button ID="btnexport" runat="server" CssClass="button" OnClick="exporting" Text="Export" />
                        </center>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            User Name
                                         

                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <%# Eval("NAME")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Details
                                         

                                                 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                           <%# Eval("DETAILS")%>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Contact Name
                                            

                                                  
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <%# Eval("CONTACT_NAME")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Contact No
                                             

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                            <%# Eval("MOBILE")%>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Email Id
                                           

                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <%# Eval("EMAIL")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <EmptyDataRowStyle />
                                <SelectedRowStyle />
                                <HeaderStyle />
                                <EditRowStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>





