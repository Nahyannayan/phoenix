﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="comEditPlainText.aspx.vb" Inherits="Transport_GPS_Tracking_Communication_comEditPlainText" %>

<%@ Register Src="comEditPlainText.ascx" TagName="comEditPlainText" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Email Plain Text Templates
           
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <uc1:comEditPlainText ID="comEditPlainText1" runat="server" />

            </div>
        </div>
    </div>

</asp:Content>

