<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comEditPlainText.ascx.vb"
    Inherits="masscom_UserControls_comEditPlainText" %>
<%--<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>--%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

 <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../cssfiles/Popup.css" rel="stylesheet" />

<script type="text/javascript">

    function openview(val) {
        window.open('comPlainTextView.aspx?temp_id=' + val);
        return false;

    }


    function opennew() {
        var mid = document.getElementById('<%= Hiddenmid.ClientID %>').value;
        window.open("comCreatePlainText.aspx?mid=" + mid, "", "dialogWidth:800px; dialogHeight:600px; center:yes");
        window.location.reload = window.location.reload
    }

    function Openss(id) {

        var strOpen;
        var gpid = document.getElementById('<%= HiddenGroupdid.ClientID %>').value;
        var mid = document.getElementById('<%= Hiddenmid.ClientID %>').value;
        if (gpid != "") {
            strOpen = 'comSendMessagePage.aspx'
            strOpen += "?mid=" + mid + "&messagetype=EMAIL&grpid=" + gpid + "&logid=&templateid=" + id;
            //window.showModalDialog(strOpen, "", "dialogWidth:350px;dialogHeight:350px;center:yes");
            return ShowWindowWithClose(strOpen, 'search', '35%', '55%')
            return false;

        }
        else {
            strOpen = 'comExcelEmailData.aspx'
            strOpen += "?mid=" + mid + "&Tab=0&DataId=0&templateid=" + id;
            //window.showModalDialog(strOpen, "", "dialogWidth:800px;dialogHeight:600px;center:yes");
            return ShowWindowWithClose(strOpen, 'search', '55%', '85%')
            return false;
        }

    }
    function CloseFrame() {     
        jQuery.fancybox.close();
        return false;
    }
</script>
<div class="matters">
    <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
    <asp:Panel ID="Panel3" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
           <%-- <tr>
                <td class="title-bg-lite">Email Plain Text Templates
                
                </td>
            </tr>--%>
            <tr>
                <td>

                    <asp:LinkButton ID="Linkadd" runat="server">Create New Template</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GrdEmailText" runat="server" EmptyDataText="Email Plain Text not added yet."
                        Width="100%" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                        <Columns>
                            <asp:TemplateField HeaderText="Template ID">
                                <HeaderTemplate>
                                    Template&nbsp;ID
                                                <br />
                                    <asp:TextBox ID="Txt1" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                        ImageUrl="~/Images/forum_search.gif" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:HiddenField ID="HiddenFieldId" runat="server" Value='<%# Eval("EML_ID") %>' />
                                    <center>
                                        <%# Eval("EML_ID") %></center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date">
                                <HeaderTemplate>
                                    Date
                                                <br />
                                    <asp:TextBox ID="Txt2" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                        ImageUrl="~/Images/forum_search.gif" />
                                    <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2"
                                        TargetControlID="Txt2">
                                    </ajaxToolkit:CalendarExtender>

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%#Eval("EML_DATE", "{0:dd/MMM/yyyy}")%></center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email Text">
                                <HeaderTemplate>
                                    Email Title
                                                <br />
                                    <asp:TextBox ID="Txt3" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search"
                                        ImageUrl="~/Images/forum_search.gif" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkView" Text=' <%# Eval("EML_TITLE") %> ' OnClientClick=' <%# Eval("openview") %>'
                                        runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Attachments">
                                <HeaderTemplate>
                                    Attachments
                                                <br />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:GridView ID="GrdAttachment" runat="server" AutoGenerateColumns="false" OnRowCommand="GrdTextAttachment_RowCommand"
                                        ShowHeader="false" Width="100%" CssClass="table table-bordered table-row">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Name">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkattachment" runat="server" CausesValidation="false" CommandName="select"
                                                        Text=' <%# Eval("Name") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Size (Bytes)">
                                                <ItemTemplate>
                                                    <div align="right">
                                                        -(Size
                                                        <%#Eval("length")%>
                                                        Bytes)
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandName="Remove">Delete</asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" ConfirmText="Are you sure you want to delete this attachment?"
                                                        TargetControlID="lnkdelete">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delete">
                                <HeaderTemplate>
                                    Delete
                                                <br />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandArgument='<%# Eval("EML_ID") %>'
                                            CommandName="RemoveDir">Delete</asp:LinkButton></center>
                                    <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" ConfirmText="Are you sure you want to delete this template?"
                                        TargetControlID="lnkdelete">
                                    </ajaxToolkit:ConfirmButtonExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit">
                                <HeaderTemplate>
                                    Edit
                                                <br />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="false" CommandArgument='<%# Eval("EML_ID") %>'
                                            CommandName="Editing">Edit</asp:LinkButton></center>
                                    <ajaxToolkit:ConfirmButtonExtender ID="C2" runat="server" ConfirmText="Are you sure you want to edit this template?"
                                        TargetControlID="lnkEdit">
                                    </ajaxToolkit:ConfirmButtonExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Send">
                                <HeaderTemplate>
                                    Send
                                                <br />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <asp:LinkButton ID="lnksend" OnClientClick='<%#Eval("OPENW")%>' runat="server">Send</asp:LinkButton>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle />
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                        <EmptyDataRowStyle />
                        <EditRowStyle />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel4" runat="server" Visible="False">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">Update Plain Text Templates
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <span class="field-label">Title</span>
                                        </td>

                                        <td width="100%">
                                            <asp:TextBox ID="txtTitle" Width="50%" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="field-label">From&nbsp;Email&nbsp;Id </span>
                                        </td>

                                        <td>
                                            <asp:TextBox ID="txtFrom" Width="50%" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="field-label">Subject</span>
                                        </td>

                                        <td>
                                            <asp:TextBox ID="txtsubject" Width="50%" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="field-label">Display</span>
                                        </td>

                                        <td>
                                            <asp:TextBox ID="txtdisplay" Width="50%" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="field-label">Host</span>
                                        </td>

                                        <td>
                                            <asp:TextBox ID="txthost" Width="50%" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="field-label">Port</span>
                                        </td>

                                        <td>
                                            <asp:TextBox ID="txtport" Width="50%" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="field-label">Username</span>
                                        </td>

                                        <td>
                                            <asp:TextBox ID="txtusername" Width="50%" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="field-label">Password</span>
                                        </td>

                                        <td>
                                            <asp:TextBox ID="txtpassword" Width="50%" runat="server" TextMode="Password"></asp:TextBox>
                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="* Please provide if any change in Password"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>

                                        <td>
                                            <telerik:RadEditor ID="txtEditEmailText" runat="server" EditModes="All" Height="600px"
                                                ToolsFile="xml/FullSetOfTools.xml" Width="750px">
                                            </telerik:RadEditor>
                                            <div id="TRDynamic" runat="server">
                                                <asp:LinkButton ID="LinkDynamic" OnClientClick="javascript:return false;" runat="server">Dynamic Text</asp:LinkButton>
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="Panel1" runat="server">
                                                            <table>
                                                                <tr>
                                                                    <td colspan="1">
                                                                        <span class="field-label">Template</span>
                                                                    </td>

                                                                    <td>
                                                                        <asp:DropDownList ID="ddtemplate" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddtemplate_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="1">
                                                                        <span class="field-label">Fields</span>
                                                                    </td>

                                                                    <td>
                                                                        <asp:DropDownList ID="ddfields" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" colspan="1"></td>
                                                                    <td align="center" colspan="1"></td>
                                                                    <td align="center">
                                                                        <asp:Button ID="btninsert" runat="server" CssClass="button" Text="Insert" OnClick="btninsert_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                                            AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkDynamic" Collapsed="true"
                                                            CollapsedSize="0" CollapsedText="Dynamic Text" ExpandControlID="LinkDynamic"
                                                            ExpandedSize="100" ExpandedText="Hide" ScrollContents="false" TargetControlID="Panel1"
                                                            TextLabelID="LinkDynamic">
                                                        </ajaxToolkit:CollapsiblePanelExtender>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="field-label">Attachments</span>
                                        </td>

                                        <td>
                                            <asp:FileUpload ID="FileUploadEditEmailtext" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btneditplaintextsave" runat="server" CssClass="button" Text="Save"
                                    ValidationGroup="EdText" Width="118px" />
                                <asp:Button ID="btnplaintextcancel" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Cancel" Width="113px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEditEmailText"
            Display="None" ErrorMessage="Please Enter Email Text" SetFocusOnError="True"
            ValidationGroup="EdText"></asp:RequiredFieldValidator>
        <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowMessageBox="True"
            ShowSummary="False" ValidationGroup="EdText" />
    </asp:Panel>
    <asp:HiddenField ID="HiddenFieldTemplateid" runat="server" />
    <asp:HiddenField ID="HiddenPassword" runat="server" />
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:HiddenField ID="HiddenGroupdid" runat="server" />
    <asp:HiddenField ID="Hiddenmid" runat="server" />

    
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>
</div>
