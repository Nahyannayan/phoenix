﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="GroupList.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Medical_Communication_GroupList" %>


<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Email/SMS"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:LinkButton ID="lnkadd" runat="server">New Group</asp:LinkButton>
                <table width="100%">

                    <tr>
                        <td align="left">
                            <asp:GridView ID="GridInfo" runat="server" AllowPaging="True" PageSize="20" ShowFooter="true" CssClass="table table-bordered table-row"
                                AutoGenerateColumns="false" EmptyDataText="Information not available." Width="100%">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Group ID
                                            <br />
                                            <asp:TextBox ID="Txt1"   runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                                ID="ImageSearch1" CausesValidation="false" runat="server" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <%# Eval("GROUP_ID")%>
                                    <asp:HiddenField ID="Hiddengroupid" Value='<%# Eval("GROUP_ID")%>' runat="server" />
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Group Name
                                            <br />
                                            <asp:TextBox ID="Txt2"   runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                                ID="ImageSearch2" CausesValidation="false" runat="server" CommandName="search"
                                                ImageUrl="~/Images/forum_search.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("GROUP_NAME")%> &nbsp;(<%# Eval("GROUP_TYPE")%>)
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            User Type
                                            <br />
                                            <asp:DropDownList ID="ddUserType" AutoPostBack="true" OnSelectedIndexChanged="search" runat="server">
                                                <asp:ListItem Text="ALL" Value="-1"></asp:ListItem>
                                                <asp:ListItem Text="STUDENT" Value="STUDENT"></asp:ListItem>
                                                <asp:ListItem Text="STAFF" Value="STAFF"></asp:ListItem>
                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <%# Eval("GROUP_USER_TYPE")%>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            List
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <asp:LinkButton ID="lnkList" CommandName="list" CommandArgument='<%# Eval("GROUP_ID")%>' runat="server">List</asp:LinkButton>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Delete
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <asp:LinkButton ID="lnkdelete" CommandName="deleting" CommandArgument='<%# Eval("GROUP_ID")%>' runat="server">Delete</asp:LinkButton>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Send
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <asp:LinkButton ID="lnksms" CommandName="sms" CommandArgument='<%# Eval("GROUP_ID")%>' runat="server" Enabled='<%# Bind("SMS_FT_ENABLED") %>'>SMS</asp:LinkButton>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Send
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <asp:LinkButton ID="lnkemail" CommandName="email" CommandArgument='<%# Eval("GROUP_ID")%>' runat="server">Email</asp:LinkButton>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem"   Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle CssClass="gridheader_pop"  Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                            <asp:HiddenField ID="hiddenmodule" runat="server" />
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</asp:Content>

