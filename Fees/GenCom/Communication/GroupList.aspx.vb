﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class Medical_Communication_GroupList
    Inherits BasePage

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hiddenmodule.Value = "GEN"
            BindGrid()
        End If
    End Sub


    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim groupid = ""
        Dim groupname = ""
        Dim use_type = "-1"

        If GridInfo.Rows.Count > 0 Then
            groupid = DirectCast(GridInfo.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
            groupname = DirectCast(GridInfo.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
            use_type = DirectCast(GridInfo.HeaderRow.FindControl("ddUserType"), DropDownList).SelectedValue
        End If


        Dim query = " select A.*, BSU_SMS_FT_ENABLED as SMS_FT_ENABLED from COM_V2_GROUP_MASTER A WITH (NOLOCK) INNER JOIN OASIS..BUSINESSUNIT_M B WITH (NOLOCK) ON GROUP_BSU_ID=BSU_ID where GROUP_EMP_ID='" & Session("EmployeeId") & "' AND GROUP_BSU_ID='" & Session("sbsuid") & "' and GROUP_MODULE='" & hiddenmodule.Value & "' and GROUP_DELETE='False'"

        If groupid <> "" Then
            query &= " AND GROUP_ID LIKE '%" & groupid & "%'"
        End If

        If groupname <> "" Then
            query &= " AND GROUP_NAME LIKE '%" & groupname & "%'"
        End If

        If use_type <> "-1" Then
            query &= " AND GROUP_USER_TYPE = '" & use_type & "'"
        End If

        query &= " order by GROUP_DATE desc"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        GridInfo.DataSource = ds
        GridInfo.DataBind()

    End Sub

    Protected Sub lnkadd_Click(sender As Object, e As System.EventArgs) Handles lnkadd.Click
        Response.Redirect("~/GenCom/Pages/ScheduleGroups.aspx")
    End Sub

    Public Sub search(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid()
    End Sub

    Protected Sub GridInfo_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridInfo.RowCommand

        If e.CommandName = "search" Then
            BindGrid()
        End If

        If e.CommandName = "list" Then
            Response.Redirect("ListGroupsAddOnList.aspx?mid=" & hiddenmodule.Value & "&gpid=" & e.CommandArgument)
        End If

        If e.CommandName = "deleting" Then

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim query = "UPDATE COM_V2_GROUP_MASTER SET GROUP_DELETE='True' where GROUP_ID='" & e.CommandArgument & "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, query)
            BindGrid()
        End If


        If e.CommandName = "sms" Then
            Response.Redirect("comManageSMS.aspx?mid=" & hiddenmodule.Value & "&gpid=" & e.CommandArgument)
        End If

        If e.CommandName = "email" Then
            Response.Redirect("comEditPlainText.aspx?mid=" & hiddenmodule.Value & "&gpid=" & e.CommandArgument)
        End If


    End Sub


    Protected Sub GridInfo_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging
        GridInfo.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class
