﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="comCreatePlainText.aspx.vb" Inherits="Transport_GPS_Tracking_Communication_comCreatePlainText" %>

<%@ Register src="comCreatePlainText.ascx" tagname="comCreatePlainText" tagprefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Create New Template
           
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">



        <uc1:comCreatePlainText ID="comCreatePlainText1" runat="server" />

                
            </div>
        </div>
    </div>
</asp:Content>
