﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comSendMessage.ascx.vb"
    Inherits="Transport_GPS_Tracking_Communication_comSendMessage" %>


<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
<link href="../../cssfiles/Popup.css" rel="stylesheet" />

<script type="text/javascript">
    var XMLHTTPRequestObject = false;
    if (window.ActiveXObject) {
        XMLHTTPRequestObject = new ActiveXObject('Microsoft.XMLHTTP');
    }

    var PWidth = 0
    window.setTimeout('BindData()', 500);

    function BindData() {

        var sendid = document.getElementById('<%= HiddenSendingId.ClientID %>').value

        if (sendid != '') {

            var datasource = 'Webservice/GetMessageStatusV2.asmx/GetEmailNormalStatus?Sending_id=' + sendid;

            XMLHTTPRequestObject.open('GET', datasource, true);
            XMLHTTPRequestObject.onreadystatechange = function () {
                if (XMLHTTPRequestObject.readystate == 4 && XMLHTTPRequestObject.status == 200) {
                    var xmlDoc = XMLHTTPRequestObject.responseXML;
                    var xmlDocc = new ActiveXObject("Microsoft.XMLDOM");
                    xmlDocc.load(xmlDoc);
                    xmlObj = xmlDocc.documentElement
                    PWidth = xmlObj.childNodes.item(0).childNodes.item(5).text;
                    if (PWidth <= 100) {

                        document.getElementById("ProgressImage").width = PWidth * 2;
                    }
                    document.getElementById('Td1').innerHTML = xmlObj.childNodes.item(0).childNodes.item(5).text + ' %'
                    document.getElementById('Td2').innerHTML = xmlObj.childNodes.item(0).childNodes.item(1).text
                    document.getElementById('Td3').innerHTML = xmlObj.childNodes.item(0).childNodes.item(2).text
                    document.getElementById('Td4').innerHTML = xmlObj.childNodes.item(0).childNodes.item(0).text
                    document.getElementById('Td5').innerHTML = xmlObj.childNodes.item(0).childNodes.item(3).text
                    document.getElementById('Td6').innerHTML = xmlObj.childNodes.item(0).childNodes.item(4).text

                }


            }


            XMLHTTPRequestObject.send(null);
        }
        timeout = window.setTimeout('BindData();', 500);
        return false;

    }

    function ClosePopup() {

        parent.CloseFrame();
    }
</script>

<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<div align="center" class="matters">

    <br />
    <br />
    <table id="TBL1" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">Send Message
            </td>
        </tr>
        <tr>
            <td>
                <%--            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>--%>
                <table width="100%">
                    <tr>
                        <td align="center">
                            <asp:RadioButtonList ID="RadioSend" runat="server" RepeatDirection="Horizontal"
                                AutoPostBack="True">
                                <asp:ListItem Selected="True" Text="Now" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Schedule" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="TR1" runat="server" visible="false">
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Date</span>
                                    </td>
                                    <td align="left" width="40%">
                                        <asp:TextBox ID="txtdate" runat="server" ValidationGroup="ss"></asp:TextBox>
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    </td>
                                    <td align="left" width="40%"></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Time</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddhour" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddmins" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnok2" runat="server" CssClass="button" Text="Send"
                                ValidationGroup="ss" />
                            <asp:Button ID="btncancel2" runat="server" CausesValidation="False" CssClass="button"
                                OnClientClick="ClosePopup();" Text="Cancel" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">

                            <asp:Label ID="lblsmessage" runat="server" CssClass="text-danger"></asp:Label>
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
                    TargetControlID="txtdate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="HiddenTemplateid" runat="server" />
                <asp:HiddenField ID="HiddenGroupid" runat="server" />
                <asp:HiddenField ID="Hiddenlogid" runat="server" />
                <asp:HiddenField ID="Hiddenbsuid" runat="server" />
                <asp:HiddenField ID="HiddenMessagetype" runat="server" />
                <asp:HiddenField ID="HiddenModule" runat="server" />
                <%--                </ContentTemplate>
            </asp:UpdatePanel>--%>
            </td>
        </tr>
    </table>

    <table id="TBL2" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">Sending Status</td>
        </tr>
        <tr>
            <td align="center">



                <table width="100%">
                    <tr>
                        <td colspan="2" style="border-style: groove" align="left">
                            <img id="ProgressImage" src="../../../Images/progressbar.gif" alt="Progress Bar" width="10" height="20" />

                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Completed</span></td>

                        <td id="Td1" align="left" width="80%"></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Success</span></td>

                        <td id="Td2" align="left"></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Failed</span></td>

                        <td id="Td3" align="left"></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Total</span></td>

                        <td id="Td4" align="left"></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Start Time</span></td>

                        <td id="Td5" align="left"></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">End Time</span></td>

                        <td id="Td6" align="left"></td>
                    </tr>
                </table>



            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HiddenSendingId" runat="server" />

</div>
