﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comCommunicationReport.ascx.vb"
    Inherits="Transport_GPS_Tracking_Communication_comCommunicationReport" %>
<script type="text/javascript">

    function openWindowActions(sendid) {

        var sFeatures;
        sFeatures = "dialogWidth: 300px; ";
        sFeatures += "dialogHeight: 300px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: yes; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var strOpen = "comChangeActions.aspx?sendid=" + sendid

        var result;
        result = window.showModalDialog(strOpen, "", sFeatures);
        window.location.reload(true)


    }

    function openLog(sendid, usertype) {
        window.open("comSendLog.aspx?send_id=" + sendid + '&usertype=' + usertype, "", "Width=1000,Height=600,scrollbars=1");
    }

    window.setTimeout('window.location.reload(true)', 30000)

</script>

<div class="card mb-3">
    <div class="card-header letter-space">
        <i class="fa fa-users mr-3"></i>
        <asp:Label ID="lblCaption" runat="server" Text="Email/SMS Report"></asp:Label>
    </div>

    <div class="card-body">
        <div class="table-responsive m-auto">

            <table width="100%">
                <tr>
                    <td width="20%"><span class="field-label">Reports</span>
                    </td>
                    <td width="30%">
                        <asp:DropDownList ID="ddBsu" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td width="20%"><span class="field-label">Type</span> 
                    </td>
                    <td width="30%">
                        <asp:DropDownList ID="DropType" runat="server" AutoPostBack="True">
                            <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="SMS" Value="SMS"></asp:ListItem>
                            <asp:ListItem Text="EMAIL" Value="EMAIL"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="4">
                            <asp:GridView ID="GridInfo" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Information not available."
                                PageSize="20" Width="100%" AllowSorting="True">
                                <RowStyle CssClass="griditem" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">Send ID
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <%# Eval("SEND_ID")%>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">BSU
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <%# Eval("BSU_SHORTNAME")%>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">Module/Type
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>

                                        <%# Eval("MODULE_TYPE")%>/<%# Eval("MESSGE_TYPE")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">Group
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>

                                        <%# Eval("GROUP_DESC")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">Message
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>

                                        <%# Eval("MESSAGE")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">Schedule/Send Date
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <%# Eval("SEND_DATE_TIME")%>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">End Date
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <%# Eval("END_DATE_TIME")%>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">Total
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <%# Eval("TOTAL_DATA")%>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">Success
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <%# Eval("SUCCESS")%>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">Failed
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <%# Eval("FAILED")%>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">Status
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <%# Eval("STATUS")%>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">Sent By
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <%# Eval("NAME")%>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">Action
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <asp:LinkButton ID="LinkAction" OnClientClick='<%# Eval("openWindowActions")%>' runat="server">Action</asp:LinkButton>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">Log
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <asp:LinkButton ID="LinkLog" OnClientClick='<%# Eval("openLog")%>' runat="server">Log</asp:LinkButton>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                            <EmptyDataRowStyle Wrap="False" />
                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                            <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                            <EditRowStyle Wrap="False" />
                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<asp:HiddenField ID="HiddenModule" runat="server" />

