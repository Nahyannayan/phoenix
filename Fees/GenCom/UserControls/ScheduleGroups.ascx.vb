﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection


Imports System.Web.Configuration
Imports System
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports GemBox.Spreadsheet
Imports ResponseHelper
Partial Class Gencom_StudRecords
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'txtason.Text = Today.ToString("dd/MMM/yyyy")
            bindAcademic_Year()
            'Bindvacc()

            TBL2.Visible = False
        End If
    End Sub

    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ACY_ID as ACY_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id = '" & Session("sBSUID") & "'"
            ddlAcdID.Items.Clear()

            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlAcdID.Items.Add(New ListItem(reader("ACY_DESCR"), reader("ACY_ID")))
            End While
            reader.Close()
            ddlAcdID.ClearSelection()
            ddlAcdID.Items.FindByValue(Session("Current_ACY_ID")).Selected = True
            ddlAcdID_SelectedIndexChanged(ddlAcdID, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcdID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcdID.SelectedIndexChanged
        bindCLM()
    End Sub
    Protected Sub btnStuNo_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindGrid()
    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindGrid()
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub
    Protected Sub ddlCurri_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurri.SelectedIndexChanged
        BindGrade()
    End Sub

    Public Sub BindGrid()
        lblmessage.Text = ""
        Dim clm As String = ddlCurri.SelectedValue
        Dim acy_id As String = ddlAcdID.SelectedValue
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Dim strQuery As String = " SELECT stu_currstatus,STP_ACD_ID,STP_BSU_ID,STU_ID,STU_NO,GRM_DISPLAY ,SNAME,GRD_DISPLAYORDER,STP_GRD_ID," & _
" STP_SCT_ID,STU_PASPRTNO,STU_EMGCONTACT,STS_FMOBILE,STS_MMOBILE,STS_GMOBILE,STS_FEMAIL,STS_MEMAIL,STS_GEMAIL FROM (SELECT DATEDIFF(YY,STU_DOB,GETDATE()) AGE, STU_DOB,STU_DOJ,STUDENT_M.STU_CURRSTATUS,STUDENT_PROMO_S.STP_ACD_ID, " & _
" STUDENT_PROMO_S.STP_BSU_ID, STUDENT_M.STU_ID, STUDENT_M.STU_NO, GRADE_BSU_M.GRM_DISPLAY + ' -  ' + SECTION_M.SCT_DESCR AS GRM_DISPLAY," & _
"  BUSINESSUNIT_M.BSU_bSTUD_DISPLAYPASSPRTNAME,CASE WHEN (SELECT     isnull(BUSINESSUNIT_M.BSU_bSTUD_DISPLAYPASSPRTNAME, 0) FROM " & _
"  OASIS.dbo.businessunit_m  WHERE  bsu_id = STUDENT_PROMO_S.STP_BSU_ID) = 1 THEN STUDENT_M.STU_PASPRTNAME ELSE " & _
" STUDENT_M.STU_FIRSTNAME + ' ' + isnull(STUDENT_M.STU_MIDNAME, '')  + ' ' + isnull(STUDENT_M.STU_LASTNAME, '') END AS sname," & _
" GRADE_M.GRD_DISPLAYORDER, STUDENT_PROMO_S.STP_GRD_ID, STUDENT_PROMO_S.STP_SCT_ID, ISNULL(STUDENT_M.STU_PASPRTNO,'') AS STU_PASPRTNO," & _
" STUDENT_M.STU_EMGCONTACT, STUDENT_D.STS_FMOBILE, " & _
" STUDENT_D.STS_MMOBILE, STUDENT_D.STS_GMOBILE ," & _
" STUDENT_D.STS_FEMAIL, " & _
" STUDENT_D.STS_MEMAIL ,STUDENT_D.STS_GEMAIL  " & _
" FROM OASIS.dbo.STUDENT_PROMO_S INNER JOIN OASIS.dbo.STUDENT_M ON STUDENT_PROMO_S.STP_STU_ID=STUDENT_M.STU_ID INNER JOIN OASIS.dbo.SECTION_M ON STUDENT_PROMO_S.STP_SCT_ID = SECTION_M.SCT_ID " & _
" INNER JOIN OASIS.dbo.GRADE_BSU_M ON GRADE_BSU_M.GRM_ID = STUDENT_PROMO_S.STP_GRM_ID INNER JOIN SECTION_M SCT ON STP_SCT_ID=SCT.SCT_ID AND SCT.SCT_EMP_ID='" & Session("EmployeeId") & "' INNER JOIN  OASIS.dbo.BUSINESSUNIT_M ON STUDENT_PROMO_S.STP_BSU_ID = " & _
" BUSINESSUNIT_M.BSU_ID INNER JOIN OASIS.dbo.GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID INNER JOIN " & _
" OASIS.dbo.STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID   ) A WHERE STU_CURRSTATUS NOT IN ('CN','TF') AND STP_BSU_ID='" & Session("sBsuid") & "' "



        If ddlCurri.SelectedValue.ToString <> "0" Then
            strQuery += " AND STP_ACD_ID=(SELECT ACD_ID  FROM  OASIS.dbo.ACADEMICYEAR_D  WHERE  (ACD_CLM_ID = '" & clm & "') AND (ACD_ACY_ID = '" & acy_id & "') AND (ACD_BSU_ID = '" & Session("sBsuid") & "'))"
        Else
            strQuery += " AND STP_ACD_ID IN(SELECT DISTINCT ACD_ID  FROM  OASIS.dbo.ACADEMICYEAR_D  WHERE  (ACD_ACY_ID = '" & acy_id & "') AND (ACD_BSU_ID = '" & Session("sBsuid") & "'))"
        End If

        If ddlGrade.SelectedValue.ToString <> "0" Then
            strQuery += " AND STP_GRD_ID='" & ddlGrade.SelectedValue.ToString & "'"
        End If

        If ddlSection.SelectedValue.ToString <> "0" Then
            strQuery += " AND STP_SCT_ID=" + ddlSection.SelectedValue.ToString
        End If

        If txtStuNo.Text <> "" Then
            strQuery += " AND STU_NO LIKE '%" + txtStuNo.Text.Trim + "%'"
        End If

        If txtName.Text <> "" Then
            strQuery += " AND SNAME LIKE '%" + txtName.Text.Trim + "%'"
        End If

       

        If txtdojfrom.Text <> "" And txtdojto.Text <> "" Then
            strQuery += " AND STU_DOJ BETWEEN '" & txtdojfrom.Text & "' AND '" & txtdojto.Text & "' "
        End If

       

        '' Dosage filter

        



        strQuery = "  SELECT a.STU_NO AS USR_ID,STUDNAME AS NAME,GRD_SEC AS DETAILS,PARENT_NAME AS CONTACT_NAME,isnull('971'+ right(replace(CONTACT_NO,'-',''),9),'') AS MOBILE, EMAIL  FROM OASIS.dbo.VV_STUDENT_DETAILS A inner join( " & strQuery & "  ) b on A.STU_ID=b.STU_ID "
        Hiddenquery.Value = strQuery
        strQuery += " ORDER BY STUDNAME "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        GridInfo.DataSource = ds
        GridInfo.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            TBL2.Visible = True
        Else
            TBL2.Visible = False
        End If

        If ddGropType.SelectedValue = "G" Then
            GridInfo.Columns(0).Visible = False
        Else
            GridInfo.Columns(0).Visible = True
        End If

    End Sub

    Sub bindCLM()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim acd_id As String = ddlAcdID.SelectedValue

        str_query = " select clm_descr,clm_id from dbo.CURRICULUM_M where clm_id in " & _
" (select distinct acd_clm_id from academicyear_d where acd_acy_id='" & acd_id & "' and acd_bsu_id='" & Session("sBsuid") & "')"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlCurri.DataSource = ds
        ddlCurri.DataTextField = "clm_descr"
        ddlCurri.DataValueField = "clm_id"
        ddlCurri.DataBind()

        If ddlCurri.Items.Count > 1 Then
            ddlCurri.Items.Add(New ListItem("ALL", "0"))
            ddlCurri.ClearSelection()
            ddlCurri.Items.FindByText("ALL").Selected = True
            trCLM.Visible = True
        Else
            trCLM.Visible = False
        End If
        ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
    End Sub



    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim clm As String = ddlCurri.SelectedValue
        Dim acy_id As String = ddlAcdID.SelectedValue

        Dim ds As DataSet

        If clm <> "0" Then
            str_query = " SELECT     distinct GRADE_BSU_M.GRM_GRD_ID, GRADE_BSU_M.GRM_DISPLAY,GRADE_M.GRD_DISPLAYORDER " & _
           " FROM GRADE_BSU_M INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID INNER JOIN SECTION_M SCT ON GRM_ID=SCT_GRM_ID" & _
           " WHERE SCT_EMP_ID='" & Session("EmployeeId") & "' AND (GRADE_BSU_M.GRM_BSU_ID = '" & Session("sBsuid") & "') AND (GRADE_BSU_M.GRM_ACY_ID = '" & acy_id & "') AND (GRADE_BSU_M.GRM_ACD_ID = " & _
           " (SELECT ACD_ID  FROM  ACADEMICYEAR_D  WHERE  (ACD_CLM_ID ='" & clm & "') AND " & _
           "  (ACD_ACY_ID = '" & acy_id & "') AND (ACD_BSU_ID = '" & Session("sBsuid") & "'))) order by GRADE_M.GRD_DISPLAYORDER "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "grm_display"
            ddlGrade.DataValueField = "grm_grd_id"
            ddlGrade.DataBind()
            'ddlGrade.Items.Add(New ListItem("ALL", "0"))
            'ddlGrade.ClearSelection()
            'ddlGrade.Items.FindByText("ALL").Selected = True
            ddlGrade.Enabled = True
            'Else
            '    ddlGrade.Items.Add(New ListItem("ALL", "0"))
            '    ddlGrade.ClearSelection()
            '    ddlGrade.Items.FindByText("ALL").Selected = True
            '    ddlGrade.Enabled = False
        End If


        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)


    End Sub

    Sub BindSection()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim clm As String = ddlCurri.SelectedValue
        Dim GRD_ID As String = ddlGrade.SelectedValue
        Dim acy_id As String = ddlAcdID.SelectedValue
        If clm <> "0" Then
            Dim str_query As String = " SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE  SCT_EMP_ID='" & Session("EmployeeId") & "' AND SCT_GRD_ID= '" & GRD_ID & "'" & _
           " AND SCT_ACD_ID=(SELECT ACD_ID  FROM  ACADEMICYEAR_D  WHERE  (ACD_CLM_ID = '" & clm & "') AND " & _
            " (ACD_ACY_ID = '" & acy_id & "') AND (ACD_BSU_ID = '" & Session("sBsuid") & "')) ORDER BY SCT_DESCR"

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Add(New ListItem("ALL", "0"))
            ddlSection.ClearSelection()
            ddlSection.Items.FindByText("ALL").Selected = True
            ddlSection.Enabled = True
            'Else
            '    ddlSection.Items.Add(New ListItem("ALL", "0"))
            '    ddlSection.ClearSelection()
            '    ddlSection.Items.FindByText("ALL").Selected = True
            '    ddlSection.Enabled = False
        End If

    End Sub


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindGrid()

    End Sub


    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging
        GridInfo.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Public Sub subBindPreviewGrid()
        lblmessage.Text = ""
        If Validate() Then
            TBL2.Visible = True
            lblmessage.Text = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(13) As SqlClient.SqlParameter

            Dim user_id = ""
            Dim user_name = ""
            Dim details = ""
            Dim contactname = ""
            Dim contactno = ""
            Dim email = ""

            If GridInfo.Rows.Count > 0 Then
                user_id = DirectCast(GridInfo.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
                user_name = DirectCast(GridInfo.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
                details = DirectCast(GridInfo.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim()
                contactname = DirectCast(GridInfo.HeaderRow.FindControl("Txt4"), TextBox).Text.Trim()
                contactno = DirectCast(GridInfo.HeaderRow.FindControl("Txt5"), TextBox).Text.Trim()
                email = DirectCast(GridInfo.HeaderRow.FindControl("Txt6"), TextBox).Text.Trim()
            End If


            Dim query = "select * from (" & Hiddenquery.Value & ") a where 1=1 "


            If user_id <> "" Then
                query &= " AND usr_id like '%" & user_id & "%'"
            End If

            If user_name <> "" Then
                query &= " AND NAME like '%" & user_name & "%'"
            End If

            If details <> "" Then
                query &= " AND DETAILS like '%" & details & "%'"
            End If

            If contactname <> "" Then
                query &= " AND CONTACT_NAME like '%" & contactname & "%'"
            End If

            If contactno <> "" Then
                query &= " AND MOBILE like '%" & contactno & "%'"
            End If

            If email <> "" Then
                query &= " AND EMAIL like '%" & email & "%'"
            End If

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query, pParms)

            GridInfo.DataSource = ds
            GridInfo.DataBind()

            If GridInfo.Rows.Count > 0 Then

                DirectCast(GridInfo.HeaderRow.FindControl("Txt1"), TextBox).Text = user_id
                DirectCast(GridInfo.HeaderRow.FindControl("Txt2"), TextBox).Text = user_name
                DirectCast(GridInfo.HeaderRow.FindControl("Txt3"), TextBox).Text = details
                DirectCast(GridInfo.HeaderRow.FindControl("Txt4"), TextBox).Text = contactname
                DirectCast(GridInfo.HeaderRow.FindControl("Txt5"), TextBox).Text = contactno
                DirectCast(GridInfo.HeaderRow.FindControl("Txt6"), TextBox).Text = email

                If ddGropType.SelectedValue = "G" Then
                    GridInfo.Columns(0).Visible = False
                Else
                    GridInfo.Columns(0).Visible = True
                End If

            End If

        End If

    End Sub

    Public Function Validate() As Boolean
        Dim rval = True
        lblmessage.Text = ""

        If txtgroupname.Text = "" Then
            rval = False
            lblmessage.Text = "Please enter Group description"
        End If

        Return rval
    End Function

    Protected Sub GridInfo_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridInfo.RowCommand

        If e.CommandName = "search" Then
            subBindPreviewGrid()
        End If

        If e.CommandName = "add" Then

            Dim hash As New Hashtable

            If Session("smsids") Is Nothing Then
            Else
                hash = Session("smsids")
            End If

            For Each row As GridViewRow In GridInfo.Rows

                If DirectCast(row.FindControl("ch1"), CheckBox).Checked = True Then
                    Dim id = DirectCast(row.FindControl("HiddenUserid"), HiddenField).Value.Trim()

                    If Not hash.ContainsKey(id) Then
                        hash.Add(id, "'" & id & "'")
                    End If

                End If

            Next

            Session("smsids") = hash

            AddonGridBind()

        End If

    End Sub

    Public Sub AddonGridBind()
        lblmessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(7) As SqlClient.SqlParameter


        Dim query = "select * from (" & Hiddenquery.Value & ") a where 1=1 "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        Dim hash As Hashtable
        hash = Session("smsids")
        Dim val = ""
        For Each Item As Object In hash
            If val = "" Then
                val = Item.Value
            Else
                val &= "," & Item.Value
            End If

        Next
        If val <> "" Then
            Dim filter = "USR_ID in (" & val & ")"
            Dim dvwView As New DataView
            dvwView = ds.Tables(0).DefaultView
            dvwView.RowFilter = filter

            GridInfo0.DataSource = dvwView
            GridInfo0.DataBind()
        Else
            GridInfo0.DataSource = Nothing
            GridInfo0.DataBind()
        End If


    End Sub

    Protected Sub GridInfo0_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridInfo0.RowCommand


        If e.CommandName = "deleting" Then

            Dim hash As Hashtable
            hash = Session("smsids")
            For Each row As GridViewRow In GridInfo0.Rows
                If DirectCast(row.FindControl("ch2"), CheckBox).Checked = True Then
                    Dim id = DirectCast(row.FindControl("HiddenUserid"), HiddenField).Value.Trim()
                    hash.Remove(id)
                End If

            Next
            Session("smsids") = hash
            AddonGridBind()

        End If


    End Sub


    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim hash As New Hashtable
        Dim val = ""
        If Session("smsids") Is Nothing Then

        Else

            hash = Session("smsids")

            For Each Item As Object In hash
                If val = "" Then
                    val = Item.Value
                Else
                    val &= "," & Item.Value
                End If
            Next

        End If

        If Validate() Then

            lblmessage.Text = ""
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(12) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@GROUP_NAME", txtgroupname.Text.Trim())
            pParms(1) = New SqlClient.SqlParameter("@GROUP_MODULE", "GEN")
            pParms(2) = New SqlClient.SqlParameter("@GROUP_TYPE", ddGropType.SelectedValue)
            pParms(3) = New SqlClient.SqlParameter("@GROUP_USER_TYPE", "STUDENT")
            pParms(4) = New SqlClient.SqlParameter("@GROUP_BSU_ID", Session("sbsuid"))
            pParms(5) = New SqlClient.SqlParameter("@GROUP_QUERY", Hiddenquery.Value)
            pParms(6) = New SqlClient.SqlParameter("@GROUP_ADD_ON_IDS", val)
            pParms(7) = New SqlClient.SqlParameter("@GROUP_SEND_TO", "P")
            pParms(8) = New SqlClient.SqlParameter("@GROUP_EMP_ID", Session("EmployeeId"))
            pParms(9) = New SqlClient.SqlParameter("@OPTION", 1)

            lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "COM_V2_GROUP_MASTER_TRAN", pParms)


        End If


        If lblmessage.Text.IndexOf("exists") > -1 Then

        Else
            hash.Clear()
            Session("smsids") = hash
            Response.Redirect("~/GenCom/Communication/GroupList.aspx")
        End If

    End Sub






End Class
