﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ScheduleGroups.ascx.vb" Inherits="Gencom_StudRecords" %>


<script type="text/javascript">
    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }

    function change_chk_stateg2(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch2") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }




</script>
<div class="matters">
    <table id="tblStud" runat="server" width="100%">
        <tr>
            <td align="left" class="matters" width="20%"><span class="field-label">Group Name</span></td>
            <td align="left" class="matters" width="30%" >
                <asp:TextBox ID="txtgroupname" runat="server"></asp:TextBox><br />
                <asp:DropDownList ID="ddGropType" runat="server">
                    <asp:ListItem Text="Group" Value="G"></asp:ListItem>
                    <asp:ListItem Text="Add On" Value="AD"></asp:ListItem>
                </asp:DropDownList>
            </td>
      
            <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
            <td align="left" class="matters"  width="30%" >
                <asp:DropDownList ID="ddlAcdID" runat="server"  
                    OnSelectedIndexChanged="ddlAcdID_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList></td>
        </tr>
        <tr runat="server" id="trCLM">
            <td align="left" class="matters"><span class="field-label">Curriculum <span   color: #ff0000">*</span></span></td>
            <td align="left" class="matters"  >
                <asp:DropDownList ID="ddlCurri" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCurri_SelectedIndexChanged">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="matters"><span class="field-label">Select Grade <span style="color: #ff0000">*</span></span></td>
            <td align="left" class="matters">
                <asp:DropDownList ID="ddlGrade"   runat="server" AutoPostBack="True"  >
                </asp:DropDownList></td>
            <td align="left" class="matters"><span class="field-label">Select Section</span> </td>
            <td align="left" class="matters">
                <asp:DropDownList ID="ddlSection"   runat="server" >
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="matters"><span class="field-label">Student No</span></td>
            <td align="left" class="matters">
                <asp:TextBox ID="txtStuNo" runat="server"  >
                </asp:TextBox>
                <ajaxToolkit:AutoCompleteExtender ID="acSTU_NO" runat="server" BehaviorID="AutoCompleteEx1"
                    CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                    CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                    ServiceMethod="StudentNo" ServicePath="~/Students/WebServices/StudentService.asmx"
                    TargetControlID="txtStuNo">
                    <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                </ajaxToolkit:AutoCompleteExtender>
            </td>
            <td align="left" class="matters"><span class="field-label">Name</span></td>
            <td align="left" class="matters">
                <asp:TextBox ID="txtName" runat="server"  >
                </asp:TextBox>
                <ajaxToolkit:AutoCompleteExtender ID="acSAME" runat="server" BehaviorID="AutoCompleteEx2"
                    CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                    CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                    ServiceMethod="StudentNAME" ServicePath="~/Students/WebServices/StudentService.asmx"
                    TargetControlID="txtNAME">
                    <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                </ajaxToolkit:AutoCompleteExtender>
            </td>

        </tr>
        <tr>
            <td align="left" class="matters"><span class="field-label">Date of Join(From)</span></td>
            <td align="left" class="matters">
                <asp:TextBox ID="txtdojfrom" runat="server"  > </asp:TextBox></td>
            <td align="left" class="matters"><span class="field-label">Date of Join(To)</span></td>
            <td align="left" class="matters">
                <asp:TextBox ID="txtdojto" runat="server" > </asp:TextBox></td>
        </tr>

        <tr>
            <td colspan="4" align="center">
                <asp:Button ID="btnSearch" runat="server" Text="Preview" CssClass="button" TabIndex="4"   CausesValidation="False" />
            </td>
        </tr>

    </table>

    <%--<ajaxToolkit:CalendarExtender id="CalendarExtender3" TargetControlID="txtason" PopupButtonId="txtason"  Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>--%>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="txtdojfrom" PopupButtonID="txtdojfrom" Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" TargetControlID="txtdojto" PopupButtonID="txtdojto" Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>


    <br />



    <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
    <br />
    <table id="TBL2"  width="100%" runat="server">
        <tr>
            <td class="title-bg-small">User List</td>
        </tr>
        <tr>
            <td align="left">
                <asp:GridView ID="GridInfo" runat="server" AllowPaging="True" ShowFooter="true" CssClass="table table-bordered table-row"
                    AutoGenerateColumns="false" EmptyDataText="Information not available."
                    Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Check
                                       <br />
                                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                                ToolTip="Click here to select/deselect all rows" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <asp:CheckBox ID="ch1" runat="server" />
                                </center>
                            </ItemTemplate>
                            <FooterTemplate>
                                <center>
                            <asp:Button ID="btnadd" runat="server" CssClass="button" CommandName="add" Text="Add" />
                        </center>

                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                User ID
                                      <br />
                                            <asp:TextBox ID="Txt1"  runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch1" CausesValidation="false" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%# Eval("USR_ID")%>
                                <asp:HiddenField ID="HiddenUserid" Value='<%# Eval("USR_ID")%>' runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                User Name
                                              <br />
                                            <asp:TextBox ID="Txt2"   runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch2" CausesValidation="false" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />


                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("NAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Details
                                               <br />
                                            <asp:TextBox ID="Txt3"  runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch3" CausesValidation="false" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />


                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                           <%# Eval("DETAILS")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Contact Name
                                               <br />
                                            <asp:TextBox ID="Txt4"  runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch4" CausesValidation="false" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("CONTACT_NAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Contact No
                                               <br />
                                            <asp:TextBox ID="Txt5"   runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch5" CausesValidation="false" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                            <%# Eval("MOBILE")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Email Id
                                               <br />
                                            <asp:TextBox ID="Txt6"  runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch6" CausesValidation="false" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("EMAIL")%>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <RowStyle CssClass="griditem"   Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop"   Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>

                <br />

                <asp:GridView ID="GridInfo0" runat="server" ShowFooter="true" CssClass="table table-bordered table-row"
                    AutoGenerateColumns="false"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Check
                                       <br />
                                            <asp:CheckBox ID="chkAll0" runat="server" onclick="javascript:change_chk_stateg2(this);"
                                                ToolTip="Click here to select/deselect all rows" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <asp:CheckBox ID="ch2" runat="server" />
                                </center>
                            </ItemTemplate>
                            <FooterTemplate>
                                <center>
                            <asp:Button ID="btndelete" runat="server" CssClass="button" CommandName="deleting" Text="Delete" />
                        </center>

                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                User ID
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%# Eval("USR_ID")%>
      <asp:HiddenField ID="HiddenUserid" Value='<%# Eval("USR_ID")%>' runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                User Name
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("NAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Details
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                           <%# Eval("DETAILS")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Contact Name
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("CONTACT_NAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Contact No
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                            <%# Eval("MOBILE")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Email Id
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("EMAIL")%>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <RowStyle CssClass="griditem"   Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop"   Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
                <br />
                <center>
            <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save"  />
            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel"   />
                <br />
            </center>

            </td>
        </tr>
    </table>

</div>
<asp:HiddenField ID="Hiddenquery" runat="server" />
