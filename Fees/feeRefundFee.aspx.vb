Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports RestSharp
Imports System.Web.Script.Serialization
Imports Newtonsoft.Json.Linq
Imports System.Net
Imports com.ni.dp.util
Imports System.Xml

Partial Class Fees_feeRefundFee
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property IsRefundallowed() As Boolean
        Get
            If ViewState(IsRefundallowed) Is Nothing Then
                ViewState(IsRefundallowed) = False
            End If
            Return ViewState(IsRefundallowed)
        End Get
        Set(ByVal value As Boolean)
            ViewState(IsRefundallowed) = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            txtChqNo.Attributes.Add("readonly", "readonly")
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            h_bIsonDAX.Value = Session("BSU_IsOnDAX")
            InitialiseCompnents()
            'Session("FeeCollection") = FeeCollection.CreateFeeCollection
            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_REFUND_FEE_APPROVAL _
                And ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_REFUND_REQUEST) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Select Case ViewState("MainMnu_code").ToString
                    Case OASISConstants.MNU_FEE_REFUND_REQUEST
                        lblHead.Text = "Fee Refund Request"
                        'tbl_Approval.Visible = False
                        If Request.QueryString("view_id") <> "" Then
                            ViewState("datamode") = "view"
                            'SET_VIEWDATA(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")))
                            LoadSavedData(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")))
                            'tbl_Allocation.Visible = False
                            'tbl_Approval.Visible = True
                            btnSettlement.Visible = True
                        ElseIf ViewState("datamode") = "add" Then
                            If Not Request.QueryString("SID") Is Nothing Then
                                h_STUD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("SID").Replace(" ", "+"))
                                FillSTUNames(h_STUD_ID.Value)
                            End If
                            btnSettlement.Visible = False
                        Else
                            btnSettlement.Visible = False
                        End If
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Case OASISConstants.MNU_FEE_REFUND_FEE_APPROVAL
                        lblHead.Text = "Fee Refund Request Approval"
                        btnApprove.Visible = True
                        btnReject.Visible = True
                        rbEnrollment.Enabled = False
                        rbEnquiry.Enabled = False
                        txtBankCharge.Attributes.Add("readonly", "readonly")
                        txtFrom.Attributes.Add("readonly", "readonly")
                        'imgFrom.Enabled = False
                        tbl_Allocation.Visible = False
                        tbl_Approval.Visible = True
                        If Request.QueryString("view_id") <> "" Then
                            'SET_VIEWDATA(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")))
                            LoadSavedData(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")))
                            SetTCReference()
                            btnSettlement.Visible = True
                            tbl_Allocation.Visible = False
                            tbl_Approval.Visible = True
                        ElseIf ViewState("datamode") = "add" Then
                            If Not Request.QueryString("SID") Is Nothing Then
                                h_STUD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("SID").Replace(" ", "+"))
                                FillSTUNames(h_STUD_ID.Value)
                            End If
                            btnSettlement.Visible = False
                        Else
                            btnSettlement.Visible = False
                        End If
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        btnSave.Visible = False
                End Select
            End If
        End If
    End Sub

    Sub DisableChequeDetails()
        If Session("BSU_IsOnDAX") = "1" Then
            ChkBearer.Enabled = False
            txtChqBook.Enabled = False
            txtChqNo.Enabled = False
        Else
            ChkBearer.Enabled = True
            txtChqBook.Enabled = True
            txtChqNo.Enabled = True
        End If


    End Sub
    Sub set_bankaccount()
        Dim str_bankact_name As String = UtilityObj.GetDataFromSQL("SELECT BSU.BSU_PAYMENTBANK_ACT_ID+'|'+ACT.ACT_NAME " _
        & " FROM VW_OSO_BUSINESSUNIT_M AS BSU INNER JOIN" _
        & " VW_OSA_ACCOUNTS_M AS ACT ON BSU.BSU_PAYMENTBANK_ACT_ID = ACT.ACT_ID" _
        & " WHERE (BSU.BSU_ID = '" & Session("sBsuid") & "')", ConnectionManger.GetOASISFINConnectionString)
        If str_bankact_name <> "--" Then
            txtBankCode.Text = str_bankact_name.Split("|")(0)
            txtBankDescr.Text = str_bankact_name.Split("|")(1)
        End If
    End Sub

    Sub LoadSavedData(ByVal FRH_ID As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim STR_SQL As String = "SELECT FRH.FRH_BSU_ID, FRH.FRH_ACD_ID, FRH.FRH_STU_TYPE, FRH.FRH_STU_ID, " _
        & " FRH.FRH_DATE, FRH.FRH_NARRATION, FRH.FRH_ACT_ID, FRH.FRH_PAIDTO, " _
        & " FRH.FRH_TOTAL, FRH.FRH_FAR_ID, ACT.ACT_NAME, FRH.FRH_CHQDT, " _
        & " FRH.FRH_SCT_ID, FRH.FRH_BANK_CASH, FAR.FAR_REMARKS, FRH.FRH_VHH_DOCNO, " _
        & " isnull(FRH.FRH_bPosted,0) FRH_bPosted,ISNULL(FRH_BANKCHARGE,0) FRH_BANKCHARGE,FRH.FRH_SOURCE FROM FEES.FEE_REFUND_H AS FRH " _
        & " INNER JOIN vw_OSF_ACCOUNTS_M AS ACT ON FRH.FRH_ACT_ID = ACT.ACT_ID " _
        & " LEFT OUTER JOIN FEES.FEEADJREQUEST_H AS FAR ON FRH.FRH_FAR_ID = FAR.FAR_ID WHERE FRH.FRH_ID='" & FRH_ID & "'"
        Dim dsFRH As DataSet
        dsFRH = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, STR_SQL)
        rbBank.Enabled = False
        rbCash.Enabled = False
        If dsFRH.Tables(0).Rows.Count > 0 Then
            ViewState("FRH_VHH_DOCNO") = dsFRH.Tables(0).Rows(0)("FRH_VHH_DOCNO")
            ViewState("FRH_BANK_CASH") = dsFRH.Tables(0).Rows(0)("FRH_BANK_CASH")
            txtBankCharge.Text = Format(dsFRH.Tables(0).Rows(0)("FRH_BANKCHARGE"), "0.00")

            ViewState("FRH_SOURCE") = dsFRH.Tables(0).Rows(0)("FRH_SOURCE")

            If Not ViewState("FRH_VHH_DOCNO") Is Nothing And dsFRH.Tables(0).Rows(0)("FRH_bPosted") = True Then
                btnPrint1.Visible = True
                chkPrintChq.Visible = True
            End If
            If dsFRH.Tables(0).Rows(0)("FRH_BANK_CASH") = "C" Then
                chkPrintChq.Visible = False
                rbBank.Checked = False
                rbCash.Checked = True
                rbCard.Checked = False
                txtCashAcc.Text = dsFRH.Tables(0).Rows(0)("FRH_ACT_ID")
                txtCashDescr.Text = dsFRH.Tables(0).Rows(0)("ACT_NAME")
            ElseIf dsFRH.Tables(0).Rows(0)("FRH_BANK_CASH") = "B" Then
                rbCash.Checked = False
                rbBank.Checked = True
                rbCard.Checked = False
                txtBankCode.Text = dsFRH.Tables(0).Rows(0)("FRH_ACT_ID")
                txtBankDescr.Text = dsFRH.Tables(0).Rows(0)("ACT_NAME")
            ElseIf dsFRH.Tables(0).Rows(0)("FRH_BANK_CASH") = "O" Then
                rbCash.Checked = False
                rbBank.Checked = False
                rbCard.Checked = True
                txtBankCode.Text = dsFRH.Tables(0).Rows(0)("FRH_ACT_ID")
                txtBankDescr.Text = dsFRH.Tables(0).Rows(0)("ACT_NAME")
            End If
            Set_CashorBank()
            set_chequeno_controls()
            txPaidto.Text = dsFRH.Tables(0).Rows(0)("FRH_PAIDTO")
            txtAmount.Text = Format(dsFRH.Tables(0).Rows(0)("FRH_TOTAL"), "0.00")
            txtFrom.Text = Format(dsFRH.Tables(0).Rows(0)("FRH_DATE"), "dd/MMM/yyyy")
            txtChqdt.Text = Format(dsFRH.Tables(0).Rows(0)("FRH_CHQDT"), "dd/MMM/yyyy")
            txtRemarks.Text = dsFRH.Tables(0).Rows(0)("FRH_NARRATION")
            Dim strSTU_TYP As String = dsFRH.Tables(0).Rows(0)("FRH_STU_TYPE").ToString()
            H_STU_TYPE.Value = strSTU_TYP
            If strSTU_TYP = "E" Then
                rbEnquiry.Checked = True
            Else
                rbEnrollment.Checked = True
            End If
            txtAdjustment.Text = dsFRH.Tables(0).Rows(0)("FAR_REMARKS").ToString
            h_Adjustment.Value = dsFRH.Tables(0).Rows(0)("FRH_FAR_ID").ToString

            Dim SQL_FRD As String = "SELECT  FRD_ID ,FRD_FRH_ID ,FRD_FEE_ID ,FRD_AMOUNT ,FRD_STU_ID , " & _
            "FRD_NARRATION FROM FEES.FEE_REFUND_D WHERE FRD_FRH_ID=" & FRH_ID & ""

            Dim dtFRD As DataTable = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, SQL_FRD).Tables(0)
            If dtFRD.Rows.Count > 0 Then
                For Each dr As DataRow In dtFRD.Rows
                    txtStudName.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "SELECT STU_NO FROM VW_OSO_STUDENT_ENQUIRY WHERE STU_ID='" & dr("FRD_STU_ID") & "' and STU_TYPE='" & strSTU_TYP & "'")
                    AddStudent()
                Next
                Dim dtREfundSummary As DataTable = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "EXEC FEES.GET_REFUND_SUMMARY @FRH_ID=" & FRH_ID & "").Tables(0)
                ViewState("gvRefundSummary") = dtREfundSummary
                BindRefundSummary(dtREfundSummary)
            End If
        End If
    End Sub

    Sub SET_VIEWDATA(ByVal p_FRD_FRH_ID As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim STR_SQL As String = "SELECT FRH.FRH_BSU_ID, FRH.FRH_ACD_ID, FRH.FRH_STU_TYPE, FRH.FRH_STU_ID, " _
        & " FRH.FRH_DATE, FRH.FRH_NARRATION, FRH.FRH_ACT_ID, FRH.FRH_PAIDTO, STU.STU_NO, " _
        & " STU.STU_NAME, FRH.FRH_TOTAL, FRH.FRH_FAR_ID, ACT.ACT_NAME, FRH.FRH_CHQDT, " _
        & " FRH.FRH_SCT_ID, FRH.FRH_BANK_CASH, FAR.FAR_REMARKS, FRH.FRH_VHH_DOCNO, " _
        & " isnull(FRH.FRH_bPosted,0) FRH_bPosted,ISNULL(FRH_BANKCHARGE,0) FRH_BANKCHARGE FROM FEES.FEE_REFUND_H AS FRH " _
        & " INNER JOIN VW_OSO_STUDENT_ENQUIRY AS STU ON FRH.FRH_STU_ID = STU.STU_ID " _
        & " INNER JOIN vw_OSF_ACCOUNTS_M AS ACT ON FRH.FRH_ACT_ID = ACT.ACT_ID " _
        & " LEFT OUTER JOIN FEES.FEEADJREQUEST_H AS FAR ON FRH.FRH_FAR_ID = FAR.FAR_ID WHERE FRH.FRH_ID='" & p_FRD_FRH_ID & "'"
        Dim DS1 As New DataSet
        DS1 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, STR_SQL)
        rbBank.Enabled = False
        rbCash.Enabled = False
        H_STU_TYPE.Value = DS1.Tables(0).Rows(0)("FRH_STU_TYPE")
        h_Student_no.Value = DS1.Tables(0).Rows(0)("FRH_STU_ID")
        ViewState("FRH_VHH_DOCNO") = DS1.Tables(0).Rows(0)("FRH_VHH_DOCNO")
        ViewState("FRH_BANK_CASH") = DS1.Tables(0).Rows(0)("FRH_BANK_CASH")
        txtBankCharge.Text = Format(DS1.Tables(0).Rows(0)("FRH_BANKCHARGE"), "0.00")

        If Not ViewState("FRH_VHH_DOCNO") Is Nothing And DS1.Tables(0).Rows(0)("FRH_bPosted") = True Then
            btnPrint1.Visible = True
            chkPrintChq.Visible = True
        End If
        If DS1.Tables(0).Rows(0)("FRH_BANK_CASH") = "C" Then
            chkPrintChq.Visible = False
            rbBank.Checked = False
            rbCard.Checked = False
            rbCash.Checked = True
            txtCashAcc.Text = DS1.Tables(0).Rows(0)("FRH_ACT_ID")
            txtCashDescr.Text = DS1.Tables(0).Rows(0)("ACT_NAME")
        ElseIf DS1.Tables(0).Rows(0)("FRH_BANK_CASH") = "B" Then
            rbCash.Checked = False
            rbCard.Checked = False
            rbBank.Checked = True
            txtBankCode.Text = DS1.Tables(0).Rows(0)("FRH_ACT_ID")
            txtBankDescr.Text = DS1.Tables(0).Rows(0)("ACT_NAME")
        ElseIf DS1.Tables(0).Rows(0)("FRH_BANK_CASH") = "O" Then
            rbCash.Checked = False
            rbBank.Checked = False
            rbCard.Checked = True
            txtBankCode.Text = DS1.Tables(0).Rows(0)("FRH_ACT_ID")
            txtBankDescr.Text = DS1.Tables(0).Rows(0)("ACT_NAME")
        End If
        Set_CashorBank()
        set_chequeno_controls()
        txPaidto.Text = DS1.Tables(0).Rows(0)("FRH_PAIDTO")
        txtAmount.Text = Format(DS1.Tables(0).Rows(0)("FRH_TOTAL"), "0.00")
        txtFrom.Text = Format(DS1.Tables(0).Rows(0)("FRH_DATE"), "dd/MMM/yyyy")
        txtChqdt.Text = Format(DS1.Tables(0).Rows(0)("FRH_CHQDT"), "dd/MMM/yyyy")
        txtRemarks.Text = DS1.Tables(0).Rows(0)("FRH_NARRATION")
        Dim strSTU_TYP As String = DS1.Tables(0).Rows(0)("FRH_STU_TYPE")
        If strSTU_TYP = "E" Then
            rbEnquiry.Checked = True
        Else
            rbEnrollment.Checked = True
        End If
        'txtChqNo.Text = DS1.Tables(0).Rows(0)("FRH_STU_TYPE")
        txtAdjustment.Text = DS1.Tables(0).Rows(0)("FAR_REMARKS").ToString
        h_Adjustment.Value = DS1.Tables(0).Rows(0)("FRH_FAR_ID").ToString

        STR_SQL = "SELECT FRD_ID,  FEES.FEES_M.FEE_DESCR AS Fee, FEES.FEE_REFUND_D.FRD_AMOUNT AS Amount" _
            & " FROM  FEES.FEE_REFUND_D INNER JOIN " _
            & " FEES.FEES_M ON FEES.FEE_REFUND_D.FRD_FEE_ID = FEES.FEES_M.FEE_ID " _
            & " WHERE   FEES.FEE_REFUND_D.FRD_FRH_ID='" & p_FRD_FRH_ID & "'"
        Dim DS2 As New DataSet
        DS2 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, STR_SQL)
        gvApproval.DataSource = DS2.Tables(0)
        gvApproval.DataBind()
        StudentDetails()
    End Sub

    Sub InitialiseCompnents()
        H_STU_TYPE.Value = "S"
        txtBankCharge.Text = "0"
        gvRefund.Attributes.Add("bordercolor", "#1b80b6")
        gvNetRefund.Attributes.Add("bordercolor", "#1b80b6")
        Set_CashorBank()
        set_chequeno_controls()
        txtAmount.Attributes.Add("readonly", "readonly")
        txtCashAcc.Attributes.Add("readonly", "readonly")
        txtCashDescr.Attributes.Add("readonly", "readonly")
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtChqdt.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtBankCode.Attributes.Add("readonly", "readonly")
        txtBankDescr.Attributes.Add("readonly", "readonly")
        txtChqBook.Attributes.Add("readonly", "readonly")
        txtChqNo.Attributes.Add("readonly", "readonly")
        txtAdjustment.Attributes.Add("readonly", "readonly")
        txtChqBook.Attributes.Add("readonly", "readonly")
        txtProvCode.Attributes.Add("readonly", "readonly")
        txtProvDescr.Attributes.Add("readonly", "readonly")
        'txPaidto.Attributes.Add("readonly", "readonly")
        set_bankaccount()
        DisableChequeDetails()
    End Sub

    Sub clear_All()
        txtBankCharge.Text = "0"
        txtAmount.Text = ""
        h_Adjustment.Value = ""
        h_Student_no.Value = ""
        Me.h_STUD_ID.Value = ""
        Me.h_Student_no.Value = ""
        H_STU_TYPE.Value = ""
        txtCashAcc.Text = ""
        txtCashDescr.Text = ""
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtChqdt.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtBankCode.Text = ""
        txtBankDescr.Text = ""
        txtChqBook.Text = ""
        txtChqNo.Text = ""
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtProvCode.Text = ""
        txtProvDescr.Text = ""
        ClearStudentData()
        set_bankaccount()
        txtRemarks.Text = ""
        txtAdjustment.Text = ""
        lblAdjType.Text = ""
        txPaidto.Text = ""
        chkFee.Checked = False
        chkLab.Checked = False
        chkLibrary.Checked = False
        chkRegistrar.Checked = False
        btnSettlement.Visible = False
        setStudntData()
        gvRefund.DataBind()
        gvNetRefund.DataBind()
        Set_GridTotal()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            '  lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim str_error As String = ""
        If Not IsDate(txtFrom.Text) Then
            str_error = str_error & "Invalid from date <br />"
        End If
        If h_Student_no.Value = "" Then
            str_error = str_error & "Please select student <br />"
        End If
        If h_Adjustment.Value = "" Then
            h_Adjustment.Value = 0
            'str_error = str_error & "Please select adjustment <br />"
        End If
        If txtBankCode.Text = "" Then
            str_error = str_error & "Please select bank <br />"
        End If
        'If dtFrom > Now.Date Then
        '    str_error = str_error &"Invalid date(Future Date)!!!" 
        'End If
        Dim lblAmt As New Label
        Dim dRefundAmount As Decimal = 0
        If gvRefundSummary.Rows.Count > 0 And gvRefundSummary.Rows.Count > 0 Then
            'dRefundAmount = Convert.ToDecimal(gvRefund.Rows(gvRefund.Rows.Count - 1).Cells(1).Text)
            lblAmt = CType(gvRefundSummary.FooterRow.FindControl("lblAmtTotal"), Label)
            dRefundAmount = Convert.ToDecimal(lblAmt.Text)
            If dRefundAmount <= 0 Then
                str_error = str_error & "Invalid selection <br />"
            End If
        Else
            str_error = str_error & "Invalid selection <br />"
        End If

        If Not IsDate(txtChqdt.Text) Then
            str_error = str_error & "Invalid cheque date<br />"
        End If

        If Not IsNumeric(txtBankCharge.Text) Then
            txtBankCharge.Text = "0"
        End If

        If txtRemarks.Text = "" Then
            str_error = str_error & "Please enter narration<br />"
        End If
        Dim STR_BankOrCash As String = String.Empty
        Dim STR_ACCOUNT As String = String.Empty
        If rbBank.Checked Then
            STR_BankOrCash = "B"
            If txtBankCode.Text = "" Then
                str_error = str_error & "Invalid bank<br />"
            Else
                STR_ACCOUNT = txtBankCode.Text
            End If
        Else
            STR_BankOrCash = "C"
            If txtCashAcc.Text = "" Then
                str_error = str_error & "Invalid cash account<br />"
            Else
                STR_ACCOUNT = txtCashAcc.Text
            End If
            If Session("BSU_IsOnDAX") <> "1" And AccountFunctions.CheckAccountBalance(txtCashAcc.Text.Trim, Session("sBsuid"), txtFrom.Text.Trim) < dRefundAmount Then
                str_error = str_error & "There is not Enough Balance in the Account"
            End If
        End If

        If str_error <> "" Then
            '  lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dtFrom As DateTime
        dtFrom = CDate(txtFrom.Text)
        'Dim str_FRH_ID As String = "0"
        'If Request.QueryString("view_id") <> "" Then
        '    str_FRH_ID = Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+"))
        'End If

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()

        Dim str_NEW_FRH_ID As String = ""
        Dim dDiffAmount As Decimal = 0
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000" 'current acd of student selected in sp so here scd is pasased as 0
            Dim FRH_STU_ID As String = Me.gvRefundSummary.DataKeys(gvRefundSummary.Rows.Count - 1)("STU_ID")
            retval = FeeRefund.F_SaveFEE_REFUND_H(0, Session("sBsuid"), 0, H_STU_TYPE.Value, _
                FRH_STU_ID, h_Adjustment.Value, txtFrom.Text, txtChqdt.Text, False, str_NEW_FRH_ID, False, _
             txtRemarks.Text, "", STR_BankOrCash, STR_ACCOUNT, "", dRefundAmount, txtBankCharge.Text, stTrans, 0, "", 0, Session("sUsr_id"), "PENDING")
            If retval = "0" Then
                For Each gvr As GridViewRow In gvRefundSummary.Rows
                    Dim STU_ID As String = Me.gvRefundSummary.DataKeys(gvr.RowIndex)("STU_ID")
                    Dim FEEIDs As String = DirectCast(gvr.FindControl("lblFeeIDs"), Label).Text
                    Dim dAmount As Double = Convert.ToDouble(DirectCast(gvr.FindControl("lblAmt"), Label).Text)

                    Dim FEE_ID_AMOUNT As String()
                    If FEEIDs.Trim <> "" Then
                        FEE_ID_AMOUNT = FEEIDs.Split("|")
                    Else
                        FEE_ID_AMOUNT = FEEIDs.Split("")
                    End If
                    If dAmount > 0 And FEE_ID_AMOUNT.Length > 0 Then
                        For i As Int16 = 0 To FEE_ID_AMOUNT.Length - 1
                            If retval = 0 And FEE_ID_AMOUNT(i).Trim <> "" Then
                                Dim FRD_FEE_ID As String = FEE_ID_AMOUNT(i).Split("#")(0)
                                Dim FRD_NARRATION As String = SqlHelper.ExecuteScalar(objConn.ConnectionString, CommandType.Text, "SELECT MAX(FEE_DESCR) FROM FEES.FEES_M WHERE FEE_ID=" & IIf(FRD_FEE_ID.Trim <> "", FRD_FEE_ID, 0) & "")
                                Dim FRD_AMOUNT As Double = Convert.ToDouble(FEE_ID_AMOUNT(i).Split("#")(1))
                                retval = FeeRefund.F_SaveFEE_REFUND_D(0, str_NEW_FRH_ID, FRD_AMOUNT, _
                                    FRD_FEE_ID, FRD_NARRATION, STU_ID, stTrans)

                                If retval <> 0 Then
                                    Exit For
                                End If
                                dDiffAmount = dDiffAmount + dAmount - FRD_AMOUNT
                            End If
                        Next
                    Else
                        Exit For
                    End If

                Next
                'For Each gvr As GridViewRow In gvRefund.Rows
                '    Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtApprAmt"), TextBox)
                '    Dim lblID As Label = CType(gvr.FindControl("lblID"), Label)
                '    Dim dAmount As Decimal = Convert.ToDecimal(gvr.Cells(1).Text)
                '    Dim aprAmt As Decimal = Convert.ToDecimal(txtAmountToPay.Text)
                '    If dAmount < aprAmt Or aprAmt < 0 Then
                '        retval = 940 'Approved Amount should not be greater than Actual
                '        Exit For
                '    End If
                '    If Not lblID Is Nothing Then
                '        If dAmount > 0 Then
                '            If CInt(lblID.Text) < 1000 Then 'ID=1000 means total column
                '                retval = FeeRefund.F_SaveFEE_REFUND_D(0, str_NEW_FRH_ID, aprAmt, _
                '                lblID.Text, gvr.Cells(0).Text, stTrans)
                '                dDiffAmount = dDiffAmount + dAmount - aprAmt
                '            End If
                '            If retval <> 0 Then
                '                Exit For
                '            End If
                '        End If
                '    End If
                'Next
            End If
            If dDiffAmount < CDec(txtBankCharge.Text) Or CDec(txtBankCharge.Text) < 0 Then
                retval = 941 'Please check bank charge
            End If


            If retval = "0" Then
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_NEW_FRH_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                ' lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                clear_All()
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            ' lblError.Text = getErrorMessage(1000)
            usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Sub SettleFee()

        If IsDate(txtFrom.Text) And h_Student_no.Value <> "" Then
            Dim retval As Integer
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                retval = FeeCollection.F_SettleFee_Normal(Session("sBsuid"), txtFrom.Text, _
                h_Student_no.Value, rbEnquiry.Checked, stTrans)
                If retval = "0" Then
                    stTrans.Commit()
                Else
                    stTrans.Rollback()
                    '  lblError.Text = getErrorMessage(retval)
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Catch ex As Exception
                '  lblError.Text = getErrorMessage(1000)
                usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try
        End If
    End Sub

    Sub setEditdata()
        txtFrom.Attributes.Remove("readonly")
        imgFrom.Enabled = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        rbBank.Enabled = True
        rbCash.Enabled = True
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        setStudntData()
        BindRefundData()
        Set_GridTotal()
        SetTCReference()
    End Sub
    Private Sub BindRefundData()
        ViewState("gvRefund") = Nothing
        ViewState("gvNetRefund") = Nothing
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 100)
        pParms(0).Value = Session("sBsuid")
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(1).Value = h_Student_no.Value
        pParms(3) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(3).Value = H_STU_TYPE.Value
        pParms(4) = New SqlClient.SqlParameter("@MODE", SqlDbType.VarChar, 10)
        pParms(4).Value = "HEADER"

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GetRefundData", pParms)

        If Not dsData Is Nothing Then
            If dsData.Tables(0).Columns.Contains("IsRefundallowed") Then
                IsRefundallowed = dsData.Tables(0).Rows(0).Item("IsRefundallowed")
            End If

            ViewState("gvRefund") = dsData.Tables(0)
            gvRefund.DataSource = dsData.Tables(0)
            gvRefund.DataBind()
        End If
        pParms(4).Value = "DETAIL"
        Dim dsDetail As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GetRefundData", pParms)
        If Not dsDetail Is Nothing Then
            ViewState("gvNetRefund") = dsDetail.Tables(0)
            gvNetRefund.DataSource = dsDetail.Tables(0)
            gvNetRefund.DataBind()
        End If

        If ViewState("gvRefund") Is Nothing AndAlso dsDetail.Tables(0).Rows.Count <= 0 Then
            tbl_Allocation.Visible = False
        Else
            tbl_Allocation.Visible = True
        End If
    End Sub

    Private Sub SetTCReference()
        If h_Student_no.Value <> "" Then
            Dim STR_SQL As String = "SELECT TCM.TCM_bLabAppr, TCM.TCM_bFeeAppr, TCM.TCM_bLibAppr,TCM.TCM_ID, " & _
            " TCM_bRegAppr,'Refund'AS EVENT, TCM.TCM_TCSO, TCM.TCM_LASTATTDATE, ISNULL(TCM.TCM_REASON, '0') TCM_REASON " & _
            " FROM VW_OSO_STUDENT_M INNER JOIN TCM_M TCM WITH(NOLOCK) ON TCM_BSU_ID = STU_BSU_ID " & _
            " AND TCM_ACD_ID = STU_ACD_ID AND TCM_STU_ID = STU_ID  " & _
            " WHERE ISNULL(STU_bActive, 0) = 1 AND ISNULL(TCM_ISSUEDATE,'1/1/1900') = '1/1/1900' " & _
            " AND ISNULL(TCM_bCANCELLED, 0) = 0 AND ISNULL(TCM_bRegAppr, 0) = 1 AND ISNULL(TCM_bCANCELLED,0) = 0 " & _
            " AND TCM_BSU_ID = '" & Session("sBsuid") & "' AND STU_ID = " & h_Student_no.Value
            Dim dr As SqlDataReader
            Dim bDataExists As Boolean = False
            dr = SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnectionString, CommandType.Text, STR_SQL)
            While (dr.Read())
                Try
                    bDataExists = True
                    chkFee.Checked = dr("TCM_bFeeAppr")
                    chkLab.Checked = dr("TCM_bLabAppr")
                    chkLibrary.Checked = dr("TCM_bLibAppr")
                    chkRegistrar.Checked = dr("TCM_bRegAppr")
                    lblAdjType.Text = "Last Att. Date : " & Format(dr("TCM_LASTATTDATE"), OASISConstants.DateFormat)
                    txtAdjustment.Text = dr("TCM_REASON")
                    h_Adjustment.Value = dr("TCM_ID")
                Catch ex As Exception

                End Try
                Exit While
            End While
            If Not bDataExists Then
                chkFee.Checked = False
                chkLab.Checked = False
                chkLibrary.Checked = False
                chkRegistrar.Checked = False
                lblAdjType.Text = ""
                txtAdjustment.Text = ""
                h_Adjustment.Value = ""
            End If
        End If
    End Sub

    Sub setStudntData()
        If rbEnquiry.Checked Then
            H_STU_TYPE.Value = "E"
        Else
            H_STU_TYPE.Value = "S"
        End If
        'StudentDetails()

        SettleFee()
    End Sub

    Sub StudentDetails()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
                       CommandType.Text, " FEES.GetStudentDetailsForConcession '" & h_Student_no.Value & "'")
        gvStudentDetails.DataSource = ds.Tables(0)
        gvStudentDetails.DataBind()
    End Sub

    Sub Set_GridTotal()
        Dim dRefundAmount As Decimal = 0
        Dim dNetRefundAmount As Decimal
        Dim dtTotalApprvAmt As Double = 0
        Dim txtAmt As TextBox
        For i As Integer = 0 To gvRefund.Rows.Count - 2
            Dim gvRefundRow As GridViewRow = gvRefund.Rows(i)
            txtAmt = CType(gvRefundRow.FindControl("txtApprAmt"), TextBox)
            If txtAmt Is Nothing OrElse txtAmt.Text = "" Then Continue For
            dtTotalApprvAmt += CDbl(txtAmt.Text)
        Next
        If gvRefund.Rows.Count > 1 And gvNetRefund.Rows.Count > 1 Then
            'dRefundAmount = Convert.ToDecimal(gvRefund.Rows(gvRefund.Rows.Count - 1).Cells(1).Text)
            dNetRefundAmount = Convert.ToDecimal(gvNetRefund.Rows(gvNetRefund.Rows.Count - 1).Cells(1).Text)
            txtAmt = CType(gvRefund.Rows(gvRefund.Rows.Count - 1).FindControl("txtApprAmt"), TextBox)
            If Not txtAmt Is Nothing Then
                txtAmt.Text = dtTotalApprvAmt
                txtAmt.Attributes.Add("readonly", "readonly")
            End If
            If (dtTotalApprvAmt <= dNetRefundAmount * -1 AndAlso dtTotalApprvAmt > 0) Or IsRefundallowed = True Then
                btnSave.Enabled = True
                btnAddUpdate.Enabled = True
            Else
                btnSave.Enabled = False
                btnAddUpdate.Enabled = False
            End If
        Else
            btnAddUpdate.Enabled = False
        End If
        If gvRefundSummary.Rows.Count < 1 Then
            btnSave.Enabled = False
        Else
            btnSave.Enabled = True
        End If
    End Sub

    Protected Sub rbEnrollment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnrollment.CheckedChanged
        ClearStudentData()
    End Sub

    Protected Sub rbEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnquiry.CheckedChanged
        ClearStudentData()
    End Sub

    Sub ClearStudentData()
        Me.lblStudent.Text = ""
        ViewState("gvSelectedStudents") = Nothing
        ViewState("gvRefund") = Nothing
        ViewState("gvNetRefund") = Nothing
        ViewState("gvRefundSummary") = Nothing
        setStudntData()
        gvSelectedStudents.DataBind()
        gvRefundSummary.DataBind()
        gvRefund.DataBind()
        gvNetRefund.DataBind()
        Set_GridTotal()
    End Sub

    'Protected Sub txtStdNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStdNo.TextChanged
    '    Try
    '        Dim str_data, str_sql As String
    '        txtStdNo.Text = txtStdNo.Text.Trim
    '        Dim iStdnolength As Integer = txtStdNo.Text.Length
    '        If rbEnrollment.Checked Then
    '            If iStdnolength < 9 Then
    '                If txtStdNo.Text.Trim.Length < 8 Then
    '                    For i As Integer = iStdnolength + 1 To 8
    '                        txtStdNo.Text = "0" & txtStdNo.Text
    '                    Next
    '                End If
    '                txtStdNo.Text = Session("sBsuid") & txtStdNo.Text
    '            End If
    '            str_sql = "SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM VW_OSO_STUDENT_M" _
    '             & " WHERE     (STU_bActive = 1) AND (STU_BSU_ID = '" & Session("sBsuid") & "') AND  STU_NO='" & txtStdNo.Text & "'"
    '            str_data = GetDataFromSQL(str_sql, ConnectionManger.GetOASIS_FEESConnectionString)
    '        Else
    '            str_data = GetDataFromSQL("SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM FEES.vw_OSO_ENQUIRY_COMP" _
    '            & " WHERE     (STU_BSU_ID = '" & Session("sBsuid") & "') AND  STU_NO='" & txtStdNo.Text & "'", _
    '            ConnectionManger.GetOASIS_FEESConnectionString)
    '        End If
    '        If str_data <> "--" Then
    '            h_Student_no.Value = str_data.Split("|")(0)
    '            txtStdNo.Text = str_data.Split("|")(1)
    '            txtStudentname.Text = str_data.Split("|")(2)
    '            setStudntData()
    '            SetTCReference()
    '        Else
    '            h_Student_no.Value = ""
    '            txtStudentname.Text = ""
    '            lblNoStudent.Text = "Student # Entered is not valid  !!!"
    '        End If
    '    Catch ex As Exception
    '    End Try
    'End Sub

    Protected Sub rbCash_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCash.CheckedChanged
        Set_CashorBank()
    End Sub

    Sub Set_CashorBank()
        If rbCash.Checked Then
            tr_Bank.Visible = False
            tr_Cheque.Visible = False
            tr_Cash.Visible = True
            tr_Provision.Visible = False
            tr_ChqType.Visible = False
        Else
            tr_Bank.Visible = True
            If btnApprove.Visible Then
                tr_Cheque.Visible = True
                tr_Provision.Visible = True
                tr_ChqType.Visible = True
            End If
            FillLotNo()
            tr_Cash.Visible = False
        End If
    End Sub

    Private Sub FillLotNo()
        If txtBankCode.Text <> "" Then
            Dim str_sql As String = " SELECT TOP 1 ISNULL(CHQBOOK_M.CHB_LOTNO,'') AS LOT_NO, " & _
            " CHB_ID, ISNULL(MIN(ISNULL(CHD_NO,'')),'') AS CHD_NO FROM CHQBOOK_M INNER JOIN " & _
            " CHQBOOK_D ON CHQBOOK_M.CHB_ID = CHQBOOK_D.CHD_CHB_ID WHERE " & _
            " (CHQBOOK_D.CHD_ALLOTED = 0  ) AND (CHQBOOK_M.CHB_ACT_ID = '" & txtBankCode.Text & "')" & _
            " AND CHQBOOK_M.CHB_BSU_ID = '" & Session("sBSUID") & "'"

            str_sql += "GROUP BY CHB_LOTNO, CHB_ID "
            hCheqBook.Value = "0"
            txtChqNo.Text = "0"
            txtChqBook.Text = ""
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_sql)
            While (dr.Read() And Session("BSU_IsOnDAX") = "0")
                hCheqBook.Value = dr("CHB_ID")
                txtChqNo.Text = dr("CHD_NO")
                If dr("LOT_NO") <> 0 Then
                    txtChqBook.Text = dr("LOT_NO")
                Else
                    txtChqBook.Text = ""
                End If
            End While
        End If
    End Sub

    Protected Sub rbBank_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbBank.CheckedChanged
        Set_CashorBank()
    End Sub
    Private Function REFUND_MIGS(ByVal frh_id As String, ByVal pSRC As String, ByVal objcls As ClsPayment, ByVal stTrans As SqlTransaction) As Boolean
        Dim bPaymentSuccess As Boolean = False
        Try

            If objcls.GATEWAY_TYPE = "MIGS" Then
                Try
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    Dim User, Password, Accesscode, MerchantID, MerchTxnRef, MerchDetail, TranNo As String
                    Dim DS As New DataSet
                    DS = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, _
                    "EXEC [FEES].[GetDataforRefund]  @FCO_FCO_ID=" & objcls.FCO_FCO_ID)
                    If DS.Tables.Count > 0 Then
                        If DS.Tables(0).Rows.Count > 0 Then
                            User = DS.Tables(0).Rows(0)("BSU_QUERYDRUSER")
                            Password = DS.Tables(0).Rows(0)("BSU_QUERYDRPASSWORD")
                            Accesscode = DS.Tables(0).Rows(0)("BSU_MERCHANTCODE")
                            MerchantID = DS.Tables(0).Rows(0)("BSU_MERCHANTID")
                            MerchTxnRef = DS.Tables(0).Rows(0)("FCO_FCO_ID")
                            MerchDetail = DS.Tables(0).Rows(0)("MERCH_DETAIL")
                            TranNo = DS.Tables(0).Rows(0)("FCO_VPC_TRANNO")
                            Dim strResponse As String = ""
                            Dim myWebClient As New System.Net.WebClient
                            Dim ValueCollection As New System.Collections.Specialized.NameValueCollection
                            'ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Command"), System.Web.HttpUtility.UrlEncode("queryDR"))

                            ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Version"), System.Web.HttpUtility.UrlEncode("1"))
                            ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_AccessCode"), System.Web.HttpUtility.UrlEncode(Accesscode))
                            ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_MerchTxnRef"), System.Web.HttpUtility.UrlEncode(MerchTxnRef))
                            ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Merchant"), System.Web.HttpUtility.UrlEncode(MerchantID))
                            ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_TransNo"), System.Web.HttpUtility.UrlEncode(TranNo))
                            ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_User"), System.Web.HttpUtility.UrlEncode(User))
                            ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Password"), System.Web.HttpUtility.UrlEncode(Password))
                            ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Command"), System.Web.HttpUtility.UrlEncode("refund"))
                            ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Currency"), System.Web.HttpUtility.UrlEncode(objcls.CURRENCY))
                            ValueCollection.Add(System.Web.HttpUtility.UrlEncode("vpc_Amount"), System.Web.HttpUtility.UrlEncode(objcls.TOTAL_AMOUNT))

                            Dim responseArray As Byte() = myWebClient.UploadValues("https://migs.mastercard.com.au/vpcdps", "POST", ValueCollection)
                            strResponse = System.Text.Encoding.ASCII.GetString(responseArray)
                            Dim params As New Hashtable
                            splitResponse(strResponse, params)
                            Dim recno As String = "", msgServer As String = ""
                           
                            If null2unknown(params("vpc_TxnResponseCode")).ToString.Trim <> "0" Then
                                bPaymentSuccess = False
                                usrMessageBar.ShowNotification(null2unknown(params("vpc_Message")).ToString.Trim, UserControls_usrMessageBar.WarningType.Danger)
                            Else
                                bPaymentSuccess = True
                                usrMessageBar.ShowNotification("Refund done successfully.", UserControls_usrMessageBar.WarningType.Success)
                            End If
                        End If
                    Else
                        bPaymentSuccess = False
                        'lblError.Text = "Invalid Data "
                        usrMessageBar.ShowNotification("Invalid Data ", UserControls_usrMessageBar.WarningType.Danger)
                    End If

                Catch ex As Exception
                    bPaymentSuccess = False
                    'lblError.Text = "Error occured while processing void -  " & ex.Message
                    usrMessageBar.ShowNotification("Error occured while processing refund -  " & ex.Message, UserControls_usrMessageBar.WarningType.Danger)
                End Try
            End If
            Return bPaymentSuccess
        Catch ex As Exception
            Return bPaymentSuccess
        End Try
    End Function

    Private Function REFUND_MPGS(ByVal frh_id As String, ByVal pSRC As String, ByVal objcls As ClsPayment, ByVal stTrans As SqlTransaction) As Boolean
        Dim bPaymentSuccess As Boolean = False
        Try
            If objcls.GATEWAY_TYPE = "MPGS" Then
                'ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                Dim FetchOrderURL As String = objcls.API_URL.Replace("session", "order/") & objcls.MERCHANT_TXN_REF_ID & "/transaction/" & "TRN" & frh_id
                Dim client = New RestClient((New Uri(FetchOrderURL).AbsoluteUri))
                Dim Request = New RestRequest(Method.PUT)
                Request.AddHeader("cache-control", "no-cache")
                Request.AddHeader("Accept-Encoding", "gzip, deflate")
                Request.AddHeader("Host", "ap-gateway.mastercard.com")
                Request.AddHeader("Cache-Control", "no-cache")
                Request.AddHeader("Accept", "*/*")
                Request.AddHeader("Authorization", "Basic " & Convert.ToBase64String(Encoding.ASCII.GetBytes("merchant." & objcls.MERCHANT_ID & ":" & objcls.API_PASSWORD)))
                Request.AddHeader("Content-Type", "application/json")

                Dim CurrentUrl As String = HttpContext.Current.Request.Url.AbsoluteUri.ToString().Split("?")(0)
                Dim currentPage As String = System.IO.Path.GetFileName(CurrentUrl)
                Dim ResultPage As String = CurrentUrl.Replace(currentPage, "PaymentResult.aspx").Replace("http://", IIf(Page.Request.Url.Host.Contains("localhost"), "https://", "https://"))

                Dim transaction As PutRefundData.transaction = New PutRefundData.transaction(objcls.TOTAL_AMOUNT, objcls.CURRENCY)
                Dim PutRefundData As PutRefundData.PutRefundData = New PutRefundData.PutRefundData("REFUND", transaction)

                Dim jsonReq = New JavaScriptSerializer().Serialize(PutRefundData)
                Request.AddHeader("Content-Length", jsonReq.Length)
                Request.AddParameter("undefined", jsonReq, ParameterType.RequestBody)


                Dim Response As IRestResponse = client.Execute(Request)
                Dim jsonResponse = JObject.Parse(Response.Content)
                ClsPayment.UPDATE_FEE_REFUND_API_RESPONSE_DATA(pSRC, frh_id, Response.Content.ToString(), stTrans)
                Dim OrderStatus As String = "", vpc_AuthResponseCode As String = String.Empty, jsonResult As Object = Nothing
                jsonResult = jsonResponse("result")
                If Not jsonResult Is Nothing AndAlso (jsonResult.ToObject(Of String)().ToUpper = "SUCCESS" Or jsonResult.ToObject(Of String)().ToUpper = "FAILURE") Then
                    If Not jsonResponse("order") Is Nothing Then
                        Dim JSonOrder As JObject = JObject.Parse(jsonResponse("order").ToString())
                        If Not JSonOrder Is Nothing Then
                            OrderStatus = ValidateValue(JSonOrder.Property("status")).ToUpper
                        End If
                        If OrderStatus <> "" Then
                            Dim JSon_authorizationResponse As JObject = Nothing
                            If Not jsonResponse("authorizationResponse") Is Nothing Then
                                JSon_authorizationResponse = JObject.Parse(jsonResponse("authorizationResponse").ToString())
                                If Not JSon_authorizationResponse Is Nothing Then
                                    vpc_AuthResponseCode = ValidateValue(JSon_authorizationResponse.Property("responseCode"))
                                End If
                            End If

                            Dim json_transaction As JObject = Nothing
                            Dim json_transaction_acquirer As JObject = Nothing
                            Dim JSon_response As JObject = Nothing
                            Dim JSon_sof As JObject = Nothing
                            Dim JSon_sof_provided As JObject = Nothing
                            Dim JSon_sof_provided_card As JObject = Nothing
                            Dim Tprovided As JToken = Nothing

                            If Not jsonResponse("response") Is Nothing Then
                                JSon_response = JObject.Parse(jsonResponse("response").ToString())
                            End If
                            If Not jsonResponse("transaction") Is Nothing Then
                                json_transaction = JObject.Parse(jsonResponse("transaction").ToString())
                            End If
                            If Not jsonResponse("sourceOfFunds") Is Nothing Then
                                JSon_sof = JObject.Parse(jsonResponse("sourceOfFunds").ToString())
                            End If
                            If Not JSon_sof Is Nothing AndAlso Not JSon_sof("provided") Is Nothing Then
                                JSon_sof_provided = JObject.Parse(JSon_sof("provided").ToString())
                            End If
                            If Not JSon_sof_provided Is Nothing AndAlso Not JSon_sof_provided("card") Is Nothing Then
                                JSon_sof_provided_card = JObject.Parse(JSon_sof_provided("card").ToString())
                            End If
                            Dim TauthorizationCode As JToken = Nothing, Tbrand As JToken = Nothing, Tnumber As JToken = Nothing, Tid As JToken = Nothing, TacquirerMessage As JToken = Nothing,
                                TBatchno As JToken = Nothing
                            Dim vpc_AuthorizeId As String = String.Empty, vpc_CardType As String = String.Empty, vpc_CardNo As String = String.Empty, vpc_TransactionNo As String = String.Empty,
                            vpc_Message As String = String.Empty, vpc_3DSInfo = String.Empty, recno As String = String.Empty, msgServer As String = String.Empty,
                            vpc_AcqResponseCode As String = String.Empty, vpc_Batchno As String = String.Empty, vpc_TxnResponseCode As String = String.Empty
                            If Not json_transaction Is Nothing Then
                                If (json_transaction.TryGetValue("authorizationCode", TauthorizationCode) = True) Then
                                    vpc_AuthorizeId = ValidateValue(json_transaction.Property("authorizationCode"))
                                End If
                                If (json_transaction.TryGetValue("id", Tnumber) = True) Then
                                    vpc_TransactionNo = ValidateValue(json_transaction.Property("id"))
                                End If
                                If Not json_transaction Is Nothing AndAlso Not json_transaction("acquirer") Is Nothing Then
                                    json_transaction_acquirer = JObject.Parse(json_transaction("acquirer").ToString())
                                End If
                                If Not json_transaction_acquirer Is Nothing Then
                                    If (json_transaction_acquirer.TryGetValue("batch", TBatchno) = True) Then
                                        vpc_Batchno = ValidateValue(json_transaction_acquirer.Property("batch"))
                                    End If
                                End If
                            End If

                            If Not JSon_sof_provided_card Is Nothing AndAlso (JSon_sof_provided_card.TryGetValue("brand", Tbrand) = True) Then
                                vpc_CardType = ValidateValue(JSon_sof_provided_card.Property("brand"))
                            End If
                            If Not JSon_sof_provided_card Is Nothing AndAlso (JSon_sof_provided_card.TryGetValue("number", Tnumber) = True) Then
                                vpc_CardNo = ValidateValue(JSon_sof_provided_card.Property("number"))
                            End If
                            If Not JSon_response Is Nothing AndAlso (JSon_response.TryGetValue("acquirerMessage", TacquirerMessage) = True) Then
                                vpc_Message = ValidateValue(JSon_response.Property("acquirerMessage"))
                            End If
                            If Not JSon_response Is Nothing AndAlso (JSon_response.TryGetValue("acquirerCode", TacquirerMessage) = True) Then
                                vpc_AcqResponseCode = ValidateValue(JSon_response.Property("acquirerCode"))
                            End If
                            If (OrderStatus = "PARTIALLY_REFUNDED" Or OrderStatus = "PARTIALLY_REFUNDED" Or OrderStatus = "REFUNDED") And vpc_AuthResponseCode = "00" Then
                                'Dim retval As String = ""
                                vpc_TxnResponseCode = 0
                                bPaymentSuccess = True
                            Else
                                bPaymentSuccess = False

                                If vpc_Message <> "" Then

                                    usrMessageBar.ShowNotification(vpc_Message, UserControls_usrMessageBar.WarningType.Success)
                                End If
                            End If 'OrderStatus = "CAPTURED" And vpc_AuthResponseCode = "00"

                        End If 'OrderStatus <> ""
                    End If 'Not jsonResponse("order") Is Nothing
                ElseIf Not jsonResponse("result") Is Nothing AndAlso jsonResponse("result").ToObject(Of String)().ToUpper = "ERROR" Then
                    UtilityObj.Errorlog("PaymentResult.aspx, Error: Process API(SRC:" & pSRC & ",FRH_ID:" & frh_id & ",URL:" & FetchOrderURL & "), resulting in " & jsonResponse("result").ToObject(Of String)().ToUpper)

                End If
            End If
            Return bPaymentSuccess
        Catch ex As Exception
            Return bPaymentSuccess
        End Try
    End Function

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim str_error As String = ""
        If rbBank.Checked And Session("BSU_IsOnDAX") = "0" Then
            If (txtChqNo.Text = "" Or hCheqBook.Value = "") And rbCheque.Checked Then
                str_error = str_error & "Please Select Cheque"
            End If
        End If

        Dim STR_BankOrCash As String = String.Empty
        Dim STR_ACCOUNT As String = String.Empty
        Dim STR_DOCTYPE As String = String.Empty
        Dim STR_DOCNO As String = String.Empty
        If rbBank.Checked Then
            STR_DOCTYPE = "BP"
            STR_BankOrCash = "B"
            If txtBankCode.Text = "" Then
                str_error = str_error & "Invalid bank<br />"
            Else
                STR_ACCOUNT = txtBankCode.Text
            End If
            If rbOthers.Checked And txtrefChequeno.Text.Trim = "" Then
                str_error = str_error & "Invalid Ref. No. <br />"
            End If
        ElseIf rbCash.Checked Then
            STR_DOCTYPE = "CP"
            STR_BankOrCash = "C"
            If txtCashAcc.Text = "" Then
                str_error = str_error & "Invalid cash account<br />"
            Else
                STR_ACCOUNT = txtCashAcc.Text
            End If
            If Session("BSU_IsOnDAX") <> "1" And AccountFunctions.CheckAccountBalance(txtCashAcc.Text.Trim, Session("sBsuid"), txtFrom.Text.Trim) < CDbl(txtAmount.Text) Then
                'lblError.Text = "There is not Enough Balance in the Account"
                usrMessageBar.ShowNotification("There is not Enough Balance in the Account", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
        ElseIf rbCard.Checked Then
            STR_DOCTYPE = "BP"
            STR_BankOrCash = "O"
            If txtBankCode.Text = "" Then
                str_error = str_error & "Invalid bank<br />"
            Else
                STR_ACCOUNT = txtBankCode.Text
            End If
            'If rbOthers.Checked And txtrefChequeno.Text.Trim = "" Then
            '    str_error = str_error & "Invalid Ref. No. <br />"
            'End If
        End If
        Dim lblAmt As New Label
        Dim dRefundAmount As Decimal = 0
        If gvRefundSummary.Rows.Count > 0 And gvRefundSummary.Rows.Count > 0 Then
            'dRefundAmount = Convert.ToDecimal(gvRefund.Rows(gvRefund.Rows.Count - 1).Cells(1).Text)
            lblAmt = CType(gvRefundSummary.FooterRow.FindControl("lblAmtTotal"), Label)
            dRefundAmount = Convert.ToDecimal(lblAmt.Text)
            If dRefundAmount <= 0 Then
                str_error = str_error & "Invalid selection <br />"
            End If
        Else
            str_error = str_error & "Invalid selection <br />"
        End If
        Dim frh_id As String = Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+"))
        If str_error <> "" Then
            '  lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            If h_Adjustment.Value = "" Then
                h_Adjustment.Value = 0
            End If
            Dim retval As String = "1000"
            AccountFunctions.GetNextDocId(STR_DOCTYPE, Session("sBsuid"), CType(txtFrom.Text, Date).Month, CType(txtFrom.Text, Date).Year)
            ViewState("doctype") = STR_DOCTYPE
            STR_DOCNO = "" 'current acd of student selected in sp so here scd is pasased as 0
            Dim FRH_STU_ID As String = Me.gvRefundSummary.DataKeys(gvRefundSummary.Rows.Count - 1)("STU_ID")
            retval = FeeRefund.F_SaveFEE_REFUND_H(frh_id, Session("sBsuid"), 0, H_STU_TYPE.Value, _
            FRH_STU_ID, h_Adjustment.Value, txtFrom.Text, txtChqdt.Text, False, 0, False, _
            txtRemarks.Text, "", STR_BankOrCash, STR_ACCOUNT, txPaidto.Text, dRefundAmount, txtBankCharge.Text, stTrans, 0, "", 0, Session("sUsr_id"), "APPROVED")
            Dim dTotalAmount As Decimal = 0
            Dim dDiffAmount As Decimal = 0
            Dim STU_NOs As String = ""
            If retval = "0" Then
                For Each gvr As GridViewRow In gvRefundSummary.Rows
                    Dim STU_ID As String = Me.gvRefundSummary.DataKeys(gvr.RowIndex)("STU_ID")
                    Dim FEEIDs As String = DirectCast(gvr.FindControl("lblFeeIDs"), Label).Text
                    Dim dAmount As Double = Convert.ToDouble(DirectCast(gvr.FindControl("lblAmt"), Label).Text)
                    STU_NOs = STU_NOs + IIf(STU_NOs = "", "", ",") + gvr.Cells(0).Text

                    Dim FEE_ID_AMOUNT As String()
                    If FEEIDs.Trim <> "" Then
                        FEE_ID_AMOUNT = FEEIDs.Split("|")
                    Else
                        FEE_ID_AMOUNT = FEEIDs.Split("")
                    End If
                    If dAmount > 0 And FEE_ID_AMOUNT.Length > 0 Then
                        For i As Int16 = 0 To FEE_ID_AMOUNT.Length - 1
                            If retval = 0 And FEE_ID_AMOUNT(i).Trim <> "" Then
                                Dim FRD_FEE_ID As String = FEE_ID_AMOUNT(i).Split("#")(0)
                                Dim FRD_NARRATION As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT MAX(FEE_DESCR) FROM OASIS_FEES.FEES.FEES_M WITH(NOLOCK) WHERE FEE_ID=" & IIf(FRD_FEE_ID.Trim <> "", FRD_FEE_ID, 0) & "")
                                Dim FRD_AMOUNT As Double = Convert.ToDouble(FEE_ID_AMOUNT(i).Split("#")(1))
                                Dim FRD_ID As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "SELECT FRD_ID FROM OASIS_FEES.FEES.FEE_REFUND_D WITH(NOLOCK) where FRD_FRH_ID = " & frh_id & " AND FRD_FEE_ID=" & FRD_FEE_ID & " AND FRD_STU_ID=" & STU_ID & "")
                                retval = FeeRefund.F_SaveFEE_REFUND_D(FRD_ID, frh_id, FRD_AMOUNT, _
                                    FRD_FEE_ID, FRD_NARRATION, STU_ID, stTrans)

                                If retval <> 0 Then
                                    Exit For
                                End If
                                dDiffAmount = dDiffAmount + dAmount - FRD_AMOUNT
                            End If
                        Next
                    Else
                        Exit For
                    End If

                Next
                
            End If
            If retval = 0 Then
                If dTotalAmount <> txtAmount.Text Then
                    txtAmount.Text = dTotalAmount
                End If
                retval = F_GenerateRefundVoucher(objConn, txtFrom.Text, "Fee Refund for : " & STU_NOs & " " & txtRemarks.Text, dRefundAmount, STR_DOCTYPE, _
                STR_DOCNO, FRH_STU_ID, stTrans)
            End If

            If ViewState("datamode") <> "edit" And retval = 0 Then
                retval = FeeRefund.PostFeeRefund(frh_id, _
                STR_DOCNO, txtFrom.Text, stTrans)
            End If

            '---------------REFUND API CALL
            Dim bPaymentSuccess As Boolean = True
            If ViewState("FRH_SOURCE").ToString = "ONLINE_REFUND" And retval = 0 Then
                bPaymentSuccess = False
                Dim pSRC As String = "FEE_REFUND"
                Dim objcls As New ClsPayment
                objcls.GET_REFUND_API_INFO(pSRC, frh_id, Session("sBsuid"))

                Select Case objcls.GATEWAY_TYPE
                    Case "MPGS"
                        bPaymentSuccess = REFUND_MPGS(frh_id, pSRC, objcls, stTrans)
                    Case "MIGS"
                        bPaymentSuccess = REFUND_MIGS(frh_id, pSRC, objcls, stTrans)
                    Case "DIRECPAY"
                        bPaymentSuccess = ProcessDirecPayRefund(objcls)
                    Case "EBPG"

                End Select
            End If
            '-------------REFUND API CALL ENDS HERE


            If retval = "0" And bPaymentSuccess Then
                stTrans.Commit()
                ViewState("FRH_VHH_DOCNO") = STR_DOCNO
                ViewState("FRH_BANK_CASH") = STR_BankOrCash
                If STR_BankOrCash = "B" Then
                    chkPrintChq.Visible = True
                End If
                btnPrint1.Visible = True
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, STR_DOCNO, _
            "Edit", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                ' lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                clear_All()
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                If bPaymentSuccess = False Then
                    usrMessageBar.ShowNotification("Fee refund failed from payment gateway", UserControls_usrMessageBar.WarningType.Danger)
                Else
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If

            End If
        Catch ex As Exception
            stTrans.Rollback()
            '  lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Shared Function null2unknown(ByVal req As Object) As String
        Try
            If req Is Nothing Then
                Return String.Empty
            Else
                Return System.Web.HttpUtility.UrlDecode(req.ToString().Trim)
            End If
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    Public Shared Sub splitResponse(ByVal data As String, ByRef params As Hashtable)
        Dim pairs As String()
        Dim equalsIndex As Integer
        Dim name As String
        Dim value As String
        ' Check if there was a response
        If Len(data) > 0 Then
            ' Check if there are any paramenters in the response
            If InStr(data, "=") > 0 Then
                ' Get the parameters out of the response
                pairs = Split(data, "&")
                For Each pair As String In pairs
                    ' If there is a key/value pair in this item then store it
                    equalsIndex = InStr(pair, "=")
                    If equalsIndex > 1 And Len(pair) > equalsIndex Then
                        name = Left(pair, equalsIndex - 1)
                        value = Right(pair, Len(pair) - equalsIndex)
                        params.Add(name, System.Web.HttpUtility.UrlEncode(value))
                    End If
                Next
            Else ' There were no parameters so create an error
                params.Add("vpc_Message", "The data contained in the response was invalid or corrupt, the data is: <pre>" & data & "</pre>")
            End If
        Else ' There was no data so create an error
            params.Add("vpc_Message", "There was no data contained in the response")
        End If

    End Sub
    Protected Sub txtApprAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal()
    End Sub

    Protected Sub txtApprAmt_Approve_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal_Approve()
    End Sub

    Sub Set_GridTotal_Approve()
        Dim dRefundAmount As Decimal = 0
        Dim txtAmt As TextBox
        For i As Integer = 0 To gvApproval.Rows.Count - 1
            Dim gvApprovalRow As GridViewRow = gvApproval.Rows(i)
            txtAmt = CType(gvApprovalRow.FindControl("txtApprAmt"), TextBox)
            If txtAmt Is Nothing OrElse txtAmt.Text = "" Then Continue For
            dRefundAmount += CDbl(txtAmt.Text)
        Next
        txtAmount.Text = Format(dRefundAmount, "0.00")
    End Sub

    Protected Sub btnPrint1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint1.Click
        If Not ViewState("FRH_VHH_DOCNO") Is Nothing Then
            If ViewState("FRH_BANK_CASH") = "C" Then
                Session("ReportSource") = VoucherReports.CashPaymentVoucher(Session("sBSUID"), Session("F_YEAR"), Session("SUB_ID"), "CP", ViewState("FRH_VHH_DOCNO"), True)
                '  Response.Redirect("../Reports/ASPX Report/rptviewer.aspx")
                ReportLoadSelection_2()
            Else
                If chkPrintChq.Checked Then
                    Session("ReportSource") = AccountsReports.ChquePrint(ViewState("FRH_VHH_DOCNO"), "", Session("sBSUID"), Session("F_YEAR"), "BP")
                    If Session("ReportSource").Equals(Nothing) Then
                        '  lblError.Text = "Cheque Format not supported for printing"
                        usrMessageBar.ShowNotification("Cheque Format not supported for printing", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    Response.Redirect("../Accounts/accChqPrint.aspx?ChequePrint=BP", True)
                Else
                    Session("ReportSource") = VoucherReports.BankPaymentVoucher(Session("sBSUID"), Session("F_YEAR"), Session("SUB_ID"), "BP", ViewState("FRH_VHH_DOCNO"), False, True)
                    '      Response.Redirect("../Reports/ASPX Report/rptviewer.aspx")
                    ReportLoadSelection_2()
                End If
            End If
        End If

    End Sub

    Sub ReportLoadSelection_2()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Dim str_error As String = ""
        Dim STR_BankOrCash As String = String.Empty
        Dim STR_ACCOUNT As String = String.Empty
        Dim STR_DOCNO As String = String.Empty
        If rbBank.Checked Then
            STR_BankOrCash = "B"
        ElseIf rbCash.Checked Then
            STR_BankOrCash = "C"
        ElseIf rbCard.Checked Then
            STR_BankOrCash = "O"
        End If
        If h_Adjustment.Value = "" Then
            h_Adjustment.Value = 0
        End If
        Dim FRH_STU_ID As String = Me.gvRefundSummary.DataKeys(gvRefundSummary.Rows.Count - 1)("STU_ID")
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            retval = FeeRefund.F_SaveFEE_REFUND_H(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")), Session("sBsuid"), 0, H_STU_TYPE.Value, _
               FRH_STU_ID, h_Adjustment.Value, txtFrom.Text, txtChqdt.Text, False, 0, True, _
            txtRemarks.Text, "", STR_BankOrCash, STR_ACCOUNT, txPaidto.Text, txtAmount.Text, txtBankCharge.Text, stTrans, 0, "", 0, Session("sUsr_id"), "REJECTED")

            If retval = "0" Then
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, STR_DOCNO, _
            "Edit", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                ' lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                clear_All()
            Else
                stTrans.Rollback()
                ' lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            '  lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Public Function F_GenerateRefundVoucher(ByVal objConn As SqlConnection, ByVal p_Date As Date, ByVal p_Narration As String, _
    ByVal p_Amount As String, ByVal p_Doctype As String, ByRef STR_DOCNO As String, ByVal STU_ID As String, ByVal stTrans As SqlTransaction) As String
        Dim SqlCmd As New SqlCommand("F_GenerateRefundVoucher", objConn, stTrans)
        SqlCmd.CommandType = CommandType.StoredProcedure

        SqlCmd.Parameters.AddWithValue("@VHH_SUB_ID", Session("SUB_ID"))
        SqlCmd.Parameters.AddWithValue("@VHH_BSU_ID", Session("sBsuid"))
        SqlCmd.Parameters.AddWithValue("@VHH_FYEAR", Session("F_YEAR"))
        SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", p_Doctype)

        If rbCash.Checked Then
            SqlCmd.Parameters.AddWithValue("@VHD_CHQID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHD_CHQDT", "1/jan/1900")
            SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtCashAcc.Text)
            SqlCmd.Parameters.AddWithValue("@VHD_CHQNO", "")
        Else
            SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtBankCode.Text)
            If rbCheque.Checked = True Then

                If Session("BSU_IsOnDAX") = "1" Then
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQNO", "0")
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQID", "0")
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQDT", Trim(txtFrom.Text))
                Else
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQDT", Trim(txtChqdt.Text))
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQNO", txtChqNo.Text)
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQID", Convert.ToInt32(hCheqBook.Value))
                End If

            Else
                If Session("BSU_IsOnDAX") = "1" Then
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQNO", "0")
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQID", "0")
                Else
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQID", 0)
                    SqlCmd.Parameters.AddWithValue("@VHD_CHQNO", txtrefChequeno.Text)
                End If
                SqlCmd.Parameters.AddWithValue("@VHD_CHQDT", txtFrom.Text)
            End If
        End If

        SqlCmd.Parameters.AddWithValue("@VHH_CUR_ID", Session("BSU_CURRENCY"))
        SqlCmd.Parameters.AddWithValue("@VHH_NARRATION", p_Narration)
        SqlCmd.Parameters.AddWithValue("@AMOUNT", p_Amount)
        SqlCmd.Parameters.AddWithValue("@VHH_RECEIVEDBY", txPaidto.Text)

        SqlCmd.Parameters.AddWithValue("@VHH_PROV_ACT_ID", txtProvCode.Text)
        SqlCmd.Parameters.AddWithValue("@STU_ID", STU_ID)
        SqlCmd.Parameters.AddWithValue("@STU_NAME", "Multiple Students")
        SqlCmd.Parameters.AddWithValue("@VHH_DOCDT", p_Date)
        SqlCmd.Parameters.AddWithValue("@VHH_LOCK", Session("sUsr_name"))
        SqlCmd.Parameters.AddWithValue("@VHH_BBearer", ChkBearer.Checked)
        SqlCmd.Parameters.Add("@VHH_DOCNO", SqlDbType.VarChar, 20)
        SqlCmd.Parameters("@VHH_DOCNO").Direction = ParameterDirection.Output
        SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
        SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue


        SqlCmd.ExecuteNonQuery()
        If SqlCmd.Parameters("@ReturnValue").Value = 0 Then
            STR_DOCNO = CStr(SqlCmd.Parameters("@VHH_DOCNO").Value)
        End If
        Return SqlCmd.Parameters("@ReturnValue").Value
    End Function

    Protected Sub rbCheque_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCheque.CheckedChanged
        set_chequeno_controls()
    End Sub

    Protected Sub rbOthers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbOthers.CheckedChanged
        set_chequeno_controls()
    End Sub

    Sub set_chequeno_controls()
        If rbOthers.Checked = True Then
            txtrefChequeno.Enabled = True
            txtChqBook.Text = ""
            txtChqNo.Text = ""
        Else
            txtrefChequeno.Enabled = False
            txtrefChequeno.Text = ""
        End If
    End Sub

    Protected Sub txtStudName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        AddStudent()
    End Sub
    Protected Sub lblAddNewStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddNewStudent.Click
        AddStudent()
    End Sub
    Protected Sub imgStudentN_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudentN.Click
        FillSTUNames(h_STUD_ID.Value)
    End Sub
    Private Sub AddStudent()
        If txtStudName.Text <> "" Then
            Dim str_stuid As String = FeeCommon.GetTransportStudentID(txtStudName.Text, Session("sBsuid"), Me.rbEnquiry.Checked, False)
            If str_stuid <> "" Then
                If h_STUD_ID.Value.EndsWith("||") Then
                    h_STUD_ID.Value = h_STUD_ID.Value & str_stuid
                Else
                    h_STUD_ID.Value = h_STUD_ID.Value & "||" & str_stuid
                End If
                txtStudName.Text = ""
                FillSTUNames(h_STUD_ID.Value)
                h_STUD_ID.Value = ""
            End If
        End If
    End Sub
    Private Function FillSTUNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_Sql As String
        Dim DTSelectedStudents As DataTable
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        Dim TableName As String = ""
        TableName = IIf(rbEnquiry.Checked, "dbo.VW_OSO_STUDENT_ENQUIRY", "FEES.VW_OSO_STUDENT_DETAILS")
        str_Sql = " SELECT STU_NO , STU_NAME,STU_ID FROM " & TableName & " WHERE STU_ID IN (" + condition + ")"
        DTSelectedStudents = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql).Tables(0)
        If gvSelectedStudents.Rows.Count > 0 And DTSelectedStudents.Rows.Count > 0 Then
            Dim dtExisting As DataTable = ViewState("gvSelectedStudents")
            dtExisting.Merge(RemoveDuplicateRows(DTSelectedStudents))
            ViewState("gvSelectedStudents") = dtExisting
        Else
            ViewState("gvSelectedStudents") = DTSelectedStudents
        End If

        BindSelectedStudents(ViewState("gvSelectedStudents"))
        If DTSelectedStudents Is Nothing Or DTSelectedStudents.Rows.Count <= 0 Then
            Return False
        Else
            h_STUD_ID.Value = ""
        End If
        Return True
    End Function
    Private Sub BindSelectedStudents(ByVal DTT As DataTable)
        gvSelectedStudents.DataSource = DTT
        gvSelectedStudents.DataBind()
    End Sub
    Private Function RemoveDuplicateRows(ByVal DT2 As DataTable) As DataTable
        For Each gvr As GridViewRow In Me.gvSelectedStudents.Rows
            Dim StudentID As String = Me.gvSelectedStudents.DataKeys(gvr.RowIndex)("STU_ID")
            For j As Int16 = DT2.Rows.Count - 1 To 0 Step -1
                Dim dr As DataRow = DT2.Rows(j)
                If dr("STU_ID").ToString = StudentID Then
                    DT2.Rows.Remove(dr)
                    j -= 1
                End If
            Next
        Next

        Return DT2
    End Function


    Protected Sub gvSelectedStudents_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSelectedStudents.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim GvSelectedStuds As GridView = DirectCast(e.Row.FindControl("gvStdDetails"), GridView)
            Dim LnkPayHistory As LinkButton = DirectCast(e.Row.FindControl("lnkPayHistory"), LinkButton)
            Dim StudentNo As String = Me.gvSelectedStudents.DataKeys(e.Row.RowIndex)("STU_ID")
            LnkPayHistory.Attributes.Add("OnClick", "return Showdata('" + StudentNo + "',1);")
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
                           CommandType.Text, " FEES.GetStudentDetailsForConcession '" & StudentNo & "'," & rbEnquiry.Checked & "")
            GvSelectedStuds.DataSource = ds.Tables(0)
            GvSelectedStuds.DataBind()
        End If
    End Sub

    Protected Sub lnkView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.lblStudent.Text = ""
        Dim LNK As LinkButton = DirectCast(sender, LinkButton)
        Dim gvr As GridViewRow = DirectCast(LNK.NamingContainer, GridViewRow)
        Dim StudentID As String = Me.gvSelectedStudents.DataKeys(gvr.RowIndex)("STU_ID")
        Dim StudentNo As String = DirectCast(gvr.FindControl("lblStudID"), Label).Text
        Dim StudentName As String = DirectCast(gvr.FindControl("lblStudentName"), Label).Text
        Me.gvSelectedStudents.SelectedIndex = gvr.RowIndex
        h_Student_no.Value = StudentID
        imgStudent_Click(Nothing, Nothing)
        Me.lblStudent.Text = "(" + StudentNo + ") - " + StudentName
    End Sub


    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim LNK As LinkButton = DirectCast(sender, LinkButton)
        Dim gvr As GridViewRow = DirectCast(LNK.NamingContainer, GridViewRow)
        Dim STUID As Int32 = gvSelectedStudents.DataKeys(gvr.RowIndex)("STU_ID")
        If ValidateStudentDelete(STUID) = True Then
            DirectCast(ViewState("gvSelectedStudents"), DataTable).Rows.RemoveAt(gvr.RowIndex)
            DirectCast(ViewState("gvSelectedStudents"), DataTable).AcceptChanges()
            BindSelectedStudents(ViewState("gvSelectedStudents"))
            ClearRefundDetails()
        End If
    End Sub
    Function ValidateStudentDelete(ByVal stuID As Int32) As Boolean
        If gvRefundSummary.Rows.Count > 0 Then
            For Each gvr As GridViewRow In gvRefundSummary.Rows
                If gvRefundSummary.DataKeys(gvr.RowIndex)("STU_ID") = stuID Then
                    ValidateStudentDelete = False
                    Exit Function
                End If
            Next
        End If
        ValidateStudentDelete = True
    End Function

    Protected Sub btnAddUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUpdate.Click
        If Me.gvRefund.Rows.Count > 0 Then
            Dim RefundAmount As Double = 0
            Dim FeeIDs As String = ""
            For Each gvr As GridViewRow In gvRefund.Rows
                If gvr.RowType = DataControlRowType.DataRow Then

                    If gvr.Cells(0).Text.ToUpper = "TOTAL" Or Me.gvRefund.DataKeys(gvr.RowIndex)("FEE_ID") = "999999" Then
                        RefundAmount = DirectCast(gvr.FindControl("txtApprAmt"), TextBox).Text
                    ElseIf Val(DirectCast(gvr.FindControl("txtApprAmt"), TextBox).Text) > 0 Then
                        FeeIDs = FeeIDs & "|" & Me.gvRefund.DataKeys(gvr.RowIndex)("FEE_ID") & "#" & DirectCast(gvr.FindControl("txtApprAmt"), TextBox).Text
                    End If

                End If
            Next
            If RefundAmount > 0 Then
                If ViewState("gvRefundSummary") Is Nothing Then
                    Dim dtTemp As DataTable = CreategvRefundSummarySchema()
                    Dim drTemp As DataRow = dtTemp.NewRow
                    drTemp("STU_ID") = h_Student_no.Value
                    drTemp("STU_NO") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "SELECT STU_NO FROM dbo.VW_OSO_STUDENT_ENQUIRY WHERE STU_ID='" & h_Student_no.Value & "'")
                    drTemp("STU_NAME") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "SELECT STU_NAME FROM dbo.VW_OSO_STUDENT_ENQUIRY WHERE STU_ID='" & h_Student_no.Value & "'")
                    drTemp("AMOUNT") = RefundAmount
                    drTemp("FEE_IDs") = FeeIDs

                    dtTemp.Rows.Add(drTemp)
                    ViewState("gvRefundSummary") = dtTemp
                    BindRefundSummary(dtTemp)
                Else
                    For Each gvrow As GridViewRow In Me.gvRefundSummary.Rows
                        If h_Student_no.Value = Me.gvRefundSummary.DataKeys(gvrow.RowIndex)("STU_ID") Then 'Duplicate row
                            '      Me.lblError2.Text = "Refund detail for student already added in current transaction, Please remove it from below and try again."
                            usrMessageBar.ShowNotification("Refund detail for student already added in current transaction, Please remove it from below and try again.", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                    Next
                    Dim dtTemp As DataTable = DirectCast(ViewState("gvRefundSummary"), DataTable)
                    Dim drTemp As DataRow = dtTemp.NewRow
                    drTemp("STU_ID") = h_Student_no.Value
                    drTemp("STU_NO") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "SELECT STU_NO FROM dbo.VW_OSO_STUDENT_ENQUIRY WHERE STU_ID='" & h_Student_no.Value & "'")
                    drTemp("STU_NAME") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "SELECT STU_NAME FROM dbo.VW_OSO_STUDENT_ENQUIRY WHERE STU_ID='" & h_Student_no.Value & "'")
                    drTemp("AMOUNT") = RefundAmount
                    drTemp("FEE_IDs") = FeeIDs

                    dtTemp.Rows.Add(drTemp)
                    ViewState("gvRefundSummary") = dtTemp
                    BindRefundSummary(dtTemp)
                End If
                ClearRefundDetails()
            End If

        End If
    End Sub
    Private Sub BindRefundSummary(ByVal DT As DataTable)
        Me.gvRefundSummary.DataSource = DT
        Me.gvRefundSummary.DataBind()
    End Sub
    Private Function CreategvRefundSummarySchema() As DataTable
        Dim dtRefundSummary As New DataTable
        dtRefundSummary.Columns.Add("STU_ID", Type.GetType("System.String"))
        dtRefundSummary.Columns.Add("STU_NO", Type.GetType("System.String"))
        dtRefundSummary.Columns.Add("STU_NAME", Type.GetType("System.String"))
        dtRefundSummary.Columns.Add("FEE_IDs", Type.GetType("System.String"))
        dtRefundSummary.Columns.Add("AMOUNT", Type.GetType("System.Double"))

        Return dtRefundSummary
    End Function
    Private Sub ClearRefundDetails()
        h_Student_no.Value = "0"
        imgStudent_Click(Nothing, Nothing)
        Me.lblStudent.Text = ""
        Me.gvSelectedStudents.SelectedIndex = -1
    End Sub
    Protected Sub btnCancelUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelUpdate.Click
        ClearRefundDetails()
    End Sub

    Protected Sub gvRefundSummary_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRefundSummary.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim gvFeeDetails As GridView = DirectCast(e.Row.FindControl("gvFeeDetails"), GridView)
            Dim LnkPayHistory As LinkButton = DirectCast(e.Row.FindControl("lnkBtnFeeDetails"), LinkButton)
            Dim LblFEEIDs As Label = DirectCast(e.Row.FindControl("lblFeeIDs"), Label)
            Dim FEEIDs As String() = LblFEEIDs.Text.Split("|")
            Dim QRY As String = ""
            For i As Int16 = 0 To FEEIDs.Length - 1
                If FEEIDs(i) <> "" Then
                    Dim FEEID As String = FEEIDs(i).Split("#")(0)
                    Dim AMT As Double = FEEIDs(i).Split("#")(1)
                    QRY = QRY & IIf(QRY = "", "", " UNION ALL ") & " SELECT FEE_DESCR AS FEE," & AMT & " AS AMOUNT FROM OASIS_FEES.FEES.FEES_M WHERE FEE_ID='" & FEEID & "' "
                End If
            Next
            gvFeeDetails.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, QRY)
            gvFeeDetails.DataBind()

        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            DirectCast(e.Row.FindControl("lblAmtTotal"), Label).Text = Format(DirectCast(ViewState("gvRefundSummary"), DataTable).Compute("SUM(AMOUNT)", ""), Session("BSU_DataFormatString"))
            Me.txtAmount.Text = DirectCast(e.Row.FindControl("lblAmtTotal"), Label).Text
        End If
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim LNK As LinkButton = DirectCast(sender, LinkButton)
        Dim gvr As GridViewRow = DirectCast(LNK.NamingContainer, GridViewRow)
        DirectCast(ViewState("gvRefundSummary"), DataTable).Rows.RemoveAt(gvr.RowIndex)
        DirectCast(ViewState("gvRefundSummary"), DataTable).AcceptChanges()
        BindRefundSummary(ViewState("gvRefundSummary"))
    End Sub

    Protected Sub gvRefund_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRefund.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(0).Text.ToUpper = "TOTAL" Then
                DirectCast(e.Row.FindControl("txtApprAmt"), TextBox).Enabled = False
            Else
                DirectCast(e.Row.FindControl("txtApprAmt"), TextBox).Enabled = True
            End If
        End If
    End Sub
    Protected Sub btnSettlement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSettlement.Click

        CallReport()

    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@FRH_ID", Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_FEES"
            .reportParameters = param
            .reportPath = Server.MapPath("~/FEES/Reports/RPT/rptSettlement.rpt")

        End With
        Session("rptClass") = rptClass
        '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub h_STUD_ID_ValueChanged(sender As Object, e As EventArgs)
        FillSTUNames(h_STUD_ID.Value)
    End Sub


    Public Shared Function GetDoubleVal(ByVal Value As Object) As Double
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function
    Public Shared Function ValidateValue(ByVal parameter As JProperty) As String
        ValidateValue = ""
        Try
            Dim msgProperty = parameter
            If msgProperty IsNot Nothing Then
                ValidateValue = msgProperty.Value.ToString
            End If
        Catch ex As Exception

        End Try
    End Function
    Private Function ProcessDirecPayRefund(ByRef objcls As ClsPayment) As Boolean
        ProcessDirecPayRefund = False
        Try
            Dim DS As New DataSet
            Dim aesEncrypt = New EncDec()
            Dim requestObj = New RefundRequest()
            DS = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text,
            "EXEC [FEES].[GetDataforRefund]  @FCO_FCO_ID=" & objcls.FCO_FCO_ID)
            If DS.Tables.Count > 0 Then
                If DS.Tables(0).Rows.Count > 0 Then
                    requestObj.MerchantID = DS.Tables(0).Rows(0)("BSU_MERCHANTID")
                    requestObj.CollaboratorID = DS.Tables(0).Rows(0)("BSU_MERCHANTCODE")
                    requestObj.TransactionDetails().ReferenceID = DS.Tables(0).Rows(0)("FCO_VPC_TRANNO")
                    requestObj.Amount().RefundRequestID = DS.Tables(0).Rows(0)("FRO_ID")
                    requestObj.Amount().RefundAmount = objcls.TOTAL_AMOUNT
                   
                    Dim RequestString As String = Convert.ToString(requestObj.MerchantID) + "||" + requestObj.CollaboratorID + "||" + aesEncrypt.Encrypt(objcls.API_PASSWORD, configureRequest(requestObj)) '"1||010|3346224"
                    Dim client = New RestClient(objcls.QUERYDR_REFUND_URL)
                    client.Timeout = -1
                    Dim request = New RestRequest(Method.POST)
                    request.AddHeader("Content-Type", "application/xml")
                    Dim doc As New XmlDocument
                    doc.LoadXml("<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://uilogic.dp.toml.com/'><SOAP-ENV:Body>  <ns1:invokeRefundAPI>      <requestparameters>" + RequestString + "</requestparameters>    </ns1:invokeRefundAPI>  </SOAP-ENV:Body></SOAP-ENV:Envelope>")
                    request.AddParameter("application/xml", doc.InnerXml, ParameterType.RequestBody)
                    Dim Response As IRestResponse = client.Execute(request)
                    Dim docR As New XmlDocument
                    docR.LoadXml(Response.Content)
                    Dim objResponse As RefundResponse = getRefundResponse(docR.InnerText)

                    If objResponse.StatusBlock.StatusFlag.ToUpper = "SUCCESS" Then
                        ProcessDirecPayRefund = True
                    End If
                End If
            Else
                usrMessageBar.ShowNotification("Invalid Data ", UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            usrMessageBar.ShowNotification("Error occured while processing refund -  " & ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Function
    Public Shared Function configureRequest(ByVal RefundRequest As RefundRequest) As String

        Dim fieldFlag As String = ""
        Dim paramFlag As String = ""
        Dim paramString As String = ""
        Dim requestString As String = "" '1110100||
        Dim paramObj As RefundRequest = RefundRequest
        If Not paramObj.TransactionDetails() Is Nothing Then
            fieldFlag = fieldFlag + "1"
            If Not paramObj.TransactionDetails().ReferenceID = Nothing Then
                paramFlag = paramFlag + "1"
                paramString = paramString + "|" + Convert.ToString(paramObj.TransactionDetails().ReferenceID)
            Else
                paramFlag = paramFlag + "0"
            End If
            If Not paramObj.TransactionDetails().MerchantOrderNumber = Nothing Then
                paramFlag = paramFlag + "1"
                paramString = paramString + "|" + paramObj.TransactionDetails().MerchantOrderNumber
            Else
                paramFlag = paramFlag + "0"
            End If
            requestString = paramFlag + paramString
        Else
            fieldFlag = fieldFlag + "0"
        End If
        paramFlag = ""
        paramString = ""
        If Not paramObj.Amount() Is Nothing Then
            fieldFlag = fieldFlag + "1"
            If Not paramObj.Amount().RefundRequestID = Nothing Then
                paramFlag = paramFlag + "1"
                paramString = paramString + "|" + paramObj.Amount().RefundRequestID
            Else
                paramFlag = paramFlag + "0"
            End If
            If Not paramObj.Amount().RefundAmount = Nothing Then
                paramFlag = paramFlag + "1"
                paramString = paramString + "|" + Convert.ToString(paramObj.Amount().RefundAmount)
            Else
                paramFlag = paramFlag + "0"
            End If
            If Not paramObj.Amount().TransactionAmount = Nothing Then
                paramFlag = paramFlag + "1"
                paramString = paramString + "|" + Convert.ToString(paramObj.Amount().TransactionAmount)
            Else
                paramFlag = paramFlag + "0"
            End If
            requestString = requestString + "||" + paramFlag + paramString
        Else
            fieldFlag = fieldFlag + "0"
        End If
        Return fieldFlag + "||" + requestString
    End Function

    Public Shared Function getRefundResponse(ByVal formParams As String) As RefundResponse

        Dim strResult As String = ConstructResult(formParams)
        Dim respDirectPay As New RefundResponse()

        Dim ResultValues As String() = strResult.Split(New String() {"||"}, StringSplitOptions.None)
        Dim index As Integer = 0
        For Each values As String In ResultValues
            If Not (values = "Null") Then
                Dim subVal As String() = values.Split("|")
                Dim subIndex As Integer = 0
                For Each val As String In subVal
                    If Not val = "Null" Then
                        Select Case index
                            Case 0
                                Select Case subIndex
                                    Case 0
                                        respDirectPay.TransactionRefundDetails().ReferenceID = Convert.ToInt64(val)
                                    Case 1
                                        respDirectPay.TransactionRefundDetails().MerchantOrderNumber = val
                                    Case 2
                                        respDirectPay.TransactionRefundDetails().RefundReferenceNumber = val
                                End Select
                            Case 1
                                Select Case subIndex
                                    Case 0
                                        respDirectPay.AmountRefund().TotalRefundedAmount = Convert.ToInt64(val)
                                    Case 1
                                        respDirectPay.AmountRefund().Amountavailableforrefund = Convert.ToDecimal(val)
                                    Case 2
                                        respDirectPay.AmountRefund().Currency = val

                                End Select
                            Case 2
                                Select Case subIndex
                                    Case 0
                                        respDirectPay.StatusBlock().StatusFlag = val
                                    Case 1
                                        respDirectPay.StatusBlock().ReasonCode = val
                                    Case 2
                                        respDirectPay.StatusBlock().ReasonDescription = val
                                End Select
                            Case Else
                                Exit Select
                        End Select
                    End If
                    subIndex = subIndex + 1
                Next
            End If
            index = index + 1
        Next
        Return respDirectPay


    End Function
    Public Shared Function ConstructResult(ByVal strResult As String) As String
        Dim value2Obj As String() = strResult.Split(New String() {"||"}, StringSplitOptions.None) 'Dim parts2 As String() = Regex.Split(strResult, "\|\|")
        Dim _constructResult As String = ""
        Dim subConstructResult As String = ""
        Dim cou As Integer = 1
        For Each c As Char In value2Obj(0)
            If (c = "1") Then
                Dim subCou As Integer = 1
                Dim subVal As String() = value2Obj(cou).Split("|")
                For Each s As Char In subVal(0)
                    If (s = "1") Then
                        If subCou = 1 Then
                            subConstructResult = subVal(subCou)
                            subCou = subCou + 1
                        Else
                            subConstructResult = subConstructResult + "|" + subVal(subCou)
                            subCou = subCou + 1
                        End If
                    Else
                        If subCou = 1 Then
                            subConstructResult = "Null"
                        Else
                            subConstructResult = subConstructResult + "|Null"
                        End If
                    End If
                Next
                If _constructResult = "" Then
                    _constructResult = subConstructResult
                    cou = cou + 1
                Else
                    _constructResult = _constructResult + "||" + subConstructResult
                    cou = cou + 1
                End If
            Else
                If _constructResult = "" Then
                    _constructResult = "Null"

                Else
                    _constructResult = _constructResult + "||Null"

                End If

            End If

        Next
        Return _constructResult
    End Function
End Class
