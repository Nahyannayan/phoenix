Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Partial Class Transport_feeSetUpServicesAndCharge
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            txtFrom.Text = Format(Now.Date, OASISConstants.DateFormat)
            txtStdNo.Text = Request.QueryString("stuid")
            Dim vACD_ID As String = Request.QueryString("acd")
            FeeCommon.GetStudentWithLocation_Transport(h_Student_no.Value, txtStdNo.Text, _
            txtStudentname.Text, Request.QueryString("bsu"), False, "", "", Now.Date, vACD_ID)
            txtStdNo.Attributes.Add("readonly", "readonly")
            txtStudentname.Attributes.Add("readonly", "readonly")
            txtLocation.Attributes.Add("readonly", "readonly")
        End If
    End Sub

    'ts
 
    Private Function NewTransportRequestForCollection(ByVal p_STU_NO As String, ByVal p_FROMDATE As String, _
        ByVal p_STU_SBL_ID_PICKUP As String, ByVal p_USR As String, ByVal p_Transaction As SqlTransaction) As String
        '  '@return_value = [dbo].[NewTransportRequestForCollection]
        '@STU_ID = 60941,
        '@STU_SBL_ID_PICKUP = 12,
        '@STU_PICKUP = NULL,
        '@STU_SBL_ID_DROPOFF = NULL,
        '@STU_DROPOFF = NULL,
        '@FROMDATE = N'12-OCT-2008'
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(0).Value = p_STU_NO
        pParms(1) = New SqlClient.SqlParameter("@STU_SBL_ID_PICKUP", SqlDbType.Int)
        pParms(1).Value = p_STU_SBL_ID_PICKUP
        pParms(2) = New SqlClient.SqlParameter("@STU_PICKUP", SqlDbType.Int)
        pParms(2).Value = System.DBNull.Value
        pParms(3) = New SqlClient.SqlParameter("@STU_SBL_ID_DROPOFF", SqlDbType.Int)
        pParms(3).Value = System.DBNull.Value
        pParms(4) = New SqlClient.SqlParameter("@STU_DROPOFF", SqlDbType.Int)
        pParms(4).Value = System.DBNull.Value
        pParms(5) = New SqlClient.SqlParameter("@FROMDATE", SqlDbType.VarChar, 100)
        pParms(5).Value = p_FROMDATE
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        pParms(7) = New SqlClient.SqlParameter("@USR", SqlDbType.VarChar, 100)
        pParms(7).Value = p_USR
        pParms(8) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(8).Value = Request.QueryString("acd")
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(p_Transaction, _
          CommandType.StoredProcedure, "NewTransportRequestForCollection", pParms)
        Return pParms(6).Value
    End Function


    Function CheckForErrors() As String
        Dim str_error As String = ""

        If Not IsDate(txtFrom.Text) Then
            str_error = str_error & "Invalid from date <br />"
        End If
        If h_Student_no.Value = "" Then
            str_error = str_error & "Please select student <br />"
        End If
        If H_Location.Value = "" Or txtLocation.Text = "" Then
            str_error = str_error & "Please select location <br />"
        End If
        Return str_error
    End Function

    Sub Clear_all()
        txtStdNo.Text = ""
        txtStudentname.Text = ""
        txtLocation.Text = ""
        H_Location.Value = ""
        h_Student_no.Value = ""
    End Sub

    Protected Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        Dim str_error As String = CheckForErrors()
        Dim STR_BSU_Provider As String = feeGenerateTransportFeeCharge.Get_BSU_Provider(Request.QueryString("bsu"), Request.QueryString("acd"))

        If STR_BSU_Provider = "" Then
            lblError.Text = "Invalid business unit!!!"
            Exit Sub
        End If
        If str_error <> "" Then
            lblError.Text = str_error
            Exit Sub
        End If
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction

        Try
            Dim retval As String

            retval = NewTransportRequestForCollection(h_Student_no.Value, txtFrom.Text, _
            H_Location.Value, Session("sUsr_name"), stTrans)

            'retval = feeGenerateTransportFeeCharge.F_GenerateTransportFeeCharge_For_BSU(STR_BSU_Provider, _
            'Request.QueryString("acd"), txtFrom.Text, Request.QueryString("bsu"), _
            'h_Student_no.Value, objConn, stTrans)
            If retval = "0" Then
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable("Charge+Service student", _
                Request.QueryString("bsu") & " - " & txtFrom.Text & " - " & Request.QueryString("acd"), _
                "Insert", Page.User.Identity.Name.ToString, Me.Page)
                stTrans.Commit()
                lblError.Text = getErrorMessage("0")
                Clear_all()
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = getErrorMessage("1000")
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
End Class
