<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FEEConcessionTransCancel.aspx.vb" Inherits="Fees_FEEConcessionTransCancel" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>  
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script src="../Scripts/PopupJQuery.js?1=2"></script>
 <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
   <link href="../cssfiles/Popup.css" rel="stylesheet" />

    
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
        </script>
  <script lang="javascript" type="text/javascript">
      function GetStudent()
      {     
        var sFeatures;
        var sFeatures;
        sFeatures="dialogWidth: 750px; ";
        sFeatures+="dialogHeight: 475px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var NameandCode;
        var result;
        var url = "ShowStudent.aspx?type=CONCESSION";
          //result = window.showModalDialog(url,"", sFeatures)
        return ShowWindowWithClose(url, 'search', '55%', '85%')
        return false;
        <%--if(result != '' && result != undefined)
        {
            NameandCode = result.split('||');
            document.getElementById('<%=h_STUD_ID.ClientID %>').value=NameandCode[0];
            document.getElementById('<%=txtStud_Name.ClientID %>').value=NameandCode[1];
             document.getElementById('<%= txtStdNo.ClientID %>' ).value=NameandCode[2] ;
            return true;
        }
        else
        {
            return false;
        }--%>
      }
      function setCancelConValue(result) {
          var NameandCode = result.split('||');
            document.getElementById('<%=h_STUD_ID.ClientID %>').value=NameandCode[0];
            document.getElementById('<%=txtStud_Name.ClientID %>').value=NameandCode[1];
          document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];
          CloseFrame();
          return false;
      }
      function CloseFrame() {
          jQuery.fancybox.close();
      }
    function GETReference(HEAD_SUB)
      {     
        var sFeatures;
        var sFeatures;
        var NameandCode;
        var result;
        var REF_TYPE;
        var STUD_ID;
        var url;
        var h_FCT_ID_HEAD = '<%=h_FCT_ID_HEAD.ClientID %>';
        var h_FCT_ID_DET = '<%=h_FCT_ID_DET.ClientID %>';
        var H_REFID_HEAD = '<%=H_REFID_HEAD.ClientID %>';
        var txtRefHEAD ='<%=txtRefHEAD.ClientID %>';
        var txtRefIDsub='<%=txtRefIDsub.ClientID %>';
        var H_REFID_SUB='<%=H_REFID_SUB.ClientID %>';
        if(HEAD_SUB == 0)
        {
         REF_TYPE = document.getElementById(h_FCT_ID_HEAD).value;
        }
        else
        {
         REF_TYPE = document.getElementById(h_FCT_ID_DET).value;
        }
         if (REF_TYPE=="")
        {
              alert('Please Select Concession Type')
              return false;
        }
        
        STUD_ID = document.getElementById('<%=h_STUD_ID.ClientID %>').value;
        if (STUD_ID=="")
        { 
              alert('Please Select Student')
              return false;
        }
        //alert(REF_TYPE);
        if(REF_TYPE == 1)
        {
            sFeatures="dialogWidth: 750px; ";
            sFeatures+="dialogHeight: 475px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";    
            url = "ShowStudent.aspx?type=REFER&STU_ID=" + STUD_ID;
        }
        else if(REF_TYPE == 2)
        {
            sFeatures="dialogWidth: 460px; ";
            sFeatures+="dialogHeight: 370px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: no; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            url = "../Payroll/empShowMasterEmp.aspx?id=REFEP&STU_ID=" + STUD_ID;
        }
        else
        {
            return false;
        }
        result = window.showModalDialog(url,"", sFeatures)
        if(result != '' && result != undefined)
        {        
            if(HEAD_SUB == 0)
            {
                  if(REF_TYPE == 1)
                   {
                    NameandCode = result.split('||');
                    document.getElementById(H_REFID_HEAD).value=NameandCode[0];
                    document.getElementById(txtRefHEAD).value=NameandCode[1];
                    if  (document.getElementById(txtRefIDsub).value=='' && (document.getElementById(h_FCT_ID_DET).value==document.getElementById(h_FCT_ID_HEAD).value) )
                       {
                          document.getElementById(H_REFID_SUB).value=NameandCode[0];
                          document.getElementById(txtRefIDsub).value=NameandCode[1];
                       }  
                    }
                  else if(REF_TYPE == 2)
                  {
                    NameandCode = result.split('___');
                    document.getElementById(H_REFID_HEAD).value=NameandCode[1]; 
                    document.getElementById(txtRefHEAD).value=NameandCode[0];
                     if  (document.getElementById(txtRefIDsub).value=='' && (document.getElementById(h_FCT_ID_DET).value==document.getElementById(h_FCT_ID_HEAD).value))
                       {
                          document.getElementById(H_REFID_SUB).value=NameandCode[1];
                          document.getElementById(txtRefIDsub).value=NameandCode[0];
                       } 
                  } 
            }
            else
            {
                  if(REF_TYPE == 1)
                   {
                    NameandCode = result.split('||');
                    document.getElementById(H_REFID_SUB).value=NameandCode[0];
                    document.getElementById(txtRefIDsub).value=NameandCode[1];
                   }
                  else if(REF_TYPE == 2)
                  {
                    NameandCode = result.split('___');
                    document.getElementById(H_REFID_SUB).value=NameandCode[1];
                    document.getElementById(txtRefIDsub).value=NameandCode[0];
                  }
            }
            return false;
        }
        else
        {
            return false;
        }
      }
function GetConcession(HEAD) 
   {  
        var sFeatures,url;
        sFeatures="dialogWidth: 600px; ";
        sFeatures+="dialogHeight: 500px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var NameandCode;
        var result; 
          url= "../common/PopupFormIDhidden.aspx?iD=CONCESSION&MULTISELECT=FALSE";
          result = window.showModalDialog(url,"", sFeatures);
           if (result=='' || result==undefined)
            {
            return false;
            }             
            NameandCode = result.split('___');
            if (HEAD==1) 
            {
               document.getElementById('<%= h_FCT_ID_HEAD.ClientID %>').value=NameandCode[0]; 
               document.getElementById('<%= h_FCM_ID_HEAD.ClientID %>').value=NameandCode[1]; 
               document.getElementById('<%= txtConcession_Head.ClientID %>' ).value=NameandCode[2];
                    document.getElementById('<%= H_REFID_HEAD.ClientID %>' ).value='';
                    document.getElementById('<%= txtRefHEAD.ClientID %>' ).value=''; 
               if  (document.getElementById('<%= txtConcession_Det.ClientID %>' ).value=='')
               {
                   document.getElementById('<%= h_FCT_ID_DET.ClientID %>').value=NameandCode[0]; 
                   document.getElementById('<%= h_FCM_ID_DET.ClientID %>').value=NameandCode[1]; 
                   document.getElementById('<%= txtConcession_Det.ClientID %>' ).value=NameandCode[2] ; 
               }
             }
             else
             {
               document.getElementById('<%= h_FCT_ID_DET.ClientID %>').value=NameandCode[0]; 
               document.getElementById('<%= h_FCM_ID_DET.ClientID %>').value=NameandCode[1]; 
               document.getElementById('<%= txtConcession_Det.ClientID %>' ).value=NameandCode[2] ; 
                 document.getElementById('<%= H_REFID_SUB.ClientID %>').value='';
                 document.getElementById('<%= txtRefIDsub.ClientID %>').value='';
             } 
             return false;
    }
                    Sys.Application.add_load( 
                function CheckForPrint()  
                     {
                       if (document.getElementById('<%= h_print.ClientID %>' ).value!='')
                       { 
                       document.getElementById('<%= h_print.ClientID %>' ).value=''; 
                      showModalDialog('../Reports/ASPX Report/RptViewerModal.aspx', '',"dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;"); 
                       }
                     } 
                    );
  </script>
    <link href="../cssfiles/Popup.css?1=2" rel="stylesheet" />
       <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>Cancel Fee Concession
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
    <table border="0"  width="75%" align="center">
        <tr>
            <td align="left">
    <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="VALSUB" CssClass="error" />
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" ValidationGroup="VALMAIN" />
            </td>
        </tr>
    </table> 
    <table align="center" border="1"  width="75%">
      <%--  <tr class="subheader_img">
            <td align="left" colspan="4" style="height: 19px"> Cancel Fee Concession</td>
        </tr>--%>
        <tr>
            <td align="left" class="matters">
                Academic year</td>
            <td align="left" class="matters" colspan="3">
                <asp:DropDownList
                        ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                    </asp:DropDownList>
                </td>
        </tr>
        <tr>
            <td align="left" class="matters">
                Student</td>
            <td align="left" class="matters" colspan="3">
                <div>
                <asp:TextBox ID="txtStdNo" runat="server" ></asp:TextBox>
                <asp:ImageButton
                    ID="imgCompany" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="return GetStudent();"/>&nbsp;<asp:TextBox ID="txtStud_Name" runat="server" Width="295px"  >
                        </asp:TextBox>
                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtStud_Name"
                    ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>--%>
                </div>
            </td>
        </tr>
        
        <tr>
            <td align="left" class="matters">
                Select Concession</td>
            <td align="left" class="matters">
                <asp:DropDownList id="ddlConcession" runat="server" OnSelectedIndexChanged="ddlConcession_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left" class="matters" colspan="1">
                Date</td>
            <td align="left" class="matters" colspan="1">
                <asp:TextBox ID="txtDate" runat="server" ></asp:TextBox>
                <asp:ImageButton ID="imgDate" runat="Server" AlternateText="Click to show calendar"
                    ImageUrl="~/Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtDate"
                    ErrorMessage="Date Required" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="VALMAIN">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td align="left" class="matters">
                From Date</td>
            <td align="left" class="matters">
                <asp:TextBox ID="txtFromDT" runat="server" AutoPostBack="True" OnTextChanged="txtFromDT_TextChanged"></asp:TextBox>
                <asp:ImageButton ID="ImagefromDate" runat="server" AlternateText="Click to show calendar"
                    ImageUrl="~/Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFromDT"
                    ErrorMessage="From Date Reqired" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtFromDT"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="VALMAIN">*</asp:RegularExpressionValidator></td>
            <td align="left" class="matters" colspan="1">
                To Date</td>
            <td align="left" class="matters" colspan="1">
                <asp:TextBox ID="txtToDT" runat="server"  AutoPostBack="True" OnTextChanged="txtToDT_TextChanged"></asp:TextBox>
                <asp:ImageButton ID="imgToDT" runat="server" AlternateText="Click to show calendar"
                    ImageUrl="~/Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtToDT"
                    ErrorMessage="To Date Required" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtToDT"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="VALMAIN">*</asp:RegularExpressionValidator>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="txtToDT"
                    ControlToValidate="txtFromDT" ErrorMessage="From Date should be less than To Date"
                    Operator="LessThan" Type="Date"  ValidationGroup="VALMAIN" Enabled="False">*</asp:CompareValidator></td>
        </tr>
        <tr>
            <td align="left" class="matters">
                Concession Type</td>
            <td align="left" class="matters" colspan="3">
                <asp:TextBox ID="txtConcession_Head" runat="server" ></asp:TextBox>
                <asp:ImageButton
                    ID="imgConcession_Head" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="return GetConcession(1);" Enabled="False" Visible="False" />&nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" class="matters">
                Ref. Details</td>
            <td align="left" class="matters" colspan="3">
                <asp:TextBox ID="txtRefHEAD" runat="server" ></asp:TextBox>
                <asp:ImageButton
                    ID="imgRefHead" runat="server" ImageUrl="~/Images/pickbutton.gif" OnClientClick="return GETReference(0);" Enabled="False" Visible="False" /></td>
        </tr>
        <tr>
            <td align="left" class="matters">
                Remarks</td>
            <td align="left" class="matters" colspan="3">
                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"  ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtRemarks"
                    ErrorMessage="Please specify remarks" ValidationGroup="VALMAIN">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td align="left" class="title-bg-light" colspan="4" valign="middle"> Concession Details</td>
        </tr>
        <tr style="display:none">
            <td align="left" class="matters" style="height: 21px">
                Concession Type</td>
            <td align="left" class="matters" style="height: 21px">
                <asp:TextBox ID="txtConcession_Det" runat="server" Width="151px" SkinID="TextBoxNormal"></asp:TextBox>
                <asp:ImageButton
                    ID="imgConcession_Det" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="return GetConcession(0);" Enabled="False" Visible="False" /></td>
            <td align="left" class="matters" colspan="1" style="height: 21px">
                Ref. Details</td>
            <td align="left" class="matters" colspan="1" style="height: 21px">
                <asp:TextBox ID="txtRefIDsub" runat="server" Width="129px" SkinID="TextBoxNormal"></asp:TextBox>&nbsp;<asp:ImageButton
                    ID="imgRefSub" runat="server" ImageUrl="~/Images/pickbutton.gif" OnClientClick="return GETReference(1);" Enabled="False" Visible="False" /></td>
        </tr>
        <tr>
            <td align="left" class="matters">
                Fee Type</td>
            <td align="left" class="matters">
                <asp:DropDownList ID="ddlFeeType" runat="server" AutoPostBack="True" SkinID="DropDownListNormal" Enabled="False">
                </asp:DropDownList>
                </td>
            <td align="left" class="matters" colspan="1">
                Total Fee</td>
            <td align="left" class="matters" colspan="1">
                <asp:TextBox ID="txtTotalFee" runat="server" AutoCompleteType="disabled" Width="122px" SkinID="TextBoxNormal"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left" class="matters">
                Charge Type</td>
            <td align="left" class="matters">
                <asp:RadioButton ID="radAmount" runat="server" Text="Amount" ValidationGroup="AMTTYPE" GroupName="ChargeType" />
                <asp:RadioButton ID="radPercentage" runat="server" Text="Percentage" ValidationGroup="AMTTYPE" GroupName="ChargeType" /></td>
            <td align="left" class="matters" colspan="1">
                Actual
                Amount/Percentage</td>
            <td align="left" class="matters" colspan="1">
                <asp:TextBox ID="txtAmount" runat="server" Width="122px" AutoCompleteType="disabled" SkinID="TextBoxNormal"></asp:TextBox>&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Amount required" ControlToValidate="txtAmount" ValidationGroup="VALSUB">*</asp:RequiredFieldValidator>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Amount should be valid"
                    Operator="DataTypeCheck" Type="Double" ControlToValidate="txtAmount" ValidationGroup="VALSUB">*</asp:CompareValidator></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="2">
            </td>
            <td align="left" class="matters" colspan="1">
                Cancel Amount/Percentage</td>
            <td align="left" class="matters" colspan="1">
                <asp:TextBox ID="txtPercAmountCancel" runat="server" Width="122px" AutoCompleteType="disabled" SkinID="TextBoxNormal">
                </asp:TextBox>
                <asp:LinkButton id="lnkFill_Cancel" runat="server" OnClick="lnkFill_Cancel_Click">(Update)</asp:LinkButton></td>
        </tr>
        <tr>
            <td align="center" class="matters" colspan="4">
                <asp:Label ID="lblAlert" CssClass="taxamtmessage" runat="server"></asp:Label></td>
        </tr>
        </table>
         <table border="1" align="center" width="75%" >
             <tr class="title-bg-light">
                 <td align="left" class="gridheader_pop" colspan="2">
                     Monthly/Termly Split up(Cancellation Details)</td> 
             </tr>
        <tr>
            <td align="center" class="matters" >
            <asp:GridView ID="gvMonthly" runat="server" AutoGenerateColumns="False" CellPadding="4" SkinID="GridViewNormal">
                    <Columns>
                        <asp:BoundField DataField="DESCR" HeaderText="Term/Month" ReadOnly="True"></asp:BoundField>
                        <asp:TemplateField HeaderText="Amount To Cancel "><ItemTemplate>
                        <asp:TextBox ID="txtAmount" runat="server" AutoCompleteType="Disabled" Text='<%# Bind("CUR_AMOUNT") %>' Width="102px" SkinID="TextBoxNormal"></asp:TextBox>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Charge Dt."><ItemTemplate>
                        <TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="HEIGHT: 24px">
                        <asp:TextBox id="txtChargeDate" runat="server" Text='<%# Bind("FDD_DATE", "{0:dd/MMM/yyyy}") %>' SkinID="TextBoxNormal" Width="100px" OnPreRender="txtChargeDate_PreRender"></asp:TextBox>
                         </TD><TD style="HEIGHT: 24px">&nbsp; <asp:ImageButton id="imgChargeDate" tabIndex=4 runat="server" ImageUrl="~/Images/calendar.gif" ></asp:ImageButton></TD></TR></TBODY></TABLE>
                         <ajaxToolkit:CalendarExtender id="CalChargeDate" runat="server" CssClass="MyCalendar" TargetControlID="txtChargeDate" PopupButtonID="imgChargeDate" Format="dd/MMM/yyyy" PopupPosition="TopLeft">
                        </ajaxToolkit:CalendarExtender> 
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="FDD_AMOUNT" HeaderText="Actual Amount">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="ID" Visible="False"><ItemTemplate>
                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                    </Columns> 
                </asp:GridView> 
                </td>
            <td width="20%" "> <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="VALSUB" /><asp:Button ID="btnDetCancel" runat="server" CssClass="button" Text="Cancel" /></td>
        </tr>
             <tr class="title-bg-light">
                 <td align="left" class="gridheader_pop" colspan="2">
                     Fee Concession Details(Actual)</td> 
             </tr>
        <tr>
            <td class="matters" colspan="2">
                <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Data Added Yet..." >
                    <Columns>
<asp:TemplateField HeaderText="FEE_ID" Visible="False"><ItemTemplate>
                                <asp:Label ID="lblFCD_ID" runat="server" Text='<%# bind("FCD_ID") %>'></asp:Label>
                            
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Concession Type"><ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# bind("FCM_DESCR") %>'></asp:Label>
                            
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Fee Type"><ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# bind("FEE_DESCR") %>'></asp:Label>
                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Amount Type"><ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# bind("AMT_TYPE") %>'></asp:Label>
                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Amount/%"><ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# bind("AMOUNT") %>'></asp:Label>
                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Edit"><ItemTemplate>
                                <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Delete"><ItemTemplate>
                                <asp:LinkButton ID="lnkBtnDelete" runat="server" OnClick="lnkBtnDelete_Click">Delete</asp:LinkButton>
                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns> 
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="matters" colspan="2" align="center">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="VALMAIN" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                <asp:Button id="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    OnClick="btnDelete_Click" OnClientClick="return confirm('Are you sure you want to Delete This Record ?');"
                    Text="Delete" />
                <asp:Button id="btnPrint" runat="server" CssClass="button" onclick="btnPrint_Click"
                    Text="Print" /></td>
        </tr>
    </table>

    </div>
        </div>
    </div>
    <ajaxToolkit:CalendarExtender ID="Calendarextender1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgToDT" TargetControlID="txtToDT">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="Calendarextender2" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="ImagefromDate" TargetControlID="txtFromDT">
    </ajaxToolkit:CalendarExtender>
    <asp:HiddenField ID="h_FCT_ID_HEAD" runat="server" />
    <asp:HiddenField ID="h_FCM_ID_HEAD" runat="server" />
    <asp:HiddenField ID="h_FCM_ID_DET" runat="server" />
    <asp:HiddenField ID="H_REFID_HEAD" runat="server" />
    <asp:HiddenField ID="h_FCT_ID_DET" runat="server" />
    <asp:HiddenField ID="H_REFID_SUB" runat="server" />
    <asp:HiddenField id="h_GRD_ID" runat="server" />
    <asp:HiddenField ID="h_STUD_ID" runat="server" />
    <asp:HiddenField ID="h_FCH_ID" runat="server" />
    <asp:HiddenField id="h_print" runat="server" />
    <asp:HiddenField id="h_Tdate" runat="server" />
    <asp:HiddenField id="h_Fdate" runat="server" />
    
   
</asp:Content>



