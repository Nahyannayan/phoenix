﻿
Partial Class Fees_ShowAdvInvoices
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim TRAN_TYPE As String = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
            Dim STU_TYPE As String = Encr_decrData.Decrypt(Request.QueryString("STYP").Replace(" ", "+"))
            Dim STU_ID As String = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
            Dim FEE_ID As String = Encr_decrData.Decrypt(Request.QueryString("FID").Replace(" ", "+"))
            If TRAN_TYPE = "INV" Then
                divLedger.Visible = False
                divInvoice.Visible = True
                LOAD_INVOICE(STU_TYPE, STU_ID, FEE_ID)
            ElseIf TRAN_TYPE = "STULEDGER" Then
                divInvoice.Visible = False
                divLedger.Visible = True
                LOAD_LEDGER(STU_TYPE, STU_ID, FEE_ID)
            End If
        End If
    End Sub

    Private Sub LOAD_INVOICE(ByVal STU_TYPE As String, ByVal STU_ID As String, ByVal FEE_ID As String)
        Dim objclsTaxFunctions As New clsTaxFunctions
        objclsTaxFunctions.PROVIDER_BSU_ID = Session("sBsuId")
        objclsTaxFunctions.STU_BSU_ID = Session("sBsuId")
        objclsTaxFunctions.STU_ID = STU_ID
        objclsTaxFunctions.FEE_ID = FEE_ID
        objclsTaxFunctions.bSUMMARY = False
        gvAdvances.DataSource = objclsTaxFunctions.GET_ADVANCE_INV_STATEMENT(STU_TYPE)
        gvAdvances.DataBind()
    End Sub

    Private Sub LOAD_LEDGER(ByVal STU_TYPE As String, ByVal STU_ID As String, ByVal FEE_ID As String)
        Dim objclsTaxFunctions As New clsTaxFunctions
        objclsTaxFunctions.PROVIDER_BSU_ID = Session("sBsuId")
        objclsTaxFunctions.STU_BSU_ID = Session("sBsuId")
        objclsTaxFunctions.STU_TYPE = STU_TYPE
        objclsTaxFunctions.STU_ID = STU_ID
        objclsTaxFunctions.FEE_ID = FEE_ID
        objclsTaxFunctions.FROM_DATE = Format(DateTime.Today.AddMonths(-12), OASISConstants.DataBaseDateFormat)
        objclsTaxFunctions.TO_DATE = Format(DateTime.Today, OASISConstants.DataBaseDateFormat)
        gvStuLedger.DataSource = objclsTaxFunctions.GET_STU_LEDGER
        gvStuLedger.DataBind()
    End Sub
End Class
