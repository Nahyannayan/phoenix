Imports System.Data
Imports System.Data.SqlClient

Partial Class fees_FeeReminderTemplateforTransport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.AppendHeader("Pragma", "no-cache")
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> "F300230" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim tblbUSuper As Boolean = Session("sBusper")
                If tblbUSuper = True Then
                    ddlBusinessUnit.DataSource = AccessRoleUser.GetBusinessUnits()
                    ddlBusinessUnit.DataTextField = "bsu_name"
                    ddlBusinessUnit.DataValueField = "bsu_id"
                Else
                    ddlBusinessUnit.DataSource = AccessRoleUser.GetTotalBUnit(Session("sUsr_id"))
                    ddlBusinessUnit.DataTextField = "bname"
                    ddlBusinessUnit.DataValueField = "B_unit"
                End If
                ddlBusinessUnit.DataBind()
                ddlBusinessUnit.SelectedIndex = -1
                ddlBusinessUnit.SelectedValue = Session("sBSUID")
            End If
            If ViewState("datamode") = "view" Then
                Dim FRM_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                Dim vViewFEERem As FeeReminderTemplate = FeeReminderTemplate.GetFeeReminderTemplate(FRM_ID, Me.ddlBusinessUnit.SelectedValue, ConnectionManger.GetOASISTransportConnection)
                ddlBusinessUnit.ClearSelection()
                ddlBusinessUnit.SelectedValue = vViewFEERem.FRM_BSU_ID
                ddlLevel.SelectedValue = vViewFEERem.FRM_Level
                'txtRemarks.Content = vViewFEERem.FRM_REMARKS.Replace("<", "[[").Replace(">", "]]")
                'txtShortDescr.Content = vViewFEERem.FRM_SHORT_DESCR.Replace("<", "[[").Replace(">", "]]")
                'txtSignature.Content = vViewFEERem.FRM_SIGNATORY.Replace("<", "[[").Replace(">", "]]")
                'txtAcknowlg.Content = vViewFEERem.FRM_Acknowledgement.Replace("<", "[[").Replace(">", "]]")
                'txtSMS.Content = vViewFEERem.FRM_SMS.Replace("<", "[[").Replace(">", "]]")

                txtRemarks.Content = vViewFEERem.FRM_REMARKS
                txtShortDescr.Content = vViewFEERem.FRM_SHORT_DESCR
                txtSignature.Content = vViewFEERem.FRM_SIGNATORY
                txtAcknowlg.Content = vViewFEERem.FRM_Acknowledgement
                txtSMS.Text = vViewFEERem.FRM_SMS
                Session("sFEE_REMINDER_TEMPLATE") = vViewFEERem
                DissableControls(True)
            End If
        End If
    End Sub

    Private Sub DissableControls(ByVal bDissable As Boolean)
        ddlBusinessUnit.Enabled = Not bDissable
        ddlLevel.Enabled = Not bDissable
        txtRemarks.Enabled = Not bDissable
        txtShortDescr.Enabled = Not bDissable
        txtSignature.Enabled = Not bDissable
        txtAcknowlg.Enabled = Not bDissable
        txtSMS.Enabled = Not bDissable
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim vFeeRem As New FeeReminderTemplate
        If Not Session("sFEE_REMINDER_TEMPLATE") Is Nothing Then
            vFeeRem = Session("sFEE_REMINDER_TEMPLATE")
        End If
        vFeeRem.FRM_BSU_ID = ddlBusinessUnit.SelectedValue
        vFeeRem.FRM_Level = ddlLevel.SelectedValue

        'vFeeRem.FRM_REMARKS = txtRemarks.Content.Replace("[[", "<").Replace("]]", ">")
        'vFeeRem.FRM_SHORT_DESCR = txtShortDescr.Content.Replace("[[", "<").Replace("]]", ">")
        'vFeeRem.FRM_SIGNATORY = txtSignature.Content.Replace("[[", "<").Replace("]]", ">")
        'vFeeRem.FRM_Acknowledgement = txtAcknowlg.Content.Replace("[[", "<").Replace("]]", ">")
        'vFeeRem.FRM_SMS = txtSMS.Content.Replace("[[", "<").Replace("]]", ">")


        vFeeRem.FRM_REMARKS = txtRemarks.Content
        vFeeRem.FRM_SHORT_DESCR = txtShortDescr.Content
        vFeeRem.FRM_SIGNATORY = txtSignature.Content
        vFeeRem.FRM_Acknowledgement = txtAcknowlg.Content
        vFeeRem.FRM_SMS = txtSMS.Text
        Dim conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction("FEE_REMINDER")
        Dim retVal As Integer = FeeReminderTemplate.SaveFeeReminderTemplate(vFeeRem, trans, conn)
        If retVal <> 0 Then
            trans.Rollback()
            'lblError.Text = UtilityObj.getErrorMessage(retVal)
            usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
        Else
            trans.Commit()
            'lblError.Text = "Data updated Successfully"
            usrMessageBar.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
            ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
            'If flagAudit <> 0 Then
            '    Throw New ArgumentException("Could not process your request")
            'End If
        End If
    End Sub

    Private Sub ClearDetails()
        ddlBusinessUnit.ClearSelection()
        ddlLevel.ClearSelection()
        txtRemarks.Content = ""
        txtShortDescr.Content = ""
        txtSignature.Content = ""
        txtAcknowlg.Content = ""
        txtSMS.Text = ""
        Session("sFEE_REMINDER_TEMPLATE") = Nothing
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        DissableControls(False)
        UtilityObj.beforeLoopingControls(Me.Page)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearDetails()
        DissableControls(False)
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ddlLevel.Enabled = True
    End Sub

End Class
