Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Sql
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO

Partial Class Fees_Fee_Adj_InterUnit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public Property IsProviderInitiated() As Boolean
        Get
            Return ViewState("IsProviderInitiated")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsProviderInitiated") = value
        End Set
    End Property
    Public Property IsProviderReceiving() As Boolean
        Get
            Return ViewState("IsProviderReceiving")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsProviderReceiving") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnUpload)
        'smScriptManager.RegisterPostBackControl(uplValidator)
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            imgStudent.Attributes.Add("onClick", "return getStudent();")

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F300149" And ViewState("MainMnu_code") <> "F300152" And ViewState("MainMnu_code") <> "F300184" And ViewState("MainMnu_code") <> "F300185") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                If ViewState("MainMnu_code") = "F300184" Or ViewState("MainMnu_code") = "F300185" Then
                    IsProviderInitiated = True
                    trStudentBSU.Style("display") = "contents"
                Else
                    IsProviderInitiated = False
                    trStudentBSU.Style("display") = "none"
                End If
                FEE_ADJ_INTERUNIT.SetProvidersStudentBSU(ddlStudentBSU, Session("sBsuid"))
                Select Case ViewState("MainMnu_code")
                    Case "F300149", "F300185"
                        Dim CurBsUnit As String = Session("sBsuid")
                        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Case "F300152", "F300184"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), "2", ViewState("datamode"))
                        ViewState("status") = Encr_decrData.Decrypt(Request.QueryString("status").Replace(" ", "+")).ToString
                        If (ViewState("status") = "N") Then
                            btnApprove.Visible = True
                            btnReject.Visible = True
                        End If

                End Select

            End If
            ClearDetails()
            h_BSUID.Value = Session("sBsuid")
            lnkRefundable.Visible = False
            Panel1.Visible = False
            FEE_ADJ_INTERUNIT.PopulateBSU(ddlOtherBSU, h_BSUID.Value, False)
            chkIsNonSchool.Checked = False
            FEE_ADJ_INTERUNIT.PopulateACDYear(ddlAcademicYear, ddlStudentBSU.SelectedValue)
            Me.hf_FAI_ID.Value = "-1"
            gvFeeDetails.Attributes.Add("bordercolor", "#1b80b6")
            'Me.btnRecall.Attributes.Add("OnClientClick", "return ConfirmRecall();")
            txtHeaderRemarks.Attributes.Add("OnBlur", "FillDetailRemarks()")
            Session("sFEE_ADJ_INTERUNIT") = Nothing
            txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
            ViewState("gvFeeDetails") = FEE_ADJ_INTERUNIT.CreateGvFeeDetailSchema

            If ViewState("datamode") = "view" Or ViewState("datamode") = "approve" Then
                'pnlCover.Enabled = False
                Dim objFAI As New FEE_ADJ_INTERUNIT
                objFAI.FAI_ID = Encr_decrData.Decrypt(Request.QueryString("FAI_ID").Replace(" ", "+"))
                hf_FAI_ID.Value = objFAI.FAI_ID
                objFAI.LoadSavedAdjustmentH()
                txtDate.Text = objFAI.FAI_DATE
                chkIsNonSchool.Checked = objFAI.FAI_CR_IsProvider

                txtOTU_STU_Detail.Text = objFAI.FAI_STU_DETAILS
                txtHeaderRemarks.Text = objFAI.FAI_REMARKS
                ddlStudentBSU.SelectedValue = objFAI.FAI_DR_STU_BSU_ID
                FEE_ADJ_INTERUNIT.PopulateACDYear(ddlAcademicYear, ddlStudentBSU.SelectedValue)
                ddlAcademicYear.SelectedValue = objFAI.FAI_ACD_ID
                h_STUD_ID.Value = objFAI.FAI_STU_ID
                radStud.Checked = IIf(objFAI.FAI_STU_TYPE = "S", True, False)
                radEnq.Checked = IIf(objFAI.FAI_STU_TYPE = "E", True, False)
                chkIsNonSchool.Checked = objFAI.FAI_CR_IsProvider
                FEE_ADJ_INTERUNIT.PopulateBSU(ddlOtherBSU, h_BSUID.Value, chkIsNonSchool.Checked)
                ddlOtherBSU.SelectedValue = objFAI.FAI_CR_BSU_ID
                ddlStudentBSU.SelectedValue = objFAI.FAI_DR_STU_BSU_ID
                Me.lblInfo.Text = objFAI.FAI_COMMENTS 'show commnets if entry was recalled

                If Not objFAI.FAI_STU_TYPE = "S" Then
                    radEnq.Checked = True
                Else
                    radStud.Checked = True
                End If

                SetStudentDetails(objFAI.FAI_BSU_ID, objFAI.FAI_STU_TYPE, objFAI.FAI_STU_ID)
                ViewState("gvFeeDetails") = objFAI.LoadSavedAdjustmentS()

                BindFeeDetails()
                'If ViewState("datamode") = "approve" Then
                DissableAllControls(True)
                LoadDoxGrid()
                'End If
                'Me.trSuppDocs.Visible = True
                Dim MyStatus As String, SqlStr As String
                SqlStr = "SELECT isnull(FAI_APPR_STATUS,'N') from FEES.FEE_ADJ_INTERUNIT_H WHERE FAI_ID=" & objFAI.FAI_ID.ToString
                MyStatus = Mainclass.getDataValue(SqlStr, "OASIS_FEESConnectionString")
                Dim err_string As String = ""
                If MyStatus <> "N" Then
                    'Me.lblError.Text = "Modifications not allowed! Adjustment has been Approved/Rejected"
                    err_string = "Modifications not allowed! Adjustment has been Approved/Rejected"
                    usrMessageBar2.ShowNotification(err_string, UserControls_usrMessageBar.WarningType.Danger)
                    DissableAllControls(True)
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), "2", ViewState("datamode"))
                    If MyStatus = "A" Then 'If adjustment is approved from source unit
                        If objFAI.FAI_CR_APPR_STATUS = "N" And objFAI.FAI_CR_STU_ID = 0 Then 'If adjustment is not accepted or rejected by destination unit
                            Me.btnRecall.Visible = True
                        End If
                    End If
                End If
                If objFAI.FAI_CR_APPR_STATUS <> "N" Or objFAI.FAI_CR_STU_ID <> 0 Then
                    'Me.lblError.Text = "Modifications not allowed! Adjustment has been accepted at the other business unit"
                    If err_string = "" Then
                        err_string = "Modifications not allowed! Adjustment has been accepted at the other business unit"
                        usrMessageBar2.ShowNotification(err_string, UserControls_usrMessageBar.WarningType.Danger)
                        DissableAllControls(True)
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), "2", ViewState("datamode"))
                    End If
                End If

            End If
            PopulateFeeType()
        End If
    End Sub
    Public Sub SetStudentDetails(ByVal ProviderBSUID As String, ByVal STU_TYPE As String, ByVal STU_ID As String)
        Dim str_sql As String = String.Empty
        Dim ds As New DataTable
        Dim sqlParam(2) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@ProviderBsu_id", ProviderBSUID, SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@STU_TYPE", STU_TYPE, SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@STU_ID", STU_ID, SqlDbType.VarChar)
        ds = Mainclass.getDataTable("GetAllStudentDetails", sqlParam, ConnectionManger.GetOASIS_FEESConnectionString)
        If ds.Rows.Count > 0 Then
            txtStdNo.Text = ds.Rows(0).Item("STU_NO")
            txtStudentname.Text = ds.Rows(0).Item("STU_NAME")
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            ClearDetails()
        Catch
        End Try
    End Sub

    Private Sub DissableAllControls(ByVal dissble As Boolean)
        txtDate.ReadOnly = dissble
        txtHeaderRemarks.ReadOnly = dissble
        txtOTU_STU_Detail.ReadOnly = dissble
        txtDetRemarks.ReadOnly = dissble
        txtDetAmount.ReadOnly = dissble
        ddlFeeType.Enabled = Not dissble
        ddlAcademicYear.Enabled = Not dissble
        ddlOtherBSU.Enabled = Not dissble
        ddlStudentBSU.Enabled = Not dissble
        btnDetAdd.Enabled = Not dissble
        imgDate.Enabled = Not dissble
        CalendarExtender1.Enabled = Not dissble
        calendarButtonExtender.Enabled = Not dissble
        txtStdNo.ReadOnly = dissble
        txtStudentname.ReadOnly = dissble
        imgStudent.Enabled = Not dissble
        chkIsNonSchool.Enabled = Not dissble
        Me.UploadDocPhoto.Enabled = Not dissble
        Me.chkShared.Enabled = Not dissble
        Me.btnUpload.Enabled = Not dissble
        'Me.gvSuppDocs.Columns(3).Visible = Not dissble
        Me.gvSuppDocs.Columns(4).Visible = Not dissble
    End Sub

    Private Sub GridBindAdjustments()

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar2.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        Dim conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim trans As SqlTransaction
        Dim retVal As Integer = 1000
        Dim vNewFAI_ID As String = String.Empty
        Dim bEdit As Boolean = False
        trans = conn.BeginTransaction("FEE_ADJ_INTER")
        hf_FAI_ID.Value = IIf(Val(hf_FAI_ID.Value) <= 0, 0, Val(hf_FAI_ID.Value))
        If ViewState("datamode") = "edit" Then
            bEdit = True
        End If
        retVal = ValidateAndSave(conn, trans)
        If retVal <> "0" Then
            trans.Rollback()
            'If lblError.Text = "" Then
            '    lblError.Text = UtilityObj.getErrorMessage(retVal)
            'End If
            usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
        Else
            trans.Commit()
            'lblError.Text = UtilityObj.getErrorMessage(retVal)
            usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            ClearDetails()
            If chkPrint.Checked Then
                h_print.Value = "print"
                PrintReceiptAdjustmentRequest(hf_FAI_ID.Value)
            End If
            ViewState("datamode") = "save"
            'h_print.Value = "save"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), "2", ViewState("datamode"))
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
        End If
    End Sub

    Private Function ValidateAndSave(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As String
        ValidateAndSave = "0"
        'Me.lblError.Text = ""
        If h_STUD_ID.Value = "" Then
            'Me.lblError.Text = "Please select the student"
            usrMessageBar2.ShowNotification("Please select the student", UserControls_usrMessageBar.WarningType.Danger)
            ValidateAndSave = "1"
            Exit Function
        ElseIf txtDate.Text.Trim = "" Or Not IsDate(txtDate.Text) Then
            'Me.lblError.Text = "Please select the date"
            usrMessageBar2.ShowNotification("Please select the date", UserControls_usrMessageBar.WarningType.Danger)
            ValidateAndSave = "1"
            Exit Function
        ElseIf ddlOtherBSU.SelectedValue = "0" Then
            'Me.lblError.Text = "Please select the Other Business unit / School"
            usrMessageBar2.ShowNotification("Please select the Other Business unit / School", UserControls_usrMessageBar.WarningType.Danger)
            ValidateAndSave = "1"
            Exit Function
        ElseIf txtOTU_STU_Detail.Text.Trim = "" Then
            'Me.lblError.Text = "Please enter other school student details"
            usrMessageBar2.ShowNotification("Please enter other school student details", UserControls_usrMessageBar.WarningType.Danger)
            ValidateAndSave = "1"
            Exit Function
        ElseIf Me.gvFeeDetails.Rows.Count <= 0 Then
            'Me.lblError.Text = "Please add the fee-wise adjustment details"
            usrMessageBar2.ShowNotification("Please add the fee-wise adjustment details", UserControls_usrMessageBar.WarningType.Danger)
            ValidateAndSave = "1"
            Exit Function
        End If
        Dim objFAI As New FEE_ADJ_INTERUNIT
        Try

            objFAI.FAI_ID = Val(hf_FAI_ID.Value)
            objFAI.FAI_DATE = txtDate.Text
            objFAI.FAI_ACD_ID = ddlAcademicYear.SelectedValue
            objFAI.FAI_BSU_ID = h_BSUID.Value
            objFAI.FAI_STU_ID = h_STUD_ID.Value

            objFAI.FAI_DR_STU_BSU_ID = ddlStudentBSU.SelectedValue
            objFAI.FAI_DR_IsProvider = IsProviderInitiated
            objFAI.FAI_CR_IsProvider = chkIsNonSchool.Checked


            objFAI.FAI_STU_TYPE = IIf(radEnq.Checked, "E", "S")
            objFAI.FAI_CR_BSU_ID = ddlOtherBSU.SelectedValue
            objFAI.FAI_STU_DETAILS = txtOTU_STU_Detail.Text
            objFAI.FAI_REMARKS = txtHeaderRemarks.Text
            objFAI.User = Session("sUsr_name")

            Select Case ViewState("datamode")
                Case "add"
                    objFAI.PROCESS = "NEW"
                Case "edit"
                    objFAI.PROCESS = "EDIT"
                Case "approve"
                    objFAI.PROCESS = "APPROVE"
                Case "reject"
                    objFAI.PROCESS = "REJECT"
                Case "recall"
                    objFAI.PROCESS = "RECALL"
            End Select

            Dim retValH As String = "0"
            Dim retValD As String = "0"
            retValH = objFAI.SaveAdjustmentHeader(objCon, TRANS)

            If retValH = "0" Then
                Dim txtAppAmount As TextBox
                Dim lblFEE_ID As Label, lblAmount As Label, lblRemarks As Label
                For Each gvr As GridViewRow In gvFeeDetails.Rows 'Saving Adjustment request detail
                    If retValD = "0" Then
                        txtAppAmount = DirectCast(gvr.FindControl("txtAppAmt"), TextBox)
                        lblFEE_ID = DirectCast(gvr.FindControl("lblFEE_ID"), Label)
                        lblAmount = DirectCast(gvr.FindControl("lblAmount"), Label)
                        lblRemarks = DirectCast(gvr.FindControl("lblRemarks"), Label)

                        objFAI.FAID_FAI_ID = objFAI.FAI_ID
                        objFAI.FAID_AMOUNT = IIf(IsNumeric(lblAmount.Text.Trim), Convert.ToDecimal(lblAmount.Text.Trim), 0)
                        objFAI.FAID_FEE_ID = lblFEE_ID.Text
                        objFAI.FAID_REMARKS = lblRemarks.Text
                        objFAI.FAID_DRCR = "DR"
                        objFAI.FRS_FSP_ID = 0
                        retValD = objFAI.SaveAdjustment_Detail(objCon, TRANS)
                        'retValD = objFAI.F_SAVEFEEADJREQUEST_S(objCon, TRANS)
                        'DirectCast(ViewState("gvFeeDetails"), DataTable).Rows(gvr.RowIndex)("FRS_ID") = objFAI.FRS_ID
                        'DirectCast(ViewState("gvFeeDetails"), DataTable).Rows(gvr.RowIndex)("APPR_AMOUNT") = Val(txtAppAmount.Text)
                    Else
                        ValidateAndSave = retValD
                        Exit For
                    End If
                Next
                If retValD = "0" Then
                    For Each gvr As GridViewRow In gvSuppDocs.Rows
                        Dim hf_FASID As HiddenField = DirectCast(gvr.FindControl("hf_FASID"), HiddenField)
                        If Not hf_FASID Is Nothing Then
                            Dim QryInsert As String = "UPDATE FEES.FEE_ADJ_INTERUNIT_S SET FAS_DR_FAI_ID=" & objFAI.FAI_ID & " WHERE FAS_ID=" & hf_FASID.Value & ""
                            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, QryInsert)
                        End If
                    Next
                End If
                If retValD = "0" And objFAI.PROCESS = "APPROVE" Then 'Saving adjustment request approve
                    objFAI.APPROVE_STAGE = "INITIAL_APPROVE"
                    retValD = objFAI.ApproveAdjustment_IU(objCon, TRANS)
                End If
                ValidateAndSave = retValD
            Else
                ValidateAndSave = retValH
            End If

        Catch ex As Exception
            ValidateAndSave = "1"
        End Try
    End Function
    Private Sub ClearDetails()
        '  hf_FAI_ID.Value = "0"
        h_BSUID.Value = Session("sBSUID")
        h_STUD_ID.Value = ""
        txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
        txtHeaderRemarks.Text = ""
        txtOTU_STU_Detail.Text = ""
        ddlOtherBSU.SelectedIndex = -1
        ddlFeeType.ClearSelection()
        Session("sFEE_ADJ_INTERUNIT") = Nothing
        ViewState("gvFeeDetails") = Nothing

        BindFeeDetails()
        ClearSubDetails()
        GridBindAdjustments()
        h_STUD_ID.Value = ""
        txtStdNo.Text = ""
        txtStudentname.Text = ""
        ddlFeeType.DataSource = Nothing
        ddlFeeType.DataBind()
        h_CanEdit.Value = ""
        Me.gvSuppDocs.DataSource = Nothing
        Me.gvSuppDocs.DataBind()
        lblInfo.Text = ""

    End Sub

    Private Sub ClearSubDetails()
        txtDetAmount.Text = "0.00"
        txtDetRemarks.Text = ""
        ddlFeeType.SelectedIndex = -1
        btnDetAdd.Text = "Add"
        ddlFeeType_SelectedIndexChanged(Nothing, Nothing)
        lbDescription.Text = ""
        lblErrorFooter.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ClearDetails()
        hf_FAI_ID.Value = "0"
        DissableAllControls(False)
        gvFeeDetails.Columns(3).Visible = False
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        imgStudent.Enabled = True
    End Sub

    Protected Sub btnDetAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetAdd.Click
        lblErrorFooter.Text = ""
        If Not CheckDuplicate(ddlFeeType.SelectedValue) Then
            If IsNumeric(txtDetAmount.Text) AndAlso CDbl(txtDetAmount.Text) <> 0 And (IsNumeric(lblBalanceAmt.Text) AndAlso CDbl(lblBalanceAmt.Text) <> 0) Or IsProviderInitiated = True Then
                If ViewState("gvFeeDetails") Is Nothing Then
                    ViewState("gvFeeDetails") = FEE_ADJ_INTERUNIT.CreateGvFeeDetailSchema
                End If
                Dim dr As DataRow = DirectCast(ViewState("gvFeeDetails"), DataTable).NewRow
                dr("FEE_ID") = ddlFeeType.SelectedValue
                dr("FEE_TYPE") = ddlFeeType.SelectedItem.Text
                dr("DURATION") = 0
                dr("FEE_AMOUNT") = CDbl(txtDetAmount.Text)
                dr("FEE_REMARKS") = txtDetRemarks.Text.Trim
                DirectCast(ViewState("gvFeeDetails"), DataTable).Rows.Add(dr)
                DirectCast(ViewState("gvFeeDetails"), DataTable).AcceptChanges()
                BindFeeDetails()
                ClearSubDetails()
            Else
                lblErrorFooter.Text = "Invalid Amount!!!"
                Exit Sub
            End If
        Else
            lblErrorFooter.Text = "Fee Type already exists!!!"
        End If
    End Sub
    Private Function CheckDuplicate(ByVal FEEID As String) As Boolean
        CheckDuplicate = False
        If Not ViewState("gvFeeDetails") Is Nothing Then
            For Each dr As DataRow In DirectCast(ViewState("gvFeeDetails"), DataTable).Rows
                If dr("FEE_ID").ToString.Trim = FEEID Then
                    CheckDuplicate = True
                    Exit For
                End If
            Next
        End If
    End Function
    Private Sub BindFeeDetails()
        gvFeeDetails.DataSource = ViewState("gvFeeDetails")
        gvFeeDetails.DataBind()
    End Sub
    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        Dim lblAmt As New Label
        lblAmt = TryCast(sender.parent.FindControl("lblAmount"), Label)
        Dim lblRemarks As New Label
        lblRemarks = TryCast(sender.parent.FindControl("lblRemarks"), Label)

        Dim gvr As GridViewRow = TryCast(lblFEE_ID.NamingContainer, GridViewRow)
        If Not lblFEE_ID Is Nothing AndAlso Not lblAmt Is Nothing AndAlso Not lblRemarks Is Nothing Then
            ddlFeeType.SelectedValue = lblFEE_ID.Text
            txtDetAmount.Text = lblAmt.Text
            txtDetRemarks.Text = lblRemarks.Text
            DirectCast(ViewState("gvFeeDetails"), DataTable).Rows.RemoveAt(gvr.RowIndex)
            DirectCast(ViewState("gvFeeDetails"), DataTable).AcceptChanges()
            BindFeeDetails()
        End If
    End Sub
    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        Dim gvr As GridViewRow = TryCast(lblFEE_ID.NamingContainer, GridViewRow)
        DirectCast(ViewState("gvFeeDetails"), DataTable).Rows.RemoveAt(gvr.RowIndex)
        DirectCast(ViewState("gvFeeDetails"), DataTable).AcceptChanges()
        BindFeeDetails()
    End Sub

    Protected Sub btnSubCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubCancel.Click
        ClearSubDetails()
    End Sub

    Private Sub GridBindRefundableDetails()
        Dim stuTYPE As String = "S"
        If radEnq.Checked Then
            stuTYPE = "E"
        End If
        If h_STUD_ID.Value <> "" Then
            Dim str_sql As String = "SELECT SUM(CASE DRCR WHEN 'DR' THEN AMOUNT ELSE 0 END) PAID_AMT , " & _
                   " SUM(CASE DRCR WHEN 'CR' THEN AMOUNT ELSE 0 END) CHARGED_AMT ," & _
                   " DOCDATE, FEE_DESCR FROM FEES.[VW_AlLSTUDENTSDATA]  AS fn_STUDENTLEDGERALL_1 " & _
                   " WHERE STU_ID = " & h_STUD_ID.Value & " AND STU_TYPE = '" & stuTYPE & "' " & _
                   " GROUP BY DOCDATE, FEE_DESCR, STU_ID"
            gvFeePaidHistory.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_sql)
            gvFeePaidHistory.DataBind()
        End If

    End Sub

    Private Sub PopulateFeeType()
        Dim httabFPM_ID As New Hashtable
        If h_STUD_ID.Value.ToString <> "" Then
            ddlFeeType.DataSource = FEE_ADJ_INTERUNIT.PopulateFeeMaster(h_BSUID.Value, h_STUD_ID.Value, txtDate.Text, IIf(radStud.Checked, "S", "E"), 0)
        End If
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()
        ddlFeeType_SelectedIndexChanged(Nothing, Nothing)
        'Session("FPM_IDs") = httabFPM_ID
    End Sub

    Private Sub ClearEnteredDatas()
        ClearSubDetails()
        Session("sFEE_ADJ_INTERUNIT") = Nothing
        GridBindAdjustments()
    End Sub

    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        Dim vDate As String = txtDate.Text
        Try

        Catch
            'lblError.Text = "Invalid Date"
            usrMessageBar2.ShowNotification("Invalid Date", UserControls_usrMessageBar.WarningType.Danger)
        End Try
        ClearDetails()
        txtDate.Text = vDate
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        If h_CanEdit.Value = "False" Then
            'lblError.Text = "Auto Entry Can Not Modify..!"
            usrMessageBar2.ShowNotification("Auto Entry Can Not Modify..!", UserControls_usrMessageBar.WarningType.Danger)
            pnlCover.Enabled = False
            Exit Sub
        Else
            pnlCover.Enabled = True
        End If

        DissableAllControls(False)
        BindToDoxGrid()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub radStud_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStud.CheckedChanged
        ClearDetails()
    End Sub

    Protected Sub radEnq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnq.CheckedChanged
        ClearDetails()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If hf_FAI_ID.Value <> "0" Then
            h_print.Value = "print"
            PrintReceiptAdjustmentRequest(hf_FAI_ID.Value)
        End If
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnApprove.Click
        Try
            If IsValidApprovalDate() And ValidateApproval() Then
                Dim objConn = ConnectionManger.GetOASIS_FEESConnection
                Dim sTrans As SqlTransaction = objConn.BeginTransaction
                Dim retVal As Integer = 1000
                retVal = ValidateAndSave(objConn, sTrans)
                If retVal <> "0" Then
                    sTrans.Rollback()
                    'If lblError.Text = "" Then
                    '    lblError.Text = UtilityObj.getErrorMessage(retVal)
                    'End If
                    usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
                Else
                    sTrans.Commit()
                    'lblError.Text = "Adjustment request has been Approved"
                    usrMessageBar2.ShowNotification("Adjustment request has been Approved", UserControls_usrMessageBar.WarningType.Success)
                    ClearDetails()
                    If chkPrint.Checked Then
                        h_print.Value = "print"
                        PrintReceiptAdjustmentRequest(hf_FAI_ID.Value)
                    End If
                    btnApprove.Visible = False
                    btnReject.Visible = False
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), 1, ViewState("datamode"))
                    'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
                End If
            End If
        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar2.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Private Function IsValidApprovalDate() As Boolean
        If CDate(Format(DateTime.Now, OASISConstants.DataBaseDateFormat)) < CDate(txtDate.Text) Then
            'lblError.Text = "Approval Date should not be less than Requested Date"
            usrMessageBar2.ShowNotification("Approval Date should not be less than Requested Date", UserControls_usrMessageBar.WarningType.Danger)
            Return False
        End If
        Return True
    End Function

    Private Function ValidateApproval() As Boolean
        ValidateApproval = True
        Dim lblFEE_ID As Label, lblAmount As Label
        For Each gvr As GridViewRow In gvFeeDetails.Rows
            lblFEE_ID = DirectCast(gvr.FindControl("lblFEE_ID"), Label)
            lblAmount = DirectCast(gvr.FindControl("lblAmount"), Label)
            If (Not lblFEE_ID Is Nothing) AndAlso (Not lblAmount Is Nothing) Then
                If CDec(lblAmount.Text) < 0 Then
                    'lblError.Text = "Invalid Amount!!!"
                    usrMessageBar2.ShowNotification("Invalid Amount!!!", UserControls_usrMessageBar.WarningType.Danger)
                    ValidateApproval = False
                    Exit Function
                End If
                If CDec(lblAmount.Text) > CDec(lblAmount.Text) Then
                    'lblError.Text = "Approved Amount should not be more than Requested.."
                    usrMessageBar2.ShowNotification("Approved Amount should not be more than Requested..", UserControls_usrMessageBar.WarningType.Danger)
                    ValidateApproval = False
                    Exit Function
                End If
            End If
        Next
    End Function

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReject.Click
        Try
            ViewState("datamode") = "reject"
            Dim objConn = ConnectionManger.GetOASIS_FEESConnection
            Dim sTrans As SqlTransaction = objConn.BeginTransaction
            Dim retVal As Integer = 1000
            retVal = ValidateAndSave(objConn, sTrans)
            If retVal <> "0" Then
                sTrans.Rollback()
                'If lblError.Text = "" Then
                '    lblError.Text = UtilityObj.getErrorMessage(retVal)
                'End If
                usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                sTrans.Commit()
                'lblError.Text = "Adjustment request has been rejected"
                usrMessageBar2.ShowNotification("Adjustment request has been rejected", UserControls_usrMessageBar.WarningType.Success)
                ClearDetails()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), 1, ViewState("datamode"))
                'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub ddlFeeType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFeeType.SelectedIndexChanged
        Dim stud_typ As String = String.Empty
        lblBalanceAmt.Text = 0
        If radEnq.Checked Then
            stud_typ = "E"
        ElseIf radStud.Checked Then
            stud_typ = "S"
        End If
        If ddlFeeType.Items Is Nothing OrElse ddlFeeType.Items.Count <= 0 Then Exit Sub
        If h_STUD_ID.Value <> "" Then
            Dim dt = FEE_ADJ_INTERUNIT.PopulateFeeMaster(h_BSUID.Value, h_STUD_ID.Value, txtDate.Text, stud_typ, ddlFeeType.SelectedValue)
            If dt.Rows.Count > 0 Then
                lblBalanceAmt.Text = IIf(Not dt Is Nothing AndAlso dt.Rows.Count > 0, dt.Rows(0)("BALANCE"), "0.00")
            End If
        End If
    End Sub
    Protected Sub txtStudentname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'GetLessonDetails()
        ClearEnteredDatas()
        SetStudentDetails(ddlStudentBSU.SelectedValue, IIf(radEnq.Checked, "E", "S"), h_STUD_ID.Value)
        PopulateFeeType()
        GridBindRefundableDetails()
    End Sub
    Protected Sub PrintReceiptAdjustmentRequest(ByVal vFAI_ID As Integer)
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim cmd As New SqlCommand("[FEES].[GET_FEE_ADJUSTMENTREQUEST_IU]")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpFAI_ID As New SqlParameter("@FAI_ID", SqlDbType.Int)
        sqlpFAI_ID.Value = vFAI_ID
        cmd.Parameters.Add(sqlpFAI_ID)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("RPT_CAPTION") = "FEE ADJUSTMENT REQUEST INTER UNIT"
        params("UserName") = Session("sUsr_name")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../fees/Reports/RPT/rptFEEAdjustmentRequest_IU.rpt"

        Session("ReportSource") = repSource
        h_print.Value = "print"
    End Sub

    Protected Sub chkIsNonSchool_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIsNonSchool.CheckedChanged
        FEE_ADJ_INTERUNIT.PopulateBSU(ddlOtherBSU, Session("sBsuid"), chkIsNonSchool.Checked)
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        ClearEnteredDatas()
        SetStudentDetails(ddlStudentBSU.SelectedValue, IIf(radEnq.Checked, "E", "S"), h_STUD_ID.Value)
        PopulateFeeType()
        GridBindRefundableDetails()
    End Sub

    Protected Sub ddlStudentBSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStudentBSU.SelectedIndexChanged
        ClearDetails()
        FEE_ADJ_INTERUNIT.PopulateACDYear(ddlAcademicYear, ddlStudentBSU.SelectedValue)

    End Sub

    Protected Sub txtStdNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStdNo.TextChanged
        Try
            Dim str_data, str_sql As String
            txtStdNo.Text = txtStdNo.Text.Trim
            Dim iStdnolength As Integer = txtStdNo.Text.Length
            If radStud.Checked Then
                If iStdnolength < 9 Then
                    If txtStdNo.Text.Trim.Length < 8 Then
                        For i As Integer = iStdnolength + 1 To 8
                            txtStdNo.Text = "0" & txtStdNo.Text
                        Next
                    End If
                    txtStdNo.Text = ddlStudentBSU.SelectedValue & txtStdNo.Text
                End If
                str_sql = "SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM VW_OSO_STUDENT_M" _
                 & " WHERE     (STU_bActive = 1) AND (STU_BSU_ID = '" & ddlStudentBSU.SelectedValue & "') AND  STU_NO='" & txtStdNo.Text & "'"
                str_data = Mainclass.getDataValue(str_sql, "OASIS_FEESConnectionString")
            Else
                str_sql = "SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM FEES.vw_OSO_ENQUIRY_COMP" _
                & " WHERE     (STU_BSU_ID = '" & ddlStudentBSU.SelectedValue & "') AND  STU_NO='" & txtStdNo.Text & "'"

                str_data = Mainclass.getDataValue(str_sql, "OASIS_FEESConnectionString")
            End If
            If str_data <> "--" Then
                ClearEnteredDatas()
                h_STUD_ID.Value = str_data.Split("|")(0)
                SetStudentDetails(ddlStudentBSU.SelectedValue, IIf(radEnq.Checked, "E", "S"), h_STUD_ID.Value)
                PopulateFeeType()
                GridBindRefundableDetails()
            Else
                ClearEnteredDatas()
                h_STUD_ID.Value = "0"
                txtStudentname.Text = ""
                'lblError.Text = "Student # Entered is not valid  !!!"
                usrMessageBar2.ShowNotification("Student # Entered is not valid  !!!", UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If UploadDocPhoto.FileName <> "" Then
            Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Fees\"
            Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName
            Dim strFilepath As String = str_img & str_tempfilename

            Try
                UploadDocPhoto.PostedFile.SaveAs(strFilepath)
            Catch ex As Exception
                Exit Sub
            End Try
            'If hdnAssetImagePath.Value <> "" Then
            If Not IO.File.Exists(strFilepath) Then
                'lblError.Text = "Invalid FilePath!!!!"
                usrMessageBar2.ShowNotification("Invalid FilePath!!!!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            Else
                Dim ContentType As String = getContentType(strFilepath)
                Dim FileExt As String = str_tempfilename.Substring(str_tempfilename.Length - 4)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 5)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 6)
                Try
                    Dim RandVal As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CAST(RAND() * 1000000 AS INT) AS RANDVAL")
                    Dim NewFileName As String = RandVal & "_" & Session("sBsuid") & "_DR" & FileExt
                    IO.File.Move(strFilepath, str_img & NewFileName)
                    Dim retVal As Integer = 1000
                    Dim conn = ConnectionManger.GetOASIS_FEESConnection
                    Dim trans As SqlTransaction = conn.BeginTransaction()
                    Dim objFAI As New FEE_ADJ_INTERUNIT
                    objFAI.FAS_ID = 0
                    objFAI.FAS_CONTENT_TYPE = ContentType
                    objFAI.FAS_DR_FAI_ID = IIf(Val(Me.hf_FAI_ID.Value) = -1, 0, Val(Me.hf_FAI_ID.Value))
                    objFAI.FAS_ORG_FILENAME = UploadDocPhoto.FileName
                    objFAI.FAS_USER = Session("sUsr_name")
                    objFAI.FAS_FILENAME = NewFileName
                    objFAI.FAS_bShared = chkShared.Checked
                    retVal = objFAI.SAVE_ADJ_INTERUNIT_S(conn, trans)
                    If retVal = "0" Then
                        trans.Commit()
                        If ViewState("gvSuppDocs") Is Nothing Then
                            LoadDoxGrid()
                        End If
                        If Not ViewState("gvSuppDocs") Is Nothing And Val(Me.hf_FAI_ID.Value) <= 0 Then
                            Dim dr As DataRow = TryCast(ViewState("gvSuppDocs"), DataTable).NewRow
                            dr("FAS_ID") = objFAI.FAS_ID
                            dr("FAS_CONTENT_TYPE") = objFAI.FAS_CONTENT_TYPE
                            dr("FAS_ORG_FILENAME") = objFAI.FAS_ORG_FILENAME
                            dr("FAS_FILENAME") = objFAI.FAS_FILENAME
                            dr("FAS_bShared") = objFAI.FAS_bShared
                            dr("FAS_USER") = objFAI.FAS_USER
                            TryCast(ViewState("gvSuppDocs"), DataTable).Rows.Add(dr)
                            TryCast(ViewState("gvSuppDocs"), DataTable).AcceptChanges()
                            BindToDoxGrid()
                        ElseIf Val(Me.hf_FAI_ID.Value) > 0 Then
                            LoadDoxGrid()
                        End If

                    Else
                        trans.Rollback()
                        IO.File.Delete(str_img & NewFileName)
                    End If
                Catch ex As Exception

                End Try
            End If
        ElseIf Not ViewState("imgAsset") Is Nothing Then

        Else
            ViewState("imgAsset") = Nothing
        End If
    End Sub
    Private Function getContentType(ByVal FilePath As String) As String
        Dim filename As String = Path.GetFileName(FilePath)
        Dim ext As String = Path.GetExtension(filename)
        Select Case ext
            Case ".doc"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".xlsx"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                getContentType = "image/jpg"
                Exit Select
            Case ".png"
                getContentType = "image/png"
                Exit Select
            Case ".gif"
                getContentType = "image/gif"
                Exit Select
            Case ".pdf"
                getContentType = "application/pdf"
                Exit Select
            Case Else
                getContentType = "text/html"
        End Select
    End Function
    Private Sub LoadDoxGrid()
        Try
            Dim qry As String = "SELECT FAS_ID,FAS_CONTENT_TYPE,FAS_DR_FAI_ID,FAS_ORG_FILENAME,FAS_LOG_DATE,FAS_USER,FAS_FILENAME,ISNULL(FAS_bShared,0)FAS_bShared FROM FEES.FEE_ADJ_INTERUNIT_S WHERE ISNULL(FAS_CR_FAI_ID,0)=0 AND ISNULL(FAS_DR_FAI_ID,1)=" & Val(Me.hf_FAI_ID.Value) & ""
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry)
            'If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            ViewState("gvSuppDocs") = ds.Tables(0)
            BindToDoxGrid()
            'End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindToDoxGrid()
        Me.gvSuppDocs.DataSource = ViewState("gvSuppDocs")
        Me.gvSuppDocs.DataBind()
    End Sub

    Protected Sub gvSuppDocs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSuppDocs.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hf_ContentType As HiddenField = DirectCast(e.Row.FindControl("hf_ContentType"), HiddenField)
            Dim hf_filename As HiddenField = DirectCast(e.Row.FindControl("hf_filename"), HiddenField)
            Dim lnkFileName As LinkButton = DirectCast(e.Row.FindControl("lnkFileName"), LinkButton)
            lnkFileName.Attributes.Add("onClick", " showDocument('" & (hf_ContentType.Value) & "','" & (hf_filename.Value) & "'); return false;")
            Dim chkSharedOB As CheckBox = DirectCast(e.Row.FindControl("chkSharedOB"), CheckBox)
            chkSharedOB.Enabled = gvSuppDocs.Columns(4).Visible
        End If
    End Sub
    Protected Sub lnkDeleteSD_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnkDelete As New LinkButton
        lnkDelete = TryCast(sender.parent.FindControl("lnkDelete"), LinkButton)
        Dim hf_FASID As HiddenField = DirectCast(sender.parent.FindControl("hf_FASID"), HiddenField)
        Dim hf_filename As HiddenField = DirectCast(sender.parent.FindControl("hf_filename"), HiddenField)

        Dim gvr As GridViewRow = TryCast(hf_FASID.NamingContainer, GridViewRow)
        If Not hf_FASID Is Nothing Then
            Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "Fees\"
            Dim strFilepath As String = str_img & hf_filename.Value
            If IO.File.Exists(strFilepath) Then
                Try
                    IO.File.Delete(strFilepath)
                Catch ex As Exception

                End Try
            End If
            Dim QryInsert As String = "DELETE FEES.FEE_ADJ_INTERUNIT_S WHERE FAS_ID=" & hf_FASID.Value & ""
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, QryInsert)
            TryCast(ViewState("gvSuppDocs"), DataTable).Rows(gvr.RowIndex).Delete()
            TryCast(ViewState("gvSuppDocs"), DataTable).AcceptChanges()
            BindToDoxGrid()
        End If
    End Sub

    Protected Sub chkSharedOB_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSharedOB As CheckBox = DirectCast(sender.parent.Findcontrol("chkSharedOB"), CheckBox)
        Dim hf_FASID As HiddenField = DirectCast(sender.parent.FindControl("hf_FASID"), HiddenField)
        Dim gvr As GridViewRow = TryCast(hf_FASID.NamingContainer, GridViewRow)
        If Not chkSharedOB Is Nothing AndAlso Not hf_FASID Is Nothing Then
            Dim bShared As Int16 = IIf(chkSharedOB.Checked, 1, 0)
            Dim QryInsert As String = "UPDATE FEES.FEE_ADJ_INTERUNIT_S SET FAS_bShared=" & bShared & " WHERE FAS_ID=" & hf_FASID.Value & ""
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, QryInsert)
            'dr("FAS_bShared") = objFAI.FAS_bShared
            'dr("FAS_USER") = objFAI.FAS_USER
            TryCast(ViewState("gvSuppDocs"), DataTable).Rows(gvr.RowIndex)("FAS_bShared") = bShared
            TryCast(ViewState("gvSuppDocs"), DataTable).AcceptChanges()
            BindToDoxGrid()
        End If
    End Sub

    Protected Sub btnRecall_Click(sender As Object, e As EventArgs) Handles btnRecall.Click
        ViewState("datamode") = "recall"
        Dim objConn = ConnectionManger.GetOASIS_FEESConnection
        Dim sTrans As SqlTransaction = objConn.BeginTransaction
        Dim retVal As Integer = 1000
        retVal = ValidateAndSave(objConn, sTrans)
        If retVal <> "0" Then
            sTrans.Rollback()
            'If lblError.Text = "" Then
            '    lblError.Text = UtilityObj.getErrorMessage(retVal)
            'End If
            usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
        Else
            sTrans.Commit()
            ''sTrans.Rollback()
            'lblError.Text = "Adjustment request has been recalled"
            usrMessageBar2.ShowNotification("Adjustment request has been recalled", UserControls_usrMessageBar.WarningType.Success)
            'ClearDetails()
            ViewState("datamode") = "approve"
            btnRecall.Visible = False
            If chkPrint.Checked Then
                h_print.Value = "print"
                PrintReceiptAdjustmentRequest(hf_FAI_ID.Value)
            End If
            ViewState("status") = "N"
            If (ViewState("status") = "N") Then
                btnApprove.Visible = True
                btnReject.Visible = True
            End If
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), "2", ViewState("datamode"))
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
        End If
    End Sub
End Class
