Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeOnlineReconcillationBB
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainObj As Mainclass = New Mainclass()
    Dim TAmnt As Double = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnFind)
        If Page.IsPostBack = False Then
            doClear()
            initialize_components()
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Page.Title = OASISConstants.Gemstitle
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "F300201") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            UtilityObj.beforeLoopingControls(Me.Page)
        End If
        gvExcel.Attributes.Add("bordercolor", "#1b80b6")
        btnSave.Visible = True
    End Sub

    Public Sub getdata(ByVal filePath As String, ByVal Fname As String)
        Try
            Dim Query As String = ""
            'Query = " SELECT NO,DATE,STUDENTID,CASH,FEECODE,PAID,CHEQUE,CHKNO,CHKDATE,BANK  FROM  " & filePath & Fname
            Query = " SELECT * FROM  " & filePath & Fname
            Dim _table As DataTable
            _table = Mainclass.FecthFromDBFodbc(Query, filePath)
            If _table.Rows.Count.Equals(0) Then
                Throw New Exception("Could not process of you are request.date not match with excel..!")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar2.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Public Sub getdataExcel(ByVal filePath As String)
        Try
            Dim excelQuery As String = ""
            excelQuery = " SELECT * FROM  [TableName]"
            'excelQuery = "SELECT * from [Sheet1$]"
            Dim _table As DataTable
            _table = Mainclass.FetchFromExcelIntoDataTable(filePath, 1, 1, 23)
            '_table = Mainclass.FetchFromExcel(excelQuery, filePath)
            If _table.Rows.Count.Equals(0) Then
                Throw New Exception("Could not process of you are request.Invalid data in excel..!")
            End If
            _table.Columns(5).ColumnName = "FOCID"
            _table.Columns(13).ColumnName = "STATUS"

            Dim dtView As New DataView(_table)
            dtView.RowFilter = "STATUS = '0'"
            Session("ExcelTable") = Nothing
            Session("ExcelTable") = dtView.ToTable()
            getRefID(Session("ExcelTable"))
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar2.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Sub initialize_components()
        Session("ExcelTable") = New DataTable
    End Sub

    Private Sub doClear()
        gvExcel.DataSource = Nothing
        gvExcel.DataBind() 
        Session("ExcelTable") = Nothing
        HidUpload.Value = ""
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            doInsert()
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar2.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Private Sub UpLoadDBF()
        If uploadFile.HasFile Then
            Dim FName As String = "Online" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString()
            FName += uploadFile.FileName.ToString().Substring(uploadFile.FileName.Length - 4)
            Dim filePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            If Not Directory.Exists(filePath & "\OnlineExcel") Then
                Directory.CreateDirectory(filePath & "\OnlineExcel")
            End If
            Dim FolderPath As String = filePath & "\OnlineExcel\"
            filePath = filePath & "\OnlineExcel\" & FName.Replace(":", "@")

            If uploadFile.HasFile Then
                If File.Exists(filePath) Then
                    File.Delete(filePath)
                End If
                uploadFile.SaveAs(filePath)

                Try
                    getdataExcel(filePath)
                Catch ex As Exception
                    Errorlog(ex.Message)
                    'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
                    usrMessageBar2.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
                End Try
            End If
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        If Not uploadFile.HasFile Then
            'lblError.Text = "Select Particular File...!"
            usrMessageBar2.ShowNotification("Select Particular File...!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        doClear()
        If Not (uploadFile.FileName.EndsWith("xls", StringComparison.OrdinalIgnoreCase) Or uploadFile.FileName.EndsWith("xlsx", StringComparison.OrdinalIgnoreCase) Or uploadFile.FileName.EndsWith("csv", StringComparison.OrdinalIgnoreCase)) Then
            'lblError.Text = "Invalid file type.. Only Excel files are alowed.!"
            usrMessageBar2.ShowNotification("Invalid file type.. Only Excel files are alowed.!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        UpLoadDBF()
    End Sub

    Sub getDetails(ByVal FocId As String)
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
        pParms(0).Value = Session("sBsuID")
        pParms(1) = New SqlClient.SqlParameter("@FOC_IDs", SqlDbType.VarChar)
        pParms(1).Value = FocId
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
          CommandType.StoredProcedure, "FEES.OnlineFee_Review_Transport", pParms)
        gvExcel.DataSource = dsData.Tables(0)
        gvExcel.DataBind()
        If dsData.Tables(0).Rows.Count > 0 Then
            gvExcel.FooterRow.Cells(0).Text = dsData.Tables(0).Rows.Count.ToString()
            gvExcel.FooterRow.Cells(5).Text = TAmnt.ToString("##,#.00")
        End If
    End Sub

    Sub getRefID(ByVal idTable As DataTable)
        Dim x As Int32 = 0
        Dim FocId As String = ""
        For x = 0 To idTable.Rows.Count - 1
            If idTable.Rows(x)(5).ToString() <> "" Then
                If FocId = "" Then
                    FocId = idTable.Rows(x)(5).ToString()
                Else
                    FocId += "@" & idTable.Rows(x)(5).ToString()
                End If
            End If
        Next
        HidUpload.Value = FocId
        getDetails(FocId)
    End Sub

    Private Sub doInsert()
        Dim FctId As String = ""
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim _parameter As String(,) = New String(6, 1) {}
        Try
            For Each grow As GridViewRow In gvExcel.Rows

                Dim ChkSelect As HtmlInputCheckBox = CType(grow.FindControl("ChkSelect"), HtmlInputCheckBox)
                If ChkSelect IsNot Nothing And ChkSelect.Checked Then
                    Dim filterExp As String = "FOCID='" & ChkSelect.Value & "'"
                    Dim _dtRow() As DataRow = Session("ExcelTable").Select(filterExp)
                    Dim pParms(20) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.Int)
                    pParms(0).Value = ChkSelect.Value
                    pParms(1) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCECODE", SqlDbType.VarChar)
                    pParms(1).Value = _dtRow(0).Item(13).ToString()
                    pParms(2) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCEDESCR", SqlDbType.VarChar)
                    pParms(2).Value = _dtRow(0).Item(12).ToString()
                    pParms(3) = New SqlClient.SqlParameter("@FCO_VPC_MESSAGE", SqlDbType.VarChar)
                    pParms(3).Value = _dtRow(0).Item(12).ToString()
                    pParms(4) = New SqlClient.SqlParameter("@FCO_VPC_RECNO", SqlDbType.VarChar)
                    pParms(4).Value = _dtRow(0).Item(0).ToString()
                    pParms(5) = New SqlClient.SqlParameter("@FCO_VPC_TRANNO", SqlDbType.VarChar)
                    pParms(5).Value = _dtRow(0).Item(0).ToString()
                    pParms(6) = New SqlClient.SqlParameter("@FCO_VPC_ACQRESCODE", SqlDbType.VarChar)
                    pParms(6).Value = _dtRow(0).Item(13).ToString()
                    pParms(7) = New SqlClient.SqlParameter("@FCO_VPC_BANKAUTID", SqlDbType.VarChar)
                    pParms(7).Value = _dtRow(0).Item(14).ToString()
                    pParms(8) = New SqlClient.SqlParameter("@FCO_VPC_BATCHNO", SqlDbType.VarChar)
                    pParms(8).Value = _dtRow(0).Item(8).ToString()
                    pParms(9) = New SqlClient.SqlParameter("@FCO_VPC_CARDTYPE", SqlDbType.VarChar)
                    pParms(9).Value = "MC"
                    If _dtRow(0).Item(18).ToString().Trim() = "Visa" Then
                        pParms(9).Value = "VC"
                    End If
                    pParms(10) = New SqlClient.SqlParameter("@FCO_VPC_HASHCODE_RESULT", SqlDbType.VarChar)
                    pParms(10).Value = ""
                    pParms(11) = New SqlClient.SqlParameter("@FCO_VPC_ORDERINFO", SqlDbType.VarChar)
                    pParms(11).Value = _dtRow(0).Item(3).ToString()
                    pParms(12) = New SqlClient.SqlParameter("@FCO_VPC_AMOUNT", SqlDbType.Decimal)
                    pParms(12).Value = (Convert.ToDecimal(_dtRow(0).Item(10).ToString()) * 100).ToString()
                    pParms(13) = New SqlClient.SqlParameter("@FCO_VPC_MERCHANT_ID", SqlDbType.VarChar)
                    pParms(13).Value = _dtRow(0).Item(2).ToString()
                    pParms(14) = New SqlClient.SqlParameter("@FCO_VPC_COMMAD", SqlDbType.VarChar)
                    pParms(14).Value = ""
                    pParms(15) = New SqlClient.SqlParameter("@FCO_VPC_VERSION", SqlDbType.VarChar)
                    pParms(15).Value = ""
                    pParms(16) = New SqlClient.SqlParameter("@FCO_VPC_3DS_INFO", SqlDbType.VarChar)
                    pParms(16).Value = ""
                    pParms(17) = New SqlClient.SqlParameter("@RESPONSE", SqlDbType.VarChar, 50)
                    pParms(17).Direction = ParameterDirection.Output
                    pParms(18) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 50)
                    pParms(18).Direction = ParameterDirection.Output
                    pParms(19) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                    pParms(19).Direction = ParameterDirection.ReturnValue
                    Dim retval As Integer
                    retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[TRANSPORT].[F_SaveFEECOLLECTION_H_ONLINE_PAYMENT]", pParms)
                    If pParms(19).Value = "0" And pParms(18).Value <> "" Then
                        'lblError.Text = pParms(17).Value.ToString()
                        usrMessageBar2.ShowNotification(pParms(17).Value.ToString(), UserControls_usrMessageBar.WarningType.Success)
                        HidUpload.Value = HidUpload.Value.Replace(ChkSelect.Value & "@", "")
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, pParms(18).Value, "INSERT", Page.User.Identity.Name.ToString, Me.Page)
                    Else
                        'lblError.Text = pParms(17).Value.ToString()
                        usrMessageBar2.ShowNotification(pParms(17).Value.ToString(), UserControls_usrMessageBar.WarningType.Danger)
                        stTrans.Rollback()
                        objConn.Close()
                        grow.BackColor = Drawing.Color.BurlyWood
                        Exit Sub
                    End If
                End If
            Next
        Catch ex As Exception
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar2.ShowNotification(getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
            stTrans.Rollback()
            objConn.Close()
        End Try
        stTrans.Commit()
        If objConn.State = ConnectionState.Open Then
            objConn.Close()
        End If
        getDetails(HidUpload.Value)
    End Sub

    Protected Sub gvExcel_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            TAmnt += Convert.ToDouble(e.Row.Cells(5).Text)
        End If
    End Sub
End Class

