
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeTranspotReminderExclude
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            gvFEEReminder.Attributes.Add("bordercolor", "#1b80b6")
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ClientScript.RegisterStartupScript(Me.GetType(), _
                "script", "<script language='javascript'>  CheckOnPostback(); </script>")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_TRANSPORT_REMINDER_EXCLUDE Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                txtAsOnDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                ddlBusinessunit.DataBind()
                'BindAcademicYear(ddlBusinessunit.SelectedValue)
                Session("liUserList") = New ArrayList
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                btnSave.Visible = True
                btnAdd.Visible = False
                If ViewState("datamode") = "view" Then
                    Dim FRH_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FRH_ID").Replace(" ", "+"))
                    Dim vFEE_REM As FEEReminder = FEEReminder.GetHeaderDetails(FRH_ID)
                    If vFEE_REM Is Nothing Then
                        Exit Sub
                    End If
                    txtAsOnDate.Text = Format(vFEE_REM.FRH_ASONDATE, OASISConstants.DateFormat)
                    txtDate.Text = Format(vFEE_REM.FRH_DT, OASISConstants.DateFormat)
                    'Select Case vFEE_REM.FRH_Level
                    '    Case ReminderType.FIRST
                    '        radFirstRM.Checked = True
                    '    Case ReminderType.SECOND
                    '        radSecRM.Checked = True
                    '    Case ReminderType.THIRD
                    '        radThirdRM.Checked = True
                    'End Select
                    'lblAcademicYear.Text = vFEE_REM.FRH_ACY_DESCR
                    'GridBind(FRH_ID)
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
        GridBind()
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvFEEReminder.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvFEEReminder.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub ClearSubGrid()
        gvFEEReminder.DataSource = Nothing
        gvFEEReminder.DataBind()
    End Sub

    Private Sub GridBind()
        Dim IDs As String() = h_STUD_ID.Value.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_Sql As String
        Dim ds As DataSet = Nothing
        For i As Integer = 0 To IDs.Length - 1
            If IDs(i) = "" Then Continue For
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        If h_STUD_ID.Value <> "" Then
            str_Sql = "SELECT STU_NO, STU_ID, STU_NAME,GRD_DISPLAY,PARENT_NAME,PARENT_MOBILE FROM VW_OSO_STUDENT_M WHERE STU_ID IN (" + condition + ")"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        End If
        gvFEEReminder.DataSource = ds
        gvFEEReminder.DataBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If (Session("liUserList") Is Nothing) OrElse (Session("liUserList").Count <= 0) Then
            'lblError.Text = "Please Select atleast 1 Student..."
            'Exit Sub
        End If
        Dim conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
        Dim trans As SqlTransaction
        Dim stud_ids As Hashtable = GetSelectedStudentsList()
        trans = conn.BeginTransaction("FEE_REMINDER")
        Dim dtAsOndate As DateTime = CDate(txtAsOnDate.Text)
        Dim dtDate As DateTime = CDate(txtDate.Text)
        Dim vFEE_REMX As New FEEReminderExclude
        'vFEE_REM.FRH_ACD_ID = ddlAcademicYear.SelectedValue
        vFEE_REMX.FRE_FROMDT = dtAsOndate
        vFEE_REMX.FRE_TODT = dtDate

        vFEE_REMX.FRE_BSU_ID = Session("sBSUID")
        vFEE_REMX.FRE_STU_BSU_ID = ddlBusinessunit.SelectedValue
        Try
            Dim vFRH_ID As Integer
            Dim retVal As Integer = FEEReminderExclude.SaveFEEReminderExclude(stud_ids, vFEE_REMX, conn, trans)
            If retVal > 0 Then
                trans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(retVal)
            Else
                trans.Commit()
                'lblError.Text = "Data updated Successfully"
                usrMessageBar.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim str_KEY As String = "INSERT"
                If ViewState("datamode") <> "edit" Then
                    str_KEY = "EDIT"
                End If
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable("", vFRH_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                'If chkPrint.Checked Then
                '    PrintReminder(vFRH_ID)
                'End If
            End If
            Response.Redirect("feeTranspotReminderExcludeView.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            conn.Close()
        End Try

    End Sub

    Private Function GetSelectedStudentsList() As Hashtable
        Dim htList As New Hashtable
        Dim IDs As String() = h_STUD_ID.Value.Split("||")
        For i As Integer = 0 To IDs.Length - 1
            If IDs(i) <> "" Then htList(IDs(i)) = True
        Next
        Return htList
    End Function

    Private Sub ClearDetails()
        txtAsOnDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        txtDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
        'radThirdRM.Checked = True
        'lblError.Text = ""
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'ViewState("datamode") = "add"
        'ClearDetails()
        'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'BindAcademicYear(ddlBusinessunit.SelectedValue)
    End Sub

    Protected Sub gvFEEReminder_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

End Class
