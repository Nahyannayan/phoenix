<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeAdjustment_HeadtoHead.aspx.vb" Inherits="Fees_FeeAdjustment" Theme="General" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript">

        function GetStudent(type) {
            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 750px; ";
            sFeatures += "dialogHeight: 475px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var selACD_ID = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            var STUD_TYP;
            if (type == "1") {
                STUD_TYP = document.getElementById('<%=radEnqDR.ClientID %>').checked;
            }
            else {
                STUD_TYP = document.getElementById('<%=radEnqCR.ClientID %>').checked;
            }
            var url;
            if (STUD_TYP == true)
                url = "ShowStudent.aspx?TYPE=TCENQ&VAL=1&ACD_ID=" + selACD_ID;
            else
                url = "ShowStudent.aspx?TYPE=TC&VAL=1&ACD_ID=" + selACD_ID;
            document.getElementById('<%=hf_type.ClientID %>').value = type;
            //var url = "ShowStudent.aspx?TYPE=TC&VAL=" + selType + "&ACD_ID=" + selACD_ID;
            //result = window.showModalDialog(url, "", sFeatures)
            var oWnd = radopen(url, "pop_fee");
        <%--if (result != '' && result != undefined) {
            NameandCode = result.split('||');
            if (type == "1") {
                document.getElementById('<%=h_STUD_ID_DR.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStudNameDR.ClientID %>').value = NameandCode[1];
            }
            else {
                document.getElementById('<%=h_STUD_ID_CR.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStudNameCR.ClientID %>').value = NameandCode[1];
            }
            return false;
        }
        else {
            return false;
        }--%>
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(arg.NameandCode);
                if (document.getElementById('<%=hf_type.ClientID %>').value == "1") {
                    document.getElementById('<%=h_STUD_ID_DR.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtStudNameDR.ClientID %>').value = NameandCode[1];
                }
                else {
                    document.getElementById('<%=h_STUD_ID_CR.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtStudNameCR.ClientID %>').value = NameandCode[1];
                }
                <%--__doPostBack('<%= txtFeeCounter.ClientID%>', 'TextChanged');--%>
            }
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_fee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fee Adjustments(Between heads)"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label id="lblError" runat="server" EnableViewState="False" SkinID="LabelError"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="SUBERROR"></asp:ValidationSummary>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="MAINERROR"></asp:ValidationSummary>
                        </td>
                    </tr>
                </table>
                <table align="center" class="BlueTable" cellpadding="5" cellspacing="0" width="80%">
                    <tr class="subheader_img">
                        <td align="left" colspan="8" style="height: 19px"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="4"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Date  required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtDate">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:RadioButton ID="radStudDR" runat="server" AutoPostBack="true" Checked="True" GroupName="DR_STUD_TYPE"
                                Text="Student" />
                            <asp:RadioButton ID="radEnqDR" runat="server" AutoPostBack="true"
                                GroupName="DR_STUD_TYPE" Text="Enquiry" /></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student(DR)</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStudNameDR" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgProcess" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetStudent(1); return false;"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Student Name required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtStudNameDR">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Grade</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Fee Type (DR)</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlFeeTypeDR" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%">
                            <asp:Label ID="lblBalance" runat="server" Text="Balance Amount" CssClass="field-label"></asp:Label></td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblBalanceAmt" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:RadioButton ID="radStudCR" runat="server" AutoPostBack="true" Checked="True" GroupName="CR_STUD_TYPE"
                                Text="Student" /><asp:RadioButton ID="radEnqCR" runat="server" AutoPostBack="true"
                                    GroupName="CR_STUD_TYPE" Text="Enquiry" /></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student(CR)</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtStudNameCR" runat="server"></asp:TextBox>
                            <asp:ImageButton
                                ID="ImgCRStud" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetStudent(0);" /><asp:RequiredFieldValidator
                                    ID="RequiredFieldValidator3" runat="server" co="" ControlToValidate="txtStudNameCR"
                                    ErrorMessage="Student Name required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Fee Type(CR)</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlFEETYPECR" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Amount</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDetAmount" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Amount required"
                                ValidationGroup="SUBERROR" ControlToValidate="txtDetAmount">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtDetRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Detailed Remarks required"
                                ValidationGroup="SUBERROR" ControlToValidate="txtDetRemarks">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" OnClick="btnAdd_Click" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" OnClick="btnCancel_Click" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_STUD_ID_DR" runat="server" />
                <asp:HiddenField ID="h_STUD_ID_CR" runat="server" />
                <asp:HiddenField ID="h_GRD_ID_DR" runat="server" />
                <asp:HiddenField ID="h_GRD_ID_CR" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="hf_type" runat="server" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>

