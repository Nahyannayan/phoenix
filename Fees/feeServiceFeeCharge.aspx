<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeServiceFeeCharge.aspx.vb" Inherits="Fees_feeServiceFeeCharge" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Service Charge
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>--%>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Select Service</span></td>
                        <td align="left" width="30%">&nbsp;<asp:DropDownList ID="ddService" runat="server" DataSourceID="ODSSERVICES_SYS"
                            DataTextField="SVC_DESCRIPTION" DataValueField="SVB_ID" AutoPostBack="True" Width="212px">
                        </asp:DropDownList>
                        </td>
                        <td align="left" width="20%">
                            <span class="field-label">From Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />&nbsp;
                        </td>
                    </tr>
                    <tr id="tr_Transport" runat="server">
                        <td align="left">
                            <span class="field-label">Pick Up Points</span></td>
                        <td align="left" colspan="3">&nbsp;<asp:DropDownList ID="ddPickPoint" runat="server" DataSourceID="obdTPTPICKUPPOINTS_M"
                            DataTextField="PNT_DESCRIPTION" DataValueField="PNT_ID" Width="228px">
                        </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Rate</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtAmount" runat="server" AutoCompleteType="Disabled"></asp:TextBox>&nbsp;
                 <asp:Label ID="lblChargeSchedule" runat="server" CssClass="error"></asp:Label>
                        </td>
                        <td align="left">
                            <span class="field-label">Grade</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddGrade" runat="server" DataSourceID="odsGrade"
                                DataTextField="GRD_DISPLAY" DataValueField="GRD_ID">
                            </asp:DropDownList>&nbsp;
             <asp:Button ID="btnAdddetails" runat="server" CssClass="button" Text="Add" /></td>
                    </tr>
                    <tr runat="server" id="tr_Deatailhead">
                        <td colspan="4" align="left" class="title-bg">Service Details </td>
                    </tr>

                    <tr runat="server" id="tr_Deatails">
                        <td colspan="4" align="center">
                            <asp:GridView ID="gvAttendance" runat="server" CssClass="table table-bordered table-row" Width="100%" AutoGenerateColumns="False" EmptyDataText="No Details Added">
                                <Columns>
                                    <asp:BoundField DataField="Fdate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="From Date"
                                        HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SVB_DESCR" HeaderText="Service">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Amount" HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PNT_DESCR" HeaderText="Pick up point">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:CommandField ShowDeleteButton="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>
                                    <asp:TemplateField Visible="False">
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                                                Text="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                                Text="Cancel"></asp:LinkButton>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                                Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <HeaderStyle CssClass="gridheader_new" />

                            </asp:GridView>
                            &nbsp; &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>

                    </tr>
                </table>
                <asp:ObjectDataSource ID="odsGrade" runat="server" SelectMethod="GetGRADE_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter Name="BSU_ID" SessionField="sbsuid" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="ACD_ID" SessionField="Current_ACD_ID"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ODSSERVICES_SYS" runat="server" SelectMethod="GetSERVICES_SYS"
                    TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuId" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="obdTPTPICKUPPOINTS_M" runat="server"
                    SelectMethod="GetTPTPICKUPPOINTS_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                    PopupButtonID="txtFrom" TargetControlID="txtFrom" CssClass="MyCalendar" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                    PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>

