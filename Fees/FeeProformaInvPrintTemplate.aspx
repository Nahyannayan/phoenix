<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FeeProformaInvPrintTemplate.aspx.vb" Inherits="fees_FeeProformaInvPrintTemplate"
    Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %> 
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
  function CheckForPrint() {
      if (document.getElementById('<%= h_print.ClientID %>').value != '') {
          document.getElementById('<%= h_print.ClientID %>').value = '';
          showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
      }
  }
    );
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="ProformaInvoice Template"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                <table align="center" cellpadding="5" cellspacing="0" width="100%" style="border-collapse: collapse;">
                    <%-- <tr class="subheader_img">
            <td align="left" colspan="2">
                <asp:Label ID="lblHeader" runat="server" Text="ProformaInvoice Template"></asp:Label></td>
        </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Version.No</span></td>
                        <td align="left">
                            <asp:Label ID="lblVsnNo" runat="server"  CssClass="field-value" Width="60%"></asp:Label>
                        </td>
                         </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Payment Mode</span></td>
                        <td align="left" >
                            <telerik:RadEditor ID="txtPayMode" runat="server" StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                                StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span"
                                ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                            </telerik:RadEditor>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Bank Transfer detail</span></td>
                        <td align="left" >
                            <telerik:RadEditor ID="txtBankTfrDetail" runat="server"
                                StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                                StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span" ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                            </telerik:RadEditor>
                        </td>
                         </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Promotions</span></td>
                        <td align="left" >
                            <telerik:RadEditor ID="txtPromotion" runat="server"
                                StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                                StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span" ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                            </telerik:RadEditor>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Fee Ammendments</span></td>
                        <td align="left" >
                            <telerik:RadEditor ID="txtFeeDescr" runat="server" StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                                StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span"
                                ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                            </telerik:RadEditor>
                        </td>
                         </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Footer</span></td>
                        <td align="left" >
                            <telerik:RadEditor ID="txtFooter" runat="server"  StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                                StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span"
                                ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                            </telerik:RadEditor>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Show Refer A Friend</span></td>
                        <td align="left" >
                            <asp:CheckBox ID="chkRAF" runat="server" />
                            <asp:Image ID="imgRAF" runat="server" ImageUrl="~/Images/ReferAFriend.png" />
                        </td>
                         </tr>
                    <tr>
                        <td align="left"><span class="field-label">ThankYou Message</span></td>
                        <td align="left">
                            <telerik:RadEditor ID="txtThankU" runat="server" StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                                StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span"
                                ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                            </telerik:RadEditor>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" OnClick="btnEdit_Click" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" UseSubmitBehavior="false" />
                            <%-- <asp:Button id="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" />--%>
                            <asp:Button ID="btnPreview" runat="server" CausesValidation="False" CssClass="button"
                                Text="Preview" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" OnClick="btnCancel_Click" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_print" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
