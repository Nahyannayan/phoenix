<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeTransportAdjustment.aspx.vb" Inherits="FEES_FeeTransportAdjustment" Theme="General" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript">
        Sys.Application.add_load(
        function CheckForPrint() {
            if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                document.getElementById('<%= h_print.ClientID %>').value = '';
                //showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                return ShowWindowWithClose('../Reports/ASPX Report/RptViewerModal.aspx', 'search', '55%', '85%')
                return false;
            }
        }
    );

        function FillDetailRemarks() {
            var HeaderRemarks;
            HeaderRemarks = document.getElementById('<%=txtHeaderRemarks.ClientID %>').value;
            if (HeaderRemarks != '') {
                document.getElementById('<%=txtDetRemarks.ClientID %>').value = HeaderRemarks;
            }
            return false;
        }
        function GetStudent() {
            var sFeatures, url;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (document.getElementById('<%= radStud.ClientID %>').checked == true)
                url = "ShowStudentTransport.aspx?type=STUD&bsu=" + document.getElementById('<%= ddBusinessunit.ClientID %>').value;
            else
                url = "ShowStudentTransport.aspx?type=ENQ_COMP&COMP_ID=-1&bsu=" + document.getElementById('<%= ddBusinessunit.ClientID %>').value;
            //result = window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_fee");
            <%--if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById('<%= h_STUD_ID.ClientID %>').value = NameandCode[0];
            document.getElementById('<%= txtStudNo.ClientID %>').value = NameandCode[2];
            document.getElementById('<%= txtStudName.ClientID %>').value = NameandCode[1];

            return true;--%>
        }
        function GetService() {
            var sFeatures, url;
            sFeatures = "dialogWidth:600px; ";
            sFeatures += "dialogHeight: 500px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var SSV_ID;
            var result;
            var STUDID = document.getElementById('<%= h_STUD_ID.ClientID %>').value

            url = "ShowStudentTransportServicedetails.aspx?bsu=" + document.getElementById('<%= ddBusinessunit.ClientID %>').value + "&STUID=" + STUDID + "&ACDID=" + document.getElementById('<%=ddlAcademicYear.ClientID %>').value + "";

            //result = window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_fee2");
            <%--if (result == '' || result == undefined) {
                return false;
            }
            SSV_ID = result.split('||')
            // alert(SSV_ID)
            document.getElementById('<%= HFssv_id.ClientID %>').value = SSV_ID[0];
            document.getElementById('<%= txtssv_id.ClientID %>').value = SSV_ID[1];
            var roundtrip = SSV_ID[2];

            if (roundtrip == 1) {
                document.getElementById('<%= CBoneway.ClientID %>').checked = true;
            }
            return true;--%>
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(arg.NameandCode);
                document.getElementById('<%= h_STUD_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= txtStudNo.ClientID %>').value = NameandCode[2];
                document.getElementById('<%= txtStudName.ClientID %>').value = NameandCode[1];
                <%--__doPostBack('<%= txtFeeCounter.ClientID%>', 'TextChanged');--%>
            }
        }
        function OnClientClose2(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(arg.NameandCode);
                document.getElementById('<%= HFssv_id.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= txtssv_id.ClientID %>').value = NameandCode[1];
                var roundtrip = NameandCode[2];

                if (roundtrip == 1) {
                    document.getElementById('<%= CBoneway.ClientID %>').checked = true;
                }
                <%--__doPostBack('<%= txtFeeCounter.ClientID%>', 'TextChanged');--%>
            }
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_fee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_fee2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fee Adjustments"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" SkinID="Error"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="SUBERROR"></asp:ValidationSummary>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="MAINERROR"></asp:ValidationSummary>
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="0" cellspacing="0" width="100%" border="0">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddBusinessunit" runat="server" AutoPostBack="True"
                                DataSourceID="odsSERVICES_BSU_M" DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"
                                TabIndex="5" OnSelectedIndexChanged="ddBusinessunit_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server" Width="107px"></asp:TextBox>
                            <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="4"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Date  required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtDate">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:RadioButton ID="radStud" runat="server" AutoPostBack="true" Checked="True" GroupName="ENQ_STUD"
                                Text="Student" />
                            <asp:RadioButton ID="radEnq" runat="server" AutoPostBack="true"
                                GroupName="ENQ_STUD" Text="Enquiry" />
                            <asp:RadioButton ID="radNegtive" runat="server" Text="-Ve" GroupName="ADJTYPE" Visible="False" />
                            <asp:RadioButton ID="radPositive" runat="server" Text="+Ve" Checked="True" GroupName="ADJTYPE" Visible="False" /></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStudNo" runat="server" AutoPostBack="True" OnTextChanged="txtStudNo_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgProcess" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetStudent();"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Student Name required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtStudName">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr runat="server" id="trStudDetails">
                        <td align="left" width="20%"><span class="field-label">Grade</span></td>
                        <td align="left" colspan="3">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblGrade" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Division</span>
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblDivision" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">BusNo</span>
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblBusNo" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr runat="server" id="trserviceDetails">
                        <td align="left" width="20%"><span class="field-label">Service Area</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtssv_id" runat="server" AutoPostBack="True" OnTextChanged="txtssv_id_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgservice" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetService();" OnClick="imgservice_Click"></asp:ImageButton></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Narration</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtHeaderRemarks" runat="server" TextMode="MultiLine" Height="10px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Header remarks required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtHeaderRemarks">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg-lite" colspan="4">Fee Adjustments Details...</td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Fee Type</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlFeeType" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Amount</span></td>
                        <td align="left" width="30%">
                            <table width="100%">
                                <tr>
                                    <td width="70%">
                                        <asp:TextBox ID="txtDetAmount" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Amount required"
                                            ValidationGroup="SUBERROR" ControlToValidate="txtDetAmount">*</asp:RequiredFieldValidator>(-ve for CR entry)
                                    </td>
                                    <td width="30%">
                                        <asp:CheckBox ID="CBoneway" runat="server" Text="One Way"></asp:CheckBox>
                                    </td>
                                </tr>
                            </table>



                        </td>

                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Narration</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtDetRemarks" runat="server" TextMode="MultiLine" Height="10px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Detailed Remarks required"
                                ValidationGroup="SUBERROR" ControlToValidate="txtDetRemarks">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="right" colspan="4">
                            <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="SUBERROR" />
                            <asp:Button ID="btnSubCancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                    <tr id="tr_Deatails" runat="server">
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="FeeId" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fee Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFeeType" runat="server" Text='<%# bind("FEE_TYPE") %>'></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# bind("FEE_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# bind("FEE_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="One Way">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ckboneway" runat="server" Checked='<%# Bind("ONE_WAY") %>' Enabled="False"></asp:CheckBox>
                                            <asp:HiddenField ID="H_SSVID" runat="server" Value='<%# Bind("SSV_ID") %>'></asp:HiddenField>
                                            <br />
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" OnClick="lnkEdit_Click" runat="server">Edit</asp:LinkButton>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr runat="server" id="Tr1">
                        <td align="left" class="title-bg-lite" colspan="4">Location Details</td>
                    </tr>
                    <tr runat="server" id="Tr2">
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvStudentLocation" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found"
                                Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="Area">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblArea" runat="server" Text='<%# Bind("STU_AREA") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pickup">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPickup" runat="server" Text='<%# Bind("STU_PICKUP") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DropOff">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDropOff" runat="server" Text='<%# Bind("STU_DROPOFF") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr runat="server" id="Tr3">
                        <td align="left" class="title-bg-lite" colspan="4">Ledger Details</td>
                    </tr>
                    <tr runat="server" id="Tr4">
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvLedger" runat="server" AutoGenerateColumns="True" EmptyDataText="No Data Found"
                                CssClass="table table-bordered table-row" Width="100%">
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr runat="server" id="Tr5">
                        <td align="left" colspan="4">
                            <asp:CheckBox ID="chkPrint" runat="server" Checked="True" Text="Print After Save" CssClass="field-label"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnPrint" runat="server" OnClick="btnPrint_Click"
                                Text="Print" CssClass="button" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>


                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <asp:HiddenField ID="h_GRD_ID" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="HFssv_id" runat="server"></asp:HiddenField>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>


                <script type="text/javascript" lang="javascript">
                    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
                        $.fancybox({
                            type: 'iframe',
                            //maxWidth: 300,
                            href: gotourl,
                            //maxHeight: 600,
                            fitToView: true,
                            padding: 6,
                            width: w,
                            height: h,
                            autoSize: false,
                            openEffect: 'none',
                            showLoading: true,
                            closeClick: true,
                            closeEffect: 'fade',
                            'closeBtn': true,
                            afterLoad: function () {
                                this.title = '';//ShowTitle(pageTitle);
                            },
                            helpers: {
                                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                                title: { type: 'inside' }
                            },
                            onComplete: function () {
                                $("#fancybox-wrap").css({ 'top': '90px' });

                            },
                            onCleanup: function () {
                                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                                if (hfPostBack == "Y")
                                    window.location.reload(true);
                            }
                        });

                        return false;
                    }
                </script>

            </div>
        </div>
    </div>

</asp:Content>

