﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Globalization.CultureInfo
Imports UtilityObj

Partial Class Fees_FeeCardQuery
    Inherits System.Web.UI.Page
    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64
    Dim MainObj As Mainclass = New Mainclass()

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Not IsPostBack Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            txtFrom.Text = Format(Date.Now.AddMonths(-3), "dd/MMM/yyyy")
            txtTo.Text = Format(Date.Now, "dd/MMM/yyyy")
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> "F733035" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            setModifyvalues(0)
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            ' lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            lblError.Text = ""
        End If
    End Sub

    Private Sub setModifyvalues(ByVal flag As Integer)
        'bindDropDown(ddlSchool, "select BSU_ID, BSU_NAME from BUSINESSUNIT_M INNER JOIN BUSSEGMENT_M on BUS_BSG_ID = BSG_ID WHERE REPORTGROUP1='PSI' order by BSU_NAME")
        If ddlSchool.SelectedValue.Length = 0 Then
            bindDropDown(ddlSchool, "Select bsu_id,bsu_name from BUSINESSUNIT_M where  BSU_ENQ_GROUP=1  and  BSU_COUNTRY_ID IN (172) AND BSU_ID<>'300001' ORDER BY bsu_name")
            ddlSchool.SelectedValue = Session("sBsuid")
            txtBSUId.Text = ddlSchool.SelectedValue
        End If
        If flag = 2 Or ddlGrade.SelectedValue.Length = 0 Then
            txtBSUId.Text = ddlSchool.SelectedValue
            bindDropDown(ddlGrade, "select  distinct GRM_GRD_ID,GRM_DISPLAY,GRD_DISPLAYORDER from grade_BSU_M inner join grade_M on GRD_ID=GRM_GRD_ID where GRM_BSU_ID='" & ddlSchool.SelectedValue & "' order by GRD_DISPLAYORDER")
        End If
        If flag > 0 Or ddlSection.SelectedValue.Length = 0 Then bindDropDown(ddlSection, "SELECT 0 SCT_ID, '' SCT_DESCR union all SELECT SCT_ID, SCT_DESCR FROM SECTION_M inner JOIN ACADEMICYEAR_D on SCT_ACD_ID=ACD_ID where ACD_BSU_ID='" & ddlSchool.SelectedValue & "' AND SCT_GRD_ID='" & ddlGrade.SelectedValue & "' AND year(ACD_STARTDT)=" & Session("F_YEAR"))
    End Sub

    Private Sub bindDropDown(ByRef ddlToBind As DropDownList, ByVal strSQL As String)
        Dim dt As DataTable = MainObj.getRecords(strSQL, "OASISConnectionString")

        ddlToBind.DataSource = dt
        ddlToBind.DataValueField = dt.Columns(0).ColumnName
        ddlToBind.DataTextField = dt.Columns(1).ColumnName
        ddlToBind.DataBind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblError.Text = ""
        pnlResult.Visible = False
        txtStudentName.Text = System.Text.RegularExpressions.Regex.Replace(txtStudentName.Text, "\s{2,}", " ")
        If txtStudentName.Text.Trim.Length < 3 Then
            'lblError.Text = "Student Name should contain atleast 3 characters"
            usrMessageBar.ShowNotification("Student Name should contain atleast 3 characters", UserControls_usrMessageBar.WarningType.Danger)
        End If
        If txtStudentId.Text.Trim.Length < 5 Then
            ' lblError.Text &= IIf(lblError.Text.Length > 0, ",", "") & " Student Id should contain atleast 5 characters"
            usrMessageBar.ShowNotification(IIf(lblError.Text.Length > 0, ",", "") & " Student Id should contain atleast 5 characters", UserControls_usrMessageBar.WarningType.Danger)
        End If
        If Not IsDate(txtDOB.Text) Then
            ' lblError.Text &= IIf(lblError.Text.Length > 0, ",", "") & " Invalid Date Format"
            usrMessageBar.ShowNotification(IIf(lblError.Text.Length > 0, ",", "") & " Invalid Date Format", UserControls_usrMessageBar.WarningType.Danger)
        End If
        If lblError.Text.Length > 0 Then Exit Sub
        '@Student_Name varchar(100), @DOB datetime, @BSU_ID varchar(20), 
        '@Grd_id varchar(10), @Sct_id varchar(10), @Student_Id varchar(10), @Mobile varchar(10)
        Dim pParms(13) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@Student_Name", txtStudentName.Text, SqlDbType.VarChar)
        pParms(2) = Mainclass.CreateSqlParameter("@DOB", txtDOB.Text, SqlDbType.DateTime)
        pParms(3) = Mainclass.CreateSqlParameter("@BSU_Id", ddlSchool.SelectedValue, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@Grd_id", ddlGrade.SelectedValue, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@Sct_id", ddlSection.SelectedValue, SqlDbType.VarChar)
        pParms(6) = Mainclass.CreateSqlParameter("@Student_Id", txtStudentId.Text, SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@Mobile", txtMobileNo.Text, SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@Fyear", Session("F_YEAR"), SqlDbType.VarChar)

        Dim studentName() As String = {"", "", ""}        
        studentName(0) = txtStudentName.Text.Split(" ")(0)
        If txtStudentName.Text.Split(" ").Length > 1 Then studentName(1) = txtStudentName.Text.Split(" ")(1)
        If txtStudentName.Text.Split(" ").Length > 2 Then studentName(2) = txtStudentName.Text.Split(" ")(2)
        pParms(9) = Mainclass.CreateSqlParameter("@FN", studentName(0), SqlDbType.VarChar)
        pParms(10) = Mainclass.CreateSqlParameter("@MN", studentName(1), SqlDbType.VarChar)
        pParms(11) = Mainclass.CreateSqlParameter("@LN", studentName(2), SqlDbType.VarChar)
        pParms(12) = Mainclass.CreateSqlParameter("@User", Session("sUsr_name").ToString(), SqlDbType.VarChar)
        pParms(13) = Mainclass.CreateSqlParameter("@ClientIP", Request.UserHostAddress, SqlDbType.VarChar)

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As New DataSet
            ds = Mainclass.getDataSet("GetDataForNBAD", pParms, str_conn)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dsFill As New DataTable
                dsFill.Columns.Add(New DataColumn("label", System.Type.GetType("System.String")))
                dsFill.Columns.Add(New DataColumn("text", System.Type.GetType("System.String")))
                h_ColWidths.Value = ds.Tables(0).Rows(0)(0)
                For colCount As Integer = 1 To ds.Tables(0).Columns.Count - 1
                    If ds.Tables(0).Columns(colCount).ColumnName.Length > 2 Then dsFill.Rows.Add(ds.Tables(0).Columns(colCount).ColumnName, ds.Tables(0).Rows(0)(colCount))
                Next
                dsFill.AcceptChanges()
                rptResult.DataSource = dsFill
                rptResult.DataBind()
                pnlResult.Visible = True
            Else
                pnlResult.Visible = False
                ' lblError.Text = "Input data does not match. Please try again...."
                usrMessageBar.ShowNotification("Input data does not match. Please try again....", UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            'lblError.Text = getErrorMessage(str_success)
            Errorlog(ex.Message)
            ' lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        Finally

        End Try
        'Dim encObj As New Encryption64
        'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, IIf(pnlResult.Visible, 1, 0), ViewState("datamode"), Page.User.Identity.Name.ToString, Me.Page)
        'If flagAudit <> 0 Then
        '    Throw New ArgumentException("Could not process your request")
        'End If
        'ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
        'Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub ddlSchool_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSchool.SelectedIndexChanged
        setModifyvalues(2)
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        setModifyvalues(3)
    End Sub

    Protected Sub rptResult_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptResult.ItemDataBound
        Dim txtResult As TextBox = CType(e.Item.FindControl("txtResult"), TextBox)
        txtResult.Width = h_ColWidths.Value.Split(",")(e.Item.ItemIndex)
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtDOB.Text = ""
        txtMobileNo.Text = ""
        txtStudentName.Text = ""
        txtStudentId.Text = ""
        pnlResult.Visible = False
    End Sub
    'Protected Sub btnLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoad.Click
    '    Dim RptHead = ""
    '    Dim dateHead = "From " & txtFrom.Text.ToString() & " To " & txtTo.Text.ToString()
    '    Dim RptName = "../FEES/REPORTS/RPT/rptFeeCardQueryAbsents.rpt"

    '    Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
    '    Dim cmd As New SqlCommand("FEES.CardQueryAbsents")
    '    cmd.CommandType = CommandType.StoredProcedure

    '    Dim sqlpFRMDT As New SqlParameter("@DTFROm", SqlDbType.DateTime)
    '    sqlpFRMDT.Value = CDate(txtFrom.Text)
    '    cmd.Parameters.Add(sqlpFRMDT)

    '    Dim sqlpTODT As New SqlParameter("@DTTO", SqlDbType.DateTime)
    '    sqlpTODT.Value = CDate(txtTo.Text)
    '    cmd.Parameters.Add(sqlpTODT)

    '    Dim sqlBSUID As New SqlParameter("@BSUID", SqlDbType.VarChar)
    '    sqlBSUID.Value = Session("sBsuid")
    '    cmd.Parameters.Add(sqlBSUID)

    '    cmd.Connection = New SqlConnection(str_conn)
    '    Dim repSource As New MyReportClass
    '    Dim params As New Hashtable
    '    params("userName") = Session("sUsr_name")
    '    params("RptHead") = RptHead
    '    If txtFrom.Text.Equals(txtTo.Text) Then
    '        dateHead = "As On " & txtTo.Text.ToString()
    '    End If
    '    params("DateHead") = dateHead


    '    repSource.Parameter = params
    '    repSource.Command = cmd
    '    'repSource.IncludeBSUImage = True
    '    'repSource.HeaderBSUID = Session("sBsuid")
    '    repSource.ResourceName = RptName

    '    Session("ReportSource") = repSource
    '    Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
    'End Sub

    Protected Sub btnLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoad.Click

    End Sub

    Protected Sub btnview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnview.Click
        Dim RptHead = "Leavers List"
        Dim DtF As DateTime = CDate(txtFrom.Text.Trim)
        Dim DtT As DateTime = CDate(txtTo.Text.Trim)
        Dim dateHead = "For the Period " & DtF.ToString("MMM") & " " & DtF.Day.ToString & " " & DtF.Year.ToString & " To " & DtT.ToString("MMM") & "  " & DtT.Day & "  " & DtT.Year & ""
        Dim RptName = "../../FEES/REPORTS/RPT/rptFeeQueryCardAbsents.rpt"

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim cmd As New SqlCommand("FEES.CardQueryAbsents")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFRMDT As New SqlParameter("@DTFROm", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFrom.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtTo.Text)
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("RptHead") = RptHead
        If txtFrom.Text.Equals(txtTo.Text) Then
            dateHead = "As On " & txtTo.Text.ToString()
        End If
        params("DateHead") = dateHead

        repSource.Parameter = params
        repSource.Command = cmd
        'repSource.IncludeBSUImage = True
        'repSource.HeaderBSUID = Session("sBsuid")
        repSource.ResourceName = RptName

        Session("ReportSource") = repSource
        'Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
