﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AmbassadorProgramEntry.aspx.vb" Inherits="Fees_AmbassadorProgramEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <title>OASIS:: GEMS AMBASSADOR PROGRAM</title>
    <link rel="stylesheet" href="../cssfiles/bootstrap.css" type="text/css" media="screen" />

      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <style  type="text/css">
body{
    background: #eeeeee;
}
#popup{
    position: fixed;
    background: rgba(215,208,208,.8);
    display: none;
    top: 50%;
    left: 50%;
	transform: translate(-50%, -50%);

    width: 40%;
    height: 50%;
    border: 1px solid #000;
    border-radius: 5px;
    padding: 5px;
    color: #fff;
} 

/*media screen*/
.form-inline {
    
    display: inline !important;
   
}
.table {
 
  margin-bottom: 1rem;   
  /*background-color: transparent!important;*/
  font-family: 'Raleway', sans-serif !important;
  font-size : 12px !important;
  color:black;
  border-color: #8dc24c !important;/*rgb(27, 128, 182)*/
}

.table th,
.table td {
  padding: 0.5rem;
  vertical-align: middle;
  /*border-top: 1px solid #dee2e6;*/
  border-color: #8dc24c;/*rgb(27, 128, 182)*/  
  
}
      .table tr {
          border: 1px !important;
      }

.table th,
.table td {
  padding: 0.0rem;
  padding-left: 0.2rem;
    padding-right : 0.2rem;
  vertical-align: middle;
  margin:.5px;

  border:1px solid #dee2e6 !important;
  /*border-bottom: 0px solid #dee2e6 !important;*/
}

.table th {
  border:1px solid #8dc24c !important;
  /*border-bottom: 0px solid #dee2e6 !important;*/
  background-color:#8dc24c !important;
}
    table-striped tbody tr:nth-of-type(odd) {
        background-color: #e3f3e7;
    }

</style>

    <style type="text/css">
        table {
            /*border-collapse: collapse;*/
            border-collapse: inherit !important;
        }

        *,
        *::before,
        *::after {
            /*box-sizing: border-box;*/
            box-sizing: inherit !important;
        }
    </style>

    <div class="container">
        <div id="formdata">
     

            <div class="row">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    Font-Size="10px"></asp:Label>
            </div>
            <div class="row" id="rowa">

                <div class="col-lg-12 text-center" id="cola">
                    
                         <div class="col-2 border">Student No:</div>
                                <div class="col-10" >
                                    <input type="text" id="txtStdNo" runat="server"/> &nbsp;           
                    <a href="#p" id="open_popup" class="open_popup" runat="server"><img border="0" alt="" src="../Images/cal.gif"/></a>
                           <input type="text" id="txt_name" runat="server"/>     <asp:Button ID="lblbtn" runat="server" style="display: none" /></div>
              
                </div>

              
                           
                          

            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" OnClick="btnSave_Click" Text="Save" />
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" OnClick="btnCancel_Click" Text="Cancel" />
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hfStu_id" runat="server"></asp:HiddenField>

    <script  type="text/javascript" >
      function  popup(){
            
                
                $('#overlay, #popup').css('display', 'block');
                $('#formdata').find('input, textarea, button, select').attr("disabled", true);
                return false;
      }

 

        function closepopup() {
            $("#popup").css("display", "none");
            $('#formdata').find('input, textarea, button, select').attr("disabled", false);
        }

        $(function(){
            $("#open_popup").click(function () {
             
                $('#overlay, #popup').css('display', 'block');
                $('#formdata').find('input, textarea, button, select').attr("disabled", true);
            });

            $("#close_popup").click(function(){
	
                $("#popup").css("display", "none");	
                $('#formdata').find('input, textarea, button, select').attr("disabled", false);

		
	
            }); 
  
        });
  
  

        $(function() {
        
            $('#table2 a').click(function () {  $("#<%=hfStu_id.ClientID%>").val(this.text); $("#<%= txt_name.ClientID %>").val(this.text);  $('#formdata').find('input, textarea, button, select').attr("disabled", false);  $("#popup").css("display", "none"); });
	 	
	

        });

    </script>


    <script type="text/javascript"> 
        $(document).ready(function() {
            //$('#example').DataTable();
            $('#example').DataTable( {
                "paging":   true,
                "ordering": false,
                "info":     false,
                "pagingType": "simple"
            } );
        } );
    </script>


    <div id="overlay"></div>
	
      <div id="popup" style="width: 90%; height: 50%; overflow-y: auto;">
       	 <a href="#" id="close_popup" runat="server">Close</a>  <h4>Select Students</h4>
		
	<div   id="table2">
	<table class="table table-striped table-bordered  table-hover" id="example" >   <!--style="width: 90%; height: 50%; "-->
	    <%=Session("TABLE_ROW")%>
    


    </table>
	</div>

     </div>
    
</asp:Content>





