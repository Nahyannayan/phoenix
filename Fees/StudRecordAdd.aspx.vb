Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Fee_StudRecordAdd
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64

    Private Property BSU_bUPD_STU_CLIENT() As Boolean
        Get
            Return ViewState("BSU_bUPD_STU_CLIENT")
        End Get
        Set(ByVal value As Boolean)
            ViewState("BSU_bUPD_STU_CLIENT") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            h_Mode.Value = ViewState("datamode")
            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F300191" And ViewState("MainMnu_code") <> "F300191" And ViewState("MainMnu_code") <> "F300191" And ViewState("MainMnu_code") <> "F300191") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights
                callYEAR_DESCRBind()
                callGrade_ACDBind()
                callGrade_Section()
                callCurrent_BsuShift()
                Call GetReligion_info()
                Call GetNational_info()
                Call GetEmirate_info()
                LoadNonGEMSUnits()
                SetFlag()
                If CurBsUnit = "900350" Or CurBsUnit = "500601" Or CurBsUnit = "500555" Then 'E-SPORTS
                    ShowESportsSection(True)
                    'AccessStudentClass.PopulateBSU(Me.ddlGEMSSchool)
                    PopulateBSU(Me.ddlGEMSSchool)
                    'AccessStudentClass.PopulateVenue(Me.ddlVenue, CurBsUnit)
                    PopulateVenue(Me.ddlVenue, CurBsUnit)
                ElseIf CurBsUnit = "500933" Then 'Al Nabooda
                    PopulateBSU(Me.ddlGEMSSchool)
                    ShowNaboodaSection(True)
                Else
                    ShowESportsSection(False)
                End If

                Me.txtFDT.Text = DateTime.Now.ToString("dd/MMM/yyyy")
                Me.txtTDT.Text = DateTime.Now.AddMonths(1).ToString("dd/MMM/yyyy")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                If ViewState("datamode") = "view" Then
                    ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    FILLDETAILS()
                    Call control_Disable()
                End If
                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Public Shared Sub PopulateVenue(ByVal ddlBSU As DropDownList, ByVal BSU_ID As String)
        Dim dsVENUE As DataSet
        Dim qry As String
        qry = " SELECT 0 EAV_ID,'Please Select' EAV_DESCR union all "
        qry &= "SELECT DISTINCT EAV_ID,EAV_DESCR FROM TRANSPORT.EXTRA_ACTIVITY_VENUE_M INNER JOIN " & _
              "TRANSPORT.EXTRA_ACTIVITY_SERVICES_VENUE_D ON EAV_ID=EAD_EAV_ID WHERE EAD_STU_BSU_ID = '" & BSU_ID & "'"

        dsVENUE = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, qry)
        ddlBSU.DataTextField = "EAV_DESCR"
        ddlBSU.DataValueField = "EAV_ID"
        ddlBSU.DataSource = dsVENUE
        ddlBSU.DataBind()

    End Sub
    Protected Sub hlClearGEMSStudentSel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlClearGEMSStudentSel.Click
        ClearStudentDetails()
    End Sub
    Sub SetFlag()
        BSU_bUPD_STU_CLIENT = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "SELECT ISNULL(BSU_bUPD_STU_CLIENT,0)BSU_bUPD_STU_CLIENT FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & Session("sBsuId") & "'")
    End Sub

    Public Shared Sub PopulateBSU(ByVal ddlBSU As DropDownList)
        Dim dsBSU As DataSet
        Dim qry As String
        qry = "SELECT '0' BSU_ID,'NON-GEMS SCHOOL' BSU_NAME UNION ALL SELECT BSU_ID,BSU_NAME FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE ISNULL(BSU_Bschool,0)=1 ORDER BY BSU_NAME"

        dsBSU = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        ddlBSU.DataTextField = "BSU_NAME"
        ddlBSU.DataValueField = "BSU_ID"
        ddlBSU.DataSource = dsBSU
        ddlBSU.DataBind()
        ddlBSU.selectedValue = "0"
    End Sub
    Private Sub LoadNonGEMSUnits()
        Dim QRY As String = "SELECT NGB_ID ,NGB_NAME FROM dbo.NONGEMS_BUSINESSUNIT_M WHERE ISNULL(NGB_bDELETED, 0) = 0 ORDER BY NGB_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, QRY)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ddlTptClient.DataValueField = "NGB_ID"
            ddlTptClient.DataTextField = "NGB_NAME"
            ddlTptClient.DataSource = ds.Tables(0)
            ddlTptClient.DataBind()
            ddlTptClient.SelectedIndex = 0
        End If
    End Sub

    Private Sub control_Disable()
        ddlAcademicYear.Attributes.Add("Readonly", "Readonly")
        ddlGEMSSchool.Enabled = False
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next

            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub FILLDETAILS()
        Dim STU_ID As Integer
        STU_ID = ViewState("viewid")
        Try
            Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim lstrSQL, lstrSQL2 As String
            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim i As Integer

            Dim BSU_ID As String
            Dim ACD_ID As Integer
            Dim GRD_ID As String
            Dim SCT_ID As Integer
            Dim SHF_ID As Integer
            Dim STU_RLG_ID As String
            Dim STU_NATIONALITY As String


            lstrSQL = "SELECT STU_FEE_ID,STU_BSU_ID,STU_GENDER,STU_ACD_ID,STU_GRD_ID,STU_SHF_ID,STU_SCT_ID,STU_DOJ,STU_DOB,STU_FIRSTNAME,STU_MIDNAME, " _
            & " STU_LASTNAME,ISNULL(STU_GEMS_BSU_ID,'') STU_GEMS_BSU_ID,ISNULL(STU_GEMS_STU_ID,0) STU_GEMS_STU_ID ,ISNULL(STU_SVC_EAV_ID,0) STU_SVC_EAV_ID, " _
            & " STU_SVC_FROMDT, STU_SVC_TODT,ISNULL(STU_RLG_ID,'') STU_RLG_ID,isNULL(STU_NATIONALITY,5) as STU_NATIONALITY,ISNULL(STU_CLIENT_BSU_ID,'')STU_CLIENT_BSU_ID, " _
            & " STS_FFirstName,STS_FMidName,STS_FLastNAme,STS_FMobile,STS_FEmail,STS_FCOMPOBOX,STS_FEMIR,ISNULL(STU_NONGEMS_BSU_ID,'')STU_NONGEMS_BSU_ID, " _
            & " ISNULL(STU_FEE_ID,'')STU_FEE_ID " _
            & " FROM STUDENT_M A INNER JOIN STUDENT_D B ON A.STU_SIBLING_ID=B.STS_STU_ID" _
            & " WHERE STU_ID='" & STU_ID & "'"
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)

            'btnSettle.Visible = True
            If ds.Tables(0).Rows.Count > 0 Then
                ACD_ID = ds.Tables(0).Rows(0)("STU_ACD_ID")
                BSU_ID = ds.Tables(0).Rows(0)("STU_BSU_ID")
                If BSU_ID = "900350" Or BSU_ID = "500601" Or BSU_ID = "500555" Then
                    Dim GEMS_SCHOOL = ds.Tables(0).Rows(0)("STU_GEMS_BSU_ID")
                    For ItemTypeCounter As Integer = 0 To ddlGEMSSchool.Items.Count - 1
                        If ddlGEMSSchool.Items(ItemTypeCounter).Value = GEMS_SCHOOL Then
                            ddlGEMSSchool.SelectedIndex = ItemTypeCounter
                        End If
                    Next
                    Dim VENUE = ds.Tables(0).Rows(0)("STU_SVC_EAV_ID")
                    For ItemTypeCounter As Integer = 0 To ddlVenue.Items.Count - 1
                        If ddlVenue.Items(ItemTypeCounter).Value = VENUE Then
                            ddlVenue.SelectedIndex = ItemTypeCounter
                        End If
                    Next
                    Me.txtFDT.Text = Convert.ToDateTime(ds.Tables(0).Rows(0)("STU_SVC_FROMDT")).ToString("dd/MMM/yyyy")
                    Me.txtTDT.Text = Convert.ToDateTime(ds.Tables(0).Rows(0)("STU_SVC_TODT")).ToString("dd/MMM/yyyy")
                    Me.h_STUD_ID.Value = ds.Tables(0).Rows(0)("STU_GEMS_STU_ID")
                    Dim dsSTU As DataSet = AccessStudentClass.GET_STUDENT_DETIALS(Me.h_STUD_ID.Value)
                    If Not dsSTU Is Nothing AndAlso dsSTU.Tables.Count > 0 AndAlso dsSTU.Tables(0).Rows.Count > 0 Then
                        Me.txtStdNo.Text = dsSTU.Tables(0).Rows(0)("STU_NO").ToString
                        Me.txtStudentname.Text = dsSTU.Tables(0).Rows(0)("STU_FIRSTNAME").ToString & " " & dsSTU.Tables(0).Rows(0)("STU_MIDNAME").ToString & " " & dsSTU.Tables(0).Rows(0)("STU_LASTNAME").ToString
                    End If
                End If
                If BSU_bUPD_STU_CLIENT Then
                    Me.txtFeeId.Text = ds.Tables(0).Rows(0)("STU_FEE_ID").ToString
                End If
                If BSU_ID = "500933" Then 'Al Nabooda
                    tr_ClientSchool.Visible = True
                    Dim NON_GEMS = ds.Tables(0).Rows(0)("STU_NONGEMS_BSU_ID").ToString
                    If Not NON_GEMS Is Nothing AndAlso NON_GEMS <> "" Then
                        ddlGEMSSchool.SelectedValue = "0"
                        For ItemTypeCounter As Integer = 0 To ddlTptClient.Items.Count - 1
                            If ddlTptClient.Items(ItemTypeCounter).Value = NON_GEMS Then
                                ddlTptClient.SelectedIndex = ItemTypeCounter
                            End If
                        Next
                        DisableStudentDetail(False)
                    Else
                        Dim GEMS_SCHOOL = ds.Tables(0).Rows(0)("STU_CLIENT_BSU_ID").ToString
                        For ItemTypeCounter As Integer = 0 To ddlGEMSSchool.Items.Count - 1
                            If ddlGEMSSchool.Items(ItemTypeCounter).Value = GEMS_SCHOOL Then
                                ddlGEMSSchool.SelectedIndex = ItemTypeCounter
                            End If
                        Next
                        tr_ClientSchool.Visible = False
                        DisableStudentDetail(True)
                    End If
                    Me.h_STUD_ID.Value = STU_ID
                    Dim dsSTU As DataSet = AccessStudentClass.GET_STUDENT_DETIALS(Me.h_STUD_ID.Value)
                    If Not dsSTU Is Nothing AndAlso dsSTU.Tables.Count > 0 AndAlso dsSTU.Tables(0).Rows.Count > 0 Then
                        Me.txtStdNo.Text = dsSTU.Tables(0).Rows(0)("STU_NO").ToString
                        Me.txtStudentname.Text = dsSTU.Tables(0).Rows(0)("STU_FIRSTNAME").ToString & " " & dsSTU.Tables(0).Rows(0)("STU_MIDNAME").ToString & " " & dsSTU.Tables(0).Rows(0)("STU_LASTNAME").ToString
                    End If
                End If
                For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                    'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = ACD_ID Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                Next
                GRD_ID = ds.Tables(0).Rows(0)("STU_GRD_ID")
                For ItemTypeCounter As Integer = 0 To ddlGrade.Items.Count - 1
                    'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                    If ddlGrade.Items(ItemTypeCounter).Value = GRD_ID Then
                        ddlGrade.SelectedIndex = ItemTypeCounter
                    End If
                Next
                callGrade_Section()

                SCT_ID = ds.Tables(0).Rows(0)("STU_SCT_ID")
                For ItemTypeCounter As Integer = 0 To ddlSection.Items.Count - 1
                    'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                    If ddlSection.Items(ItemTypeCounter).Value = SCT_ID Then
                        ddlSection.SelectedIndex = ItemTypeCounter
                    End If
                Next
                SHF_ID = ds.Tables(0).Rows(0)("STU_SHF_ID")
                For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
                    'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                    If ddlShift.Items(ItemTypeCounter).Value = SHF_ID Then
                        ddlShift.SelectedIndex = ItemTypeCounter
                    End If
                Next

                STU_RLG_ID = ds.Tables(0).Rows(0)("STU_RLG_ID")
                For ItemTypeCounter As Integer = 0 To ddlReligion.Items.Count - 1
                    'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                    If ddlReligion.Items(ItemTypeCounter).Value = STU_RLG_ID Then
                        ddlReligion.SelectedIndex = ItemTypeCounter
                    End If
                Next

                STU_NATIONALITY = ds.Tables(0).Rows(0)("STU_NATIONALITY")
                For ItemTypeCounter As Integer = 0 To ddlNationality_Birth.Items.Count - 1
                    'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                    If ddlNationality_Birth.Items(ItemTypeCounter).Value = STU_NATIONALITY Then
                        ddlNationality_Birth.SelectedIndex = ItemTypeCounter
                    End If
                Next
                Try
                    ddlFCOMEmirate.SelectedValue = ds.Tables(0).Rows(0)("STS_FEMIR").ToString
                Catch
                End Try

                If IsDate(ds.Tables(0).Rows(0)("STU_DOJ")) = True Then
                    txtDoj.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((ds.Tables(0).Rows(0)("STU_DOJ"))))
                End If

                If IsDate(ds.Tables(0).Rows(0)("STU_DOB")) = True Then
                    txtDob.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((ds.Tables(0).Rows(0)("STU_DOB"))))
                End If

                txtFeeId.Text = ds.Tables(0).Rows(0)("STU_FEE_ID")
                txtFname_E.Text = DoNullCheck(ds.Tables(0).Rows(0)("STU_FIRSTNAME"))
                txtMname_E.Text = DoNullCheck(ds.Tables(0).Rows(0)("STU_MIDNAME"))
                txtLname_E.Text = DoNullCheck(ds.Tables(0).Rows(0)("STU_LASTNAME"))


                txtFname_P.Text = DoNullCheck(ds.Tables(0).Rows(0)("STS_FFIRSTNAME"))
                txtMname_P.Text = DoNullCheck(ds.Tables(0).Rows(0)("STS_FMIDNAME"))
                txtLname_P.Text = DoNullCheck(ds.Tables(0).Rows(0)("STS_FLASTNAME"))
                txtFEmail.Text = DoNullCheck(ds.Tables(0).Rows(0)("STS_FEMAIL"))
                txtFMobile_No.Text = DoNullCheck(ds.Tables(0).Rows(0)("STS_FMobile"))
                txtFCOMPOBOX.Text = DoNullCheck(ds.Tables(0).Rows(0)("STS_FCOMPOBOX"))


                Dim temp_gender As String
                temp_gender = Convert.ToString(ds.Tables(0).Rows(0)("STU_GENDER"))
                If UCase(temp_gender) = "F" Then
                    rdFemale.Checked = True

                ElseIf UCase(temp_gender) = "M" Then
                    rdMale.Checked = True

                End If
            End If
        Catch ex As Exception
            'lblError.Text = "Record Not Found !!! "
            usrMessageBar.ShowNotification("Record Not Found !!! ", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub

    Public Function DoNullCheck(ByVal val As Object) As Object
        If val Is DBNull.Value Then
            Return ""
        End If
        Return val
    End Function

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGrade.Items.Clear()
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While
                End If
            End Using
            'If Session("sBsuid") = "900350" Then
            di = New ListItem("--", "0")
            ddlGrade.Items.Insert(0, di)
            ddlGrade.SelectedValue = "0"
            ' End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section(Session("sBsuid"), ACD_ID, GRD_ID)
                ddlSection.Items.Clear()

                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub GetNational_info()
        Try
            Using AllNational_reader As SqlDataReader = AccessStudentClass.GetNational()
                Dim di_National As ListItem
                ddlNationality_Birth.Items.Clear()


                If AllNational_reader.HasRows = True Then
                    While AllNational_reader.Read()
                        di_National = New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID"))
                        ddlNationality_Birth.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                    End While

                    For ItemTypeCounter As Integer = 0 To ddlNationality_Birth.Items.Count - 1
                        'keep loop until you get the Country to Not Available into  the SelectedIndex

                        If ddlNationality_Birth.Items(ItemTypeCounter).Value = ViewState("temp_Nationality") Then
                            ddlNationality_Birth.SelectedIndex = ItemTypeCounter
                        End If

                    Next
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetNational_info")
        End Try
    End Sub
    Sub GetEmirate_info()
        Try
            Using AllEmirate_reader As SqlDataReader = AccessStudentClass.GetEmirate()
                Dim di_Emirate As ListItem
                ddlFCOMEmirate.Items.Clear()




                If AllEmirate_reader.HasRows = True Then
                    While AllEmirate_reader.Read
                        di_Emirate = New ListItem(AllEmirate_reader("EMR_DESCR"), AllEmirate_reader("EMR_CODE"))
                        ddlFCOMEmirate.Items.Add(New ListItem(AllEmirate_reader("EMR_DESCR"), AllEmirate_reader("EMR_CODE")))
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetEmirate_info")
        End Try
    End Sub

    Sub GetReligion_info()
        Try

            Using AllReligion_reader As SqlDataReader = AccessStudentClass.GetReligion()

                Dim di_Religion As ListItem
                ddlReligion.Items.Clear()
                'di_Religion = New ListItem("--", "--")
                ' ddlReligion.Items.Add(di_Religion)
                If AllReligion_reader.HasRows = True Then
                    While AllReligion_reader.Read

                        di_Religion = New ListItem(AllReligion_reader("RLG_DESCR"), AllReligion_reader("RLG_ID"))
                        ddlReligion.Items.Add(di_Religion)

                    End While
                    For ItemTypeCounter As Integer = 0 To ddlReligion.Items.Count - 1
                        'keep loop until you get the Country to Not Available into  the SelectedIndex
                        If ddlReligion.Items(ItemTypeCounter).Value = ViewState("temp_Rlg_ID") Then
                            ddlReligion.SelectedIndex = ItemTypeCounter
                        End If


                    Next

                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetReligion_info")
        End Try
    End Sub
    Public Sub callCurrent_BsuShift()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Current_BsuShiftReader As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Session("sBsuid"), ACD_ID)
                ddlShift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read
                        di = New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID"))
                        ddlShift.Items.Add(di)
                    End While
                    For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
                        'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                        If Not Session("Current_ACD_ID") Is Nothing Then
                            If UCase(ddlShift.Items(ItemTypeCounter).Text) = "NORMAL" Then
                                ddlShift.SelectedIndex = ItemTypeCounter
                            End If
                        End If
                    Next
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"

        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"

        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim FromTo_Date As String = ""
        Dim i As Integer = 0

        'If txtFromDate.Text <> "" And txtToDate.Text <> "" Then
        '    FromTo_Date = " AND STUDENT_M.STU_DOJ BETWEEN '" + Format(Date.Parse(txtFromDate.Text), "yyyy-MM-dd") + "' AND '" + Format(Date.Parse(txtToDate.Text), "yyyy-MM-dd") + "'"
        'End If

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", Session("sbsuid"))
        param.Add("@Grade", ddlGrade.SelectedItem.Value)
        If ddlSection.SelectedItem.Value = "0" Then
            param.Add("@Section", Nothing)
        Else
            param.Add("@Section", ddlSection.SelectedItem.Value)
        End If
        param.Add("@ACCYEAR", ddlAcademicYear.SelectedItem.Value)
        param.Add("@SHIFT", ddlShift.SelectedItem.Value)
        param.Add("Acad_Year", ddlAcademicYear.SelectedItem.Text)
        param.Add("Grade_Dis", ddlGrade.SelectedItem.Text)
        param.Add("Section_Dis", ddlSection.SelectedItem.Text)
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))



        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            If ViewState("MainMnu_code") = "S200011" Then
                .reportPath = Server.MapPath("../RPT/rptClassListTemplate.rpt")
            ElseIf ViewState("MainMnu_code") = "S200015" Then
                .reportPath = Server.MapPath("../RPT/rptStudentList.rpt")
            ElseIf ViewState("MainMnu_code") = "S200020" Then
                .reportPath = Server.MapPath("../RPT/rptParentDetails.rpt")
            ElseIf ViewState("MainMnu_code") = "S200025" Then 'student details
                .reportPath = Server.MapPath("../RPT/rptStudentDetails.rpt")
            End If
            'file:///C:\Documents and Settings\lijo.joseph.GEMSEDUCATION\Desktop\oasis Project\OASISMAR16\Students\Reports\RPT\rptAdmission_Detail.rpt
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callGrade_ACDBind()
        callGrade_Section()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim status As Integer
        Dim transaction As SqlTransaction
        If Page.IsValid = True Then
            Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    If ddlGrade.SelectedValue = "0" Then
                        'lblError.Text = "Please Select the Grade."
                        usrMessageBar.ShowNotification("Please Select the Grade.", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If

                    If Session("sBsuid") = "900350" Or Session("sBsuid") = "500601" Or Session("sBsuid") = "500555" Then 'ESports and ELL
                        If ddlVenue.SelectedValue = "0" Then
                            'lblError.Text = "Please Select the Venue."
                            usrMessageBar.ShowNotification("Please Select the Venue.", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                        Dim PeriodTable As DataTable
                        PeriodTable = GetPeriodForExtraActivityVenue(ddlAcademicYear.SelectedValue, Session("sBsuid"), ddlVenue.SelectedValue)
                        If Not PeriodTable Is Nothing Then
                            If PeriodTable.Rows.Count > 0 Then
                                If Not (CDate(txtFDT.Text) >= CDate(PeriodTable.Rows(0).Item("EAD_FROMDT")).ToString("dd/MMM/yyyy") And CDate(txtFDT.Text) <= CDate(PeriodTable.Rows(0).Item("EAD_TODT")).ToString("dd/MMM/yyyy")) Then
                                    'lblError.Text = "InValid Period Start Date."
                                    usrMessageBar.ShowNotification("InValid Period Start Date.", UserControls_usrMessageBar.WarningType.Danger)
                                    Exit Sub
                                End If
                                If Not (CDate(txtTDT.Text) >= CDate(PeriodTable.Rows(0).Item("EAD_FROMDT")).ToString("dd/MMM/yyyy") And CDate(txtTDT.Text) <= CDate(PeriodTable.Rows(0).Item("EAD_TODT")).ToString("dd/MMM/yyyy")) Then
                                    'lblError.Text = "InValid Period End Date."
                                    usrMessageBar.ShowNotification("InValid Period End Date.", UserControls_usrMessageBar.WarningType.Danger)
                                    Exit Sub
                                End If
                            End If
                        End If


                    End If

                    status = SaveStudent(transaction)
                    If status <> 0 Then
                        Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                    End If
                    transaction.Commit()

                    'lblError.Text = "Record Updated Successfully"
                    usrMessageBar.ShowNotification("Record Updated Successfully", UserControls_usrMessageBar.WarningType.Success)
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    If Session("sBsuid") = "900350" Or Session("sBsuid") = "500601" Or Session("sBsuid") = "500555" Then
                        ClearDetails()
                        ViewState("datamode") = "add"
                    End If

                Catch myex As ArgumentException
                    transaction.Rollback()
                    'lblError.Text = myex.Message
                    usrMessageBar.ShowNotification(myex.Message, UserControls_usrMessageBar.WarningType.Danger)
                Catch ex As Exception
                    transaction.Rollback()
                    'lblError.Text = "Record could not be Updated"
                    usrMessageBar.ShowNotification("Record could not be Updated", UserControls_usrMessageBar.WarningType.Danger)

                End Try
            End Using
        End If

    End Sub
    Private Function ClearDetails()

        ddlAcademicYear.SelectedItem.Value = Nothing
        ddlGrade.SelectedItem.Value = Nothing
        ddlGrade.SelectedItem.Value = Nothing
        ddlShift.SelectedItem.Value = Nothing
        ddlSection.SelectedItem.Value = Nothing
        txtDoj.Text = ""
        txtFeeId.Text = ""
        txtFname_E.Text = ""
        txtMname_E.Text = ""
        txtLname_E.Text = ""

        ddlReligion.SelectedItem.Value = Nothing
        txtDob.Text = ""
        ddlNationality_Birth.SelectedItem.Value = Nothing
        txtFname_P.Text = ""
        txtMname_P.Text = ""
        txtLname_P.Text = ""
        txtFCOMPOBOX.Text = ""
        ddlFCOMEmirate.SelectedItem.Value = Nothing
        txtFMobile_No.Text = ""
        txtFEmail.Text = ""
        Me.h_STUD_ID.Value = ""
        Me.ddlVenue.SelectedValue = "0"
        Me.ddlGEMSSchool.SelectedValue = "0"
        ClearStudentDetails()

    End Function

    Function SaveStudent(ByVal trans As SqlTransaction) As Integer
        Dim Status As Integer
        Dim STU_NONGEMS_BSU_ID As String = ""
        Dim STU_ACD_ID As Integer = ddlAcademicYear.SelectedItem.Value
        Dim STU_GRD_ID As String = ddlGrade.SelectedItem.Value
        Dim STU_GRM_ID As String = ddlGrade.SelectedItem.Value
        Dim STU_SHF_ID As Integer = ddlShift.SelectedItem.Value
        Dim STU_SCT_ID As Integer = ddlSection.SelectedItem.Value
        Dim STU_DOJ As String = txtDoj.Text
        Dim STU_FEE_Id As String = txtFeeId.Text
        Dim STU_FIRSTNAME As String = txtFname_E.Text
        Dim STU_MIDNAME As String = txtMname_E.Text
        Dim STU_LASTNAME As String = txtLname_E.Text
        Dim STU_GENDER As String
        If rdFemale.Checked = True Then
            STU_GENDER = "F"
        Else
            STU_GENDER = "M"
        End If
        Dim STU_RLG_ID As String = ddlReligion.SelectedItem.Value
        Dim STU_DOB As String = txtDob.Text
        Dim STU_NATIONALITY As String = ddlNationality_Birth.SelectedItem.Value
        Dim F_FIRSTNAME As String = txtFname_P.Text
        Dim F_MIDNAME As String = txtMname_P.Text
        Dim F_LASTNAME As String = txtLname_P.Text
        Dim F_POBox As String = txtFCOMPOBOX.Text
        Dim F_Emirate As String = ddlFCOMEmirate.SelectedItem.Value
        Dim F_Mobile As String = txtFMobile_No.Text
        Dim F_Email As String = txtFEmail.Text
        Dim GEMS_SCHOOL As String = "0", VENUE As Integer = 0, STU_ID As Long = 0
        If Me.h_STUD_ID.Value <> "" Then
            STU_ID = Me.h_STUD_ID.Value
            GEMS_SCHOOL = Me.ddlGEMSSchool.SelectedValue
        End If
        If Not ddlVenue.SelectedItem Is Nothing Then
            VENUE = Me.ddlVenue.SelectedValue
        Else
            VENUE = 0
        End If
        If GEMS_SCHOOL = "0" And tr_ClientSchool.Visible = True Then 'transport service for a non GEMS school
            STU_NONGEMS_BSU_ID = ddlTptClient.SelectedValue
            BSU_bUPD_STU_CLIENT = False
        End If
        If Not IsDate(Me.txtFDT.Text.Trim) Then
            Me.txtFDT.Text = "01/Jan/1900"
        End If
        If Not IsDate(Me.txtTDT.Text.Trim) Then
            Me.txtTDT.Text = "01/Jan/1900"
        End If


        Try
            ' If Session("sBsuId").ToString = "500933" Then 'Al Nabooda
            If BSU_bUPD_STU_CLIENT Then 'if true then update student
                Status = AccessStudentClass.Transport_UpdateStudent(h_STUD_ID.Value, Session("sBsuId"), _
                STU_ACD_ID, STU_GRD_ID, STU_SHF_ID, STU_SCT_ID, STU_FIRSTNAME, STU_MIDNAME, STU_LASTNAME, _
                STU_DOJ, STU_FEE_Id, STU_DOB, STU_GENDER, STU_RLG_ID, STU_NATIONALITY, _
                F_FIRSTNAME, F_MIDNAME, F_LASTNAME, F_POBox, F_Emirate, F_Mobile, F_Email, _
                GEMS_SCHOOL, STU_ID, VENUE, Me.txtFDT.Text, Me.txtTDT.Text, "UPDATE", STU_NONGEMS_BSU_ID, trans)
            Else
                If ViewState("datamode") = "add" Then
                    Status = AccessStudentClass.Transport_SaveStudent(0, Session("sBSUID"), _
                  STU_ACD_ID, STU_GRD_ID, STU_SHF_ID, STU_SCT_ID, STU_FIRSTNAME, STU_MIDNAME, STU_LASTNAME, _
                  STU_DOJ, STU_FEE_Id, STU_DOB, STU_GENDER, STU_RLG_ID, STU_NATIONALITY, _
                 F_FIRSTNAME, F_MIDNAME, F_LASTNAME, F_POBox, F_Emirate, F_Mobile, F_Email, _
                 GEMS_SCHOOL, STU_ID, VENUE, Me.txtFDT.Text, Me.txtTDT.Text, "ADD", STU_NONGEMS_BSU_ID, trans)
                Else
                    Status = AccessStudentClass.Transport_SaveStudent(ViewState("viewid"), Session("sBSUID"), _
                 STU_ACD_ID, STU_GRD_ID, STU_SHF_ID, STU_SCT_ID, STU_FIRSTNAME, STU_MIDNAME, STU_LASTNAME, _
                 STU_DOJ, STU_FEE_Id, STU_DOB, STU_GENDER, STU_RLG_ID, STU_NATIONALITY, _
                F_FIRSTNAME, F_MIDNAME, F_LASTNAME, F_POBox, F_Emirate, F_Mobile, F_Email, _
                 GEMS_SCHOOL, STU_ID, VENUE, Me.txtFDT.Text, Me.txtTDT.Text, "UPDATE", STU_NONGEMS_BSU_ID, trans)
                End If
            End If


            Return Status
        Catch ex As Exception
            Return 1000
        Finally
            SetFlag()
        End Try
    End Function

    Sub ShowESportsSection(ByVal bVisible As Boolean)
        Me.tr_Header.Visible = bVisible
        Me.tr_1.Visible = bVisible
        Me.tr_2.Visible = bVisible
        Me.tr_3.Visible = bVisible
        Me.tr_4.Visible = bVisible
        Me.tr_5.Visible = bVisible
    End Sub
    Sub ShowNaboodaSection(ByVal bVisible As Boolean)
        Me.tr_Header.Visible = bVisible
        tr_ClientSchool.Visible = bVisible
        Me.tr_1.Visible = bVisible
        Me.tr_2.Visible = False
        Me.tr_3.Visible = False
        Me.tr_4.Visible = False
        Me.tr_5.Visible = False
    End Sub
    Public Shared Function GET_STUDENT_DETIALS(ByVal STU_ID As Long) As DataSet
        Try
            Dim QRY As String = "SELECT STU_NO,ISNULL(STU_SIBLING_ID,STU_ID) STU_SIBLING_ID,STU_GRD_ID,ISNULL(STU_FIRSTNAME,'')STU_FIRSTNAME," & _
                "ISNULL(STU_MIDNAME,'')STU_MIDNAME,ISNULL(STU_LASTNAME,'')STU_LASTNAME,STU_DOJ,STU_DOB,ISNULL(STU_NATIONALITY,'0')STU_NATIONALITY," & _
                "ISNULL(STU_RLG_ID,'0')STU_RLG_ID,STU_GENDER ,STS_FEMIR,ISNULL(STU_FEE_ID,'')STU_FEE_ID FROM dbo.STUDENT_M WITH(NOLOCK)  " & _
                "LEFT OUTER JOIN STUDENT_D WITH(NOLOCK) ON ISNULL(STU_SIBLING_ID,STU_ID)=STS_STU_ID WHERE STU_ID=" & STU_ID & ""
            GET_STUDENT_DETIALS = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, QRY)
        Catch ex As Exception
            GET_STUDENT_DETIALS = Nothing
        End Try
    End Function
    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        If ViewState("datamode") <> "add" Then
            Exit Sub
        End If
        If Me.h_STUD_ID.Value <> "" AndAlso Convert.ToInt64(Me.h_STUD_ID.Value) > 0 Then
            Dim STU_SIBLING_ID As Long = Me.h_STUD_ID.Value
            'Dim dsSTU As DataSet = AccessStudentClass.GET_STUDENT_DETIALS(Me.h_STUD_ID.Value)
            Dim dsSTU As DataSet = GET_STUDENT_DETIALS(Me.h_STUD_ID.Value)
            If Not dsSTU Is Nothing AndAlso dsSTU.Tables.Count > 0 AndAlso dsSTU.Tables(0).Rows.Count > 0 Then
                Me.txtStdNo.Text = dsSTU.Tables(0).Rows(0)("STU_NO").ToString
                STU_SIBLING_ID = dsSTU.Tables(0).Rows(0)("STU_SIBLING_ID").ToString
                Dim DOB = Convert.ToDateTime(dsSTU.Tables(0).Rows(0)("STU_DOB")).ToString("dd/MMM/yyyy")
                Me.txtDob.Text = DOB 'TryCast(dsSTU.Tables(0).Rows(0)("STU_DOB"), DateTime)
                Dim DOJ = Convert.ToDateTime(dsSTU.Tables(0).Rows(0)("STU_DOJ")).ToString("dd/MMM/yyyy")
                Me.txtDoj.Text = DOJ
                Me.txtFname_E.Text = dsSTU.Tables(0).Rows(0)("STU_FIRSTNAME").ToString
                Me.txtMname_E.Text = dsSTU.Tables(0).Rows(0)("STU_MIDNAME").ToString
                Me.txtLname_E.Text = dsSTU.Tables(0).Rows(0)("STU_LASTNAME").ToString
                Me.txtStudentname.Text = Me.txtFname_E.Text & " " & Me.txtMname_E.Text & " " & Me.txtLname_E.Text
                If Not ddlGrade.Items.FindByValue(dsSTU.Tables(0).Rows(0)("STU_GRD_ID").ToString) Is Nothing Then
                    ddlGrade.SelectedValue = dsSTU.Tables(0).Rows(0)("STU_GRD_ID").ToString
                    callGrade_Section()
                End If
                If Not ddlFCOMEmirate.Items.FindByValue(dsSTU.Tables(0).Rows(0)("STS_FEMIR").ToString) Is Nothing Then
                    ddlFCOMEmirate.SelectedValue = dsSTU.Tables(0).Rows(0)("STS_FEMIR").ToString
                Else
                    If Not ddlFCOMEmirate.Items.FindByValue("DXB") Is Nothing Then
                        ddlFCOMEmirate.SelectedValue = "DXB"
                    End If
                End If
                Dim Nationality = dsSTU.Tables(0).Rows(0)("STU_NATIONALITY").ToString
                If Nationality <> "" AndAlso IsNumeric(Nationality) Then
                    Try
                        Me.ddlNationality_Birth.SelectedValue = Nationality
                    Catch
                    End Try
                End If
                Me.rdMale.Checked = IIf(dsSTU.Tables(0).Rows(0)("STU_GENDER").ToString = "M", True, False)
                Dim Religion = dsSTU.Tables(0).Rows(0)("STU_RLG_ID").ToString
                If Religion <> "" Then
                    Try
                        Me.ddlReligion.SelectedValue = Religion
                    Catch
                    End Try
                End If
                If BSU_bUPD_STU_CLIENT Then
                    txtFeeId.Text = dsSTU.Tables(0).Rows(0)("STU_FEE_ID").ToString
                End If
                If Session("sBsuId") = "500933" Then 'Al Nabooda, disable editing  details of GEMS school student
                    If ddlGEMSSchool.SelectedValue <> "0" Then 'if GEMS School
                        DisableStudentDetail(True)
                    Else
                        DisableStudentDetail(False)
                    End If
                End If
            End If
            Dim dsPARENT As DataSet = AccessStudentClass.GET_PARENT_DETIALS(STU_SIBLING_ID)
            If Not dsPARENT Is Nothing AndAlso dsPARENT.Tables.Count > 0 AndAlso dsPARENT.Tables(0).Rows.Count > 0 Then
                Me.txtFname_P.Text = dsPARENT.Tables(0).Rows(0)("STS_FFIRSTNAME").ToString
                Me.txtMname_P.Text = dsPARENT.Tables(0).Rows(0)("STS_FMIDNAME").ToString
                Me.txtLname_P.Text = dsPARENT.Tables(0).Rows(0)("STS_FLASTNAME").ToString
                Me.txtFCOMPOBOX.Text = dsPARENT.Tables(0).Rows(0)("STS_FCOMPOBOX").ToString
                Dim FCOMEmirate = dsPARENT.Tables(0).Rows(0)("STS_FEMIR").ToString
                If FCOMEmirate <> "" Then
                    Try
                        Me.ddlFCOMEmirate.SelectedValue = FCOMEmirate
                    Catch
                    End Try
                End If
                Me.txtFMobile_No.Text = dsPARENT.Tables(0).Rows(0)("STS_FMOBILE").ToString
                Me.txtFEmail.Text = dsPARENT.Tables(0).Rows(0)("STS_FEmail").ToString
            End If
        Else
            Me.txtDob.Text = ""
            Me.txtDoj.Text = ""
            Me.txtFname_E.Text = ""
            Me.txtMname_E.Text = ""
            Me.txtLname_E.Text = ""
            Me.ddlNationality_Birth.SelectedIndex = 0
            Me.rdMale.Checked = True
            Me.ddlReligion.SelectedIndex = 0
            Me.txtFname_P.Text = ""
            Me.txtMname_P.Text = ""
            Me.txtLname_P.Text = ""
            Me.txtFCOMPOBOX.Text = ""
            Me.ddlFCOMEmirate.SelectedIndex = 0
            Me.txtFMobile_No.Text = ""
            Me.txtFEmail.Text = ""
            Me.txtFeeId.Text = ""
        End If
    End Sub

    Protected Sub ddlGEMSSchool_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGEMSSchool.SelectedIndexChanged
        ClearStudentDetails()
        If ddlGEMSSchool.SelectedValue = "0" Then
            tr_ClientSchool.Visible = True
            tr_2.Visible = False
            DisableStudentDetail(False)
        Else
            tr_ClientSchool.Visible = False
            tr_2.Visible = True
            DisableStudentDetail(True)
        End If
    End Sub
    Protected Sub ddlVenue_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVenue.SelectedIndexChanged
        Try
            Dim PeriodTable As datatable
            PeriodTable = GetPeriodForExtraActivityVenue(ddlAcademicYear.selectedValue, Session("sBsuid"), ddlVenue.SelectedValue)
            If Not PeriodTable Is Nothing Then
                If PeriodTable.Rows.Count > 0 Then
                    txtFDT.text = CDate(PeriodTable.rows(0).item("EAD_FROMDT")).toString("dd/MMM/yyyy")
                    txtTDT.text = CDate(PeriodTable.rows(0).item("EAD_TODT")).toString("dd/MMM/yyyy")
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub

    Public Shared Function GetPeriodForExtraActivityVenue(ByVal ACD_ID As int16, ByVal BSU_ID As String, ByVal VenueID As int16) As datatable

        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim sqlParam(2) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@ACD_ID", ACD_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.varchar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@EAV_ID", VenueID, SqlDbType.Int)
            mTable = Mainclass.getDataTable("[TRANSPORT].[GetPeriodOfExtraActivityVenue]", sqlParam, str_conn)
            Return mTable
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub ClearStudentDetails()
        If ViewState("datamode") <> "add" Then
            Exit Sub
        End If
        Me.h_STUD_ID.Value = ""
        Me.txtStdNo.Text = ""
        Me.txtStudentname.Text = ""
        imgStudent_Click(Nothing, Nothing)
    End Sub
    Sub DisableStudentDetail(ByVal bDisable As Boolean)
        'academic year detail section
        tr_ACD_tr1.Disabled = bDisable
        tr_ACD_tr2.Disabled = bDisable
        'StudentDetails section
        tr_SDtr1.Disabled = bDisable
        tr_SDtr2.Disabled = bDisable
        tr_SDtr3.Disabled = bDisable
        tr_SDtr4.Disabled = bDisable
        imgBtnDOJ.Enabled = Not bDisable
        imgBtnDOB.Enabled = Not bDisable
        txtDob.Enabled = Not bDisable
        txtDoj.Enabled = Not bDisable
        txtFeeId.Enabled = Not bDisable
        txtFname_E.Enabled = Not bDisable
        txtMname_E.Enabled = Not bDisable
        txtLname_E.Enabled = Not bDisable
        'Parent details section
        tr_PDtr1.Disabled = bDisable
        tr_PDtr2.Disabled = bDisable
        tr_PDtr3.Disabled = bDisable
        txtFname_P.Enabled = Not bDisable
        txtMname_P.Enabled = Not bDisable
        txtLname_P.Enabled = Not bDisable
        txtFCOMPOBOX.Enabled = Not bDisable
        txtFMobile_No.Enabled = Not bDisable
        txtFEmail.Enabled = Not bDisable
    End Sub

    Protected Sub txtStudentname_TextChanged(sender As Object, e As EventArgs)
        If ViewState("datamode") <> "add" Then
            Exit Sub
        End If
        If Me.h_STUD_ID.Value <> "" AndAlso Convert.ToInt64(Me.h_STUD_ID.Value) > 0 Then
            Dim STU_SIBLING_ID As Long = Me.h_STUD_ID.Value
            'Dim dsSTU As DataSet = AccessStudentClass.GET_STUDENT_DETIALS(Me.h_STUD_ID.Value)
            Dim dsSTU As DataSet = GET_STUDENT_DETIALS(Me.h_STUD_ID.Value)
            If Not dsSTU Is Nothing AndAlso dsSTU.Tables.Count > 0 AndAlso dsSTU.Tables(0).Rows.Count > 0 Then
                Me.txtStdNo.Text = dsSTU.Tables(0).Rows(0)("STU_NO").ToString
                STU_SIBLING_ID = dsSTU.Tables(0).Rows(0)("STU_SIBLING_ID").ToString
                Dim DOB = Convert.ToDateTime(dsSTU.Tables(0).Rows(0)("STU_DOB")).ToString("dd/MMM/yyyy")
                Me.txtDob.Text = DOB 'TryCast(dsSTU.Tables(0).Rows(0)("STU_DOB"), DateTime)
                Dim DOJ = Convert.ToDateTime(dsSTU.Tables(0).Rows(0)("STU_DOJ")).ToString("dd/MMM/yyyy")
                Me.txtDoj.Text = DOJ
                Me.txtFname_E.Text = dsSTU.Tables(0).Rows(0)("STU_FIRSTNAME").ToString
                Me.txtMname_E.Text = dsSTU.Tables(0).Rows(0)("STU_MIDNAME").ToString
                Me.txtLname_E.Text = dsSTU.Tables(0).Rows(0)("STU_LASTNAME").ToString
                Me.txtStudentname.Text = Me.txtFname_E.Text & " " & Me.txtMname_E.Text & " " & Me.txtLname_E.Text
                If Not ddlGrade.Items.FindByValue(dsSTU.Tables(0).Rows(0)("STU_GRD_ID").ToString) Is Nothing Then
                    ddlGrade.SelectedValue = dsSTU.Tables(0).Rows(0)("STU_GRD_ID").ToString
                    callGrade_Section()
                End If
                If Not ddlFCOMEmirate.Items.FindByValue(dsSTU.Tables(0).Rows(0)("STS_FEMIR").ToString) Is Nothing Then
                    ddlFCOMEmirate.SelectedValue = dsSTU.Tables(0).Rows(0)("STS_FEMIR").ToString
                Else
                    If Not ddlFCOMEmirate.Items.FindByValue("DXB") Is Nothing Then
                        ddlFCOMEmirate.SelectedValue = "DXB"
                    End If
                End If
                Dim Nationality = dsSTU.Tables(0).Rows(0)("STU_NATIONALITY").ToString
                If Nationality <> "" AndAlso IsNumeric(Nationality) Then
                    Try
                        Me.ddlNationality_Birth.SelectedValue = Nationality
                    Catch
                    End Try
                End If
                Me.rdMale.Checked = IIf(dsSTU.Tables(0).Rows(0)("STU_GENDER").ToString = "M", True, False)
                Dim Religion = dsSTU.Tables(0).Rows(0)("STU_RLG_ID").ToString
                If Religion <> "" Then
                    Try
                        Me.ddlReligion.SelectedValue = Religion
                    Catch
                    End Try
                End If
                If BSU_bUPD_STU_CLIENT Then
                    txtFeeId.Text = dsSTU.Tables(0).Rows(0)("STU_FEE_ID").ToString
                End If
                If Session("sBsuId") = "500933" Then 'Al Nabooda, disable editing  details of GEMS school student
                    If ddlGEMSSchool.SelectedValue <> "0" Then 'if GEMS School
                        DisableStudentDetail(True)
                    Else
                        DisableStudentDetail(False)
                    End If
                End If
            End If
            Dim dsPARENT As DataSet = AccessStudentClass.GET_PARENT_DETIALS(STU_SIBLING_ID)
            If Not dsPARENT Is Nothing AndAlso dsPARENT.Tables.Count > 0 AndAlso dsPARENT.Tables(0).Rows.Count > 0 Then
                Me.txtFname_P.Text = dsPARENT.Tables(0).Rows(0)("STS_FFIRSTNAME").ToString
                Me.txtMname_P.Text = dsPARENT.Tables(0).Rows(0)("STS_FMIDNAME").ToString
                Me.txtLname_P.Text = dsPARENT.Tables(0).Rows(0)("STS_FLASTNAME").ToString
                Me.txtFCOMPOBOX.Text = dsPARENT.Tables(0).Rows(0)("STS_FCOMPOBOX").ToString
                Dim FCOMEmirate = dsPARENT.Tables(0).Rows(0)("STS_FEMIR").ToString
                If FCOMEmirate <> "" Then
                    Try
                        Me.ddlFCOMEmirate.SelectedValue = FCOMEmirate
                    Catch
                    End Try
                End If
                Me.txtFMobile_No.Text = dsPARENT.Tables(0).Rows(0)("STS_FMOBILE").ToString
                Me.txtFEmail.Text = dsPARENT.Tables(0).Rows(0)("STS_FEmail").ToString
            End If
        Else
            Me.txtDob.Text = ""
            Me.txtDoj.Text = ""
            Me.txtFname_E.Text = ""
            Me.txtMname_E.Text = ""
            Me.txtLname_E.Text = ""
            Me.ddlNationality_Birth.SelectedIndex = 0
            Me.rdMale.Checked = True
            Me.ddlReligion.SelectedIndex = 0
            Me.txtFname_P.Text = ""
            Me.txtMname_P.Text = ""
            Me.txtLname_P.Text = ""
            Me.txtFCOMPOBOX.Text = ""
            Me.ddlFCOMEmirate.SelectedIndex = 0
            Me.txtFMobile_No.Text = ""
            Me.txtFEmail.Text = ""
            Me.txtFeeId.Text = ""
        End If
    End Sub
End Class
