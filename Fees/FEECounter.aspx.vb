Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Fees_FEECounter
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

  
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim MainMnu_code As String

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            Page.Title = OASISConstants.Gemstitle
            'lblError.Text = ""
            txtBSUDESCR.Attributes.Add("ReadOnly", "ReadOnly")
            'txtCounterDescr.Attributes.Add("ReadOnly", "ReadOnly")
            txtUser.Attributes.Add("ReadOnly", "ReadOnly")
            h_FEE_COUNTER_ID.Value = 0
            ViewState("datamode") = "none"
            txtBSUID.Text = Session("sBsuid")
            txtBSUDESCR.Text = Session("sBsuName")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Or MainMnu_code <> OASISConstants.MNU_FEE_COUNTER Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                If Session("sBsuid") <> Nothing Then
                    txtBSUDESCR.Text = Session("BSU_Name")
                End If
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                'disable the control based on the rights 
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If ViewState("datamode") = "view" Then
                DissableControls(True)
                Dim vFCM_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FCM_ID").Replace(" ", "+"))
                FillDetails(vFCM_ID)
            End If
        End If
    End Sub

    Private Sub FillDetails(ByVal vFCM_ID As Integer)
        Dim vFEE_COUNTER As FEECounter
        vFEE_COUNTER = FEECounter.GetDetails(vFCM_ID)
        txtBSUID.Text = vFEE_COUNTER.BSU_ID
        txtBSUDESCR.Text = vFEE_COUNTER.BSU_NAME
        txtCounterDescr.Text = vFEE_COUNTER.FEE_COUNTER_DESCR
        txtUser.Text = vFEE_COUNTER.USER_NAME
        h_UserId.Value = vFEE_COUNTER.USER_ID
        h_FEE_COUNTER_ID.Value = vFEE_COUNTER.FEE_COUNTER_ID
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
        ClearAllFields()
        DissableControls(False)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Private Sub DissableControls(ByVal dissable As Boolean)
        txtBSUID.ReadOnly = dissable
        imgBSUNIT.Enabled = Not dissable
        imgUser.Enabled = Not dissable
        txtCounterDescr.ReadOnly = dissable
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        DissableControls(False)
        UtilityObj.beforeLoopingControls(Me.Page)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Not CheckBSUUnit(txtBSUID.Text) Then
            'lblError.Text = "Invalid Business Unit"
            usrMessageBar.ShowNotification("Invalid Business Unit", UserControls_usrMessageBar.WarningType.Danger)
            Return
        End If
        Dim vFEE_COUNTER As New FEECounter
        vFEE_COUNTER.BSU_ID = txtBSUID.Text
        vFEE_COUNTER.BSU_NAME = txtBSUDESCR.Text
        vFEE_COUNTER.FEE_COUNTER_DESCR = txtCounterDescr.Text
        vFEE_COUNTER.USER_NAME = txtUser.Text
        vFEE_COUNTER.USER_ID = h_UserId.Value
        vFEE_COUNTER.FEE_COUNTER_ID = h_FEE_COUNTER_ID.Value
        Dim objConn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
        Dim stTrans As SqlTransaction
        Dim errNo As Integer
        Try
            objConn.Close()
            objConn.Open()
            stTrans = objConn.BeginTransaction
            errNo = FEECounter.SaveDetails(vFEE_COUNTER, objConn, stTrans)
            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
            errNo = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
            If errNo = 0 Then
                stTrans.Commit()
                ClearAllFields()
                'lblError.Text = "Fee Counter setup saved sucessfully..."
                usrMessageBar.ShowNotification("Fee Counter setup saved sucessfully...", UserControls_usrMessageBar.WarningType.Success)
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                stTrans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(errNo)
                usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(errNo), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            stTrans.Rollback()
            'lblError.Text = UtilityObj.getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
            usrMessageBar.ShowNotification(UtilityObj.getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED), UserControls_usrMessageBar.WarningType.Danger)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub ClearAllFields()
        txtBSUDESCR.Text = ""
        txtBSUID.Text = ""
        txtCounterDescr.Text = ""
        txtUser.Text = ""
        h_FEE_COUNTER_ID.Value = 0
        h_UserId.Value = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearAllFields()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub txtBSUID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CheckBSUUnit(txtBSUID.Text)
    End Sub

    Private Function CheckBSUUnit(ByVal vBSUID As String) As Boolean
        Dim str_sql As String
        str_sql = "select isnull(BSU_NAME,'') from BUSINESSUNIT_M WHERE BSU_ID = '" & vBSUID & "'"
        Dim str_Conn As String = ConnectionManger.GetOASISConnectionString()
        Dim bsuName As String = SqlHelper.ExecuteScalar(str_Conn, CommandType.Text, str_sql)
        If bsuName <> "" Then
            txtBSUDESCR.Text = bsuName
            Return True
        Else
            txtBSUDESCR.Text = ""
            Return False
        End If
    End Function

End Class
