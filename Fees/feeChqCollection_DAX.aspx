<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeChqCollection_DAX.aspx.vb" Inherits="Fees_feeChqCollection_DAX" Theme="General" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function ChangeAllCheckBoxStates(checkState) {
            document.getElementById('<%=txtAmount.ClientID %>').value = 0
            var chk_state = document.getElementById("chkSelectall").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                        getAmount(document.forms[0].elements[i]);
                    }
            }
        }

        function getAmount(Obj) {
            var Amnt = document.getElementById('<%=txtAmount.ClientID %>').value;
      var SelAmnt = Obj.value

      if (isNaN(SelAmnt))
          SelAmnt = 0;

      var TotalAmnt = Number(Amnt) + Number(SelAmnt)
      if (Obj.checked == false)
          TotalAmnt = Number(Amnt) - Number(SelAmnt)
      if (Number(TotalAmnt) < 0)
          TotalAmnt = 0;
      document.getElementById('<%=txtAmount.ClientID %>').value = TotalAmnt.toFixed(2);
    }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:TextBox ID="txtDocDate" runat="server" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td align="left" class="matters" width="20%"><span class="field-label">Select Bank</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlHeaderBank" runat="server" DataSourceID="SqlHeaderBank"
                                DataTextField="BANK" DataValueField="BANK" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="tr_Businessunit" runat="server">
                        <td align="left" class="matters" width="20%"><span class="field-label">Business Unit</span> </td>
                        <td class="matters" align="left">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" OnSelectedIndexChanged="ddlBusinessunit_SelectedIndexChanged"
                                TabIndex="5">
                            </asp:DropDownList></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValChqDate" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="4">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" Width="100%" CssClass="table table-row table-bordered">
                                <Columns>
                                    <asp:TemplateField Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVHD_CHQID" runat="server" Text='<%# Bind("VHD_CHQID") %>'></asp:Label>
                                            <asp:Label ID="lblVHD_COL_ID" runat="server" Text='<%# Bind("VHD_COL_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <input id="chkSelectall" type="checkbox" style="cursor: hand" onclick="ChangeAllCheckBoxStates(true);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkPosted" runat="server" type="checkbox" style="cursor: hand" value='<%# Bind("AMOUNT") %>' onclick="getAmount(this)" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="DocNo">
                                        <HeaderTemplate>
                                            Doc No<br />
                                            <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearch6" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("VHD_DOCNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Bank<br />
                                            <asp:TextBox ID="txtBank" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch1" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblBank" runat="server" Text='<%# Bind("BANK") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Account<br />

                                            <asp:TextBox ID="txtCreditor" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch2" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccount" runat="server" Text='<%# Bind("ACCOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Chq. No.<br />
                                            <asp:TextBox ID="txtChqNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration4" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblCheqNo" runat="server" Text='<%# Bind("VHD_CHQNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Chq. Date">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtChqDate" runat="server" Width="89px" Text='<%# bind("VHD_CHQDT","{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtChqDate"
                                                ErrorMessage="Enter Proper Date" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                                ValidationGroup="ValChqDate">*</asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtChqDate"
                                                ErrorMessage="Enter Cheq. Date" ValidationGroup="ValChqDate">*</asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Currency<br />
                                            <asp:TextBox ID="txtCurrency" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnCurrencySearch5" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblCurrency" runat="server" Text='<%# Bind("CUR_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("AMOUNT")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="field-label">Amount</span></td>
                        <td>
                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="ValChqDate" />
                            <asp:Button ID="btnPrint" runat="server" Text="Print" /></td>
                    </tr>
                </table>
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <asp:SqlDataSource ID="SqlHeaderBank" runat="server" ConnectionString="<%$ ConnectionStrings:OASIS_DAXConnectionString %>"
                    SelectCommand="SELECT distinct  [BANK] FROM FIN.[vw_OSA_CHEQCOLLECTION_FEE] WHERE ([VHH_BSU_ID] = @VHH_BSU_ID)">
                    <SelectParameters>
                        <asp:SessionParameter Name="VHH_BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:HiddenField ID="h_DOC_NO" runat="server" />
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtDocDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
        

 <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
