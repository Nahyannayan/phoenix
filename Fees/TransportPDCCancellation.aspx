﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="TransportPDCCancellation.aspx.vb" Inherits="Fees_TransportPDCCancellation" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <%--<script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />--%>
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>
    <%--<link href="../cssfiles/title.css" rel="stylesheet" />--%>
    <link href="../cssfiles/popup.css" rel="stylesheet" />
    <style type="text/css">
        .selectedRow {
            border-bottom: #bbd9ee 1px solid;
            text-align: left;
            padding-bottom: 6px;
            padding-left: 4px;
            padding-right: 4px;
            font-size: 0.99em;
            border-top: #bbd9ee 1px solid;
            padding-top: 6px;
            background-color: #dce88e;
        }

       

        .RadComboBox_Default .rcbReadOnly {
            background-image:none !important;
            background-color:transparent !important;

        }
        .RadComboBox_Default .rcbDisabled {
            background-color:rgba(0,0,0,0.01) !important;
        }
        .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
            background-color:transparent !important;
            border-radius:0px !important;
            border:0px !important;
            padding:initial !important;
            box-shadow: none !important;
        }
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <script type="text/javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Fee Collection PDC Cancellation
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table align="center" width="100%">
        <tr>
            <td align="center">                
                    <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                    <uc1:usrMessageBar runat="server" ID="usrMessageBar" />       
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlPage" runat="server">
        <table width="100%">
            
            <tr>
                <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                
                <td align="left"  width="30%">
                    <telerik:RadComboBox ID="ddlBusinessunit" runat="server" Filter="Contains" RenderMode="Lightweight" Width="100%" AutoPostBack="true" ZIndex="2000" ToolTip="Type in unit name or short code" DataSourceID="odsSERVICES_BSU_M" DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID"></telerik:RadComboBox>
                </td>
                <td align="left" width="20%"></td>
                <td align="left" width="30%"></td>
            </tr>
            <tr>
                <td align="left"><span class="field-label">Academic Year</span></td>
               
                <td align="left">
                    <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td align="left"><span class="field-label">Date</span></td>
                
                <td align="left">
                    <asp:TextBox ID="txtDocDate" runat="server"></asp:TextBox>
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif" />
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="ImageButton2" TargetControlID="txtDocDate">
                    </ajaxToolkit:CalendarExtender>
                    <asp:RequiredFieldValidator ID="rfvtxtOPendte" runat="server" ControlToValidate="txtDocDate" CssClass="validate_error" Display="Dynamic" Text="The date is required!" ValidationGroup="DOCDATE" />
                    <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtDocDate" Display="Dynamic" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$" ValidationGroup="DOCDATE">*</asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td align="left"><span class="field-label">Student Type</span></td>
               
                <td align="left">
                    <asp:RadioButton ID="rbtnEnrollment" runat="server" CssClass="field-label" Checked="True" GroupName="STYPE" Text="Enrollment" AutoPostBack="true" />
                    <asp:RadioButton ID="rbtnEnquiry" runat="server" CssClass="field-label" GroupName="STYPE" Text="Enquiry" AutoPostBack="true" />
                </td>
            
                <td align="left"><span class="field-label">Student</span></td>
                
                <td align="left">
                    <table width="100%">
                        <tr>
                            <td align="left" width="40%">
                                <asp:TextBox ID="txtStdNo" runat="server" AutoPostBack="True"></asp:TextBox>
                    <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getStudent();return false;" />
                            </td>
                            <td align="left" width="60%">
                                <asp:TextBox ID="txtStudentname" runat="server"></asp:TextBox>
                    <asp:ImageButton ID="imgStudentN" runat="server" Style="display: none;" ImageUrl="~/Images/forum_search.gif"
                        OnClick="imgStudentN_Click" />
                    <asp:HiddenField ID="h_STUD_ID" runat="server" />
                            </td>
                        </tr>
                    </table>
                    
                    

                </td>
            </tr>
            <tr>
                <td align="left"><span class="field-label">Select Receipt</span>
                </td>
                
                <td align="left" colspan="3">
                    <asp:GridView ID="gvReceipts" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row" width="100%" AllowPaging="True" PageSize="5">
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle CssClass="selectedRow" />
                        <HeaderStyle CssClass="gridheader_pop" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                        <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblFCLID" runat="server" Text='<%# Bind("FCL_ID")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Receipt Date" DataField="FCL_DATE" DataFormatString="{0:dd/MMM/yyyy}" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Receipt No" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRecNo" Text='<%# Bind("FCL_RECNO") %>' runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Amount" DataField="FCL_AMOUNT" DataFormatString="{0:N}" ItemStyle-HorizontalAlign="Right" />--%>
                            <asp:CommandField ShowSelectButton="True" ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="left"><span class="field-label">PDC</span>
                </td>
                
                <td align="left" colspan="3">
                    <asp:GridView ID="gvPDC" runat="server" AutoGenerateColumns="False" ShowFooter="True" width="100%"  CssClass="table table-bordered table-row">
                        <RowStyle CssClass="griditem_dark"/>
                        <SelectedRowStyle CssClass="selectedRow" />
                        <HeaderStyle CssClass="gridheader_pop" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                        <Columns>
                            <asp:TemplateField Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblFCQID" runat="server" Text='<%# Eval("FCD_UNIQCHQ_ID")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <a href="javascript:expandcollapse('div<%# Eval("FCD_UNIQCHQ_ID")%>', 'one');">
                                        <img id="imgdiv<%# Eval("FCD_UNIQCHQ_ID")%>" alt="Click to show/hide details of cheque no <%# Eval("FCQ_CHQNO")%>" width="14px" height="20px" border="0" src="../Images/expand.jpg" />
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelectPDC" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Chq.No">
                                <ItemTemplate>
                                    <asp:Label ID="lblChqNo" runat="server" Text='<%# Bind("FCQ_CHQNO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField HeaderText="Chq.Date" DataField="FCQ_DATE" DataFormatString="{0:dd/MMM/yyyy}" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Amount" DataField="FCQ_AMOUNT" DataFormatString="{0:N}" ItemStyle-HorizontalAlign="Right">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Bank" DataField="BNK_DESCRIPTION" />
                            <asp:BoundField HeaderText="Emirate" DataField="EMR_DESCR" />
                            <asp:BoundField HeaderText="Receipt Date" DataField="FCL_DATE" DataFormatString="{0:dd/MMM/yyyy}" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td colspan="100%" width="100%">
                                            <div id="div<%# Eval("FCD_UNIQCHQ_ID") %>" style="display: none; position: relative; left: 5px; overflow: auto; width: 96%">
                                                <asp:GridView ID="gvPDCSplit" runat="server"  CssClass="table table-bordered table-row" Width="100%" AutoGenerateColumns="false" RowStyle-Font-Size="Small" >
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Student Id" DataField="STU_NO" />
                                                        <asp:BoundField HeaderText="Name" DataField="STU_NAME" />
                                                        <asp:BoundField HeaderText="Receipt.No" DataField="FCL_RECNO" />
                                                        <asp:BoundField HeaderText="Fee Type" DataField="FEE_DESCR" />
                                                        <asp:BoundField HeaderText="Amount" DataField="FCA_AMOUNT" DataFormatString="{0:N}" ItemStyle-HorizontalAlign="Right" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="left"><span class="field-label">Comments</span>
                </td>
                
                <td align="left" colspan="3">
                    <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" SkinID="MultiText" MaxLength="500" Width="50%"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtTitle" ControlToValidate="txtComments"
                        Display="Dynamic" Text="* Narration is required!" runat="server" ValidationGroup="DOCDATE" CssClass="validate_error" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table border="0" cellpadding="0" cellspacing="0"
        align="center" width="100%">
        <tr>
            <td align="center">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" ValidationGroup="DOCDATE" /><asp:Button ID="btnApprove" runat="server" Text="Approve" Visible="false" CssClass="button" /><asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="button" /><asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="button" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server"
                    TargetControlID="btnApprove"
                    ConfirmText="You are about to Approve the PDC cancellation, Continue?" />
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server"
                    TargetControlID="btnDelete"
                    ConfirmText="Are you sure you want to delete PDC cancellation?" />
            </td>
        </tr>
    </table>
    <div id="alertpopup" class="darkPanelM" runat="server" style="display: none; z-index: 3000 !important;">
        <div id="divboxpanelconfirm" runat="server" style="height: 50%; width: 25%; margin-left: 10%; margin-top: 15%;">
            <div class="holderInner" style="height: 90%; width: 98%;">
                <center>
                    <table cellpadding="5" cellspacing="2" border="3" style="height: 90% !important; width: 100% !important; overflow-y: scroll;"
                        class="tableNoborder">
                        <tr class="subheader_img">
                            <td style="border: 0 !important;">
                                <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
                                INFO
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" style="border: 0 !important;">
                                <span class="ui-icon ui-icon-info" style="float: left; margin: 0 7px 7px 0;"></span>
                                <asp:Label ID="lblMsg" CssClass="matters" runat="server"></asp:Label><br />
                                <br />
                                <br />

                                <asp:Label ID="lblMsg2" runat="server" BackColor="Beige" Font-Names="Verdana" ForeColor="Navy"
                                    Font-Size="10pt"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="border: 0;">
                                <asp:Button ID="btnOkay" runat="server" CausesValidation="False" CssClass="button"
                                    Text="OK" />
                                <asp:Button ID="btnClose" runat="server" CausesValidation="False" CssClass="button"
                                    Text="CANCEL" />
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
    <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" SelectMethod="SERVICES_BSU_M"
        TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <table width="100">
        <tr>
            <td width="100%" align="center">
                <asp:Panel ID="pnlViewPopup" runat="server" CssClass="RadDarkPanlvisible" Style="display: none; height: 900px !important; width: 100% !important;">
                    <div class="RadPanelQual" runat="server" id="divViewPopup">
                        <table width="100%">
                            <tr>
                                <td>
                                    <iframe id="ifrmViewPopup" runat="server"></iframe>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function expandcollapse(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "block";
                if (row == 'alt') {
                    img.src = "../Images/collapse.jpg";
                }
                else {
                    img.src = "../Images/collapse.jpg";
                }
                img.alt = "Close to view cheque(s)";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../Images/expand.jpg";
                }
                else {
                    img.src = "../Images/expand.jpg";
                }
                img.alt = "Expand to show receipts";
            }
        }

        function getStudent() {
            var sFeatures, url, stuName;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            stuName = '<%= txtStudentname.ClientID%>';
            var NameandCode;
            var result;
            var prevAcd = document.getElementById('<%= h_PREV_ACD.ClientID %>').value;
            //var BSU_ID = document.getElementById('<%= ddlBusinessunit.ClientID %>').value;
            var dropFind = $find("<%= ddlBusinessunit.ClientID%>");
            var BSU_ID = dropFind.get_value();
            var ACD_ID = document.getElementById('<%= ddlAcdYear.ClientID%>').value;
            if (document.getElementById('<%= rbtnEnquiry.ClientID%>').checked == false)
                url = "ShowStudentTransport.aspx?type=STU_TRAN&bsu=" + BSU_ID + '&acd=' + ACD_ID + '&prevacd=' + prevAcd + '&codeorname=' + document.getElementById(stuName).value;
            else
                url = "ShowStudentTransport.aspx?type=ENQ_COMP&COMP_ID=-1&bsu=" + BSU_ID;
            <%-- result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById('<%= h_STUD_ID.ClientID%>').value = NameandCode[0];
            document.getElementById(stuName).value = NameandCode[1];
            document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];
            return true;--%>
            var oWnd = radopen(url, "pop_getstudent");
        }


        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            stuName = '<%= txtStudentname.ClientID%>';
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%= h_STUD_ID.ClientID%>').value = NameandCode[0];
                document.getElementById(stuName).value = NameandCode[1];
                document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];
                __doPostBack('<%= txtStdNo.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        
        <Windows>
            <telerik:RadWindow ID="pop_getstudent" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <asp:HiddenField ID="h_PREV_ACD" runat="server" />

                </div>
            </div>
        </div>

</asp:Content>

