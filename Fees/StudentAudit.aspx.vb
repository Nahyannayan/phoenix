﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration


Partial Class Fees_StudentAudit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ViewState("STU_ID") = 0
            If Not Request.QueryString("ID") Is Nothing Then
                ViewState("STU_ID") = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
            End If

            LOAD_RPT_HEADER()
            LOAD_AUDIT_HEADERS()
            LOAD_TCRECORDS()
            LOAD_TC_AUDIT()
            LOAD_CRITICALDATA_CHANGES()
            LOAD_STUDENT_SERVICES_AUDIT()
        End If
    End Sub

    Sub LOAD_RPT_HEADER()

        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(lstrConn)
        Try
            imgLogo.ImageUrl = "GetLogo.aspx?BSU_ID=" & Session("sBsuid")
            lblHeader.Text = "Report Name: Student Audit"
            Dim cur_dat_time As String
            cur_dat_time = DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt")
            lbldate.Text = "Print Date: " + cur_dat_time
            lblusername.Text = "Printed By: " + Session("sUsr_name")

            Dim adpt As New SqlDataAdapter
            Dim ds As New DataSet

            Dim cmd As New SqlCommand("ReportHeader_Subreport", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpIMG_BSU_ID As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 100)
            sqlpIMG_BSU_ID.Value = Session("sbSUID")
            cmd.Parameters.Add(sqlpIMG_BSU_ID)

            Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
            sqlpIMG_TYPE.Value = "LOGO"
            cmd.Parameters.Add(sqlpIMG_TYPE)

            adpt.SelectCommand = cmd
            objConn.Open()
            adpt.Fill(ds)
            If ds.Tables(0).Rows.Count > 0 Then
                lblSchool.Text = ds.Tables(0).Rows(0).Item("BSU_NAME")
            End If

        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub
    Sub LOAD_AUDIT_HEADERS()
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("EXEC dbo.GET_STUDENT_AUDIT_HEADER_DATA @STU_TYPE = @STUTYPE, @STU_BSU_ID= @BSU_ID, @STU_ID = @STUID, @DATATYPE = @DATATYPE")

            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STUID", SqlDbType.BigInt)
            pParms(0).Value = ViewState("STU_ID")
            pParms(1) = New SqlClient.SqlParameter("@STUTYPE", SqlDbType.VarChar, 2)
            pParms(1).Value = "S"
            pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = Session("sBsuId")
            pParms(3) = New SqlClient.SqlParameter("@DATATYPE", SqlDbType.VarChar, 10)
            pParms(3).Value = "STUDENT"

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                dtlStudentInfo.HeaderTemplate = New HeaderTemplate(ds.Tables(0).Rows(0)("HEADING").ToString)
                dtlStudentInfo.HeaderStyle.CssClass = "title-bg"
                dtlStudentInfo.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                dtlStudentInfo.DataSource = ds
                dtlStudentInfo.DataBind()
            End If
            'ReDim Preserve pParms(3)
            pParms(3) = New SqlClient.SqlParameter("@DATATYPE", SqlDbType.VarChar, 10)
            pParms(3).Value = "STATUS"

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                dtlStudentServiceInfo.HeaderTemplate = New HeaderTemplate(ds.Tables(0).Rows(0)("HEADING").ToString)
                dtlStudentServiceInfo.HeaderStyle.CssClass = "title-bg"
                dtlStudentServiceInfo.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                dtlStudentServiceInfo.DataSource = ds
                dtlStudentServiceInfo.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub LOAD_TCRECORDS()
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("EXEC [STU].[AUDIT_TC] @STU_ID = @STUID")

            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STUID", SqlDbType.BigInt)
            pParms(0).Value = ViewState("STU_ID")

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                gvTCRecords.DataSource = ds
                gvTCRecords.DataBind()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Sub LOAD_TC_AUDIT()
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("EXEC [STU].[AUDIT_TCTRAN] @STU_ID = @STUID")

            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STUID", SqlDbType.BigInt)
            pParms(0).Value = ViewState("STU_ID")

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                gvTCTransAudit.DataSource = ds
                gvTCTransAudit.DataBind()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Sub LOAD_CRITICALDATA_CHANGES()
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("EXEC [STU].[AUDIT_CRITICAL_CHANGE] @STU_ID = @STUID")

            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STUID", SqlDbType.BigInt)
            pParms(0).Value = ViewState("STU_ID")

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                gvCriticalChanges.DataSource = ds
                gvCriticalChanges.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub LOAD_STUDENT_SERVICES_AUDIT()
        Try
            Dim ds As New DataSet
            Dim Qry As New StringBuilder
            Qry.Append("EXEC STU.[SERVICES_TRANS_AUDIT] @STU_ID = @STUID")

            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STUID", SqlDbType.BigInt)
            pParms(0).Value = ViewState("STU_ID")

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, Qry.ToString, pParms)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                gvServiceAudit.DataSource = ds
                gvServiceAudit.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class


