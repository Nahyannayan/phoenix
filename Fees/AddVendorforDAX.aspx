﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="AddVendorforDAX.aspx.vb" Inherits="Fees_AddVendorforDAX" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
        <style>
        .RadComboBoxDropDown .rcbItem>label, .RadComboBoxDropDown .rcbHovered>label, .RadComboBoxDropDown .rcbDisabled>label, .RadComboBoxDropDown .rcbLoading>label, .RadComboBoxDropDown .rcbCheckAllItems>label, .RadComboBoxDropDown .rcbCheckAllItemsHovered>label {
    display: inline;
    float: left;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border:0!important;
}
.RadComboBox_Default .rcbInner {
    padding:10px;
    border-color: #dee2da!important;
    border-radius: 6px!important;
    box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
    width:80%;
    background-image: none !important;
    background-color:transparent !important;
}
.RadComboBox_Default .rcbInput {
    font-family:'Nunito', sans-serif !important;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border: 0!important;
    box-shadow: none;
}
.RadComboBox_Default .rcbActionButton {
    border: 0px;
    background-image: none !important;
    height:100% !important;
    color:transparent !important;
    background-color:transparent !important;
}
    </style>
 
  <script>    
      function GetEMPName() {

            var oWnd = radopen("../Accounts/accShowEmpDetail.aspx?id=EN&Bsu_id=" + GetBsuId(), "pop_student");
        }

        function OnClientClose1(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                 document.getElementById('<%=h_EMP_ID.ClientID%>').value = NameandCode[1];
                document.getElementById('<%=txtEmployee.ClientID%>').value = NameandCode[0];
		         __doPostBack('<%=txtEmployee.ClientID%>', 'TextChanged');
            }
        }
  
function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>

       <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Add Vendor"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table style="width: 100%;">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="Error"></asp:Label><asp:ValidationSummary ID="ValidationSummary1" runat="server"
                                    CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <table  width="100%">
                    <tr>
                        <td class="matters" align="left" width="20%"><span class="field-label" >Vendor Code</span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtVendorCode"
                            ErrorMessage="Vendor code required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></td>
                        <td class="matters" align="left" width="30%">
                            <asp:TextBox ID="txtVendorCode" runat="server"></asp:TextBox>

                        </td>
                        <td class="matters" align="left" width="20%"><span class="field-label">Working Unit</span></td>
                        <td class="matters" align="left" width="30%">
                            <telerik:RadComboBox ID="ddlWorkUnit" runat="server" RenderMode="Lightweight" AutoPostBack="True" Filter="Contains" Width="100%" ZIndex="2000"></telerik:RadComboBox>

                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="left"><span class="field-label">Select Employee</span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmployee"
                            ErrorMessage="Please select the Employee" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></td>
                        <td class="matters" align="left">
                            <asp:TextBox ID="txtEmployee" runat="server" AutoPostBack="true" OnTextChanged="txtEmployee_TextChanged" placeholder="Type in or Search Employee Number or Name" ></asp:TextBox>
                            <asp:ImageButton ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetEMPName();return false;" />
                            <asp:LinkButton ID="lbtnClearEmployee" runat="server" CausesValidation="False">Clear</asp:LinkButton>
                            <asp:LinkButton ID="lbtnLoadEmpDetail" runat="server" CausesValidation="False" OnClick="lbtnLoadEmpDetail_Click"></asp:LinkButton>
                          <%--  <ajaxToolkit:TextBoxWatermarkExtender ID="TBWE3" runat="server"
                                TargetControlID="txtEmployee"
                                WatermarkText="Type in or Search Employee Number or Name"
                                WatermarkCssClass="watermarked" />--%>
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server"
                                TargetControlID="lbtnClearEmployee"
                                ConfirmText="Are you sure you want to remove the Employee selection?" />
                            <asp:HiddenField ID="h_EMP_ID" runat="server" />
                        </td>
                        <td align="center" colspan="2"  >
                            <table width="100%"  >
                                <tr>
                                    <td align="left" rowspan="5" >
                                        <asp:Image ID="imgEmpImage" runat="server" Height="132px" Width="120px" /></td>
                                    <td align="left"><span class="field-label">Employee Id</span></td>
                                    <td align="left" class="matters" >
                                        <asp:Label ID="lblEmpNo" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters2" ><span class="field-label">Name</span></td>
                                    <td align="left" class="matters" >
                                        <asp:Label ID="lblEmpName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters2" ><span class="field-label">Department</span></td>
                                    <td align="left" class="matters" >
                                        <asp:Label ID="lblDept" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters2" ><span class="field-label">Designation</span>
                                    </td>
                                    <td align="left" class="matters" >
                                        <asp:Label ID="lblEmpDesig" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters2" ><span class="field-label">Working Unit</span></td>
                                    <td align="left" class="matters" >
                                        <asp:Label ID="lblWorkingUnit" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="left"><span class="field-label">Business Unit</span>&nbsp;<a tooltip="select the business unit to which the vendor is added"></a></td>
                        <td class="matters" align="left" colspan="2">
                            <telerik:RadComboBox ID="ddlBSU" runat="server" Filter="Contains" RenderMode="Lightweight" Width="100%" ZIndex="2000"></telerik:RadComboBox>
                            
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server"
                                TargetControlID="btnSave"
                                ConfirmText="Are you sure you want to save the Vendor code?" />
                        </td>
                        <td ></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center"><asp:Button ID="btnSave" runat="server" Text="SAVE" CssClass="button" ValidationGroup="MAINERROR" /></td>
                    </tr>
                    <tr id="trheading" runat="server">
                        <td class="title-bg" colspan="4" align="left">
                            <asp:Label ID="lblSubHeading" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="center" colspan="4">
                            <asp:GridView ID="gvVendor" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                Width="100%" AllowPaging="True" PageSize="30" DataKeyNames="VDR_ID,EMP_ID">
                                <Columns>
                                    <asp:TemplateField HeaderText="BSU">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgvBSU_SHORT" runat="server" Text='<%# Bind("BSU_SHORTNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Code">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgvVDR_CODE" runat="server" Text='<%# Bind("VDR_CODE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="VDR_LOGDT" HeaderText="Added On"  DataFormatString="{0:dd/MMM/yyyy HH:mm:ss}"  ItemStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnDelete" runat="server" CommandArgument='<%# Bind("VDR_ID")%>' OnClick="lbtnDelete_Click">Delete</asp:LinkButton>
                                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server"
                                                TargetControlID="lbtnDelete"
                                                ConfirmText="Are you sure you want to delete the Vendor code?" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            InitEmployeeAutoComplete();
        });
        function InitializeRequest(sender, args) {
        }
        function EndRequest(sender, args) {
            // after update occur on UpdatePanel re-init the Autocomplete
            InitEmployeeAutoComplete();
        }
        function InitEmployeeAutoComplete() {

            $("#<%=txtEmployee.ClientID%>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "AddVendorforDAX.aspx/GetEmployee",
                        data: "{ 'BSUID': '" + GetBsuId() + "','prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1] + ' - ' + item.split('-')[0],
                                    val: item.split('-')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=h_EMP_ID.ClientID%>").val(i.item.val);
                    document.getElementById('<%=lbtnLoadEmpDetail.ClientID%>').click();
                },
                minLength: 1
            });
        }
        function GetBsuId() {
            var dropFind = $find("<%= ddlWorkUnit.ClientID%>");
            var valueFind = dropFind.get_value();
            return valueFind;
        }
        function GetEmpId() {
            alert($("#<%=h_EMP_ID.ClientID%>").val());
            return false;
        }

        <%--function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EN&Bsu_id=" + GetBsuId(), "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_EMP_ID.ClientID%>').value = NameandCode[1];
                document.getElementById('<%=txtEmployee.ClientID%>').value = NameandCode[0];
                return true;
            }
            return false;
        }--%>
    </script>
</asp:Content>

