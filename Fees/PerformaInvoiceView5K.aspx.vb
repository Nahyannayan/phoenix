Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_PerformaInvoiceView5K
    Inherits BasePage
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "F100170" And ViewState("MainMnu_code") <> "S400012") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                hlAddNew.NavigateUrl = "PerformaInvoice5K.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                ddAcademic.DataBind()
                ddAcademic.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                Session("liUserList") = New ArrayList
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim str_Sql As String = ""
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""

            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then

                '   --- FILTER CONDITIONS ---
                '   -- 1   FPH_INOICENO
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtInvNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FPH_INOICENO", lstrCondn1)

                '   -- 2  Date
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "DATE", lstrCondn2)

                '   -- 3  COMP_NAME
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtCompany")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "COMP_NAME", lstrCondn3)

                '   -- 4   ACAD_GRADE
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ACAD_GRADE", lstrCondn4)

                '   -- 5  STU_NO
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStudNo")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn5)

                '   -- 5  STU_NAME
                larrSearchOpr = h_Selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStudName")
                lstrCondn6 = txtSearch.Text
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STUD_NAME", lstrCondn6)
            End If

            Dim str_cond As String = String.Empty
            str_cond = " BSU_ID = '" & Session("sBSUID") & "' AND FPH_ACD_ID = " & ddAcademic.SelectedValue
            If radStudent.Checked Then
                str_Sql = "select MAX(isnull(COMP_NAME,'')) COMP_NAME,DATE, " & _
                " MAX(isnull(STUD_NAME,'')) STUD_NAME, MAX(isnull(STU_NO,'')) STU_NO,MAX(GRD_SEC) GRD_SEC," & _
                " MAX(isnull(ACAD_GRADE,'')) ACAD_GRADE , MAX(isnull(FPH_COMP_ID,'')) FPH_COMP_ID, " & _
                " FPH_INOICENO from FEES.vw_OSO_FEES_PERFORMAINVOICE " & _
                " WHERE FPH_STU_TYPE='S' AND ISNULL(FPH_bAdvanceBooking,0) = 1  AND " & str_cond & str_Filter & _
                " GROUP BY FPH_INOICENO, DATE ORDER BY DATE DESC,FPH_INOICENO DESC"
            ElseIf radEnquiry.Checked Then
                str_Sql = "select MAX(isnull(COMP_NAME,'')) COMP_NAME,DATE, " & _
                " MAX(isnull(STUD_NAME,'')) STUD_NAME, MAX(isnull(STU_NO,'')) STU_NO,MAX(GRD_SEC) GRD_SEC," & _
                " MAX(isnull(ACAD_GRADE,'')) ACAD_GRADE , MAX(isnull(FPH_COMP_ID,'')) FPH_COMP_ID, " & _
                " FPH_INOICENO from FEES.vw_OSO_FEES_PERFORMAINVOICE_ENQ " & _
                " WHERE FPH_STU_TYPE='E' AND ISNULL(FPH_bAdvanceBooking,0) = 1 AND " & str_cond & str_Filter & _
                " GROUP BY FPH_INOICENO, DATE ORDER BY DATE DESC,FPH_INOICENO DESC"
            End If
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            'gvJournal.DataBind()
            txtSearch = gvJournal.HeaderRow.FindControl("txtInvNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtCompany")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtStudNo")
            txtSearch.Text = lstrCondn5

            txtSearch = gvJournal.HeaderRow.FindControl("txtStudName")
            txtSearch.Text = lstrCondn6
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        SetChk(Page)
        GridBind()
        SetChk(Page)
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblInvNo As New Label
            Dim vCOMP_ID As Integer
            Dim str_ENQ As String
            lblInvNo = TryCast(e.Row.FindControl("lblInvNo"), Label)
            Dim lblCOMP_ID As Label = sender.Parent.parent.findcontrol("lblCOMP_ID")
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing AndAlso lblInvNo IsNot Nothing Then
                If lblCOMP_ID IsNot Nothing Then
                    vCOMP_ID = lblCOMP_ID.Text
                Else
                    vCOMP_ID = -1
                End If
                If radEnquiry.Checked Then
                    str_ENQ = "ENQ"
                Else
                    str_ENQ = "STUD"
                End If
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "PerformaInvoice5K.aspx?inv_no=" & Encr_decrData.Encrypt(lblInvNo.Text) & _
                "&COMP_ID=" & Encr_decrData.Encrypt(vCOMP_ID) & _
                "&MainMnu_code=" & Request.QueryString("MainMnu_code") & _
                "&datamode=" & ViewState("datamode") & "&TYPE=" & Encr_decrData.Encrypt(str_ENQ)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub lblPrintReciept_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblINV_NO As Label = sender.Parent.parent.findcontrol("lblInvNo")
        Dim lblCOMP_ID As Label = sender.Parent.parent.findcontrol("lblCOMP_ID")
        Dim lblCompanyName As Label = New Label
        lblCompanyName = sender.Parent.parent.FindControl("lblCompanyName")
        If lblCompanyName.Text <> "" Then
            Session("ReportSource") = FEEPERFORMAINVOICE.PrintGroup(lblINV_NO.Text, True, Session("sBsuid"), Session("sUsr_name"))
        Else
            Session("ReportSource") = FEEPERFORMAINVOICE.PrintReceipt("'" & lblINV_NO.Text & "'", False, Session("sBsuid"), radEnquiry.Checked, Session("sUsr_name"), True)
        End If
        FEEPERFORMAINVOICE.F_SAVEFEE_PERFORMAINVOICE_H_PRINTCOUNT(Session("sBsuid"), lblINV_NO.Text, Session("sUsr_name"))
        h_print.Value = "print"
    End Sub

    Protected Function PrintReceiptX(ByVal vINV_NOs As ArrayList) As MyReportClass
        Dim ienum As IEnumerator = vINV_NOs.GetEnumerator
        Dim str_invnos As String = String.Empty
        Dim str_invnos_audit As String = String.Empty
        Dim comma As String = String.Empty
        Dim pipe As String = String.Empty
        While (ienum.MoveNext())
            str_invnos += comma & " '" & ienum.Current & "'"
            comma = ", "
            str_invnos_audit += pipe & ienum.Current
            pipe = "|"
        End While
        h_print.Value = "print"
        FEEPERFORMAINVOICE.F_SAVEFEE_PERFORMAINVOICE_H_PRINTCOUNT(Session("sBsuid"), str_invnos_audit, Session("sUsr_name"))
        Return FEEPERFORMAINVOICE.PrintReceipt(str_invnos, False, Session("sBsuid"), radEnquiry.Checked, Session("sUsr_name"), True)
    End Function

    Protected Sub PrintReceiptGroup(ByVal vINV_NOs As ArrayList)
        Dim ienum As IEnumerator = vINV_NOs.GetEnumerator
        Dim str_invnos As String = String.Empty
        Dim str_invnos_audit As String = String.Empty
        Dim comma As String = String.Empty
        Dim pipe As String = String.Empty
        While (ienum.MoveNext())
            str_invnos += ienum.Current & "@"
            str_invnos_audit += pipe & ienum.Current
            pipe = "|"
        End While
        If str_invnos = "" Then
            lblError.Text = "Please Select Invoice(s)!!!"
            Exit Sub
        Else
            h_print.Value = "print"
            FEEPERFORMAINVOICE.F_SAVEFEE_PERFORMAINVOICE_H_PRINTCOUNT(Session("sBsuid"), str_invnos_audit, Session("sUsr_name"))
            Session("ReportSource") = FEEPERFORMAINVOICE.PrintGroup(str_invnos, True, Session("sBsuid"), Session("sUsr_name"))
        End If
    End Sub


    Protected Sub ddAcademic_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddAcademic.SelectedIndexChanged
        GridBind()
        Session("liUserList") = New ArrayList
    End Sub

    Protected Sub radStudent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStudent.CheckedChanged
        GridBind()
        Session("liUserList") = New ArrayList
    End Sub

    Protected Sub radEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnquiry.CheckedChanged
        GridBind()
        Session("liUserList") = New ArrayList
    End Sub

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        SetChk(Page)
        If Session("liUserList").Count > 0 Then
            Session("ReportSource") = PrintReceiptX(Session("liUserList"))
        Else
            lblError.Text = "Please Select Invoice(s)!!!"
        End If
    End Sub

    Function isAgainstCompany() As Boolean
        Session("liUserList") = New ArrayList
        For Each dr As GridViewRow In gvJournal.Rows
            Dim chkControl As HtmlInputCheckBox = New HtmlInputCheckBox()
            Dim lblCompanyName As Label = New Label
            chkControl = dr.FindControl("chkControl")
            lblCompanyName = dr.FindControl("lblCompanyName")
            If chkControl IsNot Nothing And chkControl.Checked Then
                If lblCompanyName.Text = "" Then
                    Return True
                Else
                    Session("liUserList").Add(chkControl.Value)
                End If
            End If
        Next
        Return False
    End Function

    Protected Sub linkgrpInvoice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles linkgrpInvoice.Click
        If isAgainstCompany() Then
            lblError.Text = "Group invoice can be printed against a company only"
            Exit Sub
        End If
        PrintReceiptGroup(Session("liUserList"))
    End Sub

End Class
