<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeCollectionProFormaInvoice.aspx.vb" Inherits="Fees_FeeCollectionProFormaInvoice" Title="Untitled Page" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">
    <script lang="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script type="text/javascript" lang="javascript">
        Sys.Application.add_load(
                function CheckForPrint() {
                    ShowHideBankAccount();
                    var hf_print = document.getElementById('<%= h_print.ClientID %>');
                    if (hf_print.value.indexOf('?') == 0)
                        // showModelessDialog('FeeReceipt.aspx' + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                        Popup('FeeReceipt.aspx' + document.getElementById('<%= h_print.ClientID %>').value);
                    else if (hf_print.value == 'audit')
                        // showModelessDialog('../Reports/ASPX Report/rptReportViewerModelView.aspx?paging=1', '', "dialogWidth:800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                        Popup('../Reports/ASPX Report/rptReportViewerModelView.aspx?paging=1');
                    hf_print.value = '';
                    var hid = document.getElementById('<%=h_hideBank.ClientID %>').value
                    if (hid == 2) Hidebank('tr_chq2', 2);
                    if (hid == 3) {
                        Hidebank('tr_chq2', 2);
                        Hidebank('tr_chq3', 3);
                    }
                });
                function HideAll() {
                    document.getElementById('<%= pnlCheque.ClientID %>').style.display = 'none';
                    document.getElementById('<%= pnlCreditCard.ClientID %>').style.display = 'none';
                    return false;
                }
        <%--function GetInvoice() {
    var sFeatures;
    var lstrVal;
    var lintScrVal;
    var pMode;
    var NameandCode;
    sFeatures = "dialogWidth: 760px; ";
    sFeatures += "dialogHeight: 420px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    pMode = "UNPAIDPROFORMAINVOICE"
    url = "../common/PopupSelect.aspx?id=" + pMode;
    result = window.showModalDialog(url, "", sFeatures);
    if (result == '' || result == undefined) {
        //        
        return false;
    }
    NameandCode = result.split('___');

    document.getElementById("<%=txtInvoice.ClientId %>").value = NameandCode[1];
}--%>

        //function PayTransportFee(url) {
        //    var sFeatures;
        //    sFeatures = "dialogWidth: 800px; dialogHeight: 670px; help: no; resizable: no; scroll: yes; status: no; unadorned: no; ";
        //    var NameandCode;
        //    var result;
        //    result = window.showModalDialog(url, "", sFeatures);
        //    if (result == '' || result == undefined) {
        //        return false;
        //    }
        //    return false;
        //}
        <%--  function getBankOrEmirate(mode, ctrl) {
            var sFeatures, url;
            sFeatures = "dialogWidth: 600px; dialogHeight: 500px; help: no; resizable: no; scroll: yes; status: no; unadorned: no; ";
            var NameandCode;
            var result;
            if (mode == 1)
                url = "../common/PopupFormIDhidden.aspx?iD=BANK&MULTISELECT=FALSE";
            else
                url = "../common/PopupFormIDhidden.aspx?iD=EMIRATE&MULTISELECT=FALSE";
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            if (mode == 1) {
                if (ctrl == 1) {
                    document.getElementById('<%= h_Bank1.ClientID %>').value = NameandCode[0];
            document.getElementById('<%= txtBank1.ClientID %>').value = NameandCode[1];
        }
        else if (ctrl == 2) {
            document.getElementById('<%= h_Bank2.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= txtBank2.ClientID %>').value = NameandCode[1];
            }
            else if (ctrl == 3) {
                document.getElementById('<%= h_Bank3.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= txtBank3.ClientID %>').value = NameandCode[1];
            }
}
    return false;
}--%>

<%--function Showdata(mode) {
    var sFeatures, url;
    sFeatures = "dialogWidth:750px; dialogHeight: 400px; help: no; resizable: no; scroll: yes; status: no; unadorned: no; ";
    var NameandCode;
    var STU_ID = document.getElementById('<%= h_Student_no.ClientID %>').value;
    var ACD_ID = document.getElementById('<%= ddlAcademicYear.ClientID %>').value;
    var result;
    if (mode == 1) {
        url = "../common/PaymentHistory.aspx?id=RECEIPTHISTORY&stuid=" + STU_ID;
        sFeatures = sFeatures.replace('400px', '650px');
        sFeatures = sFeatures.replace('750px', '850px');
    }
    else if (mode == 2)
        url = "../common/PopupShowData.aspx?id=CHARGEHISTORY&stuid=" + STU_ID;
    else if (mode == 3)
        url = "../common/PopupShowData.aspx?id=SIBBLINGS&stuid=" + STU_ID;
    else if (mode == 4)
        url = "../common/PopupShowData.aspx?id=CONCESSION_FEES&stuid=" + STU_ID + "&acd=" + ACD_ID;
    else if (mode == 5)
        url = "../common/PopupShowData.aspx?id=ADJUSTMENT&stuid=" + STU_ID;
    else if (mode == 6)
        url = "../common/PopupShowData.aspx?id=CHQBOUNCE&stuid=" + STU_ID;
    else if (mode == 7)
        url = "../common/PopupShowData.aspx?id=ADVPAY&stuid=" + STU_ID;
    else if (mode == 8)
        url = "../common/PopupShowData.aspx?id=AGING&stuid=" + STU_ID;
    else if (mode == 9)
        url = "FeeCalculateFee.aspx";
    result = window.showModalDialog(url, "", sFeatures);
    return false;
}--%>
        function getRoundOff() {
            var roundOff = '<%=Session("BSU_ROUNDOFF") %>';
            var amt;
            amt = parseFloat(roundOff)
            if (isNaN(amt))
                amt = 2;
            return amt;
        }
        function CheckAmount(e) {
            var amt;
            amt = parseFloat(e.value)
            if (isNaN(amt))
                amt = 0;
            e.value = amt.toFixed(getRoundOff());
            return true;
        }
        function UpdateSum() {
            var txtCCTotal, txtChequeTotal1, txtChequeTotal2, txtChequeTotal3, txtCashTotal,
            txtTotal, txtReturn, txtReceivedTotal, txtBalance, txtBankTotal;


            txtChequeTotal1 = parseFloat(document.getElementById('<%=txtChequeTotal1.ClientID %>').value);
            if (isNaN(txtChequeTotal1))
                txtChequeTotal1 = 0;
            txtChequeTotal2 = parseFloat(document.getElementById('<%=txtChequeTotal2.ClientID %>').value);
            if (isNaN(txtChequeTotal2))
                txtChequeTotal2 = 0;
            txtChequeTotal3 = parseFloat(document.getElementById('<%=txtChequeTotal3.ClientID %>').value);
    if (isNaN(txtChequeTotal3))
        txtChequeTotal3 = 0;

    if (isNaN(txtTotal))
        txtTotal = 0;
    txtReceivedTotal = txtChequeTotal1 + txtChequeTotal2 + txtChequeTotal3;
    txtBankTotal = txtChequeTotal1 + txtChequeTotal2 + txtChequeTotal3;
    txtBalance = 0;
    if (txtCashTotal > 0)
        if (txtReceivedTotal > txtTotal) {
            txtBalance = txtReceivedTotal - txtTotal;
            if (txtBalance > txtCashTotal)
                txtBalance = 0;
        }
    document.getElementById('<%=txtReceivedTotal.ClientID %>').value = txtReceivedTotal.toFixed(getRoundOff());

    document.getElementById('<%=txtBankTotal.ClientID %>').value = txtBankTotal.toFixed(getRoundOff());


            document.getElementById('<%=txtChequeTotal1.ClientID %>').value = txtChequeTotal1.toFixed(getRoundOff());
            document.getElementById('<%=txtChequeTotal2.ClientID %>').value = txtChequeTotal2.toFixed(getRoundOff());
            document.getElementById('<%=txtChequeTotal3.ClientID %>').value = txtChequeTotal3.toFixed(getRoundOff());
            document.getElementById('<%=txtBalance.ClientID %>').value = txtBalance.toFixed(getRoundOff());
        }
        function HideAddFee() {
            if (document.getElementById('tr_AddFee').style.display == 'none')
                document.getElementById('tr_AddFee').style.display = 'block';
            else
                document.getElementById('tr_AddFee').style.display = 'none';
            return false;
        }
        function Hidebank(id, index) {
            document.getElementById(id).style.display = 'block';
            document.getElementById('<%=h_hideBank.ClientID %>').value = index;
            return false;
        }

<%-- function getCheque() {
     var sFeatures, url;
     sFeatures = "dialogWidth: 700px; dialogHeight: 500px; help: no; resizable: no; scroll: yes; status: no; unadorned: no; ";
     var NameandCode;
     var result;
     url = "../common/PopupFormThreeIDHidden.aspx?iD=FEE_CHEQUE&MULTISELECT=FALSE";
     result = window.showModalDialog(url, "", sFeatures);
     if (result == '' || result == undefined)
         return false;
     NameandCode = result.split('||');
     document.getElementById('<%= txtChqno1.ClientID %>').readOnly = true;
    document.getElementById('<%= h_Chequeid.ClientID %>').value = NameandCode[0];
    document.getElementById('<%= txtChqno1.ClientID %>').value = NameandCode[1];
    return true;
}--%>
        function ClearCheque() {
            document.getElementById('<%= txtChqno1.ClientID %>').readOnly = '';
            document.getElementById('<%= h_Chequeid.ClientID %>').value = '';
            document.getElementById('<%= txtChqno1.ClientID %>').value = '';
            return false;
        }
    </script>
    <script>
        function Showdata(mode) {
            var url;
            var NameandCode;
            var STU_ID = document.getElementById('<%= h_Student_no.ClientID %>').value;
            var ACD_ID = document.getElementById('<%= ddlAcademicYear.ClientID %>').value;
            if (mode == 1) {
                url = "../common/PaymentHistory.aspx?id=RECEIPTHISTORY&stuid=" + STU_ID;
            }
            else if (mode == 2)
                url = "../common/PopupShowData.aspx?id=CHARGEHISTORY&stuid=" + STU_ID;
            else if (mode == 3)
                url = "../common/PopupShowData.aspx?id=SIBBLINGS&stuid=" + STU_ID;
            else if (mode == 4)
                url = "../common/PopupShowData.aspx?id=CONCESSION_FEES&stuid=" + STU_ID + "&acd=" + ACD_ID;
            else if (mode == 5)
                url = "../common/PopupShowData.aspx?id=ADJUSTMENT&stuid=" + STU_ID;
            else if (mode == 6)
                url = "../common/PopupShowData.aspx?id=CHQBOUNCE&stuid=" + STU_ID;
            else if (mode == 7)
                url = "../common/PopupShowData.aspx?id=ADVPAY&stuid=" + STU_ID;
            else if (mode == 8)
                url = "../common/PopupShowData.aspx?id=AGING&stuid=" + STU_ID;
            else if (mode == 9)
                url = "FeeCalculateFee.aspx";
            Popup(url);
        }
        function PayTransportFee(url) {
            Popup(url);
        }
        function GetInvoice() {
            var pMode;
            pMode = "UNPAIDPROFORMAINVOICE"
            url = "../common/PopupSelect.aspx?id=" + pMode;
            var oWnd = radopen(url, "pop_GetInvoice");

        }
        function OnClientClose1(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                document.getElementById("<%=txtInvoice.ClientId %>").value = NameandCode[1];
                __doPostBack('<%= txtInvoice.ClientID%>', 'TextChanged');
            }
        }
        function getBankOrEmirate(mode, ctrl) {
            var url;
            document.getElementById('<%= hf_mode.ClientID%>').value = mode
            document.getElementById('<%= hf_ctrl.ClientID%>').value = ctrl
            if (mode == 1)
                url = "../common/PopupFormIDhidden.aspx?iD=BANK&MULTISELECT=FALSE";
            else
                url = "../common/PopupFormIDhidden.aspx?iD=EMIRATE&MULTISELECT=FALSE";
            var oWnd = radopen(url, "pop_getBankOrEmirate");

        }

        function OnClientClose2(oWnd, args) {

            var mode = document.getElementById('<%= hf_mode.ClientID%>').value
            var ctrl = document.getElementById('<%= hf_ctrl.ClientID%>').value
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameCode.split('||');

                if (mode == 1) {
                    if (ctrl == 1) {
                        document.getElementById('<%= h_Bank1.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%= txtBank1.ClientID %>').value = NameandCode[1];
                        if (NameandCode[2] == "BANKTRF") {
                            $("#tdhBA").show(250);
                            $("#tdBA1").show(250);
                        }
                        else {
                            $("#tdhBA").hide(250);
                            $("#tdBA1").hide(250);
                        }
                    }
                    else if (ctrl == 2) {
                        document.getElementById('<%= h_Bank2.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%= txtBank2.ClientID %>').value = NameandCode[1];
                        if (NameandCode[2] == "BANKTRF")
                            $("#tdBA2").show(250);
                        else
                            $("#tdBA2").hide(250);
                    }
                    else if (ctrl == 3) {
                        document.getElementById('<%= h_Bank3.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%= txtBank3.ClientID %>').value = NameandCode[1];
                        if (NameandCode[2] == "BANKTRF")
                            $("#tdBA3").show(250);
                        else
                            $("#tdBA3").hide(250);
                    }
        }
    }
}

function getCheque() {
    var oWnd = radopen("../common/PopupFormThreeIDHidden.aspx?iD=FEE_CHEQUE&MULTISELECT=FALSE", "pop_getCheque");


}

function OnClientClose3(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg) {

        NameandCode = arg.NameandCode.split('||');

        document.getElementById('<%= txtChqno1.ClientID %>').readOnly = true;
        document.getElementById('<%= h_Chequeid.ClientID %>').value = NameandCode[0];
        document.getElementById('<%= txtChqno1.ClientID %>').value = NameandCode[1];
    }
}


function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}


    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_GetInvoice" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getBankOrEmirate" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getCheque" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getBank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Fee Collection (Company)
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Pro Forma Invoice</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:TextBox ID="txtInvoice" runat="server" OnTextChanged="txtInvoice_TextChanged" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgInvoice" runat="server"
                                ImageUrl="~/Images/forum_search.gif" OnClientClick="GetInvoice(); return false" />
                        </td>
                        <td align="left" class="matters" valign="middle" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" class="matters" valign="middle" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True" TabIndex="2"></asp:TextBox><asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" CssClass="MyCalendar" runat="server"
                                PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr id="EnrollmentRow" runat="server">
                        <td align="left" class="matters"><span class="field-label">Enrollment/Enquiry</span></td>
                        <td align="left" class="matters">
                            <asp:RadioButton ID="rbEnrollment" runat="server" Checked="True" GroupName="mode" Text="Enrollment#" AutoPostBack="True" TabIndex="1" />
                            <asp:RadioButton ID="rbEnquiry" runat="server" GroupName="mode" Text="Enquiry#" AutoPostBack="True" /></td>
                        <td align="left"></td>
                        <td align="left">
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                                PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr id="StudentRow" runat="server">
                        <td align="left" class="matters"><span class="field-label">Select Student</span></td>
                        <td align="left" class="matters" colspan="3">
                            <uc1:usrSelStudent ID="UsrSelStudent1" runat="server" />
                            <asp:ImageButton ID="lnkDiscount" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Misc/discount.gif"
                                TabIndex="8" OnClientClick="return false;"></asp:ImageButton>
                            <asp:ImageButton ID="ImgMore" runat="server" ImageUrl="~/Images/Misc/moreinfo_blue.jpg" ImageAlign="AbsMiddle" OnClientClick="return false;" TabIndex="8" />
                            <asp:ImageButton ID="lnkTransportfee" runat="server"
                                ImageAlign="AbsMiddle" ImageUrl="~/Images/Misc/bus.gif"
                                TabIndex="8"></asp:ImageButton>
                            <asp:ImageButton ID="lnkCatering" runat="server"
                                ImageAlign="AbsMiddle" ImageUrl="~/Images/Misc/Catering.gif"
                                TabIndex="8"></asp:ImageButton>
                            <asp:HiddenField ID="h_Student_no" runat="server" />
                            <ajaxToolkit:PopupControlExtender ID="pceMoreDetails" runat="server" PopupControlID="pnlMoreinfo"
                                Position="Bottom" TargetControlID="ImgMore">
                            </ajaxToolkit:PopupControlExtender>
                            <ajaxToolkit:PopupControlExtender ID="pceDiscount" runat="server"
                                PopupControlID="pnlDiscount" Position="Bottom" TargetControlID="lnkDiscount">
                            </ajaxToolkit:PopupControlExtender>
                        </td>
                    </tr>
                    <tr id="AcademicYearRow" runat="server">
                        <td align="left" class="matters"><span class="field-label">Academic year</span></td>
                        <td align="left" class="matters" colspan="3">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:CheckBox ID="chkCoBrand" runat="server" OnCheckedChanged="chkCoBrand_CheckedChanged"
                                Text="Co-brand Payment" AutoPostBack="True"></asp:CheckBox>
                            <asp:Label ID="labGrade" runat="server"></asp:Label>

                            <asp:Label ID="lblStuStatus" runat="server"></asp:Label>

                            <asp:Label ID="lbLDAdate" runat="server"></asp:Label>

                            <asp:Label ID="lbTCdate" runat="server"></asp:Label>
                            <asp:Label ID="lbStuDOJ" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="title-bg">
                        <td colspan="4" align="left">Student Fee Details(Total Paying)</td>
                    </tr>
                    <tr>
                        <td class="matters" align="left" colspan="4">
                            <asp:GridView ID="grvStudent" runat="server"
                                OnPageIndexChanging="grvStudent_PageIndexChanging" Width="100%"
                                AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                CssClass="table table-row table-bordered" EnableModelValidation="True" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Student No">
                                        <HeaderTemplate>
                                            Student No<br />
                                            <asp:TextBox ID="txtStudentNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudentNoSearch" runat="server" ImageAlign="Top"
                                                ImageUrl="../Images/forum_search.gif" OnClick="GridSearchHandler" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudentNo" runat="server" Text='<%# Bind("StudentNo") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="15%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <HeaderTemplate>
                                            Student Name<br />
                                            <asp:TextBox ID="txtStudentName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudentNameSearch" runat="server" ImageAlign="Top"
                                                ImageUrl="../Images/forum_search.gif" OnClick="GridSearchHandler" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudentName" runat="server" Text='<%# Bind("StudentName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="40%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="JoiningDate" DataFormatString="{0:dd/MMM/yyyy}"
                                        HeaderText="Joining Date">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10%" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            Grade<br />
                                            <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudentGradeSearch" runat="server" ImageAlign="Top"
                                                ImageUrl="../Images/forum_search.gif" OnClick="GridSearchHandler" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("Grade") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="17%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paying Now">
                                        <HeaderTemplate>
                                            Paying Now<br />
                                            <asp:TextBox ID="txtPayingNow" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnPayingNowSearch" runat="server" ImageAlign="Top"
                                                ImageUrl="../Images/forum_search.gif" OnClick="GridSearchHandler" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPayingNow" runat="server" Text='<%# Bind("PayingNow") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="15%" />
                                    </asp:TemplateField>
                                    <asp:CommandField DeleteText="Remove" ShowDeleteButton="True">
                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                    </asp:CommandField>
                                    <asp:TemplateField HeaderText="Student Id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudentId" runat="server" Text='<%# Bind("StudentId") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoice Id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInvoiceId" runat="server" Text='<%# Bind("InvoiceId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr class="title-bg">
                        <td colspan="4" align="left">Fee Breakup Per Fee Type</td>
                    </tr>
                    <tr>
                        <td class="matters" align="left" colspan="4">
                            <asp:GridView ID="grvFee" runat="server" Width="100%"
                                AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                CssClass="table table-row table-bordered" EnableModelValidation="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Fee Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFeeType" runat="server" Text='<%# Bind("FeeType") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="80%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paying Now">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPayingNow_Fee" runat="server"
                                                Text='<%# Bind("Amount") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" Width="20%" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="some" runat="server">
                        <td style="display: none" class="matters" align="right" colspan="4">
                            <asp:GridView ID="gvFeeCollection" runat="server" Width="100%" AutoGenerateColumns="False" EmptyDataText="No Details Added" CssClass="table table-row table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="feeidandamount" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFSR_FEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("CLOSING") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="C.P">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ChkPost" runat="server" Enabled="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OPENING" HeaderText="Opening" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MONTHLY_AMOUNT" HeaderText="Charge" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CONC_AMOUNT" HeaderText="Concession" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ADJUSTMENT" HeaderText="Adjustment" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PAID_AMOUNT" HeaderText="Paid" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CLOSING" HeaderText="Net" DataFormatString="{0:0.00}">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Discount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiscount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Discount")) %>' AutoPostBack="True" onFocus="this.select();" Style="text-align: right" OnTextChanged="txtAmountToPay_TextChanged" TabIndex="25"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Discount">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDiscount" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Discount")) %>' AutoPostBack="True" onFocus="this.select();" onblur="return CheckAmount(this);" Style="text-align: right" OnTextChanged="txtAmountToPay_TextChanged" TabIndex="25"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paying Now">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmountToPay" runat="server" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>' AutoPostBack="True" onFocus="this.select();" onblur="return CheckAmount(this);" Style="text-align: right" OnTextChanged="txtAmountToPay_TextChanged" TabIndex="25"></asp:TextBox>
                                            <asp:LinkButton ID="lbCancel" Visible="false" runat="server" OnClick="lbCancel_Click">Cancel</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:LinkButton ID="lnkMoreFee" OnClientClick="return HideAddFee();" runat="server">Add More Fee Head(s)</asp:LinkButton></td>
                    </tr>
                    <tr id="tr_AddFee" style="display: none">
                        <td colspan="1" align="left" class="matters"><span class="field-label">Fee Type</span></td>
                        <td colspan="1" align="left" class="matters">
                            <asp:DropDownList ID="ddFeetype" runat="server" SkinID="DropDownListNormal" TabIndex="9">
                            </asp:DropDownList>
                            <asp:CheckBox ID="ChkChgPost" runat="server" Text="Charge & Post" /></td>
                        <td align="left" class="matters"></td>
                        <td colspan="1" align="left" class="matters">
                            <asp:TextBox ID="txtAmountAdd" runat="server" Style="text-align: right" AutoCompleteType="Disabled" TabIndex="10"></asp:TextBox>
                            <asp:Button ID="btnAddDetails" runat="server" CssClass="button" Text="Add" CausesValidation="False" TabIndex="12" />
                            <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/close_red.gif" OnClientClick="return HideAddFee();" TabIndex="89" /></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="1"><span class="field-label">Total Outstanding</span></td>
                        <td align="left" class="matters" colspan="1">
                            <asp:TextBox ID="txtOutstanding" runat="server" Style="text-align: right" TabIndex="200"></asp:TextBox></td>
                        <td class="matters">(<asp:Label ID="lblTotalDiscount" runat="server" Style="text-align: right" CssClass="field-label"></asp:Label>)<span class="field-label">Total
             Fee</span></td>
                        <td class="matters" colspan="1">
                            <asp:TextBox ID="txtTotal" runat="server" Style="text-align: right"
                                TabIndex="200"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="title-bg">
                        <td align="left" colspan="2">Payment Details</td>
                        <td align="left" colspan="2">Total</td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Cheque Total </span></td>
                        <td align="left">
                            <asp:TextBox ID="txtBankTotal" autocomplete="off" Style="text-align: right" runat="server" onFocus="this.select();" onBlur="UpdateSum();" AutoCompleteType="Disabled" TabIndex="65"></asp:TextBox>
                            <ajaxToolkit:PopupControlExtender ID="pceCheque" runat="server" PopupControlID="pnlCheque" Position="Center" TargetControlID="txtBankTotal">
                            </ajaxToolkit:PopupControlExtender>
                        </td>
                        <td align="left" rowspan="1"></td>
                        <td align="left" rowspan="1">
                            <asp:TextBox ID="txtDue" runat="server" Style="text-align: right" TabIndex="205"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Narration</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"
                                SkinID="MultiText" TabIndex="160"></asp:TextBox>
                        </td>
                        <td align="left"></td>
                        <td align="left">
                            <asp:TextBox ID="txtReceivedTotal" Style="text-align: right" runat="server" TabIndex="205"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"></td>
                        <td align="left">
                            <asp:LinkButton ID="lbViewTotal" runat="server" Visible="False">Preview Total</asp:LinkButton>
                        </td>
                        <td align="left"></td>
                        <td align="left">
                            <asp:TextBox ID="txtBalance" runat="server" Style="text-align: right" TabIndex="220"></asp:TextBox></td>
                    </tr>
                    <tr>

                        <td class="matters" colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add"
                                CausesValidation="False" Visible="False" /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="165" /><asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                            <asp:CheckBox ID="chkRemember" runat="server" Text="Remember Payment Info"
                                Visible="False" />
                            <asp:Button ID="btnNext" runat="server" CausesValidation="False"
                                CssClass="button" TabIndex="160" Text="Next" Visible="False" /></td>
                    </tr>
                </table>
                <asp:Panel ID="pnlCreditCard" Style="display: none" runat="server">
                    <table width="100%">
                        <tr class="title-bg-light">
                            <td align="left" colspan="2">Credit Card          
                      
                <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/close_red.gif" OnClientClick="return HideAll();" TabIndex="54" Style="text-align: right" /></td>
                        </tr>
                        <tr>
                            <td align="left" rowspan="1" class="matters_Colln">Card Type:</td>
                            <td align="left">
                                <asp:DropDownList ID="ddCreditcard" runat="server" DataSourceID="SqlCreditCard"
                                    DataTextField="CRI_DESCR" DataValueField="CRR_ID" SkinID="DropDownListNormal" TabIndex="55">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" class="matters_Colln">Authorisation Code:</td>
                            <td align="left" colspan="1" style="font-size: 12pt">
                                <asp:TextBox ID="txtCreditno" runat="server" AutoCompleteType="Disabled" TabIndex="60" SkinID="TextBoxCollection"></asp:TextBox></td>
                        </tr>
                    </table>
                    <asp:SqlDataSource ID="SqlCreditCard" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                        SelectCommand="exec GetCreditCardListForCollection 'FEES'"></asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnlMoreinfo" runat="server" Style="display: none;">
                    <table cellpadding="3" cellspacing="0" style="background-color: #FFFFCC; border-right: #ff9e18 1pt solid; border-top: #ff9e18 1pt solid; border-left: #ff9e18 1pt solid; border-bottom: #ff9e18 1pt solid;">
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" align="absMiddle" />
                                <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="return Showdata(1);">View Payment History</asp:LinkButton></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton3" runat="server" OnClientClick="return Showdata(2);">View Charge Details</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="lnkAdvReciept" runat="server" OnClientClick="return Showdata(7);">View Advance Receipt</asp:LinkButton></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton4" runat="server" OnClientClick="return Showdata(3);">Sibling Informaion</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton5" runat="server" OnClientClick="return Showdata(8);">Ageing</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton7" runat="server" OnClientClick="return Showdata(4);">Concession Details</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton6" runat="server" OnClientClick="return Showdata(5);">Fee Adjustment</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="return Showdata(6);">Cheque History</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="LinkButton8" runat="server" OnClientClick="return Showdata(9);">Calculate Fee</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <img src="../Images/Misc/orange-arrow.gif" />
                                <asp:LinkButton ID="lbStudentAudit" runat="server">Student Audit</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlCheque" Style="display: none" CssClass="panel-cover" Width="80%" runat="server">
                    <table width="100%">
                        <tr class="title-bg-light">
                            <td align="left">Bank</td>
                            <td align="left" id="tdhBA">Bank Account
                            </td>
                            <td align="left">Emirate</td>
                            <td align="left">Cheque #<asp:LinkButton ID="lnkClear" OnClientClick="return ClearCheque();" runat="server">(Clear)</asp:LinkButton></td>
                            <td align="left">Amount</td>
                            <td align="left">Cheque Date</td>
                            <td align="left"></td>
                        </tr>
                        <tr id="tr_chq1">
                            <td align="left">
                                <asp:TextBox ID="txtBank1" runat="server" AutoPostBack="True" TabIndex="70" SkinID="TextBoxCollection"></asp:TextBox>
                                <asp:ImageButton ID="imgBank1" runat="server" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Images/forum_search.gif" OnClientClick="getBankOrEmirate(1,1);return false;" TabIndex="75" /></td>
                            <td id="tdBA1" align="left">
                                <asp:TextBox ID="txtBankAct1" runat="server" onfocus="this.blur();" AutoPostBack="True" TabIndex="70"
                                    Style="width: 80% !important"></asp:TextBox>
                                <asp:ImageButton ID="imgBankAct1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="getBank(1); return false;" TabIndex="75" /><asp:HiddenField ID="hfBankAct1" runat="server" />
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlEmirate1" runat="server" DataSourceID="SqlEmirate_m" DataTextField="EMR_DESCR"
                                    DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="78">
                                </asp:DropDownList></td>
                            <td align="left">
                                <asp:TextBox ID="txtChqno1" runat="server" TabIndex="80" AutoCompleteType="Disabled" SkinID="TextBoxCollection"></asp:TextBox>
                                <asp:ImageButton ID="imgCheque" runat="server" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Images/forum_search.gif" OnClientClick="getCheque() ; return false" TabIndex="75" /></td>
                            <td align="left">
                                <asp:TextBox ID="txtChequeTotal1" onBlur="UpdateSum();" onFocus="this.select();" runat="server" Style="text-align: right" TabIndex="85" AutoCompleteType="Disabled" SkinID="TextBoxCollection"></asp:TextBox></td>
                            <td align="left">
                                <asp:TextBox ID="txtChqDate1" runat="server" TabIndex="87" SkinID="TextBoxCollection"></asp:TextBox>
                                <asp:ImageButton ID="imgChequedate1" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="88" /></td>
                            <td align="left">
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/close_red.gif" OnClientClick="return HideAll();" TabIndex="89" />
                                <%--<asp:LinkButton id="LinkButton9" runat="server" OnClientClick="Hidebank('tr_chq2',2);return false;">Add</asp:LinkButton></td>--%>
                        </tr>
                        <tr id="tr_chq2" style="display: none">
                            <td align="left">
                                <asp:TextBox ID="txtBank2" runat="server" AutoPostBack="True" TabIndex="90" SkinID="TextBoxCollection"></asp:TextBox>
                                <asp:ImageButton ID="imgBank2" runat="server" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Images/forum_search.gif" OnClientClick="getBankOrEmirate(1,2);return false" TabIndex="95" /></td>
                            <td id="tdBA2" align="left">
                                <asp:TextBox ID="txtBankAct2" runat="server" onfocus="this.blur();" AutoPostBack="True" TabIndex="70"
                                    Style="width: 80% !important"></asp:TextBox>
                                <asp:ImageButton ID="imgBankAct2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="  getBank(2); return false;" TabIndex="75" /><asp:HiddenField ID="hfBankAct2" runat="server" />
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlEmirate2" runat="server" DataSourceID="SqlEmirate_m" DataTextField="EMR_DESCR"
                                    DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="100">
                                </asp:DropDownList></td>
                            <td align="left">
                                <asp:TextBox ID="txtChqno2" runat="server" TabIndex="105" AutoCompleteType="Disabled" SkinID="TextBoxCollection"></asp:TextBox></td>
                            <td align="left">
                                <asp:TextBox ID="txtChequeTotal2" onBlur="UpdateSum();" onFocus="this.select();" runat="server" Style="text-align: right" TabIndex="110" AutoCompleteType="Disabled" SkinID="TextBoxCollection"></asp:TextBox></td>
                            <td align="left">
                                <asp:TextBox ID="txtChqDate2" runat="server" TabIndex="115" SkinID="TextBoxCollection"></asp:TextBox>
                                <asp:ImageButton ID="imgChequedate2" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="120" /></td>
                            <td align="left">
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/close_red.gif" OnClientClick="return HideAll();" TabIndex="121" />
                                <asp:LinkButton ID="LinkButton10" runat="server" OnClientClick="Hidebank('tr_chq3',3);return false;">Add</asp:LinkButton></td>
                        </tr>
                        <tr id="tr_chq3" style="display: none">
                            <td align="left">
                                <asp:TextBox ID="txtBank3" runat="server" AutoPostBack="True" TabIndex="125" SkinID="TextBoxCollection"></asp:TextBox>
                                <asp:ImageButton ID="imgBank3" runat="server" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Images/forum_search.gif" OnClientClick="getBankOrEmirate(1,3);return false" TabIndex="130" /></td>
                            <td id="tdBA3" align="left">
                                <asp:TextBox ID="txtBankAct3" runat="server" onfocus="this.blur();" AutoPostBack="True" TabIndex="70"
                                    Style="width: 80% !important"></asp:TextBox>
                                <asp:ImageButton ID="imgBankAct3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="  getBank(3);return false;" TabIndex="75" /><asp:HiddenField ID="hfBankAct3" runat="server" />
                            </td>
                            <td align="center">
                                <asp:DropDownList ID="ddlEmirate3" runat="server" DataSourceID="SqlEmirate_m" DataTextField="EMR_DESCR"
                                    DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="135">
                                </asp:DropDownList></td>
                            <td align="left">
                                <asp:TextBox ID="txtChqno3" runat="server" TabIndex="140" AutoCompleteType="Disabled" SkinID="TextBoxCollection"></asp:TextBox></td>
                            <td align="left">
                                <asp:TextBox ID="txtChequeTotal3" onBlur="UpdateSum();" onFocus="this.select();" Style="text-align: right" runat="server" TabIndex="145" AutoCompleteType="Disabled" SkinID="TextBoxCollection"></asp:TextBox></td>
                            <td align="left">
                                <asp:TextBox ID="txtChqDate3" runat="server" TabIndex="150" SkinID="TextBoxCollection"></asp:TextBox>
                                <asp:ImageButton ID="imgChequedate3" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="156" /></td>
                            <td align="left">
                                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/close_red.gif" OnClientClick="return HideAll();" TabIndex="157" /></td>
                        </tr>
                    </table>
                    <asp:Label ID="lblBankInvalid" runat="server" EnableViewState="False" SkinID="LabelError"></asp:Label>
                    <asp:HiddenField ID="h_Bank1" runat="server" />
                    <asp:HiddenField ID="h_Bank2" runat="server" />
                    <asp:HiddenField ID="h_Bank3" runat="server" />
                    <asp:SqlDataSource ID="SqlEmirate_m" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                        SelectCommand="SELECT     EMR_CODE, EMR_DESCR, EMR_ENQ_GROUP, EMR_CTY_ID&#13;&#10;FROM         EMIRATE_M&#13;&#10;WHERE     (EMR_CTY_ID IN&#13;&#10;                          (SELECT     BSU_COUNTRY_ID&#13;&#10;                            FROM          BUSINESSUNIT_M&#13;&#10;                            WHERE      (BSU_ID =@BSU_ID)))">
                        <SelectParameters>
                            <asp:SessionParameter SessionField="sBsuid" Name="BSU_ID"></asp:SessionParameter>
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <ajaxToolkit:CalendarExtender ID="calCheque1" CssClass="MyCalendar" runat="server"
                        PopupButtonID="imgChequedate1" TargetControlID="txtChqDate1" Format="dd/MMM/yyyy" Enabled="True" PopupPosition="TopLeft">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="calCheque2" CssClass="MyCalendar" runat="server"
                        PopupButtonID="imgChequedate2" TargetControlID="txtChqDate2" Format="dd/MMM/yyyy" Enabled="True" PopupPosition="TopLeft">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="calCheque3" CssClass="MyCalendar" runat="server"
                        PopupButtonID="imgChequedate3" TargetControlID="txtChqDate3" Format="dd/MMM/yyyy" Enabled="True" PopupPosition="TopLeft">
                    </ajaxToolkit:CalendarExtender>
                </asp:Panel>

                <asp:Panel ID="pnlDiscount" runat="server">
                    <table>
                        <tr>
                            <td align="left">
                                <asp:GridView ID="gvDiscount" runat="server" CssClass="table table-row table-bordered"
                                    Width="100%" EmptyDataText="No Data Found" AutoGenerateColumns="False"
                                    Visible="False">
                                    <Columns>
                                        <asp:BoundField DataField="Scheme" HeaderText="Scheme" Visible="False">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Priod" HeaderText="Valid period">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Slab" HeaderText="Fee Terms">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Fees" HeaderText="Fees">
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PayNow" HeaderText="Pay Now">
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="YouSave" HeaderText="You Save">
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Select">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbApply" runat="server" OnClick="lbApply_Click" OnClientClick="tblErrortop.click();return true;">Select</asp:LinkButton>
                                                <asp:Label ID="lblFee_ID" runat="server" Text='<%# Bind("FEE_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblFds_ID" runat="server" Text='<%# Bind("FDS_ID") %>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_hideBank" runat="server" />
                <asp:HiddenField ID="h_Chequeid" runat="server" />
                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_selected_menu_3" runat="server" />
                <asp:HiddenField ID="h_selected_menu_4" runat="server" />
                <asp:HiddenField ID="h_selected_menu_5" runat="server" />
                <asp:HiddenField ID="h_selected_menu_6" runat="server" />
                <asp:HiddenField ID="h_selected_menu_7" runat="server" />
                <asp:HiddenField ID="hf_mode" runat="server" />
                <asp:HiddenField ID="hf_ctrl" runat="server" />
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
    <script type="text/javascript">

        function getBank(mode) {
            var url;
            document.getElementById('<%= hf_mode.ClientID%>').value = mode

            var txtBankCode;
            if (mode == 1)
                txtBankCode = document.getElementById('<%=txtBankAct1.ClientID%>').value;
            var oWnd = radopen("../ACCOUNTS/PopUp.aspx?ShowType=BANK&codeorname=" + txtBankCode, "pop_getBank");


        }
        function OnClientClose4(oWnd, args) {

            var mode = document.getElementById('<%= hf_mode.ClientID%>').value
            var ctrl = document.getElementById('<%= hf_ctrl.ClientID%>').value
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg) {

        NameandCode = arg.NameandCode.split('||');

        if (mode == 1) {
            document.getElementById('<%=hfBankAct1.ClientID%>').value = NameandCode[0];
                    document.getElementById('<%=txtBankAct1.ClientID%>').value = NameandCode[0];
                }
                else if (mode == 2) {
                    document.getElementById('<%=hfBankAct2.ClientID%>').value = NameandCode[0];
                    document.getElementById('<%=txtBankAct2.ClientID%>').value = NameandCode[0];
                }
                else if (mode == 3) {
                    document.getElementById('<%=hfBankAct3.ClientID%>').value = NameandCode[0];
            document.getElementById('<%=txtBankAct3.ClientID%>').value = NameandCode[0];
        }
}
}
function ShowHideBankAccount() {
    if (document.getElementById('<%= txtBank1.ClientID%>').value != 'Bank Transfer') {
                $("#tdhBA").hide();
                $("#tdBA1").hide();
            }
            else {
                $("#tdhBA").show();
                $("#tdBA1").show();
            }

            if (document.getElementById('<%= txtBank2.ClientID%>').value != 'Bank Transfer')
                $("#tdBA2").hide();
            else
                $("#tdBA2").show();
            if (document.getElementById('<%= txtBank3.ClientID%>').value != 'Bank Transfer')
                $("#tdBA3").hide();
            else
                $("#tdBA3").show();
        }
    </script>
</asp:Content>

