Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data


Partial Class Fees_FeeAdjustment
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Not IsPostBack Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Dim USR_NAME As String = Session("sUsr_name")
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ADJUSTMENTS_HEADTOHEAD Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Dim CurBsUnit As String = Session("sBsuid")
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            FillACD()

            h_BSUID.Value = Session("sbsuid")
            txtStudNameDR.Attributes.Add("ReadOnly", "ReadOnly")
            txtStudNameCR.Attributes.Add("ReadOnly", "ReadOnly")
            Session("sFEE_ADJUSTMENT") = Nothing
            txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)

            If ViewState("datamode") = "view" Then
                Dim STU_TYP As STUDENTTYPE = STUDENTTYPE.STUDENT
                Dim FAH_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FAH_ID").Replace(" ", "+"))
                Dim vFEE_ADJ As FEEADJUSTMENT = FEEADJUSTMENT.GetFeeAdjustmentsInternalTransfer(FAH_ID)
                txtDate.Text = Format(vFEE_ADJ.FAH_DATE, OASISConstants.DateFormat)
                txtStudNameDR.Text = vFEE_ADJ.FAH_STU_NAME
                h_STUD_ID_DR.Value = vFEE_ADJ.FAH_STU_ID
                If vFEE_ADJ.FAH_STU_TYPE = "S" Then
                    STU_TYP = STUDENTTYPE.STUDENT
                ElseIf vFEE_ADJ.FAH_STU_TYPE = "E" Then
                    STU_TYP = STUDENTTYPE.ENQUIRY
                End If
                ddlGrade.DataSource = FEEPERFORMAINVOICE.GetGradeDetails(vFEE_ADJ.FAH_STU_ID, STU_TYP)
                ddlGrade.DataTextField = "GRM_DISPLAY"
                ddlGrade.DataValueField = "GRD_ID"
                ddlGrade.DataBind()
                ddlGrade.Enabled = False

                h_STUD_ID_CR.Value = vFEE_ADJ.FAH_STU_ID_CR
                txtStudNameCR.Text = vFEE_ADJ.FAH_STU_NAME_CR
                txtDetRemarks.Text = vFEE_ADJ.FAH_REMARKS
                PopulateFeeType(vFEE_ADJ.FAH_GRD_ID, ddlFeeTypeDR)
                ddlFeeTypeDR_SelectedIndexChanged(Nothing, Nothing)
                If radEnqDR.Checked Then
                    STU_TYP = STUDENTTYPE.ENQUIRY
                ElseIf radStudDR.Checked Then
                    STU_TYP = STUDENTTYPE.STUDENT
                End If
                h_GRD_ID_CR.Value = FEEPERFORMAINVOICE.GetGradeDetails(h_STUD_ID_DR.Value, False, STU_TYP)
                PopulateFeeType(h_GRD_ID_CR.Value, ddlFEETYPECR)
                If vFEE_ADJ.FAH_STU_TYPE = "S" Then
                    radStudDR.Checked = True
                Else
                    radEnqDR.Checked = True
                End If
                ViewState("EID") = vFEE_ADJ.FAH_ID
                If vFEE_ADJ.FAH_STU_TYPE_CR = "S" Then

                    radStudCR.Checked = True
                Else
                    radEnqCR.Checked = True
                End If
                If Not vFEE_ADJ.FEE_ADJ_DET Is Nothing Then
                    GetFeeDetailsFilled(vFEE_ADJ.FEE_ADJ_DET)
                End If
                DissableAllControls(True)
                If Not vFEE_ADJ.bAllowEdit Then
                    btnEdit.Visible = False
                    btnDelete.Visible = False
                End If
                Session("vFEEHEADtoHEAD") = vFEE_ADJ
                'txtStudName.Text = PROC_DESCR
                'imgProcess.Enabled = False
                'gvFeeDetails.Columns(3).Visible = False
            End If
        End If
    End Sub

    Private Sub GetFeeDetailsFilled(ByVal vFEEDET As Hashtable)
        For Each vFEE_ADJ_DET As FEEADJUSTMENT_S In vFEEDET.Values
            ddlFEETYPECR.ClearSelection()
            ddlFeeTypeDR.ClearSelection()
            If Not ddlFEETYPECR.Items.FindByValue(vFEE_ADJ_DET.FAD_TO_FEE_ID) Is Nothing Then
                ddlFEETYPECR.SelectedIndex = -1
                ddlFEETYPECR.Items.FindByValue(vFEE_ADJ_DET.FAD_TO_FEE_ID).Selected = True
            End If
            If Not ddlFeeTypeDR.Items.FindByValue(vFEE_ADJ_DET.FAD_FEE_ID) Is Nothing Then
                ddlFeeTypeDR.SelectedIndex = -1
                ddlFeeTypeDR.Items.FindByValue(vFEE_ADJ_DET.FAD_FEE_ID).Selected = True
            End If 
            txtDetAmount.Text = vFEE_ADJ_DET.FAD_AMOUNT
        Next
    End Sub

    Private Sub PopulateFeeType(ByVal vGRD_ID As String, ByVal ddlFeeType As DropDownList)
        ddlFeeType.DataSource = FEEADJUSTMENT.PopulateFeeMaster(h_BSUID.Value, ddlAcademicYear.SelectedValue, vGRD_ID)
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()
    End Sub

    Private Sub DissableAllControls(ByVal dissble As Boolean)
        txtDate.ReadOnly = dissble
        txtDetRemarks.ReadOnly = dissble
        txtDetAmount.ReadOnly = dissble
        ddlFeeTypeDR.Enabled = Not dissble
        ddlFEETYPECR.Enabled = Not dissble
        imgDate.Enabled = Not dissble
        imgProcess.Enabled = Not dissble
        CalendarExtender1.Enabled = Not dissble
        calendarButtonExtender.Enabled = Not dissble
        radEnqCR.Enabled = Not dissble
        radStudCR.Enabled = Not dissble
        radEnqDR.Enabled = Not dissble
        radStudDR.Enabled = Not dissble
        ImgCRStud.Enabled = Not dissble
        ddlAcademicYear.Enabled = Not dissble
        ddlGrade.Enabled = Not dissble
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar2.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If h_STUD_ID_CR.Value = h_STUD_ID_DR.Value AndAlso _
        h_GRD_ID_CR.Value = h_GRD_ID_CR.Value AndAlso _
        ddlFEETYPECR.SelectedValue = ddlFeeTypeDR.SelectedValue Then
            'lblError.Text = "Fee Transfering to the same head is not allowed.."
            usrMessageBar2.ShowNotification("Fee Transfering to the same head is not allowed..", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Math.Abs(CDbl(txtDetAmount.Text)) > Math.Abs(CDbl(lblBalanceAmt.Text)) Then
            'lblError.Text = "Transfering amount is greater than Balance amount.."
            usrMessageBar2.ShowNotification("Transfering amount is greater than Balance amount..", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If CDbl(txtDetAmount.Text) < 0 Then
            'lblError.Text = "Transfer amount is not valid .."
            usrMessageBar2.ShowNotification("Transfer amount is not valid ..", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If


        Dim vFEE_ADJ As New FEEADJUSTMENT
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim trans As SqlTransaction
        If (Not ViewState("EID") Is Nothing) AndAlso (Not Session("vFEEHEADtoHEAD") Is Nothing) Then
            vFEE_ADJ = Session("vFEEHEADtoHEAD")
            vFEE_ADJ.bEdit = True
        End If
        vFEE_ADJ.FAH_ACD_ID = ddlAcademicYear.SelectedValue
        vFEE_ADJ.FAH_BSU_ID = Session("sBSUID")
        vFEE_ADJ.FAH_STU_ID = h_STUD_ID_DR.Value
        vFEE_ADJ.FAH_GRD_ID = h_GRD_ID_DR.Value
        vFEE_ADJ.FAH_STU_ID_CR = h_STUD_ID_CR.Value
        vFEE_ADJ.FAH_GRD_ID_CR = h_GRD_ID_CR.Value
        vFEE_ADJ.FAH_REMARKS = txtDetRemarks.Text
        vFEE_ADJ.ADJUSTMENTTYPE = 1
        If radEnqDR.Checked Then
            vFEE_ADJ.FAH_STU_TYPE = "E"
        ElseIf radStudDR.Checked Then
            vFEE_ADJ.FAH_STU_TYPE = "S"
        End If
        If radEnqCR.Checked Then
            vFEE_ADJ.FAH_STU_TYPE_CR = "E"
        ElseIf radStudCR.Checked Then
            vFEE_ADJ.FAH_STU_TYPE_CR = "S"
        End If
        vFEE_ADJ.FAH_DATE = CDate(txtDate.Text)
        If Not ViewState("EID") Is Nothing Then
        End If

        Dim vFEE_DET As New FEEADJUSTMENT_S
        If (Not ViewState("EID") Is Nothing) AndAlso (Not Session("vFEEHEADtoHEAD") Is Nothing) Then
            Dim ienum As IDictionaryEnumerator = vFEE_ADJ.FEE_ADJ_DET.GetEnumerator
            While (ienum.MoveNext())
                vFEE_DET = ienum.Value
                'vFEE_DET.bEdit = True
                Exit While
            End While
        End If
        Dim eId As Integer = -1
        vFEE_DET.AdjustmentType = 1
        vFEE_DET.FAD_AMOUNT = CDbl(txtDetAmount.Text)
        vFEE_DET.FEE_TYPE = ddlFeeTypeDR.SelectedItem.Text
        vFEE_DET.FAD_FEE_ID = ddlFeeTypeDR.SelectedValue
        vFEE_DET.TO_FEE_TYPE = ddlFEETYPECR.SelectedItem.Text
        vFEE_DET.FAD_TO_FEE_ID = ddlFEETYPECR.SelectedValue
        Dim httab As New Hashtable
        httab(vFEE_DET.FAD_FEE_ID) = vFEE_DET
        vFEE_ADJ.FEE_ADJ_DET = httab
        trans = conn.BeginTransaction("sFEE_ADJUSTMENT")
        Dim retVal As Integer = FEEADJUSTMENT.SaveDetailsInternal(vFEE_ADJ, conn, trans)
        If retVal > 0 Then
            trans.Rollback()
            'lblError.Text = UtilityObj.getErrorMessage(retVal)
            usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
        Else
            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
            'If flagAudit <> 0 Then
            '    Throw New ArgumentException("Could not process your request")
            'End If            
            trans.Commit()
            'lblError.Text = "Data updated Successfully"
            usrMessageBar2.ShowNotification("Data updated Successfully", UserControls_usrMessageBar.WarningType.Success)
            ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        End If
    End Sub

    Private Sub ClearDetails()
        h_BSUID.Value = Session("sBSUID")
        h_STUD_ID_DR.Value = ""
        txtStudNameDR.Text = ""
        h_STUD_ID_CR.Value = ""
        txtStudNameCR.Text = ""
        txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
        ddlFeeTypeDR.ClearSelection()
        Session("vFEEHEADtoHEAD") = Nothing
        txtDetAmount.Text = ""
        txtDetRemarks.Text = ""
        ddlFeeTypeDR.SelectedIndex = -1
        ddlFEETYPECR.SelectedIndex = -1
    End Sub

    'Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
    '    ViewState("datamode") = "edit"
    '    gvFeeDetails.Columns(3).Visible = True
    '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    '    UtilityObj.beforeLoopingControls(Me.Page)
    'End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearDetails()
        DissableAllControls(False)
        ViewState("datamode") = "add"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        imgProcess.Enabled = True
    End Sub

    Protected Sub imgProcess_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgProcess.Click
        If h_STUD_ID_DR.Value <> "" Then
            ddlGrade.Enabled = True
            Dim STUD_ID As Integer = h_STUD_ID_DR.Value.Split("||")(0)
            FillGradeDetails(STUD_ID)
            h_GRD_ID_DR.Value = ddlGrade.SelectedValue
            PopulateFeeType(h_GRD_ID_DR.Value, ddlFeeTypeDR)
            ddlFeeTypeDR_SelectedIndexChanged(Nothing, Nothing)
            'Fill the CR details also if it is empty
            If txtStudNameCR.Text = "" Then
                radEnqCR.Checked = radEnqDR.Checked
                radStudCR.Checked = radStudDR.Checked
                txtStudNameCR.Text = txtStudNameDR.Text
                h_STUD_ID_CR.Value = h_STUD_ID_DR.Value
                h_GRD_ID_CR.Value = h_GRD_ID_DR.Value
                PopulateFeeType(h_GRD_ID_CR.Value, ddlFEETYPECR)
            End If
        End If
    End Sub

    Private Sub FillGradeDetails(ByVal STU_ID As Integer)
        Dim STU_TYP As STUDENTTYPE = STUDENTTYPE.STUDENT
        If radEnqDR.Checked Then
            STU_TYP = STUDENTTYPE.ENQUIRY
        ElseIf radStudDR.Checked Then
            STU_TYP = STUDENTTYPE.STUDENT
        End If
        ddlGrade.DataSource = FEEPERFORMAINVOICE.GetGradeDetails(STU_ID, STU_TYP)
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        ddlGrade.Enabled = False
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ClearDetails()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        DissableAllControls(False)
        MakeEditable()
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Private Sub MakeEditable()
        If Session("vFEEHEADtoHEAD") Is Nothing Then Return
        Dim vFEE_ADJ As New FEEADJUSTMENT
        vFEE_ADJ = Session("vFEEHEADtoHEAD")
        vFEE_ADJ.bEdit = True
        'For Each vFEE_DET As FEEADJUSTMENT_S In vFEE_ADJ.FEE_ADJ_DET.Values
        '    vFEE_DET.bEdit = True
        'Next
        Session("vFEEHEADtoHEAD") = vFEE_ADJ
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Session("sFEE_ADJUSTMENT") Is Nothing Then Return
        Dim vFEE_ADJ As FEEADJUSTMENT = Session("sFEE_ADJUSTMENT")
        vFEE_ADJ.bDelete = True
        vFEE_ADJ.bEdit = True
        For Each vFEE_DET As FEEADJUSTMENT_S In vFEE_ADJ.FEE_ADJ_DET.Values
            vFEE_DET.bDelete = True
            'vFEE_DET.bEdit = True
        Next
        Session("sFEE_ADJUSTMENT") = vFEE_ADJ
        btnSave_Click(sender, e)
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        h_GRD_ID_DR.Value = ddlGrade.SelectedValue
        PopulateFeeType(h_GRD_ID_DR.Value, ddlFEETYPECR)
    End Sub

    Protected Sub radStud_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStudDR.CheckedChanged
        ClearDetails()
    End Sub

    Protected Sub radEnq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnqDR.CheckedChanged
        ClearDetails()
    End Sub

    Protected Sub radEnqCR_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnqCR.CheckedChanged
        txtStudNameCR.Text = ""
        h_STUD_ID_CR.Value = ""
        h_GRD_ID_CR.Value = ""
    End Sub

    Protected Sub ImgCRStud_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgCRStud.Click
        If h_STUD_ID_CR.Value <> "" Then
            Dim STUD_ID As Integer = h_STUD_ID_CR.Value.Split("||")(0)
            Dim STU_TYP As STUDENTTYPE = STUDENTTYPE.STUDENT
            If radEnqCR.Checked Then
                STU_TYP = STUDENTTYPE.ENQUIRY
            ElseIf radStudCR.Checked Then
                STU_TYP = STUDENTTYPE.STUDENT
            End If
            h_GRD_ID_CR.Value = FEEPERFORMAINVOICE.GetGradeDetails(STUD_ID, False, STU_TYP)
            PopulateFeeType(h_GRD_ID_CR.Value, ddlFEETYPECR)
        End If
    End Sub

    Protected Sub radStudCR_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStudCR.CheckedChanged
        txtStudNameCR.Text = ""
        h_STUD_ID_CR.Value = ""
        h_GRD_ID_CR.Value = ""
    End Sub

    Protected Sub ddlFeeTypeDR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFeeTypeDR.SelectedIndexChanged
        Dim stud_typ As String = String.Empty
        If radEnqDR.Checked Then
            stud_typ = "E"
        ElseIf radStudDR.Checked Then
            stud_typ = "S"
        End If
        If ddlFeeTypeDR.Items Is Nothing OrElse ddlFeeTypeDR.Items.Count <= 0 Then Exit Sub
        lblBalanceAmt.Text = FEEADJUSTMENT.GetBalanceAmountForFeeType(h_BSUID.Value, stud_typ, h_STUD_ID_DR.Value, txtDate.Text, ddlFeeTypeDR.SelectedValue)
    End Sub
    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBsuid"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        'SetAcademicYearDate()
    End Sub
End Class
