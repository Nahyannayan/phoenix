﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BulkOtherfeeCharge.aspx.vb" Inherits="Fees_BulkOtherfeeCharge" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Bulk Other Fee Charge</title>
    <script src="/PHOENIXBETA/vendor/jquery/jquery.min.js"></script>
    <script src="/PHOENIXBETA/vendor/jquery-ui/jquery-ui.min.js"></script>
    <base target="_self" />
    <link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="/PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Page level plugin CSS-->
    <link href="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/PHOENIXBETA/cssfiles/sb-admin.css" rel="stylesheet" />
    <%--<style type="text/css" > 
 .odd{background-color: white;} 
 .even{background-color: gray;} 
</style>--%>
    <script language="javascript" type="text/javascript">
        var EnglishToArabicNumber = function () {

            var map =
                [
                    "٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩"
                ]
            $(".ToArabicNumber").each(function () {
                $(this).html($(this).html().replace(
                    /\d(?=[^<>]*(<|$))/g,
                    function ($0) { return map[$0] }))
            });
            try {
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                if (prm != null) {
                    prm.add_endRequest(function (sender, e) {
                        if (sender._postBackSettings.panelsToUpdate != null) {
                            var map =
                                [
                                    "٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩"
                                ]
                            $(".ToArabicNumber").each(function () {
                                $(this).html($(this).html().replace(
                                    /\d(?=[^<>]*(<|$))/g,
                                    function ($0) { return map[$0] }))
                            });
                        }
                    });
                };
            } catch (e) {

            }

        }
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
    <script language="javascript" type="text/javascript">




        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            // var lstrChk = document.getElementById("chkAL").checked;
            var chk_state = document.getElementById("chkAL").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }
        }
    </script>
</head>
<body onload="listen_window();">
    <form id="form1" runat="server">




        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2">
                    <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />

                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Data" AllowPaging="True" PageSize="17" CssClass="table table-bordered table-row ToArabicNumber custom-th" meta:resourcekey="gvGroupResource1">
                        <Columns>
                            <asp:TemplateField HeaderText="Select" meta:resourcekey="TemplateFieldResource1">
                                <HeaderTemplate>
                                    <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                        value="Check All" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("FOH_ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" meta:resourcekey="TemplateFieldResource2">
                                <HeaderTemplate>
                                    Date
                                    <br />
                                    <asp:TextBox ID="txtSearchDate" runat="server" meta:resourcekey="txtSearchDateResource1"></asp:TextBox>
                                    <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" meta:resourcekey="btnDateSearchResource1" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDate" runat="server" Text='<%# Bind("FOH_DATE", "{0:dd/MMM/yyyy}") %>' meta:resourcekey="lblDateResource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fee Description" meta:resourcekey="TemplateFieldResource3">
                                <HeaderTemplate>
                                    Fee Description
                                    <br />
                                    <asp:TextBox ID="txtSearchFee" runat="server" meta:resourcekey="txtSearchFeeResource1"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchFee" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" meta:resourcekey="btnSearchFeeResource1" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbFee" runat="server" Text='<%# Bind("FEE_DESCR") %>' meta:resourcekey="lbFeeResource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Narration" meta:resourcekey="TemplateFieldResource4">
                                <HeaderTemplate>
                                    Narration
                                    <br />
                                    <asp:TextBox ID="txtSearchNarration" runat="server" Width="150px" meta:resourcekey="txtSearchNarrationResource1"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchNarration" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" meta:resourcekey="btnSearchNarrationResource1" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbNarration" runat="server" Text='<%# Bind("FOH_NARRATION") %>' meta:resourcekey="lbNarrationResource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>

            </tr>
            <tr>
                <td class="text-left flip">
                    <asp:CheckBox ID="chkSelAll" runat="server" AutoPostBack="True" CssClass="radiobutton"
                        Text="Select All" meta:resourcekey="chkSelAllResource1" />
                </td>

                <td align="center">
                    <asp:Button ID="btnFinish" runat="server" CssClass="button" Text="Finish" meta:resourcekey="btnFinishResource1" /></td>
            </tr>
        </table>

    </form>
</body>
</html>
