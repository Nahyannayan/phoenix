Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeTransportPerformaInvoiceView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "feeTransportPerformaInvoice.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif" 
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "F300160" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case "F300160"
                            lblHead.Text = "Transport Pro forma Invoice"
                            ViewState("trantype") = "A"
                    End Select
                    h_print.Value = ""
                    h_Export.Value = "No"
                    ddBusinessunit.DataBind()
                    gridbind()
                    Session("liUserList") = New ArrayList
                End If
                Dim datet As Date = Convert.ToDateTime("2154-12-31T23:59:59").ToUniversalTime
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                '     lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtReceiptno FPH_INOICENO
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FPH_INOICENO", lstrCondn1)

                '   -- 2  txtDate
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FPH_DT", lstrCondn2)

                '   -- 3  txtGrade
                larrSearchOpr = h_selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRD_DISPLAY", lstrCondn3)

                '   -- 4   txtStuno
                larrSearchOpr = h_selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn4)

                '   -- 5  txtStuname
                larrSearchOpr = h_selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn5)

             
           
                '   -- 7  txtDesc
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
                lstrCondn7 = txtSearch.Text
                If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FPH_NARRATION", lstrCondn7)
            End If

            Dim str_cond As String = String.Empty
            gvJournal.Columns(8).Visible = False ' hide emailing error desc column
            If rblEmailStatus.SelectedValue = "E" Then
                str_cond = str_cond & " AND isnull(FPH_Email,0) =2 "
            ElseIf rblEmailStatus.SelectedValue = "N" Then
                str_cond = str_cond & " AND isnull(FPH_Email,0) =0 "
            ElseIf rblEmailStatus.SelectedValue = "P" Then
                str_cond = str_cond & " AND isnull(FPH_Email,0) =1 AND isnull(FPH_Fetched,0) =0 "
            ElseIf rblEmailStatus.SelectedValue = "F" Then
                str_cond = str_cond & " AND isnull(FPH_Email,0) =1 AND isnull(FPH_Fetched,0) =1 "
                gvJournal.Columns(8).Visible = True
            Else
                gvJournal.Columns(8).Visible = False
            End If

            str_Sql = " SELECT FPH_ID, FPH_BSU_ID, FPH_STU_BSU_ID, FPH_INOICENO, FPH_DT, FPH_ACD_ID, " _
            & " FPD_STU_ID, FPH_SCH_ID, FPD_GRD_ID, STU_NO, STU_NAME, GRD_DISPLAY, BSU_NAME," _
            & " SBL_DESCRIPTION, FPH_NARRATION," _
            & " ISNULL(( SELECT SUM(  FPD_AMOUNT) FROM TRANSPORT.VW_TRANSPORT_INVOICE_D WHERE FPH_BSU_ID=TTH.FPH_BSU_ID AND FPD_FPH_ID =TTH.FPH_ID AND STU_ID=TTH.FPD_STU_ID),0) AS AMOUNT, " _
            & " ISNULL(FPH_COMP_ID,0)FPH_COMP_ID,ISNULL(FPH_EmailStatus,'')FPH_EmailStatus FROM TRANSPORT.VW_TRANSPORT_INVOICE_H TTH " _
            & " where  FPH_BSU_ID='" & Session("sBSUID") & "'" _
            & " AND FPH_STU_BSU_ID='" & ddBusinessunit.SelectedItem.Value & "' " & str_cond & " " & str_Filter _
            & " ORDER BY FPH_DT  DESC ,FPH_INOICENO DESC,STU_NAME"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If

            txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn5

          

            txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
            txtSearch.Text = lstrCondn7

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblELA_ID As New Label, lblFPH_ID As New Label, lblFPH_INVNO As New Label
            lblELA_ID = TryCast(e.Row.FindControl("lblELA_ID"), Label)
            lblFPH_ID = TryCast(e.Row.FindControl("lblFPH_ID"), Label)
            lblFPH_INVNO = TryCast(e.Row.FindControl("lblReceipt"), Label)
            Dim hlEdit As New HyperLink, lbVoucher As New LinkButton
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            lbVoucher = TryCast(e.Row.FindControl("lbVoucher"), LinkButton)
            Dim COMP_ID As Integer = gvJournal.DataKeys(e.Row.RowIndex)(0)
            If hlEdit IsNot Nothing And lblELA_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "feeTransportPerformaInvoice.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
            If Not lbVoucher Is Nothing AndAlso Not lblFPH_ID Is Nothing AndAlso Not lblFPH_INVNO Is Nothing Then
                Dim QueryString = "&FPHID=" & Encr_decrData.Encrypt(lblFPH_ID.Text) & "&INVNO=" & Encr_decrData.Encrypt(lblFPH_INVNO.Text) & "&SBSU=" & Encr_decrData.Encrypt(ddBusinessunit.SelectedValue) & "&COMPID=" & Encr_decrData.Encrypt(COMP_ID) & ""
                lbVoucher.Attributes.Add("onClick", "return ShowWindowWithClose('FeeReportInterpose.aspx?TYPE=" & Encr_decrData.Encrypt("TPINV") & QueryString & "', '', '60%', '75%');")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
        Session("liUserList") = New ArrayList
    End Sub


    Protected Sub lbVoucher_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblFPH_ID As Label = sender.Parent.parent.findcontrol("lblFPH_ID") 
            Dim lblFPH_INVNO As Label = sender.Parent.parent.findcontrol("lblReceipt")
            Dim gvr As GridViewRow = TryCast(sender.Parent.parent, GridViewRow)
            Dim COMP_ID As Integer = gvJournal.DataKeys(gvr.RowIndex)(0)
            If COMP_ID > 0 Then
                Session("ReportSource") = FeeTranspotInvoice.PrintReceiptComp(lblFPH_INVNO.Text, Session("sBsuid"), Me.ddBusinessunit.SelectedValue, _
                                            Session("sUsr_name"))
                h_print.Value = "print"
                h_Export.Value = "No"
            Else
                Session("ReportSource") = FeeTranspotInvoice.PrintReceipt(lblFPH_ID.Text, Session("sBsuid"), _
                            Session("sUsr_name"), ddBusinessunit.SelectedItem.Value)
                h_print.Value = "print"
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub 


    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBusinessunit.SelectedIndexChanged
        gridbind()
        Session("liUserList") = New ArrayList
    End Sub

    Protected Sub lbExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblFPH_ID As Label = sender.Parent.parent.findcontrol("lblFPH_ID")
            Dim lblFPH_INVNO As Label = sender.Parent.parent.findcontrol("lblReceipt")
            'PrintReceipt(lblFPH_ID.Text)
            Dim gvr As GridViewRow = TryCast(sender.Parent.parent, GridViewRow)
            'gvr.RowIndex
            Dim COMP_ID As Integer = gvJournal.DataKeys(gvr.RowIndex)(0)
            If COMP_ID > 0 Then
                Session("ReportSource") = FeeTranspotInvoice.PrintReceiptComp(lblFPH_INVNO.Text, Session("sBsuid"), Me.ddBusinessunit.SelectedValue, _
                                            Session("sUsr_name"))
                h_print.Value = "print"
                h_Export.Value = "No"
            Else
                Session("ReportSource") = FeeTranspotInvoice.PrintReceipt(lblFPH_ID.Text, Session("sBsuid"), _
                            Session("sUsr_name"), ddBusinessunit.SelectedItem.Value)
                h_print.Value = "print"
                h_Export.Value = "Yes"
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Dim InvNos As String = ""
        SetChk(Page)
        If Session("liUserList").Count > 0 Then

            Dim lblFPH_ID As Label = sender.Parent.parent.findcontrol("lblFPH_ID")
            Dim lblFPH_INVNO As Label = sender.Parent.parent.findcontrol("lblReceipt")
            Dim i As Int16
            For i = 0 To Session("liUserList").Count - 1
                InvNos = InvNos & Session("liUserList")(i) & ","
            Next
            InvNos = Left(InvNos, Len(InvNos) - 1)

            If Mainclass.getDataValue("Select count(distinct FPH_PVM_ID) FROM TRANSPORT.VW_TRANSPORT_INVOICE_H WHERE  FPH_ID  in(" & InvNos & ") AND FPH_BSU_ID='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString") > 1 Then
                ' lblError.Text = "You can not print Different version at same time..!!!"
                usrMessageBar.ShowNotification("You can not print Different version at same time..!!!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If



            Session("ReportSource") = FeeTranspotInvoice.PrintReceipt(InvNos, Session("sBsuid"), _
                           Session("sUsr_name"), ddBusinessunit.SelectedItem.Value)
            h_print.Value = "print"
            h_Export.Value = "Yes"

        Else
            '  lblError.Text = "Please Select Invoice(s)!!!"
            usrMessageBar.ShowNotification("Please Select Invoice(s)!!!", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub
    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    If chk.Name = "chkEMail" Then Continue For
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub
    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub
    Protected Sub btnEmail_Click(sender As Object, e As EventArgs) Handles btnEmail.Click
        Dim chkControl As New HtmlInputCheckBox
        For Each grow As GridViewRow In gvJournal.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl Is Nothing Then Exit Sub
            If chkControl.Checked Then
                If Not doEMail(DirectCast(grow.FindControl("lblReceipt"), Label).Text) Then
                    Exit Sub
                End If
            End If
        Next
        '    lblError.Text = "Email Processed for Proforma Invoice"
        usrMessageBar.ShowNotification("Email Processed for Proforma Invoice", UserControls_usrMessageBar.WarningType.Danger)
        gridbind()
    End Sub
    Private Function doEMail(ByVal InvoiceNo As String) As Boolean
        Dim retval As String = String.Empty
        Dim sqlParam(8) As SqlParameter
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        sqlParam(0) = Mainclass.CreateSqlParameter("@FPH_INVOICENO", InvoiceNo, SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBSUID"), SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@STU_BSU_ID", Me.ddBusinessunit.SelectedValue, SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@ACD_ID", "0", SqlDbType.VarChar)
        sqlParam(4) = Mainclass.CreateSqlParameter("@AUD_WINUSER", Page.User.Identity.Name.ToString().Trim, SqlDbType.VarChar)
        sqlParam(5) = Mainclass.CreateSqlParameter("@Aud_form", Master.MenuName.ToString().Trim, SqlDbType.VarChar)
        sqlParam(6) = Mainclass.CreateSqlParameter("@Aud_user", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(7) = Mainclass.CreateSqlParameter("@Aud_module", Session("sModule"), SqlDbType.VarChar)
        sqlParam(8) = Mainclass.CreateSqlParameter("@v_ReturnMsg", retval, SqlDbType.VarChar, True)
        Dim str_success As String
        str_success = Mainclass.ExecuteParamQRY(str_conn, "EMailProformaInvoice", sqlParam)
        If str_success <> 0 Then
            ' lblError.Text = sqlParam(8).Value
            usrMessageBar.ShowNotification( sqlParam(8).Value, UserControls_usrMessageBar.WarningType.Danger)
            doEMail = False
        Else
            doEMail = True
        End If

    End Function

    Protected Sub rblEmailStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblEmailStatus.SelectedIndexChanged
        gridbind()
    End Sub
End Class
