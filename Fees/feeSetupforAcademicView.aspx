<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeSetupforAcademicView.aspx.vb" Inherits="Fees_feeSetupforAcademicView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">

        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/ChkCopy/) > 0)) {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Fee Setup
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr valign="top">
                        <td valign="top" align="left">
                            <asp:HyperLink ID="hlAddnew" runat="server">Add New</asp:HyperLink>
                            <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>--%>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" valign="top">
                            <table width="100%" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" width="30%">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" width="50%">
                                                    <asp:DropDownList ID="ddlAcademic" runat="server" AutoPostBack="True"
                                                        DataSourceID="odsGetBSUAcademicYear" DataTextField="ACY_DESCR" DataValueField="ACD_ID" SkinID="DropDownListNormal">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" width="50%">
                                                    <asp:CheckBox ID="ChkCopy" runat="server" Text="Copy Fee Setup" AutoPostBack="True" OnCheckedChanged="ChkCopy_CheckedChanged1"></asp:CheckBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td align="right" id="TrNew" runat="server" visible="false" width="20%">
                                        <span class="field-label">New Academic Year</span> </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcdYearCopy" runat="server">
                                        </asp:DropDownList>
                                        <asp:Button ID="btnCopy" runat="server" CssClass="button"
                                            OnClick="btnSave_Click" Text="Copy" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="4">
                            <asp:GridView ID="gvJournal" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="CheckMaster" runat="server" onclick="fnSelectAll(this)"></asp:CheckBox>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox Style="cursor: hand" ID="ChkSelect" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Grade<br />
                                            <asp:TextBox ID="txtAgegroup" runat="server" Width="25%"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("GRD_DISPLAY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Fee Types<br />
                                            <asp:TextBox ID="txtFeeTypes" runat="server" Width="40%"></asp:TextBox>
                                            <asp:ImageButton ID="btnFtypSearcha" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("FEE_TYPES")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="From Date">
                                        <HeaderTemplate>
                                            From Dt<br />
                                            <asp:TextBox ID="txtFDate" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("FSP_FROMDT","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="150px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date">
                                        <HeaderTemplate>
                                            Stream<br />
                                            <asp:TextBox ID="txtTDate" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="labStream" runat="server" Text='<%# Bind("STM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="150px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FEE_ID" Visible="False">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGRD_ID" runat="server" Text='<%# Bind("GRD_ID") %>'></asp:Label>
                                            <asp:Label ID="lbStrId" runat="server" Text='<%# Bind("FSP_STM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
    <asp:ObjectDataSource ID="odsGetBSUAcademicYear" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetBSUAcademicYear" TypeName="FeeCommon">
        <SelectParameters>
            <asp:SessionParameter Name="BSU_ID" SessionField="sBsuid" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
