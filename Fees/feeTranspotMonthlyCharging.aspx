<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeTranspotMonthlyCharging.aspx.vb" Inherits="Fees_feeTranspotMonthlyCharging" Theme="General" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="true">
<%--    <link href="../Scripts/JQuery-ui-1.10.3.css" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>--%>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(function () {
            var $ajaxImage = $("#ajaxImage");
            var $progressbar = $("#progressbar");
            var $fsProgress = $("#fsProgress");
            // Hide the AJAX spinner image and the progress bar, by default.
            $ajaxImage.hide();
            $progressbar.hide();
            $fsProgress.hide();
            var intervalHandle = setInterval(function () {

                var $statusDiv = $("#statusDiv");
                var $startProcessButton = $("#<%= btnSave.ClientID%>");
                var $PostbackButton = $("#<%= btnPostback.ClientID%>");
                var $lblError = $("#<%= lblError.ClientID%>");
                var $h_status = $("#<%= h_STATUS.ClientID%>");
                var $h_errormessage = $("#<%= h_errormessage.ClientID%>");
                //// Go hit the web service to get the latest status
                if ($("#<%=h_Processeing.ClientID %>").val() == '1') {
                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: "../AjaxServices.asmx/GetStatus",
                        contentType: "application/json; charset=utf-8",
                        success: function (response, status, xhr) {
                            if (response.d == null) {
                                $ajaxImage.hide();
                                $progressbar.hide();
                                $fsProgress.hide();
                                // Stop this setInterval loop that we're in.
                                clearInterval(intervalHandle);
                            }
                            else {
                                // Show the AJAX spinner image and progress bar
                                $fsProgress.show();
                                $ajaxImage.show();
                                $progressbar.show();
                                $startProcessButton.attr("disabled", "disabled");
                                // Set the status text and the progress back value
                                $statusDiv.html(response.d.PercentComplete + "% " + response.d.WorkDescription);
                                $progressbar.progressbar({
                                    value: response.d.PercentComplete
                                });
                                $lblError.html(response.d.WorkDescription);
                                // Process is complete, start to reset the UI (the response.d == null above, will do the rest).
                                if (response.d.IsComplete) {
                                    $startProcessButton.removeAttr("disabled");
                                    $ajaxImage.hide();
                                    $progressbar.hide();
                                    $fsProgress.hide();
                                    $lblError.html(response.d.WorkDescription);
                                    $h_errormessage.val(response.d.WorkDescription);
                                    if (response.d.IsSuccess) {
                                        $h_status.val("0")
                                        $PostbackButton.click();
                                        $PostbackButton.unbind('click');
                                        $h_status.val("")
                                    }
                                    else
                                        $h_status.val("1");
                                }
                            }
                        }
                    });
                }
            }, 1000);
        });


        function GetStudent() {
            var sFeatures;
            var sFeatures;
            sFeatures = "dialogWidth: 875px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GroupSel = document.getElementById('<%=chkStudentwise.ClientID %>').checked;
            var COMP_ID = '';
            var STUD_TYP = document.getElementById('<%=radEnquiry.ClientID %>').checked;
            var url;
            if (GroupSel == true)
                if (STUD_TYP == true) {
                    var ACD_ID = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
                    url = "ShowStudentMultiTransport.aspx?TYPE=ENQ_COMP&COMP_ID=" + COMP_ID + "&MULTI_SEL=" + GroupSel + "&ACD_ID=" + ACD_ID;
                }
                else {
                    url = "ShowStudentMultiTransport.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu=" + document.getElementById('<%= ddBusinessunit.ClientID %>').value;
                    //result = window.showModalDialog(url, "", sFeatures);
                    var oWnd = radopen(url, "pop_getstudent");
                }
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=txtStudName.ClientID %>').value = 'The following Student(s) selected';
            document.getElementById('<%=h_STUD_ID.ClientID %>').value = result;
        }
        return true;
    }
    --%>
            
                        
        } 

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.NameandCode;
                document.getElementById('<%=txtStudName.ClientID %>').value = 'The following Student(s) selected';
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = arg.NameandCode;
                __doPostBack('<%=txtStudName.ClientID%>', 'TextChanged');
               
            }
        }

    

function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>


    <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getstudent" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Transport Fee Charging
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table align="center" width=100%">
        <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
            </td>
        </tr>
    </table>
    <table align="center" width="100%">
        
        <tr>
            <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
            <td align="left" width="30%">
                <asp:DropDownList ID="ddBusinessunit" runat="server" DataSourceID="odsSERVICES_BSU_M"
                    DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" AutoPostBack="True" TabIndex="5">
                </asp:DropDownList>
            </td>
            <td align="left" width="20%"></td>
            <td align="left" width="30%"></td>
        </tr>
        <tr>
            <td align="left"><span class="field-label">Academic Year</span></td>
            <td align="left">
                <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" TabIndex="10">
                </asp:DropDownList></td>
            <td align="left"><span class="field-label">Date</span></td>
            <td align="left"><asp:TextBox ID="txtFrom" runat="server" TabIndex="15"></asp:TextBox>
                <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="20" />&nbsp;
         
            </td>
        </tr>
        <tr>
            <td align="left"><asp:CheckBox ID="chkStudentwise" runat="server"  CssClass="field-label" Text="Select Student" /></td>
            <td align="left" colspan="3">
                <asp:RadioButton ID="radStudent" runat="server" CssClass="field-label" Checked="True" GroupName="STUD_ENQ"
                    Text="Student" />
                <asp:RadioButton ID="radEnquiry" runat="server" CssClass="field-label" GroupName="STUD_ENQ" Text="Enquiry" Enabled="False"  />
                <br />
                <asp:TextBox ID="txtStudName" runat="server" AutoPostBack="true" OnTextChanged="txtStudName_TextChanged"></asp:TextBox>
                <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetStudent(); return false;" />
                <asp:LinkButton ID="lblAddNewStudent" runat="server" CausesValidation="False">Add</asp:LinkButton><br />
                <asp:GridView ID="gvStudentDetails" runat="server" CssClass="table table-bordered table-row" AllowPaging="True" AutoGenerateColumns="False"
                 Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Student #">
                            <ItemTemplate>
                                <asp:Label ID="lblStudID" runat="server" Text='<%# bind("STU_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student Name">
                            <ItemTemplate>
                                <asp:Label ID="lblStudentName" runat="server" Text='<%# bind("STU_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete" Visible="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDelete" runat="server">Delete</asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <fieldset id="fsProgress">
                    <legend>Processing Status</legend>
                    <div id="ajaxImage" style="display: inline;">
                        <img alt="" src="../Images/Misc/AjaxLoading.gif" />
                    </div>
                    <div id="statusDiv"></div>
                    <div id="progressbar" style="height: 20px; width: 200px;"></div>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="105" />
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                <asp:Button ID="btnPostback" runat="server" CssClass="button" CausesValidation="False" style="width: 0px !important; height: 0px !important; min-width: 0px;"    TabIndex="5000"  OnClick="btnPostback_Click" /></td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
        PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
    </ajaxToolkit:CalendarExtender>
    <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" SelectMethod="SERVICES_BSU_M"
        TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:HiddenField ID="h_STUD_ID" runat="server" />
    <asp:HiddenField ID="h_STATUS" runat="server" />
    <asp:HiddenField ID="h_errormessage" runat="server" />
    <asp:HiddenField ID="h_Processeing" runat="server" />
    <div id="divLoading" class="screenCenter" style="display: none">
        <br />
        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading1.gif" /><br />
        Please Wait....
    </div>

                </div>
            </div>
        </div>
</asp:Content>



