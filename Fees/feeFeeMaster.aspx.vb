Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_feeFeeMaster
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            ''acFEE_CHRG_INADV.SelectedUnit = Session("sBsuid")
            'acFEE_CONCESSION_ACT_ID.SelectedUnit = Session("sBsuid")
            'acFEE_DISC_ACT_ID.SelectedUnit = Session("sBsuid")
            'acFEE_INC_ACT_ID.SelectedUnit = Session("sBsuid")
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If

            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            txtLinkToStage.Attributes.Add("ReadOnly", "ReadOnly")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "F100115" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("viewid") <> "" Then
                setViewData()
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            Else
                ResetViewData()
            End If
        End If
    End Sub

    Public Shared Function GetFEEGROUP_M() As DataTable
        Dim sql_query As String = " SELECT FGP_ID , FGP_DESCR FROM  FEES.FEE_GROUP_M "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Private Sub setModifyHeader(ByVal p_Modifyid As String)
        On Error Resume Next 'setting header data on view/edit
        Dim str_Sql As String
        str_Sql = "SELECT     FEE_ID, FEE_DESCR, FEE_FTP_ID, FEE_bConcession, FEE_ChargedFor_SCH_ID, " _
        & " FEE_Collection_SCH_ID, FEE_bFixed, FEE_SVC_ID, FEE_COL_ID, FEE_INC_ACT_ID, " _
        & " FEE_CHRG_INADV_ACT_ID,FEE_AX_INC_ACT_ID,FEE_AX_CHRG_INADV_ACT_ID, FEE_CONCESSION_ACT_ID, FEE_DISC_ACT_ID, FEE_ORDER, FEE_SETOFFORDER, " _
        & " FEE_LATEAMTTYPE, FEE_LATEAMT, FEE_LATEDAYS, FEE_bREVENUE_SCH_ID, FEE_bJOINPRORATA, " _
        & " FEE_bDiscontinuePRORATA,FEE_bSETUPBYGRADE, PRO_ID, PRO_DESCRIPTION,FEE_FGP_ID,FEE_bShowInOtherColl FROM fees.FEES_M " & _
        " INNER JOIN PROCESSFO_SYS_M ON FEE_PRO_ID = PROCESSFO_SYS_M.PRO_ID WHERE FEE_ID='" & p_Modifyid & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            txtRemarks.Text = ds.Tables(0).Rows(0)("FEE_DESCR")
            chkConcession.Checked = ds.Tables(0).Rows(0)("FEE_bConcession")
            ddFeeType.DataBind()
            ddServicelink.DataBind()
            ddCollectionSchedule.DataBind()
            ddCollectiontype.DataBind()
            ddChargeschedule.DataBind()

            ddFeeType.SelectedIndex = -1
            ddServicelink.SelectedIndex = -1
            ddCollectionSchedule.SelectedIndex = -1
            ddCollectiontype.SelectedIndex = -1
            ddChargeschedule.SelectedIndex = -1

            ddFeeType.Items.FindByValue(ds.Tables(0).Rows(0)("FEE_FTP_ID")).Selected = True
            ddServicelink.Items.FindByValue(ds.Tables(0).Rows(0)("FEE_SVC_ID")).Selected = True
            ddCollectiontype.Items.FindByValue(ds.Tables(0).Rows(0)("FEE_COL_ID")).Selected = True

            ddCollectionSchedule.Items.FindByValue(ds.Tables(0).Rows(0)("FEE_Collection_SCH_ID")).Selected = True
            ddChargeschedule.Items.FindByValue(ds.Tables(0).Rows(0)("FEE_ChargedFor_SCH_ID")).Selected = True

            chkFixed.Checked = ds.Tables(0).Rows(0)("FEE_bFixed")
            txtFEE_INC_ACT_ID.Text = ds.Tables(0).Rows(0)("FEE_INC_ACT_ID")
            txtFEE_INC_ACT_ID_TextChanged(txtFEE_INC_ACT_ID, Nothing)

            txtFEE_CHRG_INADV.Text = ds.Tables(0).Rows(0)("FEE_CHRG_INADV_ACT_ID")
            txtFEE_CHRG_INADV_TextChanged(txtFEE_CHRG_INADV, Nothing)
            txtFEE_CONCESSION_ACT_ID.Text = ds.Tables(0).Rows(0)("FEE_CONCESSION_ACT_ID")
            'txtFEE_CONCESSION_ACT_ID_TextChanged(txtFEE_CONCESSION_ACT_ID, Nothing)
            txtFEE_DISC_ACT_ID.Text = ds.Tables(0).Rows(0)("FEE_DISC_ACT_ID")
            'txtFEE_DISC_ACT_ID_TextChanged(txtFEE_DISC_ACT_ID, Nothing)
            txtFeeorder.Text = ds.Tables(0).Rows(0)("FEE_ORDER")
            txtSettlementorder.Text = ds.Tables(0).Rows(0)("FEE_SETOFFORDER")
            txtLateAmount.Text = ds.Tables(0).Rows(0)("FEE_LATEAMT")

            txtLatefeedays.Text = ds.Tables(0).Rows(0)("FEE_LATEDAYS")
            chkRevenueMonthly.Checked = ds.Tables(0).Rows(0)("FEE_bREVENUE_SCH_ID")
            chkJoin.Checked = ds.Tables(0).Rows(0)("FEE_bJOINPRORATA")
            chkSetupbygrade.Checked = ds.Tables(0).Rows(0)("FEE_bSETUPBYGRADE")
            chkDiscontinue.Checked = ds.Tables(0).Rows(0)("FEE_bDiscontinuePRORATA")

            hf_PRO_ID.Value = ds.Tables(0).Rows(0)("PRO_ID")
            txtLinkToStage.Text = ds.Tables(0).Rows(0)("PRO_DESCRIPTION")

            txtFEE_AX_INC_ACT_ID.Text = ds.Tables(0).Rows(0)("FEE_AX_INC_ACT_ID")
            txtFEE_AX_INC_ACT_ID_TextChanged(txtFEE_AX_INC_ACT_ID, Nothing)
            txtFEE_AX_CHRG_INADV.Text = ds.Tables(0).Rows(0)("FEE_AX_CHRG_INADV_ACT_ID")
            txtFEE_AX_CHRG_INADV_TextChanged(txtFEE_AX_CHRG_INADV, Nothing)

            ddFeeGroup.SelectedValue = ds.Tables(0).Rows(0)("FEE_FGP_ID")
            chk_ShowFeeHead.Checked = ds.Tables(0).Rows(0)("FEE_bShowInOtherColl")

            'h_catid.Value = ds.Tables(0).Rows(0)("BGS_ECT_ID")
            'txtCategory.Text = ds.Tables(0).Rows(0)("ECT_DESCR").ToString
            ''ddMonthstatusPeriodically.SelectedIndex = -1
            ''ddMonthstatusPeriodically.Items.FindByValue(ds.Tables(0).Rows(0)("BLS_ELT_ID").ToString).Selected = True
            'If IsDate(ds.Tables(0).Rows(0)("BGS_DTFROM")) Then
            '    txtFrom.Text = Format(CDate(ds.Tables(0).Rows(0)("BGS_DTFROM")), "dd/MMM/yyyy")
            'End If
            'If IsDate(ds.Tables(0).Rows(0)("BGS_DTTO")) Then
            '    txtTo.Text = Format(CDate(ds.Tables(0).Rows(0)("BGS_DTTO")), "dd/MMM/yyyy")
            'End If
            'txtRemarks.Text = ds.Tables(0).Rows(0)("BGS_DESCRIPTION")
            'txtGrafrom.Text = ds.Tables(0).Rows(0)("BGS_SLABFROM")
            ''txtGrato.Text = ds.Tables(0).Rows(0)("BGS_SLABTO")
            ''txtProvdays.Text = ds.Tables(0).Rows(0)("BGS_PROVISIONDAYS")
            ''txtActualDays.Text = ds.Tables(0).Rows(0)("BGS_ACTUALDAYS")
            ''ddContracttype.Items.FindByValue(ds.Tables(0).Rows(0)("BGS_CONTRACTTYPE")).Selected = True
            'ddTrantype.Items.FindByValue(ds.Tables(0).Rows(0)("BGS_TRANTYPE")).Selected = True
        End If
    End Sub


    Sub setViewData()
        txtFEE_INC_ACT_ID.Attributes.Add("readonly", "readonly")
        txtFEE_CHRG_INADV.Attributes.Add("readonly", "readonly")
        txtFEE_CONCESSION_ACT_ID.Attributes.Add("readonly", "readonly")
        txtFEE_DISC_ACT_ID.Attributes.Add("readonly", "readonly")
        txtFeeorder.Attributes.Add("readonly", "readonly")
        txtSettlementorder.Attributes.Add("readonly", "readonly")
        txtLateAmount.Attributes.Add("readonly", "readonly")
        txtLatefeedays.Attributes.Add("readonly", "readonly")
        ddFeeGroup.Enabled = False

        ddFeeType.Enabled = False
        chkConcession.Enabled = False
        ddServicelink.Enabled = False
        ddCollectionSchedule.Enabled = False
        chkFixed.Enabled = False
        rbAmount.Enabled = False
        ddChargeschedule.Enabled = False
        chkJoin.Enabled = False
        rbPercentage.Enabled = False
        ddServicelink.Enabled = False
        ddCollectiontype.Enabled = False
        chkRevenueMonthly.Enabled = False
        chkDiscontinue.Enabled = False
        chkSetupbygrade.Enabled = False
        img1.Enabled = False
        img2.Enabled = False
        img3.Enabled = False
        img4.Enabled = False
        ImageButton1.Enabled = False
    End Sub


    Sub ResetViewData()
        'ddFeeType.Enabled = True
        chkConcession.Enabled = True
        ddServicelink.Enabled = True
        ddCollectionSchedule.Enabled = True
        chkDiscontinue.Enabled = True
        chkFixed.Enabled = True
        rbAmount.Enabled = True
        ddChargeschedule.Enabled = True
        chkJoin.Enabled = True
        rbPercentage.Enabled = True
        ddServicelink.Enabled = True
        ddCollectiontype.Enabled = True
        chkRevenueMonthly.Enabled = True
        chkSetupbygrade.Enabled = True
        img1.Enabled = True
        img2.Enabled = True
        img3.Enabled = True
        img4.Enabled = True
        ImageButton1.Enabled = True

        'txtRemarks.Attributes.Remove("readonly")
        txtFEE_INC_ACT_ID.Attributes.Remove("readonly")
        txtFEE_CHRG_INADV.Attributes.Remove("readonly")
        txtFEE_AX_INC_ACT_ID.Attributes.Remove("readonly")
        txtFEE_AX_CHRG_INADV.Attributes.Remove("readonly")
        txtFEE_CONCESSION_ACT_ID.Attributes.Remove("readonly")
        txtFEE_DISC_ACT_ID.Attributes.Remove("readonly")
        txtFeeorder.Attributes.Remove("readonly")
        txtSettlementorder.Attributes.Remove("readonly")
        txtLateAmount.Attributes.Remove("readonly")
        txtLatefeedays.Attributes.Remove("readonly")
        If ViewState("datamode") = "add" Then
            FeeMaster.SetDefaultLinkToState(txtLinkToStage, hf_PRO_ID)
        End If
    End Sub


    Sub clear_All()
        txtRemarks.Text = ""
        txtFEE_INC_ACT_ID.Text = ""
        txtFEE_CHRG_INADV.Text = ""
        txtFEE_CONCESSION_ACT_ID.Text = ""
        txtFEE_DISC_ACT_ID.Text = ""
        txtFeeorder.Text = ""
        txtSettlementorder.Text = ""
        txtLateAmount.Text = ""
        txtLatefeedays.Text = ""

        txtFEE_AX_INC_ACT_ID.Text = ""
        txtFEE_AX_CHRG_INADV.Text = ""
    End Sub


    Function checkErrors() As String
        Dim Str_Error As String = ""
        'If txtRemarks.Text.Trim = "" Then
        '    Str_Error = "Please Enter Remarks <Br>"
        'End If
        'If txtFrom.Text.Trim = "" Then
        '    Str_Error = Str_Error & "Please Enter From Date <Br>"
        'Else
        '    If IsDate(txtFrom.Text) = False Then
        '        Str_Error = Str_Error & "Please Enter Valid From Date <Br>"
        '    End If
        'End If
        'If IsDate(txtFrom.Text) = False Then
        '    Str_Error = Str_Error & "Please Valid To Date <Br>"
        'End If
        If IsNumeric(txtFeeorder.Text) = False Then
            Str_Error = Str_Error & "Please Valid Fee order <Br>"
        End If
        If IsNumeric(txtSettlementorder.Text) = False Then
            Str_Error = Str_Error & "Please Valid Settlement order <Br>"
        End If

        If IsNumeric(txtLateAmount.Text) = False Then
            Str_Error = Str_Error & "Please Valid Late Amount/Percentage <Br>"
        End If
        If IsNumeric(txtLatefeedays.Text) = False Then
            Str_Error = Str_Error & "Please Valid Late fee days <Br>"
        End If

        Dim STR_ACCCODE As String = txtFEE_INC_ACT_ID.Text.Split("-")(0)
        Dim STR_ACCNAME As String = AccountFunctions.Validate_Account(STR_ACCCODE, Session("sbsuid"), "INCOME")
        If STR_ACCNAME = "" Then
            Str_Error = Str_Error & "Invalid Income Account <Br>"
        End If

        STR_ACCCODE = txtFEE_CHRG_INADV.Text.Split("-")(0)
        STR_ACCNAME = AccountFunctions.Validate_Account(STR_ACCCODE, Session("sbsuid"), "CHARGE")
        If STR_ACCNAME = "" Then
            Str_Error = Str_Error & "Invalid Charge Account <Br>"
        End If

        STR_ACCCODE = txtFEE_AX_CHRG_INADV.Text.Split("-")(0)
        STR_ACCNAME = AccountFunctions.Validate_Account(STR_ACCCODE, Session("sbsuid"), "CHARGE")
        If STR_ACCNAME = "" Then
            Str_Error = Str_Error & "Invalid Charge (AX) Account <Br>"
        End If

        STR_ACCCODE = txtFEE_AX_INC_ACT_ID.Text.Split("-")(0)
        STR_ACCNAME = AccountFunctions.Validate_Account(STR_ACCCODE, Session("sbsuid"), "INCOME")
        If STR_ACCNAME = "" Then
            Str_Error = Str_Error & "Invalid Income (AX) Account <Br>"
        End If


        If ddFeeGroup.SelectedValue <= 0 Then
            Str_Error = Str_Error & "Please Select Valid Fee Group <Br>"
        End If
        'STR_ACCCODE = txtFEE_CONCESSION_ACT_ID.Text.Split("-")(0)
        'STR_ACCNAME = AccountFunctions.Validate_Account(STR_ACCCODE, Session("sbsuid"), "CONCESSION")
        'If STR_ACCNAME = "" Then
        '    Str_Error = Str_Error & "Invalid Concession Account <Br>"
        'End If
        'STR_ACCCODE = txtFEE_DISC_ACT_ID.Text.Split("-")(0)
        'STR_ACCNAME = AccountFunctions.Validate_Account(STR_ACCCODE, Session("sbsuid"), "FEEDISC")
        'If STR_ACCNAME = "" Then
        '    Str_Error = Str_Error & "Invalid Discount Account <Br>"
        '    txtFEE_DISC_ACT_ID.Focus() 
        'End If

        checkErrors = Str_Error
    End Function

    Private Function AllowEdit() As Boolean
        If ViewState("datamode") = "edit" Then
            Dim Fee_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            Dim strSql As String = "SELECT COUNT(*) FROM FEES.FEESETUP_S WHERE FSP_FEE_ID = " & Fee_ID
            Dim retVal As Integer = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, strSql)
            If retVal > 0 Then Return False Else Return True
        End If
        Return True
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Warning)
            Exit Sub
        End If
        If txtLateAmount.Text = "" Then txtLateAmount.Text = "0"
        If txtLatefeedays.Text = "" Then txtLatefeedays.Text = "0"
        Dim str_error As String = checkErrors()
        If str_error <> "" Then
            'lblError.Text = "Please check the Following : <br>" & str_error
            usrMessageBar.ShowNotification("Please check the Following : <br>" & str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            Dim iLatefeetype As Integer = 1
            Dim Fee_ID As String = "0"
            If ViewState("datamode") = "edit" Then
                Fee_ID = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                'If Not AllowEdit() Then
                retval = 201
                'Else
                If rbPercentage.Checked Then
                    iLatefeetype = 2
                End If
                retval = FeeMaster.F_SaveFEES_M(Fee_ID, txtRemarks.Text, ddFeeType.SelectedItem.Value, _
                chkConcession.Checked, ddChargeschedule.SelectedItem.Value, ddCollectionSchedule.SelectedItem.Value, _
                chkFixed.Checked, ddServicelink.SelectedItem.Value, ddCollectiontype.SelectedItem.Value, _
                txtFEE_INC_ACT_ID.Text.Split("-")(0), txtFEE_CHRG_INADV.Text.Split("-")(0), _
                txtFEE_CONCESSION_ACT_ID.Text.Split("-")(0), txtFEE_DISC_ACT_ID.Text.Split("-")(0), _
                txtFeeorder.Text, txtSettlementorder.Text, iLatefeetype, txtLateAmount.Text, _
                txtLatefeedays.Text, chkRevenueMonthly.Checked, chkJoin.Checked, _
                chkDiscontinue.Checked, chkSetupbygrade.Checked, hf_PRO_ID.Value,
                txtFEE_AX_INC_ACT_ID.Text.Split("-")(0), txtFEE_AX_CHRG_INADV.Text.Split("-")(0), ddFeeGroup.SelectedItem.Value, ddEventType.SelectedItem.Value, IIf(chk_ShowFeeHead.Checked, 1, 0),
                stTrans)
                'End If
            Else
                If rbPercentage.Checked Then
                    iLatefeetype = 2
                End If
                retval = FeeMaster.F_SaveFEES_M(Fee_ID, txtRemarks.Text, ddFeeType.SelectedItem.Value, _
                chkConcession.Checked, ddChargeschedule.SelectedItem.Value, ddCollectionSchedule.SelectedItem.Value, _
                chkFixed.Checked, ddServicelink.SelectedItem.Value, ddCollectiontype.SelectedItem.Value, _
                txtFEE_INC_ACT_ID.Text.Split("-")(0), txtFEE_CHRG_INADV.Text.Split("-")(0), _
                txtFEE_CONCESSION_ACT_ID.Text.Split("-")(0), txtFEE_DISC_ACT_ID.Text.Split("-")(0), _
                txtFeeorder.Text, txtSettlementorder.Text, iLatefeetype, txtLateAmount.Text, _
                txtLatefeedays.Text, chkRevenueMonthly.Checked, chkJoin.Checked, _
                chkDiscontinue.Checked, chkSetupbygrade.Checked, hf_PRO_ID.Value,
                txtFEE_AX_INC_ACT_ID.Text.Split("-")(0), txtFEE_AX_CHRG_INADV.Text.Split("-")(0), ddFeeGroup.SelectedItem.Value, ddEventType.SelectedItem.Value, IIf(chk_ShowFeeHead.Checked, 1, 0),
                stTrans)
            End If
            If retval = "0" Then
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                txtRemarks.Text & "-", _
                "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                'lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                clear_All()
            Else
                stTrans.Rollback()
                If retval = 201 Then
                    'lblError.Text = "The Fee Type is already in use. Editing is not allowed"
                    usrMessageBar.ShowNotification("The Fee Type is already in use. Editing is not allowed", UserControls_usrMessageBar.WarningType.Information)
                Else
                    'lblError.Text = "Cannot save. There is some errors"
                    usrMessageBar.ShowNotification("Cannot save. There is some errors", UserControls_usrMessageBar.WarningType.Danger)
                End If
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            'lblError.Text = "Cannot save. There is some errors"
            usrMessageBar.ShowNotification("Cannot save. There is some errors", UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            setViewData()
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub


    Protected Sub btnAdd_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ResetViewData()
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()
    End Sub


    Protected Sub txtFEE_INC_ACT_ID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFEE_INC_ACT_ID.TextChanged
        chk_txtFEE_INC_ACT_ID()
    End Sub


    Protected Sub txtFEE_CHRG_INADV_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFEE_CHRG_INADV.TextChanged
        chk_txtFEE_CHRG_INADV()
    End Sub

    Protected Sub txtFEE_AX_INC_ACT_ID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFEE_INC_ACT_ID.TextChanged
        chk_txtFEE_AX_INC_ACT_ID()
    End Sub


    Protected Sub txtFEE_AX_CHRG_INADV_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFEE_CHRG_INADV.TextChanged
        chk_txtFEE_AX_CHRG_INADV()
    End Sub


    Protected Sub txtFEE_CONCESSION_ACT_ID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFEE_CONCESSION_ACT_ID.TextChanged
        chk_txtFEE_CONCESSION_ACT_ID()
    End Sub


    Protected Sub txtFEE_DISC_ACT_ID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFEE_DISC_ACT_ID.TextChanged
        chk_txtFEE_DISC_ACT_ID()
    End Sub


    Sub chk_txtFEE_INC_ACT_ID()
        Dim STR_ACCCODE As String = txtFEE_INC_ACT_ID.Text.Split("-")(0)
        Dim STR_ACCNAME As String = AccountFunctions.Validate_Account(STR_ACCCODE, Session("sbsuid"), "INCOME")
        If STR_ACCNAME = "" Then
            'lblError.Text = "Invalid Income Account"
            usrMessageBar.ShowNotification("Invalid Income Account", UserControls_usrMessageBar.WarningType.Danger)
            txtFEE_INC_ACT_ID.Focus()
        Else
            txtFEE_INC_ACT_ID.Text = STR_ACCCODE & "-" & STR_ACCNAME
            txtFEE_CHRG_INADV.Focus()
        End If
    End Sub


    Sub chk_txtFEE_CHRG_INADV()
        Dim STR_ACCCODE As String = txtFEE_CHRG_INADV.Text.Split("-")(0)
        Dim STR_ACCNAME As String = AccountFunctions.Validate_Account(STR_ACCCODE, Session("sbsuid"), "CHARGE")
        If STR_ACCNAME = "" Then
            'lblError.Text = "Invalid Charge Account"
            usrMessageBar.ShowNotification("Invalid Income Account", UserControls_usrMessageBar.WarningType.Danger)
            txtFEE_CHRG_INADV.Focus()
        Else
            txtFEE_CHRG_INADV.Text = STR_ACCCODE & "-" & STR_ACCNAME
            txtFEE_CONCESSION_ACT_ID.Focus()
        End If
    End Sub

    Sub chk_txtFEE_AX_INC_ACT_ID()
        Dim STR_ACCCODE As String = txtFEE_AX_INC_ACT_ID.Text.Split("-")(0)
        Dim STR_ACCNAME As String = AccountFunctions.Validate_Account(STR_ACCCODE, Session("sbsuid"), "INCOME")
        If STR_ACCNAME = "" Then
            'lblError.Text = "Invalid Income Account"
            usrMessageBar.ShowNotification("Invalid Income Account", UserControls_usrMessageBar.WarningType.Danger)
            txtFEE_AX_INC_ACT_ID.Focus()
        Else
            txtFEE_AX_INC_ACT_ID.Text = STR_ACCCODE & "-" & STR_ACCNAME
            txtFEE_AX_CHRG_INADV.Focus()
        End If
    End Sub


    Sub chk_txtFEE_AX_CHRG_INADV()
        Dim STR_ACCCODE As String = txtFEE_AX_CHRG_INADV.Text.Split("-")(0)
        Dim STR_ACCNAME As String = AccountFunctions.Validate_Account(STR_ACCCODE, Session("sbsuid"), "CHARGE")
        If STR_ACCNAME = "" Then
            'lblError.Text = "Invalid Charge Account"
            usrMessageBar.ShowNotification("Invalid Charge Account", UserControls_usrMessageBar.WarningType.Danger)
            txtFEE_AX_CHRG_INADV.Focus()
        Else
            txtFEE_AX_CHRG_INADV.Text = STR_ACCCODE & "-" & STR_ACCNAME
            'txtFEE_CONCESSION_ACT_ID.Focus()
        End If
    End Sub

    Sub chk_txtFEE_CONCESSION_ACT_ID()
        Dim STR_ACCCODE As String = txtFEE_CONCESSION_ACT_ID.Text.Split("-")(0)
        Dim STR_ACCNAME As String = AccountFunctions.Validate_Account(STR_ACCCODE, Session("sbsuid"), "CONCESSION")
        If STR_ACCNAME = "" Then
            'lblError.Text = "Invalid Concession Account"
            usrMessageBar.ShowNotification("Invalid Income Account", UserControls_usrMessageBar.WarningType.Danger)
            txtFEE_CONCESSION_ACT_ID.Focus()
        Else
            txtFEE_CONCESSION_ACT_ID.Text = STR_ACCCODE & "-" & STR_ACCNAME
            txtFEE_DISC_ACT_ID.Focus()
        End If
    End Sub


    Sub chk_txtFEE_DISC_ACT_ID()
        Dim STR_ACCCODE As String = txtFEE_DISC_ACT_ID.Text.Split("-")(0)
        Dim STR_ACCNAME As String = AccountFunctions.Validate_Account(STR_ACCCODE, Session("sbsuid"), "FEEDISC")
        If STR_ACCNAME = "" Then
            'lblError.Text = "Invalid Discount Account"
            usrMessageBar.ShowNotification("Invalid Income Account", UserControls_usrMessageBar.WarningType.Danger)
            txtFEE_DISC_ACT_ID.Focus()
        Else
            txtFEE_DISC_ACT_ID.Text = STR_ACCCODE & "-" & STR_ACCNAME
        End If
    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        '   --- Remove ReadOnly From The Form
        If Not AllowEdit() Then
            'lblError.Text = "The Fee Type is already in use. Editing is not allowed"
            usrMessageBar.ShowNotification("The Fee Type is already in use. Editing is not allowed", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        ResetViewData()
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


End Class

