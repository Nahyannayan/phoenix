
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Data.Design
Imports UtilityObj

Partial Class FEEPerformaInvoiceView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim BSU_IsPERFORMAINV_POSTREQUIRE As Boolean = False

    Public Property CTY_ID() As Integer
        Get
            Return ViewState("CTY_ID")
        End Get
        Set(ByVal value As Integer)
            ViewState("CTY_ID") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(lbtnExport)
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try

                'BSU_IsPERFORMAINV_POSTREQUIRE
                Dim ds7 As New DataSet
                Dim pParms0(2) As SqlClient.SqlParameter
                pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms0(1).Value = Session("sBSUID")
                ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[dbo].[GetBsuDetails]", pParms0)
                BSU_IsPERFORMAINV_POSTREQUIRE = Convert.ToBoolean(ds7.Tables(0).Rows(0)("BSU_IsPERFORMAINV_POSTREQUIRE"))
                ViewState("BSU_IsPERFORMAINV_POSTREQUIRE") = BSU_IsPERFORMAINV_POSTREQUIRE
                If BSU_IsPERFORMAINV_POSTREQUIRE = True Then
                    postInv.Visible = True
                    btnPostInv.Visible = True
                Else
                    postInv.Visible = False
                    btnPostInv.Visible = False
                End If
                'BSU_IsPERFORMAINV_POSTREQUIRE


                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_PERFORMAINVOICE And ViewState("MainMnu_code") <> "F100136") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                CTY_ID = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BSU_COUNTRY_ID,0) BSU_COUNTRY_ID FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & Session("sBsuid") & "'")
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                hlAddNew.NavigateUrl = "PerformaInvoiceNew.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                ddAcademic.DataBind()
                ddAcademic.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                Session("liUserList") = New ArrayList
                GridBind()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
                '  lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
            Dim str_Sql As String = ""
            Dim ds As New DataSet
            Dim str_Filter As String = ""
            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7, lstrCondn8 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            lstrCondn8 = ""
            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then

                '   --- FILTER CONDITIONS ---
                '   -- 1   FPH_INOICENO
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtInvNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FPH_INOICENO", lstrCondn1)

                '   -- 2  Date
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "DATE", lstrCondn2)

                '   -- 3  COMP_NAME
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtCompany")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then
                    str_Filter = str_Filter & " AND (" & Replace(SetCondn(lstrOpr, "COMP_NAME", lstrCondn3), " AND ", " ")
                    str_Filter = str_Filter & Replace(SetCondn(lstrOpr, "EMail_ID", lstrCondn3), " AND ", " OR ") & " )"
                End If

                '   -- 4   ACAD_GRADE
                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRD_SEC", lstrCondn4)

                '   -- 5  STU_NO
                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStudNo")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn5)

                '   -- 5  STU_NAME
                larrSearchOpr = h_Selected_menu_6.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStudName")
                lstrCondn6 = txtSearch.Text
                If (lstrCondn6 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STUD_NAME", lstrCondn6)
            End If

            Dim str_cond As String = String.Empty
            str_cond = " BSU_ID = '" & Session("sBSUID") & "' AND FPH_ACD_ID = " & ddAcademic.SelectedValue

            gvJournal.Columns(8).Visible = False
            If radSent.Checked Then
                str_cond = str_cond & " AND isnull(FPH_Email,0) =2 "
                gvJournal.Columns(8).Visible = True
            End If
            If radNotSent.Checked Then
                str_cond = str_cond & " AND isnull(FPH_Email,0) =0 "
            End If
            If radProcessed.Checked Then
                str_cond = str_cond & " AND isnull(FPH_Email,0) =1 AND isnull(FPH_Fetched,0) =0 "
            End If
            If radEmailFailed.Checked Then
                str_cond = str_cond & " AND isnull(FPH_Email,0) =1 AND isnull(FPH_Fetched,0) =1 "
                gvJournal.Columns(8).Visible = True
            End If

            If radStudent.Checked Then
                str_Sql = "SELECT FPH_ID, MAX(isnull(COMP_NAME,''))  + case when MAX(isnull(COMP_NAME,'')) <>'' then '<BR/>' else '' end  + MAX(isnull([EMail_ID],'')) COMP_NAME,DATE, " & _
                " MAX(isnull(STUD_NAME,'')) STUD_NAME, MAX(isnull(STU_NO,'')) STU_NO,MAX(GRD_SEC) GRD_SEC," & _
                " MAX(isnull(ACAD_GRADE,'')) ACAD_GRADE , MAX(isnull(FPH_COMP_ID,'')) FPH_COMP_ID, " & _
                " FPH_INOICENO,CASE WHEN ISNULL(FPH_EmailStatus, '') LIKE '%Successfully sent%' THEN ISNULL(FPH_EmailStatus, '') + ' ON ' + CAST(FPH_EMAIL_DT AS VARCHAR) ELSE FPH_EmailStatus END FPH_EmailStatus " & _
                " FROM FEES.vw_OSO_FEES_PERFORMAINVOICE " & _
                " WHERE FPH_STU_TYPE='S' AND ISNULL(FPH_bAdvanceBooking,0) = 0 AND "
            ElseIf radEnquiry.Checked Then
                str_Sql = "SELECT FPH_ID, MAX(isnull(COMP_NAME,'')) COMP_NAME,DATE, " & _
                " MAX(isnull(STUD_NAME,'')) STUD_NAME, MAX(isnull(STU_NO,'')) STU_NO,MAX(GRD_SEC) GRD_SEC," & _
                " MAX(isnull(ACAD_GRADE,'')) ACAD_GRADE , MAX(isnull(FPH_COMP_ID,'')) FPH_COMP_ID, " & _
                " FPH_INOICENO, CASE WHEN ISNULL(FPH_EmailStatus, '') LIKE '%Successfully sent%' THEN ISNULL(FPH_EmailStatus, '') + ' ON ' + CAST(FPH_EMAIL_DT AS VARCHAR) ELSE FPH_EmailStatus END FPH_EmailStatus " & _
                " FROM FEES.vw_OSO_FEES_PERFORMAINVOICE_ENQ " & _
                " WHERE FPH_STU_TYPE='E' AND ISNULL(FPH_bAdvanceBooking,0) = 0 AND "
            End If

            If CTY_ID = 172 Then 'Applicable only for UAE schools--Added by Jacob on 31/AUG/2015 , suggested by Shakeel.
                If ViewState("MainMnu_code") = OASISConstants.MNU_FEE_PERFORMAINVOICE Then
                    str_Sql = str_Sql & " isnull(FPH_bTuitionINV,0) = 1 AND "
                ElseIf ViewState("MainMnu_code") = "F100136" Then
                    str_Sql = str_Sql & " isnull(FPH_bTuitionINV,0) = 0 AND "
                End If
            End If
            If radFeeType.Checked Then
                str_Sql = str_Sql & " isnull(FPH_IsServiceInv,0) = 0 AND "
            ElseIf radServiceType.Checked Then
                str_Sql = str_Sql & " isnull(FPH_IsServiceInv,0) = 1 AND "
            End If

            If ViewState("BSU_IsPERFORMAINV_POSTREQUIRE") = True Then
                If radPosted.Checked Then
                    str_Sql = str_Sql & " isnull(FPH_bPOSTED,0) = 1 AND "
                ElseIf radNotPosted.Checked Then
                    str_Sql = str_Sql & " isnull(FPH_bPOSTED,0) = 0 AND "
                End If
            End If


            str_Sql = str_Sql & str_cond & str_Filter
            str_Sql = str_Sql & " GROUP BY FPH_ID,FPH_INOICENO, DATE,FPH_EmailStatus,FPH_EMAIL_DT  "
            str_Sql = str_Sql & " ORDER BY DATE DESC,FPH_INOICENO DESC "
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            'gvJournal.DataBind()
            txtSearch = gvJournal.HeaderRow.FindControl("txtInvNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtCompany")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtStudNo")
            txtSearch.Text = lstrCondn5

            txtSearch = gvJournal.HeaderRow.FindControl("txtStudName")
            txtSearch.Text = lstrCondn6
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        SetChk(Page)
        GridBind()
        SetChk(Page)
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblInvNo As New Label
            Dim vCOMP_ID As Integer
            Dim str_ENQ As String
            lblInvNo = TryCast(e.Row.FindControl("lblInvNo"), Label)
            Dim lblCOMP_ID As Label = sender.Parent.parent.findcontrol("lblCOMP_ID")
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing AndAlso lblInvNo IsNot Nothing Then
                If lblCOMP_ID IsNot Nothing Then
                    vCOMP_ID = lblCOMP_ID.Text
                Else
                    vCOMP_ID = -1
                End If
                If radEnquiry.Checked Then
                    str_ENQ = "ENQ"
                Else
                    str_ENQ = "STUD"
                End If
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "PerformaInvoiceNew.aspx?inv_no=" & Encr_decrData.Encrypt(lblInvNo.Text) & _
                "&COMP_ID=" & Encr_decrData.Encrypt(vCOMP_ID) & _
                "&MainMnu_code=" & Request.QueryString("MainMnu_code") & _
                "&datamode=" & ViewState("datamode") & "&TYPE=" & Encr_decrData.Encrypt(str_ENQ) & "&ACDID=" & Encr_decrData.Encrypt(ddAcademic.SelectedValue)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub lblPrintReciept_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblINV_NO As Label = sender.Parent.parent.findcontrol("lblInvNo")
        Dim lblCOMP_ID As Label = sender.Parent.parent.findcontrol("lblCOMP_ID")
        If lblCOMP_ID.Text <> "-1" Then
            Session("ReportSource") = FEEPERFORMAINVOICE.PrintReceipt("'" & lblINV_NO.Text & "'", True, Session("sBsuid"), radEnquiry.Checked, Session("sUsr_name"))
        Else
            Session("ReportSource") = FEEPERFORMAINVOICE.PrintReceipt("'" & lblINV_NO.Text & "'", False, Session("sBsuid"), radEnquiry.Checked, Session("sUsr_name"))
        End If
        FEEPERFORMAINVOICE.F_SAVEFEE_PERFORMAINVOICE_H_PRINTCOUNT(Session("sBsuid"), lblINV_NO.Text, Session("sUsr_name"))
        h_print.Value = "print"
    End Sub

    Protected Function PrintReceiptX(ByVal vINV_NOs As ArrayList) As MyReportClass
        Dim ienum As IEnumerator = vINV_NOs.GetEnumerator
        Dim str_invnos As String = String.Empty
        Dim str_invnos_audit As String = String.Empty
        Dim comma As String = String.Empty
        Dim pipe As String = String.Empty
        While (ienum.MoveNext())
            str_invnos += comma & " '" & ienum.Current & "'"
            comma = ", "
            str_invnos_audit += pipe & ienum.Current
            pipe = "|"
        End While
        Dim sQL As String

        Dim IsCompany As Boolean, CompInvCount As String, CompNParentInv As String
        sQL = "SELECT isnull(COUNT(FPH_ID),0) FROM FEES.FEE_PERFORMAINVOICE_H WHERE FPH_BSU_ID ='" & Session("sBsuid") & "' AND FPH_INOICENO IN (" & Replace(str_invnos, "|", ",") & ") AND ISNULL(FPH_COMP_ID,0)>0"

        CompInvCount = Mainclass.getDataValue(sQL, "OASIS_FEESConnectionString").ToString
        If Val(CompInvCount) > 1 Then
            '  lblError.Text = "Multiple Company Invoice cannot be printed together, Please print the Company invoice individually."
            usrMessageBar.ShowNotification("Multiple Company Invoice cannot be printed together, Please print the Company invoice individually.", UserControls_usrMessageBar.WarningType.Danger)
            PrintReceiptX = Nothing
            Exit Function
        End If
        sQL = "SELECT isnull(COUNT(FPH_ID),0) FROM FEES.FEE_PERFORMAINVOICE_H WHERE FPH_BSU_ID ='" & Session("sBsuid") & "' AND FPH_INOICENO IN (" & Replace(str_invnos, "|", ",") & ") HAVING MAX(FPH_COMP_ID)<>MIN(FPH_COMP_ID) AND MIN(FPH_COMP_ID)<0"
        CompNParentInv = Mainclass.getDataValue(sQL, "OASIS_FEESConnectionString").ToString
        If Val(CompNParentInv) > 0 Then
            ' lblError.Text = "Please print the Company Invoice and Parent Invoice Seperately."
            usrMessageBar.ShowNotification("Please print the Company Invoice and Parent Invoice Seperately.", UserControls_usrMessageBar.WarningType.Danger)
            PrintReceiptX = Nothing
            Exit Function
        End If

        h_print.Value = "print"
        sQL = "SELECT isnull(MAX(CASE WHEN ISNULL(FPH_COMP_ID ,0)>0 THEN 1 ELSE 0 END),0) FROM FEES.FEE_PERFORMAINVOICE_H WHERE FPH_BSU_ID ='" & Session("sBsuid") & "' AND FPH_INOICENO IN (" & Replace(str_invnos, "|", ",") & ")"
        IsCompany = Mainclass.getDataValue(sQL, "OASIS_FEESConnectionString")
        FEEPERFORMAINVOICE.F_SAVEFEE_PERFORMAINVOICE_H_PRINTCOUNT(Session("sBsuid"), str_invnos_audit, Session("sUsr_name"))
        Return FEEPERFORMAINVOICE.PrintReceipt(str_invnos, IsCompany, Session("sBsuid"), radEnquiry.Checked, Session("sUsr_name"))
        'Return PrintReceipt(str_invnos, False, Session("sBsuid"), radEnquiry.Checked, Session("sUsr_name"))
    End Function

    Protected Sub ddAcademic_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddAcademic.SelectedIndexChanged
        GridBind()
        Session("liUserList") = New ArrayList
    End Sub

    Protected Sub radStudent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStudent.CheckedChanged
        GridBind()
        Session("liUserList") = New ArrayList
    End Sub

    Protected Sub radEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnquiry.CheckedChanged
        GridBind()
        Session("liUserList") = New ArrayList
    End Sub

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If chk.Name = "chkEMail" Then Continue For
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        SetChk(Page)
        If Session("liUserList").Count > 0 Then
            Session("ReportSource") = PrintReceiptX(Session("liUserList"))
        Else
            ' lblError.Text = "Please Select Invoice(s)!!!"
            usrMessageBar.ShowNotification("Please Select Invoice(s)!!!", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub

    Protected Sub btnPostInv_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles btnPostInv.Click
        Dim chkControl As New HtmlInputCheckBox
        For Each grow As GridViewRow In gvJournal.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl Is Nothing Then Exit Sub
            If chkControl.Checked Then
                If Not doPostInvoice(DirectCast(grow.FindControl("lblInvNo"), Label).Text) Then
                    Exit Sub
                End If
            End If
        Next
        '    lblError.Text = "Proforma Invoice Posted"
        usrMessageBar.ShowNotification("Proforma Invoice Posted", UserControls_usrMessageBar.WarningType.Success)
        GridBind()
    End Sub

    Private Function doPostInvoice(ByVal InvoiceNo As String) As Boolean
        Dim sqlParam(5) As SqlParameter
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        sqlParam(0) = Mainclass.CreateSqlParameter("@FPH_INVOICENO", InvoiceNo, SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBSUID"), SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@ACD_ID", ddAcademic.SelectedValue, SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@USER", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(4) = Mainclass.CreateSqlParameter("@ReturnMsg", "", SqlDbType.VarChar, True)
        Dim str_success As String
        str_success = Mainclass.ExecuteParamQRY(str_conn, "Fees.PostProformaInvoice", sqlParam)
        If str_success <> 0 Then
            '  lblError.Text = sqlParam(7).Value
            usrMessageBar.ShowNotification(sqlParam(4).Value, UserControls_usrMessageBar.WarningType.Danger)
            doPostInvoice = False
        Else
            doPostInvoice = True
        End If

    End Function
    Protected Sub btnEMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEMail.Click
        Dim chkControl As New HtmlInputCheckBox
        For Each grow As GridViewRow In gvJournal.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl Is Nothing Then Exit Sub
            If chkControl.Checked Then
                If Not doEMail(DirectCast(grow.FindControl("lblInvNo"), Label).Text) Then
                    Exit Sub
                End If
            End If
        Next
        '    lblError.Text = "Email Processed for Proforma Invoice"
        usrMessageBar.ShowNotification("Email Processed for Proforma Invoice", UserControls_usrMessageBar.WarningType.Danger)
        GridBind()
    End Sub
    Private Function doEMail(ByVal InvoiceNo As String) As Boolean
        Dim sqlParam(8) As SqlParameter
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        sqlParam(0) = Mainclass.CreateSqlParameter("@FPH_INVOICENO", InvoiceNo, SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBSUID"), SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@ACD_ID", ddAcademic.SelectedValue, SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@AUD_WINUSER", Page.User.Identity.Name.ToString(), SqlDbType.VarChar)
        sqlParam(4) = Mainclass.CreateSqlParameter("@Aud_form", Master.MenuName.ToString(), SqlDbType.VarChar)
        sqlParam(5) = Mainclass.CreateSqlParameter("@Aud_user", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(6) = Mainclass.CreateSqlParameter("@Aud_module", Session("sModule"), SqlDbType.VarChar)
        sqlParam(7) = Mainclass.CreateSqlParameter("@v_ReturnMsg", Session("sModule"), SqlDbType.VarChar, True)
        sqlParam(8) = Mainclass.CreateSqlParameter("@CONTACTS", rblContacts.SelectedValue, SqlDbType.VarChar)
        Dim str_success As String
        str_success = Mainclass.ExecuteParamQRY(str_conn, "EMailProformaInvoice", sqlParam)
        If str_success <> 0 Then
            '  lblError.Text = sqlParam(7).Value
            usrMessageBar.ShowNotification(sqlParam(7).Value, UserControls_usrMessageBar.WarningType.Danger)
            doEMail = False
        Else
            doEMail = True
        End If

    End Function

    Protected Sub chkEMail_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radSent.CheckedChanged, radProcessed.CheckedChanged, radAll.CheckedChanged, radNotSent.CheckedChanged, radEmailFailed.CheckedChanged
        GridBind()
        Session("liUserList") = New ArrayList
    End Sub

    Protected Sub radAllType_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAllType.CheckedChanged
        GridBind()
        Session("liUserList") = New ArrayList
    End Sub

    Protected Sub radFeeType_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radFeeType.CheckedChanged
        GridBind()
        Session("liUserList") = New ArrayList
    End Sub

    Protected Sub radAllPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAllPosted.CheckedChanged
        GridBind()

        If radNotPosted.Checked Then
            btnPostInv.Visible = True
        Else
            btnPostInv.Visible = False
        End If
    End Sub

    Protected Sub radNotPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radNotPosted.CheckedChanged
        GridBind()

        If radNotPosted.Checked Then
            btnPostInv.Visible = True
        Else
            btnPostInv.Visible = False
        End If
    End Sub

    Protected Sub radPosted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPosted.CheckedChanged
        GridBind()

        If radNotPosted.Checked Then
            btnPostInv.Visible = True
        Else
            btnPostInv.Visible = False
        End If
    End Sub


    Protected Sub radServiceType_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radServiceType.CheckedChanged
        GridBind()
        Session("liUserList") = New ArrayList
    End Sub

    Protected Sub lbtnExport_Click(sender As Object, e As EventArgs) Handles lbtnExport.Click
        Dim chkControl As New HtmlInputCheckBox
        Dim InvoiceNos As String = ""
        '  Me.lblError.Text = ""
        For Each grow As GridViewRow In gvJournal.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl Is Nothing Then Exit Sub
            If chkControl.Checked Then
                If DirectCast(grow.FindControl("lblInvNo"), Label).Text <> "" Then
                    InvoiceNos = InvoiceNos & IIf(InvoiceNos = "", "", "|") & DirectCast(grow.FindControl("lblInvNo"), Label).Text

                End If
            End If
        Next
        If InvoiceNos <> "" Then
            Dim qry As String = ""
            qry = "EXEC [FEES].[SP_OSO_FEES_PROFORMA_REPORT_VER_1]   @FPH_INVOICENO='" & InvoiceNos & "',@BSU_ID = '" & Session("sBsuid") & "', " & _
        " @bEnquiry=0 ,@ExportData=1 "
            Dim dt As DataTable = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry).Tables(0)
            If dt.Rows.Count > 0 Then
                ExportToExcel(dt, "ExportedList")
            End If
        Else
            '      Me.lblError.Text = "Please select the Invoice(s) to be exported."
            usrMessageBar.ShowNotification("Please select the Invoice(s) to be exported.", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub
    Private Sub ExportToExcel(dt As DataTable, FileName__1 As String)
        If dt.Rows.Count > 0 Then
            Dim filename__2 As String = FileName__1 & ".xls"

            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=" & filename__2 & "")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"

            Using sw As New StringWriter()
                Dim hw As New System.Web.UI.HtmlTextWriter(sw)

                Dim dgGrid As New DataGrid()
                dgGrid.AutoGenerateColumns = False
                'dgGrid.Columns.Add(createb)
                Dim INVOICENO As New BoundColumn
                INVOICENO.DataField = "INVOICENO"
                INVOICENO.HeaderText = "InvoiceNo"
                Dim StudentNo As New BoundColumn
                StudentNo.DataField = "StudentNo"
                StudentNo.HeaderText = "StudentNo"
                StudentNo.DataFormatString = "{0:F}"

                Dim Name As New BoundColumn
                Name.DataField = "Name"
                Name.HeaderText = "Name"
                Dim Parent As New BoundColumn
                Parent.DataField = "Parent"
                Parent.HeaderText = "Parent"
                Dim Grade As New BoundColumn
                Grade.DataField = "Grade"
                Grade.HeaderText = "Grade"
                Dim AMOUNT As New BoundColumn
                AMOUNT.DataField = "AMOUNT"
                AMOUNT.HeaderText = "Amount"
                AMOUNT.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
                AMOUNT.DataFormatString = "{0:N}"

                dgGrid.Columns.Add(INVOICENO)
                dgGrid.Columns.Add(StudentNo)
                dgGrid.Columns.Add(Name)
                dgGrid.Columns.Add(Parent)
                dgGrid.Columns.Add(Grade)
                dgGrid.Columns.Add(AMOUNT)


                dgGrid.DataSource = dt
                dgGrid.DataBind()
                'Get the HTML for the control.
                dgGrid.RenderControl(hw)

                'style to format numbers to string
                Me.EnableViewState = False
                Dim style As String = "<style> .textmode { } </style>"
                Response.Write(style)
                Response.Output.Write(sw.ToString())
                Response.Flush()
                Response.End()
            End Using
        End If
    End Sub
End Class
