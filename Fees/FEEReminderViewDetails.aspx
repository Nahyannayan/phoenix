<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FEEReminderViewDetails.aspx.vb" Inherits="Fees_FEEReminder" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    &nbsp;<script lang="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script><script lang="javascript" type="text/javascript">
                 Sys.Application.add_load(
         function CheckForPrint() {
             if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                 var hdnvalue = document.getElementById('<%= h_print.ClientID %>').value;
                 document.getElementById('<%= h_print.ClientID %>').value = '';
                 //showModelessDialog('../Reports/ASPX Report/RptViewerModalView.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                 var url = "";
                 if (hdnvalue == "print_auto") {
                     var stuids = document.getElementById('<%= hfStuIds.ClientID%>').value;
                     var frhid = document.getElementById('<%= hfFrhId.ClientID%>').value;
                     url = "../Fees/popFeeReminder.aspx?" + "sid=" + stuids + "&frhid=" + frhid;
                     ShowSubWindowWithClose(url, "", "60%", "80%");
                 }
                 else {
                     url = "../Reports/ASPX Report/RptViewerModalView.aspx";
                     var oWnd = radopen(url, "pop_clickforprint");
                 }
             }
         }
    );
         function ChangeAllCheckBoxStates(checkState) {
             var chk_state = document.getElementById("chkAL").checked;
             for (i = 0; i < document.forms[0].elements.length; i++) {
                 if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                     if (document.forms[0].elements[i].type == 'checkbox') {
                         document.forms[0].elements[i].checked = chk_state;
                     }
             }
         }


         function ChangeCheckBoxState(id, checkState) {
             var cb = document.getElementById(id);
             if (cb != null)
                 cb.checked = checkState;
         }

         function ChangeAllCheckBoxStates(checkState) {
             var chk_state = document.getElementById("chkAL").checked;
             for (i = 0; i < document.forms[0].elements.length; i++) {
                 if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                     if (document.forms[0].elements[i].type == 'checkbox') {
                         document.forms[0].elements[i].checked = chk_state;
                     }
             }
         }





         function autoSizeWithCalendar(oWindow) {
             var iframe = oWindow.get_contentFrame();
             var body = iframe.contentWindow.document.body;

             var height = body.scrollHeight;
             var width = body.scrollWidth;

             var iframeBounds = $telerik.getBounds(iframe);
             var heightDelta = height - iframeBounds.height;
             var widthDelta = width - iframeBounds.width;

             if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
             if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
             oWindow.center();
         }

    </script>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_clickforprint" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Fee Reminder...
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError" CssClass="error"></asp:Label>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="10%"><span class="field-label">Reminder Title</span></td>
                        <td align="left" width="25%">
                            <asp:Label ID="lblReminderTitle" runat="server" CssClass="field-value"></asp:Label></td>
                        <td alifn="left" width="10%"></td>
                        <td alifn="left" width="20%"></td>
                        <td align="left" width="10%"><span class="field-label">Date</span> </td>
                        <td align="left" width="25%">
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td align="left"><span class="field-label">Academic Year</span> </td>
                        <td align="left">
                            <asp:Label ID="lblAcademicYear" runat="server" CssClass="field-value"></asp:Label></td>
                        <td align="left"><span class="field-label">Reminder Type</span></td>
                        <td align="left">
                            <asp:Label ID="lblReminderType" runat="server" CssClass="field-value"></asp:Label></td>
                        <td align="left"><span class="field-label">As On</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtAsOnDate" runat="server" AutoPostBack="True"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td align="right" colspan="6">
                            <asp:RadioButton ID="radAll" runat="server" CssClass="field-label" GroupName="Email" Text="All"
                                AutoPostBack="True" Checked="True" />&nbsp;
                <asp:RadioButton ID="radSent" runat="server" CssClass="field-label" GroupName="Email" Text="Emailed"
                    AutoPostBack="True" />
                            <asp:RadioButton ID="radEmailProcessed" runat="server" CssClass="field-label" GroupName="Email" Text="Processed"
                                AutoPostBack="True" />
                            <asp:RadioButton ID="radNotSent" runat="server" CssClass="field-label" GroupName="Email"
                                Text="Not Emailed" AutoPostBack="True" />
                            <asp:RadioButton ID="radEmailFailed" runat="server" CssClass="field-label" GroupName="Email" Text="Failed"
                                AutoPostBack="True" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:GridView ID="gvFEEReminder" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30" SkinID="GridViewView">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>
                                            <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%--<input id="chkControl" runat="server" type="checkbox"  />   --%>
                                            <asp:CheckBox ID="chkControl" runat="server" Checked='<%# Eval("CHKD")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="FCH_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("FRD_STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student ID">
                                        <HeaderTemplate>
                                            Student ID<br />
                                            <asp:TextBox ID="txtStuno" runat="server" SkinID="Gridtxt" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchno" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Name<br />
                                            <asp:TextBox ID="txtStuname" runat="server" Width="75%" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label><br />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            Grade<br />
                                            <asp:TextBox ID="txtGrade" runat="server" SkinID="Gridtxt" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnGradeSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# bind("GRM_DISPLAY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <HeaderTemplate>
                                            Section<br />
                                            <asp:TextBox ID="txtSection" runat="server" SkinID="Gridtxt" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnSectionSearchd" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# bind("SCT_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Due Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("DUE_AMT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FRD_EmailStatus" HeaderText="Error Description"
                                        Visible="False" />
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkViewSummary" runat="server" OnClick="lnkViewSummary_Click">Summary</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:Button ID="btnSelectAll" runat="server" CssClass="button" Text="Select All" />
                            &nbsp;<asp:Button ID="btnDeselectAll" runat="server" CssClass="button" Text="Clear All" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:GridView ID="gvSTU_RemDetails" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" SkinID="GridViewView" Width="100%">
                                <Columns>
                                    <asp:BoundField HeaderText="Acad. Year" DataField="ACY_DESCR" />
                                    <asp:BoundField HeaderText="Fee Descr." DataField="FEE_DESCR" />
                                    <asp:BoundField HeaderText="Amount" DataField="FRD_AMOUNT" />
                                    <asp:BoundField HeaderText="Excluded" DataField="FRD_bExclude" />
                                    <asp:BoundField DataField="FRD_REMARKS" HeaderText="Remarks" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Add" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                            <asp:Button ID="btnEMail" runat="server" CssClass="button" Text="Send E-Mail" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                        <%--<asp:LinkButton ID="btnHidden" runat="server"></asp:LinkButton>
                        <ajaxToolkit:ModalPopupExtender ID="MPE1" BackgroundCssClass="ModalPopupBG" TargetControlID="btnHidden"
                            PopupControlID="pnlPreview" OkControlID="btnClose" CancelControlID="btnCancelpop" RepositionMode="RepositionOnWindowResize"
                            DropShadow="true" PopupDragHandleControlID="gridheader" Drag="true" runat="server">
                        </ajaxToolkit:ModalPopupExtender>--%>
                    </tr>
                </table>


                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <asp:HiddenField ID="h_print" runat="server" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_Selected_Value" runat="server" type="hidden" value="=" />
                <asp:HiddenField ID="h_IsArabic" runat="server" />
                <input id="hfStuIds" runat="server" type="hidden" value="" />
                <input id="hfFrhId" runat="server" type="hidden" value="" />
            </div>
        </div>
    </div>
    <script type="text/javascript" lang="javascript">
        function ShowSubWindowWithClose(idmId, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: idmId,
                maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = fancyTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                }
            });

            return false;
        }
        function fancyTitle(title) {
            if (title != '') {
                //var counterText = currentOpts.custom_counterText;
                var container = '<div id="fancybox-custom-title-container" style="text-align:left !important;" class="darkPanelFooter"><span id="fancybox-custom-title" CssClass="TitlePl">' + title + '</span></div>';
                //var $title = $('<span id="fancybox-custom-title" CssClass="TitlePl"></span>');
                //$title.text(title);
                //$container.append($title);
                return container;
            }
        }
    </script>
</asp:Content>
