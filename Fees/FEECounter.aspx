<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FEECounter.aspx.vb" Inherits="Fees_FEECounter" title="Untitled Page" %>
<%@ MasterType  virtualPath="~/mainMasterPage.master"%>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">
       function GetBSUName()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            <%--result = window.showModalDialog("../Accounts/selBussinessUnit.aspx?multiSelect=false","", sFeatures)
            if(result != "" && result != "undefined")
            {
               NameandCode = result.split('___');
               document.getElementById('<%=txtBSUID.ClientID %>').value=NameandCode[0]; 
               document.getElementById('<%=txtBSUDESCR.ClientID %>').value=NameandCode[1]; 
               document.getElementById('<%=h_UserId.ClientID %>').value = ""; 
               document.getElementById('<%=txtUser.ClientID %>').value = ""; 
            }
            return false;
        }--%>
           var url = "../Accounts/selBussinessUnit.aspx?multiSelect=false";
            var oWnd = radopen(url, "pop_bsuname");
                        
        } 

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=txtBSUID.ClientID %>').value=NameandCode[0]; 
               document.getElementById('<%=txtBSUDESCR.ClientID %>').value=NameandCode[1]; 
               document.getElementById('<%=h_UserId.ClientID %>').value = ""; 
               document.getElementById('<%=txtUser.ClientID %>').value = "";
                __doPostBack('<%=txtBSUDESCR.ClientID%>', 'TextChanged');
            }
        }


       function GetUser()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var vBSU_ID = document.getElementById('<%=txtBSUID.ClientID %>').value;
            <%--result = window.showModalDialog("../Common/PopupForm.aspx?multiSelect=false&ID=USER&BSU_ID=" + vBSU_ID,"", sFeatures)
            if(result != "" && result != "undefined")
            {
               NameandCode = result.split('___');
               document.getElementById('<%=h_UserId.ClientID %>').value=NameandCode[0]; 
               document.getElementById('<%=txtUser.ClientID %>').value=NameandCode[1]; 
            }
            return false;
        }--%>
        
           var url = "../Common/PopupForm.aspx?multiSelect=false&ID=USER&BSU_ID=" + vBSU_ID;
            var oWnd = radopen(url, "pop_getuser");
                        
        } 

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.NameandCode.split('||');
               document.getElementById('<%=h_UserId.ClientID %>').value=NameandCode[0]; 
               document.getElementById('<%=txtUser.ClientID%>').value=NameandCode[1]; 
                __doPostBack('<%=txtUser.ClientID%>', 'TextChanged');
            }
        }
       

function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>


    <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getuser" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_bsuname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Fee Counter Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
    <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="FEE_COUNTER" />
    <br />
    <table align="center" width="100%">
        
        <tr>
            <td align="left" width="15%">
                <span class="field-label">Counter Description</span></td>
            
            <td align="left" width="30%">
                <asp:TextBox ID="txtCounterDescr" runat="server" CssClass="inputbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCounterDescr"
                    ErrorMessage="Counter Description Required" ValidationGroup="FEE_COUNTER">*</asp:RequiredFieldValidator></td>
        
            <td align="left" width="15%">
                <span class="field-label">Business Unit</span></td>
            
            <td align="left"  width="40%">
                <table width="100%">
                    <tr>
                        <td align="left" width="40%">
                            <asp:TextBox ID="txtBSUID" runat="server" CssClass="inputbox" AutoPostBack="True" OnTextChanged="txtBSUID_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgBSUNIT" runat="server" ImageUrl="../Images/forum_search.gif"
                    OnClientClick="GetBSUName(); return false;" TabIndex="12" />
                        </td>
                        <td align="left" width="60%">
                             <asp:TextBox ID="txtBSUDESCR" runat="server" CssClass="inputbox" TabIndex="-1"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBSUDESCR"
                    ErrorMessage="Business Unit Required" ValidationGroup="FEE_COUNTER">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                
               
               </td>
        </tr>
        <tr>
            <td align="left">
                <span class="field-label">User</span></td>
           
            <td align="left">
                <asp:TextBox ID="txtUser" runat="server" CssClass="inputbox"></asp:TextBox>&nbsp;<asp:ImageButton
                    ID="imgUser" runat="server" ImageUrl="../Images/forum_search.gif" OnClientClick="GetUser(); return false;"
                    TabIndex="12" /></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    OnClick="btnAdd_Click" Text="Add" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    OnClick="btnEdit_Click" Text="Edit" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" OnClick="btnSave_Click"
                    Text="Save" ValidationGroup="FEE_COUNTER" />
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    OnClick="btnCancel_Click" Text="Cancel" /></td>
        </tr>
    </table>
    <asp:HiddenField ID="h_FEE_COUNTER_ID" runat="server" />
    <asp:HiddenField ID="h_UserId" runat="server" />

            </div>
        </div>
</div>
</asp:Content>

