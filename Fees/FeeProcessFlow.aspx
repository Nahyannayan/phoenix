<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeProcessFlow.aspx.vb" Inherits="Fees_FeeProcessFlow" Title="Untitled Page" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript">
        function GetProcesses() {

            var NameandCode;
            var result;
            var bsu_id = document.getElementById('<%=h_BSUID.ClientID %>').value;
        var ACD_ID = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
        var url = "../Common/popupForm.aspx?BSU_ID=" + bsu_id + "&ID=PROCESS&multiSelect=false&ACD_ID=" + ACD_ID;
        var oWnd = radopen(url, "pop_getprocess");
        <%-- result = window.showModalDialog(url,"", sFeatures)
        if(result != '' && result != undefined)
        {
            NameandCode = result.split('___');
            document.getElementById('<%=h_PROCESSID.ClientID %>').value=NameandCode[0]; 
            document.getElementById('<%=txtProcessName.ClientID %>').value=NameandCode[1]; 
            return false;
       }
        else
        {
            return false;
        }
      }--%>
    }


        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_PROCESSID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtProcessName.ClientID %>').value = NameandCode[1];
                __doPostBack('<%=txtProcessName.ClientID%>', 'TextChanged');
            }
        }



        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">

        <Windows>
            <telerik:RadWindow ID="pop_getprocess" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Link Fees To Stages
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td valign="top" align="left">
                            <%--<asp:Label id="lblError" runat="server" EnableViewState="False"></asp:Label>--%>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                        <a href="#">
                                            <asp:ImageButton ID="btnInfo" runat="server" ImageAlign="Top" ImageUrl="~/Images/Help/Help.png"
                                                OnClientClick="return false;" Width="30px" />
                                        </a></td>

                                    <td align="left" width="20%">
                                        <span class="field-label">Process</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtProcessName" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgProcess" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="GetProcesses(); return false;"></asp:ImageButton></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Fee Type</span></td>

                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlFeeType" runat="server">
                                        </asp:DropDownList>&nbsp;<asp:CheckBox ID="chkmandatory" runat="server" CssClass="field-label" Text="Mandatory"></asp:CheckBox>
                                        <asp:LinkButton ID="lnkbtnAdd" runat="server">Add</asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td align="left" class="title-bg" colspan="4">Fees</td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4" valign="top">
                                        <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" Width="100%" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField Visible="False" HeaderText="FeeId">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fee Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFeeType" runat="server" Text='<%# bind("FEE_TYPE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Mandatory">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblbRequired" runat="server" Text='<%# bind("bRequired") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" OnClick="btnAdd_Click" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" OnClick="btnEdit_Click" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Delete" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" OnClick="btnCancel_Click" /></td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top">

                            <div id="info" align="left" style="display: none; width: 250px; z-index: 2; opacity: 0; filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0); font-size: 12px; border: solid 1px #CCCCCC; background-color: #FFFFFF; padding: 5px;">
                                <div id="btnCloseParent" style="float: right; opacity: 0; filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);">
                                    <asp:ImageButton ID="btnClose" runat="server" OnClientClick="return false;" ImageUrl="~/Images/close.png" ToolTip="Close"></asp:ImageButton>
                                </div>
                                <div>
                                    <p>
                                        <asp:TreeView ID="trViewFeeCollections" runat="server" EnableClientScript="False"
                                            ShowExpandCollapse="False">
                                        </asp:TreeView>
                                        &nbsp;
                                    </p>
                                </div>
                            </div>
                            <!-- "Wire frame" div used to transition from the button to the info panel -->
                            <div id="flyout" style="display: none; overflow: hidden; z-index: 2; background-color: #FFFFFF; border: solid 1px #D0D0D0;"></div>

                        </td>
                    </tr>
                </table>


                <asp:HiddenField ID="h_PROCESSID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_BSUID" runat="server"></asp:HiddenField>
                <ajaxToolkit:AnimationExtender ID="OpenAnimation" runat="server" TargetControlID="btnInfo">
                    <Animations>
                <OnClick>
                    <Sequence>
                        <%-- Disable the button so it can't be clicked again --%>
                        <EnableAction Enabled="false" />
                        
                        <%-- Position the wire frame on top of the button and show it --%>
                        <ScriptAction Script="Cover($get('ctl00_SampleContent_btnInfo'), $get('flyout'));" />
                        <StyleAction AnimationTarget="flyout" Attribute="display" Value="block"/>
                        
                        <%-- Move the wire frame from the button's bounds to the info panel's bounds --%>
                        <Parallel AnimationTarget="flyout" Duration=".3" Fps="25">
                            <Move Horizontal="150" Vertical="-50" />
                            <Resize Width="260" Height="280" />
                            <Color PropertyKey="backgroundColor" StartValue="#AAAAAA" EndValue="#FFFFFF" />
                        </Parallel>
                        
                        <%-- Move the info panel on top of the wire frame, fade it in, and hide the frame --%>
                        <ScriptAction Script="Cover($get('flyout'), $get('info'), true);" />
                        <StyleAction AnimationTarget="info" Attribute="display" Value="block"/>
                        <FadeIn AnimationTarget="info" Duration=".2"/>
                        <StyleAction AnimationTarget="flyout" Attribute="display" Value="none"/>
                        
                        <%-- Flash the text/border red and fade in the "close" button --%>
                        <Parallel AnimationTarget="info" Duration=".5">
                            <Color PropertyKey="color" StartValue="#666666" EndValue="#FF0000" />
                            <Color PropertyKey="borderColor" StartValue="#666666" EndValue="#FF0000" />
                        </Parallel>
                        <Parallel AnimationTarget="info" Duration=".5">
                            <Color PropertyKey="color" StartValue="#FF0000" EndValue="#666666" />
                            <Color PropertyKey="borderColor" StartValue="#FF0000" EndValue="#666666" />
                            <FadeIn AnimationTarget="btnCloseParent" MaximumOpacity=".9" />
                        </Parallel>
                    </Sequence>
                </OnClick>
                    </Animations>
                </ajaxToolkit:AnimationExtender>
                <ajaxToolkit:AnimationExtender ID="CloseAnimation" runat="server" TargetControlID="btnClose">
                    <Animations>
                <OnClick>
                    <Sequence AnimationTarget="info">
                        <%--  Shrink the info panel out of view --%>
                        <StyleAction Attribute="overflow" Value="hidden"/>
                        <Parallel Duration=".3" Fps="15">
                            <Scale ScaleFactor="0.05" Center="true" ScaleFont="true" FontUnit="px" />
                            <FadeOut />
                        </Parallel>
                        
                        <%--  Reset the sample so it can be played again --%>
                        <StyleAction Attribute="display" Value="none"/>
                        <StyleAction Attribute="width" Value="250px"/>
                        <StyleAction Attribute="height" Value=""/>
                        <StyleAction Attribute="fontSize" Value="12px"/>
                        <OpacityAction AnimationTarget="btnCloseParent" Opacity="0" />
                        
                        <%--  Enable the button so it can be played again --%>
                        <EnableAction AnimationTarget="btnInfo" Enabled="true" />
                    </Sequence>
                </OnClick>
                <OnMouseOver>
                    <Color Duration=".2" PropertyKey="color" StartValue="#FFFFFF" EndValue="#FF0000" />
                </OnMouseOver>
                <OnMouseOut>
                    <Color Duration=".2" PropertyKey="color" StartValue="#FF0000" EndValue="#FFFFFF" />
                </OnMouseOut>
                    </Animations>
                </ajaxToolkit:AnimationExtender>

            </div>
        </div>
    </div>
</asp:Content>

