<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeGenCCAll.aspx.vb" Inherits="Fees_FeeGenCCAll" title="Untitled Page" %>
<%@ MasterType  virtualPath="~/mainMasterPage.master"%>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function CheckAmount(e)
    {var amt;
     amt=parseFloat(e.value);
     if (isNaN(amt))
           amt= 0;
          e.value= amt.toFixed(2);
          UpdateSum();
     return true;
    } 
 function UpdateSum()
 {    
   var txtBankCom, txtNetAmount, txtdepAmount;   
        txtBankCom=parseFloat(document.getElementById('<%=txtBankCom.ClientID %>').value);
        if (isNaN(txtBankCom) )
            txtBankCom=0;
        txtdepAmount=parseFloat(document.getElementById('<%=txtdepAmount.ClientID %>').value);
        if (isNaN(txtdepAmount) )
            txtdepAmount=0;
        txtNetAmount=txtdepAmount-txtBankCom;             
        document.getElementById('<%=txtNetAmount.ClientID %>').value=txtNetAmount.toFixed(2);   
  }     
  
  function ChangeAllCheckBoxStates(checkState)
    {    
    document.getElementById('<%=txtdepAmount.ClientID %>').value = 0;
    document.getElementById('<%=txtBankCom.ClientID %>').value = 0;
    document.getElementById('<%=txtNetAmount.ClientID %>').value = 0;       
        var chk_state=document.getElementById("chkAL").checked;
       for(i=0; i<document.forms[0].elements.length; i++)
        {
           if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
            if (document.forms[0].elements[i].type=='checkbox')
            {
                document.forms[0].elements[i].checked=chk_state;
                getAmount(document.forms[0].elements[i]);             
            }
        }
    }   
    
    function getAmount(Obj)
    {
    var Amnt = document.getElementById('<%=txtdepAmount.ClientID %>').value;
    var ExBankCharges = document.getElementById('<%=txtBankCom.ClientID %>').value; 
    var NameandCode;
    
    var result = Obj.value
    //alert(result);
    NameandCode = result.split('@');
    var SelAmnt = NameandCode[0]
    var crRate = NameandCode[1]
    //alert(SelAmnt +'............' + crRate);
     if (isNaN(SelAmnt))
           SelAmnt= 0;
       if (isNaN(crRate))
       crRate= 0;
           
    var BankCharges = (Number(SelAmnt) * Number(crRate)) / 100 ;
    var TotalAmnt = Number(Amnt) + Number(SelAmnt);
    var TotBankCharges = Number(ExBankCharges) + Number(BankCharges);
    
    if (Obj.checked == false)
    {
    TotalAmnt = Number(Amnt) - Number(SelAmnt);
    TotBankCharges = Number(ExBankCharges) - Number(BankCharges);
    }
    if (Number(TotalAmnt) < 0)
    TotalAmnt = 0;
     if (Number(TotBankCharges) < 1)
    TotBankCharges = 0;
    var NetAmount = Number(TotalAmnt) - Number(TotBankCharges)
    if (Number(NetAmount) < 0)
    NetAmount = 0;
    
    document.getElementById('<%=txtdepAmount.ClientID %>').value = TotalAmnt.toFixed(2); 
    document.getElementById('<%=txtBankCom.ClientID %>').value = TotBankCharges.toFixed(2);
    document.getElementById('<%=txtNetAmount.ClientID %>').value = NetAmount.toFixed(2);

}
    function getBank() {
        var sFeatures;
        sFeatures = "dialogWidth: 820px; ";
        sFeatures += "dialogHeight: 450px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        <%-- result = window.showModalDialog("../Accounts/accjvshowaccount.aspx?bankcash=b", "", sFeatures)
    if (result == '' || result == undefined)
    { return false; }
    lstrVal = result.split('___');
    document.getElementById('<%=txtBankCode.ClientId %>').value = lstrVal[0];
    document.getElementById('<%=txtBankDescr.ClientId %>').value = lstrVal[1];
}     --%>

        var url = "../Accounts/accjvshowaccount.aspx?bankcash=b";
        var oWnd = radopen(url, "pop_getbank");
    }

    function OnClientClose1(oWnd, args) {
        //get the transferred arguments

        var arg = args.get_argument();

        if (arg) {

            NameandCode = arg.NameandCode.split('||');
            document.getElementById('<%=txtBankCode.ClientId %>').value = NameandCode[0];
                document.getElementById('<%=txtBankDescr.ClientId %>').value = NameandCode[1];
                __doPostBack('<%=txtBankDescr.ClientID%>', 'TextChanged');
            }
    }

     Sys.Application.add_load( 
                function CheckForPrint()  
                     {
                       if (document.getElementById('<%= h_print.ClientID %>' ).value!='')
                       { 
                       document.getElementById('<%= h_print.ClientID %>' ).value=''; 
                      showModelessDialog ('../Reports/ASPX Report/RptViewerModal.aspx', '',"dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;"); 
                       }
                     }
                    );
                    
                    


    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }

    </script>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getbank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i> Credit Card Clearance
        </div>
        <div class="card-body">
            <div class="table-responsive">

        <table width="100%" align="center">    
     
      <tr><td width="100%" >      
        <table align="center" width="100%">               
            <tr valign="top">
                <td align="left" colspan="6" width="100%">
                <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error" ></asp:Label>
                    <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                </td>
            </tr>
        </table>
       <table width="100%" align="center" >
       
            <tr>
                <td width="20%" align="left">
                   <span class="field-label">Doc No</span></td>
                <td width="30%" align="left">
                    <asp:TextBox ID="txtdocNo" runat="server"></asp:TextBox></td>
                <td width="20%" align="left">
                   <span class="field-label">Doc Date</span></td>
               
                 <td width="30%" align="left">
                    <asp:TextBox ID="txtdocDate" runat="server" AutoPostBack="True"></asp:TextBox>
                     <asp:ImageButton ID="imgDocDate" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
            </tr>
          <tr>
                <td align="left">
                   <span class="field-label">Bank Account</span></td>
                <td colspan="3" align="left">
                    <table width="100%">
                        <tr>
                            <td align="left" width="40%" class="p-0">
                                <asp:TextBox ID="txtBankCode" runat="server"></asp:TextBox>
                    <a href="#" onclick="getBank();return false;">
                          <img border="0" src="../Images/cal.gif" id="IMG3" /></a>
                            </td>
                            <td align="left" width="60%" class="p-0">
                                <asp:TextBox ID="txtBankDescr" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                        
                                       
                </td>               
            </tr>
                   
           <tr>
                <td align="left">
                    <span class="field-label">Cash Flow</span></td>
                <td align="left">
                   <asp:TextBox ID="txtCashFlow" runat="server">
                   </asp:TextBox>
                 <a href="#" onclick="popUp('460','400','CASHFLOW_BR','<%=txtCashFlow.ClientId %>')">
                          <img border="0" src="../Images/cal.gif" id="IMG1"  /></a> 
                              </td>
           
               <td align="left">
                   <span class="field-label">Narration</span></td>
               <td align="left">
                   <asp:TextBox ID="txtNarration" runat="server" CssClass="inputbox_multi" SkinID="MultiText"
                       TabIndex="16" TextMode="MultiLine">CREDIT CARD COLLECTION DEPOSIT</asp:TextBox></td>
           </tr>
            <tr>
                <td align="left">
                  <span class="field-label">Deposit Amount</span></td>
                <td align="left">
                   <asp:TextBox ID="txtdepAmount" runat="server" ></asp:TextBox>
                    <img src="../Images/cal.gif" id="IMG8" language="javascript"  /></td>
           
                <td align="left">
                   <span class="field-label">Bank Charges</span></td>
                <td align="left">
                    <asp:TextBox ID="txtBankCom" runat="server" onBlur="CheckAmount(this);"></asp:TextBox></td>
            </tr>   
           <tr>
               <td align="left">
                   <span class="field-label">Net</span></td>
               <td align="left">
                   <asp:TextBox ID="txtNetAmount" runat="server"></asp:TextBox></td>
               <td></td>
               <td></td>
           </tr>
           <tr>
               <td align="left" colspan="4">
                    <asp:GridView ID="gvDTL" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" EmptyDataText="No transaction details added yet." Width="100%" SkinID="GridViewView" OnRowDataBound="gvDTL_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="CRR_ID" HeaderText="CrrId" ReadOnly="True" ></asp:BoundField>
                            <asp:BoundField DataField="DocDate" HeaderText="Doc Date" ReadOnly="True"></asp:BoundField>
                            <asp:BoundField DataField="VHH_DOCNO" HeaderText="Doc No" ReadOnly="True"></asp:BoundField>
                            <asp:BoundField DataField="CRR_RATE" HeaderText="Commission" ReadOnly="True"></asp:BoundField>
                            <asp:BoundField DataField="CRR_CRI_ID" HeaderText="Card Type" ReadOnly="True"></asp:BoundField>
                            <asp:BoundField DataField="tranAmount" HeaderText="Transaction Amount" ReadOnly="True">
                            <ItemStyle Width="100px"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="VHH_NARRATION" HeaderText="Narration" ReadOnly="True"></asp:BoundField>
                            <asp:TemplateField HeaderText="Select"><HeaderTemplate>
                             <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All" />
                            </HeaderTemplate>
                            <ItemTemplate>
                             <input id="chkVCD_ID" runat="server"  style ="cursor:hand" type="checkbox" value ='<%# Bind("RATEAMNT") %>' onclick ="getAmount(this)" />
                            </ItemTemplate>
                            <ItemStyle></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
           </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:CheckBox id="ChkPrint" runat="server" CssClass="field-label" Text="Print Voucher">
                    </asp:CheckBox>
              <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Post" />
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                        TabIndex="30" Text="Cancel" /></td></tr>              
          </table>  
          </td>
          </tr>
          </table> 

                </div>
            </div>
        </div>

    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgDocDate" TargetControlID="txtdocDate">
    </ajaxToolkit:CalendarExtender>
    <asp:HiddenField ID="hColln" runat="server" /> 
    <asp:HiddenField ID="h_print" runat="server" />
    <asp:HiddenField ID="hCardAccount" runat="server" />
</asp:Content>
