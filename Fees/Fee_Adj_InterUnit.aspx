<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="Fee_Adj_InterUnit.aspx.vb" Inherits="Fees_Fee_Adj_InterUnit" Theme="General" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../Scripts/PopupJQuery.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />
    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
             function CheckForPrint() {

                 if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                     document.getElementById('<%= h_print.ClientID %>').value = '';
                     //alert(1);
                     //showModalDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                     return ShowWindowWithClose('../Reports/ASPX Report/RptViewerModal.aspx', 'search', '55%', '85%')
                     return false;
                 }
             }
    );

             function FillDetailRemarks() {
                 var HeaderRemarks;
                 HeaderRemarks = document.getElementById('<%=txtHeaderRemarks.ClientID %>').value;
                 if (HeaderRemarks != '')
                     document.getElementById('<%=txtDetRemarks.ClientID %>').value = HeaderRemarks;
                 return false;
             }
             function ChangeTextEnteringCaption(checkedItem) {
                 var lblRadText;
                 var lbl = checkedItem.parentElement.getElementsByTagName('label');
                 lblRadText = lbl[0].innerHTML;
                 if (lblRadText == 'Amount')
                     document.getElementById('<%=txtDetAmount.ClientID %>').value = document.getElementById('<%=h_Amount.ClientID %>').value;
                 else
                     document.getElementById('<%=txtDetAmount.ClientID %>').value = document.getElementById('<%=h_WEEKS_MONTHS_COUNT.ClientID %>').value;
                 return false;
             }

             function getStudent() {

                 var sFeatures;
                 var lstrVal;
                 var lintScrVal;
                 var pMode;
                 var sFeatures;
                 sFeatures = "dialogWidth: 875px; ";
                 sFeatures += "dialogHeight: 600px; ";
                 sFeatures += "help: no; ";
                 sFeatures += "resizable: no; ";
                 sFeatures += "scroll: yes; ";
                 sFeatures += "status: no; ";
                 sFeatures += "unadorned: no; ";


                 var ProviderBSU = document.getElementById('<%= h_BSUID.ClientID %>').value;
                 var StuBsuId = document.getElementById('<%= ddlStudentBSU.ClientID %>').value;

                 if (document.getElementById('<%= radStud.ClientID %>').checked == true)
                     Stu_type = "S";
                 else
                     Stu_type = "E";

                 pMode = "ALL_SOURCE_STUDENTS"
                 //url = "../Fees/ShowStudent.aspx?type=" + pMode + "&ProviderBSUID=" + ProviderBSU + "&Stu_Bsu_Id=" + StuBsuId + "&STU_TYPE=" + Stu_type;
                 url = "ShowStudent.aspx?type=" + pMode + "&ProviderBSUID=" + ProviderBSU + "&Stu_Bsu_Id=" + StuBsuId + "&STU_TYPE=" + Stu_type;
                 //return ShowWindowWithClose(url, 'search', '55%', '85%')
                 //return false;
                 var oWnd = radopen(url, "pop_fee");
             <%--result = window.showModalDialog(url, "", sFeatures);
           if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('||');
            document.getElementById('<%=txtStudentname.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0]; 
            return true;--%>
             }
        function SetAllStudentValue(result) {
            var NameandCode = result.split('||');
            document.getElementById('<%=txtStdNo.ClientID%>').value = NameandCode[2];
            document.getElementById('<%=txtStudentname.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
            CloseFrame();
            return false;
        }
        function CloseFrame() {
            jQuery.fancybox.close();
        }


        function showDocument(contenttype, filename) {
            var sFeatures;

            sFeatures = "dialogWidth: 1200px; ";
            sFeatures += "dialogHeight: 500px; ";
            sFeatures += "help: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "resizable:yes; ";
            //alert("../Common/MultiDocViewer.aspx?id=0&path=Fees&filename=" + filename + "&contenttype=" + contenttype);
            //result = window.showModalDialog("../Common/MultiDocViewer.aspx?id=0&path=Fees&filename=" + filename + "&contenttype=" + contenttype, "", sFeatures)
            return ShowWindowWithClose("../Common/MultiDocViewer.aspx?id=0&path=Fees&filename=" + filename + "&contenttype=" + contenttype, 'search', '55%', '85%')
            return false;
        }
        function ConfirmRecall() {
            return confirm("Are you sure you want to ReCall the adjustment?");
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(arg.NameandCode);
                document.getElementById('<%=txtStdNo.ClientID%>').value = NameandCode[2];
                document.getElementById('<%=txtStudentname.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_STUD_ID.ClientID %>').value = NameandCode[0];
               __doPostBack('<%= txtStudentname.ClientID%>', 'TextChanged');
            }
        }
    </script>
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_fee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fee Adjustments Request..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="SUBERROR" CssClass="error" />
                            <asp:HiddenField ID="hf_FAI_ID" runat="server" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="MAINERROR" CssClass="error" />
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" SkinID="Error" />--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlCover" runat="server">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <%--  <tr class="subheader_img">
                <td align="left" colspan="4" style="height: 19px">
                    <asp:Label ID="lblHeader" runat="server" Text="Fee Adjustments Request..."></asp:Label>
                </td>
            </tr>--%>
                        <tr id="trStudentBSU" runat="server">
                            <td align="left" width="20%"><span class="field-label">Student Business Unit</span>
                            </td>
                            <td align="left" colspan="3">
                                <asp:DropDownList ID="ddlStudentBSU" runat="server" AutoPostBack="True"
                                    TabIndex="5">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Academic Year</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Date</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                &nbsp;<asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                    TabIndex="4" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtDate"
                                    ErrorMessage="Date  required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDate"
                                    ErrorMessage="Date Required" ValidationGroup="SUBERROR">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Student Type</span>
                            </td>
                            <td align="left" colspan="3">
                                <asp:RadioButton ID="radStud" AutoPostBack="true" runat="server" GroupName="ENQ_STUD"
                                    Text="Student" Checked="True" />
                                <asp:RadioButton ID="radEnq" AutoPostBack="true" runat="server" GroupName="ENQ_STUD"
                                    Text="Enquiry" />
                                &nbsp;<asp:LinkButton ID="lnkRefundable" OnClientClick="return false;" runat="server">View Refundable Fees</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Student</span>
                            </td>
                            <%-- <td align="left" colspan="3">
                                <table width="100%">
                                    <tr>--%>

                            <td align="left" width="30%">
                                <asp:TextBox ID="txtStdNo" runat="server" AutoPostBack="True"></asp:TextBox>
                                <asp:ImageButton ID="imgStudent" runat="server" OnClientClick="getStudent(); return false;"
                                    ImageUrl="~/Images/cal.gif" /></td>
                            <td align="left" colspan="2">
                                <asp:TextBox ID="txtStudentname" runat="server"></asp:TextBox>

                            </td>
                            <%-- </tr>
                                </table>
                            </td>--%>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Other Unit</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlOtherBSU" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td align="left" width="20%"></td>
                            <td align="left" width="30%">
                                <asp:CheckBox ID="chkIsNonSchool" runat="server" AutoPostBack="True" Text="Service Provider Business Unit" CssClass="field-label" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Other Unit Student Details</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtOTU_STU_Detail" runat="server" TextMode="MultiLine" MaxLength="200"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Other Unit Student details required"
                                    ValidationGroup="MAINERROR" ControlToValidate="txtOTU_STU_Detail">*</asp:RequiredFieldValidator>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Remarks</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtHeaderRemarks" runat="server" TextMode="MultiLine" MaxLength="200"></asp:TextBox>
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Header remarks required"
                    ValidationGroup="MAINERROR" ControlToValidate="txtHeaderRemarks">*</asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="title-bg-lite" colspan="4">Fee Adjustments Details...
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Fee Type</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlFeeType" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="left" width="20%">
                                <asp:Label ID="lblBalance" runat="server" Text="Balance Amount" CssClass="field-label"></asp:Label>
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblBalanceAmt" runat="server" CssClass="field-value">0.00</asp:Label>
                            </td>
                        </tr>
                        <tr id="tr_ToStudent0" runat="server">
                            <td align="left" width="20%"><span class="field-label">Amount</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtDetAmount" runat="server" Style="text-align: right;" AutoCompleteType="Disabled">0.00</asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Amount required"
                                    ValidationGroup="SUBERROR" ControlToValidate="txtDetAmount">*</asp:RequiredFieldValidator>&nbsp;<asp:Label
                                        ID="lbDescription" runat="server"></asp:Label>
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server" TargetControlID="txtDetAmount"
                                    FilterType="Custom,Numbers" ValidChars="." />
                            </td>
                            <td align="left" width="20%"><span class="field-label">Remarks</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtDetRemarks" runat="server" TextMode="MultiLine" MaxLength="200"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Detailed Remarks required"
                                    ValidationGroup="SUBERROR" ControlToValidate="txtDetRemarks">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr id="tr_ToStudent1" runat="server" visible="false">
                            <td align="left" colspan="4">
                                <asp:RadioButton ID="radStudCR" runat="server" AutoPostBack="true" Checked="True"
                                    GroupName="CR_STUD_TYPE" Text="Student"></asp:RadioButton><asp:RadioButton ID="radEnqCR"
                                        runat="server" AutoPostBack="true" GroupName="CR_STUD_TYPE" Text="Enquiry"></asp:RadioButton>
                            </td>
                        </tr>

                        <tr>
                            <td align="left" width="20%"><span class="field-label">Supporting Docs</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:FileUpload ID="UploadDocPhoto" runat="server" ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                            </td>
                            <td align="left" width="20%">
                                <asp:CheckBox ID="chkShared" runat="server" Text="Share with other unit" CssClass="field-label" /></td>
                            <td align="left" width="30%">
                                <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" />
                                <asp:RegularExpressionValidator ID="uplValidator" runat="server" ControlToValidate="UploadDocPhoto"
                                    ErrorMessage="Only Excel, Word, Pdf and Image formats are allowed" ValidationExpression="(.+\.([Xx][Ll][Ss])|.+\.([Xx][Ll][Ss][Xx])|
                    .+\.([Dd][Oo][Cc])|.+\.([Dd][Oo][Cc][Xx])|.+\.([Pp][Dd][Ff])|.+\.([Pp][Nn][Gg])|.+\.([Jj][Pp][Gg])|.+\.([Jj][Pp][Ee][Gg])|.+\.([Gg][Ii][Ff]))"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"></td>
                            <td align="left" colspan="3">
                                <asp:GridView ID="gvSuppDocs" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="FileName">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkFileName" Text='<%# Bind("FAS_ORG_FILENAME") %>' runat="server"></asp:LinkButton>
                                                <asp:HiddenField ID="hf_ContentType" Value='<%# Bind("FAS_CONTENT_TYPE") %>' runat="server" />
                                                <asp:HiddenField ID="hf_filename" Value='<%# Bind("FAS_FILENAME") %>' runat="server" />
                                                <asp:HiddenField ID="hf_FASID" Value='<%# Bind("FAS_ID") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="FAS_CONTENT_TYPE" HeaderText="FileType" />
                                        <asp:BoundField DataField="FAS_USER" HeaderText="Uploaded By" />
                                        <asp:TemplateField HeaderText="Shared">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSharedOB" Checked='<%# Bind("FAS_bShared") %>' AutoPostBack="true" runat="server"
                                                    OnCheckedChanged="chkSharedOB_CheckedChanged" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDeleteSD" OnClick="lnkDeleteSD_Click" Text='Remove' runat="server"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr id="tr_Addbutton" runat="server">
                            <td align="left" colspan="4" style="text-align: right;">
                                <asp:Label ID="lblErrorFooter" runat="server" CssClass="error" EnableViewState="False" />

                                <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="SUBERROR" />
                                <asp:Button ID="btnSubCancel" runat="server" CssClass="button" Text="Cancel" />
                            </td>
                        </tr>
                        <tr id="tr_Deatails" runat="server">
                            <td align="center" colspan="4">
                                <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                    EmptyDataText="No Data Found" EnableModelValidation="True" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:TemplateField HeaderText="FeeId" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fee Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFeeType" runat="server" Text='<%# bind("FEE_TYPE") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Duration" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDuration" runat="server" Text='<%# bind("DURATION") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("FEE_AMOUNT", "{0:###,###.00}")%>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remarks">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRemarks" runat="server" Text='<%# bind("FEE_REMARKS") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ApprovedAmount" Visible="false">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAppAmt" runat="server" AutoCompleteType="Disabled" CssClass="textboxmedium"
                                                    SkinID="TextBoxNormal" Style="text-align: right;" Text='<%# Bind("FEE_AMOUNT", "{0:###,###.00}")%>'
                                                    Width="110px"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="ftbeApp" runat="server" FilterType="Custom,Numbers"
                                                    TargetControlID="txtAppAmt" ValidChars="." />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click">Edit</asp:LinkButton>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4">
                                <asp:CheckBox ID="chkPrint" runat="server" Checked="True" Text="Print After Save" CssClass="field-label" />
                                <asp:Label ID="lblInfo" runat="server" CssClass="error" EnableViewState="False" SkinID="Error" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                    <tr align="center">
                        <td>
                            <asp:Button ID="btnRecall" runat="server" CausesValidation="False" CssClass="button"
                                Text="ReCall" Visible="False" />
                            <ajaxToolkit:ConfirmButtonExtender ID="cbe" runat="server"
                                TargetControlID="btnRecall"
                                ConfirmText="Are you sure you want to Recall?" />
                            <asp:Button ID="btnApprove" runat="server" CausesValidation="False" CssClass="button"
                                Text="Approve" Visible="False" />
                            <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" Visible="False" />
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
                <p></p>
            </div>
        </div>
    </div>
    <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" PopupControlID="Panel1"
        Position="left" TargetControlID="lnkRefundable">
    </ajaxToolkit:PopupControlExtender>
    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
    <asp:HiddenField ID="h_WEEKS_MONTHS_COUNT" runat="server" />
    <asp:SqlDataSource ID="sqlADJREASON" runat="server" ConnectionString="<%$ ConnectionStrings:OASIS_FEESConnectionString %>"
        SelectCommand="SELECT ARM_ID, ARM_DESCR&#13;&#10;FROM FEES.ADJ_REASON_M where ARM_bSHOW=1 order by ARM_DESCR  "></asp:SqlDataSource>
    <asp:HiddenField ID="h_STUD_ID" runat="server" />
    <asp:HiddenField ID="hf_GRD_ID" runat="server" />
    <asp:HiddenField ID="h_Amount" runat="server" />
    <asp:HiddenField ID="h_BSUID" runat="server" />
    <asp:HiddenField ID="h_print" runat="server" />
    <asp:HiddenField ID="h_CanEdit" runat="server" />
    <asp:Panel ID="Panel1" runat="server">
        <asp:GridView ID="gvFeePaidHistory" runat="server" SkinID="GridViewNormal" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="FEE_DESCR" HeaderText="FEE Type" />
                <asp:BoundField DataField="DOCDATE" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False"
                    HeaderText="Date" />
                <asp:BoundField DataField="PAID_AMT" HeaderText="Paid Amount" />
                <asp:BoundField DataField="CHARGED_AMT" HeaderText="Charged Amt" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
</asp:Content>
