Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports Microsoft.VisualBasic

Partial Class ShowStudent
    Inherits BasePage
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                gvGroup.Attributes.Add("bordercolor", "#1b80b6")
                h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                ddlAcademicYear.Items.Clear()
                'FillACD()

                Dim dtFiltr As DataTable = GetPopupFilter()

                ddlType.DataSource = dtFiltr
                ddlType.DataTextField = "DESCR"
                ddlType.DataValueField = "ID"
                ddlType.DataBind()

                FillBusinessUnit()

                If Not Session("sUsr_name") Is Nothing Then

                    ''added by nahyan to hide parent mobile while accessing from dependant add ESS 04feb2016
                    Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
                    Dim mnu_code As String = IIf(Request.QueryString("MainMnu_code") Is Nothing, "", Request.QueryString("MainMnu_code"))
                    mnu_code = Encr_decrData.Decrypt(mnu_code.Replace(" ", "+"))
                    If mnu_code <> "F100130" Then
                        enrlbl_id.Visible = False
                        enrdrp_id.Visible = False
                    Else
                        enrlbl_id.Visible = True
                        enrdrp_id.Visible = True
                    End If
                    If type = "DEP" Then
                        gvGroup.Columns(5).Visible = False
                    End If
                    GridBind()
                Else
                    gvGroup.DataBind()
                End If

            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');") 
            Response.Write("} </script>" & vbCrLf)
        End If
        set_Menu_Img()
    End Sub

    'Protected Sub btnAddStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddStudent.Click
    '    Dim Ids As String = String.Empty

    '    For Each gvrow As GridViewRow In Grid_Sibling.Rows
    '        Dim chk As CheckBox = DirectCast(gvrow.FindControl("chkChild"), CheckBox)
    '        If chk IsNot Nothing And chk.Checked Then
    '            Ids += Grid_Sibling.DataKeys(gvrow.RowIndex).Value.ToString() + ","
    '        End If
    '    Next

    '    ''checking if seat is available

    '    Ids = Ids.Trim(",".ToCharArray())
    '    hdnSelected.Value = Ids
    '    Session("liSelectedStudentList") = Ids
    '    hdnSelectedStudent.Value = Ids

    '    Response.Write("<script language='javascript'> function listen_window(){")
    '    Response.Write(" var oArg = new Object();")
    '    Response.Write("oArg.NameandCode ='" & Ids & "||" & Ids.Replace("'", "\'") & "||" & Ids & "';")
    '    Response.Write("var oWnd = GetRadWindow('" & Ids & "||" & Ids.Replace("'", "\'") & "||" & Ids & "');")
    '    Response.Write("oWnd.close(oArg);")
    '    Response.Write("} </script>")
    '    h_SelectedId.Value = "Close"

    '    'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Script11weasd", "setTrainerToParent();", True)

    'End Sub
    Private Sub GridBind()
        Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
        Select Case type
            Case "STUD_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GridbindStudentCompany(COMP_ID)
            Case "ENQ_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GridbindEnquiryCompany(COMP_ID)
            Case "REFER"
                Dim STU_ID As String = IIf(Request.QueryString("STU_ID") = "", -1, Request.QueryString("STU_ID"))
                GridbindStudents(STU_ID, DDLBusinessUnit.SelectedValue.ToString(), txtStdNo.Text.Trim().ToString(), txtStudName.Text.Trim().ToString())
            Case "REFERBB"
                Dim STU_ID As String = IIf(Request.QueryString("STU_ID") = "", -1, Request.QueryString("STU_ID"))
                GridbindStudentsBBRefer(STU_ID)
            Case "TC"
                Dim TYPE_VAL As Integer = IIf(Request.QueryString("VAL") = "", -1, Request.QueryString("VAL"))
                Dim vACD_ID As Integer = IIf(Request.QueryString("ACD_ID") = "", -1, Request.QueryString("ACD_ID"))
                If TYPE_VAL = 1 Then
                    GridbindTC(False, vACD_ID)
                ElseIf TYPE_VAL = 2 Then
                    GridbindTC(True)
                ElseIf TYPE_VAL = 3 Then
                    GridbindTC(False)
                End If
            Case "TCENQ"
                Dim TYPE_VAL As Integer = IIf(Request.QueryString("VAL") = "", -1, Request.QueryString("VAL"))
                Dim vACD_ID As Integer = IIf(Request.QueryString("ACD_ID") = "", -1, Request.QueryString("ACD_ID"))
                If TYPE_VAL = 1 Then
                    GridbindTCEnquiry(False, vACD_ID)
                ElseIf TYPE_VAL = 2 Then
                    GridbindTCEnquiry(True)
                ElseIf TYPE_VAL = 3 Then
                    GridbindTCEnquiry(False)
                End If
            Case "STUD_DATE"
                Dim vACD_ID As Integer = IIf(Request.QueryString("ACD_ID") = "", -1, Request.QueryString("ACD_ID"))
                Dim vVALDATE As String = IIf(Request.QueryString("VALDATE") = "", -1, Request.QueryString("VALDATE"))
                If Not IsDate(vVALDATE) Then
                    vVALDATE = Now.Date
                End If
                GridbindStudentWithDate(vACD_ID, vVALDATE)
            Case "ENQ_DATE"
                Dim vACD_ID As Integer = IIf(Request.QueryString("ACD_ID") = "", -1, Request.QueryString("ACD_ID"))
                GridbindStudentsENQ_DATE(vACD_ID)
            Case "CONCESSION"
                ConcessionStudents()
            Case "EOS"
                Dim EMP_ID As String
                EMP_ID = Request.QueryString("EMPID")
                getStudentOfEmployee(EMP_ID)
            Case "SERVICE_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                GetServiceStudents(COMP_ID)
            Case "ALL_SOURCE_STUDENTS"
                GridBindAllSourceStudents()
            Case Else
                GridbindStudents()

        End Select
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvGroup.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub GridbindTCEnquiry(ByVal bTC As Boolean, Optional ByVal vACD_ID As Integer = 0)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile, str_filter_sfSRno As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile, str_txtSR As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_filter_sfSRno = ""
            str_txtSR = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "REPLACE(PARENT_MOBILE, '-', '')", Trim(txtSearch.Text))
                ''SR number ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtSR")
                str_txtSR = txtSearch.Text
                str_filter_sfSRno = SetCondn(str_search, "SF_ENQID", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = String.Empty
            If bTC Then
                ' str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
                '& " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
                '& " FROM VW_OSO_STUDENT_M INNER JOIN TCM_M ON " _
                '& " TCM_BSU_ID =STU_BSU_ID AND TCM_ACD_ID = STU_ACD_ID AND TCM_STU_ID = STU_ID " _
                '& " WHERE STU_bActive=1  AND STU_BSU_ID='" & Session("sBsuid") & "' " _
                '& " AND isnull(TCM_ISSUEDATE,'1/1/1900') = '1/1/1900' AND TCM_bCANCELLED = 0 AND " & _
                '" TCM_bRegAppr = 1 And TCM_bLabAppr = 1 And TCM_bLibAppr = 1 "
            Else
                'Select * from FEES.vw_OSO_ENQUIRY_COMP 
                str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, REPLACE(PARENT_MOBILE, '-', '') AS PARENT_MOBILE, PARENT_NAME ," _
                & " ISNULL(GRM_DISPLAY,GRD_DISPLAY)GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID,SF_ENQID " _
                & " FROM FEES.vw_OSO_ENQUIRY_COMP " _
                & " WHERE STU_BSU_ID='" & Session("sBsuid") & "' "
                If vACD_ID <> 0 Then
                    str_Sql += " AND STU_ACD_ID = " & vACD_ID
                Else
                    'str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue
                End If
                str_Sql &= " AND STU_STATUS<>'DEL' "
            End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text,
            str_Sql & str_filter_grade & str_filter_code &
            str_filter_name & str_filter_parname & str_filter_parmobile & str_filter_sfSRno & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            txtSearch = gvGroup.HeaderRow.FindControl("txtSR")
            txtSearch.Text = str_txtSR
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindStudentWithDate(ByVal vACD_ID As Integer, ByVal vDate As Date)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "REPLACE(PARENT_MOBILE, '-', '')", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = String.Empty
            str_Sql = " SELECT DISTINCT STU_ID, STU_NO, STU_BSU_ID, LTRIM(RTRIM(STU_NAME)) STU_NAME, STU_GENDER, REPLACE(PARENT_MOBILE, '-', '') AS PARENT_MOBILE, PARENT_NAME ," _
                & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, '' SF_ENQID" _
                & " FROM OASIS_FEES.FEES.VW_OSO_STUDENT_ADJ_VIEW LEFT OUTER JOIN dbo.TCM_M ON TCM_STU_ID = STU_ID AND ISNULL(TCM_bCANCELLED, 0) = 0  " _
                & " WHERE STU_BSU_ID='" & Session("sBsuid") & "' AND ISNULL(TCM_bCLEARANCE, 0) = 0 " &
                " AND ISNULL(CASE WHEN isnull(STU_CURRSTATUS, '') = 'C' " &
                " THEN STU_CANCELDATE ELSE GETDATE() + 1 END, GETDATE() + 1)>= '" & vDate & "'"
            If vACD_ID <> 0 Then
                str_Sql += " AND STU_ACD_ID = " & vACD_ID
            Else
                str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text,
            str_Sql & str_filter_grade & str_filter_code &
            str_filter_name & str_filter_parname & str_filter_parmobile & " ORDER BY LTRIM(RTRIM(STU_NAME)) ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindTC(ByVal bTC As Boolean, Optional ByVal vACD_ID As Integer = 0)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "REPLACE(PARENT_MOBILE, '-', '')", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = String.Empty
            If bTC Then
                str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, REPLACE(PARENT_MOBILE, '-', '') AS PARENT_MOBILE, PARENT_NAME ," _
               & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, '' SF_ENQID" _
               & " FROM VW_OSO_STUDENT_M INNER JOIN TCM_M ON " _
               & " TCM_BSU_ID =STU_BSU_ID AND TCM_ACD_ID = STU_ACD_ID AND TCM_STU_ID = STU_ID " _
               & " WHERE STU_bActive=1  AND STU_BSU_ID='" & Session("sBsuid") & "' " _
               & " AND isnull(TCM_ISSUEDATE,'1/1/1900') = '1/1/1900' AND TCM_bCANCELLED = 0 AND " &
               " TCM_bRegAppr = 1 AND ISNULL(TCM_bCANCELLED,0) = 0 " '"And TCM_bLabAppr = 1 And TCM_bLibAppr = 1 "
                str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue
            Else
                str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, REPLACE(PARENT_MOBILE, '-', '') AS PARENT_MOBILE, PARENT_NAME ," _
                & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, '' SF_ENQID" _
                & " FROM OASIS_FEES.FEES.VW_OSO_STUDENT_ADJ_VIEW   " _
                & " WHERE  STU_BSU_ID='" & Session("sBsuid") & "' "
                If vACD_ID <> 0 Then
                    str_Sql += " AND STU_ACD_ID = " & vACD_ID
                Else
                    str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue
                End If
            End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text,
            str_Sql & str_filter_grade & str_filter_code &
            str_filter_name & str_filter_parname & str_filter_parmobile & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindStudents(Optional ByVal STUD_ID As String = "", Optional ByVal STR_BSU_ID As String = "",
                                 Optional ByVal STR_STUD_NO As String = "", Optional ByVal STR_STUD_NAME As String = "")
        Dim BSU_ID As String = Session("sBsuid")

        If Request.QueryString("bsu") <> "" Then
            BSU_ID = Request.QueryString("bsu")
        End If
        If Not STR_BSU_ID = "" Then
            BSU_ID = DDLBusinessUnit.SelectedValue.ToString()
        End If
        If STUD_ID = "-1" Then
            STUD_ID = ""
        End If
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                'str_Sid_search = h_Selected_menu_3.Value.Split("__")
                'str_search = str_Sid_search(0)
                'txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                'str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "REPLACE(PARENT_MOBILE, '-', '')", Trim(txtSearch.Text))
            End If
            Dim str_Sql As String = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, REPLACE(PARENT_MOBILE, '-', '') AS PARENT_MOBILE, PARENT_NAME ," _
            & " STU_CURRSTATUS, GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, '' SF_ENQID" _
            & " FROM VW_OSO_STUDENT_M " _
            & " WHERE STU_bActive=1  AND STU_BSU_ID='" & BSU_ID & "' AND STU_NO = '" & STR_STUD_NO & "' AND STU_ID <> '" & STUD_ID & "' AND STU_NAME LIKE '%" & STR_STUD_NAME & "%'" _
            & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS<>'CN'"
            str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue
            'If STUD_ID <> "" Then
            '    str_Sql += " AND STU_SIBLING_ID <> STU_ID  AND STU_ID <> '" & STUD_ID & "'"
            '    ' & " AND STU_SIBLING_ID = ( select STU_SIBLING_ID from  STUDENT_M where STU_ID='" & STUD_ID & "')"
            'End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text,
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_parname &
            str_filter_parmobile & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)



                Grid_Sibling.DataBind()
                Dim columnCount1 As Integer = Grid_Sibling.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                Grid_Sibling.Rows(0).Cells.Clear()
                Grid_Sibling.Rows(0).Cells.Add(New TableCell)
                Grid_Sibling.Rows(0).Cells(0).ColumnSpan = columnCount1
                Grid_Sibling.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                Grid_Sibling.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

            Else
                gvGroup.DataBind()

                GetSiblingStudents(txtStdNo.Text.Trim().ToString())
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            'txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            'txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindStudentsBBRefer(Optional ByVal STUD_ID As String = "")
        Dim BSU_ID As String = Session("sBsuid")
        If Request.QueryString("bsu") <> "" Then
            BSU_ID = Request.QueryString("bsu")
        End If
        If STUD_ID = "-1" Then
            STUD_ID = ""
        End If
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "REPLACE(PARENT_MOBILE, '-', '')", Trim(txtSearch.Text))
            End If
            Dim str_Sql As String = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, REPLACE(PARENT_MOBILE, '-', '') AS PARENT_MOBILE, PARENT_NAME ," _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, '' SF_ENQID" _
            & " FROM VW_OSO_STUDENT_M " _
            & " WHERE STU_bActive=1  AND STU_BSU_ID='" & BSU_ID & "' AND STU_ID <> '" & STUD_ID & "'" _
            & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS<>'CN'"
            str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue
            'If STUD_ID <> "" Then
            '    str_Sql += " AND STU_SIBLING_ID <> STU_ID  AND STU_ID <> '" & STUD_ID & "'"
            '    ' & " AND STU_SIBLING_ID = ( select STU_SIBLING_ID from  STUDENT_M where STU_ID='" & STUD_ID & "')"
            'End If
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text,
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_parname &
            str_filter_parmobile & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindStudentsENQ_DATE(ByVal vACD_ID As Integer)
        Dim BSU_ID As String = Session("sBsuid")
        If Request.QueryString("bsu") <> "" Then
            BSU_ID = Request.QueryString("bsu")
        End If
        If vACD_ID = -1 Then
            vACD_ID = 0
        End If
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile, str_filter_sfSRno As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile, str_txtSR As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_filter_sfSRno = ""
            str_txtSR = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "REPLACE(PARENT_MOBILE, '-', '')", Trim(txtSearch.Text))
                ''SR number ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtSR")
                str_txtSR = txtSearch.Text
                str_filter_sfSRno = SetCondn(str_search, "SF_ENQID", Trim(txtSearch.Text))
            End If
            Dim str_Sql As String = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, REPLACE(PARENT_MOBILE, '-', '') AS PARENT_MOBILE, PARENT_NAME ," _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, 1 STU_bActive,SF_ENQID " _
            & " FROM FEES.vw_OSO_ENQUIRY_COMP " _
            & " WHERE  STU_BSU_ID='" & BSU_ID & "' "

            If vACD_ID <> 0 Then
                str_Sql += " AND  STU_ACD_ID= " & vACD_ID & " "
            Else
                'str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue
            End If
            str_Sql &= " AND STU_STATUS<>'DEL' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text,
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_parname &
            str_filter_parmobile & str_filter_sfSRno & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            txtSearch = gvGroup.HeaderRow.FindControl("txtSR")
            txtSearch.Text = str_txtSR
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub getStudentOfEmployee(ByVal EMP_Id As String)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "REPLACE(PARENT_MOBILE, '-', '')", Trim(txtSearch.Text))
            End If
            Dim str_Sql, EMP_MOB_NO As String
            EMP_MOB_NO = ""
            If EMP_Id <> "" Then
                str_Sql = "select right(EMD_CUR_MOBILE,6) from EMPLOYEE_D WHERE EMD_EMP_ID='" & EMP_Id & "'"
                EMP_MOB_NO = Mainclass.getDataValue(str_Sql, "OASISConnectionString")
            End If

            str_Sql = " SELECT      STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, REPLACE(PARENT_MOBILE, '-', '') AS PARENT_MOBILE, PARENT_NAME , " _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, '' SF_ENQID FROM   FEES.FEE_CONCESSION_H INNER JOIN " _
            & " VW_OSO_STUDENT_M ON FEES.FEE_CONCESSION_H.FCH_BSU_ID = VW_OSO_STUDENT_M.STU_BSU_ID AND " _
            & " FEES.FEE_CONCESSION_H.FCH_STU_ID = VW_OSO_STUDENT_M.STU_ID " _
            & " WHERE STU_bActive=1  AND STU_CURRSTATUS='EN' AND ISNULL(FCH_bDeleted,0) = 0  "

            str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue
            If EMP_MOB_NO <> "" And EMP_Id <> "" Then
                str_Sql += " AND right(PARENT_MOBILE,6) = '" & EMP_MOB_NO & "'"
            Else
                str_Sql += " AND 1=2 "
            End If
            Dim str_filter_refund_acd As String = ""
            Dim SqlGroupby As String = ""
            SqlGroupby = " GROUP BY STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME , " _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text,
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_parname &
            str_filter_parmobile & str_filter_refund_acd & SqlGroupby & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub ConcessionStudents(Optional ByVal STUD_ID As String = "")
        Dim BSU_ID As String = Session("sBsuid")
        If Request.QueryString("bsu") <> "" Then
            BSU_ID = Request.QueryString("bsu")
        End If
        If STUD_ID = "-1" Then
            STUD_ID = ""
        End If
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "REPLACE(PARENT_MOBILE, '-', '')", Trim(txtSearch.Text))
            End If
            Dim str_Sql As String = " SELECT      STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, REPLACE(PARENT_MOBILE, '-', '') AS PARENT_MOBILE, PARENT_NAME , " _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, '' SF_ENQID FROM   FEES.FEE_CONCESSION_H INNER JOIN " _
            & " VW_OSO_STUDENT_M ON FEES.FEE_CONCESSION_H.FCH_BSU_ID = VW_OSO_STUDENT_M.STU_BSU_ID AND " _
            & " FEES.FEE_CONCESSION_H.FCH_STU_ID = VW_OSO_STUDENT_M.STU_ID " _
            & " WHERE STU_bActive=1  AND ISNULL(FCH_bDeleted,0) = 0 AND STU_BSU_ID='" & BSU_ID & "' "
            str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue

            If STUD_ID <> "" Then
                str_Sql += " AND STU_SIBLING_ID <> STU_ID  AND STU_ID <> '" & STUD_ID & "'" _
                & " AND STU_SIBLING_ID = ( select STU_SIBLING_ID from  STUDENT_M where STU_ID='" & STUD_ID & "')"
            End If

            Dim str_filter_refund_acd As String = ""
            'If Request.QueryString("refund") <> "" Then
            '    str_filter_refund_acd = " AND STU_ID IN ( SELECT FAR_STU_ID FROM FEES.FEEADJREQUEST_H " _
            '    & " WHERE (FAR_BSU_ID = '" & BSU_ID & "') AND (FAR_STU_TYP = 's') AND (FAR_ApprStatus = 'A')  " _
            '    & " AND (FAR_EVENT IN ('2' )) AND (FAR_ID NOT IN (SELECT FRH_FAR_ID FROM FEES.FEE_REFUND_H))) "
            'End If
            'If Request.QueryString("acd_id") <> "" Then
            '    str_filter_refund_acd = " AND STU_ACD_ID = '" & Request.QueryString("acd_id") & "'"
            'End If  
            Dim SqlGroupby As String = ""
            SqlGroupby = " GROUP BY STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME , " _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text,
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_parname &
            str_filter_parmobile & str_filter_refund_acd & SqlGroupby & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindEnquiryCompany(ByVal COMP_ID As Integer)
        Try
            Dim str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_grade As String
            Dim str_txtCode, str_txtName, str_txtGrade As String
            str_filter_code = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_mode = Request.QueryString("ShowType")
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType 
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = " Select * from FEES.vw_OSO_ENQUIRY_COMP "
            str_Sql += " WHERE  STU_BSU_ID='" & Session("sBSUID") & "'"
            If COMP_ID <> "-1" Then
                str_Sql += " AND  COMP_ID = " & COMP_ID
            End If
            str_Sql &= " AND STU_STATUS<>'DEL' "
            'str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue

            If Request.QueryString("EXL_TYPE") IsNot Nothing Then
                Dim EXL_TYPE As String = Request.QueryString("EXL_TYPE")
                str_Sql &= " AND STU_ID not in (select EXL_STU_ID from OASIS_FEES.[FEES].[STUDENT_EXCLUDE_LIST] WHERE EXL_TYPE='" & EXL_TYPE & "'  and EXL_BSU_ID='" & Session("sBSUID") & "') "
                If EXL_TYPE = "PI" Then
                    str_Sql &= " AND STU_ID not in (select TCM_STU_ID from OASIS..TCM_M where isnull( TCM_Bcancelled,0)=0  and TCM_BSU_ID='" & Session("sBSUID") & "' and ISNULL(TCM_LASTATTDATE,GETDATE()+1)<GETDATE()) "
                End If
            End If

            Dim str_filter_refund As String = ""
            If Request.QueryString("refund") <> "" Then
                str_filter_refund = " AND STU_ID IN ( SELECT FAR_STU_ID FROM FEES.FEEADJREQUEST_H " _
                & " WHERE (FAR_BSU_ID = '" & Session("sBSUID") & "') AND (FAR_STU_TYP = 'E') AND (FAR_ApprStatus = 'A')  " _
                & " AND (FAR_EVENT IN ('R', 'W')) AND (FAR_ID NOT IN (SELECT FRH_FAR_ID FROM FEES.FEE_REFUND_H))) "
            End If







            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text,
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_refund & " ORDER BY STU_NAME ")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub GetServiceStudents(ByVal COMP_ID As Integer)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade As String
            Dim str_txtCode, str_txtName, str_txtGrade As String
            str_filter_code = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_mode = Request.QueryString("ShowType")
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType 
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
            End If
            Dim BSU As String = ""
            If Request.QueryString("BSU") IsNot Nothing Then
                BSU = Request.QueryString("BSU")
            End If

            Dim str_Sql As String = " SELECT STU_ID,STU_NO,STU_BSU_ID,STU_NAME,STU_GENDER, " &
                                    "PARENT_MOBILE,PARENT_NAME,GRD_DISPLAY,STU_ACD_ID , " &
                                    "ACY_DESCR, STU_GRD_ID, STU_bActive, '' SF_ENQID " &
                                    "FROM OASIS_SERVICES.dbo.VW_OSO_STUDENT_M " &
                                    "WHERE STU_BSU_ID = '" & BSU & "' AND " &
                                    "STU_ID IN ( SELECT DISTINCT SSR_STU_ID FROM STUDENT_SERVICES_REQUEST WITH(NOLOCK) WHERE SSR_BSU_ID = '500610' ) "

            If Request.QueryString("STUID") IsNot Nothing Then
                Dim STUID As String = Request.QueryString("STUID")
                str_Sql = str_Sql & " AND STU_SIBLING_ID <> STU_ID AND STU_SIBLING_ID=" & STUID & ""
            End If
            If COMP_ID <> "-1" Then
                str_Sql = str_Sql & " AND ISNULL(COMP_ID,0) = " & COMP_ID
            End If
            str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text,
            str_Sql & str_filter_grade & str_filter_code & str_filter_name & " ORDER BY LTRIM(RTRIM(STU_NAME))")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub GridbindStudentCompany(ByVal COMP_ID As Integer)
        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade As String
            Dim str_txtCode, str_txtName, str_txtGrade As String
            str_filter_code = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_mode = Request.QueryString("ShowType")
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType 
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
            End If

            Dim str_Sql As String = " select * from ( SELECT STU_ID, STU_NO, " &
            " STU_BSU_ID, STU_NAME, GRM_DESCR,SCT_DESCR,STU_GENDER,PARENT_NAME,PARENT_MOBILE, " &
            " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, " &
            " CASE isnull(oasis.dbo.STUDENT_D.STS_FEESPONSOR,0) WHEN 4 " &
            " THEN IsNULL(oasis.dbo.STUDENT_D.STS_F_COMP_ID, '')   " &
            " WHEN 5 THEN IsNULL(oasis.dbo.STUDENT_D.STS_M_COMP_ID, '')   " &
            " WHEN 6 THEN IsNULL(oasis.dbo.STUDENT_D.STS_G_COMP_ID, '')  END AS COMP_ID, '' SF_ENQID " &
            " FROM VW_OSO_STUDENT_M  INNER JOIN" &
            " dbo.STUDENT_D ON ISNULL(dbo.VW_OSO_STUDENT_M.STU_SIBLING_ID, STU_ID) = STS_STU_ID" &
            " AND  STU_ACD_ID IN (SELECT ACD_ID FROM ACADEMICYEAR_D WHERE (ACD_CURRENT=1 or ACD_ENDDT > GETDATE()) AND  ACD_BSU_ID = '" & Session("sBSUID") & "' )  " &
            " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS not in('CN','TF') "

            If Request.QueryString("NewStudent") IsNot Nothing And Request.QueryString("ACD_ID") IsNot Nothing Then
                Dim NewStudent As String = Request.QueryString("NewStudent")
                Dim ACD_ID As String = Request.QueryString("ACD_ID")
                If NewStudent Then
                    str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) >= (SELECT ACD_STARTDT FROM ACADEMICYEAR_D WHERE ACD_ID= " & Val(ACD_ID) & ")"
                    str_Sql &= " AND STU_ACD_ID=" & ACD_ID.ToString
                    str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) > dateadd(month,-2,getdate()) "
                Else
                    'str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01'))  < (SELECT ACD_STARTDT FROM ACADEMICYEAR_D WHERE ACD_ID= " & Val(ACD_ID) & ")"
                    str_Sql &= "  AND CONVERT(datetime, ISNULL(STU_DOJ,'2100-01-01')) < dateadd(month,-2,getdate()) "

                End If
            Else
                str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue
            End If

            str_Sql &= " ) a "

            If Request.QueryString("IsService") IsNot Nothing Then
                Dim IsService As String = Request.QueryString("IsService")
                If IsService Then
                    str_Sql &= " INNER JOIN (SELECT DISTINCT SSV_STU_ID FROM OASIS_FEES.FEES.VW_PROFORMA_STUD_SERVICES WHERE isnull(SSV_TODATE,getdate())>=getdate() and  SSV_BSU_ID='" & Session("sBSUID") & "' "
                    str_Sql &= " AND  SSV_ACD_ID IN (SELECT ACD_ID FROM ACADEMICYEAR_D WHERE (ACD_CURRENT=1 or ACD_ENDDT > GETDATE()) AND  ACD_BSU_ID = '" & Session("sBSUID") & "')"
                    If Request.QueryString("FeeTypes") IsNot Nothing Then
                        Dim FeeTypes As String = Request.QueryString("FeeTypes")
                        str_Sql &= " AND  SSV_FEE_ID IN (select * from dbo.fnSplitMe('" & FeeTypes & "','|'))"
                    End If
                    str_Sql &= ")AS B ON a.STU_ID=B.SSV_STU_ID "
                End If
            End If

            str_Sql &= "WHERE a.STU_BSU_ID='" & Session("sBSUID") & "'"

            If Request.QueryString("EXL_TYPE") IsNot Nothing Then
                Dim EXL_TYPE As String = Request.QueryString("EXL_TYPE")
                str_Sql &= " AND a.STU_ID not in (select EXL_STU_ID from OASIS_FEES.[FEES].[STUDENT_EXCLUDE_LIST] WHERE EXL_TYPE='" & EXL_TYPE & "'  and EXL_BSU_ID='" & Session("sBSUID") & "') "
                If EXL_TYPE = "PI" Then
                    If Request.QueryString("EXCL_TC") IsNot Nothing AndAlso Request.QueryString("EXCL_TC") = "1" Then 'Exclude future TC students, added condition by Jacob on 08/Jun/2016
                        str_Sql &= " AND a.STU_ID not in (select TCM_STU_ID from OASIS..TCM_M where isnull( TCM_Bcancelled,0)=0 AND ISNULL(TCM_bPREAPPROVED,0)<>0 AND TCM_BSU_ID='" & Session("sBSUID") & "') "
                    Else
                        str_Sql &= " AND a.STU_ID not in (select TCM_STU_ID from OASIS..TCM_M where isnull( TCM_Bcancelled,0)=0 AND ISNULL(TCM_bPREAPPROVED,0)<>0 AND TCM_BSU_ID='" & Session("sBSUID") & "' and ISNULL(TCM_LASTATTDATE,GETDATE()+1)<GETDATE()) "
                    End If
                End If
            End If

            If COMP_ID <> "-1" Then
                str_Sql += " AND a.COMP_ID = " & COMP_ID
            End If
            Dim ds As New DataSet
            'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, _
            'str_Sql & str_filter_grade & str_filter_code & str_filter_name & " ORDER BY " & IIf(str_filter_grade <> "", "LTRIM(RTRIM(GRM_DESCR)),LTRIM(RTRIM(SCT_DESCR))", "LTRIM(RTRIM(STU_NAME))") & " ")

            Dim NewStudent_fltr As String = ""
            Dim ACD_ID_fltr As String = ""
            Dim IsService_fltr As String = ""
            Dim FeeTypes_fltr As String = ""
            Dim EXL_TYPE_fltr As String = ""
            Dim EXCL_TC_fltr As String = ""
            Dim Enroll_type_fltr As String = ""
            If Request.QueryString("NewStudent") IsNot Nothing Then
                NewStudent_fltr = Request.QueryString("NewStudent")
            End If

            If Request.QueryString("ACD_ID") IsNot Nothing Then
                ACD_ID_fltr = Request.QueryString("ACD_ID")
            End If
            If Request.QueryString("IsService") IsNot Nothing Then
                IsService_fltr = Request.QueryString("IsService")
            End If

            If Request.QueryString("FeeTypes") IsNot Nothing Then
                FeeTypes_fltr = Request.QueryString("FeeTypes")
            End If

            If Request.QueryString("EXL_TYPE") IsNot Nothing Then
                EXL_TYPE_fltr = Request.QueryString("EXL_TYPE")
            End If

            If Request.QueryString("EXCL_TC") IsNot Nothing Then
                EXCL_TC_fltr = Request.QueryString("EXCL_TC")
            End If

            'If ddlType.SelectedValue = "1" Then
            '    Enroll_type_fltr = "ALL"
            'Else
            '    Enroll_type_fltr = "ENROLL"
            'End If

            Enroll_type_fltr = ddlType.SelectedValue

            Dim pParms(13) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
            pParms(0).Value = 1
            pParms(1) = New SqlClient.SqlParameter("@BSU_CODE", SqlDbType.VarChar)
            pParms(1).Value = Session("sBSUID")
            pParms(2) = New SqlClient.SqlParameter("@NewStudent", SqlDbType.VarChar)
            pParms(2).Value = NewStudent_fltr
            pParms(3) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.VarChar)
            pParms(3).Value = ACD_ID_fltr
            pParms(4) = New SqlClient.SqlParameter("@POP_ACD_ID", SqlDbType.VarChar)
            pParms(4).Value = ddlAcademicYear.SelectedValue
            pParms(5) = New SqlClient.SqlParameter("@IsService", SqlDbType.VarChar)
            pParms(5).Value = IsService_fltr
            pParms(6) = New SqlClient.SqlParameter("@FEE_TYPE", SqlDbType.VarChar)
            pParms(6).Value = FeeTypes_fltr
            pParms(7) = New SqlClient.SqlParameter("@EXL_TYPE", SqlDbType.VarChar)
            pParms(7).Value = EXL_TYPE_fltr
            pParms(8) = New SqlClient.SqlParameter("@EXCL_TC", SqlDbType.VarChar)
            pParms(8).Value = EXCL_TC_fltr
            pParms(9) = New SqlClient.SqlParameter("@COMP_ID", SqlDbType.VarChar)
            pParms(9).Value = COMP_ID
            pParms(10) = New SqlClient.SqlParameter("@STU_NO_FLTR", SqlDbType.VarChar)
            pParms(10).Value = str_txtCode
            pParms(11) = New SqlClient.SqlParameter("@STU_NAME_FLTR", SqlDbType.VarChar)
            pParms(11).Value = str_txtName
            pParms(12) = New SqlClient.SqlParameter("@STU_GRD_FLTR", SqlDbType.VarChar)
            pParms(12).Value = str_txtGrade
            pParms(13) = New SqlClient.SqlParameter("@ENROLL_TYPE_FLTR", SqlDbType.VarChar)
            pParms(13).Value = Enroll_type_fltr
            str_Sql = "[OASIS].[DBO].[GET_STU_NEW_POPUP_LIST]"
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, str_Sql, pParms)



            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub DDCutomerSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub DDAccountType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub DDBankorCash_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSTU_ID As New Label
        Dim lbClose As New LinkButton
        lbClose = sender
        lblSTU_ID = sender.Parent.FindControl("lblSTU_ID")
        Dim lblCode As New Label
        lblCode = sender.Parent.FindControl("lblCode")
        lblCode.Text = lblCode.Text.Replace("___", "||")
        If (Not lblSTU_ID Is Nothing) Then

            Dim Ids As String = String.Empty

            For Each gvrow As GridViewRow In Grid_Sibling.Rows
                Dim chk As CheckBox = DirectCast(gvrow.FindControl("chkChild"), CheckBox)
                If chk IsNot Nothing And chk.Checked Then
                    Ids += Grid_Sibling.DataKeys(gvrow.RowIndex).Value.ToString() + ","
                End If
            Next

            ''checking if seat is available

            Ids = Ids.Trim(",".ToCharArray())
            'hdnSelected.Value = Ids
            Session("liSelectedSiblingStudent") = Ids
            hdnSelectedSiblingStudent.Value = Ids


            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblSTU_ID.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblCode.Text & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & lblSTU_ID.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblCode.Text & "';")
            Response.Write("var oWnd = GetRadWindow('" & lblSTU_ID.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblCode.Text & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvGroup.PageIndex = 0
        GridBind()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvGroup.PageIndex = 0
        GridBind()
    End Sub

    Protected Sub btnSearchControl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvGroup.PageIndex = 0
        GridBind()
    End Sub


    Private Sub GridBindAllSourceStudents()
        Dim ProviderBSUID, STU_TYPE, Stu_Bsu_Id As String
        If Request.QueryString("ProviderBSUID").ToString <> "" Then
            ProviderBSUID = Request.QueryString("ProviderBSUID").ToString
        End If
        If Request.QueryString("STU_TYPE").ToString <> "" Then
            STU_TYPE = Request.QueryString("STU_TYPE").ToString
        End If
        If Request.QueryString("Stu_Bsu_Id").ToString <> "" Then
            Stu_Bsu_Id = Request.QueryString("Stu_Bsu_Id").ToString
        End If

        Try
            Dim str_mode, str_search, str_filter_code, str_filter_name, str_filter_grade, str_filter_parname, str_filter_parmobile, str_filter_sfSRno As String
            Dim str_txtCode, str_txtName, str_txtGrade, str_txtPName, str_txtMobile, str_txtSR As String
            str_filter_code = ""
            str_filter_parname = ""
            str_filter_parmobile = ""
            str_filter_name = ""
            str_filter_grade = ""
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_txtPName = ""
            str_txtMobile = ""
            str_filter_sfSRno = ""
            str_txtSR = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
                ''SR number ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtSR")
                str_txtSR = txtSearch.Text
                str_filter_sfSRno = SetCondn(str_search, "SF_ENQID", Trim(txtSearch.Text))
            End If
            Dim str_Sql As String
            Dim ds As New DataSet
            Select Case ProviderBSUID
                Case "900501", "900500"
                    str_Sql = " SELECT DISTINCT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
                               & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, '' SF_ENQID" _
                               & " FROM VW_OSO_STUDENT_M INNER JOIN VW_STUDENT_SERVICES_D_MAX ON STU_ID=SSV_STU_ID" _
                               & " WHERE STU_bActive=1  AND STU_BSU_ID='" & Stu_Bsu_Id & "' "
                    str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue
                    ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text,
                    str_Sql & str_filter_grade & str_filter_code &
                    str_filter_name & str_filter_parname & str_filter_parmobile & " ORDER BY STU_NAME")
                Case Else
                    If STU_TYPE = "E" Then
                        str_Sql = " SELECT DISTINCT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
                                & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, 1 STU_bActive,SF_ENQID" _
                                & " FROM FEES.vw_OSO_ENQUIRY_COMP " _
                                & " WHERE  STU_BSU_ID='" & ProviderBSUID & "' "
                        str_Sql &= " AND STU_STATUS<>'DEL' "
                        'str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue
                        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text,
                        str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_parname &
                        str_filter_parmobile & str_filter_sfSRno & " ORDER BY STU_NAME ")
                    Else
                        str_Sql = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
                            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive, '' SF_ENQID" _
                            & " FROM VW_OSO_STUDENT_M " _
                            & " WHERE STU_bActive=1  AND STU_BSU_ID='" & ProviderBSUID & "'" _
                            & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS<>'CN'"
                        str_Sql += " AND STU_ACD_ID = " & ddlAcademicYear.SelectedValue
                        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text,
                            str_Sql & str_filter_grade & str_filter_code & str_filter_name & str_filter_parname &
                            str_filter_parmobile & " ORDER BY STU_NAME ")
                    End If

            End Select

            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            txtSearch = gvGroup.HeaderRow.FindControl("txtSR")
            txtSearch.Text = str_txtSR
            set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Public Shared Function GetPopupFilter() As DataTable
        Dim sql_query As String = " SELECT * FROM DBO.VW_STU_POPUP_FILTER"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Sub FillACD()

        Dim Stu_Bsu_Id As String = IIf(Request.QueryString("Stu_Bsu_Id") Is Nothing, "", Request.QueryString("Stu_Bsu_Id"))
        If Stu_Bsu_Id = "" Then
            Stu_Bsu_Id = Session("sBSUID")
        End If

        Stu_Bsu_Id = DDLBusinessUnit.SelectedValue.ToString()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Stu_Bsu_Id)
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next

    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub DDLBusinessUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusinessUnit.SelectedIndexChanged
        FillACD()
        'Dim STU_ID As String = IIf(Request.QueryString("STU_ID") = "", -1, Request.QueryString("STU_ID"))
        'GridbindStudents(STU_ID, DDLBusinessUnit.SelectedValue.ToString(),)
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim STU_ID As String = IIf(Request.QueryString("STU_ID") = "", -1, Request.QueryString("STU_ID"))
        GridbindStudents(STU_ID, DDLBusinessUnit.SelectedValue.ToString(), txtStdNo.Text.Trim().ToString(), txtStudName.Text.Trim().ToString())
    End Sub

    Private Sub GetSiblingStudents(Optional ByVal STR_STUD_NO As String = "")
        Dim BSU_ID As String = Session("sBsuid")
        If Request.QueryString("bsu") <> "" Then
            BSU_ID = Request.QueryString("bsu")
        End If
        BSU_ID = DDLBusinessUnit.SelectedValue.ToString()
        Try
            Dim ds As New DataSet
            Using con As New SqlConnection(ConnectionManger.GetOASISConnectionString)
                Using cmd As New SqlCommand("[FEES].[GET_SIBLING_STUDENT_DETAILS]", con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@STU_NO", STR_STUD_NO)
                    cmd.Parameters.AddWithValue("@BSU_ID", BSU_ID)
                    cmd.Parameters.AddWithValue("@STU_ACD_ID", ddlAcademicYear.SelectedValue)
                    con.Open()
                    Using Adpt As New SqlDataAdapter(cmd)
                        ds = New DataSet()
                        Adpt.Fill(ds)
                    End Using
                    con.Close()
                End Using
            End Using

            Grid_Sibling.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                Grid_Sibling.DataBind()
                Dim columnCount As Integer = Grid_Sibling.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                Grid_Sibling.Rows(0).Cells.Clear()
                Grid_Sibling.Rows(0).Cells.Add(New TableCell)
                Grid_Sibling.Rows(0).Cells(0).ColumnSpan = columnCount
                Grid_Sibling.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                Grid_Sibling.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                Grid_Sibling.DataBind()
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub FillBusinessUnit()
        Dim str_conn = ConnectionManger.GetOASISConnectionString()
        Dim ds As DataSet
        Using con As New SqlConnection(str_conn)
            Using cmd As New SqlCommand("[FEES].[GET_ACTIVE_BUSINESS_UNIT]", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
                con.Open()
                Using Adpt As New SqlDataAdapter(cmd)
                    ds = New DataSet()
                    Adpt.Fill(ds)
                    DDLBusinessUnit.DataSource = ds
                    DDLBusinessUnit.DataTextField = "BSU_NAME"
                    DDLBusinessUnit.DataValueField = "BSU_ID"
                    DDLBusinessUnit.DataBind()
                End Using
                con.Close()
            End Using
        End Using

        FillACD()
    End Sub
End Class
