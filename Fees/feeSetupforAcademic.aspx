<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="feeSetupforAcademic.aspx.vb" Inherits="Fees_feeSetupforAcademic" Title="Untitled Page"
    Theme="General" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server" EnableViewState="false">

    <script type="text/javascript" language="javascript">
        function getLOCATION() {
            var sFeatures, url;

            var NameandCode;
            var result;
            url = "../common/PopupFormIDhidden.aspx?iD=LOCATIONFEE&bsu=<%= Session("sBsuid") %>&MULTISELECT=FALSE";
          <%--result = window.showModalDialog(url,"", sFeatures); 
           if (result=='' || result==undefined)
            { return false; }             
            NameandCode = result.split('___');
           document.getElementById('<%= H_Location.ClientID %>').value=NameandCode[0]; 
           document.getElementById('<%= txtLocation.ClientID %>' ).value=NameandCode[1] ; 
           return false;
    } --%>


            var oWnd = radopen(url, "pop_getlocation");

        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%= H_Location.ClientID %>').value = NameandCode[0];
           document.getElementById('<%= txtLocation.ClientID %>').value = NameandCode[1];
           __doPostBack('<%=txtLocation.ClientID%>', 'TextChanged');
       }
   }

   function Showdata(mode) {
       var sFeatures, url;
       sFeatures = "dialogWidth:600px; ";
       sFeatures += "dialogHeight: 500px; ";
       sFeatures += "help: no; ";
       sFeatures += "resizable: no; ";
       sFeatures += "scroll: yes; ";
       sFeatures += "status: no; ";
       sFeatures += "unadorned: no; ";
       var NameandCode;
       var result;
       if (mode == 1)
           url = "../common/PopupShowData.aspx?id=FEEFORGRADE&grd=" + document.getElementById('<%= ddlGrade.ClientID %>').value + "&acd=" + document.getElementById('<%= ddlAcademic.ClientID %>').value;
            //result = window.showModalDialog(url,"", sFeatures); 
            //return false;
        var oWnd = radopen(url, "pop_showdata");
    }


    function CheckAmount(e) {
        var amt;
        amt = parseFloat(e.value)
        if (isNaN(amt))
            amt = 0;
        e.value = amt.toFixed(2);
        return true;
    }

    function GetLinkToStage() {

        var NameandCode;
        var result;
       <%--result = window.showModalDialog("../common/PopupForm.aspx?id=LINKTOSTAGE&multiSelect=false","", sFeatures); 
        if (result=='' || result==undefined)
        {    return false;      } 
         NameandCode=result.split('___');     
         document.getElementById('<%=txtLinkToStage.ClientID %>').value=NameandCode[1];
         document.getElementById('<%=hf_PRO_ID.ClientID %>').value=NameandCode[0];
        return false;
    } --%>
    var url = "../common/PopupForm.aspx?id=LINKTOSTAGE&multiSelect=false";
    var oWnd = radopen(url, "pop_getlinktostage");

}

function OnClientClose2(oWnd, args) {
    //get the transferred arguments

    var arg = args.get_argument();

    if (arg) {
        NameandCode = arg.NameandCode.split('||');
        document.getElementById('<%=txtLinkToStage.ClientID %>').value = NameandCode[1];
           document.getElementById('<%=hf_PRO_ID.ClientID %>').value = NameandCode[0];
           __doPostBack('<%=txtLinkToStage.ClientID%>', 'TextChanged');
       }
   }

   function ClearStage() {
       document.getElementById('<%=txtLinkToStage.ClientID %>').value = '';
            document.getElementById('<%=hf_PRO_ID.ClientID %>').value = '';
        }




        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getlocation" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_showdata" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getlinktostage" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Fee Setup For Academic Year
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>--%>
                            <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademic" runat="server" DataSourceID="odsGetBSUAcademicYear"
                                DataTextField="ACY_DESCR" DataValueField="ACD_ID" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%">
                            <span class="field-label">From Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server" AutoCompleteType="None"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Fee Type</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlFeetype" runat="server" DataSourceID="odsGetFEETYPE_M" DataTextField="FEE_DESCR"
                                DataValueField="FEE_ID" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <span class="field-label">Grade</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server" DataSourceID="odsGetGRADE_M"
                                DataTextField="GRM_DISPLAY" DataValueField="GRM_GRD_ID" SkinID="DropDownListNormal">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Payment Plan</span>
                        </td>
                        <td align="left">
                            <asp:CheckBox ID="chkAllowFlexible" runat="server" Text="Allow Flexible Plan" />
                        </td>
                        <td align="left">
                            <span class="field-label">Stream</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlStream" runat="server" DataSourceID="odsStream_m" DataTextField="STM_DESCR"
                                DataValueField="STM_ID" SkinID="DropDownListNormal">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Collection Schedule</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlCollectionSchedule" runat="server" DataSourceID="odsGetSCHEDULE_M"
                                DataTextField="SCH_DESCR" DataValueField="SCH_ID" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList>
                            <asp:CheckBox ID="chkDefaultPlan" runat="server" Text="Default Plan" AutoPostBack="True" />
                        </td>
                        <td align="left">
                            <span class="field-label">Payment Plan (If current plan failed)</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlChargeScheduleFail" runat="server" DataSourceID="odsGetSCHEDULE_M"
                                DataTextField="SCH_DESCR" DataValueField="SCH_ID" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Pro Rata (Join)</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlJoinprorata" runat="server" DataSourceID="SqlProrata" DataTextField="FPM_DESCR"
                                DataValueField="FPM_ID" SkinID="DropDownListNormal">
                            </asp:DropDownList></td>
                        <td align="left">
                            <span class="field-label">Pro Rata (Discontinue)</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlDiscontinueprorata" runat="server" DataSourceID="SqlProrata"
                                DataTextField="FPM_DESCR" DataValueField="FPM_ID" SkinID="DropDownListNormal">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Invoice Due Date</span></td>
                        <td align="left">
                            <asp:UpdatePanel ID="UP1" runat="server">
                                <ContentTemplate>
                                    <asp:CheckBox ID="chkInvDueDt" runat="server" AutoPostBack="true" OnCheckedChanged="chkInvDueDt_CheckedChanged" /><asp:TextBox
                                        ID="txtInvDueDt" runat="server" AutoCompleteType="None"></asp:TextBox>
                                    <asp:ImageButton ID="imgInvDueDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                        TabIndex="4" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" CssClass="MyCalendar" runat="server"
                                        PopupButtonID="imgInvDueDt" TargetControlID="txtInvDueDt" Format="dd/MMM/yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" PopupButtonID="txtInvDueDt"
                                        TargetControlID="txtInvDueDt" CssClass="MyCalendar" Format="dd/MMM/yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td align="left">
                            <span class="field-label">Revenue Recognition</span></td>
                        <td align="left">
                            <%--<asp:DropDownList ID="ddlRevReco" runat="server" DataSourceID="odsGetSCHEDULE_M_NEW"  
                                DataTextField="SCH_DESCR" DataValueField="SCH_ID" AutoPostBack="True" SkinID="DropDownListNormal">
                                 <asp:ListItem Text="--" Value ="-1"></asp:ListItem>
                            </asp:DropDownList></td>--%>
                            <asp:DropDownList ID="ddlRevReco" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Active</span></td>
                        <td align="left">
                            <asp:CheckBox ID="chkActive" runat="server" Checked="True" Enabled="False" /></td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                    <tr id="trDiscRemarks" runat="server" visible="false">
                        <td align="left">
                            <span class="field-label">Discontinuation Remarks</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtDiscRemarks" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Link To Stage</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtLinkToStage" runat="server">
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="imgLinkToStage" runat="server" ImageUrl="~/Images/cal.gif"
                                OnClientClick="GetLinkToStage();return false;"></asp:ImageButton>
                            <asp:LinkButton ID="lnkClear" runat="server" OnClientClick="ClearStage();return false;">Clear</asp:LinkButton></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr id="tr_Transport" runat="server">
                        <td align="left">
                            <span class="field-label">Area</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtLocation" runat="server" AutoPostBack="True"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgLocation" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Misc/Route.png"
                    OnClientClick="getLOCATION();return false;" TabIndex="30" />&nbsp;
                <asp:CheckBox ID="chkWeekly" runat="server" Text="Weekly" Visible="False" /></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Amount</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtHAmount" runat="server" AutoCompleteType="Disabled"
                                AutoPostBack="True"></asp:TextBox>
                            <asp:CheckBox ID="chkRefundable" runat="server" Text="Refundable" />
                            <asp:LinkButton ID="lbViewHistory" runat="server" OnClientClick="Showdata(1); return false;">View History</asp:LinkButton>
                            <asp:Label ID="lblChargeSchedule" runat="server" SkinID="LabelError" Visible="False"></asp:Label></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr style="display: none;">
                        <td colspan="4" class="title-bg" align="left">Late Fee Charges</td>
                    </tr>
                    <tr style="display: none;">
                        <td align="left">
                            <span class="field-label">Charge Type</span></td>
                        <td align="left" colspan="3">
                            <asp:RadioButton ID="rbPercentage" runat="server" CssClass="field-label" GroupName="type" Text="Percentage" />
                            <asp:RadioButton ID="rbAmount" runat="server" CssClass="field-label" GroupName="type" Text="Amount" Checked="true" /></td>
                    </tr>
                    <tr style="display: none;">
                        <td align="left">
                            <span class="field-label">Late Fee Amount/Percentage</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtLateAmount" runat="server" AutoCompleteType="Disabled">0</asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtLateAmount"
                                CssClass="error" ErrorMessage="Please enter late fee Amount/Percentage" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                        <td align="left">
                            <span class="field-label">Fee Late Days</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtLatefeedays" runat="server" Width="50px" AutoCompleteType="Disabled">0</asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtLatefeedays"
                                CssClass="error" ErrorMessage="Please Enter late fee days" ValidationGroup="main">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr id="tr_DetailViewDetailHeader" runat="server">
                        <td colspan="4" align="left" class="ui-slider-horizontal">
                            <span class="field-label">Fee Details</span></td>
                    </tr>
                    <tr id="tr_DetailViewDetails" runat="server">
                        <td align="left" colspan="4">
                            <asp:GridView ID="gvFeeSetup" runat="server" CssClass="table table-bordered table-row" Width="100%" AutoGenerateColumns="False"
                                EmptyDataText="No Details Added">
                                <Columns>
                                    <asp:BoundField DataField="Fdate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="From Date"
                                        HtmlEncode="False" ReadOnly="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="tdate" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="To Date"
                                        HtmlEncode="False" ReadOnly="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Acd_year" HeaderText="Academic Year" ReadOnly="True">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Amount" DataFormatString="{0:0.00}" HeaderText="Amount"
                                        ReadOnly="True">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Fee_descr" HeaderText="Fee" ReadOnly="True">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField ApplyFormatInEditMode="True" DataField="Grd_id" HeaderText="Grade"
                                        ReadOnly="True">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SCH_DESCR" HeaderText="Plan" ReadOnly="True">
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:CommandField ShowDeleteButton="True" Visible="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:CommandField>
                                    <asp:TemplateField HeaderText="View" ShowHeader="False" Visible="False">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblMessage" runat="server" Text="Viewing" EnableViewState="False"></asp:Label>
                                            <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" CausesValidation="True"
                                                Visible="False" OnClientClick="alert('Editing is currently disabled');return false;"
                                                CommandName="Update">
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" CausesValidation="False"
                                                Visible="False" CommandName="Cancel"></asp:LinkButton>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" Text="View" CausesValidation="False"
                                                CommandName="Edit"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="return (confirm('The Record will be Permanently Deleted. Do You Want to Continue?'));"
                                                CommandName="Delete">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:GridView ID="gvMonthly" Width="100%" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="Term/Month" ReadOnly="True" InsertVisible="False"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmount" onfocus="this.select();" runat="server" Text='<%# Bind("FDD_AMOUNT") %>'
                                                onblur="return CheckAmount(this);" AutoCompleteType="Disabled" Width="102px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Charge Dt.">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtChargeDate" runat="server" Text='<%# Bind("FDD_DATE" ) %>' Width="100px"></asp:TextBox>
                                            <asp:ImageButton ID="imgChargeDate" TabIndex="4" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalChargeDate" runat="server" PopupPosition="TopLeft"
                                                PopupButtonID="imgChargeDate" TargetControlID="txtChargeDate" CssClass="MyCalendar"
                                                Format="dd/MMM/yyyy">
                                            </ajaxToolkit:CalendarExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Interest/Discount">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtOtherAmount" onfocus="this.select();" runat="server" Text='<%# Bind("FDD_OTHCHARGE") %>'
                                                onblur="return CheckAmount(this);" AutoCompleteType="Disabled" Width="102px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Due Dt.">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDueDate" runat="server" Text='<%# Bind("FDD_DUEDT" ) %>'></asp:TextBox>
                                            <asp:ImageButton ID="imgFirstMemoDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                TabIndex="4" />
                                            <ajaxToolkit:CalendarExtender ID="CalFirstMemoDate" runat="server" PopupPosition="TopLeft"
                                                PopupButtonID="imgFirstMemoDate" TargetControlID="txtDueDate" CssClass="MyCalendar"
                                                Format="dd/MMM/yyyy">
                                            </ajaxToolkit:CalendarExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" /><asp:Button
                                ID="btnEdit" runat="server" CssClass="button" Text="Edit" CausesValidation="False" /><asp:Button
                                    ID="btnSave" runat="server" CssClass="button" Text="Save" /><asp:Button ID="btnDelete"
                                        runat="server" CssClass="button" Text="Delete" CausesValidation="False" /><asp:Button
                                            ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                    PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="txtFrom"
                    TargetControlID="txtFrom" CssClass="MyCalendar" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <asp:ObjectDataSource ID="odsGetBSUAcademicYear" runat="server" SelectMethod="GetBSUAcademicYear"
                    TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:SessionParameter Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsGetSCHEDULE_M" runat="server" SelectMethod="GetSCHEDULE_M"
                    TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="false" Name="bisCollection" Type="Boolean"></asp:Parameter>
                    </SelectParameters>
                </asp:ObjectDataSource>

                <asp:ObjectDataSource ID="odsGetSCHEDULE_M_NEW" runat="server" SelectMethod="GetSCHEDULE_M_NEW"
                    TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="false" Name="bisCollection" Type="Boolean"></asp:Parameter>
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsGetFEETYPE_M" runat="server" SelectMethod="GetFEES_M"
                    TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:SessionParameter Name="BSU_ID" SessionField="sBsuid" Type="String" />
                        <asp:ControlParameter ControlID="ddlAcademic" Name="ACD_ID" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsGetGRADE_M" runat="server" SelectMethod="GetGRADE_M"
                    TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:SessionParameter Name="BSU_ID" SessionField="sbsuid" Type="String" />
                        <asp:ControlParameter ControlID="ddlAcademic" Name="ACD_ID" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsStream_m" runat="server" SelectMethod="GetSTREAM_M"
                    TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sbsuid" Type="String" />
                        <asp:ControlParameter ControlID="ddlAcademic" DefaultValue="" Name="ACD_ID" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:SqlDataSource ID="SqlProrata" runat="server" ConnectionString="<%$ ConnectionStrings:OASIS_FEESConnectionString %>"
                    SelectCommand="SELECT       FPM_ID, FPM_DESCR  &#13;&#10;FROM         FEES.FEES_PRORATA_M where FPM_Bactive=1&#13;&#10;"></asp:SqlDataSource>
                <asp:HiddenField ID="H_Location" runat="server" />
                <asp:HiddenField ID="hf_PRO_ID" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>

</asp:Content>
