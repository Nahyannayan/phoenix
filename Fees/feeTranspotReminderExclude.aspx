<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="feeTranspotReminderExclude.aspx.vb" Inherits="Fees_feeTranspotReminderExclude" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
function CheckForPrint() {
    if (document.getElementById('<%= h_print.ClientID %>').value != '') {
        document.getElementById('<%= h_print.ClientID %>').value = '';
           showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
       }
}
    );


   function ChangeCheckBoxState(id, checkState) {
       var cb = document.getElementById(id);
       if (cb != null)
           cb.checked = checkState;
   }

   function GetStudent() {
       var sFeatures;
       var sFeatures;
       sFeatures = "dialogWidth: 875px; ";
       sFeatures += "dialogHeight: 600px; ";
       sFeatures += "help: no; ";
       sFeatures += "resizable: no; ";
       sFeatures += "scroll: yes; ";
       sFeatures += "status: no; ";
       sFeatures += "unadorned: no; ";
       var NameandCode;
       var result;
       var url;
       url = "ShowStudentMultiTransport.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu=" + document.getElementById('<%= ddlBusinessunit.ClientID %>').value;
       <%-- result = window.showModalDialog(url,"", sFeatures);
        if(result != '' && result != undefined)
         {        
            document.getElementById('<%=txtStudName.ClientID %>').value='Multiple Students selected'; 
            document.getElementById('<%=h_STUD_ID.ClientID %>').value=result;
         }
        return true; 
      }  --%>

          var url = "ShowStudentMulti.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu=" + document.getElementById('<%=ddlBusinessunit.ClientID%>').value;
          var oWnd = radopen(url, "pop_getstudent");

      }

      function OnClientClose1(oWnd, args) {
          //get the transferred arguments

          var arg = args.get_argument();

          if (arg) {

              NameandCode = arg.NameandCode.split('||');
              document.getElementById('<%=txtStudName.ClientID %>').value = 'Multiple Students selected';
              document.getElementById('<%=h_STUD_ID.ClientID %>').value = arg.NameandCode;
              __doPostBack('<%=txtStudName.ClientID%>', 'TextChanged');
          }
      }



      function autoSizeWithCalendar(oWindow) {
          var iframe = oWindow.get_contentFrame();
          var body = iframe.contentWindow.document.body;

          var height = body.scrollHeight;
          var width = body.scrollWidth;

          var iframeBounds = $telerik.getBounds(iframe);
          var heightDelta = height - iframeBounds.height;
          var widthDelta = width - iframeBounds.width;

          if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
          if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
          oWindow.center();
      }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getstudent" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Reminder Exclusions
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />

                <asp:Label ID="lblError" runat="server" SkinID="LabelError" CssClass="error"></asp:Label>
                <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                <table align="center" width="100%">

                    <tr valign="top">
                        <td align="left" width="20%"><span class="field-label">Select Business Unit</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True" DataSourceID="odsSERVICES_BSU_M"
                                DataTextField="BSU_NAME" DataValueField="SVB_BSU_ID" SkinID="DropDownListNormal"
                                TabIndex="5" OnSelectedIndexChanged="ddlBusinessunit_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>

                    <tr valign="top">
                        <td align="left"><span class="field-label">Exclude From Date</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>&nbsp<asp:ImageButton ID="ImgDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDate"
                                    ErrorMessage="Please Select Date" ValidationGroup="MAINERROR" Height="9px" Width="2px">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left"><span class="field-label">To Date</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAsOnDate" runat="server" AutoPostBack="True"></asp:TextBox>&nbsp<asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAsOnDate"
                                    ErrorMessage="Please Select Date" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr valign="top">
                        <td align="left"><span class="field-label">Select students</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtStudName" runat="server" SkinID="TextBoxNormal"></asp:TextBox>&nbsp;<asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetStudent(); return false;"></asp:ImageButton></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvFEEReminder" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30" SkinID="GridViewView" OnSelectedIndexChanged="gvFEEReminder_SelectedIndexChanged">
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="FCH_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("STU_ID") %>' __designer:wfdid="w13"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student#">
                                        <HeaderTemplate>
                                            Student #<br />
                                                            <asp:TextBox ID="txtStuno" runat="server" SkinID="Gridtxt" Width="75%" __designer:wfdid="w21"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchno" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w22"></asp:ImageButton>
                                                                
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("STU_NO") %>' __designer:wfdid="w20"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Student Name<br />
                                            <asp:TextBox ID="txtStuName" runat="server" SkinID="Gridtxt" Width="75%" __designer:wfdid="w18"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchname" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w19"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STU_NAME") %>' __designer:wfdid="w17"></asp:Label><br />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                           Grade #<br />
                                           <asp:TextBox ID="txtGrade" runat="server" SkinID="Gridtxt" Width="75%" __designer:wfdid="w24"></asp:TextBox>
                                           <asp:ImageButton ID="btnSearchGrade" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w25"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRD_DISPLAY") %>' __designer:wfdid="w23"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Parent Name">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            ParentName<br />
                                            <asp:TextBox ID="txtParent" runat="server" SkinID="Gridtxt" Width="75%" __designer:wfdid="w27"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchPName" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w28"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblParentName" runat="server" Text='<%# Bind("PARENT_NAME") %>' __designer:wfdid="w26"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Parent Mob No.">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            Parent Mob No.<br />
                                            <asp:TextBox ID="txtParentMobNo" runat="server" Width="75%" __designer:wfdid="w30"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchMobNo" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" __designer:wfdid="w31"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblParentMobNo" runat="server" Text='<%# BIND("PARENT_MOBILE") %>' __designer:wfdid="w29"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Parent Company"></asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="5" style="text-align: left" valign="top"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="5" style="text-align: left" valign="top"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="5" valign="top">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_Selected_Value" runat="server" type="hidden" value="=" />
                <ajaxToolkit:CalendarExtender
                    ID="calFromDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate"
                    TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender
                    ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImgDate"
                    TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <asp:Panel ID="Panel1" runat="server">
                </asp:Panel>
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_STUD_ID" runat="server" />

                </div>
            </div>
        </div>

</asp:Content>

