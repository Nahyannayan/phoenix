<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FEEConcessiontransView.aspx.vb" Inherits="FEEConcessiontransView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">



    <script language="javascript" type="text/javascript">
        function fnVoucherMSg() {
            if (document.getElementById('<%=chkPrint.ClientID %>').checked == true) {
                var curr_elem;
                var countChecked;
                countChecked = 0;
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    curr_elem = document.forms[0].elements[i];
                    if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/chkPrint/) > 0) && (curr_elem.name != '')) {
                        if (curr_elem.checked)
                            countChecked = countChecked + 1;
                    }
                }
                if (countChecked > 1)
                    return confirm('Only first voucher will be printed(Multiple vouchers are selected)');
                else
                    return true;
            }
        }

      <%--  Sys.Application.add_load(
               function CheckForPrint() {
                   if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                     document.getElementById('<%= h_print.ClientID %>').value = '';
                     //showModelessDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                     var url = "../Reports/ASPX Report/RptViewerModal.aspx";
                     var oWnd = radopen(url, "pop_appaddload");
                 }
             }
                    );--%>


         Sys.Application.add_load(
    function CheckForPrint() {
        var url = '../Reports/ASPX Report/RptViewerModal.aspx';
        if (document.getElementById('<%= h_print.ClientID %>').value != '') {
            document.getElementById('<%= h_print.ClientID %>').value = '';           
            $.fancybox({
                width: '850px',
                height: '700px',
                dialogWidth: '850px',
                dialogHeight: '700px',
                transitionIn: 'fade',
                transitionOut: 'fade',
                type: 'iframe',
                autoSize: false,
                fitToView: true,
                href: url
            });
            return false;
        }
    }
                    );


        


             function fnSelectAll(master_box) {
                 var curr_elem;
                 var checkbox_checked_status;
                 for (var i = 0; i < document.forms[0].elements.length; i++) {
                     curr_elem = document.forms[0].elements[i];
                     if ((curr_elem.type == 'checkbox') && !(curr_elem.name.search(/chkPrint/) > 0)) {
                         curr_elem.checked = !master_box.checked;
                     }
                 }
                 master_box.checked = !master_box.checked;
             }



             function autoSizeWithCalendar(oWindow) {
                 var iframe = oWindow.get_contentFrame();
                 var body = iframe.contentWindow.document.body;

                 var height = body.scrollHeight;
                 var width = body.scrollWidth;

                 var iframeBounds = $telerik.getBounds(iframe);
                 var heightDelta = height - iframeBounds.height;
                 var widthDelta = width - iframeBounds.width;

                 if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                 if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                 oWindow.center();
             }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_appaddload" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="720px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr valign="top">
                        <td valign="top" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <%--<asp:Label ID="lblError" runat="server"></asp:Label>--%>
                            <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" width="100%">
                    <tr>
                        <td align="left" width="15%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList></td>
                        <td align="left" width="10%">
                            <asp:Label ID="lblDate" runat="server" Visible="False"><span class="field-label">Date</span></asp:Label></td>
                        <td align="left" width="20%">
                            <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True" Width="75%" Visible="False"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4"
                                Visible="False" />
                        </td>
                        <td align="right" width="25%">
                            <asp:RadioButton ID="rbUnposted" runat="server" AutoPostBack="True" Checked="True"
                                CssClass="radiobutton" GroupName="POST" Text="Open" OnCheckedChanged="rbUnposted_CheckedChanged" />
                            <asp:RadioButton ID="rbPosted" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Text="Posted" OnCheckedChanged="rbPosted_CheckedChanged" />
                            <asp:RadioButton ID="rbtRejected" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Text="Rejected" OnCheckedChanged="rbtRejected_CheckedChanged" />
                            <asp:RadioButton ID="rbAll" runat="server" AutoPostBack="True" CssClass="radiobutton"
                                GroupName="POST" Height="16px" Text="All" OnCheckedChanged="rbAll_CheckedChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" width="100%" colspan="6">
                            <asp:GridView ID="gvFEEConcessionDet" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30" CssClass="table table-bordered table-row">
                                <Columns>
                                     <asp:TemplateField HeaderText="Post" Visible="False">
                                        <HeaderTemplate>
                                            Select All
                                            <input id="chkSelectall" type="checkbox" onclick="fnSelectAll(this)" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkPost" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FCH_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFCH_ID" runat="server" Text='<%# Bind("FCH_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student#">
                                        <HeaderTemplate>
                                            Student ID<br />
                                            <asp:TextBox ID="txtStuno" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchno" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Student Name<br />
                                            <asp:TextBox ID="txtStuname" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearchd" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Concession">
                                        <HeaderTemplate>
                                            Concession<br />
                                            <asp:TextBox ID="txtConcession" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnConcession" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("FCM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                         <HeaderTemplate>
                                            Ref. Details<br />
                                            <asp:TextBox ID="txtRefHEAD" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnRefHEAD" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                         <ItemTemplate>
                                            <asp:Label ID="lblRefDet" runat="server" Text='<%# Bind("FCH_REF_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date<br />
                                            <asp:TextBox ID="txtFrom" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnFrom" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("FCH_DT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Period">
                                        <HeaderTemplate>
                                            Period<br />
                                            <asp:TextBox ID="txtPeriod" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnNarration" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPeriod" runat="server" Text='<%# Bind("PERIOD")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderTemplate>
                                            Narration<br />
                                            <asp:TextBox ID="txtRemarks" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("FCH_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="AMOUNT" HeaderText="Amount"></asp:BoundField>
                                    <asp:BoundField DataField="FCH_RECNO" HeaderText="DocNo"></asp:BoundField>
                                    <asp:BoundField DataField="CANCEL_DOCNO" HeaderText="Cancelled" Visible="false"></asp:BoundField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>     
                                    <asp:TemplateField HeaderText="Print">
                                        <ItemTemplate>
                                           <asp:LinkButton ID="hlPrint" runat="server" OnClick="hlPrint_Click"> Print</asp:LinkButton>                                           
                                        </ItemTemplate>
                                         <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>                              
                                    <asp:BoundField DataField="FCH_STATUS" HeaderText="Status" Visible="false" />
                                </Columns>
                            </asp:GridView>
                            <asp:CheckBox ID="chkPrint" runat="server" Checked="True"  Text="Print Voucher"
                                Visible="False"></asp:CheckBox>
                            <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Approve" Visible="False"
                                OnClientClick="return fnVoucherMSg();" />
                            <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" Visible="False" OnClientClick="return fnVoucherMSg();"
                                OnClick="btnReject_Click" />
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                            <asp:HiddenField ID="h_print" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>

    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
        runat="server" type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server"
            type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden"
                value="=" /><input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
</asp:Content>
