﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DrillDownPage.aspx.vb" Inherits="DrillDownPage" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="/PHOENIXBETA/vendor/jquery/jquery.min.js"></script>
<script src="/PHOENIXBETA/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/PHOENIXBETA/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Bootstrap core CSS-->
<link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/>
<!-- Custom fonts for this template-->
<link href="/PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<!-- Page level plugin CSS-->
<link href="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/PHOENIXBETA/cssfiles/sb-admin.css" rel="stylesheet"/>
<script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
<script type="text/javascript" src="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
<link type="text/css" href="/PHOENIXBETA/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
   <style>
        .k-chart, .k-gauge, .k-sparkline, .k-stockchart, .k-chart text {
            font-family: Raleway, sans-serif !important;
        }
        .Label1
		{
			position: absolute;
			font-weight: bold;
			font-size: 12px;
			top: 70%;
			left: 47%;
		}

.pad-all {
    padding: 15px;
}


   
.panel-default.panel-colorful {
    background-color: #e6eced;
    color: #7a878e;
}

.panel-default .panel-heading {
    background-color: #e6eced;
    border-color: #f5f7f8;
}

.panel-footer {
    background-color: #fdfdfe;
    color: #7a878e;
    border-color: rgba(0,0,0,0.02);
    position: relative;
}

.panel-primary .panel-heading, .panel-primary .panel-footer, .panel-primary.panel-colorful {
    background-color: #25476a;
    border-color: #25476a;
    color: #fff;
}

.panel-primary.panel-colorful {
    box-shadow: 0 1px 1px #0b141e;
}

.panel-info .panel-heading, .panel-info .panel-footer, .panel-info.panel-colorful {
    background-color: #03a9f4;
    border-color: #03a9f4;
    color: #fff;
}

.panel-info.panel-colorful {
    box-shadow: 0 1px 1px #02638f;
}

.panel-success .panel-heading, .panel-success .panel-footer, .panel-success.panel-colorful {
    background-color: #8bc34a;
    border-color: #8bc34a;
    color: #fff;
}

.panel-success.panel-colorful {
    box-shadow: 0 1px 1px #577d2a;
}

.panel-warning .panel-heading, .panel-warning .panel-footer, .panel-warning.panel-colorful {
    background-color: #ffb300;
    border-color: #ffb300;
    color: #fff;
}

.panel-warning.panel-colorful {
    box-shadow: 0 1px 1px #996b00;
}

.panel-danger .panel-heading, .panel-danger .panel-footer, .panel-danger.panel-colorful {
    background-color: #f44336;
    border-color: #f44336;
    color: #fff;
}

.panel-danger.panel-colorful {
    box-shadow: 0 1px 1px #ba160a;
}

.panel-mint .panel-heading, .panel-mint .panel-footer, .panel-mint.panel-colorful {
    background-color: #26a69a;
    border-color: #26a69a;
    color: #fff;
}

.panel-mint.panel-colorful {
    box-shadow: 0 1px 1px #13534d;
}

.panel-purple .panel-heading, .panel-purple .panel-footer, .panel-purple.panel-colorful {
    background-color: #ab47bc;
    border-color: #ab47bc;
    color: #fff;
}

.panel-purple.panel-colorful {
    box-shadow: 0 1px 1px #682a73;
}

.panel-pink .panel-heading, .panel-pink .panel-footer, .panel-pink.panel-colorful {
    background-color: #f06292;
    border-color: #f06292;
    color: #fff;
}

.panel-pink.panel-colorful {
    box-shadow: 0 1px 1px #d71556;
}

.panel-dark .panel-heading, .panel-dark .panel-footer, .panel-dark.panel-colorful {
    background-color: #3a444e;
    border-color: #3a444e;
    color: #fff;
}

.panel-dark.panel-colorful {
    box-shadow: 0 1px 1px #0f1114;
}

.panel-colorful > .panel-heading {
    border: 0;
}



.panel-bordered-default, .panel-default.panel-bordered {
    border: 1px solid #bdcccf;
}

.panel-bordered-primary, .panel-primary.panel-bordered {
    border: 1px solid #25476a;
}

.panel-bordered-info, .panel-info.panel-bordered {
    border: 1px solid #03a9f4;
}

.panel-bordered-success, .panel-success.panel-bordered {
    border: 1px solid #8bc34a;
}

.panel-bordered-warning, .panel-warning.panel-bordered {
    border: 1px solid #ffb300;
}

.panel-bordered-danger, .panel-danger.panel-bordered {
    border: 1px solid #f44336;
}

.panel-bordered-mint, .panel-mint.panel-bordered {
    border: 1px solid #26a69a;
}

.panel-bordered-purple, .panel-purple.panel-bordered {
    border: 1px solid #ab47bc;
}

.panel-bordered-pink, .panel-pink.panel-bordered {
    border: 1px solid #f06292;
}

.panel-bordered-dark, .panel-dark.panel-bordered {
    border: 1px solid #3a444e;
}







    </style>
    
</head>
<body>
   <script type="text/javascript">
        function divBar_Click()
        {
            document.getElementById('<%= btnTest.ClientID%>').click();
        }
    </script>

    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" >
        </ajaxToolkit:ToolkitScriptManager>
         <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <%--<AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadHtmlChart1" LoadingPanelID="LoadingPanel1"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
            </AjaxSettings>--%>
        </telerik:RadAjaxManager>
        <%--<telerik:RadAjaxLoadingPanel ID="LoadingPanel1" Height="77px" Width="113px" runat="server">
        </telerik:RadAjaxLoadingPanel>--%>


        <asp:Button id="btnTest" runat="server" style="display:none;"/>
    <section class="col-md-12 col-lg-12 col-sm-12 colContainerBox">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="form-group">
                <div class="row">
                      <div class="col-md-12 col-lg-12 col-sm-12" style="padding-bottom: 2%;">
                    <%--<asp:Repeater ID="rptDashboard" runat="server">
                        <ItemTemplate>--%>
                          
                                <div class="card" style=" box-shadow: 4px 4px 10px rgba(0,0,0,0.16);width:100%;">
                                        <div id="divBar" runat="server"  onclick="divBar_Click()">
                                         <div class="card-header card-dash"><asp:Label id="lblBar" runat="server" CssClass="card-dash-title"></asp:Label>
                                  </div>
                                        <Telerik:RadHtmlChart runat="server" ID="RadHtmlChart1"  Skin="Metro" width="900px" OnClientSeriesClicked="OnClientSeriesClicked">
                                            <PlotArea>
                                                <Series>
                                                    <Telerik:ColumnSeries Name="X" DataFieldY="Y">
                                                        <TooltipsAppearance  >
                                                            <ClientTemplate>#=dataItem.Z# </ClientTemplate>
                                                            </TooltipsAppearance>
                                                        <LabelsAppearance Visible="true" />
                                                    </Telerik:ColumnSeries>
                                                </Series>
                                                <XAxis DataLabelsField="X">
                                                     <MajorGridLines Visible="false" />
                                                     <MinorGridLines Visible="false"/>
                                                </XAxis>
                                                <YAxis>
                                                    <MajorGridLines Visible="false" />
                                                    <MinorGridLines Visible="false" />
                                                    <LabelsAppearance  />
                                                </YAxis>
                                            </PlotArea>
                                            <Legend>
                                                <Appearance Visible="false" />
                                            </Legend>
                                           
                                        </Telerik:RadHtmlChart>
                                    </div>
                                    <div id="divPie" runat="server" onclick="divBar_Click()">
                                        <div class="card-header card-dash"><asp:Label id="lblPie" runat="server" CssClass="card-dash-title"></asp:Label>
                                  </div>
                                        <Telerik:RadHtmlChart runat="server" ID="RadHtmlChart2" Height="250" Skin="Metro">
                                            <PlotArea>
                                                <Series>
                                                    <Telerik:PieSeries DataFieldY="Y" NameField="X">
                                                        <LabelsAppearance >
                                                        </LabelsAppearance>
                                                        <TooltipsAppearance>
                                                            <ClientTemplate>#=dataItem.X# </ClientTemplate>
                                                        </TooltipsAppearance>
                                                    </Telerik:PieSeries>
                                                </Series>
                                                <YAxis>
                                                </YAxis>
                                            </PlotArea>
                                            <Legend>

                                                <Appearance Position="Right" Visible="true" />
                                            </Legend>
                                           
                                        </Telerik:RadHtmlChart>
                                    </div>
                                    <div id="divDonut" runat="server" onclick="divBar_Click()">
                                        <div class="card-header card-dash"><asp:Label id="lblDonut" runat="server" CssClass="card-dash-title"></asp:Label>
                                 </div>
                                        <Telerik:RadHtmlChart runat="server" ID="RadHtmlChart3" Height="250" Skin="Metro">
                                            <PlotArea>
                                                <Series>
                                                    <Telerik:DonutSeries DataFieldY="Y" NameField="X">
                                                        <LabelsAppearance >
                                                        </LabelsAppearance>
                                                        <TooltipsAppearance>
                                                            <ClientTemplate>#=dataItem.X# </ClientTemplate>
                                                        </TooltipsAppearance>
                                                    </Telerik:DonutSeries>
                                                </Series>
                                                <YAxis>
                                                </YAxis>
                                            </PlotArea>
                                            <Legend>
                                                <Appearance Position="Right" Visible="true" />
                                            </Legend>
                                           
                                        </Telerik:RadHtmlChart>
                                    </div>

                                    <div id="divGauge" runat="server" onclick="divBar_Click()">
                                        <div class="card-header card-dash"><asp:Label id="lblGauge" runat="server" CssClass="card-dash-title"></asp:Label>
                                  </div>
                                       
                                        <Telerik:RadRadialGauge runat="server" ID="RadRadialGauge1" Height="250px">
                                            <Pointer Value="50.2" >
                                                <Cap Size="0.1"  />
                                            </Pointer>
                                            <Scale Min="0" Max="100" MajorUnit="20">
                                                <Labels Format="{0}" />
                                                <Ranges>
                                                    <Telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                                    <Telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                                    <Telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                                    <Telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />

                                                </Ranges>
                                            </Scale>
                                        </Telerik:RadRadialGauge>
                                         <asp:Label ID="lblGaugeVal" Text="97" runat="server" CssClass="Label1" />
                                    </div>
                                    <div id="divSeries" runat="server" onclick="divBar_Click()">
                                         <div class="card-header card-dash"><asp:Label id="lblSeries" runat="server" CssClass="card-dash-title"></asp:Label>
                                  </div>
                                        <Telerik:RadHtmlChart runat="server" ID="RadHtmlChart4" Height="250" >
                                            <PlotArea>
                                                <Series>
                                                    <Telerik:ColumnSeries DataFieldY="Y1" >

                                                  
                                                         <TooltipsAppearance Visible="true">
                                                             <ClientTemplate>#=dataItem.NAME1# </ClientTemplate>
                                                        </TooltipsAppearance>
                                                       
                                                        <LabelsAppearance Visible="true" > <ClientTemplate>#=dataItem.Y1# </ClientTemplate></LabelsAppearance>
                                                       
                                                    </Telerik:ColumnSeries>

                                                    <Telerik:ColumnSeries DataFieldY="Y2" Name="" >
                                                        
                                                         <TooltipsAppearance Visible="true">
                                                             <ClientTemplate>#=dataItem.NAME2# </ClientTemplate>
                                                        </TooltipsAppearance>

                                                         <LabelsAppearance Visible="true" > <ClientTemplate>#=dataItem.Y2# </ClientTemplate></LabelsAppearance>

                                                    </Telerik:ColumnSeries>
                                                    <Telerik:ColumnSeries DataFieldY="Y3" Name="">
                                                       
                                                       <TooltipsAppearance Visible="true">
                                                             <ClientTemplate>#=dataItem.NAME3# </ClientTemplate>
                                                        </TooltipsAppearance>

                                                        <LabelsAppearance Visible="true" > <ClientTemplate>#=dataItem.Y3# </ClientTemplate></LabelsAppearance>

                                                    </Telerik:ColumnSeries>
                                                </Series>
                                                <XAxis DataLabelsField="X">
                                                    <MajorGridLines Visible="false" />
                                                    <MinorGridLines Visible="false" />
                                                </XAxis>
                                                <YAxis>
                                                    <MajorGridLines Visible="false" />
                                                    <MinorGridLines Visible="false" />
                                                </YAxis>
                                            </PlotArea>
                                            <Legend>
                                                <Appearance Position="Bottom">
                                                </Appearance>
                                            </Legend>
                                            
                                        </Telerik:RadHtmlChart>
                                    </div>
                                    <div id="divLine" runat="server" onclick="divBar_Click()">
                                         <div class="card-header card-dash"><asp:Label id="lblLine" runat="server" CssClass="card-dash-title"></asp:Label>
                                  </div>
                                        <Telerik:RadHtmlChart runat="server" ID="RadHtmlChart5" Height="250px" >
                                            <PlotArea>
                                                <Series>
                                                    <Telerik:LineSeries DataFieldY="Y1" Name="2016">

                                                        <LabelsAppearance Visible="false">
                                                        </LabelsAppearance>
                                                    </Telerik:LineSeries>
                                                    <Telerik:LineSeries DataFieldY="Y2" Name="2017">

                                                        <LabelsAppearance Visible="false">
                                                        </LabelsAppearance>
                                                    </Telerik:LineSeries>
                                                    <Telerik:LineSeries DataFieldY="Y3" Name="2018">

                                                        <LabelsAppearance Visible="false">
                                                        </LabelsAppearance>
                                                    </Telerik:LineSeries>
                                                </Series>
                                                <XAxis DataLabelsField="X">
                                                    <MajorGridLines Visible="false" />
                                                    <MinorGridLines Visible="false" />
                                                </XAxis>
                                                <YAxis>
                                                    <MajorGridLines Visible="false" />
                                                    <MinorGridLines Visible="false" />
                                                </YAxis>
                                            </PlotArea>
                                            
                                            
                                        </Telerik:RadHtmlChart>
                                    </div>
                                    <div id="divStacked" runat="server"  onclick="divBar_Click()">
                                         <div class="card-header card-dash"><asp:Label id="lblStacked" runat="server" CssClass="card-dash-title"></asp:Label>
                                  </div>
                                        <Telerik:RadHtmlChart runat="server" ID="RadHtmlChart6"  Skin="Metro" width="1200px" OnClientSeriesClicked="OnClientSeriesClicked" >
                                            <PlotArea>
                                                <Series>
                                                    <Telerik:ColumnSeries  DataFieldY="Y1"  Stacked="true" Name="">
                                                        <LabelsAppearance Visible="true" Position="Center" >  <ClientTemplate>#=dataItem.Y1# </ClientTemplate></LabelsAppearance>
                                                        <TooltipsAppearance Visible="true">
                                                             <ClientTemplate>#=dataItem.X1# </ClientTemplate>
                                                        </TooltipsAppearance>
                                                    </Telerik:ColumnSeries>
                                                    <Telerik:ColumnSeries DataFieldY="Y2" Stacked="true"  Name="">
                                                        <LabelsAppearance Visible="true" Position="Center"> <ClientTemplate>#=dataItem.Y2# </ClientTemplate></LabelsAppearance>
                                                        <TooltipsAppearance Visible="true">
                                                             <ClientTemplate>#=dataItem.X2# </ClientTemplate>
                                                        </TooltipsAppearance>
                                                    </Telerik:ColumnSeries>

                                                </Series>
                                                <XAxis DataLabelsField="X">
                                                    <MajorGridLines Visible="false" />
                                                    <MinorGridLines Visible="false" />
                                                    <LabelsAppearance Visible="true"></LabelsAppearance>
                                                </XAxis>
                                                <YAxis>
                                                    <MajorGridLines Visible="false" />
                                                    <MinorGridLines Visible="false" />
                                                </YAxis>
                                            </PlotArea>
                                            <Legend>
                                                <Appearance Position="Bottom" Visible="true" />
                                            </Legend>
                                           
                                        </Telerik:RadHtmlChart>
                                    </div>
                                    <div id="divRepeater" runat="server" onclick="divBar_Click()">
                                        <div class="card-header card-dash"><asp:Label id="lblRepeater" runat="server" CssClass="card-dash-title"></asp:Label>
                                  </div>
                                        <div class="row">
                                       <asp:Repeater ID="rptRepeater" runat="server">
                                           <ItemTemplate>
                                       <div class="col-sm-2">
					
					               
					                <div class='<%# Eval("CSS")%>'>
					                    <div class="pad-all text-center">
					                        <span class="text-3x text-thin"><%# Eval("X")%></span>
					                        <p><%# Eval("Y")%></p>
					                        
					                    </div>
					                </div>
					               </div>
                                           </ItemTemplate>
                                       </asp:Repeater>
                                            </div>
                                    </div>
                                    <div id="divGrid" runat="server" onclick="divBar_Click()">
                                        <div class="card-header card-dash"><asp:Label id="lblGrid" runat="server" CssClass="card-dash-title"></asp:Label>
                                  </div>
                                       <telerik:RadGrid RenderMode="Lightweight" ID="RadGrid1" runat="server" AllowPaging="false" CellSpacing="0"
                            PageSize="5" GridLines="Both" CssClass="table table-bordered table-row" Width="100%" >
                            <MasterTableView>
                            </MasterTableView>
                            <HeaderStyle BackColor="#F5F5F5"   HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle  HorizontalAlign="Center"  />
                            <AlternatingItemStyle  HorizontalAlign="Center" />
                            <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                        </telerik:RadGrid>
                                    </div>
                                    <div id="divPage" runat="server" onclick="divBar_Click()" style="width:100%;height:800px;" >
                                        <iframe id="ifPage" runat="server" width="100%" height="800px"></iframe>
                                       
                                    </div>
                                </div>
                            
                            <asp:HiddenField ID="hdnID" runat="server"  />
                            <asp:HiddenField ID="hdnType" runat="server"  />
                            <asp:HiddenField ID="hdnTitle" runat="server"  />
                            <asp:HiddenField ID="hdndrilldown" runat="server"  />
                            <asp:HiddenField ID="hdnPage" runat="server"  />
                           <asp:HiddenField ID="hdnNext" runat="server"  />
                          <asp:HiddenField ID="hdParams" runat="server"  />
                            <asp:HiddenField ID="hdNextParam" runat="server"  />
                          <asp:HiddenField ID="hdnURL" runat="server"  />
                        <%--</ItemTemplate>

                    </asp:Repeater>--%>
                    </div>

                    </div>
                <div class="row">

                                <asp:Button id="btnBack" runat="server" Text="Back" CssClass="button float-right"/>
                            </div>
                </div>
            </div>
        </section>
    </form>
</body>
     <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript">
                function OnClientSeriesClicked(sender, args) {
                   
                   // alert($find("<%= RadAjaxManager1.ClientID%>").ajaxRequest(ControlID));
                    $find("<%= RadAjaxManager1.ClientID%>").ajaxRequest(args.get_category());
                    // if (args.get_seriesName() != "Months")
                    // $find("<%= RadAjaxManager1.ClientID%>").ajaxRequest(args.get_category());
             }
            </script>
        </telerik:RadCodeBlock>
</html>
