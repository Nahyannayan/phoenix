<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master"  CodeFile="senStudentProfile.aspx.vb" Inherits="SenRegister_senStudentProfile" %>

<%@ Register Src="UserControls/senMonitoring.ascx" TagName="senMonitoring" TagPrefix="uc29" %>

<%@ Register Src="UserControls/senStudentBasicInformation.ascx" TagName="senStudentBasicInformation" TagPrefix="uc2" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="UserControls/senStudentProfile.ascx" TagName="senStudentProfile"TagPrefix="uc1" %>
   
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage"  runat="server">
   
     
   <script type="text/javascript" >
   
        window.setTimeout('setpath()',100);
        
        function setpath()
        {
           var path = window.location.href
           var Rpath=path.substring(path.indexOf('?'),path.length)
           var objFrame
           //alert(Rpath);
           //Access
           objFrame=document.getElementById("F3"); 
           objFrame.src="Pages/senAccess.aspx" + Rpath
           
           //Ace
           objFrame=document.getElementById("F4"); 
           objFrame.src="Pages/senACe.aspx" + Rpath
           
           //Du
           objFrame=document.getElementById("F5"); 
           objFrame.src="Pages/senDyslexiaUnit.aspx" + Rpath
           
            //ELL
           objFrame=document.getElementById("F6"); 
           objFrame.src="Pages/senELL.aspx" + Rpath
           
           //Wellbeing
           objFrame=document.getElementById("F7"); 
           objFrame.src="Pages/senWellbeing.aspx" + Rpath
           
           //Inclusion Tracker
           objFrame=document.getElementById("F8"); 
           objFrame.src="Pages/senInclusionTracker.aspx" + Rpath
           
           //LSA
           objFrame=document.getElementById("F9"); 
           objFrame.src="Pages/senLSA.aspx" + Rpath
           
           //MAGT
           objFrame=document.getElementById("F10"); 
           objFrame.src="Pages/senMAGT.aspx" + Rpath
           
           //Medical
           objFrame=document.getElementById("F12"); 
           objFrame.src="Pages/senMedical.aspx" + Rpath
           
           //Special Agencies
           objFrame=document.getElementById("F13"); 
           objFrame.src="Pages/senSpecialistsOutsideAgencies.aspx" + Rpath
           
            
           //Teacher Input
           objFrame=document.getElementById("F14"); 
           objFrame.src="Pages/senTeacherInput.aspx" + Rpath
           
           //IEP
           objFrame=document.getElementById("F15"); 
           objFrame.src="Pages/senIEP.aspx" + Rpath
        }
   
  
   
   </script>
     <div align="left" >
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
      
            <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                <tr>
                <td >
                
                </td>
                    <td >
                        <uc2:senStudentBasicInformation id="SenStudentBasicInformation1" runat="server"></uc2:senStudentBasicInformation>
                    </td>
                </tr>
            </table>
           
        <br />
        
        <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
        <ajaxToolkit:TabPanel ID="T1" runat="server">
        <ContentTemplate>
            <uc1:senStudentProfile ID="SenStudentProfile1" runat="server" />
        </ContentTemplate>
            <HeaderTemplate>
               <span  >Stu Profile</span>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
        
         <ajaxToolkit:TabPanel ID="T2" runat="server">
        <ContentTemplate>
        <uc29:senMonitoring ID="SenMonitoring1" runat="server" />
        </ContentTemplate>
             <HeaderTemplate>
              <span >Monitoring</span>
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
        <ajaxToolkit:TabPanel ID="T3" runat="server">
        <ContentTemplate>
        
        <iframe id="F3" height="3000px" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="100%"></iframe>
        
        </ContentTemplate>
             <HeaderTemplate>
                  <span >Access</span>
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
        
        
         <ajaxToolkit:TabPanel ID="T4" runat="server">
        <ContentTemplate>
        
         <iframe id="F4" height="3000px"  scrolling="auto" marginwidth="0px"  frameborder="0"  width="100%"></iframe>
        
        </ContentTemplate>
             <HeaderTemplate>  
                 <span  >ACe</span>
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
        
        <ajaxToolkit:TabPanel ID="T5" runat="server">
        <ContentTemplate>
        
        <iframe id="F5" height="3000px" scrolling="auto" marginwidth="0px" frameborder="0" width="100%"></iframe>

        </ContentTemplate>
             <HeaderTemplate>
                 <span  >DU</span>
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
        
         <ajaxToolkit:TabPanel ID="T6" runat="server">
        <ContentTemplate>
        <iframe id="F6" height="3000px" scrolling="auto" marginwidth="0px"  frameborder="0" width="100%"></iframe>

        </ContentTemplate>
             <HeaderTemplate>
                <span  >ELL</span>
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
        
         <ajaxToolkit:TabPanel ID="T7" runat="server">
        <ContentTemplate>
        <iframe id="F7" height="3000px" scrolling="auto" marginwidth="0px"  frameborder="0" width="100%"></iframe>
      
        </ContentTemplate>
             <HeaderTemplate>
               <span  >Wellbeing</span>
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
        
         <ajaxToolkit:TabPanel ID="T8" runat="server">
        <ContentTemplate>
        <iframe id="F8" height="3000px" scrolling="auto" marginwidth="0px"  frameborder="0" width="100%"></iframe>
        
        </ContentTemplate>
             <HeaderTemplate>
              <span >Inclusion Tracker</span>
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
        
        
         <ajaxToolkit:TabPanel ID="T9" runat="server">
        <ContentTemplate>
        
        <iframe id="F9" height="3000px" scrolling="auto" marginwidth="0px"  frameborder="0" width="100%"></iframe>
        
        </ContentTemplate>
             <HeaderTemplate>
               <span  >LSA</span>
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
        
         <ajaxToolkit:TabPanel ID="T10" runat="server">
        <ContentTemplate>
        <iframe id="F10" height="3000px" scrolling="auto" marginwidth="0px"  frameborder="0" width="100%"></iframe>
       
        </ContentTemplate>
             <HeaderTemplate>
             <span  >MAGT</span>
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
         <ajaxToolkit:TabPanel ID="T11" runat="server">
        <ContentTemplate>
        
        
        </ContentTemplate>
             <HeaderTemplate>
                <span  >Enrich</span>
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
        <ajaxToolkit:TabPanel ID="T12" runat="server">
        <ContentTemplate>
        <iframe id="F12" height="3000px" scrolling="auto" marginwidth="0px"  frameborder="0" width="100%"></iframe>
        
        </ContentTemplate>
             <HeaderTemplate>
               <span  >Med Info</span>
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
        
         <ajaxToolkit:TabPanel ID="T13" runat="server">
        <ContentTemplate>
        <iframe id="F13" height="3000px" scrolling="auto" marginwidth="0px"  frameborder="0" width="100%"></iframe>
         
        </ContentTemplate>
             <HeaderTemplate>
               <span >Sp/O Agencies</span>
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
        
         <ajaxToolkit:TabPanel ID="T14" runat="server">
        <ContentTemplate>
         <iframe id="F14" height="3000px" scrolling="auto" marginwidth="0px"  frameborder="0" width="100%"></iframe>
         
        </ContentTemplate>
             <HeaderTemplate>
              <span  >T Input</span>
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
        
         <ajaxToolkit:TabPanel ID="T15" runat="server">
        <ContentTemplate>
         <iframe id="F15" height="3000px" scrolling="auto" marginwidth="0px"  frameborder="0" width="100%"></iframe>
        </ContentTemplate>
             <HeaderTemplate>
             <span  >IEP</span>
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>
        
        
        
         <asp:HiddenField ID="Hiddenstuid" runat="server" />
        <asp:HiddenField ID="Hiddenbsuid" runat="server" />
        
              </ContentTemplate>
    </asp:UpdatePanel>
        
</div>
</asp:Content>
