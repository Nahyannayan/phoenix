<%@ Page Language="VB" AutoEventWireup="false" CodeFile="senWellbeing.aspx.vb" Inherits="SenRegister_Pages_senWellbeing" %>

<%@ Register Src="../UserControls/senMonitoringWellbeing.ascx" TagName="senMonitoringWellbeing"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/senWellbeing.ascx" TagName="senWellbeing" TagPrefix="uc2" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   <title></title>
    <%--<link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
       <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
   <div class="matters">
    
        <ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:senMonitoringWellbeing ID="SenMonitoringWellbeing1" runat="server" />
           <uc2:senWellbeing ID="SenWellbeing1" runat="server" />
      
     
        </ContentTemplate>
        </asp:UpdatePanel>
      
       
        
       
        
     
    
    </div>
    </form>
</body>
</html>
