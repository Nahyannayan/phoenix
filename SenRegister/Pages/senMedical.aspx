<%@ Page Language="VB" AutoEventWireup="false" CodeFile="senMedical.aspx.vb" Inherits="SenRegister_Pages_senMedical" %>

<%@ Register Src="../UserControls/senMonitoringMedical.ascx" TagName="senMonitoringMedical"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/senMedical.ascx" TagName="senMedical" TagPrefix="uc2" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <title></title>
    <%--<link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
       <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
     <div class="matters">
    
        <ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:senMonitoringMedical ID="SenMonitoringMedical1" runat="server" />
            <uc2:senMedical ID="SenMedical1" runat="server" />
        </ContentTemplate>
        </asp:UpdatePanel>
        
         
       
    
    </div>
    </form>
</body>
</html>
