<%@ Page Language="VB" AutoEventWireup="false" CodeFile="senELL.aspx.vb" Inherits="SenRegister_Pages_senELL" %>

<%@ Register Src="../UserControls/senELL.ascx" TagName="senELL" TagPrefix="uc2" %>

<%@ Register Src="../UserControls/senMonitoringELL.ascx" TagName="senMonitoringELL"
    TagPrefix="uc1" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
 <title></title>
    <%--<link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
       <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
      <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div >
    
        <ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:senMonitoringELL ID="SenMonitoringELL1" runat="server" />
            <uc2:senELL ID="SenELL1" runat="server" />
     
        </ContentTemplate>
        </asp:UpdatePanel>
        
       
        
       
        
     
    
    </div>
    </form>
</body>
</html>
