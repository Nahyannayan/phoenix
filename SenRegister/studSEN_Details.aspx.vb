Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class studSEN_Details
    Inherits BasePage
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Page.IsPostBack = False Then
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'hardcode the menu code
                If USR_NAME = "" Then
                    Response.Redirect("~\noAccess.aspx")
                End If
                Dim Cstu_id As String = Session("Cstu_id")
                If Not Request.QueryString("Stu_id") And Not Request.QueryString("Sib_id") Is Nothing Then
                    ''  ViewState("stu_id") = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
                    ViewState("stu_id") = Request.QueryString("Stu_id").ToString
                    ViewState("Sib_id") = Request.QueryString("Sib_id").ToString
                    'Session("sUsr_name") = 0
                Else
                    ViewState("stu_id") = ""
                    Response.Redirect("~\noAccess.aspx")
                End If '

                '  ViewState("viewid") = Session("Trans_Stu_ID")
                HF_SibId.Value = ViewState("Sib_id")
                HF_stuid.Value = ViewState("stu_id")
               
                binddetails(HF_stuid.Value)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub GetNational(ByVal ddlcountry As DropDownList, ByVal status As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sqlGetNational As String = ""

        sqlGetNational = "Select CTY_ID,case CTY_ID when '5' then 'Not Available' else isnull(CTY_NATIONALITY,'') end CTY_NATIONALITY ,CTY_DESCR from Country_m  order by CTY_NATIONALITY"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sqlGetNational)
        ddlcountry.Items.Clear()
        ddlcountry.DataSource = ds.Tables(0)
        If status = "C" Then
            ddlcountry.DataTextField = "CTY_DESCR"
        Else
            ddlcountry.DataTextField = "CTY_NATIONALITY"
        End If
        ddlcountry.DataValueField = "CTY_ID"

        ddlcountry.DataBind()
        Dim lst As New ListItem
        lst.Text = "--Select--"
        lst.Value = "0"
        ddlcountry.Items.Insert(0, lst)
    End Sub
    Sub bindEmirates(ByVal ddlEmirates As DropDownList)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "select EMR_CODE,EMR_DESCR from EMIRATE_M"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlEmirates.Items.Clear()
            ddlEmirates.DataSource = ds.Tables(0)
            ddlEmirates.DataTextField = "EMR_DESCR"
            ddlEmirates.DataValueField = "EMR_CODE"
            ddlEmirates.DataBind()
            Dim lst As New ListItem
            lst.Text = "--Select--"
            lst.Value = "0"
            ddlEmirates.Items.Insert(0, lst)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub binddetails(ByVal stu_id As String)
        ' AccessStudentClass.GetStudent_D(stu_id)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim ds As New DataSet
            'Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GetStudent_M_DDetails(ViewState("viewid"), Session("sBsuid"))
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "SEN_KHDA_PROFILE_V2_1", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        ltStudName.Text = Convert.ToString(readerStudent_Detail("STU_PASPRTNAME"))
                        ltStudId.Text = Convert.ToString(readerStudent_Detail("STU_NO"))
                        'ltCLM.Text = Convert.ToString(readerStudent_Detail("clm_descr"))
                        ltGrd.Text = Convert.ToString(readerStudent_Detail("GRM_DISPLAY"))
                        ltSct.Text = Convert.ToString(readerStudent_Detail("SCT_DESCR"))

                        If readerStudent_Detail("KHDA_bBEHAVE") = 1 Then
                            rbBEHAVE_Yes.Checked = True
                            txtBEHAVE_Note.Text = Convert.ToString(readerStudent_Detail("KHDA_BEHAVE"))
                            txtBehave_Action.Text = Convert.ToString(readerStudent_Detail("KHDA_BEHAVE_Actions"))
                        Else
                            rbBEHAVE_No.Checked = True
                        End If


                        If readerStudent_Detail("KHDA_bSENSORY") = 1 Then
                            rbSENSORY_Yes.Checked = True
                            txtSensory_Note.Text = Convert.ToString(readerStudent_Detail("KHDA_SENSORY"))
                            txtSensory_action.Text = Convert.ToString(readerStudent_Detail("KHDA_SENSORY_Actions"))
                        Else
                            rbSENSORY_No.Checked = True
                        End If


                        If readerStudent_Detail("KHDA_bMEDICAL") = 1 Then
                            rbMedical_Yes.Checked = True
                            txtMedical_Note.Text = Convert.ToString(readerStudent_Detail("KHDA_MEDICAL"))
                            txtMedical_Action.Text = Convert.ToString(readerStudent_Detail("KHDA_MEDICAL_Actions"))
                        Else
                            rbMedical_No.Checked = True
                        End If


                        If readerStudent_Detail("KHDA_bCOMM") = 1 Then
                            rbCommunication_Yes.Checked = True
                            txtCommunication_Note.Text = Convert.ToString(readerStudent_Detail("KHDA_COMM"))
                            txtCommunication_Action.Text = Convert.ToString(readerStudent_Detail("KHDA_COMM_Actions"))
                        Else
                            rbCommunication_No.Checked = True
                        End If


                        If readerStudent_Detail("KHDA_bLEARNING") = 1 Then
                            rbLearning_Yes.Checked = True
                            txtLearning_Note.Text = Convert.ToString(readerStudent_Detail("KHDA_LEARNING"))
                            txtLearning_action.Text = Convert.ToString(readerStudent_Detail("KHDA_LEARNING_Actions"))
                        Else
                            rbLearning_No.Checked = True
                        End If


                        If readerStudent_Detail("KHDA_bGIFTED") = 1 Then
                            rbGifted_Yes.Checked = True
                            txtGifted_Note.Text = Convert.ToString(readerStudent_Detail("KHDA_GIFTED"))
                            txtGifted_Action.Text = Convert.ToString(readerStudent_Detail("KHDA_GIFTED_Actions"))
                        Else
                            rbGifted_No.Checked = True
                        End If

                        If readerStudent_Detail("KHDA_bSEN") = 1 Then
                            rdSen_Yes.Checked = True
                            txtSen.Text = Convert.ToString(readerStudent_Detail("KHDA_SEN"))
                        Else
                            rdSen_No.Checked = True
                        End If

                        If readerStudent_Detail("KHDA_bEAL") = 1 Then
                            rdEAL_YES.Checked = True
                            txtEAL.Text = Convert.ToString(readerStudent_Detail("KHDA_EAL"))
                            txtEAL_Action.Text = Convert.ToString(readerStudent_Detail("KHDA_EAL_Actions"))
                        Else
                            rdEAL_NO.Checked = True
                        End If

                        If readerStudent_Detail("KHDA_bDISABLED") = 1 Then
                            rbDisabled_Yes.Checked = True
                            txtDisabled_Note.Text = Convert.ToString(readerStudent_Detail("KHDA_DISABLED"))
                            txtDisabled_Action.Text = Convert.ToString(readerStudent_Detail("KHDA_DISABLED_Actions"))
                        Else
                            rbDisabled_No.Checked = True
                        End If

                        If readerStudent_Detail("KHDA_bSpeech_Disorder") = 1 Then
                            rdSpeech_Yes.Checked = True
                            txtSpeech_Note.Text = Convert.ToString(readerStudent_Detail("KHDA_Speech_Disorder"))
                            txtSpeech_Action.Text = Convert.ToString(readerStudent_Detail("KHDA_Speech_Disorder_Actions"))
                        Else
                            rdSpeech_No.Checked = True
                        End If

                        If readerStudent_Detail("KHDA_bLearning_Difficulties") = 1 Then
                            rdspecificLearn_Yes.Checked = True
                            txtSpecificLEarn_Note.Text = Convert.ToString(readerStudent_Detail("KHDA_Learning_Difficulties"))
                            txtSpecificLEarn_Action.Text = Convert.ToString(readerStudent_Detail("KHDA_Learning_Difficulties_ACtions"))
                        Else
                            rdspecificLearn_No.Checked = True
                        End If

                        If readerStudent_Detail("KHDA_wave") = 1 Then
                            rdblWave.SelectedValue = "1"
                        ElseIf readerStudent_Detail("KHDA_wave") = 2 Then
                            rdblWave.SelectedValue = "2"
                        ElseIf readerStudent_Detail("KHDA_wave") = 3 Then
                            rdblWave.SelectedValue = "3"
                        End If

                        If readerStudent_Detail("KHDA_bhearingsensor") = True Then
                            rbHearingImpairmentYes.Checked = True
                            txtHearing_Note.Text = Convert.ToString(readerStudent_Detail("KHDA_hearingsensor"))
                            txtHearing_action.Text = Convert.ToString(readerStudent_Detail("KHDA_hearingsensor_actions"))
                        Else
                            rbHearingImpairmentNo.Checked = True
                        End If

                        If readerStudent_Detail("KHDA_bprofoundPMLD") = True Then
                            rbProfound_Yes.Checked = True
                            txtProfound_Note.Text = Convert.ToString(readerStudent_Detail("KHDA_profoundPMLD"))
                            txtProfound_Action.Text = Convert.ToString(readerStudent_Detail("KHDA_profoundPMLD_actions"))
                        Else
                            rbProfound_No.Checked = True
                        End If

                        If readerStudent_Detail("KHDA_bassessedsyndrome") = True Then
                            rb_AssessedYes.Checked = True
                            txtAssesses_Note.Text = Convert.ToString(readerStudent_Detail("KHDA_assessedsyndrome"))
                            txtAssesses_Action.Text = Convert.ToString(readerStudent_Detail("KHDA_assessedsyndrome_actions"))
                        Else
                            rb_AssessedNo.Checked = True
                        End If

                        If readerStudent_Detail("KHDA_bDyslexia") = True Then
                            rb_DyslexiaYes.Checked = True
                            txtDyslexia_Note.Text = Convert.ToString(readerStudent_Detail("KHDA_Dyslexia"))
                            txtDyslexia_Action.Text = Convert.ToString(readerStudent_Detail("KHDA_Dyslexia_actions"))
                        Else
                            rb_Dyslexia_No.Checked = True
                        End If


                        If readerStudent_Detail("KHDA_bDysgraphia") = True Then
                            rb_DysgraphiaYes.Checked = True
                            txtDysgraphia_Note.Text = Convert.ToString(readerStudent_Detail("KHDA_Dysgraphia"))
                            txtDysgraphia_Action.Text = Convert.ToString(readerStudent_Detail("KHDA_Dysgraphia_actions"))
                        Else
                            rb_DysgraphiaNo.Checked = True
                        End If

                        If readerStudent_Detail("KHDA_bDyscalculia") = True Then
                            rb_DyscalculiaYes.Checked = True
                            txtDyscalculia_note.Text = Convert.ToString(readerStudent_Detail("KHDA_Dyscalculia"))
                            txtDyscalculia_Action.Text = Convert.ToString(readerStudent_Detail("KHDA_Dyscalculia_Actions"))
                        Else
                            rb_DyscalculiaNo.Checked = True
                        End If

                        If readerStudent_Detail("KHDA_EAL_Wave") = 1 Then
                            rblELLWave.SelectedValue = "1"
                        ElseIf readerStudent_Detail("KHDA_EAL_Wave") = 2 Then
                            rblELLWave.SelectedValue = "2"
                        ElseIf readerStudent_Detail("KHDA_EAL_Wave") = 3 Then
                            rblELLWave.SelectedValue = "3"
                        End If
                        '
                    End While
                Else
                    'lblerror.Text = "No Records Found "
                End If
            End Using
            'If ds.Tables(0).Rows.Count > 0 Then
            'Else
            '    lblerror.Text = "No Records Found "
            'End If
        Catch ex As Exception
            'lblerror.Text = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim con As New SqlConnection(str_conn)
        Dim transaction As SqlTransaction

        Dim STU_bBEHAVE As Integer
        Dim STU_bSENSORY As Integer
        Dim STU_bMEDICAL As Integer
        Dim STU_bCOMM As Integer
        Dim STU_bLEARNNG As Integer
        Dim STU_bGIFTED As Integer
        Dim STU_bDISABLED As Integer
        Dim STU_bSEN As Integer
        Dim STU_bEAL As Integer
        Dim STU_bSpeechDisorder As Integer
        Dim STU_bLearnDifficulty As Integer


        Dim STU_bHearingSensor As Integer
        Dim STU_wave123 As Integer
        Dim STU_bProfound As Integer
        Dim STU_assessedsyndrome As Integer
        Dim STU_Dyslexia As Integer
        Dim Dysgraphia As Integer
        Dim Dyscalculia As Integer
        Dim STU_ELLwave123 As Integer

        




        If rbBEHAVE_Yes.Checked = True Then
            STU_bBEHAVE = 1
        Else
            STU_bBEHAVE = 0
            txtBEHAVE_Note.Text = ""
        End If

        If rbSENSORY_Yes.Checked = True Then
            STU_bSENSORY = 1
        Else
            STU_bSENSORY = 0
            txtSensory_Note.Text = ""
        End If


        If rbMedical_Yes.Checked = True Then
            STU_bMEDICAL = 1
        Else
            STU_bMEDICAL = 0
            txtMedical_Note.Text = ""
        End If


        If rbCommunication_Yes.Checked = True Then
            STU_bCOMM = 1
        Else
            STU_bCOMM = 0
            txtCommunication_Note.Text = ""
        End If


        If rbLearning_Yes.Checked = True Then
            STU_bLEARNNG = 1
        Else
            STU_bLEARNNG = 0
            txtLearning_Note.Text = ""
        End If

        If rbGifted_Yes.Checked = True Then
            STU_bGIFTED = 1
        Else
            STU_bGIFTED = 0
            txtGifted_Note.Text = ""
        End If


        If rbDisabled_Yes.Checked = True Then
            STU_bDISABLED = 1
        Else
            STU_bDISABLED = 0
            txtDisabled_Note.Text = ""
        End If

        If rdSen_Yes.Checked = True Then
            STU_bSEN = 1
        Else
            STU_bSEN = 0
            txtSen.Text = ""
        End If

        If rdEAL_YES.Checked = True Then
            STU_bEAL = 1
        Else
            STU_bEAL = 0
            txtEAL.Text = ""
        End If

        If rdSpeech_Yes.Checked = True Then
            STU_bSpeechDisorder = 1
        Else
            STU_bSpeechDisorder = 0
            txtSpeech_Note.Text = ""
        End If

        If rdspecificLearn_Yes.Checked = True Then
            STU_bLearnDifficulty = 1
        Else
            STU_bLearnDifficulty = 0
            txtSpecificLEarn_Note.Text = ""
        End If

        ''nahyan on 4feb2019
        If Not rdblWave.SelectedItem Is Nothing Then
            If rdblWave.SelectedItem.Value = "1" Then
                STU_wave123 = 1
            ElseIf rdblWave.SelectedItem.Value = "2" Then
                STU_wave123 = 2
            ElseIf rdblWave.SelectedItem.Value = "3" Then
                STU_wave123 = 3
            End If
        End If

        If rbHearingImpairmentYes.Checked = True Then
            STU_bHearingSensor = 1
        Else
            STU_bHearingSensor = 0
            txtHearing_Note.Text = ""
        End If
        If rbProfound_Yes.Checked = True Then
            STU_bProfound = 1
        Else
            STU_bProfound = 0
            txtProfound_Note.Text = ""
        End If
        If rb_AssessedYes.Checked = True Then
            STU_assessedsyndrome = 1
        Else
            STU_assessedsyndrome = 0
            txtAssesses_Note.Text = ""
        End If
        If rb_DyslexiaYes.Checked = True Then
            STU_Dyslexia = 1
        Else
            STU_Dyslexia = 0
            txtDyslexia_Note.Text = ""
        End If
        If rb_DysgraphiaYes.Checked = True Then
            Dysgraphia = 1
        Else
            Dysgraphia = 0
            txtDysgraphia_Note.Text = ""
        End If
        If rb_DyscalculiaYes.Checked = True Then
            Dyscalculia = 1
        Else
            Dyscalculia = 0
            txtDyscalculia_note.Text = ""
        End If

        ''nahyan on 4feb2019
        If Not rblELLWave.SelectedItem Is Nothing Then
            If rblELLWave.SelectedItem.Value = "1" Then
                STU_ELLwave123 = 1
            ElseIf rblELLWave.SelectedItem.Value = "2" Then
                STU_ELLwave123 = 2
            ElseIf rblELLWave.SelectedItem.Value = "3" Then
                STU_ELLwave123 = 3
            End If
        End If

        con.Open()
        transaction = con.BeginTransaction("trans")
        Try
            If Page.IsValid = True Then
                Dim param(54) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@STU_ID", HF_stuid.Value)
                param(1) = New SqlClient.SqlParameter("@STU_bBEHAVE", STU_bBEHAVE)
                param(2) = New SqlClient.SqlParameter("@STU_BEHAVE", txtBEHAVE_Note.Text)
                param(3) = New SqlClient.SqlParameter("@STU_bSENSORY", STU_bSENSORY)
                param(4) = New SqlClient.SqlParameter("@STU_SENSORY", txtSensory_Note.Text)
                param(5) = New SqlClient.SqlParameter("@STU_bMEDICAL", STU_bMEDICAL)
                param(6) = New SqlClient.SqlParameter("@STU_MEDICAL", txtMedical_Note.Text)
                param(7) = New SqlClient.SqlParameter("@STU_bCOMM", STU_bCOMM)
                param(8) = New SqlClient.SqlParameter("@STU_COMM", txtCommunication_Note.Text)
                param(9) = New SqlClient.SqlParameter("@STU_bLEARNING", STU_bLEARNNG)
                param(10) = New SqlClient.SqlParameter("@STU_LEARNING", txtLearning_Note.Text)
                param(11) = New SqlClient.SqlParameter("@STU_bGIFTED", STU_bGIFTED)
                param(12) = New SqlClient.SqlParameter("@STU_GIFTED", txtGifted_Note.Text)
                param(13) = New SqlClient.SqlParameter("@STU_bDISABLED", STU_bDISABLED)
                param(14) = New SqlClient.SqlParameter("@STU_DISABLED", txtDisabled_Note.Text)
                param(15) = New SqlClient.SqlParameter("@STU_bSEN", STU_bSEN)
                param(16) = New SqlClient.SqlParameter("@STU_SEN", txtSen.Text)
                param(17) = New SqlClient.SqlParameter("@STU_bEAL", STU_bEAL)
                param(18) = New SqlClient.SqlParameter("@STU_EAL", txtEAL.Text)
                param(19) = New SqlClient.SqlParameter("@USR", Session("sUsr_name"))


                param(20) = New SqlClient.SqlParameter("@KHDA_bSpeech_Disorder", STU_bSpeechDisorder)
                param(21) = New SqlClient.SqlParameter("@KHDA_Speech_Disorder", txtSpeech_Note.Text)
                param(22) = New SqlClient.SqlParameter("@KHDA_bLearning_Difficulties", STU_bLearnDifficulty)
                param(23) = New SqlClient.SqlParameter("@KHDA_Learning_Difficulties", txtSpecificLEarn_Note.Text)

                ''nahyan on 4feb2019

                'Dim STU_bHearingSensor As Integer
                'Dim STU_wave123 As Integer
                'Dim STU_bProfound As Integer
                'Dim STU_assessedsyndrome As Integer
                'Dim STU_Dyslexia As Integer
                'Dim Dysgraphia As Integer
                'Dim Dyscalculia As Integer

                param(24) = New SqlClient.SqlParameter("@KHDA_STU_bHearingSensor", STU_bHearingSensor)
                param(25) = New SqlClient.SqlParameter("@KHDA_STU_HearingSensor", txtHearing_Note.Text)
                param(26) = New SqlClient.SqlParameter("@KHDA_STU_HearingSensor_action", txtHearing_action.Text)
                param(27) = New SqlClient.SqlParameter("@KHDA_STU_wave123", STU_wave123)
                param(28) = New SqlClient.SqlParameter("@KHDA_STU_bProfound", STU_bProfound)
                param(29) = New SqlClient.SqlParameter("@KHDA_STU_Profound", txtProfound_Note.Text)
                param(30) = New SqlClient.SqlParameter("@KHDA_STU_Profound_action", txtProfound_Action.Text)
                param(31) = New SqlClient.SqlParameter("@STU_bassessedsyndrome", STU_assessedsyndrome)
                param(32) = New SqlClient.SqlParameter("@STU_assessedsyndrome", txtAssesses_Note.Text)
                param(33) = New SqlClient.SqlParameter("@STU_assessedsyndrome_action", txtAssesses_Action.Text)

                param(34) = New SqlClient.SqlParameter("@STU_bDyslexia", STU_Dyslexia)
                param(35) = New SqlClient.SqlParameter("@STU_Dyslexia", txtDyslexia_Note.Text)
                param(36) = New SqlClient.SqlParameter("@STU_Dyslexia_action", txtDyslexia_Action.Text)

                param(37) = New SqlClient.SqlParameter("@STU_bDysgraphia", Dysgraphia)
                param(38) = New SqlClient.SqlParameter("@STU_Dysgraphia", txtDysgraphia_Note.Text)
                param(39) = New SqlClient.SqlParameter("@STU_Dysgraphia_action", txtDysgraphia_Action.Text)

                param(40) = New SqlClient.SqlParameter("@STU_bDyscalculia", Dyscalculia)
                param(41) = New SqlClient.SqlParameter("@STU_Dyscalculia", txtDyscalculia_note.Text)
                param(42) = New SqlClient.SqlParameter("@STU_Dyscalculia_action", txtDyscalculia_Action.Text)

                param(43) = New SqlClient.SqlParameter("@STU_BEHAVE_Action", txtBehave_Action.Text)
                param(44) = New SqlClient.SqlParameter("@STU_SENSORY_Action", txtSensory_action.Text)
                param(45) = New SqlClient.SqlParameter("@STU_MEDICAL_action", txtMedical_Action.Text)
                param(46) = New SqlClient.SqlParameter("@STU_COMM_Action", txtCommunication_Action.Text)
                param(47) = New SqlClient.SqlParameter("@STU_LEARNING_Action", txtLearning_action.Text)
                param(48) = New SqlClient.SqlParameter("@STU_GIFTED_Action", txtGifted_Action.Text)
                param(49) = New SqlClient.SqlParameter("@STU_DISABLED_Action", txtDisabled_Action.Text)
                param(50) = New SqlClient.SqlParameter("@STU_EAL_Action", txtEAL_Action.Text)

                param(51) = New SqlClient.SqlParameter("@KHDA_Speech_Disorder_Action", txtSpeech_Action.Text)
                param(52) = New SqlClient.SqlParameter("@KHDA_Learning_Difficulties_Action", txtSpecificLEarn_Action.Text)
                param(53) = New SqlClient.SqlParameter("@KHDA_ELLWAVE", STU_ELLwave123)

                
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVE_KHDA_SEN_DETAILS_Complete ", param)

            End If
            transaction.Commit()
            lblerror.Text = "Records Saved sucessfully...!!!!"
        Catch ex As Exception
            transaction.Rollback()
            lblerror.Text = "Insertion Failed...!!!!"
        End Try
    End Sub
End Class
