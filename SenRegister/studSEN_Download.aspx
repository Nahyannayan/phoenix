﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="studSEN_Download.aspx.vb" Inherits="SenRegister_studSEN_Download" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <base target="_self" />
    <title></title>
    <!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <%--    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <%--<style type="text/css">
        BODY
        {
        	margin:0; font-family: verdana, Arial, Helvetica, sans-serif; font-size:11px;
           background:url('../images/Common/PageBody/bg1.png') repeat-x 0 0; color:#666;
            padding-right: 0px;
            padding-left: 0px;
            padding-bottom: 0px;
          
            
        }
        

    </style>
    --%>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
               <tr>
                    <td align="left">
                       <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                 
                                <td align="left" colspan="6" valign="middle" class="title-bg-lite">
                                    
                                  Upload Details
                                </td>
                            </tr>
                            <tr id="trAcd2" runat="server">
                                <td align="left" width="10%">
                                    <span class="field-label">Name</span>
                                </td>

                                <td align="left" width="30%">
                                    <asp:Label ID="lblname" runat="server" Text=" " CssClass="field-value"></asp:Label>

                                </td>
                                <td align="left" width="10%">
                                    <span class="field-label">Student ID</span>
                                </td>

                                <td align="left" width="20%">
                                    <asp:Label ID="lblStuID" runat="server" Text=" " CssClass="field-value"></asp:Label>
                                </td>
                                <td align="left" width="20%">
                                    <span class="field-label">Grade & Section</span>
                                </td>

                                <td align="left" width="10%">
                                    <asp:Label ID="lblGrade" runat="server" Text=" " CssClass="field-value"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="field-label">Category</span>
                                </td>

                                <td>
                                    <asp:DropDownList ID="ddlcategory" runat="server" AutoPostBack="true">
                                        <asp:ListItem>--Select--</asp:ListItem>
                                        <asp:ListItem>IEP</asp:ListItem>
                                        <asp:ListItem>IAP</asp:ListItem>
                                        <asp:ListItem>BIP</asp:ListItem>
                                        <asp:ListItem>OTHER</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <span class="field-label">Select File</span>
                                </td>

                                <td>
                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                </td>
                                <td align="center" colspan="2">
                                    <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="button" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <asp:GridView ID="gvSkill" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords."
                                         PageSize="20" Width="100%">
                                        <RowStyle CssClass="griditem"  />
                                        <EmptyDataRowStyle  />
                                        <Columns>

                                            <asp:TemplateField HeaderText="objid" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSksId" runat="server" Text='<%# Bind("doc_id") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="File Name">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td align="center" >
                                                                <asp:ImageButton
                                                                    ID="imgF" runat="server"  OnClick="imgF_Click" CommandArgument='<%# Bind("doc_id") %>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:LinkButton ID="txtfile" runat="server" 
                                                                    Text='<%# Bind("doc_name") %>' CommandName="View" CommandArgument='<%# Bind("doc_id") %>'
                                                                    OnClick="txtfile_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" ></ItemStyle>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Category">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcat" runat="server" Text='<%# Bind("doc_category") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldate" runat="server" Text='<%# Bind("doc_date","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="objid" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDelete" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="index" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblIndex" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="delete"
                                                        Text="Delete"></asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="Selected item will be deleted permanently.Are you sure you want to continue?"
                                                        runat="server">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle />
                                        <HeaderStyle  />
                                        <EditRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative"  />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="H_STU_ID" runat="server" />
        </div>
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
    </form>
</body>
</html>
