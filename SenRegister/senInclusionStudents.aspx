<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master"
    CodeFile="senInclusionStudents.aspx.vb" Inherits="SenRegister_senInclusionStudents" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Students Profile
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <div>
                    <div>
                        <br />
                        <br />
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr class="title-bg-lite">

                                <td colspan="4">Students in Inclusion Sector </td>
                            </tr>

                            <tr>

                                <td align="left" width="20%">
                                    <span class="field-label">Academic Year </span>
                                </td>
                                <td align="left" width="30%">
                                    <asp:DropDownList
                                        ID="ddaccyear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddaccyear_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" width="20%"></td>
                                <td align="left" width="30%"></td>
                            </tr>


                            <tr>
                                <td colspan="4">
                                    <asp:GridView ID="GrdView" AutoGenerateColumns="False" EmptyDataText="Students not added yet"  CssClass="table table-bordered table-row"
                                        Width="100%" runat="server" AllowPaging="True" PageSize="15" >

                                        <Columns>
                                            <asp:TemplateField HeaderText="Student Number">
                                                <HeaderTemplate>
                                                    Student Number
                                         <br />
                                                                <asp:TextBox ID="txtnumber" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="ImageSearch1" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />
                                                           
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="HiddensStuid" Value='<%#Eval("stu_id")%>' runat="server" />
                                                    <%#Eval("stu_no")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Student Name">
                                                <HeaderTemplate>
                                                   Student Name
                                       <br />
                                                                <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="ImageSearch2" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />

                                                         
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("STU_NAME")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Grade">
                                                <HeaderTemplate>
                                                   Grade
                                       <br />
                                                                <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="ImageSearch3" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />

                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("GRM_DISPLAY")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Section">
                                                <HeaderTemplate>
                                                   Section
                                       <br />
                                                                <asp:TextBox ID="txtSection" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="ImageSearch4" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />

                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("SCT_DESCR")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Profile">
                                                <HeaderTemplate>
                                                  
                                                                Profile
                                         
                                                </HeaderTemplate>

                                                <ItemTemplate>
                                                    <center>
                                   <asp:LinkButton ID="lnkview" CommandName="view" CommandArgument='<%#Eval("stu_id")%>' runat="server">View</asp:LinkButton>
                               </center>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="griditem" />
                                        <EmptyDataRowStyle />
                                        <SelectedRowStyle />
                                        <HeaderStyle />
                                        <EditRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
                </div>


            </div>
        </div>
    </div>


</asp:Content>
