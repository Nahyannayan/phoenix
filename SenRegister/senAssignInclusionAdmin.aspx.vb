Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_senAssignInclusionAdmin
    Inherits BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CheckMenuRights()
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            CheckSubAdmin()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "SN00030") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub
    Public Sub BindDesignation()
        Dim dddes As DropDownList = DirectCast(GrdAssignAceAdmin.HeaderRow.FindControl("dddes"), DropDownList)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select des_id , des_descr from EMPDESIGNATION_M where des_flag='SD' order by des_descr "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dddes.DataSource = ds
        dddes.DataValueField = "des_id"
        dddes.DataTextField = "des_descr"
        dddes.DataBind()
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Select a Designation"
        dddes.Items.Insert(0, list)

    End Sub
    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select EMP_ID, isnull(EMP_FNAME,'')+' ' +isnull(EMP_MNAME,'')+' '+isnull(EMP_LNAME,'') ENAME, DES_DESCR,EMP_DES_ID from dbo.EMPLOYEE_M a " & _
                        " inner join  dbo.EMPDESIGNATION_M b on a.EMP_DES_ID=b.DES_ID where EMP_BSU_ID='" & Hiddenbsuid.Value & "' and des_flag='SD' and EMP_bACTIVE='True' and EMP_STATUS <> 4 "
        Dim txtname As String
        Dim desigid As DropDownList

        If GrdAssignAceAdmin.Rows.Count > 0 Then
            txtname = DirectCast(GrdAssignAceAdmin.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            desigid = DirectCast(GrdAssignAceAdmin.HeaderRow.FindControl("dddes"), DropDownList)

            If txtname.Trim() <> "" Then
                str_query &= " and isnull(EMP_FNAME,'')+ isnull(EMP_MNAME,'')+isnull(EMP_LNAME,'') like '%" & txtname.Replace(" ", "") & "%' "
            End If
            If desigid.SelectedValue > -1 Then
                str_query &= " and EMP_DES_ID= '" & desigid.SelectedValue & "'"
            End If

        End If

        str_query &= " order by EMP_FNAME "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("ENAME")
            dt.Columns.Add("EMP_ID")
            dt.Columns.Add("EMP_DES_ID")
            dt.Columns.Add("DES_DESCR")

            Dim dr As DataRow = dt.NewRow()
            dr("ENAME") = ""
            dr("EMP_ID") = ""
            dr("EMP_DES_ID") = ""
            dr("DES_DESCR") = ""
            dt.Rows.Add(dr)
            GrdAssignAceAdmin.DataSource = dt
            GrdAssignAceAdmin.DataBind()
            ' DirectCast(GrdAssignAceAdmin.FooterRow.FindControl("btnaddmembers"), Button).Visible = False
        Else
            GrdAssignAceAdmin.DataSource = ds
            GrdAssignAceAdmin.DataBind()
            'DirectCast(GrdAssignAceAdmin.FooterRow.FindControl("btnaddmembers"), Button).Visible = True
            ' ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdAssignAceAdmin.FooterRow.FindControl("btnaddmembers"), Button))
        End If

        If GrdAssignAceAdmin.Rows.Count > 0 Then
            BindDesignation()
            DirectCast(GrdAssignAceAdmin.HeaderRow.FindControl("txtname"), TextBox).Text = txtname
            If desigid Is Nothing Then
            Else
                DirectCast(GrdAssignAceAdmin.HeaderRow.FindControl("dddes"), DropDownList).SelectedValue = desigid.SelectedValue
            End If
        End If
        

    End Sub
    Public Sub CheckSubAdmin()
        Dim returnvalue As String = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim str_query = "select count(*) from SEN_ACE_ADMIN where EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim val As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If Convert.ToInt32(val) = 0 Then
            lblmessage.Text = "ACE admin is not assigned.Please assign an ACE admin."
            BindGrid()
        Else
            Panel2.Visible = False
            Panel1.Visible = True
            Dim ds As DataSet
            str_query = "select * from sen_ace_admin a " & _
                        " inner join oasis.dbo.employee_m b on a.emp_id=b.emp_id " & _
                        " inner join oasis.dbo.empdesignation_m c on b.emp_des_id =c.des_id " & _
                        " where a.emp_bsu_id ='" & Hiddenbsuid.Value & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            lblempid.Text = ds.Tables(0).Rows(0).Item("EMP_ID").ToString()
            lblempno.Text = ds.Tables(0).Rows(0).Item("EMPNO").ToString()
            lblname.Text = ds.Tables(0).Rows(0).Item("EMP_FNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("EMP_MNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("EMP_LNAME").ToString()
            lbldes.Text = ds.Tables(0).Rows(0).Item("DES_DESCR").ToString()
        End If


    End Sub
    Protected Sub GrdAssignAceAdmin_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdAssignAceAdmin.RowCommand

        Try

      
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            If e.CommandName = "Assign" Then
                Dim empid As String = e.CommandArgument

                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EMP_ID", empid)
                pParms(1) = New SqlClient.SqlParameter("@EMP_BSU_ID", Hiddenbsuid.Value)
                lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "INSERT_SEN_ACE_ADMIN", pParms)
                CheckSubAdmin()
            End If
            If e.CommandName = "Change" Then
                ''Delete existing data
                Dim str_query = "Update SEN_ACE_ADMIN set EMP_ID='" & e.CommandArgument & "' where EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

                'Dim empid As String = e.CommandArgument
                'Dim pParms(2) As SqlClient.SqlParameter
                'pParms(0) = New SqlClient.SqlParameter("@EMP_ID", empid)
                'pParms(1) = New SqlClient.SqlParameter("@EMP_BSU_ID", Hiddenbsuid.Value)
                'lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "INSERT_SEN_ACE_ADMIN", pParms)
                CheckSubAdmin()

            End If

            If e.CommandName = "search" Then
                BindGrid()
            End If

        Catch ex As Exception

            lblmessage.Text = "Error : " & ex.Message

        End Try
    End Sub


    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkdelete.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim str_query = "Delete SEN_ACE_ADMIN where EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        lblmessage.Text = "Deleted"
        CheckSubAdmin()
        Panel1.Visible = False
        Panel2.Visible = True
    End Sub

    Protected Sub lnkchange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkchange.Click
        '' Show the reassign button in gridview
        Panel2.Visible = True
        Panel1.Visible = False
        lblmessage.Text = ""
        BindGrid()
        For Each row As GridViewRow In GrdAssignAceAdmin.Rows
            DirectCast(row.FindControl("btnassign"), Button).Visible = False
            DirectCast(row.FindControl("btnchange"), Button).Visible = True
        Next

    End Sub

    
    Protected Sub GrdAssignAceAdmin_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GrdAssignAceAdmin.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub
    Protected Sub ddesschange(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid()
    End Sub
End Class
