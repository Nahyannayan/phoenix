<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="StudSEN_UPDATE.aspx.vb" Inherits="StudSEN_UPDATE" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />
    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>


    <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            position: fixed;
            width: 70%;
            
        }
    </style>

    <script language="javascript" type="text/javascript">

        function popup_new(Sibid, stuid) {

            var sFeatures;
            sFeatures = "dialogWidth: 950px; ";
            sFeatures += "dialogHeight: 620px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: yes; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            // alert(Sibid+','+stuid)
            //            
            url = 'studSEN_details.aspx?Stu_id=' + stuid + '&Sib_id=' + Sibid + '';
            //alert(status) 
            return ShowWindowWithClose(url, 'search', '95%', '85%')
            return false;
            //result = window.showModalDialog(url, "", sFeatures);

            //if (result == '' || result == undefined) {
            //    return false;
            //}

        }
        function CloseFrame() {
            jQuery.fancybox.close();
        }

        function test3(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid1()%>").src = path;
            document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
        }
        function test4(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid2()%>").src = path;
            document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
        }



        function test5(val) {
            //alert(val);
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid3()%>").src = path;
            document.getElementById("<%=h_selected_menu_3.ClientID %>").value = val + '__' + path;
        }


        function test6(val) {
            //alert(val);
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid4()%>").src = path;
            document.getElementById("<%=h_selected_menu_4.ClientID %>").value = val + '__' + path;
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            KHDA Profile
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:HiddenField ID="HF_stuid" runat="server" Visible="False" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select Academic year </span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:GridView ID="gvStudentRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%" CssClass="table table-bordered table-row">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="-" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("eqs_id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fee Id">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("STU_DOJ", "{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblSTUD_IDH" runat="server" Text="FEE ID"></asp:Label>
                                            <br />

                                            <asp:TextBox ID="txtSTUD_ID" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchStud_ID" OnClick="btnSearchStud_ID_Click" runat="server"
                                                ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTUD_ID" runat="server" Text='<%# Bind("STU_FEE_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("STU_No") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblStud_NameH" runat="server" Text="Student Name"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtStud_Name" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchStud_Name" OnClick="btnSearchStud_Name_Click" runat="server"
                                                ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblStud_Name" OnClick="lblStud_Name_Click" runat="server" Text='<%# Bind("STU_PASPRTNAME") %>'
                                                CommandName="Select" OnClientClick="<%# &quot;popup_new('&quot; & Container.DataItem(&quot;STU_SIBLING_ID&quot;)& &quot;','&quot; & Container.DataItem(&quot;stu_id&quot;) & &quot;');return false;&quot; %>"></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("SName") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <HeaderTemplate>

                                            <asp:Label ID="lblGradeH" runat="server" Text="Grade Section"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtStud_Grade" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchGrade" OnClick="btnSearchGrade_Click" runat="server"
                                                ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <asp:Label ID="lblGradeH" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated">
                                        <HeaderTemplate>
                                            Updated
                                                       <br />
                                            <asp:DropDownList ID="ddlgvDoc" runat="server" CssClass="listbox" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlgvDoc_SelectedIndexChanged">
                                                <asp:ListItem>ALL</asp:ListItem>
                                                <asp:ListItem>YES</asp:ListItem>
                                                <asp:ListItem>NO</asp:ListItem>
                                            </asp:DropDownList>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="imgDoc" runat="server" ImageUrl='<%# BIND("STU_bUPDATEDIMG") %>' HorizontalAlign="Center"></asp:Image>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUpdatedDate" Text='<%# Bind("STU_UPDATED_DATE") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Emergency Contact">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMGCONTACT" Text='<%# Bind("STU_EMGCONTACT") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Upload" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDownload" runat="server" CausesValidation="False" OnClick="lnkDownload_Click"
                                                Text="Upload"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="20px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student ID" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("stu_id") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudentID" runat="server" Text='<%# Bind("stu_id") %>'></asp:Label>
                                            <asp:Label ID="lblSIBLING_ID" runat="server" Text='<%# Bind("STU_SIBLING_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlsetup" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">
                        <div>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="right">
                                        <asp:LinkButton ID="lbPnlJobClose" CssClass="closebtnFee" runat="server" OnClick="lbPnlJobClose_Click">Close</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <iframe runat="server" src="#" frameborder="0" scrolling="yes"
                                            width="100%" height="200px" id="IfRefClose"></iframe>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </asp:Panel>




            </div>
        </div>
    </div>


    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });

                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>
</asp:Content>
