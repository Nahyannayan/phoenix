<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="senAssignInclusionAdmin.aspx.vb" Inherits="SenRegister_senAssignInclusionAdmin" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Inclusion Admin
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <div align="left">
                    <center>
        <%--  <asp:ScriptManager id="ScriptManager1" runat="server">
        </asp:ScriptManager>--%><asp:Label ID="lblmessage" runat="server" ></asp:Label>
       
        <asp:Panel ID="Panel2" runat="server" Width="100%" >
         <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="title-bg-lite">
                                Select an Inclusion Administrator</td>
                        </tr>
                        <tr>
                            <td >
        <asp:GridView ID="GrdAssignAceAdmin" runat="server" AutoGenerateColumns="false"  AllowPaging="True" OnPageIndexChanging="GrdAssignAceAdmin_PageIndexChanging" 
            CssClass="table table-bordered table-row" Width="100%"  PageSize="10">
         <FooterStyle  />
         <EmptyDataRowStyle  />
            <Columns>
<asp:TemplateField HeaderText="Name">
                                    <HeaderTemplate>
                                       
                                                    Name
                                                    <br />
                                                    <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="ImageSearch" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"/>
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("ENAME") %>
                                        <asp:HiddenField ID="Hiddenempid" runat="server" Value='<%# Eval("EMP_ID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle  />
                                  
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Designation">
                                    <HeaderTemplate>
                                       
                                                    Designation
                                                     <br /> 
                                                     <asp:DropDownList ID="dddes" AutoPostBack="true" OnSelectedIndexChanged="ddesschange" runat="server" ></asp:DropDownList>
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="HiddenDesId" runat="server" Value='<%# Eval("EMP_DES_ID") %>' />
                                        <%# Eval("DES_DESCR") %>
                                    </ItemTemplate>
                                    <ItemStyle  />
                                   
                                </asp:TemplateField>
<asp:TemplateField><ItemTemplate>
                    <center><asp:Button ID="btnassign" CssClass="button" CommandArgument='<%# Eval("EMP_ID") %>' CommandName="Assign" runat="server" Text="Assign" /></center>
                   <ajaxToolkit:ConfirmButtonExtender ID="CBE1" ConfirmText="You are about to assign this person as Inclusion Administrator.Do you want to continue ?" TargetControlID="btnassign" runat="server"></ajaxToolkit:ConfirmButtonExtender>
                    
                     <center><asp:Button ID="btnchange" CssClass="button" CommandArgument='<%# Eval("EMP_ID") %>' CommandName="Change" Visible="false" runat="server" Text="Change" /></center>
                   <ajaxToolkit:ConfirmButtonExtender ID="CBE2" ConfirmText="You are about to assign this person as Inclusion Administrator.Do you want to continue ?" TargetControlID="btnchange" runat="server"></ajaxToolkit:ConfirmButtonExtender>

                    
</ItemTemplate>
<ItemStyle  />
</asp:TemplateField>
</Columns>
        <RowStyle CssClass="griditem"  />
                        <EmptyDataRowStyle  />
                        <SelectedRowStyle  />
                        <HeaderStyle  />
                        <EditRowStyle  />
                        <AlternatingRowStyle CssClass="griditem_alternative"  />
        </asp:GridView>
         </td>
                    </tr>
               </table>
        </asp:Panel>
        <asp:Panel ID="Panel1" runat="server" Width="100%" Visible="False">
         <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="title-bg-lite">
                                Inclusion Administrator</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%"  >
                                    <tr>
                                        <td  align="right">
                                            <asp:LinkButton ID="Linkoptions" OnClientClick="javascript:return false;" runat="server">Options</asp:LinkButton>
                                
                                            <asp:Panel ID="PanelOptions" runat="server" Height="50px" Width="125px">
                                               
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%"  >
                                                    <tr>
                                                        <td >
                                                          <center><asp:LinkButton ID="lnkdelete" runat="server">Remove</asp:LinkButton></center>  
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td >
                                                            <center><asp:LinkButton ID="lnkchange" runat="server">Change</asp:LinkButton></center>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <table>
                <tr>
                    <td >
                       <span class="field-label">   Employee Id</span></td>
                    <td >
                        <asp:Label ID="lblempid" runat="server" Width="100%"></asp:Label></td>
                </tr>
                <tr>
                    <td >
                         <span class="field-label"> Employee Number</span></td>
                    <td >
                        <asp:Label ID="lblempno" runat="server" Width="100%"></asp:Label></td>
                </tr>
                <tr>
                    <td >
                         <span class="field-label"> Name</span></td>
                    <td >
                        <asp:Label ID="lblname" runat="server" Width="100%"></asp:Label></td>
                </tr>
                <tr>
                    <td >
                        <span class="field-label">  Designation</span></td>
                    <td >
                        <asp:Label ID="lbldes" runat="server" Width="100%"></asp:Label></td>
                </tr>
                                </table>
                            </td>
                        </tr>
               </table>
            
            
            
            
            
            
            </asp:Panel>
        <asp:HiddenField ID="Hiddenbsuid" runat="server" />&nbsp;<br />
        </center>
                </div>
                <ajaxToolkit:ConfirmButtonExtender
                    ID="CBE2" runat="server" ConfirmText="You are about to delete Inclusion Administrator.Do you want to continue ?"
                    TargetControlID="lnkdelete">
                </ajaxToolkit:ConfirmButtonExtender>
                <ajaxToolkit:ConfirmButtonExtender ID="CBE3" runat="server" ConfirmText="You are about to change Inclusion Administrator.Do you want to continue ?"
                    TargetControlID="lnkchange">
                </ajaxToolkit:ConfirmButtonExtender>
                <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                    AutoCollapse="false" AutoExpand="False" CollapseControlID="Linkoptions"
                    Collapsed="True" CollapsedSize="0" CollapsedText="Option" ExpandControlID="Linkoptions"
                    ExpandedSize="100" ExpandedText="Close Option" ScrollContents="false" TargetControlID="PanelOptions"
                    TextLabelID="Linkoptions">
                </ajaxToolkit:CollapsiblePanelExtender>



            </div>
        </div>
    </div>

</asp:Content>
