<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="senAssignInclusionMembers.aspx.vb" Inherits="SenRegister_senAssignInclusionMembers" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <script type="text/javascript">

        window.setTimeout('setpath()', 100);

        function setpath() {
            var path = window.location.href
            var Rpath = path.substring(path.indexOf('?'), path.length)
            var objFrame

            //Inclusion Members
            objFrame = document.getElementById("F1");
            objFrame.src = "Pages/semInclusionMembers.aspx" + Rpath

            //Add Section Members
            objFrame = document.getElementById("F2");
            objFrame.src = "Pages/semAddInclusionMembers.aspx" + Rpath

            //Assign Head of Section
            objFrame = document.getElementById("F3");
            objFrame.src = "Pages/semAssignInclusionHead.aspx" + Rpath

            //Remove Section Members
            objFrame = document.getElementById("F4");
            objFrame.src = "Pages/semRemoveInclusionMembers.aspx" + Rpath


        }



    </script>

     
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Inclusion Staffs
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <div >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr align="left">
                            <td>

                                <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0" >
                                    <ajaxToolkit:TabPanel ID="T0" HeaderText="Inclusion Members" runat="server">
                                        <ContentTemplate>

                                            <iframe id="F1" height="500px" scrolling="auto" width="100%" frameborder="0" marginwidth="0" ></iframe>

                                        </ContentTemplate>
                                        <HeaderTemplate>
                                            Inclusion Members
                                        </HeaderTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="T1" runat="server" HeaderText="Add Section Members">
                                        <ContentTemplate>

                                            <iframe id="F2" height="500px" scrolling="auto" marginwidth="0px" frameborder="0" width="100%"></iframe>

                                        </ContentTemplate>
                                        <HeaderTemplate>
                                            Add Section Members
                                        </HeaderTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="T2" runat="server" HeaderText=" Assign Head of Section">
                                        <ContentTemplate>

                                            <iframe id="F3" height="500px" scrolling="auto" marginwidth="0px" frameborder="0" width="100%"></iframe>

                                        </ContentTemplate>
                                        <HeaderTemplate>
                                            Assign Head of Section
                                        </HeaderTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="T3" HeaderText="Remove Department Members" runat="server">
                                        <ContentTemplate>

                                            <iframe id="F4" height="500px" scrolling="auto" marginwidth="0px" frameborder="0" width="100%"></iframe>

                                        </ContentTemplate>
                                        <HeaderTemplate>
                                            Remove Section Members
                                        </HeaderTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>

                                <asp:HiddenField ID="Hiddenbsuid" runat="server" />



                            </td>
                        </tr>
                    </table>
                </div>



            </div>
        </div>
    </div>

</asp:Content>
