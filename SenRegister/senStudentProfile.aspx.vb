Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_senStudentProfile
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                CheckMenuRights()
          
                Dim Encr_decrData As New Encryption64
                Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
                Hiddenstuid.Value = stu_id
                Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
                TabSelect()
                SealFileCheck()
            Catch ex As Exception


            End Try
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub
    Public Sub SealFileCheck()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select SEAL_FILE from SEN_ACE_STUDENTS where STU_ID='" & Hiddenstuid.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        If ds.Tables(0).Rows.Count > 0 Then
          
            If ds.Tables(0).Rows(0).Item("SEAL_FILE").ToString() <> "" Then

                Session("SEAL_FILE") = ds.Tables(0).Rows(0).Item("SEAL_FILE")
                If ds.Tables(0).Rows(0).Item("SEAL_FILE").ToString() = "True" Then
                    Tab1.Visible = False
                End If

            End If
         
        End If

    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "SN00080" And ViewState("MainMnu_code") <> "SN00090") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub
    Public Sub TabSelect()
       
        T2.Enabled = False
        T3.Enabled = False
        T4.Enabled = False
        T5.Enabled = False
        T6.Enabled = False
        T7.Enabled = False
        T8.Enabled = False
        T9.Enabled = False
        T10.Enabled = False
        T11.Enabled = False
        T12.Enabled = False
        T13.Enabled = False
        T14.Enabled = False
        T15.Enabled = False
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT * FROM SEN_DEPARTMENTS A " & _
                                 " LEFT JOIN SEN_DEPARTMENT_STUDENTS B ON  A.DEPT_ID= B.DEPT_ID AND B.STU_ID='" & Hiddenstuid.Value & "' AND B.STU_BSU_ID='" & Hiddenbsuid.Value & "' AND B.ACTIVE='True'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        Dim i = 0
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim dept_id = ds.Tables(0).Rows(i).Item("DEPT_ID").ToString()
            Dim active = ds.Tables(0).Rows(i).Item("ACTIVE").ToString()
            If active = "True" Then
                Dim TabId = ds.Tables(0).Rows(i).Item("DEPT_TAB_ID").ToString()

                Select Case TabId
                    Case "T2"
                        T2.Enabled = True
                        Exit Select
                    Case "T3"
                        T3.Enabled = True
                        Exit Select
                    Case "T4"
                        T4.Enabled = True
                        Exit Select
                    Case "T5"
                        T5.Enabled = True
                        Exit Select
                    Case "T6"
                        T6.Enabled = True
                        Exit Select
                    Case "T7"
                        T7.Enabled = True
                        Exit Select
                    Case "T8"
                        T8.Enabled = True
                        Exit Select
                    Case "T9"
                        T9.Enabled = True
                        Exit Select
                    Case "T10"
                        T10.Enabled = True
                        Exit Select
                    Case "T11"
                        T11.Enabled = True
                        Exit Select
                    Case "T12"
                        T12.Enabled = True
                        Exit Select
                    Case "T13"
                        T13.Enabled = True
                        Exit Select
                    Case "T14"
                        T14.Enabled = True
                        Exit Select
                    Case "T15"
                        T15.Enabled = True
                        Exit Select
                End Select

            End If
        Next

        If Session("TabIndex") Is Nothing Then
            Tab1.ActiveTabIndex = 0
        Else
            Tab1.ActiveTabIndex = Session("TabIndex")
        End If

    End Sub
End Class
