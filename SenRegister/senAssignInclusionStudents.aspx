<%@ Page Language="VB" AutoEventWireup="false" CodeFile="senAssignInclusionStudents.aspx.vb" MasterPageFile="~/mainMasterPage.master"  Inherits="SenRegister_senAssignInclusionStudents" %>

<%@ Register Src="UserControls/semInclusionStudents.ascx" TagName="semInclusionStudents"
    TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
   <script type="text/javascript" >
   
        //window.setTimeout('setpath()',100);
        
       

                     $(document).ready(function () {
                         setpath();
                     });
                

        function setpath()
        {
           var path = window.location.href
           var Rpath=path.substring(path.indexOf('?'),path.length)
           var objFrame
           
//           //Students in Inclusion Sector
//           objFrame=document.getElementById("F1"); 
//           objFrame.src="Pages/semInclusionStudents.aspx" + Rpath
           
           //Add Students In Inclusion Sector
           objFrame=document.getElementById("F2"); 
           objFrame.src="Pages/semAddInclusionStudents.aspx" + Rpath
           
           //Remove Students From Inclusion Sector
           objFrame=document.getElementById("F3"); 
           objFrame.src="Pages/semRemoveInclusionStudents.aspx" + Rpath
               

        }
   
  
   
   </script>
   
     <style>
        .ajax__tab_xp .ajax__tab_header {
            font-family: inherit !important;
            font-size: inherit !important;
            background-image: none !important;
            
        }
        .ajax__tab_xp .ajax__tab_body {
            font-family :inherit !important;
            border: 1px solid rgba(0, 0, 0, 0.2) !important;
        }
          .ajax__tab_xp .ajax__tab_header .ajax__tab_tab {
                height: auto !important;
                padding: inherit !important;
                background-image: none !important;

                display: block;
       width: 100%;
       padding: 10px 20px !important;
       color: #495057;
       background-clip: padding-box;
       border: 1px solid rgba(0, 0, 0, 0.2) !important;
       border-top-left-radius : 0.75rem;
       border-top-right-radius: 0.75rem;
      
       border-bottom: none !important;
       background: rgb(193,194,194) !important;
       transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out !important;
       
       background: -moz-linear-gradient(left,  rgba(193,194,194,1) 0%, rgba(234,234,234,1) 100%) !important; /* FF3.6+ */
       background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(193,194,194,1)), color-stop(100%,rgba(234,234,234,1))) !important; /* Chrome,Safari4+ */
       background: -webkit-linear-gradient(left,  rgba(193,194,194,1) 0%,rgba(234,234,234,1) 100%) !important; /* Chrome10+,Safari5.1+ */
       background: -o-linear-gradient(left,  rgba(193,194,194,1) 0%,rgba(234,234,234,1) 100%) !important; /* Opera 11.10+ */
       background: -ms-linear-gradient(left,  rgba(193,194,194,1) 0%,rgba(234,234,234,1) 100%) !important; /* IE10+ */
       background: linear-gradient(to right,  rgba(193,194,194,1) 0%,rgba(234,234,234,1) 100%) !important; /* W3C */
       filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c1c2c2', endColorstr='#eaeaea',GradientType=1 ) !important; /* IE6-9 */
                }

          .ajax__tab_xp .ajax__tab_header .ajax__tab_tab:hover {
                height: auto !important;
                padding: inherit !important;
                background-image: none !important;

                display: block;
       width: 100%;
       padding: 10px 20px !important;
       background-clip: padding-box;
       border: 1px solid rgba(0, 0, 0, 0.2) !important;
       border-top-left-radius : 0.75rem;
       border-top-right-radius: 0.75rem;
      
       border-bottom: none !important;
       color: #ffffff;
       background: rgb(193,194,194) !important;
       background: -moz-linear-gradient(left,  rgba(234,234,234,1) 0%, rgba(193,194,194,1) 100%) !important; /* FF3.6+ */
       background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(234,234,234,1)), color-stop(100%,rgba(193,194,194,1))) !important; /* Chrome,Safari4+ */
       background: -webkit-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* Chrome10+,Safari5.1+ */
       background: -o-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* Opera 11.10+ */
       background: -ms-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* IE10+ */
       background: linear-gradient(to right,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* W3C */
       filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c1c2c2', endColorstr='#eaeaea',GradientType=1 ) !important; /* IE6-9 */
        }

        .ajax__tab_default .ajax__tab_tab {
            overflow: inherit !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_outer {
            padding-right: inherit !important;
            background-image: none !important;
            height: inherit !important;
            margin-right: 6px;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_active .ajax__tab_outer {
            background-image: none !important;
            margin-right: 6px;
            background-color: #ffffff !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_active .ajax__tab_inner {
            background-image: none !important;
           
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_inner {
            background-image: none !important;
            padding-left:0px !important;
        }

       .ajax__tab_inner:hover {
                /*background-color: #cecece !important;*/
        }
            .ajax__tab_tab:focus {
                outline: 0px;
            }
        .ajax__tab_active {
            background-color: #ffffff !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_active .ajax__tab_tab {
            background-image: none !important;

            display: block;
            width: 100%;
            padding: 10px 20px !important;
            color: #495057;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, 0.2) !important;
            border-top-left-radius : 0.75rem;
            border-top-right-radius: 0.75rem;
            
            border-bottom: none !important;
            background: rgb(193,194,194) !important;
       background: -moz-linear-gradient(left,  rgba(234,234,234,0.3) 0%, rgba(193,194,194,0.3) 100%) !important; /* FF3.6+ */
       background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(234,234,234,0.3)), color-stop(100%,rgba(193,194,194,0.3))) !important; /* Chrome,Safari4+ */
       background: -webkit-linear-gradient(left,  rgba(234,234,234,0.3) 0%,rgba(193,194,194,0.3) 100%) !important; /* Chrome10+,Safari5.1+ */
       background: -o-linear-gradient(left,  rgba(234,234,234,0.3) 0%,rgba(193,194,194,0.3) 100%) !important; /* Opera 11.10+ */
       background: -ms-linear-gradient(left,  rgba(234,234,234,0.3) 0%,rgba(193,194,194,0.3) 100%) !important; /* IE10+ */
       background: linear-gradient(to right,  rgba(234,234,234,0.3) 0%,rgba(193,194,194,0.3) 100%) !important; /* W3C */
       filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c1c2c2', endColorstr='#eaeaea',GradientType=1 ) !important; /* IE6-9 */
            
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_active .ajax__tab_tab:hover {
            background-image: none !important;

            display: block;
            width: 100%;
            padding: 10px 20px !important;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, 0.2) !important;
            border-top-left-radius : 0.75rem;
            border-top-right-radius: 0.75rem;
            border-bottom: none !important;

         color: #ffffff;
       background: rgb(193,194,194) !important;
       background: -moz-linear-gradient(left,  rgba(234,234,234,1) 0%, rgba(193,194,194,1) 100%) !important; /* FF3.6+ */
       background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(234,234,234,1)), color-stop(100%,rgba(193,194,194,1))) !important; /* Chrome,Safari4+ */
       background: -webkit-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* Chrome10+,Safari5.1+ */
       background: -o-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* Opera 11.10+ */
       background: -ms-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* IE10+ */
       background: linear-gradient(to right,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* W3C */
       filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c1c2c2', endColorstr='#eaeaea',GradientType=1 ) !important; /* IE6-9 */
         }
        
        .ajax__tab_tab:focus {
            border:0px solid transparent !important;
        }
    </style>
        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Inclusion Students
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

  <div  align="left">
         <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="2" >
         
         <ajaxToolkit:TabPanel ID="T1" HeaderText="Students in Inclusion Sector" runat="server">
        <ContentTemplate> 
        
       <%-- <iframe id="F1" height="1000px" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="700"></iframe>--%>
         <uc1:semInclusionStudents ID="SemInclusionStudents1" runat="server" />
         
         </ContentTemplate>
         
            <HeaderTemplate>
                Students in Inclusion Sector
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
         <ajaxToolkit:TabPanel ID="T2" HeaderText="Add Students In Inclusion Sector" runat="server">
        <ContentTemplate>    
           
          <iframe id="F2" height="500px" scrolling="auto"  marginwidth="0"  frameborder="0"  width="100%"></iframe>

         
         </ContentTemplate>
         
             <HeaderTemplate>
                 Add Students In Inclusion Sector
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        
         <ajaxToolkit:TabPanel ID="T3" HeaderText=" Remove Students From Inclusion Sector" runat="server">
        <ContentTemplate>
        
          <iframe id="F3" height="500px" scrolling="auto"  marginwidth="0"  frameborder="0"  width="100%"></iframe>

         
         </ContentTemplate>
             <HeaderTemplate>
                 Remove Students From Inclusion Sector
             </HeaderTemplate>
        </ajaxToolkit:TabPanel>
     
        </ajaxToolkit:TabContainer>

         
               
    </div>
        <asp:HiddenField ID="Hiddenbsuid" runat="server" />
  

            </div>
        </div>
    </div>


</asp:Content>


