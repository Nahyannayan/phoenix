﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class SenRegister_studSEN_Download
    Inherits BasePage
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            H_STU_ID.Value = Request.QueryString("Stu_id")
            
            Try

                getdata(H_STU_ID.Value)
                gridbind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
    Sub getdata(ByVal id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""
        Dim subj As String = ""

        str_query = "select STU_NO,upper(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME ,'')) as STU_NAME ," _
        & " (STU_GRD_ID +' ' + SCT_DESCR) as Grade  from  VW_STUDENT_M inner join oasis..SECTION_M on STU_SCT_ID=SCT_ID " _
        & " where STU_ID = " + id

        If str_query <> "" Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count >= 1 Then

                lblname.Text = ds.Tables(0).Rows(0).Item("STU_NAME")
                lblStuID.Text = ds.Tables(0).Rows(0).Item("STU_NO")
                lblGrade.Text = ds.Tables(0).Rows(0).Item("Grade")
            End If
        End If
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString

            Dim cmd As New SqlCommand
            Dim filebyte As Byte() = Nothing
            Dim FilePath As String = ""
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            cmd = New SqlCommand("dbo.saveSEN_DOCUMENTS", objConn)

            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@DOC_STU_ID", H_STU_ID.Value)
            cmd.Parameters.AddWithValue("@DOC_CATEGORY", ddlcategory.SelectedValue)
            If FileUpload1.HasFile Then
                Dim p As SqlParameter
                FilePath = Server.MapPath("~/Curriculum/ReportDownloads") & "/" & FileUpload1.FileName
                FileUpload1.SaveAs(FilePath)
                filebyte = System.IO.File.ReadAllBytes(FilePath)

                p = cmd.Parameters.AddWithValue("@DOC_FILE", filebyte)

                cmd.Parameters.AddWithValue("@DOC_NAME", FileUpload1.FileName)
                cmd.Parameters.AddWithValue("@DOC_TYPE", FileUpload1.PostedFile.ContentType)

            End If

            cmd.ExecuteNonQuery()

            cmd.Dispose()
            objConn.Close()
            lblError.Text = "Uploaded"
            gridbind()
            File.Delete(FilePath)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Public Sub gridbind()
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet

            str_Sql = "select doc_id,doc_name,doc_category,doc_date from SEN_ACE_STUD_DOCUMENT where doc_stu_id=" + H_STU_ID.Value

            If ddlcategory.SelectedValue <> "--Select--" Then
                str_Sql += " and doc_category='" + ddlcategory.SelectedValue + "'"

            End If
            


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                gvSkill.DataSource = ds.Tables(0)
                gvSkill.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvSkill.DataSource = ds.Tables(0)
                Try
                    gvSkill.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvSkill.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvSkill.Rows(0).Cells.Clear()
                gvSkill.Rows(0).Cells.Add(New TableCell)
                gvSkill.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSkill.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSkill.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


        Catch ex As Exception

        End Try

    End Sub
    Public Function GetData(ByVal cmd As SqlCommand) As DataTable

        Dim dt As New DataTable
        Dim strConnString As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim con As New SqlConnection(strConnString)
        Dim sda As New SqlDataAdapter

        cmd.CommandType = CommandType.Text
        cmd.Connection = con
        Try
            con.Open()
            sda.SelectCommand = cmd
            sda.Fill(dt)
            Return dt
        Catch ex As Exception
            Response.Write(ex.Message)
            Return Nothing
        Finally
            con.Close()
            sda.Dispose()
            con.Dispose()
        End Try
    End Function
    Protected Sub download(ByVal dt As DataTable)
        Try
            Dim bytes() As Byte = CType(dt.Rows(0)("doc_file"), Byte())

            'Response.Buffer = True

            'Response.Charset = ""
            Dim fname As String = dt.Rows(0)("doc_name").ToString()
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = dt.Rows(0)("doc_type").ToString()
            Response.AddHeader("content-disposition", "attachment;filename=" & fname)
            Response.BinaryWrite(bytes)

            Response.Flush()
            Response.Close()

        Catch ex As Exception
            Response.Write(ex.Message)
        Finally



        End Try

    End Sub
    Protected Sub imgF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select doc_file,doc_name,doc_type from SEN_ACE_STUD_DOCUMENT where doc_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If
    End Sub
    Protected Sub txtfile_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select doc_file,doc_name,doc_type from SEN_ACE_STUD_DOCUMENT where doc_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetData(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If

    End Sub
    Protected Sub gvSkill_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSkill.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim txtfile As LinkButton = e.Row.FindControl("txtfile")
                Dim img As ImageButton = e.Row.FindControl("imgF")
                Dim s As String = System.IO.Path.GetExtension(txtfile.Text)
                If s = ".xls" Then
                    img.ImageUrl = "~/Curriculum/images/xls.jpg"
                ElseIf s = ".doc" Then
                    img.ImageUrl = "~/Curriculum/images/doc.jpg"
                ElseIf s = ".pdf" Then
                    img.ImageUrl = "~/Curriculum/images/pdf.jpg"
                Else
                    img.ImageUrl = ""
                End If


                'Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                ScriptManager1.RegisterPostBackControl(txtfile)
                ScriptManager1.RegisterPostBackControl(img)


            End If
        Catch ex As Exception
        End Try


    End Sub

    Protected Sub ddlcategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcategory.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub gvSkill_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvSkill.RowDeleting
        Try
            gvSkill.SelectedIndex = e.RowIndex


            Dim row As GridViewRow = gvSkill.Rows(e.RowIndex)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("lblSksId"), Label)

            If lblID.Text <> "" Then
                Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
                Dim str_query As String = ""

                str_query = "DELETE FROM SEN_ACE_STUD_DOCUMENT WHERE doc_ID=" & lblID.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            End If
            gridbind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvSkill_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSkill.RowCommand


        Try
            If e.CommandName = "View" Then

                Dim i As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvSkill.Rows(i), GridViewRow)

                Dim lblId As New Label
                lblId = selectedRow.FindControl("lblSksId")
                Dim strQuery As String = "select doc_file,doc_name,doc_type from SEN_ACE_STUD_DOCUMENT where doc_id=" & lblId.Text

                Dim cmd As SqlCommand = New SqlCommand(strQuery)

                Dim dt As DataTable = GetData(cmd)

                If dt IsNot Nothing Then

                    download(dt)

                End If



            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
