<%@ Page Language="VB" AutoEventWireup="false" CodeFile="studSEN_Details.aspx.vb" Inherits="studSEN_Details" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title>::GEMS PHOENIX::Update Student Details:: </title>

    <script language="javascript" type="text/javascript">
        function closew() {
            if (confirm('Do you want to exit') == true) {
                //window.returnValue = true
                //window.close()
                CloseFrame();
            }
            else {
                return false
            }
        }

        function CloseFrame() {
            parent.CloseFrame();
        }
    </script>


    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <%-- <link href="../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/example.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            border-style: none;
            border-color: inherit;
            border-width: 0;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 9px;
            background-image: url('../Images/bgblue.gif');
            background-repeat: repeat-x;
            font-weight: bold;
            color: BLACK;
            height: 16px;
        }
     table.BlueTable_SS
{
	BORDER-RIGHT: #1b80b6 1pt solid;
	padding: 2px; border-collapse:collapse;
	BORDER-BOTTOM: #1b80b6 1pt solid; 
	-moz-border-radius:0;
} 
table.BlueTable_SS td 
{
	border-width: 1px;padding: 2px; 	
	border-left: #1b80b6 1pt solid; border-top: #1b80b6 1pt solid;
	border-bottom : #1b80b6 1pt solid;
	-moz-border-radius:0;
}
        .style3
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 9px;
            font-weight: bold;
            color: #1B80B6;
            }
        .style4
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 9px;
            font-weight: bold;
            color: #1B80B6;
            width: 184px;
        }
        </style>--%>
</head>
<base target="_self" />
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>

        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr class="title-bg-lite">
                <td align="left" valign="middle">KHDA - SEN Profile</td>
            </tr>
        </table>





        <table id="Table2" border="0" cellpadding="0" cellspacing="0" width="100%" runat="server">
            <tr>
                <td align="left">
                    <span class="field-label">Name</span></td>

                <td colspan="5" align="left">
                    <asp:Literal ID="ltStudName" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td align="left">
                    <span class="field-label">Student ID</span> </td>

                <td colspan="0" align="left">
                    <asp:Literal ID="ltStudId" runat="server"></asp:Literal></td>
                <td align="left">
                    <span class="field-label">Grade</span></td>

                <td colspan="0" align="left">
                    <asp:Literal ID="ltGrd" runat="server"></asp:Literal></td>
                <td align="left">
                    <span class="field-label">Section</span></td>

                <td align="left">
                    <asp:Literal ID="ltSct" runat="server"></asp:Literal></td>
            </tr>
        </table>

           <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr class="title-bg-lite">
                <td align="left" valign="middle">SEND Categories</td>
            </tr>
        </table>

        <table id="table1" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left" width="30%"><span class="field-label">SEND </span></td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rdSen_Yes" runat="server" GroupName="sen"
                        Text="Yes" />
                    <asp:RadioButton ID="rdSen_No" runat="server" GroupName="sen" Text="No" /></td>
                <td align="left" width="50%" colspan="2">General Comments
                    <br />
                    <asp:TextBox ID="txtSen" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
               
            </tr>
            <tr>
                <td align="left" width="30%"><span class="field-label">Wave </span></td>
               <td colspan="3">
                   <asp:RadioButtonList ID="rdblWave" runat="server" RepeatDirection="Horizontal">
                       <asp:ListItem Value="1">Wave 1</asp:ListItem>
                       <asp:ListItem Value="2">Wave 2</asp:ListItem>
                       <asp:ListItem Value="3">Wave 3</asp:ListItem>
                   </asp:RadioButtonList>
               </td>
                </tr>

            <tr>
                <td align="left" width="30%"> <span class="field-label">Behavioural, Social, Emotional</span></td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rbBEHAVE_Yes" runat="server" GroupName="BEHAVE"
                        Text="Yes" />
                    <asp:RadioButton ID="rbBEHAVE_No" runat="server" GroupName="BEHAVE" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtBEHAVE_Note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                 <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtBehave_Action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>


            <tr>
                <td align="left" width="30%"><span class="field-label">Sensory (Visual & Hearing Impairment)</span> </td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rbSENSORY_Yes" runat="server" GroupName="Sensory"
                        Text="Yes" />
                    <asp:RadioButton ID="rbSENSORY_No" runat="server" GroupName="Sensory" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtSensory_Note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                 <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtSensory_action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>
              <tr>
                <td align="left" width="30%"><span class="field-label">Hearing Impairment (sensory impairment)</span> </td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rbHearingImpairmentYes" runat="server" GroupName="HearingImp"
                        Text="Yes" />
                    <asp:RadioButton ID="rbHearingImpairmentNo" runat="server" GroupName="HearingImp" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtHearing_Note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                    <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtHearing_action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>
              <tr>
                <td align="left" width="30%"><span class="field-label">Physical disability</span> </td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rbDisabled_Yes" runat="server" GroupName="Disabled"
                        Text="Yes" />
                    <asp:RadioButton ID="rbDisabled_No" runat="server" GroupName="Disabled" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtDisabled_Note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                   <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtDisabled_Action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" width="30%"><span class="field-label">Medical Conditions or Health Related Disabilities</span></td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rbMedical_Yes" runat="server" GroupName="Medical"
                        Text="Yes" />
                    <asp:RadioButton ID="rbMedical_No" runat="server" GroupName="Medical" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtMedical_Note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                 <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtMedical_Action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" width="30%"><span class="field-label">Speech and language disorders </span></td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rdSpeech_Yes" runat="server" GroupName="Speech"
                        Text="Yes" />
                    <asp:RadioButton ID="rdSpeech_No" runat="server" GroupName="Speech" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtSpeech_Note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                  <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtSpeech_Action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>


            <tr>
                <td align="left" width="30%"><span class="field-label">Communication and Interaction(eg.,ASD)</span></td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rbCommunication_Yes" runat="server" GroupName="Communication"
                        Text="Yes" />
                    <asp:RadioButton ID="rbCommunication_No" runat="server" GroupName="Communication" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtCommunication_Note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                  <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtCommunication_Action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>



            <tr>
                <td align="left" width="30%"><span class="field-label">General learning difficulties</span></td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rbLearning_Yes" runat="server" GroupName="Learning"
                        Text="Yes" />
                    <asp:RadioButton ID="rbLearning_No" runat="server" GroupName="Learning" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtLearning_Note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                 <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtLearning_action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>

            <tr>
                <td align="left" width="30%"><span class="field-label">Specific learning difficulties </span> </td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rdspecificLearn_Yes" runat="server" GroupName="spLearn"
                        Text="Yes" />
                    <asp:RadioButton ID="rdspecificLearn_No" runat="server" GroupName="spLearn" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtSpecificLEarn_Note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                 <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtSpecificLEarn_Action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>
             <tr>
                <td align="left" width="30%"><span class="field-label">Profound and Multiple Learning Difficulty (PMLD) (general learning difficulties) </span> </td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rbProfound_Yes" runat="server" GroupName="spProfound"
                        Text="Yes" />
                    <asp:RadioButton ID="rbProfound_No" runat="server" GroupName="spProfound" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtProfound_Note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                  <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtProfound_Action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" width="30%"><span class="field-label">Assessed Syndrome (general learning difficulties)</span> </td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rb_AssessedYes" runat="server" GroupName="spAssessed"
                        Text="Yes" />
                    <asp:RadioButton ID="rb_AssessedNo" runat="server" GroupName="spAssessed" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtAssesses_Note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                 <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtAssesses_Action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" width="30%"><span class="field-label">Dyslexia- Reading (specific learning difficulty)</span> </td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rb_DyslexiaYes" runat="server" GroupName="spDyslex"
                        Text="Yes" />
                    <asp:RadioButton ID="rb_Dyslexia_No" runat="server" GroupName="spDyslex" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtDyslexia_Note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtDyslexia_Action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>
             <tr>
                <td align="left" width="30%"><span class="field-label">Dysgraphia- Writing (specific learning difficulty)</span> </td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rb_DysgraphiaYes" runat="server" GroupName="spDysgrap"
                        Text="Yes" />
                    <asp:RadioButton ID="rb_DysgraphiaNo" runat="server" GroupName="spDysgrap" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtDysgraphia_Note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                  <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtDysgraphia_Action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>
             <tr>
                <td align="left" width="30%"><span class="field-label">Dyscalculia- Mathematics (specific learning difficulty)</span> </td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rb_DyscalculiaYes" runat="server" GroupName="spDyscalculia"
                        Text="Yes" />
                    <asp:RadioButton ID="rb_DyscalculiaNo" runat="server" GroupName="spDyscalculia" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtDyscalculia_note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                   <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtDyscalculia_Action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>
            </table>
           <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr class="title-bg-lite">
                <td align="left" valign="middle">Gifted & Talented Categories</td>
            </tr>
        </table>
        <table id="table12" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left" width="30%"><span class="field-label">Is the child gifted and talented?</span></td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rbGifted_Yes" runat="server" GroupName="Gifted"
                        Text="Yes" />
                    <asp:RadioButton ID="rbGifted_No" runat="server" GroupName="Gifted" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtGifted_Note" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                 <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtGifted_Action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>
            </table>
             <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr class="title-bg-lite">
                <td align="left" valign="middle">ELL Categories</td>
            </tr>
        </table>
         <table id="table4" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left" width="30%"><span class="field-label">Student require English support </span></td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rdEAL_YES" runat="server" GroupName="eal"
                        Text="Yes" />
                    <asp:RadioButton ID="rdEAL_NO" runat="server" GroupName="eal" Text="No" /></td>
                <td align="left" width="25%">General Comments
                    <br />
                    <asp:TextBox ID="txtEAL" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
                  <td align="left" width="25%">Action Taken
                    <br />
                    <asp:TextBox ID="txtEAL_Action" runat="server" TextMode="MultiLine"
                        Rows="4" MaxLength="255"></asp:TextBox></td>
            </tr>
              <tr>
                <td align="left" width="30%"><span class="field-label">ELL Wave </span></td>
               <td colspan="3">
                   <asp:RadioButtonList ID="rblELLWave" runat="server" RepeatDirection="Horizontal">
                       <asp:ListItem Value="1">Wave 1</asp:ListItem>
                       <asp:ListItem Value="2">Wave 2</asp:ListItem>
                       <asp:ListItem Value="3">Wave 3</asp:ListItem>
                   </asp:RadioButtonList>
               </td>
                </tr>

        </table>

        <table id="table3" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="center">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                        ValidationGroup="gr" />
                    &nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="button" OnClientClick="return closew();"
                        Text="Cancel" />
                </td>
            </tr>

            <tr>

                <td align="center">
                    <asp:Label ID="lblerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                </td>
            </tr>


        </table>
        <asp:HiddenField ID="HF_stuid" runat="server" />
        <asp:HiddenField ID="HF_SibId" runat="server" />


        <%--    <table border="1" bordercolor="#1b80b6" cellpadding="0" cellspacing="0"
            style="border-right: 0px solid; border-top: #d0e0ff 0px solid; border-left: 0px solid; border-bottom: 1px solid" width="100%">
            <tr style="border-right: 0px; border-top: 0px; border-left: 0px">
                <td align="left" colspan="6" style="border-right: 0px; border-top: 0px; font-weight: bold;
                    font-size: 8pt; background-image: url(../../Images/bgblue.gif); border-left: 0px;
                    color: white; font-family: Verdana; height: 30px">
                    Contact Details</td>
            </tr>
            <tr>
                <td class="matters" colspan="6" rowspan="4" valign="top">
                &nbsp;</td>
            </tr>
        </table>--%>
    </form>
</body>
</html>
