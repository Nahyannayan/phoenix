Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_semInclusionMembers
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            MainGridView()

        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub MainGridView()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim str_query = "select a.dept_id,a.dept_desc,(c.emp_salute +' '+c.emp_fname +' '+c.emp_mname+' ' +c.emp_lname) as empname from SEN_DEPARTMENTS a " & _
                        " left join SEN_DEPARTMENT_HOD b on a.DEPT_ID=b.EMP_DEPT_ID and b.EMP_BSU_ID='" & Hiddenbsuid.Value & "'" & _
                        " left join oasis.dbo.employee_m c on c.emp_id=b.emp_id  "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GrdView.DataSource = ds
        GrdView.DataBind()
        For Each row As GridViewRow In GrdView.Rows
            Dim deptid As String = DirectCast(row.FindControl("Hiddendeptid"), HiddenField).Value
            Dim GridMembers As GridView = DirectCast(row.FindControl("GridMembers"), GridView)
            str_query = "SELECT * FROM SEN_DEPARTMENT_MEMBERS A  " & _
                                       " INNER JOIN OASIS.DBO.EMPLOYEE_M B ON A.EMP_ID = B.EMP_ID " & _
                                       " INNER JOIN OASIS.DBO.EMPDESIGNATION_M C ON B.EMP_DES_ID=C.DES_ID " & _
                                       " WHERE A.EMP_BSU_ID='" & Hiddenbsuid.Value & "' AND A.EMP_DEPT_ID='" & deptid & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            GridMembers.DataSource = ds
            GridMembers.DataBind()

        Next

    End Sub

End Class
