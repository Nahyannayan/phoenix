Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Math
Imports System.IO

Partial Class SenRegister_UserControls_senACe
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindPrimaryDetails()
            BindControls()
            BindAceDetails()
            BindNotes()
            BindACeResults()
            BindStuStrength()
            BindStuDevelopment()
            BindPreviousSchools()
            BindRecommendationsMaster()
            BindRecommendataion()
            BindConcerninfo()
            BindScreeningAce()
            DepEntry()
            LockFileCheck()


        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(LinkFileInfo)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(T4lnkSRReportinfo)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(T4lnkSRResultsinfo)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(T4lnkSRfeedbackinfo)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(T4lnkRecomPath)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(t4lnkconcerninfo)

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(T4btnResultsSave)

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(T4btnscreeningsave)
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub


    Public Function ExactAge(ByVal BirthDate As Object, ByVal Dateofjoin As Object) As String

        Dim yer As Integer, mon As Integer, d As Integer
        Dim dt As Date
        Dim sAns As String

        If Not IsDate(BirthDate) Then Exit Function
        dt = CDate(BirthDate)
        If dt > Now Then Exit Function

        yer = Year(dt)
        mon = Month(dt)
        d = Day(dt)
        yer = (Convert.ToDateTime(Dateofjoin).Year) - yer
        mon = (Convert.ToDateTime(Dateofjoin).Month) - mon
        d = (Convert.ToDateTime(Dateofjoin).Day) - d


        If Sign(d) = -1 Then
            d = 30 - Abs(d)
            mon = mon - 1
        End If

        If Sign(mon) = -1 Then
            mon = 12 - Abs(mon)
            yer = yer - 1
        End If

        sAns = yer & " year(s) " & mon & " month(s) " & d _
         & " day(s) old."
        lblagetesting.Text = sAns
        ExactAge = sAns

    End Function

    Public Sub DepEntry()

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT ENTRY_ACE FROM SEN_ACE_STUDENTS WHERE STU_ID='" & Hiddenstuid.Value & "' "
        Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)

        If Not IsDBNull(val) Then
            T4lblDateenteredACe.Text = Convert.ToDateTime(val).ToString("dd/MMM/yyyy")
            Try
                If T4lblDateenteredACe.Text.Trim() <> "" Then
                    T4lblTimeLag.Text = DateDiff(DateInterval.Day, Convert.ToDateTime(T4lbldateentered.Text), Convert.ToDateTime(T4lblDateenteredACe.Text), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.System) & " Days"
                End If
            Catch ex As Exception

            End Try
        End If


    End Sub


    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then
            T4refsave.Enabled = False
            T4btnscreeningsave.Enabled = False
            T4btnResultsSave.Enabled = False
            T4btnSaveRecom.Enabled = False
            T4btnConsernInfoSave.Enabled = False
            T4btnstrsave.Enabled = False
            T4btnStuDelpSave.Enabled = False
            lnkentrysave.Enabled = False
        End If



    End Sub

    Public Sub BindScreeningAce()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT * FROM SEN_DEPARTMENT_ACE_SCREENING WHERE STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            T4CheckBoxScreenedbyace.Checked = True
            t4txtscreeningdate.Text = ds.Tables(0).Rows(0).Item("DATE").ToString()
            T4txtscreeningType.Text = ds.Tables(0).Rows(0).Item("TYPE").ToString()
            If t4txtscreeningdate.Text <> "" Then
                str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                strQuery = "select STU_DOB from student_m " & _
                                         " where STU_ID='" & Hiddenstuid.Value & "'"
                Dim dob As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)
                If dob.Trim() <> "" Then
                    ExactAge(dob.Trim(), t4txtscreeningdate.Text.Trim())
                End If
            End If

        End If

    End Sub
    Public Sub BindConcerninfo()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT * FROM SEN_DEPARTMENT_ACE_CONCERN_INFO WHERE STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            t4hidconcerninfo.Value = ds.Tables(0).Rows(0).Item("CONCERN_INFO").ToString()
            T4txtConsernInfo.Text = ds.Tables(0).Rows(0).Item("CONCERN_INFO").ToString()
            'T4txtConsernInfo.Visible = False
            'T4btnConsernInfoSave.Visible = False
            If T4txtConsernInfo.Text.Trim() <> "" Then
                't4lnkconcerninfo.Visible = True
            Else

                t4lnkconcerninfo.Visible = False
            End If

        End If

    End Sub
    Public Sub BindRecommendataion()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT * FROM SEN_DEPARTMENT_ACE_RECOMMENDATION WHERE STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        If ds.Tables(0).Rows.Count > 0 Then

            T4ddRecom.SelectedValue = ds.Tables(0).Rows(0).Item("RECOM_ACTION").ToString()
            T4txtdateRecom.Text = ds.Tables(0).Rows(0).Item("DATE_RECOM").ToString()
            T4txtdateRecomDone.Text = ds.Tables(0).Rows(0).Item("DATE_RECOM_DONE").ToString()
            T4txtdateRecomResults.Text = ds.Tables(0).Rows(0).Item("RECOM_RESULT").ToString()
            T4hidRecomPath.Value = ds.Tables(0).Rows(0).Item("RECOM_FILE_INFO").ToString()
            If T4hidRecomPath.Value <> "" Then
                'T4lnkRecomPath.Visible = True
                T4txtRecomFileInfo.Text = ds.Tables(0).Rows(0).Item("RECOM_FILE_INFO").ToString()
            Else
                T4lnkRecomPath.Visible = False
            End If

        End If

        ''Display Alert
        Alert.Visible = False
        If T4txtdateRecom.Text <> "" Then

            If T4txtdateRecom.Text < Today.Date Then
                Alert.Visible = True
            End If

        End If


    End Sub
    Public Sub BindRecommendationsMaster()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT * FROM SEN_DEPARTMENT_ACE_RECOMMENDATION_MASTER WHERE RECOMMENDATION_BSU_ID='" & Hiddenbsuid.Value & "' "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        If ds.Tables(0).Rows.Count > 0 Then
            T4ddRecom.DataSource = ds
            T4ddRecom.DataTextField = "RECOMMENDATION"
            T4ddRecom.DataValueField = "RECOMMENDATION_ID"
            T4ddRecom.DataBind()

        End If
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Recommendation Actions"
        T4ddRecom.Items.Insert(0, list)


    End Sub

    Public Sub BindPreviousSchools()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " Select STU_PREVSCHI,STU_PREVSCHI_FROMDATE,STU_PREVSCHI_TODATE,STU_PREVSCHI_bSPMEDICATION,STU_PREVSCHI_SPMEDICATION_REMARKS from oasis.dbo.student_m where stu_id='" & Hiddenstuid.Value & "'union ALL " & _
                                 " Select STU_PREVSCHII,STU_PREVSCHII_FROMDATE,STU_PREVSCHII_TODATE,STU_PREVSCHII_bSPMEDICATION,STU_PREVSCHII_SPMEDICATION_REMARKS from oasis.dbo.student_m where stu_id='" & Hiddenstuid.Value & "'union ALL " & _
                                 " Select STU_PREVSCHIII,STU_PREVSCHIII_FROMDATE,STU_PREVSCHIII_TODATE,STU_PREVSCHIII_bSPMEDICATION,STU_PREVSCHIII_SPMEDICATION_REMARKS from oasis.dbo.student_m where stu_id='" & Hiddenstuid.Value & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T4GrdPreviousSchools.DataSource = ds
        T4GrdPreviousSchools.DataBind()

    End Sub
    Public Sub BindPrimaryDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select c.stu_doj,b.BSU_NAME,b.BSU_SHORTNAME,ENTRY_DATE from SEN_DEPARTMENT_STUDENTS a " & _
                                 " inner join oasis.dbo.BUSINESSUNIT_M b on a.stu_bsu_id = b.bsu_id " & _
                                 " inner join oasis.dbo.student_m c on c.stu_id= a.stu_id  " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.STU_BSU_ID='" & Hiddenbsuid.Value & "' and a.dept_id='3' and a.active='true' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            T4lbldateenteredlabel.Text = "Date Entered " & ds.Tables(0).Rows(0).Item("BSU_SHORTNAME").ToString()
            T4lbldateentered.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("stu_doj").ToString()).ToString("dd/MMM/yyyy")
        End If


    End Sub
    Public Sub BindControls()

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from " & _
                                        " oasis.dbo.EMPLOYEE_M b where b.emp_bsu_id='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T4ddrefferedby.DataSource = ds
        T4ddrefferedby.DataTextField = "empname"
        T4ddrefferedby.DataValueField = "EMP_ID"
        T4ddrefferedby.DataBind()

        strQuery = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id and a.EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        T4ddStaffs.DataSource = ds
        T4ddStaffNotes.DataSource = ds

        T4ddStaffs.DataTextField = "empname"
        T4ddStaffNotes.DataTextField = "empname"


        T4ddStaffs.DataValueField = "EMP_ID"
        T4ddStaffNotes.DataValueField = "EMP_ID"


        T4ddStaffs.DataBind()
        T4ddStaffNotes.DataBind()

        Dim list As New ListItem
        list.Text = "Select an Inclusion Member"
        list.Value = "-1"
        T4ddStaffs.Items.Insert(0, list)

        Dim list1 As New ListItem
        list1.Text = "Select a referred  Member"
        list1.Value = "-1"
        T4ddrefferedby.Items.Insert(0, list1)

        Dim list2 As New ListItem
        list2.Text = "Select an Inclusion Member"
        list2.Value = "-1"
        T4ddStaffNotes.Items.Insert(0, list2)


    End Sub
    Protected Sub T4refsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4refsave.Click
        lblstatusmessage.Text = ""
        Try

            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            If T4RadioButtonListReffer.SelectedValue = "CS" Then
                pParms(2) = New SqlClient.SqlParameter("@REFERRED_BY", T4RadioButtonListReffer.SelectedValue)
                pParms(3) = New SqlClient.SqlParameter("@REFERRED_BY_CS_NAME", T4ddrefferedby.SelectedValue)

            ElseIf T4RadioButtonListReffer.SelectedValue = "PSM" Then
                pParms(2) = New SqlClient.SqlParameter("@REFERRED_BY", T4RadioButtonListReffer.SelectedValue)
                pParms(3) = New SqlClient.SqlParameter("@REFERRED_BY_PSM_NAME", T4txtrefferedby.Text.Trim())
            ElseIf T4RadioButtonListReffer.SelectedValue = "P" Then
                pParms(2) = New SqlClient.SqlParameter("@REFERRED_BY", T4RadioButtonListReffer.SelectedValue)
                pParms(3) = New SqlClient.SqlParameter("@PARENT_REFER", RadioReferparent.SelectedValue)
            Else
                pParms(2) = New SqlClient.SqlParameter("@REFERRED_BY", T4RadioButtonListReffer.SelectedValue)
                pParms(3) = New SqlClient.SqlParameter("@REFERRED_BY_O_NAME", T4txtrefferedby.Text.Trim())
            End If

            pParms(5) = New SqlClient.SqlParameter("@REFERRED_DATE", T4txtrefferdate.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@REFERRED_SUMMARY", T4txtReffersummary.Text.Trim())
            pParms(7) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T4ddStaffs.SelectedValue)
            pParms(8) = New SqlClient.SqlParameter("@FILE_INFO", txtAceinfo.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ACE_DETAILS", pParms)
            BindAceDetails()

            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Public Sub BindAceDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_ACE_DETAILS where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        LinkFileInfo.Visible = False
        If ds.Tables(0).Rows.Count > 0 Then
            Dim val As String = ds.Tables(0).Rows(0).Item("REFERRED_BY").ToString()
            T4RadioButtonListReffer.SelectedValue = val
            If val = "CS" Then
                T4ddrefferedby.Visible = True
                T4txtrefferedby.Visible = False
                RadioReferparent.Visible = False
                T4ddrefferedby.SelectedValue = ds.Tables(0).Rows(0).Item("REFERRED_BY_CS_NAME").ToString()

            ElseIf val = "PSM" Then
                T4ddrefferedby.Visible = False
                T4txtrefferedby.Visible = True
                RadioReferparent.Visible = False
                T4txtrefferedby.Text = ds.Tables(0).Rows(0).Item("REFERRED_BY_PSM_NAME").ToString()
            ElseIf val = "P" Then
                T4ddrefferedby.Visible = False
                T4txtrefferedby.Visible = False
                RadioReferparent.Visible = True
                RadioReferparent.SelectedValue = ds.Tables(0).Rows(0).Item("PARENT_REFER").ToString()
            Else
                T4ddrefferedby.Visible = False
                T4txtrefferedby.Visible = True
                RadioReferparent.Visible = False
                T4txtrefferedby.Text = ds.Tables(0).Rows(0).Item("REFERRED_BY_O_NAME").ToString()
            End If

            txtAceinfo.Text = ds.Tables(0).Rows(0).Item("FILE_INFO").ToString()

            If txtAceinfo.Text.Trim() <> "" Then
                'txtAceinfo.Visible = False
                'LinkFileInfo.Visible = True
            Else
                LinkFileInfo.Visible = False
                'txtAceinfo.Visible = True
            End If

            T4txtrefferdate.Text = ds.Tables(0).Rows(0).Item("REFERRED_DATE").ToString()
            T4txtReffersummary.Text = ds.Tables(0).Rows(0).Item("REFERRED_SUMMARY").ToString()

            Try
                T4ddStaffs.Items.FindByValue(ds.Tables(0).Rows(0).Item("ENTRY_EMP_ID").ToString()).Selected = True

            Catch ex As Exception

            End Try

            'T4refsave.Visible = False
            'T4RadioButtonListReffer.Enabled = False
        Else
            'T4RadioButtonListReffer.Enabled = True
            'T4refsave.Visible = True
        End If
    End Sub
    Public Sub BindNotes()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select NOTES_TEXT,ENTRY_DATE, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname , '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(NOTES_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview from SEN_DEPARTMENT_ACE_NOTES a inner join oasis.dbo.EMPLOYEE_M b on  a.ENTRY_EMP_ID=b.emp_id where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "' order by A.ENTRY_DATE desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T4GrdNotes.DataSource = ds
        T4GrdNotes.DataBind()
    End Sub
    Public Sub BindStuStrength()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select COMMENTS_TEXT,ENTRY_DATE,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(COMMENTS_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview from SEN_DEPARTMENT_ACE_STU_INT_STR where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' order by ENTRY_DATE desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T4GrdStuStrength.DataSource = ds
        T4GrdStuStrength.DataBind()

    End Sub
    Public Sub BindStuDevelopment()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select COMMENTS_TEXT,ENTRY_DATE,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(COMMENTS_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview from SEN_DEPARTMENT_ACE_STU_AREA_DEVELOPMENT where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' order by ENTRY_DATE desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T4Grdstudelp.DataSource = ds
        T4Grdstudelp.DataBind()

    End Sub
    Public Sub BindACeResults()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_ACE_RESULTS where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            'T4txtBPVS.Text = ds.Tables(0).Rows(0).Item("BPVS").ToString()
            T4txtNNAT.Text = ds.Tables(0).Rows(0).Item("NNAT").ToString()
            T4txtSpelling.Text = ds.Tables(0).Rows(0).Item("SPELLING").ToString()
            T4txtWordID.Text = ds.Tables(0).Rows(0).Item("WORDID").ToString()
            T4txtWordAttack.Text = ds.Tables(0).Rows(0).Item("WORDATTACK").ToString()
            T4txtDigitMemory.Text = ds.Tables(0).Rows(0).Item("DIGITMEMORY").ToString()
            T4txtNumber.Text = ds.Tables(0).Rows(0).Item("NUMBER").ToString()
            T4txtNonwordRepetition.Text = ds.Tables(0).Rows(0).Item("NONWORDREPTITION").ToString()
            'T4txtBVP.Text = ds.Tables(0).Rows(0).Item("BERRY_VISUAL_PRECEPTION").ToString()
            T4txtBMD.Text = ds.Tables(0).Rows(0).Item("BERRY_MOTOR_DEVELOPMENT").ToString()
            t4ddDIIndex.SelectedValue = ds.Tables(0).Rows(0).Item("DI_INDEX").ToString()
            t4txtIQEstimate.Text = ds.Tables(0).Rows(0).Item("IQ_ESTIMATE").ToString()
            t4txtexpattscore.Text = ds.Tables(0).Rows(0).Item("EXPECTED_ATTAINMENT_SCORE").ToString()
            t4txtother.Text = ds.Tables(0).Rows(0).Item("OTHER").ToString()
            t4txtRdate.Text = ds.Tables(0).Rows(0).Item("DATE").ToString()
            T4txtAge.Text = ds.Tables(0).Rows(0).Item("AGE").ToString()
            T4hidSRReportinfo.Value = ds.Tables(0).Rows(0).Item("FILE_INFO_REPORT").ToString()
            T4hidSRResultsinfo.Value = ds.Tables(0).Rows(0).Item("FILE_INFO_RESULTS").ToString()
            T4hidSRfeedbackinfo.Value = ds.Tables(0).Rows(0).Item("FILE_INFO_FEEDBACK").ToString()

            If T4hidSRReportinfo.Value <> "" Then
                'T4lnkSRReportinfo.Visible = True
                T4txtSRReportinfo.Text = ds.Tables(0).Rows(0).Item("FILE_INFO_REPORT").ToString()
            Else
                T4lnkSRReportinfo.Visible = False

            End If

            If T4hidSRResultsinfo.Value <> "" Then
                'T4lnkSRResultsinfo.Visible = True
                T4txtSRResultsinfo.Text = ds.Tables(0).Rows(0).Item("FILE_INFO_RESULTS").ToString()
            Else
                T4lnkSRResultsinfo.Visible = False

            End If

            If T4hidSRfeedbackinfo.Value <> "" Then
                'T4lnkSRfeedbackinfo.Visible = True
                T4txtSRfeedbackinfo.Text = ds.Tables(0).Rows(0).Item("FILE_INFO_FEEDBACK").ToString()
            Else
                T4lnkSRfeedbackinfo.Visible = False

            End If



            'T4btnResultsSave.Visible = False
        Else
            'T4btnResultsSave.Visible = True
        End If

    End Sub
    Protected Sub T4RadioButtonListReffer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4RadioButtonListReffer.SelectedIndexChanged
        If T4RadioButtonListReffer.SelectedValue = "CS" Then
            T4ddrefferedby.Visible = True
            T4txtrefferedby.Visible = False
            RadioReferparent.Visible = False
        ElseIf T4RadioButtonListReffer.SelectedValue = "PSM" Then
            T4ddrefferedby.Visible = False
            T4txtrefferedby.Visible = True
            RadioReferparent.Visible = False
        ElseIf T4RadioButtonListReffer.SelectedValue = "P" Then
            T4ddrefferedby.Visible = False
            T4txtrefferedby.Visible = False
            RadioReferparent.Visible = True
        Else
            T4ddrefferedby.Visible = False
            T4txtrefferedby.Visible = True
            RadioReferparent.Visible = False
        End If
    End Sub

    Protected Sub T4NotesSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4NotesSave.Click
        lblstatusmessage.Text = ""
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@NOTES_TEXT", T4Notes.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T4ddStaffNotes.SelectedValue)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ACE_NOTES", pParms)
            T4Notes.Text = ""
            BindNotes()
            lblstatusmessage.Text = "Successfully Saved"
        Catch ex As Exception

        End Try

    End Sub




    Protected Sub T4btnResultsSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4btnResultsSave.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(21) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        'pParms(2) = New SqlClient.SqlParameter("@BPVS", T4txtBPVS.Text.Trim())
        pParms(3) = New SqlClient.SqlParameter("@NNAT", T4txtNNAT.Text.Trim())
        pParms(4) = New SqlClient.SqlParameter("@SPELLING", T4txtSpelling.Text.Trim())
        pParms(5) = New SqlClient.SqlParameter("@WORDID", T4txtWordID.Text.Trim())
        pParms(6) = New SqlClient.SqlParameter("@WORDATTACK", T4txtWordAttack.Text.Trim())
        pParms(7) = New SqlClient.SqlParameter("@DIGITMEMORY", T4txtDigitMemory.Text.Trim())
        pParms(8) = New SqlClient.SqlParameter("@NUMBER", T4txtNumber.Text.Trim())
        pParms(9) = New SqlClient.SqlParameter("@NONWORDREPTITION", T4txtNonwordRepetition.Text.Trim())
        'pParms(10) = New SqlClient.SqlParameter("@BERRY_VISUAL_PRECEPTION", T4txtBVP.Text.Trim())
        pParms(11) = New SqlClient.SqlParameter("@BERRY_MOTOR_DEVELOPMENT", T4txtBMD.Text.Trim())
        pParms(12) = New SqlClient.SqlParameter("@DI_INDEX", t4ddDIIndex.SelectedValue.Trim())
        pParms(13) = New SqlClient.SqlParameter("@IQ_ESTIMATE", t4txtIQEstimate.Text.Trim())
        pParms(14) = New SqlClient.SqlParameter("@EXPECTED_ATTAINMENT_SCORE", t4txtexpattscore.Text.Trim())
        pParms(15) = New SqlClient.SqlParameter("@OTHER", t4txtother.Text.Trim())
        pParms(16) = New SqlClient.SqlParameter("@DATE", t4txtRdate.Text.Trim())
        pParms(17) = New SqlClient.SqlParameter("@AGE", T4txtAge.Text.Trim())
        pParms(18) = New SqlClient.SqlParameter("@FILE_INFO_REPORT", T4txtSRReportinfo.Text.Trim())
        pParms(19) = New SqlClient.SqlParameter("@FILE_INFO_RESULTS", T4txtSRResultsinfo.Text.Trim())
        pParms(20) = New SqlClient.SqlParameter("@FILE_INFO_FEEDBACK", T4txtSRfeedbackinfo.Text.Trim())

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ACE_RESULTS", pParms)
        'BindACeResults()
        Session("TabIndex") = 3
        'Dim Encr_decrData As New Encryption64
        'Dim stu_id As String = Encr_decrData.Encrypt(Hiddenstuid.Value)
        'Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        'Response.Redirect("senStudentProfile.aspx?Stu_id=" & stu_id & mInfo)
        Response.Write("<script type='text/javascript'> parent.location.reload() </script>")
    End Sub

    Protected Sub T4btnstrsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4btnstrsave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@COMMENTS_TEXT", T4txtcommentstrength.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ACE_STU_INT_STR", pParms)
            T4txtcommentstrength.Text = ""
            BindStuStrength()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T4btnStuDelpSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4btnStuDelpSave.Click

        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@COMMENTS_TEXT", T4txtstudelp.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ACE_STU_AREA_DEVELOPMENT", pParms)
            T4txtstudelp.Text = ""
            BindStuDevelopment()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub


    Protected Sub T4GrdNotes_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T4GrdNotes.PageIndexChanging
        T4GrdNotes.PageIndex = e.NewPageIndex
        BindNotes()
    End Sub

    Protected Sub T4GrdStuStrength_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T4GrdStuStrength.PageIndexChanging
        T4GrdStuStrength.PageIndex = e.NewPageIndex
        BindStuStrength()
    End Sub

    Protected Sub T4Grdstudelp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T4Grdstudelp.PageIndexChanging
        T4Grdstudelp.PageIndex = e.NewPageIndex
        BindStuDevelopment()
    End Sub

    Protected Sub LinkFileInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkFileInfo.Click
        Try
            Dim path = txtAceinfo.Text.Trim()

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()

            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub T4lnkSRReportinfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4lnkSRReportinfo.Click
        Try
            Dim path = T4hidSRReportinfo.Value

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub T4lnkSRResultsinfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4lnkSRResultsinfo.Click
        Try
            Dim path = T4hidSRResultsinfo.Value

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub T4lnkSRfeedbackinfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4lnkSRfeedbackinfo.Click
        Try
            Dim path = T4hidSRfeedbackinfo.Value
            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub T4btnSaveRecom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4btnSaveRecom.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@RECOM_ACTION", T4ddRecom.SelectedValue)
        pParms(3) = New SqlClient.SqlParameter("@DATE_RECOM", T4txtdateRecom.Text.Trim())
        pParms(4) = New SqlClient.SqlParameter("@DATE_RECOM_DONE", T4txtdateRecomDone.Text.Trim())
        pParms(5) = New SqlClient.SqlParameter("@RECOM_RESULT", T4txtdateRecomResults.Text.Trim())
        pParms(6) = New SqlClient.SqlParameter("@RECOM_FILE_INFO", T4txtRecomFileInfo.Text.Trim())
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ACE_RECOMMENDATION", pParms)
        BindRecommendataion()

        Dim pParms1(4) As SqlClient.SqlParameter
        pParms1(0) = New SqlClient.SqlParameter("@SUB_MONITOR_ID", 1) 'Recommendations monitoring update
        pParms1(1) = New SqlClient.SqlParameter("@SUB_MONITOR_STU_ID", Hiddenstuid.Value)
        pParms1(2) = New SqlClient.SqlParameter("@SUB_MONITOR_BSU_ID", Hiddenbsuid.Value)
        pParms1(3) = New SqlClient.SqlParameter("@SUB_MONITOR_ACTIVE", True)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_SUB_MONITORING_DATA", pParms1)

        Session("TabIndex") = 3
        'Dim Encr_decrData As New Encryption64
        'Dim stu_id As String = Encr_decrData.Encrypt(Hiddenstuid.Value)
        'Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        'Response.Redirect("senStudentProfile.aspx?Stu_id=" & stu_id & mInfo)
        Response.Write("<script type='text/javascript'> parent.location.reload() </script>")

    End Sub

    Protected Sub T4lnkRecomPath_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4lnkRecomPath.Click
        Try
            Dim path = T4hidRecomPath.Value
            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub T4btnConsernInfoSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4btnConsernInfoSave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@CONCERN_INFO", T4txtConsernInfo.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ACE_CONCERN_INFO", pParms)
            BindConcerninfo()
            lblstatusmessage.Text = "Successfully Saved"
        Catch ex As Exception

        End Try


    End Sub

    Protected Sub t4lnkconcerninfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles t4lnkconcerninfo.Click
        Try
            Dim path = t4hidconcerninfo.Value
            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub T4btnscreeningsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4btnscreeningsave.Click

        If T4CheckBoxScreenedbyace.Checked Then

            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@DATE", t4txtscreeningdate.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@TYPE", T4txtscreeningType.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ACE_SCREENING", pParms)
            'BindScreeningAce()

            'If t4txtscreeningdate.Text <> "" Then
            '    str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            '    Dim strQuery As String = "select STU_DOB from student_m " & _
            '                             " where STU_ID='" & Hiddenstuid.Value & "'"
            '    Dim dob As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)
            '    If dob.Trim() <> "" Then
            '        ExactAge(dob.Trim(), t4txtscreeningdate.Text.Trim())
            '    End If
            'End If

        Else

            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim strQuery As String = "Delete SEN_DEPARTMENT_ACE_SCREENING where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)

        End If

        Session("TabIndex") = 3
        'Dim Encr_decrData As New Encryption64
        'Dim stu_id As String = Encr_decrData.Encrypt(Hiddenstuid.Value)
        'Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        'Response.Redirect("senStudentProfile.aspx?Stu_id=" & stu_id & mInfo)
        Response.Write("<script type='text/javascript'> parent.location.reload() </script>")
    End Sub

    Protected Sub lnkentrysave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkentrysave.Click
        Try
            lblstatusmessage.Text = ""
            If T4lblDateenteredACe.Text <> "" Then
                Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
                Dim strQuery As String = "UPDATE SEN_ACE_STUDENTS SET  ENTRY_ACE='" & T4lblDateenteredACe.Text & "' WHERE STU_ID='" & Hiddenstuid.Value & "' "
                Dim val = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
                DepEntry()
                lblstatusmessage.Text = "Successfully Saved"
            End If
        Catch ex As Exception

        End Try


    End Sub

End Class
