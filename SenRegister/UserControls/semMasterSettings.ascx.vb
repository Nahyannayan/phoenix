Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_semMasterSettings
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sbsuid")
            BindLanguage()
            RecomendationActionMaster()
            InterventionsMaster()
            BindSEN()
            BindSOA()
        End If

    End Sub

    Public Sub BindSOA()

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim ds As DataSet
        Dim strQuery As String = "select CATEGORY_DES as  CATEGORY FROM SEN_DEPARTMENT_SPEC_CATEGORY_MASTER WHERE CATEGORY_BSU_ID='" & Hiddenbsuid.Value & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        Gridsoas.DataSource = ds
        Gridsoas.DataBind()

    End Sub

    Public Sub BindSEN()

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim ds As DataSet
        Dim strQuery As String = "select NEED_DES as  NEEDS FROM SEN_DEPARTMENT_MEDICAL_SPECIAL_LEARNING_MASTER WHERE NEED_BSU_ID='" & Hiddenbsuid.Value & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        GridSLnS.DataSource = ds
        GridSLnS.DataBind()

    End Sub
    Public Sub InterventionsMaster()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT CATEGORY_DES as CATEGORY FROM SEN_DEPARTMENT_INCLUSION_TRACKER_INTERVENTION_MAIN WHERE CATEGORY_BSU_ID='" & Hiddenbsuid.Value & "' "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        GridInterventions.DataSource = ds
        GridInterventions.DataBind()

    End Sub


    Public Sub RecomendationActionMaster()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT RECOMMENDATION FROM SEN_DEPARTMENT_ACE_RECOMMENDATION_MASTER WHERE RECOMMENDATION_BSU_ID='" & Hiddenbsuid.Value & "' "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        GridActions.DataSource = ds
        GridActions.DataBind()

    End Sub
    Public Sub BindLanguage()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT LANGUAGE_DESC as LANGUAGE FROM SEN_DEPARTMENT_ELL_LANGUAGES WHERE LANGUAGE_BSU_ID='" & Hiddenbsuid.Value & "' "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        GridLanguage.DataSource = ds
        GridLanguage.DataBind()


    End Sub

    Protected Sub btnlanguagesave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlanguagesave.Click
        If txtlanguage.Text.Trim() <> "" Then
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim strQuery As String = "INSERT INTO SEN_DEPARTMENT_ELL_LANGUAGES (LANGUAGE_DESC,LANGUAGE_BSU_ID) VALUES('" & txtlanguage.Text.Trim() & "','" & Hiddenbsuid.Value & "' )"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
            BindLanguage()
            lblmessage.Text = "Successfully Saved"
        End If

    End Sub


    Protected Sub btnactionssave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnactionssave.Click

        If txtactions.Text.Trim() <> "" Then
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim strQuery As String = "INSERT INTO SEN_DEPARTMENT_ACE_RECOMMENDATION_MASTER (RECOMMENDATION,RECOMMENDATION_BSU_ID) VALUES('" & txtactions.Text.Trim() & "','" & Hiddenbsuid.Value & "' )"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
            RecomendationActionMaster()
            lblmessage.Text = "Successfully Saved"
        End If


    End Sub


    Protected Sub btnintersave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnintersave.Click

        If txtinterventions.Text.Trim() <> "" Then
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim strQuery As String = "INSERT INTO SEN_DEPARTMENT_INCLUSION_TRACKER_INTERVENTION_MAIN (CATEGORY_DES,CATEGORY_BSU_ID) VALUES('" & txtinterventions.Text.Trim() & "','" & Hiddenbsuid.Value & "' )"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
            InterventionsMaster()
            lblmessage.Text = "Successfully Saved"
        End If

    End Sub


    Protected Sub btnslnssave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnslnssave.Click

        If txtslns.Text.Trim() <> "" Then
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim strQuery As String = "INSERT INTO SEN_DEPARTMENT_MEDICAL_SPECIAL_LEARNING_MASTER (NEED_DES,NEED_BSU_ID) VALUES('" & txtslns.Text.Trim() & "','" & Hiddenbsuid.Value & "' )"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
            BindSEN()
            lblmessage.Text = "Successfully Saved"
        End If

    End Sub

    Protected Sub btnsoassave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsoassave.Click
        If txtsoas.Text.Trim() <> "" Then
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim strQuery As String = "INSERT INTO SEN_DEPARTMENT_SPEC_CATEGORY_MASTER (CATEGORY_DES,CATEGORY_BSU_ID) VALUES('" & txtsoas.Text.Trim() & "','" & Hiddenbsuid.Value & "' )"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
            BindSOA()
            lblmessage.Text = "Successfully Saved"
        End If

    End Sub

End Class
