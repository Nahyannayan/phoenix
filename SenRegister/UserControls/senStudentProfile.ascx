<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senStudentProfile.ascx.vb" Inherits="SenRegister_UserControls_senStudentProfile" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="senStudentBasicInformation.ascx" TagName="senStudentBasicInformation"
    TagPrefix="uc1" %>

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

<div>
    <asp:Panel ID="Panel3" runat="server">

        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">Other Details</td>
            </tr>
            <tr>
                <td>

                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>  <span class="field-label">Nationality</span></td>
                     
                            <td>
                                <asp:Label ID="lblnationality" runat="server"></asp:Label></td>
                            <td>  <span class="field-label">School House</span></td>
                     
                            <td>
                                <asp:Label ID="lblschoolhouse" runat="server"></asp:Label></td>
                            <td>  <span class="field-label">Religion</span></td>
                    
                            <td>
                                <asp:Label ID="lblreligion" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                         <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <asp:LinkButton ID="lnksiblings" OnClientClick="javascript:return false;" runat="server">Siblings</asp:LinkButton>
                                <asp:Panel ID="Show" runat="server" CssClass="panel-cover">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="title-bg-lite">
                                                <%#Eval("dept_desc")%>
                                    Siblings Details</td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:GridView ID="GridSiblings" runat="server"  CssClass="table table-bordered table-row" AutoGenerateColumns="false" EmptyDataText="No Siblings">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <%#Eval("STU_FIRSTNAME")%>
                                                    &nbsp;
                                                    <%#Eval("STU_MIDNAME")%>
                                                    &nbsp;
                                                    <%#Eval("STU_LASTNAME")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="BSU">
                                                            <ItemTemplate>
                                                                <%#Eval("BSU_SHORTNAME")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle />
                                                    <RowStyle CssClass="griditem"  />
                                                    <SelectedRowStyle  />
                                                    <AlternatingRowStyle CssClass="griditem_alternative"  />
                                                    <EmptyDataRowStyle  />
                                                    <EditRowStyle  />
                                                    <FooterStyle  />
                                                    <PagerStyle  HorizontalAlign="Center" />

                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <ajaxToolkit:HoverMenuExtender ID="hm2" runat="server" PopupControlID="Show" PopupPosition="Left"
                                    TargetControlID="lnksiblings">
                                </ajaxToolkit:HoverMenuExtender>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="field-label">Dominant Hand</span></td>
                           
                            <td>
                                <asp:RadioButtonList ID="RadioButtonListDominantHand" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="R">Right</asp:ListItem>
                                    <asp:ListItem Value="L">Left</asp:ListItem>
                                </asp:RadioButtonList></td>
                            <td align="right" valign="top"><span class="field-label">Glasses</span></td>
                         
                            <td>
                                <asp:RadioButtonList ID="RadioButtonListGlass" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="Y">Yes</asp:ListItem>
                                    <asp:ListItem Value="N">No</asp:ListItem>
                                </asp:RadioButtonList></td>
                            <td></td>
                           
                            <td>
                                <asp:CheckBox ID="CheckBoxoutofclass" runat="server" Text="Out-Of-Class" CssClass="field-label" /></td>
                        </tr>
                    </table>
                    <asp:LinkButton ID="lnksavestudetails" runat="server" CausesValidation="False">Save</asp:LinkButton></td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel4" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">Student File Information</td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <asp:CheckBox ID="CkConInfo" runat="server" Text="Confidential Information"  CssClass="field-label"/>
                                <asp:CheckBox ID="CkConWellInfo" runat="server" Text="Confidential Wellbeing Information" CssClass="field-label" /></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="Ckmedicallearning" runat="server" Text="Relevant Medical /Learning history (as per admission form)?" CssClass="field-label" /></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="CkSpecialCircum" runat="server" Text="Special Circumstances" CssClass="field-label" /></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="CkFHACe" runat="server" Text="File Held in ACe"  CssClass="field-label"/>
                                <asp:CheckBox ID="CkFHDU" runat="server" Text="File held in DU" CssClass="field-label" />
                                <asp:CheckBox ID="CkFHLS" runat="server" Text="File Held in LS" CssClass="field-label"/></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:LinkButton ID="lnksaveinfo" runat="server" CausesValidation="False">Save</asp:LinkButton></td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <asp:Label ID="lbldateofentrylabel" runat="server" CssClass="field-label"></asp:Label></td>
                   
                            <td>
                                <asp:Label ID="lbldateofentry" runat="server"></asp:Label></td>
                             <td></td>
                             <td></td>
                        </tr>
                        <tr>
                            <td><span class="field-label">Date of Entry to Inclusion Register</span></td>
                        
                            <td>
                                <asp:Label ID="lbldateofEntryInclusion" runat="server"></asp:Label></td>
                             <td></td>
                             <td></td>
                        </tr>
                        <tr>
                            <td><span class="field-label">Time Lapse</span></td>
                         
                            <td>
                                <asp:Label ID="lbltimelapsed" runat="server"></asp:Label></td>
                              <td></td>
                             <td></td>
                        </tr>
                    </table>


                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel1" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">Areas of Activity</td>
            </tr>
            <tr>
                <td><div class="checkbox-list">
                    <asp:CheckBoxList ID="CheckBoxListDepartments" runat="server" RepeatColumns="4" RepeatDirection="Horizontal" >
                    </asp:CheckBoxList></div><br />
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td><asp:CheckBox ID="CheckAuthoritytojsp" runat="server" CssClass="field-label"/>
                               
                            </td>
                            <td><span class="field-label">Date of Authority</span> </td>
                            <td>
                                <asp:TextBox ID="txtauthdate" runat="server"></asp:TextBox></td>
                            <td><asp:Image ID="Image1" ImageUrl="~/Images/calendar.gif" runat="server" /></td>
                        </tr>
                    </table>
                    <asp:LinkButton ID="lnksave" runat="server" CausesValidation="False">Save</asp:LinkButton>

                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel5" runat="server">


        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">Contact Details</td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td><span class="field-label">Home Address</span></td>
                           
                            <td>
                                <asp:Label ID="lblhomeaddress" runat="server"></asp:Label></td>
                            <td><span class="field-label">Postal Address</span></td>
                          
                            <td>
                                <asp:Label ID="lblpostaladdress" runat="server"></asp:Label></td>
                            <td><span class="field-label">City</span></td>
                       
                            <td>
                                <asp:Label ID="lblcity" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td><span class="field-label">Home Telephone</span></td>
                        
                            <td>
                                <asp:Label ID="lblhomephone" runat="server"></asp:Label></td>
                            <td><span class="field-label">Mother Telephone</span></td>
                         
                            <td>
                                <asp:Label ID="lblmotherphone" runat="server"></asp:Label></td>
                            <td><span class="field-label">Mother Email</span></td>
                            
                            <td>
                                <asp:Label ID="lblmotheremail" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td></td>
                
                            <td></td>
                            <td><span class="field-label">Father Telephone</span></td>
                      
                            <td>
                                <asp:Label ID="lblfatherphone" runat="server"></asp:Label></td>
                            <td><span class="field-label">Father Email</span></td>
                     
                            <td>
                                <asp:Label ID="lblfatheremail" runat="server"></asp:Label></td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="Label2" runat="server"></asp:Label>
    <asp:Panel ID="PanelShow" runat="server"
         CssClass="panel-cover" Style="display: none" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">Status
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td align="center">

                                            <asp:Label ID="lblstatusmessage" runat="server" EnableTheming="False" ></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><%--<asp:Button ID="btnok" CssClass="button" runat="server" Text="Ok" Width="80px" ValidationGroup="s" OnClick="btnok_Click" />--%>
                                            <asp:Button ID="btncancel" runat="server" Text="OK" CssClass="button" ></asp:Button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btncancel" DropShadow="true" PopupControlID="PanelShow" RepositionMode="RepositionOnWindowResizeAndScroll"
        TargetControlID="Label2">
    </ajaxToolkit:ModalPopupExtender>
    <br />
    <asp:HiddenField ID="Hiddenstuid" runat="server" />
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <ajaxToolkit:CalendarExtender ID="CE1" TargetControlID="txtauthdate" PopupButtonID="Image1" runat="server" Format="dd/MMM/yyyy"></ajaxToolkit:CalendarExtender>

</div>
