<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senLSA.ascx.vb" Inherits="SenRegister_UserControls_senLSA" %>
<asp:Label ID="lblstatusmessage" runat="server" EnableTheming="False" CssClass="error"></asp:Label>
<br />
<br />
<div >
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="4">LSA Details</td>
        </tr>
        <tr>
            <td width="20%">
                <asp:Label ID="T9lbldateenteredlabel" runat="server"></asp:Label></td>
            <td width="30%">
                <asp:Label ID="T9lbldateentered" runat="server"></asp:Label></td>
            <td width="20%"><span class="field-label">Date Entered LSA</span></td>         
            <td width="30%">
                <asp:TextBox ID="T9lblDateenteredLSA" runat="server" onfocus="javascript:this.blur();return false;"></asp:TextBox>
                <asp:Image ID="Image11" runat="server" ImageUrl="~/Images/calendar.gif" />
                <asp:LinkButton ID="lnkentrysave" runat="server" CausesValidation="False">Save</asp:LinkButton>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="Image11" TargetControlID="T9lblDateenteredLSA">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td width="20%"><span class="field-label">Time Lag</span></td>           
            <td width="30%">
                <asp:Label ID="T9lblTimeLag" runat="server"></asp:Label></td>
          
        </tr>
    </table>
<br />
    <table  Width="100%">
        <tr>
            <td class="title-bg">
                LS IEP
                <asp:CheckBox ID="T9checkLSIEP" runat="server" AutoPostBack="True" />
                (File Location / Directory)</td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="T9txtLSinfo" runat="server" ></asp:TextBox>
                <asp:Button ID="T9btIEPinfoSave" runat="server" CssClass="button" Text="Save" CausesValidation="False" />
                <asp:LinkButton ID="T9lnkIEPinfoPath" runat="server" Visible="False">Path</asp:LinkButton></td>
        </tr>
    </table>
<br />
<%--<asp:LinkButton ID="T9lnknotes" runat="server" OnClientClick="javascript:return false;">Enter Support History</asp:LinkButton>
<br />
<br />--%>
<asp:Panel ID="T9Panel1" runat="server" CssClass="panel-cover" >
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="4">Current Support and History</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:RadioButtonList ID="T9RadioButtonListhistory" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="PT">Part-Time LSA</asp:ListItem>
                    <asp:ListItem Value="ST">Shared LSA</asp:ListItem>
                    <asp:ListItem Value="FT">Full-Time</asp:ListItem>
                </asp:RadioButtonList></td>
        </tr>
        <tr>
            <td width="20%"> <span class="field-label">LSA Name</span></td>
            <td width="30%">
                <asp:DropDownList ID="T9DDHistory" runat="server" ValidationGroup="T9History">
                </asp:DropDownList></td>
            <td width="20%"><span class="field-label">Date Support Commenced </span></td>
            <td width="30%">
                <asp:TextBox ID="T9txtDatecommenced" onfocus="javascript:this.blur();return false;" runat="server" ValidationGroup="T9History">
                </asp:TextBox>&nbsp;<asp:Image ID="Image1"
                    runat="server" ImageUrl="~/Images/calendar.gif" /></td>
        </tr>

        <tr>
            <td width="20%"> <span class="field-label">Date Support Discontinued</span></td>          
            <td width="30%">
                <asp:TextBox ID="T9txtDatediscontinued" onfocus="javascript:this.blur();return false;" runat="server" ValidationGroup="T9History"></asp:TextBox>&nbsp;<asp:Image ID="Image2"
                    runat="server" ImageUrl="~/Images/calendar.gif" /></td>
             <td width="20%"><span class="field-label">Notes</span></td>
            <td width="30%">
                <asp:TextBox ID="T9xtxReason" runat="server"  TextMode="MultiLine"
                    ValidationGroup="T9History"></asp:TextBox></td>
        </tr>
           
        <tr>
            <td colspan="4" align="center">
                <asp:Button ID="T9historysave" runat="server" CssClass="button" Text="Save" ValidationGroup="T9History" /></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:RequiredFieldValidator ID="T9RequiredFieldValidator2" runat="server" ErrorMessage="Please Select LSA Name" ControlToValidate="T9DDHistory" Display="None" InitialValue="-1" SetFocusOnError="True" ValidationGroup="T9History"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="T9RequiredFieldValidator3" runat="server" ErrorMessage="Please Select Date Commenced" ControlToValidate="T9txtDatecommenced" Display="None" SetFocusOnError="True" ValidationGroup="T9History"></asp:RequiredFieldValidator>&nbsp;
            <asp:RequiredFieldValidator ID="T9RequiredFieldValidator5" runat="server" ErrorMessage="Please Enter Reason" ControlToValidate="T9xtxReason" Display="None" SetFocusOnError="True" ValidationGroup="T9History"></asp:RequiredFieldValidator>
                <asp:ValidationSummary ID="T9ValidationSummary2" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="T9History" />
            </td>
        </tr>
    </table>
</asp:Panel>
<%--<ajaxToolkit:CollapsiblePanelExtender ID="T9CollapsiblePanelExtender1" runat="server"
    AutoCollapse="False" AutoExpand="False" CollapseControlID="T9lnknotes" Collapsed="false" 
    CollapsedSize="0" CollapsedText="Enter Support History" ExpandControlID="T9lnknotes" ExpandedSize="250"
    ExpandedText="Hide-Support History" ScrollContents="false" TargetControlID="T9Panel1" TextLabelID="T9lnknotes">
</ajaxToolkit:CollapsiblePanelExtender>--%>
    <table width="100%">
        <tr>
            <td class="title-bg">Support History</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T9GrdHitory" AutoGenerateColumns="false" EmptyDataText="No Records Added yet" runat="server" AllowPaging="True" Width="100%" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Type">
                            <HeaderTemplate>
                               Type
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("TIME")%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="LSA Name">
                            <HeaderTemplate>
                                LSA Name
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("empname")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Support Commenced">
                            <HeaderTemplate>
                                Date Support Commenced
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("DATE_COMMENCED")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Support Discontinued">
                            <HeaderTemplate>
                                Date Support Discontinued
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("DATE_DISCONTINUED")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reasons">
                            <HeaderTemplate>
                              Notes
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T9lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T9Panel1" runat="server" >
                                    <%#Eval("REASONS")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T9CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T9lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T9lblview"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T9Panel1"
                                    TextLabelID="T9lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="Date Entry">
<ItemTemplate>
<%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
</ItemTemplate>
</asp:TemplateField>--%>
                    </Columns>
                    <RowStyle CssClass="griditem"  Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />

                </asp:GridView>


            </td>
        </tr>
    </table>
<br />
<asp:LinkButton ID="T9lnknotes1" runat="server" OnClientClick="javascript:return false;">Enter Notes</asp:LinkButton>
<br />
<br />
<asp:Panel ID="T9Panel2" runat="server" CssClass="panel-cover">
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="4">Enter Notes</td>
        </tr>
        <tr>
            <td width="20%"><span class="field-label">Author</span></td>
            <td width="30%">
                <asp:DropDownList ID="T9DDStaffnotes" runat="server" ValidationGroup="T9NOTES">
                </asp:DropDownList></td>  
             <td width="20%"><span class="field-label">Comment</span></td>
             <td width="30%">
                <asp:TextBox ID="T9txtcomments" runat="server"  TextMode="MultiLine"
                    ValidationGroup="T9NOTES" ></asp:TextBox></td>         
        </tr>         
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="T9btnsavecomments" runat="server" CssClass="button" Text="Save" ValidationGroup="T9NOTES" /></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="T9DDStaffnotes"
                    Display="None" ErrorMessage="Please Select a Author" InitialValue="-1" SetFocusOnError="True"
                    ValidationGroup="T9NOTES"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="T9txtcomments"
                    Display="None" ErrorMessage="Please Enter Notes" SetFocusOnError="True" ValidationGroup="T9NOTES"></asp:RequiredFieldValidator>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                    ShowSummary="False" ValidationGroup="T9NOTES" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajaxToolkit:CollapsiblePanelExtender ID="T6CollapsiblePanelExtender1" runat="server"
    AutoCollapse="False" AutoExpand="False" CollapseControlID="T9lnknotes1" Collapsed="true"
    CollapsedSize="0" CollapsedText="Enter Notes" ExpandControlID="T9lnknotes1"
    ExpandedText="Hide-Notes" ScrollContents="false" TargetControlID="T9Panel2" TextLabelID="T9lnknotes1">
</ajaxToolkit:CollapsiblePanelExtender>
<table  Width="100%">
    <tr>
        <td class="title-bg">
            Progress/Notes</td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="T9GrdComments" runat="server" AutoGenerateColumns="false" EmptyDataText="No Comments added yet" AllowPaging="True" Width="100%" CssClass="table table-bordered table-row">
                <Columns>
                    <asp:TemplateField HeaderText="Date">
                        <HeaderTemplate>
                              Date
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                        </ItemTemplate>
                        <ItemStyle  />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comments">
                        <HeaderTemplate>
                          Comments
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="T9lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                            <asp:Panel ID="T9Panel1" runat="server">
                                <%#Eval("COMMENT_TEXT")%>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="T9CollapsiblePanelExtender1" runat="server"
                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T9lblview" Collapsed="true"
                                CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T9lblview"
                                ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T9Panel1"
                                TextLabelID="T9lblview">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Author">
                        <HeaderTemplate>
                            Author
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center><%#Eval("empname")%></center>
                        </ItemTemplate>
                        <ItemStyle  />
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                <EditRowStyle Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
</table>
<ajaxToolkit:CalendarExtender ID="T9CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
    TargetControlID="T9txtDatecommenced">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="T9CE2" runat="server" Format="dd/MMM/yyyy"
    PopupButtonID="Image2" TargetControlID="T9txtDatediscontinued">
</ajaxToolkit:CalendarExtender>
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:HiddenField ID="Hiddenstuid" runat="server" /><asp:HiddenField ID="HiddenLSinfoPath" runat="server" />
</div>
