<%@ Control Language="VB" AutoEventWireup="false" CodeFile="semAssignInclusionHead.ascx.vb" Inherits="SenRegister_UserControls_semAssignInclusionHead" %>


<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">


<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">Assign Head of Section</td>
    </tr>
    <tr>
        <td align="left">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center" colspan="4">
                        <asp:Label ID="lblmessagehod" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td align="left" width="20%">
                        <span class="field-label">Section </span>
                    </td>
                    <td align="left" width="30%">
                        <asp:DropDownList ID="dddepartmenthodselect" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="20%"></td>
                    <td align="left" width="30%"></td>
                </tr>
                <tr>
                    <td align="left" colspan="4">
                        <asp:RadioButtonList ID="RadioButtonListSelectHod" runat="server">
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>

                    <td align="center" colspan="4">
                        <asp:Button ID="btnhodsave" runat="server" CssClass="button" Text="Save" Visible="False" />
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <asp:Button ID="btnhodchange" CssClass="button" runat="server" OnClick="btnhodchange_Click"
                                        Text="Change" Visible="False" /></td>
                                <td>
                                    <asp:Button ID="btnhodcancel" CssClass="button" runat="server" OnClick="btncancel_Click"
                                        Text="Cancel" Visible="False" /></td>
                            </tr>
                        </table>
                    </td>
                    <%-- <td align="left">
                                       
                                        </td>
                                           <td align="left">
                                        </td>--%>
                </tr>
            </table>
            <asp:LinkButton ID="Linkoptions" runat="server" OnClientClick="javascript:return false;">Options</asp:LinkButton>
            <asp:Panel ID="PanelOptions" runat="server" CssClass="panel-cover">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <center>
                                                    <asp:LinkButton ID="lnkdelete" runat="server" Visible="False" OnClick="lnkdelete_Click1">Remove</asp:LinkButton>
                                                </center>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <center>
                                                    <asp:LinkButton ID="lnkchange" runat="server" Visible="False" OnClick="lnkchange_Click">Change</asp:LinkButton>
                                                </center>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
</table>
<ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server"
    TargetControlID="PanelOptions" Enabled="True" TextLabelID="Linkoptions" ExpandedText="Hide Option"
    CollapsedText="Options" ExpandedSize="100" CollapsedSize="0" ExpandControlID="Linkoptions"
    Collapsed="True" CollapseControlID="Linkoptions">
</ajaxToolkit:CollapsiblePanelExtender>
<ajaxToolkit:ConfirmButtonExtender ID="CBE2" runat="server" ConfirmText="You are about to remove head of section.Do you want to continue ?"
    Enabled="True" TargetControlID="lnkdelete">
</ajaxToolkit:ConfirmButtonExtender>
<ajaxToolkit:ConfirmButtonExtender ID="CBE3" runat="server" ConfirmText="You are about to change head of section.Do you want to continue ?"
    Enabled="True" TargetControlID="lnkchange">
</ajaxToolkit:ConfirmButtonExtender>
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
