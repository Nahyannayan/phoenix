Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senStudentProfile
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Dim Encr_decrData As New Encryption64
                Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
                Hiddenstuid.Value = stu_id
                Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
                BindSectionDepartments()
                BindOtherDetails()
                BindSecondaryDetails()
                BindContactDetails()
                BindSiblingDetails()
                ''Time Lag to find out
                lbltimelapsed.Text = DateDiff(DateInterval.Day, Convert.ToDateTime(lbldateofentry.Text), Convert.ToDateTime(lbldateofEntryInclusion.Text), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.System) & " Days"
                LockFileCheck()
            End If
        Catch ex As Exception
            'Throw ex
            'Response.Write("<Script>alert('Error')</Script>")
        End Try
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then

            lnksavestudetails.Enabled = False
            lnksaveinfo.Enabled = False
            lnksave.Enabled = False
        End If

    End Sub
    Public Sub BindSiblingDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim strQuery As String = "select stu_id,STU_FIRSTNAME ,STU_MIDNAME,STU_LASTNAME,BSU_NAME,BSU_SHORTNAME,stu_bsu_id,STU_SIBLING_ID  from student_m  " & _
                                 " inner join BUSINESSUNIT_M on stu_bsu_id=bsu_id " & _
                                 " where STU_SIBLING_ID=(select STU_SIBLING_ID from student_m where stu_id='" & Hiddenstuid.Value & "') " & _
                                 " and stu_id <>'" & Hiddenstuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        GridSiblings.DataSource = ds
        GridSiblings.DataBind()

    End Sub

    Public Sub BindSectionDepartments()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT * FROM SEN_DEPARTMENTS A " & _
                                 " LEFT JOIN SEN_DEPARTMENT_STUDENTS B ON  A.DEPT_ID= B.DEPT_ID AND B.STU_ID='" & Hiddenstuid.Value & "' AND B.STU_BSU_ID='" & Hiddenbsuid.Value & "' WHERE A.DEPT_ACTIVE='True'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        CheckBoxListDepartments.DataSource = ds
        CheckBoxListDepartments.DataTextField = "DEPT_DESC"
        CheckBoxListDepartments.DataValueField = "DEPT_ID"
        CheckBoxListDepartments.DataBind()
        Dim i = 0
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim dept_id = ds.Tables(0).Rows(i).Item("DEPT_ID").ToString()
            Dim active = ds.Tables(0).Rows(i).Item("ACTIVE").ToString()
            If active = "True" Then
                For Each item As ListItem In CheckBoxListDepartments.Items
                    If item.Value = dept_id Then
                        item.Selected = True
                    End If
                Next
            End If
        Next

    End Sub
    Public Sub BindContactDetails()
        Try

            Dim lstrComADDR As String


            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim strQuery As String = "select * from student_m " & _
                                     " left join student_d on stu_sibling_id=sts_stu_id inner join BUSINESSUNIT_M on bsu_id=stu_bsu_id where STU_SIBLING_ID='" & Hiddenstuid.Value & "'"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim primarycontact As String = ds.Tables(0).Rows(0).Item("STU_PRIMARYCONTACT").ToString()



                If primarycontact = "F" Then
                    lstrComADDR = ds.Tables(0).Rows(0).Item("STS_FCOMAPARTNO").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_FCOMBLDG").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_FCOMAREA").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_FCOMSTREET").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_FCOMCITY").ToString()
                    If lstrComADDR = "" Then
                        lstrComADDR = ds.Tables(0).Rows(0).Item("STS_FCOMADDR1").ToString()
                    End If
                    If lstrComADDR = "" Then
                        lstrComADDR = ds.Tables(0).Rows(0).Item("STS_FCOMPOBox").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_FCOMCity").ToString()
                    End If


                    lblhomeaddress.Text = lstrComADDR
                    lblhomephone.Text = ds.Tables(0).Rows(0).Item("STS_FRESPHONE").ToString()

                    lblpostaladdress.Text = ds.Tables(0).Rows(0).Item("STS_FCOMPOBox").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_FCOMCity").ToString()
                    lblcity.Text = ds.Tables(0).Rows(0).Item("STS_FCOMCity").ToString()
                ElseIf primarycontact = "M" Then
                    lstrComADDR = ds.Tables(0).Rows(0).Item("STS_MCOMAPARTNO").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMBLDG").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMAREA").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMSTREET").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMCITY").ToString()
                    If lstrComADDR = "" Then
                        lstrComADDR = ds.Tables(0).Rows(0).Item("STS_MCOMADDR1").ToString()
                    End If
                    If lstrComADDR = "" Then
                        lstrComADDR = ds.Tables(0).Rows(0).Item("STS_MCOMPOBox").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMCity").ToString()
                    End If


                    lblhomeaddress.Text = lstrComADDR
                    lblhomephone.Text = ds.Tables(0).Rows(0).Item("STS_MRESPHONE").ToString()

                    lblpostaladdress.Text = ds.Tables(0).Rows(0).Item("STS_MCOMPOBox").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMCity").ToString()
                    lblcity.Text = ds.Tables(0).Rows(0).Item("STS_MCOMCity").ToString()
                ElseIf primarycontact = "G" Then
                    lstrComADDR = ds.Tables(0).Rows(0).Item("STS_GCOMAPARTNO").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_GCOMBLDG").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_GCOMAREA").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_GCOMSTREET").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_GCOMCITY").ToString()
                    If lstrComADDR = "" Then
                        lstrComADDR = ds.Tables(0).Rows(0).Item("STS_GCOMADDR1").ToString()
                    End If
                    If lstrComADDR = "" Then
                        lstrComADDR = ds.Tables(0).Rows(0).Item("STS_GCOMPOBox").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_GCOMCity").ToString()
                    End If


                    lblhomeaddress.Text = lstrComADDR
                    lblhomephone.Text = ds.Tables(0).Rows(0).Item("STS_GRESPHONE").ToString()

                    lblpostaladdress.Text = ds.Tables(0).Rows(0).Item("STS_GCOMPOBox").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_GCOMCity").ToString()
                    lblcity.Text = ds.Tables(0).Rows(0).Item("STS_GCOMCity").ToString()
                End If


                lblmotheremail.Text = ds.Tables(0).Rows(0).Item("STS_MEMAIL").ToString()
                lblmotherphone.Text = ds.Tables(0).Rows(0).Item("STS_MMOBILE").ToString()
                lblfatheremail.Text = ds.Tables(0).Rows(0).Item("STS_FEMAIL").ToString()
                lblfatherphone.Text = ds.Tables(0).Rows(0).Item("STS_FMOBILE").ToString()

                If ds.Tables(0).Rows(0).Item("STU_DOJ").ToString() <> "" Then
                    lbldateofentry.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_DOJ").ToString()).ToString("dd/MMM/yyyy")
                End If

                CheckAuthoritytojsp.Text = "Authority to release record to " & ds.Tables(0).Rows(0).Item("BSU_SHORTNAME").ToString()

                lbldateofentrylabel.Text = "Date of Entry to " & ds.Tables(0).Rows(0).Item("BSU_SHORTNAME").ToString()


            End If

        Catch ex As Exception

        End Try
    End Sub
    Public Sub BindOtherDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim strQuery As String = "select b.CTY_NATIONALITY,c.RLG_DESCR,HOUSE_DESCRIPTION from student_m a " & _
                                 " inner join country_m b on b.CTY_ID=a.STU_NATIONALITY " & _
                                 " inner join RELIGION_M c on c.RLG_ID=STU_RLG_ID " & _
                                 " left join HOUSE_M d on d.HOUSE_ID=a.STU_HOUSE_ID " & _
                                 "  where a.stu_id='" & Hiddenstuid.Value & "' and a.stu_bsu_id='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        If ds.Tables(0).Rows.Count > 0 Then
            lblnationality.Text = ds.Tables(0).Rows(0).Item("CTY_NATIONALITY").ToString()
            lblschoolhouse.Text = ds.Tables(0).Rows(0).Item("HOUSE_DESCRIPTION").ToString()
            lblreligion.Text = ds.Tables(0).Rows(0).Item("RLG_DESCR").ToString()
        End If



    End Sub
    Public Sub BindSecondaryDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_ACE_STUDENTS where stu_id='" & Hiddenstuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then


            If ds.Tables(0).Rows(0).Item("STU_DOMINANT_HAND").ToString().Trim() <> "" Then
                Dim val = ds.Tables(0).Rows(0).Item("STU_DOMINANT_HAND").ToString().Trim()
                RadioButtonListDominantHand.SelectedValue = val
            End If

            If ds.Tables(0).Rows(0).Item("STU_GLASS").ToString().Trim() <> "" Then
                Dim val = ds.Tables(0).Rows(0).Item("STU_GLASS").ToString().Trim()
                RadioButtonListGlass.SelectedValue = val
            End If
            If ds.Tables(0).Rows(0).Item("STU_OUT_OF_CLASS").ToString().Trim() <> "" Then
                If ds.Tables(0).Rows(0).Item("STU_OUT_OF_CLASS").ToString().Trim() = "True" Then
                    CheckBoxoutofclass.Checked = True
                Else
                    CheckBoxoutofclass.Checked = False
                End If
            End If

            If ds.Tables(0).Rows(0).Item("AUTH_TO_SCHOOL").ToString().Trim() <> "" Then
                If ds.Tables(0).Rows(0).Item("AUTH_TO_SCHOOL").ToString().Trim() = "True" Then
                    CheckAuthoritytojsp.Checked = True
                Else
                    CheckAuthoritytojsp.Checked = False
                End If
            End If

            If ds.Tables(0).Rows(0).Item("AUTH_TO_SCHOOL_DATE").ToString().Trim() <> "" Then
                txtauthdate.Text = ds.Tables(0).Rows(0).Item("AUTH_TO_SCHOOL_DATE").ToString().Trim()
            End If

            If ds.Tables(0).Rows(0).Item("CONFIDENTIAL_INFORMATION").ToString().Trim() <> "" Then
                If ds.Tables(0).Rows(0).Item("CONFIDENTIAL_INFORMATION").ToString().Trim() = "True" Then
                    CkConInfo.Checked = True
                Else
                    CkConInfo.Checked = False
                End If
            End If
            If ds.Tables(0).Rows(0).Item("MEDICAL_LEARNING_HISTORY").ToString().Trim() <> "" Then
                If ds.Tables(0).Rows(0).Item("MEDICAL_LEARNING_HISTORY").ToString().Trim() = "True" Then
                    Ckmedicallearning.Checked = True
                Else
                    Ckmedicallearning.Checked = False
                End If
            End If
            If ds.Tables(0).Rows(0).Item("SPECIAL_CIRCUMSTANCES").ToString().Trim() <> "" Then
                If ds.Tables(0).Rows(0).Item("SPECIAL_CIRCUMSTANCES").ToString().Trim() = "True" Then
                    CkSpecialCircum.Checked = True
                Else
                    CkSpecialCircum.Checked = False
                End If
            End If
            If ds.Tables(0).Rows(0).Item("FILE_HELD_ACE").ToString().Trim() <> "" Then
                If ds.Tables(0).Rows(0).Item("FILE_HELD_ACE").ToString().Trim() = "True" Then
                    CkFHACe.Checked = True
                Else
                    CkFHACe.Checked = False
                End If
            End If
            If ds.Tables(0).Rows(0).Item("FILE_HELD_DU").ToString().Trim() <> "" Then
                If ds.Tables(0).Rows(0).Item("FILE_HELD_DU").ToString().Trim() = "True" Then
                    CkFHDU.Checked = True
                Else
                    CkFHDU.Checked = False
                End If
            End If
            If ds.Tables(0).Rows(0).Item("FILE_HELD_LS").ToString().Trim() <> "" Then
                If ds.Tables(0).Rows(0).Item("FILE_HELD_LS").ToString().Trim() = "True" Then
                    CkFHLS.Checked = True
                Else
                    CkFHLS.Checked = False
                End If
            End If

            If ds.Tables(0).Rows(0).Item("CONFIDENTIAL_WELLBEING_INFORMATION").ToString().Trim() <> "" Then
                If ds.Tables(0).Rows(0).Item("CONFIDENTIAL_WELLBEING_INFORMATION").ToString().Trim() = "True" Then
                    CkConWellInfo.Checked = True
                Else
                    CkConWellInfo.Checked = False
                End If
            End If



            lbldateofEntryInclusion.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ACE_ENTRY_DATE").ToString().Trim()).ToString("dd/MMM/yyyy")
        End If

    End Sub

    Protected Sub lnksave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnksave.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString

        For Each item As ListItem In CheckBoxListDepartments.Items

            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@DEPT_ID", item.Value)
            pParms(3) = New SqlClient.SqlParameter("@SELECTED", item.Selected)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_STUDENTS", pParms)

        Next
        ''BindSectionDepartments()

        Dim AuthJC As String = "False"
        Dim AuthDate As String = txtauthdate.Text.Trim()
        If CheckAuthoritytojsp.Checked Then
            AuthJC = "True"
        End If
        Dim strQuery As String = "Update SEN_ACE_STUDENTS set AUTH_TO_SCHOOL='" & AuthJC & "', AUTH_TO_SCHOOL_DATE='" & AuthDate & "' where STU_ID='" & Hiddenstuid.Value & "' and STU_BSU_ID='" & Hiddenbsuid.Value & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)

        Dim Encr_decrData As New Encryption64
        Dim stu_id As String = Encr_decrData.Encrypt(Hiddenstuid.Value)
        Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        Response.Redirect("senStudentProfile.aspx?Stu_id=" & stu_id & mInfo)
    End Sub

    Protected Sub lnksavestudetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnksavestudetails.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString

        Dim dominanthand As String = RadioButtonListDominantHand.SelectedValue
        Dim Glass As String = RadioButtonListGlass.SelectedValue
        Dim Out_of_Class As Boolean = False

        If CheckBoxoutofclass.Checked Then
            Out_of_Class = True
        End If
        Dim strQuery As String = "Update SEN_ACE_STUDENTS set STU_DOMINANT_HAND='" & dominanthand & "', STU_GLASS='" & Glass & "', STU_OUT_OF_CLASS='" & Out_of_Class & "' where stu_id='" & Hiddenstuid.Value & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
        BindSecondaryDetails()

        lblstatusmessage.Text = "Successfully Saved"
        MO1.Show()

    End Sub

    Protected Sub lnksaveinfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnksaveinfo.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString

        Dim Confidential As Boolean = False
        Dim RelevantInfo As Boolean = False
        Dim SpecialCircum As Boolean = False
        Dim FileHAce As Boolean = False
        Dim FileHDU As Boolean = False
        Dim FileLS As Boolean = False
        Dim ConWellInfo As Boolean = False

        If CkConInfo.Checked Then
            Confidential = True
        End If
        If Ckmedicallearning.Checked Then
            RelevantInfo = True
        End If
        If CkSpecialCircum.Checked Then
            SpecialCircum = True
        End If
        If CkFHACe.Checked Then
            FileHAce = True
        End If
        If CkFHDU.Checked Then
            FileHDU = True
        End If
        If CkFHLS.Checked Then
            FileLS = True
        End If
        If CkConWellInfo.Checked Then
            ConWellInfo = True
        End If

        Dim pParms(9) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@CONFI", Confidential)
        pParms(1) = New SqlClient.SqlParameter("@RELINFO", RelevantInfo)
        pParms(2) = New SqlClient.SqlParameter("@SPECIALCIRCUM", SpecialCircum)
        pParms(3) = New SqlClient.SqlParameter("@FHACE", FileHAce)
        pParms(4) = New SqlClient.SqlParameter("@FHDU", FileHDU)
        pParms(5) = New SqlClient.SqlParameter("@FHLS", FileLS)
        pParms(6) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(7) = New SqlClient.SqlParameter("@STU_BSU_ID", Hiddenbsuid.Value)
        pParms(8) = New SqlClient.SqlParameter("@CONFIDENTIAL_WELLBEING_INFORMATION", ConWellInfo)

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_STUDENT_FILE_INFORMATION", pParms)
        BindSecondaryDetails()

        lblstatusmessage.Text = "Successfully Saved"
        MO1.Show()

    End Sub
End Class
