<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senDyslexiaUnit.ascx.vb"
    Inherits="SenRegister_UserControls_senDyslexiaUnit" %>
<asp:HiddenField ID="Hiddenstuid" runat="server" />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:Label ID="lblstatusmessage" runat="server" EnableTheming="False" CssClass="error"></asp:Label>


<div>
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="4">DU Details</td>
        </tr>
        <tr>
            <td width="20%">
                <asp:Label ID="T4lbldateenteredlabel" runat="server"></asp:Label></td>
            <td width="30%">
                <asp:Label ID="T4lbldateentered" runat="server"></asp:Label></td>
            <td width="20%"><span class="field-label">Date Entered DU</span></td>
            <td width="30%">
                <asp:TextBox ID="T4lblDateenteredDu" runat="server" onfocus="javascript:this.blur();return false;"></asp:TextBox>
                <asp:Image ID="Image11" runat="server" ImageUrl="~/Images/calendar.gif" />
                <asp:LinkButton ID="lnkentrysave" runat="server" CausesValidation="False">Save</asp:LinkButton>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="Image11" TargetControlID="T4lblDateenteredDu">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td width="20%"><span class="field-label">Time Lag</span></td>
            <td width="30%">
                <asp:Label ID="T4lblTimeLag" runat="server"></asp:Label></td>
            <td colspan="2"></td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg">Ace Screening Details</td>
        </tr>
        <tr>
            <td>
                <table width="100%" class="table table-bordered">
                    <tr>
                        <td colspan="3">Dylexia Index</td>
                        <td>Date of Screening</td>
                        <td>Screening Type</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:TextBox ID="T4txtduindex" ReadOnly="true" runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="T4txtduassessment" ReadOnly="true" runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="T4txtdusentype" ReadOnly="true" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="3">No.of lessons per week</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:TextBox ID="T4txtweeklylessontotal" runat="server"></asp:TextBox></td>
                        <td>
                            <asp:LinkButton ID="T4txtsavebasic" runat="server" CausesValidation="False">Save</asp:LinkButton></td>
                        <td></td>
                    </tr>
                </table>
                <%--<ajaxToolkit:CollapsiblePanelExtender id="T11CollapsiblePanelExtender4" runat="server" TextLabelID="T11lnkview1" TargetControlID="T11Panel1" ScrollContents="false" ExpandedText="Hide" ExpandedSize="200" ExpandControlID="T11lnkview1" CollapsedText="Enter Enrichment Details" CollapsedSize="0" Collapsed="true" CollapseControlID="T11lnkview1" AutoExpand="False" AutoCollapse="False">
</ajaxToolkit:CollapsiblePanelExtender>--%>
                <ajaxToolkit:FilteredTextBoxExtender ID="T4FILTER1" runat="server" TargetControlID="T4txtweeklylessontotal"
                    FilterType="Numbers">
                </ajaxToolkit:FilteredTextBoxExtender>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg">Assign Dylexia Unit Staff</td>
        </tr>
        <tr>
            <td>
                <table class="table table-bordered" width="100%">
                    <tr>
                        <td colspan="3">Current Teacher Assigned</td>
                        <td colspan="3">Date Assigned</td>
                        <td colspan="1">Date Complete</td>
                        <td colspan="3">Lessons per week-Teacher</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:TextBox ID="T4txtcurrentteacher" runat="server"></asp:TextBox></td>
                        <td colspan="3">
                            <asp:TextBox ID="T4txtcurrentteacherdate" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td colspan="1">
                            <asp:TextBox ID="T4txtDateComTeacher" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />&nbsp;</td>
                        <td colspan="3">
                            <asp:TextBox ID="T4txtweeklylessonteacher" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="3">Current DU TA Assigned
                        </td>
                        <td colspan="3">Date Assigned</td>
                        <td colspan="1">Date Complete</td>
                        <td colspan="3">Lessons per week-TA</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:TextBox ID="T4txtcurrentTA" runat="server"></asp:TextBox></td>
                        <td colspan="3">
                            <asp:TextBox ID="T4txtcurrentTAdate" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                            <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td colspan="1">
                            <asp:TextBox ID="T4txtDateComTA" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                            <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td colspan="3">
                            <asp:TextBox ID="T4txtweeklylessonTA" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="10">
                            <asp:LinkButton ID="T4lnksaveteacher" runat="server" CausesValidation="False">Save</asp:LinkButton></td>
                    </tr>
                </table>
                <br />
                <asp:LinkButton ID="T4lnkview1" OnClientClick="javascript:return false;" runat="server">Teacher History</asp:LinkButton><br />
                <br />
                <asp:Panel ID="T4Panel1" runat="server">
                    <table width="100%">
                        <tr>
                            <td class="title-bg">Teacher History</td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:GridView ID="T4GrdTeacherhistory" AutoGenerateColumns="False" EmptyDataText="No History" CssClass="table table-bordered table-row"
                                    runat="server" AllowPaging="True" PageSize="5" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date Assigned">
                                            <HeaderTemplate>
                                                Date Assigned
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("DATE_ASSIGNED_TEACHER")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date Completed">
                                            <HeaderTemplate>
                                                Date Completed
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("DATE_COMPLETED_TEACHER")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Current Teacher">
                                            <HeaderTemplate>
                                                Current Teacher
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("CURRENT_TEACHER")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Weekly Lessons">
                                            <HeaderTemplate>
                                                Weekly Lessons
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("WEELKY_LESSON_TEACHER")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Current DU TA">
                                            <HeaderTemplate>
                                                Current DU TA
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("CURRENT_TA")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date Assigned">
                                            <HeaderTemplate>
                                                Date Assigned
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("DATE_ASSIGNED_TA")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date Completed">
                                            <HeaderTemplate>
                                                Date Completed
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("DATE_COMPLETED_TA")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Weekly Lessons">
                                            <HeaderTemplate>
                                                Weekly Lessons
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("WEELKY_LESSON_TA")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="griditem" Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:CollapsiblePanelExtender ID="T4CollapsiblePanelExtender1" runat="server"
                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T4lnkview1" Collapsed="true"
                    CollapsedSize="0" CollapsedText="Teacher History" ExpandControlID="T4lnkview1"
                    ExpandedText="Hide-Teacher History" ScrollContents="false" TargetControlID="T4Panel1"
                    TextLabelID="T4lnkview1">
                </ajaxToolkit:CollapsiblePanelExtender>
            </td>
        </tr>
    </table>
    <br />
    <asp:LinkButton ID="T4lnkview2" runat="server" OnClientClick="javascript:return false;">Enter Comprehension Details</asp:LinkButton>
    <br />
    <br />
    <asp:Panel ID="T4Panel2" runat="server">
        <table width="100%">
            <tr>
                <td class="title-bg">DU Assessment Details</td>
            </tr>
            <tr>
                <td>
                    <table width="100%" class="table table-bordered">
                        <tr>
                            <td></td>

                            <td align="center">Age</td>
                            <td>Std Score</td>
                            <td>Percentile</td>
                        </tr>
                        <tr>
                            <td>Word Identification</td>

                            <td>
                                <asp:TextBox ID="T4txtWordIdentification_Age" runat="server" Width="50px"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="T4txtWordIdentification_std_score" runat="server" Width="50px"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="T4txtWordIdentification_percentile" runat="server" Width="50px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Word Attack</td>

                            <td>
                                <asp:TextBox ID="T4txtWordAttack_age" runat="server" Width="50px"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="T4txtWordAttack_std_score" runat="server" Width="50px"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="T4txtWordAttack_percentile" runat="server" Width="50px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Word Comprehension</td>

                            <td>
                                <asp:TextBox ID="T4txtwordcomrehension_age" runat="server" Width="50px"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="T4txtwordcomrehension_std_score" runat="server" Width="50px"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="T4txtwordcomrehension_percentile" runat="server" Width="50px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Passage Comprehension</td>

                            <td>
                                <asp:TextBox ID="T4txtpassagecomprehentions_age" runat="server" Width="50px"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="T4txtpassagecomprehentions_std_score" runat="server" Width="50px"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="T4txtpassagecomprehentions_percentile" runat="server" Width="50px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Spelling</td>

                            <td>
                                <asp:TextBox ID="T4txtspelling_age" runat="server" Width="50px"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="T4txtspelling_std_score" runat="server" Width="50px"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="center" colspan="3">
                                <asp:Button ID="T4btnsavecomprehension" CssClass="button" runat="server" Text="Save"
                                    CausesValidation="False" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="T4CollapsiblePanelExtender2" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T4lnkview2" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter Comprehension Details" ExpandControlID="T4lnkview2"
        ExpandedText="Hide-Comprehension Details" ScrollContents="false" TargetControlID="T4Panel2"
        TextLabelID="T4lnkview2">
    </ajaxToolkit:CollapsiblePanelExtender>
    <br />

    <table width="100%">
        <tr>
            <td class="title-bg">DU Assessment Details</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T4GrdComprehension" AutoGenerateColumns="False" EmptyDataText="No History" CssClass="table table-bordered table-row"
                    runat="server" AllowPaging="True" PageSize="5" Width="100%">
                    <Columns>
                        <%-- <asp:TemplateField HeaderText="Date">
                        <HeaderTemplate>
                        <table  >
                                    <tr>
                                        <td align="center"  >
                                           Date
                                        </td>
                                    </tr>
                         
                                </table>
                        </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("ENTRY_DATE","{0:dd/MMM/yyyy}")%>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Word Identification">
                            <HeaderTemplate>
                                Word Identification
                                <br />
                                <table width="100%">
                                    <tr>
                                        <td align="center" style="width: 33%">A
                                        </td>
                                        <td align="center" style="width: 33%">S
                                        </td>
                                        <td align="center" style="width: 33%">P
                                        </td>
                                    </tr>
                                </table>

                                <%--<table  width="100%">
                                    <tr >
                                        <td align="center" colspan="3">
                                            
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="center" style="width: 33%">
                                        </td>
                                        <td align="center" style="width: 33%">
                                        </td>
                                        <td align="center" style="width: 33%">
                                        </td>
                                    </tr>
                                </table>--%>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td align="center" style="width: 33%">
                                            <%#Eval("WORD_IDENTIFICATION_AGE")%>
                                        </td>
                                        <td align="center" style="width: 33%">
                                            <%#Eval("WORD_IDENTIFICATION_STD_SCORE")%>
                                        </td>
                                        <td align="center" style="width: 33%">
                                            <%#Eval("WORD_IDENTIFICATION_PERCENTILE")%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Word Attack">
                            <HeaderTemplate>
                                Word Attack
                                <br />
                                <table width="100%">
                                    <tr>
                                        <td align="center" style="width: 33%">A
                                        </td>
                                        <td align="center" style="width: 33%">S
                                        </td>
                                        <td align="center" style="width: 33%">P
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table style="width: 100%">
                                    <tr>
                                        <td align="center" style="width: 33%">
                                            <%#Eval("WORD_ATTACK_AGE")%>
                                        </td>
                                        <td align="center" style="width: 33%">
                                            <%#Eval("WORD_ATTACK_STD_SCORE")%>
                                        </td>
                                        <td align="center" style="width: 33%">
                                            <%#Eval("WORD_ATTACK_PERCENTILE")%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Word Comprehension">
                            <HeaderTemplate>
                                Word Comprehension<br />
                                <table width="100%">
                                    <tr>
                                        <td align="center" style="width: 33%">A
                                        </td>
                                        <td align="center" style="width: 33%">S
                                        </td>
                                        <td align="center" style="width: 33%">P
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table style="width: 100%">
                                    <tr>
                                        <td align="center" style="width: 33%">
                                            <%#Eval("WORD_COMPREHENSION_AGE")%>
                                        </td>
                                        <td align="center" style="width: 33%">
                                            <%#Eval("WORD_COMPREHENSION_STD_SCORE")%>
                                        </td>
                                        <td align="center" style="width: 33%">
                                            <%#Eval("WORD_COMPREHENSION_PERCENTILE")%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Passage Comprehension<br />
                                <table width="100%">
                                    <tr>
                                        <td align="center" style="width: 33%">A
                                        </td>
                                        <td align="center" style="width: 33%">S
                                        </td>
                                        <td align="center" style="width: 33%">P
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table style="width: 100%">
                                    <tr>
                                        <td align="center" style="width: 33%">
                                            <%#Eval("PASSAGE_COMPREHENSION_AGE")%>
                                        </td>
                                        <td align="center" style="width: 33%">
                                            <%#Eval("PASSAGE_COMPREHENSION_STD_SCORE")%>
                                        </td>
                                        <td align="center" style="width: 33%">
                                            <%#Eval("PASSAGE_COMPREHENSION_PERCENTILE")%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Spelling">
                            <HeaderTemplate>
                                Spelling<br />
                                <table width="100%">
                                    <tr>
                                        <td align="center" style="width: 50%">A
                                        </td>
                                        <td align="center" style="width: 50%">S
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table style="width: 100%">
                                    <tr>
                                        <td align="center" style="width: 50%">
                                            <%#Eval("SPELLING_AGE")%>
                                        </td>
                                        <td align="center" style="width: 50%">
                                            <%#Eval("SPELLING_STD_SCORE")%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
                A-Age,&nbsp;S-Std Score,&nbsp;P-Percentile
                                              
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="4">IQ and Expected Attainment</td>
        </tr>
        <tr>
            <td><span class="field-label">Estimated IQ</span>
            </td>
            <td>
                <asp:TextBox ID="T4txtIQ" runat="server"></asp:TextBox></td>
            <td colspan="2">
                <asp:Button ID="T4btnIQSave" runat="server" Text="Save" CssClass="button" CausesValidation="False" /></td>
        </tr>
        <tr>
            <td><span class="field-label">Expected Score </span></td>

            <td>
                <asp:TextBox ID="T4txtExpectedscore" ReadOnly="true" runat="server"></asp:TextBox></td>
            <td colspan="4"></td>
        </tr>
    </table>
    <ajaxToolkit:FilteredTextBoxExtender ID="f1" FilterType="Numbers" TargetControlID="T4txtIQ" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
    <br />
    <asp:LinkButton ID="T4lnkview3" runat="server" OnClientClick="javascript:return false;">Enter WPM Details</asp:LinkButton>
    <br />
    <br />
    <asp:Panel ID="T4Panel3" runat="server" Height="50px">
        <table width="100%">
            <tr>
                <td class="title-bg">Enter Words Per Minute Details (WPM)</td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="20%"><span class="field-label">Reading WPM</span></td>

                            <td width="30%">
                                <asp:TextBox ID="T4txtreadingwpd" runat="server"></asp:TextBox></td>
                            <td width="20%"><span class="field-label">Writing WPM</span></td>

                            <td width="30%">
                                <asp:TextBox ID="T4txtwritingwpd" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="20%"><span class="field-label">Typing WPM</span></td>

                            <td width="30%">
                                <asp:TextBox ID="T4txttypingWPD" runat="server"></asp:TextBox></td>
                            <td width="20%"><span class="field-label">Date</span></td>

                            <td width="30%">
                                <asp:TextBox ID="t4txtwpmdate" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="T4btnsavewpm" CssClass="button" runat="server" Text="Save" CausesValidation="False" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <ajaxToolkit:CollapsiblePanelExtender ID="T4CollapsiblePanelExtender3" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T4lnkview3" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter WPM Details" ExpandControlID="T4lnkview3"
        ExpandedText="Hide-WPM Details" ScrollContents="false" TargetControlID="T4Panel3"
        TextLabelID="T4lnkview3">
    </ajaxToolkit:CollapsiblePanelExtender>
    <table width="100%">
        <tr>
            <td class="title-bg">Words Per Minute Details </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T4GrdWPM" AutoGenerateColumns="False" EmptyDataText="No History" CssClass="table table-bordered table-row"
                    runat="server" AllowPaging="True" PageSize="5" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderTemplate>
                                Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("ENTRY_DATE","{0:dd/MMM/yyyy}")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reading WPM">
                            <HeaderTemplate>
                                Reading WPM
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("READING_WPM")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Writing WPM">
                            <HeaderTemplate>
                                Writing WPM
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("WRITING_WPM")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Typing WPM">
                            <HeaderTemplate>
                                Typing WPM
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("TYPING_WPM")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg">DU Interventions</td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                            <asp:CheckBox ID="DuI1" Text="Acceleread/Accelerwrite" runat="server" /></td>
                        <td>
                            <asp:TextBox ID="txtDuI1" runat="server"></asp:TextBox>
                            <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="DuI2" Text="CoWriter/Write Out Loud" runat="server" /></td>
                        <td>
                            <asp:TextBox ID="txtDuI2" runat="server"></asp:TextBox>
                            <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="DuI3" Text="Laptop" runat="server" /></td>
                        <td>
                            <asp:TextBox ID="txtDuI3" runat="server"></asp:TextBox>
                            <asp:Image ID="Image8" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="DuI4" Text="Alphasmart" runat="server" /></td>
                        <td>
                            <asp:TextBox ID="txtDuI4" runat="server"></asp:TextBox>
                            <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="DuI5" Text="Other" runat="server" /></td>
                        <td>
                            <asp:TextBox ID="txtDuI5" runat="server"></asp:TextBox>
                            <asp:Image ID="Image10" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td>
                            <asp:TextBox ID="t4txtduinter" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="3">
                            <asp:Button ID="T4btnsaveDuinterventions" CssClass="button" runat="server" Text="Save"
                                CausesValidation="False" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="T4CE8" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image6"
                    TargetControlID="txtDuI1">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="T4CE9" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image7"
                    TargetControlID="txtDuI2">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="T4CE10" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image8"
                    TargetControlID="txtDuI3">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="T4CE11" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image9"
                    TargetControlID="txtDuI4">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="T4CE12" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image10"
                    TargetControlID="txtDuI5">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg">DU IEP
                <asp:CheckBox ID="t4checkduiep" runat="server" AutoPostBack="True" />
                (File Location / Directory)</td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtduiepinfo" runat="server"></asp:TextBox>
                <asp:Button ID="t4btnduiepinfosave" runat="server" CssClass="button" Text="Save" CausesValidation="False" />
                <asp:LinkButton ID="t4lnkduieppath" runat="server" Visible="False">Path</asp:LinkButton></td>
        </tr>
    </table>
    <br />
    <asp:LinkButton ID="T4lnkview4" runat="server" OnClientClick="javascript:return false;">Enter Notes</asp:LinkButton>
    <br />
    <br />
    <asp:Panel ID="T4Panel4" runat="server">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4">Enter Notes</td>
            </tr>
            <tr>
                <td><span class="field-label">Author</span></td>
                <td>
                    <asp:DropDownList ID="T4ddstaffnotes" runat="server" ValidationGroup="T4notes">
                    </asp:DropDownList></td>
                <td><span class="field-label">Comments</span></td>
                <td>
                    <asp:TextBox ID="T4txtnotes" runat="server" TextMode="MultiLine"
                        ValidationGroup="T4notes"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="T4btnnotessave" runat="server" CssClass="button" Text="Save" ValidationGroup="T4notes" /></td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="T4CollapsiblePanelExtender4" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T4lnkview4" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter Notes" ExpandControlID="T4lnkview4"
        ExpandedText="Hide-Notes" ScrollContents="false" TargetControlID="T4Panel4" TextLabelID="T4lnkview4">
    </ajaxToolkit:CollapsiblePanelExtender>
    <table width="100%">
        <tr>
            <td class="title-bg">Notes</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T4GrdNotes" runat="server" AutoGenerateColumns="false" EmptyDataText="No Notes added yet" CssClass="table table-bordered table-row"
                    Width="100%" AllowPaging="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderTemplate>
                                Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("ENTRY_DATE","{0:dd/MMM/yyyy}") %>
                            </ItemTemplate>
                            <ItemStyle />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comment">
                            <HeaderTemplate>
                                Comment
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T4lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T4Panel1" runat="server">
                                    <%#Eval("DU_NOTE")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T4CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T4lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T4lblview"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T4Panel1"
                                    TextLabelID="T4lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Author">
                            <HeaderTemplate>
                                Author
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center> <%#Eval("empname")%></center>
                            </ItemTemplate>
                            <ItemStyle />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="T4ddstaffnotes"
        Display="None" ErrorMessage="Please Select Author" InitialValue="-1" SetFocusOnError="True"
        ValidationGroup="T4notes"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="T4txtnotes"
        Display="None" ErrorMessage="Please Enter Notes" SetFocusOnError="True" ValidationGroup="T4notes"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="T4notes" />
    <br />
    <ajaxToolkit:CalendarExtender ID="T4CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image2"
        TargetControlID="T4txtcurrentteacherdate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T4CE3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image3"
        TargetControlID="T4txtcurrentTAdate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T4CE5" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image5"
        TargetControlID="T4txtDateComTA">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T4CE6" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
        TargetControlID="T4txtDateComTeacher">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T4CE7" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image4"
        TargetControlID="t4txtwpmdate">
    </ajaxToolkit:CalendarExtender>

    <br />
    <ajaxToolkit:FilteredTextBoxExtender ID="T4FILTER2" runat="server" TargetControlID="T4txtweeklylessonteacher"
        FilterType="Numbers">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="T4FILTER3" runat="server" TargetControlID="T4txtweeklylessonTA"
        FilterType="Numbers">
    </ajaxToolkit:FilteredTextBoxExtender>
    <asp:HiddenField ID="HiddenDuIEPInfo" runat="server" />

</div>
