<%@ Control Language="VB" AutoEventWireup="false" CodeFile="semRemoveInclusionStudents.ascx.vb"
    Inherits="SenRegister_UserControls_semRemoveInclusionStudents" %>

<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr class="title-bg-lite">
        <td colspan="4">Remove Students </td>
    </tr>

    <tr>

        <td align="left" width="20%">
            <span class="field-label">Academic Year </span>
        </td>
        <td align="left" width="30%">
            <asp:DropDownList ID="ddaccyear"
                runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" width="20%"></td>
        <td align="left" width="30%"></td>
    </tr>
    <tr>
        <td colspan="4">
            <asp:GridView ID="Grdstudentremove" AutoGenerateColumns="False" runat="server" EmptyDataText="No Records Found" CssClass="table table-bordered table-row"
                Width="100%" ShowFooter="True" AllowPaging="True" PageSize="15">
                <Columns>
                    <asp:TemplateField HeaderText="Student Number">
                        <HeaderTemplate>
                            Student Number
                                        <br />
                                        <asp:TextBox ID="txtnumber" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch1" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="HiddensStuid" Value='<%#Eval("stu_id")%>' runat="server" />
                            <%#Eval("stu_no")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Student Name">
                        <HeaderTemplate>
                           Student Name
                                        <br />
                                        <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch2" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("STU_NAME")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Grade">
                        <HeaderTemplate>
                            Grade
                                        <br />
                                        <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch3" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("GRM_DISPLAY")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Section">
                        <HeaderTemplate>
                            Section
                                        <br />
                                        <asp:TextBox ID="txtSection" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch4" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("SCT_DESCR")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Check">
                        <HeaderTemplate>
                            
                                        Check
                                       
                        </HeaderTemplate>
                        <FooterTemplate>
                            <center>
                                <asp:Button ID="btnremove" runat="server" CssClass="button" CommandName="remove"
                                    Text="Remove" /></center>
                            <ajaxToolkit:ConfirmButtonExtender ID="CBE1" runat="server" ConfirmText="Selected students will be removed from Inclusion department.Do you want to continue ?"
                                TargetControlID="btnremove">
                            </ajaxToolkit:ConfirmButtonExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:CheckBox ID="Check" runat="server" /></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem"  />
                <EmptyDataRowStyle  />
                <SelectedRowStyle  />
                <HeaderStyle  />
                <EditRowStyle  />
                <AlternatingRowStyle CssClass="griditem_alternative"  />
            </asp:GridView>
            <asp:Label ID="lblsturemovemessage" runat="server" ForeColor="Red"></asp:Label><br />
            <asp:Label ID="lblmessager" runat="server" Visible="False" ForeColor="Red">Note- Students listed above does not belong to any of the Inclusion sections.</asp:Label></td>
    </tr>
</table>
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
