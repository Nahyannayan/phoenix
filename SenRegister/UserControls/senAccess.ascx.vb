Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senAccess
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindControls()
            BindGridReasons()
            'BindGridResults()
            LockFileCheck()
        End If

        'For Each row As GridViewRow In T3GrdReasons.Rows
        '    Dim lnkPath As LinkButton = DirectCast(row.FindControl("lnkPath"), LinkButton)
        '    ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkPath)
        'Next
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then
            T3btnResultSave.Enabled = False
            T3btnResults.Enabled = False
        End If



    End Sub

    Public Sub BindControls()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id and  a.EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        T3ddstaflist.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T3ddresultStaff.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T3ddstaflist.DataTextField = "empname"
        T3ddresultStaff.DataTextField = "empname"
        T3ddstaflist.DataValueField = "EMP_ID"
        T3ddresultStaff.DataValueField = "EMP_ID"
        T3ddstaflist.DataBind()
        T3ddresultStaff.DataBind()
        Dim list As New ListItem
        list.Text = "Select an Inclusion Member"
        list.Value = "-1"
        T3ddstaflist.Items.Insert(0, list)
        T3ddresultStaff.Items.Insert(0, list)

    End Sub
    Public Sub BindGridReasons()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,ACCESS_REASON_ID,REQUESTED_DATE,REQUESTED_BY,REASON_TEXT,a.ENTRY_DATE,(substring(REASON_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview,(EMP_FNAME + ' ' + EMP_LNAME) as authempname ,RESULT_TEXT,c.ENTRY_DATE as resulteDate," & _
                                 " (substring(RESULT_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview2,(select EMP_FNAME +' ' + EMP_LNAME from oasis.dbo.EMPLOYEE_M em where em.emp_id=c.ENTRY_EMP_ID) resultentryauthor,RESULT_TEXT,LINK_INFO " & _
                                 " ,(case when LINK_INFO is NULL THEN 'False' else 'True' end ) showpath " & _
                                 " ,(case when c.RESULT_TEXT is NULL THEN 'True' else 'False' end ) show " & _
                                 " from SEN_DEPARTMENT_ACCESS_REASONS a  inner join  oasis.dbo.EMPLOYEE_M b on a.ENTRY_EMP_ID=b.emp_id  " & _
                                 " left join  dbo.SEN_DEPARTMENT_ACCESS_RESULTS c on c.ACE_REQUEST_Id=a.ACCESS_REASON_ID " & _
                                 " where  a.stu_id='" & Hiddenstuid.Value & "' and a.bsu_id='" & Hiddenbsuid.Value & "' order by a.ENTRY_DATE DESC "
        T3GrdReasons.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T3GrdReasons.DataBind()

        'For Each row As GridViewRow In T3GrdReasons.Rows
        '    Dim lnkPath As LinkButton = DirectCast(row.FindControl("lnkPath"), LinkButton)
        '    ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkPath)
        'Next

    End Sub
    'Public Sub BindGridResults()
    '    Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
    '    Dim strQuery As String = "select '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,ENTRY_DATE,RESULT_TEXT,(substring(RESULT_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as authempname from SEN_DEPARTMENT_ACCESS_RESULTS a  " & _
    '                             " inner join   oasis.dbo.EMPLOYEE_M b on a.ENTRY_EMP_ID=b.emp_id and a.stu_id='" & Hiddenstuid.Value & "' and a.bsu_id='" & Hiddenbsuid.Value & "' order by a.ENTRY_DATE DESC "
    '    T3GrdResult.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
    '    T3GrdResult.DataBind()
    'End Sub
    Protected Sub T3btnResultSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T3btnResultSave.Click

        Try
            lblstatusmessage.Text = ""
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@REQUESTED_BY", T3txtAccessRequestedBy.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@REQUESTED_DATE", T3txtRequestedDate.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@REASON_TEXT", T3txtReason.Text.Trim())
            pParms(5) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T3ddstaflist.SelectedValue)

            If txtAccessArrangementinfo.Text.Trim() <> "" Then
                pParms(6) = New SqlClient.SqlParameter("@LINK_INFO", txtAccessArrangementinfo.Text.Trim())
            End If

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ACCESS_REASONS", pParms)
            T3txtAccessRequestedBy.Text = ""
            T3txtRequestedDate.Text = ""
            T3txtReason.Text = ""
            txtAccessArrangementinfo.Text = ""
            T3ddstaflist.SelectedIndex = 0
            BindGridReasons()
            lblstatusmessage.Text = "Successfully Saved"
        Catch ex As Exception
            lblstatusmessage.Text = "Error : " & ex.Message
        End Try


    End Sub

    Protected Sub T3GrdReasons_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T3GrdReasons.PageIndexChanging
        T3GrdReasons.PageIndex = e.NewPageIndex
        BindGridReasons()
    End Sub

    Protected Sub T3btnResults_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T3btnResults.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T3ddresultStaff.SelectedValue)
        pParms(3) = New SqlClient.SqlParameter("@RESULT_TEXT", T3txtResults.Text.Trim())
        pParms(4) = New SqlClient.SqlParameter("@ACE_REQUEST_ID", HiddenRequestid.Value)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ACCESS_RESULTS", pParms)
        T3txtResults.Text = ""
        T3ddresultStaff.SelectedIndex = 0
        HiddenRequestid.Value = ""
        T3Panel2.Visible = False
        'BindGridResults()
        BindGridReasons()
        lblstatusmessage.Text = "Successfully Saved"

    End Sub

    'Protected Sub T3GrdResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T3GrdResult.PageIndexChanging
    '    T3GrdResult.PageIndex = e.NewPageIndex
    '    BindGridResults()
    'End Sub

    Protected Sub T3GrdReasons_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles T3GrdReasons.RowCommand
        Try
            If e.CommandName = "Results" Then
                HiddenRequestid.Value = e.CommandArgument
                T3Panel2.Visible = True

            End If
            'If e.CommandName = "Path" Then
            '    Dim path = e.CommandArgument
            '    HttpContext.Current.Response.ContentType = "application/octect-stream"
            '    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            '    HttpContext.Current.Response.Clear()
            '    HttpContext.Current.Response.WriteFile(path)
            '    HttpContext.Current.Response.End()
            'End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T3btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T3btnCancel.Click
        HiddenRequestid.Value = ""
        T3Panel2.Visible = False
    End Sub
End Class
