<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senMonitoringEnrichment.ascx.vb" Inherits="SenRegister_UserControls_senMonitoringEnrichment" %>
<div class="matters">
<table border="0" cellpadding="0" cellspacing="0" width="700">
    <tr>
        <td  align="right">
            <asp:LinkButton ID="MonitorView" OnClientClick="javascript:return false;" runat="server">Monitoring</asp:LinkButton>
            <asp:Panel ID="Panel2" runat="server" Height="50px" BorderColor="Silver" BorderStyle="Solid" BorderWidth="2px">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td >
            <asp:CheckBox ID="MonitoringCheckBox" runat="server" Text="Monitoring" /></td>
                    <td  >
                    </td>
                </tr>
                <tr>
                    <td id="TD1"  colspan="2">
                <table>
                    <tr>
                        <td >
                            Due Date</td>
                        <td >
                            :</td>
                        <td >
                            <asp:TextBox ID="txtdatemonotor" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox></td>
                        <td>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td >
                <asp:LinkButton ID="MonitoringSave" CausesValidation="false" runat="server">Save</asp:LinkButton></td>
                    </tr>
                    <tr>
                    </tr>
                </table>
                        <asp:LinkButton ID="MonitorNotes" OnClientClick="javascript:return false;" runat="server">Notes</asp:LinkButton>
                        <asp:Panel ID="Panel1" runat="server" >
                            <asp:TextBox ID="txtMonitornotes" SkinID="MultiText" runat="server" Height="62px" TextMode="MultiLine" Width="287px"></asp:TextBox></asp:Panel>
                    </td>
                </tr>
            </table>
               
            </asp:Panel> 
            <br />
           
        </td>
    </tr>
</table>
<ajaxToolkit:CalendarExtender ID="T3CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
    TargetControlID="txtdatemonotor">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
    AutoCollapse="False" AutoExpand="False" CollapseControlID="MonitorNotes" Collapsed="true"
    CollapsedSize="0" CollapsedText="Notes" ExpandControlID="MonitorNotes"
    ExpandedSize="50" ExpandedText="Hide-Notes" ScrollContents="false" TargetControlID="Panel1"
    TextLabelID="MonitorNotes">
</ajaxToolkit:CollapsiblePanelExtender><ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
    AutoCollapse="False" ScrollContents="false"  AutoExpand="False" CollapseControlID="MonitorView" Collapsed="true"
    CollapsedSize="0" CollapsedText="Monitoring" ExpandControlID="MonitorView"
    ExpandedSize="130" ExpandedText="Hide-Monitoring"  TargetControlID="Panel2"
    TextLabelID="MonitorView">
</ajaxToolkit:CollapsiblePanelExtender>
<asp:HiddenField ID="Hiddenstuid" runat="server" />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
</div>