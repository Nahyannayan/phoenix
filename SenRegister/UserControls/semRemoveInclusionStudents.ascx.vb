Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_semRemoveInclusionStudents
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim studClass As New studClass
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            ddaccyear = studClass.PopulateAcademicYear(ddaccyear, Session("clm").ToString, Session("sbsuid").ToString)
            BindStudentRemoveGrid()

        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindStudentRemoveGrid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        'Dim strQuery As String = "select * from sen_ace_students a " & _
        '                         " inner join oasis.dbo.student_m b on a.stu_id= b.stu_id " & _
        '                         " and a.stu_bsu_id='" & Hiddenbsuid.Value & "' and a.active='false' order by a.stu_id"


        Dim strQuery = "SELECT sn.STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,''))," & _
                       " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR " & _
                       " FROM oasis.dbo.STUDENT_M AS A INNER JOIN oasis.dbo.GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" & _
                       " INNER JOIN oasis.dbo.SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " & _
                       " INNER JOIN sen_ace_students sn on sn.stu_id = A.stu_id " & _
                       " WHERE STU_ACD_ID = " + ddaccyear.SelectedValue.ToString + " and sn.active='false'"


        Dim txtnumber As String
        Dim txtname As String
        Dim txtGrade As String
        Dim txtSection As String


        If Grdstudentremove.Rows.Count > 0 Then
            txtnumber = DirectCast(Grdstudentremove.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(Grdstudentremove.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txtGrade = DirectCast(Grdstudentremove.HeaderRow.FindControl("txtGrade"), TextBox).Text.Trim()
            txtSection = DirectCast(Grdstudentremove.HeaderRow.FindControl("txtSection"), TextBox).Text.Trim()

            If txtnumber.Trim() <> "" Then
                strQuery &= " and STU_NO like '%" & txtnumber.Replace(" ", "") & "%' "
            End If

            If txtname.Trim() <> "" Then
                strQuery &= " and isnull(STU_FIRSTNAME,'')+ isnull(STU_MIDNAME,'')+isnull(STU_LASTNAME,'') like '%" & txtname.Replace(" ", "") & "%' "
            End If

            If txtGrade.Trim() <> "" Then
                strQuery &= " and GRM_DISPLAY like '%" & txtGrade.Replace(" ", "") & "%' "
            End If

            If txtSection.Trim() <> "" Then
                strQuery &= " and SCT_DESCR like '%" & txtSection.Replace(" ", "") & "%' "
            End If

        End If

        strQuery &= " order by STU_FIRSTNAME "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("stu_id")
            dt.Columns.Add("stu_no")
            dt.Columns.Add("STU_NAME")
            dt.Columns.Add("GRM_DISPLAY")
            dt.Columns.Add("SCT_DESCR")

            Dim dr As DataRow = dt.NewRow()
            dr("stu_id") = ""
            dr("stu_no") = ""
            dr("STU_NAME") = ""
            dr("GRM_DISPLAY") = ""
            dr("SCT_DESCR") = ""

            dt.Rows.Add(dr)
            Grdstudentremove.DataSource = dt
            Grdstudentremove.DataBind()
            DirectCast(Grdstudentremove.FooterRow.FindControl("btnremove"), Button).Visible = False
        Else
            Grdstudentremove.DataSource = ds
            Grdstudentremove.DataBind()
            DirectCast(Grdstudentremove.FooterRow.FindControl("btnremove"), Button).Visible = True
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(Grdstudentremove.FooterRow.FindControl("btnremove"), Button))

        End If

        If Grdstudentremove.Rows.Count > 0 Then

            DirectCast(Grdstudentremove.HeaderRow.FindControl("txtnumber"), TextBox).Text = txtnumber
            DirectCast(Grdstudentremove.HeaderRow.FindControl("txtname"), TextBox).Text = txtname
            DirectCast(Grdstudentremove.HeaderRow.FindControl("txtGrade"), TextBox).Text = txtGrade
            DirectCast(Grdstudentremove.HeaderRow.FindControl("txtSection"), TextBox).Text = txtSection

        End If

        If Grdstudentremove.Rows.Count > 0 Then
            lblmessager.Visible = True
        End If



    End Sub

    Protected Sub Grdstudentremove_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grdstudentremove.RowCommand
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim flag = 0
        If e.CommandName = "remove" Then
            For Each row As GridViewRow In Grdstudentremove.Rows
                Dim check As CheckBox = DirectCast(row.FindControl("Check"), CheckBox)
                If check.Checked Then
                    flag = 1
                    Dim stuid = DirectCast(row.FindControl("HiddensStuid"), HiddenField).Value
                    ' Dim strQuery As String = "delete from SEN_ACE_STUDENTS where STU_ID='" & stuid & "' and STU_BSU_ID='" & Hiddenbsuid.Value & "'"

                    Dim strQuery As String = "exec DELETE_SEN_ACE_STUDENTS " + stuid + ",'" + Hiddenbsuid.Value + "'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
                End If
            Next
            If flag = 1 Then
                ''lblsturemovemessage.Text = "Selected student removed from the list."
                ''BindStudentRemoveGrid()
                Session("StuTabIndex") = 2
                'Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()

                'Response.Redirect("senAssignInclusionStudents.aspx" & mInfo)
                Response.Write("<script type='text/javascript'> parent.location.reload() </script>")
            Else
                lblsturemovemessage.Text = "Please select students."
            End If
        End If

        If e.CommandName = "search" Then
            BindStudentRemoveGrid()
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(Grdstudentremove.FooterRow.FindControl("btnremove"), Button))


    End Sub

    Protected Sub ddaccyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddaccyear.SelectedIndexChanged
        BindStudentRemoveGrid()
    End Sub

    Protected Sub Grdstudentremove_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grdstudentremove.PageIndexChanging
        Grdstudentremove.PageIndex = e.NewPageIndex
        BindStudentRemoveGrid()
    End Sub

End Class
