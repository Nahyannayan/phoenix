<%@ Control Language="VB" AutoEventWireup="false" CodeFile="semAddInclusionMembers.ascx.vb"
    Inherits="SenRegister_UserControls_semAddInclusionMembers" %>


<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">


<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="center" colspan="4">
            <asp:Label ID="lblmessage" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="left" width="20%"><span class="field-label">Section </span>
        </td>
        <td align="left" width="30%">
            <asp:DropDownList ID="ddDepartments" runat="server">
            </asp:DropDownList></td>

        <td align="left" width="20%"></td>

        <td align="left" width="30%"></td>
    </tr>
    <tr>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">Add Section Members</td>
    </tr>
    <tr>
        <td align="left">
            <asp:GridView ID="GrdAssigndeptmembers" runat="server" AutoGenerateColumns="False" ShowFooter="True" AllowPaging="True" Width="100%"
                CssClass="table table-bordered table-row" PageSize="15">

                <Columns>
                    <asp:TemplateField HeaderText="Name">
                        <HeaderTemplate>
                            Name
                                                    <br />
                            <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageSearch" runat="server" CommandName="search" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("ENAME") %>
                            <asp:HiddenField ID="Hiddenempid" runat="server" Value='<%# Eval("EMP_ID") %>' />
                        </ItemTemplate>
                        <ItemStyle />

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Designation">
                        <HeaderTemplate>
                            Designation
                                                     <br />
                            <asp:DropDownList ID="dddes" AutoPostBack="true" OnSelectedIndexChanged="ddesschange" runat="server"></asp:DropDownList>

                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="HiddenDesId" runat="server" Value='<%# Eval("EMP_DES_ID") %>' />
                            <%# Eval("DES_DESCR") %>
                        </ItemTemplate>
                        <ItemStyle />

                    </asp:TemplateField>
                    <asp:TemplateField>
                        <FooterTemplate>
                            <center>
                                            <asp:Button ID="btnaddmembers" CommandName="AddMembers" CssClass="button" runat="server"
                                                Text="Add" /></center>
                            <ajaxToolkit:ConfirmButtonExtender ID="CBE1" runat="server" ConfirmText="Selected members will be assigned to section .Do you want to continue ?"
                                TargetControlID="btnaddmembers">
                            </ajaxToolkit:ConfirmButtonExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <center>
                                            <asp:CheckBox ID="Checklist" runat="server" />
                                            <center>
                        </ItemTemplate>
                        <ItemStyle />

                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" />
                <EmptyDataRowStyle />
                <SelectedRowStyle />
                <HeaderStyle />
                <EditRowStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>
            <asp:HiddenField ID="Hiddenbsuid" runat="server" />
        </td>
    </tr>
</table>
