<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senIEP.ascx.vb" Inherits="SenRegister_UserControls_senIEP" %>
<asp:HiddenField ID="Hiddenstuid" runat="server" />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:HiddenField ID="T15HiddenFieldIEPid" runat="server" />
<asp:Label ID="lblstatusmessage" runat="server" EnableTheming="False" CssClass="error"></asp:Label><br />
<br />
<div >
    <table id="T15T2" runat="server" visible="true"
        cellpadding="5" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg" colspan="4">IEP Overview</td>
        </tr>
        <tr>
            <td width="20%"><span class="field-label">Author</span></td>
            <td width="30%">
                <asp:DropDownList ID="T15ddStaffs" runat="server" ValidationGroup="T15Save">
                </asp:DropDownList></td>
            <td width="20%"><span class="field-label">IEP Tracking Category</span>
            </td>
            <td width="30%">
                <asp:DropDownList ID="T15ddIEPTrackingCategory" runat="server" ValidationGroup="T15Save">
                </asp:DropDownList></td>
        </tr>

        <tr>
            <td width="20%"><span class="field-label">Date Written</span></td>
            <td width="30%">
                <asp:TextBox ID="T15txtdatewritten" runat="server" onfocus="javascript:this.blur();return false;" ValidationGroup="T15Save"></asp:TextBox>
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />
            </td>
            <td width="20%"><span class="field-label">Comments</span></td>
            <td width="30%">
                <asp:TextBox ID="T15txtdatewrittencomments" runat="server" 
                    TextMode="MultiLine"  ValidationGroup="T15Save"></asp:TextBox></td>
        </tr>
        <tr>
            <td width="20%"><span class="field-label">Review Date</span></td>
            <td width="30%">
                <asp:TextBox ID="T15txtdatereview" runat="server" onfocus="javascript:this.blur();return false;" ValidationGroup="T15Save"></asp:TextBox>
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar.gif" />
            </td>
            <td colspan="2"> </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Panel ID="T15Panel1" runat="server" Width="100%" >
                    <asp:Button ID="T15btnsave" runat="server" CssClass="button" Text="Save" ValidationGroup="T15Save" />
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:RequiredFieldValidator ID="T15RequiredFieldValidator1" runat="server" Display="None"
        SetFocusOnError="True" ValidationGroup="T15Save" ControlToValidate="T15ddStaffs"
        ErrorMessage="Please Select Inclusion Member (Author)" InitialValue="-1"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="T15RequiredFieldValidator2" runat="server" Display="None"
        SetFocusOnError="True" ValidationGroup="T15Save" ControlToValidate="T15ddIEPTrackingCategory"
        ErrorMessage="Please Select IEP Category" InitialValue="-1"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="T15RequiredFieldValidator3" runat="server" Display="None"
        SetFocusOnError="True" ValidationGroup="T15Save" ControlToValidate="T15txtdatewritten"
        ErrorMessage="Please Enter Date Written"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="T15RequiredFieldValidator5" runat="server" Display="None"
        SetFocusOnError="True" ValidationGroup="T15Save" ControlToValidate="T15txtdatereview"
        ErrorMessage="Please Enter Date Review"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="T15Save" />
    <br />
    <table id="T15T1" runat="server" visible="false" width="100%">
        <tr>
            <td class="title-bg" colspan="4">Review Comments</td>
        </tr>
        <tr>
            <td width="20%"><span class="field-label">Comments</span></td>
            <td width="30%">
                <asp:TextBox ID="T15txtdatereviewcomments" runat="server" 
                    TextMode="MultiLine"  ValidationGroup="T15Update"></asp:TextBox></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Panel ID="T15Panel2" runat="server" Visible="False" Width="100%" >
                    <asp:Button ID="T15btnupdate" runat="server" CssClass="button" Text="Update" ValidationGroup="T15Update" />
                    <asp:Button ID="T15btncancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:RequiredFieldValidator ID="T15RequiredFieldValidator7" runat="server" ControlToValidate="T15txtdatereviewcomments"
        Display="None" ErrorMessage="Please Enter Comments" SetFocusOnError="True" ValidationGroup="T15Update"></asp:RequiredFieldValidator>
    <ajaxToolkit:CalendarExtender ID="T15CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
        TargetControlID="T15txtdatewritten">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T15CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image2"
        TargetControlID="T15txtdatereview">
    </ajaxToolkit:CalendarExtender>
    <table  width="100%">
        <tr>
            <td class="title-bg">
                IEP Information</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T15GrdIEP" AutoGenerateColumns="false" runat="server" AllowPaging="True" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="IEP Category">
                            <HeaderTemplate>
                              IEP Category
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("IEP_CATEGORY_DES")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Written">
                            <HeaderTemplate>
                                Date Written
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("DATE_WRITTEN")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comments">
                            <HeaderTemplate>
                               Comments
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T15lblview" runat="server" Text='<%#Eval("tempview1")%>'></asp:Label>
                                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T15Panel1" runat="server" CssClass="panel-cover">
                                    <%#Eval("DATE_WRITTEN_COMMENTS")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T15CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T15lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview1")%>' ExpandControlID="T15lblview"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T15Panel1"
                                    TextLabelID="T15lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Review Date">
                            <HeaderTemplate>
                                Review Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("REVIEW_DATE")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comments">
                            <HeaderTemplate>
                                Comments
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T15lblview2" runat="server" Text='<%#Eval("tempview2")%>'></asp:Label>
                                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T15Panel12" runat="server" CssClass="panel-cover">
                                    <%#Eval("REVIEW_DATE_COMMENTS")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T15CollapsiblePanelExtender12" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T15lblview2" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview2")%>' ExpandControlID="T15lblview2"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T15Panel12"
                                    TextLabelID="T15lblview2">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Review Entry Date">
                            <HeaderTemplate>
                               Review Entry Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("REVIEW_ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Author">
                            <HeaderTemplate>
                                 Author
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("empname")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Update">
                            <HeaderTemplate>
                               Update
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="T15lnkupdate" CommandName="Updating" CausesValidation="false"
                                    CommandArgument='<%#Eval("IEP_ID")%>' runat="server">Update</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                     <RowStyle CssClass="griditem"  Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg">IEP Tracking</td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="title-bg">Literacy</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="T8GrdIEPTrackingLiteracy" runat="server" AllowPaging="True" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                                PageSize="5" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Date Written">
                                        <HeaderTemplate>
                                            Date
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("DATE_WRITTEN")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comments">
                                        <HeaderTemplate>
                                          Comments
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="T8lblview" runat="server" Text='<%#Eval("tempview1")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                            <asp:Panel ID="T8Panel1" runat="server" CssClass="panel-cover">
                                                <%#Eval("DATE_WRITTEN_COMMENTS")%>
                                            </asp:Panel>
                                            <ajaxToolkit:CollapsiblePanelExtender ID="T8CollapsiblePanelExtender1" runat="server"
                                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T8lblview" Collapsed="true"
                                                CollapsedSize="0" CollapsedText='<%#Eval("tempview1")%>' ExpandControlID="T8lblview"
                                                ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T8Panel1"
                                                TextLabelID="T8lblview">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Review Date">
                                        <HeaderTemplate>
                                           Review Date
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("REVIEW_DATE")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comments">
                                        <HeaderTemplate>
                                            Comments
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="T8lblview2" runat="server" Text='<%#Eval("tempview2")%>'></asp:Label>
                                            <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                            <asp:Panel ID="T8Panel12" runat="server" CssClass="panel-cover">
                                                <%#Eval("REVIEW_DATE_COMMENTS")%>
                                            </asp:Panel>
                                            <ajaxToolkit:CollapsiblePanelExtender ID="T8CollapsiblePanelExtender12" runat="server"
                                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T8lblview2" Collapsed="true"
                                                CollapsedSize="0" CollapsedText='<%#Eval("tempview2")%>' ExpandControlID="T8lblview2"
                                                ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T8Panel12"
                                                TextLabelID="T8lblview2">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Review Entry Date">
                                        <HeaderTemplate>
                                          Review Entry Date
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("REVIEW_ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Author">
                                        <HeaderTemplate>
                                        Author
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("empname")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="100%">
                    <tr>
                        <td class="title-bg">Maths</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="T8GrdIEPTrackingMaths" runat="server" AllowPaging="True" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                                PageSize="5" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Date Written">
                                        <HeaderTemplate>
                                            Date Written
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("DATE_WRITTEN")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comments">
                                        <HeaderTemplate>
                                           Comments
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="T8lblview" runat="server" Text='<%#Eval("tempview1")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                            <asp:Panel ID="T8Panel1" runat="server" CssClass="panel-cover">
                                                <%#Eval("DATE_WRITTEN_COMMENTS")%>
                                            </asp:Panel>
                                            <ajaxToolkit:CollapsiblePanelExtender ID="T8CollapsiblePanelExtender1" runat="server"
                                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T8lblview" Collapsed="true"
                                                CollapsedSize="0" CollapsedText='<%#Eval("tempview1")%>' ExpandControlID="T8lblview"
                                                ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T8Panel1"
                                                TextLabelID="T8lblview">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Review Date">
                                        <HeaderTemplate>
                                           Review Date
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("REVIEW_DATE")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comments">
                                        <HeaderTemplate>
                                          Comments
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="T8lblview2" runat="server" Text='<%#Eval("tempview2")%>'></asp:Label>
                                            <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                            <asp:Panel ID="T8Panel12" runat="server" CssClass="panel-cover">
                                                <%#Eval("REVIEW_DATE_COMMENTS")%>
                                            </asp:Panel>
                                            <ajaxToolkit:CollapsiblePanelExtender ID="T8CollapsiblePanelExtender12" runat="server"
                                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T8lblview2" Collapsed="true"
                                                CollapsedSize="0" CollapsedText='<%#Eval("tempview2")%>' ExpandControlID="T8lblview2"
                                                ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T8Panel12"
                                                TextLabelID="T8lblview2">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Review Entry Date">
                                        <HeaderTemplate>
                                           >Review Entry Date
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("REVIEW_ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Author">
                                        <HeaderTemplate>
                                        Author
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("empname")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem"  Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="100%">
                    <tr>
                        <td class="title-bg">Behavioural</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="T8GrdIEPTrackingBehavioural" runat="server" AllowPaging="True" CssClass="table table-bordered table-row"
                                AutoGenerateColumns="false" PageSize="5" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Date Written">
                                        <HeaderTemplate>
                                         Date Written
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("DATE_WRITTEN")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comments">
                                        <HeaderTemplate>
                                            Comments
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="T8lblview" runat="server" Text='<%#Eval("tempview1")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                            <asp:Panel ID="T8Panel1" runat="server" CssClass="panel-cover">
                                                <%#Eval("DATE_WRITTEN_COMMENTS")%>
                                            </asp:Panel>
                                            <ajaxToolkit:CollapsiblePanelExtender ID="T8CollapsiblePanelExtender1" runat="server"
                                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T8lblview" Collapsed="true"
                                                CollapsedSize="0" CollapsedText='<%#Eval("tempview1")%>' ExpandControlID="T8lblview"
                                                ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T8Panel1"
                                                TextLabelID="T8lblview">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Review Date">
                                        <HeaderTemplate>
                                           Review Date
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("REVIEW_DATE")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comments">
                                        <HeaderTemplate>
                                           Comments
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="T8lblview2" runat="server" Text='<%#Eval("tempview2")%>'></asp:Label>
                                            <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                            <asp:Panel ID="T8Panel12" runat="server" CssClass="panel-cover">
                                                <%#Eval("REVIEW_DATE_COMMENTS")%>
                                            </asp:Panel>
                                            <ajaxToolkit:CollapsiblePanelExtender ID="T8CollapsiblePanelExtender12" runat="server"
                                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T8lblview2" Collapsed="true"
                                                CollapsedSize="0" CollapsedText='<%#Eval("tempview2")%>' ExpandControlID="T8lblview2"
                                                ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T8Panel12"
                                                TextLabelID="T8lblview2">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Review Entry Date">
                                        <HeaderTemplate>
                                          Review Entry Date
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("REVIEW_ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Author">
                                        <HeaderTemplate>
                                           Author
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("empname")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
</div>
