Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Math

Partial Class SenRegister_UserControls_senStudentBasicInformation
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindControls()
            BindLockSealFiles()
            LockFileSealEnable()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub LockFileSealEnable()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EMP_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        Dim val As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "CHECK_PRINCIPAL_INC_ADMIN", pParms)
        CheckLock.Enabled = val
        CheckSeal.Enabled = val
        CheckActiveInactive.Enabled = val

    End Sub
    Public Sub BindLockSealFiles()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select LOCK_FILE,SEAL_FILE,ACTIVE_INACTIVE from SEN_ACE_STUDENTS where STU_ID='" & Hiddenstuid.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("LOCK_FILE").ToString() <> "" Then
                CheckLock.Checked = ds.Tables(0).Rows(0).Item("LOCK_FILE")
                Session("LOCK_FILE") = ds.Tables(0).Rows(0).Item("LOCK_FILE")
            Else
                Session("LOCK_FILE") = False
            End If
            If ds.Tables(0).Rows(0).Item("SEAL_FILE").ToString() <> "" Then
                CheckSeal.Checked = ds.Tables(0).Rows(0).Item("SEAL_FILE")
                Session("SEAL_FILE") = ds.Tables(0).Rows(0).Item("SEAL_FILE")
            Else
                Session("SEAL_FILE") = False
            End If
            If ds.Tables(0).Rows(0).Item("ACTIVE_INACTIVE").ToString() <> "" Then
                CheckActiveInactive.Checked = ds.Tables(0).Rows(0).Item("ACTIVE_INACTIVE")
            End If
        End If

        ''check if it the emp is a principle or inclusion admin , then false



    End Sub

    Public Sub BindControls()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim strQuery As String = "select (emp_salute +' ' +emp_fname + ' '+ emp_mname + ' ' +emp_lname) as empname,ACD_AGE_CUTOFF,stu_no,stu_bsu_id, stu_firstname,stu_lastname,stu_midname,stu_gender,stu_dob,sct_descr,grm_display,stm_descr,shf_descr,STU_PHOTOPATH from STUDENT_M " & _
                                 " LEFT JOIN SECTION_M ON   SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID " & _
                                 " LEFT JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                                 " LEFT JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
                                 " LEFT JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID " & _
                                 " LEFT JOIN ACADEMICYEAR_D ON ACADEMICYEAR_D.ACD_ID =STUDENT_M.STU_ACD_ID  " & _
                                 " LEFT JOIN EMPLOYEE_M ON EMPLOYEE_M.EMP_ID=SECTION_M.SCT_EMP_ID " & _
                                 " where STUDENT_M.stu_id='" & Hiddenstuid.Value & "' and stu_bsu_id='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            lblstuno.Text = ds.Tables(0).Rows(0).Item("stu_no").ToString()
            lblstulastname.Text = ds.Tables(0).Rows(0).Item("stu_lastname").ToString()
            lblstufirstname.Text = ds.Tables(0).Rows(0).Item("stu_firstname").ToString()
            lblstumiddlename.Text = ds.Tables(0).Rows(0).Item("stu_midname").ToString()
            lblgender.Text = ds.Tables(0).Rows(0).Item("stu_gender").ToString()
            If lblgender.Text = "M" Then
                lblgender.Text = "Male"
            Else
                lblgender.Text = "Female"
            End If
            lbldob.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("stu_dob").ToString()).ToString("dd/MMM/yyyy")
            lblclass.Text = ds.Tables(0).Rows(0).Item("grm_display").ToString() & " - " & ds.Tables(0).Rows(0).Item("sct_descr").ToString()
            lblclassteacher.Text = ds.Tables(0).Rows(0).Item("empname").ToString()
            lblshift.Text = ds.Tables(0).Rows(0).Item("shf_descr").ToString()
            lblstream.Text = ds.Tables(0).Rows(0).Item("stm_descr").ToString()
            'If ds.Tables(0).Rows(0).Item("STU_PHOTOPATH").ToString().Trim() <> "" Then
            '    Image2.ImageUrl = ds.Tables(0).Rows(0).Item("STU_PHOTOPATH").ToString()
            'End If
            CalculateAge(ds)
        End If
    End Sub
    Public Function ExactAge(ByVal BirthDate As Object) As String

        Dim yer As Integer, mon As Integer, d As Integer
        Dim dt As Date
        Dim sAns As String

        If Not IsDate(BirthDate) Then Exit Function
        dt = CDate(BirthDate)
        If dt > Now Then Exit Function

        yer = Year(dt)
        mon = Month(dt)
        d = Day(dt)
        yer = (Today.Year) - yer
        mon = (Today.Month) - mon
        d = (Today.Day) - d

        If Sign(d) = -1 Then
            d = 30 - Abs(d)
            mon = mon - 1
        End If

        If Sign(mon) = -1 Then
            mon = 12 - Abs(mon)
            yer = yer - 1
        End If

        sAns = yer & " year(s) " & mon & " month(s) " ' & d _
        ' & " day(s) old."

        ExactAge = sAns

    End Function

    Public Sub CalculateAge(ByVal ds As DataSet)
        'Dim dob As Date = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_DOB"))
        'Dim cutoffdate As Date = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ACD_AGE_CUTOFF"))
        'Dim years As Integer = Math.Truncate(((cutoffdate - dob).TotalDays) / 365)
        'Dim months As Integer = Math.Truncate((((cutoffdate - dob).TotalDays) Mod 365) / 30.436875)
        'If months = 12 Then
        '    years = years + 1
        'End If

        'lblage.Text = years

        lblage.Text = ExactAge(Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_DOB")))

        ''Check Summmer Born
        Dim dt As Date
        dt = CDate(Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_DOB")))
        Dim m As Integer = Month(dt)

        If m = 6 Or m = 7 Or m = 8 Then
            Image1.ImageUrl = "~/Images/tick.gif"
        Else
            Image1.ImageUrl = "~/Images/cross.png"
        End If

    End Sub

    Protected Sub CheckLock_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckLock.CheckedChanged
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "UPDATE SEN_ACE_STUDENTS SET LOCK_FILE='" & CheckLock.Checked & "' WHERE STU_ID='" & Hiddenstuid.Value & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
        Dim Encr_decrData As New Encryption64
        Dim stu_id As String = Encr_decrData.Encrypt(Hiddenstuid.Value)
        Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        Response.Redirect("senStudentProfile.aspx?Stu_id=" & stu_id & mInfo)

    End Sub

    Protected Sub CheckSeal_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckSeal.CheckedChanged
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "UPDATE SEN_ACE_STUDENTS SET SEAL_FILE='" & CheckSeal.Checked & "' WHERE STU_ID='" & Hiddenstuid.Value & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
        Dim Encr_decrData As New Encryption64
        Dim stu_id As String = Encr_decrData.Encrypt(Hiddenstuid.Value)
        Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        Response.Redirect("senStudentProfile.aspx?Stu_id=" & stu_id & mInfo)

    End Sub

    Protected Sub CheckActiveInactive_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckActiveInactive.CheckedChanged
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "UPDATE SEN_ACE_STUDENTS SET ACTIVE_INACTIVE='" & CheckActiveInactive.Checked & "' WHERE STU_ID='" & Hiddenstuid.Value & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
        Dim Encr_decrData As New Encryption64
        Dim stu_id As String = Encr_decrData.Encrypt(Hiddenstuid.Value)
        Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        Response.Redirect("senStudentProfile.aspx?Stu_id=" & stu_id & mInfo)

    End Sub
End Class
