Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_semAddInclusionMembers
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            BindDepartments()
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindGrid()

        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindDesignation()
        Dim dddes As DropDownList = DirectCast(GrdAssigndeptmembers.HeaderRow.FindControl("dddes"), DropDownList)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select des_id , des_descr from EMPDESIGNATION_M where des_flag='SD' order by des_descr "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dddes.DataSource = ds
        dddes.DataValueField = "des_id"
        dddes.DataTextField = "des_descr"
        dddes.DataBind()
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Select a Designation"
        dddes.Items.Insert(0, list)

    End Sub
    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select EMP_ID, isnull(EMP_FNAME,'')+' ' +isnull(EMP_MNAME,'')+' '+isnull(EMP_LNAME,'') ENAME, DES_DESCR,EMP_DES_ID from dbo.EMPLOYEE_M a " & _
                        " inner join  dbo.EMPDESIGNATION_M b on a.EMP_DES_ID=b.DES_ID where EMP_BSU_ID='" & Hiddenbsuid.Value & "' and des_flag='SD' and EMP_bACTIVE='True' and EMP_STATUS <> 4 "
        Dim txtname As String
        Dim desigid As DropDownList

        If GrdAssigndeptmembers.Rows.Count > 0 Then
            txtname = DirectCast(GrdAssigndeptmembers.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            desigid = DirectCast(GrdAssigndeptmembers.HeaderRow.FindControl("dddes"), DropDownList)

            If txtname.Trim() <> "" Then
                str_query &= " and isnull(EMP_FNAME,'')+ isnull(EMP_MNAME,'')+isnull(EMP_LNAME,'') like '%" & txtname.Replace(" ", "") & "%' "
            End If
            If desigid.SelectedValue > -1 Then
                str_query &= " and EMP_DES_ID= '" & desigid.SelectedValue & "'"
            End If

        End If

        str_query &= " order by EMP_FNAME "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("ENAME")
            dt.Columns.Add("EMP_ID")
            dt.Columns.Add("EMP_DES_ID")
            dt.Columns.Add("DES_DESCR")

            Dim dr As DataRow = dt.NewRow()
            dr("ENAME") = ""
            dr("EMP_ID") = ""
            dr("EMP_DES_ID") = ""
            dr("DES_DESCR") = ""
            dt.Rows.Add(dr)
            GrdAssigndeptmembers.DataSource = dt
            GrdAssigndeptmembers.DataBind()
            DirectCast(GrdAssigndeptmembers.FooterRow.FindControl("btnaddmembers"), Button).Visible = False
        Else
            GrdAssigndeptmembers.DataSource = ds
            GrdAssigndeptmembers.DataBind()
            DirectCast(GrdAssigndeptmembers.FooterRow.FindControl("btnaddmembers"), Button).Visible = True
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdAssigndeptmembers.FooterRow.FindControl("btnaddmembers"), Button))
        End If

        If GrdAssigndeptmembers.Rows.Count > 0 Then
            BindDesignation()
            DirectCast(GrdAssigndeptmembers.HeaderRow.FindControl("txtname"), TextBox).Text = txtname
            If desigid Is Nothing Then
            Else
                DirectCast(GrdAssigndeptmembers.HeaderRow.FindControl("dddes"), DropDownList).SelectedValue = desigid.SelectedValue
            End If
        End If


    End Sub

    Public Sub BindDepartments()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim str_query = "select * from SEN_DEPARTMENTS"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddDepartments.DataSource = ds
        ddDepartments.DataValueField = "DEPT_ID"
        ddDepartments.DataTextField = "DEPT_DESC"
        ddDepartments.DataBind()
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Select a Section"
        ddDepartments.Items.Insert(0, list)
      

    End Sub

    Protected Sub GrdAssigndeptmembers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdAssigndeptmembers.RowCommand
        Dim flag = 0
        lblmessage.Text = ""
        If e.CommandName = "AddMembers" Then
            If ddDepartments.SelectedIndex > 0 Then

                Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
                For Each row As GridViewRow In GrdAssigndeptmembers.Rows
                    Dim check As CheckBox = DirectCast(row.FindControl("Checklist"), CheckBox)
                    If check.Checked Then
                        flag = 1
                        Dim empid As String = DirectCast(row.FindControl("Hiddenempid"), HiddenField).Value
                        Dim pParms(3) As SqlClient.SqlParameter
                        pParms(0) = New SqlClient.SqlParameter("@EMP_ID", empid)
                        pParms(1) = New SqlClient.SqlParameter("@EMP_BSU_ID", Hiddenbsuid.Value)
                        pParms(2) = New SqlClient.SqlParameter("@EMP_DEPT_ID", ddDepartments.SelectedValue)
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_MEMBERS", pParms)
                    End If
                Next
                If flag = 1 Then
                    lblmessage.Text = "Members added to selected Section."
                    Session("MemberTabIndex") = 1
                    'Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()

                    'Response.Redirect("senAssignInclusionMembers.aspx" & mInfo)
                    ' ''MainGridView()
                    Response.Write("<script type='text/javascript'> parent.location.reload() </script>")
                Else
                    lblmessage.Text = "Please Select Members"
                End If
            Else
                lblmessage.Text = "Please Select a Section"
            End If

            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdAssigndeptmembers.FooterRow.FindControl("btnaddmembers"), Button))

        End If
        If e.CommandName = "search" Then
            BindGrid()
        End If

    End Sub

    Protected Sub GrdAssigndeptmembers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdAssigndeptmembers.PageIndexChanging

        GrdAssigndeptmembers.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub

    Protected Sub ddesschange(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid()
    End Sub
End Class
