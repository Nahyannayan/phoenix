<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senMonitoringIEP.ascx.vb" Inherits="SenRegister_UserControls_senMonitoringIEP" %>
<div >
    <table width="100%">
        <tr>
            <td align="right">
                <asp:LinkButton ID="MonitorView" OnClientClick="javascript:return false;" runat="server">Monitoring</asp:LinkButton>
                <asp:Panel ID="Panel2" runat="server" CssClass="panell-cover">
                    <table border="0">
                        <tr>
                            <td colspan="4">
                                <asp:CheckBox ID="MonitoringCheckBox" runat="server" Text="Monitoring" CssClass="field-label" /></td>
                           
                        </tr>
                        <tr>
                           <%-- <td id="TD1" colspan="2">
                                <table>
                                    <tr>--%>
                                        <td width="20%"><span class="field-label"> Due Date</span></td>
                                       
                                        <td width="30%">
                                            <asp:TextBox ID="txtdatemonotor" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        </td>
                                       
                                        <td colspan="2">
                                            <asp:LinkButton ID="MonitoringSave" CausesValidation="false" runat="server">Save</asp:LinkButton></td>
                                <%--    </tr>
                                    <tr>
                                    </tr>
                                </table>--%>
                               
                          <%--  </td>--%>
                        </tr>
                        <tr>
                            <td colspan="4">
                                 <asp:LinkButton ID="MonitorNotes" OnClientClick="javascript:return false;" runat="server">Notes</asp:LinkButton>
                                <asp:Panel ID="Panel1" runat="server">
                                    <asp:TextBox ID="txtMonitornotes" SkinID="MultiText" runat="server" Height="62px" TextMode="MultiLine" Width="287px"></asp:TextBox>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>

                </asp:Panel>
                <br />

            </td>
        </tr>
    </table>
<ajaxToolkit:CalendarExtender ID="T3CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
    TargetControlID="txtdatemonotor">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
    AutoCollapse="False" AutoExpand="False" CollapseControlID="MonitorNotes" Collapsed="true"
    CollapsedSize="0" CollapsedText="Notes" ExpandControlID="MonitorNotes"
    ExpandedText="Hide-Notes" ScrollContents="false" TargetControlID="Panel1"
    TextLabelID="MonitorNotes">
</ajaxToolkit:CollapsiblePanelExtender><ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
    AutoCollapse="False" ScrollContents="false"  AutoExpand="False" CollapseControlID="MonitorView" Collapsed="true"
    CollapsedSize="0" CollapsedText="Monitoring" ExpandControlID="MonitorView"
    ExpandedText="Hide-Monitoring"  TargetControlID="Panel2"
    TextLabelID="MonitorView">
</ajaxToolkit:CollapsiblePanelExtender>
<asp:HiddenField ID="Hiddenstuid" runat="server" />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
</div>