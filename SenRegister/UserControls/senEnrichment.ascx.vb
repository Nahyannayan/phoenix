Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senEnrichment
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Not IsPostBack Then
        '    Dim Encr_decrData As New Encryption64
        '    Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
        '    Hiddenstuid.Value = stu_id
        '    Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
        '    BindPrimaryDetails()
        '    BindControls()
        '    BindGrid()
        '    BindNotes()
        'End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub BindNotes()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select  '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(ENRICH_NOTE,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, entry_date,ENRICH_NOTE,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from SEN_DEPARTMENT_ENRICHMENT_NOTES a " & _
                                 " inner join oasis.dbo.employee_m b on a.entry_emp_id= b.emp_id " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "'order by a.entry_date desc "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T11GrdNotes.DataSource = ds
        T11GrdNotes.DataBind()
    End Sub
    Public Sub BindControls()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_ENRICHMENT_CATEGORY"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T11ddENRICHCategory.DataSource = ds
        T11ddENRICHCategory.DataTextField = "ENRICH_CATEGORY_DESC"
        T11ddENRICHCategory.DataValueField = "ENRICH_CATEGORY_ID"
        T11ddENRICHCategory.DataBind()
        Dim list As New ListItem
        list.Text = "Select a Enrichment Category"
        list.Value = "-1"
        T11ddENRICHCategory.Items.Insert(0, list)

        ''Staffs
        strQuery = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id and a.EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        T11ddStaffs.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T11ddstaffnotes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T11ddStaffs.DataTextField = "empname"
        T11ddstaffnotes.DataTextField = "empname"
        T11ddStaffs.DataValueField = "EMP_ID"
        T11ddstaffnotes.DataValueField = "EMP_ID"
        T11ddStaffs.DataBind()
        T11ddstaffnotes.DataBind()
        Dim list1 As New ListItem
        list1.Text = "Select an Inclusion Member"
        list1.Value = "-1"
        T11ddStaffs.Items.Insert(0, list1)
        T11ddstaffnotes.Items.Insert(0, list1)

    End Sub
    Public Sub BindPrimaryDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select c.stu_doj,b.BSU_NAME,b.BSU_SHORTNAME,ENTRY_DATE from SEN_DEPARTMENT_STUDENTS a " & _
                                 " inner join oasis.dbo.BUSINESSUNIT_M b on a.stu_bsu_id = b.bsu_id " & _
                                 " inner join oasis.dbo.student_m c on c.stu_id= a.stu_id  " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.STU_BSU_ID='" & Hiddenbsuid.Value & "' and a.dept_id='11' and a.active='true' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            T11lbldateenteredlabel.Text = "Date Entered " & ds.Tables(0).Rows(0).Item("BSU_SHORTNAME").ToString()
            T11lbldateentered.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("stu_doj").ToString()).ToString("dd/MMM/yyyy")
            T11lblDateenteredenrichment.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ENTRY_DATE").ToString()).ToString("dd/MMM/yyyy")
            Try
                T11lblTimeLag.Text = DateDiff(DateInterval.Day, Convert.ToDateTime(T11lbldateentered.Text), Convert.ToDateTime(T11lblDateenteredenrichment.Text), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.System) & " Days"
            Catch ex As Exception

            End Try
        End If
    End Sub

    Public Sub BindGrid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim i = 0
        Dim ds As DataSet
        For i = 1 To 6
            Dim strQuery As String = " select '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(MATERIALS_COMMENT_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, MATERIALS_COMMENT_TEXT,ENTRY_DATE,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from SEN_DEPARTMENT_ENRICHMENT a " & _
                                 " inner join oasis.dbo.employee_m  b on a.entry_emp_id=b.emp_id " & _
                                 " where a.ENRICH_CATEGORY_ID='" & i & "' and a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "' order by a.ENTRY_DATE desc "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

            If i = 1 Then
                T11GridView1.DataSource = ds
                T11GridView1.DataBind()
            End If
            If i = 2 Then
                T11GridView2.DataSource = ds
                T11GridView2.DataBind()
            End If
            If i = 3 Then
                T11GridView3.DataSource = ds
                T11GridView3.DataBind()
            End If
            If i = 4 Then
                T11GridView4.DataSource = ds
                T11GridView4.DataBind()
            End If
            If i = 5 Then
                T11GridView5.DataSource = ds
                T11GridView5.DataBind()
            End If
            If i = 6 Then
                T11GridView6.DataSource = ds
                T11GridView6.DataBind()
            End If


        Next
    End Sub


    Protected Sub T11btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T11btnsave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@ENRICH_CATEGORY_ID", T11ddENRICHCategory.SelectedValue)
            pParms(3) = New SqlClient.SqlParameter("@MATERIALS_COMMENTS_TEXT", T11txtmatcomments.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T11ddStaffs.SelectedValue)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ENRICHMENT", pParms)
            T11ddENRICHCategory.SelectedIndex = 0
            T11ddStaffs.SelectedIndex = 0
            T11txtmatcomments.Text = ""
            BindGrid()
            lblstatusmessage.Text = "Successfully Saved"
        Catch ex As Exception

        End Try

    End Sub


    Protected Sub T11CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T11CheckBox1.CheckedChanged
        If T11CheckBox1.Checked Then
            T11GridView1.Visible = True
        Else
            T11GridView1.Visible = False
        End If
    End Sub

    Protected Sub T11CheckBox2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T11CheckBox2.CheckedChanged
        If T11CheckBox2.Checked Then
            T11GridView2.Visible = True
        Else
            T11GridView2.Visible = False
        End If
    End Sub

    Protected Sub T11CheckBox3_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T11CheckBox3.CheckedChanged
        If T11CheckBox3.Checked Then
            T11GridView3.Visible = True
        Else
            T11GridView3.Visible = False
        End If
    End Sub

    Protected Sub T11CheckBox4_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T11CheckBox4.CheckedChanged
        If T11CheckBox4.Checked Then
            T11GridView4.Visible = True
        Else
            T11GridView4.Visible = False
        End If
    End Sub

    Protected Sub T11CheckBox5_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T11CheckBox5.CheckedChanged
        If T11CheckBox5.Checked Then
            T11GridView5.Visible = True
        Else
            T11GridView5.Visible = False
        End If
    End Sub

    Protected Sub T11CheckBox6_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T11CheckBox6.CheckedChanged
        If T11CheckBox6.Checked Then
            T11GridView6.Visible = True
        Else
            T11GridView6.Visible = False
        End If
    End Sub

   

    Protected Sub T11btnnotessave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T11btnnotessave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@COMMENT_TEXT", T11txtnotes.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T11ddstaffnotes.SelectedValue)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ENRICHMENT_NOTES", pParms)
            T11ddstaffnotes.SelectedIndex = 0
            T11txtnotes.Text = ""
            BindNotes()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T11GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T11GridView1.PageIndexChanging
        T11GridView1.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T11GridView2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T11GridView2.PageIndexChanging
        T11GridView2.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T11GridView3_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T11GridView3.PageIndexChanging
        T11GridView3.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T11GridView4_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T11GridView4.PageIndexChanging
        T11GridView4.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T11GridView5_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T11GridView5.PageIndexChanging
        T11GridView5.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T11GridView6_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T11GridView6.PageIndexChanging
        T11GridView6.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T11GrdNotes_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T11GrdNotes.PageIndexChanging
        T11GrdNotes.PageIndex = e.NewPageIndex
        BindNotes()
    End Sub
End Class
