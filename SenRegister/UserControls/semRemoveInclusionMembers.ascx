<%@ Control Language="VB" AutoEventWireup="false" CodeFile="semRemoveInclusionMembers.ascx.vb"
    Inherits="SenRegister_UserControls_semRemoveInclusionMembers" %>


<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">


<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="center" colspan="4">
            <asp:Label ID="lblmemberremovemessage" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>
            <span class="field-label">Section</span>
        </td>


        <td>
            <asp:DropDownList ID="ddremovedepartmentmembers" runat="server" AutoPostBack="True">
            </asp:DropDownList></td>

        <td align="left"></td>
        <td align="left"></td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">Remove Section Members</td>
    </tr>
    <tr>
        <td align="left">
            <asp:GridView ID="GrdRemoveMembers" runat="server" AutoGenerateColumns="False" EmptyDataText="No memebers in selected section"
                CssClass="table table-bordered table-row" Width="100%" ShowFooter="True">

                <Columns>
                    <asp:TemplateField HeaderText="Name">
                        <HeaderTemplate>
                            Name
                                              
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="Hiddenmemberid" runat="server" Value='<%# Eval("DEP_MEM_ID") %>' />
                            <asp:HiddenField ID="Hiddenempid" runat="server" Value='<%# Eval("EMP_ID") %>' />
                            <%# Eval("EMP_SALUTE") %>
                                        &nbsp;
                                        <%# Eval("EMP_FNAME") %>
                                        &nbsp;
                                        <%# Eval("EMP_MNAME") %>
                                        &nbsp;
                                        <%# Eval("EMP_LNAME") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Designation">
                        <HeaderTemplate>
                            Designation
                                              
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="HiddenDesId" runat="server" Value='<%# Eval("EMP_DES_ID") %>' />
                            <%# Eval("DES_DESCR") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <FooterTemplate>
                            <center>
                                            <asp:Button ID="btnremovemembers" CssClass="button" CommandName="RemoveMembers" runat="server"
                                                Text="Remove" /></center>
                            <ajaxToolkit:ConfirmButtonExtender ID="CBE1" runat="server" ConfirmText="Selected members will be deleted from section.Do you want to continue ?"
                                TargetControlID="btnremovemembers">
                            </ajaxToolkit:ConfirmButtonExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <center>
                                            <asp:CheckBox ID="Checklist" runat="server" />
                                            <center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" />
                <EmptyDataRowStyle />
                <SelectedRowStyle />
                <HeaderStyle />
                <EditRowStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>
            <asp:HiddenField ID="Hiddenbsuid" runat="server" />

        </td>
    </tr>
</table>
