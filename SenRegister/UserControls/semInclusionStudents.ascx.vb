Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_semInclusionStudents
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim studClass As New studClass
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            ddaccyear = studClass.PopulateAcademicYear(ddaccyear, Session("clm").ToString, Session("sbsuid").ToString)
            BindStudentView()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindStudentView()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        'Dim strQuery As String = "select * from sen_ace_students a " & _
        '                         " inner join oasis.dbo.student_m b on a.stu_id= b.stu_id " & _
        '                         " and a.stu_bsu_id='" & Hiddenbsuid.Value & "' order by a.stu_id"
        Dim strQuery = "SELECT sn.STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," & _
                       " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR " & _
                       " FROM oasis.dbo.STUDENT_M AS A INNER JOIN oasis.dbo.GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" & _
                       " INNER JOIN oasis.dbo.SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " & _
                       " INNER JOIN sen_ace_students sn on sn.stu_id = A.stu_id " & _
                       " WHERE STU_ACD_ID = " + ddaccyear.SelectedValue.ToString + ""


        Dim txtnumber As String
        Dim txtname As String
        Dim txtGrade As String
        Dim txtSection As String


        If GrdView.Rows.Count > 0 Then
            txtnumber = DirectCast(GrdView.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdView.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txtGrade = DirectCast(GrdView.HeaderRow.FindControl("txtGrade"), TextBox).Text.Trim()
            txtSection = DirectCast(GrdView.HeaderRow.FindControl("txtSection"), TextBox).Text.Trim()

            If txtnumber.Trim() <> "" Then
                strQuery &= " and STU_NO like '%" & txtnumber.Replace(" ", "") & "%' "
            End If

            If txtname.Trim() <> "" Then
                strQuery &= " and isnull(STU_FIRSTNAME,'')+ isnull(STU_MIDNAME,'')+isnull(STU_LASTNAME,'') like '%" & txtname.Replace(" ", "") & "%' "
            End If

            If txtGrade.Trim() <> "" Then
                strQuery &= " and GRM_DISPLAY like '%" & txtGrade.Replace(" ", "") & "%' "
            End If

            If txtSection.Trim() <> "" Then
                strQuery &= " and SCT_DESCR like '%" & txtSection.Replace(" ", "") & "%' "
            End If

        End If

        strQuery &= " order by GRM_DISPLAY,SCT_DESCR,STU_FIRSTNAME "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("stu_id")
            dt.Columns.Add("stu_no")
            dt.Columns.Add("STU_NAME")
            dt.Columns.Add("GRM_DISPLAY")
            dt.Columns.Add("SCT_DESCR")

            Dim dr As DataRow = dt.NewRow()
            dr("stu_id") = ""
            dr("stu_no") = ""
            dr("STU_NAME") = ""
            dr("GRM_DISPLAY") = ""
            dr("SCT_DESCR") = ""

            dt.Rows.Add(dr)
            GrdView.DataSource = dt
            GrdView.DataBind()

        Else
            GrdView.DataSource = ds
            GrdView.DataBind()
        End If

        If GrdView.Rows.Count > 0 Then

            DirectCast(GrdView.HeaderRow.FindControl("txtnumber"), TextBox).Text = txtnumber
            DirectCast(GrdView.HeaderRow.FindControl("txtname"), TextBox).Text = txtname
            DirectCast(GrdView.HeaderRow.FindControl("txtGrade"), TextBox).Text = txtGrade
            DirectCast(GrdView.HeaderRow.FindControl("txtSection"), TextBox).Text = txtSection

        End If



    End Sub
    Protected Sub GrdView_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdView.RowCommand
        If e.CommandName = "view" Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Encrypt(e.CommandArgument)
            Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()

            Response.Redirect("senStudentProfile.aspx?Stu_id=" & stu_id & mInfo)
        End If
        If e.CommandName = "search" Then
            BindStudentView()
        End If

    End Sub

    Protected Sub ddaccyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddaccyear.SelectedIndexChanged
        BindStudentView()
    End Sub

    Protected Sub GrdView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdView.PageIndexChanging
        GrdView.PageIndex = e.NewPageIndex
        BindStudentView()
    End Sub
End Class
