Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senMonitoringACe
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindControl()
            LockFileCheck()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(MonitoringSave)
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then
            Panel2.Enabled = False
        End If

    End Sub
    Public Sub BindControl()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select active, MONITORING_DATE,MONITOR_NOTES from SEN_DEPARTMENT_MONITORING  " & _
                                 " where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' and DEPARTMENT_ID='3'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn.Trim, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            MonitoringCheckBox.Checked = ds.Tables(0).Rows(0).Item("active")
            If MonitoringCheckBox.Checked Then
                txtdatemonotor.Text = ds.Tables(0).Rows(0).Item("MONITORING_DATE").ToString()
                txtMonitornotes.Text = ds.Tables(0).Rows(0).Item("MONITOR_NOTES").ToString()
            
            End If
        Else
            MonitoringCheckBox.Checked = False

        End If

        'Recommendations
        strQuery = " select SUB_MONITOR_ACTIVE from SEN_DEPARTMENT_SUB_MONITORING_DATA  " & _
                                " where SUB_MONITOR_STU_ID='" & Hiddenstuid.Value & "'  and SUB_MONITOR_ID='1'"
        ds = SqlHelper.ExecuteDataset(str_conn.Trim, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            CheckRecommendations.Checked = ds.Tables(0).Rows(0).Item("SUB_MONITOR_ACTIVE")
        End If


        'Causes for Concern
        strQuery = " select SUB_MONITOR_ACTIVE from SEN_DEPARTMENT_SUB_MONITORING_DATA  " & _
                              " where SUB_MONITOR_STU_ID='" & Hiddenstuid.Value & "'  and SUB_MONITOR_ID='2'"
        ds = SqlHelper.ExecuteDataset(str_conn.Trim, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            CheckCausesforConcern.Checked = ds.Tables(0).Rows(0).Item("SUB_MONITOR_ACTIVE")
        End If

    End Sub
    Public Sub Monitorsave()
        Dim active = False
        Dim MDate = txtdatemonotor.Text.Trim()
        If MonitoringCheckBox.Checked Then
            active = True
        Else
            active = False
            MDate = ""
        End If

        If CheckRecommendations.Checked Or CheckCausesforConcern.Checked Then
            active = True
        End If

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@DEPARTMENT_ID", "3")
        pParms(3) = New SqlClient.SqlParameter("@MONITORING_DATE", MDate)
        pParms(4) = New SqlClient.SqlParameter("@ACTIVE", active)
        pParms(5) = New SqlClient.SqlParameter("@MONITOR_NOTES", txtMonitornotes.Text.Trim())
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_MONITORING", pParms)

        'Recommendations
        If CheckRecommendations.Checked Then
            active = True
        Else
            active = False
        End If

        Dim pParms1(4) As SqlClient.SqlParameter
        pParms1(0) = New SqlClient.SqlParameter("@SUB_MONITOR_ID", 1) 'Recommendations
        pParms1(1) = New SqlClient.SqlParameter("@SUB_MONITOR_STU_ID", Hiddenstuid.Value)
        pParms1(2) = New SqlClient.SqlParameter("@SUB_MONITOR_BSU_ID", Hiddenbsuid.Value)
        pParms1(3) = New SqlClient.SqlParameter("@SUB_MONITOR_ACTIVE", active)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_SUB_MONITORING_DATA", pParms1)

        'Causes for Concern
        If CheckCausesforConcern.Checked Then
            active = True
        Else
            active = False
        End If

        Dim pParms2(4) As SqlClient.SqlParameter
        pParms2(0) = New SqlClient.SqlParameter("@SUB_MONITOR_ID", 2) 'Causes for Concern
        pParms2(1) = New SqlClient.SqlParameter("@SUB_MONITOR_STU_ID", Hiddenstuid.Value)
        pParms2(2) = New SqlClient.SqlParameter("@SUB_MONITOR_BSU_ID", Hiddenbsuid.Value)
        pParms2(3) = New SqlClient.SqlParameter("@SUB_MONITOR_ACTIVE", active)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_SUB_MONITORING_DATA", pParms2)



        Session("TabIndex") = 3
        'Dim Encr_decrData As New Encryption64
        'Dim stu_id As String = Encr_decrData.Encrypt(Hiddenstuid.Value)
        'Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        'Response.Redirect("senStudentProfile.aspx?Stu_id=" & stu_id & mInfo)

        Response.Write("<script type='text/javascript'> parent.location.reload() </script>")

    End Sub
    Protected Sub MonitoringSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MonitoringSave.Click
        Monitorsave()

    End Sub

    'Protected Sub MonitoringCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MonitoringCheckBox.CheckedChanged
    '    If MonitoringCheckBox.Checked Then
    '        TD1.Visible = True
    '    Else
    '        TD1.Visible = False
    '        Monitorsave()
    '    End If
    'End Sub
End Class
