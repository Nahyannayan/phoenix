Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_semAddInclusionStudents
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim studClass As New studClass
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            ddaccyear = studClass.PopulateAcademicYear(ddaccyear, Session("clm").ToString, Session("sbsuid").ToString)
            BindStudentView()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub BindStudentView()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        'Dim strQuery As String = "select * from sen_ace_students a " & _
        '                         " inner join oasis.dbo.student_m b on a.stu_id= b.stu_id " & _
        '                         " and a.stu_bsu_id='" & Hiddenbsuid.Value & "' order by a.stu_id"
        Dim strQuery = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,''))," & _
                       " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR " & _
                       " FROM oasis.dbo.STUDENT_M AS A INNER JOIN oasis.dbo.GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" & _
                       " INNER JOIN oasis.dbo.SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " & _
                       " WHERE STU_ACD_ID = " + ddaccyear.SelectedValue.ToString + " AND STU_LEAVEDATE IS NULL and STU_CURRSTATUS <>'CN'"


        Dim txtnumber As String
        Dim txtname As String
        Dim txtGrade As String
        Dim txtSection As String


        If GrdStudents.Rows.Count > 0 Then
            txtnumber = DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txtGrade = DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text.Trim()
            txtSection = DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text.Trim()

            If txtnumber.Trim() <> "" Then
                strQuery &= " and STU_NO like '%" & txtnumber.Replace(" ", "") & "%' "
            End If

            If txtname.Trim() <> "" Then
                strQuery &= " and isnull(STU_FIRSTNAME,'')+ isnull(STU_MIDNAME,'')+isnull(STU_LASTNAME,'') like '%" & txtname.Replace(" ", "") & "%' "
            End If

            If txtGrade.Trim() <> "" Then
                strQuery &= " and GRM_DISPLAY like '%" & txtGrade.Replace(" ", "") & "%' "
            End If

            If txtSection.Trim() <> "" Then
                strQuery &= " and SCT_DESCR like '%" & txtSection.Replace(" ", "") & "%' "
            End If

        End If

        strQuery &= "order by GRM_DISPLAY,SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME,STU_NO "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("stu_id")
            dt.Columns.Add("stu_no")
            dt.Columns.Add("STU_NAME")
            dt.Columns.Add("GRM_DISPLAY")
            dt.Columns.Add("SCT_DESCR")

            Dim dr As DataRow = dt.NewRow()
            dr("stu_id") = ""
            dr("stu_no") = ""
            dr("STU_NAME") = ""
            dr("GRM_DISPLAY") = ""
            dr("SCT_DESCR") = ""

            dt.Rows.Add(dr)
            GrdStudents.DataSource = dt
            GrdStudents.DataBind()
            DirectCast(GrdStudents.FooterRow.FindControl("btnSave"), Button).Visible = False
        Else
            GrdStudents.DataSource = ds
            GrdStudents.DataBind()
            DirectCast(GrdStudents.FooterRow.FindControl("btnSave"), Button).Visible = True
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStudents.FooterRow.FindControl("btnSave"), Button))

        End If

        If GrdStudents.Rows.Count > 0 Then

            DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text = txtnumber
            DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text = txtname
            DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text = txtGrade
            DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text = txtSection

        End If



    End Sub

    Protected Sub GrdStudents_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdStudents.RowCommand
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim flag = 0
        If e.CommandName = "save" Then
            For Each row As GridViewRow In GrdStudents.Rows
                Dim check As CheckBox = DirectCast(row.FindControl("Check"), CheckBox)
                If check.Checked Then
                    flag = 1
                    Dim stuid = DirectCast(row.FindControl("HiddensStuid"), HiddenField).Value
                    Dim pParms(2) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@STU_ID", stuid)
                    pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", Hiddenbsuid.Value)
                    lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "INSERT_SEN_ACE_STUDENTS", pParms)
                End If
            Next
            If flag = 0 Then
                lblmessage.Text = "Please select students."
            Else
                Session("StuTabIndex") = 1
                'Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()

                'Response.Redirect("senAssignInclusionStudents.aspx" & mInfo)
                Response.Write("<script type='text/javascript'> parent.location.reload() </script>")
            End If
           
        End If

        If e.CommandName = "search" Then
            BindStudentView()
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStudents.FooterRow.FindControl("btnSave"), Button))


    End Sub

    Protected Sub GrdStudents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdStudents.PageIndexChanging
        GrdStudents.PageIndex = e.NewPageIndex
        BindStudentView()
    End Sub

    Protected Sub ddaccyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddaccyear.SelectedIndexChanged
        BindStudentView()
    End Sub
End Class
