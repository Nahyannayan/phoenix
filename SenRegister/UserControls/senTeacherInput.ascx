<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senTeacherInput.ascx.vb" Inherits="SenRegister_UserControls_senTeacherInput" %>
<asp:HiddenField ID="Hiddenstuid" runat="server" />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:Label ID="lblstatusmessage" runat="server" EnableTheming="False" CssClass="error"></asp:Label>
<br />
<br />
<div>
    <asp:LinkButton ID="T14lnknotes" runat="server" OnClientClick="javascript:return false;">Enter Notes</asp:LinkButton><br />
    <br />
    <ajaxToolkit:CollapsiblePanelExtender
        ID="T14CollapsiblePanelExtender1" runat="server" AutoCollapse="False" AutoExpand="False"
        CollapseControlID="T14lnknotes" Collapsed="true" CollapsedSize="0" CollapsedText="Enter Notes"
        ExpandControlID="T14lnknotes" ExpandedText="Hide" ScrollContents="false"
        TargetControlID="T14Panel1" TextLabelID="T14lnknotes">
    </ajaxToolkit:CollapsiblePanelExtender>
    <asp:Panel ID="T14Panel1" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4">Enter Notes</td>
            </tr>
            <tr>
                <td width="20%">
                    <asp:DropDownList ID="T14ddStaffNotes" runat="server" ValidationGroup="T14Notes">
                    </asp:DropDownList></td>
                <td width="30%">
                    <asp:TextBox ID="T14Notes" runat="server" TextMode="MultiLine"
                        ValidationGroup="T14Notes"></asp:TextBox></td>
                <td colspan="2"></td>
            </tr>

            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="T14NotesSave" CssClass="button" runat="server" Text="Save" ValidationGroup="T14Notes" /></td>
            </tr>
        </table>
    </asp:Panel>
    <table width="100%">
        <tr>
            <td class="title-bg">Notes</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T14GrdNotes" runat="server" AutoGenerateColumns="false" EmptyDataText="No Notes added yet" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderTemplate>
                                Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                            </ItemTemplate>
                            <ItemStyle Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Notes">
                            <HeaderTemplate>
                                Notes
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T14lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T14Panel1" runat="server" CssClass="panel-cover">
                                    <%#Eval("NOTES_TEXT")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T14CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T14lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T14lblview"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T14Panel1"
                                    TextLabelID="T14lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Author">
                            <HeaderTemplate>
                                Author
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center> <%#Eval("empname")%></center>
                            </ItemTemplate>
                            <ItemStyle />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:RequiredFieldValidator ID="T14RequiredFieldValidator6" runat="server" ControlToValidate="T14ddStaffNotes"
        Display="None" ErrorMessage="Please Select a Inclusion Member (Entered By)" InitialValue="-1"
        SetFocusOnError="True" ValidationGroup="T14Notes"></asp:RequiredFieldValidator><asp:RequiredFieldValidator
            ID="T14RequiredFieldValidator7" runat="server" ControlToValidate="T14Notes" Display="None"
            ErrorMessage="Please Enter Notes" SetFocusOnError="True" ValidationGroup="T14Notes"></asp:RequiredFieldValidator><asp:ValidationSummary
                ID="ValidationSummary2" runat="server" ShowMessageBox="True" ShowSummary="False"
                ValidationGroup="T14Notes" />
</div>
