<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senAccess.ascx.vb" Inherits="SenRegister_UserControls_senAccess" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Label ID="lblstatusmessage" runat="server" EnableTheming="False" CssClass="error"></asp:Label><br />
<asp:LinkButton ID="T3lnkreasons" OnClientClick="javascript:return false;" runat="server">Enter Access Requests and Results</asp:LinkButton>

<asp:Panel ID="T3Panel1" runat="server" CssClass="panel-cover">
    <table width="100%">
        <tr>
            <td class="title-bg">
                Enter Reasons</td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <%--<td>
                            <table>
                                <tr>--%>
                                    <td width="20%">
                                       <span class="field-label">Access Requested by </span> </td>                                   
                                    <td width="30%">
                                        <asp:TextBox ID="T3txtAccessRequestedBy" runat="server" ValidationGroup="T3Reasons"></asp:TextBox></td>
                                    <td width="20%">
                                       <span class="field-label"> Date </span></td>                                  
                                    <td width="30%">
                                        <asp:TextBox ID="T3txtRequestedDate" onfocus="javascript:this.blur();return false;" runat="server" ValidationGroup="T3Reasons">
                                        </asp:TextBox><asp:Image ID="Image1" ImageUrl="~/Images/calendar.gif" runat="server" /></td>
                                   
                              <%--  </tr>
                            </table>
                        </td>--%>
                    </tr>
                    <tr>
                        <td width="20%">
                          <span class="field-label">  Reason </span></td>
                        <td width="30%">
                               <asp:TextBox ID="T3txtReason" runat="server"  TextMode="MultiLine"
                                ValidationGroup="T3Reasons"></asp:TextBox>
                        </td>
                         <td width="20%">
                           <span class="field-label"> Author </span>
                           </td>
                        <td width="30%">
                             <asp:DropDownList ID="T3ddstaflist" runat="server" ValidationGroup="T3Reasons">
                            </asp:DropDownList>
                        </td>
                   </tr>                                
                    <tr>
                        <td width="20%">
                          <span class="field-label">  Access Arrangements Information (File Location / Directory) </span> </td>
                        <td width="30%">
                            <asp:TextBox ID="txtAccessArrangementinfo"  runat="server"></asp:TextBox></td>
                    </tr>
                  
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="T3btnResultSave" runat="server" CssClass="button" Text="Save" ValidationGroup="T3Reasons" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
<table  width="100%">
    <tr>
        <td class="title-bg">
            Access Requests and Results</td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="T3GrdReasons" AutoGenerateColumns="false" EmptyDataText="No Reasons added yet" CssClass="table table-bordered table-row"
                runat="server" AllowPaging="True" PageSize="5">
                <Columns>
                    <asp:TemplateField HeaderText="Requested">
                        <HeaderTemplate>
                            Requested
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="HiddenRequestid" Value=' <%#Eval("ACCESS_REASON_ID")%>' runat="server" />
                            <%#Eval("REQUESTED_BY")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date">
                        <HeaderTemplate>
                            Date
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("REQUESTED_DATE")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reason">
                        <HeaderTemplate>
                            Reason
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="T3lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                            <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                            <asp:Panel ID="T3Panel1" runat="server" CssClass="panel-cover">
                                <%#Eval("REASON_TEXT")%>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="T3CollapsiblePanelExtender1" runat="server"
                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T3lblview" Collapsed="true"
                                CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T3lblview"
                                ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T3Panel1"
                                TextLabelID="T3lblview">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Author">
                        <HeaderTemplate>
                            Author
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("authempname")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Path">
                        <HeaderTemplate>
                            Path
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%--<asp:LinkButton ID="lnkPath" CommandName="Path" CausesValidation="false" Visible='<%#Eval("showpath")%>'  CommandArgument='<%#Eval("LINK_INFO")%>' runat="server">Path</asp:LinkButton>--%>
                            <%--<center>--%>
                            <asp:LinkButton ID="LinkView" Text="Path"  OnClientClick="javascript:return false;" runat="server"></asp:LinkButton>
                            <asp:Panel id="Show"  runat="server" cssclass="panel-cover" >
                            <%#Eval("LINK_INFO")%>
                             </asp:Panel>
                            <ajaxToolkit:HoverMenuExtender ID="hm2" runat="server" TargetControlID="LinkView" PopupControlID="Show" PopupPosition="Top"  />

                            <%--</center>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Entry Date">
                        <HeaderTemplate>
                            Entry Date
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Results">
                        <HeaderTemplate>
                           Results
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="T3lblviewR" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                            <asp:Panel ID="T3Panel1R" runat="server" cssclass="panel-cover">
                                <%#Eval("RESULT_TEXT")%>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="T3CollapsiblePanelExtender1R" runat="server"
                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T3lblviewR" Collapsed="true"
                                CollapsedSize="0" CollapsedText='<%#Eval("tempview2")%>' ExpandControlID="T3lblviewR"
                                ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T3Panel1R"
                                TextLabelID="T3lblviewR">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Author">
                        <HeaderTemplate>
                          Author
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("resultentryauthor")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date">
                        <HeaderTemplate>
                            Date
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("resulteDate", "{0:dd/MMM/yyyy}")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Results">
                        <HeaderTemplate>
                           Results
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkResults" CommandName="Results" CausesValidation="false" Visible='<%#Eval("show")%>' CommandArgument='<%#Eval("ACCESS_REASON_ID")%>' runat="server">Results</asp:LinkButton>

                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem"  Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                <EditRowStyle Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
</table>


<asp:Panel ID="T3Panel2"  Visible="false" runat="server" CssClass="panel-cover" >
    <table  width="100%">
        <tr>
            <td class="title-bg">
                Enter Results</td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td width="20%"> 
                           <span class="field-label"> Result</span></td>
                        <td width="30%">
                            <asp:TextBox ID="T3txtResults" runat="server" Height="200px" SkinID="MultiText" TextMode="MultiLine"
                                ValidationGroup="T3Results" Width="500px"></asp:TextBox></td>
                        <td width="20%">
                           <span class="field-label">  Entry By </span></td>
                        <td width="30%">
                            <asp:DropDownList ID="T3ddresultStaff" runat="server" ValidationGroup="T3Results">
                            </asp:DropDownList></td>
                    </tr>
                   
                    <tr>
                        <td align="center">
                            <asp:Button ID="T3btnResults" runat="server" CssClass="button" Text="Save" ValidationGroup="T3Results" />
                            <asp:Button ID="T3btnCancel" runat="server" CausesValidation="false"  CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table><asp:HiddenField ID="HiddenRequestid" runat="server" />
<asp:RequiredFieldValidator ID="T3RequiredFieldValidator5" runat="server" ControlToValidate="T3txtResults"
    Display="None" ErrorMessage="Please Enter Comments (Results)" SetFocusOnError="True"
    ValidationGroup="T3Results"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="T3RequiredFieldValidator6" runat="server" ControlToValidate="T3ddresultStaff"
    Display="None" ErrorMessage="Please Select Author" InitialValue="-1" ValidationGroup="T3Results"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="T3ValidationSummary2" runat="server" ShowMessageBox="True"
    ShowSummary="False" ValidationGroup="T3Results" />
            </td>
        </tr>
    </table>
</asp:Panel>
<br />

<asp:RequiredFieldValidator ID="T3RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Name (Access Requested by)"
    ControlToValidate="T3txtAccessRequestedBy" Display="None" SetFocusOnError="True"
    ValidationGroup="T3Reasons"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="T3RequiredFieldValidator2" runat="server" ErrorMessage="Please Enter Requested Date"
    ControlToValidate="T3txtRequestedDate" Display="None" SetFocusOnError="True"
    ValidationGroup="T3Reasons"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="T3RequiredFieldValidator3" runat="server" ErrorMessage="Please Enter Reason"
    Display="None" ControlToValidate="T3txtReason" SetFocusOnError="True" ValidationGroup="T3Reasons"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="T3RequiredFieldValidator4" runat="server" ErrorMessage="Please Select Author"
    ControlToValidate="T3ddstaflist" Display="None" InitialValue="-1" SetFocusOnError="True"
    ValidationGroup="T3Reasons"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="T3ValidationSummary1" runat="server" ShowMessageBox="True"
    ShowSummary="False" ValidationGroup="T3Reasons" />
&nbsp; &nbsp;
<asp:HiddenField ID="Hiddenstuid" runat="server" />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<ajaxToolkit:CalendarExtender ID="T3CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
    TargetControlID="T3txtRequestedDate">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CollapsiblePanelExtender ID="T3CollapsiblePanelExtender1" runat="server"
    AutoCollapse="False" AutoExpand="False" CollapseControlID="T3lnkreasons" Collapsed="true"
    CollapsedSize="0" CollapsedText="Enter Access Requests and Results" ExpandControlID="T3lnkreasons"
    ExpandedText="Hide-Access Requests and Results" ScrollContents="false" TargetControlID="T3Panel1"
    TextLabelID="T3lnkreasons">
</ajaxToolkit:CollapsiblePanelExtender>




