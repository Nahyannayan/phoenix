<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senWellbeing.ascx.vb"
    Inherits="SenRegister_UserControls_senWellbeing" %>
<asp:Label ID="lblstatusmessage" runat="server" EnableTheming="False" CssClass="error"></asp:Label>

<div >
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="4">Wellbeing Details</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="T6lbldateenteredlabel" runat="server"></asp:Label></td>

            <td>
                <asp:Label ID="T6lbldateentered" runat="server"></asp:Label></td>
            <td><span class="field-label">Date Entered Wellbeing</span></td>
          
            <td>
                <asp:TextBox ID="T6lblDateenteredwellbeing" runat="server" onfocus="javascript:this.blur();return false;"></asp:TextBox>
                <asp:Image ID="Image11" runat="server" ImageUrl="~/Images/calendar.gif" />
                <asp:LinkButton ID="lnkentrysave" runat="server" CausesValidation="False">Save</asp:LinkButton>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="Image11" TargetControlID="T6lblDateenteredwellbeing">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td><span class="field-label">Time Lag</span></td>

            <td>
                <asp:Label ID="T6lblTimeLag" runat="server"></asp:Label></td>

        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="T6CheckBoxWELL1" runat="server" Text="Wellbeing 1:1" AutoPostBack="True" CssClass="field-label" /></td>
            
            <td >
                <asp:CheckBox ID="T6CheckRelLerhistory" runat="server" Text="Relevant Medical/Learning History" CssClass="field-label"
                    AutoPostBack="True" /></td>
        </tr>
    </table>
    <br />
    <asp:LinkButton ID="T6LnkWfC" runat="server" OnClientClick="javascript:return false;">Enter Wellbeing Focus Groups </asp:LinkButton>
    <br />
    <br />
    <asp:Panel ID="T6Panel2" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4">Wellbeing Focus Groups</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="T6CheckBoxWellFocusGroup" runat="server" Text="Wellbeing Focus Group" /></td>                              
                <td width="20%"><span class="field-label">Group</span></td>              
                <td width="30%">
                    <asp:TextBox ID="T6txtGroups" runat="server"></asp:TextBox></td>               
            </tr>
            <tr>
                <td width="20%"><span class="field-label">Date Commenced </span>
                </td>              
                <td width="30%">
                    <asp:TextBox ID="T6txtdatecommenced" onfocus="javascript:this.blur();return false;"
                        runat="server"></asp:TextBox>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                <td width="20%"><span class="field-label">Date Discontinued</span></td>              
                <td width="30%"> 
                    <asp:TextBox ID="T6txtdatediscontinued" onfocus="javascript:this.blur();return false;"
                        runat="server"></asp:TextBox></td>
                <td>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
            </tr>
            <tr>
                <td align="left" ><span class="field-label">Issue</span></td>
                <td align="left" >
                    <asp:TextBox ID="T6txtissues" runat="server"  TextMode="MultiLine"
                        ValidationGroup="T6save" ></asp:TextBox></td>
                <td colspan="2"></td>
            </tr>           
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="T6btnsave" runat="server" CssClass="button" Text="Save" CausesValidation="False" /></td>               
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="T6CollapsiblePanelExtender2" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T6LnkWfC" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter-Wellbeing Focus Groups " ExpandControlID="T6LnkWfC"
         ExpandedText="Hide-Wellbeing Focus Groups" ScrollContents="false"
        TargetControlID="T6Panel2" TextLabelID="T6LnkWfC">
    </ajaxToolkit:CollapsiblePanelExtender>
    <br />
    <table  width="100%">
        <tr>
            <td class="title-bg">
                Wellbeing Focus Groups</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GrdWellFGroup" AutoGenerateColumns="false" EmptyDataText="No history available" CssClass="table table-bordered table-row"
                    runat="server" AllowPaging="True" Width="100%" PageSize="5">
                  
                    <Columns>
                        <asp:TemplateField HeaderText="Group">
                            <HeaderTemplate>
                                Group
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("GROUPS")%>
                            </ItemTemplate>
                            <ItemStyle  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Commenced">
                            <HeaderTemplate>
                               Commenced
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("DATE_COMMENCED")%>
                            </ItemTemplate>
                            <ItemStyle />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Discontinued">
                            <HeaderTemplate>
                              Discontinued
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("DATE_DISCONTINUED")%>
                            </ItemTemplate>
                            <ItemStyle/>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Issue">
                            <HeaderTemplate>
                                 Issue
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T6lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T6Panel1" runat="server">
                                    <%#Eval("ISSUE")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T6CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T6lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T6lblview"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T6Panel1"
                                    TextLabelID="T6lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="4">Other Details</td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="T6CheckPs" Text="Parenting Skills" runat="server" /></td>
            <td><span class="field-label">From Date</span></td>
            <td>
                <asp:TextBox ID="T6F1" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
            <td><span class="field-label">To Date</span></td>
            <td>
                <asp:TextBox ID="T6T1" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="T6PaSGroup" Text="Parent/Child Support Group" runat="server" /></td>
            <td><span class="field-label">From Date</span></td>
            <td>
                <asp:TextBox ID="T6F2" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
            <td><span class="field-label">To Date</span></td>
            <td>
                <asp:TextBox ID="T6T2" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="T6TPPa" Text="Triple P Parenting" runat="server" /></td>
            <td><span class="field-label">From Date</span></td>
            <td>
                <asp:TextBox ID="T6F3" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
            <td><span class="field-label">To Date</span></td>
            <td>
                <asp:TextBox ID="T6T3" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                <asp:Image ID="Image8" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnodsave" runat="server" CausesValidation="false" CssClass="button"
                    Text="Save" /></td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="T6CE3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image3"
        TargetControlID="T6F1">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T6CE4" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image4"
        TargetControlID="T6T1">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T6CE5" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image5"
        TargetControlID="T6F2">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T6CE6" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image6"
        TargetControlID="T6T2">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T6CE7" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image7"
        TargetControlID="T6F3">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T6CE8" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image8"
        TargetControlID="T6T3">
    </ajaxToolkit:CalendarExtender>
    <br />
    <br />
    <table  width="100%">
        <tr>
            <td class="title-bg">
                Wellbeing IEP
                <asp:CheckBox ID="T6checkIEP" runat="server" AutoPostBack="True" CssClass="field=label" />
                (File Location / Directory)</td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="T6txtwellinfo" runat="server" ></asp:TextBox>
                <asp:Button ID="T6btIEPinfoSave" runat="server" Text="Save" CssClass="button" CausesValidation="False" />
                <asp:LinkButton ID="T6lnkIEPinfoPath" runat="server" Visible="False">Path</asp:LinkButton></td>
        </tr>
    </table>
    <br />
    <asp:LinkButton ID="T6lnknotes" runat="server" OnClientClick="javascript:return false;">Enter Notes</asp:LinkButton>
    <br />
    <br />
    <asp:Panel ID="T6Panel1" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4">Enter Notes</td>
            </tr>
            <tr>
                <td width="20%"> <span class="field-label">Author</span>
                </td>
                <td width="30%">
                     <asp:DropDownList ID="T6ddstaffs" runat="server" ValidationGroup="T6Comments">
                    </asp:DropDownList></td>
               <td width="20%"><span class="field-label">Comments</span></td>
                <td >
                    <asp:TextBox ID="T6txtcomments" runat="server" 
                        TextMode="MultiLine" ValidationGroup="T6save" ></asp:TextBox></td>
            </tr>       
            <tr>
                <td colspan="4" align="center">
                    <asp:Button ID="T6brnsave" runat="server" CssClass="button" Text="Save" ValidationGroup="T6Comments" /></td>
            </tr>
        </table>
    </asp:Panel>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="T6ddstaffs"
        Display="None" ErrorMessage="Please Select a Author" InitialValue="-1" SetFocusOnError="True"
        ValidationGroup="T6Comments"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="T6txtcomments"
        Display="None" ErrorMessage="Please Enter Comments" SetFocusOnError="True" ValidationGroup="T6Comments"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="T6Comments"
        ShowMessageBox="True" ShowSummary="False" />
    <ajaxToolkit:CollapsiblePanelExtender ID="T6CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T6lnknotes" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter Notes" ExpandControlID="T6lnknotes" 
        ExpandedText="Hide-Notes" ScrollContents="false" TargetControlID="T6Panel1" TextLabelID="T6lnknotes">
    </ajaxToolkit:CollapsiblePanelExtender>
    <table  width="100%">
        <tr>
            <td class="title-bg">
                Notes</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T6GrdComments" AutoGenerateColumns="false" EmptyDataText="No Comments added yet" CssClass="table table-bordered table-row"
                    runat="server" AllowPaging="True" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderTemplate>
                                Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                            </ItemTemplate>
                            <ItemStyle  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comments">
                            <HeaderTemplate>
                                                                            Comments
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T6lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T6Panel1" runat="server" >
                                    <%#Eval("COMMENTS_TEXT")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T6CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T6lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T6lblview"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T6Panel1"
                                    TextLabelID="T6lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Author">
                            <HeaderTemplate>
                                 Author
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("empname")%>
                                </center>
                            </ItemTemplate>
                            <ItemStyle  />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <asp:HiddenField ID="Hiddenstuid" runat="server" />
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:HiddenField ID="HiddenWellIEPInfo" runat="server" />
    <ajaxToolkit:CalendarExtender ID="T6CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
        TargetControlID="T6txtdatecommenced">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T6CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image2"
        TargetControlID="T6txtdatediscontinued">
    </ajaxToolkit:CalendarExtender>
</div>
