Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_semInclusionMembersSearch
    Inherits System.Web.UI.UserControl
    ' Delegate declaration 
    Public Delegate Sub OnButtonClick(ByVal ds As DataSet)


    ' Event declaration 
    Public Event btnHandler As OnButtonClick
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindDepartments()
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub BindDepartments()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim str_query = "select * from SEN_DEPARTMENTS"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddDepartments.DataSource = ds
        ddDepartments.DataValueField = "DEPT_ID"
        ddDepartments.DataTextField = "DEPT_DESC"
        ddDepartments.DataBind()
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Select a Section"
        ddDepartments.Items.Insert(0, list)

    End Sub
    Protected Sub btnremovesearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnremovesearch.Click
        If ddDepartments.SelectedValue > 0 Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            If txtempidsearch.Text.Trim() <> "" Then
                pParms(0) = New SqlClient.SqlParameter("@EMP_ID", txtempidsearch.Text.Trim())
            End If
            If txtempnosearch.Text.Trim() <> "" Then
                pParms(1) = New SqlClient.SqlParameter("@EMPNO", txtempnosearch.Text.Trim())
            End If
            If txtempnamesearch.Text.Trim() <> "" Then
                pParms(2) = New SqlClient.SqlParameter("@EMP_NAME", txtempnamesearch.Text.Trim().Replace(" ", ""))
            End If
            If dddesignationsearch.SelectedIndex > 0 Then
                pParms(3) = New SqlClient.SqlParameter("@EMP_DES_ID", dddesignationsearch.SelectedValue)
            End If
            If ddDepartments.SelectedIndex > 0 Then
                pParms(4) = New SqlClient.SqlParameter("@EMP_DEPT_ID", ddDepartments.SelectedValue)
            End If

            pParms(5) = New SqlClient.SqlParameter("@EMP_BSU_ID", Hiddenbsuid.Value)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SEARCH_SEN_DEPARTMENT_MEMBERS", pParms)
            RaiseEvent btnHandler(ds)
        End If
    End Sub
End Class
