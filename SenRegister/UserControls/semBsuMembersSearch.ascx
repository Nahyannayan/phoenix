<%@ Control Language="VB" AutoEventWireup="false" CodeFile="semBsuMembersSearch.ascx.vb" Inherits="SenRegister_UserControls_semBsuMembersSearch" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<div class="matters"> 

<asp:UpdatePanel id="UpdatePanel1" runat="server">
    <contenttemplate>
<table>
    <tr>
        <td >
            <asp:TextBox ID="txtempid" runat="server"></asp:TextBox></td>
        <td >
            <asp:TextBox ID="txtempno" runat="server"></asp:TextBox></td>
        <td >
            <asp:TextBox ID="txtempname" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td colspan="3" >
            <asp:DropDownList ID="dddesignation" runat="server">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td >
        </td>
        <td align="center" >
            <asp:Button ID="btnsearch" runat="server" Text="Search" CausesValidation="False" CssClass="button" /></td>
        <td >
        </td>
    </tr>
</table>
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
            TargetControlID="txtempid" WatermarkCssClass="watermarked" WatermarkText="Employee ID">
        </ajaxToolkit:TextBoxWatermarkExtender>
        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
            TargetControlID="txtempno" WatermarkCssClass="watermarked" WatermarkText="Employee No">
        </ajaxToolkit:TextBoxWatermarkExtender>
        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server"
            TargetControlID="txtempname" WatermarkCssClass="watermarked" WatermarkText="Employee Name">
        </ajaxToolkit:TextBoxWatermarkExtender>
    </contenttemplate>
</asp:UpdatePanel></div>
