<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senELL.ascx.vb" Inherits="SenRegister_UserControls_senELL" %>
<asp:Label ID="lblstatusmessage" runat="server" EnableTheming="False" CssClass="error"></asp:Label>

<div >
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="4">ELL Details</td>
        </tr>
        <tr>
            <td width="20%">
                <asp:Label ID="T5lbldateenteredlabel" runat="server"></asp:Label></td>
            <td width="30%">
                <asp:Label ID="T5lbldateentered" runat="server"></asp:Label></td>
            <td width="20%"><span class="field-label">Date Entered ELL</span></td>

            <td width="30%">
                <asp:TextBox ID="T5lblDateenteredELL" runat="server" onfocus="javascript:this.blur();return false;"></asp:TextBox>
                <asp:Image
                    ID="Image11" runat="server" ImageUrl="~/Images/calendar.gif" />
                <asp:LinkButton ID="lnkentrysave"
                    runat="server" CausesValidation="False">Save</asp:LinkButton>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="Image11" TargetControlID="T5lblDateenteredELL">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td width="20%"><span class="field-label">Time Lag</span></td>
            <td width="30%">
                <asp:Label ID="T5lblTimeLag" runat="server"></asp:Label></td>
            <td colspan="2"></td>

        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="4">Languages</td>
        </tr>
        <tr>
            <td width="20%"><span class="field-label">Language 1</span></td>

            <td width="30%">
                <asp:DropDownList ID="T5ddLang1" runat="server">
                </asp:DropDownList></td>
            <td id="D1" runat="server" colspan="2"><span class="field-label">OR </span>
                       <asp:TextBox ID="T5txtDialect1" runat="server" ValidationGroup="T5lang"></asp:TextBox></td>
        </tr>
        <tr>
            <td><span class="field-label">Language 2</span></td>

            <td>
                <asp:DropDownList ID="T5ddLang2" runat="server">
                </asp:DropDownList></td>
            <td id="D2" runat="server" colspan="2"><span class="field-label">OR </span>
            <asp:TextBox ID="T5txtDialect2" runat="server" ValidationGroup="T5lang"></asp:TextBox></td>
        </tr>
        <tr>
            <td width=20%><span class="field-label">Language 3</span></td>

            <td width="30%">
                <asp:DropDownList ID="T5ddLang3" runat="server">
                </asp:DropDownList></td>
            <td id="D3" runat="server" colspan="2"><span class="field-label">OR </span>
            <asp:TextBox ID="T5txtDialect3" runat="server" ValidationGroup="T5lang"></asp:TextBox></td>
        </tr>
        <tr>
            <td runat="server" width="20%"><span class="field-label">Date Pupil Started Using/Learning English </span></td>
            <td width="30%">
                <asp:TextBox ID="T5txtlearningenglish" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox><asp:Image
                    ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
        </tr>
        <tr>
            <td runat="server" align="center" colspan="4">
                <asp:Button ID="btnlanguagesave" runat="server" CausesValidation="false" CssClass="button" Text="Save" /></td>
        </tr>
    </table>
    <br />
    <asp:LinkButton ID="T5lnkcontrack" runat="server" OnClientClick="javascript:return false;">Enter Continuum Tracker</asp:LinkButton><br />
    <br />
    <asp:Panel ID="T5Panel2" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4">Continuum Tracker</td>
            </tr>
            <tr>
                <td width="20%"><span class="field-label">Speaking</span></td>                 
                <td width="30%"> 
                    <asp:DropDownList ID="T5ddspeking" runat="server" ValidationGroup="T5lang">
                        <asp:ListItem Value="-1">Select Level</asp:ListItem>
                        <asp:ListItem Value="A">A</asp:ListItem>
                        <asp:ListItem Value="B">B</asp:ListItem>
                        <asp:ListItem Value="C">C</asp:ListItem>
                        <asp:ListItem Value="D">D</asp:ListItem>
                        <asp:ListItem Value="E">E</asp:ListItem>
                        <asp:ListItem Value="F">F</asp:ListItem>
                        <asp:ListItem Value="G">G</asp:ListItem>
                    </asp:DropDownList></td>
                <td width="20%"><span class="field-label">Listening</span></td>                
                <td width="30%"> 
                    <asp:DropDownList ID="T5ddlistening" runat="server" ValidationGroup="T5lang">
                        <asp:ListItem Value="-1">Select Level</asp:ListItem>
                        <asp:ListItem Value="A">A</asp:ListItem>
                        <asp:ListItem Value="B">B</asp:ListItem>
                        <asp:ListItem Value="C">C</asp:ListItem>
                        <asp:ListItem Value="D">D</asp:ListItem>
                        <asp:ListItem Value="E">E</asp:ListItem>
                        <asp:ListItem Value="F">F</asp:ListItem>
                        <asp:ListItem Value="G">G</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td><span class="field-label">Reading</span></td>
                <td>
                    <asp:DropDownList ID="T5ddreading" runat="server" ValidationGroup="T5lang">
                        <asp:ListItem Value="-1">Select Level</asp:ListItem>
                        <asp:ListItem Value="A">A</asp:ListItem>
                        <asp:ListItem Value="B">B</asp:ListItem>
                        <asp:ListItem Value="C">C</asp:ListItem>
                        <asp:ListItem Value="D">D</asp:ListItem>
                        <asp:ListItem Value="E">E</asp:ListItem>
                        <asp:ListItem Value="F">F</asp:ListItem>
                        <asp:ListItem Value="G">G</asp:ListItem>
                    </asp:DropDownList></td>
                <td><span class="field-label">Writing</span></td>               
                <td>
                    <asp:DropDownList ID="T5ddWriting" runat="server" ValidationGroup="T5lang">
                        <asp:ListItem Value="-1">Select Level</asp:ListItem>
                        <asp:ListItem Value="A">A</asp:ListItem>
                        <asp:ListItem Value="B">B</asp:ListItem>
                        <asp:ListItem Value="C">C</asp:ListItem>
                        <asp:ListItem Value="D">D</asp:ListItem>
                        <asp:ListItem Value="E">E</asp:ListItem>
                        <asp:ListItem Value="F">F</asp:ListItem>
                        <asp:ListItem Value="G">G</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td width="20%"><span class="field-label">Enter Continuum  Date</span></td>
                <td width="30%">
                    <asp:TextBox ID="T5txtContTrak" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                    <asp:Image
                        ID="Image2" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                 <td width="20%"><span class="field-label">Exit Continuum Date</span></td>                 
                 <td width="30%">
                    <asp:TextBox ID="T5txtContTrakExit" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                    <asp:Image
                        ID="Image3" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
            </tr>
           
            <tr>
                <td colspan="4">
                    <asp:Button ID="T5btnsave" runat="server" CausesValidation="false" CssClass="button" Text="Add" /></td>

            </tr>

        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="T5CollapsiblePanelExtender2" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T5lnkcontrack" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter Continuum Tracker" ExpandControlID="T5lnkcontrack" 
        ExpandedText="Hide-Continuum Tracker" ScrollContents="false" TargetControlID="T5Panel2" TextLabelID="T5lnknotes">
    </ajaxToolkit:CollapsiblePanelExtender>
    <br />
    <table  width="100%">
        <tr>
            <td class="title-bg">Continuum Tracker</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T5GridContTracker" runat="server" AutoGenerateColumns="false" EmptyDataText="No data" Width="100%" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">

                    <Columns>
                        <asp:TemplateField HeaderText="Enter Continuum Date">
                            <HeaderTemplate>
                               Enter Continuum Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center><%#Eval("ENTRY_DATE")%></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Exit Continuum Date">
                            <HeaderTemplate>
                               Exit Continuum Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>  <%#Eval("EXIT_DATE")%></center>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Speaking">
                            <HeaderTemplate>
                                Speaking</HeaderTemplate>
                            <ItemTemplate>
                                <center>  <%#Eval("SPEAKING_LEVEL")%></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Listening">
                            <HeaderTemplate>
                                Listening
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center> <%#Eval("LISTENING_LEVEL")%></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reading">
                            <HeaderTemplate>
                               Reading
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center> <%#Eval("READING_LEVEL")%></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Writing">
                            <HeaderTemplate>
                              Writing
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center> <%#Eval("WRITING_LEVEL")%></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem"  Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <asp:LinkButton ID="T5lnknotes" runat="server" OnClientClick="javascript:return false;">Enter Notes</asp:LinkButton>
    <br />
    <br />
    <asp:Panel ID="T5Panel1" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4">Enter Progress/Comments</td>
            </tr>
            <tr>
                <td><span class="field-label">Author </span></td>
                <td>
                    <asp:DropDownList ID="T5ddStaffNotes" runat="server" ValidationGroup="T5notes">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="T5RequiredFieldValidator8" runat="server" ErrorMessage="Please Select Author" ControlToValidate="T5ddStaffNotes" Display="None" InitialValue="-1" SetFocusOnError="True" ValidationGroup="T5notes"></asp:RequiredFieldValidator><asp:RequiredFieldValidator ID="T5RequiredFieldValidator9" runat="server" ErrorMessage="Please Enter Comment" ControlToValidate="T5Notes" Display="None" SetFocusOnError="True" ValidationGroup="T5notes"></asp:RequiredFieldValidator><asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="T5notes" />
                </td>
                <td><span class="field-label">Progress/Comments </span></td>
                <td>
                    <asp:TextBox ID="T5Notes" runat="server" TextMode="MultiLine"
                        ValidationGroup="T5notes"></asp:TextBox></td>
            </tr>           
            <tr>
                <td colspan="4" align="center">
                    <asp:Button ID="T5btnnotessave" runat="server" CssClass="button" Text="Save" ValidationGroup="T5notes" /></td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="T5CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T5lnknotes" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter Notes" ExpandControlID="T5lnknotes" 
        ExpandedText="Hide-Notes" ScrollContents="false" TargetControlID="T5Panel1" TextLabelID="T5lnknotes">
    </ajaxToolkit:CollapsiblePanelExtender>
    <table  width="100%">
        <tr>
            <td class="title-bg">Progress/Notes</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T5GrdComments" runat="server" AutoGenerateColumns="false" EmptyDataText="No Comments added yet" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderTemplate>
                              Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                            </ItemTemplate>
                            <ItemStyle  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comments">
                            <HeaderTemplate>
                              Comments
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T5lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T5Panel1" runat="server" >
                                    <%#Eval("COMMENT_TEXT")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T5CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T5lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T5lblview"
                                     ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T5Panel1"
                                    TextLabelID="T5lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Author">
                            <HeaderTemplate>
                                Author
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center><%#Eval("empname")%></center>
                            </ItemTemplate>
                            <ItemStyle />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem"  Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>  
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:HiddenField ID="Hiddenstuid" runat="server" />
    <ajaxToolkit:CalendarExtender ID="T5CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
        TargetControlID="T5txtlearningenglish">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T5CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image2"
        TargetControlID="T5txtContTrak">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T5CE3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image3"
        TargetControlID="T5txtContTrakExit">
    </ajaxToolkit:CalendarExtender>
</div>
