<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senMAGT.ascx.vb" Inherits="SenRegister_UserControls_senMAGT" %>

<script type="text/javascript" >

var flag = 0

window.setTimeout('hidepanel();', 1000);

function hidepanel()
{

    if (flag == 0)
    {

    document.getElementById('<%=Panel1.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel2.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel3.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel4.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel5.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel6.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel7.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel8.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel9.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel10.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel11.ClientID %>').style.visibility = 'hidden'; 
    flag=1

    }
}




function showhide(checkbox,panel)
{

if (checkbox.checked)
{
        if (panel=='Panel1')
        {
        document.getElementById('<%=Panel1.ClientID %>').style.visibility = 'visible'; 
        }
        if (panel=='Panel2')
        {
        document.getElementById('<%=Panel2.ClientID %>').style.visibility = 'visible'; 
        }
        if (panel=='Panel3')
        {
        document.getElementById('<%=Panel3.ClientID %>').style.visibility = 'visible'; 
        }
        if (panel=='Panel4')
        {
        document.getElementById('<%=Panel4.ClientID %>').style.visibility = 'visible'; 
        }
        if (panel=='Panel5')
        {
        document.getElementById('<%=Panel5.ClientID %>').style.visibility = 'visible'; 
        }
        if (panel=='Panel6')
        {
        document.getElementById('<%=Panel6.ClientID %>').style.visibility = 'visible'; 
        }
        if (panel=='Panel7')
        {
        document.getElementById('<%=Panel7.ClientID %>').style.visibility = 'visible'; 
        }
        if (panel=='Panel8')
        {
        document.getElementById('<%=Panel8.ClientID %>').style.visibility = 'visible'; 
        }
        if (panel=='Panel9')
        {
        document.getElementById('<%=Panel9.ClientID %>').style.visibility = 'visible'; 
        }
        if (panel=='Panel10')
        {
        document.getElementById('<%=Panel10.ClientID %>').style.visibility = 'visible'; 
        }
        if (panel=='Panel11')
        {
        document.getElementById('<%=Panel11.ClientID %>').style.visibility = 'visible'; 
        }
}
else
{

        if (panel=='Panel1')
        {
        document.getElementById('<%=Panel1.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel2')
        {
        document.getElementById('<%=Panel2.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel3')
        {
        document.getElementById('<%=Panel3.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel4')
        {
        document.getElementById('<%=Panel4.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel5')
        {
        document.getElementById('<%=Panel5.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel6')
        {
        document.getElementById('<%=Panel6.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel7')
        {
        document.getElementById('<%=Panel7.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel8')
        {
        document.getElementById('<%=Panel8.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel9')
        {
        document.getElementById('<%=Panel9.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel10')
        {
        document.getElementById('<%=Panel10.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel11')
        {
        document.getElementById('<%=Panel11.ClientID %>').style.visibility = 'hidden'; 
        }

}


}



</script>


<asp:HiddenField ID="Hiddenstuid" runat="server" />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:Label ID="lblstatusmessage" runat="server" EnableTheming="False" CssClass="error"></asp:Label>

<div >
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="4">MAGT Details</td>
        </tr>
        <tr>
            <td width="20%">
                <asp:Label ID="T10lbldateenteredlabel" runat="server"></asp:Label></td>
            <td width="30%">
                <asp:Label ID="T10lbldateentered" runat="server"></asp:Label></td>
            <td width="20%">
                <span class="field-label">Date Entered MAGT</span></td>
            <td width="30%">
                <asp:TextBox ID="T10lblDateenteredmagt" runat="server" onfocus="javascript:this.blur();return false;"></asp:TextBox>
                <asp:Image ID="Image11" runat="server" ImageUrl="~/Images/calendar.gif" />
                <asp:LinkButton ID="lnkentrysave" runat="server" CausesValidation="False">Save</asp:LinkButton>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="Image11" TargetControlID="T10lblDateenteredmagt">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td width="20%"><span class="field-label">Time Lag</span></td>
            <td width="30%">
                <asp:Label ID="T10lblTimeLag" runat="server"></asp:Label></td>
        </tr>
    </table>
    <br />
    <asp:LinkButton ID="T10lnkview1" OnClientClick="javascript:return false;" runat="server">Enter MAGT Details</asp:LinkButton>
    <br />
    <br />
    <asp:Panel ID="T10Panel1" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4">Enter MAGT Details</td>
            </tr>
            <tr>
                <td width="20%"> <span class="field-label">MAGT Category</span></td>
                <td width="30%">
                    <asp:DropDownList ID="T10ddMAGTCategory" runat="server" ValidationGroup="T10comments">
                    </asp:DropDownList></td>
                <td width="20%"><span class="field-label">Refer Date</span></td>
                <td width="30%">
                    <asp:TextBox ID="T10txtReferDate" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>&nbsp;<asp:Image ID="Image1"
                        runat="server" ImageUrl="~/Images/calendar.gif" /></td>
            </tr>
          
            <tr>
                <td width="20%"><span class="field-label">Refer By</span></t>
                <td width="30%">
                    <asp:TextBox ID="T10TxtReferBy" runat="server"></asp:TextBox></td>
                <td width="20%"><span class="field-label">Start Date</span></td>
                <td width="30%">
                    <asp:TextBox ID="T10txtstartdate" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>&nbsp;<asp:Image ID="Image2"
                        runat="server" ImageUrl="~/Images/calendar.gif" /></td>
            </tr>
           
            <tr>
                <td width="20%"><span class="field-label">End Date</span></td>
                <td width="30%">
                    <asp:TextBox ID="T10txtenddate" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>&nbsp;<asp:Image ID="Image3"
                        runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                <td width="20%"><span class="field-label">Author</span></td>
                <td width="30%">
                    <asp:DropDownList ID="T10ddStaffs" runat="server" ValidationGroup="T10comments">
                    </asp:DropDownList></td>
            </tr>
           
            <tr>
                <td width="20%"><span class="field-label">Materials Used/Comments</span></td>
                <td width="30%">
                    <asp:TextBox ID="T10txtmatcomments" runat="server"
                        TextMode="MultiLine" ValidationGroup="T10comments" ></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="T10btnsave" runat="server" CssClass="button" Text="Save" ValidationGroup="T10comments" /></td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="T10CollapsiblePanelExtender4" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T10lnkview1" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter MAGT Details" ExpandControlID="T10lnkview1"
        ExpandedText="Hide-MAGT Details" ScrollContents="false" TargetControlID="T10Panel1"
        TextLabelID="T10lnkview1">
    </ajaxToolkit:CollapsiblePanelExtender>
    <ajaxToolkit:CalendarExtender ID="T10CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
        TargetControlID="T10txtReferDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T10CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image2"
        TargetControlID="T10txtstartdate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T10CE3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image3"
        TargetControlID="T10txtenddate">
    </ajaxToolkit:CalendarExtender>
    <br />
    <asp:RequiredFieldValidator ID="T10RequiredFieldValidator1" runat="server" ControlToValidate="T10ddMAGTCategory"
        Display="None" ErrorMessage="Please Select MAGT Category" InitialValue="-1" SetFocusOnError="True"
        ValidationGroup="T10comments"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="T10RequiredFieldValidator2" runat="server" ControlToValidate="T10ddStaffs"
        Display="None" ErrorMessage="Please Select Author" SetFocusOnError="True" ValidationGroup="T10comments"
        InitialValue="-1"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="T10RequiredFieldValidator3" runat="server" ControlToValidate="T10txtmatcomments"
        Display="None" ErrorMessage="Please Enter Comments" SetFocusOnError="True" ValidationGroup="T10comments"></asp:RequiredFieldValidator>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="T10txtReferDate"
        Display="None" ErrorMessage="Please Enter Refer Date" SetFocusOnError="True"
        ValidationGroup="T10comments"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="T10TxtReferBy"
        Display="None" ErrorMessage="Please Enter Refer By" SetFocusOnError="True" ValidationGroup="T10comments"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="T10txtstartdate"
        Display="None" ErrorMessage="Please Enter Start Date" SetFocusOnError="True"
        ValidationGroup="T10comments"></asp:RequiredFieldValidator>
    <br />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="T10comments" />
    <br />
    <br />
    <asp:CheckBox ID="T10CheckBox1" runat="server" onclick="javascript:showhide(this,'Panel1');"  Text="MAGT Art"  CssClass="field-label"/>&nbsp;<br />
    <asp:Panel ID="Panel1" runat="server" CssClass="panel-cover" >
        <table  width="100%">
            <tr>
                <td class="title-bg">
                    MAGT Art</td>
            </tr>
            <tr>
                <td> 
    <asp:GridView ID="T10GridView1" AutoGenerateColumns="false" EmptyDataText="No Records added yet" CssClass="table table-bordered table-row"
        runat="server" AllowPaging="True" PageSize="5" Width="100%" >
        <Columns>
            <asp:TemplateField HeaderText="Refer Date">
                <HeaderTemplate>
                    Refer Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Refer By">
                <HeaderTemplate>
                    Refer By
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_BY")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start Date">
                <HeaderTemplate>
                    Start Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("START_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Date">
                <HeaderTemplate>
                     End Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("END_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Materials Used/Comments">
                <HeaderTemplate>
                  Materials Used/Comments
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="T10lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                    <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                    <asp:Panel ID="T10Panel1" runat="server" CssClass="panel-cover">
                        <%#Eval("MATERIALS_COMMENT_TEXT")%>
                    </asp:Panel>
                    <ajaxToolkit:CollapsiblePanelExtender ID="T10CollapsiblePanelExtender1" runat="server"
                        AutoCollapse="False" AutoExpand="False" CollapseControlID="T10lblview" Collapsed="true"
                        CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T10lblview"
                        ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T10Panel1"
                        TextLabelID="T10lblview">
                    </ajaxToolkit:CollapsiblePanelExtender>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author">
                <HeaderTemplate>
                     Author
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("empname")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="griditem"  Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    &nbsp;<br />
    <asp:CheckBox ID="T10CheckBox2" runat="server" onclick="javascript:showhide(this,'Panel2');"  Text="MAGT Geography"  CssClass="field-label"/><br />
    <asp:Panel ID="Panel2" runat="server" CssClass="panel-cover"  >
        <table  width="100%">
            <tr>
                <td class="title-bg">
                    MAGT Geography</td>
            </tr>
            <tr>
                <td>
    <asp:GridView ID="T10GridView2" AllowPaging="True" PageSize="5"  AutoGenerateColumns="false" CssClass="table table-bordered table-row"
        EmptyDataText="No Records added yet" runat="server" Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Refer Date">
                <HeaderTemplate>
                   Refer Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Refer By">
                <HeaderTemplate>
                    Refer By
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_BY")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start Date">
                <HeaderTemplate>
                     Start Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("START_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Date">
                <HeaderTemplate>
                    End Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("END_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Materials Used/Comments">
                <HeaderTemplate>
                   Materials Used/Comments
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="T10lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                    <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                    <asp:Panel ID="T10Panel1" runat="server" CssClass="panel-cover">
                        <%#Eval("MATERIALS_COMMENT_TEXT")%>
                    </asp:Panel>
                    <ajaxToolkit:CollapsiblePanelExtender ID="T10CollapsiblePanelExtender1" runat="server"
                        AutoCollapse="False" AutoExpand="False" CollapseControlID="T10lblview" Collapsed="true"
                        CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T10lblview"
                        ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T10Panel1"
                        TextLabelID="T10lblview">
                    </ajaxToolkit:CollapsiblePanelExtender>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author">
                <HeaderTemplate>
                    Author
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("empname")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
         <RowStyle CssClass="griditem" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
    </asp:GridView>
    </td>
            </tr>
        </table>
    </asp:Panel>
    &nbsp;<br />
    <br />
    <asp:CheckBox ID="T10CheckBox3" runat="server" onclick="javascript:showhide(this,'Panel3');" Text="MAGT History" CssClass="field-label" /><br />
    <asp:Panel ID="Panel3" runat="server" CssClass="panel-cover">
        <table  width="100%">
            <tr>
                <td class="title-bg">
                    MAGT History</td>
            </tr>
            <tr>
                <td>
    <asp:GridView ID="T10GridView3" AllowPaging="True" PageSize="5" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
        EmptyDataText="No Records added yet" runat="server" Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Refer Date">
                <HeaderTemplate>
                    Refer Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Refer By">
                <HeaderTemplate>
                     Refer By
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_BY")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start Date">
                <HeaderTemplate>
                  Start Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("START_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Date">
                <HeaderTemplate>
                     End Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("END_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Materials Used/Comments">
                <HeaderTemplate>
                    Materials Used/Comments
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="T10lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                    <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                    <asp:Panel ID="T10Panel1" runat="server" CssClass="panel-cover">
                        <%#Eval("MATERIALS_COMMENT_TEXT")%>
                    </asp:Panel>
                    <ajaxToolkit:CollapsiblePanelExtender ID="T10CollapsiblePanelExtender1" runat="server"
                        AutoCollapse="False" AutoExpand="False" CollapseControlID="T10lblview" Collapsed="true"
                        CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T10lblview"
                        ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T10Panel1"
                        TextLabelID="T10lblview">
                    </ajaxToolkit:CollapsiblePanelExtender>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author">
                <HeaderTemplate>
                     Author
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("empname")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="griditem"  Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    &nbsp;<br />
    <asp:CheckBox ID="T10CheckBox4" runat="server" onclick="javascript:showhide(this,'Panel4');"   Text="MAGT ICT" CssClass="field-label" /><br />
    <asp:Panel ID="Panel4" runat="server" CssClass="panel-cover">
        <table  width="100%">
            <tr>
                <td class="title-bg">
                    MAGT &nbsp;ICT</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="T10GridView4" AllowPaging="True" PageSize="5"  CssClass="table table-bordered table-row"
        AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server"
        Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Refer Date">
                <HeaderTemplate>
                   Refer Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Refer By">
                <HeaderTemplate>
                    Refer By
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_BY")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start Date">
                <HeaderTemplate>
                   Start Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("START_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Date">
                <HeaderTemplate>
                    End Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("END_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Materials Used/Comments">
                <HeaderTemplate>
                    Materials Used/Comments
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="T10lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                    <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                    <asp:Panel ID="T10Panel1" runat="server" CssClass="panel-cover">
                        <%#Eval("MATERIALS_COMMENT_TEXT")%>
                    </asp:Panel>
                    <ajaxToolkit:CollapsiblePanelExtender ID="T10CollapsiblePanelExtender1" runat="server"
                        AutoCollapse="False" AutoExpand="False" CollapseControlID="T10lblview" Collapsed="true"
                        CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T10lblview"
                        ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T10Panel1"
                        TextLabelID="T10lblview">
                    </ajaxToolkit:CollapsiblePanelExtender>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author">
                <HeaderTemplate>
                    Author
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("empname")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="griditem"  Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    &nbsp;
    <br />
    <asp:CheckBox ID="T10CheckBox5" runat="server" onclick="javascript:showhide(this,'Panel5');"  Text="MAGT Languages"  CssClass="field-label"/><br />
    <asp:Panel ID="Panel5" runat="server" CssClass="panel-cover">
        <table  width="100%">
            <tr>
                <td class="title-bg">
                    MAGT&nbsp; Languages</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="T10GridView5" AllowPaging="True" PageSize="5"  CssClass="table table-bordered table-row"
        AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server"
        Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Refer Date">
                <HeaderTemplate>
                    Refer Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Refer By">
                <HeaderTemplate>
                    Refer By
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_BY")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start Date">
                <HeaderTemplate>
                     Start Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("START_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Date">
                <HeaderTemplate>
                   End Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("END_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Materials Used/Comments">
                <HeaderTemplate>
                      Materials Used/Comments
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="T10lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                    <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                    <asp:Panel ID="T10Panel1" runat="server" CssClass="panel-cover">
                        <%#Eval("MATERIALS_COMMENT_TEXT")%>
                    </asp:Panel>
                    <ajaxToolkit:CollapsiblePanelExtender ID="T10CollapsiblePanelExtender1" runat="server"
                        AutoCollapse="False" AutoExpand="False" CollapseControlID="T10lblview" Collapsed="true"
                        CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T10lblview"
                        ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T10Panel1"
                        TextLabelID="T10lblview">
                    </ajaxToolkit:CollapsiblePanelExtender>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author">
                <HeaderTemplate>
                    Author
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("empname")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="griditem" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    &nbsp;
    <br />
    <asp:CheckBox ID="T10CheckBox6" runat="server" onclick="javascript:showhide(this,'Panel6');"  Text="MAGT Literacy" CssClass="field-label" /><br />
    <asp:Panel ID="Panel6" runat="server" CssClass="panel-cover">
        <table  width="100%">
            <tr>
                <td class="title-bg">
                    MAGT Literacy</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="T10GridView6" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row"
        AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server"
        Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Refer Date">
                <HeaderTemplate>
                   Refer Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Refer By">
                <HeaderTemplate>
                    Refer By
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_BY")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start Date">
                <HeaderTemplate>
                      Start Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("START_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Date">
                <HeaderTemplate>
                      End Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("END_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Materials Used/Comments">
                <HeaderTemplate>
                    Materials Used/Comments
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="T10lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                    <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                    <asp:Panel ID="T10Panel1" runat="server" CssClass="panel-cover">
                        <%#Eval("MATERIALS_COMMENT_TEXT")%>
                    </asp:Panel>
                    <ajaxToolkit:CollapsiblePanelExtender ID="T10CollapsiblePanelExtender1" runat="server"
                        AutoCollapse="False" AutoExpand="False" CollapseControlID="T10lblview" Collapsed="true"
                        CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T10lblview"
                        ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T10Panel1"
                        TextLabelID="T10lblview">
                    </ajaxToolkit:CollapsiblePanelExtender>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author">
                <HeaderTemplate>
                    Author
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("empname")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="griditem"  Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    &nbsp;
    <br />
    <asp:CheckBox ID="T10CheckBox7" runat="server" onclick="javascript:showhide(this,'Panel7');"  Text="MAGT Maths" CssClass="field-label" /><br />
    <asp:Panel ID="Panel7" runat="server" CssClass="panel-cover"> 
        <table  width="100%">
            <tr>
                <td class="title-bg">
                    MAGT&nbsp; Maths</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="T10GridView7" AllowPaging="True" PageSize="5"  CssClass="table table-bordered table-row"
        AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server"
        Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Refer Date">
                <HeaderTemplate>
                   Refer Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Refer By">
                <HeaderTemplate>
                    Refer By
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_BY")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start Date">
                <HeaderTemplate>
                   Start Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("START_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Date">
                <HeaderTemplate>
                   End Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("END_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Materials Used/Comments">
                <HeaderTemplate>
                     Materials Used/Comments
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="T10lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                    <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                    <asp:Panel ID="T10Panel1" runat="server" CssClass="panel-cover">
                        <%#Eval("MATERIALS_COMMENT_TEXT")%>
                    </asp:Panel>
                    <ajaxToolkit:CollapsiblePanelExtender ID="T10CollapsiblePanelExtender1" runat="server"
                        AutoCollapse="False" AutoExpand="False" CollapseControlID="T10lblview" Collapsed="true"
                        CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T10lblview"
                        ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T10Panel1"
                        TextLabelID="T10lblview">
                    </ajaxToolkit:CollapsiblePanelExtender>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author">
                <HeaderTemplate>
                     Author
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("empname")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
       <RowStyle CssClass="griditem"  Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    &nbsp;
    <br />
    <asp:CheckBox ID="T10CheckBox8" runat="server" onclick="javascript:showhide(this,'Panel8');"  Text="MAGT Music" CssClass="field-label" /><br />
    <asp:Panel ID="Panel8" runat="server" CssClass="panel-cover">
        <table  width="100%">
            <tr>
                <td class="title-bg">
                    MAGT&nbsp; Music</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="T10GridView8" AllowPaging="True" PageSize="5"  CssClass="table table-bordered table-row"
        AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server"
        Width="100%">        
        <Columns>
            <asp:TemplateField HeaderText="Refer Date">
                <HeaderTemplate>
                    Refer Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Refer By">
                <HeaderTemplate>
                     Refer By
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_BY")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start Date">
                <HeaderTemplate>
                    Start Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("START_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Date">
                <HeaderTemplate>
                    End Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("END_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Materials Used/Comments">
                <HeaderTemplate>
                      Materials Used/Comments
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="T10lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                    <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                    <asp:Panel ID="T10Panel1" runat="server" CssClass="panel-cover">
                        <%#Eval("MATERIALS_COMMENT_TEXT")%>
                    </asp:Panel>
                    <ajaxToolkit:CollapsiblePanelExtender ID="T10CollapsiblePanelExtender1" runat="server"
                        AutoCollapse="False" AutoExpand="False" CollapseControlID="T10lblview" Collapsed="true"
                        CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T10lblview"
                        ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T10Panel1"
                        TextLabelID="T10lblview">
                    </ajaxToolkit:CollapsiblePanelExtender>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author">
                <HeaderTemplate>
                     Author
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("empname")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
         <RowStyle CssClass="griditem"  Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    &nbsp;
    <br />
    <asp:CheckBox ID="T10CheckBox9" runat="server" onclick="javascript:showhide(this,'Panel9');"  Text="MAGT Performing  Arts" CssClass="field-label" /><br />
    <asp:Panel ID="Panel9" runat="server" CssClass="panel-cover">
        <table  width="100%">
            <tr>
                <td class="title-bg">
                    MAGT Performing Arts</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="T10GridView9" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row"
        AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server"
        Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Refer Date">
                <HeaderTemplate>
                    Refer Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Refer By">
                <HeaderTemplate>
                     Refer By
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_BY")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start Date">
                <HeaderTemplate>
                    Start Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("START_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Date">
                <HeaderTemplate>
                   End Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("END_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Materials Used/Comments">
                <HeaderTemplate>
                     Materials Used/Comments
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="T10lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                    <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                    <asp:Panel ID="T10Panel1" runat="server" CssClass="panel-cover">
                        <%#Eval("MATERIALS_COMMENT_TEXT")%>
                    </asp:Panel>
                    <ajaxToolkit:CollapsiblePanelExtender ID="T10CollapsiblePanelExtender1" runat="server"
                        AutoCollapse="False" AutoExpand="False" CollapseControlID="T10lblview" Collapsed="true"
                        CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T10lblview"
                        ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T10Panel1"
                        TextLabelID="T10lblview">
                    </ajaxToolkit:CollapsiblePanelExtender>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author">
                <HeaderTemplate>
                      Author
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("empname")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="griditem"  Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    &nbsp;
    <br />
    <asp:CheckBox ID="T10CheckBox10" runat="server" onclick="javascript:showhide(this,'Panel10');"  Text="MAGT Physical Education" /><br />
    <asp:Panel ID="Panel10" runat="server" CssClass="panel-cover">
        <table  width="100%">
            <tr>
                <td class="title-bg">
                    MAGT&nbsp; Physical Education</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="T10GridView10" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row"
        AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server"
        Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Refer Date">
                <HeaderTemplate>
                   Refer Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Refer By">
                <HeaderTemplate>
                   Refer By
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_BY")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start Date">
                <HeaderTemplate>
                  Start Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("START_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Date">
                <HeaderTemplate>
                  End Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("END_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Materials Used/Comments">
                <HeaderTemplate>
                    Materials Used/Comments
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="T10lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                    <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                    <asp:Panel ID="T10Panel1" runat="server" CssClass="panel-cover">
                        <%#Eval("MATERIALS_COMMENT_TEXT")%>
                    </asp:Panel>
                    <ajaxToolkit:CollapsiblePanelExtender ID="T10CollapsiblePanelExtender1" runat="server"
                        AutoCollapse="False" AutoExpand="False" CollapseControlID="T10lblview" Collapsed="true"
                        CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T10lblview"
                         ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T10Panel1"
                        TextLabelID="T10lblview">
                    </ajaxToolkit:CollapsiblePanelExtender>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author">
                <HeaderTemplate>
                      Author
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("empname")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
         <RowStyle CssClass="griditem" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    &nbsp;
    <br />
    <asp:CheckBox ID="T10CheckBox11" runat="server" onclick="javascript:showhide(this,'Panel11');"   Text="MAGT Science" CssClass="field-label" /><br />
    <asp:Panel ID="Panel11" runat="server" CssClass="panel-cover">
        <table  width="100%">
            <tr>
                <td class="title-bg">
                    MAGT&nbsp; Science</td>
            </tr>
            <tr>
                <td>
    <asp:GridView ID="T10GridView11" AllowPaging="True" PageSize="5"  CssClass="table table-bordered table-row"
        AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server"
        Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Refer Date">
                <HeaderTemplate>
                     Refer Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Refer By">
                <HeaderTemplate>
                      Refer By
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("REFER_BY")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Start Date">
                <HeaderTemplate>
                   Start Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("START_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Date">
                <HeaderTemplate>
                    End Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("END_DATE")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Materials Used/Comments">
                <HeaderTemplate>
                    Materials Used/Comments
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="T10lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                    <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                    <asp:Panel ID="T10Panel1" runat="server" CssClass="panel-cover">
                        <%#Eval("MATERIALS_COMMENT_TEXT")%>
                    </asp:Panel>
                    <ajaxToolkit:CollapsiblePanelExtender ID="T10CollapsiblePanelExtender1" runat="server"
                        AutoCollapse="False" AutoExpand="False" CollapseControlID="T10lblview" Collapsed="true"
                        CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T10lblview"
                        ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T10Panel1"
                        TextLabelID="T10lblview">
                    </ajaxToolkit:CollapsiblePanelExtender>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author">
                <HeaderTemplate>
                     Author          
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("empname")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
         <RowStyle CssClass="griditem"  Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    &nbsp;<br />
    <asp:LinkButton ID="T10lnkview2" OnClientClick="javascript:return false;" runat="server">Enter Notes</asp:LinkButton>
    <br />
    <br />
    <asp:Panel ID="T10Panel2" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4">Enter Notes</td>
            </tr>
            <tr>
                <td><span class="field-label">Author</span></td>
                <td>
                    <asp:DropDownList ID="T10ddstaffnotes" runat="server" ValidationGroup="T10notes">
                    </asp:DropDownList></td>
                <td><span class="field-label">Comments</span></td>
                <td>
                    <asp:TextBox ID="T10txtnotes" runat="server" TextMode="MultiLine"
                        ValidationGroup="T10notes" ></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" colspan="4" >
                    <asp:Button ID="T10btnnotessave" runat="server" CssClass="button" Text="Save" ValidationGroup="T10notes" /></td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="T10CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T10lnkview2" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter Notes" ExpandControlID="T10lnkview2" 
        ExpandedText="Hide-Notes" ScrollContents="false" TargetControlID="T10Panel2" TextLabelID="T10lnkview2">
    </ajaxToolkit:CollapsiblePanelExtender>
    <table  width="100%">
        <tr>
            <td class="title-bg">
                Notes</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T10GrdNotes" AutoGenerateColumns="false" EmptyDataText="No Notes added yet" CssClass="table table-bordered table-row"
                    runat="server" AllowPaging="True" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Date">
                         <HeaderTemplate>
                                Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("ENTRY_DATE","{0:dd/MMM/yyyy}") %>
                            </ItemTemplate>
                            <ItemStyle Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comment">
                         <HeaderTemplate>
                                  Comment
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T10lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T10Panel1" runat="server" CssClass="panel-cover">
                                    <%#Eval("MAGT_NOTE")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T10CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T10lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T10lblview"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T10Panel1"
                                    TextLabelID="T10lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Author">
                         <HeaderTemplate>
                                 Author
                            </HeaderTemplate>
                            <ItemTemplate>
                              <center><%#Eval("empname")%></center>
                            </ItemTemplate>
                            <ItemStyle  />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem"  Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="None"
        ErrorMessage="Please Select Author" SetFocusOnError="True" ControlToValidate="T10ddstaffnotes"
        InitialValue="-1" ValidationGroup="T10notes"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="None"
        ErrorMessage="Please Enter Notes" SetFocusOnError="True" ControlToValidate="T10txtnotes"
        ValidationGroup="T10notes"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="T10notes" />
</div>
