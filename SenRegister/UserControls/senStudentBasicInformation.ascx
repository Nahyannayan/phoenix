<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senStudentBasicInformation.ascx.vb" Inherits="SenRegister_UserControls_senStudentBasicInformation" %>

<%--<script type="text/javascript">

 window.setTimeout('check();', 1000);
 
 
 function check()
 {
 //alert(document.forms[0].elements.length)
 for(i=0; i<document.forms[0].elements.length; i++)
     {
                           
                           if(document.forms[0].elements[i].type=="checkbox")
                              {
                              var currentid =document.forms[0].elements[i].id; 
                              var currentID = document.getElementById(currentid);
		                        if (currentID.addEventListener)
		                        {
			                        currentID.addEventListener('click', alertMsg, false);
		                        }
		                        else
		                        {
		                       
			                        // For IE
			                        currentID.attachEvent('onclick', alertMsg, false);
		                        }

                              
                              
                              }
     }

 }
 function alertMsg()
 {
 //alert('FILE LOCKED'); 
 return false;
 }
</script>--%>

<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

<div >
 <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="title-bg-lite">
                                Student Details
                                <asp:CheckBox ID="CheckLock" Text="LOCK FILE" runat="server" AutoPostBack="True" />
                                <asp:CheckBox ID="CheckSeal" Text="SEAL FILE" runat="server" AutoPostBack="True" />
                                <asp:CheckBox ID="CheckActiveInactive" Text="Active/Inactive" runat="server" AutoPostBack="True" /></td>
                        </tr>
                        <tr>
                            <td >
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
        
            <span class="field-label">  Student Number</span></td>
        
        <td >
            <asp:Label ID="lblstuno" runat="server" ></asp:Label></td>
        <td colspan="7" align="right">
            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/stuimage.gif" /></td>
    </tr>
    <tr>
        <td >
            <span class="field-label">  FirstName</span></td>
     
        <td>
            <asp:Label ID="lblstufirstname" runat="server"></asp:Label>
        </td>
        <td>
            <span class="field-label">  Middle Name</span></td>
      
        <td >
            <asp:Label ID="lblstumiddlename" runat="server"></asp:Label></td>
        <td >
             <span class="field-label"> Last Name</span></td>
      
        <td >
            <asp:Label ID="lblstulastname" runat="server"></asp:Label></td>
        <td>
            <span class="field-label">  Gender</span></td>
      
        <td >
            <asp:Label ID="lblgender" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>
             <span class="field-label"> Date of Birth</span></td>
    
        <td >
            <asp:Label ID="lbldob" runat="server"></asp:Label></td>
        <td>
            <span class="field-label">  Age</span></td>
    
        <td >
            <asp:Label ID="lblage" runat="server"></asp:Label></td>
        <td >
             <span class="field-label"> Class</span></td>
      
        <td >
            <asp:Label ID="lblclass" runat="server"></asp:Label></td>
        <td >
             <span class="field-label"> Shift</span></td>
      
        <td >
            <asp:Label ID="lblshift" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td >
             <span class="field-label"> Stream</span></td>
   
        <td >
            <asp:Label ID="lblstream" runat="server"></asp:Label></td>
        <td >
             <span class="field-label"> Class Teacher</span></td>
    
        <td >
            <asp:Label ID="lblclassteacher" runat="server"></asp:Label></td>
        <td >
             <span class="field-label"> Summer Born</span></td>
      
        <td >
            <asp:Image ID="Image1" runat="server" /></td>
        <td >
        </td>
        <td >
        </td>
        <td >
        </td>
    </tr>
</table>



 </td>
                    </tr>
               </table>
<asp:HiddenField ID="Hiddenstuid" runat="server" />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:HiddenField ID="HiddenPrincipalBit" runat="server" />
    <asp:HiddenField ID="HiddenInclusionHeadBit" runat="server" />
</div>