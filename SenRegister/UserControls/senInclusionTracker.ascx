<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senInclusionTracker.ascx.vb"
    Inherits="SenRegister_UserControls_senInclusionTracker" %>
<asp:Label ID="lblstatusmessage" runat="server" EnableTheming="False" CssClass="error"></asp:Label>
<div>
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="4">PIVATS
                <asp:CheckBox ID="T8CheckPIVATS" runat="server" />(File Location / Directory)<asp:TextBox
                    ID="txtinfo" runat="server"></asp:TextBox>
                <asp:Button ID="T8btnSaveonfo" runat="server" CausesValidation="false"
                    CssClass="button" Text="Save" />
                <asp:LinkButton ID="T8linkPathInfo" CausesValidation="false" runat="server">Path</asp:LinkButton></td>
        </tr>
        <tr>
            <td width="20%">
                <span class="field-label">PIVATS</span></td>
            <td wdth="30%">
                <asp:RadioButtonList ID="T8RadioButtonListPIVATS" runat="server" RepeatDirection="Horizontal"
                    AutoPostBack="True">
                    <asp:ListItem Selected="True" Value="True">Y</asp:ListItem>
                    <asp:ListItem Value="False">N</asp:ListItem>
                </asp:RadioButtonList></td>
            <td colspan="2"></td>
            <%--<table>
    <tr>
        <td >
            NFERs Date</td>
        <td>
            :</td>
        <td >
            <asp:TextBox ID="T8txtnfersdate" runat="server" ValidationGroup="T8main"></asp:TextBox></td>
        <td style="width: 3px">
            <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
    </tr>
</table>--%>
        </tr>
    </table>
    <%--<ajaxToolkit:CalendarExtender ID="T8CE5" runat="server" Format="dd/MMM/yyyy"
    PopupButtonID="Image5" TargetControlID="T8txtnfersdate">
</ajaxToolkit:CalendarExtender>--%>
    <asp:HiddenField ID="T8Hiddeninfo" runat="server" />
    <br />
    <asp:LinkButton ID="T8lnkview4" runat="server" OnClientClick="javascript:return false;">Enter SATS</asp:LinkButton>
    <br />
    <br />
    <asp:Panel ID="T8Panel4" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4">Enter SATS</td>
            </tr>
            <tr>
                <td width="20%"><span class="field-label">Year</span></td>
                <td width="30%">
                    <asp:TextBox ID="T8txtsatsYear" runat="server" ValidationGroup="T8sats"></asp:TextBox></td>
                <td width="20%"><span class="field-label">Class</span>
                </t>
                <td width="30%">
                    <asp:TextBox ID="T8txtsatsclass" runat="server" ValidationGroup="T8sats"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="4" class="title-bg-lite">Reading</td>
            </tr>
            <tr>
                <td width="20%"><span class="field-label">TA</span></td>
                <td width="30%">
                    <asp:TextBox ID="T8txtsatsreadingta" runat="server"></asp:TextBox></td>
                <td width="20%"><span class="field-label">Exam</span></td>
                <td width="30%">
                    <asp:TextBox ID="T8txtsatsreadingexam" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="4" class="title-bg-lite">Writing</td>
            </tr>
            <tr>
                <td width="20%"><span class="field-label">TA</span></td>
                <td width="30%">
                    <asp:TextBox ID="T8txtsatswritingta" runat="server"></asp:TextBox></td>
                <td width="20%"><span class="field-label">Exam</span></td>
                <td width="30%">
                    <asp:TextBox ID="T8txtsatswritingexam" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="4" class="title-bg-lite">Maths</td>
            </tr>
            <tr>
                <td width="20%"><span class="field-label">TA</span></td>
                <td width="30%">
                    <asp:TextBox ID="T8txtsatsmathsta" runat="server"></asp:TextBox></td>
                <td width="20%"><span class="field-label">Exam</span></td>
                <td width="30%">
                    <asp:TextBox ID="T8txtsatsmathsexam" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="T8satssave" runat="server" CssClass="button" Text="Save" ValidationGroup="T8sats" CausesValidation="False" /></td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="T8CollapsiblePanelExtender4" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T8lnkview4" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter SATS" ExpandControlID="T8lnkview4"
        ExpandedText="Hide-SATS" ScrollContents="false" TargetControlID="T8Panel4" TextLabelID="T8lnkview4">
    </ajaxToolkit:CollapsiblePanelExtender>
    <table width="100%">
        <tr>
            <td class="title-bg">SATS</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T8GrdSats" AutoGenerateColumns="false" EmptyDataText="Records not yet added" CssClass="table table-bordered table-row"
                    runat="server" AllowPaging="True" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Year">
                            <HeaderTemplate>
                                Year
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HiddenField ID="T8HiddenFieldSATSID" Value='<%# Eval("SATS_ID") %>' runat="server" />
                                <center> <%# Eval("YEAR") %></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Class">
                            <HeaderTemplate>
                                Class
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center><%# Eval("CLASS") %></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reading">
                            <HeaderTemplate>
                                Reading
                                <br />
                                <table  width="100%">
                                    <tr >
                                        <td align="center" style="width: 50%">TA
                                        </td>
                                        <td align="center" style="width: 50%">Exam
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                           <%--     <center>--%>
                                <asp:GridView ID="T8GrdSatsReading" Width="100%" ShowHeader="false" runat="server" CssClass="table table-bordered table-row">
                                
                                </asp:GridView>
                              <%--  </center>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Writing">
                            <HeaderTemplate>
                                Writing <br />
                                <table  width="100%">                                  
                                    <tr >
                                        <td align="center" style="width: 50%">TA
                                        </td>
                                        <td align="center" style="width: 50%">Exam
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%--<center>--%> <asp:GridView ID="T8GrdSatsWriting" Width="100%" ShowHeader="false" runat="server" CssClass="table table-bordered table-row">
                                </asp:GridView><%--</center>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Maths">
                            <HeaderTemplate>
                                Maths <br />
                                <table  width="100%">                                    
                                    <tr >
                                        <td align="center" style="width: 50%">TA
                                        </td>
                                        <td align="center" style="width: 50%">Exam
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%--<center> --%><asp:GridView ID="T8GrdSatsMaths" Width="100%" ShowHeader="false" runat="server" CssClass="table table-bordered table-row">
                                </asp:GridView><%--</center>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem"  Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:RequiredFieldValidator ID="T8RequiredFieldValidator8" runat="server" ControlToValidate="T8txtsatsYear"
        Display="None" ErrorMessage="Please Enter Year" SetFocusOnError="True" ValidationGroup="T8sats"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="T8RequiredFieldValidator9" runat="server" ControlToValidate="T8txtsatsclass"
        Display="None" ErrorMessage="Please Enter Class" SetFocusOnError="True" ValidationGroup="T8sats"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary4" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="T8sats" />
    <br />
    <br />
    <asp:LinkButton ID="T8lnkview3" OnClientClick="javascript:return false;" runat="server">Enter Verbal/Non Verbal</asp:LinkButton>
    <br />
    <br />
    <asp:Panel ID="T8Panel3" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4"> Enter Verbal/Non Verbal</td>
            </tr>
            <tr>
                <td width="20%"><span class="field-label">Verbal</span></td>
                <td width="30%">
                    <asp:TextBox ID="T8txtverbal" runat="server" ValidationGroup="T8verbnonverb"></asp:TextBox></td>
                <td width="20%"><span class="field-label">Non Verbal</span></td>
                <td width="30%">
                    <asp:TextBox ID="T8txtnonverbal" runat="server" ValidationGroup="T8verbnonverb"></asp:TextBox></td>
               
            </tr>
            <tr>
                 <td colspan="4" align="center">
                    <asp:Button ID="T8btnvernverb" runat="server" CssClass="button" Text="Save" ValidationGroup="T8verbnonverb" /></td>
            </tr>
        </table>
        <asp:RequiredFieldValidator ID="T8RequiredFieldValidator6" runat="server" ControlToValidate="T8txtverbal"
            Display="None" ErrorMessage="Please Enter Verbal" SetFocusOnError="True" ValidationGroup="T8verbnonverb"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="T8RequiredFieldValidator7" runat="server" ControlToValidate="T8txtnonverbal"
            Display="None" ErrorMessage="Please Enter Non Verbal" SetFocusOnError="True"
            ValidationGroup="T8verbnonverb"></asp:RequiredFieldValidator>
        <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowMessageBox="True"
            ShowSummary="False" ValidationGroup="T8verbnonverb" />
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="T8CollapsiblePanelExtender3" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T8lnkview3" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter Verbal/Non Verbal" ExpandControlID="T8lnkview3"
        ExpandedText="Hide-Verbal/Non Verbal" ScrollContents="false"
        TargetControlID="T8Panel3" TextLabelID="T8lnkview3">
    </ajaxToolkit:CollapsiblePanelExtender>
    <table width="100%">
        <tr>
            <td class="title-bg">Verbal/Non Verbal</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T8Grdverbnonverb" AutoGenerateColumns="false" EmptyDataText="No Entry added yet" CssClass="table table-bordered table-row"
                    runat="server" AllowPaging="True" PageSize="5" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Verbal">
                            <HeaderTemplate>
                              Verbal
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>  <%# Eval("VERB_TEXT") %></center>
                            </ItemTemplate>
                            <ItemStyle Width="50%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Non Verbal">
                            <HeaderTemplate>
                              Non Verbal
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center> <%# Eval("NON_VERB_TEXT") %></center>
                            </ItemTemplate>
                            <ItemStyle Width="50%" />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem"  Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="4">Interventions at
                <asp:Label ID="T8BsuLabel" runat="server" Text=""></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:CheckBoxList ID="T8CheckBoxListCategory" runat="server" RepeatDirection="Horizontal"
                    RepeatColumns="3">
                    <%--<asp:ListItem Value="0">Ruth Miskin Reading Books</asp:ListItem>
    <asp:ListItem Value="1">Ruth Miskin  Fresh Start</asp:ListItem>
    <asp:ListItem Value="2">Ruth Miskin  Spelling</asp:ListItem>
    <asp:ListItem Value="3">Ruth Miskin  Comprehension</asp:ListItem>
    <asp:ListItem Value="4">Word Shark</asp:ListItem>
    <asp:ListItem Value="5">Lexion</asp:ListItem>
    <asp:ListItem Value="6">Touch Typing</asp:ListItem>
    <asp:ListItem Value="7">Numicon</asp:ListItem>
    <asp:ListItem Value="8">Accelerread/Write</asp:ListItem>
    <asp:ListItem Value="9">Alphasmart</asp:ListItem>
    <asp:ListItem Value="10">Number Shark</asp:ListItem>
    <asp:ListItem Value="11">Clicker</asp:ListItem>
    <asp:ListItem Value="12">The Listening Programme</asp:ListItem>
    <asp:ListItem Value="13">SRA Reading Lab</asp:ListItem>--%>
                </asp:CheckBoxList>
                <%--<table border="0" cellpadding="0" cellspacing="0" style="width: 285px">
                    <tr>
                        <td align="right">
                            </td>
                    </tr>
                </table>--%> <br />
                <asp:CheckBoxList ID="T8CheckBoxListSubCategory" runat="server" RepeatColumns="2"
                                RepeatDirection="Horizontal">
                                <%-- <asp:ListItem Value="1A">1A</asp:ListItem>
                <asp:ListItem Value="2A">2A</asp:ListItem>
                <asp:ListItem Value="1B">1B</asp:ListItem>
                <asp:ListItem Value="2B">2B</asp:ListItem>
                <asp:ListItem Value="1C">1C</asp:ListItem>
                <asp:ListItem Value="2C">2C</asp:ListItem>--%>
                            </asp:CheckBoxList>

                <asp:LinkButton ID="T8lnkTracking" runat="server" CausesValidation="False">Save</asp:LinkButton></td>
        </tr>
    </table>
    <br />
    <asp:LinkButton ID="T8lnkview1" OnClientClick="javascript:return false;" runat="server">Enter Intervention Tracking</asp:LinkButton>
    <br />
    <br />
    <asp:Panel ID="T8Panel1" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4">Enter Intervention Tracking</td>
            </tr>
            <tr>
                <td width="20%"><span class="field-label">Start Date</span></td>

                <td width="30%">
                    <asp:TextBox ID="T8txtStartDate" runat="server" onfocus="javascript:this.blur();return false;" ValidationGroup="T8save"></asp:TextBox>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" /></t>
                <td width="20%"><span class="field-label">End Date</span></td>

                <td width="30%">
                    <asp:TextBox ID="T8txtEndDate" onfocus="javascript:this.blur();return false;" runat="server" ValidationGroup="T8save"></asp:TextBox>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar.gif" /></td>                
            </tr>
            <tr>
                <td width="20%"><span class="field-label">Intervention</span></td>

                <td width="30% ">
                    <asp:DropDownList ID="T8ddIntervention" runat="server" ValidationGroup="T8save">
                    </asp:DropDownList></td>
                <td width="20%"><span class="field-label">Outcome</span></td>
                <td width="30%">
                    <asp:TextBox ID="T8txtOutCome" runat="server" TextMode="MultiLine"
                        ValidationGroup="T8save" ></asp:TextBox></td>
            </tr>            
            <tr>
                <td colspan="4" align="center">
                    <asp:Button ID="T8btnsave" runat="server" CssClass="button" Text="Save" ValidationGroup="T8save" /></td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="T8CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T8lnkview1" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter Intervention Tracking" ExpandControlID="T8lnkview1"
        ExpandedText="Hide-Intervention Tracking" ScrollContents="false"
        TargetControlID="T8Panel1" TextLabelID="T8lnkview1">
    </ajaxToolkit:CollapsiblePanelExtender>
    <table width="100%">
        <tr>
            <td class="title-bg">Intervention Tracking</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T8GrdInterTracking" AutoGenerateColumns="false" runat="server" CssClass="table table-bordered table-row"
                    AllowPaging="True" PageSize="5" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Start Date">
                            <HeaderTemplate>
                              Start Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("START_DATE")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="End Date">
                            <HeaderTemplate>
                                End Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("END_DATE")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Intervention">
                            <HeaderTemplate>
                              Intervention
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("CATEGORY_DES")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Outcome">
                            <HeaderTemplate>
                              Outcome
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("OUTCOME_TEXT") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <ajaxToolkit:CalendarExtender ID="T8CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
        TargetControlID="T8txtStartDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T8CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image2"
        TargetControlID="T8txtEndDate">
    </ajaxToolkit:CalendarExtender>
    <br />
    <asp:LinkButton ID="T8lnkView2" OnClientClick="javascript:return false;" runat="server">Enter Group Work</asp:LinkButton>
    <br />
    <br />
    <asp:Panel ID="T8Panel2" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4">Enter Group Work</td>
            </tr>
            <tr>
                <td width="20%"><span class="field-label"> Start Date</span><</td>
                <td width="30%">
                    <asp:TextBox ID="T8txtgpstaartdate" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                    <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                <td width="20%"><span class="field-label">End Date </span></td>
                <td width="30%">
                    <asp:TextBox ID="T8txtgpenddate" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                    <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
            </tr>
            <tr>
                <td width="20% "><span class="field-label">Group Type </span></td>
                <td width="30%">
                    <asp:TextBox ID="T8txtgrouptype" runat="server"></asp:TextBox></td>
                <td width="20%"><span class="field-label">Results</span></td>
                <td width="30%">
                    <asp:TextBox  ID="T8txtresult" runat="server" TextMode="MultiLine"
                        ValidationGroup="T8save"></asp:TextBox></td>
            </tr>           
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="T8gpSave" runat="server" CssClass="button" Text="Save" CausesValidation="False" /></td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg">Group Work</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T8GrdGroupWork" EmptyDataText="No Groups Works added yet" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                    runat="server" AllowPaging="True" PageSize="5" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Start Date">
                            <HeaderTemplate>
                            Start Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("START_DATE")%>
                            </ItemTemplate>
                            <ItemStyle />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="End Date">
                            <HeaderTemplate>
                                End Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("END_DATE")%>
                            </ItemTemplate>
                            <ItemStyle  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Group Type">
                            <HeaderTemplate>
                                Group Type
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center> <%#Eval("GROUP_TYPE")%></center>
                            </ItemTemplate>
                            <ItemStyle />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Result">
                            <HeaderTemplate>
                                Result
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T8lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T8Panel1" runat="server" >
                                    <%#Eval("RESULT_TEXT")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T8CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T8lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T8lblview"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T8Panel1"
                                    TextLabelID="T8lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem"  Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <ajaxToolkit:CollapsiblePanelExtender ID="T8CollapsiblePanelExtender2" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T8lnkview2" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter Group Work" ExpandControlID="T8lnkview2"
        ExpandedText="Hide-Group Work" ScrollContents="false" TargetControlID="T8Panel2"
        TextLabelID="T8lnkview2">
    </ajaxToolkit:CollapsiblePanelExtender>
    <ajaxToolkit:CalendarExtender ID="T8CE3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image3"
        TargetControlID="T8txtgpstaartdate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T8CE4" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image4"
        TargetControlID="T8txtgpenddate">
    </ajaxToolkit:CalendarExtender>
    &nbsp;
    <asp:RequiredFieldValidator ID="T8RequiredFieldValidator3" runat="server" ErrorMessage="Select a Intervention"
        ControlToValidate="T8ddIntervention" Display="None" InitialValue="-1" SetFocusOnError="True"
        ValidationGroup="T8save"></asp:RequiredFieldValidator>&nbsp;
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="T8save" />
    &nbsp;<br />
    <br />
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="4">Enter LUCID Details</td>
        </tr>
        <tr>
            <td width="20%"><span class="field-label">Date</span>
            </td>
            <td width="30%">
                <asp:TextBox ID="txtluciddate" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/calendar.gif" />
                <ajaxToolkit:CalendarExtender ID="CalendarELUCIS" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="Image5" TargetControlID="txtluciddate">
                </ajaxToolkit:CalendarExtender>
            </td>
            <td width="20%"><span class="field-label">Phonological Processing SS</span></td>
            <td width="30%">
                <asp:TextBox ID="txtlucidppss" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="20%"><span class="field-label">Auditory Sequential Memory SS</span></td>
            <td wdth="30%">
                <asp:TextBox ID="txtlucidasmss" runat="server"></asp:TextBox>
            </td>
            <td width="20%"><span class="field-label">Visual Verbal Memory SS</span></td>
            <td width="30%">
                <asp:TextBox ID="txtlucidvvmss" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Button ID="btnlucidsave" runat="server" CssClass="button" CausesValidation="false"
                    Text="Save" /></td>
        </tr>
    </table>       
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg">LUCID</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridLUCID" EmptyDataText="No records added yet" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                    runat="server" AllowPaging="True" PageSize="5" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderTemplate>
                                Date
                            </HeaderTemplate>

                            <ItemTemplate>
                                <center>
                                    <%#Eval("LUCID_DATE")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PP-SS">
                            <HeaderTemplate>
                              PP-SS
                            </HeaderTemplate>

                            <ItemTemplate>
                                <center>
                                    <%#Eval("PP_SS")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ASM-SS">
                            <HeaderTemplate>
                             ASM-SS
                            </HeaderTemplate>

                            <ItemTemplate>
                                <center>
                                    <%#Eval("ASM_SS")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VVM-SS">
                            <HeaderTemplate>
                                VVM-SS
                            </HeaderTemplate>

                            <ItemTemplate>
                                <center>
                                    <%#Eval("VVM_SS")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
                PP-SS (Phonological Processing SS)<br />
                ASM-SS (Auditory Sequential Memory SS)<br />
                VVM-SS (Visual Verbal Memory SS)</td>
        </tr>
    </table>
    <br />
    <br />

    <br />
    <br />
    <asp:HiddenField ID="Hiddenstuid" runat="server" />
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
</div>
