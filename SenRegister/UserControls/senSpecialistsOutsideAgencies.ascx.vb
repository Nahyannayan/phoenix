Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senSpecialistsOutsideAgencies
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindControls()
            BindSCategoryMaster()
            BindCategory()
            BindNotes()
            LockFileCheck()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindSCategoryMaster()

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_SPEC_CATEGORY_MASTER where CATEGORY_BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        T13CheckBoxListspec.DataSource = ds
        T13CheckBoxListspec.DataTextField = "CATEGORY_DES"
        T13CheckBoxListspec.DataValueField = "RECORD_ID"
        T13CheckBoxListspec.DataBind()

    End Sub
    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then

            T13btnsave.Enabled = False
            T13NotesSave.Enabled = False

        End If

    End Sub
    Public Sub BindControls()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id and a.EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        T13ddStaffNotes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T13ddStaffNotes.DataTextField = "empname"
        T13ddStaffNotes.DataValueField = "EMP_ID"
        T13ddStaffNotes.DataBind()
        Dim list As New ListItem
        list.Text = "Select an Inclusion Member"
        list.Value = "-1"
        T13ddStaffNotes.Items.Insert(0, list)

    End Sub
    Public Sub BindCategory()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_SPEC_CATEGORY where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        'T13T1.Visible = False
        'T13T2.Visible = False
        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim catid = ds.Tables(0).Rows(i).Item("CATEGORY_ID").ToString()
                'Dim catval = ds.Tables(0).Rows(i).Item("CATEGORY_VALUE").ToString()
                For Each item As ListItem In T13CheckBoxListspec.Items
                    If item.Value = catid Then
                        item.Selected = True
                        'If item.Value = "15" Then
                        '    T13txtoutsidetuition.Text = catval
                        '    'T13T1.Visible = True
                        'End If
                        'If item.Value = "17" Then
                        '    T13txtothers.Text = catval
                        '    'T13T2.Visible = True
                        'End If
                    End If
                Next
            Next
        End If

        strQuery = "select OUTSIDE_TUTION_TYPE,OTHER from  SEN_DEPARTMENT_SPEC_CATEGORY_OTHER where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)


        If ds.Tables(0).Rows.Count > 0 Then
            T13txtoutsidetuition.Text = ds.Tables(0).Rows(0).Item("OUTSIDE_TUTION_TYPE").ToString()
            T13txtothers.Text = ds.Tables(0).Rows(0).Item("OTHER").ToString()
        End If


    End Sub
    Public Sub BindNotes()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select NOTES_TEXT,ENTRY_DATE, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname , '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(NOTES_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview from SEN_DEPARTMENT_SPEC_NOTES a inner join oasis.dbo.EMPLOYEE_M b on  a.ENTRY_EMP_ID=b.emp_id where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "' order by A.ENTRY_DATE desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T13GrdNotes.DataSource = ds
        T13GrdNotes.DataBind()
    End Sub
    'Protected Sub T13CheckBoxListspec_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T13CheckBoxListspec.SelectedIndexChanged
    '    If T13CheckBoxListspec.SelectedIndex = 14 Then
    '        T13T1.Visible = True

    '    End If
    '    For Each item As ListItem In T13CheckBoxListspec.Items
    '        If item.Value = "15" Then
    '            If item.Selected Then
    '                T13T1.Visible = True
    '            Else
    '                T13T1.Visible = False
    '            End If
    '        End If
    '        If item.Value = "17" Then
    '            If item.Selected Then
    '                T13T2.Visible = True
    '            Else
    '                T13T2.Visible = False
    '            End If

    '        End If
    '    Next
    'End Sub

    Protected Sub T13btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T13btnsave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString

            For Each item As ListItem In T13CheckBoxListspec.Items

                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
                pParms(2) = New SqlClient.SqlParameter("@CATEGORY_ID", item.Value)
                'If item.Value = "15" Then
                '    pParms(3) = New SqlClient.SqlParameter("@CATEGORY_VALUE", T13txtoutsidetuition.Text.Trim())
                'End If
                'If item.Value = "17" Then
                '    pParms(3) = New SqlClient.SqlParameter("@CATEGORY_VALUE", T13txtothers.Text.Trim())
                'End If
                pParms(3) = New SqlClient.SqlParameter("@SELECTED", item.Selected)
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_SPEC_CATEGORY", pParms)
            Next

            Dim pParms1(4) As SqlClient.SqlParameter
            pParms1(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms1(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms1(2) = New SqlClient.SqlParameter("@OUTSIDE_TUTION_TYPE", T13txtoutsidetuition.Text)
            pParms1(3) = New SqlClient.SqlParameter("@OTHER", T13txtothers.Text)

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_SPEC_CATEGORY_OTHER", pParms1)


            BindCategory()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try
       
    End Sub

    Protected Sub T13NotesSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T13NotesSave.Click
        lblstatusmessage.Text = ""
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@NOTES_TEXT", T13Notes.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T13ddStaffNotes.SelectedValue)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_SPEC_NOTES", pParms)
            T13Notes.Text = ""
            T13ddStaffNotes.SelectedValue = "-1"
            BindNotes()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T13GrdNotes_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T13GrdNotes.PageIndexChanging
        T13GrdNotes.PageIndex = e.NewPageIndex
        BindNotes()
    End Sub
End Class
