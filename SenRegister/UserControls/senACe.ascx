<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senACe.ascx.vb" Inherits="SenRegister_UserControls_senACe" %>

<asp:Label ID="lblstatusmessage" runat="server" EnableTheming="False" CssClass="error"></asp:Label><br />


<div>
    <table width="100%">
        <tr>
            <td class="title-bg">ACE Entry</td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td width="20%">
                            <asp:Label ID="T4lbldateenteredlabel" runat="server"></asp:Label></td>
                        <td width="30%">
                            <asp:Label ID="T4lbldateentered" runat="server"></asp:Label>
                        </t>
                        <td width="20%">
                            <span class="field-label">Date Entered ACe</span></td>
                        <td width="30%">
                            <asp:TextBox ID="T4lblDateenteredACe" runat="server" onfocus="javascript:this.blur();return false;"></asp:TextBox>
                            <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <asp:LinkButton ID="lnkentrysave" runat="server" CausesValidation="False">Save</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%">
                            <span class="field-label">Time Lag </span></td>
                        <td width="30%">
                            <asp:Label ID="T4lblTimeLag" runat="server"></asp:Label></td>
                        <td colspan="2"></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="Image6" TargetControlID="T4lblDateenteredACe">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg">Referred By</td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td colspan="4">
                            <asp:RadioButtonList ID="T4RadioButtonListReffer" runat="server" RepeatDirection="Horizontal"
                                AutoPostBack="True">
                                <asp:ListItem Selected="True" Value="CS" >Current Staff</asp:ListItem>
                                <asp:ListItem Value="PSM">Past Staff Member</asp:ListItem>
                                <asp:ListItem Value="P">Parent</asp:ListItem>
                                <asp:ListItem Value="0">Others</asp:ListItem>
                            </asp:RadioButtonList></td>
                    </tr>
                    <tr>
                        <td width="20%">
                            <span class="field-label">Refered By </span></td>

                        <td width="30%">
                            <asp:DropDownList ID="T4ddrefferedby" runat="server">
                            </asp:DropDownList><asp:TextBox ID="T4txtrefferedby" runat="server" Visible="False"></asp:TextBox><asp:RadioButtonList
                                ID="RadioReferparent" RepeatDirection="Horizontal" runat="server" Visible="False">
                                <asp:ListItem Selected="True" Value="F">Father</asp:ListItem>
                                <asp:ListItem Value="M">Mother</asp:ListItem>
                            </asp:RadioButtonList></td>

                        <td width="20%">
                            <span class="field-label">Referral Date </span></td>

                        <td width="30%">
                            <asp:TextBox ID="T4txtrefferdate" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                    </tr>
                    <tr>
                        <td width="20%">
                            <span class="field-label">Author of Entry </span></td>

                        <td width="30%">
                            <asp:DropDownList ID="T4ddStaffs" runat="server">
                            </asp:DropDownList></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">Summary of Reasons for Referral (Full details see file)</span></td>
                        <td width="30%">
                            <asp:TextBox ID="T4txtReffersummary" runat="server"
                                TextMode="MultiLine" ValidationGroup="T3Reasons" ></asp:TextBox>
                        </td>
                        <td width="20%"><span class="field-label">File Location / Directory </span>
                        </td>
                        <td width="30%">
                            <asp:TextBox ID="txtAceinfo" runat="server"></asp:TextBox>
                            <asp:LinkButton ID="LinkFileInfo" CausesValidation="false" runat="server">File Info</asp:LinkButton></td>
                    </tr>


                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="T4refsave" CssClass="button" runat="server" Text="Save" CausesValidation="False" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg">No. of Previous Schools Attended</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T4GrdPreviousSchools" EmptyDataText="No info available" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                    runat="server" Width="100%">

                    <Columns>
                        <asp:TemplateField HeaderText="Name of school">
                            <HeaderTemplate>
                                Name of school
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("STU_PREVSCHI")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="From Date">
                            <HeaderTemplate>
                                From Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("STU_PREVSCHI_FROMDATE", "{0:dd/MMM/yyyy}")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="To Date">
                            <HeaderTemplate>
                                To Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("STU_PREVSCHI_TODATE", "{0:dd/MMM/yyyy}")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Support">
                            <HeaderTemplate>
                                Support
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("STU_PREVSCHI_bSPMEDICATION")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Information/Contact">
                            <HeaderTemplate>
                                Information/Contact
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("STU_PREVSCHI_SPMEDICATION_REMARKS")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <asp:LinkButton ID="T4lnknotes" OnClientClick="javascript:return false;" runat="server">Enter Notes</asp:LinkButton><br />
    <br />
    <ajaxToolkit:CollapsiblePanelExtender ID="T4CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T4lnknotes" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter Notes" ExpandControlID="T4lnknotes"
        ExpandedText="Hide-Notes" ScrollContents="false" TargetControlID="T4Panel1" TextLabelID="T4lnknotes">
    </ajaxToolkit:CollapsiblePanelExtender>
    <asp:Panel ID="T4Panel1" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg">Enter Notes</td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:DropDownList ID="T4ddStaffNotes" runat="server" ValidationGroup="T4Notes">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="T4Notes" runat="server" TextMode="MultiLine"
                                    ValidationGroup="T4Notes"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="T4NotesSave" CssClass="button" runat="server" Text="Save" ValidationGroup="T4Notes" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table width="100%">
        <tr>
            <td class="title-bg">Notes</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T4GrdNotes" EmptyDataText="No Notes added yet" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                    runat="server" Width="100%" AllowPaging="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderTemplate>
                                Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center><%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%></center>

                            </ItemTemplate>
                            <ItemStyle />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Notes">
                            <HeaderTemplate>
                                >Notes
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T4lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T4Panel1" runat="server" CssClass="panel-cover">
                                    <%#Eval("NOTES_TEXT")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T4CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T4lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T4lblview"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T4Panel1"
                                    TextLabelID="T4lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Author">
                            <HeaderTemplate>
                                Author
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center><%#Eval("empname")%></center>
                            </ItemTemplate>
                            <ItemStyle />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:RequiredFieldValidator ID="T4RequiredFieldValidator6" runat="server" ErrorMessage="Please Select a Inclusion Member (Entered By)"
        ControlToValidate="T4ddStaffNotes" Display="None" InitialValue="-1" SetFocusOnError="True"
        ValidationGroup="T4Notes"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="T4RequiredFieldValidator7" runat="server" ErrorMessage="Please Enter Notes"
        ControlToValidate="T4Notes" Display="None" SetFocusOnError="True" ValidationGroup="T4Notes"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="T4Notes" />
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg">Screened by ACe</td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="T4CheckBoxScreenedbyace" runat="server" Text="Screened by ACe" />
                <asp:Panel ID="T4PanelScreening" runat="server" CssClass="panel-cover">
                    <table width="100%">
                        <tr>
                            <td width="20%"><span class="field-label">Date</span></td>
                            <td width="30%">
                                <asp:TextBox ID="t4txtscreeningdate" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                            <td width="20%"><span class="field-label">Age at time of testing  </span>
                            </td>
                            <td width="30%">
                                <asp:Label ID="lblagetesting" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td width="20%"><span class="field-label">Type </span></td>

                            <td width="30%">
                                <asp:TextBox ID="T4txtscreeningType" runat="server"></asp:TextBox></td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="T4btnscreeningsave" runat="server" CssClass="button" Text="Save" CausesValidation="False" /></td>

                        </tr>
                    </table>
                </asp:Panel>

            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg">Results</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td width="20%"><span class="field-label">Age at time of testing </span></td>
                        <td width="30%">
                            <asp:TextBox ID="T4txtAge" runat="server"></asp:TextBox>
                        </td>

                    </tr>
                    <tr class="title-bg">
                        <td colspan="4">Beery Visual Perception</td>
                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">Word ID</span></td>
                        <td width="30%">
                            <asp:TextBox ID="T4txtWordID" runat="server"></asp:TextBox></td>

                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">Word Attack</span></td>
                        <td width="30%">
                            <asp:TextBox ID="T4txtWordAttack" runat="server"></asp:TextBox></td>
                        <td width="20%"><span class="field-label">Digit Memory</span></td>
                        <td width="30%">
                            <asp:TextBox ID="T4txtDigitMemory" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">Beery Motor Development</span></td>

                        <td width="30%">
                            <asp:TextBox ID="T4txtBMD" runat="server"></asp:TextBox></td>
                        <td width="20%"><span class="field-label">Nonword Repetition</span></td>

                        <td width="30%">
                            <asp:TextBox ID="T4txtNonwordRepetition" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">DI Index</span></td>
                        <td width="30%">
                            <asp:DropDownList ID="t4ddDIIndex" runat="server">
                                <asp:ListItem Value="0" Text="Index"></asp:ListItem>
                                <asp:ListItem Value="A" Text="A"></asp:ListItem>
                                <asp:ListItem Value="B" Text="B"></asp:ListItem>
                                <asp:ListItem Value="C" Text="C"></asp:ListItem>
                                <asp:ListItem Value="D" Text="D"></asp:ListItem>
                                <asp:ListItem Value="E" Text="E"></asp:ListItem>
                                <asp:ListItem Value="F" Text="F"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td width="20%"><span class="field-label">NNAT</span></td>
                        <td width="30%">
                            <asp:TextBox ID="T4txtNNAT"  runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">IQ Estimate</span></td>
                        <td width="30%">
                            <asp:TextBox ID="t4txtIQEstimate" runat="server"></asp:TextBox></td>
                        <td width="20%"><span class="field-label">Spelling</span></td>
                        <td width="30%">
                            <asp:TextBox ID="T4txtSpelling" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">Expected Attainment Score</span></td>
                        <td width="30%">
                            <asp:TextBox ID="t4txtexpattscore" runat="server"></asp:TextBox></td>
                        <td width="20%"><span class="field-label">Number</span></td>
                        <td width="30%">
                            <asp:TextBox ID="T4txtNumber" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><span class="field-label">Other</span>
                        </td>                        
                        <td>
                            <asp:TextBox ID="t4txtother" runat="server" EnableTheming="false" TextMode="MultiLine"></asp:TextBox></td>
                        <td><span class="field-label">Date</span></td>
                        <td>
                            <asp:TextBox ID="t4txtRdate" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">Screening Results Report (File Location / Directory) </span> 
                            <asp:LinkButton ID="T4lnkSRReportinfo" runat="server" Visible="False">Path</asp:LinkButton></td>
                         <td width="30%">
                            <asp:TextBox ID="T4txtSRReportinfo" runat="server" ></asp:TextBox></td>
                        <td width="20%" ><span class="field-label">Screening Results (File Location / Directory) </span>
                            <asp:LinkButton ID="T4lnkSRResultsinfo" runat="server" Visible="False">Path</asp:LinkButton></td>
                        <td width="30%" >
                            <asp:TextBox ID="T4txtSRResultsinfo" runat="server" ></asp:TextBox></td>
                    </tr>                                    
                    <tr>
                        <td width="20%"><span class="field-label">Screening Feedback/Parent Meeting (File Location / Directory)</span>
                            <asp:LinkButton ID="T4lnkSRfeedbackinfo" runat="server" Visible="False">Path</asp:LinkButton></td>
                        <td width="30%">
                            <asp:TextBox ID="T4txtSRfeedbackinfo" runat="server" ></asp:TextBox></td>
                        <td colspan="2"></td>
                    </tr>                  
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="T4btnResultsSave" CssClass="button" runat="server" Text="Save" CausesValidation="False" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg">Recommendation<img id="Alert" alt="Alert" src="../../Images/info.gif" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td width="20%"><span class="field-label">Recommended Action</span></td>                        
                        <td width="30%">
                            <asp:DropDownList ID="T4ddRecom" runat="server">
                            </asp:DropDownList></td>
                        <td width="20%"><span class="field-label">Date Recommended</span></td>                        
                        <td width="30%">
                            <asp:TextBox ID="T4txtdateRecom" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                            <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                    </tr>
                   
                    <tr>
                        <td width="20%"><span class="field-label">Date Done</span></td>                       
                        <td width="30%">
                            <asp:TextBox ID="T4txtdateRecomDone" onfocus="javascript:this.blur();return false;" runat="server"></asp:TextBox>
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td width="20%"><span class="field-label">Results</span></td>                       
                        <td width="30%">
                            <asp:TextBox ID="T4txtdateRecomResults" runat="server"></asp:TextBox></td>
                    </tr>                   
                    <tr>
                        <td width="20%" ><span class="field-label">Refer to minutes of meeting (File Location / Directory) </span>
                            <asp:LinkButton ID="T4lnkRecomPath" runat="server" Visible="False">Path</asp:LinkButton></td>
                        <td width="30%">
                            <asp:TextBox ID="T4txtRecomFileInfo" runat="server"></asp:TextBox></td>
                        <td colspan="2"></td>
                    </tr>                    
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="T4btnSaveRecom" runat="server" CausesValidation="false" CssClass="button" Text="Save" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table id="TABLE1"
        width="100%">
        <tr>
            <td class="title-bg">Causes for Concern (File Location / Directory)</td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="T4txtConsernInfo" runat="server" ></asp:TextBox>
                <asp:Button ID="T4btnConsernInfoSave" runat="server" CssClass="button" Text="Save" />
                <asp:LinkButton ID="t4lnkconcerninfo" runat="server" Visible="False">Path</asp:LinkButton></td>
        </tr>
    </table>
    <br />
    <asp:LinkButton ID="T4knkstuintstr" runat="server" OnClientClick="javascript:return false;">Enter Student Interests/Strengths</asp:LinkButton>
    <br />
    <ajaxToolkit:CollapsiblePanelExtender ID="T4CollapsiblePanelExtender2" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T4knkstuintstr" Collapsed="false"
        CollapsedSize="0" CollapsedText="Enter Student Interests/Strengths" ExpandControlID="T4knkstuintstr"
        ExpandedText="Hide-Student Interests/Strengths" ScrollContents="false" TargetControlID="T4Panel2"
        TextLabelID="T4knkstuintstr">
    </ajaxToolkit:CollapsiblePanelExtender>
    <br />
    <asp:Panel ID="T4Panel2" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg">Enter Student Interests/Strengths</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="T4txtcommentstrength" runat="server" 
                        TextMode="MultiLine" ValidationGroup="T4Strength" ></asp:TextBox></td>                
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="T4btnstrsave" runat="server" CssClass="button" Text="Save" ValidationGroup="T4Strength" /></td>
            </tr>                     
        </table>
    </asp:Panel>
    <table width="100%">
        <tr>
            <td class="title-bg">Student Interests/Strengths</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T4GrdStuStrength" EmptyDataText="No comments added yet" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                    runat="server" Width="100%" AllowPaging="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderTemplate>
                             Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                            </ItemTemplate>
                            <ItemStyle />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comments">
                            <HeaderTemplate>
                                Comments
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T4lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T4Panel1" runat="server" CssClass="panel-cover">
                                    <%#Eval("COMMENTS_TEXT")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T4CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T4lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T4lblview"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T4Panel1"
                                    TextLabelID="T4lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem"  Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:RequiredFieldValidator ID="T4RequiredFieldValidator8" runat="server" ControlToValidate="T4txtcommentstrength"
        Display="None" ErrorMessage="Please Enter Comments (Student Interest/Strength)"
        SetFocusOnError="True" ValidationGroup="T4Strength"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="T4Strength" />
    <br />
    <asp:LinkButton ID="T4lnkstudelp" runat="server" OnClientClick="javascript:return false;">Enter Students Areas for Development</asp:LinkButton>
    <br />
    <ajaxToolkit:CollapsiblePanelExtender ID="T4CollapsiblePanelExtender3" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T4lnkstudelp" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter Students Areas for Development" ExpandControlID="T4lnkstudelp"
        ExpandedText="Hide-Students Areas for Development" ScrollContents="false" TargetControlID="T4Panel3"
        TextLabelID="T4lnkstudelp">
    </ajaxToolkit:CollapsiblePanelExtender>
    <br />
    <asp:Panel ID="T4Panel3" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg">Enter Students Areas for Development</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="T4txtstudelp" runat="server"  TextMode="MultiLine"
                        ValidationGroup="T4Development" ></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="T4btnStuDelpSave" CssClass="button" runat="server" Text="Save" ValidationGroup="T4Development" /></td>
            </tr>
        </table>
    </asp:Panel>
    <table width="100%">
        <tr>
            <td class="title-bg">Students Areas for Development</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T4Grdstudelp" EmptyDataText="No comments added yet" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                    runat="server" Width="100%" AllowPaging="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderTemplate>
                                Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                            </ItemTemplate>
                            <ItemStyle  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comments">
                            <HeaderTemplate>
                               Comments
                            </HeaderTemplate>

                            <ItemTemplate>
                                <asp:Label ID="T4lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T4Panel1" runat="server" CssClass="panel-cover">
                                    <%#Eval("COMMENTS_TEXT")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T4CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T4lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T4lblview"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T4Panel1"
                                    TextLabelID="T4lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem"  Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:RequiredFieldValidator ID="T4RequiredFieldValidator9" runat="server" ControlToValidate="T4txtstudelp"
        Display="None" ErrorMessage="Please Enter Comments (Student Area for Development)"
        SetFocusOnError="True" ValidationGroup="T4Development"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary4" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="T4Development" />
    <br />
    <br />
    <asp:HiddenField ID="Hiddenstuid" runat="server" />
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:HiddenField ID="T4hidSRReportinfo" runat="server" />
    <asp:HiddenField ID="T4hidSRResultsinfo" runat="server" />
    <asp:HiddenField ID="T4hidSRfeedbackinfo" runat="server" />
    <asp:HiddenField ID="T4hidRecomPath" runat="server" />
    <asp:HiddenField ID="t4hidconcerninfo" runat="server" />
    <ajaxToolkit:CalendarExtender ID="T4CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
        TargetControlID="T4txtrefferdate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T4CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image2"
        TargetControlID="t4txtRdate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T4CE3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image3"
        TargetControlID="T4txtdateRecom">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T4CE4" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image4"
        TargetControlID="T4txtdateRecomDone">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="T4CE5" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image5"
        TargetControlID="t4txtscreeningdate">
    </ajaxToolkit:CalendarExtender>
    <%-- <asp:RequiredFieldValidator ID="T4RequiredFieldValidator1" runat="server" ErrorMessage="Please Select Inclusion Member(Referenced by)"
        ControlToValidate="T4ddrefferedby" Display="None" InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="T4RequiredFieldValidator2" runat="server" ErrorMessage="Please Enter Inclusion Member(Referenced by)"
        ControlToValidate="T4txtrefferedby" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="T4RequiredFieldValidator3" runat="server" ErrorMessage="Please Enter Referral Date"
        ControlToValidate="T4txtrefferdate" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="T4RequiredFieldValidator4" runat="server" ErrorMessage="Please Select Inclusion Member (Enter By)"
        ControlToValidate="T4ddStaffs" Display="None" InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="T4RequiredFieldValidator5" runat="server" ErrorMessage="Please Enter Referral Summary"
        ControlToValidate="T4txtReffersummary" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>&nbsp;<asp:ValidationSummary
            ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
    --%>
</div>
