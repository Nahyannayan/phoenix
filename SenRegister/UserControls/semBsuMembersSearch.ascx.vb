Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_semBsuMembersSearch
    Inherits System.Web.UI.UserControl

    ' Delegate declaration 
    Public Delegate Sub OnButtonClick(ByVal ds As DataSet)


    ' Event declaration 
    Public Event btnHandler As OnButtonClick


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindDesignation()
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub BindDesignation()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select des_id , des_descr from EMPDESIGNATION_M"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dddesignation.DataSource = ds
        dddesignation.DataValueField = "des_id"
        dddesignation.DataTextField = "des_descr"
        dddesignation.DataBind()
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Select a Designation"
        dddesignation.Items.Insert(0, list)
    End Sub
    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(5) As SqlClient.SqlParameter
        If txtempid.Text.Trim() <> "" Then
            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", txtempid.Text.Trim())
        End If
        If txtempno.Text.Trim() <> "" Then
            pParms(1) = New SqlClient.SqlParameter("@EMPNO", txtempno.Text.Trim())
        End If
        If txtempname.Text.Trim() <> "" Then
            pParms(2) = New SqlClient.SqlParameter("@EMP_NAME", txtempname.Text.Trim().Replace(" ", ""))
        End If
        If dddesignation.SelectedIndex > 0 Then
            pParms(3) = New SqlClient.SqlParameter("@EMP_DES_ID", dddesignation.SelectedValue)
        End If

        pParms(4) = New SqlClient.SqlParameter("@EMP_BSU_ID", Hiddenbsuid.Value)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SEARCH_SEN_BSU_MEMBERS", pParms)

        RaiseEvent btnHandler(ds)
    End Sub

End Class
