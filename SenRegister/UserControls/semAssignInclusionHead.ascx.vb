Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_semAssignInclusionHead
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            BindDepartments()
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''

        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindDepartments()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim str_query = "select * from SEN_DEPARTMENTS"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dddepartmenthodselect.DataSource = ds
        dddepartmenthodselect.DataValueField = "DEPT_ID"
        dddepartmenthodselect.DataTextField = "DEPT_DESC"
        dddepartmenthodselect.DataBind()
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Select a Section"
        dddepartmenthodselect.Items.Insert(0, list)


    End Sub

    Public Sub BindSelectHod()
        lblmessagehod.Text = ""
        RadioButtonListSelectHod.Items.Clear()
        btnhodsave.Visible = False
        If dddepartmenthodselect.SelectedIndex > 0 Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim str_query = "select m.emp_id,(m.emp_salute +' '+m.emp_fname +' '+m.emp_mname+' ' +m.emp_lname) as empname from SEN_DEPARTMENT_MEMBERS c " & _
                            " inner join oasis.dbo.employee_m m on c.emp_id=m.emp_id " & _
                            " where c.emp_dept_id='" & dddepartmenthodselect.SelectedValue & "' and c.emp_bsu_id='" & Hiddenbsuid.Value & "'"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count > 0 Then
                btnhodsave.Visible = True
                RadioButtonListSelectHod.DataSource = ds
                RadioButtonListSelectHod.DataTextField = "empname"
                RadioButtonListSelectHod.DataValueField = "emp_id"
                RadioButtonListSelectHod.DataBind()
                lnkdelete.Visible = False
                lnkchange.Visible = False
                str_query = "SELECT EMP_ID FROM SEN_DEPARTMENT_HOD WHERE EMP_BSU_ID='" & Hiddenbsuid.Value & "' AND EMP_DEPT_ID='" & dddepartmenthodselect.SelectedValue & "'"
                Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
                If val Is Nothing Then
                    RadioButtonListSelectHod.Enabled = True
                    btnhodsave.Visible = True
                Else
                    RadioButtonListSelectHod.SelectedValue = val
                    RadioButtonListSelectHod.Enabled = False
                    btnhodsave.Visible = False
                    lnkdelete.Visible = True
                    lnkchange.Visible = True
                    lblmessagehod.Text = dddepartmenthodselect.SelectedItem.Text & " Section Head assigned to :" & RadioButtonListSelectHod.SelectedItem.Text
                End If
            Else
                lblmessagehod.Text = "Members are not added to " & dddepartmenthodselect.SelectedItem.Text & " section.Please add members and select a Head of Section"
            End If
        End If
    End Sub

    Protected Sub dddepartmenthodselect_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddepartmenthodselect.SelectedIndexChanged
        BindSelectHod()
    End Sub

    Protected Sub btnhodsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnhodsave.Click
        Dim flag = 0
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        If dddepartmenthodselect.SelectedIndex > 0 Then
            For Each item As ListItem In RadioButtonListSelectHod.Items
                If item.Selected Then
                    flag = 1
                    Dim pParms(3) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@EMP_ID", item.Value)
                    pParms(1) = New SqlClient.SqlParameter("@EMP_BSU_ID", Hiddenbsuid.Value)
                    pParms(2) = New SqlClient.SqlParameter("@EMP_DEPT_ID", dddepartmenthodselect.SelectedValue)
                    lblmessagehod.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_HOD", pParms)
                End If
            Next
            If flag = 0 Then
                lblmessagehod.Text = "Please select a Section Head"
            Else
                Session("MemberTabIndex") = 2
                Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()

                Response.Redirect("senAssignInclusionMembers.aspx" & mInfo)
            End If
            'MainGridView()
            'BindSelectHod()
        End If

    End Sub

    Protected Sub lnkchange_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnhodsave.Visible = False
        btnhodchange.Visible = True
        btnhodcancel.Visible = True
        RadioButtonListSelectHod.Enabled = True
        dddepartmenthodselect.Enabled = False
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnhodcancel.Visible = False
        btnhodchange.Visible = False
        dddepartmenthodselect.Enabled = True
        BindSelectHod()
    End Sub

    Protected Sub lnkdelete_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim str_query = "Delete from SEN_DEPARTMENT_HOD where  EMP_DEPT_ID='" & dddepartmenthodselect.SelectedValue & "' and EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        lblmessagehod.Text = "Head of selected section has been Deleted"
        'MainGridView()
        'BindSelectHod()
        Session("MemberTabIndex") = 2
        Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()

        Response.Redirect("senAssignInclusionMembers.aspx" & mInfo)
    End Sub

    Protected Sub btnhodchange_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim str_query = "Update SEN_DEPARTMENT_HOD set EMP_ID ='" & RadioButtonListSelectHod.SelectedValue & "' where  EMP_DEPT_ID='" & dddepartmenthodselect.SelectedValue & "' and EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        'MainGridView()
        'BindSelectHod()
        'btnhodcancel.Visible = False
        'btnhodchange.Visible = False
        Session("MemberTabIndex") = 2
        Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()

        Response.Redirect("senAssignInclusionMembers.aspx" & mInfo)
        ''lblmemberremovemessage.Text = "Please Select a Section for searching."
    End Sub


End Class
