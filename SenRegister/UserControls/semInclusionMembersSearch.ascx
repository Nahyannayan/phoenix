<%@ Control Language="VB" AutoEventWireup="false" CodeFile="semInclusionMembersSearch.ascx.vb" Inherits="SenRegister_UserControls_semInclusionMembersSearch" %>
<div class="matters" >
<asp:LinkButton ID="lnkSearchTab2" runat="server" OnClientClick="javascript:return false;">Search Members</asp:LinkButton><asp:Panel ID="Panel2" runat="server" Height="50px" Width="125px">
    <table>
        <tr>
            <td colspan="3">
                <asp:DropDownList ID="ddDepartments" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:TextBox ID="txtempidsearch" runat="server"></asp:TextBox></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtempnosearch" runat="server"></asp:TextBox></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtempnamesearch" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:DropDownList ID="dddesignationsearch" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td align="center" style="width: 100px">
                <asp:Button ID="btnremovesearch" CssClass="button" runat="server" Text="Search" /></td>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
</asp:Panel>
<ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
    CollapseControlID="lnkSearchTab2" CollapsedSize="0" CollapsedText="Search Members"
    Enabled="True" ExpandControlID="lnkSearchTab2" ExpandedSize="100" ExpandedText="Hide Search"
    TargetControlID="Panel2" TextLabelID="lnkSearchTab2">
</ajaxToolkit:CollapsiblePanelExtender>
<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
    TargetControlID="txtempidsearch" WatermarkCssClass="watermarked" WatermarkText="Employee ID">
</ajaxToolkit:TextBoxWatermarkExtender>
<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
    TargetControlID="txtempnosearch" WatermarkCssClass="watermarked" WatermarkText="Employee No">
</ajaxToolkit:TextBoxWatermarkExtender>
<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server"
    TargetControlID="txtempnamesearch" WatermarkCssClass="watermarked" WatermarkText="Employee Name">
</ajaxToolkit:TextBoxWatermarkExtender>
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
</div>