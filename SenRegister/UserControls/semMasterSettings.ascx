<%@ Control Language="VB" AutoEventWireup="false" CodeFile="semMasterSettings.ascx.vb" Inherits="SenRegister_UserControls_semMasterSettings" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>


<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<style>
    .ajax__tab_xp .ajax__tab_header {
            font-family: inherit !important;
            font-size: inherit !important;
            background-image: none !important;
            
        }
        .ajax__tab_xp .ajax__tab_body {
            font-family :inherit !important;
            border: 1px solid rgba(0, 0, 0, 0.2) !important;
        }
          .ajax__tab_xp .ajax__tab_header .ajax__tab_tab {
                height: auto !important;
                padding: inherit !important;
                background-image: none !important;

                display: block;
       width: 100%;
       padding: 10px 20px !important;
       color: #495057;
       background-clip: padding-box;
       border: 1px solid rgba(0, 0, 0, 0.2) !important;
       border-top-left-radius : 0.75rem;
       border-top-right-radius: 0.75rem;
      
       border-bottom: none !important;
       background: rgb(193,194,194) !important;
       transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out !important;
       
       background: -moz-linear-gradient(left,  rgba(193,194,194,1) 0%, rgba(234,234,234,1) 100%) !important; /* FF3.6+ */
       background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(193,194,194,1)), color-stop(100%,rgba(234,234,234,1))) !important; /* Chrome,Safari4+ */
       background: -webkit-linear-gradient(left,  rgba(193,194,194,1) 0%,rgba(234,234,234,1) 100%) !important; /* Chrome10+,Safari5.1+ */
       background: -o-linear-gradient(left,  rgba(193,194,194,1) 0%,rgba(234,234,234,1) 100%) !important; /* Opera 11.10+ */
       background: -ms-linear-gradient(left,  rgba(193,194,194,1) 0%,rgba(234,234,234,1) 100%) !important; /* IE10+ */
       background: linear-gradient(to right,  rgba(193,194,194,1) 0%,rgba(234,234,234,1) 100%) !important; /* W3C */
       filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c1c2c2', endColorstr='#eaeaea',GradientType=1 ) !important; /* IE6-9 */
                }

          .ajax__tab_xp .ajax__tab_header .ajax__tab_tab:hover {
                height: auto !important;
                padding: inherit !important;
                background-image: none !important;

                display: block;
       width: 100%;
       padding: 10px 20px !important;
       background-clip: padding-box;
       border: 1px solid rgba(0, 0, 0, 0.2) !important;
       border-top-left-radius : 0.75rem;
       border-top-right-radius: 0.75rem;
      
       border-bottom: none !important;
       color: #ffffff;
       background: rgb(193,194,194) !important;
       background: -moz-linear-gradient(left,  rgba(234,234,234,1) 0%, rgba(193,194,194,1) 100%) !important; /* FF3.6+ */
       background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(234,234,234,1)), color-stop(100%,rgba(193,194,194,1))) !important; /* Chrome,Safari4+ */
       background: -webkit-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* Chrome10+,Safari5.1+ */
       background: -o-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* Opera 11.10+ */
       background: -ms-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* IE10+ */
       background: linear-gradient(to right,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* W3C */
       filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c1c2c2', endColorstr='#eaeaea',GradientType=1 ) !important; /* IE6-9 */
        }

        .ajax__tab_default .ajax__tab_tab {
            overflow: inherit !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_outer {
            padding-right: inherit !important;
            background-image: none !important;
            height: inherit !important;
            margin-right: 6px;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_active .ajax__tab_outer {
            background-image: none !important;
            margin-right: 6px;
            background-color: #ffffff !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_active .ajax__tab_inner {
            background-image: none !important;
           
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_inner {
            background-image: none !important;
            padding-left:0px !important;
        }

       
            .ajax__tab_tab:focus {
                outline: 0px;
            }
        .ajax__tab_active {
            background-color: #ffffff !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_active .ajax__tab_tab {
            background-image: none !important;

            display: block;
            width: 100%;
            padding: 10px 20px !important;
            color: #495057;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, 0.2) !important;
            border-top-left-radius : 0.75rem;
            border-top-right-radius: 0.75rem;
            
            border-bottom: none !important;
            background: rgb(193,194,194) !important;
       background: -moz-linear-gradient(left,  rgba(234,234,234,0.3) 0%, rgba(193,194,194,0.3) 100%) !important; /* FF3.6+ */
       background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(234,234,234,0.3)), color-stop(100%,rgba(193,194,194,0.3))) !important; /* Chrome,Safari4+ */
       background: -webkit-linear-gradient(left,  rgba(234,234,234,0.3) 0%,rgba(193,194,194,0.3) 100%) !important; /* Chrome10+,Safari5.1+ */
       background: -o-linear-gradient(left,  rgba(234,234,234,0.3) 0%,rgba(193,194,194,0.3) 100%) !important; /* Opera 11.10+ */
       background: -ms-linear-gradient(left,  rgba(234,234,234,0.3) 0%,rgba(193,194,194,0.3) 100%) !important; /* IE10+ */
       background: linear-gradient(to right,  rgba(234,234,234,0.3) 0%,rgba(193,194,194,0.3) 100%) !important; /* W3C */
       filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c1c2c2', endColorstr='#eaeaea',GradientType=1 ) !important; /* IE6-9 */
            
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_active .ajax__tab_tab:hover {
            background-image: none !important;

            display: block;
            width: 100%;
            padding: 10px 20px !important;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, 0.2) !important;
            border-top-left-radius : 0.75rem;
            border-top-right-radius: 0.75rem;
            border-bottom: none !important;

         color: #ffffff;
       background: rgb(193,194,194) !important;
       background: -moz-linear-gradient(left,  rgba(234,234,234,1) 0%, rgba(193,194,194,1) 100%) !important; /* FF3.6+ */
       background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(234,234,234,1)), color-stop(100%,rgba(193,194,194,1))) !important; /* Chrome,Safari4+ */
       background: -webkit-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* Chrome10+,Safari5.1+ */
       background: -o-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* Opera 11.10+ */
       background: -ms-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* IE10+ */
       background: linear-gradient(to right,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%) !important; /* W3C */
       filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c1c2c2', endColorstr='#eaeaea',GradientType=1 ) !important; /* IE6-9 */
         }
        
        .ajax__tab_tab:focus {
            border:0px solid transparent !important;
        }
</style>

&nbsp;<div align="left" class="matters">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>



            <asp:Label ID="lblmessage" runat="server"></asp:Label>
            <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">

                <ajaxToolkit:TabPanel ID="T3" runat="server">
                    <ContentTemplate>
                    </ContentTemplate>
                    <HeaderTemplate>
                        <span>Access</span>
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>



                <ajaxToolkit:TabPanel ID="T4" runat="server">
                    <ContentTemplate>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="title-bg-lite">Recommendation Actions Settings</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GridActions" Width="100%" runat="server" CssClass="table table-bordered table-row">
                                        <RowStyle CssClass="griditem" />
                                        <EmptyDataRowStyle />
                                        <SelectedRowStyle />
                                        <HeaderStyle />
                                        <EditRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtactions" runat="server"></asp:TextBox>
                                    <asp:Button ID="btnactionssave" CssClass="button" runat="server" Text="Save" />
                                </td>
                            </tr>
                        </table>

                    </ContentTemplate>
                    <HeaderTemplate>
                        <span>ACe</span>
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>


                <ajaxToolkit:TabPanel ID="T5" runat="server">
                    <ContentTemplate>
                    </ContentTemplate>
                    <HeaderTemplate>
                        <span>DU</span>
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>


                <ajaxToolkit:TabPanel ID="T6" runat="server">
                    <ContentTemplate>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="title-bg-lite">Language Settings</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GridLanguage" Width="100%" runat="server" CssClass="table table-bordered table-row">
                                        <RowStyle CssClass="griditem" />
                                        <EmptyDataRowStyle />
                                        <SelectedRowStyle />
                                        <HeaderStyle />
                                        <EditRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtlanguage" runat="server"></asp:TextBox>
                                    <asp:Button ID="btnlanguagesave" CssClass="button" runat="server" Text="Save" />
                                </td>
                            </tr>
                        </table>


                    </ContentTemplate>
                    <HeaderTemplate>
                        <span>ELL</span>
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>


                <ajaxToolkit:TabPanel ID="T7" runat="server">
                    <ContentTemplate>
                    </ContentTemplate>
                    <HeaderTemplate>
                        <span>Wellbeing</span>
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>


                <ajaxToolkit:TabPanel ID="T8" runat="server">
                    <ContentTemplate>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="title-bg-lite">Interventions Settings</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GridInterventions" Width="100%" runat="server" CssClass="table table-bordered table-row">
                                        <RowStyle CssClass="griditem" />
                                        <EmptyDataRowStyle />
                                        <SelectedRowStyle />
                                        <HeaderStyle />
                                        <EditRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtinterventions" runat="server"></asp:TextBox>
                                    <asp:Button ID="btnintersave" CssClass="button" runat="server" Text="Save" />
                                </td>
                            </tr>
                        </table>

                    </ContentTemplate>
                    <HeaderTemplate>
                        <span>Inclusion Tracker</span>
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>



                <ajaxToolkit:TabPanel ID="T9" runat="server">
                    <ContentTemplate>
                    </ContentTemplate>
                    <HeaderTemplate>
                        <span>LSA</span>
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>


                <ajaxToolkit:TabPanel ID="T10" runat="server">
                    <ContentTemplate>
                    </ContentTemplate>
                    <HeaderTemplate>
                        <span>MAGT</span>
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>

                <ajaxToolkit:TabPanel ID="T11" runat="server">
                    <ContentTemplate>
                    </ContentTemplate>
                    <HeaderTemplate>
                        <span>Enrich</span>
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>

                <ajaxToolkit:TabPanel ID="T12" runat="server">
                    <ContentTemplate>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="title-bg-lite">Specific Learning Needs Settings</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GridSLnS" Width="100%" runat="server" CssClass="table table-bordered table-row">
                                        <RowStyle CssClass="griditem" />
                                        <EmptyDataRowStyle />
                                        <SelectedRowStyle />
                                        <HeaderStyle />
                                        <EditRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtslns" runat="server"></asp:TextBox>
                                    <asp:Button ID="btnslnssave" CssClass="button" runat="server" Text="Save" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <HeaderTemplate>
                        <span>Med Info</span>
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>


                <ajaxToolkit:TabPanel ID="T13" runat="server">
                    <ContentTemplate>

                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="title-bg-lite">Specialists Outside Agencies Settings</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="Gridsoas" Width="100%" runat="server" CssClass="table table-bordered table-row">
                                        <RowStyle CssClass="griditem" />
                                        <EmptyDataRowStyle />
                                        <SelectedRowStyle />
                                        <HeaderStyle />
                                        <EditRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtsoas" runat="server"></asp:TextBox>
                                    <asp:Button ID="btnsoassave" CssClass="button" runat="server" Text="Save" />
                                </td>
                            </tr>
                        </table>

                    </ContentTemplate>
                    <HeaderTemplate>
                        <span>Sp/O Agencies</span>
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>


                <ajaxToolkit:TabPanel ID="T14" runat="server">
                    <ContentTemplate>
                    </ContentTemplate>
                    <HeaderTemplate>
                        <span>T Input</span>
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>


                <ajaxToolkit:TabPanel ID="T15" runat="server">
                    <ContentTemplate>
                    </ContentTemplate>
                    <HeaderTemplate>
                        <span>IEP</span>
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>
            <asp:HiddenField ID="Hiddenbsuid" runat="server" />


        </ContentTemplate>
    </asp:UpdatePanel>



</div>
