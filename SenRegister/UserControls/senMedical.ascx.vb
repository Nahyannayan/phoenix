Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senMedical
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindGrid()
            BindNotes()
            BindAuthors()
            SLNMaster()
            BindSpecificLearningNeeds()
            BindConfi()
            LockFileCheck()

        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub SLNMaster()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim ds As DataSet
        Dim strQuery As String = "select NEED_ID, NEED_DES FROM SEN_DEPARTMENT_MEDICAL_SPECIAL_LEARNING_MASTER WHERE NEED_BSU_ID='" & Hiddenbsuid.Value & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T12CheckSPL.DataSource = ds
        T12CheckSPL.DataTextField = "NEED_DES"
        T12CheckSPL.DataValueField = "NEED_ID"
        T12CheckSPL.DataBind()


    End Sub



    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then

            T12CheckConfi.Enabled = False
            T12btndave.Enabled = False
            T12btnallergysave.Enabled = False
            T12btnmedicationsave.Enabled = False
            T12btnothersave.Enabled = False
            T12btnnotessave.Enabled = False
        End If

    End Sub
    Public Sub BindNotes()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim ds As DataSet
        Dim strQuery As String = "select " & _
                                 " case type when '0' then convert(bit,'True') else convert(bit,'False') end as medical , " & _
                                 " case type when '1' then convert(bit,'True') else convert(bit,'False') end as Emotional , " & _
                                 " (select count(TYPE) from SEN_DEPARTMENT_MEDICAL_NOTES WHERE  type='0' and  STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' )as MCount, " & _
                                 " (select count(TYPE) from SEN_DEPARTMENT_MEDICAL_NOTES WHERE  type='1' and  STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' )as ECount, " & _
                                 " (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(COMMENTS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, COMMENTS,ENTRY_DATE from SEN_DEPARTMENT_MEDICAL_NOTES a" & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.entry_emp_id=b.emp_id " & _
                                 "  where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "' order by a.entry_date desc"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T12GrdNotes.DataSource = ds
        T12GrdNotes.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            Dim Mcount As Label = DirectCast(T12GrdNotes.FooterRow.FindControl("T12lblMtotal"), Label)
            Dim Ecount As Label = DirectCast(T12GrdNotes.FooterRow.FindControl("T12lblEtotal"), Label)
            Dim Tcount As Label = DirectCast(T12GrdNotes.FooterRow.FindControl("lbltotal"), Label)
            Mcount.Text = "M:" & ds.Tables(0).Rows(0).Item("MCount").ToString()
            Ecount.Text = "E:" & ds.Tables(0).Rows(0).Item("ECount").ToString()
            Tcount.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("MCount").ToString()) + Convert.ToInt32(ds.Tables(0).Rows(0).Item("ECount").ToString())
        End If
    End Sub
    Public Sub BindGrid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString

        ''Alergy
        Dim strQuery As String = "select (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(RESULT_OF_EXPOSURE,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview1,(substring(TREATMENT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview2, ALLERGY,RESULT_OF_EXPOSURE,TREATMENT,ENTRY_DATE from SEN_DEPARTMENT_MEDICAL_DETAILS_ALLERGIES a" & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.entry_emp_id=b.emp_id " & _
                                 "  where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "'"
        T12GridView1.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T12GridView1.DataBind()

        ''Medication
        strQuery = "select (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(CONDITION_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview1,(substring(ADMINISTERED_AT_SCHOOL,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview2, MEDICATION_NAME,CONDITION_TEXT,ADMINISTERED_AT_SCHOOL,ENTRY_DATE from SEN_DEPARTMENT_MEDICAL_DETAILS_MEDICATION a " & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.entry_emp_id=b.emp_id " & _
                                 "  where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "'"
        T12GridView2.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T12GridView2.DataBind()
        Dim i = 0
        For i = 3 To 7
            Dim ds As DataSet
            strQuery = "select (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(COMMENTS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview,COMMENTS,ENTRY_DATE from SEN_DEPARTMENT_MEDICAL_DETAILS_OTHERS a " & _
                       " inner join oasis.dbo.EMPLOYEE_M b on a.entry_emp_id=b.emp_id " & _
                       " WHERE A.OTH_ID='" & i & "' and a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
            If i = 3 Then
                T12GridView3.DataSource = ds
                T12GridView3.DataBind()
            ElseIf i = 4 Then
                T12GridView4.DataSource = ds
                T12GridView4.DataBind()
            ElseIf i = 5 Then
                T12GridView5.DataSource = ds
                T12GridView5.DataBind()
            ElseIf i = 6 Then
                T12GridView6.DataSource = ds
                T12GridView6.DataBind()
            ElseIf i = 7 Then
                T12GridView7.DataSource = ds
                T12GridView7.DataBind()
            End If


        Next

    End Sub
    Public Sub BindAuthors()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id and a.EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        T12ddstaffs.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T12ddnotesstaff.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        T12ddstaffs.DataTextField = "empname"
        T12ddstaffs.DataValueField = "EMP_ID"
        T12ddnotesstaff.DataTextField = "empname"
        T12ddnotesstaff.DataValueField = "EMP_ID"
        T12ddstaffs.DataBind()
        T12ddnotesstaff.DataBind()
        Dim list As New ListItem
        list.Text = "Select an Inclusion Member"
        list.Value = "-1"
        T12ddstaffs.Items.Insert(0, list)
        T12ddnotesstaff.Items.Insert(0, list)
    End Sub
    Public Sub BindConfi()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select CONFIDENTIAL from SEN_DEPARTMENT_MEDICAL_MAIN  " & _
                                 "  where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim val As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)
        If val <> "" Then
            If val = "True" Then
                T12CheckConfi.Checked = True
            Else
                T12CheckConfi.Checked = False
            End If
        Else
            T12CheckConfi.Checked = False
        End If

    End Sub


    Public Sub BindSpecificLearningNeeds()
        'T12T1.Visible = False
        'T12T2.Visible = False
        'T12T3.Visible = False
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select SPECIFIC_NEED_ID from SEN_DEPARTMENT_MEDICAL_SPECIAL_LEARNING_NEEDS  " & _
                                 "  where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        Dim i = 0
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim id As String = ds.Tables(0).Rows(i).Item("SPECIFIC_NEED_ID").ToString()
            'Dim val As String = ds.Tables(0).Rows(i).Item("SPECIFIC_NEED_VALUE").ToString()
            For Each item As ListItem In T12CheckSPL.Items
                If item.Value = id Then
                    item.Selected = True
                    'If item.Value = "19" Then
                    '    'T12T1.Visible = True
                    '    T12txtphysicalimpairment.Text = val
                    'ElseIf item.Value = "20" Then
                    '    'T12T2.Visible = True
                    '    T12txtEpilepsy.Text = val
                    'ElseIf item.Value = "22" Then
                    '    'T12T3.Visible = True
                    '    T12txtother.Text = val
                    'End If
                End If
            Next


        Next

        strQuery = " select * from SEN_DEPARTMENT_MEDICAL_SPECIAL_LEARNING_NEEDS_OTHERS  " & _
                                 "  where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)


        If ds.Tables(0).Rows.Count > 0 Then
            T12txtphysicalimpairment.Text = ds.Tables(0).Rows(0).Item("PHYSICAL_IMPAIRMENT").ToString()
            T12txtEpilepsy.Text = ds.Tables(0).Rows(0).Item("EPILEPSY_TYPE").ToString()
            T12txtother.Text = ds.Tables(0).Rows(0).Item("OTHERS").ToString()
        End If




    End Sub
    'Protected Sub T12CheckSPL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12CheckSPL.SelectedIndexChanged
    '    T12T1.Visible = False
    '    T12T2.Visible = False
    '    T12T3.Visible = False
    '    For Each item As ListItem In T12CheckSPL.Items
    '        If item.Selected Then
    '            If item.Value = "19" Then
    '                T12T1.Visible = True
    '            ElseIf item.Value = "20" Then
    '                T12T2.Visible = True
    '            ElseIf item.Value = "22" Then
    '                T12T3.Visible = True
    '            End If
    '        End If
    '    Next
    'End Sub

    Protected Sub T12btndave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12btndave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            For Each item As ListItem In T12CheckSPL.Items

                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
                pParms(2) = New SqlClient.SqlParameter("@SPECIFIC_NEED_ID", item.Value)
                'If item.Value = "19" Then
                '    pParms(3) = New SqlClient.SqlParameter("@SPECIFIC_NEED_VALUE", T12txtphysicalimpairment.Text.Trim())
                'ElseIf item.Value = "20" Then
                '    pParms(3) = New SqlClient.SqlParameter("@SPECIFIC_NEED_VALUE", T12txtEpilepsy.Text.Trim())
                'ElseIf item.Value = "22" Then
                '    pParms(3) = New SqlClient.SqlParameter("@SPECIFIC_NEED_VALUE", T12txtother.Text.Trim())
                'End If
                pParms(4) = New SqlClient.SqlParameter("@SELECTED", item.Selected)
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_MEDICAL_SPECIAL_LEARNING_NEEDS", pParms)
            Next

            Dim pParms1(5) As SqlClient.SqlParameter
            pParms1(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms1(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms1(2) = New SqlClient.SqlParameter("@PHYSICAL_IMPAIRMENT", T12txtphysicalimpairment.Text.Trim())
            pParms1(3) = New SqlClient.SqlParameter("@EPILEPSY_TYPE", T12txtEpilepsy.Text.Trim())
            pParms1(4) = New SqlClient.SqlParameter("@OTHERS", T12txtother.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_MEDICAL_SPECIAL_LEARNING_NEEDS_OTHERS", pParms1)


            BindSpecificLearningNeeds()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try


    End Sub

    Protected Sub T12CheckConfi_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12CheckConfi.CheckedChanged
        Dim val = "False"
        If T12CheckConfi.Checked Then
            val = "True"
        End If
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@CONFIDENTIAL", val)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_MEDICAL_MAIN", pParms)
        BindConfi()
    End Sub


    Protected Sub T12ddIssue_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12ddIssue.SelectedIndexChanged
        T12TBD1.Visible = False
        T12TBD2.Visible = False
        T12TBD3.Visible = False
        If T12ddIssue.SelectedIndex > 0 Then
            T12MAIN.Visible = True
            T12lbltitle.Text = T12ddIssue.SelectedItem.Text
            If T12ddIssue.SelectedValue = "1" Then

                T12TBD1.Visible = True
            ElseIf T12ddIssue.SelectedValue = "2" Then
                T12TBD2.Visible = True
            Else
                T12TBD3.Visible = True
            End If
        Else
            T12MAIN.Visible = False
        End If
    End Sub

    Protected Sub T12btnallergysave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12btnallergysave.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@ALLERGY", T12txtallery.Text.Trim())
        pParms(3) = New SqlClient.SqlParameter("@RESULT_OF_EXPOSURE", T12txtresultofallergy.Text.Trim())
        pParms(4) = New SqlClient.SqlParameter("@TREATMENT", T12txtallertytreatment.Text.Trim())
        pParms(5) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T12ddstaffs.SelectedValue)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_MEDICAL_DETAILS_ALLERGIES", pParms)
        BindGrid()
    End Sub

    Protected Sub T12btnmedicationsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12btnmedicationsave.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@MEDICATION_NAME", T12txtmedicationname.Text.Trim())
        pParms(3) = New SqlClient.SqlParameter("@CONDITION_TEXT", T12txtCondition.Text.Trim())
        pParms(4) = New SqlClient.SqlParameter("@ADMINISTERED_AT_SCHOOL", T12txtadministeredatschool.Text.Trim())
        pParms(5) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T12ddstaffs.SelectedValue)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_MEDICAL_DETAILS_MEDICATION", pParms)
        BindGrid()
    End Sub


    Protected Sub T12btnothersave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12btnothersave.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@OTH_ID", T12ddIssue.SelectedValue)
        pParms(3) = New SqlClient.SqlParameter("@COMMENTS", T12txtcomments.Text.Trim())
        pParms(4) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T12ddstaffs.SelectedValue)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_MEDICAL_DETAILS_OTHERS", pParms)
        BindGrid()
    End Sub

    'Protected Sub T12CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12CheckBox1.CheckedChanged
    '    If T12CheckBox1.Checked Then
    '        T12G1.Visible = True
    '    Else
    '        T12G1.Visible = False
    '    End If
    'End Sub

    'Protected Sub T12CheckBox2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12CheckBox2.CheckedChanged
    '    If T12CheckBox2.Checked Then
    '        T12G2.Visible = True
    '    Else
    '        T12G2.Visible = False
    '    End If
    'End Sub

    'Protected Sub T12CheckBox3_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12CheckBox3.CheckedChanged
    '    If T12CheckBox3.Checked Then
    '        T12G3.Visible = True
    '    Else
    '        T12G3.Visible = False
    '    End If
    'End Sub

    'Protected Sub T12CheckBox4_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12CheckBox4.CheckedChanged
    '    If T12CheckBox4.Checked Then
    '        T12G4.Visible = True
    '    Else
    '        T12G4.Visible = False
    '    End If
    'End Sub

    'Protected Sub T12CheckBox5_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12CheckBox5.CheckedChanged
    '    If T12CheckBox5.Checked Then
    '        T12G5.Visible = True
    '    Else
    '        T12G5.Visible = False
    '    End If
    'End Sub

    'Protected Sub T12CheckBox6_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12CheckBox6.CheckedChanged
    '    If T12CheckBox6.Checked Then
    '        T12G6.Visible = True
    '    Else
    '        T12G6.Visible = False
    '    End If
    'End Sub

    'Protected Sub T12CheckBox7_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12CheckBox7.CheckedChanged
    '    If T12CheckBox7.Checked Then
    '        T12G7.Visible = True
    '    Else
    '        T12G7.Visible = False
    '    End If
    'End Sub

    Protected Sub T12btnnotessave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T12btnnotessave.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@TYPE", T12RadioButtontype.SelectedValue)
        pParms(3) = New SqlClient.SqlParameter("@COMMENTS", T12txtNotes.Text.Trim())
        pParms(4) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T12ddnotesstaff.SelectedValue)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_MEDICAL_NOTES", pParms)
        T12txtNotes.Text = ""
        T12ddnotesstaff.SelectedIndex = 0
        BindNotes()
    End Sub

End Class
