Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_semRemoveInclusionMembers
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            BindDepartments()
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''

        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub BindDepartments()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim str_query = "select * from SEN_DEPARTMENTS"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddremovedepartmentmembers.DataSource = ds
        ddremovedepartmentmembers.DataValueField = "DEPT_ID"
        ddremovedepartmentmembers.DataTextField = "DEPT_DESC"
        ddremovedepartmentmembers.DataBind()

        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Select a Section"


        ddremovedepartmentmembers.Items.Insert(0, list)

    End Sub

    Protected Sub ddremovedepartmentmembers_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddremovedepartmentmembers.SelectedIndexChanged
        BindGridRemoveMembers()
    End Sub

    Public Sub BindGridRemoveMembers()
        lblmemberremovemessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        GrdRemoveMembers.Controls.Clear()
        If ddremovedepartmentmembers.SelectedIndex > 0 Then
            Dim str_query = "SELECT * FROM SEN_DEPARTMENT_MEMBERS A  " & _
                            " INNER JOIN OASIS.DBO.EMPLOYEE_M B ON A.EMP_ID = B.EMP_ID " & _
                            " INNER JOIN OASIS.DBO.EMPDESIGNATION_M C ON B.EMP_DES_ID=C.DES_ID " & _
                            " WHERE A.EMP_BSU_ID='" & Hiddenbsuid.Value & "' AND A.EMP_DEPT_ID='" & ddremovedepartmentmembers.SelectedValue & "'"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            GrdRemoveMembers.DataSource = ds
            GrdRemoveMembers.DataBind()
            ''MainGridView()
            'Session("MemberTabIndex") = 3
            'Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()

            'Response.Redirect("senAssignInclusionMembers.aspx" & mInfo)

        End If
    End Sub

    Protected Sub GrdRemoveMembers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdRemoveMembers.RowCommand
        Dim flag = 0
        If e.CommandName = "RemoveMembers" Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            For Each row As GridViewRow In GrdRemoveMembers.Rows
                Dim check As CheckBox = DirectCast(row.FindControl("Checklist"), CheckBox)
                If check.Checked Then
                    flag = 1
                    Dim memid As String = DirectCast(row.FindControl("Hiddenmemberid"), HiddenField).Value
                    Dim empid As String = DirectCast(row.FindControl("Hiddenempid"), HiddenField).Value
                    Dim pParms(4) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@DEP_MEM_ID", memid)
                    pParms(1) = New SqlClient.SqlParameter("@EMP_ID", empid)
                    pParms(2) = New SqlClient.SqlParameter("@EMP_BSU_ID", Hiddenbsuid.Value)
                    pParms(3) = New SqlClient.SqlParameter("@EMP_DEPT_ID", ddremovedepartmentmembers.SelectedValue)
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "DELETE_SEN_DEPARTMENT_MEMBERS", pParms)
                End If
            Next
            If flag = 1 Then
                lblmemberremovemessage.Text = "Members removed from selected Section."
                Session("MemberTabIndex") = 3
                'Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()

                'Response.Redirect("senAssignInclusionMembers.aspx" & mInfo)

                Response.Write("<script type='text/javascript'> parent.location.reload() </script>")

            Else
                lblmemberremovemessage.Text = "Please Select members to remove"
            End If
        End If
        ''BindGridRemoveMembers()
    End Sub

    'Protected Sub btnremovesearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnremovesearch.Click
    '    If ddremovedepartmentmembers.SelectedValue > 0 Then
    '        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
    '        Dim pParms(6) As SqlClient.SqlParameter
    '        If txtempidsearch.Text.Trim() <> "" Then
    '            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", txtempidsearch.Text.Trim())
    '        End If
    '        If txtempnosearch.Text.Trim() <> "" Then
    '            pParms(1) = New SqlClient.SqlParameter("@EMPNO", txtempnosearch.Text.Trim())
    '        End If
    '        If txtempnamesearch.Text.Trim() <> "" Then
    '            pParms(2) = New SqlClient.SqlParameter("@EMP_NAME", txtempnamesearch.Text.Trim().Replace(" ", ""))
    '        End If
    '        If dddesignationsearch.SelectedIndex > 0 Then
    '            pParms(3) = New SqlClient.SqlParameter("@EMP_DES_ID", dddesignationsearch.SelectedValue)
    '        End If

    '        pParms(4) = New SqlClient.SqlParameter("@EMP_BSU_ID", Hiddenbsuid.Value)
    '        pParms(5) = New SqlClient.SqlParameter("@EMP_DEPT_ID", ddremovedepartmentmembers.SelectedValue)


    '        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SEARCH_SEN_DEPARTMENT_MEMBERS", pParms)
    '        GrdRemoveMembers.DataSource = ds
    '        GrdRemoveMembers.DataBind()
    '    Else
    '        lblmemberremovemessage.Text = "Please Select a Section for searching."
    '    End If

    'End Sub

End Class
