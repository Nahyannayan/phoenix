Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_semInclusionStudentSearch
    Inherits System.Web.UI.UserControl

    Dim studClass As New studClass
    ' Delegate declaration 
    Public Delegate Sub OnButtonClick(ByVal ds As DataSet)


    ' Event declaration 
    Public Event btnHandler As OnButtonClick
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Hiddenbsu.Value = Session("sbsuid") ''"125016" ''
            Hiddenacyid.Value = Session("Current_ACY_ID") '"2" '
            Hiddenacdid.Value = Session("Current_ACD_ID") '"79" '

            Dim list As New ListItem
            list.Text = "All"
            list.Value = "0"
            ddshift.Items.Insert(0, list)
            ddclm.Items.Insert(0, list)
            ddstream.Items.Insert(0, list)
            ddsection.Items.Insert(0, list)
            BindPartAControls()
            BindClm()
            BindGrade()
            BindShift()
            BindStream()
            BindSection()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub BindPartAControls()


        Dim acc As New studClass
        Dim accid = Hiddenacyid.Value
        ddgrade = acc.PopulateGrade(ddgrade, Hiddenacdid.Value)
        Dim list3 As New ListItem
        list3.Text = "All"
        list3.Value = "0"
        ddgrade.Items.Insert(0, list3)


    End Sub
    Public Sub BindClm()
        ddclm.Items.Clear()
        ddclm.Visible = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = ""
        str_query = "select distinct ACADEMICYEAR_D.acd_id,CURRICULUM_M.CLM_DESCR from ACADEMICYEAR_D " & _
                    " inner join CURRICULUM_M on ACADEMICYEAR_D.ACD_CLM_ID=CURRICULUM_M.CLM_ID and ACADEMICYEAR_D.ACD_BSU_ID= '" & Hiddenbsu.Value & "'" & _
                    " and ACD_ACY_ID='" & Hiddenacyid.Value & "' AND ACD_CURRENT='TRUE'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then

            ddclm.DataSource = ds
            ddclm.DataTextField = "CLM_DESCR"
            ddclm.DataValueField = "ACD_ID"
            ddclm.DataBind()
        End If
        Dim list As New ListItem
        list.Text = "All"
        list.Value = "0"
        ddclm.Items.Insert(0, list)
    End Sub

    Protected Sub ddgrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddgrade.SelectedIndexChanged

        BindShift()
        BindStream()
        BindSection()

    End Sub
    Public Sub BindSection()
        ddsection.Items.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query = "select sct_id,sct_descr from section_m where sct_grm_id='" & ddstream.SelectedValue & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        If ds.Tables(0).Rows.Count > 0 Then
            ddsection.DataSource = ds
            ddsection.DataTextField = "SCT_DESCR"
            ddsection.DataValueField = "SCT_ID"
            ddsection.DataBind()
        End If
        Dim list As New ListItem
        list.Text = "All"
        list.Value = "0"
        ddsection.Items.Insert(0, list)
    End Sub

    Protected Sub ddshift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddshift.SelectedIndexChanged
        BindStream()
        BindSection()
    End Sub
    Public Sub BindStream()
        ddstream.Visible = True
        Dim stu As New studClass
        ddstream = stu.PopulateGradeStream(ddstream, ddgrade.SelectedValue, Hiddenacdid.Value, ddshift.SelectedValue)
        Dim list3 As New ListItem
        list3.Text = "All"
        list3.Value = "0"
        ddstream.Items.Insert(0, list3)
    End Sub
    Public Sub BindShift()
        ddshift.Visible = True
        Dim stu As New studClass
        ddshift = stu.PopulateGradeShift(ddshift, ddgrade.SelectedValue, Hiddenacdid.Value)
        Dim list3 As New ListItem
        list3.Text = "All"
        list3.Value = "0"
        ddshift.Items.Insert(0, list3)
    End Sub
    Public Sub BindGrade()
        Dim studClass As New studClass
        ddgrade = studClass.PopulateGrade(ddgrade, ddclm.SelectedValue.ToString())
        Dim list3 As New ListItem
        list3.Text = "All"
        list3.Value = "0"
        ddgrade.Items.Insert(0, list3)
    End Sub
    Protected Sub ddclm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrade()
        BindShift()
        BindStream()
        BindSection()
    End Sub

    Protected Sub ddstream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindSection()
    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click

    End Sub
    Public Sub Search()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim PartAFilter As String = ""

        PartAFilter = "select * from student_m.stu_bsu_id in('" & Hiddenbsu.Value & "')"

        If ddclm.SelectedIndex > 0 Then
            If PartAFilter <> "" Then
                PartAFilter += " AND " & "student_m.stu_acd_id in('" & ddclm.SelectedValue & "')"
            Else
                PartAFilter = "student_m.stu_acd_id in('" & ddclm.SelectedValue & "')"
            End If

        End If

        If ddgrade.SelectedIndex > 0 Then
            If PartAFilter <> "" Then
                PartAFilter += " AND " & "student_m.stu_grd_id in('" & ddgrade.SelectedValue & "')"
            Else
                PartAFilter = "student_m.stu_grd_id in('" & ddgrade.SelectedValue & "')"
            End If

        End If

        If ddsection.SelectedIndex > 0 Then
            If PartAFilter <> "" Then
                PartAFilter += " AND " & "student_m.stu_sct_id in('" & ddsection.SelectedValue & "')"
            Else
                PartAFilter = "student_m.stu_sct_id in('" & ddsection.SelectedValue & "')"
            End If
        End If

        If ddstream.SelectedIndex > 0 Then
            If PartAFilter <> "" Then
                PartAFilter += " AND " & "student_m.stu_grm_id in('" & ddstream.SelectedValue & "')"
            Else
                PartAFilter = "student_m.stu_grm_id in('" & ddstream.SelectedValue & "')"
            End If

        End If
        If ddshift.SelectedIndex > 0 Then
            If PartAFilter <> "" Then
                PartAFilter += " AND " & "student_m.stu_shf_id in('" & ddshift.SelectedValue & "')"
            Else
                PartAFilter = "student_m.stu_shf_id in('" & ddshift.SelectedValue & "')"
            End If
        End If

        If txtstudentsearchnumber.Text.Trim() <> "" Then
            If PartAFilter <> "" Then
                PartAFilter += " AND " & "student_m.STU_NO in('" & txtstudentsearchnumber.Text.Trim() & "')"
            Else
                PartAFilter = "student_m.STU_NO in('" & txtstudentsearchnumber.Text.Trim() & "')"
            End If
        End If

        If txtstudentsearchname.Text.Trim() <> "" Then
            If PartAFilter <> "" Then
                PartAFilter += " AND " & "student_m.STU_FIRSTNAME in('" & txtstudentsearchname.Text.Trim() & "')"
            Else
                PartAFilter = "student_m.STU_FIRSTNAME in('" & txtstudentsearchname.Text.Trim() & "')"
            End If
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, PartAFilter)
        RaiseEvent btnHandler(ds)

    End Sub
End Class
