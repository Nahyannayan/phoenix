Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senLSA
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindPrimaryDetails()
            BindAuthor()
            'BindDetails()
            BindHistory()
            'BindAuthorNotes()
            BindNotes()
            BindLSInfoPath()
            DepEntry()
            LockFileCheck()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(T9lnkIEPinfoPath)
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub DepEntry()

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT ENTRY_LSA FROM SEN_ACE_STUDENTS WHERE STU_ID='" & Hiddenstuid.Value & "'"
        Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)

        If Not IsDBNull(val) Then
            T9lblDateenteredLSA.Text = Convert.ToDateTime(val).ToString("dd/MMM/yyyy")
            Try
                If T9lblDateenteredLSA.Text.Trim() <> "" Then
                    T9lblTimeLag.Text = DateDiff(DateInterval.Day, Convert.ToDateTime(T9lbldateentered.Text), Convert.ToDateTime(T9lblDateenteredLSA.Text), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.System) & " Days"
                End If
            Catch ex As Exception

            End Try
        End If


    End Sub
    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then

            T9checkLSIEP.Enabled = False
            T9btIEPinfoSave.Enabled = False
            T9historysave.Enabled = False
            T9btnsavecomments.Enabled = False
            lnkentrysave.Enabled = False
        End If

    End Sub
    Public Sub BindLSInfoPath()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_LSA_IEP " & _
                                 " where STU_ID='" & Hiddenstuid.Value & "' AND BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            T9checkLSIEP.Checked = ds.Tables(0).Rows(0).Item("LS_IEP")
            HiddenLSinfoPath.Value = ds.Tables(0).Rows(0).Item("LS_IEP_FILE_INFO").ToString()
            If HiddenLSinfoPath.Value <> "" Then
                T9txtLSinfo.Text = ds.Tables(0).Rows(0).Item("LS_IEP_FILE_INFO").ToString()
                'T9lnkIEPinfoPath.Visible = True
            Else
                T9lnkIEPinfoPath.Visible = False
            End If

        End If

    End Sub
    Public Sub BindNotes()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(COMMENT_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, COMMENT_TEXT,ENTRY_DATE,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from SEN_DEPARTMENT_LSA_NOTES a " & _
                                 " inner join oasis.dbo.employee_m b on a.entry_emp_id=b.emp_id " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "' order by a.entry_date desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T9GrdComments.DataSource = ds
        T9GrdComments.DataBind()
    End Sub
    'Public Sub BindAuthorNotes()
    '    Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
    '    Dim strQuery As String = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
    '                             " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id "
    '    T9DDStaffnotes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
    '    T9DDStaffnotes.DataTextField = "empname"
    '    T9DDStaffnotes.DataValueField = "EMP_ID"
    '    T9DDStaffnotes.DataBind()
    '    Dim list As New ListItem
    '    list.Text = "Select an Inclusion Member"
    '    list.Value = "-1"
    '    T9DDStaffnotes.Items.Insert(0, list)

    'End Sub
    Public Sub BindHistory()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(REASONS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname,TIME,LSA_EMP_ID,DATE_COMMENCED,DATE_DISCONTINUED,REASONS,ENTRY_DATE from SEN_DEPARTMENT_LSA_SUPPORT_HISTORY a " & _
                                 " inner join oasis.dbo.employee_m b on a.lsa_emp_id=b.emp_id " & _
                                 " where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' order by a.entry_date desc"
        T9GrdHitory.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T9GrdHitory.DataBind()

    End Sub
    'Public Sub BindDetails()
    '    Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
    '    Dim strQuery As String = "select * from SEN_DEPARTMENT_LSA_CURRENTLY_SUPPORTED   " & _
    '                             " where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
    '    Dim ds As DataSet
    '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        Dim val = ds.Tables(0).Rows(0).Item("CURRENTLY_SUPPORTED").ToString()
    '        T9RadioButtonListTIME.SelectedValue = val
    '        val = ds.Tables(0).Rows(0).Item("LSA_EMP_ID").ToString()
    '        T9DDstaffs.SelectedValue = val
    '        T9lnksavetime.Visible = False
    '        T9RadioButtonListTIME.Enabled = False
    '    Else
    '        T9RadioButtonListTIME.Enabled = True
    '        T9lnksavetime.Visible = True
    '    End If

    'End Sub
    Public Sub BindAuthor()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id where a.EMP_DEPT_ID='9' and a.EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        'T9DDstaffs.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        T9DDHistory.DataSource = ds
        'T9DDstaffs.DataTextField = "empname"
        'T9DDstaffs.DataValueField = "EMP_ID"
        T9DDHistory.DataTextField = "empname"
        T9DDHistory.DataValueField = "EMP_ID"
        'T9DDstaffs.DataBind()
        T9DDHistory.DataBind()
        'Dim list1 As New ListItem
        'list1.Text = "Select an LSA Member"
        'list1.Value = "-1"
        'T9DDstaffs.Items.Insert(0, list1)
        Dim list2 As New ListItem
        list2.Text = "Select an LSA Member"
        list2.Value = "-1"
        T9DDHistory.Items.Insert(0, list2)

        strQuery = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id and a.EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        ''Notes
        T9DDStaffnotes.DataSource = ds
        T9DDStaffnotes.DataTextField = "empname"
        T9DDStaffnotes.DataValueField = "EMP_ID"
        T9DDStaffnotes.DataBind()
        Dim list As New ListItem
        list.Text = "Select an Inclusion Member"
        list.Value = "-1"
        T9DDStaffnotes.Items.Insert(0, list)

    End Sub
    Public Sub BindPrimaryDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select c.stu_doj,b.BSU_NAME,b.BSU_SHORTNAME,ENTRY_DATE from SEN_DEPARTMENT_STUDENTS a " & _
                                 " inner join oasis.dbo.BUSINESSUNIT_M b on a.stu_bsu_id = b.bsu_id " & _
                                 " inner join oasis.dbo.student_m c on c.stu_id= a.stu_id  " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.STU_BSU_ID='" & Hiddenbsuid.Value & "' and a.dept_id='9' and a.active='true' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            T9lbldateenteredlabel.Text = "Date Entered " & ds.Tables(0).Rows(0).Item("BSU_SHORTNAME").ToString()
            T9lbldateentered.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("stu_doj").ToString()).ToString("dd/MMM/yyyy")
          
        End If

    End Sub

    'Protected Sub T9lnksavetime_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T9lnksavetime.Click
    '    Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
    '    Dim pParms(4) As SqlClient.SqlParameter
    '    pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
    '    pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
    '    pParms(2) = New SqlClient.SqlParameter("@CURRENTLY_SUPPORTED", T9RadioButtonListTIME.SelectedValue)
    '    pParms(3) = New SqlClient.SqlParameter("@LSA_EMP_ID", T9DDstaffs.SelectedValue)
    '    SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_LSA_CURRENTLY_SUPPORTED", pParms)
    '    BindDetails()
    'End Sub

    Protected Sub T9historysave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T9historysave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@TIME ", T9RadioButtonListhistory.SelectedItem.Text)
            pParms(3) = New SqlClient.SqlParameter("@LSA_EMP_ID", T9DDHistory.SelectedValue)
            pParms(4) = New SqlClient.SqlParameter("@DATE_COMMENCED", T9txtDatecommenced.Text.Trim())
            pParms(5) = New SqlClient.SqlParameter("@DATE_DISCONTINUED", T9txtDatediscontinued.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@REASON", T9xtxReason.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_LSA_SUPPORT_HISTORY", pParms)
            T9RadioButtonListhistory.SelectedIndex = 0
            T9DDHistory.SelectedIndex = 0
            T9txtDatecommenced.Text = ""
            T9txtDatediscontinued.Text = ""
            T9xtxReason.Text = ""
            BindHistory()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T9btnsavecomments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T9btnsavecomments.Click

        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@COMMENT_TEXT", T9txtcomments.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T9DDStaffnotes.SelectedValue)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_LSA_NOTES", pParms)
            T9DDStaffnotes.SelectedIndex = 0
            T9txtcomments.Text = ""
            BindNotes()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T9GrdHitory_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T9GrdHitory.PageIndexChanging
        T9GrdHitory.PageIndex = e.NewPageIndex
        BindHistory()
    End Sub

    Protected Sub T9GrdComments_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T9GrdComments.PageIndexChanging
        T9GrdComments.PageIndex = e.NewPageIndex
        BindNotes()
    End Sub

    Protected Sub T9checkLSIEP_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T9checkLSIEP.CheckedChanged
        Dim val = False
        If T9checkLSIEP.Checked Then
            val = True
        End If
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@LS_IEP", val)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_LSA_IEP", pParms)
        BindLSInfoPath()
    End Sub

    Protected Sub T9btIEPinfoSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T9btIEPinfoSave.Click
        lblstatusmessage.Text = ""
        Try
            Dim val = False
            If T9checkLSIEP.Checked Then
                val = True
            End If
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@LS_IEP", val)
            pParms(3) = New SqlClient.SqlParameter("@LS_IEP_FILE_INFO", T9txtLSinfo.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_LSA_IEP", pParms)
            BindLSInfoPath()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    'Protected Sub T9lnkIEPinfoPath_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T9lnkIEPinfoPath.Click
    '    Try
    '        Dim path = HiddenLSinfoPath.Value
    '        HttpContext.Current.Response.ContentType = "application/octect-stream"
    '        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
    '        HttpContext.Current.Response.Clear()
    '        HttpContext.Current.Response.WriteFile(path)
    '        HttpContext.Current.Response.End()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Protected Sub lnkentrysave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkentrysave.Click
        lblstatusmessage.Text = ""

        Try
            If T9lblDateenteredLSA.Text <> "" Then
                Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
                Dim strQuery As String = "UPDATE SEN_ACE_STUDENTS SET  ENTRY_LSA='" & T9lblDateenteredLSA.Text & "' WHERE STU_ID='" & Hiddenstuid.Value & "' "
                Dim val = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
                DepEntry()
                lblstatusmessage.Text = "Successfully Saved"

            End If
        Catch ex As Exception

        End Try

    End Sub
End Class
