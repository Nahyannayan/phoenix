Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senDyslexiaUnit
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindAuthor()
            BindPrimaryDetails()
            BindControls()
            BindCurrentTeacher()
            BindActiveTeachers()
            BindTeacherHistory()
            BindComprehensionGrid()
            BindWPMGrid()
            BindInterventions()
            BindNotes()
            BindDetailsFromAcePage()
            BindIQ()
            BindDuIEP()
            DepEntry()
            LockFileCheck()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(t4lnkduieppath)
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub DepEntry()

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT ENTRY_DU FROM SEN_ACE_STUDENTS WHERE STU_ID='" & Hiddenstuid.Value & "' "
        Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)

        If Not IsDBNull(val) Then
            T4lblDateenteredDu.Text = Convert.ToDateTime(val).ToString("dd/MMM/yyyy")
            Try
                If T4lblDateenteredDu.Text.Trim() <> "" Then
                    T4lblTimeLag.Text = DateDiff(DateInterval.Day, Convert.ToDateTime(T4lbldateentered.Text), Convert.ToDateTime(T4lblDateenteredDu.Text), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.System) & " Days"
                End If
            Catch ex As Exception

            End Try
        End If


    End Sub
    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then
            T4txtsavebasic.Enabled = False
            T4lnksaveteacher.Enabled = False
            T4btnsavecomprehension.Enabled = False
            T4btnIQSave.Enabled = False
            T4btnsavewpm.Enabled = False
            T4btnsaveDuinterventions.Enabled = False
            T4btnnotessave.Enabled = False
            t4btnduiepinfosave.Enabled = False
            lnkentrysave.Enabled = False
        End If

    End Sub
    Public Sub BindDuIEP()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString

        Dim strQuery As String = "select * from SEN_DEPARTMENT_DU_IEP  " & _
                                 "WHERE STU_ID='" & Hiddenstuid.Value & "' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            t4checkduiep.Checked = ds.Tables(0).Rows(0).Item("DU_IEP")
            If ds.Tables(0).Rows(0).Item("DU_IEP_FILE_INFO").ToString() <> "" Then
                txtduiepinfo.Text = ds.Tables(0).Rows(0).Item("DU_IEP_FILE_INFO").ToString()
                't4btnduiepinfosave.Visible = False
                't4lnkduieppath.Visible = True
                HiddenDuIEPInfo.Value = ds.Tables(0).Rows(0).Item("DU_IEP_FILE_INFO").ToString()
            Else
                t4lnkduieppath.Visible = False
            End If
        End If

    End Sub
    Public Sub BindIQ()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString

        Dim strQuery As String = "select IQ,EXPECTED_SCORE from SEN_DEPARTMENT_DU_IQ  " & _
                                 "WHERE STU_ID='" & Hiddenstuid.Value & "' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        If ds.Tables(0).Rows.Count > 0 Then
            T4txtIQ.Text = ds.Tables(0).Rows(0).Item("IQ").ToString()
            T4txtExpectedscore.Text = ds.Tables(0).Rows(0).Item("EXPECTED_SCORE").ToString()

        End If

    End Sub
    Public Sub BindDetailsFromAcePage()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString

        Dim strQuery As String = "select A.DATE,A.TYPE from dbo.SEN_DEPARTMENT_ACE_SCREENING A  " & _
                                 "WHERE A.STU_ID='" & Hiddenstuid.Value & "' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        If ds.Tables(0).Rows.Count > 0 Then
            T4txtduassessment.Text = ds.Tables(0).Rows(0).Item("DATE").ToString()
            T4txtdusentype.Text = ds.Tables(0).Rows(0).Item("TYPE").ToString()
        End If

        strQuery = "Select DI_INDEX from SEN_DEPARTMENT_ACE_RESULTS  Where STU_ID='" & Hiddenstuid.Value & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        If ds.Tables(0).Rows.Count > 0 Then
            T4txtduindex.Text = ds.Tables(0).Rows(0).Item("DI_INDEX").ToString()
        End If

    End Sub
    Public Sub BindAuthor()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString

        Dim strQuery As String = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
                               " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id and a.EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        T4ddstaffnotes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T4ddstaffnotes.DataTextField = "empname"
        T4ddstaffnotes.DataValueField = "EMP_ID"
        T4ddstaffnotes.DataBind()
        Dim list1 As New ListItem
        list1.Text = "Select an Inclusion Member"
        list1.Value = "-1"
        T4ddstaffnotes.Items.Insert(0, list1)
    End Sub
    Public Sub BindNotes()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select  '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(du_note,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, entry_date,du_note,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from SEN_DEPARTMENT_DU_NOTES a " & _
                                 " inner join oasis.dbo.employee_m b on a.entry_emp_id= b.emp_id " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "'order by a.entry_date desc "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T4GrdNotes.DataSource = ds
        T4GrdNotes.DataBind()
    End Sub
    
    Public Sub BindInterventions()

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select * from SEN_DEPARTMENT_DU_INTERVENTIONS " & _
                                 " where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then

            t4txtduinter.Text = ds.Tables(0).Rows(0).Item("OTHERS_DES")

            DuI1.Checked = ds.Tables(0).Rows(0).Item("AR_AW")
            DuI2.Checked = ds.Tables(0).Rows(0).Item("CO_WRITE")
            DuI3.Checked = ds.Tables(0).Rows(0).Item("LAPTOP")
            DuI4.Checked = ds.Tables(0).Rows(0).Item("ALPHASMART")
            DuI5.Checked = ds.Tables(0).Rows(0).Item("OTHERS")

            If ds.Tables(0).Rows(0).Item("AR_AW_DATE").ToString() <> "" Then
                txtDuI1.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("AR_AW_DATE").ToString()).ToString("dd/MMM/yyyy")
            End If

            If ds.Tables(0).Rows(0).Item("CO_WRITE_DATE").ToString() <> "" Then
                txtDuI2.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("CO_WRITE_DATE").ToString()).ToString("dd/MMM/yyyy")
            End If

            If ds.Tables(0).Rows(0).Item("LAPTOP_DATE").ToString() <> "" Then
                txtDuI3.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("LAPTOP_DATE").ToString()).ToString("dd/MMM/yyyy")
            End If

            If ds.Tables(0).Rows(0).Item("ALPHASMART_DATE").ToString() <> "" Then
                txtDuI4.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ALPHASMART_DATE").ToString()).ToString("dd/MMM/yyyy")
            End If

            If ds.Tables(0).Rows(0).Item("OTHERS_DATE").ToString() <> "" Then

                txtDuI5.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OTHERS_DATE").ToString()).ToString("dd/MMM/yyyy")
            End If


        End If
    End Sub
    Public Sub BindWPMGrid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select * from SEN_DEPARTMENT_DU_WPM " & _
                                 " where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' order by entry_date desc"

        T4GrdWPM.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T4GrdWPM.DataBind()
    End Sub
    Public Sub BindComprehensionGrid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select * from SEN_DEPARTMENT_DU_COMPREHENSION " & _
                                 " where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' order by entry_date desc"

        T4GrdComprehension.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T4GrdComprehension.DataBind()

    End Sub
    Public Sub BindTeacherHistory()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select CURRENT_TEACHER,DATE_ASSIGNED_TEACHER,WEELKY_LESSON_TEACHER,CURRENT_TA,DATE_ASSIGNED_TA,WEELKY_LESSON_TA,ENTRY_DATE,DATE_COMPLETED_TEACHER,DATE_COMPLETED_TA from  SEN_DEPARTMENT_DU_TEACHER a " & _
                                 " where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' order by a.entry_date desc"

        T4GrdTeacherhistory.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T4GrdTeacherhistory.DataBind()

    End Sub

    Public Sub BindActiveTeachers()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select * from SEN_DEPARTMENT_DU_TEACHER " & _
                                 " where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' and ACTIVE='True'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            T4txtcurrentteacher.Text = ds.Tables(0).Rows(0).Item("CURRENT_TEACHER").ToString()
            T4txtcurrentteacherdate.Text = ds.Tables(0).Rows(0).Item("DATE_ASSIGNED_TEACHER").ToString()
            T4txtweeklylessonteacher.Text = ds.Tables(0).Rows(0).Item("WEELKY_LESSON_TEACHER").ToString()
            T4txtcurrentTA.Text = ds.Tables(0).Rows(0).Item("CURRENT_TA").ToString()
            T4txtcurrentTAdate.Text = ds.Tables(0).Rows(0).Item("DATE_ASSIGNED_TA").ToString()
            T4txtweeklylessonTA.Text = ds.Tables(0).Rows(0).Item("WEELKY_LESSON_TA").ToString()
            T4txtDateComTeacher.Text = ds.Tables(0).Rows(0).Item("DATE_COMPLETED_TEACHER").ToString()
            T4txtDateComTA.Text = ds.Tables(0).Rows(0).Item("DATE_COMPLETED_TA").ToString()
        End If

    End Sub

    Public Sub BindCurrentTeacher()
        'Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString

        'Dim strQuery As String = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
        '                         " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id "
        'T4DDcurrentteacher.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        'T4DDcurrentteacher.DataTextField = "empname"
        'T4DDcurrentteacher.DataValueField = "EMP_ID"
        'T4DDcurrentteacher.DataBind()
        'Dim list1 As New ListItem
        'list1.Text = "Select a Inclusion Member"
        'list1.Value = "-1"
        'T4DDcurrentteacher.Items.Insert(0, list1)


    End Sub

    Public Sub BindControls()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select * from SEN_DEPARTMENT_DU " & _
                                 " where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            T4txtweeklylessontotal.Text = ds.Tables(0).Rows(0).Item("WEEKLY_LESSONS_TOTAL").ToString()
        End If
    End Sub
    Public Sub BindPrimaryDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select c.stu_doj,b.BSU_NAME,b.BSU_SHORTNAME,ENTRY_DATE from SEN_DEPARTMENT_STUDENTS a " & _
                                 " inner join oasis.dbo.BUSINESSUNIT_M b on a.stu_bsu_id = b.bsu_id " & _
                                 " inner join oasis.dbo.student_m c on c.stu_id= a.stu_id  " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.STU_BSU_ID='" & Hiddenbsuid.Value & "' and a.dept_id='4' and a.active='true' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            T4lbldateenteredlabel.Text = "Date Entered " & ds.Tables(0).Rows(0).Item("BSU_SHORTNAME").ToString()
            T4lbldateentered.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("stu_doj").ToString()).ToString("dd/MMM/yyyy")
        End If

    End Sub

    Protected Sub T4txtsavebasic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4txtsavebasic.Click
        lblstatusmessage.Text = ""

        Try
            If T4txtweeklylessontotal.Text <> "" Then
                Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
                pParms(5) = New SqlClient.SqlParameter("@WEEKLY_LESSONS_TOTAL", T4txtweeklylessontotal.Text.Trim())
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_DU", pParms)
                BindControls()
                lblstatusmessage.Text = "Successfully Saved"
            End If

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T4lnksaveteacher_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4lnksaveteacher.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(12) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@CURRENT_TEACHER", T4txtcurrentteacher.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@DATE_ASSIGNED_TEACHER", T4txtcurrentteacherdate.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@WEELKY_LESSON_TEACHER", T4txtweeklylessonteacher.Text.Trim())
            pParms(5) = New SqlClient.SqlParameter("@CURRENT_TA", T4txtcurrentTA.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@DATE_ASSIGNED_TA", T4txtcurrentTAdate.Text.Trim())
            pParms(7) = New SqlClient.SqlParameter("@WEELKY_LESSON_TA", T4txtweeklylessonTA.Text.Trim())
            pParms(8) = New SqlClient.SqlParameter("@DATE_COMPLETED_TEACHER", T4txtDateComTeacher.Text.Trim())
            pParms(9) = New SqlClient.SqlParameter("@DATE_COMPLETED_TA", T4txtDateComTA.Text.Trim())

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_DU_TEACHER", pParms)
            BindActiveTeachers()
            BindTeacherHistory()
            lblstatusmessage.Text = "Successfully Saved"
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T4GrdTeacherhistory_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T4GrdTeacherhistory.PageIndexChanging
        T4GrdTeacherhistory.PageIndex = e.NewPageIndex
        BindTeacherHistory()
    End Sub


    Protected Sub T4btnsavecomprehension_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4btnsavecomprehension.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(16) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@WORD_IDENTIFICATION_AGE", T4txtWordIdentification_Age.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@WORD_IDENTIFICATION_STD_SCORE", T4txtWordIdentification_std_score.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@WORD_IDENTIFICATION_PERCENTILE", T4txtWordIdentification_percentile.Text.Trim())
            pParms(5) = New SqlClient.SqlParameter("@WORD_ATTACK_AGE", T4txtWordAttack_age.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@WORD_ATTACK_STD_SCORE", T4txtWordAttack_std_score.Text.Trim())
            pParms(7) = New SqlClient.SqlParameter("@WORD_ATTACK_PERCENTILE", T4txtWordAttack_percentile.Text.Trim())
            pParms(8) = New SqlClient.SqlParameter("@WORD_COMPREHENSION_AGE", T4txtwordcomrehension_age.Text.Trim())
            pParms(9) = New SqlClient.SqlParameter("@WORD_COMPREHENSION_STD_SCORE", T4txtwordcomrehension_std_score.Text.Trim())
            pParms(10) = New SqlClient.SqlParameter("@WORD_COMPREHENSION_PERCENTILE", T4txtwordcomrehension_percentile.Text.Trim())
            pParms(11) = New SqlClient.SqlParameter("@PASSAGE_COMPREHENSION_AGE", T4txtpassagecomprehentions_age.Text.Trim())
            pParms(12) = New SqlClient.SqlParameter("@PASSAGE_COMPREHENSION_STD_SCORE", T4txtpassagecomprehentions_std_score.Text.Trim())
            pParms(13) = New SqlClient.SqlParameter("@PASSAGE_COMPREHENSION_PERCENTILE", T4txtpassagecomprehentions_percentile.Text.Trim())
            pParms(14) = New SqlClient.SqlParameter("@SPELLING_AGE", T4txtspelling_age.Text.Trim())
            pParms(15) = New SqlClient.SqlParameter("@SPELLING_STD_SCORE", T4txtspelling_std_score.Text.Trim())

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_DU_COMPREHENSION", pParms) '' Heading Changed as per new requirement "DU Assessment  Details"

            T4txtWordIdentification_Age.Text = ""
            T4txtWordIdentification_std_score.Text = ""
            T4txtWordIdentification_percentile.Text = ""
            T4txtWordAttack_age.Text = ""
            T4txtWordAttack_std_score.Text = ""
            T4txtWordAttack_percentile.Text = ""
            T4txtwordcomrehension_age.Text = ""
            T4txtwordcomrehension_std_score.Text = ""
            T4txtwordcomrehension_percentile.Text = ""
            T4txtpassagecomprehentions_age.Text = ""
            T4txtpassagecomprehentions_std_score.Text = ""
            T4txtpassagecomprehentions_percentile.Text = ""
            T4txtspelling_age.Text = ""
            T4txtspelling_std_score.Text = ""
            BindComprehensionGrid()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception


        End Try

    End Sub

    Protected Sub T4btnsavewpm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4btnsavewpm.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@READING_WPM", T4txtreadingwpd.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@WRITING_WPM", T4txtwritingwpd.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@TYPING_WPM", T4txttypingWPD.Text.Trim())

            If t4txtwpmdate.Text.Trim() <> "" Then
                pParms(5) = New SqlClient.SqlParameter("@WPM_DATE", Convert.ToDateTime(t4txtwpmdate.Text.Trim()))
            End If

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_DU_WPM", pParms)
            T4txtreadingwpd.Text = ""
            T4txtwritingwpd.Text = ""
            T4txttypingWPD.Text = ""
            t4txtwpmdate.Text = ""
            BindWPMGrid()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T4GrdComprehension_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T4GrdComprehension.PageIndexChanging
        T4GrdComprehension.PageIndex = e.NewPageIndex
        BindComprehensionGrid()
    End Sub

    Protected Sub T4GrdWPM_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T4GrdWPM.PageIndexChanging
        T4GrdWPM.PageIndex = e.NewPageIndex
        BindWPMGrid()
    End Sub

    Protected Sub T4btnsaveDuinterventions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4btnsaveDuinterventions.Click
        lblstatusmessage.Text = ""

        Try

            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString


            Dim pParms(13) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@AR_AW", DuI1.Checked)
            pParms(3) = New SqlClient.SqlParameter("@CO_WRITE", DuI2.Checked)
            pParms(4) = New SqlClient.SqlParameter("@LAPTOP", DuI3.Checked)
            pParms(5) = New SqlClient.SqlParameter("@ALPHASMART", DuI4.Checked)
            pParms(6) = New SqlClient.SqlParameter("@OTHERS", DuI5.Checked)
            pParms(7) = New SqlClient.SqlParameter("@OTHERS_DES", t4txtduinter.Text.Trim())

            If txtDuI1.Text.Trim() <> "" Then
                pParms(8) = New SqlClient.SqlParameter("@AR_AW_DATE", Convert.ToDateTime(txtDuI1.Text.Trim()))
            End If

            If txtDuI2.Text.Trim() <> "" Then
                pParms(9) = New SqlClient.SqlParameter("@CO_WRITE_DATE", Convert.ToDateTime(txtDuI2.Text.Trim()))
            End If

            If txtDuI3.Text.Trim() <> "" Then
                pParms(10) = New SqlClient.SqlParameter("@LAPTOP_DATE", Convert.ToDateTime(txtDuI3.Text.Trim()))
            End If


            If txtDuI4.Text.Trim() <> "" Then
                pParms(11) = New SqlClient.SqlParameter("@ALPHASMART_DATE", Convert.ToDateTime(txtDuI4.Text.Trim()))
            End If

            If txtDuI5.Text.Trim() <> "" Then
                pParms(12) = New SqlClient.SqlParameter("@OTHERS_DATE", Convert.ToDateTime(txtDuI5.Text.Trim()))
            End If

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_DU_INTERVENTIONS", pParms)
            BindInterventions()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try



    End Sub

   

    Protected Sub T4btnnotessave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4btnnotessave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@COMMENT_TEXT", T4txtnotes.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T4ddstaffnotes.SelectedValue)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_DU_NOTES", pParms)
            T4ddstaffnotes.SelectedIndex = 0
            T4txtnotes.Text = ""
            BindNotes()
            lblstatusmessage.Text = "Successfully Saved"
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T4GrdNotes_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T4GrdNotes.PageIndexChanging
        T4GrdNotes.PageIndex = e.NewPageIndex
        BindNotes()
    End Sub

    Protected Sub T4btnIQSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T4btnIQSave.Click
        lblstatusmessage.Text = ""

        Try
            If T4txtIQ.Text <> "" Then
                Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
                pParms(2) = New SqlClient.SqlParameter("@IQ", T4txtIQ.Text.Trim())
                pParms(3) = New SqlClient.SqlParameter("@EXPECTED_SCORE", ExpectedScore(T4txtIQ.Text.Trim()))
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_DU_IQ", pParms)
                BindIQ()
                lblstatusmessage.Text = "Successfully Saved"

            End If
        Catch ex As Exception

        End Try


    End Sub

    Public Function ExpectedScore(ByVal IQ As String)
        Dim returnval = ""

        If IQ < 100 Then
            returnval = ((100 - IQ) / 2) + IQ
        Else
            returnval = (((IQ - 100) / 2) + 100)
        End If

        Return returnval
    End Function


    Protected Sub t4checkduiep_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles t4checkduiep.CheckedChanged
        Dim val = False

        If t4checkduiep.Checked = True Then
            val = True
        End If

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@DU_IEP", val)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_DU_IEP", pParms)
        BindDuIEP()
    End Sub

    Protected Sub t4btnduiepinfosave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles t4btnduiepinfosave.Click
        lblstatusmessage.Text = ""

        Try
            Dim val = False

            If t4checkduiep.Checked = True Then
                val = True
            End If

            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@DU_IEP", val)
            pParms(3) = New SqlClient.SqlParameter("@DU_IEP_FILE_INFO", txtduiepinfo.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_DU_IEP", pParms)
            BindDuIEP()
            lblstatusmessage.Text = "Successfully Saved"
        Catch ex As Exception

        End Try

    End Sub

    'Protected Sub t4lnkduieppath_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles t4lnkduieppath.Click
    '    Try
    '        Dim path = HiddenDuIEPInfo.Value
    '        HttpContext.Current.Response.ContentType = "application/octect-stream"
    '        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
    '        HttpContext.Current.Response.Clear()
    '        HttpContext.Current.Response.WriteFile(path)
    '        HttpContext.Current.Response.End()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Protected Sub lnkentrysave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkentrysave.Click
        lblstatusmessage.Text = ""

        Try
            If T4lblDateenteredDu.Text <> "" Then
                Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
                Dim strQuery As String = "UPDATE SEN_ACE_STUDENTS SET  ENTRY_DU='" & T4lblDateenteredDu.Text & "' WHERE STU_ID='" & Hiddenstuid.Value & "' "
                Dim val = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
                DepEntry()
                lblstatusmessage.Text = "Successfully Saved"

            End If
        Catch ex As Exception

        End Try

    End Sub
End Class
