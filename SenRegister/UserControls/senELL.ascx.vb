Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senELL
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindPrimaryDetails()
            Bindlanguage()
            BindDetails()
            BindAuthor()
            BindNotes()
            BindLanguagedetails()
            DepEntry()
            LockFileCheck()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub DepEntry()

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT ENTRY_ELL FROM SEN_ACE_STUDENTS WHERE STU_ID='" & Hiddenstuid.Value & "'"
        Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)

        If Not IsDBNull(val) Then
            T5lblDateenteredELL.Text = Convert.ToDateTime(val).ToString("dd/MMM/yyyy")
            Try
                If T5lblDateenteredELL.Text.Trim() <> "" Then
                    T5lblTimeLag.Text = DateDiff(DateInterval.Day, Convert.ToDateTime(T5lbldateentered.Text), Convert.ToDateTime(T5lblDateenteredELL.Text), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.System) & " Days"
                End If
            Catch ex As Exception

            End Try
        End If


    End Sub
    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then
            btnlanguagesave.Enabled = False
            T5btnsave.Enabled = False
            T5btnnotessave.Enabled = False
            lnkentrysave.Enabled = False
        End If

    End Sub
    Public Sub BindLanguagedetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_ELL_LANGUAGE_DATA " & _
                                 " where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        If ds.Tables(0).Rows.Count > 0 Then
            T5ddLang1.SelectedValue = ds.Tables(0).Rows(0).Item("LANGUAGE_1_ID").ToString()
            T5ddLang2.SelectedValue = ds.Tables(0).Rows(0).Item("LANGUAGE_2_ID").ToString()
            T5ddLang3.SelectedValue = ds.Tables(0).Rows(0).Item("LANGUAGE_3_ID").ToString()

            T5txtDialect1.Text = ds.Tables(0).Rows(0).Item("LANUAGE_1_DIALECT").ToString()
            T5txtDialect2.Text = ds.Tables(0).Rows(0).Item("LANUAGE_2_DIALECT").ToString()
            T5txtDialect3.Text = ds.Tables(0).Rows(0).Item("LANUAGE_3_DIALECT").ToString()
            T5txtlearningenglish.Text = ds.Tables(0).Rows(0).Item("DATE_LEARNING_ENGLISH").ToString()
           
        End If

    End Sub
    Public Sub BindNotes()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(COMMENT_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, COMMENT_TEXT,ENTRY_DATE,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from SEN_DEPARTMENT_ELL_NOTES a " & _
                                 " inner join oasis.dbo.employee_m b on a.entry_emp_id=b.emp_id " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "' order by a.entry_date desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T5GrdComments.DataSource = ds
        T5GrdComments.DataBind()

    End Sub
    Public Sub BindAuthor()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id and a.EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        T5ddStaffNotes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T5ddStaffNotes.DataTextField = "empname"
        T5ddStaffNotes.DataValueField = "EMP_ID"
        T5ddStaffNotes.DataBind()
        Dim list As New ListItem
        list.Text = "Select a Inclusion Member"
        list.Value = "-1"
        T5ddStaffNotes.Items.Insert(0, list)

    End Sub
    Public Sub BindDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_ELL " & _
                                 " where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T5GridContTracker.DataSource = ds
        T5GridContTracker.DataBind()
    End Sub
    Public Sub BindPrimaryDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select c.stu_doj,b.BSU_NAME,b.BSU_SHORTNAME,ENTRY_DATE from SEN_DEPARTMENT_STUDENTS a " & _
                                 " inner join oasis.dbo.BUSINESSUNIT_M b on a.stu_bsu_id = b.bsu_id " & _
                                 " inner join oasis.dbo.student_m c on c.stu_id= a.stu_id  " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.STU_BSU_ID='" & Hiddenbsuid.Value & "' and a.dept_id='5' and a.active='true' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            T5lbldateenteredlabel.Text = "Date Entered " & ds.Tables(0).Rows(0).Item("BSU_SHORTNAME").ToString()
            T5lbldateentered.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("stu_doj").ToString()).ToString("dd/MMM/yyyy")
        End If

    End Sub
    Public Sub Bindlanguage()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "Select * from SEN_DEPARTMENT_ELL_LANGUAGES where LANGUAGE_BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T5ddLang1.DataSource = ds
        T5ddLang2.DataSource = ds
        T5ddLang3.DataSource = ds
        T5ddLang1.DataTextField = "LANGUAGE_DESC"
        T5ddLang2.DataTextField = "LANGUAGE_DESC"
        T5ddLang3.DataTextField = "LANGUAGE_DESC"
        T5ddLang1.DataValueField = "LANGUAGE_ID"
        T5ddLang2.DataValueField = "LANGUAGE_ID"
        T5ddLang3.DataValueField = "LANGUAGE_ID"
        T5ddLang1.DataBind()
        T5ddLang2.DataBind()
        T5ddLang3.DataBind()
        Dim list1 As New ListItem
        list1.Text = "Select a Language"
        list1.Value = "-1"
        T5ddLang1.Items.Insert(0, list1)
        Dim list2 As New ListItem
        list2.Text = "Select a Language"
        list2.Value = "-1"
        T5ddLang2.Items.Insert(0, list2)
        Dim list3 As New ListItem
        list3.Text = "Select a Language"
        list3.Value = "-1"
        T5ddLang3.Items.Insert(0, list3)


    End Sub

    Protected Sub T5btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T5btnsave.Click
        lblstatusmessage.Text = ""

        Try

            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@SPEAKING_LEVEL", T5ddspeking.SelectedValue)
            pParms(3) = New SqlClient.SqlParameter("@LISTENING_LEVEL", T5ddlistening.SelectedValue)
            pParms(4) = New SqlClient.SqlParameter("@READING_LEVEL", T5ddreading.SelectedValue)
            pParms(5) = New SqlClient.SqlParameter("@WRITING_LEVEL", T5ddWriting.SelectedValue)
            pParms(6) = New SqlClient.SqlParameter("@ENTRY_DATE", T5txtContTrak.Text)
            pParms(7) = New SqlClient.SqlParameter("@EXIT_DATE", T5txtContTrakExit.Text)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ELL", pParms)
            T5ddspeking.SelectedIndex = 0
            T5ddlistening.SelectedIndex = 0
            T5ddreading.SelectedIndex = 0
            T5ddWriting.SelectedIndex = 0
            T5txtContTrak.Text = ""
            T5txtContTrakExit.Text = ""
            BindDetails()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    'Protected Sub T5ddLang1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T5ddLang1.SelectedIndexChanged
    '    If T5ddLang1.SelectedIndex > 0 Then
    '        T5txtDialect1.Text = ""
    '        D1.Visible = False
    '    Else
    '        D1.Visible = True
    '    End If

    'End Sub

    'Protected Sub T5ddLang2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T5ddLang2.SelectedIndexChanged
    '    If T5ddLang2.SelectedIndex > 0 Then
    '        T5txtDialect2.Text = ""
    '        D2.Visible = False
    '    Else
    '        D2.Visible = True
    '    End If
    'End Sub

    'Protected Sub T5ddLang3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T5ddLang3.SelectedIndexChanged
    '    If T5ddLang3.SelectedIndex > 0 Then
    '        T5txtDialect3.Text = ""
    '        D3.Visible = False
    '    Else
    '        D3.Visible = True
    '    End If
    'End Sub

   
    Protected Sub T5btnnotessave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T5btnnotessave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@COMMENT_TEXT", T5Notes.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T5ddStaffNotes.SelectedValue)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ELL_NOTES", pParms)
            T5ddStaffNotes.SelectedIndex = 0
            T5Notes.Text = ""
            BindNotes()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T5GrdComments_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T5GrdComments.PageIndexChanging
        T5GrdComments.PageIndex = e.NewPageIndex
        BindNotes()
    End Sub

    Protected Sub btnlanguagesave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlanguagesave.Click
        lblstatusmessage.Text = ""

        Try

            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@LANGUAGE_1_ID", T5ddLang1.SelectedValue)
            If T5txtDialect1.Text.Trim() <> "" Then
                pParms(3) = New SqlClient.SqlParameter("@LANUAGE_1_DIALECT", T5txtDialect1.Text.Trim())
            End If

            pParms(4) = New SqlClient.SqlParameter("@LANGUAGE_2_ID", T5ddLang2.SelectedValue)

            If T5txtDialect2.Text.Trim() <> "" Then
                pParms(5) = New SqlClient.SqlParameter("@LANUAGE_2_DIALECT", T5txtDialect2.Text.Trim())
            End If

            pParms(6) = New SqlClient.SqlParameter("@LANGUAGE_3_ID", T5ddLang3.SelectedValue)
            If T5txtDialect3.Text.Trim() <> "" Then
                pParms(7) = New SqlClient.SqlParameter("@LANUAGE_3_DIALECT", T5txtDialect3.Text.Trim())
            End If
            pParms(8) = New SqlClient.SqlParameter("@DATE_LEARNING_ENGLISH", T5txtlearningenglish.Text.Trim())

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_ELL_LANGUAGE_DATA", pParms)
            BindLanguagedetails()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try
       
    End Sub

    Protected Sub T5GridContTracker_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T5GridContTracker.PageIndexChanging
        T5GridContTracker.PageIndex = e.NewPageIndex
        BindDetails()
    End Sub

    Protected Sub lnkentrysave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkentrysave.Click
        lblstatusmessage.Text = ""

        Try
            If T5lblDateenteredELL.Text <> "" Then
                Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
                Dim strQuery As String = "UPDATE SEN_ACE_STUDENTS SET  ENTRY_ELL='" & T5lblDateenteredELL.Text & "' WHERE STU_ID='" & Hiddenstuid.Value & "' "
                Dim val = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
                DepEntry()
                lblstatusmessage.Text = "Successfully Saved"

            End If
        Catch ex As Exception

        End Try

    End Sub
End Class
