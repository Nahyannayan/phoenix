Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senMonitoring
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindGrid()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindGrid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(MONITOR_NOTES,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview,b.DEPT_ID, DEPT_DESC,MONITORING_DATE,a.ACTIVE,MONITOR_NOTES,DEPT_TAB_ID from SEN_DEPARTMENT_MONITORING a " & _
                                 " left join SEN_DEPARTMENTS b on a.DEPARTMENT_ID=b.DEPT_ID " & _
                                 " inner join SEN_DEPARTMENT_STUDENTS c on c.STU_ID=a.STU_ID and c.DEPT_ID=b.DEPT_ID  and c.ACTIVE='True' " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn.Trim, CommandType.Text, strQuery)
        T1GrdMonitoring.DataSource = ds
        T1GrdMonitoring.DataBind()


        For Each row As GridViewRow In T1GrdMonitoring.Rows

            Dim dept_id = DirectCast(row.FindControl("HiddenDeptID"), HiddenField).Value
            strQuery = "select sub_monitor_des, sub_monitor_active from SEN_DEPARTMENT_SUB_MONITORING_MASTER a " & _
                       " left join SEN_DEPARTMENT_SUB_MONITORING_DATA b on a.sub_monitor_id=b.sub_monitor_id " & _
                       " where b.sub_monitor_stu_id='" & Hiddenstuid.Value & "' and a.dept_id='" & dept_id & "' "

            ds = SqlHelper.ExecuteDataset(str_conn.Trim, CommandType.Text, strQuery)

            If ds.Tables(0).Rows.Count > 0 Then

                Dim checkbox As New CheckBoxList
                checkbox.DataSource = ds
                checkbox.DataTextField = "sub_monitor_des"
                checkbox.DataValueField = "sub_monitor_active"
                checkbox.DataBind()


                For Each item As ListItem In checkbox.Items
                    If item.Value = "True" Then
                        item.Selected = True
                    End If
                Next

                checkbox.Enabled = False

                Dim place As PlaceHolder = DirectCast(row.FindControl("PlaceSub"), PlaceHolder)
                place.Controls.Add(checkbox)

            End If



        Next

    End Sub

    Protected Sub T1GrdMonitoring_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles T1GrdMonitoring.RowCommand

        If e.CommandName = "GoDep" Then
            Session("TabIndex") = e.CommandArgument.ToString().Substring(1) - 1
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Encrypt(Hiddenstuid.Value)
            Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
            Response.Redirect("senStudentProfile.aspx?Stu_id=" & stu_id & mInfo)

        End If

    End Sub
End Class
