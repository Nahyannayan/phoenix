<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senEnrichment.ascx.vb" Inherits="SenRegister_UserControls_senEnrichment" %>
<asp:HiddenField ID="Hiddenstuid" runat="server" />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:Label ID="lblstatusmessage" runat="server" EnableTheming="False" Font-Bold="True"
    Font-Size="Larger" ForeColor="Red"></asp:Label>&nbsp;<br />
<br />
<div class="matters">
 <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700px" >
                        <tr>
                            <td class="subheader_img">
                                Enrichment Details</td>
                        </tr>
                        <tr>
                            <td >
<table>
    <tr>
        <td>
            <asp:Label ID="T11lbldateenteredlabel" runat="server"></asp:Label></td>
        <td>
            :</td>
        <td>
            <asp:Label ID="T11lbldateentered" runat="server"></asp:Label></td>
        <td>
            Date Entered Enrichment</td>
        <td>
            :</td>
        <td>
            <asp:Label ID="T11lblDateenteredenrichment" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>
            Time Lag</td>
        <td>
            :</td>
        <td>
            <asp:Label ID="T11lblTimeLag" runat="server"></asp:Label></td>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
</table>

 </td>
                    </tr>
               </table>
<br />
<asp:LinkButton ID="T11lnkview1"  OnClientClick="javascript:return false;" runat="server">Enter Enrichment Details</asp:LinkButton>
<br />
<br />
<asp:Panel ID="T11Panel1" runat="server" Height="50px" >
 <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700" >
                        <tr>
                            <td class="subheader_img">
                                Enter Enrichment Details</td>
                        </tr>
                        <tr>
                            <td >
<table>
    <tr>
        <td >
            Enrichment Category</td>
        <td >
            <asp:DropDownList ID="T11ddENRICHCategory" runat="server" ValidationGroup="T11comments">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td >
            Author</td>
        <td style="width: 100px"><asp:DropDownList ID="T11ddStaffs" runat="server" ValidationGroup="T11comments">
        </asp:DropDownList></td>
    </tr>
    <tr>
        <td >
            Materials Used/Comments</td>
        <td >
            <asp:TextBox ID="T11txtmatcomments" runat="server" Height="200px" SkinID="MultiText"
                TextMode="MultiLine" ValidationGroup="T11comments" Width="500px"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="center" colspan="3">
            <asp:Button ID="T11btnsave" runat="server" CssClass="button" Text="Save" ValidationGroup="T11comments" /></td>
    </tr>
</table>


 </td>
                    </tr>
               </table>
</asp:Panel>
<ajaxToolkit:CollapsiblePanelExtender ID="T11CollapsiblePanelExtender4" runat="server"
    AutoCollapse="False" AutoExpand="False" CollapseControlID="T11lnkview1" Collapsed="true"
    CollapsedSize="0" CollapsedText="Enter Enrichment Details" ExpandControlID="T11lnkview1" ExpandedSize="200"
    ExpandedText="Hide" ScrollContents="false" TargetControlID="T11Panel1" TextLabelID="T11lnkview1">
</ajaxToolkit:CollapsiblePanelExtender>
<br />
<asp:RequiredFieldValidator ID="T11RequiredFieldValidator1" runat="server" ControlToValidate="T11ddENRICHCategory"
    Display="None" ErrorMessage="Please Select Enrichment Category" InitialValue="-1" SetFocusOnError="True"
    ValidationGroup="T11comments"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="T11RequiredFieldValidator2" runat="server" ControlToValidate="T11ddStaffs"
    Display="None" ErrorMessage="Please Select Author" SetFocusOnError="True" ValidationGroup="T11comments" InitialValue="-1"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="T11RequiredFieldValidator3" runat="server" ControlToValidate="T11txtmatcomments"
    Display="None" ErrorMessage="Please Enter Comments" SetFocusOnError="True" ValidationGroup="T11comments"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
    ShowSummary="False" ValidationGroup="T11comments" />
<br />
<br />
<br />
<asp:CheckBox ID="T11CheckBox1" runat="server" AutoPostBack="True" Text="Literacy Enp Term 1" />&nbsp;<br />
<br />
<asp:GridView ID="T11GridView1" AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server" AllowPaging="True" PageSize="5" Visible="False" Width="700px">
<Columns>
<asp:TemplateField HeaderText="Date">
<ItemTemplate>
<%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Materials Used/Comments">
<ItemTemplate>
  <asp:Label ID="T11lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
   <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
    <asp:Panel ID="T11Panel1" runat="server" Height="50px">
    <%#Eval("MATERIALS_COMMENT_TEXT")%>
    </asp:Panel>
<ajaxToolkit:CollapsiblePanelExtender ID="T11CollapsiblePanelExtender1" runat="server"
    AutoCollapse="False" AutoExpand="False" CollapseControlID="T11lblview"
    Collapsed="true" CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T11lblview"
    ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true"  TargetControlID="T11Panel1"
    TextLabelID="T11lblview">
</ajaxToolkit:CollapsiblePanelExtender>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Author">
<ItemTemplate>
<%#Eval("empname")%>
</ItemTemplate>
</asp:TemplateField>
</Columns>
 <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
</asp:GridView>
<br />
<asp:CheckBox ID="T11CheckBox2" runat="server" AutoPostBack="True" Text="Maths EnP Term 2" /><br />
<br />
<asp:GridView ID="T11GridView2" AllowPaging="True" PageSize="5" Visible="False" AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server" Width="700px">
    <Columns>
        <asp:TemplateField HeaderText="Date">
            <ItemTemplate>
                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Materials Used/Comments">
            <ItemTemplate>
                <asp:Label ID="T11lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                <asp:Panel ID="T11Panel1" runat="server" Height="50px">
                    <%#Eval("MATERIALS_COMMENT_TEXT")%>
                </asp:Panel>
                <ajaxToolkit:CollapsiblePanelExtender ID="T11CollapsiblePanelExtender1" runat="server"
    AutoCollapse="False" AutoExpand="False" CollapseControlID="T11lblview"
    Collapsed="true" CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T11lblview"
    ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true"  TargetControlID="T11Panel1"
    TextLabelID="T11lblview">
                </ajaxToolkit:CollapsiblePanelExtender>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Author">
            <ItemTemplate>
                <%#Eval("empname")%>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
     <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                     <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                     <SelectedRowStyle CssClass="Green" Wrap="False" />
                     <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" BackColor="White" />
                     <EmptyDataRowStyle Wrap="False" />
                     <EditRowStyle Wrap="False" />
</asp:GridView>
<br />
<asp:CheckBox ID="T11CheckBox3" runat="server" AutoPostBack="True" Text="Literacy Booster Term 2" /><br />
<br />
<asp:GridView ID="T11GridView3" AllowPaging="True" PageSize="5" Visible="False" AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server" Width="700px">
    <Columns>
        <asp:TemplateField HeaderText="Date">
            <ItemTemplate>
                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Materials Used/Comments">
            <ItemTemplate>
                <asp:Label ID="T11lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                <asp:Panel ID="T11Panel1" runat="server" Height="50px">
                    <%#Eval("MATERIALS_COMMENT_TEXT")%>
                </asp:Panel>
                <ajaxToolkit:CollapsiblePanelExtender ID="T11CollapsiblePanelExtender1" runat="server"
    AutoCollapse="False" AutoExpand="False" CollapseControlID="T11lblview"
    Collapsed="true" CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T11lblview"
    ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true"  TargetControlID="T11Panel1"
    TextLabelID="T11lblview">
                </ajaxToolkit:CollapsiblePanelExtender>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Author">
            <ItemTemplate>
                <%#Eval("empname")%>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
     <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
</asp:GridView>
<br />
<asp:CheckBox ID="T11CheckBox4" runat="server" AutoPostBack="True" Text="Literacy Booster Term 3" /><br />
&nbsp;<asp:GridView ID="T11GridView4" AllowPaging="True" PageSize="5" Visible="False" AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server" Width="700px">
    <Columns>
        <asp:TemplateField HeaderText="Date">
            <ItemTemplate>
                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Materials Used/Comments">
            <ItemTemplate>
                <asp:Label ID="T11lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                <asp:Panel ID="T11Panel1" runat="server" Height="50px">
                    <%#Eval("MATERIALS_COMMENT_TEXT")%>
                </asp:Panel>
                <ajaxToolkit:CollapsiblePanelExtender ID="T11CollapsiblePanelExtender1" runat="server"
    AutoCollapse="False" AutoExpand="False" CollapseControlID="T11lblview"
    Collapsed="true" CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T11lblview"
    ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true"  TargetControlID="T11Panel1"
    TextLabelID="T11lblview">
                </ajaxToolkit:CollapsiblePanelExtender>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Author">
            <ItemTemplate>
                <%#Eval("empname")%>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
     <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
</asp:GridView>
<br />
<asp:CheckBox ID="T11CheckBox5" runat="server" AutoPostBack="True" Text="Maths Booster Term 2" /><br />
&nbsp;<asp:GridView ID="T11GridView5" AllowPaging="True" PageSize="5" Visible="False" AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server" Width="700px">
    <Columns>
        <asp:TemplateField HeaderText="Date">
            <ItemTemplate>
                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Materials Used/Comments">
            <ItemTemplate>
                <asp:Label ID="T11lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                <asp:Panel ID="T11Panel1" runat="server" Height="50px">
                    <%#Eval("MATERIALS_COMMENT_TEXT")%>
                </asp:Panel>
                <ajaxToolkit:CollapsiblePanelExtender ID="T11CollapsiblePanelExtender1" runat="server"
    AutoCollapse="False" AutoExpand="False" CollapseControlID="T11lblview"
    Collapsed="true" CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T11lblview"
    ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true"  TargetControlID="T11Panel1"
    TextLabelID="T11lblview">
                </ajaxToolkit:CollapsiblePanelExtender>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Author">
            <ItemTemplate>
                <%#Eval("empname")%>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
</asp:GridView>
<br />
<asp:CheckBox ID="T11CheckBox6" runat="server" AutoPostBack="True" Text="Maths Booster Term 3" /><br />
&nbsp;<asp:GridView ID="T11GridView6" AllowPaging="True" PageSize="5" Visible="False" AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server" Width="700px">
    <Columns>
        <asp:TemplateField HeaderText="Date">
            <ItemTemplate>
                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Materials Used/Comments">
            <ItemTemplate>
                <asp:Label ID="T11lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                <asp:Panel ID="T11Panel1" runat="server" Height="50px">
                    <%#Eval("MATERIALS_COMMENT_TEXT")%>
                </asp:Panel>
                <ajaxToolkit:CollapsiblePanelExtender ID="T11CollapsiblePanelExtender1" runat="server"
    AutoCollapse="False" AutoExpand="False" CollapseControlID="T11lblview"
    Collapsed="true" CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T11lblview"
    ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true"  TargetControlID="T11Panel1"
    TextLabelID="T11lblview">
                </ajaxToolkit:CollapsiblePanelExtender>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Author">
            <ItemTemplate>
                <%#Eval("empname")%>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
     <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
</asp:GridView>
&nbsp;&nbsp;<br />
<asp:LinkButton ID="T11lnkview2"  OnClientClick="javascript:return false;" runat="server">Enter Notes</asp:LinkButton>
<br />
<br />
<asp:Panel ID="T11Panel2" runat="server" Height="50px" >
 <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700px" >
                        <tr>
                            <td class="subheader_img">
                                Enter Notes</td>
                        </tr>
                        <tr>
                            <td >
<table>
    <tr>
        <td >
            Author</td>
        <td >
            <asp:DropDownList ID="T11ddstaffnotes" runat="server" ValidationGroup="T11notes">
            </asp:DropDownList></td>
        <td >
        </td>
    </tr>
    <tr>
        <td colspan="3">
            Comments</td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:TextBox ID="T11txtnotes" runat="server" Height="200px" SkinID="MultiText" TextMode="MultiLine"
                ValidationGroup="T11notes" Width="500px"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="center" colspan="3" style="height: 26px">
            <asp:Button ID="T11btnnotessave" CssClass="button" runat="server" Text="Save" ValidationGroup="T11notes" /></td>
    </tr>
</table>



 </td>
                    </tr>
               </table>
</asp:Panel><ajaxToolkit:CollapsiblePanelExtender ID="T11CollapsiblePanelExtender1" runat="server"
    AutoCollapse="False" AutoExpand="False" CollapseControlID="T11lnkview2" Collapsed="true"
    CollapsedSize="0" CollapsedText="Enter Notes" ExpandControlID="T11lnkview2" ExpandedSize="200"
    ExpandedText="Hide" ScrollContents="false" TargetControlID="T11Panel2" TextLabelID="T11lnkview2">
</ajaxToolkit:CollapsiblePanelExtender>
 <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700px" >
                        <tr>
                            <td class="subheader_img">
                                Notes</td>
                        </tr>
                        <tr>
                            <td >
<asp:GridView ID="T11GrdNotes" AutoGenerateColumns="false" EmptyDataText="No Notes added yet" runat="server" AllowPaging="True" Width="100%">
<Columns>
<asp:TemplateField HeaderText="Date">
<ItemTemplate>
<%# Eval("ENTRY_DATE","{0:dd/MMM/yyyy}") %>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Comment">
<ItemTemplate>
 <asp:Label ID="T11lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
 <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                            <asp:Panel ID="T11Panel1" runat="server" Height="50px">
                                <%#Eval("ENRICH_NOTE")%>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="T11CollapsiblePanelExtender1" runat="server"
                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T11lblview" Collapsed="true"
                                CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T11lblview"
                                ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T11Panel1"
                                TextLabelID="T11lblview">
                            </ajaxToolkit:CollapsiblePanelExtender>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Author">
<ItemTemplate>
<%#Eval("empname")%>
</ItemTemplate>
</asp:TemplateField>
</Columns>
  <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
</asp:GridView>

 </td>
                    </tr>
               </table>
<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="None"
    ErrorMessage="Please Select Author" SetFocusOnError="True" ControlToValidate="T11ddstaffnotes" InitialValue="-1" ValidationGroup="T11notes"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="None"
    ErrorMessage="Please Enter Notes" SetFocusOnError="True" ControlToValidate="T11txtnotes" ValidationGroup="T11notes"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
    ShowSummary="False" ValidationGroup="T11notes" />
</div>