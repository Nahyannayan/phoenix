Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senMonitoringAccess
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindControl()
            LockFileCheck()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(MonitoringSave)
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then
            Panel2.Enabled = False
        End If

    End Sub
    Public Sub BindControl()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select active, MONITORING_DATE,MONITOR_NOTES from SEN_DEPARTMENT_MONITORING  " & _
                                 " where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' and DEPARTMENT_ID='2'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn.Trim, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            MonitoringCheckBox.Checked = ds.Tables(0).Rows(0).Item("active")
            If MonitoringCheckBox.Checked Then
                txtdatemonotor.Text = ds.Tables(0).Rows(0).Item("MONITORING_DATE").ToString()
                txtMonitornotes.Text = ds.Tables(0).Rows(0).Item("MONITOR_NOTES").ToString()
            End If
        Else            
            MonitoringCheckBox.Checked = False
        End If


    End Sub
    Public Sub Monitorsave()
        Dim active = False
        Dim MDate = txtdatemonotor.Text.Trim()
        If MonitoringCheckBox.Checked Then
            active = True
        Else
            active = False
            MDate = ""
        End If
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@DEPARTMENT_ID", "2")
        pParms(3) = New SqlClient.SqlParameter("@MONITORING_DATE", MDate)
        pParms(4) = New SqlClient.SqlParameter("@ACTIVE", active)
        pParms(5) = New SqlClient.SqlParameter("@MONITOR_NOTES", txtMonitornotes.Text.Trim())
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_MONITORING", pParms)
        Session("TabIndex") = 2
        'Dim Encr_decrData As New Encryption64
        'Dim stu_id As String = Encr_decrData.Encrypt(Hiddenstuid.Value)
        'Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        'Response.Redirect("senStudentProfile.aspx?Stu_id=" & stu_id & mInfo)
        Response.Write("<script type='text/javascript'> parent.location.reload() </script>")
    End Sub
    Protected Sub MonitoringSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MonitoringSave.Click
        Monitorsave()

    End Sub

    'Protected Sub MonitoringCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MonitoringCheckBox.CheckedChanged
    '    If MonitoringCheckBox.Checked Then
    '        TD1.Visible = True
    '    Else
    '        TD1.Visible = False
    '        Monitorsave()
    '    End If
    'End Sub
End Class
