Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senInclusionTracker
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindMainData()
            BindInterventions()
            BindControls()
            BindInterventionTracking()
            BindGroupWorkGrid()
            BindVerbNonVerbGrid()
            BindSATS()
            BindInfo()
            bindbsushort()
            BindLucid()
            LockFileCheck()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(T8linkPathInfo)
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then

            T8btnSaveonfo.Enabled = False
            T8RadioButtonListPIVATS.Enabled = False
            T8satssave.Enabled = False
            T8satssave.Enabled = False
            T8lnkTracking.Enabled = False
            T8btnsave.Enabled = False
            T8gpSave.Enabled = False
            btnlucidsave.Enabled = False
            T8btnvernverb.Enabled = False
        End If

    End Sub
    Public Sub BindLucid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_INCLUSION_TRACKER_LUCID where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "' order by LUCID_DATE desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        GridLUCID.DataSource = ds
        GridLUCID.DataBind()
        txtluciddate.Text = ""
        txtlucidppss.Text = ""
        txtlucidasmss.Text = ""
        txtlucidvvmss.Text = ""

    End Sub
    Public Sub bindbsushort()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim strQuery As String = "select BSU_SHORTNAME from student_m " & _
                                 " inner join student_d on stu_id=sts_stu_id inner join BUSINESSUNIT_M on bsu_id=stu_bsu_id where stu_id='" & Hiddenstuid.Value & "'"
        T8BsuLabel.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)
    End Sub
    Public Sub BindInfo()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_INCLUSION_TRACKER_PIVATS_INFO where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            T8Hiddeninfo.Value = ds.Tables(0).Rows(0).Item("PIVATS_FILE_INFO").ToString()
            txtinfo.Text = ds.Tables(0).Rows(0).Item("PIVATS_FILE_INFO").ToString()
            T8CheckPIVATS.Checked = True
            'T8linkPathInfo.Visible = True
        Else
            T8linkPathInfo.Visible = False
            T8CheckPIVATS.Checked = False
        End If

        If txtinfo.Text.Trim() = "" Then
            T8CheckPIVATS.Checked = False
            T8linkPathInfo.Visible = False
        End If





    End Sub
    Public Sub BindSATS()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_INCLUSION_TRACKER_SATS where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T8GrdSats.DataSource = ds
        T8GrdSats.DataBind()
        For Each row As GridViewRow In T8GrdSats.Rows
            Dim satsid As String = DirectCast(row.FindControl("T8HiddenFieldSATSID"), HiddenField).Value
            ''Reading 
            strQuery = "select READING_TA,READING_EXAM from SEN_DEPARTMENT_INCLUSION_TRACKER_SATS_READING where SATS_ID='" & satsid & "' "
            Dim ReadingGrid As GridView = DirectCast(row.FindControl("T8GrdSatsReading"), GridView)
            ReadingGrid.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
            ReadingGrid.DataBind()
            ''Writing
            strQuery = "select WRITING_TA,WRITING_EXAM from SEN_DEPARTMENT_INCLUSION_TRACKER_SATS_WRITING where SATS_ID='" & satsid & "' "
            Dim WritingGrid As GridView = DirectCast(row.FindControl("T8GrdSatsWriting"), GridView)
            WritingGrid.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
            WritingGrid.DataBind()
            ''Maths
            strQuery = "select MATHS_TA,MATHS_EXAM from SEN_DEPARTMENT_INCLUSION_TRACKER_SATS_MATHS where SATS_ID='" & satsid & "' "
            Dim MathsGrid As GridView = DirectCast(row.FindControl("T8GrdSatsMaths"), GridView)
            MathsGrid.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
            MathsGrid.DataBind()

        Next

    End Sub
    Public Sub BindVerbNonVerbGrid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_INCLUSION_TRACKER_VERB_NON_VERB where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T8Grdverbnonverb.DataSource = ds
        T8Grdverbnonverb.DataBind()
    End Sub
    Public Sub BindMainData()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_INCLUSION_TRACKER_MAIN where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            'T8txtnfersdate.Text = ds.Tables(0).Rows(0).Item("NFERS_DATE").ToString()
            If ds.Tables(0).Rows(0).Item("PIVATS").ToString() = "True" Then
                T8RadioButtonListPIVATS.SelectedIndex = 0
            Else
                T8RadioButtonListPIVATS.SelectedIndex = 1
            End If

        End If
      
    End Sub
    Public Sub BindGroupWorkGrid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(RESULT_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview,START_DATE,END_DATE,GROUP_TYPE,RESULT_TEXT,ENTRY_DATE from SEN_DEPARTMENT_INCLUSION_TRACKER_GROUP_WORK where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'order by ENTRY_DATE desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T8GrdGroupWork.DataSource = ds
        T8GrdGroupWork.DataBind()

    End Sub


    Public Sub BindInterventions()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_INCLUSION_TRACKER_INTERVENTION_MAIN WHERE CATEGORY_BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T8CheckBoxListCategory.DataSource = ds
        T8CheckBoxListCategory.DataTextField = "CATEGORY_DES"
        T8CheckBoxListCategory.DataValueField = "CATEGORY_ID"
        T8CheckBoxListCategory.DataBind()
        'strQuery = "select * from SEN_DEPARTMENT_INCLUSION_TRACKER_INTERVENTION_SUB"
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        'T8CheckBoxListSubCategory.DataSource = ds
        'T8CheckBoxListSubCategory.DataTextField = "SUB_CATEGORY_DES"
        'T8CheckBoxListSubCategory.DataValueField = "SUB_CATEGORY_ID"
        'T8CheckBoxListSubCategory.DataBind()
    End Sub
    Public Sub BindControls()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_INCLUSION_TRACKER_CATEGORY where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        Dim flag = 0
        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim catid = ds.Tables(0).Rows(i).Item("CATEGORY_ID").ToString()
                For Each item As ListItem In T8CheckBoxListCategory.Items
                    If item.Value = catid Then
                        item.Selected = True
                        'If item.Value = "13" Then
                        '    flag = 1
                        'End If
                    End If
                Next
            Next
            'For i = 0 To ds.Tables(0).Rows.Count - 1
            '    Dim catid = ds.Tables(0).Rows(i).Item("CATEGORY_ID").ToString()
            '    For Each item As ListItem In T8CheckBoxListSubCategory.Items
            '        If item.Value = catid Then
            '            item.Selected = True
            '        End If
            '    Next
            'Next
        End If
        BindDropdownInterventions()
    End Sub
    Public Sub BindDropdownInterventions()
        T8ddIntervention.Items.Clear()
        Dim dt As New DataTable
        dt.Columns.Add("ID")
        dt.Columns.Add("Intervention")
        For Each item As ListItem In T8CheckBoxListCategory.Items
            If item.Selected Then 'And item.Value <> "13"
                Dim dr As DataRow = dt.NewRow()
                dr.Item("ID") = item.Value
                dr.Item("Intervention") = item.Text
                dt.Rows.Add(dr)
            End If
        Next
        'For Each item As ListItem In T8CheckBoxListSubCategory.Items
        '    If item.Selected Then
        '        Dim dr As DataRow = dt.NewRow()
        '        dr.Item("ID") = item.Value
        '        dr.Item("Intervention") = "SRA -" & item.Text
        '        dt.Rows.Add(dr)
        '    End If
        'Next
        T8ddIntervention.DataSource = dt
        T8ddIntervention.DataTextField = "Intervention"
        T8ddIntervention.DataValueField = "ID"
        T8ddIntervention.DataBind()
        Dim list As New ListItem
        list.Text = "Select a Interventions"
        list.Value = "-1"
        T8ddIntervention.Items.Insert(0, list)
    End Sub

    Public Sub BindInterventionTracking()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select START_DATE,END_DATE,CATEGORY_DES,OUTCOME_TEXT,ENTRY_DATE FROM SEN_DEPARTMENT_INCLUSION_TRACKER_INTERVENTION_TRACKING ab " & _
                                 " INNER JOIN SEN_DEPARTMENT_INCLUSION_TRACKER_CATEGORY a on ab.INTERVENTION_ID= a.CATEGORY_ID " & _
                                 " left join SEN_DEPARTMENT_INCLUSION_TRACKER_INTERVENTION_MAIN b on convert(varchar,a.CATEGORY_ID)=convert(varchar,b.CATEGORY_ID) " & _
                                 " where ab.STU_ID='" & Hiddenstuid.Value & "' and ab.BSU_ID='" & Hiddenbsuid.Value & "' order by ab.entry_date desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T8GrdInterTracking.DataSource = ds
        T8GrdInterTracking.DataBind()


    End Sub
    Protected Sub T8lnkTracking_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T8lnkTracking.Click
        lblstatusmessage.Text = ""

        Try

      
            Dim flag = 0
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString

            For Each item As ListItem In T8CheckBoxListCategory.Items

                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
                pParms(2) = New SqlClient.SqlParameter("@CATEGORY_ID", item.Value)
                'If item.Value = "13" Then
                '    flag = 1
                '    pParms(3) = New SqlClient.SqlParameter("@SUB_CATEGORY", "True")
                'Else
                '    pParms(3) = New SqlClient.SqlParameter("@SUB_CATEGORY", "False")

                'End If

                pParms(3) = New SqlClient.SqlParameter("@SUB_CATEGORY", "False") '' Comment this if above comment is removed

                pParms(4) = New SqlClient.SqlParameter("@SELECTED", item.Selected)
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_INCLUSION_TRACKER_CATEGORY", pParms)


            Next

            'If flag = 1 Then
            '    For Each item As ListItem In T8CheckBoxListSubCategory.Items
            '        Dim pParms(6) As SqlClient.SqlParameter
            '        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            '        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            '        pParms(2) = New SqlClient.SqlParameter("@CATEGORY_ID", item.Value)
            '        pParms(3) = New SqlClient.SqlParameter("@MAIN_CATEGORY_ID", "13")
            '        pParms(4) = New SqlClient.SqlParameter("@SUB_CATEGORY", "False")
            '        pParms(5) = New SqlClient.SqlParameter("@SELECTED", item.Selected)
            '        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_INCLUSION_TRACKER_CATEGORY", pParms)

            '    Next
            'End If
            BindControls()

            lblstatusmessage.Text = "Successfully Saved"
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub T8btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T8btnsave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@START_DATE", T8txtStartDate.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@END_DATE", T8txtEndDate.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@INTERVENTION_ID", T8ddIntervention.SelectedValue)
            pParms(5) = New SqlClient.SqlParameter("@OUTCOME_TEXT", T8txtOutCome.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_INCLUSION_TRACKER_INTERVENTION_TRACKING", pParms)
            T8txtStartDate.Text = ""
            T8txtEndDate.Text = ""
            T8ddIntervention.SelectedIndex = 0
            T8txtOutCome.Text = ""
            BindInterventionTracking()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T8gpSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T8gpSave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@START_DATE", T8txtgpstaartdate.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@END_DATE", T8txtgpenddate.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@GROUP_TYPE", T8txtgrouptype.Text.Trim())
            pParms(5) = New SqlClient.SqlParameter("@RESULT_TEXT", T8txtresult.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_INCLUSION_TRACKER_GROUP_WORK", pParms)
            BindGroupWorkGrid()
            T8txtgpstaartdate.Text = ""
            T8txtgpenddate.Text = ""
            T8txtgrouptype.Text = ""
            T8txtresult.Text = ""
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try


    End Sub

  

    Protected Sub T8GrdGroupWork_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T8GrdGroupWork.PageIndexChanging
        T8GrdGroupWork.PageIndex = e.NewPageIndex
        BindGroupWorkGrid()
    End Sub

    Protected Sub T8GrdInterTracking_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T8GrdInterTracking.PageIndexChanging
        T8GrdInterTracking.PageIndex = e.NewPageIndex
        BindInterventionTracking()
    End Sub

    

    Protected Sub T8btnvernverb_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T8btnvernverb.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@VERB", T8txtverbal.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@NON_VERB", T8txtnonverbal.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_INCLUSION_TRACKER_VERB_NON_VERB", pParms)
            T8txtverbal.Text = ""
            T8txtnonverbal.Text = ""
            BindVerbNonVerbGrid()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T8satssave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T8satssave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@YEAR", T8txtsatsYear.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@CLASS", T8txtsatsclass.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@READING_TA", T8txtsatsreadingta.Text.Trim())
            pParms(5) = New SqlClient.SqlParameter("@READING_EXAM", T8txtsatsreadingexam.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@WRITING_TA", T8txtsatswritingta.Text.Trim())
            pParms(7) = New SqlClient.SqlParameter("@WRITING_EXAM", T8txtsatswritingexam.Text.Trim())
            pParms(8) = New SqlClient.SqlParameter("@MATHS_TA", T8txtsatsmathsta.Text.Trim())
            pParms(9) = New SqlClient.SqlParameter("@MATHS_EXAM", T8txtsatsmathsexam.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_INCLUSION_TRACKER_SATS", pParms)
            T8txtsatsYear.Text = ""
            T8txtsatsclass.Text = ""
            T8txtsatsreadingta.Text = ""
            T8txtsatsreadingexam.Text = ""
            T8txtsatswritingta.Text = ""
            T8txtsatswritingexam.Text = ""
            T8txtsatsmathsta.Text = ""
            T8txtsatsmathsexam.Text = ""
            BindSATS()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T8Grdverbnonverb_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T8Grdverbnonverb.PageIndexChanging
        T8Grdverbnonverb.PageIndex = e.NewPageIndex
        BindVerbNonVerbGrid()
    End Sub

    Protected Sub T8GrdSats_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T8GrdSats.PageIndexChanging
        T8GrdSats.PageIndex = e.NewPageIndex
        BindSATS()
    End Sub

    'Protected Sub T8CheckPIVATS_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T8CheckPIVATS.CheckedChanged
    '    If T8CheckPIVATS.Checked Then
    '        txtinfo.Visible = True
    '        T8btnSaveonfo.Visible = True
    '        T8linkPathInfo.Visible = False
    '    Else
    '        txtinfo.Visible = False
    '        T8btnSaveonfo.Visible = False
    '        T8linkPathInfo.Visible = False
    '    End If

    'End Sub

    Protected Sub T8btnSaveonfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T8btnSaveonfo.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@PIVATS_FILE_INFO", txtinfo.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_INCLUSION_TRACKER_PIVATS_INFO", pParms)
            BindInfo()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub


    'Protected Sub T8linkPathInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T8linkPathInfo.Click
    '    Try
    '        Dim path = T8Hiddeninfo.Value
    '        HttpContext.Current.Response.ContentType = "application/octect-stream"
    '        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
    '        HttpContext.Current.Response.Clear()
    '        HttpContext.Current.Response.WriteFile(path)
    '        HttpContext.Current.Response.End()
    '    Catch ex As Exception

    '    End Try

    'End Sub

    Protected Sub T8RadioButtonListPIVATS_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T8RadioButtonListPIVATS.SelectedIndexChanged
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@PIVATS", T8RadioButtonListPIVATS.SelectedValue)
        'pParms(3) = New SqlClient.SqlParameter("@NFERS_DATE", T8txtnfersdate.Text.Trim())
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_INCLUSION_TRACKER_MAIN", pParms)
        BindMainData()
    End Sub

   
    Protected Sub btnlucidsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlucidsave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@LUCID_DATE", txtluciddate.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@PP_SS", txtlucidppss.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@ASM_SS", txtlucidasmss.Text.Trim())
            pParms(5) = New SqlClient.SqlParameter("@VVM_SS", txtlucidvvmss.Text.Trim())

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_INCLUSION_TRACKER_LUCID", pParms)
            BindLucid()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GridLUCID_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridLUCID.PageIndexChanging
        GridLUCID.PageIndex = e.NewPageIndex
        BindLucid()
    End Sub


End Class
