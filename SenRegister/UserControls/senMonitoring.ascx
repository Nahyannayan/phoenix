<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senMonitoring.ascx.vb" Inherits="SenRegister_UserControls_senMonitoring" %>

<div class="matters">
    <table width="100%">
        <tr>
            <td class="title-bg">Monitoring</td>
        </tr>
        <tr>
            <td>

                <asp:GridView ID="T1GrdMonitoring" AutoGenerateColumns="false" EmptyDataText="Monitoring information not added yet" Width="100%" runat="server" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Monitor">
                            <HeaderTemplate>
                                Monitor
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddenDeptID" Value='<%#Eval("DEPT_ID")%>' runat="server" />
                                <center>   
<asp:CheckBox ID="Check" Checked='<%#Eval("ACTIVE")%>' Enabled="false" runat="server" /></center>
                            </ItemTemplate>
                            <ItemStyle />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Section">
                            <HeaderTemplate>
                              Section
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkSection" Text='<%#Eval("DEPT_DESC")%>' CausesValidation="false" CommandArgument='<%#Eval("DEPT_TAB_ID")%>' CommandName="GoDep" runat="server"></asp:LinkButton>
                                <asp:PlaceHolder ID="PlaceSub" runat="server"></asp:PlaceHolder>
                            </ItemTemplate>
                            <ItemStyle  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Monitor Date">
                            <HeaderTemplate>
                                Monitor Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("MONITORING_DATE")%>
                            </ItemTemplate>
                            <ItemStyle  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Notes">
                            <HeaderTemplate>
                               Notes
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T4lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T4Panel1" runat="server" CssClass="panel-cover" >
                                    <%#Eval("MONITOR_NOTES")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T4CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T4lblview"
                                    Collapsed="true" CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T4lblview"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T4Panel1"
                                    TextLabelID="T4lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False"  />
                    <RowStyle CssClass="griditem"  Wrap="False"  />                    
                    <EmptyDataRowStyle Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <FooterStyle  />
                    <PagerStyle HorizontalAlign="Center" />

                </asp:GridView>


            </td>
        </tr>
    </table>
    <asp:HiddenField ID="Hiddenstuid" runat="server" />
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />

</div>