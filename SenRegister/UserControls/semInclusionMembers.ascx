<%@ Control Language="VB" AutoEventWireup="false" CodeFile="semInclusionMembers.ascx.vb" Inherits="SenRegister_UserControls_semInclusionMembers" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">Inclusion Members</td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GrdView" AutoGenerateColumns="False" EmptyDataText="Members are not assigned" CssClass="table table-bordered table-row"
                Width="100%" runat="server">

                <Columns>
                    <%--<asp:TemplateField HeaderText="ID">
 <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           ID
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>





<ItemTemplate>
            <asp:HiddenField ID="Hiddendeptid" Value='<%#Eval("dept_id")%>' runat="server" />
           <%#Eval("dept_id")%>
        
</ItemTemplate>
</asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Section">
                        <HeaderTemplate>
                            Section
                                       
                        </HeaderTemplate>





                        <ItemTemplate>
                            <asp:HiddenField ID="Hiddendeptid" Value='<%#Eval("dept_id")%>' runat="server" />
                            <%#Eval("dept_desc")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HOD">
                        <HeaderTemplate>
                            HOD
                                       
                        </HeaderTemplate>






                        <ItemTemplate>
                            <%#Eval("empname")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Members">
                        <HeaderTemplate>
                            Members
                                    
                        </HeaderTemplate>





                        <ItemTemplate>
                            <center>    <asp:LinkButton ID="LinkView" runat="server" OnClientClick="javascript:return false;"
            Text="View"></asp:LinkButton></center>

                            <asp:Panel ID="Show" runat="server" BackColor="White" BorderColor="Black" Height="50px">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="title-bg-lite">
                                            <%#Eval("dept_desc")%> - Members</td>
                                    </tr>
                                    <tr>
                                        <td align="left">

                                            <asp:GridView ID="GridMembers" EmptyDataText="Members are not assigned" Width="100%" AutoGenerateColumns="false" runat="server" CssClass="table table-bordered table-row">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Name">
                                                        <HeaderTemplate>
                                                            Name
                                     
                                                        </HeaderTemplate>





                                                        <ItemTemplate>
                                                            <%# Eval("EMP_SALUTE") %>
         &nbsp;
         <%# Eval("EMP_FNAME") %>
         &nbsp;
         <%# Eval("EMP_MNAME") %>
         &nbsp;
         <%# Eval("EMP_LNAME") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Designation">
                                                        <HeaderTemplate>
                                                            Designation
                                    
                                                        </HeaderTemplate>





                                                        <ItemTemplate>
                                                            <%#Eval("DES_DESCR")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle />
                                                <RowStyle CssClass="griditem" />
                                                <SelectedRowStyle />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                                <EmptyDataRowStyle />
                                                <EditRowStyle />

                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <ajaxToolkit:HoverMenuExtender ID="hm2" runat="server" PopupControlID="Show" PopupPosition="Left"
                                TargetControlID="LinkView">
                            </ajaxToolkit:HoverMenuExtender>

                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" />
                <EmptyDataRowStyle />
                <SelectedRowStyle />
                <HeaderStyle />
                <EditRowStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>
        </td>
    </tr>
</table>
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
