Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senWellbeing
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindAuthors()
            BindPrimaryDetails()
            BindControls()
            BindNotes()
            BindWellIEP()
            BindWellBeingGroups()
            BindRelMedHistory()
            BindOtherDetails()
            DepEntry()
            LockFileCheck()
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(T6CheckBoxWELL1)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(T6lnkIEPinfoPath)
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub DepEntry()

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT ENTRY_WELLBEING FROM SEN_ACE_STUDENTS WHERE STU_ID='" & Hiddenstuid.Value & "' "
        Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)

        If Not IsDBNull(val) Then
            T6lblDateenteredwellbeing.Text = Convert.ToDateTime(val).ToString("dd/MMM/yyyy")
            Try
                If T6lblDateenteredwellbeing.Text.Trim() <> "" Then
                    T6lblTimeLag.Text = DateDiff(DateInterval.Day, Convert.ToDateTime(T6lbldateentered.Text), Convert.ToDateTime(T6lblDateenteredwellbeing.Text), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.System) & " Days"
                End If
            Catch ex As Exception

            End Try
        End If


    End Sub
    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then
            T6CheckBoxWELL1.Enabled = False
            T6CheckRelLerhistory.Enabled = False
            T6btnsave.Enabled = False
            btnodsave.Enabled = False
            T6checkIEP.Enabled = False
            T6btIEPinfoSave.Enabled = False
            T6brnsave.Enabled = False
            lnkentrysave.Enabled = False
        End If

    End Sub
    Public Sub BindOtherDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery = "select *  from SEN_DEPARTMENT_WELLBEING_OTHER_DETAILS where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        If ds.Tables(0).Rows.Count > 0 Then
            T6CheckPs.Checked = ds.Tables(0).Rows(0).Item("PARENTAL_SKILLS")
            T6F1.Text = ds.Tables(0).Rows(0).Item("PS_FROM_DATE").ToString()
            T6T1.Text = ds.Tables(0).Rows(0).Item("PS_TO_DATE").ToString()
            T6PaSGroup.Checked = ds.Tables(0).Rows(0).Item("PARENT_CHILD_SUPPORT_GROUP")
            T6F2.Text = ds.Tables(0).Rows(0).Item("PCSG_FROM_DATE").ToString()
            T6T2.Text = ds.Tables(0).Rows(0).Item("PCSG_TO_DATE").ToString()
            T6TPPa.Checked = ds.Tables(0).Rows(0).Item("TRIPLE_P_PARENTING")
            T6F3.Text = ds.Tables(0).Rows(0).Item("TPP_FROM_DATE").ToString()
            T6T3.Text = ds.Tables(0).Rows(0).Item("TPP_TO_DATE").ToString()

        End If

    End Sub
    Public Sub BindRelMedHistory()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery = "select REL_MED_HISTORY  from SEN_DEPARTMENT_WELLBEING_REL_MED_HISTORY where STU_ID='" & Hiddenstuid.Value & "' and BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            T6CheckRelLerhistory.Checked = ds.Tables(0).Rows(0).Item("REL_MED_HISTORY")
        End If
    End Sub

    Public Sub BindWellBeingGroups()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select *,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(ISSUE,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview from SEN_DEPARTMENT_WELLBEING a " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' AND a.BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        GrdWellFGroup.DataSource = ds
        GrdWellFGroup.DataBind()

    End Sub
    Public Sub BindWellIEP()
        T6lnkIEPinfoPath.Visible = False
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_WELLBEING_IEP " & _
                                 " where STU_ID='" & Hiddenstuid.Value & "' AND BSU_ID='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            T6checkIEP.Checked = ds.Tables(0).Rows(0).Item("WELL_IEP")
            HiddenWellIEPInfo.Value = ds.Tables(0).Rows(0).Item("WELL_IEP_FILE_INFO").ToString()
            If HiddenWellIEPInfo.Value <> "" Then
                T6txtwellinfo.Text = ds.Tables(0).Rows(0).Item("WELL_IEP_FILE_INFO").ToString()
                'T6btIEPinfoSave.Visible = False
                'T6lnkIEPinfoPath.Visible = True
            Else
                T6lnkIEPinfoPath.Visible = False
            End If
        End If

    End Sub
    Public Sub BindAuthors()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id and a.EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        T6ddstaffs.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T6ddstaffs.DataTextField = "empname"
        T6ddstaffs.DataValueField = "EMP_ID"
        T6ddstaffs.DataBind()
        Dim list As New ListItem
        list.Text = "Select an Inclusion Member"
        list.Value = "-1"
        T6ddstaffs.Items.Insert(0, list)
    End Sub
    Public Sub BindPrimaryDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select c.stu_doj,b.BSU_NAME,b.BSU_SHORTNAME,ENTRY_DATE from SEN_DEPARTMENT_STUDENTS a " & _
                                 " inner join oasis.dbo.BUSINESSUNIT_M b on a.stu_bsu_id = b.bsu_id " & _
                                 " inner join oasis.dbo.student_m c on c.stu_id= a.stu_id  " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.STU_BSU_ID='" & Hiddenbsuid.Value & "' and a.dept_id='6' and a.active='true' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            T6lbldateenteredlabel.Text = "Date Entered " & ds.Tables(0).Rows(0).Item("BSU_SHORTNAME").ToString()
            T6lbldateentered.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("stu_doj").ToString()).ToString("dd/MMM/yyyy")
          
        End If
    End Sub

    Public Sub BindControls()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        'Dim strQuery As String = "select DATE_COMMENCED,DATE_DISCONTINUED,WELLBEING_FOCUS_GROUP,ISSUE from SEN_DEPARTMENT_WELLBEING a " & _
        '                         " where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "'"
        'Dim ds As DataSet
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        'If ds.Tables(0).Rows.Count > 0 Then
        '    T6txtdatecommenced.Text = ds.Tables(0).Rows(0).Item("DATE_COMMENCED").ToString()
        '    T6txtdatediscontinued.Text = ds.Tables(0).Rows(0).Item("DATE_DISCONTINUED").ToString()
        '    T6txtissues.Text = ds.Tables(0).Rows(0).Item("ISSUE").ToString()
        '    If ds.Tables(0).Rows(0).Item("WELLBEING_FOCUS_GROUP").ToString().Trim() <> "" Then
        '        If ds.Tables(0).Rows(0).Item("WELLBEING_FOCUS_GROUP").ToString() = "True" Then
        '            T6CheckBoxWellFocusGroup.Checked = True
        '        Else
        '            T6CheckBoxWellFocusGroup.Checked = False
        '        End If
        '    Else
        '        T6CheckBoxWellFocusGroup.Checked = False
        '    End If

        '    T6btnsave.Visible = False
        'Else
        '    T6btnsave.Visible = True
        'End If


        ''Check Wellbeing 1.1
        Dim strQuery = "select active  from SEN_DEPARTMENT_STUDENTS where STU_ID='" & Hiddenstuid.Value & "' and STU_BSU_ID='" & Hiddenbsuid.Value & "' and dept_id='7'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("ACTIVE").ToString().Trim() <> "" Then
                If ds.Tables(0).Rows(0).Item("ACTIVE").ToString() = "True" Then
                    T6CheckBoxWELL1.Checked = True
                Else
                    T6CheckBoxWELL1.Checked = False
                End If
            Else
                T6CheckBoxWELL1.Checked = False
            End If
        End If

    End Sub
    Public Sub BindNotes()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(COMMENTS_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, COMMENTS_TEXT,ENTRY_DATE,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from SEN_DEPARTMENT_WELLBEING_COMMENTS a " & _
                                 " inner join oasis.dbo.employee_m b on a.entry_emp_id=b.emp_id " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "' order by a.entry_date desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T6GrdComments.DataSource = ds
        T6GrdComments.DataBind()

    End Sub
    Protected Sub T6btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T6btnsave.Click
        lblstatusmessage.Text = ""

        Try
            Dim focusgroup = "False"
            Dim wellbeing1 = "False"
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@DATE_COMMENCED", T6txtdatecommenced.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@DATE_DISCONTINUED", T6txtdatediscontinued.Text.Trim())
            If T6CheckBoxWellFocusGroup.Checked Then
                focusgroup = "True"
            End If
            pParms(4) = New SqlClient.SqlParameter("@WELLBEING_FOCUS_GROUP", focusgroup)
            pParms(5) = New SqlClient.SqlParameter("@ISSUE", T6txtissues.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@GROUPS", T6txtGroups.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_WELLBEING", pParms)
            T6txtdatecommenced.Text = ""
            T6txtdatediscontinued.Text = ""
            T6CheckBoxWellFocusGroup.Checked = False
            T6txtissues.Text = ""
            T6txtGroups.Text = ""

            BindWellBeingGroups()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T6brnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T6brnsave.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@COMMENT_TEXT", T6txtcomments.Text.Trim())
        pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T6ddstaffs.SelectedValue)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_WELLBEING_COMMENTS", pParms)
        T6txtcomments.Text = ""
        T6ddstaffs.SelectedIndex = 0
        BindNotes()


    End Sub

    Protected Sub T6GrdComments_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T6GrdComments.PageIndexChanging
        T6GrdComments.PageIndex = e.NewPageIndex
        BindNotes()
    End Sub

    Protected Sub T6btIEPinfoSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T6btIEPinfoSave.Click

        Dim val = False
        If T6checkIEP.Checked Then
            val = True
        End If
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@WELL_IEP", val)
        pParms(3) = New SqlClient.SqlParameter("@WELL_IEP_FILE_INFO", T6txtwellinfo.Text.Trim())
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_WELLBEING_IEP", pParms)
        BindWellIEP()
    End Sub

    Protected Sub T6checkIEP_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T6checkIEP.CheckedChanged

        Dim val = False
        If T6checkIEP.Checked Then
            val = True
        End If
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@WELL_IEP", val)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_WELLBEING_IEP", pParms)
        BindWellIEP()
    End Sub

    'Protected Sub T6lnkIEPinfoPath_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T6lnkIEPinfoPath.Click
    '    Try
    '        Dim path = HiddenWellIEPInfo.Value
    '        HttpContext.Current.Response.ContentType = "application/octect-stream"
    '        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
    '        HttpContext.Current.Response.Clear()
    '        HttpContext.Current.Response.WriteFile(path)
    '        HttpContext.Current.Response.End()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Protected Sub T6CheckBoxWELL1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T6CheckBoxWELL1.CheckedChanged
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim wellbeing1 = "False"
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        If T6CheckBoxWELL1.Checked Then
            wellbeing1 = "True"
        End If
        pParms(2) = New SqlClient.SqlParameter("@WELLBEING11", wellbeing1)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_WELLBEING_11", pParms)
        Session("TabIndex") = 6
        'Dim Encr_decrData As New Encryption64
        'Dim stu_id As String = Encr_decrData.Encrypt(Hiddenstuid.Value)
        'Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        'Response.Redirect("senStudentProfile.aspx?Stu_id=" & stu_id & mInfo)
        Response.Write("<script type='text/javascript'> parent.location.reload() </script>")
    End Sub

    Protected Sub GrdWellFGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdWellFGroup.PageIndexChanging
        GrdWellFGroup.PageIndex = e.NewPageIndex
        BindWellBeingGroups()
    End Sub

    Protected Sub T6CheckRelLerhistory_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T6CheckRelLerhistory.CheckedChanged
        Dim val = False
        If T6CheckRelLerhistory.Checked Then
            val = True
        End If
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
        pParms(2) = New SqlClient.SqlParameter("@REL_MED_HISTORY", val)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_WELLBEING_REL_MED_HISTORY", pParms)
        BindRelMedHistory()
    End Sub



    Protected Sub btnodsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnodsave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(11) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@PARENTAL_SKILLS", T6CheckPs.Checked)
            pParms(3) = New SqlClient.SqlParameter("@PS_FROM_DATE", T6F1.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@PS_TO_DATE", T6T1.Text.Trim())
            pParms(5) = New SqlClient.SqlParameter("@PARENT_CHILD_SUPPORT_GROUP", T6PaSGroup.Checked)
            pParms(6) = New SqlClient.SqlParameter("@PCSG_FROM_DATE", T6F2.Text.Trim())
            pParms(7) = New SqlClient.SqlParameter("@PCSG_TO_DATE", T6T2.Text.Trim())
            pParms(8) = New SqlClient.SqlParameter("@TRIPLE_P_PARENTING", T6TPPa.Checked)
            pParms(9) = New SqlClient.SqlParameter("@TPP_FROM_DATE", T6F3.Text.Trim())
            pParms(10) = New SqlClient.SqlParameter("@TPP_TO_DATE", T6T3.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_WELLBEING_OTHER_DETAILS", pParms)
            BindOtherDetails()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub lnkentrysave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkentrysave.Click
        lblstatusmessage.Text = ""

        Try
            If T6lblDateenteredwellbeing.Text <> "" Then
                Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
                Dim strQuery As String = "UPDATE SEN_ACE_STUDENTS SET  ENTRY_WELLBEING='" & T6lblDateenteredwellbeing.Text & "' WHERE STU_ID='" & Hiddenstuid.Value & "' "
                Dim val = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
                DepEntry()
                lblstatusmessage.Text = "Successfully Saved"

            End If
        Catch ex As Exception

        End Try

    End Sub
End Class
