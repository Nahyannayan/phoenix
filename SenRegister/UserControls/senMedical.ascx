<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senMedical.ascx.vb" Inherits="SenRegister_UserControls_senMedical" %>
<script type="text/javascript" >

var flag2 = 0

window.setTimeout('hidepanel2();', 1000);

function hidepanel2()
{
    if (flag2 == 0)
    {
    document.getElementById('<%=Panel1.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel2.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel3.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel4.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel5.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel6.ClientID %>').style.visibility = 'hidden'; 
    document.getElementById('<%=Panel7.ClientID %>').style.visibility = 'hidden'; 
    flag2=1

    }
    
    
    
}



function showhide2(checkbox,panel)
{

if (checkbox.checked)
{
        if (panel=='Panel1')
        {
       
        document.getElementById('<%=Panel1.ClientID %>').style.visibility = 'visible'; 
        }
         if (panel=='Panel2')
        {
       
        document.getElementById('<%=Panel2.ClientID %>').style.visibility = 'visible'; 
        }
         if (panel=='Panel3')
        {
       
        document.getElementById('<%=Panel3.ClientID %>').style.visibility = 'visible'; 
        }
         if (panel=='Panel4')
        {
       
        document.getElementById('<%=Panel4.ClientID %>').style.visibility = 'visible'; 
        }
         if (panel=='Panel5')
        {
       
        document.getElementById('<%=Panel5.ClientID %>').style.visibility = 'visible'; 
        }
         if (panel=='Panel6')
        {
       
        document.getElementById('<%=Panel6.ClientID %>').style.visibility = 'visible'; 
        }
         if (panel=='Panel7')
        {
       
        document.getElementById('<%=Panel7.ClientID %>').style.visibility = 'visible'; 
        }
}
else
{

        if (panel=='Panel1')
        {
        document.getElementById('<%=Panel1.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel2')
        {
        document.getElementById('<%=Panel2.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel3')
        {
        document.getElementById('<%=Panel3.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel4')
        {
        document.getElementById('<%=Panel4.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel5')
        {
        document.getElementById('<%=Panel5.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel6')
        {
        document.getElementById('<%=Panel6.ClientID %>').style.visibility = 'hidden'; 
        }
        if (panel=='Panel7')
        {
        document.getElementById('<%=Panel7.ClientID %>').style.visibility = 'hidden'; 
        }

}


}



</script>
<asp:HiddenField ID="Hiddenstuid" runat="server" />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:Label ID="lblstatusmessage" runat="server" EnableTheming="False" CssClass="error"></asp:Label>&nbsp;<br />
<br />
<div>
    <asp:CheckBox ID="T12CheckConfi" runat="server" Text="Confidential Medical Records" AutoPostBack="True" CssClass="field-label" />
    <br />
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="4">Specific Learning Needs</td>
        </tr>
        <tr>
            <td>
                <div class="checkbox-list">
                    <asp:CheckBoxList ID="T12CheckSPL" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                        <%-- <asp:ListItem Value="1">Asthma</asp:ListItem>
                             <asp:ListItem Value="2">Diabetes</asp:ListItem>
                            <asp:ListItem Value="3">ADHD/ADD</asp:ListItem>
                            <asp:ListItem Value="4">Dyspraxia</asp:ListItem>
                            <asp:ListItem Value="5">Dyscalculia</asp:ListItem>
                            <asp:ListItem Value="6">Downs Syndrome</asp:ListItem>
                            <asp:ListItem Value="7">Autism/Aspergers</asp:ListItem>
                            <asp:ListItem Value="8">EBP</asp:ListItem>
                            <asp:ListItem Value="9">OCD</asp:ListItem>
                            <asp:ListItem Value="10">APD</asp:ListItem>
                            <asp:ListItem Value="11">Emotional/Psychological Disturbances</asp:ListItem>
                            <asp:ListItem Value="12">VP</asp:ListItem>
                            <asp:ListItem Value="13">Developmental Delay/s</asp:ListItem>
                            <asp:ListItem Value="14">Non-specific Learning Disorder/s</asp:ListItem>
                            <asp:ListItem Value="15">Gifted</asp:ListItem>
                            <asp:ListItem Value="16">Talented</asp:ListItem>
                            <asp:ListItem Value="17">Eating Disorder</asp:ListItem>
                            <asp:ListItem Value="18">Self-Harming</asp:ListItem>
                            <asp:ListItem Value="19">Physical Impairment </asp:ListItem>
                            <asp:ListItem Value="20">Epilepsy Type</asp:ListItem>
                            <asp:ListItem Value="21">Turrents</asp:ListItem>
                            <asp:ListItem Value="22">Other</asp:ListItem>--%>
                    </asp:CheckBoxList>
                </div>
            </td>
        </tr>
        <tr>
            <td width="20%"><span class="field-label">Physical Impairment</span></td>
            <td width="30%">
                <asp:TextBox ID="T12txtphysicalimpairment" runat="server" ValidationGroup="T12Med"></asp:TextBox></td>
            <td width="20%"><span class="field-label">Epilepsy Type</span></td>
            <td width="30%">
                <asp:TextBox ID="T12txtEpilepsy" runat="server" ValidationGroup="T12Med"></asp:TextBox></td>
        </tr>
        <tr>
            <td width="20%"><span class="field-label">Other</span></td>
            <td width="30%">
                <asp:TextBox ID="T12txtother" runat="server" ValidationGroup="T12Med"></asp:TextBox></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="T12btndave" runat="server" CssClass="button" Text="Save" ValidationGroup="T12Med" CausesValidation="False" /></td>
        </tr>
    </table>
    <%--<asp:RequiredFieldValidator ID="T12RequiredFieldValidator1" runat="server" ControlToValidate="T12txtphysicalimpairment"
    Display="None" ErrorMessage="Please Enter Physical Impairment" SetFocusOnError="True"
    ValidationGroup="T12Med"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="T12RequiredFieldValidator2" runat="server" ControlToValidate="T12txtEpilepsy"
    Display="None" ErrorMessage="Please Enter Epilepsy Type" SetFocusOnError="True"
    ValidationGroup="T12Med"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="T12RequiredFieldValidator3" runat="server" ControlToValidate="T12txtother"
    Display="None" ErrorMessage="Please Enter Other Type" SetFocusOnError="True"
    ValidationGroup="T12Med"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="T12ValidationSummary1" runat="server" ShowMessageBox="True"
    ShowSummary="False" ValidationGroup="T12Med" />--%>
&nbsp;&nbsp;&nbsp;<br />
    <br />
    <asp:LinkButton ID="T12lnkview1" runat="server" OnClientClick="javascript:return false;">Enter Medical Details</asp:LinkButton>
    <br />
    <br />
    <asp:Panel ID="T12Panel1" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4">Enter Medical Details</td>
            </tr>
            <tr>
                <td><span class="field-label">Author</span>
                </td>
                <td>
                    <asp:DropDownList ID="T12ddstaffs" runat="server" ValidationGroup="T12MedDetails">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="T12RequiredFieldValidator0" runat="server" ControlToValidate="T12ddstaffs"
                        Display="None" ErrorMessage="Please Select Author" SetFocusOnError="True"
                        ValidationGroup="T12MedDetails" InitialValue="-1"></asp:RequiredFieldValidator>
                </td>
                <td><span class="field-label">Issue</span>
                </td>
                <td>
                    <asp:DropDownList ID="T12ddIssue" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="-1" Selected="true">Select an Issue</asp:ListItem>
                        <asp:ListItem Value="1">Allergies</asp:ListItem>
                        <asp:ListItem Value="2">Current Medication Plus Medication History</asp:ListItem>
                        <asp:ListItem Value="3">Hearing Issue/s</asp:ListItem>
                        <asp:ListItem Value="4">Vision Issue/s</asp:ListItem>
                        <asp:ListItem Value="5">Bullying Issue/s</asp:ListItem>
                        <asp:ListItem Value="6">Avoidance Issue/s</asp:ListItem>
                        <asp:ListItem Value="7">Motor Issue/s</asp:ListItem>

                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>
                    <table id="T12MAIN" visible="false" runat="server" width="685px">
                        <tr>
                            <td class="title-bg">
                                <asp:Label ID="T12lbltitle" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td>

                                <table id="T12TBD1" visible="False" runat="server" width="100%">
                                    <tr>
                                        <td width="20%"><span class="field-label">Allergy</span></td>
                                        <td width="30%">
                                            <asp:TextBox ID="T12txtallery" runat="server" ValidationGroup="T12MedDetails"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="field-label">Result Of Exposure</span></td>
                                        <td width="30%">
                                            <asp:TextBox ID="T12txtresultofallergy" runat="server"
                                                TextMode="MultiLine" ValidationGroup="T12MedDetails"></asp:TextBox></td>
                                        <td width="20%"><span class="field-label">Treatment</span></td>
                                        <td width="30%">
                                            <asp:TextBox ID="T12txtallertytreatment" runat="server" TextMode="MultiLine"
                                                ValidationGroup="T12MedDetails"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="T12btnallergysave" runat="server" CssClass="button" Text="Save" ValidationGroup="T12MedDetails" /></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:RequiredFieldValidator ID="T12RequiredFieldValidator4" runat="server" ControlToValidate="T12txtallery"
                                                Display="None" ErrorMessage="Please Enter Allergy" SetFocusOnError="True"
                                                ValidationGroup="T12MedDetails"></asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator ID="T12RequiredFieldValidator5" runat="server" ControlToValidate="T12txtresultofallergy"
                                                Display="None" ErrorMessage="Please Enter Result Of Exposure" SetFocusOnError="True"
                                                ValidationGroup="T12MedDetails"></asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator ID="T12RequiredFieldValidator6" runat="server" ControlToValidate="T12txtallertytreatment"
                                                Display="None" ErrorMessage="Please Enter Treatment" SetFocusOnError="True"
                                                ValidationGroup="T12MedDetails"></asp:RequiredFieldValidator>&nbsp;
                                        </td>
                                    </tr>
                                </table>

                                <table id="T12TBD2" visible="False" runat="server" width="100%">
                                    <tr>
                                        <td width="20%"><span class="field-label">Medication Name</span></td>
                                        <td width="30%">
                                            <asp:TextBox ID="T12txtmedicationname" runat="server" ValidationGroup="T12MedDetails"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td width="20%"><span class="field-label">Condition</span>
                                        </td>
                                        <td width="30%">
                                            <asp:TextBox ID="T12txtCondition" runat="server" TextMode="MultiLine"
                                                ValidationGroup="T12MedDetails"></asp:TextBox></td>
                                        <td width="20%"><span class="field-label">Administered at School</span></td>
                                        <td width="30%">
                                            <asp:TextBox ID="T12txtadministeredatschool" runat="server" TextMode="MultiLine"
                                                ValidationGroup="T12MedDetails"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="T12btnmedicationsave" runat="server" CssClass="button" Text="Save" ValidationGroup="T12MedDetails" /></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:RequiredFieldValidator ID="T12RequiredFieldValidator7" runat="server" ControlToValidate="T12txtmedicationname"
                                                Display="None" ErrorMessage="Please Enter Medication Name" SetFocusOnError="True" ValidationGroup="T12MedDetails"></asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator ID="T12RequiredFieldValidator8" runat="server" ControlToValidate="T12txtCondition"
                                                Display="None" ErrorMessage="Please Enter Condition " SetFocusOnError="True"
                                                ValidationGroup="T12MedDetails"></asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator ID="T12RequiredFieldValidator9" runat="server" ControlToValidate="T12txtadministeredatschool"
                                                Display="None" ErrorMessage="Please Enter Administered at School" SetFocusOnError="True" ValidationGroup="T12MedDetails"></asp:RequiredFieldValidator>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <table id="T12TBD3" visible="False" runat="server" width="100%">
                                    <tr>
                                        <td width="20%"><span class="field-label">Comment</span></td>
                                        <td width="30%">
                                            <asp:TextBox ID="T12txtcomments" runat="server" TextMode="MultiLine"
                                                ValidationGroup="T12MedDetails"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="T12btnothersave" CssClass="button" runat="server" Text="Save" ValidationGroup="T12MedDetails" /></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:RequiredFieldValidator ID="T12RequiredFieldValidator10" runat="server" ControlToValidate="T12txtcomments"
                                                Display="None" ErrorMessage="Please Enter Comment" SetFocusOnError="True"
                                                ValidationGroup="T12MedDetails"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                                <asp:ValidationSummary ID="T12ValidationSummary3" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="T12MedDetails" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lnkview1" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter Medical Details" ExpandControlID="T12lnkview1"
        ExpandedText="Hide-Medical Details" ScrollContents="false" TargetControlID="T12Panel1"
        TextLabelID="T12lnkview1">
    </ajaxToolkit:CollapsiblePanelExtender>
    <br />
    <table width="100%">
        <tr>
            <td class="title-bg">Medical Details</td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="T12CheckBox1" Text="Allergies" onclick="javascript:showhide2(this,'Panel1');" runat="server" CssClass="field-label" /><br />
                <asp:Panel ID="Panel1" runat="server" CssClass="panel-cover">
                    <table id="T12G1" width="100%">
                        <tr>
                            <td class="title-bg">Allergies</td>
                        </tr>
                        <tr>
                            <td>

                                <asp:GridView ID="T12GridView1" AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server" Width="100%" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date">
                                            <HeaderTemplate>
                                                Date
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Allergy">
                                            <HeaderTemplate>
                                                Allergy
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("ALLERGY")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Result OF Exposure">
                                            <HeaderTemplate>
                                                Result OF Exposure
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview1")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                                <asp:Panel ID="T12Panel1" runat="server" CssClass="panel-cover">
                                                    <%#Eval("RESULT_OF_EXPOSURE")%>
                                                </asp:Panel>
                                                <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview1")%>' ExpandControlID="T12lblview"
                                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                                    TextLabelID="T12lblview">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Treatment">
                                            <HeaderTemplate>
                                                Treatment
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="T12lblview2" runat="server" Text='<%#Eval("tempview2")%>'></asp:Label>
                                                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                                <asp:Panel ID="T12Panel12" runat="server" CssClass="panel-cover">
                                                    <%#Eval("TREATMENT")%>
                                                </asp:Panel>
                                                <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender12" runat="server"
                                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview2" Collapsed="true"
                                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview2")%>' ExpandControlID="T12lblview2"
                                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel12"
                                                    TextLabelID="T12lblview2">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Author">
                                            <HeaderTemplate>
                                                Author
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("empname")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="griditem" Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                </asp:GridView>

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <asp:CheckBox ID="T12CheckBox2" Text="Current Medication Plus Medication History" onclick="javascript:showhide2(this,'Panel2');" runat="server" CssClass="field-label" />
                <br />
                <asp:Panel ID="Panel2" runat="server" CssClass="panel-cover">
                    <table id="T12G2" width="100%">
                        <tr>
                            <td class="title-bg">Current Medication Plus Medication History</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="T12GridView2" AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server" Width="100%" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Medication Name">
                                            <ItemTemplate>
                                                <%#Eval("MEDICATION_NAME")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Condition">
                                            <ItemTemplate>
                                                <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview1")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                                <asp:Panel ID="T12Panel1" runat="server" CssClass="panel-cover">
                                                    <%#Eval("CONDITION_TEXT")%>
                                                </asp:Panel>
                                                <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview1")%>' ExpandControlID="T12lblview"
                                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                                    TextLabelID="T12lblview">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Administered at School">
                                            <ItemTemplate>
                                                <asp:Label ID="T12lblview2" runat="server" Text='<%#Eval("tempview2")%>'></asp:Label>
                                                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                                <asp:Panel ID="T12Panel12" runat="server" CssClass="panel-cover">
                                                    <%#Eval("ADMINISTERED_AT_SCHOOL")%>
                                                </asp:Panel>
                                                <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender12" runat="server"
                                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview2" Collapsed="true"
                                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview2")%>' ExpandControlID="T12lblview2"
                                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel12"
                                                    TextLabelID="T12lblview2">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Author">
                                            <ItemTemplate>
                                                <%#Eval("empname")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="griditem" Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <asp:CheckBox ID="T12CheckBox3" Text="Hearing Issue/s" runat="server" onclick="javascript:showhide2(this,'Panel3');" CssClass="field-label" />
                <br />
                <asp:Panel ID="Panel3" runat="server" CssClass="panel-cover">
                    <table id="T12G3" width="100%">
                        <tr>
                            <td class="title-bg">Hearing Issue/s</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="T12GridView3" AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server" Width="100%" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date">
                                            <HeaderTemplate>
                                                Date
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments">
                                            <HeaderTemplate>
                                                Comments
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                                <asp:Panel ID="T12Panel1" runat="server" CssClass="panel-cover">
                                                    <%#Eval("COMMENTS")%>
                                                </asp:Panel>
                                                <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                                    TextLabelID="T12lblview">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Author">
                                            <HeaderTemplate>
                                                Author
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("empname")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="griditem" Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <asp:CheckBox ID="T12CheckBox4" Text="Vision Issue/s " runat="server" onclick="javascript:showhide2(this,'Panel4');" CssClass="field-label" /><br />
                <asp:Panel ID="Panel4" runat="server" CssClass="panel-cover">
                    <table id="T12G4" width="100%">
                        <tr>
                            <td class="title-bg">Vision Issue/s
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="T12GridView4" AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server" Width="100%" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date">
                                            <HeaderTemplate>
                                                Date
                                            </HeaderTemplate>

                                            <ItemTemplate>
                                                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments">
                                            <HeaderTemplate>
                                                Comments
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                                <asp:Panel ID="T12Panel1" runat="server" CssClass="panel-cover">
                                                    <%#Eval("COMMENTS")%>
                                                </asp:Panel>
                                                <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                                    TextLabelID="T12lblview">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Author">
                                            <HeaderTemplate>
                                                Author
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("empname")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="griditem" Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <asp:CheckBox ID="T12CheckBox5" Text="Bullying Issue/s " runat="server" onclick="javascript:showhide2(this,'Panel5');" CssClass="field-label" />
                <br />
                <asp:Panel ID="Panel5" runat="server" CssClass="panel-cover">
                    <table id="T12G5" width="100%">
                        <tr>
                            <td class="title-bg">Bullying Issue/s
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="T12GridView5" AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server" Width="100%" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date">
                                            <HeaderTemplate>
                                                Date
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments">
                                            <HeaderTemplate>
                                                Comments
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                                <asp:Panel ID="T12Panel1" runat="server" CssClass="panel-cover">
                                                    <%#Eval("COMMENTS")%>
                                                </asp:Panel>
                                                <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                                    TextLabelID="T12lblview">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Author">
                                            <HeaderTemplate>
                                                Author
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("empname")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="griditem" Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <asp:CheckBox ID="T12CheckBox6" Text="Avoidance Issue/s " runat="server" onclick="javascript:showhide2(this,'Panel6');" CssClass="field-label" />
                <br />
                <asp:Panel ID="Panel6" runat="server" CssClass="panel-cover">
                    <table id="T12G6" width="100%">
                        <tr>
                            <td class="title-bg">Bullying Issue/s
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="T12GridView6" AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server" Width="100%" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date">
                                            <HeaderTemplate>
                                                Date
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments">
                                            <HeaderTemplate>
                                                Comments
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                                <asp:Panel ID="T12Panel1" runat="server" CssClass="panel-cover">
                                                    <%#Eval("COMMENTS")%>
                                                </asp:Panel>
                                                <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                                    TextLabelID="T12lblview">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Author">
                                            <HeaderTemplate>
                                                Author
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("empname")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="griditem" Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <asp:CheckBox ID="T12CheckBox7" Text="Motor Issue/s " runat="server" onclick="javascript:showhide2(this,'Panel7');" CssClass="field-label" />
                <br />
                <asp:Panel ID="Panel7" runat="server" CssClass="panel-cover">
                    <table id="T12G7" width="100%">
                        <tr>
                            <td class="title-bg">Motor Issue/s
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="T12GridView7" AutoGenerateColumns="false" EmptyDataText="No Records added yet" runat="server" Width="100%" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date">
                                            <HeaderTemplate>
                                                Date
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments">
                                            <HeaderTemplate>
                                                Comments
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                                <asp:Panel ID="T12Panel1" runat="server" CssClass="panel-cover">
                                                    <%#Eval("COMMENTS")%>
                                                </asp:Panel>
                                                <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                                    TextLabelID="T12lblview">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Author">
                                            <HeaderTemplate>
                                                Author
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%#Eval("empname")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="griditem" Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />


            </td>
        </tr>
    </table>
    <br />
    <asp:LinkButton ID="T12lnkview2" runat="server" OnClientClick="javascript:return false;">Enter Notes</asp:LinkButton>
    <br />
    <br />
    <asp:Panel ID="T12Panel2" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4">Enter Notes</td>
            </tr>
            <tr>
                <td width="20%"><span class="field-label">Author</span></td>
                <td width="30%">
                    <asp:DropDownList ID="T12ddnotesstaff" runat="server" ValidationGroup="T12notes"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="T12ddnotesstaff"
                        Display="None" ErrorMessage="Please Select Author" InitialValue="-1" SetFocusOnError="True"
                        ValidationGroup="T12notes"></asp:RequiredFieldValidator>
                </td>
                <td width="20%"><span class="field-label">Type</span></td>
                <td width="30%">
                    <asp:RadioButtonList ID="T12RadioButtontype" runat="server" RepeatDirection="Horizontal" Width="195px">
                        <asp:ListItem Selected="True" Value="0">Medical</asp:ListItem>
                        <asp:ListItem Value="1">Emotional</asp:ListItem>
                    </asp:RadioButtonList></td>
            </tr>

            <tr>
                <td><span class="field-label">Comment</span></td>
                <td>
                    <asp:TextBox ID="T12txtNotes" runat="server" TextMode="MultiLine"
                        ValidationGroup="T12notes"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="T12txtNotes"
                        Display="None" ErrorMessage="Please Enter Comments" SetFocusOnError="True" ValidationGroup="T12notes"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="T12btnnotessave" runat="server" CssClass="button" Text="Save" ValidationGroup="T12notes" />
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ShowSummary="False" ValidationGroup="T12notes" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender2" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lnkview2" Collapsed="true"
        CollapsedSize="0" CollapsedText="Enter Medical Details" ExpandControlID="T12lnkview2"
        ExpandedText="Hide-Medical Details" ScrollContents="false" TargetControlID="T12Panel2"
        TextLabelID="T12lnkview2">
    </ajaxToolkit:CollapsiblePanelExtender>
    <table width="100%">
        <tr>
            <td class="title-bg">Notes</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T12GrdNotes" AutoGenerateColumns="false" EmptyDataText="No Notes added yet" runat="server" ShowFooter="True" Width="100%" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderTemplate>
                                Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                            </ItemTemplate>
                            <ItemStyle />
                            <FooterTemplate>
                                Total
                                <asp:Label ID="lbltotal" runat="server" Text=""></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type">
                            <HeaderTemplate>
                                Type
                                <br />
                                <table width="100%">
                                    <tr>
                                        <td align="center">M</td>
                                        <td align="center">E
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <HeaderStyle />
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            <asp:CheckBox ID="T12CheckBoxmedical" Checked='<%# Eval("medical") %>' Enabled="false" runat="server" /></td>
                                        <td align="center">
                                            <asp:CheckBox ID="T12CheckBoxemotional" Checked='<%# Eval("Emotional") %>' Enabled="false" runat="server"></asp:CheckBox>
                                        </td>
                                    </tr>
                                </table>

                            </ItemTemplate>
                            <ItemStyle />
                            <FooterTemplate>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="T12lblMtotal" runat="server" Text="M"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="T12lblEtotal" runat="server" Text="E"></asp:Label></td>
                                    </tr>
                                </table>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comment">
                            <HeaderTemplate>
                                Comment
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T12Panel1" runat="server" CssClass="panel-cover">
                                    <%#Eval("COMMENTS")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                    TextLabelID="T12lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Author">
                            <HeaderTemplate>
                                Author
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center><%#Eval("empname")%></center>
                            </ItemTemplate>
                            <ItemStyle />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>

</div>