Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senIEP
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindControls()
            BindCategory()
            BindIEPGrid()
            BindIEPTrackingLiteracyGrid()
            BindIEPTrackingMathsGrid()
            BindIEPTrackingBehaviouralGrid()
            LockFileCheck()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then

            T15btnsave.Enabled = False
            T15btnupdate.Enabled = False

        End If

    End Sub
    Public Sub BindIEPTrackingLiteracyGrid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select REVIEW_ENTRY_DATE,IEP_ID,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(DATE_WRITTEN_COMMENTS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview1,(substring(REVIEW_DATE_COMMENTS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview2,DATE_WRITTEN,DATE_WRITTEN_COMMENTS,REVIEW_DATE,REVIEW_DATE_COMMENTS,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname  from SEN_DEPARTMENT_IEP a " & _
                                 " inner join oasis.dbo.employee_m c on c.emp_id = a.entry_emp_id " & _
                                 " where a.IEP_CATEGORY_ID='1' and a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "'order by a.DATE_WRITTEN_COMMENTS desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T8GrdIEPTrackingLiteracy.DataSource = ds
        T8GrdIEPTrackingLiteracy.DataBind()


    End Sub
    Public Sub BindIEPTrackingMathsGrid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select REVIEW_ENTRY_DATE,IEP_ID,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(DATE_WRITTEN_COMMENTS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview1,(substring(REVIEW_DATE_COMMENTS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview2,DATE_WRITTEN,DATE_WRITTEN_COMMENTS,REVIEW_DATE,REVIEW_DATE_COMMENTS,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname  from SEN_DEPARTMENT_IEP a " & _
                                 " inner join oasis.dbo.employee_m c on c.emp_id = a.entry_emp_id " & _
                                 " where a.IEP_CATEGORY_ID='2' and a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "'order by a.DATE_WRITTEN_COMMENTS desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T8GrdIEPTrackingMaths.DataSource = ds
        T8GrdIEPTrackingMaths.DataBind()


    End Sub

    Public Sub BindIEPTrackingBehaviouralGrid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select REVIEW_ENTRY_DATE,IEP_ID,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(DATE_WRITTEN_COMMENTS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview1,(substring(REVIEW_DATE_COMMENTS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview2,DATE_WRITTEN,DATE_WRITTEN_COMMENTS,REVIEW_DATE,REVIEW_DATE_COMMENTS,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname  from SEN_DEPARTMENT_IEP a " & _
                                 " inner join oasis.dbo.employee_m c on c.emp_id = a.entry_emp_id " & _
                                 " where a.IEP_CATEGORY_ID='3' and a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "'order by a.DATE_WRITTEN_COMMENTS desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T8GrdIEPTrackingBehavioural.DataSource = ds
        T8GrdIEPTrackingBehavioural.DataBind()


    End Sub
    Protected Sub T8GrdIEPTrackingLiteracy_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T8GrdIEPTrackingLiteracy.PageIndexChanging
        T8GrdIEPTrackingLiteracy.PageIndex = e.NewPageIndex
        BindIEPTrackingLiteracyGrid()
    End Sub

    Protected Sub T8GrdIEPTrackingMaths_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T8GrdIEPTrackingMaths.PageIndexChanging
        T8GrdIEPTrackingMaths.PageIndex = e.NewPageIndex
        BindIEPTrackingMathsGrid()
    End Sub

    Protected Sub T8GrdIEPTrackingBehavioural_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T8GrdIEPTrackingBehavioural.PageIndexChanging
        T8GrdIEPTrackingBehavioural.PageIndex = e.NewPageIndex
        BindIEPTrackingBehaviouralGrid()
    End Sub
    Public Sub BindControls()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id and a.EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        T15ddStaffs.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T15ddStaffs.DataTextField = "empname"
        T15ddStaffs.DataValueField = "EMP_ID"
        T15ddStaffs.DataBind()
        Dim list As New ListItem
        list.Text = "Select an Inclusion Member"
        list.Value = "-1"
        T15ddStaffs.Items.Insert(0, list)

    End Sub
    Public Sub BindCategory()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_IEP_CATEGORY "
        T15ddIEPTrackingCategory.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T15ddIEPTrackingCategory.DataTextField = "IEP_CATEGORY_DES"
        T15ddIEPTrackingCategory.DataValueField = "IEP_CATEGORY_ID"
        T15ddIEPTrackingCategory.DataBind()
        Dim list As New ListItem
        list.Text = "Select an IEP"
        list.Value = "-1"
        T15ddIEPTrackingCategory.Items.Insert(0, list)
    End Sub
    Public Sub BindIEPGrid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select REVIEW_ENTRY_DATE,IEP_ID,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(DATE_WRITTEN_COMMENTS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview1,(substring(REVIEW_DATE_COMMENTS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview2, IEP_CATEGORY_DES,DATE_WRITTEN,DATE_WRITTEN_COMMENTS,REVIEW_DATE,REVIEW_DATE_COMMENTS,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname  from SEN_DEPARTMENT_IEP a " & _
                                 " inner join SEN_DEPARTMENT_IEP_CATEGORY b on a.IEP_CATEGORY_ID=b.IEP_CATEGORY_ID " & _
                                 " inner join oasis.dbo.employee_m c on c.emp_id = a.entry_emp_id " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "' order by a.DATE_WRITTEN_COMMENTS desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T15GrdIEP.DataSource = ds
        T15GrdIEP.DataBind()
    End Sub
   
    Protected Sub T15btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T15btnsave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@IEP_CATEGORY_ID", T15ddIEPTrackingCategory.SelectedValue)
            pParms(3) = New SqlClient.SqlParameter("@DATE_WRITTEN", T15txtdatewritten.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@DATE_WRITTEN_COMMENTS", T15txtdatewrittencomments.Text.Trim())
            pParms(5) = New SqlClient.SqlParameter("@REVIEW_DATE", T15txtdatereview.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@REVIEW_DATE_COMMENTS", T15txtdatereviewcomments.Text.Trim())
            pParms(7) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T15ddStaffs.SelectedValue)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_IEP", pParms)
            T15ddIEPTrackingCategory.SelectedIndex = 0
            T15txtdatewritten.Text = ""
            T15txtdatewrittencomments.Text = ""
            T15txtdatereview.Text = ""
            T15txtdatereviewcomments.Text = ""
            T15ddStaffs.SelectedIndex = 0
            BindIEPGrid()
            BindIEPTrackingLiteracyGrid()
            BindIEPTrackingMathsGrid()
            BindIEPTrackingBehaviouralGrid()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try
       
    End Sub

    Protected Sub T15GrdIEP_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles T15GrdIEP.RowCommand
        If e.CommandName = "Updating" Then
            T15Panel1.Visible = False
            T15Panel2.Visible = True
            T15T1.Visible = True
            T15T2.Visible = False
            T15HiddenFieldIEPid.Value = e.CommandArgument
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim strQuery As String = "select REVIEW_DATE_COMMENTS from SEN_DEPARTMENT_IEP where IEP_ID='" & T15HiddenFieldIEPid.Value & "'"
            T15txtdatereviewcomments.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)
        End If
    End Sub

    Protected Sub T15GrdIEP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T15GrdIEP.PageIndexChanging
        T15GrdIEP.PageIndex = e.NewPageIndex
        BindIEPGrid()
    End Sub

   
    Protected Sub T15btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T15btncancel.Click
        T15Panel1.Visible = True
        T15Panel2.Visible = False
        T15T1.Visible = False
        T15T2.Visible = True
        T15txtdatereviewcomments.Text = ""
        T15HiddenFieldIEPid.Value = ""
    End Sub

    Protected Sub T15btnupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T15btnupdate.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@IEP_ID", T15HiddenFieldIEPid.Value)
            pParms(1) = New SqlClient.SqlParameter("@REVIEW_DATE_COMMENTS", T15txtdatereviewcomments.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "UPDATE_SEN_DEPARTMENT_IEP_REVIEW_DATE_COMMENTS", pParms)
            T15txtdatereviewcomments.Text = ""
            T15Panel1.Visible = True
            T15Panel2.Visible = False
            T15T1.Visible = False
            T15T2.Visible = True
            T15HiddenFieldIEPid.Value = ""
            BindIEPGrid()
            BindIEPTrackingLiteracyGrid()
            BindIEPTrackingMathsGrid()
            BindIEPTrackingBehaviouralGrid()
            lblstatusmessage.Text = "Successfully Updated"

        Catch ex As Exception

        End Try

    End Sub
End Class
