<%@ Control Language="VB" AutoEventWireup="false" CodeFile="senSpecialistsOutsideAgencies.ascx.vb" Inherits="SenRegister_UserControls_senSpecialistsOutsideAgencies" %>
<div >
    <asp:HiddenField ID="Hiddenstuid" runat="server" />
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:Label ID="lblstatusmessage" runat="server" EnableTheming="False"  CssClass="error"></asp:Label><br />
    <br />
    <table  width="100%">
        <tr>
            <td class="title-bg">Specialist Outside Agencies
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td colspan="4">
                            <div class="checkbox-list">
                            <asp:CheckBoxList ID="T13CheckBoxListspec" runat="server" >
                                <%--    <asp:ListItem Value="1">Occupational Therapy</asp:ListItem>
    <asp:ListItem Value="2">Speech &amp; Language Therapy</asp:ListItem>
    <asp:ListItem Value="3">Clinical Psychology</asp:ListItem>
    <asp:ListItem Value="4">Behavioural Optometry</asp:ListItem>
    <asp:ListItem Value="5">Auditory Processing Specialit</asp:ListItem>
    <asp:ListItem Value="6">Audiologist</asp:ListItem>
    <asp:ListItem Value="7">Optometrist</asp:ListItem>
    <asp:ListItem Value="8">ENT</asp:ListItem>
    <asp:ListItem Value="9">Nutritionist</asp:ListItem>
    <asp:ListItem Value="10">Behavioural Optometry</asp:ListItem>
    <asp:ListItem Value="11">Auditory Processing Specialit</asp:ListItem>
    <asp:ListItem Value="12">Audiologist</asp:ListItem>
    <asp:ListItem Value="13">Physiotherapy</asp:ListItem>
    <asp:ListItem Value="14">Medical Pediatric Care</asp:ListItem>
    <asp:ListItem Value="15">Outside Tuition</asp:ListItem>
    <asp:ListItem Value="16">Sound Therapy</asp:ListItem>
    <asp:ListItem Value="17">Other</asp:ListItem>--%>
                            </asp:CheckBoxList></div></td>
                    </tr>
                    <tr>
                         <td><span class="field-label">Outside Tuition Type</span></td>
                         <td>
                           <asp:TextBox ID="T13txtoutsidetuition" runat="server" ValidationGroup="T13Details"></asp:TextBox></td>
                          <td><span class="field-label">Other</span>
                          </td>
                         <td>
                           <asp:TextBox ID="T13txtothers" runat="server" ValidationGroup="T13Details"></asp:TextBox></td>
                    </tr>
                    <%--<tr>
                        <td>
                            <table id="T13T1">
                                <tr>                                   
                                    <td>:</td>                                   
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table id="T13T2">
                                <tr>                                  
                                    <td>:</td>                                   
                                </tr>
                            </table>
                        </td>
                    </tr>--%>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="T13btnsave" runat="server" CssClass="button" Text="Save" ValidationGroup="T13Details" CausesValidation="False" /></td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
    <%--<asp:RequiredFieldValidator ID="T13RequiredFieldValidator1" runat="server" ControlToValidate="T13txtoutsidetuition"
    Display="None" ErrorMessage="Please Enter Outside Tuition Type"
    SetFocusOnError="True" ValidationGroup="T13Details"></asp:RequiredFieldValidator><asp:RequiredFieldValidator
        ID="T13RequiredFieldValidator2" runat="server" ControlToValidate="T13txtothers" Display="None"
        ErrorMessage="Please Enter Other Type Details" SetFocusOnError="True" ValidationGroup="T13Details"></asp:RequiredFieldValidator><asp:ValidationSummary
            ID="T13ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False"
            ValidationGroup="T13Details" />--%>
    <br />
    <br />
    <asp:LinkButton ID="T13lnknotes" runat="server" OnClientClick="javascript:return false;">Enter Notes</asp:LinkButton>
    <br />
    <br />
    <ajaxToolkit:CollapsiblePanelExtender ID="T13CollapsiblePanelExtender1" runat="server" AutoCollapse="False" AutoExpand="False"
        CollapseControlID="T13lnknotes" Collapsed="true" CollapsedSize="0" CollapsedText="Enter Notes"
        ExpandControlID="T13lnknotes" ExpandedText="Hide-Notes" ScrollContents="false"
        TargetControlID="T13Panel1" TextLabelID="T13lnknotes">
    </ajaxToolkit:CollapsiblePanelExtender>
    <asp:Panel ID="T13Panel1" runat="server" CssClass="panel-cover">
        <table width="100%">
            <tr>
                <td class="title-bg" colspan="4">Enter Notes</td>
            </tr>
            <tr>
                <td width="20%">
                    <asp:DropDownList ID="T13ddStaffNotes" runat="server" ValidationGroup="T13Notes">
                    </asp:DropDownList></td>
                <td width="30%"> 
                    <asp:TextBox ID="T13Notes" runat="server" TextMode="MultiLine"
                        ValidationGroup="T13Notes" ></asp:TextBox></td>
                <td colspan="2"></td>
            </tr>           
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="T13NotesSave" runat="server" CssClass="button" Text="Save" ValidationGroup="T13Notes" /></td>
            </tr>

        </table>
    </asp:Panel>
    <table  width="100%">
        <tr>
            <td class="title-bg">Notes</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="T13GrdNotes" runat="server" AutoGenerateColumns="false" EmptyDataText="No Notes added yet" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderTemplate>
                                Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                            </ItemTemplate>
                            <ItemStyle Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Notes">
                            <HeaderTemplate>
                               Notes
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T13lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                <%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                <asp:Panel ID="T13Panel1" runat="server" CssClass="panel-cover">
                                    <%#Eval("NOTES_TEXT")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T13CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T13lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T13lblview"
                                    ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T13Panel1"
                                    TextLabelID="T13lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Author">
                            <HeaderTemplate>
                                Author
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center><%#Eval("empname")%></center>
                            </ItemTemplate>
                            <ItemStyle  />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem"  Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:RequiredFieldValidator ID="T13RequiredFieldValidator6" runat="server" ControlToValidate="T13ddStaffNotes"
        Display="None" ErrorMessage="Please Select a Inclusion Member (Entered By)" InitialValue="-1"
        SetFocusOnError="True" ValidationGroup="T13Notes"></asp:RequiredFieldValidator><asp:RequiredFieldValidator
            ID="T13RequiredFieldValidator7" runat="server" ControlToValidate="T13Notes" Display="None"
            ErrorMessage="Please Enter Notes" SetFocusOnError="True" ValidationGroup="T13Notes"></asp:RequiredFieldValidator><asp:ValidationSummary
                ID="ValidationSummary2" runat="server" ShowMessageBox="True" ShowSummary="False"
                ValidationGroup="T13Notes" />
</div>