Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senTeacherInput
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindControls()
            BindNotes()
            LockFileCheck()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then

            T14NotesSave.Enabled = False


        End If

    End Sub
    Public Sub BindControls()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id and a.EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        T14ddStaffNotes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T14ddStaffNotes.DataTextField = "empname"
        T14ddStaffNotes.DataValueField = "EMP_ID"
        T14ddStaffNotes.DataBind()
        Dim list As New ListItem
        list.Text = "Select an Inclusion Member"
        list.Value = "-1"
        T14ddStaffNotes.Items.Insert(0, list)

    End Sub
    Public Sub BindNotes()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select NOTES_TEXT,ENTRY_DATE, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname , '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(NOTES_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview from SEN_SEN_DEPARTMENT_TEACHER_INPUT a inner join oasis.dbo.EMPLOYEE_M b on  a.ENTRY_EMP_ID=b.emp_id where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "' order by A.ENTRY_DATE desc"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T14GrdNotes.DataSource = ds
        T14GrdNotes.DataBind()
    End Sub
    Protected Sub T14NotesSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T14NotesSave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@NOTES_TEXT", T14Notes.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T14ddStaffNotes.SelectedValue)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_SEN_DEPARTMENT_TEACHER_INPUT", pParms)
            T14ddStaffNotes.SelectedValue = "-1"
            T14Notes.Text = ""
            BindNotes()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T14GrdNotes_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T14GrdNotes.PageIndexChanging
        T14GrdNotes.PageIndex = e.NewPageIndex
        BindNotes()
    End Sub
End Class
