Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class SenRegister_UserControls_senMAGT
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim stu_id As String = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
            Hiddenstuid.Value = stu_id
            Hiddenbsuid.Value = Session("sbsuid") ''"125016" ''
            BindPrimaryDetails()
            BindControls()
            BindGrid()
            BindNotes()
            DepEntry()
            LockFileCheck()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub DepEntry()

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "SELECT ENTRY_MAGT FROM SEN_ACE_STUDENTS WHERE STU_ID='" & Hiddenstuid.Value & "' "
        Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)

        If Not IsDBNull(val) Then
            T10lblDateenteredmagt.Text = Convert.ToDateTime(val).ToString("dd/MMM/yyyy")
            Try
                If T10lblDateenteredmagt.Text.Trim() <> "" Then
                    T10lblTimeLag.Text = DateDiff(DateInterval.Day, Convert.ToDateTime(T10lbldateentered.Text), Convert.ToDateTime(T10lblDateenteredmagt.Text), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.System) & " Days"
                End If
            Catch ex As Exception

            End Try
        End If


    End Sub
    Public Sub LockFileCheck()

        If Session("LOCK_FILE").ToString() = "True" Then

            T10btnsave.Enabled = False
            T10btnnotessave.Enabled = False
            lnkentrysave.Enabled = False
        End If

    End Sub
    Public Sub BindNotes()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select  '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(magt_note,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, entry_date,magt_note,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from SEN_DEPARTMENT_MAGT_NOTES a " & _
                                 " inner join oasis.dbo.employee_m b on a.entry_emp_id= b.emp_id " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "'order by a.entry_date desc "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T10GrdNotes.DataSource = ds
        T10GrdNotes.DataBind()
    End Sub
    Public Sub BindControls()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = "select * from SEN_DEPARTMENT_MAGT_CATEGORY"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T10ddMAGTCategory.DataSource = ds
        T10ddMAGTCategory.DataTextField = "MAGT_CATEGORY_DESC"
        T10ddMAGTCategory.DataValueField = "MAGT_CATEGORY_ID"
        T10ddMAGTCategory.DataBind()
        Dim list As New ListItem
        list.Text = "Select a MAGT Category"
        list.Value = "-1"
        T10ddMAGTCategory.Items.Insert(0, list)

        ''Staffs
        strQuery = "select distinct b.EMP_ID, (EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname from sen_department_members a " & _
                                 " inner join oasis.dbo.EMPLOYEE_M b on a.emp_id=b.emp_id where a.EMP_BSU_ID='" & Hiddenbsuid.Value & "'"
        T10ddStaffs.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T10ddstaffnotes.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        T10ddStaffs.DataTextField = "empname"
        T10ddstaffnotes.DataTextField = "empname"
        T10ddStaffs.DataValueField = "EMP_ID"
        T10ddstaffnotes.DataValueField = "EMP_ID"
        T10ddStaffs.DataBind()
        T10ddstaffnotes.DataBind()
        Dim list1 As New ListItem
        list1.Text = "Select an Inclusion Member"
        list1.Value = "-1"
        T10ddStaffs.Items.Insert(0, list1)
        T10ddstaffnotes.Items.Insert(0, list1)

    End Sub
    Public Sub BindPrimaryDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim strQuery As String = " select c.stu_doj,b.BSU_NAME,b.BSU_SHORTNAME,ENTRY_DATE from SEN_DEPARTMENT_STUDENTS a " & _
                                 " inner join oasis.dbo.BUSINESSUNIT_M b on a.stu_bsu_id = b.bsu_id " & _
                                 " inner join oasis.dbo.student_m c on c.stu_id= a.stu_id  " & _
                                 " where a.STU_ID='" & Hiddenstuid.Value & "' and a.STU_BSU_ID='" & Hiddenbsuid.Value & "' and a.dept_id='10' and a.active='true' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            T10lbldateenteredlabel.Text = "Date Entered " & ds.Tables(0).Rows(0).Item("BSU_SHORTNAME").ToString()
            T10lbldateentered.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("stu_doj").ToString()).ToString("dd/MMM/yyyy")
          
        End If
    End Sub

    Public Sub BindGrid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
        Dim i = 0
        Dim ds As New DataSet
        'Dim strQuery As String = " select '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(MATERIALS_COMMENT_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, MATERIALS_COMMENT_TEXT,ENTRY_DATE,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname, REFER_DATE,REFER_BY,START_DATE,END_DATE from SEN_DEPARTMENT_MAGT a " & _
        '                        " inner join oasis.dbo.employee_m  b on a.entry_emp_id=b.emp_id " & _
        '                        " where a.MAGT_CATEGORY_ID='" & 1 & "' and a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "' order by a.ENTRY_DATE desc "
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        'T10GridView1.DataSource = ds
        'T10GridView1.DataBind()

        'Dim ds1 As New DataSet
        'strQuery = " select '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(MATERIALS_COMMENT_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, MATERIALS_COMMENT_TEXT,ENTRY_DATE,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname, REFER_DATE,REFER_BY,START_DATE,END_DATE from SEN_DEPARTMENT_MAGT a " & _
        '                        " inner join oasis.dbo.employee_m  b on a.entry_emp_id=b.emp_id " & _
        '                        " where a.MAGT_CATEGORY_ID='" & 2 & "' and a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "' order by a.ENTRY_DATE desc "
        'ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        'T10GridView2.DataSource = ds1
        'T10GridView2.DataBind()

        For i = 1 To 11
            Dim strQuery As String = " select '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(MATERIALS_COMMENT_TEXT,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, MATERIALS_COMMENT_TEXT,ENTRY_DATE,(EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME) as empname, REFER_DATE,REFER_BY,START_DATE,END_DATE from SEN_DEPARTMENT_MAGT a " & _
                                 " inner join oasis.dbo.employee_m  b on a.entry_emp_id=b.emp_id " & _
                                 " where a.MAGT_CATEGORY_ID='" & i & "' and a.STU_ID='" & Hiddenstuid.Value & "' and a.BSU_ID='" & Hiddenbsuid.Value & "' order by a.ENTRY_DATE desc "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

            If i = 1 Then ''MAGT Art
                T10GridView1.DataSource = ds
                T10GridView1.DataBind()
            End If
            If i = 2 Then ''MAGT Geography
                T10GridView2.DataSource = ds
                T10GridView2.DataBind()
            End If
            If i = 3 Then ''MAGT History
                T10GridView3.DataSource = ds
                T10GridView3.DataBind()
            End If
            If i = 4 Then ''MAGT ICT
                T10GridView4.DataSource = ds
                T10GridView4.DataBind()
            End If
            If i = 5 Then ''MAGT Languages
                T10GridView5.DataSource = ds
                T10GridView5.DataBind()
            End If
            If i = 6 Then ''MAGT Literacy
                T10GridView6.DataSource = ds
                T10GridView6.DataBind()
            End If
            If i = 7 Then ''MAGT Maths
                T10GridView7.DataSource = ds
                T10GridView7.DataBind()
            End If
            If i = 8 Then ''MAGT Music
                T10GridView8.DataSource = ds
                T10GridView8.DataBind()
            End If
            If i = 9 Then ''MAGT Performing  Arts
                T10GridView9.DataSource = ds
                T10GridView9.DataBind()
            End If
            If i = 10 Then ''MAGT Physical Education
                T10GridView10.DataSource = ds
                T10GridView10.DataBind()
            End If
            If i = 11 Then ''MAGT Science
                T10GridView11.DataSource = ds
                T10GridView11.DataBind()
            End If


        Next
    End Sub


    Protected Sub T10btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T10btnsave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@MAGT_CATEGORY_ID", T10ddMAGTCategory.SelectedValue)
            pParms(3) = New SqlClient.SqlParameter("@MATERIALS_COMMENTS_TEXT", T10txtmatcomments.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T10ddStaffs.SelectedValue)

            pParms(5) = New SqlClient.SqlParameter("@REFER_DATE", T10txtReferDate.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@REFER_BY", T10TxtReferBy.Text.Trim())
            pParms(7) = New SqlClient.SqlParameter("@START_DATE", T10txtstartdate.Text.Trim())
            pParms(8) = New SqlClient.SqlParameter("@END_DATE ", T10txtenddate.Text.Trim())

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_MAGT", pParms)
            T10ddMAGTCategory.SelectedIndex = 0
            T10ddStaffs.SelectedIndex = 0
            T10txtmatcomments.Text = ""
            T10txtReferDate.Text = ""
            T10TxtReferBy.Text = ""
            T10txtstartdate.Text = ""
            T10txtenddate.Text = ""

            BindGrid()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

   
    'Protected Sub T10CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T10CheckBox1.CheckedChanged
    '    If T10CheckBox1.Checked Then
    '        T10GridView1.Visible = True
    '    Else
    '        T10GridView1.Visible = False
    '    End If
    'End Sub

    'Protected Sub T10CheckBox2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T10CheckBox2.CheckedChanged
    '    If T10CheckBox2.Checked Then
    '        T10GridView2.Visible = True
    '    Else
    '        T10GridView2.Visible = False
    '    End If
    'End Sub

    'Protected Sub T10CheckBox3_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T10CheckBox3.CheckedChanged
    '    If T10CheckBox3.Checked Then
    '        T10GridView3.Visible = True
    '    Else
    '        T10GridView3.Visible = False
    '    End If
    'End Sub

    'Protected Sub T10CheckBox4_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T10CheckBox4.CheckedChanged
    '    If T10CheckBox4.Checked Then
    '        T10GridView4.Visible = True
    '    Else
    '        T10GridView4.Visible = False
    '    End If
    'End Sub

    'Protected Sub T10CheckBox5_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T10CheckBox5.CheckedChanged
    '    If T10CheckBox5.Checked Then
    '        T10GridView5.Visible = True
    '    Else
    '        T10GridView5.Visible = False
    '    End If
    'End Sub

    'Protected Sub T10CheckBox6_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T10CheckBox6.CheckedChanged
    '    If T10CheckBox6.Checked Then
    '        T10GridView6.Visible = True
    '    Else
    '        T10GridView6.Visible = False
    '    End If
    'End Sub

    'Protected Sub T10CheckBox7_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T10CheckBox7.CheckedChanged
    '    If T10CheckBox7.Checked Then
    '        T10GridView7.Visible = True
    '    Else
    '        T10GridView7.Visible = False
    '    End If
    'End Sub

    'Protected Sub T10CheckBox8_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T10CheckBox8.CheckedChanged
    '    If T10CheckBox8.Checked Then
    '        T10GridView8.Visible = True
    '    Else
    '        T10GridView8.Visible = False
    '    End If
    'End Sub

    'Protected Sub T10CheckBox9_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T10CheckBox9.CheckedChanged
    '    If T10CheckBox9.Checked Then
    '        T10GridView9.Visible = True
    '    Else
    '        T10GridView9.Visible = False
    '    End If
    'End Sub

    'Protected Sub T10CheckBox10_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T10CheckBox10.CheckedChanged
    '    If T10CheckBox10.Checked Then
    '        T10GridView10.Visible = True
    '    Else
    '        T10GridView10.Visible = False
    '    End If
    'End Sub

    'Protected Sub T10CheckBox11_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles T10CheckBox11.CheckedChanged
    '    If T10CheckBox11.Checked Then
    '        T10GridView11.Visible = True
    '    Else
    '        T10GridView11.Visible = False
    '    End If
    'End Sub

    Protected Sub T10btnnotessave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T10btnnotessave.Click
        lblstatusmessage.Text = ""

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Hiddenstuid.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Hiddenbsuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@COMMENT_TEXT", T10txtnotes.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", T10ddstaffnotes.SelectedValue)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_SEN_DEPARTMENT_MAGT_NOTES", pParms)
            T10ddstaffnotes.SelectedIndex = 0
            T10txtnotes.Text = ""
            BindNotes()
            lblstatusmessage.Text = "Successfully Saved"

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub T10GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T10GridView1.PageIndexChanging
        T10GridView1.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T10GridView2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T10GridView2.PageIndexChanging
        T10GridView2.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T10GridView3_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T10GridView3.PageIndexChanging
        T10GridView3.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T10GridView4_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T10GridView4.PageIndexChanging
        T10GridView4.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T10GridView5_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T10GridView5.PageIndexChanging
        T10GridView5.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T10GridView6_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T10GridView6.PageIndexChanging
        T10GridView6.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T10GridView7_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T10GridView7.PageIndexChanging
        T10GridView7.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T10GridView8_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T10GridView8.PageIndexChanging
        T10GridView8.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T10GridView9_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T10GridView9.PageIndexChanging
        T10GridView9.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T10GridView10_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T10GridView10.PageIndexChanging
        T10GridView10.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T10GridView11_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T10GridView11.PageIndexChanging
        T10GridView11.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub T10GrdNotes_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles T10GrdNotes.PageIndexChanging
        T10GrdNotes.PageIndex = e.NewPageIndex
        BindNotes()
    End Sub

    Protected Sub lnkentrysave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkentrysave.Click
        lblstatusmessage.Text = ""

        Try
            If T10lblDateenteredmagt.Text <> "" Then
                Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_SENConnectionString").ConnectionString
                Dim strQuery As String = "UPDATE SEN_ACE_STUDENTS SET  ENTRY_MAGT='" & T10lblDateenteredmagt.Text & "' WHERE STU_ID='" & Hiddenstuid.Value & "' "
                Dim val = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
                DepEntry()
                lblstatusmessage.Text = "Successfully Saved"

            End If
        Catch ex As Exception

        End Try

    End Sub
End Class
