<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryItemList.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Library_libraryItemList" %>

<%@ Register Src="UserControls/libraryItemList.ascx" TagName="libraryItemList" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Library Items
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <div>

                    <uc1:libraryItemList ID="LibraryItemList1" runat="server" />

                </div>
            </div>
        </div>
    </div>

</asp:Content>

