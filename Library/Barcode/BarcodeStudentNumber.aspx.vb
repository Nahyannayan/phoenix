Imports Microsoft.ApplicationBlocks.Data
Imports System.Data


Partial Class Library_Barcode_BarcodeStudentNumber
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
        If Not IsPostBack Then

            BindGrid()
        End If
    End Sub

    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim hash As New Hashtable

        If Session("Cardhashtable") Is Nothing Then
        Else
            hash = Session("Cardhashtable")
        End If

        Dim idictenum As IDictionaryEnumerator
        idictenum = hash.GetEnumerator()

        Dim sql_query = ""

        Dim ids = ""

        If Request.QueryString("Type") = "STUDENT" Then

            sql_query = " SELECT STU_NO AS NO,LOWER(ISNULL(STU_FIRSTNAME,'')+ ' ' +ISNULL(STU_MIDNAME,'')+ ' ' +ISNULL(STU_LASTNAME,'')) NAME, 'STUDENT' TYPE , BSU_NAME , '~/Library/Barcode/Barcode.aspx?number='+STU_NO  PRINT_NO ,'~/Library/Barcode/PrintBinaryImage.aspx?BSU_ID='+BSU_ID  PRINT_BSU FROM OASIS.dbo.STUDENT_M A " & _
                        " INNER JOIN OASIS.dbo.BUSINESSUNIT_M B ON A.STU_BSU_ID = B.BSU_ID "

            ids = " WHERE STU_ID IN ("

        ElseIf Request.QueryString("Type") = "EMPLOYEE" Then
            sql_query = " SELECT EMPNO AS NO,LOWER(ISNULL(EMP_FNAME,'')+ ' ' +ISNULL(EMP_MNAME,'')+ ' ' +ISNULL(EMP_LNAME,'')) NAME, 'EMPLOYEE' TYPE , BSU_NAME , '~/Library/Barcode/Barcode.aspx?number='+EMPNO  PRINT_NO ,'~/Library/Barcode/PrintBinaryImage.aspx?BSU_ID='+BSU_ID  PRINT_BSU FROM OASIS.dbo.EMPLOYEE_M A " & _
                        " INNER JOIN OASIS.dbo.BUSINESSUNIT_M B ON A.EMP_BSU_ID = B.BSU_ID "
            ids = " WHERE EMP_ID IN ("
        End If


        Dim i = 0

        While (idictenum.MoveNext())

            Dim key = idictenum.Key
            Dim id = idictenum.Value

            Dim stuid = idictenum.Value
            i = i + 1
            If hash.Count = i Then
                ids &= id & ")"
            Else
                ids &= id & ","
            End If


        End While

        hash.Clear()
        Session("Cardhashtable") = hash

        sql_query = sql_query + ids

        DataPrintCard.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        DataPrintCard.DataBind()
        InsertPageBreak()
    End Sub

    Public Sub InsertPageBreak()

        Dim count = DataPrintCard.Items.Count

        Dim i = 0
        Dim Tcount = 1
        For i = 0 To count - 1
            If Tcount = 8 Then
                Dim label As Label = DirectCast(DataPrintCard.Items(i).FindControl("lblpagebreak"), Label)
                label.Text = "<p style='page-break-before: always'>"
                Tcount = 0
            End If

            Tcount = Tcount + 1
        Next

    End Sub
End Class
