Imports Microsoft.ApplicationBlocks.Data


Partial Class Library_Barcode_BarcodeIssue
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim hash As New Hashtable

        If Session("Barcodehashtable") Is Nothing Then
        Else
            hash = Session("Barcodehashtable")
        End If

        Dim bsu_id = Session("sbsuid")

        If bsu_id <> "" Then

            Dim idictenum As IDictionaryEnumerator
            idictenum = hash.GetEnumerator()
            While (idictenum.MoveNext())
                Dim key = idictenum.Key
                Dim id = idictenum.Value

                ''Lib Info
                Dim labelinfo As New Label
                Dim str_query = " select BSU_SHORTNAME +'-'+ LIBRARY_CODE val from dbo.LIBRARY_ITEMS_DATA " & _
                                " where PRODUCT_BSU_ID='" & bsu_id & "' and ACCESSION_NO='" & id & "'"
                Dim info = SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_query)
                labelinfo.Text = "&nbsp;&nbsp;&nbsp;<font size='2' color='blue'>" & info & "</font><br>"
                PlaceHolder1.Controls.Add(labelinfo)

                ''Barcode
                Dim image As New Image
                image.ImageUrl = "Barcode.aspx?number=" & id
                image.Height = Request.QueryString("BCsize").ToString()
                PlaceHolder1.Controls.Add(image)
                
                ''Space

                Dim label As New Label
                label.Text = "<br><br><br>"
                PlaceHolder1.Controls.Add(label)



            End While

            hash.Clear()
        End If


        Session("Barcodehashtable") = hash

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
End Class
