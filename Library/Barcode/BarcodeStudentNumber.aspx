<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BarcodeStudentNumber.aspx.vb"
    Inherits="Library_Barcode_BarcodeStudentNumber" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Library Card</title>
    <style type="text/css"> 
    body { 
    margin: 0; 
    padding: 0; 
    } 
    </style>
</head>
<body onload="window.print()">
    <form id="form1" runat="server">
        <div>
            <asp:DataList ID="DataPrintCard" runat="server" CellSpacing="25" RepeatColumns="2"
                RepeatDirection="Horizontal">
                <ItemTemplate>
                    <div style="background-image: url(../../Images/Library/LibCard.jpg); background-repeat: no-repeat; position:static ;">
                          <br />
                          <table cellspacing="0" width="340" cellpadding="0">
                            <tr>
                                <td valign="middle"  style="width: 50px">
                                    <center>
                                        <asp:Image ID="Image1" runat="server" ImageUrl='<%#Eval("PRINT_BSU")%>' Height="50px"
                                            Width="50px" />
                                    </center>
                                </td>
                                <td colspan="2" valign="middle" align="center">
                                    <b>
                                        <asp:Label ID="lbltitle" Font-Size="Small" Text='<%#Eval("BSU_NAME")%>' runat="server"></asp:Label></b></td>
                            </tr>
                            <tr>
                                <td colspan="3" >
                                 <hr style="width:300px" /> 
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <b>LIBRARY MEMBERSHIP CARD</b></td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center" style="text-transform: capitalize; color: #0033ff;">
                                    <b><%#Eval("NAME")%></b></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Image ID="Image2" Height="60px" Width="150px" ImageUrl='<%#Eval("PRINT_NO")%>'
                                        runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px; height: 19px;">
                                </td>
                                <td style="width: 100px; height: 19px;">
                                </td>
                                <td style="width: 100px; height: 19px;">
                                </td>
                            </tr>
                        </table>

                        <br />
                        <br />
                    </div>
                    <asp:Label ID="lblpagebreak" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:DataList></div>
            
          
    </form>
</body>
</html>
