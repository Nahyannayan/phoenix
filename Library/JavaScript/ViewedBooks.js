﻿// JScript File
    var XMLHTTPRequestObject =false;
    var timeout;
    
    if(window.ActiveXObject)
    {
    XMLHTTPRequestObject =new ActiveXObject('Microsoft.XMLHTTP');
    }

         function listEntries(xmlObj) 
            {
             
              
              var div = document.getElementById("topViewItems");
              if (div.firstChild) div.removeChild(div.firstChild);
              

              var mainDiv = document.createElement("div");
              var i=0 
              
              for (i=0 ; i<xmlObj.childNodes.length; i++)
              {
                  var image = xmlObj.childNodes.item(i).childNodes.item(0).text
                  if (image != '')
                  { 
                  var a = document.createElement("a");
                  a.href=xmlObj.childNodes.item(i).childNodes.item(2).text
                  var img = document.createElement("img");
                  img.src= image 
                  var br1= document.createElement("br");
                  a.appendChild(br1);
                  var br2= document.createElement("br");
                  a.appendChild(br2);
                  a.appendChild(img);
                  var p = document.createElement('p');
                  p.appendChild(document.createTextNode(xmlObj.childNodes.item(i).childNodes.item(1).text ));
                  a.appendChild(p);
                  mainDiv.appendChild(a);
                  }
                 
              
              }
              div.appendChild(mainDiv);


            }


            function BindTopView() 
            {
                var library_division_id = document.getElementById('Hiddenlibrarydivid').value;
                var bsu_id = document.getElementById('HiddenBsuID').value;

            var datasource = '../Webservice/LibraryData.asmx/TopViewItems?library_division_id=' + library_division_id + '&bsu_id=' + bsu_id ;

         
            XMLHTTPRequestObject.open('GET',datasource,true);
            XMLHTTPRequestObject.onreadystatechange = function()
            { 
                    if(XMLHTTPRequestObject.readystate==4 && XMLHTTPRequestObject.status==200)
                    { 
                        var xmlDoc =XMLHTTPRequestObject.responseXML;
                        var xmlDocc = new ActiveXObject("Microsoft.XMLDOM"); 
                        xmlDocc.load(xmlDoc); 
                        xmlObj=xmlDocc.documentElement
                        listEntries(xmlObj) 
                        

                    }
                    else
                    {
                    
                      if (XMLHTTPRequestObject.status ==503 || XMLHTTPRequestObject.status ==504)
                       {  
                          BindTopView()
                        
                       }
                 
                    }
            }
              
            XMLHTTPRequestObject.send(null);
            timeout =  window.setTimeout('BindTopView();', 20000);
            return false;

        }

        //Javascript Error Handling

        function handleError() {

            return true;
        }
        window.onerror = handleError;
