<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master"  CodeFile="libraryQuickReturnTransactions.aspx.vb" Inherits="Library_libraryQuickReturnTransactions" %>

<%@ Register Src="UserControls/libraryQuickReturnTransactions.ascx" TagName="libraryQuickReturnTransactions"
    TagPrefix="uc1" %>
    
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server" >

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Return Item
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
     <table width="100%">
         <tr>
             <td align="left">
            
          <uc1:libraryQuickReturnTransactions ID="LibraryQuickReturnTransactions1" runat="server" />

             </td>
         </tr>
     </table>

         
   
            </div>
        </div>
    </div>
 

</asp:Content>  

