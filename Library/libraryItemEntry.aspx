<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryItemEntry.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Library_libraryItemEntry" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Src="UserControls/libraryItemEntry.ascx" TagName="libraryItemEntry"
    TagPrefix="uc3" %>

<%@ Register Src="UserControls/libraryBookEntry.ascx" TagName="libraryBookEntry"
    TagPrefix="uc1" %>
<%@ Register Src="UserControls/libraryGlobalItems.ascx" TagName="libraryGlobalItems"
    TagPrefix="uc2" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <script type="text/javascript">
        window.setTimeout('setpathExt(1)', 100);
        window.setTimeout('setpathExt()', 100);

        function setpathExt(p) {
            var path = window.location.href
            var Rpath = ''
            if (path.indexOf('?') != '-1') {
                Rpath = path.substring(path.indexOf('?'), path.length)
            }
            var objFrame

            if (p == 1) {
                //Library Global Items
                objFrame = document.getElementById("FEx1");
                objFrame.src = "TabPages/LibraryGlobalItems.aspx" + Rpath
            }

            if (p == 2) {
                //Excel Upload
                objFrame = document.getElementById("FEx2");
                objFrame.src = "libraryUploadExcelData.aspx" + Rpath
            }

            if (p == 21) {
                //Excel Upload
                objFrame = document.getElementById("FEx21");
                objFrame.src = "libraryUploadExcelDataManual.aspx" + Rpath
            }


            if (p == 3) {
                //Excel Upload
                objFrame = document.getElementById("FEx4");
                objFrame.src = "libraryUploadExcelApproval.aspx" + Rpath
            }


            if (p == 4) {
                //Excel Upload
                objFrame = document.getElementById("FEx5");
                objFrame.src = "libraryBulkPouchUpdate.aspx" + Rpath
            }


        }
        function setpathExt() {
            var path = window.location.href
            var Rpath = ''
            if (path.indexOf('?') != '-1') {
                Rpath = path.substring(path.indexOf('?'), path.length)
            }
            var objFrame


            //Library Global Items
            objFrame = document.getElementById("FEx1");
            objFrame.src = "TabPages/LibraryGlobalItems.aspx" + Rpath



            //Excel Upload
            objFrame = document.getElementById("FEx2");
            objFrame.src = "libraryUploadExcelData.aspx" + Rpath



            //Excel Upload
            objFrame = document.getElementById("FEx21");
            objFrame.src = "libraryUploadExcelDataManual.aspx" + Rpath




            //Excel Upload
            objFrame = document.getElementById("FEx4");
            objFrame.src = "libraryUploadExcelApproval.aspx" + Rpath




            //Excel Upload
            objFrame = document.getElementById("FEx5");
            objFrame.src = "libraryBulkPouchUpdate.aspx" + Rpath



        }


    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Item Entry
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <div align="left">
                    <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="HT1" runat="server">
                            <ContentTemplate>


                                <iframe id="FEx1" height="1180" scrolling="auto" marginwidth="0px" src="TabPages/LibraryGlobalItems.aspx" frameborder="0" width="100%"></iframe>


                            </ContentTemplate>
                            <HeaderTemplate>
                                Global Item Search
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="HT2" runat="server">
                            <ContentTemplate>

                                <uc1:libraryBookEntry ID="LibraryBookEntry1" runat="server" />

                            </ContentTemplate>
                            <HeaderTemplate>
                                Book Entry
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="HT3" runat="server">
                            <ContentTemplate>
                                <uc3:libraryItemEntry ID="LibraryItemEntry1" runat="server" />


                            </ContentTemplate>
                            <HeaderTemplate>
                                Item Entry
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="HT4" runat="server">
                            <ContentTemplate>
                                <iframe id="FEx2" height="800" scrolling="auto" marginwidth="0px" src="libraryUploadExcelData.aspx" frameborder="0" width="100%"></iframe>


                            </ContentTemplate>
                            <HeaderTemplate>
                                Upload Excel-With ISBN
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="HT41" runat="server">
                            <ContentTemplate>
                                <iframe id="FEx21" height="800" scrolling="auto" marginwidth="0px" src="libraryUploadExcelDataManual.aspx" frameborder="0" width="100%"></iframe>


                            </ContentTemplate>
                            <HeaderTemplate>
                                Upload Excel-Without ISBN
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                            <ContentTemplate>
                                <iframe id="FEx22" height="800" scrolling="auto" marginwidth="0px" src="libraryUploadExcelDataManual_Approve.aspx" frameborder="0" width="100%"></iframe>
                            </ContentTemplate>
                            <HeaderTemplate>
                                Upload Excel-With ISBN  And Approve
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="HT5" runat="server">
                            <ContentTemplate>
                                <iframe id="FEx3" height="800" scrolling="auto" marginwidth="0px" src="libraryStartExtractingGoogleBooks.aspx" frameborder="0" width="100%"></iframe>


                            </ContentTemplate>
                            <HeaderTemplate>
                                Get Pending Item(s)
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="HT6" runat="server">
                            <ContentTemplate>
                                <iframe id="FEx4" height="1000" scrolling="auto" marginwidth="0px" src="libraryUploadExcelApproval.aspx" frameborder="0" width="100%"></iframe>


                            </ContentTemplate>
                            <HeaderTemplate>
                                Approval of Upload Items
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="HT7" runat="server">
                            <ContentTemplate>
                                <iframe id="FEx5" height="1000" scrolling="auto" marginwidth="0px" src="libraryBulkPouchUpdate.aspx" frameborder="0" width="100%"></iframe>


                            </ContentTemplate>
                            <HeaderTemplate>
                                Bulk pouch update
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>


                    </ajaxToolkit:TabContainer>
                    <asp:HiddenField ID="Hiddenempid" runat="server" />
                    <asp:HiddenField ID="Hiddenbsuid" runat="server" />

                </div>

            </div>
        </div>
    </div>

</asp:Content>
