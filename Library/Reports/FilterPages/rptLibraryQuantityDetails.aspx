<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="rptLibraryQuantityDetails.aspx.vb" Inherits="Library_Reports_FilterPages_rptLibraryQuantityDetails" %>

<%@ Register Src="../UserControls/rptLibraryQuantityDetails.ascx" TagName="rptLibraryQuantityDetails"
    TagPrefix="uc1" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Library Quantity Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td align="left">

                            <uc1:rptLibraryQuantityDetails ID="RptLibraryQuantityDetails1" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>
