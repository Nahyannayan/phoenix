﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="rptLibraryDueRecords.aspx.vb" Inherits="Library_Reports_FilterPages_rptLibraryDueRecords" MasterPageFile="~/mainMasterPage.master" %>

<%@ Register Src="../UserControls/rptLibraryDueRecords.ascx" TagName="rptLibraryDueRecords" TagPrefix="uc1" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Library Items Dues
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <uc1:rptLibraryDueRecords ID="rptLibraryDueRecords1" runat="server" />
            </div>
        </div>
    </div>

</asp:Content>
