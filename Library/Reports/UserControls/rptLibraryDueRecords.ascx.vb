﻿Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Partial Class Library_Reports_UserControls_rptLibraryDueRecords
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Bindlibname()
                callCurrent_BsuShift()
                callGrade_ACDBind()
                callGrade_Section()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "pageload")
            End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub callCurrent_BsuShift()
        Try


            'Using Current_BsuACD As SqlDataReader = AccessStudentClass.GetActive_BSU_ACD_ID(Session("sBsuid"), Session("CLM"))
            '    If Current_BsuACD.HasRows = True Then
            '        While Current_BsuACD.Read
            ViewState("Current_ACD_ID") = Session("Current_ACD_ID")
            '        End While

            '    End If
            'End Using


            Dim di As ListItem
            Using Current_BsuShiftReader As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Session("sBsuid"), ViewState("Current_ACD_ID"))
                ddlShift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read
                        di = New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID"))
                        ddlShift.Items.Add(di)
                    End While
                    For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
                        'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                        If Not Session("Current_ACD_ID") Is Nothing Then
                            If UCase(ddlShift.Items(ItemTypeCounter).Text) = "NORMAL" Then
                                ddlShift.SelectedIndex = ItemTypeCounter
                            End If
                        End If
                    Next
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_ACDBind()
        Try
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID_LIB(ViewState("Current_ACD_ID"), Session("CLM"), ddlShift.SelectedValue)
                ddlGrade.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While
                End If
            End Using
            If ddlGrade.SelectedIndex <> -1 Then
                ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_Section()
        Try

            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex <> -1 Then

                If ddlGrade.SelectedItem.Value = "0" Then
                    GRD_ID = ""
                Else
                    GRD_ID = ddlGrade.SelectedItem.Value
                End If
            End If

            Dim SHF_ID As String = String.Empty
            If ddlShift.SelectedIndex = -1 Then
                SHF_ID = ""
            Else
                SHF_ID = ddlShift.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section_LIB(Session("sBsuid"), ViewState("Current_ACD_ID"), GRD_ID, SHF_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        callGrade_Section()
    End Sub
    Public Sub Bindlibname()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & Session("sBsuid") & "' ORDER BY LIBRARY_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl_libname.DataSource = ds
        ddl_libname.DataTextField = "LIBRARY_DIVISION_DES"
        ddl_libname.DataValueField = "LIBRARY_DIVISION_ID"
        ddl_libname.DataBind()

    End Sub
    Protected Sub btn_report_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_report.Click
        Dim param As New Hashtable
        param.Add("@PRODUCT_BSU_ID", Session("sBsuid"))
        param.Add("@LIBRARY_DIVISION_ID", ddl_libname.SelectedValue)
        If ddlGrade.SelectedIndex > 0 Then
            param.Add("@GRADE", ddlGrade.SelectedItem.Value)
        Else
            param.Add("@GRADE", DBNull.Value)
        End If
        If ddlSection.SelectedIndex > 0 Then
            param.Add("@SECTION", ddlSection.SelectedItem.Text.Trim())
        Else
            param.Add("@SECTION", DBNull.Value)
        End If
        If txtUniqueno.Text <> "" Then
            param.Add("@USER_NO", txtUniqueno.Text)
        Else
            param.Add("@USER_NO", DBNull.Value)
        End If
        If txtfromdate.Text <> "" Then
            param.Add("@ITEM_RETURN_FROMDATE", txtfromdate.Text)
        Else
            param.Add("@ITEM_RETURN_FROMDATE", DBNull.Value)
        End If
        If txttodate.Text <> "" Then
            param.Add("@ITEM_RETURN_TODATE", txttodate.Text)
        Else
            param.Add("@ITEM_RETURN_TODATE", DBNull.Value)
        End If
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "logo")
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_LIBRARY"
            .reportParameters = param
            If rbl_memberShiptype.SelectedValue = 1 Then
                param.Add("@USER_TYPE", rbl_memberShiptype.SelectedItem.Text)
                .reportPath = Server.MapPath("~/Library/Reports/Reports/rptLibraryDueRecords_students.rpt")
            ElseIf rbl_memberShiptype.SelectedValue = 2 Then
                param.Add("@USER_TYPE", rbl_memberShiptype.SelectedItem.Text)
                .reportPath = Server.MapPath("~/Library/Reports/Reports/rptLibraryDueRecords.rpt")
            Else
                param.Add("@USER_TYPE", DBNull.Value)
                .reportPath = Server.MapPath("~/Library/Reports/Reports/rptLibraryDueRecords.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
