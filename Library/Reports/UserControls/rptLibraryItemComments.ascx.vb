Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_Reports_UserControls_rptLibraryItemComments
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
           
        End If

    End Sub

    Protected Sub btncomment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncomment.Click
        Dim param As New Hashtable
        param.Add("@BSU_ID", HiddenBsuID.Value)
        param.Add("Search", "")

        If txtmasteridC.Text.Trim() <> "" Then
            param.Add("@MASTER_ID", txtmasteridC.Text.Trim())
        Else
            param.Add("@MASTER_ID", DBNull.Value)
        End If

        If txtisbn.Text.Trim() <> "" Then
            param.Add("@ISBN", txtisbn.Text.Trim())
        Else
            param.Add("@ISBN", DBNull.Value)
        End If

        If txtfromdateC.Text.Trim() <> "" Then
            param.Add("@FROM_DATE", txtfromdateC.Text.Trim())
        Else
            param.Add("@FROM_DATE", DBNull.Value)
        End If

        If txttodateC.Text.Trim() <> "" Then
            param.Add("@TO_DATE", txttodateC.Text.Trim())
        Else
            param.Add("@TO_DATE", DBNull.Value)
        End If


        Dim reportpath = "~/Library/Reports/Reports/rptLibraryItemComments.rpt"
        ViewReports(param, reportpath)

    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Public Sub ViewReports(ByVal param As Hashtable, ByVal reportpath As String)

        param.Add("UserName", Session("sUsr_name"))
        param.Add("@IMG_BSU_ID", HiddenBsuID.Value)
        param.Add("@IMG_TYPE", "LOGO")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_LIBRARY"
            .reportParameters = param
            .reportPath = Server.MapPath(reportpath)
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
End Class
