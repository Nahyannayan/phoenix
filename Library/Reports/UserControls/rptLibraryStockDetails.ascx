<%@ Control Language="VB" AutoEventWireup="false" CodeFile="rptLibraryStockDetails.ascx.vb" Inherits="Library_Reports_UserControls_rptLibraryStockDetails" %>
<table cellpadding="5" cellspacing="0" class="matters"
    width="100%">
    <%--<tr>
        <td class="subheader_img">
            Library Stock Details</td>
    </tr>--%>
    <tr>
        <td>
            <table width="100%">
                <tr>
                    <td width="20%">
                         <span class="field-label">Library Divisions</span></td>
                    
                    <td width="30%">
                        <asp:DropDownList ID="ddLibraryDivStock" runat="server" AutoPostBack="True">
                        </asp:DropDownList> </td>
                   <td width="20%">
                        <span class="field-label">Library Sub Division</span></td>
                  
                    <td width="30%">
                        <asp:DropDownList ID="ddLibSubDivStock" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    
                </tr>
                <tr>
                   
                    <td width="20%">
                      <span class="field-label">Shelf</span></td>
                   
                    <td width="30%">
                        <asp:DropDownList ID="ddshelfstock" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td width="20%">
                        <span class="field-label">Rack</span></td>
                   
                    <td width="30%">
                        <asp:DropDownList ID="ddrackstock" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td width="20%">
                       <span class="field-label"> Master ID </span></td>
                   
                    <td width="30%">
                        <asp:TextBox ID="txtmasterid" runat="server" Width="142px"></asp:TextBox>
                    </td>
                    <td width="20%">
                       <span class="field-label"> Accession No </span></td>
                  
                    <td width="30%">
                        <asp:TextBox ID="txtstockid" runat="server" Width="142px"></asp:TextBox>
                    </td>
                    
                </tr>
                <tr>
                    <td width="20%">
                        <span class="field-label">ISBN </span></td>
                    
                    <td width="30%">
                        <asp:TextBox ID="txtisbn" runat="server" Width="142px"></asp:TextBox>
                    </td>
                    <td width="20%">
                        <span class="field-label">Title </span></td>
                   
                    <td width="30%">
                        <asp:TextBox ID="txttitle" runat="server" Width="142px"></asp:TextBox>
                    </td>
                    
                </tr>
                <tr>
                    <td width="20%">
                        <span class="field-label">Author</span></td>
                   
                    <td width="30%">
                        <asp:TextBox ID="txtauthor" runat="server" Width="142px"></asp:TextBox>
                    </td>
                    <td width="20%">
                        <span class="field-label">Publisher </span></td>
                    
                    <td width="30%">
                        <asp:TextBox ID="txtpublisher" runat="server" Width="142px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td width="20%">
                        <span class="field-label">Subject</span></td>
                  
                    <td width="30%">
                        <asp:TextBox ID="txtsubjects" runat="server" Width="142px"></asp:TextBox>
                    </td>
                    <td width="20%">
                       <span class="field-label"> Publisher Date </span></td>
                   
                    <td width="30%">
                        <asp:TextBox ID="txtpublishdate" runat="server" Width="142px"></asp:TextBox>
                    </td>
                    
                </tr>
                <tr>
                    <td width="20%">
                        <span class="field-label">Entry Date</span></td>
                   
                    <td width="30%">
                        <asp:TextBox ID="txtentrydate" runat="server" Width="142px"></asp:TextBox>
                    </td>
                    <td width="20%">
                        <span class="field-label">Age Group</span></td>
                   
                    <td width="30%">
                        <asp:DropDownList ID="DDFromAge" runat="server">
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDToAge" runat="server">
                        </asp:DropDownList>
                    </td>
                   
                </tr>
                <tr>
                     <td width="20%">
                        <span class="field-label">Available</span></td>
                   
                    <td width="30%">
                        <asp:RadioButtonList ID="RadioAvl" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True" Text="All" Value="ALL"></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                            <asp:ListItem Text="No" Value="False"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td width="20%">
                       <span class="field-label"> Item </span></td>
                   
                    <td width="30%">
                        <asp:DropDownList ID="dditems" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                   
                    <td width="20%">
                        <span class="field-label">Status </span></td>
                   
                    <td width="30%">
                        <asp:DropDownList ID="ddStatus" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td align="center" colspan="4">
                        <asp:Button ID="btnstockdetails" runat="server" CssClass="button" Text="Generate Report" />
                    </td>
                </tr>
            </table>
            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                PopupButtonID="txtpublishdate" TargetControlID="txtpublishdate">
            </ajaxToolkit:CalendarExtender>
            <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                PopupButtonID="txtentrydate" TargetControlID="txtentrydate">
            </ajaxToolkit:CalendarExtender>
        </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenBsuID" runat="server" />
