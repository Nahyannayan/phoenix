<%@ Control Language="VB" AutoEventWireup="false" CodeFile="rptLibraryItems.ascx.vb" Inherits="Library_Reports_UserControls_rptLibraryItems" %>
<div class="matters">
<div align="left" class="matters">
        <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="1">
            <ajaxToolkit:TabPanel ID="HT1" runat="server">
                <ContentTemplate>
                
    <table border="1" class="matters" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
        <tr>
            <td class="subheader_img">
                Library Stock Details</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            Library Divisions</td>
                        <td>
                            :</td>
                        <td>
                            <asp:DropDownList ID="ddLibraryDivStock" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Library Sub Division</td>
                        <td>
                            :</td>
                        <td>
                            <asp:DropDownList ID="ddLibSubDivStock" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td>
                            Shelf</td>
                        <td>
                            :</td>
                        <td>
                            <asp:DropDownList ID="ddshelfstock" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td>
                            Rack</td>
                        <td>
                            :</td>
                        <td>
                            <asp:DropDownList ID="ddrackstock" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            Master ID</td>
                        <td>
                            :</td>
                        <td>
                            <asp:TextBox ID="txtmasterid" runat="server" Width="142px"></asp:TextBox></td>
                        <td>
                            Accession No</td>
                        <td>
                            :</td>
                        <td>
                            <asp:TextBox ID="txtstockid" runat="server" Width="142px"></asp:TextBox></td>
                        <td>
                            ISBN</td>
                        <td>
                            :</td>
                        <td>
                            <asp:TextBox ID="txtisbn" runat="server" Width="142px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            Title</td>
                        <td>
                            :</td>
                        <td>
                            <asp:TextBox ID="txttitle" runat="server" Width="142px"></asp:TextBox></td>
                        <td>
                            Author</td>
                        <td>
                            :</td>
                        <td>
                            <asp:TextBox ID="txtauthor" runat="server" Width="142px"></asp:TextBox></td>
                        <td>
                            Publisher</td>
                        <td>
                            :</td>
                        <td>
                            <asp:TextBox ID="txtpublisher" runat="server" Width="142px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            Subject</td>
                        <td>
                            :</td>
                        <td>
                            <asp:TextBox ID="txtsubjects" runat="server" Width="142px"></asp:TextBox></td>
                        <td>
                            Publisher Date</td>
                        <td>
                            :</td>
                        <td>
                            <asp:TextBox ID="txtpublishdate" runat="server" Width="142px"></asp:TextBox></td>
                        <td>
                            Entry Date</td>
                        <td>
                            :</td>
                        <td>
                            <asp:TextBox ID="txtentrydate" runat="server" Width="142px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            Age Group</td>
                        <td>
                            :</td>
                        <td>
                            <asp:DropDownList ID="DDFromAge" runat="server">
                            </asp:DropDownList>
                            <asp:DropDownList ID="DDToAge" runat="server">
                            </asp:DropDownList></td>
                        <td>
                            Available</td>
                        <td>
                            :</td>
                        <td>
                            <asp:RadioButtonList ID="RadioAvl" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Text="All" Value="ALL"></asp:ListItem>
                                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                <asp:ListItem Text="No" Value="False"></asp:ListItem>
                            </asp:RadioButtonList></td>
                        <td>
                            Item</td>
                        <td>
                            :</td>
                        <td>
                            <asp:DropDownList ID="dditems" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            Status</td>
                        <td>
                            :</td>
                        <td>
                            <asp:DropDownList ID="ddStatus" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="9">
                            <asp:Button ID="btnstockdetails" runat="server" CssClass="button" Text="Generate Report" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtpublishdate"
                    TargetControlID="txtpublishdate" Enabled="True">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtentrydate"
                    TargetControlID="txtentrydate" Enabled="True">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
    </table>

                </ContentTemplate>
                <HeaderTemplate>
                   Library Stock Details
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT2" runat="server">
                <ContentTemplate>

<table border="1" class="matters"  bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
    <tr>
        <td class="subheader_img">
            Library Quantity Details</td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        Library Divisions</td>
                    <td>
                        :</td>
                    <td>
                        <asp:DropDownList ID="ddlibLibDivQuality" runat="server">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        Available</td>
                    <td>
                        :</td>
                    <td>
                        <asp:RadioButtonList ID="RadioAvailable" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="All" Selected="True" Value="ALL"></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="YES"></asp:ListItem>
                            <asp:ListItem Text="No" Value="NO"></asp:ListItem>
                        </asp:RadioButtonList></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnlibmembershipsusers" runat="server" CssClass="button" Text="Generate Report" /></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

                </ContentTemplate>
                <HeaderTemplate>
                    Library Quantity Details
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>
  
    </div>
<asp:HiddenField ID="HiddenBsuID" runat="server" />
</div>