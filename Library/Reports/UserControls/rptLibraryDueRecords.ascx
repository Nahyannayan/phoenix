﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="rptLibraryDueRecords.ascx.vb" Inherits="Library_Reports_UserControls_rptLibraryDueRecords" %>

<style type="text/css">
    .style1 {
        height: 20px;
    }

    table td select, table td input[type=text], table td input[type=date], table td textarea 
    {
       min-width: 10% !important;        
    }
</style>
<script language="javascript" type="text/javascript">

    function show(cbk) {
        var aa = document.getElementById('<%=rbl_memberShiptype.ClientID %>').value
        if (aa == 1) {
            document.getElementById('<%=trgrade.ClientID %>').style.display = ''
        document.getElementById('<%=trsection.ClientID %>').style.display = ''
        document.getElementById('<%=trShift.ClientID %>').style.display = ''
    }
    else {
        document.getElementById('<%=trgrade.ClientID %>').style.display = 'none'
        document.getElementById('<%=trsection.ClientID %>').style.display = 'none'
        document.getElementById('<%=trShift.ClientID %>').style.display = 'none'
    }
}
</script>

<center>
<table width="100%" runat="server" id="tablelib" cellspacing="0">
   <%-- <tr class="subheader_img">
        <td colspan="3" align="center" style="height: 20px;">
            Library Due Records</td>
    </tr>--%>
    <tr class="matters" align="left" >
        <td >
           <span class="field-label"> Library Name </span></td>
       
        <td>
                     <asp:DropDownList ID="ddl_libname" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr class="matters" align="left" >
        <td>
           <span class="field-label">  Membership Type </span></td>
       
        <td>
            <asp:DropDownList ID="rbl_memberShiptype"
                runat="server" RepeatDirection="Horizontal" onchange="javascript:show(this)">
            <asp:ListItem Value="0" Selected="True">ALL</asp:ListItem>
                <asp:ListItem Value="1">STUDENT</asp:ListItem>
                <asp:ListItem Value="2">STAFF</asp:ListItem>
              </asp:DropDownList>
        </td>
    </tr>
    <tr class="matters" align="left" >
        <td>
           <span class="field-label">  Unique No </span></td>
      
        <td>
            <asp:TextBox ID="txtUniqueno" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr class="matters" runat="server" id="trShift" align="left" >
        <td>
          <span class="field-label">   Shift </span></td>
      
        <td>
            <asp:DropDownList ID="ddlShift" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr class="matters" runat="server" id="trgrade" align="left" >
        <td>
           <span class="field-label">  Grade </span></td>
       
        <td>
            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>
    </tr>
    <tr class="matters" runat="server" id="trsection" align="left" >
        <td>
           <span class="field-label">  Section </span></td>
     
        <td>
            <asp:DropDownList ID="ddlSection" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr class="matters" align="left" >
        <td>
            <span class="field-label"> Return Date Between </span></td>
     
        <td>
            <asp:TextBox ID="txtfromdate" runat="server" Width="36%"></asp:TextBox>
            <ajaxToolkit:CalendarExtender ID="txtfromdate_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txtfromdate">
            </ajaxToolkit:CalendarExtender>
and
            <asp:TextBox ID="txttodate" runat="server" Width="36%"></asp:TextBox>
            <ajaxToolkit:CalendarExtender ID="txttodate_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txttodate">
            </ajaxToolkit:CalendarExtender>
                </td>
    </tr>
    <tr class="matters" >
        <td align="center" colspan="4">
            <asp:Button ID="btn_report" runat="server" CssClass="button" 
                Text="Generate Report" />
        </td>
    </tr>
</table>
</center>
<script language="javascript" type="text/javascript">
    show('aa')
</script>
