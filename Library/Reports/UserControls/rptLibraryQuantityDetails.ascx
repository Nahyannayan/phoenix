<%@ Control Language="VB" AutoEventWireup="false" CodeFile="rptLibraryQuantityDetails.ascx.vb" Inherits="Library_Reports_UserControls_rptLibraryQuantityDetails" %>
<table  cellpadding="5" cellspacing="0" 
    width="100%">
   <%-- <tr>
        <td class="subheader_img">
            Library Quantity Details</td>
    </tr>--%>
    <tr>
        <td>
            <table width="100%">
                <tr>
                    <td>
                      <span class="field-label">  Library Divisions</span></td>
                   
                    <td>
                        <asp:DropDownList ID="ddlibLibDivQuality" runat="server">
                        </asp:DropDownList>
                    </td>
               
                    <td>
                        <span class="field-label"> Available </span></td>
                   
                    <td>
                        <asp:RadioButtonList ID="RadioAvailable" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True" Text="All" Value="ALL"></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="YES"></asp:ListItem>
                            <asp:ListItem Text="No" Value="NO"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>                                       
                    <td colspan="4" align="center">
                        <asp:Button ID="btnlibmembershipsusers" runat="server" CssClass="button" Text="Generate Report" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenBsuID" runat="server" />
