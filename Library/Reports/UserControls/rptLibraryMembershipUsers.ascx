<%@ Control Language="VB" AutoEventWireup="false" CodeFile="rptLibraryMembershipUsers.ascx.vb" Inherits="Library_Reports_UserControls_rptLibraryMembershipUsers" %>

<div class="matters">

<table  cellpadding="5" cellspacing="0" width="100%">
  <%--  <tr>
        <td class="subheader_img">
            Library Memberships Users</td>
    </tr>--%>
    <tr>
        <td>
            <table width="100%"> 
                <tr>
                    <td align="left">
                       <span class="field-label">Type </span></td>
                   
                    <td align="left">
                        <asp:RadioButtonList ID="RadioUser" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True" Text="STUDENT" Value="STUDENT"></asp:ListItem>
                            <asp:ListItem Text="EMPLOYEE" Value="EMPLOYEE"></asp:ListItem>
                        </asp:RadioButtonList></td>
               
                    <td align="left">
                        <span class="field-label">Library Divisions</span></td>
                   
                    <td align="left">
                        <asp:DropDownList ID="ddlibmembershipsusersLibDiv" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td align="left">
                       <span class="field-label"> Membership</span></td>
                   
                    <td align="left">
                        <asp:DropDownList ID="ddmemberships" runat="server">
                        </asp:DropDownList></td>
                    <td colspan="2"></td>
                </tr>
                <tr>                                    
                    <td align="center" colspan="4">
                        <asp:Button ID="btnlibmembershipsusers" runat="server" CssClass="button" Text="Generate Report" /></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenBsuID" runat="server" />
</div>