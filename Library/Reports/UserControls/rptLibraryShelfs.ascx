<%@ Control Language="VB" AutoEventWireup="false" CodeFile="rptLibraryShelfs.ascx.vb" Inherits="Library_Reports_UserControls_rptLibraryShelfs" %>
<table  cellpadding="5" cellspacing="0"  width="100%">
   <%-- <tr>
        <td class="subheader_img">
            Library Shelves</td>
    </tr>--%>
    <tr>
        <td>
            <table width="100%">
                <tr>
                    <td>
                       <span class="field-label"> Library Divisions </span></td>
                    <td align="left">
                        <asp:DropDownList ID="ddrptlibrarydivShelfs" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                
                    <td>
                      <span class="field-label">  Library Sub &nbsp;Divisions </span></td>
                    <td>
                        <asp:DropDownList ID="ddrptlibsubdivshelfs" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    
                    <td colspan="4" align="center"> 
                        <asp:Button ID="btnlibraryshelfs" runat="server" CssClass="button" Text="Generate Report" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenBsuID" runat="server" />
