<%@ Control Language="VB" AutoEventWireup="false" CodeFile="rptLibraryItemReservations.ascx.vb" Inherits="Library_Reports_UserControls_rptLibraryItemReservations" %>
<table cellpadding="5" cellspacing="0"
    width="100%">
    <%--<tr>
        <td class="subheader_img">
            Library Item Reservations</td>
    </tr>--%>
    <tr>
        <td>
            <table width="100%">
                <tr>
                    <td align="left">
                        <span class="field-label">Library Divisions </span></td>

                    <td align="left">
                        <asp:DropDownList ID="ddlibLibDiv" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left"></td>

                    <td align="left"></td>
                </tr>
                <tr>
                    <td align="left">
                        <span class="field-label">Master ID. </span></td>

                    <td align="left">
                        <asp:TextBox ID="txtmasterid" runat="server"></asp:TextBox>
                    </td>
                    <td align="left">
                        <span class="field-label">Accession No. </span></td>

                    <td align="left">
                        <asp:TextBox ID="txtstockid" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <span class="field-label">User No. </span></td>

                    <td align="left">
                        <asp:TextBox ID="txtuserno" runat="server"></asp:TextBox>
                    </td>
                    <td align="left">
                        <span class="field-label">User Type </span></td>

                    <td align="left">
                        <asp:RadioButtonList ID="RadioUser" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True" Text="ALL" Value="ALL"></asp:ListItem>
                            <asp:ListItem Text="STUDENT" Value="STUDENT"></asp:ListItem>
                            <asp:ListItem Text="EMPLOYEE" Value="EMPLOYEE"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>

                <tr>
                    <td align="left">
                        <span class="field-label">Select Student Grade/Section </span></td>

                    <td align="left">
                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                        </asp:DropDownList>

                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left"></td>

                    <td align="left"></td>
                </tr>


                <tr>
                    <td align="left">
                        <span class="field-label">Reservation Cancel </span></td>

                    <td align="left">
                        <asp:RadioButtonList ID="Radioreservationcancel" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True" Text="All" Value="ALL"></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="YES"></asp:ListItem>
                            <asp:ListItem Text="No" Value="NO"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td align="left">
                        <span class="field-label">Transaction Done </span></td>

                    <td align="left">
                        <asp:RadioButtonList ID="radioTransactiondone" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True" Text="All" Value="ALL"></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="YES"></asp:ListItem>
                            <asp:ListItem Text="No" Value="NO"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <span class="field-label">From Date </span></td>

                    <td align="left">
                        <asp:TextBox ID="txtfromdate" runat="server"></asp:TextBox>
                    </td>
                    <td align="left">
                        <span class="field-label">To Date </span></td>

                    <td align="left">
                        <asp:TextBox ID="txtTodate" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="4">
                        <asp:Button ID="btnlibItemtransaction" runat="server" CssClass="button" Text="Generate Report" />
                    </td>
                </tr>
            </table>
            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                PopupButtonID="txtfromdate" TargetControlID="txtfromdate">
            </ajaxToolkit:CalendarExtender>
            <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                PopupButtonID="txtTodate" TargetControlID="txtTodate">
            </ajaxToolkit:CalendarExtender>
        </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenBsuID" runat="server" />
