<%@ Control Language="VB" AutoEventWireup="false" CodeFile="rptLibraryMemberships.ascx.vb" Inherits="Library_Reports_UserControls_rptLibraryMemberships" %>
<table  cellpadding="5" cellspacing="0" class="matters"
    width="100%">
   <%-- <tr>
        <td class="subheader_img">
            Library Memberships</td>
    </tr>--%>
    <tr>
        <td>
            <table width="100%">
                <tr>
                    <td width="20%">
                       <span class="field-label">Library Divisions </span> </td>
                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddlibmembershipsLibDiv" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td></td>
                </tr>
                <tr>                   
                    <td colspan="3" align="center">
                        <asp:Button ID="btnlibmemberships" runat="server" CssClass="button" Text="Generate Report" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenBsuID" runat="server" />
