﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Library_Users_Usercontrols_libraryUserSuggestions
    Inherits System.Web.UI.UserControl


    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBsuID.Value = Encr_decrData.Decrypt(Request.QueryString("sbsuid").Replace(" ", "+"))
            Hiddenlibrarydivid.Value = Encr_decrData.Decrypt(Request.QueryString("Libraryid").Replace(" ", "+"))
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", HiddenBsuID.Value)

            If Hiddenlibrarydivid.Value <> "" Then
                pParms(1) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", Hiddenlibrarydivid.Value)
            End If

            pParms(2) = New SqlClient.SqlParameter("@SUGGESTIONS", txtsuggestions.Text.Trim())


            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_LIBRARY_USER_SUGGESTIONS", pParms)


            transaction.Commit()

            txtsuggestions.Text = ""


        Catch ex As Exception

            lblMessage.Text = "Error occurred while transactions. " & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()

        End Try


    End Sub

End Class
