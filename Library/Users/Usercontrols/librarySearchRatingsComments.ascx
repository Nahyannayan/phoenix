<%@ Control Language="VB" AutoEventWireup="false" CodeFile="librarySearchRatingsComments.ascx.vb"
    Inherits="Library_UserControls_librarySearchRatingsComments" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<script type="text/javascript">
 
function Redirect(value)
{
var bsu_id = document.getElementById('<%= HiddenBsuID.ClientID %>').value;
window.showModalDialog('libraryRatingsComments.aspx?MasterId=' + value + '&bsu_id=' + bsu_id, '', 'dialogHeight=500px,dialogWidth=530px,scrollbars=yes,resizable=no,directories=yes');
return false; 
}

function ViewDetails(value) {
    window.open('../libraryDetailView.aspx?id=' + value +'&Screen=User', '', 'Height=800px,Width=1020px,scrollbars=yes,resizable=no,directories=yes');
    return false;
}

function Reservations(Masterid, Libraryid, sbsuid) {
    window.showModalDialog('LibraryUserReservation.aspx?Masterid=' + Masterid + '&Libraryid=' + Libraryid + '&sbsuid=' + sbsuid, '', 'dialogHeight=300px,dialogWidth=530px,scrollbars=yes,resizable=no,directories=yes');
    return false;
}


function pathDetails(value) 
{
alert(value)
}

</script>

<style type="text/css">
    .keyboardInput {
        color: Black;
        border-width: 1px !important;
        border-style: solid !important;
        font-size: Medium;
        padding: 10px !important;
        font-weight: bold !important;
        height: 20px !important;
        border-color: rgba(0,0,0,0.16) !important;
        width: 400px !important;
        border-radius: 6px !important;
    }
    a {
        color: #6a923a !important;
    }
    a:hover {
        color: #333333 !important;
    }
</style>
<div class="matters">



<asp:Panel ID="P1" DefaultButton="ImageSearch" runat="server">
    <div id="divmain" align="center" style="height: 600px; vertical-align: middle; text-align:center;" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            <br />
                <asp:TextBox ID="TextBox1" runat="server" 
                    BorderWidth="2px" CssClass="keyboardInput" EnableTheming="false" ></asp:TextBox>
                &nbsp;
                <asp:ImageButton ID="ImageSearch" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" />
                   
                  <ajaxToolkit:TextBoxWatermarkExtender ID="TW1" TargetControlID="TextBox1" WatermarkText="Search......" runat="server"></ajaxToolkit:TextBoxWatermarkExtender>
       <br />
         <br />
           <br />
                <asp:GridView ID="GridItems" runat="server" Width="550px" ShowHeader="False" BorderWidth="0" CellPadding="0" CellSpacing="1" AutoGenerateColumns="False"
                    AllowPaging="True" PageSize="3">
                    <Columns>
                        <asp:TemplateField >
                            <ItemTemplate  >
                            <asp:Panel ID="T1" runat="server">
                           
                                <table  align="left" width="100%" >
                                    <tr align="left">
                                        <td rowspan="2" valign="middle"  align=center width="80px">
                                           
                                                <asp:ImageButton ID="ImageItem" runat="server" Enabled='<%# Eval("SHOWURL") %>' ImageUrl='<%# Eval("PRODUCT_IMAGE_URL") %>'
                                                    PostBackUrl='<%# Eval("PRODUCT_URL") %>' />
                                                <br />
                                               <ajaxToolkit:Rating ID="CRatings" runat="server" CurrentRating='<%# Eval("AVG_RATING") %>'
                                    Width="65px" EmptyStarCssClass="emptyRatingStar" FilledStarCssClass="filledRatingStar"
                                    MaxRating="5" ReadOnly="true" StarCssClass="ratingStar" WaitingStarCssClass="savedRatingStar">
                                </ajaxToolkit:Rating>
                                            
                                        </td>
                                     
                                        <td style="text-transform: capitalize; color: #000000; font-weight: bold;
                                            font-size:15px;">
                                            <%#Eval("ITEM_TITLE")%>
                                        </td>
                                        <td style="text-transform: capitalize; color: #000000; font-weight: bold;
                                            font-size: smaller;">
                                            &nbsp;</td>
                                    </tr>
                                    <tr align="left">
                                        <td>
                                            <table >
                                                <tr>
                                                    <td>
                                                      <b>Type</b> 
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <%#Eval("ITEM_DES")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                       <b> Author</b>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <%#Eval("AUTHOR")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b> Publisher</b>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <%#Eval("PUBLISHER")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b> Available</b>
                                                    </td>
                                                    <td>:
                                                    </td>
                                                    <td>
                                                     <b><span style="color:Red">  <%#Eval("AVAILABLE")%> </span> &nbsp;&nbsp;(<%#Eval("LIBRARY_DIVISION_DES")%>\ <%#Eval("LIBRARY_SUB_DIVISION_DES")%> )</b> <asp:ImageButton ID="ImageButton4" runat="server" ToolTip="Location" 
                                                        OnClientClick='<%# Eval("LOCATION") %>' ImageUrl="~/Images/Library/path.png" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right">
                                            <br />
                                            <asp:ImageButton ID="ImageButton3" runat="server" Height="30px" ToolTip="Details" 
                                                 OnClientClick='<%# Eval("REDIRECT") %>' ImageUrl="~/Images/Library/Details.png" Width="30px" />
                                            <br />
                                            <asp:ImageButton ID="ImageButton2" runat="server" Height="30px" ToolTip="Reservation"
                                                OnClientClick='<%# Eval("RESERVATIONS") %>' ImageUrl="~/Images/Library/location.png" Width="30px" />
                                            <br />
                                            <asp:ImageButton ID="ImageButton1" runat="server" Height="30px" 
                                                ImageUrl="~/Images/Library/comments.png" 
                                                OnClientClick='<%# Eval("COMMENT_REDIRECT") %>' ToolTip="Comments" 
                                                Width="30px" />
                                        </td>
                                    </tr>
                                </table>
                             </asp:Panel>
                               <ajaxToolkit:RoundedCornersExtender ID="RC1" Corners="All" Color="HotTrack"  Radius="4" TargetControlID="T1" runat="server"></ajaxToolkit:RoundedCornersExtender> 
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                
                <asp:HiddenField ID="HiddenBsuID" runat="server" />
                <asp:HiddenField ID="Hiddenlibrarydivid" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Panel>
</div>