﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LibraryFeaturedBook.ascx.vb"
    Inherits="Library_Users_Usercontrols_LibraryFeaturedBook" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    
<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<br />
<br />
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<table align="left" width="100%">
    <tr align="left">
        <td rowspan="2" valign="middle" align="center" width="80px">
            <asp:ImageButton ID="ImageItem" OnClientClick="javascript:return false;" runat="server" />
            <br />
            <ajaxToolkit:Rating ID="CRatings" runat="server" 
                Width="65px" EmptyStarCssClass="emptyRatingStar" FilledStarCssClass="filledRatingStar"
                MaxRating="5" ReadOnly="true" StarCssClass="ratingStar" WaitingStarCssClass="savedRatingStar">
            </ajaxToolkit:Rating>
        </td>
        <td style="text-transform: capitalize; color: #000000; font-weight: bold; font-size: larger;">
            <asp:Label ID="lbltitle" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr align="left">
        <td>
            <table>
                <tr>
                    <td>
                        <b>Type</b>
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <asp:Label ID="lbldes" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Author</b>
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <asp:Label ID="lblauthor" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Publisher</b>
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <asp:Label ID="lblpublisher" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <b>Description</b>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <asp:Label ID="lbldescription" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center" >
        <br />
        <br />
        
            <asp:Button ID="btnnext" runat="server" Text="Next" CssClass="button" 
                Width="141px" />
        </td>
    </tr>
</table>

<asp:HiddenField ID="HiddenBsuID" runat="server" />
<asp:HiddenField ID="Hiddenlibrarydivid" runat="server" />

</ContentTemplate>
</asp:UpdatePanel>







