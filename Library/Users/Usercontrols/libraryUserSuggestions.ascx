﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryUserSuggestions.ascx.vb"
    Inherits="Library_Users_Usercontrols_libraryUserSuggestions" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<div class="matters" align="center">

<br />
<br />
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="400"
                align="center">
                <tr>
                    <td class="subheader_img">
                        Suggestions
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtsuggestions" runat="server" EnableTheming="false" Height="300px"
                                        TextMode="MultiLine" Width="400px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Submit" Width="113px" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="HiddenBsuID" runat="server" />
            <asp:HiddenField ID="Hiddenlibrarydivid" runat="server" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None"
                ErrorMessage="Please enter your suggestions" ControlToValidate="txtsuggestions"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
