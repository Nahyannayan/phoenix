Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Xml
Partial Class Library_UserControls_librarySearchRatingsComments
    Inherits System.Web.UI.UserControl

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Encr_decrData.Decrypt(Request.QueryString("sbsuid").Replace(" ", "+"))
            Hiddenlibrarydivid.Value = Encr_decrData.Decrypt(Request.QueryString("Libraryid").Replace(" ", "+"))
            TextBox1.Focus()
            BindGrid()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " SELECT *,'javascript:ViewDetails(''' + CONVERT(VARCHAR,MASTER_ID) + '''); return false;' REDIRECT,'javascript:pathDetails(''' + SHELF_ROUT_DES + '''); return false;' LOCATION ,'javascript:Reservations(''' + CONVERT(VARCHAR,MASTER_ID) + ''',''' + CONVERT(VARCHAR, LIBRARY_DIVISION_ID) + ''',''' + PRODUCT_BSU_ID +'''); return false;' RESERVATIONS FROM VIEW_USERS_SEARCH where PRODUCT_BSU_ID='" & HiddenBsuID.Value & "'"


        If TextBox1.Text.Trim() <> "" Then
            str_query &= " AND REPLACE(ITEM_TITLE+AUTHOR+PUBLISHER+SUBJECT+PRODUCT_DESCRIPTION,' ','') LIKE '%" & TextBox1.Text.Trim().Replace(" ", "") & "%'"
        End If

        If Hiddenlibrarydivid.Value <> "0" Then
            str_query &= "AND LIBRARY_DIVISION_ID='" & Hiddenlibrarydivid.Value & "'"
        End If

        If Request.QueryString("NewArrival") = "1" Then

            str_query &= " ORDER BY DAYS_PERIOD_ENTRY "

        ElseIf Request.QueryString("MostRated") = "1" Then

            str_query &= " ORDER BY AVG_RATING DESC "

        Else

            str_query &= " ORDER BY ITEM_TITLE "

        End If



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GridItems.DataSource = ds
        GridItems.DataBind()

    End Sub

    Protected Sub GridItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridItems.PageIndexChanging
        GridItems.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub ImageSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSearch.Click
        BindGrid()
    End Sub

End Class
