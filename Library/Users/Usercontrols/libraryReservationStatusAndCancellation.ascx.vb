﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Library_Users_Usercontrols_libraryReservationStatusAndCancellation
    Inherits System.Web.UI.UserControl

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBsuID.Value = Encr_decrData.Decrypt(Request.QueryString("sbsuid").Replace(" ", "+"))
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Protected Sub btnStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus.Click

        BindGrid()

    End Sub

    Public Sub BindGrid()
        lblMessage.Text = ""

        Dim returnvalue = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = "SELECT * FROM  dbo.VIEW_USER_RESERVATION_HISTORY WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' AND USER_ID='" & txtUserreservationNo.Text.Trim() & "' AND USER_TYPE='" & RadioUserReservation.SelectedValue & "' ORDER BY ENTRY_DATE DESC"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            TableHistory.Visible = True
            GridHistory.DataSource = ds
            GridHistory.DataBind()
        Else
            lblMessage.Text = "No records exists."
            TableHistory.Visible = False
        End If
    End Sub
    Protected Sub GridHistory_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridHistory.PageIndexChanging

        GridHistory.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub

    Protected Sub GridHistory_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridHistory.RowCommand
        If e.CommandName = "CancelReservation" Then
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblMessage.Text = ""
            Try

                Dim query = "UPDATE LIBRARY_ITEM_RESERVATION SET TRAN_RECORD_ID=0 , RESERVATION_CANCEL='True' WHERE RESERVATION_ID='" & e.CommandArgument & "'"

                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)
                lblMessage.Text = "Reservation cancellation done successfully"
                transaction.Commit()

            Catch ex As Exception
                lblMessage.Text = "Error occurred while transactions. " & ex.Message
                transaction.Rollback()

            Finally
                connection.Close()

            End Try

            BindGrid()


        End If

    End Sub
End Class
