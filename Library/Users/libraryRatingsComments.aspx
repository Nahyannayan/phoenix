<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryRatingsComments.aspx.vb" Inherits="Library_libraryRatingsComments" %>

<%@ Register Src="~/Library/Users/Usercontrols/libraryRatingsComments.ascx" TagName="libraryRatingsComments"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Comments and Ratings Entry</title>
    <base target="_self" />
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Ratings.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Librarystyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:libraryRatingsComments ID="LibraryRatingsComments1" runat="server" />
    
    </div>
    </form>
</body>
</html>
