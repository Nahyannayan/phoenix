﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LibraryUserReservation.aspx.vb" Inherits="Library_LibraryUserReservation" %>

<%@ Register src="Usercontrols/LibraryUserReservation.ascx" tagname="LibraryUserReservation" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Library Item Reservations </title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Librarystyle.css" rel="stylesheet" type="text/css" />
    <base target="_self" /> 
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:LibraryUserReservation ID="LibraryUserReservation1" runat="server" />
    
    </div>
    </form>
</body>
</html>
