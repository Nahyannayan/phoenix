﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Xml
Partial Class Library_UserControls_libraryStudentOnlineReservation
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            ViewState("stu_id") = "95182"
            ViewState("bsu_id") = "126008"

            BindGrid()
        End If

    End Sub


    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = "select DISTINCT MASTER_ID, ITEM_DES,ITEM_TITLE,AUTHOR,PUBLISHER,A.LIBRARY_DIVISION_ID,PRODUCT_IMAGE_URL,PRODUCT_URL,'javascript:reserveopen('''+ convert(varchar,A.LIBRARY_DIVISION_ID) +''',''' + convert(varchar,MASTER_ID) +''','''+ STU_NO +''');return false;' as Rwindow from dbo.VIEW_LIBRARY_RECORDS A " & _
            " INNER JOIN (SELECT DISTINCT LIBRARY_DIVISION_ID FROM dbo.LIBRARY_MEMBERSHIP_USERS WHERE USER_TYPE='STUDENT' AND USER_ID=" & ViewState("stu_id") & ") B on A.LIBRARY_DIVISION_ID=B.LIBRARY_DIVISION_ID " & _
            " INNER JOIN OASIS.dbo.STUDENT_M C ON C.STU_ID=" & ViewState("stu_id") & " " & _
            " where AVAILABILITY='Y' AND ACTIVE =1 AND RACK_ID IS NOT NULL and (CIRCULATORY IS NULL OR CIRCULATORY=1) AND PRODUCT_BSU_ID='" & ViewState("bsu_id") & "' "

        If txtsearch.Text.Trim() <> "" Then
            Sql_Query &= " AND REPLACE(ITEM_TITLE+AUTHOR+PUBLISHER+SUBJECT,' ','') LIKE '%" & txtsearch.Text.Trim().Replace(" ", "") & "%'"
        End If


        Sql_Query &= " ORDER BY A.LIBRARY_DIVISION_ID,ITEM_TITLE "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        GridItem.DataSource = ds
        GridItem.DataBind()


    End Sub


    Protected Sub GridItem_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridItem.PageIndexChanging
        GridItem.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnshow_Click(sender As Object, e As System.EventArgs) Handles btnshow.Click
        BindGrid()
    End Sub
End Class
