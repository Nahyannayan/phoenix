﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryStudentOnlineReservation.ascx.vb" Inherits="Library_UserControls_libraryStudentOnlineReservation" %>

    <script type="text/javascript" language="javascript">

    function reserveopen(a,b,c) {
        

        var sFeatures;
        sFeatures = "dialogWidth: 530px; ";
        sFeatures += "dialogHeight: 500px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";


        var result;
        result = window.showModalDialog('libraryTransaction.aspx?Masterid=' + b + "&Libraryid=" + a + "&user_no=" + c + "&user=STUDENT", "", sFeatures);

    }

</script>
                        <table border="1" bordercolor="#1b80b6" cellpadding="5" 
    cellspacing="0" width="700">
                            <tr>
                                <td class="subheader_img">
                                    Library Search</td>
                            </tr>
                            <tr>
                                <td align="left">
                        <asp:TextBox ID="txtsearch" runat="server" Width="595px"></asp:TextBox>
                        <asp:Button ID="btnshow" runat="server" Text="Show" Width="83px" />
                                </td>
                            </tr>
</table>
                        <asp:GridView ID="GridItem" AutoGenerateColumns="false" 
    runat="server" Width="100%"
                            AllowPaging="True" ShowFooter="true" 
                            EmptyDataText="No Records Found. Please search with some other keywords."
                            >
                            <Columns>
                       
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">
                                                    Item&nbsp;Image
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:HiddenField ID="HiddenMasterId" Value='<%# Eval("MASTER_ID") %>' runat="server" />
                                            <asp:ImageButton ID="ImageItem" ImageUrl='<%# Eval("PRODUCT_IMAGE_URL") %>' ToolTip='<%# Eval("MASTER_ID") %>'  PostBackUrl='<%# Eval("PRODUCT_URL") %>'
                                                 runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">
                                                    Item&nbsp;Type
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("ITEM_DES") %>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">
                                                    Item&nbsp;Title
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("ITEM_TITLE") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">
                                                    Author
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("AUTHOR") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">
                                                    Publisher
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("PUBLISHER") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                         
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table class="BlueTable" width="100%">
                                            <tr class="matterswhite">
                                                <td align="center" colspan="2">
                                                    Item
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:LinkButton ID="lnkreserver"  OnClientClick='<%# Eval("Rwindow") %>'  runat="server">Reserve</asp:LinkButton>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                      
                            </Columns>
                            <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                            <EmptyDataRowStyle Wrap="False" />
                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                            <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                            <EditRowStyle Wrap="False" />
                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                        </asp:GridView>
                        
