﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryReservationStatusAndCancellation.aspx.vb" Inherits="Library_libraryReservationStatusAndCancellation" %>

<%@ Register src="Usercontrols/libraryReservationStatusAndCancellation.ascx" tagname="libraryReservationStatusAndCancellation" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Library Reservation</title>
    <link href="../../vendor/bootstrap/css/bootstrap.css" type="text/css" />
    <link href="../../cssfiles/sb-admin.css" type="text/css" />
    <%--<link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../../cssfiles/Librarystyle.css" rel="stylesheet" type="text/css" />

     <style type="text/css">
       body {
           background-image:none !important;
           background-color : transparent !important;
       }
     </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:libraryReservationStatusAndCancellation ID="libraryReservationStatusAndCancellation1" 
            runat="server" />
    
    </div>
    </form>
</body>
</html>
