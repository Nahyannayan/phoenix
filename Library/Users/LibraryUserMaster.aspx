<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LibraryUserMaster.aspx.vb" Inherits="Library_Users_LibraryUserMaster" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Library User Master</title>
    <link href="../../vendor/bootstrap/css/bootstrap.css" type="text/css" />
    <link href="../../cssfiles/Librarystyle.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Ratings.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../JavaScript/ViewedBooks.js"></script>

<script type="text/javascript" >

    window.setTimeout('setpathExt(1)', 1000);

    function setpathExt(val) 
    {
        var path = window.location.href
        var Rpath = ''
        if (path.indexOf('?') != '-1') {
            Rpath = path.substring(path.indexOf('?'), path.length)
        }
        var objFrame

        if (Rpath == '') {
            alert('Error in url.')
            alert('Please contact the administrator.')
            window.close()
        }
        else 
        {

            //Find Object
            objFrame = document.getElementById("FEx1");
            var objHeader = document.getElementById('<% = lblheader.ClientID %>');

            if (val == 1) {
                objHeader.innerText = "Library Search"
                Rpath = Rpath + "&NewArrival=0&MostRated=0"
                objFrame.src = "LibrarySearch.aspx" + Rpath
            }

            if (val == 2) {
                objHeader.innerText = "New Arrivals"
                Rpath = Rpath + "&NewArrival=1&MostRated=0"
                objFrame.src = "LibrarySearch.aspx" + Rpath
            }
            if (val == 3) {
                objHeader.innerText = "Most Rated"
                Rpath = Rpath + "&NewArrival=0&MostRated=1"
                objFrame.src = "LibrarySearch.aspx" + Rpath
            }
            if (val == 4) {
                objHeader.innerText = "Reservation Status"
                objFrame.src = "libraryReservationStatusAndCancellation.aspx" + Rpath
            }
            if (val == 5) {
                objHeader.innerText = "Request Items"
                objFrame.src = "libraryRequestBooks.aspx" + Rpath
            }
            if (val == 6) {
                objHeader.innerText = "Featured Book"
                objFrame.src = "LibraryFeaturedBook.aspx" + Rpath
            }

        }
    }
   
   </script> 
   <style type="text/css">
       body {
           background-image:none !important;
           background-color : transparent !important;
       }
       #rc3, #rc4 {
           background-image:none !important;
       }
       #links-bg {
    height: 60px;
   background-image: none !important;
    background-repeat: repeat-x;
    background-color: #8dc24c !important;
    border-radius: 14px !important;
}
       .cat-head {
           border-radius: 14px 14px 0 0 !important;
           background-color: #8dc24c  !important;
          background-image:none !important;
          

background: -moz-radial-gradient(center, ellipse cover, rgba(156,191,109,1) 1%, rgba(124,193,32,1) 14%, rgba(124,193,32,1) 34%, rgba(124,193,32,1) 59%, rgba(0,87,0,1) 100%, rgba(0,36,0,1) 100%, rgba(109,193,0,1) 100%) !important; /* FF3.6-15 */
background: -webkit-radial-gradient(center, ellipse cover, rgba(156,191,109,1) 1%,rgba(124,193,32,1) 14%,rgba(124,193,32,1) 34%,rgba(124,193,32,1) 59%,rgba(0,87,0,1) 100%,rgba(0,36,0,1) 100%,rgba(109,193,0,1) 100%) !important; /* Chrome10-25,Safari5.1-6 */
background: radial-gradient(ellipse at center, rgba(156,191,109,1) 1%,rgba(124,193,32,1) 14%,rgba(124,193,32,1) 34%,rgba(124,193,32,1) 59%,rgba(0,87,0,1) 100%,rgba(0,36,0,1) 100%,rgba(109,193,0,1) 100%) !important; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#9cbf6d', endColorstr='#6dc100',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
       }
       .sap {
           border-left:1px solid #efefef !important;
           background-image:none !important;
       }
       .main-head {
           background-image:none !important;
           border-radius: 14px;
           background: rgb(98,125,77) !important; /* Old browsers */
           background: -moz-radial-gradient(center, ellipse cover, rgba(98,125,77,1) 0%, rgba(31,59,8,1) 100%) !important; /* FF3.6-15 */
           background: -webkit-radial-gradient(center, ellipse cover, rgba(98,125,77,1) 0%,rgba(31,59,8,1) 100%) !important; /* Chrome10-25,Safari5.1-6 */
           background: radial-gradient(ellipse at center, rgba(98,125,77,1) 0%,rgba(31,59,8,1) 100%) !important; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
           filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#627d4d', endColorstr='#1f3b08',GradientType=1 ) !important; /* IE6-9 fallback on horizontal gradient */
       }
       .name {
           color:#8dc24c !important;
       }
       .toplinks a:hover {
           text-decoration:underline !important;
           color:#ffffff !important;
       }
   </style>
</head>
<%-- onload="BindTopView()"--%>
<body onload="BindTopView()" >
    <form id="Form1" runat="server">
        <table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td class="name">
                    <asp:Label ID="lblbsu" runat="server" Text=""></asp:Label>
                    <span class="name2"> <asp:Label ID="lbllibrary" runat="server" Text=""></asp:Label></span>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td id="rc3">
                            </td>
                            <td id="links-bg">
                                <table border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="toplinks" style="height: 16px">
                                         <asp:LinkButton ID="Link1" OnClientClick="javascript:setpathExt(1); return false;" runat="server">Library Search</asp:LinkButton>
                                        </td>
                                        <td class="sap" style="height: 16px">
                                        </td>
                                        <td class="toplinks" style="height: 16px">
                                         <asp:LinkButton ID="Link2" OnClientClick="javascript:setpathExt(2); return false;" runat="server">New Arrivals</asp:LinkButton>
                                        </td>
                                        <td class="sap" style="height: 16px">
                                        </td>
                                        <td class="toplinks" style="height: 16px">
                                        <asp:LinkButton ID="Link3" OnClientClick="javascript:setpathExt(3); return false;" runat="server">Most Rated</asp:LinkButton>
                                        </td>
                                        <td class="sap" style="height: 16px">
                                        </td>
                                        <td class="toplinks" style="height: 16px">
                                            <asp:LinkButton ID="Link4" OnClientClick="javascript:setpathExt(4); return false;" runat="server">Reservation Status</asp:LinkButton>
                                        </td>
                                        <td class="sap" style="height: 16px">
                                        </td>
                                        <td class="toplinks" style="height: 16px">
                                            <asp:LinkButton ID="Link5" OnClientClick="javascript:setpathExt(5); return false;" runat="server">Request Items</asp:LinkButton>
                                        </td>
                                        <td class="sap" style="height: 16px">
                                        </td>
                                        <td class="toplinks" style="height: 16px">
                                        <asp:LinkButton ID="Link6" OnClientClick="javascript:setpathExt(6); return false;" runat="server">Featured Book</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td id="rc4">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="background-color: #ffffff;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top">
                                            &nbsp;</td>
                                        <td valign="top">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="230" valign="top">
                                            <table width="200" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#E7EDEB">
                                                <tr>
                                                    <td class="cat-head">
                                                       Popular Books
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                       
                                                        <div style="height:500px;" >
                                                            <marquee direction="up" onmouseover="this.stop();" onmouseout="this.start();" scrollamount="3" style="height: 500px"><div id="topViewItems" style="vertical-align:middle; text-align:center "></div></marquee>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="cat-head">
                                                        Library Details
                                                    </td>
                                                </tr>
                                               <tr style="height:90px" valign="top" >
                                                  <td id="rdmessage" runat="server">
                                                      
                                                  </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="670" valign="top">
                                            <table width="98%" border="0" cellspacing="0"  cellpadding="0">
                                                <tr>
                                                    <td valign="top" style="height: 69px">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="main-head">
                                                                    <asp:Label ID="lblheader" runat="server" Text=""></asp:Label> </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="center" >
                                                                  
                                                                     <iframe id="FEx1" height="600" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="650"></iframe>
                                                                  
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                        <br />
                                                        <br />
                                                  
                                                  
                                                    </td>
                                                </tr>
                                                </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <table border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td id="rc1">
                                        </td>
                                        <td id="bottom">
                                        </td>
                                        <td id="rc2">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="28" align="center">
                                <a href="#">GEMS School Library.
                                    Powered by PHOENIX</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
<asp:HiddenField ID="HiddenBsuID" runat="server" />
<asp:HiddenField ID="Hiddenlibrarydivid" runat="server" />
    </form>
</body>
</html>