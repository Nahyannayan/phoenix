﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryViewApprovedComments.aspx.vb" Inherits="Library_Users_libraryViewApprovedComments" %>

<%@ Register src="Usercontrols/libraryViewApprovedComments.ascx" tagname="libraryViewApprovedComments" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Approved comments and ratings </title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Librarystyle.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Ratings.css" rel="stylesheet" type="text/css" />
    <base target="_self" /> 
</head>
<body>
    <form id="form1" runat="server">
    <div class="matters" align="center">
    
        <uc1:libraryViewApprovedComments ID="libraryViewApprovedComments1" 
            runat="server" />
    
    </div>
    </form>
</body>
</html>
