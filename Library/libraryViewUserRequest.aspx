﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryViewUserRequest.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Library_libraryViewUserRequest" %>

<%@ Register Src="UserControls/libraryViewUserRequest.ascx" TagName="libraryViewUserRequest" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>User New Item Request
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <div>

                    <uc1:libraryViewUserRequest ID="libraryViewUserRequest1" runat="server" />

                </div>
            </div>
        </div>
    </div>
</asp:Content>

