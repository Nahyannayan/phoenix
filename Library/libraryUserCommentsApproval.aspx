﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryUserCommentsApproval.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Library_libraryUserCommentsApproval" %>

<%@ Register Src="UserControls/libraryUserCommentsApproval.ascx" TagName="libraryUserCommentsApproval" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>User Comments Approval
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <div>

                    <uc1:libraryUserCommentsApproval ID="libraryUserCommentsApproval1"
                        runat="server" />

                </div>
            </div>
        </div>
    </div>

</asp:Content>
