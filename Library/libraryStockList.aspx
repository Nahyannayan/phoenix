﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryStockList.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Library_libraryStockList" %>

<%@ Register Src="UserControls/libraryStockList.ascx" TagName="libraryStockList" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Library Yearly Stock Entry
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <uc1:libraryStockList ID="libraryStockList1" runat="server" />
            </div>
        </div>
    </div>

</asp:Content>



