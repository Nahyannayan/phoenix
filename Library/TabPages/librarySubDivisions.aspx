<%@ Page Language="VB" AutoEventWireup="false" CodeFile="librarySubDivisions.aspx.vb" Inherits="Library_TabPages_librarySubDivisions" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../UserControls/librarySubDivisions.ascx" TagName="librarySubDivisions"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Library Sub Divisions</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:librarySubDivisions ID="LibrarySubDivisions1" runat="server" />
    
    </div>
    </form>
</body>
</html>
