<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryDetailViewUserTransactions.aspx.vb" Inherits="Library_libraryDetailViewUserTransactions" %>

<%@ Register Src="UserControls/libraryDetailViewUserTransactions.ascx" TagName="libraryDetailViewUserTransactions"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <title>Library User Transactions</title>
     <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:libraryDetailViewUserTransactions ID="LibraryDetailViewUserTransactions1" runat="server" />    
    </div>
    </form>
</body>
</html>
