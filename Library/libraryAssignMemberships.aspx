<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryAssignMemberships.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Library_libraryAssignMemberships" %>

<%@ Register Src="UserControls/libraryStaffMembership.ascx" TagName="libraryStaffMembership"
    TagPrefix="uc2" %>

<%@ Register Src="UserControls/libraryStudentMembership.ascx" TagName="libraryStudentMembership"
    TagPrefix="uc1" %>


<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
           Assign Library Membership
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
                    <ajaxToolkit:TabPanel ID="HT1" runat="server">
                        <ContentTemplate>
                            <uc1:libraryStudentMembership ID="LibraryStudentMembership1" runat="server" />



                        </ContentTemplate>
                        <HeaderTemplate>
                            Student Memberships
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="HT2" runat="server">
                        <ContentTemplate>
                            <uc2:libraryStaffMembership ID="LibraryStaffMembership1" runat="server"></uc2:libraryStaffMembership>
                        </ContentTemplate>
                        <HeaderTemplate>
                            Staff Memberships
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </div>
        </div>
    </div>


</asp:Content>
