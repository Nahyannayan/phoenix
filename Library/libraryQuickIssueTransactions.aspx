<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="libraryQuickIssueTransactions.aspx.vb" Inherits="Library_libraryQuickIssueTransactions" %>

<%@ Register Src="~/Library/UserControls/libraryQuickIssueTransactions.ascx" TagName="libraryQuickIssueTransactions"
    TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Issue Item
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td align="left">
                            <uc1:libraryQuickIssueTransactions ID="LibraryQuickIssueTransactions1" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>



</asp:Content>
