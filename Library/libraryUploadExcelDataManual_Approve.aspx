﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryUploadExcelDataManual_Approve.aspx.vb" Inherits="Library_libraryUploadExcelDataManual_Approve" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>library Upload Excel DataManual Approve</title>
    <!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/>
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet"/>
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet"/>

<script type="text/javascript">

    function ShowHideDiv() {
        if (document.getElementById('hideShow').style.visibility == 'hidden') {
            document.getElementById('hideShow').style.visibility = 'visible';
        }
        else {
            document.getElementById('hideShow').style.visibility = 'hidden';
        }

    }

    function AlertCall() {
        alert("This is a sample Excel File. You may use this excel file for entering the valid data and upload the excel file.")
    }
</script>
</head>
<body>
    <form id="form1" runat="server">
     <div>
    <asp:Label ID="lblmessage" runat="server" class="text-danger"></asp:Label>
    <asp:Panel ID="Panel1" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite" colspan="4">Excel Upload</td>
            </tr>
            <tr visible="false" runat="server">
                <td align="left" width="30%">
                    <asp:CheckBox ID="CheckAutoStockUpdate" runat="server" Text="Automatic copies update for same ISBN in uploaded Excel." CssClass="field-label" />
                </td>
                <td align="left" width="30%"></td>
                <td align="left" width="20%"></td>
                <td align="left" width="20%"></td>
            </tr>
            <tr>
                <td align="left" width="30%">
                    <asp:CheckBox ID="CheckExcel" runat="server" Checked="true" Text="Check Excel Data Count." CssClass="field-label" />
                </td>
                <td align="left" width="30%"></td>
                <td align="left" width="20%"></td>
                <td align="left" width="20%"></td>
            </tr>
            <tr>
                <td align="left" width="30%">
                    <asp:FileUpload ID="FileUpload1" runat="server" />

                    <%--<img src="../Images/icon-question.gif"  onclick="javascript:ShowHideDiv()" />--%>
                </td>
                <td align="left" width="30%">
                    <asp:Button ID="btnupload" runat="server" CssClass="button" Text="Upload" /></td>
                <td align="left" width="20%"></td>
                <td align="left" width="20%"></td>
            </tr>
        </table>
        <div id="hideShow">
            <br />
            Please upload an excel with following header format,Data with no ITEM_TITLE will 
            be discarded.
            <table class="table table-bordered table-row" width="100%">
                <tr align="center">
                    <th>Header
                    </th>
                    <th>Description
                    </th>
                    <th>Default Values
                    </th>
                    <th>Notes
                    </th>
                </tr>
                <tr>
                    <td align="left">ISBN</td>
                    <td align="left">ISBN must be 10 or 13 digit characters</td>
                    <td align="center">*
                    </td>
                    <td align="left">Manditory</td>
                </tr>
                <tr>
                    <td align="left">ITEM_TITLE</td>
                    <td align="left">Title of the Item</td>
                    <td align="center">*
                    </td>
                    <td align="left">Manditory</td>
                </tr>

                <tr>
                    <td align="left">AUTHOR</td>
                    <td align="left">Author Name</td>
                    <td align="center">&nbsp;</td>
                    <td align="left">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left">PUBLISHER</td>
                    <td align="left">Publisher Name</td>
                    <td align="center">&nbsp;</td>
                    <td align="left">&nbsp;</td>
                </tr>

                <tr>
                    <td align="left">COPIES
                    </td>
                    <td align="left">Number of Copies
                    </td>
                    <td align="center">1
                    </td>
                    <td align="left">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left">CATEGORY</td>
                    <td align="left">Category the Item belongs</td>
                    <td align="center">1</td>
                    <td align="left">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left">ACCESSION_NO
                    </td>
                    <td align="left">Accession number is a sequential number given to each new item
                    </td>
                    <td align="left">&nbsp;
                    </td>
                    <td align="left">If Accession Number is provided , then the no. of COPIES will be considered as 1 . 
                        If Accession Number exists in database, system will generate its own unique 
                        Accession Number.</td>
                </tr>
                <tr>
                    <td align="left">CALL_NO
                    </td>
                    <td align="left">A call number is a group of numbers and/or letters put together to tell you where
                        in the library to find your book.
                    </td>
                    <td align="left">&nbsp;
                    </td>
                    <td align="left">Call Numbers, a unique number given to a set of items.</td>
                </tr>
                <tr>
                    <td align="left">AGE_GROUP_FROM
                    </td>
                    <td align="left">From Age group
                    </td>
                    <td align="center">5
                    </td>
                    <td align="left">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">AGE_GROUP_TO
                    </td>
                    <td align="left">To Age group
                    </td>
                    <td align="center">75
                    </td>
                    <td align="left">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">SUPPLIER
                    </td>
                    <td align="left">Item supplied by
                    </td>
                    <td align="left">&nbsp;
                    </td>
                    <td align="left">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">PURCHASE_DATE
                    </td>
                    <td align="left">Purchase date
                    </td>
                    <td align="left">&nbsp;
                    </td>
                    <td align="left">If Valid date is not provided, the system will consider as empty.</td>
                </tr>
                <tr>
                    <td align="left">PRODUCT_PRICE_AED
                    </td>
                    <td align="left">Price of an item in AED
                    </td>
                    <td align="center">0
                    </td>
                    <td align="left">Data must be numeric , else it will considered as 0</td>
                </tr>
                 <tr>
                    <td align="left">RAK</td>
                    <td align="left"> is a group of numbers and/or letters put together to tell you where in the library to find your book. </td>
                    <td align="center">*
                    </td>
                    <td align="left">Manditory</td>
                </tr>
            </table>
            <br />
        </div>
    </asp:Panel>
    <div>

        <asp:Panel ID="Panel2" runat="server" Visible="false">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg-lite">Upload Status 
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table width="100%">
                            <tr>
                                <td><span class="field-label">Total Records in Excel</span>
                                </td>

                                <td>
                                    <asp:Label ID="Label1" runat="server" CssClass="field-value"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="field-label">Upload Success</span>
                                </td>

                                <td>
                                    <asp:Label ID="Label2" runat="server" CssClass="field-value"></asp:Label>
                                </td>
                            </tr>
                            <%--<tr>
                            <td>
                                Upload Error
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <asp:Label ID="Label3" runat="server"></asp:Label>
                            </td>
                        </tr>--%>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:HiddenField ID="HiddenBsuid" runat="server" />
        <asp:HiddenField ID="Hiddenempid" runat="server" />
    </div>
</div>
    </form>
</body>
</html>
