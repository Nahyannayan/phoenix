Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Xml
Partial Class Library_UserControls_libraryItemList
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenShowFlag.Value = 0
            HiddenBsuID.Value = Session("sbsuid")
            HiddenEmpid.Value = Session("EmployeeId")
            BindLibraryDivisions(ddLibraryDivisions)
            BindGrid()
            Session("Barcodehashtable") = Nothing
            Session("BarcodehashtableTemp") = Nothing

        End If
        If GridItem.Rows.Count > 0 Then
            Dim btn As Button = DirectCast(GridItem.FooterRow.FindControl("btnbarcode"), Button)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btn)
            Dim btn2 As Button = DirectCast(GridItem.FooterRow.FindControl("btnbarcode2"), Button)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btn2)
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnexport)
    End Sub
    Public Sub BindLibraryDivisions(ByVal ddLibraryDivisions As DropDownList)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim str_query = ""
        If Library.LibrarySuperAcess(Session("EmployeeId")) Then
            str_query = "SELECT LIBRARY_DIVISION_ID,LIBRARY_DIVISION_DES FROM LIBRARY_DIVISIONS " & _
                          "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES "
        Else

            str_query = " SELECT B.LIBRARY_DIVISION_ID,B.LIBRARY_DIVISION_DES FROM dbo.LIBRARY_LIBRARIAN A " & _
                            " INNER JOIN dbo.LIBRARY_DIVISIONS B ON A.LIBRARY_DIVISION_ID=B.LIBRARY_DIVISION_ID " & _
                            " WHERE LIBRARIAN_EMP_ID='" & HiddenEmpid.Value & "' AND ACTIVE='True' AND LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' "
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            ddLibraryDivisions.DataSource = ds
            ddLibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
            ddLibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
            ddLibraryDivisions.DataBind()
            Dim list As New ListItem
            list.Value = "-1"
            list.Text = "Select Library"
            ddLibraryDivisions.Items.Insert(0, list)
            If Session("sBusper") = "False" Then
                ddLibraryDivisions.SelectedIndex = 1
            End If
        End If

    End Sub
    Public Sub BarcodePublish(ByVal Pagewise As Boolean)

        Session("Barcodehashtable") = Nothing
        Session("BarcodehashtableTemp") = Nothing

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim hash As New Hashtable

        If Pagewise Then

            For Each row As GridViewRow In GridItem.Rows
                Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
                Dim Hid As HiddenField = DirectCast(row.FindControl("HiddenMasterId"), HiddenField)
                Dim key = Hid.Value.Trim()
                If ch.Checked Then
                    If hash.ContainsKey(key) Then
                    Else
                        hash.Add(key, key)
                    End If

                Else

                    If hash.ContainsKey(key) Then
                        hash.Remove(key)
                    Else
                    End If
                End If
            Next
        Else

            Dim ds1 As DataSet = BindGrid()

            Dim i As Integer

            For i = 0 To ds1.Tables(0).Rows.Count - 1

                Dim key = ds1.Tables(0).Rows(i).Item("MASTER_ID").ToString().Trim()
                If hash.ContainsKey(key) Then
                Else
                    hash.Add(key, key)
                End If

            Next

        End If



        Dim idictenum As IDictionaryEnumerator
        idictenum = hash.GetEnumerator()

        Session("Barcodehashtable") = Nothing
        Dim str_query = ""
        Dim ds As DataSet

        Dim hash1 As New Hashtable

        While (idictenum.MoveNext())
            Dim key = idictenum.Key
            Dim id = idictenum.Value


            str_query = " SELECT A.STOCK_ID " & _
                            " FROM  dbo.LIBRARY_ITEMS_QUANTITY A " & _
                            " INNER JOIN dbo.LIBRARY_ITEMS_MASTER BC ON A.MASTER_ID=BC.MASTER_ID " & _
                            " WHERE A.MASTER_ID='" & id & "' AND PRODUCT_BSU_ID='" & HiddenBsuID.Value & "' AND A.ACTIVE='True' ORDER BY STOCK_ID,ACCESSION_NO"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1

                Dim skey = ds.Tables(0).Rows(i).Item("STOCK_ID").ToString().Trim()
                If hash1.ContainsKey(key) Then
                Else
                    hash1.Add(skey, skey)
                End If

            Next

        End While
        Session("Barcodehashtable") = hash1
        Session("BarcodehashtableTemp") = Nothing
        If hash1.Count > 0 Then
            HiddenShowFlag.Value = 1
        Else
            HiddenShowFlag.Value = 0
        End If

    End Sub
    Public Sub GenerateBarcode(ByVal sender As Object, ByVal e As System.EventArgs)
        BarcodePublish(True)
    End Sub
    Public Sub GenerateBarcode2(ByVal sender As Object, ByVal e As System.EventArgs)
        BarcodePublish(False)
    End Sub
    Public Function BindGrid() As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " SELECT DISTINCT A.MASTER_ID,ISBN,ITEM_DES,ITEM_TITLE,AUTHOR,PUBLISHER,CASE ISNULL(PRODUCT_IMAGE_URL,'') WHEN '' THEN '~/Images/Library/noImage.gif' ELSE PRODUCT_IMAGE_URL END PRODUCT_IMAGE_URL ,PRODUCT_URL,  " & _
                        " 'javascript:Redirect(''' + CONVERT(VARCHAR,A.MASTER_ID) + '''); return false;' REDIRECT, " & _
                        " 'javascript:RedirectEdit(''' + CONVERT(VARCHAR,A.MASTER_ID) + '''); return false;' REDIRECTEDIT, " & _
                        " CASE ISNULL(PRODUCT_URL,'') WHEN '' THEN 'FALSE' ELSE 'TRUE' END SHOWURL, " & _
                        " '" & ddLibraryDivisions.SelectedValue & "' AS LIBRARY_DIVISION_ID_VAL, " & _
                        " (SELECT TOP 1 CALL_NO FROM  dbo.LIBRARY_ITEMS_QUANTITY WHERE MASTER_ID=A.MASTER_ID AND PRODUCT_BSU_ID='" & HiddenBsuID.Value & "') TOP_CALL_NO " & _
                        " ,REPLACE(CONVERT(VARCHAR(11), ISNULL(C.PURCHASE_DATE,'') , 106), ' ', '/')PURCHASE_DATE ,  isNULL(PRICE_CURRENCY,'') +  ' '+     isNULL(PRODUCT_PRICE,'') as   [PRODUCT_COST] FROM  dbo.LIBRARY_ITEMS_MASTER A WITH (NOLOCK)" & _
                        " INNER JOIN dbo.LIBRARY_ITEMS_QUANTITY C WITH (NOLOCK) ON C.MASTER_ID= A.MASTER_ID " & _
                        " INNER JOIN  dbo.LIBRARY_ITEMS B WITH (NOLOCK) ON B.ITEM_ID= C.ITEM_ID"

        If ddLibraryDivisions.SelectedIndex > 0 Then
            str_query = str_query & _
                        " INNER JOIN dbo.LIBRARY_RACKS D ON D.RACK_ID=C.RACK_ID " & _
                        " INNER JOIN dbo.LIBRARY_SHELFS E ON E.SHELF_ID= D.SHELF_ID " & _
                        " INNER JOIN dbo.LIBRARY_SUB_DIVISIONS F ON F.LIBRARY_SUB_DIVISION_ID= E.LIBRARY_SUB_DIVISION_ID " & _
                        " INNER JOIN dbo.LIBRARY_DIVISIONS G ON G.LIBRARY_DIVISION_ID =  F.LIBRARY_DIVISION_ID "

        End If

        str_query = str_query & " WHERE A.MASTER_ID IN  (SELECT DISTINCT MASTER_ID FROM dbo.LIBRARY_ITEMS_QUANTITY WHERE PRODUCT_BSU_ID='" & HiddenBsuID.Value & "' AND ACTIVE='True') AND  PRODUCT_BSU_ID='" & HiddenBsuID.Value & "'"

        If ddLibraryDivisions.SelectedIndex > 0 Then
            str_query = str_query & " AND F.LIBRARY_DIVISION_ID='" & ddLibraryDivisions.SelectedValue & "'"
        End If

        Dim Condition As String = ""

        If txtItemName.Text.Trim() <> "" Then

            Condition = Condition & " AND ITEM_TITLE LIKE '%" & txtItemName.Text.Trim().Replace("'", "").Replace(",", "") & "%'"

        End If


        If txtAuthor.Text.Trim() <> "" Then

            Condition = Condition & " AND AUTHOR LIKE '%" & txtAuthor.Text.Trim().Replace("'", "").Replace(",", "") & "%'"

        End If

        If txtPublisher.Text.Trim() <> "" Then

            Condition = Condition & " AND PUBLISHER LIKE '%" & txtPublisher.Text.Trim().Replace("'", "").Replace(",", "") & "%'"

        End If

        If txtISBN.Text.Trim() <> "" Then

            Condition = Condition & " AND ISBN LIKE '%" & txtISBN.Text.Trim() & "%'"

        End If

        If txtMasterId.Text.Trim() <> "" Then

            Condition = Condition & " AND A.MASTER_ID = '" & txtMasterId.Text.Trim() & "'"

        End If

        If txtstockid.Text.Trim() <> "" Then

            Condition = Condition & " AND C.STOCK_ID = '" & LibraryData.GetStockIDForAccessonNo(txtstockid.Text.Trim(), HiddenBsuID.Value) & "'"

        End If

        If txtcallno.Text.Trim() <> "" Then

            Condition = Condition & " AND C.CALL_NO LIKE '%" & txtcallno.Text.Trim() & "%'"

        End If

        If DropOptions.SelectedIndex > 0 Then

            If DropOptions.SelectedValue = "0" Then
                Condition = Condition & " AND ISNULL(ACCESSION_NO,'')='' "
            End If
            If DropOptions.SelectedValue = "1" Then
                Condition = Condition & " AND ISNULL(CALL_NO,'')='' "
            End If
            If DropOptions.SelectedValue = "2" Then
                Condition = Condition & " AND ISNULL(C.RACK_ID,'')='' "
            End If
            If DropOptions.SelectedValue = "3" Then
                Condition = Condition & " AND ISNULL(C.STATUS_ID,'')='' "
            End If


        End If

        If txtsubject.Text.Trim() <> "" Then
            Condition = Condition & " AND SUBJECT LIKE '%" & txtsubject.Text.Trim() & "%' "
        End If

        If Condition <> "" Then

            str_query = str_query & Condition

        End If
        str_query = str_query & " ORDER BY ITEM_TITLE"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GridItem.DataSource = ds
        GridItem.DataBind()

        lblTotal.Text = " Total Records : " & ds.Tables(0).Rows.Count

        Return ds
    End Function

    Protected Sub GridItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridItem.PageIndexChanging

        GridItem.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        BindGrid()

    End Sub

    Protected Sub ddLibraryDivisions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        BindGrid()
    End Sub

    Protected Sub DropOptions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropOptions.SelectedIndexChanged
        BindGrid()
    End Sub

    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        Dim dt As New DataTable
        dt = BindGrid().Tables(0)
        dt.Columns.Remove("PRODUCT_IMAGE_URL")
        dt.Columns.Remove("PRODUCT_URL")
        dt.Columns.Remove("REDIRECT")
        dt.Columns.Remove("REDIRECTEDIT")
        dt.Columns.Remove("SHOWURL")
        dt.Columns.Remove("LIBRARY_DIVISION_ID_VAL")

        ExportExcel(dt)
    End Sub

    Public Sub ExportExcel(ByVal dt As DataTable)

        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, "A1", True)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")
        ef.SaveXls(Response.OutputStream)

    End Sub

End Class
