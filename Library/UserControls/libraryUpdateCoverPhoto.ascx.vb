﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports GoogleBooks.GoogleBooks

Partial Class Library_UserControls_libraryUpdateCoverPhoto
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindControls(True)
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)


    End Sub


    Public Sub BindControls(ByVal extract As Boolean)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT [ITEM_TITLE],[ISBN],[PRODUCT_IMAGE_URL],ENTRY_TYPE FROM [OASIS_LIBRARY].[dbo].[LIBRARY_ITEMS_MASTER] WHERE MASTER_ID='" & Request.QueryString("id") & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then

            lbltitle.Text = "Item Title : " & ds.Tables(0).Rows(0).Item("ITEM_TITLE").ToString()
            Dim img As String = ds.Tables(0).Rows(0).Item("PRODUCT_IMAGE_URL").ToString()
            If img = "" Then
                Image1.ImageUrl = "~\Images\Library\noImage.gif"
            Else
                Image1.ImageUrl = ds.Tables(0).Rows(0).Item("PRODUCT_IMAGE_URL").ToString()
            End If

            Dim isbn As String() = ds.Tables(0).Rows(0).Item("ISBN").ToString().Replace("ISBN:", "").Split(",")

            Dim hash As Hashtable = GoogleBookData(isbn(0).Trim())

            If hash("DataFound") = "YES" Then

                If hash.ContainsKey("ImageUrl") = False Then
                    Image2.Visible = True
                    Image2.ImageUrl = "~\Images\Library\noImage.gif"
                Else
                    Image2.Visible = True
                    HiddenImageUrl.Value = hash("ImageUrl")
                    Image2.ImageUrl = HiddenImageUrl.Value
                End If
            Else

                lblmessage.Text = "No data found."

            End If

        End If

    End Sub

    Protected Sub btnupdate_Click(sender As Object, e As System.EventArgs) Handles btnupdate.Click

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "update LIBRARY_ITEMS_MASTER set PRODUCT_IMAGE_URL='" & HiddenImageUrl.Value & "' WHERE MASTER_ID='" & Request.QueryString("id") & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        lblmessage.Text = "Image updated successfully."


    End Sub

End Class
