<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryDivisions.ascx.vb" Inherits="Library_UserControls_libraryDivisions" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<div class="matters">

    <asp:LinkButton ID="LinkAdd" runat="server" OnClientClick="javascript:return false;">Add</asp:LinkButton>
    <asp:Panel ID="Panel2" runat="server">
        <table width="100%">
            <tr>
                <td width="20%"><span class="field-label">Library Division</span>
                </td>
                <td width="30%">
                    <asp:TextBox ID="TxtLibraryDivisions" runat="server"></asp:TextBox>
                </td>
                <td width="20%"><span class="field-label">Code</span>
                </td>
                <td width="30%">
                    <asp:TextBox ID="txtcode" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td><span class="field-label">Online Item Reservation Count</span>
                </td>
                <td>
                    <asp:TextBox ID="txtreservecount" runat="server"></asp:TextBox>
                </td>
                <td colspan="2" align="center">
                    <asp:Button ID="BtnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="ts1" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" TargetControlID="Panel2"
        CollapsedSize="0" ExpandedSize="150" Collapsed="true" ExpandControlID="LinkAdd"
        CollapseControlID="LinkAdd" AutoCollapse="False" AutoExpand="False" ScrollContents="false"
        TextLabelID="LinkAdd" CollapsedText="Add" ExpandedText="Hide" runat="server">
    </ajaxToolkit:CollapsiblePanelExtender>
    <br />
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
    <table width="100%">
        <tr>
            <td class="title-bg">Library Divisions
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridLibraryDivisions" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                    OnRowUpdating="GridLibraryDivisions_RowUpdating" Width="100%" OnRowCancelingEdit="GridLibraryDivisions_RowCancelingEdit"
                    OnRowCommand="GridLibraryDivisions_RowCommand" OnRowEditing="GridLibraryDivisions_RowEditing">
                    <Columns>
                        <asp:TemplateField HeaderText="Library Divisions">
                            <HeaderTemplate>
                                Library Divisions
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("LIBRARY_DIVISION_DES")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="HiddenLibraryDId" runat="server" Value='<%#Eval("LIBRARY_DIVISION_ID")%>' />
                                <asp:TextBox ID="txtDataEdit" Text='<%#Eval("LIBRARY_DIVISION_DES")%>' runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorE1" runat="server" SetFocusOnError="True"
                                    ErrorMessage="Please Enter Library Division" Display="None" ControlToValidate="txtDataEdit"
                                    ValidationGroup="Edit"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Library Code">
                            <HeaderTemplate>
                                Library Code
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("LIBRARY_CODE")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDataEdit1" Text='<%#Eval("LIBRARY_CODE")%>' runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorE2" runat="server" SetFocusOnError="True"
                                    ErrorMessage="Please Enter Library Division Code" Display="None" ControlToValidate="txtDataEdit1"
                                    ValidationGroup="Edit"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Library Reserve">
                            <HeaderTemplate>
                                Reserve Count
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("LIBRARY_ONLINE_RESERVER_COUNT")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDataEdit2" Text='<%#Eval("LIBRARY_ONLINE_RESERVER_COUNT")%>' runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorE3" runat="server" SetFocusOnError="True"
                                    ErrorMessage="Please Enter Reservation Count" Display="None" ControlToValidate="txtDataEdit2"
                                    ValidationGroup="Edit"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Edit
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkEdit" runat="server" CausesValidation="false" CommandArgument='<%#Eval("LIBRARY_DIVISION_ID")%>'
                                    CommandName="Edit">Edit</asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:LinkButton ID="LinkUpdate" runat="server" CommandArgument='<%#Eval("LIBRARY_DIVISION_ID")%>'
                                    CommandName="Update" ValidationGroup="Edit">Update</asp:LinkButton>
                                <asp:LinkButton ID="LinkCancel" runat="server" CausesValidation="false" CommandArgument='<%#Eval("LIBRARY_DIVISION_ID")%>'
                                    CommandName="Cancel">Cancel</asp:LinkButton>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Delete
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkDelete" runat="server" CausesValidation="false" CommandArgument='<%#Eval("LIBRARY_DIVISION_ID")%>'
                                    CommandName="deleting"> Delete</asp:LinkButton>
                                <ajaxToolkit:ConfirmButtonExtender ID="CF1" runat="server" ConfirmText="Are you sure, Do you want this record to be deleted ?"
                                    TargetControlID="LinkDelete">
                                </ajaxToolkit:ConfirmButtonExtender>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TxtLibraryDivisions" ValidationGroup="ts1"
        Display="None" ErrorMessage="Please Enter Library Division" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtcode" ValidationGroup="ts1"
        Display="None" ErrorMessage="Please Enter Code" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="ts1"
        ControlToValidate="txtreservecount" Display="None" ErrorMessage="Please Enter Online Reserve Count"
        SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ValidationGroup="ts1"
        ShowSummary="False" />
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True" 
        ShowSummary="False" ValidationGroup="Edit" />
    <asp:HiddenField ID="HiddenBsuID" runat="server" />
</div>
