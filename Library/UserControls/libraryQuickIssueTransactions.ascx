<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryQuickIssueTransactions.ascx.vb"
    Inherits="Library_UserControls_libraryQuickIssueTransactions" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="libraryQuickReturnTransactions_S.ascx" TagName="libraryQuickReturnTransactions_S" TagPrefix="uc1" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
<style>
    .darkPanlAlumini {
        width: 100%;
        height: 100%;
        position: fixed;
        left: 0%;
        top: 0%;
        background: rgba(0,0,0,0.2) !important;
        /*display: none;*/
        display: block;
        overflow:scroll ;
        max-height:800px;
    }

    .inner_darkPanlAlumini {
        left: 20%;
        top: 10%;
        position: fixed;
        width: 70%;
        overflow:scroll ;
        max-height:650px;
    }
    .card-body {
        padding: 0.25rem !important;
    }
    table th, table td {
        padding:0.1rem !important;
    }
</style>
<div>
    <script type="text/javascript">

        function UserTransactions(usertype, userid) {
            var result;
           result = radopen('libraryDetailViewUserTransactions.aspx?UserType=' + usertype + '&UserId=' + userid, "pop_up");
            return false;

        }

        function Rview(accno) {
            var result;
            result = radopen("libraryTransactionReservationApproval2.aspx?accno=" + accno, "pop_up2");
            return false;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="800px" >
            </telerik:RadWindow>
        </Windows>
      <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="800px" >
            </telerik:RadWindow>
        </Windows>
     </telerik:RadWindowManager>

    <asp:Panel ID="Panel1" runat="server">
        <table width="100%">
            <tr>
                <td class="title-bg-lite" colspan="3">Step 1-Select Library
                </td>
            </tr>
            <tr>
                <td width="20%" align="left"><span class="field-label">Select Library</span></td>
                <td width="40%">
                    <asp:DropDownList ID="ddLibrary" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel2" Visible="false" DefaultButton="btnIssueNext" runat="server">
        <table width="100%">
            <tr>
                <td class="title-bg-lite">Step 2-User Information
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table width="100%">
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Type</span>
                            </td>

                            <td align="left" width="30%">
                                <asp:RadioButtonList ID="RadioUserTypeIssueItem" runat="server" AutoPostBack="True"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" class="field-label" Text="STUDENT" Value="STUDENT"></asp:ListItem>
                                    <asp:ListItem Text="EMPLOYEE" class="field-label" Value="EMPLOYEE"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Member ID.</span>
                            </td>

                            <td align="left" width="30%">
                                <asp:TextBox ID="txtuserno" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                       
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="btnIssueNext" runat="server" CssClass="button" Text="Next" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel3" runat="server" Visible="False">
        <table width="100%">
            <tr>
                <td class="title-bg-lite">Step 3-Item Transactions
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel5" DefaultButton="btnIssue" runat="server">
                        <table width="100%">
                            <tr>
                                <td align="left"><span class="field-label">Accession No.</span>
                                </td>

                                <td align="left">
                                    <asp:TextBox ID="txtstockid" runat="server"></asp:TextBox>
                                </td>
                                <td rowspan="4" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="title-bg-lite"><span class="field-label">User Information</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table id="UserStudent" runat="server" width="100%">
                                                    <tr>
                                                        <td align="left" width="50%"><span class="field-label">User Type</span>
                                                        </td>

                                                        <td id="stuType" runat="server" align="left" width="50%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left"><span class="field-label" width="50%">Member Name</span>
                                                        </td>

                                                        <td id="stuMembername" runat="server" align="left" width="50%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left"><span class="field-label" width="50%">Member Number</span>
                                                        </td>

                                                        <td id="stuMemberNo" runat="server" align="left" width="50%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left"><span class="field-label" width="50%">Grade</span>
                                                        </td>

                                                        <td id="stuGrade" runat="server" align="left" width="50%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left"><span class="field-label" width="50%">Section</span>
                                                        </td>

                                                        <td id="stuSection" runat="server" align="left" width="50%"></td>
                                                    </tr>
                                                </table>
                                                <table id="UserEmployee" runat="server">
                                                    <tr>
                                                        <td align="left"><span class="field-label" width="50%">User Type</span>
                                                        </td>

                                                        <td id="empType" runat="server" align="left" width="50%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left"><span class="field-label" width="50%">Member Name</span>
                                                        </td>

                                                        <td id="empMembername" runat="server" align="left" width="50%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left"><span class="field-label" width="50%">Member Number</span>
                                                        </td>

                                                        <td id="empMemberNo" runat="server" align="left" width="50%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left"><span class="field-label" width="50%">Designation</span>
                                                        </td>

                                                        <td id="empdesc" runat="server" align="left" width="50%"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table width="100%">
                                        <tr>
                                            <td class="title-bg-lite">Membership Information
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left" width="50%"><span class="field-label">Membership</span>
                                                        </td>

                                                        <td id="Td1" runat="server" align="left" width="50%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left"><span class="field-label" width="50%">Type</span>
                                                        </td>

                                                        <td id="Td2" runat="server" align="left" width="50%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left"><span class="field-label" width="50%">Max Items</span>
                                                        </td>

                                                        <td id="Td3" runat="server" align="left" width="50%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left"><span class="field-label" width="50%">Lending Days</span>
                                                        </td>

                                                        <td id="Td4" runat="server" align="left" width="50%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" colspan="2">
                                                            <asp:LinkButton ID="lnkHistory" runat="server" Text="Transaction History"></asp:LinkButton></td>
                                                    </tr>
                                                </table>
                                            </td>

                                        </tr>

                                    </table>
                                </td>

                                <td rowspan="4" valign="top">
                                    <asp:GridView ID="GridItem" runat="server" AutoGenerateColumns="false" ShowFooter="true"
                                        CssClass="table table-bordered table-row" Width="100%">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Check">
                                                <HeaderTemplate>
                                                    <center>
                                                        <%--<asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                                            ToolTip="Click here to select/deselect all rows (Deleting Items)" />--%>
                                                    </center>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <asp:CheckBox ID="ch1" runat="server" Checked='<%#Eval("EDIT_ENABLE")%>'  Enabled='<%#Eval("EDIT_ENABLE")%>' Text="" />
                                                    </center>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <center>
                                                        <asp:Button ID="btnIssue" runat="server" CssClass="button" OnClick="btnIssuepack_Click"
                                                            Text="Issue" ToolTip="Issue selected items" />
                                                    </center>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Accession No
                                                          
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <asp:HiddenField ID="HiddenBookId" runat="server" Value='<%# Eval("STOCK_ID") %>' />
                                                        <asp:HiddenField ID="HiddenStockId" runat="server" Value='<%#Eval("ACCESSION_NO")%>' />
                                                        <%#Eval("STOCK_ID")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Item Image
                                                           
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <asp:HiddenField ID="HiddenMasterId" runat="server" Value='<%# Eval("MASTER_ID") %>' />
                                                        <asp:Image ID="Image" runat="server" Height="50px" ImageUrl='<%# Eval("PRODUCT_IMAGE_URL") %>'
                                                            Width="40px" />
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Call No
                                                          
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("CALL_NO")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Status
                                                           
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("STATUS_DESCRIPTION")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="griditem" />
                                        <EmptyDataRowStyle />
                                        <SelectedRowStyle />
                                        <HeaderStyle />
                                        <EditRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"><span class="field-label">Membership Type</span>
                                </td>

                                <td align="left">
                                    <asp:DropDownList ID="ddmemebershipissue" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"><span class="field-label">Issue Item Status</span>
                                </td>

                                <td align="left">
                                    <asp:DropDownList ID="ddissuestatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddmemebershipissue_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"><span class="field-label">Notes</span>
                                </td>

                                <td align="left">
                                    <asp:TextBox ID="txtissuenotes" runat="server" EnableTheming="False"
                                        TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                               
                                <td align="center" colspan="2">
                                    <asp:Button ID="btnIssue" runat="server" CssClass="button" Text="Issue" />
                                    <asp:Button ID="Nextusertop" runat="server" CssClass="button" Text="Next User" />
                                </td>

                            </tr>
                        </table>
                        <asp:Panel ID="Panel4" runat="server">
                            <table width="100%">
                                <tr>
                                    <td class="title-bg-lite">Items Issued to User
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="GridTransactions" runat="server" Width="100%"
                                            AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Library Divisions">
                                                    <HeaderTemplate>
                                                        Accession No
                                                              
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <center>
                                                            <%#Eval("ACCESSION_NO")%>
                                                        </center>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Library Sub Divisions">
                                                    <HeaderTemplate>
                                                        Item Type
                                                                
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <center>
                                                            <%#Eval("ITEM_DES")%>
                                                        </center>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Shelf">
                                                    <HeaderTemplate>
                                                        Item Title
                                                               
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <center>
                                                            <%#Eval("ITEM_TITLE")%>
                                                        </center>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Racks">
                                                    <HeaderTemplate>
                                                        Issue Date
                                                               
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <center>                                                      
                                                            <%#Eval("ITEM_TAKEN_DATE", "{0:dd/MMM/yyyy}")%>
                                                        </center>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Racks">
                                                    <HeaderTemplate>
                                                        Tentative Return Date
                                                               
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <center>
                                                            <%#Eval("ITEM_RETURN_DATE")%>
                                                        </center>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reservation">
                                                    <HeaderTemplate>
                                                        Reservation
                                                           
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtn_Reservation" runat="server" Text="View" OnClientClick='<%# EVAL("ACCESSION_NO", "return Rview({0})") %>' OnClick="lnkbtn_Reservation_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reissuecount">
                                                    <HeaderTemplate>
                                                        Reissue count
                                                          
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReissueCount" runat="server" Text='<%#Eval("ReissueCount")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reissue">
                                                    <HeaderTemplate>
                                                        Reissue
                                                           
                                                    </HeaderTemplate>
                                                    <ItemTemplate>

                                                        <asp:HiddenField ID="HF_ACCESSION_NO" runat="server" Value='<%#Eval("ACCESSION_NO")%>' />
                                                        <asp:HiddenField ID="HF_STOCK_ID" runat="server" Value='<%#Eval("STOCK_ID")%>' />
                                                        <asp:HiddenField ID="HF_RECORD_ID" runat="server" Value='<%#Eval("TRAN_RECORD_ID")%>' />
                                                        <asp:HiddenField ID="HF_librarydivid" runat="server" Value='<%#Eval("LIBRARY_DIVISION_ID")%>' />

                                                        <asp:LinkButton ID="lnkbtn_reisssue" runat="server" Text="Reissue"
                                                            OnClick="lnkbtn_reisssue_Click"></asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender_reisssue" ConfirmText="Proceed to reissue?"
                                                            TargetControlID="lnkbtn_reisssue" runat="server">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Return">
                                                    <HeaderTemplate>
                                                        Return
                                                          
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtn_ReturnBook" runat="server" Text="Return"
                                                            OnClick="lnkbtn_ReturnBook_Click"></asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderReturnBook" ConfirmText="Proceed to return item?"
                                                            TargetControlID="lnkbtn_ReturnBook" runat="server">
                                                        </ajaxToolkit:ConfirmButtonExtender>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" />
                                            <EmptyDataRowStyle />
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <EditRowStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            &nbsp;
                        </asp:Panel>
                    </asp:Panel>
                    &nbsp;
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <asp:Button ID="Nextuser" runat="server" CssClass="button" Text="Next User" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="Label1" runat="server"></asp:Label>


    <asp:Panel ID="PnlReturn" runat="server" CssClass="darkPanlAlumini" Visible="false">
        <div class="panel-cover inner_darkPanlAlumini">
            <div>
                <div style="float: right">
                    <asp:ImageButton  ID="lnkClose" ImageUrl="../../Images/close.png" ToolTip="click here to close"
                        runat="server" CausesValidation="False" OnClick="lnkClose_Click"></asp:ImageButton>
                </div>
                <div class="title-bg-lite">
                    Library Details
                </div>                
            </div>
            <div>
                <uc1:libraryQuickReturnTransactions_S ID="libraryQuickReturnTransactions_S1"
                    runat="server" />
            </div>
        </div>
    </asp:Panel>


    <asp:Panel ID="PanelTrans" runat="server" CssClass="darkPanlAlumini" Visible="false">
        <div class="panel-cover inner_darkPanlAlumini">
            <div>
                <div style="float: right">
                    <asp:ImageButton ID="lnkTransClose" ImageUrl="../../Images/close.png"  ToolTip="click here to close"
                        runat="server"  CausesValidation="False" OnClick="lnkTransClose_Click"></asp:ImageButton>
                </div>
                <div class="title-bg-lite">
                    Library Transactions History
                </div>
                
            </div>
            <div>
                <asp:GridView ID="grdTransactionHistory" runat="server" Width="100%" CssClass="table table-bordered table-row"
                    AutoGenerateColumns="False">
                    <Columns>
                        <asp:TemplateField HeaderText="Library Divisions">
                            <HeaderTemplate>
                                Accession No
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                                            <%#Eval("ACCESSION_NO")%>
                                                        </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Library Sub Divisions">
                            <HeaderTemplate>
                                Item Type
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                                            <%#Eval("ITEM_DES")%>
                                                        </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Shelf">
                            <HeaderTemplate>
                                Item Title
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                                            <%#Eval("ITEM_TITLE")%>
                                                        </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Racks">
                            <HeaderTemplate>
                                Issue Date
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>                                                      
                                                            <%#Eval("ITEM_TAKEN_DATE", "{0:dd/MMM/yyyy}")%>
                                                        </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Racks">
                            <HeaderTemplate>
                                Tentative Return Date
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                                            <%#Eval("ITEM_RETURN_DATE")%>
                                                        </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Racks">
                            <HeaderTemplate>
                                Return Date
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                                            <%#Eval("ITEM_ACTUAL_RETURN_DATE")%>
                                                        </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" />
                    <EmptyDataRowStyle />
                    <SelectedRowStyle />
                    <HeaderStyle />
                    <EditRowStyle />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>


    <asp:Panel ID="PanelStatusupdate" runat="server" DefaultButton="btnmok"
        CssClass="panel-cover" Style="display: none">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">Library Message
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <table>
                                <tbody>
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblMessage" runat="server" CssClass="error"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnmok" OnClick="btnmessageok_Click" runat="server"
                                                CssClass="button" Text="Ok" ValidationGroup="s" CausesValidation="False"></asp:Button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            &nbsp;
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
        DropShadow="true" PopupControlID="PanelStatusupdate" RepositionMode="RepositionOnWindowResizeAndScroll"
        TargetControlID="Label1">
    </ajaxToolkit:ModalPopupExtender>
    <asp:HiddenField ID="HiddenBsuID" runat="server" />
    <asp:HiddenField ID="Hiddenlibrarydivid" runat="server" />
    <asp:HiddenField ID="HiddenUserID" runat="server" />
    <asp:HiddenField ID="HiddenEmpid" runat="server" />
    <asp:HiddenField ID="HiddenAccessionNo" runat="server" />

</div>
