Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryGlobalItems
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            HiddenEmpid.Value = Session("EmployeeId")
            HiddenDirect.Value = 0
            'BindGrid()
            BindCurrency()
            BindFromTo()
            BindCategory()

        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnexport)
    End Sub
    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = "Select ITEM_ID,ITEM_DES from  LIBRARY_ITEMS where ITEM_PRI_ID='" & childnodeValue & "' AND ITEM_BSU_ID='" & HiddenBsuID.Value & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("ITEM_DES").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("ITEM_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If


        For Each node As TreeNode In ParentNode.ChildNodes
            BindChildNodes(node)
        Next


    End Sub

    Public Sub BindCategory()
        TreeItemCategory.Nodes.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()

        Dim Sql_Query = "Select ITEM_ID,ITEM_DES from  LIBRARY_ITEMS where ITEM_PRI_ID=0 AND ITEM_BSU_ID='" & HiddenBsuID.Value & "' or ITEM_ID='1' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("ITEM_DES").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("ITEM_ID").ToString()
                TreeItemCategory.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In TreeItemCategory.Nodes
            BindChildNodes(node)
        Next

        TreeItemCategory.CollapseAll()

        If TreeItemCategory.Nodes.Count > 0 Then
            TreeItemCategory.Nodes(0).Checked = True '' Book Checked
        End If

    End Sub

    Public Sub BindFromTo()
        Dim i
        For i = 0 To 70

            Dim list As New ListItem
            list.Value = i + 5
            list.Text = i + 5

            UDDFromAge.Items.Insert(i, list)
            UDDToAge.Items.Insert(i, list)
        Next
    End Sub

    Public Sub BindCurrency()
        Dim str_conn = ConfigurationManager.ConnectionStrings("MainDB").ConnectionString
        Dim str_query = "SELECT CUR_ID,upper(CUR_DESCR)CUR_DESCR FROM CURRENCY_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Uddcurrency.DataSource = ds
        Uddcurrency.DataTextField = "CUR_DESCR"
        Uddcurrency.DataValueField = "CUR_ID"
        Uddcurrency.DataBind()

    End Sub

    Public Function BindGrid() As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " SELECT MASTER_ID,ITEM_TITLE,AUTHOR,PUBLISHER,CASE ISNULL(PRODUCT_IMAGE_URL,'') WHEN '' THEN '~/Images/Library/noImage.gif' ELSE PRODUCT_IMAGE_URL END PRODUCT_IMAGE_URL ,PRODUCT_URL,  " & _
                        " 'javascript:Redirect(''' + CONVERT(VARCHAR,MASTER_ID) + '''); return false;' REDIRECT, " & _
                        " 'javascript:Redirect2(''' + CONVERT(VARCHAR,MASTER_ID) + '''); return false;' REDIRECT2, " & _
                        " CASE ISNULL(PRODUCT_URL,'') WHEN '' THEN 'FALSE' ELSE 'TRUE' END SHOWURL, " & _
                        " (SELECT COUNT(*) FROM LIBRARY_ITEMS_QUANTITY WHERE MASTER_ID= A.MASTER_ID AND ACTIVE='True' AND PRODUCT_BSU_ID='" & HiddenBsuID.Value & "') ACTIVE_COUNT " & _
                        " FROM  dbo.LIBRARY_ITEMS_MASTER A  WITH (NOLOCK) WHERE ACTIVE='True'"

        Dim Condition As String = ""

        If txtItemName.Text.Trim() <> "" Then

            Condition = Condition & " AND ITEM_TITLE LIKE '%" & txtItemName.Text.Trim() & "%'"

        End If


        If txtAuthor.Text.Trim() <> "" Then

            Condition = Condition & " AND AUTHOR LIKE '%" & txtAuthor.Text.Trim() & "%'"

        End If

        If txtPublisher.Text.Trim() <> "" Then

            Condition = Condition & " AND PUBLISHER LIKE '%" & txtPublisher.Text.Trim() & "%'"

        End If

        If txtISBN.Text.Trim() <> "" Then

            Condition = Condition & " AND ISBN LIKE '%" & txtISBN.Text.Trim() & "%'"

        End If

        If txtMasterId.Text.Trim() <> "" Then

            Condition = Condition & " AND MASTER_ID ='" & txtMasterId.Text.Trim() & "'"

        End If

        If Condition <> "" Then

            str_query = str_query & " " & Condition

        End If

        str_query = str_query & " ORDER BY ITEM_TITLE"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GridItem.DataSource = ds
        GridItem.DataBind()

        lbltotal.Text = " Total Items :" & ds.Tables(0).Rows.Count

        Return ds
    End Function



    Protected Sub GridItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridItem.PageIndexChanging
        GridItem.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)

        If e.CommandName = "UpdateQuantity" Then
            LibraryData.isOffLine(Session("sBusper"))
            HiddenMasterId.Value = e.CommandArgument
            ''Check if item  already exists for this school. if yes,then hide item type
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
            Dim str_query = "SELECT * FROM  LIBRARY_ITEMS_QUANTITY WHERE MASTER_ID='" & HiddenMasterId.Value & "' and PRODUCT_BSU_ID='" & HiddenBsuID.Value & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If ds.Tables(0).Rows.Count > 0 Then
                TD_Item_Type1.Visible = False
                TD_Item_Type2.Visible = False
            Else
                TD_Item_Type1.Visible = True
                TD_Item_Type2.Visible = True
            End If

            MO2.Show()
        End If

    End Sub

    Protected Sub btnmessageok_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO1.Hide()
    End Sub

    Protected Sub btncancel3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO2.Hide()
    End Sub

    Public Function GetNodeValue(ByVal ParentNode As TreeNode) As String
        Dim val = ""

        For Each node As TreeNode In ParentNode.ChildNodes
            If node.Checked Then
                val = node.Value
                'node.Checked = False
                Exit For
            Else
                val = GetNodeValue(node)

                If val <> "" Then
                    Exit For
                End If

            End If

        Next

        Return val

    End Function

    Protected Sub btnstockupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LibraryData.isOffLine(Session("sBusper"))
        Dim Masterid = HiddenMasterId.Value
        Dim Quantity = Utxtstock.Text.Trim()

        If Utxtstock.Text <> "" Then


            Dim val = ""
            For Each node As TreeNode In TreeItemCategory.Nodes
                If node.Checked Then
                    val = node.Value
                    'node.Checked = False
                    Exit For
                Else
                    val = GetNodeValue(node)
                    If val <> "" Then
                        Exit For
                    End If
                End If
            Next

            If val = "" Then
                val = 1 ' Book
            End If


            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblMessage.Text = ""
            Dim LibraryMessage As String = ""

            If Utxtcost.Text.Trim() = "" Then
                Utxtcost.Text = 0
            End If

            Try
                Dim pParms(11) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@MASTER_ID", HiddenMasterId.Value)
                pParms(1) = New SqlClient.SqlParameter("@PRODUCT_BSU_ID", HiddenBsuID.Value)
                pParms(2) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", HiddenEmpid.Value)
                pParms(3) = New SqlClient.SqlParameter("@QUANTITY", Utxtstock.Text.Trim())
                pParms(4) = New SqlClient.SqlParameter("@PRODUCT_PRICE", Utxtcost.Text.Trim())
                pParms(5) = New SqlClient.SqlParameter("@PRICE_CURRENCY", Uddcurrency.SelectedValue)
                pParms(6) = New SqlClient.SqlParameter("@FROM_AGE_GROUP", UDDFromAge.SelectedValue)
                pParms(7) = New SqlClient.SqlParameter("@TO_AGE_GROUP", UDDToAge.SelectedValue)
                pParms(8) = New SqlClient.SqlParameter("@SUPPLIER", Utxtsupplier.Text.Trim())
                If Utxtpurchasedate.Text.Trim() <> "" Then
                    pParms(9) = New SqlClient.SqlParameter("@PURCHASE_DATE", Utxtpurchasedate.Text.Trim())
                End If
                pParms(10) = New SqlClient.SqlParameter("@ITEM_ID", val)


                LibraryMessage = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_ITEM_STOCK", pParms)
                LibraryMessage = LibraryMessage & "<br> You will be redirected to a page to Updated the Item Rack and Status details.<br>You may also change the Accession and Call Numbers if necessary."
                transaction.Commit()
                Utxtstock.Text = ""
                Utxtcost.Text = ""
                Utxtsupplier.Text = ""
                Utxtpurchasedate.Text = ""
                BindGrid()
                HiddenDirect.Value = 1
            Catch ex As Exception
                transaction.Rollback()
                LibraryMessage = "Error occured while saving . " & ex.Message
            Finally
                connection.Close()

            End Try

            lblMessage.Text = LibraryMessage

            MO2.Hide()
            MO1.Show()
        Else
            MO2.Show()
        End If


    End Sub


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub


    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        Dim dt As New DataTable
        dt = BindGrid().Tables(0)
        dt.Columns.Remove("PRODUCT_IMAGE_URL")
        dt.Columns.Remove("PRODUCT_URL")
        dt.Columns.Remove("REDIRECT")
        dt.Columns.Remove("SHOWURL")

        ExportExcel(dt)
    End Sub

    Public Sub ExportExcel(ByVal dt As DataTable)

        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, "A1", True)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")
        ef.SaveXls(Response.OutputStream)

    End Sub
 
End Class
