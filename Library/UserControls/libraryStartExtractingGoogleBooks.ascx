﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryStartExtractingGoogleBooks.ascx.vb"
    Inherits="Library_UserControls_libraryStartExtractingGoogleBooks" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">

<div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">Retrieving Status :
            <asp:Label ID="lblstatus" runat="server"></asp:Label>

                <asp:CheckBox ID="CheckStop" runat="server" Text="Stop the Process" />
            </td>
            <td id="TD1" class="title-bg-lite" runat="server">Last Item Uploaded
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <table width="100%">
                    <tr>
                        <td align="left" width="40%">
                            <span class="field-label">Pending retrieving</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:Label ID="Label1" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td>
                            <span class="field-label">Total data for retrieving</span></td>

                        <td>
                            <asp:Label ID="Label2" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td>
                            <span class="field-label">Total error while retrieving  </span></td>

                        <td>
                            <asp:Label ID="Label3" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td>
                            <span class="field-label">Total success while retrieving</span>
                        </td>

                        <td>
                            <asp:Label ID="Label4" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td>
                            <span class="field-label">Last error message</span></td>

                        <td>
                            <asp:Label ID="Label8" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td align="left" width="30%"></td>
                    </tr>
                </table>
            </td>
            <td id="TD2" align="left" valign="top" runat="server">
                <table width="100%">
                    <tr>
                        <td>
                            <span class="field-label">Title</span></td>

                        <td>
                            <asp:Label ID="Label5" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                        <td rowspan="3" align="center">
                            <asp:Image ID="Image1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="field-label">Author</span></td>

                        <td>
                            <asp:Label ID="Label6" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="field-label">ISBN</span></td>

                        <td>
                            <asp:Label ID="Label7" runat="server" CssClass="field-value"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <asp:HiddenField ID="HiddenBsuid" runat="server" />
</div>

<script type="text/javascript">
    window.setTimeout('setRefresh()', 10000);


    function setRefresh() {

        var Stoptemp = document.getElementById("<%=CheckStop.ClientID %>").checked

        if (Stoptemp == false) {

            window.location.reload(true);
        } else {

            window.setTimeout('setRefresh()', 10000);
        }

    }

</script>

