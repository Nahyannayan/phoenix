<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryRacks.ascx.vb" Inherits="Library_UserControls_libraryRacks" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<div class="matters">


    <asp:LinkButton ID="LinkAdd" runat="server" OnClientClick="javascript:return false;">Add</asp:LinkButton>
    <asp:Panel ID="Panel2" runat="server">
        <table width="100%">
            <tr>
                <td width="20%"><span class="field-label">Library Divisions</span></td>
                <td width="30%">
                    <asp:DropDownList ID="ddLibraryDivisions" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddLibraryDivisions_SelectedIndexChanged">
                    </asp:DropDownList></td>
                <td width="20%"><span class="field-label">Library Sub Divisions</span></td>
                <td width="30%">
                    <asp:DropDownList ID="ddLibrarySubDivisions" runat="server" OnSelectedIndexChanged="ddLibrarySubDivisions_SelectedIndexChanged" AutoPostBack="True">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td><span class="field-label">Shelf Name / Id</span></td>
                <td>
                    <asp:DropDownList ID="ddShelfName" runat="server">
                    </asp:DropDownList></td>
                <td><span class="field-label">Rack Name / Id</span></td>
                <td>
                    <asp:TextBox ID="txtRackName" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td><span class="field-label">Rack Description</span></td>
                <td>
                    <asp:TextBox ID="txtRackDescription" runat="server" EnableTheming="False"
                        TextMode="MultiLine"></asp:TextBox></td>
                <td align="center" colspan="2">
                    <asp:Button ID="btnSave" runat="server" Text="Add" CssClass="button" ValidationGroup="ts4" /></td>
            </tr>
        </table>
    </asp:Panel>

    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkAdd" Collapsed="true"
        CollapsedSize="0" CollapsedText="Add" ExpandControlID="LinkAdd" ExpandedSize="300"
        ExpandedText="Hide" ScrollContents="false" TargetControlID="Panel2" TextLabelID="LinkAdd">
    </ajaxToolkit:CollapsiblePanelExtender>
    <br />
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
    <table width="100%">
        <tr>
            <td class="title-bg">Library Racks</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridRacks" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                    Width="100%" AllowPaging="True" OnRowCancelingEdit="GridRacks_RowCancelingEdit" OnRowEditing="GridRacks_RowEditing" OnRowUpdating="GridRacks_RowUpdating" OnRowCommand="GridRacks_RowCommand" OnPageIndexChanging="GridRacks_PageIndexChanging">
                    <Columns>

                        <asp:TemplateField HeaderText="Library Divisions">
                            <HeaderTemplate>
                                Library Divisions
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%#Eval("LIBRARY_DIVISION_DES")%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Library Sub Divisions">
                            <HeaderTemplate>
                                Library Sub-Divisions 
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%#Eval("LIBRARY_SUB_DIVISION_DES")%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="Shelf">
                            <HeaderTemplate>
                                Shelf
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%#Eval("SHELF_NAME")%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Racks">
                            <HeaderTemplate>
                                Racks
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%#Eval("RACK_NAME")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="HiddenShelfId" runat="server" Value='<%#Eval("SHELF_ID")%>' />
                                <asp:HiddenField ID="HiddenRackId" runat="server" Value='<%#Eval("RACK_ID")%>' />
                                <asp:TextBox ID="txtDataEdit" runat="server" Text='<%#Eval("RACK_NAME")%>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorE1" runat="server" ControlToValidate="txtDataEdit"
                                    Display="None" ErrorMessage="Please Enter Rack Name" SetFocusOnError="True"
                                    ValidationGroup="Edit"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Descriptions">
                            <HeaderTemplate>
                                Descriptions
                            </HeaderTemplate>
                            <ItemTemplate>

                                <asp:Label ID="Elblview" runat="server" Text='<%#Eval("tempview1")%>'></asp:Label>
                                <asp:Panel ID="E4Panel1" runat="server">
                                    <%#Eval("RACK_DES")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="Elblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview1")%>' ExpandControlID="Elblview"
                                    ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="E4Panel1"
                                    TextLabelID="Elblview">
                                </ajaxToolkit:CollapsiblePanelExtender>


                            </ItemTemplate>
                            <EditItemTemplate>

                                <asp:TextBox ID="txtRackDescription" runat="server" Text='<%#Eval("RACK_DES")%>' EnableTheming="False"
                                    TextMode="MultiLine"></asp:TextBox>


                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>



                        <asp:TemplateField>
                            <HeaderTemplate>
                                Edit
                            </HeaderTemplate>
                            <ItemTemplate>

                                <asp:LinkButton ID="LinkEdit" runat="server" CausesValidation="false" CommandArgument='<%#Eval("RACK_ID")%>'
                                    CommandName="Edit">Edit</asp:LinkButton>

                            </ItemTemplate>
                            <EditItemTemplate>

                                <asp:LinkButton ID="LinkUpdate" runat="server" CommandArgument='<%#Eval("RACK_ID")%>'
                                    CommandName="Update" ValidationGroup="Edit">Update</asp:LinkButton>
                                <asp:LinkButton ID="LinkCancel" runat="server" CausesValidation="false" CommandArgument='<%#Eval("RACK_ID")%>'
                                    CommandName="Cancel">Cancel</asp:LinkButton>

                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Delete
                            </HeaderTemplate>
                            <ItemTemplate>

                                <asp:LinkButton ID="LinkDelete" runat="server" CausesValidation="false" CommandArgument='<%#Eval("RACK_ID")%>'
                                    CommandName="deleting"> Delete</asp:LinkButton>
                                <ajaxToolkit:ConfirmButtonExtender ID="CF1" runat="server" ConfirmText="Are you sure, Do you want this record to be deleted ?"
                                    TargetControlID="LinkDelete">
                                </ajaxToolkit:ConfirmButtonExtender>

                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HiddenBsuID" runat="server" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRackName" ValidationGroup="ts4"
        Display="None" ErrorMessage="Please Enter Rack Name" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:ValidationSummary
            ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="Edit" />
</div>
