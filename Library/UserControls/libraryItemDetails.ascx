﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryItemDetails.ascx.vb"
    Inherits="Library_UserControls_libraryItemDetails" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<script type="text/javascript">

    function opentran(a) {
        window.open('libraryDetailViewStockTransactions.aspx?stock_id=' + a, '', 'Height=700px,Width=1000px,scrollbars=yes,resizable=no,directories=yes');
    }

    function openAction(value) {
        window.open('libraryStockTran.aspx?stock_id=' + value, '', 'Height=700px,Width=1020px,scrollbars=yes,resizable=no,directories=yes');
        return false;
    }

    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }

</script>
<div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg-lite">Library Item Search
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table>
                            <tr>
                                <td align="left" width="13%"><span class="field-label">Library</span>
                                </td>

                                <td align="left" width="20%">
                                    <asp:DropDownList ID="ddLibraryDivisions" runat="server" AutoPostBack="True" >
                                    </asp:DropDownList>
                                </td>
                                <td align="left" width="12%"><span class="field-label">Accession No</span>
                                </td>

                                <td align="left" width="20%">
                                    <asp:TextBox ID="txtstockid" runat="server"></asp:TextBox>
                                </td>
                                <td align="left" width="12%"><span class="field-label">Call No</span>
                                </td>

                                <td align="left" width="20%">
                                    <asp:TextBox ID="txtcallno" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="13%"><span class="field-label">Author</span>
                                </td>

                                <td align="left">
                                    <asp:TextBox ID="txtAuthor" runat="server"></asp:TextBox>
                                </td>
                                <td align="left"><span class="field-label">ISBN</span>
                                </td>

                                <td align="left">
                                    <asp:TextBox ID="txtISBN" runat="server"></asp:TextBox>
                                </td>
                                <td align="left"><span class="field-label">Publisher</span>
                                </td>

                                <td align="left">
                                    <asp:TextBox ID="txtPublisher" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"><span class="field-label">Item Title</span>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtItemName" runat="server"></asp:TextBox>
                                </td>
                                <td align="left"><span class="field-label">Available</span>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddAvailable" runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="All" Value="-1" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="YES" Value="Y"></asp:ListItem>
                                        <asp:ListItem Text="NO" Value="N"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="left"><span class="field-label">Status</span>
                                </td>

                                <td align="left">
                                    <asp:DropDownList ID="ddStatus" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"><span class="field-label">Edit Options</span>
                                </td>
                             
                                <td align="left">
                                    <asp:DropDownList ID="DropEdit" runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="Edit Options" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Accession No" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Call No" Value="1"></asp:ListItem>
                                        <%--   <asp:ListItem Text="Item Type" Value="2"></asp:ListItem>--%>
                                        <asp:ListItem Text="Assign Rack" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Item Status" Value="4"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="left"><span class="field-label">Options</span>
                                </td>
                            
                                <td align="left" >
                                    <asp:DropDownList ID="DropOptions" runat="server" AutoPostBack="True">
                                        <asp:ListItem Selected="True" Text="All" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Not Assigned Accession No" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Not Assigned Call No" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Not Assigned Rack" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Not Assigned Status" Value="3"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="left">&nbsp;
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtMasterId" Visible="false" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"><span class="field-label">Subject</span>
                                </td>
                              
                                <td align="left">
                                    <asp:TextBox ID="txtsubject" runat="server"></asp:TextBox>
                                </td>
                                <td align="left"><span class="field-label">Library Sub Divisions</span></td>
                              
                                <td align="left" >
                                    <asp:DropDownList ID="dropsubdiv" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>&nbsp;
                                </td>
                                <td align="left">
                                    <asp:CheckBox ID="CheckPouch" Text="Pouch/Box" runat="server" CssClass="field-label" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="6">
                                    <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" />
                                    <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Export" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <asp:Label ID="lblMessage" runat="server" CssClass="text-danger"></asp:Label>
            <br />
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg-lite">Library Items :
                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                   
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GridItem" AutoGenerateColumns="false" runat="server" Width="100%" ShowFooter="true" CssClass="table table-bordered table-row"
                            AllowPaging="True" EmptyDataText="No Records Found. Please search with some other keywords.">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       Accession No
                                              
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("ACCESSION_NO")%>
                                            <asp:TextBox ID="txtAccessionNo" Text='<%#Eval("ACCESSION_NO")%>' Visible="false"
                                                Enabled='<%# Eval("EDIT_ENABLE") %>'  runat="server"></asp:TextBox>
                                            <asp:HiddenField ID="HiddenStockid" Value='<%#Eval("STOCK_ID")%>' runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Item Image
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:HiddenField ID="HiddenMasterId" Value='<%# Eval("MASTER_ID") %>' runat="server" />
                                            <asp:ImageButton ID="ImageItem" ImageUrl='<%# Eval("PRODUCT_IMAGE_URL") %>' OnClientClick="javascript:return false;"
                                                runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       Call No
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("CALL_NO")%>
                                            <asp:TextBox ID="txtCallNo" Text='<%#Eval("CALL_NO")%>' Visible="false" Enabled='<%# Eval("EDIT_ENABLE") %>'
                                                runat="server"></asp:TextBox>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Item Title
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("ITEM_TITLE") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Author
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("AUTHOR") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Publisher
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("PUBLISHER") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       Item Type
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("ITEM_DES") %>
                                            <asp:CheckBox ID="CheckType" Enabled='<%# Eval("EDIT_ENABLE") %>' Visible="false"
                                                runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       Library
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("LIBRARY_DIVISION_DES")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       Library Sub
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("LIBRARY_SUB_DIVISION_DES")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       Shelf
                                             
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("SHELF_NAME")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       Rack
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("RACK_NAME")%>
                                            <br />
                                            <asp:CheckBox ID="CheckRack" Enabled='<%# Eval("EDIT_ENABLE") %>' Visible="false"
                                                runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                     <asp:TemplateField>
                                    <HeaderTemplate>
                                       Pouch Accession No
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("POUCH")%>
                                          
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Status
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("STATUS_DESCRIPTION")%>
                                            <br />
                                            <asp:CheckBox ID="CheckStatus" Enabled='<%# Eval("EDIT_ENABLE") %>' Visible="false"
                                                runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Available
                                              
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:Image ID="Image1" ImageUrl='<%#Eval("AVAILABLE_URL")%>' runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                      Active
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:Image ID="Image2" ImageUrl='<%#Eval("ACTIVE_URL")%>' runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Actions
                                              
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:LinkButton ID="LinkAction" OnClientClick='<%#Eval("OPEN_ACTIONS")%>' runat="server">Actions</asp:LinkButton>
                                        </center>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <center>
                                    <asp:Button ID="btnbarcode" runat="server" CssClass="button" OnClick="GenerateBarcode"
                                                ToolTip="Issue Barcode" Text="BC" />
                                                </center>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       Tran
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:LinkButton ID="LinkTran" OnClientClick='<%#Eval("OPEN_TRAN")%>' runat="server">Tran</asp:LinkButton>
                                        </center>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <center>
                                      <asp:Button ID="btnbarcode2" runat="server" CssClass="button" OnClick="GenerateBarcode2"
                                                ToolTip="Issue All Barcode" Text="BC(All)" />
                                                </center>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete">
                                    <HeaderTemplate>

                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                            ToolTip="Click here to select/deselect all rows (Deleting Items)" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:CheckBox ID="ch1" Text="" runat="server" /></center>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <center>
                                            <asp:Button ID="btndelete" runat="server" CssClass="button" OnClick="deletebooks" OnClientClick="editOption()"
                                                ToolTip="Delete Items" Text="Delete" />
                                                <ajaxToolkit:ConfirmButtonExtender ID="CF1" ConfirmText="Do you want to delete the selected records?" TargetControlID="btndelete" runat="server" ></ajaxToolkit:ConfirmButtonExtender>
                                       
                                        </center>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle Wrap="False" />
                            <SelectedRowStyle />
                            <HeaderStyle />
                            <EditRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                        <br />
                        <center>
                            <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" 
                                Visible="False" />
                        </center>
                    </td>
                </tr>
            </table>
            <br />
            <asp:HiddenField ID="HiddenBsuID" runat="server" />
            <asp:HiddenField ID="HiddenEmpid" runat="server" />
            <asp:HiddenField ID="HiddenShowFlag" Value="0" runat="server" />
            <asp:HiddenField ID="HiddenStockIds" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<script type="text/javascript">
    function editOption() {
        if (document.getElementById("<%=HiddenShowFlag.ClientID %>").value == '1') {
            document.getElementById("<%=HiddenShowFlag.ClientID %>").value = 0

            var sFeatures;
            sFeatures = "dialogWidth: 500px; ";
            sFeatures += "dialogHeight: 300px; ";

            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";


            var qstring = document.getElementById("<%=HiddenStockIds.ClientID %>").value + '&Option=' + document.getElementById("<%=DropEdit.ClientID %>").value

            var strOpen = 'TabPages/libraryEditOption.aspx?Stock_id=' + qstring

            window.showModalDialog(strOpen, "", sFeatures);

        }
    }
</script>






