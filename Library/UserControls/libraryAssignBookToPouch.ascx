﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryAssignBookToPouch.ascx.vb"
    Inherits="Library_UserControls_libraryAssignBookToPouch" %>

<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<script type="text/javascript">

    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }

</script>
<div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">Add Items in this Pouch/Box
            </td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Accession Number</span>
                        </td>

                        <td align="left" width="40%">
                            <asp:TextBox ID="txtaccessionnumber" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" width="20%">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="button" /></td>
                        <td align="left" width="20%">
                            <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Export" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblmessage" runat="server" CssClass="text-danger"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left">Note: Please update rack before you add to the pouch.
                Item added to the pouch must belong to the same rack as that of pouch.</td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">Items in this Pouch/Box
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:GridView ID="GridItem" runat="server" AllowPaging="True" ShowFooter="true" PageSize="50"
                    CssClass="table table-bordered table-row" AutoGenerateColumns="false" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Check">
                            <HeaderTemplate>
                                <center>
                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                        ToolTip="Click here to select/deselect all rows (Deleting Items)" />
                                </center>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:CheckBox ID="ch1" Text=""  Enabled='<%#Eval("EDIT_ENABLE")%>' runat="server" />
                                </center>
                            </ItemTemplate>
                            <FooterTemplate>
                                <center>
                                    <asp:Button ID="btnDelete" CssClass="button" runat="server" OnClick="btnDelete_Click"
                                        ToolTip="Delete from Pouch" Text="Delete" />
                                </center>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Accession No
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:HiddenField ID="HiddenBookId" runat="server" Value='<%# Eval("STOCK_ID") %>' />
                                    <asp:HiddenField ID="HiddenStockId" Value='<%#Eval("ACCESSION_NO")%>' runat="server" />
                                    <%#Eval("ACCESSION_NO")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Item Image
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:HiddenField ID="HiddenMasterId" runat="server" Value='<%# Eval("MASTER_ID") %>' />
                                    <asp:Image ID="Image" Width="40px" Height="50px" ImageUrl='<%# Eval("PRODUCT_IMAGE_URL") %>'
                                        runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Book Name
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                   <%#Eval("ITEM_TITLE")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <%--<asp:TemplateField>
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Accession&nbsp;No
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("ACCESSION_NO")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Call No
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("CALL_NO")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Library Divisions">
                            <HeaderTemplate>
                                Library Divisions
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("LIBRARY_DIVISION_DES")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Library Sub Divisions">
                            <HeaderTemplate>
                                Library Sub Divisions
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("LIBRARY_SUB_DIVISION_DES")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Price">
                            <HeaderTemplate>
                                Price
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("PRICE")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Row">
                            <HeaderTemplate>
                                Row
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("SHELF_ROW")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Shelf">
                            <HeaderTemplate>
                                Shelf
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("SHELF_NAME")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Rack
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("RACK_NAME")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Status
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("STATUS_DESCRIPTION")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" />
                    <EmptyDataRowStyle />
                    <SelectedRowStyle />
                    <HeaderStyle />
                    <EditRowStyle />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</div>
