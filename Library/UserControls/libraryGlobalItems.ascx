<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryGlobalItems.ascx.vb" Inherits="Library_UserControls_libraryGlobalItems" %>
 <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">

 <script type="text/javascript" >
 
function Redirect(value)
{
window.open('../libraryDetailView.aspx?id='+ value , '','Height=800px,Width=1020px,scrollbars=yes,resizable=no,directories=yes');
return false;
}

function Redirect2(value) {
    window.showModalDialog('../libraryUpdateCoverPhoto.aspx?id='+ value , "", "dialogWidth:550px; dialogHeight:250px; center:yes");
    window.location.reload();
} 


function OnTreeClick(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");

if(isChkBoxClick)
{
                        for(i=0; i<document.forms[0].elements.length; i++)
                           {
                               var currentid =document.forms[0].elements[i].id; 
                               if(document.forms[0].elements[i].type=="checkbox" && currentid != event.srcElement.id)
                                  {
                                    document.forms[0].elements[i].checked=false;
                                  }
                            }
}

}

function RedirectEdit()
{
var value= document.getElementById('<%= HiddenMasterId.ClientID %>').value;
var direct=document.getElementById('<%= HiddenDirect.ClientID %>').value;
if (direct == 1)
{
document.getElementById('<%= HiddenDirect.ClientID %>').value=0;
window.open('../libraryEditingItem.aspx?MasterId='+ value +'&Edit=0' , '','Height=800px,Width=1020px,scrollbars=yes,resizable=no,directories=yes');
}
return false; 
} 

</script>
<div >
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
     <table border="0" cellpadding="0" cellspacing="0" Width="100%"  >
                        <tr>
                            <td class="title-bg-lite">
                                GEMS Global Search</td>
                        </tr>
                        <tr>
                            <td align="left"  >
        <table width="100%">
            <tr>
                <td align="left" width="13%" >
                   <span class="field-label"> Item Title</span>
                </td>
              
                <td align="left" width="20%"  >
                    <asp:TextBox ID="txtItemName" runat="server"></asp:TextBox></td>
                <td align="left" width="12%"  >
                   <span class="field-label"> Author</span></td>
                
                <td align="left" width="20%" >
                    <asp:TextBox ID="txtAuthor" runat="server"></asp:TextBox></td>
                <td align="left" width="12%" >
                   <span class="field-label"> Publisher</span></td>
               
                <td align="left" width="20%" >
                    <asp:TextBox ID="txtPublisher" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" >
                   <span class="field-label"> ISBN</span></td>
              
                <td align="left" >
                    <asp:TextBox ID="txtISBN" runat="server"></asp:TextBox></td>
                <td align="left" >
                   <span class="field-label"> Master ID</span></td>
                
                <td align="left" >
                    <asp:TextBox ID="txtMasterId" runat="server"></asp:TextBox></td>
                <td align="left" >
                </td>
                <td align="left" >
                </td>
               
            </tr>
            <tr>
                <td align="center" colspan="9">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CausesValidation="false" CssClass="button"  Width="100px" />
                    <asp:Button ID="btnexport" runat="server" CssClass="button" CausesValidation="false" Text="Export" 
                        Width="100px" />
                </td>
            </tr>
        </table>
        

 </td>
                    </tr>
               </table>
    
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite" align="left">
            GEMS Global Items List :
            <asp:Label ID="lbltotal" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:GridView ID="GridItem" runat="server" AutoGenerateColumns="false" AllowPaging="True" Width="100%" OnRowCommand="GridItem_RowCommand" CssClass="table table-bordered table-row">
                <Columns>
                   <%-- <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Master&nbsp;ID
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("MASTER_ID") %>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        Item Image
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:HiddenField ID="HiddenMasterId" runat="server" Value='<%# Eval("MASTER_ID") %>' />
                                <asp:ImageButton ID="ImageItem" runat="server" Enabled='<%# Eval("SHOWURL") %>' CausesValidation="false" ToolTip='<%# Eval("MASTER_ID") %>' ImageUrl='<%# Eval("PRODUCT_IMAGE_URL") %>'
                                    PostBackUrl='<%# Eval("PRODUCT_URL") %>' />
                                    <br />
                                <asp:LinkButton ID="lnkupdateimg"  OnClientClick='<%# Eval("REDIRECT2") %>' runat="server">Update</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        Item Title
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("ITEM_TITLE") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        Author
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("AUTHOR") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        Publisher
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("PUBLISHER") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        Exists Quantity
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                          <center><%#Eval("ACTIVE_COUNT")%></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        Quantity
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="UpdateQuantity" CausesValidation="false" CommandArgument='<%# Eval("MASTER_ID") %>'>Add</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        Info
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="lnkInfo" runat="server" OnClientClick='<%# Eval("REDIRECT") %>'>Info</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem"  />
                <EmptyDataRowStyle  />
                <SelectedRowStyle  />
                <HeaderStyle  />
                <EditRowStyle  />
                <AlternatingRowStyle CssClass="griditem_alternative"  />
            </asp:GridView>
        </td>
    </tr>
</table>
        <asp:Label ID="Label1" runat="server"></asp:Label>
        <asp:Panel ID="PanelStatusupdate" runat="server" CssClass="panel-cover"
            Style="display: none">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg-lite">
                        Library Message</td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <table>
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblMessage" runat="server" class="text-danger"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnmok" runat="server"  OnClientClick="javascript:RedirectEdit(); return false;" CausesValidation="False" CssClass="button"
                                                Text="Ok" ValidationGroup="s" /></td>
                                    </tr>
                                </table>
                                &nbsp;
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
            DropShadow="true" PopupControlID="PanelStatusupdate" CancelControlID="btnmok" RepositionMode="RepositionOnWindowResizeAndScroll"
            TargetControlID="Label1">
        </ajaxToolkit:ModalPopupExtender>
<asp:Label ID="Label2" runat="server"></asp:Label>
<asp:Panel ID="PanelStatus" runat="server" CssClass="panel-cover" Width="385px" Style="display: none">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">
                                            Enter Stock Quantity</td>
        </tr>
        <tr>
            <td align="left" >
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <contenttemplate>
    <table width="100%">
        <tr >
            <td id="TD_Item_Type1" runat="server">
              <span class="field-label">  Item Type</span></td>
            <td id="TD_Item_Type2" runat="server">
              <%--  <asp:Panel ID="Panel1" runat="server" Height="100px"  Width="300px" ScrollBars="Auto"  >--%>
                <div class="checkbox-list">
                    <asp:TreeView ID="TreeItemCategory" runat="server" ImageSet="Arrows" onclick="OnTreeClick(event);" ShowCheckBoxes="All">
                        <ParentNodeStyle Font-Bold="False" />
                        <HoverNodeStyle   />
                        <SelectedNodeStyle  HorizontalPadding="0px"
                            VerticalPadding="0px" />
                        <NodeStyle  HorizontalPadding="5px"
                            NodeSpacing="0px" VerticalPadding="0px" />
                    </asp:TreeView>
                    </div>
                <%--</asp:Panel>--%>
            </td>
        </tr>
        <tr>
            <td>
                <span class="field-label">  Quantity</span></td>
            <td>
                        <asp:TextBox ID="Utxtstock" runat="server" ValidationGroup="Update" CausesValidation="True"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
               <span class="field-label">   Age Group</span></td>
            <td>
                <span class="field-label">  From :</span>
                <asp:DropDownList ID="UDDFromAge" runat="server">
                </asp:DropDownList> <span class="field-label">  To :</span><asp:DropDownList ID="UDDToAge" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td>
                <span class="field-label">  Supplier</span></td>
            <td>
                <asp:TextBox ID="Utxtsupplier" runat="server" ValidationGroup="Update" CausesValidation="True"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
               <span class="field-label">   Purchase Date</span></td>
            <td>
                <asp:TextBox ID="Utxtpurchasedate" runat="server" ValidationGroup="Update" CausesValidation="True"></asp:TextBox>
                <asp:Image ID="UImage1"
                    runat="server" ImageUrl="~/Images/calendar.gif" /></td>
        </tr>
        <tr>
            <td>
                <span class="field-label">  Cost of an Item</span></td>
            <td>
                <asp:TextBox ID="Utxtcost" runat="server" ValidationGroup="Update" CausesValidation="True"></asp:TextBox>
                <asp:DropDownList ID="Uddcurrency" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="center">
                        <asp:Button ID="btnstockupdate" runat="server" OnClick="btnstockupdate_Click" Text="Update" CssClass="button" ValidationGroup="Update"  />
                                    <asp:Button ID="btncancel3" runat="server" Text="Cancel" CssClass="button" OnClick="btncancel3_Click" CausesValidation="False"  /></td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <ajaxToolkit:FilteredTextBoxExtender ID="UF3"  FilterType="Numbers" TargetControlID="Utxtstock" runat="server" >
                        </ajaxToolkit:FilteredTextBoxExtender>
                        <ajaxToolkit:ConfirmButtonExtender ID="UCF1" TargetControlID="btnstockupdate" ConfirmText="Continue with Updation?  " runat="server" ></ajaxToolkit:ConfirmButtonExtender>
                <ajaxToolkit:CalendarExtender ID="UCE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="UImage1"
                    TargetControlID="Utxtpurchasedate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:FilteredTextBoxExtender ID="UF2" runat="server" FilterType="Custom, Numbers"
                    TargetControlID="Utxtcost" ValidChars=".,">
                </ajaxToolkit:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                        <asp:RequiredFieldValidator ID="URequiredFieldValidator1" runat="server" ControlToValidate="Utxtstock"
                            Display="None" ErrorMessage="Please enter Quantity" SetFocusOnError="True" ValidationGroup="Update"></asp:RequiredFieldValidator>
                       <%-- <asp:RequiredFieldValidator ID="URequiredFieldValidator2" runat="server" ControlToValidate="Utxtsupplier"
                            Display="None" ErrorMessage="Please Enter Supplier Details" SetFocusOnError="True"
                            ValidationGroup="Update"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="URequiredFieldValidator3" runat="server" ControlToValidate="Utxtpurchasedate"
                            Display="None" ErrorMessage="Please Enter Purchase Date" SetFocusOnError="True"
                            ValidationGroup="Update"></asp:RequiredFieldValidator><asp:RequiredFieldValidator ID="URequiredFieldValidator4" runat="server" ControlToValidate="Utxtcost"
                            Display="None" ErrorMessage="Please Enter Cost of an Item" SetFocusOnError="True"
                            ValidationGroup="Update"></asp:RequiredFieldValidator>--%>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                            ShowSummary="False" ValidationGroup="Update" />
            </td>
        </tr>
    </table>
                        
                    </contenttemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="MO2" runat="server" BackgroundCssClass="modalBackground"
            CancelControlID="btncancel3" DropShadow="true" PopupControlID="PanelStatus" RepositionMode="RepositionOnWindowResizeAndScroll"
            TargetControlID="Label2">
        </ajaxToolkit:ModalPopupExtender>
        <asp:HiddenField ID="HiddenMasterId" runat="server" />
        <asp:HiddenField ID="HiddenEmpid" runat="server" />
        <asp:HiddenField ID="HiddenBsuID" runat="server" /><asp:HiddenField ID="HiddenDirect" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>

</div>