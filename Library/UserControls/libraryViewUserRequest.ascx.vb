﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Library_UserControls_libraryViewUserRequest
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            BindGrid()
        End If
    End Sub


    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = " SELECT *, " & _
                        " '<span style=''color: red;font-weight:bold''>Hide</span>' as hide, " & _
                        " ('</br><span style=''color: red;font-weight:bold''> more... </span>')tempview1, " & _
                        " (CASE REQUEST_GRANTED WHEN 'True' Then 'True' else 'False' end)  as ShowTick, " & _
                        " (CASE REQUEST_GRANTED WHEN 'True' Then 'False' else 'True' end)  as ShowLink " & _
                        " FROM LIBRARY_REQUEST_ITEMS WHERE REQ_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY RECORD_ID DESC"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        GridComments.DataSource = ds
        GridComments.DataBind()



    End Sub

    Protected Sub GridComments_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridComments.PageIndexChanging
        GridComments.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub


    Protected Sub GridComments_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridComments.RowCommand

        If e.CommandName = "approve" Then
            LibraryData.isOffLine(Session("sBusper"))
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblMessage.Text = ""
            Try

                Dim sql_guery = "UPDATE LIBRARY_REQUEST_ITEMS SET REQUEST_GRANTED='True' where RECORD_ID=" & e.CommandArgument & ""
                SqlHelper.ExecuteScalar(transaction, CommandType.Text, sql_guery)
                lblMessage.Text = "Record updated successfully"
                transaction.Commit()
                BindGrid()

            Catch ex As Exception
                transaction.Rollback()
                lblMessage.Text = "Error : " & ex.Message
            Finally
                connection.Close()

            End Try

        End If


    End Sub


End Class
