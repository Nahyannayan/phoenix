<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryMembershipTypes.ascx.vb" Inherits="Library_UserControls_libraryMembershipTypes" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<table width="100%">
    <tr>
        <td>
            <asp:LinkButton ID="LinkAdd" runat="server" OnClientClick="javascript:return false;">Add</asp:LinkButton>

        </td>
    </tr>
</table>
<asp:Panel ID="Panel2" runat="server" Width="100%" >
    <table width="100%">
        <tr>
            <td><span class="field-label" width="20%">Library Divisions</span></td>
            <td width="30%">
                <asp:DropDownList ID="ddLibraryDivisions" runat="server">
                </asp:DropDownList></td>
            <td width="20%"><span class="field-label">Membership</span></td>
            <td width="30%">
                <asp:TextBox ID="txtMembership" runat="server" ValidationGroup="Save"></asp:TextBox></td>
        </tr>
        <tr>
            <td><span class="field-label">User Type</span></td>
            <td>
                <asp:DropDownList ID="ddusertype" runat="server">
                    <asp:ListItem Text="STUDENT" Selected="True" Value="STUDENT"></asp:ListItem>
                    <asp:ListItem Text="EMPLOYEE" Value="EMPLOYEE"></asp:ListItem>
                </asp:DropDownList></td>
            <td><span class="field-label">Maximum Items</span></td>
            <td>
                <asp:TextBox ID="txtmaxitems" runat="server" MaxLength="2" ValidationGroup="Save"></asp:TextBox></td>
        </tr>
        <tr>
            <td><span class="field-label">Lending Period (Days)</span></td>
            <td>
                <asp:TextBox ID="txtlendingperiods" runat="server" MaxLength="2" ValidationGroup="Save"></asp:TextBox></td>
            <td colspan="2" align="center">
                <asp:Button ID="btnsave" CssClass="button" runat="server" Text="Save" ValidationGroup="Save" /></td>
        </tr>
    </table>
</asp:Panel>
<ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
    AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkAdd" Collapsed="true"
    CollapsedSize="0" CollapsedText="Add" ExpandControlID="LinkAdd" ExpandedSize="200"
    ExpandedText="Hide" ScrollContents="false" TargetControlID="Panel2" TextLabelID="LinkAdd">
</ajaxToolkit:CollapsiblePanelExtender>
<br />
<asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
<table width="100%">

    <tr>
        <td>
            <asp:GridView ID="GridMemberships" runat="server" AllowPaging="True" AutoGenerateColumns="false" CssClass="table table-row table-bordered"
                OnPageIndexChanging="GridMemberships_PageIndexChanging" OnRowCancelingEdit="GridMemberships_RowCancelingEdit"
                OnRowCommand="GridMemberships_RowCommand" OnRowEditing="GridMemberships_RowEditing"
                Width="100%">
                <Columns>
                    <asp:TemplateField HeaderText="Library Divisions">
                        <HeaderTemplate>
                            Library Divisions
                        </HeaderTemplate>
                        <ItemTemplate>

                            <%#Eval("LIBRARY_DIVISION_DES")%>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="User Type">
                        <HeaderTemplate>
                            User Type
 
                        </HeaderTemplate>
                        <ItemTemplate>

                            <%#Eval("USER_TYPE")%>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Memberships">
                        <HeaderTemplate>
                            Membership Name
 
                        </HeaderTemplate>
                        <ItemTemplate>


                            <%#Eval("MEMBERSHIP_DES")%>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:HiddenField ID="HiddenMembershipId" Value='<%#Eval("MEMBERSHIP_ID")%>' runat="server" />
                            <asp:HiddenField ID="HiddenLdivisionId" Value='<%#Eval("LIBRARY_DIVISION_ID")%>' runat="server" />

                            <asp:TextBox ID="txtMembershipE" runat="server" Text='<%#Eval("MEMBERSHIP_DES")%>' ValidationGroup="Edit"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="Edit" ErrorMessage="Enter Membership Name" Display="None" SetFocusOnError="True" ControlToValidate="txtMembershipE"></asp:RequiredFieldValidator>

                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Max Items">
                        <HeaderTemplate>
                            Max Items
 
                        </HeaderTemplate>
                        <ItemTemplate>

                            <%#Eval("MAX_ITEMS")%>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:TextBox ID="txtmaxitemsE" runat="server" MaxLength="2" Text='<%#Eval("MAX_ITEMS")%>' ValidationGroup="Edit"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="Edit" ErrorMessage="Enter Maximum Items" Display="None" SetFocusOnError="True" ControlToValidate="txtmaxitemsE"></asp:RequiredFieldValidator>
                            <ajaxToolkit:FilteredTextBoxExtender ID="F1E" FilterType="Numbers" TargetControlID="txtmaxitemsE" runat="server"></ajaxToolkit:FilteredTextBoxExtender>

                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Lending Days">
                        <HeaderTemplate>
                            Lending Days
 
                        </HeaderTemplate>
                        <ItemTemplate>

                            <%#Eval("LENDING_DAYS")%>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:TextBox ID="txtlendingperiodsE" runat="server" Text='<%#Eval("LENDING_DAYS")%>' MaxLength="2" ValidationGroup="Edit"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="Edit" ErrorMessage="Enter Lending Period (In Days)" Display="None" SetFocusOnError="True" ControlToValidate="txtlendingperiodsE"></asp:RequiredFieldValidator>
                            <ajaxToolkit:FilteredTextBoxExtender ID="F2E" FilterType="Numbers" TargetControlID="txtlendingperiodsE" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="Edit" ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
                            </center>
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            Edit
 
                        </HeaderTemplate>
                        <ItemTemplate>

                            <asp:LinkButton ID="LinkEdit" runat="server" CausesValidation="false" CommandArgument='<%#Eval("MEMBERSHIP_ID")%>' CommandName="Edit">Edit</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:LinkButton ID="LinkUpdate" runat="server" CommandArgument='<%#Eval("MEMBERSHIP_ID")%>'
                                CommandName="Update" ValidationGroup="Edit">Update</asp:LinkButton>
                            <asp:LinkButton ID="LinkCancel" runat="server" CausesValidation="false" CommandArgument='<%#Eval("MEMBERSHIP_ID")%>'
                                CommandName="Cancel">Cancel</asp:LinkButton>
                            </center>
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Delete
 
                        </HeaderTemplate>
                        <ItemTemplate>

                            <asp:LinkButton ID="LinkDelete" runat="server" CausesValidation="false" CommandArgument='<%#Eval("MEMBERSHIP_ID")%>'
                                CommandName="deleting"> Delete</asp:LinkButton>
                            <ajaxToolkit:ConfirmButtonExtender ID="CF1" runat="server" ConfirmText="Are you sure, Do you want this record to be deleted ?"
                                TargetControlID="LinkDelete">
                            </ajaxToolkit:ConfirmButtonExtender>
                            </center>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                <EditRowStyle Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
</table>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMembership"
    Display="None" ErrorMessage="Enter Membership Name" SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtmaxitems"
    Display="None" ErrorMessage="Enter Maximum Items" SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtlendingperiods"
    Display="None" ErrorMessage="Enter Lending Period (In Days)" SetFocusOnError="True"
    ValidationGroup="Save"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
    ShowSummary="False" ValidationGroup="Save" />
&nbsp;
                    <asp:HiddenField ID="HiddenBsuID" runat="server" />
<ajaxToolkit:FilteredTextBoxExtender ID="F1" FilterType="Numbers" TargetControlID="txtmaxitems" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
<ajaxToolkit:FilteredTextBoxExtender ID="F2" FilterType="Numbers" TargetControlID="txtlendingperiods" runat="server">
</ajaxToolkit:FilteredTextBoxExtender>


