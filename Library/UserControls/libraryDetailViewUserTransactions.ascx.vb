Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryDetailViewUserTransactions
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenUserID.Value = Request.QueryString("UserId")
            HiddenUserType.Value = Request.QueryString("UserType")
            BindTransactions()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub


    Public Sub BindTransactions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " SELECT ITEM_TITLE,PRODUCT_IMAGE_URL,A.STOCK_ID,CASE A.USER_TYPE WHEN 'STUDENT' THEN (SELECT STU_NO FROM OASIS.dbo.STUDENT_M WHERE STU_ID=USER_ID ) ELSE " & _
                        " (SELECT EMPNO FROM OASIS.dbo.EMPLOYEE_M WHERE EMP_ID=USER_ID) END USER_ID " & _
                        " ,CASE A.USER_TYPE WHEN 'STUDENT' THEN (SELECT (ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')) FROM OASIS.dbo.STUDENT_M WHERE STU_ID=USER_ID ) ELSE " & _
                        " (SELECT isnull(EMP_FNAME,'')+' ' +isnull(EMP_MNAME,'')+' '+isnull(EMP_LNAME,'') FROM OASIS.dbo.EMPLOYEE_M WHERE EMP_ID=USER_ID) END USER_NAME " & _
                        " ,A.USER_TYPE,MEMBERSHIP_DES " & _
                        " ,REPLACE(CONVERT(VARCHAR(11), ISNULL(ITEM_TAKEN_DATE,'') , 106), ' ', '/')ITEM_TAKEN_DATE " & _
                        " ,REPLACE(CONVERT(VARCHAR(11), ISNULL(ITEM_RETURN_DATE,'') , 106), ' ', '/')ITEM_RETURN_DATE " & _
                        " ,REPLACE(CONVERT(VARCHAR(11), ITEM_ACTUAL_RETURN_DATE , 106), ' ', '/')ITEM_ACTUAL_RETURN_DATE " & _
                        " ,(SELECT STATUS_DESCRIPTION FROM dbo.LIBRARY_ITEM_STATUS WHERE STATUS_ID=ITEM_BEFORE_STATUS_ID)ISSUE_STATUS " & _
                        " ,(SELECT STATUS_DESCRIPTION FROM dbo.LIBRARY_ITEM_STATUS WHERE STATUS_ID=ITEM_AFTER_STATUS_ID)RETURN_STATUS " & _
                        " ,CASE ISNULL(RESERVATION_ID,'') WHEN '' then '~/Images/cross.png' ELSE '~/Images/tick.gif' END RESERVATION  " & _
                        " ,NOTES,RETURN_NOTES,FINE_AMOUNT,CURRENCY_ID " & _
                        " ,(SELECT isnull(EMP_FNAME,'')+' ' +isnull(EMP_MNAME,'')+' '+isnull(EMP_LNAME,'') FROM OASIS.dbo.EMPLOYEE_M WHERE EMP_ID=ISSUE_EMP_ID) ISSUED_BY " & _
                        " ,(SELECT isnull(EMP_FNAME,'')+' ' +isnull(EMP_MNAME,'')+' '+isnull(EMP_LNAME,'') FROM OASIS.dbo.EMPLOYEE_M WHERE EMP_ID=RECEIVE_EMP_ID) RECEIVED_BY " & _
                        " FROM dbo.LIBRARY_TRANSACTIONS A " & _
                        " INNER JOIN  dbo.LIBRARY_ITEMS_QUANTITY B ON A.STOCK_ID=B.STOCK_ID " & _
                        " INNER JOIN  dbo.LIBRARY_ITEMS_MASTER C ON C.MASTER_ID=B.MASTER_ID " & _
                        " INNER JOIN  dbo.LIBRARY_MEMBERSHIPS D ON D.MEMBERSHIP_ID=A.MEMBERSHIP_ID " & _
                        " WHERE 1=1" 'A.USER_TYPE='" & HiddenUserType.Value & "' AND USER_ID='" & HiddenUserID.Value & "'"

        If Request.QueryString("UserType") <> "" Then
            str_query &= " AND A.USER_TYPE ='" & HiddenUserType.Value & "'"
        End If

        If Request.QueryString("UserId") <> "" Then
            str_query &= " AND USER_ID ='" & HiddenUserID.Value & "'"
        End If

        If Request.QueryString("stock_id") <> "" Then
            str_query &= " AND A.STOCK_ID ='" & Request.QueryString("stock_id") & "'"
        End If

        str_query &= " order by A.RECORD_ID desc"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        GrdUserTransaction.DataSource = ds
        GrdUserTransaction.DataBind()


    End Sub


    Protected Sub GrdTransaction_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdUserTransaction.PageIndexChanging
        GrdUserTransaction.PageIndex = e.NewPageIndex
        BindTransactions()
    End Sub

End Class
