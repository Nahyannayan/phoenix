<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryItemList.ascx.vb"
    Inherits="Library_UserControls_libraryItemList" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="libraryItemCount.ascx" TagName="libraryItemCount" TagPrefix="uc1" %>
<%--<link href="../cssfiles/Ratings.css" rel="stylesheet" type="text/css" />--%>

<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<script type="text/javascript">

    function Redirect(value) {
        window.open('libraryDetailView.aspx?id=' + value, '', 'Height=800px,Width=1020px,scrollbars=yes,resizable=no,directories=yes');
        return false;
    }
    function RedirectEdit(value) {
        window.open('libraryEditingItem.aspx?MasterId=' + value + '&Edit=1', '', 'Height=800px,Width=1020px,scrollbars=yes,resizable=no,directories=yes');
        return false;
    }
    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }

</script>

<div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg-lite">Library Item Search
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table>
                            <tr >
                                <td align="left" width="13%"><span class="field-label">Library</span></td>

                                <td align="left" width="20%">
                                    <asp:DropDownList ID="ddLibraryDivisions" runat="server" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddLibraryDivisions_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" width="12%"><span class="field-label">Accession No</span>
                                </td>

                                <td align="left" width="20%">
                                    <asp:TextBox ID="txtstockid" runat="server"></asp:TextBox>
                                </td>
                                <td align="left" width="12%"><span class="field-label">Call No</span>
                                </td>

                                <td align="left" width="20%">
                                    <asp:TextBox ID="txtcallno" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"><span class="field-label">Author</span>
                                </td>

                                <td align="left">
                                    <asp:TextBox ID="txtAuthor" runat="server"></asp:TextBox>
                                </td>
                                <td align="left"><span class="field-label">ISBN</span>
                                </td>

                                <td align="left">
                                    <asp:TextBox ID="txtISBN" runat="server"></asp:TextBox>
                                </td>
                                <td align="left"><span class="field-label">Publisher</span>
                                </td>

                                <td align="left">
                                    <asp:TextBox ID="txtPublisher" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"><span class="field-label">Item Title</span></td>

                                <td align="left">
                                    <asp:TextBox ID="txtItemName" runat="server"></asp:TextBox>
                                </td>
                                <td align="left"><span class="field-label">Option</span></td>

                                <td align="left">
                                    <asp:DropDownList ID="DropOptions" runat="server" AutoPostBack="True">
                                        <asp:ListItem Selected="True" Text="All" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Not Assigned Accession No" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Not Assigned Call No" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Not Assigned Rack" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Not Assigned Status" Value="3"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td><span class="field-label">Subject</span></td>

                                <td align="left">
                                    <asp:TextBox ID="txtsubject" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>


                                <td align="left">
                                    <asp:TextBox ID="txtMasterId" runat="server" Visible="False"></asp:TextBox>
                                </td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="6">
                                    <asp:Button ID="btnSearch" runat="server" CssClass="button"
                                        Text="Search" />
                                    <asp:Button ID="btnexport" runat="server" CssClass="button"
                                        Text="Export" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <table border="0"  cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg-lite">Library Items :
                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GridItem" AutoGenerateColumns="false" runat="server" Width="100%" CssClass="table table-bordered table-row"
                            AllowPaging="True" ShowFooter="true" EmptyDataText="No Records Found. Please search with some other keywords.">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       Call No
                                              
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("TOP_CALL_NO") %>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Item Image
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:HiddenField ID="HiddenMasterId" Value='<%# Eval("MASTER_ID") %>' runat="server" />
                                            <asp:ImageButton ID="ImageItem" ImageUrl='<%# Eval("PRODUCT_IMAGE_URL") %>' ToolTip='<%# Eval("MASTER_ID") %>'  PostBackUrl='<%# Eval("PRODUCT_URL") %>'
                                                Enabled='<%# Eval("SHOWURL") %>' runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Item Type
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("ITEM_DES") %>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       Item Title
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("ITEM_TITLE") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       Author
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("AUTHOR") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       Publisher
                                              
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("PUBLISHER") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       T/R/I/A
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <uc1:libraryItemCount ID="libraryItemCount1" LIBRARY_DIVISION_ID='<%# Eval("LIBRARY_DIVISION_ID_VAL") %>'
                                                MASTER_ID='<%# Eval("MASTER_ID") %>' runat="server" />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                       Actions
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:LinkButton ID="lnkEdit" OnClientClick='<%# Eval("REDIRECTEDIT") %>' runat="server">Actions</asp:LinkButton>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Info
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:LinkButton ID="lnkInfo" OnClientClick='<%# Eval("REDIRECT") %>' runat="server">View</asp:LinkButton>
                                        </center>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Button ID="btnbarcode2" runat="server" CssClass="button" OnClick="GenerateBarcode2"
                                            ToolTip="Issue All Barcode" Text="BC(All)" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Check">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                            ToolTip="Click here to select/deselect all rows" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:CheckBox ID="ch1" Text="" runat="server" /></center>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <center>
                                            <asp:Button ID="btnbarcode" runat="server" CssClass="button" OnClick="GenerateBarcode"
                                                ToolTip="Issue Barcode" Text="BC" />
                                        </center>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle />
                            <SelectedRowStyle  />
                            <HeaderStyle />
                            <EditRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative"  />
                        </asp:GridView>
                        <br />
                        T = Total Active Items, R = Total Items Reserved,I = Total Items Issued, A = Total Items Available 
                        
                    </td>
                </tr>
            </table>
            <br />
            <asp:HiddenField ID="HiddenBsuID" runat="server" />
            <asp:HiddenField ID="HiddenShowFlag" runat="server" />
            <asp:HiddenField ID="HiddenEmpid" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

</div>

<script type="text/javascript">

    if (document.getElementById("<%=HiddenShowFlag.ClientID %>").value == '1') {
        document.getElementById("<%=HiddenShowFlag.ClientID %>").value = 0


        var sFeatures;
        sFeatures = "dialogWidth: 800px; ";
        sFeatures += "dialogHeight: 600px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";


        var result;
        result = window.open('Barcode/BarcodeCrystal/Barcode.aspx')


    }


</script>

