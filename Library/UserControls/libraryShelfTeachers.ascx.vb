﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryShelfTeachers
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Hiddenbsuid.Value = Session("sbsuid")
            BindDesignation()
            BindLibraryDivisions()
            BindLibrarySubDivisions()
            BindGrid()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindDesignation()
    
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select des_id , des_descr from EMPDESIGNATION_M where des_flag='SD' order by des_descr "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dddes.DataSource = ds
        dddes.DataValueField = "des_id"
        dddes.DataTextField = "des_descr"
        dddes.DataBind()
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Select a Designation"
        dddes.Items.Insert(0, list)

    End Sub

    Public Sub BindLibraryDivisions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddLibraryDivisions.DataSource = ds
        ddLibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
        ddLibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
        ddLibraryDivisions.DataBind()

    End Sub

    Public Sub BindLibrarySubDivisions()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_SUB_DIVISIONS " & _
                        "WHERE LIBRARY_DIVISION_ID='" & ddLibraryDivisions.SelectedValue & "' ORDER BY LIBRARY_SUB_DIVISION_DES "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddLibrarySubDivisions.DataSource = ds
        ddLibrarySubDivisions.DataTextField = "LIBRARY_SUB_DIVISION_DES"
        ddLibrarySubDivisions.DataValueField = "LIBRARY_SUB_DIVISION_ID"
        ddLibrarySubDivisions.DataBind()

        BindShelfS()

    End Sub

    Public Sub BindShelfS()
        lblMessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_SHELFS " & _
                        "WHERE LIBRARY_SUB_DIVISION_ID='" & ddLibrarySubDivisions.SelectedValue & "' ORDER BY SHELF_NAME "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddShelfName.DataSource = ds
        ddShelfName.DataTextField = "SHELF_NAME"
        ddShelfName.DataValueField = "SHELF_ID"
        ddShelfName.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            btnSave.Visible = True
        Else
            btnSave.Visible = False
        End If

    End Sub

    Protected Sub btnStatusupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try

            Dim sql_query = ""
            For Each row As GridViewRow In GrdStaff.Rows
                Dim checkstf = DirectCast(row.FindControl("Checklist"), CheckBox).Checked
                If checkstf = True Then
                    Dim emp_id = DirectCast(row.FindControl("Hiddenempid"), HiddenField).Value

                    Dim pParms(3) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@EMP_ID", emp_id)
                    pParms(1) = New SqlClient.SqlParameter("@SHELF_ID", ddShelfName.SelectedValue)
                    pParms(2) = New SqlClient.SqlParameter("@OPTION", 1)
                    SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_DELETE_LIBRARY_TEACHER_SHELFS", pParms)

                End If

            Next

            lblMessage.Text = "Transaction done successfully."
            transaction.Commit()

        Catch ex As Exception

            lblMessage.Text = "Error while transaction. Error :" & ex.Message
            transaction.Rollback()

        Finally

            connection.Close()

        End Try

        MO1.Show()
        BindGrid()
    End Sub

    Protected Sub btncancel4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel4.Click
        MO3.Hide()
    End Sub

    Protected Sub ddLibraryDivisions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddLibraryDivisions.SelectedIndexChanged
        BindLibrarySubDivisions()
        MO3.Show()
    End Sub

    Protected Sub ddLibrarySubDivisions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddLibrarySubDivisions.SelectedIndexChanged
        BindShelfS()
        MO3.Show()
    End Sub

    Protected Sub btnmessageok_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnmok.Click
        MO1.Hide()
    End Sub

    Public Function GetData() As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select EMP_ID,EMPNO, isnull(EMP_FNAME,'')+' ' +isnull(EMP_MNAME,'')+' '+isnull(EMP_LNAME,'') ENAME, DES_DESCR,EMP_DES_ID from dbo.EMPLOYEE_M a " & _
                        " inner join  dbo.EMPDESIGNATION_M b on a.EMP_DES_ID=b.DES_ID where EMP_BSU_ID='" & Hiddenbsuid.Value & "' and des_flag='SD' and EMP_bACTIVE='True' and EMP_STATUS <> 4 "
        Dim txtname, txtnumber As String
        Dim desigid As DropDownList
        If GrdStaff.Rows.Count > 0 Then
            txtnumber = DirectCast(GrdStaff.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStaff.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            desigid = dddes

            If txtnumber.Trim() <> "" Then
                str_query &= " and EMPNO like '%" & txtnumber.Trim() & "%' "
            End If

            If txtname.Trim() <> "" Then
                str_query &= " and isnull(EMP_FNAME,'')+ isnull(EMP_MNAME,'')+isnull(EMP_LNAME,'') like '%" & txtname.Replace(" ", "").Trim() & "%' "
            End If
            If desigid.SelectedValue > -1 Then
                str_query &= " and EMP_DES_ID= '" & desigid.SelectedValue & "'"
            End If

        End If

        str_query &= " order by EMP_FNAME "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Return ds
    End Function

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim txtname, txtnumber As String
        Dim desigid As DropDownList

        If GrdStaff.Rows.Count > 0 Then
            txtnumber = DirectCast(GrdStaff.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStaff.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            desigid = dddes
        End If


        Dim ds As DataSet
        ds = GetData()

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("ENAME")
            dt.Columns.Add("EMPNO")
            dt.Columns.Add("MEMBERSHIP_DES")
            dt.Columns.Add("EMP_ID")
            dt.Columns.Add("EMP_DES_ID")
            dt.Columns.Add("DES_DESCR")

            Dim dr As DataRow = dt.NewRow()
            dr("ENAME") = ""
            dr("EMPNO") = ""
            dr("MEMBERSHIP_DES") = ""
            dr("EMP_ID") = ""
            dr("EMP_DES_ID") = ""
            dr("DES_DESCR") = ""
            dt.Rows.Add(dr)
            GrdStaff.DataSource = dt
            GrdStaff.DataBind()
            DirectCast(GrdStaff.FooterRow.FindControl("btnaddmembers"), Button).Visible = False

        Else
            GrdStaff.DataSource = ds
            GrdStaff.DataBind()

            str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
            Dim query = ""
            For Each row As GridViewRow In GrdStaff.Rows
                Dim emp_id = DirectCast(row.FindControl("Hiddenempid"), HiddenField).Value
                Dim GridShelfs As GridView = DirectCast(row.FindControl("GridShelfs"), GridView)
                query = " SELECT ASSIGN_ID,LIBRARY_DIVISION_DES,LIBRARY_SUB_DIVISION_DES,SHELF_NAME from dbo.LIBRARY_TEACHER_SHELFS A " & _
                        " INNER JOIN dbo.LIBRARY_SHELFS B ON A.SHELF_ID = B.SHELF_ID " & _
                        " INNER JOIN dbo.LIBRARY_SUB_DIVISIONS C ON C.LIBRARY_SUB_DIVISION_ID=B.LIBRARY_SUB_DIVISION_ID " & _
                        " INNER JOIN  dbo.LIBRARY_DIVISIONS E ON E.LIBRARY_DIVISION_ID=C.LIBRARY_DIVISION_ID " & _
                        " WHERE A.EMP_ID='" & emp_id & "'"
                GridShelfs.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

                GridShelfs.DataBind()

            Next

            DirectCast(GrdStaff.FooterRow.FindControl("btnaddmembers"), Button).Visible = True
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStaff.FooterRow.FindControl("btnaddmembers"), Button))

        End If

        If GrdStaff.Rows.Count > 0 Then

            DirectCast(GrdStaff.HeaderRow.FindControl("txtnumber"), TextBox).Text = txtnumber
            DirectCast(GrdStaff.HeaderRow.FindControl("txtname"), TextBox).Text = txtname

            If desigid Is Nothing Then
            Else
                dddes.SelectedValue = desigid.SelectedValue
            End If
        End If

    End Sub

    Protected Sub ddesschange(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddes.SelectedIndexChanged
        BindGrid()
    End Sub

    Protected Sub GridShelfs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdStaff.RowCommand
        If e.CommandName = "assign" Then
            MO3.Show()
        End If
        If e.CommandName = "search" Then
            BindGrid()
        End If
        If e.CommandName = "Deleting" Then

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            Try


                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ASSIGN_ID", e.CommandArgument)
                pParms(1) = New SqlClient.SqlParameter("@OPTION", 2)
                lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_DELETE_LIBRARY_TEACHER_SHELFS", pParms)
                transaction.Commit()

            Catch ex As Exception

                lblMessage.Text = "Error while transaction. Error :" & ex.Message
                transaction.Rollback()

            Finally

                connection.Close()

            End Try

            MO1.Show()
            BindGrid()

        End If
    End Sub

    Protected Sub GrdStaff_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdStaff.PageIndexChanging
        GrdStaff.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

End Class
