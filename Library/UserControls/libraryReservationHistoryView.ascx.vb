Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Xml
Partial Class Library_UserControls_libraryReservationHistoryView
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            HiddenMasterId.Value = Request.QueryString("id")
            BindReservation()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Public Sub BindReservation()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = " SELECT * " & _
                        " ,'javascript:UserTransactions(''' + USER_TYPE + ''','''+ CONVERT(VARCHAR,GET_USER_ID)  +'''); return false;' USERREDIRECT " & _
                        " FROM ( " & _
                        " SELECT RESERVATION_ID,USER_ID AS GET_USER_ID, " & _
                        " CASE ISNULL(RESERVE_START_DATE,'') WHEN '' " & _
                        " THEN CASE RESERVATION_CANCEL WHEN 'TRUE' THEN 'FALSE' ELSE 'TRUE' END " & _
                        " ELSE CASE RESERVATION_CANCEL WHEN 'TRUE' THEN 'FALSE' ELSE 'FALSE' END " & _
                        " END RESERVATION_CANCEL_VISIBLE ," & _
                        " CASE USER_TYPE WHEN 'STUDENT' THEN (SELECT STU_NO FROM OASIS.dbo.STUDENT_M WHERE STU_ID=USER_ID ) ELSE " & _
                        " (SELECT EMPNO FROM OASIS.dbo.EMPLOYEE_M WHERE EMP_ID=USER_ID) END USER_ID " & _
                        " ,USER_TYPE,CONTACT_ADDRESS, " & _
                        " CASE ISNULL(RESERVE_START_DATE,'') WHEN '' " & _
                        " THEN CASE RESERVATION_CANCEL WHEN 'TRUE' THEN '~/Images/cross.png' ELSE '~/Images/tick.gif' END " & _
                        " ELSE CASE RESERVATION_CANCEL WHEN 'TRUE' THEN '~/Images/cross.png' ELSE '~/Images/FilledStar.png' END " & _
                        " END RESERVATION_CANCEL, " & _
                        " CASE TRAN_RECORD_ID WHEN '0' THEN 'Cancelled' ELSE '' END CANCEL_RESERVATION , " & _
                        " CASE USER_TYPE WHEN 'STUDENT' THEN (SELECT (ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')) FROM OASIS.dbo.STUDENT_M WHERE STU_ID=USER_ID ) ELSE " & _
                        " (SELECT isnull(EMP_FNAME,'')+' ' +isnull(EMP_MNAME,'')+' '+isnull(EMP_LNAME,'') FROM OASIS.dbo.EMPLOYEE_M WHERE EMP_ID=USER_ID) END USER_NAME,A.ENTRY_DATE,REPLACE(CONVERT(VARCHAR(11), RESERVE_START_DATE , 106), ' ', '/')RESERVE_START_DATE ,REPLACE(CONVERT(VARCHAR(11), RESERVE_END_DATE , 106), ' ', '/')RESERVE_END_DATE FROM dbo.LIBRARY_ITEM_RESERVATION A" & _
                        " INNER JOIN dbo.LIBRARY_DIVISIONS B ON A.LIBRARY_DIVISION_ID= B.LIBRARY_DIVISION_ID " & _
                        " WHERE A.MASTER_ID='" & HiddenMasterId.Value & "' AND B.LIBRARY_BSU_ID='" & HiddenBsuID.Value & "'" & _
                        " )A "


        Dim txtnumber As String
        Dim txtname As String
        Dim txttype As String
        Dim txtContact As String

        Dim filter As String = ""

        If GrdReservation.Rows.Count > 0 Then

            txtnumber = DirectCast(GrdReservation.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdReservation.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txttype = DirectCast(GrdReservation.HeaderRow.FindControl("txtType"), TextBox).Text.Trim()
            txtContact = DirectCast(GrdReservation.HeaderRow.FindControl("txtContact"), TextBox).Text.Trim()

            If txtnumber.Trim() <> "" Then
                If filter = "" Then
                    filter &= " USER_ID like '%" & txtnumber.Replace(" ", "") & "%' "
                Else
                    filter &= " and USER_ID like '%" & txtnumber.Replace(" ", "") & "%' "
                End If
            End If

            If txtname.Trim() <> "" Then
                If filter = "" Then
                    filter &= " USER_NAME like '%" & txtname.Replace(" ", "") & "%' "
                Else
                    filter &= " and USER_NAME like '%" & txtname.Replace(" ", "") & "%' "
                End If

            End If

            If txttype.Trim() <> "" Then
                If filter = "" Then
                    filter &= " USER_TYPE like '%" & txttype.Replace(" ", "") & "%' "
                Else
                    filter &= " and USER_TYPE like '%" & txttype.Replace(" ", "") & "%' "
                End If

            End If

            If txtContact.Trim() <> "" Then
                If filter = "" Then
                    filter &= "  CONTACT_ADDRESS like '%" & txtContact.Replace(" ", "") & "%' "
                Else
                    filter &= " and  CONTACT_ADDRESS  like '%" & txtContact.Replace(" ", "") & "%' "

                End If
            End If

        End If

        If filter <> "" Then
            Sql_Query = Sql_Query & " WHERE " & filter
        End If

        Sql_Query &= " order by ENTRY_DATE  DESC "


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("RESERVATION_ID")
            dt.Columns.Add("USERREDIRECT")
            dt.Columns.Add("USER_ID")
            dt.Columns.Add("USER_TYPE")
            dt.Columns.Add("CONTACT_ADDRESS")
            dt.Columns.Add("RESERVATION_CANCEL")
            dt.Columns.Add("RESERVATION_CANCEL_VISIBLE")
            dt.Columns.Add("USER_NAME")
            dt.Columns.Add("RESERVE_START_DATE")
            dt.Columns.Add("RESERVE_END_DATE")
            dt.Columns.Add("CANCEL_RESERVATION")
            dt.Columns.Add("ENTRY_DATE")

            Dim dr As DataRow = dt.NewRow()
            dr("RESERVATION_ID") = ""
            dr("USERREDIRECT") = "" ''"javascript:UserTransactions('0','0')"
            dr("USER_ID") = ""
            dr("USER_TYPE") = ""
            dr("CONTACT_ADDRESS") = ""
            dr("RESERVATION_CANCEL") = ""
            dr("USER_NAME") = ""
            dr("RESERVATION_CANCEL_VISIBLE") = "False"
            dr("RESERVE_START_DATE") = ""
            dr("RESERVE_END_DATE") = ""
            dr("CANCEL_RESERVATION") = ""
            dr("ENTRY_DATE") = ""

            dt.Rows.Add(dr)
            GrdReservation.DataSource = dt
            GrdReservation.DataBind()

        Else
            GrdReservation.DataSource = ds
            GrdReservation.DataBind()


        End If

        If GrdReservation.Rows.Count > 0 Then

            DirectCast(GrdReservation.HeaderRow.FindControl("txtnumber"), TextBox).Text = txtnumber
            DirectCast(GrdReservation.HeaderRow.FindControl("txtname"), TextBox).Text = txtname
            DirectCast(GrdReservation.HeaderRow.FindControl("txtType"), TextBox).Text = txttype
            DirectCast(GrdReservation.HeaderRow.FindControl("txtContact"), TextBox).Text = txtContact

        End If



    End Sub


    Protected Sub GrdReservation_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdReservation.RowCommand
        If e.CommandName = "search" Then
            BindReservation()
        End If

    End Sub

    Protected Sub GrdReservation_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdReservation.PageIndexChanging
        GrdReservation.PageIndex = e.NewPageIndex
        BindReservation()
    End Sub
End Class
