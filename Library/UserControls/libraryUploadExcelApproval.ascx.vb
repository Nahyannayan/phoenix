﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class Library_UserControls_libraryUploadExcelApproval
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBsu_id.Value = Session("sbsuid")
            Attempt()
            BindGrid()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnExcel)

        If GridItem.Rows.Count > 0 Then

            If Library.LibrarySuperAcess(Session("EmployeeId")) Then
                DirectCast(GridItem.FooterRow.FindControl("btnapproveall"), Button).Visible = True
            Else
                DirectCast(GridItem.FooterRow.FindControl("btnapproveall"), Button).Visible = False
            End If
        End If

    End Sub

    Public Sub Attempt()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = " SELECT DISTINCT UPLOAD_ATTEMPT, CONVERT(VARCHAR, UPLOAD_ATTEMPT) + ' - ' + REPLACE(CONVERT(VARCHAR(11), MIN(ENTRY_DATE), 106), ' ', '/')   AS ENTRY_DATE FROM dbo.LIBRARY_ITEMS_MASTER_EXCEL WHERE ENTRY_BSU_ID='" & HiddenBsu_id.Value & "' " & _
                        " GROUP BY UPLOAD_ATTEMPT ORDER BY UPLOAD_ATTEMPT DESC "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddattempt.DataSource = ds
        ddattempt.DataValueField = "UPLOAD_ATTEMPT"
        ddattempt.DataTextField = "ENTRY_DATE"
        ddattempt.DataBind()

        Dim list As New ListItem
        list.Text = "Select Attempt"
        list.Value = "-1"
        ddattempt.Items.Insert(0, list)




    End Sub
    Protected Sub DropData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropData.SelectedIndexChanged
        BindGrid()
    End Sub

    Protected Sub DropError_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropError.SelectedIndexChanged
        BindGrid()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblMessage.Text = ""
        BindGrid()
    End Sub

    Public Function Getdata() As DataSet

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = "SELECT * FROM VIEW_UPLOADED_EXCEL WHERE ENTRY_BSU_ID='" & HiddenBsu_id.Value & "'"

        If txtrecordid.Text <> "" Then
            Sql_Query &= " AND RECORD_ID ='" & txtrecordid.Text.Trim() & "'"
        End If

        If txtItemName.Text <> "" Then
            Sql_Query &= " AND ITEM_TITLE LIKE '%" & txtItemName.Text.Trim() & "%'"
        End If

        If txtAuthor.Text <> "" Then
            Sql_Query &= " AND AUTHOR LIKE '%" & txtAuthor.Text.Trim() & "%'"
        End If

        If txtISBN.Text <> "" Then
            Sql_Query &= " AND (UPLOAD_ISBN LIKE '%" & txtISBN.Text.Trim() & "%' OR UPLOAD_ISBN2 LIKE '%" & txtISBN.Text.Trim() & "%')"
        End If


        If txtPublisher.Text <> "" Then
            Sql_Query &= " AND PUBLISHER LIKE '%" & txtPublisher.Text.Trim() & "%'"
        End If

        If DropData.SelectedValue = 0 Then
            Sql_Query &= " AND PRIMARY_MASTER_ID IS NULL"
        End If

        If DropData.SelectedValue = 1 Then
            Sql_Query &= " AND PRIMARY_MASTER_ID IS NOT NULL"
        End If

        If DropError.SelectedValue = 0 Then
            Sql_Query &= " AND ERROR_UPLOAD='True'"
        End If
        If DropError.SelectedValue = 1 Then
            Sql_Query &= " AND ERROR_DATA_RETRIEVE='True'"
        End If

        If DropApproved.SelectedIndex > 0 Then
            Sql_Query &= " AND APPROVED='" & DropApproved.SelectedValue & "'"
        End If

        If DropDataRetv.SelectedIndex > 0 Then

            If DropDataRetv.SelectedValue = "0" Then
                Sql_Query &= " AND STARTED='1' AND ERROR_DATA_RETRIEVE='False' "
            End If
            If DropDataRetv.SelectedValue = "1" Then
                Sql_Query &= " AND PRIMARY_MASTER_ID is null and STARTED='False' AND ERROR_UPLOAD='FALSE' "
            End If
            If DropDataRetv.SelectedValue = "2" Then
                Sql_Query &= " AND PRIMARY_MASTER_ID is null and ERROR_DATA_RETRIEVE='True' and STARTED='True' "
            End If
        End If

        If ddattempt.SelectedIndex > 0 Then
            Sql_Query &= " AND UPLOAD_ATTEMPT='" & ddattempt.SelectedValue & "'"
        End If

        Sql_Query &= "  ORDER BY UPLOAD_ISBN desc,UPLOAD_ISBN2 desc "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        Return ds
    End Function

    Public Sub BindGrid()

        Try
            Dim ds As DataSet = Getdata()
            GridItem.DataSource = ds
            GridItem.DataBind()

            lblTotal.Text = "Total Items Found :" & ds.Tables(0).Rows.Count
        Catch ex As Exception

        End Try


    End Sub

    Protected Sub GridItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridItem.PageIndexChanging
        GridItem.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub



    Protected Sub DropApproved_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropApproved.SelectedIndexChanged
        BindGrid()
    End Sub

    Protected Sub GridItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridItem.RowCommand

        If e.CommandName = "Deleting" Then
            LibraryData.isOffLine(Session("sBusper"))
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
            Dim Sql_Query = "DELETE  LIBRARY_ITEMS_MASTER_EXCEL WHERE RECORD_ID='" & e.CommandArgument & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, Sql_Query)
            BindGrid()
        End If

        If e.CommandName = "Approve" Then
            Approve()
        End If

        If e.CommandName = "ApproveAll" Then
            ApproveAll()
        End If

    End Sub

    Public Sub ApproveAll()

        lblMessage.Text = ""
        Dim flag = 0
        Dim i As Integer
        If DropDataRetv.SelectedValue = "-1" And DropData.SelectedValue = "1" And DropApproved.SelectedValue = "False" Then

            Dim ds As DataSet
            ds = Getdata()

            For i = 0 To ds.Tables(0).Rows.Count - 1
                Approveset(ds.Tables(0).Rows(i).Item("RECORD_ID").ToString())
            Next

            flag = 1

        End If

        If DropDataRetv.SelectedValue = 0 And DropApproved.SelectedValue = "False" Then

            Dim ds As DataSet
            ds = Getdata()

            For i = 0 To ds.Tables(0).Rows.Count - 1
                Approveset(ds.Tables(0).Rows(i).Item("RECORD_ID").ToString())
            Next

            flag = 1

        End If


        If flag = 1 Then

            lblMessage.Text = "Items approved successfully."
        Else
            lblMessage.Text = "Approval all - Works for only Item which are Pending for Approval"
        End If


    End Sub

    Public Sub Approveset(ByVal record_id As String)

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction

        lblMessage.Text = ""
        Dim sql_query As String = ""
        Dim ISBN As String = ""
        Dim master_id As String = ""
        Dim ds As DataSet
        connection.Open()
        transaction = connection.BeginTransaction()

        Try

            '' Check if Data Exists already.

            sql_query = "SELECT ISBN,PRIMARY_MASTER_ID,GOOGLE_BOOKS FROM LIBRARY_ITEMS_MASTER_EXCEL WITH (NOLOCK) WHERE RECORD_ID='" & record_id & "'"
            ds = SqlHelper.ExecuteDataset(transaction, CommandType.Text, sql_query)

            ISBN = ds.Tables(0).Rows(0).Item("ISBN").ToString()
            master_id = ds.Tables(0).Rows(0).Item("PRIMARY_MASTER_ID").ToString()
            Dim GOOGLE_BOOKS = ds.Tables(0).Rows(0).Item("GOOGLE_BOOKS").ToString()

            If GOOGLE_BOOKS = "True" Then

                If master_id = "" Then
                    If ISBN <> "" Then
                        master_id = CheckPreviousDataISBN(ISBN, transaction)
                        If master_id = "" Then ''No data found - Enter new record to master
                            EnterNewRecord(record_id, transaction)
                        Else '' Add only additional details
                            UpdateStocks(record_id, master_id, transaction)
                        End If
                    End If
                Else
                    '' Add only additional details
                    UpdateStocks(record_id, master_id, transaction)
                End If

            Else

                EnterNewRecord(record_id, transaction)

            End If


            ''Approve the Record
            ApproveRecord(record_id, transaction)

            transaction.Commit()
            lblMessage.Text = "Selected records approved successfully."

        Catch ex As Exception
            transaction.Rollback()
            lblMessage.Text = "Error occured while saving . " & ex.Message
        Finally
            connection.Close()
        End Try

    End Sub


    Public Sub Approve()
        LibraryData.isOffLine(Session("sBusper"))

        For Each row As GridViewRow In GridItem.Rows

            Dim check As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)

            If check.Checked Then

                Approveset(DirectCast(row.FindControl("HiddenMasterIdExcel"), HiddenField).Value)

            End If

        Next

        ''Bind Grid
        BindGrid()

    End Sub


    Public Sub ApproveRecord(ByVal Recordid As String, ByVal transaction As SqlTransaction)

        Dim sql_query = "UPDATE LIBRARY_ITEMS_MASTER_EXCEL SET APPROVED='True' where  RECORD_ID='" & Recordid & "'"
        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)

    End Sub

    Public Sub UpdateStocks(ByVal Recordid As String, ByVal masterid As String, ByVal transaction As SqlTransaction)

        Dim sql_query = " SELECT A.ENTRY_BSU_ID,A.ENTRY_EMP_ID,A.TOTAL_STOCK FROM dbo.LIBRARY_ITEMS_MASTER_EXCEL A WITH (NOLOCK) " & _
                        " INNER JOIN dbo.LIBRARY_ITEMS_MASTER B WITH (NOLOCK) ON A.PRIMARY_MASTER_ID= B.MASTER_ID " & _
                        " WHERE A.RECORD_ID='" & Recordid & "' AND  PRIMARY_MASTER_ID='" & masterid & "' "


        Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, sql_query)

        If ds.Tables(0).Rows.Count > 0 Then

            '' Check for Previous Item id if exists.
            sql_query = "Select Top 1 ITEM_ID from dbo.LIBRARY_ITEMS_QUANTITY where MASTER_ID='" & masterid & "' and PRODUCT_BSU_ID='" & ds.Tables(0).Rows(0).Item("ENTRY_BSU_ID").ToString() & "'"
            Dim val As String = ""

            val = SqlHelper.ExecuteScalar(transaction, CommandType.Text, sql_query)

            If val = "" Then
                val = 1
            End If

            ''Update the Item type record
            sql_query = "Update LIBRARY_ITEMS_MASTER_EXCEL set ITEM_ID='" & val & "' Where RECORD_ID='" & Recordid & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)


            sql_query = "SELECT * FROM LIBRARY_ITEMS_MASTER_EXCEL WITH (NOLOCK) WHERE RECORD_ID='" & Recordid & "'"
            Dim dsr As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, sql_query)

            If dsr.Tables(0).Rows.Count > 0 Then

                Dim pParms(13) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@MASTER_ID", masterid)
                pParms(1) = New SqlClient.SqlParameter("@PRODUCT_BSU_ID", ds.Tables(0).Rows(0).Item("ENTRY_BSU_ID").ToString())
                pParms(2) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", ds.Tables(0).Rows(0).Item("ENTRY_EMP_ID").ToString())
                pParms(3) = New SqlClient.SqlParameter("@QUANTITY", ds.Tables(0).Rows(0).Item("TOTAL_STOCK").ToString())
                pParms(4) = New SqlClient.SqlParameter("@PRODUCT_PRICE", dsr.Tables(0).Rows(0).Item("PRODUCT_PRICE").ToString())
                pParms(5) = New SqlClient.SqlParameter("@PRICE_CURRENCY", dsr.Tables(0).Rows(0).Item("PRICE_CURRENCY").ToString())
                pParms(6) = New SqlClient.SqlParameter("@FROM_AGE_GROUP", dsr.Tables(0).Rows(0).Item("FROM_AGE_GROUP").ToString())
                pParms(7) = New SqlClient.SqlParameter("@TO_AGE_GROUP", dsr.Tables(0).Rows(0).Item("TO_AGE_GROUP").ToString())
                pParms(8) = New SqlClient.SqlParameter("@SUPPLIER", dsr.Tables(0).Rows(0).Item("SUPPLIER").ToString())
                pParms(9) = New SqlClient.SqlParameter("@PURCHASE_DATE", dsr.Tables(0).Rows(0).Item("PURCHASE_DATE").ToString())
                pParms(10) = New SqlClient.SqlParameter("@ITEM_ID", val)

                Dim acc_No = dsr.Tables(0).Rows(0).Item("ACCESSION_NO").ToString().Trim()
                Dim call_no = dsr.Tables(0).Rows(0).Item("CALL_NO").ToString().Trim()
                Dim quantity = ds.Tables(0).Rows(0).Item("TOTAL_STOCK").ToString()

                If acc_No <> "" And quantity = 1 Then
                    pParms(11) = New SqlClient.SqlParameter("@ACCESSION_NO", acc_No)
                End If
                If call_no <> "" Then
                    pParms(12) = New SqlClient.SqlParameter("@CALL_NO", call_no)
                End If

                SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_ITEM_STOCK", pParms)

                ''Update the Approved Master Id
                sql_query = "UPDATE LIBRARY_ITEMS_MASTER_EXCEL SET APPROVED_MASTER_ID='" & masterid & "' where  RECORD_ID='" & Recordid & "'"
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)

            End If




        End If


    End Sub
    Public Function CheckItemCategory(ByVal itemid As String, ByVal bsu_id As String, ByVal transaction As SqlTransaction, ByVal Record_id As String) As String
        Dim ReturnValue As String = "1"

        If itemid <> "1" Then
            ReturnValue = ""
            Dim sql_query = "SELECT ITEM_ID FROM LIBRARY_ITEMS WHERE ITEM_ID='" & itemid & "' AND ITEM_BSU_ID='" & bsu_id & "'"
            ReturnValue = SqlHelper.ExecuteScalar(transaction, CommandType.Text, sql_query)
            If ReturnValue = "" Then
                ReturnValue = "1"
            End If

            ''Update the Item type record
            sql_query = "Update LIBRARY_ITEMS_MASTER_EXCEL set ITEM_ID='" & itemid & "' Where RECORD_ID='" & Record_id & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)

        Else
            ReturnValue = "1"
        End If

        Return ReturnValue
    End Function
    Public Sub EnterNewRecord(ByVal Recordid As String, ByVal transaction As SqlTransaction)

        Dim sql_query = "SELECT * FROM LIBRARY_ITEMS_MASTER_EXCEL WITH (NOLOCK) WHERE RECORD_ID='" & Recordid & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, sql_query)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim Upload_ISBN = ds.Tables(0).Rows(0).Item("UPLOAD_ISBN").ToString()
            Dim GOOGLE_BOOKS = ds.Tables(0).Rows(0).Item("GOOGLE_BOOKS")
            ''Check again if DataExists
            If CheckItemExists(Upload_ISBN, transaction, Recordid, GOOGLE_BOOKS) = False Then

                Dim item_id = CheckItemCategory(ds.Tables(0).Rows(0).Item("ITEM_ID").ToString(), ds.Tables(0).Rows(0).Item("ENTRY_BSU_ID").ToString(), transaction, Recordid)

                Dim pParms(24) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ITEM_ID", item_id)
                pParms(1) = New SqlClient.SqlParameter("@ITEM_TITLE", ds.Tables(0).Rows(0).Item("ITEM_TITLE").ToString())
                pParms(2) = New SqlClient.SqlParameter("@AUTHOR", ds.Tables(0).Rows(0).Item("AUTHOR").ToString())
                pParms(3) = New SqlClient.SqlParameter("@PUBLISHER", ds.Tables(0).Rows(0).Item("PUBLISHER").ToString())
                pParms(4) = New SqlClient.SqlParameter("@DATE_YEAR", ds.Tables(0).Rows(0).Item("DATE_YEAR").ToString())
                pParms(5) = New SqlClient.SqlParameter("@ISBN", ds.Tables(0).Rows(0).Item("ISBN").ToString())
                pParms(6) = New SqlClient.SqlParameter("@ISBN_ENC", ds.Tables(0).Rows(0).Item("ISBN_ENC").ToString())
                pParms(7) = New SqlClient.SqlParameter("@FORMAT", ds.Tables(0).Rows(0).Item("FORMAT").ToString())
                pParms(8) = New SqlClient.SqlParameter("@SUBJECT", ds.Tables(0).Rows(0).Item("SUBJECT").ToString())
                pParms(9) = New SqlClient.SqlParameter("@PRODUCT_URL", ds.Tables(0).Rows(0).Item("PRODUCT_URL").ToString())
                pParms(10) = New SqlClient.SqlParameter("@PRODUCT_IMAGE_URL", ds.Tables(0).Rows(0).Item("PRODUCT_IMAGE_URL").ToString())
                pParms(11) = New SqlClient.SqlParameter("@PRODUCT_DESCRIPTION", ds.Tables(0).Rows(0).Item("PRODUCT_DESCRIPTION").ToString())
                pParms(12) = New SqlClient.SqlParameter("@PRODUCT_PRICE", ds.Tables(0).Rows(0).Item("PRODUCT_PRICE").ToString())
                pParms(13) = New SqlClient.SqlParameter("@PRICE_CURRENCY", ds.Tables(0).Rows(0).Item("PRICE_CURRENCY").ToString())
                pParms(14) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", ds.Tables(0).Rows(0).Item("ENTRY_BSU_ID").ToString())
                pParms(15) = New SqlClient.SqlParameter("@QUANTITY", ds.Tables(0).Rows(0).Item("TOTAL_STOCK").ToString())
                pParms(16) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", ds.Tables(0).Rows(0).Item("ENTRY_EMP_ID").ToString())
                pParms(17) = New SqlClient.SqlParameter("@FROM_AGE_GROUP", ds.Tables(0).Rows(0).Item("FROM_AGE_GROUP").ToString())
                pParms(18) = New SqlClient.SqlParameter("@TO_AGE_GROUP", ds.Tables(0).Rows(0).Item("TO_AGE_GROUP").ToString())
                pParms(19) = New SqlClient.SqlParameter("@SUPPLIER", ds.Tables(0).Rows(0).Item("SUPPLIER").ToString())
                pParms(20) = New SqlClient.SqlParameter("@PURCHASE_DATE", ds.Tables(0).Rows(0).Item("PURCHASE_DATE").ToString())

                Dim accessno = ds.Tables(0).Rows(0).Item("ACCESSION_NO").ToString().Trim()
                Dim callno = ds.Tables(0).Rows(0).Item("CALL_NO").ToString().Trim()
                Dim quantity = ds.Tables(0).Rows(0).Item("TOTAL_STOCK").ToString().Trim()

                If accessno <> "" And quantity = "1" And IsNumeric(quantity) Then
                    pParms(21) = New SqlClient.SqlParameter("@ACCESSION_NO", accessno)
                End If
                If callno <> "" Then
                    pParms(22) = New SqlClient.SqlParameter("@CALL_NO", callno)
                End If

                If ds.Tables(0).Rows(0).Item("GOOGLE_BOOKS").ToString() = "True" Then
                    pParms(23) = New SqlClient.SqlParameter("@ENTRY_TYPE", "Google Book-Provider (Excel)")
                Else
                    pParms(23) = New SqlClient.SqlParameter("@ENTRY_TYPE", "Manaul")
                End If


                Dim masterid = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_LIBRARY_ITEMS_MASTER", pParms)

                ''Update the Approved Master Id
                sql_query = "UPDATE LIBRARY_ITEMS_MASTER_EXCEL SET APPROVED_MASTER_ID='" & masterid & "' where  RECORD_ID='" & Recordid & "'"
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)

            End If

        End If


    End Sub

    Public Function CheckItemExists(ByVal ISBN As String, ByVal transaction As SqlTransaction, ByVal RECORD_ID As String, ByVal googlebook As Boolean) As Boolean
        Dim returnval = False

        If googlebook Then
            Dim Sql_Query = "Select MASTER_ID from LIBRARY_ITEMS_MASTER where ISBN like '%" & ISBN & "%'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, Sql_Query)
            If ds.Tables(0).Rows.Count > 0 Then
                returnval = True
                Dim masterid = ds.Tables(0).Rows(0).Item("MASTER_ID").ToString()
                Sql_Query = "Update LIBRARY_ITEMS_MASTER_EXCEL set PRIMARY_MASTER_ID='" & masterid & "' WHERE RECORD_ID='" & RECORD_ID & "' "
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, Sql_Query)
                UpdateStocks(RECORD_ID, masterid, transaction)
            End If
        End If

        Return returnval
    End Function

    Public Function CheckPreviousDataISBN(ByVal ISBNCombination As String, ByVal transaction As SqlTransaction) As String


        Dim Returnvalue = ""

        If ISBNCombination <> "" Then

            Dim val = ISBNCombination
            Dim isbn As String()
            isbn = Regex.Split(val, "ISBN")

            Dim condition = ""
            Dim flag = 0

            Dim i = 0
            For i = 0 To isbn.Length - 1

                If isbn(i).Trim() <> "" Then
                    If flag = 0 Then
                        condition = "ISBN Like '%ISBN" & isbn(i).Trim().Replace(",", "") & "%'"
                        flag = 1
                    Else
                        condition = condition & " OR ISBN Like '%ISBN" & isbn(i).Trim().Replace(",", "") & "%'"
                    End If


                End If
            Next

            If condition <> "" Then
                Dim Sql_Query = "SELECT Top 1 MASTER_ID from LIBRARY_ITEMS_MASTER WHERE " & condition
                Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, Sql_Query)
                If ds.Tables(0).Rows.Count > 0 Then
                    Returnvalue = ds.Tables(0).Rows(0).Item("MASTER_ID").ToString()

                    '' Update Excel table for non approved books

                    Sql_Query = " UPDATE LIBRARY_ITEMS_MASTER_EXCEL SET PRIMARY_MASTER_ID='" & Returnvalue & "' WHERE " & condition & " AND APPROVED='False'"
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, Sql_Query)

                Else
                    Returnvalue = ""
                End If

            Else
                Returnvalue = ""
            End If
        Else
            Returnvalue = ""

        End If

        Return Returnvalue

    End Function

    Protected Sub DropDataRetv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDataRetv.SelectedIndexChanged
        BindGrid()
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click

        Dim dt As DataTable

        dt = Getdata().Tables(0)

        dt.Columns.Remove("RECORD_ID")
        dt.Columns.Remove("PRODUCT_IMAGE_URL")
        dt.Columns.Remove("ERROR_UPLOAD")
        dt.Columns.Remove("ERROR_DATA_RETRIEVE")
        dt.Columns.Remove("APPROVED_IMG")
        dt.Columns.Remove("STARTED")
        dt.Columns.Remove("STARTED_STATUS")
        dt.Columns.Remove("DELETING")
        dt.Columns.Remove("ALLOW_ENTRY")



        ' Create excel file.


        SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        Dim ef As ExcelFile = New ExcelFile

        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, "A1", True)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("UploadExcelFile").ToString()

        Dim pathSave As String
        pathSave = Session("EmployeeId") + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"

        ef.SaveXls(cvVirtualPath & "GroupMembers\" + pathSave)

        Dim path = cvVirtualPath & "\GroupMembers\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)

        Response.Flush()

        Response.End()
        'HttpContext.Current.Response.ContentType = "application/octect-stream"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(path)
        'HttpContext.Current.Response.End()

    End Sub

    Protected Sub ddattempt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddattempt.SelectedIndexChanged
        BindGrid()
    End Sub
End Class
