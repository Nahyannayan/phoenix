Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryQuickReturnTransactions
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            HiddenEmpid.Value = Session("EmployeeId")
            BindLibraryDivisions()
            BindItemStatus()
            BindCurrency()
        End If

    End Sub

    Public Sub BindLibraryDivisions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim str_query = ""
        If Library.LibrarySuperAcess(Session("EmployeeId")) Then

            str_query = "SELECT LIBRARY_DIVISION_ID,LIBRARY_DIVISION_DES FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES "
        Else
            str_query = " SELECT B.LIBRARY_DIVISION_ID,B.LIBRARY_DIVISION_DES FROM dbo.LIBRARY_LIBRARIAN A " & _
                                  " INNER JOIN dbo.LIBRARY_DIVISIONS B ON A.LIBRARY_DIVISION_ID=B.LIBRARY_DIVISION_ID " & _
                                  " WHERE LIBRARIAN_EMP_ID='" & HiddenEmpid.Value & "' AND ACTIVE='True' AND LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' "


        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddLibrary.DataSource = ds
        ddLibrary.DataTextField = "LIBRARY_DIVISION_DES"
        ddLibrary.DataValueField = "LIBRARY_DIVISION_ID"
        ddLibrary.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows.Count > 1 Then
                Dim list As New ListItem
                list.Value = "0"
                list.Text = "Select a library"
                ddLibrary.Items.Insert(0, list)
            Else

                Call ddLibrary_SelectedIndexChanged(ddLibrary, Nothing)

            End If
        Else
            lblMessage.Text = "Login User is not a librarian. Please assign librarian and proceed further. "
            ddLibrary.Visible = False
            MO1.Show()
        End If

    End Sub

    Public Sub BindItemStatus()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim ds As DataSet
        Dim str_query = " SELECT STATUS_ID,STATUS_DESCRIPTION FROM LIBRARY_ITEM_STATUS WHERE (STATUS_ID != 4 AND CIRCULATORY='True') " & _
                    " UNION SELECT STATUS_ID,STATUS_DESCRIPTION FROM LIBRARY_ITEM_STATUS WHERE STATUS_ID=6 " '' Lost Status
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddreturnstatusentry.DataSource = ds
        ddreturnstatusentry.DataTextField = "STATUS_DESCRIPTION"
        ddreturnstatusentry.DataValueField = "STATUS_ID"
        ddreturnstatusentry.DataBind()

        Dim list2 As New ListItem
        list2.Text = "Present Status"
        list2.Value = "0"
        ddreturnstatusentry.Items.Insert(0, list2)

    End Sub

    Public Sub BindCurrency()
        Dim str_conn = ConfigurationManager.ConnectionStrings("MainDB").ConnectionString
        Dim str_query = "SELECT CUR_ID,upper(CUR_DESCR)CUR_DESCR FROM CURRENCY_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddReturncurrency.DataSource = ds
        ddReturncurrency.DataTextField = "CUR_DESCR"
        ddReturncurrency.DataValueField = "CUR_ID"
        ddReturncurrency.DataBind()

    End Sub

    Public Sub BindReturnInfoDetails()
        HiddenRecordID.Value = ""

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = ""
        Dim ds As DataSet

        ''Check if the Item belongs to selected library.
        Sql_Query = " SELECT * FROM dbo.LIBRARY_ITEMS_QUANTITY A " & _
                    " INNER JOIN dbo.LIBRARY_RACKS B ON A.RACK_ID=B.RACK_ID " & _
                    " INNER JOIN dbo.LIBRARY_SHELFS C ON B.SHELF_ID=C.SHELF_ID " & _
                    " INNER JOIN dbo.LIBRARY_SUB_DIVISIONS D ON C.LIBRARY_SUB_DIVISION_ID= D.LIBRARY_SUB_DIVISION_ID " & _
                    " INNER JOIN dbo.LIBRARY_DIVISIONS E ON D.LIBRARY_DIVISION_ID= E.LIBRARY_DIVISION_ID " & _
                    " WHERE CONVERT(VARCHAR,A.STOCK_ID)='" & LibraryData.GetStockIDForAccessonNo(txtreturnstockid.Text.Trim(), HiddenBsuID.Value) & "' AND E.LIBRARY_DIVISION_ID='" & Hiddenlibrarydivid.Value & "' "

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)


        If ds.Tables(0).Rows.Count > 0 Then

            Sql_Query = "SELECT USER_TYPE FROM  LIBRARY_TRANSACTIONS WHERE STOCK_ID='" & LibraryData.GetStockIDForAccessonNo(txtreturnstockid.Text.Trim(), HiddenBsuID.Value) & "' AND ISNULL(ITEM_ACTUAL_RETURN_DATE,'')= ''"
            Dim UserType = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)
            Dim UserNo = ""
            If UserType = "STUDENT" Then

                Sql_Query = " SELECT D.RECORD_ID,STOCK_ID,D.USER_TYPE,LIBRARY_DIVISION_DES,MEMBERSHIP_DES,STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')),  " & _
                                " 'javascript:UserTransactions(''' + D.USER_TYPE + ''','''+ CONVERT(VARCHAR,STU_ID)  +'''); return false;' USERREDIRECT, " & _
                                        " ITEM_TAKEN_DATE, CONVERT(VARCHAR(20), ITEM_RETURN_DATE , 100) as ITEM_RETURN_DATE ,STATUS_DESCRIPTION,NOTES, " & _
                                        " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR " & _
                                        " FROM oasis.dbo.STUDENT_M AS A " & _
                                        " INNER JOIN oasis.dbo.GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID " & _
                                        " INNER JOIN oasis.dbo.SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " & _
                                        " INNER JOIN dbo.LIBRARY_TRANSACTIONS D ON A.STU_ID=D.USER_ID AND ISNULL(ITEM_ACTUAL_RETURN_DATE,'')='' AND STOCK_ID='" & LibraryData.GetStockIDForAccessonNo(txtreturnstockid.Text.Trim(), HiddenBsuID.Value) & "' " & _
                                        " INNER JOIN dbo.LIBRARY_MEMBERSHIPS E ON E.MEMBERSHIP_ID=D.MEMBERSHIP_ID " & _
                                        " INNER JOIN dbo.LIBRARY_DIVISIONS F ON F.LIBRARY_DIVISION_ID=E.LIBRARY_DIVISION_ID " & _
                                        " LEFT JOIN dbo.LIBRARY_ITEM_STATUS G ON G.STATUS_ID=D.ITEM_BEFORE_STATUS_ID "

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

                If ds.Tables(0).Rows.Count > 0 Then
                    lblReturnUserDetails.Text = "User No    : " & ds.Tables(0).Rows(0).Item("STU_NO").ToString() & "<br>" & _
                                                "Name       : " & ds.Tables(0).Rows(0).Item("STU_NAME").ToString() & "<br>" & _
                                                "Grade      : " & ds.Tables(0).Rows(0).Item("GRM_DISPLAY").ToString() & "<br>" & _
                                                "Section    : " & ds.Tables(0).Rows(0).Item("SCT_DESCR").ToString() & "<br>"

                    lblReturnMembershipType.Text = ds.Tables(0).Rows(0).Item("MEMBERSHIP_DES").ToString()
                    lblReturnTakenon.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ITEM_TAKEN_DATE")).ToString("dd/MMM/yyyy HH:mm tt")
                    lblReturnReturnDate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ITEM_RETURN_DATE")).ToString("dd/MMM/yyyy")
                    lblreturnIssueStatus.Text = ds.Tables(0).Rows(0).Item("STATUS_DESCRIPTION").ToString()
                    lblreturnIssueNotes.Text = ds.Tables(0).Rows(0).Item("NOTES").ToString()
                    HiddenRecordID.Value = ds.Tables(0).Rows(0).Item("RECORD_ID").ToString()
                    LinkReturnHistory.OnClientClick = ds.Tables(0).Rows(0).Item("USERREDIRECT").ToString()
                    UserNo = ds.Tables(0).Rows(0).Item("STU_NO").ToString()
                    HiddenAccessionReserve.Value = txtreturnstockid.Text.Trim()
                End If

                Panel3.Visible = True
                Panel2.Visible = False

            ElseIf UserType = "EMPLOYEE" Then

                Sql_Query = " select EMP_ID,EMPNO, isnull(EMP_FNAME,'')+' ' +isnull(EMP_MNAME,'')+' '+isnull(EMP_LNAME,'') ENAME, DES_DESCR,EMP_DES_ID, " & _
                                " 'javascript:UserTransactions(''' + D.USER_TYPE + ''','''+ CONVERT(VARCHAR,EMP_ID)  +'''); return false;' USERREDIRECT " & _
                                " ,MEMBERSHIP_DES,ITEM_TAKEN_DATE,ITEM_RETURN_DATE,STATUS_DESCRIPTION,NOTES,RECORD_ID  from OASIS.dbo.EMPLOYEE_M a  " & _
                                " inner join  OASIS.dbo.EMPDESIGNATION_M b on a.EMP_DES_ID=b.DES_ID " & _
                                " INNER JOIN dbo.LIBRARY_TRANSACTIONS D ON A.EMP_ID=D.USER_ID AND ISNULL(ITEM_ACTUAL_RETURN_DATE,'')='' AND STOCK_ID='" & LibraryData.GetStockIDForAccessonNo(txtreturnstockid.Text.Trim(), HiddenBsuID.Value) & "' " & _
                                " INNER JOIN dbo.LIBRARY_MEMBERSHIPS E ON E.MEMBERSHIP_ID=D.MEMBERSHIP_ID " & _
                                " INNER JOIN dbo.LIBRARY_DIVISIONS F ON F.LIBRARY_DIVISION_ID=E.LIBRARY_DIVISION_ID  " & _
                                " LEFT JOIN dbo.LIBRARY_ITEM_STATUS G ON G.STATUS_ID=D.ITEM_BEFORE_STATUS_ID "

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

                If ds.Tables(0).Rows.Count > 0 Then
                    lblReturnUserDetails.Text = "User No    : " & ds.Tables(0).Rows(0).Item("EMPNO").ToString() & "<br>" & _
                                                "Name       : " & ds.Tables(0).Rows(0).Item("ENAME").ToString() & "<br>" & _
                                                "Designation:  " & ds.Tables(0).Rows(0).Item("DES_DESCR").ToString() & "<br>"

                    lblReturnMembershipType.Text = ds.Tables(0).Rows(0).Item("MEMBERSHIP_DES").ToString()
                    lblReturnTakenon.Text = ds.Tables(0).Rows(0).Item("ITEM_TAKEN_DATE").ToString()
                    lblReturnReturnDate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ITEM_RETURN_DATE")).ToString("dd/MMM/yyyy")
                    lblreturnIssueStatus.Text = ds.Tables(0).Rows(0).Item("STATUS_DESCRIPTION").ToString()
                    lblreturnIssueNotes.Text = ds.Tables(0).Rows(0).Item("NOTES").ToString()
                    HiddenRecordID.Value = ds.Tables(0).Rows(0).Item("RECORD_ID").ToString()
                    LinkReturnHistory.OnClientClick = ds.Tables(0).Rows(0).Item("USERREDIRECT").ToString()
                    UserNo = ds.Tables(0).Rows(0).Item("EMPNO").ToString()
                End If

                Panel3.Visible = True
                Panel2.Visible = False


            ElseIf UserType = "" Then

                lblMessage.Text = "This Item not been issued to any user."
                MO1.Show()

            End If
            HiddenUserType.Value = UserType
            HiddenUserNo.Value = UserNo
            checkPouch(HiddenUserType.Value, HiddenUserNo.Value)

        Else
            lblMessage.Text = "This Item does not belong to this library division. Please check the Accession No and rack has been assigned."
            MO1.Show()
        End If

    End Sub

    Public Sub BindPouchItems(ByVal UserType As String, ByVal User_no As String, Optional ByVal showmessage As Boolean = True)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim ds As DataSet
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STOCK_ID_POUCH_ID", LibraryData.GetStockIDForAccessonNo(HiddenAccessionNo.Value, HiddenBsuID.Value))
        pParms(1) = New SqlClient.SqlParameter("@USER_TYPE", UserType)
        pParms(2) = New SqlClient.SqlParameter("@USER_NO", User_no)
        pParms(3) = New SqlClient.SqlParameter("@OPTION", 22)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "LIB_POUCH_TRAN", pParms)
        GridItem.DataSource = ds
        GridItem.DataBind()
        GridItem.Visible = True
        'If showmessage Then
        '    If ds.Tables(0).Rows.Count = 0 Then
        '        lblMessage.Text = "No available items in pouch/box to issue. Please check the transaction history of pouch and each items in pouch."
        '        MO1.Show()
        '    End If
        'End If

    End Sub
    Public Function checkPouch(ByVal UserType As String, ByVal User_no As String) As Boolean
        Dim rval = False
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = "select * from dbo.LIBRARY_ITEMS_QUANTITY where STOCK_ID_POUCH_ID=" & LibraryData.GetStockIDForAccessonNo(txtreturnstockid.Text.Trim(), HiddenBsuID.Value) & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        If ds.Tables(0).Rows.Count > 0 Then
            HiddenAccessionNo.Value = txtreturnstockid.Text.Trim()
            BindPouchItems(UserType, User_no)
            rval = True
        Else
            HiddenAccessionNo.Value = ""
            GridItem.Visible = False
        End If

        Return rval
    End Function

    'Public Sub btnReturnPouch_Click()

    'End Sub


    Protected Sub ddLibrary_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddLibrary.SelectedIndexChanged
        If ddLibrary.SelectedValue <> "0" Then
            txtreturnstockid.Focus()
            Hiddenlibrarydivid.Value = ddLibrary.SelectedValue
            Panel1.Visible = False
            Panel2.Visible = True
        End If
    End Sub

    Protected Sub btnmessageok_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO1.Hide()
    End Sub

    Public Sub ReturnTran(ByVal tran_record_id As String, ByVal accessionnumber As String)

        Dim Hash As New Hashtable
        Hash.Add("HiddenRecordID", tran_record_id)
        Hash.Add("ddreturnstatusentry", ddreturnstatusentry.SelectedValue)
        Hash.Add("txtReturnnotes", txtReturnnotes.Text.Trim())
        Hash.Add("txtreturnfineamount", txtreturnfineamount.Text.Trim())
        Hash.Add("ddReturncurrency", ddReturncurrency.SelectedValue)
        Hash.Add("HiddenEmpid", HiddenEmpid.Value)
        Hash.Add("HiddenBsuID", HiddenBsuID.Value)
        Hash.Add("txtreturnstockid", accessionnumber)

        lblMessage.Text = LibraryTransactions.ReturnItems(Hash)

    End Sub


    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturn.Click

        For Each row As GridViewRow In GridItem.Rows

            If DirectCast(row.FindControl("ch1"), CheckBox).Checked Then
                ReturnTran(DirectCast(row.FindControl("HiddenTranRecordid"), HiddenField).Value, DirectCast(row.FindControl("HiddenStockId"), HiddenField).Value)
            End If

        Next
        checkPouch(HiddenUserType.Value, HiddenUserNo.Value)

        If GridItem.Rows.Count = 0 Then

            '' If one record or pouch items all returned by the user
            ReturnTran(HiddenRecordID.Value, txtreturnstockid.Text.Trim())
            txtReturnnotes.Text = ""
            txtreturnfineamount.Text = 0
            'Panel3.Visible = False
            'Panel2.Visible = True
            txtreturnstockid.Text = ""


        End If
        libraryTransactionReservationApproval1.AccesionNo = txtreturnstockid.Text
        MO1.Show()

    End Sub

    Protected Sub NextItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles NextItem.Click
        GridItem.DataSource = Nothing
        GridItem.DataBind()
        GridItem.Visible = False
        HiddenAccessionNo.Value = ""
        HiddenUserType.Value = ""
        Panel3.Visible = False
        Panel2.Visible = True
        txtreturnstockid.Text = ""
        txtreturnstockid.Focus()
    End Sub

    Protected Sub btnreturnok_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreturnok.Click

        libraryTransactionReservationApproval1.AccesionNo = txtreturnstockid.Text

        BindReturnInfoDetails()


    End Sub

End Class
