<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryEditPrimaryItemDetails.ascx.vb" Inherits="Library_UserControls_libraryEditPrimaryItemDetails" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>



<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


<script type="text/javascript">



    function OnTreeClick(evt) {
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");

        if (isChkBoxClick) {
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid != event.srcElement.id) {
                    document.forms[0].elements[i].checked = false;
                }
            }
        }

    }


</script>


<div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr class="title-bg-lite">
                    <td colspan="2">Primary Informations</td>
                    <td align="center">
                        <center>Item Image</center>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Master ID</span></td>
                    <td>
                        <asp:Label ID="lblMasterId" runat="server" CssClass="field-value"></asp:Label></td>
                    <td align="center">
                        <asp:Image ID="Image1" runat="server" /></td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Entry Type</span></td>
                    <td>
                        <asp:Label ID="lblEntryType" runat="server" CssClass="field-value"></asp:Label></td>
                    <td align="center">
                        <span class="field-label">Other GEMS School (s)</span></td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Item Title</span></td>
                    <td>
                        <asp:TextBox ID="txttitle" runat="server" EnableTheming="False"></asp:TextBox></td>
                    <td align="center" rowspan="10" valign="top">
                        <asp:GridView ID="GridShared" runat="server" AutoGenerateColumns="false" EmptyDataText="Info not been shared." CssClass="table table-bordered table-row">
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                      BSU
                                              
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%# Eval("BSU_SHORTNAME") %></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Copies
                                              
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                            <asp:Label ID="lblcount" runat="server" ToolTip='<%# Eval("INFO") %>' Text='<%# Eval("STOCK_COUNT") %>'></asp:Label> 
                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- <asp:TemplateField>
                <HeaderTemplate>
                <table class="BlueTable" width="100%">
                     <tr class="matterswhite">
                               <td align="center" colspan="2">
                                 Info
                               </td>
                      </tr>
                 </table>
                </HeaderTemplate>
                    <ItemTemplate>
                        <center>
                           <%# Eval("INFO") %>
                        </center>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                            </Columns>
                            <SelectedRowStyle  />
                            <HeaderStyle  />
                            <EditRowStyle  />
                            <AlternatingRowStyle CssClass="griditem_alternative"  />
                        </asp:GridView>

                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Author</span></td>
                    <td>
                        <asp:TextBox ID="txtauthor" runat="server" EnableTheming="False" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Publisher</span></td>
                    <td>
                        <asp:TextBox ID="txtpublisher" runat="server" EnableTheming="False" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Date/Year</span></td>
                    <td>
                        <asp:TextBox ID="txtdateyear" runat="server" EnableTheming="False" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">ISBN</span></td>
                    <td>
                        <asp:TextBox ID="txtisbndata" runat="server" EnableTheming="False" ValidationGroup="MEntry"
                            Width="195px"></asp:TextBox><asp:LinkButton ID="lnkIsbnAdd" runat="server" CausesValidation="False"
                                OnClick="lnkIsbnAdd_Click">Add</asp:LinkButton>
                        <asp:GridView ID="GrdISBN" runat="server" AutoGenerateColumns="false" OnRowDeleting="GrdISBN_RowDeleting"
                            ShowHeader="false"  CssClass="table table-bordered table-row">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="lblData" runat="server" Text='<%# Eval("EDIT_ISBN") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <center>
                                            <asp:LinkButton ID="LinkDelete" runat="server" CausesValidation="false" CommandName="Delete">Delete</asp:LinkButton>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                            <EmptyDataRowStyle Wrap="False" />
                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                            <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                            <EditRowStyle Wrap="False" />
                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                        </asp:GridView>
                        <ajaxToolkit:FilteredTextBoxExtender ID="F4ISBN" runat="server" FilterType="Custom, Numbers, UppercaseLetters"
                            TargetControlID="txtisbndata">
                        </ajaxToolkit:FilteredTextBoxExtender>
                        Caps Characters and Numbers Only.<br />
                        Please enter only valid ISBN of a book. (ISBN can be 10 or 13 digit characters,
            its unique for a book title.)</td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">No. of Pages</span></td>
                    <td>
                        <asp:TextBox ID="txtpages" runat="server" EnableTheming="False" Width="195px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Subjects</span></td>
                    <td>
                        <asp:TextBox ID="txtSubjects" runat="server" EnableTheming="False" ValidationGroup="MEntry"></asp:TextBox>
                        <asp:LinkButton ID="lnkSubjectAdd" runat="server" CausesValidation="False"
                                OnClick="lnkSubjectAdd_Click">Add</asp:LinkButton><br />
                        <asp:Panel ID="Panel1" runat="server" Height="100px" Width="530px" ScrollBars="Auto">
                            <asp:GridView ID="GrdSubjects" runat="server" AutoGenerateColumns="false" OnRowDeleting="GrdSubjects_RowDeleting"
                                ShowHeader="false" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="lblData" runat="server" Text='<%# Eval("EDIT_LBSUBJECTS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <center>
                                            <asp:LinkButton ID="LinkDelete" runat="server" CausesValidation="false" CommandName="Delete">Delete</asp:LinkButton>
                                        </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem"  />
                                <EmptyDataRowStyle  />
                                <SelectedRowStyle />
                                <HeaderStyle  />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </asp:Panel>
                        <ajaxToolkit:FilteredTextBoxExtender ID="F5SUBJECTS" runat="server" FilterMode="InvalidChars"
                            InvalidChars="," TargetControlID="txtSubjects">
                        </ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Item Description</span></td>
                    <td>
                        <asp:TextBox ID="txtdesc" runat="server" EnableTheming="False" Height="142px" TextMode="MultiLine"
                            Width="527px"></asp:TextBox></td>
                </tr>

                <tr>
                    <td>
                        <span class="field-label">E-Book Link</span></td>
                    <td>
                        <asp:TextBox ID="txtelink" runat="server" Width="500px"></asp:TextBox></td>
                </tr>


                <tr>
                    <td>
                        <span class="field-label">Item Type</span></td>
                    <td>
                        <div class="checkbox-list">
                            <asp:TreeView ID="TreeItemCategory" runat="server" ImageSet="Arrows"
                                onclick="OnTreeClick(event);" ShowCheckBoxes="All">
                                <ParentNodeStyle Font-Bold="False" />
                                <HoverNodeStyle Font-Underline="True" />
                                <SelectedNodeStyle Font-Underline="True"
                                    HorizontalPadding="0px" VerticalPadding="0px" />
                                <NodeStyle HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />
                            </asp:TreeView>
                        </div>
                        <asp:LinkButton ID="lnkUpdateType" runat="server">Update Item Type</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center">
                        <asp:Button ID="btnUpdate" runat="server" CssClass="button"
                            OnClick="btnUpdate_Click" Text="Update" Width="86px" />
                    </td>
                </tr>
            </table>
            <asp:Label ID="Label1" runat="server"></asp:Label><br />
            <asp:Panel ID="PanelStatusupdate" runat="server" CssClass="panel-cover" Style="display: none" Width="100%">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite">Library Message</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <asp:Label ID="lblMessage" runat="server" CssClass="text-danger"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Button ID="btnmok" runat="server" CssClass="button" Text="Ok" ValidationGroup="s" CausesValidation="False" /></td>
                                        </tr>
                                    </table>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
                DropShadow="true" CancelControlID="btnmok" PopupControlID="PanelStatusupdate"
                RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="Label1">
            </ajaxToolkit:ModalPopupExtender>

            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txttitle"
                Display="None" ErrorMessage="Please Enter Title of an Item" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                    ShowSummary="False" />
            <br />
            <asp:HiddenField ID="HiddenBsuID" runat="server" />
            <asp:HiddenField ID="HiddenMasterId" runat="server" />

        </ContentTemplate>
    </asp:UpdatePanel>
</div>
