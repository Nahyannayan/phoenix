﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class Library_UserControls_libraryStockList
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            BindLibraryDivisions(ddLibraryDivisions)
            bindgrid()
        End If
    End Sub


    Protected Sub GridData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridData.RowCommand
        If e.CommandName = "ExportReport" Then
            Export(e.CommandArgument, "R")
        ElseIf e.CommandName = "ExportPending" Then
            Export(e.CommandArgument, "P")
        End If
    End Sub
    Public Sub Export(ByVal STOCK_TAKE_ID As String, ByVal DataOption As String)
        Dim dt As New DataTable()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "INSERT_UPDATE_LIBRARY_STOCK_TAKE_LIST"
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STOCK_TAKE_ID", STOCK_TAKE_ID)
        If DataOption = "R" Then
            pParms(1) = New SqlClient.SqlParameter("@OPTION", 4)
        ElseIf DataOption = "P" Then
            pParms(1) = New SqlClient.SqlParameter("@OPTION", 44)
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, str_query, pParms)



        dt = ds.Tables(0)
        ' Create excel file.
        'SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, "A1", True)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()

        Dim pathSave As String
        pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xls"

        ef.SaveXls(cvVirtualPath & "StaffExport\" + pathSave)

        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)

        Response.Flush()

        Response.End()
        'HttpContext.Current.Response.ContentType = "application/octect-stream"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(path)
        'HttpContext.Current.Response.End()


    End Sub
    Public Sub BindLibraryDivisions(ByVal ddLibraryDivisions As DropDownList)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddLibraryDivisions.DataSource = ds
            ddLibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
            ddLibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
            ddLibraryDivisions.DataBind()
        End If

        Dim list As New ListItem
        list.Text = "Select a Library Divison"
        list.Value = "-1"
        ddLibraryDivisions.Items.Insert(0, list)



    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STOCK_TAKE_NAME", txtstockname.Text.Trim())
            pParms(1) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", ddLibraryDivisions.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
            pParms(3) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", HiddenBsuID.Value)
            pParms(4) = New SqlClient.SqlParameter("@OPTION", 1)

            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_UPDATE_LIBRARY_STOCK_TAKE_LIST", pParms)
            transaction.Commit()
            bindgrid()

        Catch ex As Exception
            transaction.Rollback()
        Finally
            connection.Close()

        End Try

    End Sub


    Public Sub bindgrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "INSERT_UPDATE_LIBRARY_STOCK_TAKE_LIST"
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", HiddenBsuID.Value)
        pParms(1) = New SqlClient.SqlParameter("@OPTION", 2)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, str_query, pParms)
        GridData.DataSource = ds
        GridData.DataBind()


    End Sub

    Protected Sub GridData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridData.PageIndexChanging
        GridData.PageIndex = e.NewPageIndex
        bindgrid()
    End Sub
End Class
