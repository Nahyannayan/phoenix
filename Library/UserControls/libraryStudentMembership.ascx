<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryStudentMembership.ascx.vb"
    Inherits="Library_UserControls_libraryStudentMembership" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<script type="text/javascript">
    function change_chk_stateg1(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("CheckBar1") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }

</script>
<div class="matters">
    <table width="100%">
        <tr>
            <td><span class="field-label">Business Unit</span> </td>
            <td>
                <asp:DropDownList
                    ID="ddbsu" runat="server" AutoPostBack="True">
                </asp:DropDownList>

            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GrdStudents" AutoGenerateColumns="False" Width="100%" EmptyDataText="No record found for selected search" CssClass="table table-bordered table-row"
                    runat="server" ShowFooter="True" AllowPaging="True" PageSize="15">
                    <Columns>
                        <asp:TemplateField HeaderText="Barcode">
                            <HeaderTemplate>
                                Barcode
                                            <br />
                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg1(this);" ToolTip="Click here to select/deselect all rows" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBar1" runat="server" />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:CheckBox ID="CheckIssueAll" Text="<br>Issue All" runat="server" />
                                <br />
                                <asp:Button ID="btnbarcode" runat="server" CssClass="button" CommandName="barcode" Text="Issue" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Student Number">
                            <HeaderTemplate>
                                Student Number
                                            <br />
                                <asp:TextBox ID="txtnumber" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch1" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddensStuid" Value='<%#Eval("stu_id")%>' runat="server" />
                                <%#Eval("stu_no")%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student Name">
                            <HeaderTemplate>
                                Student Name
                                            <br />
                                <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch2" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("STU_NAME")%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                            <HeaderStyle HorizontalAlign="left" VerticalAlign="Middle" />
                            <FooterStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Grade">
                            <HeaderTemplate>
                                Grade
                                            <br />
                                <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch3" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%#Eval("GRM_DISPLAY")%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Section">
                            <HeaderTemplate>
                                Section
                                            <br />
                                <asp:TextBox ID="txtSection" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch4" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%#Eval("SCT_DESCR")%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Assign">
                            <HeaderTemplate>
                                Assign
                            </HeaderTemplate>
                            <FooterTemplate>
                                <asp:Button ID="btnSave" runat="server" CssClass="button" CommandName="save" Text="Assign" />
                            </FooterTemplate>
                            <ItemTemplate>

                                <asp:GridView ID="GridMemberships" runat="server" Width="100%" AutoGenerateColumns="False"
                                    OnRowCommand="GridMemberships_RowCommand" ShowHeader="False">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:HiddenField ID="HiddenRecordId" Value='<%#Eval("RECORD_ID")%>' runat="server" />
                                                <%#Eval("LIBRARY_DIVISION_DES")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%#Eval("MEMBERSHIP_DES")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkDelete" CommandName="Deleting" CommandArgument='<%#Eval("RECORD_ID")%>'
                                                    runat="server">Delete</asp:LinkButton>
                                                <ajaxToolkit:ConfirmButtonExtender ID="CF1" TargetControlID="LinkDelete" ConfirmText="Do you want to delete this record?"
                                                    runat="server">
                                                </ajaxToolkit:ConfirmButtonExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <br />
                                <asp:CheckBox ID="Check" runat="server" />

                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
                <br />
                <div align="center">
                    <asp:Button ID="btnupdatebulk" runat="server" CssClass="button" Text="Bulk Update" />
                </div>

                <ajaxToolkit:ConfirmButtonExtender ID="C1" TargetControlID="btnupdatebulk" ConfirmText="According to search conditions students are listed above in the grid.Do you need to assign library memebership for these students ?. Click OK to continue." runat="server"></ajaxToolkit:ConfirmButtonExtender>
            </td>
        </tr>
    </table>
    <asp:Label ID="Label1" runat="server"></asp:Label>
    <asp:Panel ID="PanelMessage" runat="server" BackColor="white" CssClass="panel-cover" Width="100%"
        Style="display: none">
        <table width="100%">
            <tr>
                <td class="title-bg">Library Message</td>
            </tr>
            <tr>
                <td align="center">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <table>
                                <tbody>
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnmok" OnClick="btnmessageok_Click" runat="server" Text="Ok" CssClass="button"
                                                ValidationGroup="s" CausesValidation="False"></asp:Button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
        DropShadow="true" PopupControlID="PanelMessage" RepositionMode="RepositionOnWindowResizeAndScroll"
        TargetControlID="Label1">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Label ID="Label3" runat="server"></asp:Label>
    <asp:Panel ID="PanelStatusUpdate" runat="server" Width="100%" BackColor="white" CssClass="panel-cover"
        Style="display: none">
        <table width="100%">
            <tr>
                <td class="title-bg">Library Message-Memberships</td>
            </tr>
            <tr>
                <td align="center">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <table style="width: 100%">
                                <tr>
                                    <td align="center">
                                        <table>
                                            <tr>
                                                <td align="left"><span class="field-label">Library Divisions</span></td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlibraryDivisions" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlibraryDivisions_SelectedIndexChanged" Width="100%">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <span class="field-label">Membership</span></td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddMemberships" runat="server" Width="100%">
                                                    </asp:DropDownList></td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnStatusupdate" runat="server" CausesValidation="False" CssClass="button"
                                            OnClick="btnStatusupdate_Click" Visible="false" Text="Update" /><asp:Button ID="btnStatusupdate2" runat="server" CausesValidation="False" CssClass="button"
                                                OnClick="btnStatusupdate2_Click" Visible="false" Text="Bulk Update" /><asp:Button ID="btncancel4"
                                                    runat="server" CausesValidation="False" CssClass="button" OnClick="btncancel4_Click"
                                                    Text="Cancel" />
                                        <asp:HiddenField ID="HiddenBulkUpdate" Value="0" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="MO3" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btncancel4" DropShadow="true" Enabled="True" PopupControlID="PanelStatusUpdate"
        RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="Label3">
    </ajaxToolkit:ModalPopupExtender>
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:HiddenField ID="HiddenShowFlag" runat="server" />
</div>
<script type="text/javascript">

    if (document.getElementById("<%=HiddenShowFlag.ClientID %>").value == '1') {
        document.getElementById("<%=HiddenShowFlag.ClientID %>").value = 0


    var sFeatures;
    sFeatures = "dialogWidth: 800px; ";
    sFeatures += "dialogHeight: 600px; ";

    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";


    var result;
    //result = window.showModalDialog('Barcode/BarcodeStudentNumber.aspx?Type=STUDENT' ,"", sFeatures);
    result = window.open('Barcode/BarcodeCrystal/BarcodeStudentLibCard.aspx')


}


</script>
