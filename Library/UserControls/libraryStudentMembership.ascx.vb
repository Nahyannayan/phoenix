Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryStudentMembership
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindBusinessUnit(ddbsu)
            Dim studClass As New studClass
            Hiddenbsuid.Value = Session("sbsuid")
            'ddaccyear = studClass.PopulateAcademicYear(ddaccyear, Session("clm").ToString, Session("sbsuid").ToString)
            BindStudentView()
            BindLibraryDevisions()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button))

    End Sub
    'ts

    Sub BindBusinessUnit(ByVal ddlBUnit As DropDownList)

        Try
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
            CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
            & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
            ddlBUnit.DataSource = ds
            ddlBUnit.DataTextField = "BSU_NAME"
            ddlBUnit.DataValueField = "BSU_ID"
            ddlBUnit.DataBind()
            If Not ddlBUnit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
                ddlBUnit.Items.FindByValue(Session("sBsuid")).Selected = True
            End If
        Catch ex As Exception

        End Try


    End Sub

    Public Sub BindLibraryDevisions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlibraryDivisions.DataSource = ds
        ddlibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
        ddlibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
        ddlibraryDivisions.DataBind()

        BindMemberships()


    End Sub

    Public Sub BindMemberships()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " SELECT MEMBERSHIP_ID,MEMBERSHIP_DES FROM dbo.LIBRARY_MEMBERSHIPS A " & _
                        " INNER JOIN dbo.LIBRARY_DIVISIONS B ON A.LIBRARY_DIVISION_ID=B.LIBRARY_DIVISION_ID " & _
                        " WHERE LIBRARY_BSU_ID='" & Hiddenbsuid.Value & "' AND A.LIBRARY_DIVISION_ID='" & ddlibraryDivisions.SelectedValue & "' AND USER_TYPE='STUDENT'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddMemberships.DataSource = ds
        ddMemberships.DataTextField = "MEMBERSHIP_DES"
        ddMemberships.DataValueField = "MEMBERSHIP_ID"
        ddMemberships.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            btnStatusupdate.Visible = True
        Else
            btnStatusupdate.Visible = False
        End If

    End Sub

    Public Function GetData() As DataSet

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim strQuery = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,''))," & _
                              " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR " & _
                              " FROM oasis.dbo.STUDENT_M AS A INNER JOIN oasis.dbo.GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" & _
                              " INNER JOIN oasis.dbo.SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " & _
                              " inner join oasis..academicyear_d ad on a.stu_Acd_id = ad.acd_id " & _
                              " WHERE stu_bsu_id = " + ddbsu.SelectedValue.ToString + " AND STU_LEAVEDATE IS NULL and STU_CURRSTATUS ='EN' and ad.acd_current=1 "

        Dim txtnumber As String
        Dim txtname As String
        Dim txtGrade As String
        Dim txtSection As String

        If GrdStudents.Rows.Count > 0 Then

            txtnumber = DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txtGrade = DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text.Trim()
            txtSection = DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text.Trim()

            If txtnumber.Trim() <> "" Then
                strQuery &= " and STU_NO like '%" & txtnumber.Replace(" ", "") & "%' "
            End If

            If txtname.Trim() <> "" Then
                strQuery &= " and isnull(STU_FIRSTNAME,'')+ isnull(STU_MIDNAME,'')+isnull(STU_LASTNAME,'') like '%" & txtname.Replace(" ", "") & "%' "
            End If

            If txtGrade.Trim() <> "" Then
                strQuery &= " and GRM_DISPLAY like '%" & txtGrade.Replace(" ", "") & "%' "
            End If

            If txtSection.Trim() <> "" Then
                strQuery &= " and SCT_DESCR like '%" & txtSection.Replace(" ", "") & "%' "
            End If

        End If

        strQuery &= "order by GRM_DISPLAY,SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME,STU_NO "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        Return ds

    End Function

    Public Sub BindStudentView()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
       
        Dim txtnumber As String
        Dim txtname As String
        Dim txtGrade As String
        Dim txtSection As String

        If GrdStudents.Rows.Count > 0 Then

            txtnumber = DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txtGrade = DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text.Trim()
            txtSection = DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text.Trim()

        End If
        
        Dim ds As DataSet

        ds = GetData()

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("stu_id")
            dt.Columns.Add("stu_no")
            dt.Columns.Add("MEMBERSHIP_DES")
            dt.Columns.Add("STU_NAME")
            dt.Columns.Add("GRM_DISPLAY")
            dt.Columns.Add("SCT_DESCR")

            Dim dr As DataRow = dt.NewRow()
            dr("stu_id") = ""
            dr("stu_no") = ""
            dr("MEMBERSHIP_DES") = ""
            dr("STU_NAME") = ""
            dr("GRM_DISPLAY") = ""
            dr("SCT_DESCR") = ""

            dt.Rows.Add(dr)
            GrdStudents.DataSource = dt
            GrdStudents.DataBind()
            DirectCast(GrdStudents.FooterRow.FindControl("btnSave"), Button).Visible = False
            'DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button).Visible = False
            btnupdatebulk.Visible = False
        Else
            btnupdatebulk.Visible = True
            GrdStudents.DataSource = ds
            GrdStudents.DataBind()

            For Each row As GridViewRow In GrdStudents.Rows
                Dim stu_id = DirectCast(row.FindControl("HiddensStuid"), HiddenField).Value
                Dim GridMemberships As GridView = DirectCast(row.FindControl("GridMemberships"), GridView)
                Dim query = " SELECT RECORD_ID,LIBRARY_DIVISION_DES,MEMBERSHIP_DES FROM  LIBRARY_MEMBERSHIP_USERS A" & _
                            " INNER JOIN LIBRARY_MEMBERSHIPS B on A.MEMBERSHIP_ID = B.MEMBERSHIP_ID " & _
                            " INNER JOIN LIBRARY_DIVISIONS C ON C.LIBRARY_DIVISION_ID=A.LIBRARY_DIVISION_ID  WHERE A.USER_TYPE='STUDENT' AND USER_ID='" & stu_id & "'"
                GridMemberships.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

                GridMemberships.DataBind()

            Next



            DirectCast(GrdStudents.FooterRow.FindControl("btnSave"), Button).Visible = True
            'DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button).Visible = True
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStudents.FooterRow.FindControl("btnSave"), Button))
            'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button))

        End If

        If GrdStudents.Rows.Count > 0 Then

            DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text = txtnumber
            DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text = txtname
            DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text = txtGrade
            DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text = txtSection

        End If



    End Sub

    Protected Sub GridMemberships_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdStudents.RowCommand

        If e.CommandName = "Deleting" Then
            LibraryData.isOffLine(Session("sBusper"))
            Dim Record_id = e.CommandArgument
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            Try
                '' Check if item taken for this membership
                Dim sqlquery = " SELECT * FROM dbo.LIBRARY_MEMBERSHIP_USERS A WITH (NOLOCK)" & _
                               " INNER JOIN dbo.LIBRARY_MEMBERSHIPS B WITH (NOLOCK) ON A.MEMBERSHIP_ID=B.MEMBERSHIP_ID " & _
                               " INNER JOIN dbo.LIBRARY_TRANSACTIONS C WITH (NOLOCK) ON C.MEMBERSHIP_ID=B.MEMBERSHIP_ID " & _
                               " INNER JOIN    dbo.LIBRARY_ITEMS_QUANTITY AS ITQ WITH (NOLOCK) ON ITQ.STOCK_ID = C.STOCK_ID " & _
                               " INNER JOIN    dbo.LIBRARY_ITEMS_MASTER AS ITM WITH (NOLOCK) ON ITM.MASTER_ID = ITQ.MASTER_ID " & _
                               " WHERE ISNULL(ITEM_ACTUAL_RETURN_DATE,'')='' AND A.RECORD_ID='" & Record_id & "' " & _
                               " AND isNULL(ITEM_EBOOK, 0) <>1 AND C.USER_ID=(SELECT USER_ID FROM dbo.LIBRARY_MEMBERSHIP_USERS WHERE RECORD_ID='" & Record_id & "' AND USER_TYPE='STUDENT') "

                Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, sqlquery)

                If ds.Tables(0).Rows.Count > 0 Then
                    lblMessage.Text = "Library Memebership is in use. <br> " & ds.Tables(0).Rows.Count & " Item(s) has been taken by this user using this membership. Record cannot be deleted.Return the item(s) and proceed with deletion."
                Else
                    Dim query = "DELETE LIBRARY_MEMBERSHIP_USERS WHERE RECORD_ID='" & Record_id & "' "
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)
                    transaction.Commit()
                    lblMessage.Text = "User Membership Deleted Successfully."
                End If


            Catch ex As Exception
                lblMessage.Text = "Error while transaction. Error :" & ex.Message
                transaction.Rollback()

            Finally
                connection.Close()

            End Try

            MO1.Show()

            BindStudentView()

        End If

    End Sub



    Protected Sub GrdStudents_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdStudents.RowCommand

        If e.CommandName = "save" Then
            HiddenBulkUpdate.Value = 0
            MO3.Show()
            ShowUpdateButton()
        End If

        If e.CommandName = "search" Then
            BindStudentView()
        End If

        If e.CommandName = "barcode" Then
            PrintBarcode()
        End If

    End Sub


    Public Sub PrintBarcode()

        Dim issueallbarcode = DirectCast(GrdStudents.FooterRow.FindControl("CheckIssueAll"), CheckBox).Checked
        Dim flag = 0
        Dim stuids = ""
        Dim i As Integer


        If issueallbarcode = True Then

            Dim ds As DataSet = GetData()

            For i = 0 To ds.Tables(0).Rows.Count - 1
                If flag = 0 Then

                    stuids = ds.Tables(0).Rows(i).Item("stu_id").ToString()
                    flag = 1
                Else
                    stuids &= "," & ds.Tables(0).Rows(i).Item("stu_id").ToString()
                End If
            Next


        Else

            For Each row As GridViewRow In GrdStudents.Rows

                If DirectCast(row.FindControl("CheckBar1"), CheckBox).Checked = True Then

                    If flag = 0 Then

                        stuids = DirectCast(row.FindControl("HiddensStuid"), HiddenField).Value
                        flag = 1
                    Else
                        stuids &= "," & DirectCast(row.FindControl("HiddensStuid"), HiddenField).Value

                    End If
                End If

            Next

        End If

        Session("libstuids") = stuids
        HiddenShowFlag.Value = 1
        ' Response.Redirect("Barcode\BarcodeCrystal\BarcodeStudentLibCard.aspx")


    End Sub



    Protected Sub GrdStudents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdStudents.PageIndexChanging
        GrdStudents.PageIndex = e.NewPageIndex
        BindStudentView()
    End Sub

 
    Protected Sub btncancel4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO3.Hide()
    End Sub

    Protected Sub btnStatusupdate2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LibraryData.isOffLine(Session("sBusper"))
        Dim ds As DataSet = GetData()

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1

                Dim stuid = ds.Tables(0).Rows(i).Item("stu_id").ToString()
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@USER_ID", stuid)
                pParms(1) = New SqlClient.SqlParameter("@LIB_MEM_ID", ddMemberships.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@USER_TYPE", "STUDENT")
                lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_LIBRARY_MEMBERSHIPS", pParms)

            Next
            transaction.Commit()
            lblMessage.Text = "Bulk - User Membership Updated Successfully."
            BindStudentView()

        Catch ex As Exception
            lblMessage.Text = "Error while transaction. Error :" & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()
        End Try

        MO1.Show()

    End Sub
    Protected Sub btnStatusupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try
            Dim flag = 0

            For Each row As GridViewRow In GrdStudents.Rows

                Dim check As CheckBox = DirectCast(row.FindControl("Check"), CheckBox)

                If check.Checked Then

                    flag = 1
                    Dim stuid = DirectCast(row.FindControl("HiddensStuid"), HiddenField).Value
                    Dim pParms(3) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@USER_ID", stuid)
                    pParms(1) = New SqlClient.SqlParameter("@LIB_MEM_ID", ddMemberships.SelectedValue)
                    pParms(2) = New SqlClient.SqlParameter("@USER_TYPE", "STUDENT")
                    lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_LIBRARY_MEMBERSHIPS", pParms)

                End If

            Next

            transaction.Commit()

            If flag = 0 Then
                lblMessage.Text = "Please select students."
            Else
                'BindStudentView()
            End If

        Catch ex As Exception
            lblMessage.Text = "Error while transaction. Error :" & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()

        End Try

        MO3.Hide()
        MO1.Show()
        BindStudentView()

    End Sub

    Protected Sub btnmessageok_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO1.Hide()
    End Sub

    Protected Sub ddlibraryDivisions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindMemberships()
        MO3.Show()
        ShowUpdateButton()
    End Sub

    Protected Sub btnupdatebulk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdatebulk.Click
        HiddenBulkUpdate.Value = 1
        MO3.Show()
        ShowUpdateButton()
    End Sub


    Public Sub ShowUpdateButton()
        If HiddenBulkUpdate.Value = 0 Then
            btnStatusupdate.Visible = True
            btnStatusupdate2.Visible = False
        Else
            btnStatusupdate.Visible = False
            btnStatusupdate2.Visible = True

        End If
    End Sub

    Protected Sub ddbsu_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddbsu.SelectedIndexChanged
        BindStudentView()
    End Sub

End Class
