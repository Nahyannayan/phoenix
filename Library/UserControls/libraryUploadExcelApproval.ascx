﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryUploadExcelApproval.ascx.vb"
    Inherits="Library_UserControls_libraryUploadExcelApproval" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">

    <script type="text/javascript" >

        function change_chk_stateg(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                }
            }
        }
    
    </script>
<div >
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
             <tr>
                    <td class="title-bg-lite">
                        Excel Upload Data Search &amp; Approval
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                   <span class="field-label"> Item Title</span></td>                               
                                <td>
                                    <asp:TextBox ID="txtItemName" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                   <span class="field-label"> Author</span>
                                </td>                             
                                <td>
                                    <asp:TextBox ID="txtAuthor" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                   <span class="field-label"> Publisher</span>
                                </td>                              
                                <td>
                                    <asp:TextBox ID="txtPublisher" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <span class="field-label"> ISBN</span></td>
                             
                                <td>
                                    <asp:TextBox ID="txtISBN" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                   <span class="field-label"> Data</span></td>
                            
                                <td>
                                    <asp:DropDownList ID="DropData" runat="server" AutoPostBack="True">
                                        <asp:ListItem Selected="True" Text="New Entry" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="GEMS Library" Value="1"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                   <span class="field-label"> Errors</span></td>
                               
                                <td>
                                    <asp:DropDownList ID="DropError" runat="server" AutoPostBack="True">
                                        <asp:ListItem Selected="True" Text="All" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Error in Excel" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Error in Retrieval" Value="1"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <span class="field-label"> Data</span></td>
                               
                                <td>
                                    <asp:DropDownList ID="DropDataRetv" AutoPostBack="true" runat="server">
                                     <asp:ListItem Selected="True" Text="All" Value="-1"></asp:ListItem>
                                     <asp:ListItem Text="Retrieved Successfully" Value="0"></asp:ListItem>
                                     <asp:ListItem Text="Retrieval Pending" Value="1"></asp:ListItem>
                                      <asp:ListItem Text="Not Found" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                   <span class="field-label"> Record Id</span></td>
                              
                                <td>
                                    <asp:TextBox ID="txtrecordid" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                   <span class="field-label"> Approved</span></td>
                             
                                <td>
                                    <asp:DropDownList ID="DropApproved" runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Approved" Value="True"></asp:ListItem>
                                        <asp:ListItem Selected="True" Text="Pending Approval" Value="False"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <span class="field-label"> Upload Attempt</span></td>
                             
                                <td>
                                    <asp:DropDownList ID="ddattempt" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                
                            </tr>
                            <tr>
                                <td align="center" colspan="6">
                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" 
                                        CssClass="button" Text="Search" />
                                    <asp:Button ID="btnExcel" runat="server" CausesValidation="false" 
                                        CssClass="button"  Text="Export Excel" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
               
            </table>
            <br />
            <asp:Label ID="lblMessage" runat="server" class="text-danger"></asp:Label>
            <br />
            <p>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                 <tr>
                        <td class="title-bg-lite">
                            Library Items :
                            <asp:Label ID="lblTotal" runat="server"></asp:Label>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GridItem" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                                EmptyDataText="No Records Found. Please search with some other keywords."   CssClass="table table-bordered table-row"
                                ShowFooter="true" Width="100%" PageSize="20">
                                <Columns>

                                 <asp:TemplateField HeaderText="Check">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                                ToolTip="Click here to select/deselect all rows" />
                                                 <center>
                                                <asp:Button ID="btnapprove" runat="server"  CommandName="Approve" CssClass="button" Text="Approve" />
                                            </center>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:CheckBox ID="ch1" Visible='<%# Eval("ALLOW_ENTRY") %>' runat="server" Text="" />
                                            </center>
                                        </ItemTemplate>
                                        <FooterTemplate>

                                            <center>
                                                <asp:Button ID="btnapproveall" runat="server"  CommandName="ApproveAll" CssClass="button" Text="Approve All" />
                                            </center>

                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            
                                                        Item Image
                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:HiddenField ID="HiddenMasterIdExcel" runat="server" Value='<%# Eval("RECORD_ID") %>' />
                                                <asp:ImageButton ID="ImageItem" runat="server" ImageUrl='<%# Eval("PRODUCT_IMAGE_URL") %>'
                                                    OnClientClick="javascript:return false;" />
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            
                                                        Uploaded Tran
                                                  
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                              Tran Id:<%# Eval("RECORD_ID")%>/<%#Eval("UPLOAD_ATTEMPT")%><br />ISBN:<%#Eval("UPLOAD_ISBN")%>/<%#Eval("UPLOAD_ISBN2")%></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                     <asp:TemplateField>
                                        <HeaderTemplate>
                                           
                                                        Item Type
                                                 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                           <center><%#Eval("ITEM_DES")%></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                           
                                                        Author
                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("AUTHOR") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            
                                                        Item Title
                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("ITEM_TITLE") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                           
                                                        ISBN
                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%#Eval("ISBN")%>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                     <asp:TemplateField>
                                        <HeaderTemplate>
                                           
                                                        A~C No 
                                                        
                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%#Eval("ACCESSION_NO")%> ~ <%#Eval("CALL_NO")%>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            
                                                        Publisher
                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("PUBLISHER") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                           
                                                        Entry Date
                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%#Eval("ENTRY_DATE")%>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            
                                                       NC/EC
                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                               <%# Eval("TOTAL_STOCK") %>/<%# Eval("EXISTS_COUNT") %></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            
                                                        Approved
                                                 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:Image ID="Image1" ImageUrl='<%# Eval("APPROVED_IMG") %>' runat="server" />
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            
                                                        Error
                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="LinkView" Text="View" OnClientClick="javascript:return false;"
                                                    runat="server"></asp:LinkButton>
                                                <asp:Panel ID="Show" BorderColor="Black" BackColor="#ffff99" runat="server" Height="50px"
                                                    Width="200px">
                                                    <%#Eval("ERROR_UPLOAD_VAL")%>
                                                    &nbsp;
                                                    <%#Eval("ERROR_DATA_RETRIEVE_VAL")%></asp:Panel>
                                                <ajaxToolkit:HoverMenuExtender ID="hm2" runat="server" TargetControlID="LinkView"
                                                    PopupControlID="Show" PopupPosition="Left" />
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                           
                                                        Delete
                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="LinkDelete" CommandName="Deleting" CommandArgument='<%# Eval("RECORD_ID") %>' Visible='<%# Eval("DELETING") %>' runat="server">Delete</asp:LinkButton>
                                            <ajaxToolkit:ConfirmButtonExtender ID="C1" ConfirmText="Do you wish to continue?" TargetControlID="LinkDelete"  runat="server"></ajaxToolkit:ConfirmButtonExtender>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                           
                                                        Status
                                                   
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:Image ID="Image2" ImageUrl='<%# Eval("STARTED_STATUS") %>' runat="server" />
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                   
                                </Columns>
                                <RowStyle CssClass="griditem"  />
                                <EmptyDataRowStyle  />
                                <SelectedRowStyle  />
                                <HeaderStyle  />
                                <EditRowStyle  />
                                <AlternatingRowStyle CssClass="griditem_alternative"  />
                            </asp:GridView>
                            
                             NC-New&nbsp;Copies. EC-Exists&nbsp;Copies. A~C&nbsp;No - Accession No / Call No
                        </td>
                    </tr>
                   
                </table>
            </p>
                    
            <asp:HiddenField ID="HiddenBsu_id" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
