Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryDetailView
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            HiddenMasterId.Value = Request.QueryString("id") '' Masterid 
            BindControls()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

        If HiddenBsuID.Value = "" Then
            TR1.Visible = False
            TR2.Visible = False
        End If

     
    End Sub


    Public Sub BindControls()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " SELECT ITEM_TITLE,AUTHOR,REPLACE(CONVERT(VARCHAR(11), ENTRY_DATE , 106), ' ', '/') AS ENTRY_DATE,ISBN,PUBLISHER,DATE_YEAR,FORMAT,(SELECT COUNT(*) FROM dbo.LIBRARY_ITEMS_QUANTITY WHERE MASTER_ID = A.MASTER_ID AND ACTIVE='True' AND PRODUCT_BSU_ID='" & HiddenBsuID.Value & "') AS TOTAL_QUANTITY, " & _
                        " ENTRY_TYPE,PRODUCT_DESCRIPTION,CASE ISNULL(PRODUCT_IMAGE_URL,'') WHEN '' THEN  'https://school.gemsoasis.com/Images/Library/noImage.gif' ELSE PRODUCT_IMAGE_URL END PRODUCT_IMAGE_URL,SUBJECT, " & _
                        " PRODUCT_URL, CASE ISNULL(PRODUCT_URL,'') WHEN '' THEN 'FALSE' ELSE 'TRUE' END SHOWURL " & _
                        " ,(SELECT BSU_SHORTNAME FROM OASIS.dbo.BUSINESSUNIT_M WHERE BSU_ID=A.ENTRY_BSU_ID ) ENTRY_BSU_NAME " & _
                        " ,(SELECT BSU_SHORTNAME FROM OASIS.dbo.BUSINESSUNIT_M WHERE BSU_ID='" & HiddenBsuID.Value & "' ) LOGIN_BSU_NAME " & _
                        " FROM dbo.LIBRARY_ITEMS_MASTER A " & _
                        " WHERE A.MASTER_ID='" & HiddenMasterId.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            lbltitle.Text = ds.Tables(0).Rows(0).Item("ITEM_TITLE").ToString()
            lblAuthor.Text = ds.Tables(0).Rows(0).Item("AUTHOR").ToString()
            lblEntryDate.Text = ds.Tables(0).Rows(0).Item("ENTRY_DATE").ToString()

            lblISBN.Text = ds.Tables(0).Rows(0).Item("ISBN").ToString()
            lblpublisher.Text = ds.Tables(0).Rows(0).Item("PUBLISHER").ToString()
            lbldateyear.Text = ds.Tables(0).Rows(0).Item("DATE_YEAR").ToString()
            lblformat.Text = ds.Tables(0).Rows(0).Item("FORMAT").ToString()

            lblEntryType.Text = ds.Tables(0).Rows(0).Item("ENTRY_TYPE").ToString()
            lblTotalQuantity.Text = ds.Tables(0).Rows(0).Item("TOTAL_QUANTITY").ToString()

            txtItemDescription.Text = ds.Tables(0).Rows(0).Item("PRODUCT_DESCRIPTION").ToString()
            lblEntryBsu.Text = "( " & ds.Tables(0).Rows(0).Item("ENTRY_BSU_NAME").ToString() & " )"
            lblBsu.Text = "( " & ds.Tables(0).Rows(0).Item("LOGIN_BSU_NAME").ToString() & " )"

            ImgItem.ImageUrl = ds.Tables(0).Rows(0).Item("PRODUCT_IMAGE_URL").ToString()
            ImgItem.PostBackUrl = ds.Tables(0).Rows(0).Item("PRODUCT_URL").ToString()
            ImgItem.Enabled = ds.Tables(0).Rows(0).Item("SHOWURL").ToString()

            Dim Categories As String = ds.Tables(0).Rows(0).Item("SUBJECT").ToString()

            Dim subject As String() = Categories.Split(",")

            Dim dt As New DataTable
            dt.Columns.Add("SUBJECTS")

            Dim i = 0

            For i = 0 To subject.Length - 1
                Dim dr As DataRow = dt.NewRow()
                dr("SUBJECTS") = subject(i)
                dt.Rows.Add(dr)
            Next

            GridCategory.DataSource = dt
            GridCategory.DataBind()


        End If



    End Sub
End Class
