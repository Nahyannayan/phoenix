﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Library_UserControls_libraryUserCommentsApproval
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            BindGrid()
        End If


    End Sub

    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = " SELECT A.COMMENT_ID,A.MASTER_ID,ITEM_TITLE,PRODUCT_IMAGE_URL,AUTHOR,PUBLISHER,COMMENTS, RATING, APPROVED, A.ENTRY_DATE, " & _
                        " '<span style=''color: red;font-weight:bold''>Hide</span>' as hide, " & _
                        " ('</br><span style=''color: red;font-weight:bold''> more... </span>')tempview1, " & _
                        " (CASE APPROVED WHEN 'True' then 'True' else 'False' end ) SHOW_TICK, " & _
                        " (CASE APPROVED WHEN 'False' then 'True' else 'False' end ) SHOW_CROSS, " & _
                        " (SELECT ITEM_DES FROM dbo.LIBRARY_ITEMS WHERE ITEM_ID=(SELECT TOP 1 ITEM_ID FROM dbo.LIBRARY_ITEMS_QUANTITY WHERE MASTER_ID=A.MASTER_ID AND PRODUCT_BSU_ID=A.COMMENT_BSU)) ITEM_DES " & _
                        " from dbo.LIBRARY_ITEMS_COMMENTS A " & _
                        " INNER JOIN dbo.LIBRARY_ITEMS_MASTER B ON A.MASTER_ID = B.MASTER_ID " & _
                        " WHERE A.COMMENT_BSU='" & HiddenBsuID.Value & "' "

        If txtMasterId.Text <> "" Then
            Sql_Query &= " AND A.MASTER_ID='" & txtMasterId.Text & "' "
        End If

        If txtItemName.Text <> "" Then
            Sql_Query &= " AND ITEM_TITLE LIKE '%" & txtItemName.Text & "%'"
        End If

        If txtAuthor.Text <> "" Then
            Sql_Query &= " AND AUTHOR LIKE '%" & txtAuthor.Text & "%'"
        End If

        If txtISBN.Text <> "" Then
            Sql_Query &= " AND ISBN LIKE '%" & txtISBN.Text & "%'"
        End If

        If txtPublisher.Text <> "" Then
            Sql_Query &= " AND PUBLISHER LIKE '%" & txtPublisher.Text & "%'"
        End If

        If ddStatus.SelectedValue <> "0" Then
            Sql_Query &= " AND APPROVED ='" & ddStatus.SelectedValue & "'"
        End If

        Sql_Query &= " ORDER BY  A.MASTER_ID  "


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        GridItem.DataSource = ds
        GridItem.DataBind()




    End Sub

    Protected Sub GridItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridItem.PageIndexChanging
        GridItem.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub


    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LibraryData.isOffLine(Session("sBusper"))
        Dim LibraryMessage = ""

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()

        Try
            Dim flag = 0
            Dim query = ""
            Dim Value = DirectCast(GridItem.FooterRow.FindControl("RadioApprovalSelect"), RadioButtonList).SelectedValue

            For Each row As GridViewRow In GridItem.Rows

                Dim HiddenStockId = DirectCast(row.FindControl("HiddenCommentId"), HiddenField).Value
                Dim CheckApprove As CheckBox = DirectCast(row.FindControl("CheckApprove"), CheckBox)


                If CheckApprove.Checked Then
                    flag = 1
                    query = "UPDATE LIBRARY_ITEMS_COMMENTS SET APPROVED='" & Value & "' WHERE COMMENT_ID='" & HiddenStockId & "'"
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)
                End If

            Next

            If flag = 0 Then
                LibraryMessage = "Please select comments for Approve/Disapprove ."
            Else
                LibraryMessage = "Selected Comments Updated Successfully."
            End If

            transaction.Commit()

        Catch ex As Exception

            transaction.Rollback()
            LibraryMessage = "Error occured while saving . " & ex.Message

        Finally

            connection.Close()

        End Try

        lblMessage.Text = LibraryMessage

        BindGrid()

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblMessage.Text = ""
        BindGrid()
    End Sub

End Class
