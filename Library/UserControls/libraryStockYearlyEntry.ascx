﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryStockYearlyEntry.ascx.vb" Inherits="Library_UserControls_libraryStockYearlyEntry" %>
<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <br />

        <!-- Bootstrap core CSS-->
        <link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Custom fonts for this template-->
        <link href="/PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- Page level plugin CSS-->
        <link href="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
        <!-- Custom styles for this template-->
        <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
        <link href="/PHOENIXBETA/cssfiles/sb-admin.css" rel="stylesheet">
        <link href="/PHOENIXBETA/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">


        <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
                <td align="center" class="title-bg-lite">Enter Accession Number</td>
            </tr>
            <tr>
                <td align="center">
                    <asp:TextBox ID="txtaccessionnumber" runat="server" AutoPostBack="True"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblMessage" runat="server" CssClass="error"></asp:Label>
                </td>
            </tr>
        </table>

        <br />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">Yearly Stock List Details
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:GridView ID="GridData" runat="server" AllowPaging="True" PageSize="5" EmptyDataText="No Records Found" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Status">
                                <HeaderTemplate>
                                    Item Image 
                                       
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                 <asp:Image ID="Image1" ImageUrl='<%#Eval("PRODUCT_IMAGE_URL")%>' runat="server" />
                             </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rout Descriptions">
                                <HeaderTemplate>
                                    Title
                                       
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                    <%#Eval("ITEM_TITLE")%>
                                </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rout Descriptions">
                                <HeaderTemplate>
                                    Accession No. 
                                       
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                    <%#Eval("ACCESSION_NO")%>
                                </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rout Descriptions">
                                <HeaderTemplate>
                                    Call No. 
                                       
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                    <%#Eval("CALL_NO")%>
                                </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rout Descriptions">
                                <HeaderTemplate>
                                    ISBN
                                       
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                    <%#Eval("ISBN")%>
                                </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rout Descriptions">
                                <HeaderTemplate>
                                    Item Status 
                                       
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                    <%#Eval("STATUS_DESCRIPTION")%>
                                </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rout Descriptions">
                                <HeaderTemplate>
                                    Scanned
                                     
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                    <%#Eval("SCAN_STATUS")%> 
                                </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rout Descriptions">
                                <HeaderTemplate>
                                    Entry Date 
                                       
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                    <%#Eval("ENTRY_DATE")%>
                                </center>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <RowStyle CssClass="griditem" />
                        <EmptyDataRowStyle />
                        <SelectedRowStyle />
                        <HeaderStyle />
                        <EditRowStyle />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                </td>
            </tr>
        </table>

        <br />

        <div align="left">
            <asp:Button ID="btnclose" runat="server" CssClass="button" Text="Completed Stock Entry " />
            <ajaxToolkit:ConfirmButtonExtender ID="Cf1" TargetControlID="btnclose" ConfirmText="Completing the stock entry will result closing of the entry process. Due and inactive items will automatically reflect to the yearly stock entry list.Do you wish to continue?" runat="server"></ajaxToolkit:ConfirmButtonExtender>
        </div>

        <asp:HiddenField ID="Hiddenstocktakeid" runat="server" />
        <asp:HiddenField ID="Hiddenlibrarydivid" runat="server" />

    </ContentTemplate>
</asp:UpdatePanel>

