﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.OleDb

Partial Class Library_UserControls_libraryBulkPouchUpdate
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            HiddenBsuid.Value = Session("sbsuid")
            Hiddenempid.Value = Session("EmployeeId")

        End If

    End Sub

    Protected Sub btnupload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupload.Click
        lblmessage.Text = ""
        If FileUpload1.HasFile Then

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
            Dim Sql_Query = ""
            Dim myDataset As New DataSet()
            Dim strConnE As String = ""
            Dim filenam As String = FileUpload1.PostedFile.FileName

            Dim path = WebConfigurationManager.AppSettings("UploadExcelFile").ToString()
            Dim filename = FileUpload1.FileName.ToString()
            Dim savepath = path + "NewsLettersEmails/" + Session("EmployeeId") + "_LIBRARY_DATA_" + Date.Now().ToString().Replace("/", "-").Replace(":", "-") + "." + GetExtension(filename)
            FileUpload1.SaveAs(savepath)


            Try
                strConnE = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & savepath & "; Extended Properties=""Excel 12.0;HDR=YES;"""
                Dim conn As New OleDbConnection(strConnE)
                Dim myData As New OleDbDataAdapter
                myData.SelectCommand = New OleDbCommand("SELECT POUCH_NO,ACCESSION_NO FROM [Sheet1$] ", conn)


                myData.TableMappings.Add("Table", "ExcelTest")
                myData.Fill(myDataset)


            Catch ex As Exception
                strConnE = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & savepath & ";Extended Properties=""Excel 8.0;"""
                Dim conn As New OleDbConnection(strConnE)
                Dim myData As New OleDbDataAdapter
                myData.SelectCommand = New OleDbCommand("SELECT POUCH_NO,ACCESSION_NO FROM [Sheet1$] ", conn)


                myData.TableMappings.Add("Table", "ExcelTest")
                myData.Fill(myDataset)

            End Try

            Try

                Dim updatedflag As Boolean = False
                If myDataset.Tables(0).Rows.Count > 0 Then
                    Dim dt As DataTable
                    dt = myDataset.Tables(0)
                    dt.Columns.Add("STATUS")

                    '' Check Pouch Accession number first in database and rack assigned and its pouch no is null...
                    If CheckPouchNoExists(dt) Then

                        ''Check Accession Number exists in database and rack assigned
                        If CheckAccessionNoExists(dt) Then

                            '' Check pouch and item exits in same library 
                            If CheckSameLibraryExists(dt) Then

                                '' Check if item issued
                                If CheckIssueItems(dt) Then
                                    ''Update the record
                                    UpdateRecords(dt)
                                    updatedflag = True

                                End If

                            End If

                        End If

                    End If

                    GridInfo.DataSource = dt
                    GridInfo.DataBind()

                    If updatedflag = False Then
                        lblmessage.Text = "Errors in Excel.Updation terminated.Please update correctly and upload again."
                        'Export Excel
                        ExportExcel(dt)

                    End If

                Else
                    lblmessage.Text = "No items found in excel"
                End If

            Catch ex As Exception

                lblmessage.Text = "Error : " & ex.Message

            End Try

        End If

    End Sub

    Public Sub ExportExcel(ByVal dt As DataTable)

        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, "A1", True)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")
        ef.SaveXls(Response.OutputStream)

    End Sub

    Private Sub UpdateRecords(ByRef dt As DataTable)

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim Sql_Query = ""
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Dim i = 0
        Dim ACCESSION_NO = ""
        Dim POUCH_NO = ""
        Try
            For i = 0 To dt.Rows.Count - 1

                POUCH_NO = dt.Rows(i).Item("POUCH_NO").ToString().Trim()
                ACCESSION_NO = dt.Rows(i).Item("ACCESSION_NO").ToString().Trim()
                Sql_Query = " UPDATE dbo.LIBRARY_ITEMS_QUANTITY SET STOCK_ID_POUCH_ID='" & POUCH_NO & "' WHERE  ACCESSION_NO='" & ACCESSION_NO & "' AND PRODUCT_BSU_ID='" & HiddenBsuid.Value & "' "
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, Sql_Query)
                transaction.Commit()
                dt.Rows(i).Item("STATUS") = "Success: Updated Successfully."
            Next

        Catch ex As Exception
            lblmessage.Text = "Error occurred while transactions. " & ex.Message
            transaction.Rollback()
        Finally
            connection.Close()

        End Try

    End Sub


    Private Function CheckIssueItems(ByRef dt As DataTable) As Boolean

        Dim returnval = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = ""

        Dim i = 0
        Dim ACCESSION_NO = ""

        For i = 0 To dt.Rows.Count - 1

            ACCESSION_NO = dt.Rows(i).Item("ACCESSION_NO").ToString().Trim()
            Sql_Query = " select MASTER_ID from dbo.VIEW_USER_TRANSACTIONS where ITEM_ACTUAL_RETURN_DATE='01/Jan/1900' and ACCESSION_NO='" & ACCESSION_NO & "' and PRODUCT_BSU_ID='" & HiddenBsuid.Value & "'  "

            If SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query).Tables(0).Rows.Count > 0 Then
                dt.Rows(i).Item("STATUS") = "Error : Item issed to user, Updation cant be done - Please check"
                returnval = False
            Else
                dt.Rows(i).Item("STATUS") = "Level 4 Check - Ok"
            End If

        Next


        Return returnval

    End Function


    Private Function CheckSameLibraryExists(ByRef dt As DataTable) As Boolean
        Dim returnval = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = ""

        Dim i = 0
        Dim ACCESSION_NO = ""
        Dim POUCH_NO = ""
        For i = 0 To dt.Rows.Count - 1

            POUCH_NO = dt.Rows(i).Item("POUCH_NO").ToString().Trim()
            ACCESSION_NO = dt.Rows(i).Item("ACCESSION_NO").ToString().Trim()
            Sql_Query = " select TOP 1  B.LIBRARY_DIVISION_ID from [VIEW_LIBRARY_RECORDS] A INNER JOIN [VIEW_LIBRARY_RECORDS] B ON B.ACCESSION_NO='" & POUCH_NO & "' " & _
                        " INNER JOIN [VIEW_LIBRARY_RECORDS] C ON C.ACCESSION_NO='" & ACCESSION_NO & "'WHERE B.LIBRARY_DIVISION_ID=C.LIBRARY_DIVISION_ID "

            If SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query).Tables(0).Rows.Count = 0 Then
                dt.Rows(i).Item("STATUS") = "Error : Pouch and Item does not below to the same library - Please check"
                returnval = False
            Else
                dt.Rows(i).Item("STATUS") = "Level 3 Check - Ok"
            End If

        Next


        Return returnval
    End Function

    Private Function CheckAccessionNoExists(ByRef dt As DataTable) As Boolean
        Dim returnval = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = ""

        Dim i = 0
        Dim ACCESSION_NO = ""

        For i = 0 To dt.Rows.Count - 1

            ACCESSION_NO = dt.Rows(i).Item("ACCESSION_NO").ToString().Trim()
            Sql_Query = "select ACCESSION_NO from VIEW_LIBRARY_RECORDS where PRODUCT_BSU_ID='" & HiddenBsuid.Value & "' and [ACCESSION_NO]='" & ACCESSION_NO & "' and [RACK_ID] IS NOT NULL "

            If SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query) = "" Then
                dt.Rows(i).Item("STATUS") = "Error : Item does not belong to this library / Rack not been assigned to this item - Please check"
                returnval = False
            Else
                dt.Rows(i).Item("STATUS") = "Level 2 Check - Ok"
            End If

        Next


        Return returnval
    End Function

    Private Function CheckPouchNoExists(ByRef dt As DataTable) As Boolean
        Dim returnval = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = ""

        Dim i = 0
        Dim POUCH_NO = ""

        For i = 0 To dt.Rows.Count - 1
            POUCH_NO = dt.Rows(i).Item("POUCH_NO").ToString().Trim()
            Sql_Query = "select ACCESSION_NO from VIEW_LIBRARY_RECORDS where PRODUCT_BSU_ID='" & HiddenBsuid.Value & "' and [ACCESSION_NO]='" & POUCH_NO & "' and [RACK_ID] IS NOT NULL and STOCK_ID_POUCH_ID is null"
            If SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query) = "" Then
                dt.Rows(i).Item("STATUS") = "Error : Pouch does not belong to this library / Rack not been assigned to this pouch / This Pouch should not been assigned to any other pouch- Please check"
                returnval = False
            Else
                dt.Rows(i).Item("STATUS") = "Level 1 Check - Ok"
            End If

        Next


        Return returnval
    End Function

    Private Function GetExtension(ByVal FileName As String) As String
        Dim split As String() = FileName.Split(".")
        Dim Extension As String = split(split.Length - 1)
        Return Extension
    End Function

End Class
