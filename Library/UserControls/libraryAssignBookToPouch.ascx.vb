﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class Library_UserControls_libraryAssignBookToPouch
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            CheckPouchIssued()
            BindGrid()
        End If

    End Sub

    Public Sub CheckPouchIssued()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim query = "select STOCK_ID from VIEW_LIBRARY_RECORDS where STOCK_ID='" & Request.QueryString("stock_id") & "' AND AVAILABILITY='N'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        If ds.Tables(0).Rows.Count > 0 Then
            btnAdd.Visible = False
            lblmessage.Text = "Pouch has been issed to user and you cannot add new items to this pouch.You can add new items to this pouch once the item is returned by user."
        End If

    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STOCK_ID_POUCH_ID", Request.QueryString("stock_id"))
        pParms(1) = New SqlClient.SqlParameter("@OPTION", 2)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "LIB_POUCH_TRAN", pParms)
        GridItem.DataSource = ds
        GridItem.DataBind()
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        lblmessage.Text = ""
        lblmessage.Text = Library.InsertPouch(Request.QueryString("stock_id"), Session("sbsuid"), LibraryData.GetStockIDForAccessonNo(txtaccessionnumber.Text, Session("sbsuid")))
        BindGrid()
        txtaccessionnumber.Text = ""

    End Sub

    Public Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblmessage.Text = ""
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblmessage.Text = ""
        Try

            For Each row As GridViewRow In GridItem.Rows
                If DirectCast(row.FindControl("ch1"), CheckBox).Checked Then
                    Dim pParms(3) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@STOCK_ID", DirectCast(row.FindControl("HiddenBookId"), HiddenField).Value)
                    pParms(1) = New SqlClient.SqlParameter("@OPTION", 3)
                    lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "LIB_POUCH_TRAN", pParms)
                End If
            Next
            transaction.Commit()
            BindGrid()

        Catch ex As Exception
            transaction.Rollback()

        Finally
            connection.Close()
        End Try
        BindGrid()
    End Sub

    Protected Sub GridItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridItem.PageIndexChanging
        GridItem.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        Dim dt As New DataTable
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STOCK_ID_POUCH_ID", Request.QueryString("stock_id"))
        pParms(1) = New SqlClient.SqlParameter("@OPTION", 2)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "LIB_POUCH_TRAN", pParms)
        dt = ds.Tables(0)
        dt.Columns.Remove("MASTER_ID")
        dt.Columns.Remove("PRODUCT_BSU_ID")
        dt.Columns.Remove("STOCK_ID")
        dt.Columns.Remove("STOCK_ID_POUCH_ID")
        dt.Columns.Remove("LIBRARY_DIVISION_ID")
        dt.Columns.Remove("LIBRARY_SUB_DIVISION_ID")
        dt.Columns.Remove("SHELF_ID")
        dt.Columns.Remove("RACK_ID")
        dt.Columns.Remove("PRODUCT_IMAGE_URL")
        dt.Columns.Remove("PRODUCT_URL")
        dt.Columns.Remove("AVALABLE")
        dt.Columns.Remove("EDIT_ENABLE")

        ExportExcel(dt)
    End Sub

    Public Sub ExportExcel(ByVal dt As DataTable)




        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As New GemBox.Spreadsheet.ExcelFile()
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
       
        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "Data.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)

        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()


    End Sub
End Class
