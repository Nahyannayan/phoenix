<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryItems.ascx.vb" Inherits="Library_UserControls_libraryItems" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<script type="text/javascript">



    function OnTreeClick(evt) {
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");

        if (isChkBoxClick) {
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid != event.srcElement.id) {
                    document.forms[0].elements[i].checked = false;
                }
            }
        }

    }






</script>

<div class="matters">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
           Global Settings
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td></td>
                                    <td align="left">
                                        <asp:TreeView ID="TreeItemCategory" runat="server"   CssClass="checkbox-list" Width="100%" style="max-height: none;"
                                              ImageSet="Arrows" onclick="OnTreeClick(event);" ShowCheckBoxes="All">
                                        </asp:TreeView>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="field-label"> Item Category</span></td>
                                    <td>
                                        <asp:TextBox ID="txtcategory" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnsave" CssClass="button" runat="server" Text="Save" />
                                        <asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Update" />
                                        <asp:Button ID="btnDelete" CssClass="button" runat="server" Text="Delete" />
                                        <asp:Button ID="btnExport" CssClass="button" runat="server" Text="Export" />

                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></td>
                                </tr>
                            </table>
                            <ajaxToolkit:ConfirmButtonExtender ID="CF1" ConfirmText="Do you wish to continue?" TargetControlID="btnDelete" runat="server"></ajaxToolkit:ConfirmButtonExtender>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<asp:HiddenField ID="Hiddenbsu_id" runat="server" />
