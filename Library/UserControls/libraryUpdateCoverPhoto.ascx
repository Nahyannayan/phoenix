﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryUpdateCoverPhoto.ascx.vb" Inherits="Library_UserControls_libraryUpdateCoverPhoto" %>

<br />

<center>


<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" 
    style="width: 542px">
    <tr>
        <td align="center" class="subheader_img" colspan="2">
            <asp:Label ID="lbltitle" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="center" class="subheader_img">
            Old</td>
        <td align="center" class="subheader_img">
            New</td>
    </tr>
    <tr>
        <td align="center">
            <asp:Image ID="Image1" runat="server" />
        </td>
        <td align="center">
            <asp:Image ID="Image2" runat="server" />
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:Button ID="btnupdate" runat="server" CssClass="button" Text="Update" 
                Width="96px" />
            <asp:Button ID="btncancel0" runat="server" CssClass="button" OnClientClick="window.close();" Text="Cancel" 
                Width="96px" />
            <br />
            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
        </td>
    </tr>
</table>

</center>
<asp:HiddenField ID="HiddenImageUrl" runat="server" />
