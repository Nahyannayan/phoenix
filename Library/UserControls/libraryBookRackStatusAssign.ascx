<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryBookRackStatusAssign.ascx.vb" Inherits="Library_UserControls_libraryBookRackStatusAssign" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


 <script type="text/javascript" >
 
function Redirect(value)
{
window.open('libraryDetailView.aspx?id='+ value , '','Height=800px,Width=1020px,scrollbars=yes,resizable=no,directories=yes');
return false; 
} 

</script>
    <script type="text/javascript" >
      function change_chk_stateg(chkThis)
             {
                    var chk_state= ! chkThis.checked ;
                     for(i=0; i<document.forms[0].elements.length; i++)
                           {
                           var currentid =document.forms[0].elements[i].id; 
                           if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("ch1")!=-1)
                         {
                          
                                document.forms[0].elements[i].checked=chk_state;
                                 document.forms[0].elements[i].click();
                             }
                          }
              }
    
    </script>
<div >

<asp:UpdatePanel id="UpdatePanel1" runat="server">
    <contenttemplate>
                        <asp:CheckBox ID="CheckNullRack" runat="server" Text="Unassigned Rack" AutoPostBack="True" OnCheckedChanged="CheckNullRack_CheckedChanged" />
                        <asp:CheckBox ID="CheckStatus" runat="server" Text="Unassigned Status" AutoPostBack="True" OnCheckedChanged="CheckStatus_CheckedChanged" /><BR />
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">
            Library Book List</td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridItem" runat="server" AllowPaging="True" ShowFooter="true" PageSize="8" CssClass="table table-bordered table-row" 
                 AutoGenerateColumns="false" Width="100%"  OnRowCommand="GridItem_RowCommand">
                <Columns>
                <asp:TemplateField HeaderText="Check"><HeaderTemplate>
       
                  <asp:CheckBox ID="chkAll"  runat="server"  onclick="javascript:change_chk_stateg(this);" ToolTip="Click here to select/deselect all rows" />
                        
                </HeaderTemplate>
                <ItemTemplate>
                <center>
                         <asp:CheckBox ID="ch1" Text="" runat="server" />
                </center>   
                </ItemTemplate>
                <FooterTemplate>
                <center>
                <asp:Button ID="btnBarcode" CssClass="button" ValidationGroup="Edit" runat="server" OnClick="btnBarcode_Click" ToolTip="Issue Barcode" Text="BC" />
                </center>
                </FooterTemplate>
                </asp:TemplateField>
                
                    <asp:TemplateField>
                        <HeaderTemplate>
                           
                                       Reference No
                                 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                              <asp:HiddenField ID="HiddenBookId" runat="server" Value='<%# Eval("STOCK_ID") %>' />
                              <asp:HiddenField ID="HiddenStockId" Value='<%#Eval("ACCESSION_NO")%>' runat="server" />
                                <%#Eval("STOCK_ID")%>
                                
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField>
                        <HeaderTemplate>
                            
                                        Item Image
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:HiddenField ID="HiddenMasterId" runat="server" Value='<%# Eval("MASTER_ID") %>' />
                                <asp:Image ID="Image" Width="40px" Height="50px"  ImageUrl='<%# Eval("PRODUCT_IMAGE_URL") %>' runat="server" />
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                       Accession No
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:TextBox ID="txtAccessionNo" Text='<%#Eval("ACCESSION_NO")%>' Enabled='<%# Eval("EDIT_ENABLE") %>' runat="server"></asp:TextBox>
                                
                            </center>
                        </ItemTemplate>
                         <FooterTemplate>
                           <center><asp:Button ID="btnUpdateAccession" CssClass="button" CommandName="UpdateAccessionNo" runat="server" Text="Assign" /></center>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                           
                                       Call No
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                             <asp:TextBox ID="txtCallNo" Text='<%#Eval("CALL_NO")%>' Enabled='<%# Eval("EDIT_ENABLE") %>' runat="server"></asp:TextBox>
                            </center>
                        </ItemTemplate>
                         <FooterTemplate>
                           <center><asp:Button ID="btnUpdatecallno" CssClass="button" CommandName="UpdateCallNo" runat="server" Text="Assign" /></center>
                        </FooterTemplate>
                    </asp:TemplateField>
                   
        <asp:TemplateField HeaderText="Library Divisions">
            <HeaderTemplate>
               
                            Library Divisions
                      
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <%#Eval("LIBRARY_DIVISION_DES")%>
                </center>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Library Sub Divisions">
            <HeaderTemplate>
               
                            Library Sub Divisions
                      
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <%#Eval("LIBRARY_SUB_DIVISION_DES")%>
                </center>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Row">
            <HeaderTemplate>
               
                            Row
                      
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <%#Eval("SHELF_ROW")%>
                </center>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Shelf">
            <HeaderTemplate>
               
                            Shelf
                     
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <%#Eval("SHELF_NAME")%>
                </center>
            </ItemTemplate>
        </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                      Rack
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                               <%#Eval("RACK_NAME")%>
                               <br />
                               <asp:CheckBox ID="CheckRack" Enabled='<%# Eval("EDIT_ENABLE") %>' runat="server" />
                            </center>
                        </ItemTemplate>
                        <FooterTemplate>
                       <center><asp:Button ID="btnUpdateRack" CssClass="button" CommandName="UpdateRack" runat="server" Text="Assign" /></center>
                        </FooterTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField>
                        <HeaderTemplate>
                            
                                      Status
                                 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                               <%#Eval("STATUS_DESCRIPTION")%>
                               <br />
                               <asp:CheckBox ID="CheckStatus" runat="server" />
                            </center>
                        </ItemTemplate>
                         <FooterTemplate>
                           <center><asp:Button ID="btnUpdateStatus" CssClass="button" CommandName="UpdateStatus" runat="server" Text="Assign" /></center>
                        </FooterTemplate>
                    </asp:TemplateField>

                </Columns>
                <RowStyle CssClass="griditem" />
                <EmptyDataRowStyle  />
                <SelectedRowStyle />
                <HeaderStyle  />
                <EditRowStyle  />
                <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>
         <%--  <asp:DropDownList ID="ddbarcodesize" ToolTip="Barcode Size (Height)" runat="server">
            <asp:ListItem Value="75">75</asp:ListItem> 
            <asp:ListItem Value="76">76</asp:ListItem> 
            <asp:ListItem Value="77">77</asp:ListItem> 
            <asp:ListItem Value="78">78</asp:ListItem> 
            <asp:ListItem Value="79">79</asp:ListItem> 
            <asp:ListItem Value="80">80</asp:ListItem> 
            <asp:ListItem Value="81">81</asp:ListItem> 
            <asp:ListItem Value="82">82</asp:ListItem> 
            <asp:ListItem Value="83">83</asp:ListItem> 
            <asp:ListItem Value="84">84</asp:ListItem> 
            <asp:ListItem Value="85">85</asp:ListItem> 

            </asp:DropDownList>--%>
            
            </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenBsuID" runat="server" />
        <asp:HiddenField ID="HiddenMasterId" runat="server" />
    </contenttemplate>
</asp:UpdatePanel><asp:Label ID="Label1" runat="server"></asp:Label><br />
    <asp:Panel ID="PanelMessage" runat="server" CssClass="panel-cover"
        Style="display: none">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">
                    Library Message</td>
            </tr>
            <tr>
                <td align="center">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="lblMessage" runat="server" class="text-danger"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnmok" runat="server" CausesValidation="False" CssClass="button"
                                            OnClick="btnmessageok_Click" Text="Ok" ValidationGroup="s" /></td>
                                </tr>
                            </table>
                            &nbsp;
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
        DropShadow="true" PopupControlID="PanelMessage" RepositionMode="RepositionOnWindowResizeAndScroll"
        TargetControlID="Label1">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Label ID="Label2" runat="server"></asp:Label>&nbsp;
    <asp:Panel ID="PanelStatus" runat="server" CssClass="panel-cover"
        Style="display: none">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">
                    Library Message</td>
            </tr>
            <tr>
                <td align="left">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="lblMessage2" runat="server" class="text-danger"></asp:Label><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
<table>
    <tr>
        <td align="left">
            Library Divisions</td>
      
        <td>
            <asp:DropDownList ID="ddLibraryDivisions" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddLibraryDivisions_SelectedIndexChanged">
            </asp:DropDownList></td>
        <td>
            Library Sub Divisions</td>
     
        <td>
            <asp:DropDownList ID="ddLibrarySubDivisions" runat="server" OnSelectedIndexChanged="ddLibrarySubDivisions_SelectedIndexChanged" AutoPostBack="True">
            </asp:DropDownList></td>
    </tr>
  
    <tr>
        <td>
            Shelf Id</td>
       
        <td>
            <asp:DropDownList ID="ddShelfName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddShelfName_SelectedIndexChanged">
            </asp:DropDownList></td>
        <td>
            Rack Id</td>
     
        <td>
            <asp:DropDownList ID="ddRacks" runat="server">
            </asp:DropDownList></td>
    </tr>
 
    <tr>
        <td align="center" colspan="4">
            <asp:Button ID="btnstockupdate" runat="server" CausesValidation="False" CssClass="button"
                OnClick="btnstockupdate_Click" Text="Update"/><asp:Button ID="btncancel3" runat="server" CausesValidation="False" CssClass="button"
                OnClick="btncancel3_Click" Text="Cancel"/></td>
    </tr>
</table>
                                        <br />
                                    </td>
                                </tr>
                            </table>
                            <ajaxToolkit:ConfirmButtonExtender ID="CF1" runat="server" ConfirmText="Continue with Updation?  "
                                TargetControlID="btnstockupdate">
                            </ajaxToolkit:ConfirmButtonExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="MO2" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btncancel3" DropShadow="true" PopupControlID="PanelStatus" RepositionMode="RepositionOnWindowResizeAndScroll"
        TargetControlID="Label2">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Label ID="Label3" runat="server"></asp:Label>
    <asp:Panel ID="PanelStatusUpdate" runat="server"
        BackColor="white" CssClass="panel-cover" Style="display: none">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">
                    Library Message-Item Status</td>
            </tr>
            <tr>
                <td align="center" >
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" colspan="2">
                            <br />
                            <asp:DropDownList ID="ddStatus" runat="server">
                            </asp:DropDownList><br /><br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnStatusupdate" runat="server" CausesValidation="False" CssClass="button"
                                            OnClick="btnStatusupdate_Click" Text="Update" /></td>
                        <td>
                            <asp:Button ID="btncancel4" runat="server" CausesValidation="False" CssClass="button"
                                            OnClick="btncancel4_Click" Text="Cancel" /></td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="MO3" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btncancel4" DropShadow="true" PopupControlID="PanelStatusUpdate" RepositionMode="RepositionOnWindowResizeAndScroll"
        TargetControlID="Label3" Enabled="True">
    </ajaxToolkit:ModalPopupExtender>
    
    <asp:HiddenField ID="HiddenShowFlag" runat="server" />
</div>

<script type="text/javascript"> 

if (document.getElementById("<%=HiddenShowFlag.ClientID %>").value == '1')
{
            document.getElementById("<%=HiddenShowFlag.ClientID %>").value = 0


            var sFeatures;
            sFeatures="dialogWidth: 800px; ";
            sFeatures+="dialogHeight: 600px; ";
          
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

         
            var result;
            result = window.open('Barcode/BarcodeCrystal/Barcode.aspx')

            
}


</script>