Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Partial Class Library_UserControls_libraryBookRackStatusAssign
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Session("Barcodehashtable") = Nothing
            Session("BarcodehashtableTemp") = Nothing

            HiddenMasterId.Value = Request.QueryString("MasterId")
            HiddenBsuID.Value = Session("sbsuid")
            Dim edit = Request.QueryString("Edit")
            If edit = 0 Then
                CheckStatus.Checked = True
                CheckNullRack.Checked = True
            End If
            HiddenShowFlag.Value = 0
            BindLibraryDivisions()
            BindStatus()
            BindGrid()


        End If
        If GridItem.Rows.Count > 0 Then
            Dim barcodebutton As Button = DirectCast(GridItem.FooterRow.FindControl("btnBarcode"), Button)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(barcodebutton)
        End If
    End Sub


    Public Sub BindStatus()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT STATUS_ID,STATUS_DESCRIPTION FROM LIBRARY_ITEM_STATUS WHERE STATUS_ID != 4 "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddStatus.DataSource = ds
        ddStatus.DataTextField = "STATUS_DESCRIPTION"
        ddStatus.DataValueField = "STATUS_ID"
        ddStatus.DataBind()


    End Sub

    Public Sub BindLibraryDivisions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddLibraryDivisions.DataSource = ds
        ddLibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
        ddLibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
        ddLibraryDivisions.DataBind()

        BindLibrarySubDivisions()

    End Sub

    Public Sub BindLibrarySubDivisions()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_SUB_DIVISIONS " & _
                        "WHERE LIBRARY_DIVISION_ID='" & ddLibraryDivisions.SelectedValue & "' ORDER BY LIBRARY_SUB_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddLibrarySubDivisions.DataSource = ds
        ddLibrarySubDivisions.DataTextField = "LIBRARY_SUB_DIVISION_DES"
        ddLibrarySubDivisions.DataValueField = "LIBRARY_SUB_DIVISION_ID"
        ddLibrarySubDivisions.DataBind()

        BindShelfS()

    End Sub


    Public Sub BindShelfS()
        lblMessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_SHELFS " & _
                        "WHERE LIBRARY_SUB_DIVISION_ID='" & ddLibrarySubDivisions.SelectedValue & "' ORDER BY SHELF_NAME DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddShelfName.DataSource = ds
        ddShelfName.DataTextField = "SHELF_NAME"
        ddShelfName.DataValueField = "SHELF_ID"
        ddShelfName.DataBind()

        BindRacks()

    End Sub

    Public Sub BindRacks()
        lblMessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "select RACK_ID,RACK_NAME from  dbo.LIBRARY_RACKS where SHELF_ID='" & ddShelfName.SelectedValue & "' ORDER BY RACK_NAME DESC "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddRacks.DataSource = ds
        ddRacks.DataTextField = "RACK_NAME"
        ddRacks.DataValueField = "RACK_ID"
        ddRacks.DataBind()

        Dim list As New ListItem
        list.Value = "0"
        list.Text = "No Rack"
        ddRacks.Items.Insert(0, list)


    End Sub


    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString


        Dim str_query = " SELECT A.MASTER_ID,ACCESSION_NO,CALL_NO,STOCK_ID,LIBRARY_DIVISION_DES,LIBRARY_SUB_DIVISION_DES,SHELF_ROW,SHELF_NAME,RACK_NAME,STATUS_DESCRIPTION, " & _
                        " PRODUCT_URL,CASE ISNULL(PRODUCT_IMAGE_URL,'') WHEN '' THEN '~/Images/Library/noImage.gif' ELSE PRODUCT_IMAGE_URL END PRODUCT_IMAGE_URL," & _
                        " CASE (SELECT TOP 1 ISNULL(ITEM_ACTUAL_RETURN_DATE,'') FROM dbo.LIBRARY_TRANSACTIONS WHERE ISNULL(ITEM_ACTUAL_RETURN_DATE,'')='' AND  STOCK_ID=A.STOCK_ID)  WHEN '' THEN '~/Images/cross.png' ELSE '~/Images/tick.gif' end AVALABLE, " & _
                        " CASE (SELECT TOP 1 ISNULL(ITEM_ACTUAL_RETURN_DATE,'') FROM dbo.LIBRARY_TRANSACTIONS WHERE ISNULL(ITEM_ACTUAL_RETURN_DATE,'')='' AND  STOCK_ID=A.STOCK_ID)  WHEN '' THEN 'False' ELSE 'True' end EDIT_ENABLE " & _
                        " FROM  dbo.LIBRARY_ITEMS_QUANTITY A " & _
                        " INNER JOIN dbo.LIBRARY_ITEMS_MASTER BC ON A.MASTER_ID=BC.MASTER_ID " & _
                        " LEFT JOIN dbo.LIBRARY_RACKS B ON A.RACK_ID=B.RACK_ID " & _
                        " LEFT JOIN dbo.LIBRARY_SHELFS C ON C.SHELF_ID=B.SHELF_ID " & _
                        " LEFT JOIN dbo.LIBRARY_SUB_DIVISIONS  D ON D.LIBRARY_SUB_DIVISION_ID=C.LIBRARY_SUB_DIVISION_ID " & _
                        " LEFT JOIN dbo.LIBRARY_DIVISIONS  E ON E.LIBRARY_DIVISION_ID=D.LIBRARY_DIVISION_ID " & _
                        " LEFT JOIN dbo.LIBRARY_ITEM_STATUS F ON F.STATUS_ID=A.STATUS_ID " & _
                        " WHERE A.MASTER_ID='" & HiddenMasterId.Value & "' AND PRODUCT_BSU_ID='" & HiddenBsuID.Value & "' "

        Dim Condition As String = ""

        If CheckNullRack.Checked Then

            Condition = Condition & " AND ISNULL(RTRIM(B.RACK_ID),'') = ''"

        End If

        If CheckStatus.Checked Then

            Condition = Condition & " AND ISNULL(RTRIM(A.STATUS_ID),'')=''"

        End If

        If Condition <> "" Then

            str_query = str_query & Condition & " ORDER BY A.MASTER_ID,A.STOCK_ID "
        Else

            str_query = str_query & " ORDER BY A.MASTER_ID,A.STOCK_ID "
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GridItem.DataSource = ds
        GridItem.DataBind()

        If GridItem.Rows.Count > 0 Then
            Dim barcodebutton As Button = DirectCast(GridItem.FooterRow.FindControl("btnBarcode"), Button)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(barcodebutton)
        End If

    End Sub


    Protected Sub GridItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridItem.PageIndexChanging
        GridItem.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub ddLibraryDivisions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindLibrarySubDivisions()
        MO2.Show()
    End Sub

    Protected Sub ddLibrarySubDivisions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindShelfS()
        MO2.Show()
    End Sub

    Protected Sub ddShelfName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindRacks()
        MO2.Show()
    End Sub

    Protected Sub GridItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        Try

       
            If e.CommandName = "UpdateRack" Then
                MO2.Show()
            End If

            If e.CommandName = "UpdateStatus" Then
                MO3.Show()
            End If
            If e.CommandName = "UpdateAccessionNo" Then
                LibraryData.isOffLine(Session("sBusper"))
                connection.Open()
                transaction = connection.BeginTransaction()
                Dim flag = 0


                For Each row As GridViewRow In GridItem.Rows
                    Dim stock_id As HiddenField = DirectCast(row.FindControl("HiddenBookId"), HiddenField)
                    Dim accession_no As TextBox = DirectCast(row.FindControl("txtAccessionNo"), TextBox)

                    If accession_no.Text.Trim() <> "" Then
                        Dim pParms(3) As SqlClient.SqlParameter
                        pParms(0) = New SqlClient.SqlParameter("@STOCK_ID", stock_id.Value.Trim())
                        pParms(1) = New SqlClient.SqlParameter("@ACCESSION_NO", accession_no.Text.Trim())
                        pParms(2) = New SqlClient.SqlParameter("@PRODUCT_BSU_ID", HiddenBsuID.Value)
                        Dim ret = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_LIBRARY_ITEM_ACCESSION_NO", pParms)
                        If ret = 0 Then
                            'accession_no.BackColor = Drawing.Color.Red
                            accession_no.Attributes.Add("style", "background-color: Red !important;")


                            flag = 1
                        Else
                            'accession_no.BackColor = Drawing.Color.White
                            accession_no.Attributes.Add("style", "background-color: White !important;")
                        End If
                    End If

                Next

                transaction.Commit()
                If flag = 0 Then
                    BindGrid()
                    lblMessage.Text = "Accession Numbers updated successfully.Now you may print the Bar Code."
                Else
                    lblMessage.Text = "Item Marked as Red Cannot be updated because the Accession No has been used by other Item. Please enter unique Accession Number."
                End If

                MO1.Show()

            End If
            If e.CommandName = "UpdateCallNo" Then
                LibraryData.isOffLine(Session("sBusper"))
                connection.Open()
                transaction = connection.BeginTransaction()
                Dim flag = 0


                For Each row As GridViewRow In GridItem.Rows
                    Dim stock_id As HiddenField = DirectCast(row.FindControl("HiddenBookId"), HiddenField)
                    Dim call_no As TextBox = DirectCast(row.FindControl("txtCallNo"), TextBox)

                    If call_no.Text.Trim() <> "" Then
                        Dim pParms(3) As SqlClient.SqlParameter
                        pParms(0) = New SqlClient.SqlParameter("@STOCK_ID", stock_id.Value.Trim())
                        pParms(1) = New SqlClient.SqlParameter("@CALL_NO", call_no.Text.Trim())
                        pParms(2) = New SqlClient.SqlParameter("@PRODUCT_BSU_ID", HiddenBsuID.Value)
                        Dim ret = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_LIBRARY_ITEM_CALL_NO", pParms)
                        If ret = 0 Then
                            call_no.BackColor = Drawing.Color.Yellow
                            flag = 1
                        Else
                            call_no.BackColor = Drawing.Color.White
                        End If
                    End If

                Next

                transaction.Commit()
                If flag = 0 Then
                    BindGrid()
                    lblMessage.Text = "Call Numbers updated successfully."
                Else
                    lblMessage.Text = "Item Marked as Yellow Cannot be updated because the Call No has been used by other Item. Please enter unique Call Number."
                End If

                MO1.Show()

            End If

        Catch ex As Exception
            lblMessage.Text = "Error occurred while transactions." & ex.Message
            transaction.Rollback()
        Finally
            connection.Close()
        End Try

    End Sub

    Protected Sub btnmessageok_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO1.Hide()
    End Sub

    Protected Sub btnstockupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()

        Try
            For Each row As GridViewRow In GridItem.Rows

                Dim Check As CheckBox = DirectCast(row.FindControl("CheckRack"), CheckBox)
                If Check.Checked Then
                    Dim BookId = DirectCast(row.FindControl("HiddenBookId"), HiddenField).Value
                    Dim pParms(2) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@STOCK_ID", BookId)
                    If ddRacks.SelectedValue <> "0" Then
                        pParms(1) = New SqlClient.SqlParameter("@RACK_ID", ddRacks.SelectedValue)
                    End If
                    lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_BOOK_RACK", pParms)

                End If

            Next

            transaction.Commit()
            BindGrid()
            MO2.Hide()
            MO1.Show()
        Catch ex As Exception
            lblMessage.Text = "Error occurred while transactions." & ex.Message
            transaction.Rollback()
        Finally
            connection.Close()

        End Try


    End Sub

    Protected Sub btncancel3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO2.Hide()
    End Sub

    Protected Sub btnStatusupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()

        Try
            For Each row As GridViewRow In GridItem.Rows

                Dim Check As CheckBox = DirectCast(row.FindControl("CheckStatus"), CheckBox)
                If Check.Checked Then
                    Dim BookId = DirectCast(row.FindControl("HiddenBookId"), HiddenField).Value
                    Dim pParms(2) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@STOCK_ID", BookId)
                    pParms(1) = New SqlClient.SqlParameter("@STATUS_ID", ddStatus.SelectedValue)
                    lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_ITEM_STATUS", pParms)

                End If

            Next

            transaction.Commit()
            BindGrid()
            MO3.Hide()
            MO1.Show()
        Catch ex As Exception
            lblMessage.Text = "Error occurred while transactions." & ex.Message
            transaction.Rollback()
        Finally
            connection.Close()

        End Try

    End Sub

    Protected Sub btncancel4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO3.Hide()
    End Sub

    Protected Sub btnBarcode_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        HiddenShowFlag.Value = 1
        Dim hash As New Hashtable

        Session("Barcodehashtable") = Nothing
        Session("BarcodehashtableTemp") = Nothing

        For Each row As GridViewRow In GridItem.Rows
            Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
            Dim Hid As HiddenField = DirectCast(row.FindControl("HiddenBookId"), HiddenField)
            Dim key = Hid.Value
            If ch.Checked Then
                If hash.ContainsKey(key) Then
                Else
                    hash.Add(key, Hid.Value)
                End If

            Else

                If hash.ContainsKey(key) Then
                    hash.Remove(key)
                Else
                End If
            End If
        Next
        If hash.Count = 0 Then
            HiddenShowFlag.Value = 0
        End If
        Session("Barcodehashtable") = hash
        Session("BarcodehashtableTemp") = Nothing
    End Sub

    Protected Sub CheckNullRack_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid()
    End Sub

    Protected Sub CheckStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid()
    End Sub
End Class
