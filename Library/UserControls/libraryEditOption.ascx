﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryEditOption.ascx.vb"
    Inherits="Library_UserControls_libraryEditOption" %>

<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>   
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<div class="matters" align="center" >
<br />
<br />
    <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
<asp:Panel ID="Panel1" runat="server">
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="250">
        <tr>
            <td class="subheader_img">
                Update Rack</td>
        </tr>
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left">
                            Library Divisions
                        </td>
                        <td style="width: 6px">
                            :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddLibraryDivisions" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddLibraryDivisions_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Library Sub Divisions
                        </td>
                        <td style="width: 6px">
                            :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddLibrarySubDivisions" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddLibrarySubDivisions_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Shelf &nbsp;Id
                        </td>
                        <td style="width: 6px">
                            :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddShelfName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddShelfName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Rack Id
                        </td>
                        <td style="width: 6px">
                            :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddRacks" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="3">
                            <asp:Button ID="btnupdaterack" runat="server" CausesValidation="False" CssClass="button"
                                OnClick="btnstockupdate_Click" Text="Update" Width="80px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <ajaxToolkit:ConfirmButtonExtender ID="CF1" runat="server" ConfirmText="Continue with Updation?"
        TargetControlID="btnupdaterack">
    </ajaxToolkit:ConfirmButtonExtender>
</asp:Panel>

<asp:Panel ID="Panel2" runat="server">
<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" 
    width="250">
    <tr>
        <td class="subheader_img">
            Update Status</td>
    </tr>
    <tr>
        <td align="center" >
        <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <br />
            <asp:DropDownList ID="ddStatus" runat="server">
            </asp:DropDownList>
            <br />
            <br />
            <br />
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:Button ID="btnStatusupdate" runat="server" CausesValidation="False" 
                CssClass="button" OnClick="btnStatusupdate_Click" Text="Update" Width="75px" />
        </td>
    </tr>
</table>
        </td>
    </tr>
</table>
</asp:Panel>

<asp:HiddenField ID="Hiddenbsu_id" runat="server" />
<asp:HiddenField ID="HiddenStock_id" runat="server" />

</div>    


</ContentTemplate>
</asp:UpdatePanel>
    
<center>
<br />
<br />

<asp:Button ID="btnclose" runat="server" OnClientClick="javascript:window.close();" CssClass="button" Text="Close" Width="75px" />
</center>
    

