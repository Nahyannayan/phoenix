<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryDetailView.ascx.vb" Inherits="Library_UserControls_libraryDetailView" %>


<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/cssfiles/sb-admin.css" rel="stylesheet"/>
<%--<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">--%>


<div align="center">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td colspan="2" class="title-bg-lite">

                <asp:Label ID="lbltitle" runat="server"></asp:Label>

            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div align="left">
                    <table border="0" cellspacing="0" width="100%">
                        <tr>
                            <td colspan="6" width="100%" class="title-bg-lite">Item Informations</td>
                        </tr>
                        <tr>
                            <td colspan="2" width="30%">
                                <span class="field-label">ISBN</span></td>
                            <td colspan="3" width="40%">
                                <asp:Label ID="lblISBN" runat="server" CssClass="field-value"></asp:Label></td>
                            <td align="center" rowspan="5">
                                <asp:ImageButton ID="ImgItem" runat="server" /></td>
                        </tr>
                        <tr>
                            <td colspan="2" width="30%">
                                <span class="field-label">Author</span></td>
                            <td colspan="3" width="40%">
                                <asp:Label ID="lblAuthor" runat="server" CssClass="field-value"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" width="30%">
                                <span class="field-label">Publisher</span></td>
                            <td colspan="3" width="40%">
                                <asp:Label ID="lblpublisher" runat="server" CssClass="field-value"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" width="30%">
                                <span class="field-label">Format</span></td>
                            <td colspan="3" width="40%">
                                <asp:Label ID="lblformat" runat="server" CssClass="field-value"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left" colspan="2" width="30%">
                                <span class="field-label">Date/Year</span></td>
                            <td colspan="3" width="40%">
                                <asp:Label ID="lbldateyear" runat="server" CssClass="field-value"></asp:Label></td>
                        </tr>
                        <tr id="TR1" runat="server">
                            <td align="left" colspan="2" width="20%">
                                <span class="field-label">Entry Date</span></td>
                            <td align="left" colspan="2" width="30%">
                                <asp:Label ID="lblEntryDate" runat="server" CssClass="field-label"></asp:Label>
                                <asp:Label ID="lblEntryBsu" runat="server" CssClass="field-label"></asp:Label></td>
                            <td align="left" width="30%">
                                <span class="field-label">Total Quantity</span>
                                <asp:Label ID="lblBsu" runat="server" CssClass="field-label"></asp:Label></td>
                            <td align="left" width="20%">
                                <asp:Label ID="lblTotalQuantity" runat="server" CssClass="field-value"></asp:Label></td>
                        </tr>
                        <tr id="TR2" runat="server">
                            <td colspan="2" width="30%">
                                <span class="field-label">Entry Type</span></td>
                            <td colspan="2" width="40%">
                                <asp:Label ID="lblEntryType" runat="server" CssClass="field-value"></asp:Label></td>
                            <td align="left"></td>
                            <td align="left"></td>
                        </tr>
                        <tr>
                            <td colspan="2" width="30%"></td>
                            <td colspan="4"></td>
                        </tr>
                        <tr>
                            <td class="title-bg-lite" colspan="6" width="100%">Item Category/Subjects</td>
                        </tr>
                        <tr align="left">
                            <td colspan="6" rowspan="3" align="left">

                                <asp:GridView ID="GridCategory" runat="server" AutoGenerateColumns="false" EmptyDataText="Information not provided."
                                    CssClass="table table-bordered table-row" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Category/Subjects">

                                            <HeaderTemplate>
                                                Category/Subjects
                                                    
                                            </HeaderTemplate>

                                            <ItemTemplate>

                                                <%#Eval("SUBJECTS")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="griditem" />
                                    <EmptyDataRowStyle />
                                    <SelectedRowStyle />
                                    <HeaderStyle />
                                    <EditRowStyle />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>





                            </td>
                        </tr>
                        <tr></tr>
                        <tr></tr>
                        <tr>
                            <td colspan="6" width="100%"><span class="field-label">Item Description</span></td>
                        </tr>
                        <tr>
                            <td colspan="6" rowspan="3">
                                <asp:TextBox ID="txtItemDescription" runat="server" TextMode="MultiLine"
                                    ReadOnly="True" EnableTheming="False"></asp:TextBox></td>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                </div>
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top"></td>
        </tr>
    </table>
</div>
<asp:HiddenField ID="HiddenMasterId" runat="server" />
<asp:HiddenField ID="HiddenBsuID" runat="server" />
