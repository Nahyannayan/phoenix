<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryLibrarian.ascx.vb"
    Inherits="Library_UserControls_libraryLibrarian" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<script type="text/javascript">
    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("CheckBar") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }

</script>

<div class="matters">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="card mb-3">
                <div class="card-header letter-space">
                    <i class="fa fa-book mr-3"></i>
                    Assign Librarian
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table   width="100%">
                            <tr>
                                <td  width="20%" ><span class="field-label"> Assign  Librarian</span> </td>
                                <td width="30%">
                        <asp:DropDownList ID="dddes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddesschange">
                        </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <asp:GridView ID="GrdStaff" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                        PageSize="15" ShowFooter="True" Width="100%" OnRowCommand="GrdStaff_RowCommand">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Employee Number">
                                                <HeaderTemplate>
                                                Employee Number<br />
                                                                <asp:TextBox ID="txtnumber"  runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="ImageSearch1" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                            <%#Eval("EMPNO")%>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <HeaderStyle HorizontalAlign ="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name">
                                                <HeaderTemplate>Name
                                                    <br />
                                                                <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="ImageSearch" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%# Eval("ENAME") %>
                                                    <asp:HiddenField ID="Hiddenempid" runat="server" Value='<%# Eval("EMP_ID") %>' />
                                                </ItemTemplate>
                                                 <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                <HeaderStyle HorizontalAlign ="Left" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Designation">
                                                <HeaderTemplate>Designation
                                                    <br />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="HiddenDesId" runat="server" Value='<%# Eval("EMP_DES_ID") %>' />
                                                    
                                            <%# Eval("DES_DESCR") %> 
                                                </ItemTemplate>
                                                 <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <HeaderStyle HorizontalAlign ="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Assign">
                                                <FooterTemplate >
                                                     
                                            <asp:Button ID="btnaddmembers" runat="server" CommandName="assign" CssClass="button"
                                                Text="Assign"   /> 
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                  
                                            <br />
                                            <asp:GridView ID="GridLibrary" runat="server"   AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered  table-row"
                                                OnRowCommand="GridLibrary_RowCommand" ShowHeader="False">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HiddenRecordId" Value='<%#Eval("LIBRARIAN_ID")%>' runat="server" />
                                                            <%#Eval("LIBRARY_DIVISION_DES")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkDelete" CommandName="Deleting" CommandArgument='<%#Eval("LIBRARIAN_ID")%>'
                                                                runat="server">Delete</asp:LinkButton>
                                                            <ajaxToolkit:ConfirmButtonExtender ID="CF1" TargetControlID="LinkDelete" ConfirmText="Do you want to delete this record?"
                                                                runat="server">
                                                            </ajaxToolkit:ConfirmButtonExtender>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <asp:CheckBox ID="Checklist" runat="server" />
                                         
                                                </ItemTemplate>
                                                 <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <HeaderStyle HorizontalAlign ="Center" VerticalAlign="Middle" />
                                                <FooterStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="griditem"   Wrap="False" />
                                        <EmptyDataRowStyle Wrap="False" />
                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                        <HeaderStyle CssClass="gridheader_pop"   Wrap="False" />
                                        <EditRowStyle Wrap="False" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                        <asp:Panel ID="PanelMessage" runat="server" BackColor="white" CssClass="panel-cover" Width="100%"
                            Style="display: none">
                            <table    >
                                <tr>
                                    <td class="title-bg">Library Message
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnmok" OnClick="btnmessageok_Click" runat="server" Text="Ok" CssClass="button"
                                                                      ValidationGroup="s" CausesValidation="False"></asp:Button>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                &nbsp;
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
                            DropShadow="true" PopupControlID="PanelMessage" RepositionMode="RepositionOnWindowResizeAndScroll"
                            TargetControlID="Label1">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Label ID="Label3" runat="server"></asp:Label>
                        <asp:Panel ID="PanelStatusUpdate" runat="server" BackColor="white" CssClass="panel-cover"
                            Style="display: none">
                            <table width="100%">
                                <tr>
                                    <td class="title-bg">Library Message
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <table   style="width: 100%">
                                                    <tr>
                                                        <td align="center">
                                                            <table  >
                                                                <tr>
                                                                    <td   align="left"><span class="field-label"> Library Divisions</span>
                                                                    </td>
                                                                    <td   align="left">
                                                                        <asp:DropDownList ID="ddlibraryDivisions" runat="server" Width="100%">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Button ID="btnStatusupdate" runat="server" CausesValidation="False" CssClass="button"
                                                                OnClick="btnStatusupdate_Click" Text="Update"   /><asp:Button ID="btncancel4"
                                                                    runat="server" CausesValidation="False" CssClass="button" OnClick="btncancel4_Click"
                                                                    Text="Cancel"  />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolkit:ModalPopupExtender ID="MO3" runat="server" BackgroundCssClass="modalBackground"
                            CancelControlID="btncancel4" DropShadow="true" Enabled="True" PopupControlID="PanelStatusUpdate"
                            RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="Label3">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:HiddenField ID="Hiddenbsuid" runat="server" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
