<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryEditStockDetails.ascx.vb" Inherits="Library_UserControls_libraryEditStockDetails" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg-lite">Stock Details Edit &amp; Barcode Issuer</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GridItem" runat="server" AllowPaging="True" AutoGenerateColumns="false" Width="100%"
                            CssClass="table table-bordered table-row" ShowFooter="True" OnPageIndexChanging="GridItem_PageIndexChanging">
                            <Columns>

                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Reference No
                                  
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                               <%#Eval("STOCK_ID")%>
                               <asp:HiddenField ID="HiddenStockId" Value='<%#Eval("STOCK_ID")%>' runat="server" />
                               <asp:HiddenField ID="HiddenCurrency" Value='<%#Eval("PRICE_CURRENCY")%>' runat="server" />
                               <asp:HiddenField ID="HiddenAgeFrom" Value='<%#Eval("FROM_AGE_GROUP")%>' runat="server" />
                               <asp:HiddenField ID="HiddenAgeTo" Value='<%#Eval("TO_AGE_GROUP")%>' runat="server" />
                               <asp:HiddenField ID="HiddenActive" Value='<%#Eval("ENABLE_ROW")%>' runat="server" />
                              
                            </center>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Purchase Date
                                    
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <asp:TextBox ID="txtpurchasedate" Text='<% # Eval("PURCHASE_DATE")%>' Enabled='<% # Eval("ENABLE_ROW")%>' ValidationGroup="Edit"  runat="server"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender id="CE1" runat="server" TargetControlID="txtpurchasedate" PopupButtonID="txtpurchasedate" Format="dd/MMM/yyyy"> </ajaxToolkit:CalendarExtender>
                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpurchasedate"
                            Display="None" ErrorMessage="Purchase Date should not be blank" SetFocusOnError="True"
                            ValidationGroup="Edit"></asp:RequiredFieldValidator>--%>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Supplier
                                  
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <asp:TextBox ID="txtsupplier" Text='<%#Eval("SUPPLIER")%>' Enabled='<% # Eval("ENABLE_ROW")%>' runat="server"  ValidationGroup="Edit" ></asp:TextBox>
                                 <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtsupplier"
                            Display="None" ErrorMessage="Please Enter Supplier Details" SetFocusOnError="True"
                            ValidationGroup="Edit"></asp:RequiredFieldValidator>--%>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Cost
                                  
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                            <asp:TextBox ID="txtcost" runat="server" Text='<%#Eval("PRODUCT_PRICE")%>' Enabled='<% # Eval("ENABLE_ROW")%>' width="4%"></asp:TextBox>
                                            <br />
            <asp:DropDownList ID="ddcurrency"   runat="server"></asp:DropDownList>
              <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtcost"
                            Display="None" ErrorMessage="Please Enter Cost Details" SetFocusOnError="True"
                            ValidationGroup="Edit"></asp:RequiredFieldValidator>--%>
                            <ajaxToolkit:FilteredTextBoxExtender id="F2" runat="server" TargetControlID="txtcost" FilterType="Custom, Numbers" ValidChars=".,">
</ajaxToolkit:FilteredTextBoxExtender>
            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Age Group 
                                  
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                          From<asp:DropDownList ID="DDFromAge" Enabled  ='<% # Eval("ENABLE_ROW")%>'   runat="server">
                            </asp:DropDownList> <br /> To <asp:DropDownList ID="DDToAge"   Enabled  ='<% # Eval("ENABLE_ROW")%>' runat="server">
                            </asp:DropDownList></center>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="DDFromAge"
                                            ControlToValidate="DDToAge" Display="None" ErrorMessage='"To Age Group"  Must be greater  than or equal to "From Age Group"'
                                            Operator="GreaterThanEqual" ValidationGroup="Edit" SetFocusOnError="True" Type="Integer"></asp:CompareValidator>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Current Status
                                   
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                        <%# Eval("STATUS_DESCRIPTION")%>
                        </center>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <center><asp:Button ID="btnUpdate" CssClass="button" OnClick="btnUpdate_Click"  ValidationGroup="Edit" runat="server" Text="Update" /></center>
                                    </FooterTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle />
                            <SelectedRowStyle />
                            <HeaderStyle />
                            <EditRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="HiddenMasterId" runat="server" />
            <asp:HiddenField ID="HiddenBsuId" runat="server" />

            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" ValidationGroup="Edit" />

            <asp:Label ID="Label11" runat="server"></asp:Label><br />
            <asp:Panel ID="PanelMessage1" runat="server" CssClass="panel-cover"
                Style="display: none">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite">Library Message</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td align="center">

                                                <asp:Label ID="lblMessage" runat="server" CssClass="text-danger"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnmok1" runat="server" CausesValidation="False" CssClass="button"
                                                    Text="Ok" ValidationGroup="s" /></td>
                                        </tr>
                                    </table>
                                    &nbsp;
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="MO11" runat="server" BackgroundCssClass="modalBackground"
                DropShadow="true" PopupControlID="PanelMessage1" CancelControlID="btnmok1" RepositionMode="RepositionOnWindowResizeAndScroll"
                TargetControlID="Label11">
            </ajaxToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>


</div>


