﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryTransactionReservationApproval.ascx.vb"
    Inherits="Library_UserControls_libraryTransactionReservationApproval" %>


<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

 <style type="text/css">
        .card-body {
        padding: 0.25rem !important;
    }
    table th, table td {
        padding:0.1rem !important;
    }
    </style>

<div>
    
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">Reservation Search
            </td>
        </tr>
        <tr>
            <td align="center">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Library Divisions</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddLibrary" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>

                    <tr>
                        <td align="left">
                            <span class="field-label">Select Grade</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <span class="field-label">Select Section</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>


                    <tr>
                        <td align="left">
                            <span class="field-label">Accession No</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtaccessionnumber" runat="server"></asp:TextBox>
                        </td>
                        <td align="left"><span class="field-label">User No</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtuserid" runat="server"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>

                        <td align="center" colspan="4">
                            <asp:Button ID="btnshow" runat="server" CssClass="button" Text="Show" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:Label ID="lbltemp" runat="server" Text="" CssClass="field-label"></asp:Label>
    <br />
    <asp:Label ID="lblmessage" runat="server" CssClass="text-danger"></asp:Label>
    
    <table width="100%" style="display:none;">
        <tr>
            <td class="title-bg-lite">Item Reservation - Approval - Send Mail
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:GridView ID="GridInfo" runat="server" AutoGenerateColumns="false" EmptyDataText="Information not available."
                    Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Image
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:Image ID="Image1" ImageUrl='<%#Eval("PRODUCT_IMAGE_URL")%>' runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Item Title
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("ITEM_TITLE")%>
                            </ItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField>
                            <HeaderTemplate>
                                Accession No
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("ACCESSION_NO")%>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField>
                            <HeaderTemplate>
                                User No
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("USER_ID")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                User Name
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("USER_NAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                User Type
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%# Eval("USER_TYPE")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Reservation Date
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%# Eval("ENTRY_DATE")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Approved Date
                                     
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%# Eval("APPROVED_DATE")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                AC/RC/RAC
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%# Eval("ACTIVE_COUNT")%>
                                    /
                                    <%# Eval("RESERVE_COUNT")%>
                                    /<%# Eval("RESERVE_APPROVED_COUNT")%></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField>
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Reservation
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="lnkapprove" CommandName="approve" CommandArgument='<%# Eval("RESERVATION_ID")%>'
                                        Visible='<%# Eval("VISIBLE")%>' runat="server">Approve</asp:LinkButton>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Issue
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="lnkissue" CommandName="Issue" CommandArgument='<%# Eval("RESERVATION_ID")%>'
                                        Visible='<%# Eval("VISIBLE")%>' runat="server">Issue</asp:LinkButton>
                                           <ajaxtoolkit:confirmbuttonextender id="cbe1" confirmtext="Do you want to issue to user?"
                        targetcontrolid="lnkissue" runat="server"></ajaxtoolkit:confirmbuttonextender>

                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Reservation
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="lnkcancel" CommandName="rcancel" CommandArgument='<%# Eval("RESERVATION_ID")%>'
                                        Visible='<%# Eval("CANCEL_VISIBLE")%>' runat="server">Cancel</asp:LinkButton>
                                         <ajaxtoolkit:confirmbuttonextender id="cbe2" confirmtext="Do you want to cancel reservation?"
                        targetcontrolid="lnkcancel" runat="server"></ajaxtoolkit:confirmbuttonextender>

                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" />
                    <EmptyDataRowStyle />
                    <SelectedRowStyle />
                    <HeaderStyle />
                    <EditRowStyle />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
                AC -Total item available count, RC -Total reservation count, RAC-Total active reservation
                count (mail sent)
            </td>
        </tr>
    </table>
</div>
