<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryTransaction.ascx.vb" Inherits="Library_UserControls_libraryTransaction" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<script type="text/javascript">


    function UserTransactions(usertype, userid) {

        window.open('libraryDetailViewUserTransactions.aspx?UserType=' + usertype + '&UserId=' + userid, '', 'Height=800px,Width=1020px,scrollbars=yes,resizable=no,directories=yes');
        return false;

    }


</script>


<div>
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />

    <table cellpadding="5" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg">Library Item Reservations Transactions </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td colspan="3">

                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
                                        <ajaxToolkit:TabPanel ID="HT3" runat="server">
                                            <ContentTemplate>
                                                <div>
                                                    <table  cellpadding="5" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="title-bg">Reservation Counter</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%">
                                                                    <tr id="T2" runat="server">
                                                                        <td>Member Type</td>

                                                                        <td>
                                                                            <asp:RadioButtonList ID="RadioUserReservation" runat="server" RepeatDirection="Horizontal">
                                                                                <asp:ListItem Text="STUDENT" Selected="True" Value="STUDENT"></asp:ListItem>
                                                                                <asp:ListItem Text="EMPLOYEE" Value="EMPLOYEE"></asp:ListItem>
                                                                            </asp:RadioButtonList>

                                                                        </td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr id="T1" runat="server">
                                                                        <td>Member ID.</td>

                                                                        <td>
                                                                            <asp:TextBox ID="txtUserreservation" runat="server"
                                                                                ValidationGroup="Reservation" AutoPostBack="True"></asp:TextBox>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Email ID</td>

                                                                        <td>
                                                                            <asp:TextBox ID="txtreservationcontactadd" runat="server" ValidationGroup="Reservation"></asp:TextBox></td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td colspan="3" align="center">
                                                                            <asp:Button ID="btnReserve" runat="server" Text="Reserve" CssClass="button" ValidationGroup="Reservation" />
                                                                            <asp:Button ID="btncancel" runat="server" OnClientClick="window.close();" CssClass="button" Text="Cancel"
                                                                                CausesValidation="false" />
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserreservation"
                                                    Display="None" ErrorMessage="Please Enter Member ID" ValidationGroup="Reservation"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtreservationcontactadd"
                                                    Display="None" ErrorMessage="Please Enter Email ID." ValidationGroup="Reservation" Enabled="false"></asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" ValidationGroup="Reservation" />

                                            </ContentTemplate>
                                            <HeaderTemplate>
                                                Reservation
                                            </HeaderTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel ID="HT4" runat="server">
                                            <ContentTemplate>

                                                <asp:GridView ID="GrdReservation" CssClass="table table-bordered table-row" runat="server" AllowPaging="True" AutoGenerateColumns="False" Width="100%" OnRowCommand="GrdReservation_RowCommand" OnSelectedIndexChanging="GrdReservation_SelectedIndexChanging">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Reservation Date">
                                                            <HeaderTemplate>
                                                                <table  width="100%">
                                                                    <tr >
                                                                        <td align="center" colspan="2">Contact
                                <br />


                                                                            <asp:TextBox ID="txtContact" runat="server"></asp:TextBox>

                                                                            <asp:ImageButton ID="ImageSearch4" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <center>
                        <asp:Image ID="Image3" ImageUrl='<%#Eval("RESERVATION_CANCEL")%>' runat="server" />
                        <asp:Panel id="Show" BorderColor="Black"   runat="server" >
                        <br />
                        <%#Eval("CONTACT_ADDRESS")%>
                        <br />
                        <%#Eval("RESERVE_START_DATE")%>
                        <br />
                        <%#Eval("RESERVE_END_DATE")%></asp:Panel>
                        <ajaxToolkit:HoverMenuExtender ID="hm2" runat="server" TargetControlID="Image3" PopupControlID="Show" PopupPosition="Left" />
                        <br />
                        <%#Eval("CANCEL_RESERVATION")%>
                        <br />
                        <asp:LinkButton ID="LinkCancel" CausesValidation="false" Visible='<%#Eval("RESERVATION_CANCEL_VISIBLE")%>' CommandName="CancelReservation" CommandArgument='<%#Eval("RESERVATION_ID")%>' runat="server">Cancel</asp:LinkButton>
                        <ajaxToolkit:ConfirmButtonExtender ID="CF12" TargetControlID="LinkCancel" ConfirmText="Cancel this reservation ?" runat="server"></ajaxToolkit:ConfirmButtonExtender>  
                    </center>
                                                            </ItemTemplate>

                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="  User ID">
                                                            <HeaderTemplate>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td align="center" colspan="2">Member ID
                                <br />
                                                                            <asp:TextBox ID="txtnumber"  runat="server"></asp:TextBox>
                                                                            <asp:ImageButton ID="ImageSearch1" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>

                                                                <%#Eval("USER_ID")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="User Name">
                                                            <HeaderTemplate>
                                                                <table  width="100%">
                                                                    <tr >
                                                                        <td align="center" colspan="2">Member Name
                                <br />
                                                                            <asp:TextBox ID="txtname"  runat="server"></asp:TextBox>
                                                                            <asp:ImageButton ID="ImageSearch2" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>

                                                                <asp:LinkButton ID="LinkReservationuser" OnClientClick='<%#Eval("USERREDIRECT")%>' runat="server"><%#Eval("USER_NAME")%></asp:LinkButton>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="User Type">
                                                            <HeaderTemplate>
                                                                <table  width="100%">
                                                                    <tr >
                                                                        <td align="center" colspan="2">Member Type
                                <br />
                                                                            <asp:TextBox ID="txtType" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="ImageSearch3" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <center>
                    
                        <%#Eval("USER_TYPE")%>
                        
                    </center>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                                    <EditRowStyle Wrap="False" />
                                                    <EmptyDataRowStyle Wrap="False" />
                                                    <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                                                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                </asp:GridView>
                                            </ContentTemplate>
                                            <HeaderTemplate>
                                                Cancel
            Reservation
                                            </HeaderTemplate>
                                        </ajaxToolkit:TabPanel>
                                    </ajaxToolkit:TabContainer>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:Label ID="Label1" runat="server"></asp:Label><asp:Panel ID="PanelStatusupdate" runat="server" BackColor="white" CssClass="modalPopup"
                                Style="display: none">
                                <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="240">
                                    <tr>
                                        <td class="subheader_img">Library Message</td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
                                                    <table>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnmok" runat="server" CausesValidation="False" CssClass="button"
                                                                    OnClick="btnmessageok_Click" Text="Ok" ValidationGroup="s" Width="80px" /></td>
                                                        </tr>
                                                    </table>
                                                    
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
                                DropShadow="true" PopupControlID="PanelStatusupdate" RepositionMode="RepositionOnWindowResizeAndScroll"
                                TargetControlID="Label1">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:HiddenField ID="HiddenMasterID" runat="server" />
                            <asp:HiddenField ID="HiddenBsuID" runat="server" />
                            <asp:HiddenField ID="HiddenUserID" runat="server" />
                            <asp:HiddenField ID="HiddenEmpid" runat="server" />
                            <asp:HiddenField ID="Hiddenlibrarydivid" runat="server" />
                            <asp:HiddenField ID="HiddenReservationId" runat="server" />
                            
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</div>
