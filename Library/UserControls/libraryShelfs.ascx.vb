Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Partial Class Library_UserControls_libraryShelfs
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            BindLibraryDivisions()
            BindLibrarySubDivisions()
            BindGrid()
            BindRows(ddRow, "")
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub
    Public Sub BindRows(ByVal Drop As DropDownList, ByVal selectstring As String)

        Dim i = 0
        For i = 0 To 49
            Dim item As New ListItem
            item.Text = "Row " & i + 1
            item.Value = "Row " & i + 1
            Drop.Items.Insert(i, item)

        Next

        Dim norow As New ListItem
        norow.Text = "No Row"
        norow.Value = "No Row"
        Drop.Items.Insert(0, norow)

        If selectstring <> "" Then
            Drop.SelectedValue = selectstring
        End If

    End Sub
    Public Sub BindLibraryDivisions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddLibraryDivisions.DataSource = ds
        ddLibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
        ddLibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
        ddLibraryDivisions.DataBind()

    End Sub

    Public Sub BindLibrarySubDivisions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_SUB_DIVISIONS " & _
                        "WHERE LIBRARY_DIVISION_ID='" & ddLibraryDivisions.SelectedValue & "' ORDER BY LIBRARY_SUB_DIVISION_DES "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddLibrarySubDivisions.DataSource = ds
        ddLibrarySubDivisions.DataTextField = "LIBRARY_SUB_DIVISION_DES"
        ddLibrarySubDivisions.DataValueField = "LIBRARY_SUB_DIVISION_ID"
        ddLibrarySubDivisions.DataBind()
        lblMessage.Text = ""
        If ds.Tables(0).Rows.Count > 0 Then
            btnSave.Visible = True
        Else
            btnSave.Visible = False
        End If

    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " SELECT SHELF_ID,A.LIBRARY_SUB_DIVISION_ID,SHELF_NAME,SHELF_DES,SHELF_ROW,SHELF_ROUT_DES,LIBRARY_DIVISION_DES,LIBRARY_SUB_DIVISION_DES, " & _
                        " '<span style=''color: red;font-weight:bold''>Hide</span>' as hide, " & _
                        " (substring(SHELF_DES,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview1, " & _
                        " (substring(SHELF_ROUT_DES,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview2 " & _
                        " FROM dbo.LIBRARY_SHELFS A " & _
                        " INNER JOIN dbo.LIBRARY_SUB_DIVISIONS B ON A.LIBRARY_SUB_DIVISION_ID=B.LIBRARY_SUB_DIVISION_ID " & _
                        " INNER JOIN dbo.LIBRARY_DIVISIONS C ON C.LIBRARY_DIVISION_ID= B.LIBRARY_DIVISION_ID " & _
                        " WHERE C.LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY SHELF_NAME  "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GridShelfs.DataSource = ds
        GridShelfs.DataBind()


    End Sub

    Protected Sub ddLibraryDivisions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindLibrarySubDivisions()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try
            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@LIBRARY_SUB_DIVISION_ID", ddLibrarySubDivisions.SelectedValue)
            pParms(1) = New SqlClient.SqlParameter("@SHELF_NAME", txtShelfName.Text.Trim().ToUpper())
            pParms(2) = New SqlClient.SqlParameter("@SHELF_DES", txtShelfDescription.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@SHELF_ROW", ddRow.SelectedValue)
            pParms(4) = New SqlClient.SqlParameter("@SHELF_ROUT_DES", txtRoutDescription.Text.Trim())
            pParms(5) = New SqlClient.SqlParameter("@OPTIONS", 1)
            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_SHELFS", pParms)
            transaction.Commit()
            BindGrid()
            txtShelfName.Text = ""
            txtShelfDescription.Text = ""
            txtRoutDescription.Text = ""


        Catch ex As Exception
            transaction.Rollback()

        Finally
            connection.Close()

        End Try


    End Sub

    Protected Sub GridShelfs_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GridShelfs.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridShelfs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)

        If e.CommandName = "deleting" Then
            LibraryData.isOffLine(Session("sBusper"))
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblMessage.Text = ""
            Try
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@SHELF_ID", e.CommandArgument)
                pParms(1) = New SqlClient.SqlParameter("@OPTIONS", 3)

                lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_SHELFS", pParms)
                transaction.Commit()
                BindGrid()
            Catch ex As Exception
                lblMessage.Text = "Error occurred while transactions." & ex.Message
                transaction.Rollback()

            Finally
                connection.Close()

            End Try

        End If

    End Sub

    Protected Sub GridShelfs_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs)
        GridShelfs.EditIndex = "-1"
        BindGrid()
    End Sub

    Protected Sub GridShelfs_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs)
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try
            Dim HiddenSubLibraryDId = DirectCast(GridShelfs.Rows(e.RowIndex).FindControl("HiddenSubLibraryDId"), HiddenField).Value
            Dim HiddenShelfId = DirectCast(GridShelfs.Rows(e.RowIndex).FindControl("HiddenShelfId"), HiddenField).Value
            Dim txtShelfName = DirectCast(GridShelfs.Rows(e.RowIndex).FindControl("txtDataEdit"), TextBox).Text.Trim()
            Dim txtShelfDes = DirectCast(GridShelfs.Rows(e.RowIndex).FindControl("txtShelfDescription"), TextBox).Text.Trim()
            Dim txtShelfRoutDes = DirectCast(GridShelfs.Rows(e.RowIndex).FindControl("txtRoutDescription"), TextBox).Text.Trim()
            Dim row = DirectCast(GridShelfs.Rows(e.RowIndex).FindControl("DropRowEdit"), DropDownList).SelectedValue
            'Dim CheckETeach = DirectCast(GridShelfs.Rows(e.RowIndex).FindControl("CheckETeach"), CheckBox).Checked


            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SHELF_ID", HiddenShelfId)
            pParms(1) = New SqlClient.SqlParameter("@LIBRARY_SUB_DIVISION_ID", HiddenSubLibraryDId)
            pParms(2) = New SqlClient.SqlParameter("@SHELF_NAME", txtShelfName)
            pParms(3) = New SqlClient.SqlParameter("@SHELF_DES", txtShelfDes)
            pParms(4) = New SqlClient.SqlParameter("@SHELF_ROUT_DES", txtShelfRoutDes)
            pParms(5) = New SqlClient.SqlParameter("@SHELF_ROW", row)
            'pParms(6) = New SqlClient.SqlParameter("@SHELF_TEACHER", CheckETeach)
            pParms(7) = New SqlClient.SqlParameter("@OPTIONS", 2)

            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_SHELFS", pParms)
            transaction.Commit()
            GridShelfs.EditIndex = "-1"
            BindGrid()
        Catch ex As Exception
            lblMessage.Text = "Error occurred while transactions. " & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()

        End Try


    End Sub

    Protected Sub GridShelfs_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        GridShelfs.EditIndex = e.NewEditIndex
        BindGrid()
        Dim DropRowEdit As DropDownList = DirectCast(GridShelfs.Rows(e.NewEditIndex).FindControl("DropRowEdit"), DropDownList)
        Dim HiddenRow As HiddenField = DirectCast(GridShelfs.Rows(e.NewEditIndex).FindControl("HiddenRow"), HiddenField)
        If HiddenRow.Value.Trim() = "" Then
            HiddenRow.Value = "No Row"
        End If
        BindRows(DropRowEdit, HiddenRow.Value)

    End Sub
End Class
