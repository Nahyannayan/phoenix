﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Library_UserControls_libraryTransactionReservationApproval
    Inherits System.Web.UI.UserControl


    Private _Accession_no As String
    Public Property AccesionNo() As String
        Get
            Return _Accession_no
        End Get
        Set(ByVal value As String)
            _Accession_no = value
            txtaccessionnumber.Text = _Accession_no
            BindGrid()
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            callGrade_ACDBind()
            BindLibraryDivisions()
            If Request.QueryString("accno") <> "" Then
                txtaccessionnumber.Text = Request.QueryString("accno").ToString()
            End If


            'BindGrid()
        End If

    End Sub

    Public Sub BindLibraryDivisions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim str_query = ""
        If Library.LibrarySuperAcess(Session("EmployeeId")) Then

            str_query = "SELECT LIBRARY_DIVISION_ID,LIBRARY_DIVISION_DES FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & Session("sbsuid") & "' ORDER BY LIBRARY_DIVISION_DES "
        Else
            str_query = " SELECT B.LIBRARY_DIVISION_ID,B.LIBRARY_DIVISION_DES FROM dbo.LIBRARY_LIBRARIAN A " & _
                                  " INNER JOIN dbo.LIBRARY_DIVISIONS B ON A.LIBRARY_DIVISION_ID=B.LIBRARY_DIVISION_ID " & _
                                  " WHERE LIBRARIAN_EMP_ID='" & Session("EmployeeId") & "' AND ACTIVE='True' AND LIBRARY_BSU_ID='" & Session("sbsuid") & "' "


        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddLibrary.DataSource = ds
        ddLibrary.DataTextField = "LIBRARY_DIVISION_DES"
        ddLibrary.DataValueField = "LIBRARY_DIVISION_ID"
        ddLibrary.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows.Count > 1 Then
                Dim list As New ListItem
                list.Value = "0"
                list.Text = "Select a library"
                ddLibrary.Items.Insert(0, list)
            Else

                BindGrid()

            End If
        Else
            lblMessage.Text = "Login User is not a librarian. Please assign librarian and proceed further. "
            ddLibrary.Visible = False
        End If

    End Sub

    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()

        Dim pParms(16) As SqlClient.SqlParameter

        If txtaccessionnumber.Text <> "" Then
            pParms(0) = New SqlClient.SqlParameter("@ACCESSION_NO", txtaccessionnumber.Text)
        End If

        If txtuserid.Text <> "" Then
            pParms(1) = New SqlClient.SqlParameter("@USER_ID", txtuserid.Text)
        End If

        pParms(2) = New SqlClient.SqlParameter("@PRODUCT_BSU_ID", Session("sbsuid"))
        pParms(3) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", ddLibrary.SelectedValue)
        pParms(4) = New SqlClient.SqlParameter("@OPTION", 1)
        pParms(5) = New SqlClient.SqlParameter("@STU_GRM_ID", ddlGrade.SelectedItem.Value)
        pParms(6) = New SqlClient.SqlParameter("@SCT_ID", ddlSection.SelectedItem.Value)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "LIBRARY_RESERVATION_APPROVAL", pParms)

        GridInfo.DataSource = ds
        GridInfo.DataBind()


    End Sub


    Protected Sub GridInfo_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging

        GridInfo.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub

    Public Sub issueItem(ByVal reservationid As String, Optional ByVal Pouch As Boolean = False)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim query = " select TOP 1  ACCESSION_NO,A.USER_ID,A.USER_TYPE,A.LIBRARY_DIVISION_ID,MEMBERSHIP_ID from LIBRARY_ITEM_RESERVATION a " & _
                    " inner join VIEW_LIBRARY_RECORDS  b on a.MASTER_ID=B.MASTER_ID AND A.LIBRARY_DIVISION_ID=B.LIBRARY_DIVISION_ID  " & _
                    " INNER JOIN dbo.LIBRARY_MEMBERSHIP_USERS C ON C.LIBRARY_DIVISION_ID=A.LIBRARY_DIVISION_ID AND C.USER_ID=A.USER_ID " & _
                    " where AVAILABILITY='Y' and RESERVATION_ID=" & reservationid & " "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim Hash As New Hashtable
            Hash.Add("HiddenBsuID", Session("sbsuid"))
            Hash.Add("txtstockid", ds.Tables(0).Rows(0).Item("ACCESSION_NO").ToString())
            Hash.Add("HiddenUserID", ds.Tables(0).Rows(0).Item("USER_ID").ToString())
            Hash.Add("RadioUserTypeIssueItem", ds.Tables(0).Rows(0).Item("USER_TYPE").ToString())
            Hash.Add("ddissuestatus", "0")
            Hash.Add("txtissuenotes", "")
            Hash.Add("HiddenEmpid", Session("EmployeeId"))
            Hash.Add("ddLibrary", ds.Tables(0).Rows(0).Item("LIBRARY_DIVISION_ID").ToString())
            Hash.Add("Hiddenlibrarydivid", ds.Tables(0).Rows(0).Item("LIBRARY_DIVISION_ID").ToString())
            Hash.Add("ddmemebershipissue", ds.Tables(0).Rows(0).Item("MEMBERSHIP_ID").ToString())
            Hash.Add("btnIssue", lbltemp)
            Hash.Add("Pouch", Pouch)
            lblmessage.Text = LibraryTransactions.IssueItems(Hash)
        Else
            lblmessage.Text = "Item cant be issued"
        End If


    End Sub

    Protected Sub GridInfo_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridInfo.RowCommand

        lblmessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim query = ""
        Dim flag = 0


        If e.CommandName = "approve" Or e.CommandName = "Issue" Then

            If e.CommandName = "Issue" Then

                issueItem(e.CommandArgument)

                If lblmessage.Text.IndexOf("successfully") > -1 Then
                    flag = 1
                End If

            End If

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            Try

                query = " UPDATE LIBRARY_ITEM_RESERVATION SET APPROVED=1,APPROVED_DATE=GETDATE() WHERE RESERVATION_ID='" & e.CommandArgument & "' "
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)


                If e.CommandName = "approve" Then
                    lblmessage.Text = "Item approved successfully.Mail has been sent."
                    flag = 1
                End If


                If flag = 1 Then
                    transaction.Commit()
                Else
                    transaction.Rollback()
                End If

            Catch ex As Exception
                lblmessage.Text = "Error while transaction. Error :" & ex.Message
                transaction.Rollback()
            Finally
                connection.Close()

            End Try





        End If


        If e.CommandName = "rcancel" Then
            query = "UPDATE LIBRARY_ITEM_RESERVATION SET TRAN_RECORD_ID=0 , RESERVATION_CANCEL='True' WHERE RESERVATION_ID='" & e.CommandArgument & "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, query)
            lblMessage.Text = "Reservation cancellation done successfully"

        End If


        BindGrid()
    End Sub




    Protected Sub btnshow_Click(sender As Object, e As System.EventArgs) Handles btnshow.Click
        BindGrid()
    End Sub

    Protected Sub ddLibrary_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddLibrary.SelectedIndexChanged
        'BindGrid()
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            ACD_ID = Session("Current_ACD_ID")
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGrade.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While

                End If
            End Using
            ddlSection.Items.Clear()
            di = New ListItem("ALL", "0")
            ddlSection.Items.Add(di)


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            ACD_ID = Session("Current_ACD_ID")
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty

            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section(Session("sBsuid"), ACD_ID, GRD_ID, SHF_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub
End Class
