Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Xml
Imports GoogleBooks.GoogleBooks


Partial Class Library_UserControls_libraryBookEntry
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenDirect1.Value = 0
            Image2.Visible = False
            HiddenBsuID.Value = Session("sbsuid")
            HiddenEmpid.Value = Session("EmployeeId")
            BindCurrency()
            BindCategory()
            BindFromTo()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnOk)
    End Sub

    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = "Select ITEM_ID,ITEM_DES from  LIBRARY_ITEMS where ITEM_PRI_ID='" & childnodeValue & "' AND ITEM_BSU_ID='" & HiddenBsuID.Value & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("ITEM_DES").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("ITEM_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If


        For Each node As TreeNode In ParentNode.ChildNodes
            BindChildNodes(node)
        Next


    End Sub

    Public Sub BindCategory()
        TreeItemCategory.Nodes.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()

        Dim Sql_Query = "Select ITEM_ID,ITEM_DES from  LIBRARY_ITEMS where ITEM_PRI_ID=0 AND ITEM_BSU_ID='" & HiddenBsuID.Value & "' or ITEM_ID='1' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("ITEM_DES").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("ITEM_ID").ToString()
                TreeItemCategory.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In TreeItemCategory.Nodes
            BindChildNodes(node)
        Next

        TreeItemCategory.CollapseAll()

        If TreeItemCategory.Nodes.Count > 0 Then
            TreeItemCategory.Nodes(0).Checked = True '' Book Checked
        End If

    End Sub

    Public Sub BindFromTo()
        Dim i
        For i = 0 To 70

            Dim list As New ListItem
            list.Value = i + 5
            list.Text = i + 5

            DDFromAge.Items.Insert(i, list)
            DDToAge.Items.Insert(i, list)
        Next
    End Sub

    Public Sub BindCurrency()
        Dim str_conn = ConfigurationManager.ConnectionStrings("MainDB").ConnectionString
        Dim str_query = "SELECT CUR_ID,upper(CUR_DESCR)CUR_DESCR FROM CURRENCY_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddcurrency.DataSource = ds
        ddcurrency.DataTextField = "CUR_DESCR"
        ddcurrency.DataValueField = "CUR_ID"
        ddcurrency.DataBind()

    End Sub

    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click

        Try

            Dim hash As Hashtable = GoogleBookData(txtISBN.Text.Trim())

            If hash("DataFound") = "YES" Then

                HiddenIsbnEnc.Value = hash("ISBNEnc")
                txttitle.Text = hash("title")
                txtisbndata.Text = hash("isbndata")
                txtauthor.Text = hash("Author")
                txtpublisher.Text = hash("Publisher")
                txtdateyear.Text = hash("Year")
                txtpages.Text = hash("Pages")
                txtsubjects.Text = hash("Subject").ToString().Replace("&amp;", "")
                txtdesc.Text = hash("Description")

                If hash.ContainsKey("ImageUrl") = False Then
                    Image2.Visible = True
                    Image2.ImageUrl = "~\Images\Library\noImage.gif"
                Else
                    Image2.Visible = True
                    HiddenImageUrl.Value = hash("ImageUrl")
                    Image2.ImageUrl = HiddenImageUrl.Value
                End If

                If hash.ContainsKey("InfoUrl") = True Then
                    HiddenProductUrl.Value = hash("InfoUrl")
                End If

                HiddenShowImage.Value = "1"

                ''Show alert if there item exists in database.
                CheckPreviousDataISBN()

            Else
                lblMessage.Text = " Please Check,ISBN Number not Valid."
                MO1.Show()
                ClearControls()
            End If
        Catch ex As Exception

            lblMessage.Text = " Error : " & ex.Message
            MO1.Show()
            ClearControls()

        End Try

    End Sub

    Public Function CheckPreviousDataISBN() As Boolean

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim Returnvalue = False

        If txtisbndata.Text.Trim() <> "" Then

            Dim val = txtisbndata.Text.Trim().Replace(":", "").Replace(",", "")
            Dim isbn As String()
            isbn = Regex.Split(val, "ISBN")

            Dim condition = ""
            Dim flag = 0

            Dim i = 0
            For i = 0 To isbn.Length - 1

                If isbn(i).Trim() <> "" Then
                    If flag = 0 Then
                        condition = "ISBN Like '%ISBN:" & isbn(i).Trim() & "%'"
                        flag = 1
                    Else
                        condition = condition & " OR ISBN Like '%ISBN:" & isbn(i).Trim() & "%'"
                    End If


                End If
            Next

            If condition <> "" Then
                Dim Sql_Query = "SELECT '<img src='''+ CASE ISNULL(PRODUCT_IMAGE_URL,'') WHEN '' THEN 'https://school.gemsoasis.com/Images/Library/noImage.gif' ELSE PRODUCT_IMAGE_URL END +'''></img> <br> ' + ITEM_TITLE  as imgurl ,MASTER_ID from LIBRARY_ITEMS_MASTER WHERE " & condition
                Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.Text, Sql_Query)
                If ds.Tables(0).Rows.Count > 0 Then

                    lblMessage.Text = "Item already Exists.<br>Please update the additional stock from the Global Item Search."
                    MO1.Show()
                    Returnvalue = False

                Else
                    Returnvalue = True
                End If

            Else
                lblMessage.Text = "ISBN Number entry not in correct format. <br>  Format: ISBN:XXXXXXXXX , ISBN:YYYYYYYYYY"
                MO1.Show()
                Returnvalue = False
            End If

        Else

            lblMessage.Text = "ISBN Number not found. <br>  Format: ISBN:XXXXXXXXX , ISBN:YYYYYYYYYY"
            MO1.Show()
            Returnvalue = False

        End If



        Return Returnvalue

    End Function

    Public Function GetNodeValue(ByVal ParentNode As TreeNode) As String
        Dim val = ""

        For Each node As TreeNode In ParentNode.ChildNodes
            If node.Checked Then
                val = node.Value
                'node.Checked = False
                Exit For
            Else
                val = GetNodeValue(node)

                If val <> "" Then
                    Exit For
                End If

            End If

        Next

        Return val

    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        LibraryData.isOffLine(Session("sBusper"))
        If CheckPreviousDataISBN() Then 'CheckPreviousDataISBN()

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblMessage.Text = ""
            Dim LibraryMessage As String = ""

            Try
                Dim val = ""
                For Each node As TreeNode In TreeItemCategory.Nodes
                    If node.Checked Then
                        val = node.Value
                        'node.Checked = False
                        Exit For
                    Else
                        val = GetNodeValue(node)
                        If val <> "" Then
                            Exit For
                        End If
                    End If
                Next

                If val = "" Then
                    val = 1 ' Book
                End If

                If txtcost.Text = "" Then
                    txtcost.Text = 0
                End If

                Dim pParms(22) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ITEM_ID", val) ''Book
                pParms(1) = New SqlClient.SqlParameter("@ITEM_TITLE", txttitle.Text.Trim())
                pParms(2) = New SqlClient.SqlParameter("@AUTHOR", txtauthor.Text.Trim())
                pParms(3) = New SqlClient.SqlParameter("@PUBLISHER", txtpublisher.Text.Trim())
                pParms(4) = New SqlClient.SqlParameter("@DATE_YEAR", txtdateyear.Text.Trim())
                pParms(5) = New SqlClient.SqlParameter("@ISBN", txtisbndata.Text.Trim())
                pParms(6) = New SqlClient.SqlParameter("@ISBN_ENC", HiddenIsbnEnc.Value)
                pParms(7) = New SqlClient.SqlParameter("@FORMAT", txtpages.Text.Trim())
                pParms(8) = New SqlClient.SqlParameter("@SUBJECT", txtsubjects.Text.Trim())
                pParms(9) = New SqlClient.SqlParameter("@PRODUCT_URL", HiddenProductUrl.Value)
                pParms(10) = New SqlClient.SqlParameter("@PRODUCT_IMAGE_URL", HiddenImageUrl.Value)
                pParms(11) = New SqlClient.SqlParameter("@PRODUCT_DESCRIPTION", txtdesc.Text.Trim())
                pParms(12) = New SqlClient.SqlParameter("@PRODUCT_PRICE", txtcost.Text.Trim())
                pParms(13) = New SqlClient.SqlParameter("@PRICE_CURRENCY", ddcurrency.SelectedValue)
                pParms(14) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", HiddenBsuID.Value)
                pParms(15) = New SqlClient.SqlParameter("@QUANTITY", txtQuantity.Text.Trim())
                pParms(16) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", HiddenEmpid.Value)
                pParms(17) = New SqlClient.SqlParameter("@FROM_AGE_GROUP", DDFromAge.SelectedValue)
                pParms(18) = New SqlClient.SqlParameter("@TO_AGE_GROUP", DDToAge.SelectedValue)
                pParms(19) = New SqlClient.SqlParameter("@SUPPLIER", txtsupplier.Text.Trim())
                If txtpurchasedate.Text.Trim() <> "" Then
                    pParms(20) = New SqlClient.SqlParameter("@PURCHASE_DATE", txtpurchasedate.Text.Trim())
                End If
                pParms(21) = New SqlClient.SqlParameter("@ENTRY_TYPE", "Google Book-Provider")


                LibraryMessage = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_LIBRARY_ITEMS_MASTER", pParms)
                transaction.Commit()
                HiddenMasterId1.Value = LibraryMessage.Trim()
                HiddenDirect1.Value = 1
                LibraryMessage = "Item Details added successfully.<br> You will be redirected to a page to Updated the Item Rack and Status details.<br>You may also change the Accession and Call Numbers if necessary."
                ClearControls()

            Catch ex As Exception
                transaction.Rollback()
                LibraryMessage = "Error occured while saving . " & ex.Message
            Finally
                connection.Close()

            End Try
            lblMessage.Text = LibraryMessage
            MO1.Show()

        End If

    End Sub

    Public Sub ClearControls()
        txtISBN.Text = ""
        txttitle.Text = ""
        txtauthor.Text = ""
        txtpublisher.Text = ""
        txtdateyear.Text = ""
        txtisbndata.Text = ""
        txtpages.Text = ""
        txtsubjects.Text = ""
        txtdesc.Text = ""
        txtcost.Text = ""
        txtQuantity.Text = ""
        txtsupplier.Text = ""
        txtpurchasedate.Text = ""
        Image2.Visible = False

    End Sub

    Protected Sub btnmessageok_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO1.Hide()
    End Sub

End Class
