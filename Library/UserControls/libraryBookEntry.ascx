<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryBookEntry.ascx.vb" Inherits="Library_UserControls_libraryBookEntry" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


  <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template--> 
    <link href="../../cssfiles/sb-admin.css" rel="stylesheet" >


<script type="text/javascript" >

function listEntries(booksInfo) 
{
  
  var div = document.getElementById("data");
  if (div.firstChild) div.removeChild(div.firstChild);

  var mainDiv = document.createElement("div");
  
  for (i in booksInfo) 
  {
    //Create a DIV for each book
    var book = booksInfo[i];
       
    var thumbnailDiv = document.createElement("div");
    thumbnailDiv.className = "thumbnail";

    // Add a link to each book's informtaion page
    var a = document.createElement("a");
    a.href = book.info_url;
    // Display a thumbnail of the book's cover
    var img = document.createElement("img");
    img.src = book.thumbnail_url; 
    a.appendChild(img);
    
    document.getElementById('<%= HiddenImageUrl.ClientID %>').value = book.thumbnail_url; 
    document.getElementById('<%= HiddenProductUrl.ClientID %>').value = book.info_url;
    
    
    thumbnailDiv.appendChild(a);

    mainDiv.appendChild(thumbnailDiv);
    
  }
  
  div.appendChild(mainDiv);

}


function search(query)
{
  // Clear any old data to prepare to display the Loading... message.
  var div = document.getElementById("data");
  if (div.firstChild) div.removeChild(div.firstChild);

  // Show a "Loading..." indicator.
  var div = document.getElementById('data');
  var p = document.createElement('p');
  p.appendChild(document.createTextNode('Loading...'));
  div.appendChild(p);

  // Delete any previous Google Booksearch JSON queries.
  var jsonScript = document.getElementById("jsonScript");
  
  if (jsonScript) 
  {
  
    jsonScript.parentNode.removeChild(jsonScript);
  }
     
  // Add a script element with the src as the user's Google Booksearch query. 
  // JSON output is specified by including the alt=json-in-script argument
  // and the callback funtion is also specified as a URI argument.
  var scriptElement = document.createElement("script");
 
  scriptElement.setAttribute("id", "jsonScript");
  
  var isbnvalue = document.getElementById('<%= txtISBN.ClientID %>').value ;

  scriptElement.setAttribute("src","http://books.google.com/books?bibkeys=" + escape(isbnvalue) + "&jscmd=viewapi&callback=listEntries");
      
  scriptElement.setAttribute("type", "text/javascript");
  // make the request to Google booksearch
  document.documentElement.firstChild.appendChild(scriptElement);
  
}


</script>

<script type="text/javascript">



function OnTreeClick(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");

if(isChkBoxClick)
{
                        for(i=0; i<document.forms[0].elements.length; i++)
                           {
                               var currentid =document.forms[0].elements[i].id; 
                               if(document.forms[0].elements[i].type=="checkbox" && currentid != event.srcElement.id)
                                  {
                                    document.forms[0].elements[i].checked=false;
                                  }
                            }
}

}


function RedirectEdit1()
{
var value= document.getElementById('<%= HiddenMasterId1.ClientID %>').value;
var direct=document.getElementById('<%= HiddenDirect1.ClientID %>').value;
if (direct == 1)
{
document.getElementById('<%= HiddenDirect1.ClientID %>').value=0;
window.open('libraryEditingItem.aspx?MasterId='+ value +'&Edit=0' , '','Height=800px,Width=1020px,scrollbars=yes,resizable=no,directories=yes');
}
return false; 
} 




</script>

<div class="matters">
<asp:UpdatePanel id="UpdatePanel1" runat="server">
    <contenttemplate>

<table border="0"  cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="left" width="20%">
           <span class="field-label"> Enter ISBN</span></td>
        <td align="left" width="30%">
<asp:TextBox ID="txtISBN" runat="server" EnableTheming="False" ValidationGroup="ISBN" ></asp:TextBox></td>
        <td align="left" width="20%">
<asp:Button ID="btnOk" runat="server"  Text="Google Book Search"  CssClass="button"  /></td>
        <td align="left" width="30%"></td>
    </tr>
    <tr>
        <td colspan="4" class="title-bg-lite">
            Primary Informations</td>
    </tr>
    <tr>
        <td align="left" >
           <span class="field-label"> Item Title</span></td>
        <td align="left" >
            <asp:TextBox ID="txttitle" runat="server" EnableTheming="False" ></asp:TextBox></td>
        <td align="left" width="20%"></td>
        <td rowspan="6" align="center">
            <div id="data" >
                <asp:Image ID="Image2" runat="server" /></div>
        </td>
    </tr>
    <tr>
        <td align="left" >
           <span class="field-label"> Author</span></td>
        <td align="left" >
            <asp:TextBox ID="txtauthor" runat="server" EnableTheming="False" ></asp:TextBox></td>
         <td align="left" width="20%"></td>
    </tr>
    <tr>
        <td align="left" >
           <span class="field-label">  Publisher</span></td>
        <td align="left" >
            <asp:TextBox ID="txtpublisher" runat="server" EnableTheming="False" ></asp:TextBox></td>
        <td align="left" width="20%"></td>
    </tr>
    <tr>
        <td align="left" >
          <span class="field-label">   Date/Year</span></td>
        <td align="left" >
            <asp:TextBox ID="txtdateyear" runat="server" EnableTheming="False" ></asp:TextBox></td>
        <td align="left" width="20%"></td>
    </tr>
    <tr>
        <td align="left" >
           <span class="field-label">  ISBN</span></td>
        <td align="left" >
            <asp:TextBox ID="txtisbndata" runat="server" EnableTheming="False" ReadOnly="True"></asp:TextBox></td>
        <td align="left" width="20%"></td>
    </tr>
    <tr>
        <td align="left" >
           <span class="field-label">  No. of Pages</span></td>
        <td align="left" >
            <asp:TextBox ID="txtpages" runat="server" EnableTheming="False" ></asp:TextBox></td>
        <td align="left" width="20%"></td>
    </tr>
    <tr>
        <td align="left" >
           <span class="field-label">  Subjects</span></td>
        <td align="left" >
            <asp:TextBox ID="txtsubjects" runat="server" EnableTheming="False" TextMode="MultiLine" ></asp:TextBox></td>
        <td align="left" width="20%"></td>
        <td rowspan="2" >
            &nbsp;</td>
    </tr>
    <tr>
        <td align="left" >
           <span class="field-label">  Item Description</span></td>
        <td align="left" >
            <asp:TextBox ID="txtdesc" runat="server" EnableTheming="False" TextMode="MultiLine"></asp:TextBox></td>
        <td align="left" width="20%"></td>
    </tr>
    <tr>
        <td class="title-bg-lite" colspan="4">
            Other Details</td>
    </tr>
    <tr>
        <td align="left">
            <span class="field-label"> Item Type</span></td>
        <td align="left" >
            <div class="checkbox-list" >
            <asp:TreeView ID="TreeItemCategory" runat="server" 
                 ImageSet="Arrows" 
                onclick="OnTreeClick(event);" ShowCheckBoxes="All">
                <ParentNodeStyle Font-Bold="False" />
                <HoverNodeStyle Font-Underline="True"  />
                <SelectedNodeStyle Font-Underline="True"  
                    HorizontalPadding="0px" VerticalPadding="0px" />
                <NodeStyle HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />
            </asp:TreeView></div>
        </td>
        <td align="left" width="20%"></td>
        <td rowspan="1">
            &nbsp;</td>
    </tr>
        <tr>
        <td align="left">
            <span class="field-label"> Quantity</span></td>
        <td align="left" rowspan="1">
            <asp:TextBox ID="txtQuantity" runat="server" ></asp:TextBox>
        </td>
            <td align="left" width="20%"></td>
    </tr>
    <tr>
        <td align="left" >
           <span class="field-label">  Age Group</span></td>
        <td rowspan="1">
            <span class="field-label">  From </span>
            <asp:DropDownList ID="DDFromAge" runat="server">
            </asp:DropDownList></td>
            <td align="left" >  <span class="field-label"> To </span><asp:DropDownList ID="DDToAge" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
           <span class="field-label">  Supplier</span></td>
        <td  rowspan="1">
            <asp:TextBox ID="txtsupplier" runat="server" ></asp:TextBox>
        </td>
          <td align="left" width="20%"></td>
    </tr>
    <tr>
        <td>
           <span class="field-label">  Purchase Date</span></td>
        <td  rowspan="1">
            <asp:TextBox ID="txtpurchasedate" runat="server"></asp:TextBox>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />
        </td>
          <td align="left" width="20%"></td>
    </tr>
    <tr>
        <td >
            <span class="field-label"> Cost of an Item</span></td>
        <td  rowspan="1" >
            <asp:TextBox ID="txtcost" runat="server" ></asp:TextBox> </td>
             <td> <asp:DropDownList ID="ddcurrency" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="4" rowspan="1">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="ISBN"
                Width="86px" />
        </td>
    </tr>
</table>
<ajaxToolkit:FilteredTextBoxExtender ID="F1"  FilterType="Numbers" TargetControlID="txtQuantity" runat="server" ></ajaxToolkit:FilteredTextBoxExtender>  <ajaxToolkit:FilteredTextBoxExtender ID="F2"  FilterType="Custom, Numbers" TargetControlID="txtcost" runat="server" ValidChars=".," >
</ajaxToolkit:FilteredTextBoxExtender>
        <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
            TargetControlID="txtpurchasedate">
        </ajaxToolkit:CalendarExtender>
<asp:Label ID="Label1" runat="server"></asp:Label><br />
<asp:Panel ID="PanelStatusupdate"
    runat="server" BackColor="white" CssClass="modalPopup" Style="display: none">
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="240">
        <tr>
            <td class="title-bg-lite">
                Library Message</td>
        </tr>
        <tr>
            <td align="center">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td align="center">
            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnmok" runat="server" CssClass="button"  OnClientClick="javascript:RedirectEdit1();return false;" Text="Ok"
                                        ValidationGroup="s" Width="80px" CausesValidation="False" /></td>
                            </tr>
                        </table>
                        &nbsp;
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
    DropShadow="true" PopupControlID="PanelStatusupdate"
    RepositionMode="RepositionOnWindowResizeAndScroll" CancelControlID="btnmok" TargetControlID="Label1">
</ajaxToolkit:ModalPopupExtender>

        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txttitle" ValidationGroup="ISBN"
            Display="None" ErrorMessage="Please Enter Title of an Item" SetFocusOnError="True"></asp:RequiredFieldValidator>
       <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtisbndata" ValidationGroup="ISBN"
            Display="None" ErrorMessage="ISBN not found. Data entry cannot be done" SetFocusOnError="True"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtQuantity" ValidationGroup="ISBN"
            Display="None" ErrorMessage="Please Enter Item Quantity" SetFocusOnError="True"></asp:RequiredFieldValidator>
<%--        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtsupplier"
            Display="None" ErrorMessage="Please Enter Supplier Details" SetFocusOnError="True"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtpurchasedate"
            Display="None" ErrorMessage="Please Enter Purchase Date" SetFocusOnError="True"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtcost"
            Display="None" ErrorMessage="Please Enter Cost of an Item" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="DDFromAge"
            ControlToValidate="DDToAge" Display="None" ErrorMessage='"To Age Group"  Must be greater  than or equal to "From Age Group"'
            Operator="GreaterThanEqual" SetFocusOnError="True" Type="Integer"></asp:CompareValidator>&nbsp;
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtISBN"
            Display="None" ErrorMessage="Please Enter ISBN Number" SetFocusOnError="True"
            ValidationGroup="ISBN"></asp:RequiredFieldValidator>
        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
            ShowSummary="False" ValidationGroup="ISBN" />
        <br />
<asp:HiddenField ID="HiddenBsuID" runat="server" /><asp:HiddenField ID="HiddenEmpid" runat="server" />
<asp:HiddenField ID="HiddenShowImage" runat="server" Value="0" />
<asp:HiddenField ID="HiddenImageUrl" runat="server" />
<asp:HiddenField ID="HiddenProductUrl" runat="server"  />
<asp:HiddenField ID="HiddenIsbnEnc" runat="server"  /><asp:HiddenField ID="HiddenRadioSelect" runat="server"  />
        <asp:HiddenField ID="HiddenMasterId1" runat="server" />
        <asp:HiddenField ID="HiddenDirect1" runat="server" />
    </contenttemplate>
</asp:UpdatePanel>
</div>
<script type="text/javascript"> 

if (document.getElementById('<%= HiddenShowImage.ClientID %>').value =='1')
{

//search(this.form)
document.getElementById('<%= HiddenShowImage.ClientID %>').value = 0 ;
}


</script>

<br />
<br />
<br />
<br />

