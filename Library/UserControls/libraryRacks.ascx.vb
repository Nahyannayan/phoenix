Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryRacks
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            BindLibraryDivisions()
            BindLibrarySubDivisions()
            BindGrid()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub


    Public Sub BindLibraryDivisions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddLibraryDivisions.DataSource = ds
        ddLibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
        ddLibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
        ddLibraryDivisions.DataBind()

    End Sub

    Public Sub BindLibrarySubDivisions()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_SUB_DIVISIONS " & _
                        "WHERE LIBRARY_DIVISION_ID='" & ddLibraryDivisions.SelectedValue & "' ORDER BY LIBRARY_SUB_DIVISION_DES "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddLibrarySubDivisions.DataSource = ds
        ddLibrarySubDivisions.DataTextField = "LIBRARY_SUB_DIVISION_DES"
        ddLibrarySubDivisions.DataValueField = "LIBRARY_SUB_DIVISION_ID"
        ddLibrarySubDivisions.DataBind()

        BindShelfS()

    End Sub


    Public Sub BindShelfS()
        lblMessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_SHELFS " & _
                        "WHERE LIBRARY_SUB_DIVISION_ID='" & ddLibrarySubDivisions.SelectedValue & "' ORDER BY SHELF_NAME "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddShelfName.DataSource = ds
        ddShelfName.DataTextField = "SHELF_NAME"
        ddShelfName.DataValueField = "SHELF_ID"
        ddShelfName.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            btnSave.Visible = True
        Else
            btnSave.Visible = False
        End If

    End Sub

    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " select RACK_ID,RACK_NAME,RACK_DES,A.SHELF_ID,SHELF_NAME,LIBRARY_SUB_DIVISION_DES,LIBRARY_DIVISION_DES, " & _
                        " '<span style=''color: red;font-weight:bold''>Hide</span>' as hide, " & _
                        " (substring(RACK_DES,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview1 " & _
                        " from dbo.LIBRARY_RACKS A  " & _
                        " INNER JOIN dbo.LIBRARY_SHELFS B ON A.SHELF_ID= B.SHELF_ID " & _
                        " INNER JOIN dbo.LIBRARY_SUB_DIVISIONS C ON C.LIBRARY_SUB_DIVISION_ID=B.LIBRARY_SUB_DIVISION_ID " & _
                        " INNER JOIN dbo.LIBRARY_DIVISIONS D ON C.LIBRARY_DIVISION_ID= D.LIBRARY_DIVISION_ID " & _
                        " WHERE D.LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER  BY RACK_NAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GridRacks.DataSource = ds
        GridRacks.DataBind()


    End Sub

    Protected Sub ddLibraryDivisions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindLibrarySubDivisions()
    End Sub

    Protected Sub ddLibrarySubDivisions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindShelfS()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@RACK_NAME", txtRackName.Text.Trim().ToUpper())
            pParms(1) = New SqlClient.SqlParameter("@RACK_DES", txtRackDescription.Text.Trim())
            pParms(2) = New SqlClient.SqlParameter("@SHELF_ID", ddShelfName.SelectedValue)
            pParms(3) = New SqlClient.SqlParameter("@OPTIONS", 1)
            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_RACKS", pParms)
            transaction.Commit()
            BindGrid()
            txtRackName.Text = ""
            txtRackDescription.Text = ""

        Catch ex As Exception
            transaction.Rollback()

        Finally
            connection.Close()

        End Try


    End Sub

    Protected Sub GridRacks_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        lblMessage.Text = ""
        GridRacks.EditIndex = e.NewEditIndex
        BindGrid()
    End Sub

    Protected Sub GridRacks_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs)
        lblMessage.Text = ""
        GridRacks.EditIndex = "-1"
        BindGrid()
    End Sub

    Protected Sub GridRacks_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs)
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try
            Dim HiddenRackId = DirectCast(GridRacks.Rows(e.RowIndex).FindControl("HiddenRackId"), HiddenField).Value
            Dim HiddenShelfId = DirectCast(GridRacks.Rows(e.RowIndex).FindControl("HiddenShelfId"), HiddenField).Value
            Dim txtRackName = DirectCast(GridRacks.Rows(e.RowIndex).FindControl("txtDataEdit"), TextBox).Text.Trim()
            Dim txtRackDescription = DirectCast(GridRacks.Rows(e.RowIndex).FindControl("txtRackDescription"), TextBox).Text.Trim()

            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@RACK_ID", HiddenRackId)
            pParms(1) = New SqlClient.SqlParameter("@SHELF_ID", HiddenShelfId)
            pParms(2) = New SqlClient.SqlParameter("@RACK_NAME", txtRackName.ToString().ToUpper())
            pParms(3) = New SqlClient.SqlParameter("@RACK_DES", txtRackDescription)
            pParms(4) = New SqlClient.SqlParameter("@OPTIONS", 2)

            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_RACKS", pParms)
            transaction.Commit()
            GridRacks.EditIndex = "-1"
            BindGrid()
        Catch ex As Exception
            lblMessage.Text = "Error occurred while transactions. " & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()

        End Try

    End Sub

    Protected Sub GridRacks_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)

        If e.CommandName = "deleting" Then
            LibraryData.isOffLine(Session("sBusper"))
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblMessage.Text = ""
            Try
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@RACK_ID", e.CommandArgument)
                pParms(1) = New SqlClient.SqlParameter("@OPTIONS", 3)
                lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_RACKS", pParms)
                transaction.Commit()
                BindGrid()
            Catch ex As Exception
                lblMessage.Text = "Error occurred while transactions." & ex.Message
                transaction.Rollback()

            Finally
                connection.Close()

            End Try

        End If

    End Sub

    Protected Sub GridRacks_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        lblMessage.Text = ""
        GridRacks.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class
