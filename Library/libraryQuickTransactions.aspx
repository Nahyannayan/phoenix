<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryQuickTransactions.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Library_libraryQuickTransactions" %>

<%@ Register Src="UserControls/libraryItemTransactions.ascx" TagName="libraryItemTransactions"
    TagPrefix="uc3" %>

<%@ Register Src="UserControls/libraryQuickReturnTransactions.ascx" TagName="libraryQuickReturnTransactions"
    TagPrefix="uc2" %>

<%@ Register Src="UserControls/libraryQuickIssueTransactions.ascx" TagName="libraryQuickIssueTransactions"
    TagPrefix="uc1" %>


<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
  <div align="left" class="matters">
        <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
            <ajaxToolkit:TabPanel ID="HT1" runat="server">
                <ContentTemplate>
                
                    <uc1:libraryQuickIssueTransactions ID="LibraryQuickIssueTransactions1" runat="server" />
        
                </ContentTemplate>
                <HeaderTemplate>
                  Items Issue
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT2" runat="server">
                <ContentTemplate>
                    <uc2:libraryQuickReturnTransactions ID="LibraryQuickReturnTransactions1" runat="server" />
                 
                
                                   
                </ContentTemplate>
                <HeaderTemplate>
                 Return Items
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
             <ajaxToolkit:TabPanel ID="HT3" runat="server">
                <ContentTemplate>
                    <uc3:libraryItemTransactions ID="LibraryItemTransactions1" runat="server" />
                  
                
                                   
                </ContentTemplate>
                <HeaderTemplate>
                Library Items and Reservations
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
        </ajaxToolkit:TabContainer>

    </div>

</asp:Content>

