﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient


Partial Class HelpDesk_Reports_UserControl_hdTaskConsolidatedReportBsu
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnexport)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnexport0)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnview)
        If GridInfo0.Rows.Count > 0 Then
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo0.HeaderRow.FindControl("lnkbsu"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo0.HeaderRow.FindControl("lnk11"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo0.HeaderRow.FindControl("lnk12"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo0.HeaderRow.FindControl("lnk13"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo0.HeaderRow.FindControl("lnk14"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo0.HeaderRow.FindControl("lnk15"), LinkButton))
        End If

        If GridInfo.Rows.Count > 0 Then
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnkbsu"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk11"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk12"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk13"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk14"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk15"), LinkButton))

        End If
        For Each row As GridViewRow In GridInfo.Rows
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(row.FindControl("lnk100"), LinkButton))
        Next
    End Sub


    Protected Sub btnview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnview.Click
        BindGrid0()
    End Sub

    Public Sub BindGrid0(Optional ByVal export As Boolean = False)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FROMDATE", txtFromdate.Text.Trim())
        pParms(1) = New SqlClient.SqlParameter("@TODATE", txtToDate.Text.Trim())
        If Library.LibrarySuperAcess(Session("EmployeeId")) Then
        Else
            pParms(2) = New SqlClient.SqlParameter("@LOGIN_EMP_ID", Session("EmployeeId"))
        End If


        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "REPORT_TASK_CONSOLIDATED_BSU_L1", pParms)
        GridInfo0.DataSource = ds
        GridInfo0.DataBind()
        If export Then
            Dim dt As New DataTable
            dt = ds.Tables(0)
            dt.Columns.Remove("CLAME_BSU_ID")
            'dt.Columns.Remove("PRODUCT_URL")
            'dt.Columns.Remove("REDIRECT")
            'dt.Columns.Remove("REDIRECTEDIT")
            'dt.Columns.Remove("SHOWURL")
            'dt.Columns.Remove("LIBRARY_DIVISION_ID_VAL")

            ExportExcel(dt)
        End If

        If GridInfo0.Rows.Count > 0 Then
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo0.HeaderRow.FindControl("lnkbsu"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo0.HeaderRow.FindControl("lnk11"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo0.HeaderRow.FindControl("lnk12"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo0.HeaderRow.FindControl("lnk13"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo0.HeaderRow.FindControl("lnk14"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo0.HeaderRow.FindControl("lnk15"), LinkButton))

        End If

    End Sub

    Public Sub BindGrid(ByVal claim_bsu_id As String, Optional ByVal export As Boolean = False)
        Hiddenclaimbsuid.Value = claim_bsu_id
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FROMDATE", txtFromdate.Text.Trim())
        pParms(1) = New SqlClient.SqlParameter("@TODATE", txtToDate.Text.Trim())
        pParms(2) = New SqlClient.SqlParameter("@CLAME_BSU_ID", claim_bsu_id)
        pParms(3) = New SqlClient.SqlParameter("@OPTION", 1)
        If Library.LibrarySuperAcess(Session("EmployeeId")) Then
        Else
            pParms(4) = New SqlClient.SqlParameter("@LOGIN_EMP_ID", Session("EmployeeId"))
        End If

        Dim ds As New DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "REPORT_TASK_CONSOLIDATED_BSU", pParms)

        GridInfo.DataSource = ds
        GridInfo.DataBind()

        If GridInfo.Rows.Count > 0 Then
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnkbsu"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk11"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk12"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk13"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk14"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk15"), LinkButton))
        End If

        For Each row As GridViewRow In GridInfo.Rows
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(row.FindControl("lnk100"), LinkButton))
        Next

        If export Then
            Dim dt As New DataTable
            dt = ds.Tables(0)
            dt.Columns.Remove("T11")
            dt.Columns.Remove("T12")
            dt.Columns.Remove("T13")
            dt.Columns.Remove("T14")
            dt.Columns.Remove("T15")

            ExportExcel(dt)
        End If


    End Sub

    Public Sub ExportExcel(ByVal dt As DataTable)

        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, "A1", True)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")
        ef.SaveXls(Response.OutputStream)

    End Sub



    Protected Sub GridInfo0_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridInfo0.RowCommand

        If e.CommandName = "view" Then
            Hiddenclaimid.Value = e.CommandArgument
            BindGrid(e.CommandArgument, False)
            MO1.Show()
        End If
        If e.CommandName = "1" Then
            BindHeader(e.CommandArgument)
        End If

    End Sub


    Public Sub BindHeader(ByVal soption As String)
       Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FROMDATE", txtFromdate.Text.Trim())
        pParms(1) = New SqlClient.SqlParameter("@TODATE", txtToDate.Text.Trim())
        If Library.LibrarySuperAcess(Session("EmployeeId")) Then
        Else
            pParms(2) = New SqlClient.SqlParameter("@LOGIN_EMP_ID", Session("EmployeeId"))
        End If
        pParms(3) = New SqlClient.SqlParameter("@OPTION", soption)


        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "REPORT_TASK_CONSOLIDATED_BSU_L1", pParms)

        If ds.Tables(1).Rows.Count > 0 Then
            Dim dt As New DataTable
            dt = ds.Tables(1)
            ExportExcel(dt)
        End If

    End Sub



    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        BindGrid0(True)
    End Sub

    Protected Sub btnexport0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport0.Click
        BindGrid(Hiddenclaimid.Value, True)
    End Sub

    Protected Sub GridInfo_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridInfo.RowCommand

        If e.CommandName = "1" Then
            Export2(e.CommandArgument)
        End If
        If e.CommandName = "100" Then
            Export2(100, e.CommandArgument)
        End If
    End Sub

    Public Sub Export2(ByVal soption As Integer, Optional ByVal caller_bsu_id As String = "")
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FROMDATE", txtFromdate.Text.Trim())
        pParms(1) = New SqlClient.SqlParameter("@TODATE", txtToDate.Text.Trim())
        pParms(3) = New SqlClient.SqlParameter("@OPTION", soption)
        pParms(2) = New SqlClient.SqlParameter("@CLAME_BSU_ID", Hiddenclaimbsuid.Value)
        If Library.LibrarySuperAcess(Session("EmployeeId")) Then
        Else
            pParms(4) = New SqlClient.SqlParameter("@LOGIN_EMP_ID", Session("EmployeeId"))
        End If

        If caller_bsu_id <> "" Then
            pParms(5) = New SqlClient.SqlParameter("@CALLER_BSU_ID", caller_bsu_id)
        End If
        Dim ds As New DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "REPORT_TASK_CONSOLIDATED_BSU", pParms)


        If ds.Tables(0).Rows.Count > 0 Then
            Dim dt As New DataTable
            dt = ds.Tables(0)
            'dt.Columns.Remove("T11")
            'dt.Columns.Remove("T12")
            'dt.Columns.Remove("T13")
            'dt.Columns.Remove("T14")
            'dt.Columns.Remove("T15")

            ExportExcel(dt)

        End If



    End Sub
End Class
