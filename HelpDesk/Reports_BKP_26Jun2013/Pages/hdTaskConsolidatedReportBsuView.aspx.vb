﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Partial Class HelpDesk_Reports_Pages_hdTaskConsolidatedReportBsuView
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Request.QueryString("option") = "11" Then
                lblmessage.Text = "Total Issue"
            End If
            If Request.QueryString("option") = "12" Then
                lblmessage.Text = "Assigned"
            End If
            If Request.QueryString("option") = "13" Then
                lblmessage.Text = "Member Completed"
            End If
            If Request.QueryString("option") = "14" Then
                lblmessage.Text = "Member Pending"
            End If
            If Request.QueryString("option") = "15" Then
                lblmessage.Text = "Member Pending Future Dates"
            End If


            BindGrid()
        End If

    End Sub

    Public Sub BindGrid(Optional ByVal export As Boolean = False)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FROMDATE", Request.QueryString("fromdate"))
        pParms(1) = New SqlClient.SqlParameter("@TODATE", Request.QueryString("todate"))
        pParms(2) = New SqlClient.SqlParameter("@CLAME_BSU_ID", Request.QueryString("clmbsuid"))
        pParms(3) = New SqlClient.SqlParameter("@CALLER_BSU_ID", Request.QueryString("callbsuid"))
        pParms(4) = New SqlClient.SqlParameter("@OPTION", Request.QueryString("option"))
        If Library.LibrarySuperAcess(Session("EmployeeId")) Then
        Else
            pParms(5) = New SqlClient.SqlParameter("@LOGIN_EMP_ID", Session("EmployeeId"))
        End If

        Dim ds As New DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "REPORT_TASK_CONSOLIDATED_BSU", pParms)

        GridTaskList.DataSource = ds
        GridTaskList.DataBind()

        If export Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dt As New DataTable

                dt = ds.Tables(0)
                dt.Columns.Remove("VIEW_TRACE_FLOW")
                dt.Columns.Remove("TASK_ID")
                dt.Columns.Remove("VIEW_TASK")
              

                ExportExcel(dt)
            End If

        End If

    End Sub

    Public Sub ExportExcel(ByVal dt As DataTable)

        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, "A1", True)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")
        ef.SaveXls(Response.OutputStream)

    End Sub


    Protected Sub LinkExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkExport.Click
        BindGrid(True)
    End Sub
End Class
