﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Partial Class HelpDesk_Reports_Pages_hdPendingIssuesAgingDashboardView
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'If Not IsPostBack Then
        '    If Request.QueryString("option") = "11" Then
        '        lblmessage.Text = "Total Issue"
        '    End If
        '    If Request.QueryString("option") = "12" Then
        '        lblmessage.Text = "Assigned"
        '    End If
        '    If Request.QueryString("option") = "13" Then
        '        lblmessage.Text = "Member Completed"
        '    End If
        '    If Request.QueryString("option") = "14" Then
        '        lblmessage.Text = "Member Pending"
        '    End If
        '    If Request.QueryString("option") = "15" Then
        '        lblmessage.Text = "Member Pending Future Dates"
        '    End If


        '    BindGrid()
        'End If

        Try
            If Not Request.QueryString("BsuName") is Nothing then
                    If Request.QueryString("BsuName") ="SELECT" then
                    Me.lblBsu.Text = "Selected Business Unit(s): All"
                    Else
                    Me.lblBsu.Text = "Selected Business Unit(s): " & Request.QueryString("BsuName")
                    End if 
            End If
            If Not Request.QueryString("Option") is Nothing then
                    If Request.QueryString("Option") ="1" then                    
                    Me.lblTitle.Text = "Business Units & Task Escalations"
                    Me.gvDashboard1.Columns(1).Visible = False
                    Me.gvDashboard1.Columns(4).Visible = False
                    Me.gvDashboard1.Columns(6).Visible = False
                    Me.gvDashboard1.Columns(8).Visible = False
                    Else
                    Me.lblTitle.Text = "Business Units, Task Categories, Task Escalation Days & Task Escalations"
                    Me.gvDashboard1.Columns(1).Visible = true
                    Me.gvDashboard1.Columns(4).Visible = true
                    Me.gvDashboard1.Columns(6).Visible = true
                    Me.gvDashboard1.Columns(8).Visible = true
                    End if 
            End If
            
        If Not Session("dsPendingIssuesAging") Is Nothing Then
                gvDashboard1.DataSource = CType(Session("dsPendingIssuesAging"), DataSet)
                gvDashboard1.DataBind()
        End If
            Me.lblmessage.Visible=false 
        Catch ex As Exception
            Me.lblmessage.Text="Error occured getting the data"
            Me.lblmessage.Visible=True 
        End Try

       

    End Sub

    Protected Sub gvDashboard_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDashboard1.PageIndexChanging
        Try
If Not Session("dsPendingIssuesAging") Is Nothing Then
            gvDashboard1.DataSource = CType(Session("dsPendingIssuesAging"), DataSet)
        End If
        Me.gvDashboard1.PageIndex = e.NewPageIndex
        gvDashboard1.DataBind()
            Me.lblmessage.Visible=false 
        Catch ex As Exception
             Me.lblmessage.Text="Error occured getting the data"
            Me.lblmessage.Visible=True 
        End Try
        
    End Sub

    

    Public Sub ExportExcel(ByVal dt As DataTable)

        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, "A1", True)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")
        ef.SaveXls(Response.OutputStream)

    End Sub


    Protected Sub LinkExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkExport.Click
Dim ds As DataSet
        If Not Session("dsPendingIssuesAging") Is Nothing Then
            ds = CType(Session("dsPendingIssuesAging"), DataSet) 
            End If
        If Not ds is Nothing then
If ds.Tables(0).Rows.Count > 0 Then
                Dim dt As New DataTable

                dt = ds.Tables(0)
                
              

                ExportExcel(dt)
            End If
        End If

    End Sub
End Class
