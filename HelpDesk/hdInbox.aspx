<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="hdInbox.aspx.vb" Inherits="HelpDesk_hdInbox" %>

<%@ Register Src="UserControls/hdInbox.ascx" TagName="hdInbox" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

     <script type="text/javascript">
         function SendMail(TaskAssignid, FromEmpId, ToEmpid, Parent_Message_Id) {

             window.showModalDialog('Pages/hdEnterMessage.aspx?TaskAssignid=' + TaskAssignid + '&FromEmpId=' + FromEmpId + '&ToEmpid=' + ToEmpid + '&Parent_Message_Id=' + Parent_Message_Id, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;


         }
         function ViewMessage(messageid, toemp_id) {

             window.open('Pages/hdViewMessage.aspx?messageid=' + messageid + '&toemp_id=' + toemp_id + '&Type=Inbox', '', 'Height:650px;Width:800px;scrollbars:auto;resizable:yes;'); return false;


         }
 </script>


<div>
        <uc1:hdInbox ID="HdInbox1" runat="server" />
    
</div>

</asp:Content>  
