﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Partial Class HelpDesk_hdMasterSettingsBsu
    Inherits BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindBsu()
            BindGrid()
        End If

    End Sub
    Public Sub BindBsu()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        'Dim Sql_Query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M ORDER BY BSU_NAME"
        Dim ds As DataSet
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@empID", Convert.ToInt32(Session("EmployeeId")))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Routing_BSU_By_User", param)
        If ds.Tables(0).Rows.Count > 0 Then
            ddbsu.DataSource = ds
            ddbsu.DataTextField = "BSU_NAME"
            ddbsu.DataValueField = "BSU_ID"
            ddbsu.DataBind()
            ddbsu.SelectedValue = Session("sbsuid") ''"999998" 'Corporate Offices' 
        End If

    End Sub
    Public Sub BindGrid()
        lblMessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Dim Sql_Query = " SELECT  A.EMP_ID,isnull(EMP_FNAME,'') + ' ' +  isnull(EMP_MNAME ,'') + ' ' + isnull(EMP_LNAME ,'') as EMP_NAME,EMPNO, EMD_CUR_MOBILE,EMD_EMAIL,'javascript:openWindow(''' + convert(varchar,A.EMP_ID) + '''); return false;' OPENW,'javascript:openAssignCategory(''' + convert(varchar,A.EMP_ID) + '''); return false;' OPENWC FROM  EMPLOYEE_M A " & _
                        " INNER JOIN EMPLOYEE_D B ON  A.EMP_ID= B.EMD_EMP_ID " & _
                        " WHERE EMP_BSU_ID='" & ddbsu.SelectedValue & "' and A.EMP_bACTIVE='True' and A.EMP_STATUS <> 4 "

        Dim name = ""
        Dim mobile = ""
        Dim email = ""
        Dim empNo = "'"
        If GridRootingEmployees.Rows.Count > 0 Then
            name = DirectCast(GridRootingEmployees.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
            mobile = DirectCast(GridRootingEmployees.HeaderRow.FindControl("Txt2"), TextBox).Text.Trim()
            email = DirectCast(GridRootingEmployees.HeaderRow.FindControl("Txt3"), TextBox).Text.Trim()
            empNo = DirectCast(GridRootingEmployees.HeaderRow.FindControl("txtEmpNo"), TextBox).Text.Trim()

            If name <> "" Then
                Sql_Query &= " and replace(EMP_FNAME,' ','') like '%" & name & "%' "
            End If

            If mobile <> "" Then
                Sql_Query &= " and replace(EMD_CUR_MOBILE,' ','') like '%" & mobile & "%' "
            End If

            If email <> "" Then
                Sql_Query &= " and replace(EMD_EMAIL,' ','') like '%" & email & "%' "
            End If


            If empNo <> "" Then
                Sql_Query &= " and replace(EMPNO,' ','') like '%" & empNo & "%' "
            End If
        End If

        Sql_Query &= " ORDER BY EMP_FNAME "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("EMP_ID")
            dt.Columns.Add("EMP_NAME")
            dt.Columns.Add("EMD_CUR_MOBILE")
            dt.Columns.Add("EMD_EMAIL")
            dt.Columns.Add("OPENW")
            dt.Columns.Add("OPENWC")

            Dim dr As DataRow = dt.NewRow()
            dr("EMP_ID") = ""
            dr("EMP_NAME") = ""
            dr("EMD_CUR_MOBILE") = ""
            dr("EMD_EMAIL") = ""
            dr("OPENW") = ""
            dr("OPENWC") = ""
            dt.Rows.Add(dr)
            GridRootingEmployees.DataSource = dt
            GridRootingEmployees.DataBind()

            DirectCast(GridRootingEmployees.Rows(0).FindControl("lnkAssign"), LinkButton).Visible = False

        Else
            GridRootingEmployees.DataSource = ds
            GridRootingEmployees.DataBind()
        End If

        If GridRootingEmployees.Rows.Count > 0 Then

            DirectCast(GridRootingEmployees.HeaderRow.FindControl("Txt1"), TextBox).Text = name
            DirectCast(GridRootingEmployees.HeaderRow.FindControl("Txt2"), TextBox).Text = mobile
            DirectCast(GridRootingEmployees.HeaderRow.FindControl("Txt3"), TextBox).Text = email

        End If


    End Sub

    Protected Sub ddbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddbsu.SelectedIndexChanged
        BindGrid()
    End Sub

    Protected Sub GridRootingEmployees_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridRootingEmployees.PageIndexChanging

        GridRootingEmployees.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub

    Protected Sub GridRootingEmployees_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridRootingEmployees.RowCommand

        If e.CommandName = "Save" Then
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblMessage.Text = ""
            Try
                For Each row As GridViewRow In GridRootingEmployees.Rows
                    Dim emp_id = DirectCast(row.FindControl("HiddenEmpId"), HiddenField).Value.Trim()
                    Dim mobile = DirectCast(row.FindControl("txtMobile"), TextBox).Text.Trim()
                    Dim email = DirectCast(row.FindControl("txtEmail"), TextBox).Text.Trim()
                    Dim sql_query = "UPDATE EMPLOYEE_D SET EMD_CUR_MOBILE='" & mobile & "' , EMD_EMAIL='" & email & "' WHERE EMD_EMP_ID='" & emp_id & "'"
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)

                Next
                transaction.Commit()
                BindGrid()
                lblMessage.Text = "Transactions done successfully"

            Catch ex As Exception
                lblMessage.Text = "Error occurred while transactions. " & ex.Message
                transaction.Rollback()
            End Try


        End If

        If e.CommandName = "search" Then
            BindGrid()
        End If

    End Sub

End Class
