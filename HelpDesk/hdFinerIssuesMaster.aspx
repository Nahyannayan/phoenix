﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdFinerIssuesMaster.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="HelpDesk_hdFinerIssuesMaster" %>

<%@ Register Src="UserControls/hdFinerIssues.ascx" TagName="hdFinerIssues" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <%--<div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>PHOENIX HELP DESK
        </div>--%>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <uc1:hdFinerIssues ID="hdFinerIssues" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

