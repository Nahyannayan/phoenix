<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdClientOwnerCommentsView.aspx.vb" Inherits="HelpDesk_Pages_hdClientOwnerCommentsView" %>

<%@ Register Src="../UserControls/hdClientOwnerCommentsView.ascx" TagName="hdClientOwnerCommentsView"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <title>Helpdesk- Client Owner Comments View</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <base target="_self" /> 
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:hdClientOwnerCommentsView ID="HdClientOwnerCommentsView1" runat="server" />
    
    </div>
    </form>
</body>
</html>
