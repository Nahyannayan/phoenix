<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdTaskAssignView.aspx.vb" Inherits="HelpDesk_Pages_hdTaskAssignView" %>

<%@ Register Src="../UserControls/hdTaskAssignView.ascx" TagName="hdTaskAssignView"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Helpdesk- Task Assign Details View </title>
   <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <base target="_self" /> 
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:hdTaskAssignView id="HdTaskAssignView1" runat="server">
        </uc1:hdTaskAssignView></div>
    </form>
</body>
</html>
