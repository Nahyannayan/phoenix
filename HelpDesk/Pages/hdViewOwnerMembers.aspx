﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdViewOwnerMembers.aspx.vb" Inherits="HelpDesk_Pages_hdViewOwnerMembers" %>

<%@ Register src="../UserControls/hdViewOwnerMembers.ascx" tagname="hdViewOwnerMembers" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Other Owners & Members</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <base target="_self" /> 
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:hdViewOwnerMembers ID="hdViewOwnerMembers1" runat="server" />
    
    </div>
    </form>
</body>
</html>
