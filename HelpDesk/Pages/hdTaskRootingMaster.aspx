<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdTaskRootingMaster.aspx.vb" Inherits="HelpDesk_Pages_hdTaskRootingMaster" %>

<%@ Register Src="../UserControls/hdTaskRootingMaster.ascx" TagName="hdTaskRootingMaster"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   <title>Helpdesk- Task Routing Master</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="matters">
    
        <ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:hdTaskRootingMaster ID="HdTaskRootingMaster1" runat="server" />
           
            
        

     
        </ContentTemplate>
        </asp:UpdatePanel>
       
    </div>
    </form>
</body>
</html>
