<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdTaskRootingMasterList.aspx.vb" Inherits="HelpDesk_Pages_hdTaskRootingMasterList" %>

<%@ Register Src="../UserControls/hdTaskRootingMasterList.ascx" TagName="hdTaskRootingMasterList"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Helpdesk- Task Routing Master List</title>
     <link type="text/css" href="../../cssfiles/sb-admin.css" rel="stylesheet" />
     <link type="text/css" href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:hdTaskRootingMasterList ID="HdTaskRootingMasterList1" runat="server" />
    
    </div>
    </form>
</body>
</html>
