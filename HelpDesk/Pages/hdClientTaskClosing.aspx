﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdClientTaskClosing.aspx.vb" Inherits="HelpDesk_Pages_hdOwnerTaskClosing" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register src="../UserControls/hdFileUpload.ascx" tagname="hdFileUpload" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Update</title>
    <base target="_self" /> 
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
    
         <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="240">
            <tr>
                <td class="subheader_img">
                    Update Status</td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <telerik:RadEditor ID="txtdetails" runat="server" EditModes="Design" ToolsFile="../xml/FullSetOfTools.xml"
                                            Width="600px">
                                        </telerik:RadEditor>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:DropDownList ID="ddstatus" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <uc1:hdFileUpload ID="hdFileUpload1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnok" runat="server" CssClass="button" Text="Ok"
                                            ValidationGroup="s" Width="80px" /><asp:Button ID="btncancel3" runat="server" OnClientClick="window.close();" CssClass="button"
                                                Text="Cancel" Width="80px" /></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="HiddenTasklistid" runat="server" />
                            <asp:HiddenField ID="HiddenEmpId" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    
    </div>
    
    
    
    </form>
</body>
</html>
