﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class HelpDesk_Pages_hdOwnerTaskClosing
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If not session("hdClientTaskClosing_EmailSent") is Nothing then
                Session.Remove("hdClientTaskClosing_EmailSent")
            End If
            BindStatus()
            HiddenTasklistid.Value = Request.QueryString("tasklistid")
            HiddenEmpId.Value = Session("EmployeeId")
        End If
    End Sub

    Public Sub BindStatus()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "select * from  TASK_STATUS_MASTER where STATUS_ID in (8,7) ORDER BY TASK_STATUS_DESCRIPTION"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddstatus.DataSource = ds
        ddstatus.DataTextField = "TASK_STATUS_DESCRIPTION"
        ddstatus.DataValueField = "STATUS_ID"
        ddstatus.DataBind()

    End Sub



    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnok.Click

        btnok.Visible = False

        If not Session("hdClientTaskClosing_EmailSent") is Nothing then

        Else       

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim uploadids = ""
        Dim ownerstatus As Boolean
        Dim clientstatus As Boolean
        Dim LOGGER_STATUS As String = ""

        If ddstatus.SelectedValue = 8 Then
            clientstatus = True
            LOGGER_STATUS = "CLOSED"
        Else
            clientstatus = False
            ownerstatus = False
            LOGGER_STATUS = "REOPENED"
        End If

        Dim pParms(10) As SqlClient.SqlParameter
        If ownerstatus.ToString() <> "" Then
            pParms(0) = New SqlClient.SqlParameter("@OWNER_STATUS_CLOSED", ownerstatus)
        End If

        pParms(1) = New SqlClient.SqlParameter("@CLIENT_STATUS_CLOSED", clientstatus)
        pParms(2) = New SqlClient.SqlParameter("@TASK_LIST_ID", HiddenTasklistid.Value)
        pParms(3) = New SqlClient.SqlParameter("@TASK_STATUS_ID", ddstatus.SelectedValue)
        pParms(4) = New SqlClient.SqlParameter("@ENTRY_NOTES", txtdetails.Content)
        pParms(5) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", HiddenEmpId.Value)
        pParms(6) = New SqlClient.SqlParameter("@LOGGER", "CLIENT")
        pParms(7) = New SqlClient.SqlParameter("@LOGGER_STATUS", LOGGER_STATUS)
        If DirectCast(hdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value <> "" Then
            uploadids = DirectCast(hdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value
            pParms(8) = New SqlClient.SqlParameter("@UPLOAD_IDS", uploadids)
        End If


        Dim val As String = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "UPDATE_TASK_LOGGER_MASTER", pParms)

        Dim loggerid = val.Split("_")(0)
        lblmessage.Text = val.Split("_")(1)
        
        If Session("hdClientTaskClosing_EmailSent") is Nothing then
           Session.Add("hdClientTaskClosing_EmailSent", 1)
           HelpDesk.SendClosingReopeningEmails(HiddenTasklistid.Value, txtdetails.Content, "CLOSED", HiddenEmpId.Value, "OWNER", loggerid, uploadids)            
        End If  
        
        txtdetails.Content = ""
        DirectCast(hdFileUpload1.FindControl("GridUpload"), GridView).Controls.Clear()
        DirectCast(hdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value = ""
        ScriptManager.RegisterStartupScript(Me, GetType(Page),   "hdClientTaskClosing", "window.close();", True )
        End If
    End Sub

 
End Class
 