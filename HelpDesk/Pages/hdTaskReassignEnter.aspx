<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdTaskReassignEnter.aspx.vb" Inherits="HelpDesk_Pages_hdTaskReassignEnter" %>

<%@ Register Src="../UserControls/hdTaskReassignEnter.ascx" TagName="hdTaskReassignEnter"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <title>Helpdesk- Task Reassign Details Entry</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <base target="_self" /> 
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:hdTaskReassignEnter ID="HdTaskReassignEnter1" runat="server" />
    
    </div>
    </form>
</body>
</html>
