<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AddUserCategories.aspx.vb" Inherits="HelpDesk_Pages_AddUserCategories" %>

<%@ Register Src="../UserControls/AddUserCategories.ascx" TagName="AddUserCategories"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Helpdesk- Add User Categories</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <base target="_self" /> 
</head>
<body>
    <form id="form1" runat="server">
    <div class="matters">
        <uc1:AddUserCategories id="AddUserCategories1" runat="server">
        </uc1:AddUserCategories></div>
    </form>
</body>
</html>
