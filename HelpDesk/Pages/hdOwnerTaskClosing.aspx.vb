﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class HelpDesk_Pages_hdOwnerTaskClosing
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenTasklistid.Value = Request.QueryString("tasklistid")
            HiddenEmpId.Value = Session("EmployeeId")
        End If
    End Sub

    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnok.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim uploadids = ""
        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OWNER_STATUS_CLOSED", True)
        pParms(1) = New SqlClient.SqlParameter("@CLIENT_STATUS_CLOSED", False)
        pParms(2) = New SqlClient.SqlParameter("@TASK_LIST_ID", HiddenTasklistid.Value)
        pParms(3) = New SqlClient.SqlParameter("@TASK_STATUS_ID", "6")
        pParms(4) = New SqlClient.SqlParameter("@ENTRY_NOTES", txtdetails.Content)
        pParms(5) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", HiddenEmpId.Value)
        pParms(6) = New SqlClient.SqlParameter("@LOGGER", "OWNER")
        pParms(7) = New SqlClient.SqlParameter("@LOGGER_STATUS", "CLOSED")

        If DirectCast(hdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value <> "" Then
            uploadids = DirectCast(hdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value
            pParms(8) = New SqlClient.SqlParameter("@UPLOAD_IDS", uploadids)
        End If


        Dim val As String = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "UPDATE_TASK_LOGGER_MASTER", pParms)

        Dim loggerid = val.Split("_")(0)
        lblmessage.Text = val.Split("_")(1)
        HelpDesk.SendClosingReopeningEmails(HiddenTasklistid.Value, txtdetails.Content, "CLOSED", HiddenEmpId.Value, "OWNER", loggerid, uploadids)
        txtdetails.Content = ""
        DirectCast(hdFileUpload1.FindControl("GridUpload"), GridView).Controls.Clear()
        DirectCast(hdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value = ""

    End Sub

 
End Class
 