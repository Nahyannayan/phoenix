<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdClientOwnerHistory.aspx.vb" Inherits="HelpDesk_Pages_hdClientOwnerHistory" %>

<%@ Register Src="../UserControls/hdClientOwnerHistory.ascx" TagName="hdClientOwnerHistory"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
      <title>Helpdesk- Client Owner History</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <base target="_self" /> 
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:hdClientOwnerHistory ID="HdClientOwnerHistory1" runat="server" />
    
    </div>
    </form>
</body>
</html>
