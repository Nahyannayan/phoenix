﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdOwnerTaskClosing.aspx.vb" Inherits="HelpDesk_Pages_hdOwnerTaskClosing" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register src="../UserControls/hdFileUpload.ascx" tagname="hdFileUpload" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Update</title>
    <base target="_self" /> 
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
    
        <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" 
            width="240">
            <tr>
                <td class="subheader_img">
                    Closing Notes
                </td>
            </tr>
            <tr>
                <td>
                 <%--   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>--%>
                            <table>
                                <tr>
                                    <td>
                                        <telerik:RadEditor ID="txtdetails" editmodes="Design" 
                                            toolsfile="../xml/FullSetOfTools.xml" width="600px" Runat="server">
                                        </telerik:RadEditor>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc1:hdFileUpload ID="hdFileUpload1" runat="server" />
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnok" runat="server" CssClass="button" OnClick="btnok_Click" 
                                            Text="Ok" ValidationGroup="s" Width="80px" />
                                        <asp:Button ID="btncancel" runat="server" CssClass="button" OnClientClick="window.close();" Text="Cancel" 
                                            Width="80px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="HiddenTasklistid" runat="server" />
                            <asp:HiddenField ID="HiddenEmpId" runat="server" />
                   <%--     </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </td>
            </tr>
        </table>
    
    </div>
    
    
    
    </form>
</body>
</html>
