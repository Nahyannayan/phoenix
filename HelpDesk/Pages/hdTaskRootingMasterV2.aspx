﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdTaskRootingMasterV2.aspx.vb" Inherits="HelpDesk_Pages_hdTaskRootingMasterV2" %>

<%@ Register src="../UserControls/hdTaskRootingMasterV2.ascx" tagname="hdTaskRootingMasterV2" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Helpdesk- Task Routing Master</title>
        <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
     <base target="_self" /> 
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:hdTaskRootingMasterV2 ID="hdTaskRootingMasterV21" runat="server" />
    
    </div>
    </form>
</body>
</html>
