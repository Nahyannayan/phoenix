﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="hdMasterSettingsBsu.aspx.vb" Inherits="HelpDesk_hdMasterSettingsBsu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script type="text/javascript" >

        function openWindow(empid) {

            window.open('hdTaskrootingMasterBsu.aspx?Emp_id=' + empid, '', 'Height=700px,Width=1000px,scrollbars=yes,resizable=no,directories=yes');
            return false;


        }

        function openAssignCategory(empid) {

            window.open('AddUserCategories.aspx?Emp_id=' + empid, '', 'Height=600px,Width=750px,scrollbars=yes,resizable=no,directories=yes');
            return false;


        }
    
    </script>
<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="850">
    <tr>
        <td class="subheader_img">
            Select BSU
            <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridRootingEmployees" runat="server" ShowFooter="true" AutoGenerateColumns="false"
                Width="100%" AllowPaging="True">
                <Columns>
                    <asp:TemplateField HeaderText="Name">
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Name
                                        <br />
                                        <asp:TextBox ID="Txt1" Width="120px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("EMP_NAME")%>
                            <asp:HiddenField ID="HiddenEmpId" runat="server" Value='<%#Eval("EMP_ID") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                        <center>
                        <asp:Button ID="btnsave" runat="server" CssClass="button" CommandName="Save" Text="Save Changes" />
                        </center>
                        </FooterTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="EMPNO">
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Employee No
                                        <br />
                                        <asp:TextBox ID="txtEmpNo" Width="120px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearchEmpno" runat="server" CausesValidation="false" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("EMPNO")%>
                         
                        </ItemTemplate>
                    
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mobile Number">
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Mobile Number
                                        <br />
                                        <asp:TextBox ID="Txt2" Width="120px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:TextBox ID="txtMobile" Text='<% #Eval("EMD_CUR_MOBILE") %>' MaxLength="20" runat="server"></asp:TextBox>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email ID">
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Email ID
                                        <br />
                                        <asp:TextBox ID="Txt3" Width="120px" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:TextBox ID="txtEmail" Text='<% #Eval("EMD_EMAIL") %>' runat="server"></asp:TextBox>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Assign">
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        <br />
                                        Assign
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="lnkAssign" OnClientClick='<% #Eval("OPENW") %>' runat="server">Assign</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                <%--    <asp:TemplateField HeaderText="Categories">
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        <br />
                                        Categories
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="lnkCategory" OnClientClick='<% #Eval("OPENWC") %>' runat="server">Assign</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
                <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <EditRowStyle Wrap="False" />
            </asp:GridView>
            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
    </tr>
</table>
</asp:Content>

