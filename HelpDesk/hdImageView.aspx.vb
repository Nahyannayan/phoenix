
Partial Class HelpDesk_hdImageView
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim filename = Request.QueryString("filename")
            Dim imagepath = Web.Configuration.WebConfigurationManager.AppSettings("UploadClipboardVirtualPath").ToString() + fileName
            Image1.ImageUrl = imagepath
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
        End If

    End Sub
End Class
