Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports InfoSoftGlobal
Partial Class HelpDesk_FusionCharts_UserControls_hdUserTaskProgressChart
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenAssignedTaskid.Value = Request.QueryString("AssignedTaskid")
            HiddenEmpID.Value = Request.QueryString("emp_id")
            FCLiteral.Text = CreateCharts()
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Public Function CreateCharts() As String
        Dim strXML As String
        strXML = ""
        strXML = strXML & "<graph caption='Task Progress' xAxisName='Date' yAxisName='Completed Percentage' decimalPrecision='0' rotateNames='1' formatNumberScale='0' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='0' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC'>"
        strXML = strXML & xml().ToString()
        strXML = strXML & "</graph>"
        Return FusionCharts.RenderChartHTML("swf/" & ddReportList.SelectedValue, "", strXML, "myNext", "600", "400", False)

    End Function


    Public Function xml() As StringBuilder

        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString
            Dim strXML As New StringBuilder

            Dim sql_query = " select PERCENTAGE,REPLACE(CONVERT(VARCHAR(11), ENTRY_DATE, 106), ' ', '/')ENTRY_DATE from dbo.TASK_STATUS_LOG  " & _
                            " where task_assign_id ='" & HiddenAssignedTaskid.Value & "' AND TASK_EMP_ID='" & HiddenEmpID.Value & "' " & _
                            " order by ENTRY_DATE "


            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                strXML.Append("<set name='" & ds.Tables(0).Rows(i).Item("ENTRY_DATE").ToString() & "' value='" & ds.Tables(0).Rows(i).Item("PERCENTAGE").ToString() & "' color='" & getFCColor() & "' />")
            Next

            Return strXML

        Catch ex As Exception

        End Try


    End Function


    Protected Sub ddReportList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddReportList.SelectedIndexChanged
        FCLiteral.Text = CreateCharts()
    End Sub

    Public arr_FCColors(20) As String
    Public FC_ColorCounter As Integer
    Public Sub New()
        'This page contains an array of colors to be used as default set of colors for FusionCharts
        'arr_FCColors is the array that would contain the hex code of colors 
        'ALL COLORS HEX CODES TO BE USED WITHOUT #


        'We also initiate a counter variable to help us cyclically rotate through
        'the array of colors.
        FC_ColorCounter = 0

        arr_FCColors(0) = "1941A5" 'Dark Blue
        arr_FCColors(1) = "AFD8F8"
        arr_FCColors(2) = "F6BD0F"
        arr_FCColors(3) = "8BBA00"
        arr_FCColors(4) = "A66EDD"
        arr_FCColors(5) = "F984A1"
        arr_FCColors(6) = "CCCC00" 'Chrome Yellow+Green
        arr_FCColors(7) = "999999" 'Grey
        arr_FCColors(8) = "0099CC" 'Blue Shade
        arr_FCColors(9) = "FF0000" 'Bright Red 
        arr_FCColors(10) = "006F00" 'Dark Green
        arr_FCColors(11) = "0099FF" 'Blue (Light)
        arr_FCColors(12) = "FF66CC" 'Dark Pink
        arr_FCColors(13) = "669966" 'Dirty green
        arr_FCColors(14) = "7C7CB4" 'Violet shade of blue
        arr_FCColors(15) = "FF9933" 'Orange
        arr_FCColors(16) = "9900FF" 'Violet
        arr_FCColors(17) = "99FFCC" 'Blue+Green Light
        arr_FCColors(18) = "CCCCFF" 'Light violet
        arr_FCColors(19) = "669900" 'Shade of green
    End Sub


    'getFCColor method helps return a color from arr_FCColors array. It uses
    'cyclic iteration to return a color from a given index. The index value is
    'maintained in FC_ColorCounter

    Public Function getFCColor() As String
        'Update index
        FC_ColorCounter = FC_ColorCounter + 1
        'Return color
        Return arr_FCColors(FC_ColorCounter Mod UBound(arr_FCColors))
    End Function

End Class
