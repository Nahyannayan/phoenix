Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports InfoSoftGlobal
Partial Class HelpDesk_FusionCharts_UserControls_hdTaskProgressCompare
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenTaskListid.Value = Request.QueryString("TaskListid")
            HiddenEmpID.Value = ""
            FCLiteral.Text = CreateChart()
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub


    Public Function CreateChart() As String

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString

        Dim strXML As String

        'Initialize <graph> element

        strXML = "<graph caption='Task Progress Comparison' xAxisName='Date' yAxisName='Completed Percentage' hovercapbg='FFFFFF' divLineColor='999999' divLineAlpha='80' numdivlines='5' decimalPrecision='0' numberPrefix='' numberSuffix='' rotateNames='1'>"
        Dim n = 0
        Dim i = 0
      
        Dim sql_query = " select DISTINCT REPLACE(CONVERT(VARCHAR(11), C.ENTRY_DATE, 106), ' ', '/') ENTRY_DATE from dbo.TASK_LIST_MASTER A " & _
                        " RIGHT JOIN dbo.TASK_ASSIGNED_LOG B ON A.TASK_LIST_ID=B.TASK_LIST_ID " & _
                        " RIGHT JOIN dbo.TASK_STATUS_LOG C ON B.TASK_ASSIGN_ID=C.TASK_ASSIGN_ID " & _
                        " INNER JOIN OASIS.dbo.EMPLOYEE_M D ON D.EMP_ID= C.TASK_EMP_ID " & _
                        " INNER JOIN OASIS.dbo.EMPLOYEE_D E ON  E.EMD_EMP_ID= D.EMP_ID " & _
                        " where A.TASK_LIST_ID='" & HiddenTaskListid.Value & "' AND ACTIVE='" & CheckActive.Checked & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

        Dim dsdates As DataSet = ds

        'Initialize  Users
        strXML = strXML & "<categories>"

        For n = 0 To ds.Tables(0).Rows.Count - 1
            strXML = strXML & "<category name='" & ds.Tables(0).Rows(n).Item("ENTRY_DATE").ToString() & "' /> "
        Next

        strXML = strXML & "</categories>"

      

        sql_query = " select DISTINCT TASK_EMP_ID,isnull(EMP_FNAME,'') + ' ' +  isnull(EMP_MNAME ,'') + ' ' + isnull(EMP_LNAME ,'') as EMP_NAME  from dbo.TASK_LIST_MASTER A " & _
                                " RIGHT JOIN dbo.TASK_ASSIGNED_LOG B ON A.TASK_LIST_ID=B.TASK_LIST_ID " & _
                                " RIGHT JOIN dbo.TASK_STATUS_LOG C ON B.TASK_ASSIGN_ID=C.TASK_ASSIGN_ID " & _
                                " INNER JOIN OASIS.dbo.EMPLOYEE_M D ON D.EMP_ID= C.TASK_EMP_ID " & _
                                " INNER JOIN OASIS.dbo.EMPLOYEE_D E ON  E.EMD_EMP_ID= D.EMP_ID " & _
                                " where A.TASK_LIST_ID='" & HiddenTaskListid.Value & "' AND ACTIVE='" & CheckActive.Checked & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

        For n = 0 To ds.Tables(0).Rows.Count - 1
            Dim emp_id = ds.Tables(0).Rows(n).Item("TASK_EMP_ID").ToString()
            Dim emp_name = ds.Tables(0).Rows(n).Item("EMP_NAME").ToString()

            Dim color = getFCColor()

            strXML = strXML & "<dataset seriesName='" & emp_name & "' color='" & color & "'>"

            Dim di
            For di = 0 To dsdates.Tables(0).Rows.Count - 1
                Dim Checkdate = dsdates.Tables(0).Rows(di).Item("ENTRY_DATE").ToString()
                sql_query = " select TOP 1 C.PERCENTAGE from dbo.TASK_LIST_MASTER A " & _
                                     " RIGHT JOIN dbo.TASK_ASSIGNED_LOG B ON A.TASK_LIST_ID=B.TASK_LIST_ID " & _
                                     " RIGHT JOIN dbo.TASK_STATUS_LOG C ON B.TASK_ASSIGN_ID=C.TASK_ASSIGN_ID " & _
                                     " INNER JOIN OASIS.dbo.EMPLOYEE_M D ON D.EMP_ID= C.TASK_EMP_ID " & _
                                     " INNER JOIN OASIS.dbo.EMPLOYEE_D E ON  E.EMD_EMP_ID= D.EMP_ID " & _
                                     " where A.TASK_LIST_ID='" & HiddenTaskListid.Value & "' AND ACTIVE='" & CheckActive.Checked & "' AND TASK_EMP_ID='" & emp_id & "'" & _
                                     " AND C.ENTRY_DATE  >=ISNULL('" & Checkdate & "',C.ENTRY_DATE)  AND  C.ENTRY_DATE  <=ISNULL(DATEADD(dd, 1, '" & Checkdate & "' ),C.ENTRY_DATE) " & _
                                     " ORDER BY TASK_STATUS_LOG_ID DESC "
                Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

                If ds1.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds1.Tables(0).Rows.Count - 1
                        Dim percentage = ds1.Tables(0).Rows(i).Item("PERCENTAGE").ToString()
                        strXML = strXML & "<set value='" & percentage & "' /> "
                    Next
                Else
                    strXML = strXML & "<set value='" & 0 & "' /> "
                End If

            Next

            strXML = strXML & "</dataset>"
        Next

        strXML = strXML & "</graph>"

        Return FusionCharts.RenderChartHTML("swf/" & ddReportList.SelectedValue, "", strXML, "myNext", "600", "400", False)

    End Function

    Public arr_FCColors(20) As String
    Public FC_ColorCounter As Integer
    Public Sub New()
        'This page contains an array of colors to be used as default set of colors for FusionCharts
        'arr_FCColors is the array that would contain the hex code of colors 
        'ALL COLORS HEX CODES TO BE USED WITHOUT #


        'We also initiate a counter variable to help us cyclically rotate through
        'the array of colors.
        FC_ColorCounter = 0

        arr_FCColors(0) = "1941A5" 'Dark Blue
        arr_FCColors(1) = "AFD8F8"
        arr_FCColors(2) = "F6BD0F"
        arr_FCColors(3) = "8BBA00"
        arr_FCColors(4) = "A66EDD"
        arr_FCColors(5) = "F984A1"
        arr_FCColors(6) = "CCCC00" 'Chrome Yellow+Green
        arr_FCColors(7) = "999999" 'Grey
        arr_FCColors(8) = "0099CC" 'Blue Shade
        arr_FCColors(9) = "FF0000" 'Bright Red 
        arr_FCColors(10) = "006F00" 'Dark Green
        arr_FCColors(11) = "0099FF" 'Blue (Light)
        arr_FCColors(12) = "FF66CC" 'Dark Pink
        arr_FCColors(13) = "669966" 'Dirty green
        arr_FCColors(14) = "7C7CB4" 'Violet shade of blue
        arr_FCColors(15) = "FF9933" 'Orange
        arr_FCColors(16) = "9900FF" 'Violet
        arr_FCColors(17) = "99FFCC" 'Blue+Green Light
        arr_FCColors(18) = "CCCCFF" 'Light violet
        arr_FCColors(19) = "669900" 'Shade of green
    End Sub


    'getFCColor method helps return a color from arr_FCColors array. It uses
    'cyclic iteration to return a color from a given index. The index value is
    'maintained in FC_ColorCounter

    Public Function getFCColor() As String
        'Update index
        FC_ColorCounter = FC_ColorCounter + 1
        'Return color
        Return arr_FCColors(FC_ColorCounter Mod UBound(arr_FCColors))
    End Function

    Protected Sub ddReportList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddReportList.SelectedIndexChanged
        FCLiteral.Text = CreateChart()
    End Sub

    Protected Sub CheckActive_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckActive.CheckedChanged
        FCLiteral.Text = CreateChart()
    End Sub

End Class



