<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdIssueChart.ascx.vb" Inherits="HelpDesk_FusionCharts_UserControls_hdUserTaskProgressChart" %>
 
<script type="text/javascript">

    function callval(a) {

        var path = window.location.href
        if (path.indexOf('?') != '-1') {
            window.location.href = path.substring(0, path.indexOf('?')) + '?tcid=' + a;
        }
        else {
            window.location.href = path + '?tcid=' + a;
        }
    }

    function cprint() {
        
        var subid = document.getElementById("<%=ddSubTab.ClientID %>").value;
        var path = window.location.href
        if (path.indexOf('?tcid') > 0) {
            var catid = path.substring(path.indexOf('?'), path.length) + '&subtabid=' + subid;

            window.showModalDialog("hdIssueChartPrint.aspx" + catid, "", "dialogWidth:800px; dialogHeight:600px; center:yes");
        }
        else {
            alert('Please select category by clicking on the chart bar.');
        }
    
    }


</script>

<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-phone-square mr-3"></i>
            Task Category -Total Issues - Graph
        </div>

 <div class="card-body">
            <div class="table-responsive m-auto">
    
    <table width="100%" align="center">
        
        



            <tr>
                        <td  width = "20%" align="left" >
                             <span class="field-label">
                            Main Tab</span>
                        </td>
                        
                        <td align="left" width = "30%">
                           
                            <asp:DropDownList ID="ddMainTab" runat="server">
                </asp:DropDownList>
                                
                        </td>
                        <td  width = "20%" align="left">
                           <span class="field-label"> Departments</span>
                        </td>
                        
                        <td  width = "30%">
                           
                             <asp:DropDownList ID="ddSubTab" runat="server">
                </asp:DropDownList>
                        </td>
                    </tr>


        <tr>
                        <td  width = "20%" align="left" >
                             <span class="field-label">
                            Chart</span>
                        </td>
                        
                        <td align="left" width = "30%">
                           
                           <asp:DropDownList ID="ddReportList" runat="server">
        <%--<asp:ListItem Text="Line" Value="FCF_Line.swf"></asp:ListItem>--%>
                                <asp:ListItem Text="Column2D" Value="FCF_Column2D.swf"></asp:ListItem>
                                <asp:ListItem Selected="True" Text="Column3D" Value="FCF_Column3D.swf"></asp:ListItem>
                            </asp:DropDownList>
                                
                        </td>
                        <td  width = "20%" align="left">
                           
                        </td>
                        
                        <td  width = "30%">
                           
                             
                        </td>
                    </tr>
        <tr>
        <td colspan="4" >
            <span class="field-label">
            Note:Click on each block to get more details of each category</span>

        </td>   
        </tr>

        <tr>
        <td colspan="4" align="center" >
            <asp:Button ID="btnView" runat="server" CssClass="button" 
                Text="View" Width="122px" />

        </td>   
        </tr>



        
        </table>
  
    
    <asp:Literal ID="FCLiteral" runat="server"></asp:Literal>
    
    <table class="style1">
        <tr>
            <td valign="bottom">
                <asp:Literal ID="FCLiteral1" runat="server"></asp:Literal>
            </td>
            <td valign="bottom">
                <asp:Literal ID="FCLiteral2" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td valign="bottom">
                <asp:Literal ID="FCLiteral3" runat="server"></asp:Literal>
            </td>
            <td valign="bottom">
                <asp:Literal ID="FCLiteral4" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <br />
</div></div></div>
<asp:LinkButton ID="lblprint"  OnClientClick="cprint();return false;"  runat="server">Print Graph</asp:LinkButton>

