<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdUserTaskProgressChart.ascx.vb" Inherits="HelpDesk_FusionCharts_UserControls_hdUserTaskProgressChart" %>

<div class="matters" align="center">
<br />
<br />
<br />
<br />
<asp:Literal ID="FCLiteral" runat="server"></asp:Literal><asp:HiddenField ID="HiddenGraphId" runat="server" />
<br />
<asp:DropDownList ID="ddReportList" runat="server" AutoPostBack="True">
    <asp:ListItem Text="Line" Value="FCF_Line.swf"></asp:ListItem>
    <asp:ListItem Text="Area2D" Value="FCF_Area2D.swf"></asp:ListItem>
    <asp:ListItem Text="Column2D"  Value="FCF_Column2D.swf"></asp:ListItem>
    <asp:ListItem Text="Column3D" Selected="True" Value="FCF_Column3D.swf"></asp:ListItem>
</asp:DropDownList>
<asp:Button ID="btncancel" runat="server" CssClass="button" OnClientClick="javascript:window.close();" Text="Cancel" Width="80px" />
<asp:HiddenField ID="HiddenAssignedTaskid" runat="server" />
<asp:HiddenField ID="HiddenEmpID" runat="server" />
</div>