Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports InfoSoftGlobal


Partial Class HelpDesk_FusionCharts_UserControls_hdUserTaskProgressChart
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindMainTab()
        End If
        If Request.QueryString("tcid") <> Nothing Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString
            Dim sql_query = "select CATEGORY_DES from dbo.TASK_CATEGORY_MASTER a inner join dbo.TASK_CATEGORY b on a.CATEGORY_ID=b.id where task_category_id=" & Request.QueryString("tcid") & ""
            Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sql_query)
            FCLiteral.Text = CreateCharts()
            FCLiteral1.Text = CreateCharts(Request.QueryString("tcid"), val & "-Issue came from (BSU)", "BSU", "Count", 1)
            FCLiteral2.Text = CreateCharts(Request.QueryString("tcid"), val & "-Issue clamed by (BSU)", "BSU", "Count", 2)
            FCLiteral3.Text = CreateCharts(Request.QueryString("tcid"), val & "-Issue Priority", "Priority", "Count", 3)
            FCLiteral4.Text = CreateCharts(Request.QueryString("tcid"), val & "-Issue status", "Status", "Count", 4)
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Public Sub BindMainTab()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select MAIN_TAB_ID,MAIN_TAB_DESCRIPTION from  MAIN_TAB_MASTER WHERE ACTIVE='True'"
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddMainTab.DataSource = ds
        ddMainTab.DataTextField = "MAIN_TAB_DESCRIPTION"
        ddMainTab.DataValueField = "MAIN_TAB_ID"
        ddMainTab.DataBind()

        If Session("main_tab") <> "" Then
            ddMainTab.SelectedValue = Session("main_tab")
        End If

        BindSubTabs()


    End Sub

    Public Sub BindSubTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = ""
        If Library.LibrarySuperAcess(Session("EmployeeId")) Then
            Sql_Query = "Select SUB_TAB_ID,SUB_TAB_DESCRIPTION from  SUB_TAB_MASTER where MAIN_TAB_ID='" & ddMainTab.SelectedValue & "' AND ACTIVE='True'"
        Else
            Sql_Query = " Select DISTINCT A.SUB_TAB_ID,SUB_TAB_DESCRIPTION from  SUB_TAB_MASTER A " & _
                        " INNER JOIN (SELECT TASK_ROOTING_MASTER.SUB_TAB_ID " & _
                        " FROM TASK_LIST_MASTER INNER JOIN TASK_CONTACT_MASTER ON TASK_LIST_MASTER.TASK_ID = TASK_CONTACT_MASTER.TASK_ID INNER JOIN " & _
                        " TASK_ROOTING_MASTER ON TASK_ROOTING_MASTER.TASK_CATEGORY_ID=TASK_LIST_MASTER.TASK_CATEGORY and EMP_TASK_BSU_ID=CALLER_BSU_ID " & _
                        " WHERE TASK_CATEGORY_OWNER='True' AND EMP_ID=" & Session("EmployeeId") & "" & _
                        " ) B ON A.SUB_TAB_ID=B.SUB_TAB_ID WHERE  MAIN_TAB_ID='" & ddMainTab.SelectedValue & "' AND ACTIVE='True' "


        End If



        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddSubTab.DataSource = ds
        ddSubTab.DataTextField = "SUB_TAB_DESCRIPTION"
        ddSubTab.DataValueField = "SUB_TAB_ID"
        ddSubTab.DataBind()

        Dim list As New ListItem
        list.Text = "Select Category"
        list.Value = "-1"
        ddSubTab.Items.Insert(0, list)

        If Session("sub_tab") <> "" Then
            ddSubTab.SelectedValue = Session("sub_tab")
        End If

        If Session("graph") <> "" Then
            ddReportList.SelectedValue = Session("graph")
        End If

    End Sub


    Public Function CreateCharts() As String
        Dim strXML As String
        strXML = ""
        strXML = strXML & "<graph caption='Issues' xAxisName='Category' yAxisName='No of Issues' decimalPrecision='0' rotateNames='1' formatNumberScale='0' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='0' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC'>"
        strXML = strXML & xml().ToString()
        strXML = strXML & "</graph>"
        Return FusionCharts.RenderChartHTML("swf/" & ddReportList.SelectedValue, "", strXML, "myNext", "600", "400", False)

    End Function

    Public Function CreateCharts(ByVal task_cat_id As String, ByVal Header As String, ByVal xval As String, ByVal yval As String, ByVal qoption As Integer) As String
        Dim strXML As String
        strXML = ""
        strXML = strXML & "<graph caption='" & Header & "' xAxisName='" & xval & "' yAxisName='" & yval & "' decimalPrecision='0' rotateNames='1' formatNumberScale='0' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='0' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC'>"
        strXML = strXML & xml(qoption, task_cat_id).ToString()
        strXML = strXML & "</graph>"
        Return FusionCharts.RenderChartHTML("swf/FCF_Pie3D.swf", "", strXML, "myNext", "600", "400", False)

    End Function

    Public Function xml() As StringBuilder

        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString
            Dim strXML As New StringBuilder

            Dim Sql_Query = ""
            If Library.LibrarySuperAcess(Session("EmployeeId")) Then

                Sql_Query = " select TASK_CATEGORY,TASK_CATEGORY_DESCRIPTION ,count(TASK_CATEGORY)COUNT from dbo.VIEW_TASK_SEARCH_FOR_TASK_CATEGORY where  " & _
                                           " TASK_CATEGORY in(Select A.TASK_CATEGORY_ID from TASK_CATEGORY_MASTER A INNER JOIN TASK_CATEGORY B ON B.ID=A.CATEGORY_ID AND SUB_TAB_ID='" & ddSubTab.SelectedValue & "') " & _
                                           " group by TASK_CATEGORY,TASK_CATEGORY_DESCRIPTION  "

            Else


                Sql_Query = " select TASK_CATEGORY,TASK_CATEGORY_DESCRIPTION ,count(TASK_CATEGORY)COUNT from dbo.VIEW_TASK_SEARCH_FOR_TASK_CATEGORY where  " & _
                                           " TASK_CATEGORY in(Select A.TASK_CATEGORY_ID from TASK_CATEGORY_MASTER A INNER JOIN TASK_CATEGORY B ON B.ID=A.CATEGORY_ID AND SUB_TAB_ID='" & ddSubTab.SelectedValue & "') " & _
                                           "  AND TASK_LIST_ID IN (SELECT TASK_LIST_ID  FROM TASK_LIST_MASTER INNER JOIN " & _
                                           " TASK_CONTACT_MASTER ON TASK_LIST_MASTER.TASK_ID = TASK_CONTACT_MASTER.TASK_ID INNER JOIN " & _
                                           " TASK_ROOTING_MASTER ON TASK_ROOTING_MASTER.TASK_CATEGORY_ID=TASK_LIST_MASTER.TASK_CATEGORY and EMP_TASK_BSU_ID=CALLER_BSU_ID " & _
                                           " WHERE TASK_CATEGORY_OWNER='True' AND EMP_ID=" & Session("EmployeeId") & " ) " & _
                                           " group by TASK_CATEGORY,TASK_CATEGORY_DESCRIPTION  "

            End If

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
            Dim jval = ""
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                jval = "javascript:callval(" & ds.Tables(0).Rows(i).Item("TASK_CATEGORY").ToString() & ")"
                strXML.Append("<set name='" & ds.Tables(0).Rows(i).Item("TASK_CATEGORY_DESCRIPTION").ToString() & "' value='" & ds.Tables(0).Rows(i).Item("COUNT").ToString() & "' color='" & getFCColor() & "' link='" & jval & "' />")
            Next

            Return strXML

        Catch ex As Exception

        End Try


    End Function

    Public Function xml(ByVal qoption As Integer, ByVal taskid As Integer) As StringBuilder

        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString
            Dim strXML As New StringBuilder

            Dim sql_query = ""
            If Library.LibrarySuperAcess(Session("EmployeeId")) Then

                If qoption = 1 Then
                    sql_query = " select c.BSU_SHORTNAME AS DATA,COUNT(TASK_LIST_ID)COUNT from dbo.VIEW_TASK_SEARCH_FOR_TASK_CATEGORY  a  " & _
                                " inner join oasis.dbo.employee_m b on a.CLAIM_EMP_ID=b.emp_id inner join OASIS.dbo.BUSINESSUNIT_M c on c.bsu_id=CALLER_BSU_ID " & _
                                " WHERE TASK_CATEGORY=" & taskid & " GROUP BY c.BSU_SHORTNAME "

                End If

                If qoption = 2 Then
                    sql_query = " select c.BSU_SHORTNAME AS DATA,COUNT(TASK_LIST_ID)COUNT from dbo.VIEW_TASK_SEARCH_FOR_TASK_CATEGORY  a  " & _
                                " inner join oasis.dbo.employee_m b on a.CLAIM_EMP_ID=b.emp_id inner join OASIS.dbo.BUSINESSUNIT_M c on c.bsu_id=EMP_BSU_ID  " & _
                                " WHERE TASK_CATEGORY=" & taskid & " GROUP BY c.BSU_SHORTNAME "

                End If

                If qoption = 3 Then
                    sql_query = " SELECT PRIORITY_DESCRIPTION AS DATA,COUNT(PRIORITY_DESCRIPTION)COUNT FROM  dbo.VIEW_TASK_SEARCH_FOR_TASK_CATEGORY  " & _
                                " WHERE TASK_CATEGORY=" & taskid & " GROUP BY PRIORITY_DESCRIPTION "
                End If

                If qoption = 4 Then
                    sql_query = " SELECT TASK_STATUS_DESCRIPTION AS DATA,COUNT(TASK_STATUS_DESCRIPTION)COUNT FROM  dbo.VIEW_TASK_SEARCH_FOR_TASK_CATEGORY  " & _
                                " WHERE TASK_CATEGORY=" & taskid & " GROUP BY TASK_STATUS_DESCRIPTION "
                End If

            Else


                If qoption = 1 Then
                    sql_query = " select c.BSU_SHORTNAME AS DATA,COUNT(TASK_LIST_ID)COUNT from dbo.VIEW_TASK_SEARCH_FOR_TASK_CATEGORY  a  " & _
                                " inner join oasis.dbo.employee_m b on a.CLAIM_EMP_ID=b.emp_id inner join OASIS.dbo.BUSINESSUNIT_M c on c.bsu_id=CALLER_BSU_ID " & _
                                " WHERE TASK_CATEGORY=" & taskid & "" & _
                                " AND TASK_LIST_ID IN (SELECT TASK_LIST_ID  FROM TASK_LIST_MASTER INNER JOIN " & _
                                " TASK_CONTACT_MASTER ON TASK_LIST_MASTER.TASK_ID = TASK_CONTACT_MASTER.TASK_ID INNER JOIN " & _
                                " TASK_ROOTING_MASTER ON TASK_ROOTING_MASTER.TASK_CATEGORY_ID=TASK_LIST_MASTER.TASK_CATEGORY and EMP_TASK_BSU_ID=CALLER_BSU_ID " & _
                                " WHERE TASK_CATEGORY_OWNER='True' AND EMP_ID=" & Session("EmployeeId") & " ) " & _
                                " GROUP BY c.BSU_SHORTNAME "

                End If

                If qoption = 2 Then
                    sql_query = " select c.BSU_SHORTNAME AS DATA,COUNT(TASK_LIST_ID)COUNT from dbo.VIEW_TASK_SEARCH_FOR_TASK_CATEGORY  a  " & _
                                " inner join oasis.dbo.employee_m b on a.CLAIM_EMP_ID=b.emp_id inner join OASIS.dbo.BUSINESSUNIT_M c on c.bsu_id=EMP_BSU_ID  " & _
                                " WHERE TASK_CATEGORY=" & taskid & "" & _
                                " AND TASK_LIST_ID IN (SELECT TASK_LIST_ID  FROM TASK_LIST_MASTER INNER JOIN " & _
                                " TASK_CONTACT_MASTER ON TASK_LIST_MASTER.TASK_ID = TASK_CONTACT_MASTER.TASK_ID INNER JOIN " & _
                                " TASK_ROOTING_MASTER ON TASK_ROOTING_MASTER.TASK_CATEGORY_ID=TASK_LIST_MASTER.TASK_CATEGORY and EMP_TASK_BSU_ID=CALLER_BSU_ID " & _
                                " WHERE TASK_CATEGORY_OWNER='True' AND EMP_ID=" & Session("EmployeeId") & " ) " & _
                                " GROUP BY c.BSU_SHORTNAME "

                End If

                If qoption = 3 Then
                    sql_query = " SELECT PRIORITY_DESCRIPTION AS DATA,COUNT(PRIORITY_DESCRIPTION)COUNT FROM  dbo.VIEW_TASK_SEARCH_FOR_TASK_CATEGORY  " & _
                                " WHERE TASK_CATEGORY=" & taskid & "" & _
                                " AND TASK_LIST_ID IN (SELECT TASK_LIST_ID  FROM TASK_LIST_MASTER INNER JOIN " & _
                                " TASK_CONTACT_MASTER ON TASK_LIST_MASTER.TASK_ID = TASK_CONTACT_MASTER.TASK_ID INNER JOIN " & _
                                " TASK_ROOTING_MASTER ON TASK_ROOTING_MASTER.TASK_CATEGORY_ID=TASK_LIST_MASTER.TASK_CATEGORY and EMP_TASK_BSU_ID=CALLER_BSU_ID " & _
                                " WHERE TASK_CATEGORY_OWNER='True' AND EMP_ID=" & Session("EmployeeId") & " ) " & _
                                " GROUP BY PRIORITY_DESCRIPTION "
                End If

                If qoption = 4 Then
                    sql_query = " SELECT TASK_STATUS_DESCRIPTION AS DATA,COUNT(TASK_STATUS_DESCRIPTION)COUNT FROM  dbo.VIEW_TASK_SEARCH_FOR_TASK_CATEGORY  " & _
                                " WHERE TASK_CATEGORY=" & taskid & "" & _
                                " AND TASK_LIST_ID IN (SELECT TASK_LIST_ID  FROM TASK_LIST_MASTER INNER JOIN " & _
                                " TASK_CONTACT_MASTER ON TASK_LIST_MASTER.TASK_ID = TASK_CONTACT_MASTER.TASK_ID INNER JOIN " & _
                                " TASK_ROOTING_MASTER ON TASK_ROOTING_MASTER.TASK_CATEGORY_ID=TASK_LIST_MASTER.TASK_CATEGORY and EMP_TASK_BSU_ID=CALLER_BSU_ID " & _
                                " WHERE TASK_CATEGORY_OWNER='True' AND EMP_ID=" & Session("EmployeeId") & " ) " & _
                                " GROUP BY TASK_STATUS_DESCRIPTION "
                End If



            End If



            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
            Dim jval = ""
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                strXML.Append("<set name='" & ds.Tables(0).Rows(i).Item("DATA").ToString() & "' value='" & ds.Tables(0).Rows(i).Item("COUNT").ToString() & "'/>")
            Next

            Return strXML

        Catch ex As Exception

        End Try


    End Function

    Public arr_FCColors(20) As String
    Public FC_ColorCounter As Integer
    Public Sub New()
        'This page contains an array of colors to be used as default set of colors for FusionCharts
        'arr_FCColors is the array that would contain the hex code of colors 
        'ALL COLORS HEX CODES TO BE USED WITHOUT #


        'We also initiate a counter variable to help us cyclically rotate through
        'the array of colors.
        FC_ColorCounter = 0

        arr_FCColors(0) = "1941A5" 'Dark Blue
        arr_FCColors(1) = "AFD8F8"
        arr_FCColors(2) = "F6BD0F"
        arr_FCColors(3) = "8BBA00"
        arr_FCColors(4) = "A66EDD"
        arr_FCColors(5) = "F984A1"
        arr_FCColors(6) = "CCCC00" 'Chrome Yellow+Green
        arr_FCColors(7) = "999999" 'Grey
        arr_FCColors(8) = "0099CC" 'Blue Shade
        arr_FCColors(9) = "FF0000" 'Bright Red 
        arr_FCColors(10) = "006F00" 'Dark Green
        arr_FCColors(11) = "0099FF" 'Blue (Light)
        arr_FCColors(12) = "FF66CC" 'Dark Pink
        arr_FCColors(13) = "669966" 'Dirty green
        arr_FCColors(14) = "7C7CB4" 'Violet shade of blue
        arr_FCColors(15) = "FF9933" 'Orange
        arr_FCColors(16) = "9900FF" 'Violet
        arr_FCColors(17) = "99FFCC" 'Blue+Green Light
        arr_FCColors(18) = "CCCCFF" 'Light violet
        arr_FCColors(19) = "669900" 'Shade of green
    End Sub

    'getFCColor method helps return a color from arr_FCColors array. It uses
    'cyclic iteration to return a color from a given index. The index value is
    'maintained in FC_ColorCounter

    Public Function getFCColor() As String
        'Update index
        FC_ColorCounter = FC_ColorCounter + 1
        'Return color
        Return arr_FCColors(FC_ColorCounter Mod UBound(arr_FCColors))
    End Function

    Protected Sub btnview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnview.Click
        Session("main_tab") = ddMainTab.SelectedValue
        Session("sub_tab") = ddSubTab.SelectedValue
        Session("graph") = ddReportList.SelectedValue
        FCLiteral.Text = CreateCharts()
    End Sub
End Class
