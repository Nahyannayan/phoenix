<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskProgressCompare.ascx.vb" Inherits="HelpDesk_FusionCharts_UserControls_hdTaskProgressCompare" %>




   <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
   <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 


<div class="card mb-3">
        
 <div class="card-body">
            <div class="table-responsive m-auto">

                                
                    <table width="100%" align="center">
    
    <tr>
        <td width="20%"></td>
        <td width="30%"></td>
        <td width="20%"></td>
        <td width="30%"></td>
    </tr>
<tr>

<td colspan="4" align="center">
    <asp:CheckBox ID="CheckActive" runat="server" AutoPostBack="True" Checked="True"
        Text="Active Members" />

</td>
</tr>
<tr>
    <td colspan="4" align="center">

        <asp:Literal ID="FCLiteral" runat="server"></asp:Literal><asp:HiddenField ID="HiddenGraphId" runat="server" />
        </td>

</tr>

<tr>
    <td colspan="4" align="center">
        <asp:DropDownList ID="ddReportList" runat="server" AutoPostBack="True" Width="300px">
    <asp:ListItem Text="Line" Value="FCF_MSLine.swf"></asp:ListItem>
    <asp:ListItem Text="Bar 2D" Value="FCF_MSBar2D.swf"></asp:ListItem>
    <asp:ListItem Text="Column2D"  Value="FCF_MSColumn2D.swf"></asp:ListItem>
    <asp:ListItem Text="Column3D" Selected="True" Value="FCF_MSColumn3D.swf"></asp:ListItem>
</asp:DropDownList>

        
        </td>

</tr>

<tr>
    <td colspan="4" align="center">
        
<asp:Button ID="btncancel" runat="server" CssClass="button" OnClientClick="javascript:window.close();" Text="Cancel" Width="80px" />
        
        </td>

</tr>
    



<asp:HiddenField ID="HiddenTaskListid" runat="server" />
<asp:HiddenField ID="HiddenEmpID" runat="server" />
                    </table>
</div></div></div>