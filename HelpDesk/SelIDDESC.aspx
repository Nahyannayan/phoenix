<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SelIDDESC.aspx.vb" Inherits="SelBussinessUnit" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Business Unit Selection</title>
    <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />
     <base target="_self" />
    <script language="javascript" type="text/javascript" src="../../../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
    function ChangeCheckBoxState(id, checkState)
        {
            var cb = document.getElementById(id);
            if (cb != null)
               cb.checked = checkState;
        }
        
        function ChangeAllCheckBoxStates(checkState)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked; 
           
            
            if (CheckBoxIDs != null)
            {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                   ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }
       
      function menu_click(val,mid)
                {
                //alert(val);
                var path;
                if (val=='LI')
                {
                path='../../../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../../../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../../../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../../../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../../../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../../../Images/operations/notendswith.gif';
                }
               if (mid==1)
                 {
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value=val+'__'+path;
                 document.getElementById('<%=getid1() %>').src = path;
                 }
                 else  if (mid==2)
                 {
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value=val+'__'+path;
                 document.getElementById('<%=getid2() %>').src = path;
                 }
                  else  if (mid==3)
                 {
                document.getElementById("<%=h_selected_menu_3.ClientID %>").value=val+'__'+path;
                 document.getElementById('<%=getid3() %>').src = path;
                 }                 
                  else  if (mid==4)
                 {
                document.getElementById("<%=h_Selected_menu_4.ClientID %>").value=val+'__'+path;
                 document.getElementById('<%=getid4() %>').src = path;
                 }                 
                }//end fn
    </script>
</head>
<body  onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
<!--1st drop down menu -->                                                   
<div id="dropmenu1" class="dropmenudiv" style="width: 110px;">
<a href="javascript:menu_click('LI',1);"><img class="img_left" alt="Any where" src= "../../../Images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:menu_click('NLI',1);"><img class="img_left" alt="Not In" src= "../../../Images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:menu_click('SW',1);"><img class="img_left" alt="Starts With" src= "../../../Images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:menu_click('NSW',1);"><img class="img_left" alt="Like" src= "../../../Images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:menu_click('EW',1);"><img class="img_left" alt="Like" src= "../../../Images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:menu_click('NEW',1);"><img class="img_left" alt="Like" src= "../../../Images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>   
<!--2nd drop down menu -->                                                
<div id="dropmenu2" class="dropmenudiv" style="width: 110px;">
<a href="javascript:menu_click('LI',2);"><img class="img_left" alt="Like" src= "../../../Images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:menu_click('NLI',2);"><img class="img_left" alt="Like" src= "../../../Images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:menu_click('SW',2);"><img class="img_left" alt="Like" src= "../../../Images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:menu_click('NSW',2);"><img class="img_left" alt="Like" src= "../../../Images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:menu_click('EW',2);"><img class="img_left" alt="Like" src= "../../../Images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:menu_click('NEW',2);"><img class="img_left" alt="Like" src= "../../../Images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>
<!--3nd drop down menu -->                                                
<div id="dropmenu3" class="dropmenudiv" style="width: 110px;">
<a href="javascript:menu_click('LI',3);"><img class="img_left" alt="Like" src= "../../../Images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:menu_click('NLI',3);"><img class="img_left" alt="Like" src= "../../../Images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:menu_click('SW',3);"><img class="img_left" alt="Like" src= "../../../Images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:menu_click('NSW',3);"><img class="img_left" alt="Like" src= "../../../Images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:menu_click('EW',3);"><img class="img_left" alt="Like" src= "../../../Images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:menu_click('NEW',3);"><img class="img_left" alt="Like" src= "../../../Images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>
 
    <form id="form1" runat="server">
     <table id="tbl" width="100%" align="center" border="0" cellpadding="0" cellspacing="0"> 
                    <tr valign = "top">
                        <td> 
                        </td>
                        <td align="center" style="text-align: left; height: 367px;">
                            &nbsp; 
                        </td> 
                            <td><asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data" Width="100%" AllowPaging="True">
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Select" SortExpression="ID">
                                        <ItemTemplate>
                                            &nbsp;<input id="chkControl" runat="server" type="checkbox" value='<%# Bind("ID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            Select
                                            <input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Id" SortExpression="ID">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>' ></asp:Label>
                                            <asp:Label ID="lblEMPNO" runat="server" Text='<%# bind("EMP_NO") %>' Visible="False"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <table style="width: 100%; height: 100%">
                                                <tr>
                                                    <td align="center" style="width: 100px">
                                                        <asp:Label ID="lblID" runat="server" CssClass="gridheader_text"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100px">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 100px; height: 12px">
                                                                    <div id="chromemenu1" class="chromestyle">
                                                                        <ul>
                                                                            <li><a href="#" rel="dropmenu1">
                                                                                <img id="mnu_1_img" runat="server" align="middle" alt="Menu" border="0" src="../../../Images/operations/like.gif" /><span
                                                                                    style="color: #0000ff; text-decoration: none">&nbsp;</span></a> </li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                                <td style="width: 100px; height: 12px">
                                                                    <asp:TextBox ID="txtCode" runat="server" Width="72px"></asp:TextBox></td>
                                                                <td style="width: 100px; height: 12px" valign="middle">
                                                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../../../Images/forum_search.gif" OnClick="btnCodeSearch_Click"
                                                                         />&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name" SortExpression="NAME">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>&nbsp;<br />
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <table style="width: 100%; height: 100%">
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="lblName" runat="server" CssClass="gridheader_text"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="height: 12px">
                                                                    <div id="chromemenu2" class="chromestyle">
                                                                        <ul>
                                                                            <li><a href="#" rel="dropmenu2">
                                                                                <img id="mnu_2_img" runat="server" align="middle" alt="Menu" border="0" src="../../../Images/operations/like.gif" /><span
                                                                                    style="color: #0000ff; text-decoration: none">&nbsp;</span></a> </li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                                <td style="width: 100px; height: 12px">
                                                                    <asp:TextBox ID="txtName" runat="server" Width="150px"></asp:TextBox></td>
                                                                <td style="width: 100px; height: 12px" valign="middle">
                                                                    <asp:ImageButton ID="btnNameSearch" runat="server" ImageAlign="Top" ImageUrl="../../../Images/forum_search.gif" OnClick="btnNameSearch_Click"
                                                                         />&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BSU Name" SortExpression="NAME">
                                        <HeaderTemplate>
                                            <table style="width: 100%; height: 100%">
                                                <tr>
                                                    <td align="center" style="width: 100px">
                                                        <asp:Label ID="lblBSUName" runat="server" CssClass="gridheader_text"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100px">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 100px; height: 12px">
                                                                    <div id="chromemenu3" class="chromestyle">
                                                                        <ul>
                                                                            <li><a href="#" rel="dropmenu3">
                                                                                <img id="mnu_3_img" runat="server" align="middle" alt="Menu" border="0" src="../../../Images/operations/like.gif" /><span
                                                                                    style="color: #0000ff; text-decoration: none">&nbsp;</span></a> </li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                                <td style="width: 100px; height: 12px">
                                                                    <asp:TextBox ID="txtBSUName" runat="server" Width="72px"></asp:TextBox></td>
                                                                <td style="width: 100px; height: 12px" valign="middle">
                                                                    <asp:ImageButton ID="btnBSUNameSearch" runat="server" ImageAlign="Top" ImageUrl="../../../Images/forum_search.gif" OnClick="btnNameSearch_Click"
                                                                         />&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="linklblBSUName" runat="server" Text='<%# Bind("DESCR") %>' OnClick="linklblBSUName_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:CheckBox ID="chkSelAll" runat="server" CssClass="radiobutton" Text="Select All"
                                Width="76px" AutoPostBack="True" />
                        </td>
                    </tr>
                    <tr> 
                        <td align="center" colspan="3" >
                             <asp:Button ID="btnFinish" runat="server" Text="Finish" CssClass="button" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_SelectedId" runat="server" type="hidden" value="" /></td>
                    </tr>
                </table>
            <script type="text/javascript">
                cssdropdown.startchrome("chromemenu1");
                if('<%=Request.QueryString("multiSelect") %>' == '' || '<%=Request.QueryString("multiSelect") %>' == 'true')
                { 
                    cssdropdown.startchrome("chromemenu2");
                }
                else
                {
                    cssdropdown.startchrome("chromemenu3");
                }
                
            </script>
    </form>
</body>
</html>