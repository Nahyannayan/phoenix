Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI
Partial Class HelpDesk_UserControls_hdTaskReassignEnter
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            HiddenTaskCategory.Value = Request.QueryString("task_category_id").ToString()
            HiddenTaskListID.Value = Request.QueryString("task_list_id").ToString()
            HiddenUserType.Value = Request.QueryString("Usertype").ToString()
            HiddenAssignedTaskid.Value = Request.QueryString("task_AssignedTaskid").ToString()
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

            HiddenSubTabID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "SELECT SUB_TAB_ID FROM TASK_CATEGORY_MASTER WHERE TASK_CATEGORY_ID='" & HiddenTaskCategory.Value & "'")



            HiddenBsuid.Value = Session("sbsuid") ''"999998"
            HiddenEmpID.Value = Session("EmployeeId") ''"12581"
            CheckAssignedTaskActive()
            BindTaskMembers()
            BindPriority()
            BindControl()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Public Sub CheckAssignedTaskActive()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "SELECT * FROM TASK_ASSIGNED_LOG WHERE TASK_ASSIGN_ID='" & HiddenAssignedTaskid.Value & "' and ACTIVE='False'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        If ds.Tables(0).Rows.Count > 0 Then
            '' Close the window
            Response.Write("<script type='text/javascript'> alert('This task has been reassigned.'); window.close(); </script> ")

        End If

    End Sub


    Public Sub BindControl()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = " SELECT ENTRY_DATE,isnull(emp_displayname,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_LNAME, '')) AS EMP_NAME,TASK_ASSIGN_DESCRIPTION,TASK_ASSIGNED_TO_EMP_ID,TASK_ASSIGN_START_DATE,TASK_ASSIGN_END_DATE,TASK_ASSIGN_PRIORITY,UPLOAD_IDS FROM TASK_ASSIGNED_LOG A " & _
                        " LEFT JOIN OASIS.dbo.EMPLOYEE_M B ON A.TASK_ASSIGNED_TO_EMP_ID=B.EMP_ID " & _
                        " WHERE TASK_ASSIGN_ID='" & HiddenAssignedTaskid.Value & "'"

        'Dim Sql_Query = "SELECT * FROM TASK_ASSIGNED_LOG WHERE TASK_ASSIGN_ID='" & HiddenAssignedTaskid.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            RadioTaskMembers.SelectedValue = ds.Tables(0).Rows(0).Item("TASK_ASSIGNED_TO_EMP_ID").ToString()
            txtdetails.Content = "<br><br><br><br><hr>-Previously Assigned Message (Below)-<hr><br>" & _
                                 "<b>Was Assigned To :" & ds.Tables(0).Rows(0).Item("EMP_NAME").ToString() & _
                                 "<br>Date     :" & ds.Tables(0).Rows(0).Item("ENTRY_DATE").ToString() & _
                                 "</b><br><br>" & _
                                 ds.Tables(0).Rows(0).Item("TASK_ASSIGN_DESCRIPTION").ToString()

            If ds.Tables(0).Rows(0).Item("TASK_ASSIGN_START_DATE").ToString() <> "" Then
                txtstartdate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("TASK_ASSIGN_START_DATE").ToString()).ToString("dd/MMM/yyyy")

            End If
            If ds.Tables(0).Rows(0).Item("TASK_ASSIGN_END_DATE").ToString() <> "" Then
                txtenddate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("TASK_ASSIGN_END_DATE").ToString()).ToString("dd/MMM/yyyy")
            End If
            ddpriority.SelectedValue = ds.Tables(0).Rows(0).Item("TASK_ASSIGN_PRIORITY").ToString()
            HdFileUploadViewAssign1.Uploadids = ds.Tables(0).Rows(0).Item("UPLOAD_IDS").ToString()
            HdFileUploadViewAssign1.ShowCheck = True
        End If

    End Sub
    Public Sub BindTaskMembers()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT DISTINCT A.EMP_ID,EMD_CUR_MOBILE,EMD_EMAIL,isnull(emp_displayname,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_LNAME, '')) EMPNAME, BSU_SHORTNAME , " & _
                        " (CASE ACTIVE WHEN 'TRUE' THEN 'False' ELSE 'True' END) DisableRow " & _
                        " from dbo.TASK_ROOTING_MASTER A " & _
                        " INNER JOIN OASIS.dbo.EMPLOYEE_M B ON  B.EMP_ID=A.EMP_ID " & _
                        " INNER JOIN OASIS.dbo.EMPLOYEE_D D ON D.EMD_EMP_ID = A.EMP_ID " & _
                        " INNER JOIN OASIS.dbo.BUSINESSUNIT_M C ON A.EMP_BSU_ID= C. BSU_ID " & _
                        " LEFT JOIN dbo.TASK_ASSIGNED_LOG ON TASK_LIST_ID='" & HiddenTaskListID.Value & "' AND TASK_ASSIGNED_TO_EMP_ID=A.EMP_ID  AND ACTIVE='TRUE' AND A.SUB_TAB_ID='" & HiddenSubTabID.Value & "'" & _
                        " WHERE  A.TASK_CATEGORY_ID='" & HiddenTaskCategory.Value & "'AND A.TASK_MEMBER='True' " & _
                        " AND A.EMP_ID IN (SELECT EMP_ID FROM TASK_ROOTING_MASTER WHERE EMP_TASK_BSU_ID=(SELECT CALLER_BSU_ID FROM TASK_CONTACT_MASTER WHERE TASK_ID=(SELECT TASK_ID FROM TASK_LIST_MASTER WHERE TASK_LIST_ID='" & HiddenTaskListID.Value & "') ))"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        RadioTaskMembers.DataSource = ds
        RadioTaskMembers.DataTextField = "EMPNAME"
        RadioTaskMembers.DataValueField = "EMP_ID"
        RadioTaskMembers.DataBind()

      


    End Sub
    Public Sub BindPriority()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT * FROM TASK_PRIORITY_MASTER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddpriority.DataSource = ds
        ddpriority.DataTextField = "PRIORITY_DESCRIPTION"
        ddpriority.DataValueField = "RECORD_ID"
        ddpriority.DataBind()

    End Sub
    Function validate() As Boolean

        Dim returnvalue As Boolean = True

        If txtstartdate.Text <> "" And txtenddate.Text <> "" Then
            If Convert.ToDateTime(txtstartdate.Text) <= Convert.ToDateTime(txtenddate.Text) Then
                returnvalue = True
            Else
                returnvalue = False
                lblmessage.Text = "Date Range is not valid."
            End If
        End If



        Return returnvalue

    End Function
    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnok.Click

        If validate() Then

            Dim connection As New System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
            Dim transaction As System.Data.SqlClient.SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            Dim commitflag = 0
            Dim Task_Assign_ID = ""
            Try
                Dim pParms(10) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TASK_ASSIGN_ID", HiddenAssignedTaskid.Value)
                pParms(1) = New SqlClient.SqlParameter("@TASK_ASSIGNED_TO_EMP_ID", RadioTaskMembers.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@TASK_ASSIGN_DESCRIPTION", txtdetails.Content)
                If txtstartdate.Text.Trim() <> "" Then
                    pParms(3) = New SqlClient.SqlParameter("@TASK_ASSIGN_START_DATE", txtstartdate.Text.Trim())
                End If
                If txtenddate.Text.Trim() <> "" Then
                    pParms(4) = New SqlClient.SqlParameter("@TASK_ASSIGN_END_DATE", txtenddate.Text.Trim())
                End If

                pParms(5) = New SqlClient.SqlParameter("@TASK_ASSIGN_PRIORITY", ddpriority.SelectedValue)
                pParms(6) = New SqlClient.SqlParameter("@TASK_ASSIGNED_BY_EMP_ID", HiddenEmpID.Value)
                pParms(7) = New SqlClient.SqlParameter("@SUB_TAB_ID", HiddenSubTabID.Value)

                ''Get attachements
                Dim grid As GridView = DirectCast(HdFileUploadViewAssign1.FindControl("GridUpload"), GridView)
                Dim val2
                Dim uploadid = ""
                For Each row As GridViewRow In grid.Rows
                    Dim check As CheckBox = DirectCast(row.FindControl("CheckUpload"), CheckBox)
                    If check.Checked Then
                        val2 = DirectCast(row.FindControl("HiddenUploadValue"), HiddenField).Value
                        If uploadid = "" Then
                            uploadid = val2
                        Else
                            uploadid = uploadid & "," & val2
                        End If

                    End If

                Next
                If uploadid <> "" Then

                    pParms(8) = New SqlClient.SqlParameter("@UPLOAD_IDS", uploadid)

                End If

                pParms(9) = New SqlClient.SqlParameter("@TASK_RELEASE", checkRelease.Checked)

                Dim val As String = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_TASK_REASSIGN_EMP", pParms)

                Dim values As String() = val.Split("-")
                lblmessage.Text = values(1)
                Task_Assign_ID = values(0)

                transaction.Commit()
                commitflag = 1

            Catch ex As Exception
                If commitflag = 0 Then
                    transaction.Rollback()
                End If
            Finally
                connection.Close()
            End Try

            If commitflag = 1 Then

                SentNotification(Task_Assign_ID, RadioTaskMembers.SelectedValue)

            End If
            btnok.Visible = False

        End If
    End Sub

    Public Sub SentNotification(ByVal Task_Assign_ID As String, ByVal assigned_emp_id As String)
        Try


            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

            Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

            Dim Sql_Query = ""
            Dim ds As DataSet
            ds = HelpDesk.GetCommunicationSettings()
            Dim username = ""
            Dim password = ""
            Dim port = ""
            Dim host = ""
            Dim fromemailid = ""
            Dim reportedby = SqlHelper.ExecuteScalar(str_conn2, CommandType.Text, "SELECT isnull(emp_displayname,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_LNAME, '')) EMPNAME from OASIS.dbo.EMPLOYEE_M  where EMP_ID='" & HiddenEmpID.Value & "'")

            If ds.Tables(0).Rows.Count > 0 Then

                username = ds.Tables(0).Rows(0).Item("USERNAME").ToString()
                password = ds.Tables(0).Rows(0).Item("PASSWORD").ToString()
                port = ds.Tables(0).Rows(0).Item("PORT").ToString()
                host = ds.Tables(0).Rows(0).Item("HOST").ToString()
                fromemailid = ds.Tables(0).Rows(0).Item("FROMEMAIL").ToString()

            End If

            Sql_Query = "select TASK_TITLE,CATEGORY_DES AS TASK_CATEGORY_DESCRIPTION,EMD_EMAIL,EMD_CUR_MOBILE from dbo.TASK_ASSIGNED_LOG A " & _
                                " INNER JOIN dbo.TASK_LIST_MASTER B ON  A.TASK_LIST_ID =B.TASK_LIST_ID " & _
                                " INNER JOIN dbo.TASK_CATEGORY_MASTER C ON C.TASK_CATEGORY_ID = B.TASK_CATEGORY " & _
                                " INNER JOIN  TASK_CATEGORY C1 ON C1.ID= C.CATEGORY_ID " & _
                                " LEFT JOIN OASIS.dbo.EMPLOYEE_D D ON D.EMD_EMP_ID=A.TASK_ASSIGNED_TO_EMP_ID " & _
                                " where TASK_ASSIGN_ID='" & Task_Assign_ID & "'"

            Dim ds3 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

            If ds3.Tables(0).Rows.Count > 0 Then

                Dim category = ds3.Tables(0).Rows(0).Item("TASK_CATEGORY_DESCRIPTION").ToString()
                Dim emailid = ds3.Tables(0).Rows(0).Item("EMD_EMAIL").ToString()
                Dim mobilenumber = ds3.Tables(0).Rows(0).Item("EMD_CUR_MOBILE").ToString()
                Dim title = ds3.Tables(0).Rows(0).Item("TASK_TITLE").ToString()

                ''Sent Email

                If ds.Tables(0).Rows.Count > 0 Then

                    Dim textdetails As String = txtdetails.Content

                    Dim sb As New Object
                    Dim pParms(23) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", HiddenTaskListID.Value)
                    pParms(1) = New SqlClient.SqlParameter("@TASK_ASSIGN_ID", Task_Assign_ID)
                    pParms(2) = New SqlClient.SqlParameter("@OPTION", 3)
                    sb = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "HD_MAIL_TEXT", pParms)

                    Dim status = HelpDesk.SendPlainTextEmails(fromemailid, emailid, HiddenTaskListID.Value & " - " & Title, sb.ToString(), username, password, host, port, HelpDesk.GetuploadIds(Task_Assign_ID, 2))
                    ''Update Email Notification Status
                    Sql_Query = "UPDATE TASK_ASSIGNED_LOG SET TASK_EMAIL_NOTIFICATION='" & status.ToString.Replace("'", "") & "' WHERE TASK_ASSIGN_ID='" & Task_Assign_ID & "'"

                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, Sql_Query)

                End If


                ''Sent SMS
                If CheckSMS.Checked Then
                    Dim message = "New Task Assigned ID : " & Task_Assign_ID & " , Category : " & category & _
                                  ", Reported By : " & reportedby & _
                                  ", Priority :" & ddpriority.SelectedItem.Text & _
                                  ".                               GEMS-Helpdesk"
                    Dim status = SmsService.sms.SendMessage(mobilenumber, message, "Helpdesk", Web.Configuration.WebConfigurationManager.AppSettings("smsUsername").ToString(), Web.Configuration.WebConfigurationManager.AppSettings("smspwd").ToString())
                    ''Update Sms Notification Status
                    Sql_Query = "UPDATE TASK_ASSIGNED_LOG SET TASK_SMS_NOTIFICATION='" & status.ToString.Replace("'", "") & "' WHERE TASK_ASSIGN_ID='" & Task_Assign_ID & "'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, Sql_Query)


                End If

            End If

            'End If

            lblmessage.Text = "Transactions done successfully"
        Catch ex As Exception
            lblmessage.Text = "Transaction  done successfully. But error in sending mails."
        End Try
    End Sub


End Class
