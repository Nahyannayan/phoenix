<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskReassign.ascx.vb" Inherits="HelpDesk_UserControls_hdTaskReassign" %>
<%@ Register Src="hdFileUploadViewAssign.ascx" TagName="hdFileUploadViewAssign" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
    
    <script type="text/javascript">
      function handleError() 
    {
      
	    return true;
    }
    window.onerror = handleError;
    
      function ViewTask(assignId)
        {
       
       // window.open('hdTaskAssignView.aspx?AssignedTaskid=' + assignId  , '','Height:700px,Width:800px,scrollbars=yes,resizable=no,directories=no'); return false;


          var url = "hdTaskAssignView.aspx?AssignedTaskid=" + assignId;
        var oWnd = radopen(url, "pop_DEDL");
        }
        
        function openimage(fname)
        {
       
            //  window.showModalDialog('hdImageView.aspx?filename=' + fname +'&Filecount='+ '1'  , '','dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

            var url = "hdImageView.aspx?filename=" + fname  +"&Filecount=1";
            var oWnd = radopen(url, "pop_DEDL");

        }
        
        function ReasignTask(assignid)
        {
          var path = window.location.href
          var Rpath=path.substring(path.indexOf('?'),path.length)
          Rpath= Rpath + '&task_AssignedTaskid='+ assignid
                  
          var returnval= window.open('../Pages/hdTaskReassignEnter.aspx'+ Rpath , '','Height=700px,Width=800px,scrollbars=yes,resizable=no,directories=no'); 

          window.close();
          return false;
        }
    
        function OnClientCloseDEDL(oWnd, args) {
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
   <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_DEDL" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseDEDL"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        
    </telerik:RadWindowManager>
         
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>  Task Reassign
        </div>
        <div class="card-body">
            <div class="table-responsive">
 <table  cellpadding="5" cellspacing="0" Width="100%" >
                      
                        <tr>
                            <td >

<asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label><asp:GridView
    ID="GridTaskStatus" runat="server" AutoGenerateColumns="false"  EmptyDataText="No members are been assigned for this task. Please assign members and proceed with reassign if necessary." AllowPaging="True" CssClass="table table-bordered table-row">
    <Columns>
        <asp:TemplateField HeaderText="Task Assign ID.">
         <HeaderTemplate>
                                
                                           Assign&nbsp;ID.
                                       
                            </HeaderTemplate>
            <ItemTemplate>
             <center>   <%#Eval("TASK_ASSIGN_ID") %></center>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Assigned By">
         <HeaderTemplate>
                             
                                           Assigned&nbsp;By
                                       
                            </HeaderTemplate>
            <ItemTemplate>
                <%#Eval("EMPASSIGNBY")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Assigned To">
         <HeaderTemplate>
                              
                                           Assigned&nbsp;To
                                       
                            </HeaderTemplate>
            <ItemTemplate>
             <%#Eval("EMPASSIGNTO")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Task Description">
         <HeaderTemplate>
                                
                                           Task&nbsp;Description
                                        
                            </HeaderTemplate>
            <ItemTemplate>
              <center><asp:LinkButton ID="LinkViewTask" OnClientClick='<%#Eval("VIEW_TASK")%>' runat="server">View</asp:LinkButton></center> 
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Start Date">
         <HeaderTemplate>
                              
                                           Start&nbsp;Date
                                       
                            </HeaderTemplate>
            <ItemTemplate>
             <center>  <%#Eval("TASK_ASSIGN_START_DATE") %></center> 
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Target Date">
         <HeaderTemplate>
                              
                                           Target&nbsp;Date
                                       
                            </HeaderTemplate>
            <ItemTemplate>
              <center>  <%#Eval("TASK_ASSIGN_END_DATE") %></center>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Priority">
         <HeaderTemplate>
                               
                                           Priority
                                       
                            </HeaderTemplate>
            <ItemTemplate>
               <center><%#Eval("PRIORITY_DESCRIPTION")%></center> 
            </ItemTemplate>
        </asp:TemplateField>
        <%-- <asp:TemplateField HeaderText="Screen">
          <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           Screen
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
          <ItemTemplate>
           <center>
             <asp:LinkButton ID="LinkShow" Visible='<%#Eval("Visible")%>' OnClientClick='<%#Eval("imagefile")%>' runat="server">Show</asp:LinkButton>
             </center>
          </ItemTemplate>
        </asp:TemplateField>--%>
        <asp:TemplateField HeaderText="Assign Date">
         <HeaderTemplate>
                               
                                           Assign&nbsp;Date
                                        
                            </HeaderTemplate>
            <ItemTemplate>
              <center>  <%#Eval("ENTRY_DATE")%></center>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Reassign">
         <HeaderTemplate>
                              
                                           Reassign
                                      
                            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <asp:LinkButton ID="LinkReassign" runat="server" Visible='<%#Eval("ACTIVE") %>'  OnClientClick='<%#Eval("REASSIGN_TASK") %>' >Reassign</asp:LinkButton>
                </center>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
                    <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <EditRowStyle Wrap="False" />
</asp:GridView>
                                <br />
                              <center>
                                <asp:Button ID="btnwcancel" runat="server" CssClass="button" OnClientClick="javascript:window.close();"
                                    Text="Close" Width="100px" />
                                    </center>
                                    
                                    </td>
                    </tr>
               </table>
        &nbsp;&nbsp;
<asp:HiddenField ID="HiddenSubTabID" runat="server" />
<asp:HiddenField ID="HiddenEmpID" runat="server" />
<asp:HiddenField ID="HiddenTaskCategory" runat="server" />
<asp:HiddenField ID="HiddenTaskListID" runat="server" />
<asp:HiddenField ID="HiddenBsuid" runat="server" /><asp:HiddenField ID="HiddenUserType" runat="server" />
    </div>