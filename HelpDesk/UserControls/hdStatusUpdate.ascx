<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdStatusUpdate.ascx.vb"
    Inherits="HelpDesk_UserControls_hdStatusUpdate" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    <%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
  
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register src="hdFileUpload.ascx" tagname="hdFileUpload" tagprefix="uc1" %>
<style type="text/css">
    .style1
    {
        color: #FF0000;
    }
</style>

<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Enter Status
        </div>
        <div class="card-body">
            <div class="table-responsive">
    
    <br />
    <br />
    <table  cellpadding="5" cellspacing="0" width="100%">
       
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <telerik:RadEditor ID="txtdetails" runat="server" EditModes="Design" ToolsFile="../xml/FullSetOfTools.xml"
                                        Width="600px">
                                    </telerik:RadEditor>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                Status
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddstatus" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id ="rowFinerIssues" runat ="server" visible="false" >
                                            <td>
                                                Select Finer Issue</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:DropDownList ID="ddFinerIssues" runat="server" AutoPostBack="True" 
                                                    Width="75%">
                                                </asp:DropDownList>
                                                &nbsp;<span class="style1">*</span></td>
                                        </tr>
                                        <tr id="TR2" runat="server">
                                            <td>
                                                Expected Completion Date
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td valign="middle">
                                                <asp:TextBox ID="txtdate" runat="server"></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="EstimateCompletionDate" runat="server"  Format="dd/MMM/yyyy"
                    PopupButtonID="imgAsOn" PopupPosition="TopLeft" TargetControlID="txtDate" Enabled ="true" >
                </ajaxToolkit:CalendarExtender>
                                                <asp:ImageButton ID="imgAsOn" runat="server" ImageUrl="~/Images/calendar.gif" 
                                                    TabIndex="4" />
                                                <asp:Label ID="lblAlert" runat="server" Text="Cannot be changed later on"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="TR1" runat="server">
                                            <td>
                                                Percentage
                                            </td>
                                            <td>
                                                &nbsp;:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddpercentage" runat="server">
                                                </asp:DropDownList>
                                                Completed
                                                <asp:CheckBox ID="chkUpload" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <uc1:hdFileUpload ID="hdFileUpload1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnok" runat="server" CssClass="button" Text="Ok" 
                                        Width="80px" />
                                    <asp:Button ID="btncancel" runat="server" CssClass="button" 
                                        OnClientClick="javascript:window.close(); return false;" Text="Close" 
                                        Width="80px" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblmessage" runat="server" ForeColor="#FF3300"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <asp:HiddenField ID="HiddenAssignedTaskid" runat="server" />
                        <asp:HiddenField ID="HiddenEmpID" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtdetails"
                            Display="None" ErrorMessage="Please Enter Status Description" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                            ShowSummary="False" />
                       
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</div>
            </div>
    </div>

