﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class HelpDesk_UserControls_hdTaskRootingMasterV3
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            GetFilter()
            BindEmployeeBsu()
            BindEmpName()
            BindBsuGrid()
            BindMainTabs()
        End If

    End Sub

    Public Sub GetFilter()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim Encr_decrData As New Encryption64
            Dim mednuid = "0"
            Try
                mednuid = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Catch ex As Exception

            End Try


            Dim Query = "SELECT * FROM ADD_ISSUE_CAT_FILTER WHERE MENU_ID='" & mednuid & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query)
            If ds.Tables(0).Rows.Count > 0 Then
                Hiddensubtabids.Value = ds.Tables(0).Rows(0).Item("SUB_TAB_IDS").ToString()
                Hiddentoplevelcatids.Value = ds.Tables(0).Rows(0).Item("TOP_LEVEL_CAT_IDS").ToString()
                Hiddentaskcatids.Value = ds.Tables(0).Rows(0).Item("TASK_CAT_IDS").ToString()
                HiddenType.Value = ds.Tables(0).Rows(0).Item("TYPE").ToString()

            End If


        Catch ex As Exception

        End Try

    End Sub

    Public Sub BindEmployeeBsu()
        Dim ds As New DataSet

        Dim query = " select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 " & _
                    " union select BSU_ID,BSU_NAME from dbo.BUSINESSUNIT_M WHERE BSU_CRM_NON_GEMS='True' order by BSU_NAME  "

        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, query)
        ddbsu.DataSource = ds
        ddbsu.DataTextField = "BSU_NAME"
        ddbsu.DataValueField = "BSU_ID"
        ddbsu.DataBind()
        ddbsu.SelectedValue = Session("sbsuid")
    End Sub


    Public Sub BindMainTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select * from  MAIN_TAB_MASTER WHERE ACTIVE='True'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddMaintab.DataSource = ds
        ddMaintab.DataTextField = "MAIN_TAB_DESCRIPTION"
        ddMaintab.DataValueField = "MAIN_TAB_ID"
        ddMaintab.DataBind()


        BindSubTabs()


    End Sub
    Public Sub BindSubTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select * from  SUB_TAB_MASTER where MAIN_TAB_ID='" & ddMaintab.SelectedValue & "' AND ACTIVE='True'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        Dim view As New DataView(ds.Tables(0))
        view.RowFilter = "SUB_TAB_ID not in (" & Hiddensubtabids.Value & ")"

        ddsubtab.DataSource = view
        ddsubtab.DataTextField = "SUB_TAB_DESCRIPTION"
        ddsubtab.DataValueField = "SUB_TAB_ID"
        ddsubtab.DataBind()

        BindJobCategory()
    End Sub

    Public Sub BindChildNodesLowlevelCategory(ByVal ParentNode As TreeNode)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = " select CONVERT(VARCHAR,LEVEL_ID) +'-'+ CONVERT(VARCHAR,A.TASK_CATEGORY_ID)TASK_CATEGORY_ID,CATEGORY_DES from dbo.TASK_MAIN_CATEGORY_LEVELS A  " & _
                        " INNER JOIN dbo.TASK_CATEGORY_MASTER B ON A.TASK_CATEGORY_ID=B.TASK_CATEGORY_ID  " & _
                        " INNER JOIN dbo.TASK_CATEGORY C ON B.CATEGORY_ID=C.ID WHERE A.MAIN_CATEGORY_ID='" & ParentNode.Value & "' "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        Dim view As New DataView(ds.Tables(0))
        view.RowFilter = "TASK_CATEGORY_ID not in (" & Hiddentaskcatids.Value & ")"
        ds.Tables.Clear()
        ds.Tables.Add(view.ToTable())

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = "<font color='red'><strong>" + ds.Tables(0).Rows(i).Item("CATEGORY_DES").ToString() + "</strong></font>"
                ChildNode.Value = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString()
                ''ChildNode.ShowCheckBox = False
                ParentNode.ChildNodes.Add(ChildNode)
                ''ChildNode.NavigateUrl = GetNavigateDepartmentEdit(ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString())
            Next
        End If

    End Sub

    Private Function GetNavigateDepartmentEdit(ByVal pId As String) As String

        Return String.Format("javascript:var popup = window.showModalDialog('YearlyDepartmentsEdit.aspx?dep_id={0}', '','dialogHeight:500px;dialogWidth:600px;scroll:auto;resizable:yes;'); window.location.reload();", pId)

    End Function


    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = "Select MAIN_CATEGORY_ID,MAIN_CATEGORY_DESC from  TASK_MAIN_CATEGORY where PRIMARY_MAIN_CATEGORY_ID='" & childnodeValue & "' AND SUB_TAB_ID='" & ddsubtab.SelectedValue & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        Dim view As New DataView(ds.Tables(0))
        view.RowFilter = "MAIN_CATEGORY_ID not in (" & Hiddentoplevelcatids.Value & ")"
        ds.Tables.Clear()
        ds.Tables.Add(view.ToTable())

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_DESC").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If

        For Each node As TreeNode In ParentNode.ChildNodes
            BindChildNodes(node)
        Next

        BindChildNodesLowlevelCategory(ParentNode)


    End Sub

    Public Sub BindJobCategory()
        TreeItemCategory.Nodes.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select MAIN_CATEGORY_ID,MAIN_CATEGORY_DESC from  TASK_MAIN_CATEGORY where PRIMARY_MAIN_CATEGORY_ID=0 AND SUB_TAB_ID='" & ddsubtab.SelectedValue & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        Dim view As New DataView(ds.Tables(0))
        view.RowFilter = "MAIN_CATEGORY_ID not in (" & Hiddentoplevelcatids.Value & ")"
        ds.Tables.Clear()
        ds.Tables.Add(view.ToTable())


        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_DESC").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_ID").ToString()
                TreeItemCategory.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In TreeItemCategory.Nodes
            BindChildNodes(node)
        Next

        'TreeItemCategory.ExpandAll()


    End Sub


    Protected Sub ddsubtab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsubtab.SelectedIndexChanged
        lblmessage.Text = ""
        BindJobCategory()
    End Sub

    Protected Sub ddMaintab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddMaintab.SelectedIndexChanged
        BindSubTabs()
    End Sub

    Protected Sub ddbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddbsu.SelectedIndexChanged
        BindEmpName()
    End Sub

    Public Sub BindEmpName()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Dim Sql_Query = " SELECT  A.EMP_ID,isnull(EMP_FNAME,'') + ' ' + isnull(EMP_MNAME ,'') + ' ' +isnull(EMP_LNAME ,'') + ' - ' + isnull(EMPNO ,'') as EMP_NAME, EMD_CUR_MOBILE,EMD_EMAIL,'javascript:openWindow(''' + convert(varchar,A.EMP_ID) + '''); return false;' OPENW,'javascript:openAssignCategory(''' + convert(varchar,A.EMP_ID) + '''); return false;' OPENWC FROM  EMPLOYEE_M A " & _
                      " INNER JOIN EMPLOYEE_D B ON  A.EMP_ID= B.EMD_EMP_ID " & _
                      " WHERE EMP_BSU_ID='" & ddbsu.SelectedValue & "' and A.EMP_bACTIVE='True' and A.EMP_STATUS <> 4 ORDER BY EMP_FNAME "
 
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddemp.DataSource = ds
        ddemp.DataTextField = "EMP_NAME"
        ddemp.DataValueField = "EMP_ID"
        ddemp.DataBind()


    End Sub

    Protected Sub ddemp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddemp.SelectedIndexChanged
        BindBsuGrid()
    End Sub

    Public Sub BindBsuGrid()
        Dim ds As New DataSet
        Dim qry = " select usr_name from dbo.USERS_M where USR_EMP_ID='" & ddemp.SelectedValue & "' AND USR_BDISABLE=0"
        Dim usr_name = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        Dim query = ""

        If Library.LibrarySuperAcess(Session("EmployeeId")) = "False" Then

            If usr_name <> "" Then
                query = " select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] ('" & usr_name & "') WHERE ISNULL(BSU_bSHOW,1)=1 " & _
                                 " union select BSU_ID,BSU_NAME from dbo.BUSINESSUNIT_M WHERE BSU_CRM_NON_GEMS='True' order by BSU_NAME  "

            Else
                query = " select BSU_ID,BSU_NAME from dbo.BUSINESSUNIT_M order by BSU_NAME "

            End If

        Else

            query = " select BSU_ID,BSU_NAME from dbo.BUSINESSUNIT_M order by BSU_NAME "

        End If


        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, query)
        GridRootingBsu.DataSource = ds
        GridRootingBsu.DataBind()

    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click


        Dim flag = 0

        If TreeItemCategory.CheckedNodes.Count > 0 Then

            Dim node As TreeNode

            For Each node In TreeItemCategory.CheckedNodes

                If node.Value.Contains("-") Then
                    Dim nval = node.Value.Split("-1")(1)

                    For Each row As GridViewRow In GridRootingBsu.Rows

                        If DirectCast(row.FindControl("Checkbsu"), CheckBox).Checked Then

                            Dim bsuid As String = DirectCast(row.FindControl("HiddenBsuid"), HiddenField).Value
                            Dim CheckOwner = CheckAccess.Items(0).Selected
                            Dim CheckMember = CheckAccess.Items(1).Selected
                            Dim CheckEmail = CheckAccess.Items(2).Selected
                            Dim CheckSMS = CheckAccess.Items(3).Selected
                            Dim reassign = CheckAccess.Items(4).Selected
                            SaveRooting(ddemp.SelectedValue, ddMaintab.SelectedValue, ddsubtab.SelectedValue, nval, bsuid, CheckOwner, CheckMember, CheckEmail, CheckSMS, reassign)
                            flag = 1
                        End If

                    Next
                End If




            Next

            If flag = 1 Then
                lblmessage.Text = "Successfully updated"
            Else
                lblmessage.Text = "Please select Business Unit"
            End If

        Else

            lblmessage.Text = "Please select the categories."

        End If

    End Sub


    Public Sub SaveRooting(ByVal emp_id As String, ByVal Main_Tab As String, ByVal Sub_Tab As String, ByVal Task_Category_ID As String, ByVal taskbsuid As String, ByVal CheckOwner As Boolean, ByVal CheckMember As Boolean, ByVal CheckEmail As Boolean, ByVal CheckSMS As Boolean, ByVal reassign As Boolean)


        Dim deleteflag = 0 '1-DELETE 0- NO DELETE

        If CheckOwner Or CheckMember Or CheckEmail Or CheckSMS Or reassign Then
            deleteflag = 0 ''No Delete
        Else
            deleteflag = 1 ''Delete
        End If

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(12) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@MAIN_TAB_ID", Main_Tab)
        pParms(1) = New SqlClient.SqlParameter("@SUB_TAB_ID", Sub_Tab)
        pParms(2) = New SqlClient.SqlParameter("@TASK_CATEGORY_ID", Task_Category_ID)
        pParms(3) = New SqlClient.SqlParameter("@TASK_CATEGORY_OWNER", CheckOwner)
        pParms(4) = New SqlClient.SqlParameter("@EMP_ID", emp_id)
        pParms(5) = New SqlClient.SqlParameter("@TASK_MEMBER", CheckMember)
        pParms(6) = New SqlClient.SqlParameter("@EMAIL_NOTIFY", CheckEmail)
        pParms(7) = New SqlClient.SqlParameter("@SMS_NOTIFY", CheckSMS)
        pParms(8) = New SqlClient.SqlParameter("@DELETE_FLAG", deleteflag)
        pParms(9) = New SqlClient.SqlParameter("@EMP_TASK_BSU_ID", taskbsuid)
        pParms(10) = New SqlClient.SqlParameter("@CAN_RESIGN", reassign)
        pParms(11) = New SqlClient.SqlParameter("@LOG_USR", Session("sUsr_name"))
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_UPDATE_DELETE_TASK_ROOTING_MASTER", pParms)




    End Sub

End Class
