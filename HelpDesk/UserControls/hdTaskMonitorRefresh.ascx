﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskMonitorRefresh.ascx.vb"
    Inherits="HelpDesk_UserControls_hdTaskMonitor" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">

    function callblur() {
        document.getElementById('<% = txtreqdate.ClientID %>').blur();
    }

    function Assign(task_list_id, task_category_id) {

        window.open('Pages/hdTaskAssignEmp.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id, '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

    }
    function ReAssign(task_list_id, task_category_id) {

        window.showModalDialog('Pages/hdTaskReassign.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id + '&Usertype=OWNER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

    }

    function History(task_list_id, task_category_id) {

        window.showModalDialog('Pages/hdTaskHistory.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id + '&Usertype=OWNER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

    }


    function openimage(fname) {
        window.showModalDialog('hdImageView.aspx?filename=' + fname + '&Filecount=' + '1', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

    }

    function ViewTaskDes(TaskListId) {

        window.open('Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

    }

    function ChartComp(TaskListid) {

        window.showModalDialog('FusionCharts/hdTaskProgressCompare.aspx?TaskListid=' + TaskListid, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

    }

    function Followup(task_list_id, to_emp_id) {

        window.showModalDialog('Pages/hdFollowupClientOwner.aspx?Task_list_id=' + task_list_id + '&to_emp_id=' + to_emp_id + '&Usertype=OWNER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

    }

    function ViewTraceFlow(task_list_id) {
        window.showModalDialog('Pages/hdTraceFlow.aspx?task_list_id=' + task_list_id, '', 'dialogHeight:600px;dialogWidth:800px;scroll:yes;resizable:yes;'); return false;

    }

    function ViewAssigned(main_tab, sub_tab, bsu_id, cat_id) {
        window.open('Pages/hdViewOwnerMembers.aspx?main_tab=' + main_tab + "&sub_tab=" + sub_tab + "&cat_id=" + cat_id + "&bsu_id=" + bsu_id, '', 'Height=650px,Width=750px,scrollbars=yes,resizable=no,directories=yes');
        return false;
    }

    window.setTimeout('loadpage();', 20000);

    function loadpage() {
        if (document.getElementById("<%=cp.ClientID %>").checked == false) {

            var path = window.location.href
            if (path.indexOf('?') != '-1') {
                var Rpath = ''
                var Fpath = ''
                var hpath = ''
                Rpath = path.substring(path.indexOf('?'), path.length)
                Rpath = Rpath.replace('tmsection=0', 'tmsection=1')
                Fpath = path.substring(0, path.indexOf('?'))
                var cpindex = document.getElementById("<%=cpindex.ClientID %>").value;
                var tpage = document.getElementById("<%=tpage.ClientID %>").value;
                Rpath = Rpath.substring(0, Rpath.indexOf('tmsection=1')) + 'tmsection=1';
                //alert(Rpath)
                hpath = Fpath + Rpath + '&cpindex=' + cpindex + '&tpage=' + tpage;
                //alert(hpath);
                window.location.href = hpath;

            }

            //window.location = window.location
        }
        else {
            window.setTimeout('loadpage();', 20000);
        }
    }
</script>

<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 

<%--<div class="matters">
    <asp:Panel ID="P1" Visible="false" runat="server">
        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
        <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="900px">
            <tr>
                <td class="subheader_img">
                    Search
                </td>
            </tr>--%>

<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-phone-square mr-3"></i>
            Task Summary Report
        </div>

 <div class="card-body">
            <div class="table-responsive m-auto">
    
                

                    <table width="100%" align="center">

                        <tr >
                            <td colspan="4">
                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                    </tr>
                        <tr >
                            <td width="20%" align="left">
                                <span class="field-label">
                                Task ID </span>
                            </td>
                            <td width="30%">
                               <asp:TextBox ID="txttasklistid" runat="server" ></asp:TextBox>
                            </td>
                            <td width="20%">
                                
                            </td>
                            <td width="30%">

                            </td>
                            </tr>

                        <tr>

                             <td width="20%" align="left">
                                <span class="field-label">
                                Required Date </span>
                            </td>
                            <td width="30%">
                               <asp:TextBox ID="txtreqdate" runat="server" Width="100px"></asp:TextBox>
                                &nbsp;<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                    OnClientClick="return false;" />
                            </td>
                            <td width="20%"><span class="field-label">

                                Entry Date </span>
                                
                            </td>
                            <td width="30%">
                                <asp:TextBox ID="txtentrydate" runat="server" Width="100px"></asp:TextBox>
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif"
                                    OnClientClick="return false;" />
                            </td>
                                                       
                        </tr>
                        
                        <tr align="left">


                             <td width="20%" align="left">
                                <span class="field-label">
                                Priority </span>
                            </td>
                            <td width="30%">
                               <asp:DropDownList ID="ddpriority" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td width="20%"><span class="field-label">

                                Status </span>
                                
                            </td>
                            <td width="30%">
                                <asp:DropDownList ID="ddstatus" runat="server" AutoPostBack="True" />
                            </td>
                            </tr>

                        <tr>
                             <td width="20%" align="left">
                                <span class="field-label">
                                Task Category </span>
                            </td>
                            <td width="30%">
                               <asp:DropDownList ID="ddTaskCategory" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td width="20%"><span class="field-label">

                                Reported By </span>
                                
                            </td>
                            <td width="30%">
                                <asp:DropDownList ID="ddreportedby" runat="server">   </asp:DropDownList>
                            </td>
                            </tr>

                        <tr >
                            <td width="20%" align="left">
                                <span class="field-label">
                                Claimed By </span>
                            </td>
                            <td width="30%">
                               <asp:DropDownList ID="ddclaimedby" runat="server"></asp:DropDownList>
                            </td>
                            <td width="20%"><span class="field-label">

                                Assigned By </span>
                                
                            </td>
                            <td width="30%">
                                <asp:DropDownList ID="ddassignedby" runat="server">
                                </asp:DropDownList>
                            </td>
                                
                            
                        </tr>
                        <tr >
                            
                             <td width="20%" align="left">
                                <span class="field-label">
                                Assigned To </span>
                            </td>
                            <td width="30%">
                               <asp:DropDownList ID="ddassignedto" runat="server"></asp:DropDownList>
                            </td>
                            <td width="20%"><span class="field-label">

                                Keyword </span>
                                
                            </td>
                            <td width="30%">
                                <asp:TextBox ID="txtkeyword" runat="server"></asp:TextBox>
                            </td>
                                
                            
                            
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="btnsearch" runat="server" Text="Search"  />
                            </td>
                        </tr>
                
                
        
        <tr>
            <td class="table-bg" colspan="4">
                Help Desk - Task Monitoring
                <asp:CheckBox ID="cp" Text="Pause" runat="server" />
                &nbsp;&nbsp;
                <asp:ImageButton ID="ImageClose" ToolTip="Close" OnClientClick='window.close();'
                    ImageUrl="../../Images/close_red1.png" runat="server" Height="16px" Width="16px" />
        </tr>
        <tr>
            <td colspan="4" width="100%">
                <asp:GridView ID="GridTaskList" runat="server" AllowPaging="True" PageSize="12" AutoGenerateColumns="False"
                    EmptyDataText="Search query did not retrieve any results. Please try with some other keyword(s)"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Task ID">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Task&nbsp;ID
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkTaskList" runat="server" OnClientClick='<%#Eval("VIEW_TRACE_FLOW")%>'
                                        Text='<%#Eval("TASK_LIST_ID")%>'></asp:LinkButton>
                                    <asp:HiddenField ID="HiddenTaskId" runat="server" Value='<%#Eval("TASK_ID")%>' />
                                    <asp:HiddenField ID="HiddenTaskListId" runat="server" Value='<%#Eval("TASK_LIST_ID")%>' />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BSU">
                            <HeaderTemplate>
                                
                                            BSU
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%# Eval("BSU_SHORTNAME")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Priority">
                            <HeaderTemplate>
                                            Priority
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("PRIORITY_DESCRIPTION")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Category">
                            <HeaderTemplate>
                                           Category
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("ROOT_TAB")%>
                                <br />
                                <%# Eval("MAIN_CATEGORY_DESC")%>
                                <br />
                                <%#Eval("TASK_CATEGORY_DESCRIPTION")%>
                            </ItemTemplate>
                                <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <HeaderTemplate>
                                            Title-Description
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkDes" runat="server" OnClientClick='<%#Eval("VIEW_TASK")%>'
                                    Text='<%#Eval("TASK_TITLE")%>' Width="150px"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reported By">
                            <HeaderTemplate>
                                            Reported by
                                            
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("EMPNAME")%>
                                <%#Eval("STUNAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Claimed By">
                            <HeaderTemplate>
                                            Claimed by
                                            
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("CLAIM_EMPNAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="">
                            <HeaderTemplate>
                                
                                            Assigned to
                                </HeaderTemplate>
                                            
                            <ItemTemplate>
                                <center>
                                    <%# Eval("ASSIGNED_BSU")%>
                                    <br />
                                    <asp:ImageButton ID="ImageReAssign" OnClientClick='<%#Eval("VIEW_ASSIGNED")%>' ToolTip="View Owners and Members."
                                        Width="20px" Height="20px" ImageUrl="~/Images/Helpdesk/Reassign.png" runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entry Date">
                            <HeaderTemplate>
                                            Entry dt
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_DATE_TIME", "{0:dd/MMM/yyyy}")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entry Date">
                            <HeaderTemplate>
                                            Owner assign dt
                                            
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%# Eval("OWNER_START_DATE", "{0:dd/MMM/yyyy}")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entry Date">
                            <HeaderTemplate>
                                            Owner expt dt
                                </HeaderTemplate>
                                            
                                        
                            <ItemTemplate>
                                <center>
                                    <%# Eval("OWNER_END_DATE", "{0:dd/MMM/yyyy}")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entry Date">
                            <HeaderTemplate>
                                            Member exp comp& dt
                                            
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%# Eval("MEMBER_EXP_DATE", "{0:dd/MMM/yyyy}")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entry Date">
                            <HeaderTemplate>
                                Member comp dt
                                            
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%# Eval("MEMBER_COMPLETED_DATE", "{0:dd/MMM/yyyy}")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entry Date">
                            <HeaderTemplate>
                                 Owner comp dt
                                            
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("OWNER_COMPLETED_DATE", "{0:dd/MMM/yyyy}")%><asp:Image ID="Imagealert" ImageUrl="~/Images/alertRed.gif" Visible='<%# Eval("VISIBLE_ALERT")%>' runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                    <%--<HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                    --%><EmptyDataRowStyle Wrap="False" />
                    <EditRowStyle Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HiddenEmpId" runat="server" />
    <asp:HiddenField ID="cpindex" runat="server" />
    <asp:HiddenField ID="tpage" runat="server" />
</div>
