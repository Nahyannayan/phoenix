<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdStatusDescriptionView.ascx.vb" Inherits="HelpDesk_UserControls_hdStatusDescriptionView" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<div class="matters" align="center">


<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<br />
<br />

     <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="240">
            <tr>
                <td class="subheader_img">
                    Task Status Description</td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td colspan="3">
                                        <telerik:RadEditor ID="txtdetails" runat="server" EditModes="Design" ToolsFile="../xml/NoTools.xml"
                                            Width="600px">
                                        </telerik:RadEditor>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="3">
                                        <asp:Button ID="btncancel" runat="server" CssClass="button" 
                                            OnClientClick="javascript:window.close();" Text="Cancel" 
                                            Width="100px" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="Hiddenstatuslogid" runat="server" /><asp:HiddenField ID="HiddenEmpID" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
</div>