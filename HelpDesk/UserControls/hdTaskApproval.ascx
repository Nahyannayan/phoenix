﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskApproval.ascx.vb" Inherits="HelpDesk_Version2_Usercontrols_hdTaskApproval" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="hdFileUpload.ascx" TagName="hdFileUpload" TagPrefix="uc1" %>

<style type="text/css">



.gridheader_new 
{
	border-style: none;
        border-color: inherit;
        border-width: 0;
        font-family: Verdana, Arial, Helvetica, sans-serif; 
	background-image:url('../../Images/GRIDHEAD.gif') ;
	    background-repeat: repeat-x;font-size: 11px;
	    font-weight:bold;color:#1b80b6;
}
</style>

<script language="javascript" type="text/javascript">

    function CheckAllRows() {
        var frm = document.forms['hdTaskApproval'];

        for (var i = 0; i < document.forms[0].length; i++) {
            if (document.forms[0].elements[i].id.indexOf('chkAction') != -1) {

                //                    document.forms[0].elements[i].checked = true;

                if (document.forms[0].elements[i].checked == true) {
                    document.forms[0].elements[i].checked = false;
                }
                else if (document.forms[0].elements[i].checked == false) {
                    document.forms[0].elements[i].checked = true;
                }

            }
        }

        for (var i = 0; i < document.forms[0].length; i++) {
            if (document.forms[0].elements[i].id.indexOf('chkAction_Header') != -1) {

                //                    document.forms[0].elements[i].checked = true;

                if (document.forms[0].elements[i].checked == true) {
                    document.forms[0].elements[i].checked = false;
                }
                else if (document.forms[0].elements[i].checked == false) {
                    document.forms[0].elements[i].checked = true;
                }

            }
        }

        return false;
    } 

       

    </script>

<div class="matters">
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" 
        width="99%">
        <tr>
            <td class="subheader_img">
                Task Approval</td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:RadioButton ID="optAwaitingApproval" runat="server" 
                                Text="Awaiting Approval" AutoPostBack="True" GroupName="1" />
                            <asp:RadioButton ID="optApproved" runat="server" Text="Approved" 
                                AutoPostBack="True" GroupName="1" />
                            <asp:RadioButton ID="optOnHold" runat="server" Text="On Hold" 
                                AutoPostBack="True" GroupName="1" />
                            <asp:RadioButton ID="optRejected" runat="server" Text="Rejected" 
                                AutoPostBack="True" GroupName="1" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            Business&nbsp;Unit
                                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="rowReportedBy" runat ="server" visible ="false">
                        <td>
                            Reported&nbsp;By/
                            <br />
                            On&nbsp;behalf&nbsp;of
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddreportedby" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Category
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddsubtab" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            &nbsp;
                            <asp:DropDownList ID="ddtoplevelcat" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            &nbsp;
                            <asp:DropDownList ID="ddJobCategory" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddmaintab" runat="server" Visible="false" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id ="rowSubject" runat ="server" visible ="false">
                        <td>
                            Subject
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:TextBox ID="txtTitle" runat="server" Width="446px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="rowPriority" runat ="server" visible ="false">
                        <td>
                            Priority
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddpriority" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id ="rowRequiredDate" runat="server" visible="false">
                        <td>
                            Required&nbsp;Date
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:TextBox ID="txtreqdate" runat="server"></asp:TextBox>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />
                        </td>
                    </tr>
                    <tr id ="rowTaskDescription" runat ="server" visible ="false">
                        <td colspan="3">
                            Task Description
                        </td>
                    </tr>
                    <tr id ="rowDetailedDescription" runat ="server" visible="false" >
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <telerik:radeditor id="txtjobdes" runat="server" editmodes="Design" toolsfile="~/HelpDesk/xml/FullSetOfTools.xml"
                                width="600px"></telerik:radeditor>
                        </td>
                    </tr>
                    <tr id ="rowAttachments" runat ="server" visible ="false">
                        <td>
                            <br />
                            Attachments
                        </td>
                        <td>
                            <br />
                            :
                        </td>
                        <td>
                            <br />
                            <uc1:hdFileUpload ID="hdFileUpload1" runat="server" />
                        </td>
                    </tr>
                    <tr >
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="3">
                <asp:GridView ID="gvTaskList" runat="server" AutoGenerateColumns="False" 
                    Width="100%" PageSize="5" 
                    EnableModelValidation="True" SkinID="GridViewView" >
                    <Columns>
                        <asp:TemplateField Visible="False">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAction_Header" runat="server" OnClick="CheckAllRows();" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkAction" runat="server" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="25px" HorizontalAlign="Center" />
                        </asp:TemplateField>
<asp:TemplateField HeaderText="Task Ref ID">
    <ItemTemplate>
        <asp:Label ID="lblTaskListMasterId" runat="server" 
            Text='<%# Bind("TASK_LIST_MASTER_ID") %>'></asp:Label>
    </ItemTemplate>
</asp:TemplateField>
                        <asp:TemplateField HeaderText="Task Category">
                            <ItemTemplate>
                                <asp:Label ID="lblTaskCategory" runat="server" 
                                    Text='<%# Bind("TASK_CATEGORY") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Subject">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkDes" runat="server" 
                                    OnClientClick='<%# Eval("VIEW_TASK") %>' Text='<%# Eval("TITLE") %>'></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="15%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Priority Hidden" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblPriority" runat="server" Text='<%# Bind("PRIORITY_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Priority">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddPriority" runat="server" Width="100%">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Initiated By">
                            <ItemTemplate>
                                <asp:Label ID="lblInitiatedBy" runat="server" 
                                    Text='<%# Bind("INITIATED_BY") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Initiated Date/Time">
                            <ItemTemplate>
                                <asp:Label ID="lblInitiatedDate" runat="server" 
                                    Text='<%# Bind("INITIATED_DATE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks">
                            <ItemTemplate>
                                <asp:TextBox ID="txtRemarks" runat="server" Rows="1" 
                                    TextMode="MultiLine" Width="97%" Height="20px" SkinID="MultiText" 
                                    Text='<%# Bind("APPROVAL_STATUS_REMARKS") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Approve">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkApprove" runat="server" OnClick="ApproveTask" 
                                    Text="Approve"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="On Hold">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkOnHold" runat="server" OnClick="HoldTask"
                                    Text="On Hold"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reject">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkReject" runat="server" OnClick="RejectTask" 
                                    Text="Reject"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
                        </td>
                    </tr>
                    <tr id ="rowButtons" runat ="server" visible="false" >
                        <td align="center" colspan="3">
                            <br />
                            <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve"
                                Width="100px" Visible="False" />
                            &nbsp;<asp:Button ID="btnOnHold" runat="server" CssClass="button" Text="On Hold"
                                Width="100px" Visible="False" />
                            &nbsp;<asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject"
                                Width="100px" Visible="False" />
                            <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" ValidationGroup="Save"
                                Width="100px" Visible="False" />
                            <br />
                        </td>
                    </tr>
                    <tr >
                        <td align="center" colspan="3">
                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
        TargetControlID="txtreqdate">
    </ajaxToolkit:CalendarExtender>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddreportedby"
        Display="None" ErrorMessage="Please Specify Reported Person" InitialValue="-1"
        SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTitle"
        Display="None" ErrorMessage="Please Enter Title" SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddJobCategory"
        Display="None" ErrorMessage="Please Select Task Category" InitialValue="0" SetFocusOnError="True"
        ValidationGroup="Save"></asp:RequiredFieldValidator>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtreqdate"
        Display="None" ErrorMessage="Please Enter Required Date" SetFocusOnError="True"
        ValidationGroup="Save"></asp:RequiredFieldValidator>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtjobdes"
        Display="None" ErrorMessage="Please Enter Job Description" SetFocusOnError="True"
        ValidationGroup="Save"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="Save" />
    <asp:HiddenField ID="HiddenBsuid" runat="server" />
    <asp:HiddenField ID="HiddenUserId" runat="server" />
    <asp:HiddenField ID="Hiddensubtabids" runat="server" />
    <asp:HiddenField ID="Hiddentoplevelcatids" runat="server" />
    <asp:HiddenField ID="Hiddentaskcatids" runat="server" />
    <asp:HiddenField ID="HiddenType" runat="server" />
</div>
