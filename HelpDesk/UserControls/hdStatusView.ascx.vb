Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI
Partial Class HelpDesk_UserControls_hdStatusView
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenAssignedTaskid.Value = Request.QueryString("AssignedTaskid")
            HiddenEmpID.Value = Session("EmployeeId")
            BindGrid()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = " select TASK_ASSIGN_ID,A.TASK_STATUS_DESCRIPTION,B.TASK_STATUS_DESCRIPTION AS STATUS_DESCRIPTION,PERCENTAGE, REPLACE(CONVERT(VARCHAR(11), ENTRY_DATE, 106), ' ', '/')ENTRY_DATE ,'javascript:StatusChart('''+ CONVERT(VARCHAR,TASK_ASSIGN_ID) +''','''+ CONVERT(VARCHAR,TASK_EMP_ID) +''');return false;' SHOWCHART ,REPLACE(CONVERT(VARCHAR(11), EXP_END_DATE, 106), ' ', '/') EXP_END_DATE, " & _
                        " 'javascript:ViewStatus(''' + CONVERT(VARCHAR, TASK_STATUS_LOG_ID ) + '''); return false;' VIEW_OPEN,B.IMAGE_PATH  " & _
                        " from dbo.TASK_STATUS_LOG A " & _
                        " INNER JOIN dbo.TASK_STATUS_MASTER B ON A.TASK_STATUS_ID= B.STATUS_ID " & _
                        " where TASK_ASSIGN_ID='" & HiddenAssignedTaskid.Value & "' ORDER BY TASK_STATUS_LOG_ID DESC"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            btnchart.Attributes.Add("OnClick", ds.Tables(0).Rows(0).Item("SHOWCHART").ToString())
            btnchart.Visible = True
        Else
            btnchart.Visible = False
        End If
        GridStatus.DataSource = ds
        GridStatus.DataBind()



    End Sub

    Protected Sub GridStatus_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridStatus.PageIndexChanging
        GridStatus.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub


End Class
