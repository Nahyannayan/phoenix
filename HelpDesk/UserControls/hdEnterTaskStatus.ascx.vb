Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI

Partial Class HelpDesk_UserControls_hdEnterTaskStatus
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            txtassignid.Text = Session("Task_ID")
            HiddenBsuid.Value = Session("sbsuid") '' "999998"
            HiddenEmpID.Value = Session("EmployeeId") ''"12581"
            BindSearchControls()
            BindStatus()
            Session("Task_ID") = ""

            If Session("sid") IsNot Nothing Then
                ddstatus.SelectedValue = Session("sid").ToString()

            End If
            BindNewAssignedTask()
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnexport)

    End Sub

    Public Sub BindStatus()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT * FROM TASK_STATUS_MASTER where STATUS_ID NOT IN (2,6,7,8)"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddstatus.DataSource = ds
        ddstatus.DataTextField = "TASK_STATUS_DESCRIPTION"
        ddstatus.DataValueField = "STATUS_ID"
        ddstatus.DataBind()

        Dim list As New ListItem
        list.Text = "Select Status"
        list.Value = "-1"
        ddstatus.Items.Insert(0, list)

    End Sub
    Public Sub BindSearchControls()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim sql_query = ""
        Dim ds As DataSet

        ''Priority
        HelpDesk.BindPriority(ddpriority)
        Dim list As New ListItem
        list.Text = "Select Priority"
        list.Value = "-1"
        ddpriority.Items.Insert(0, list)
        ''Task Category
        sql_query = "SELECT DISTINCT TASK_CATEGORY_ID , TASK_CATEGORY_DESCRIPTION FROM VIEW_SEARCH_STATUS_UPDATE_ENTRY WHERE TASK_ASSIGNED_TO_EMP_ID='" & HiddenEmpID.Value & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

        ddcategory.DataSource = ds
        ddcategory.DataTextField = "TASK_CATEGORY_DESCRIPTION"
        ddcategory.DataValueField = "TASK_CATEGORY_ID"
        ddcategory.DataBind()

        Dim list1 As New ListItem
        list1.Text = "Select Task Category"
        list1.Value = "-1"
        ddcategory.Items.Insert(0, list1)

        '' Assigned By

        sql_query = "SELECT DISTINCT TASK_ASSIGNED_BY_EMP_ID , EMPNAME FROM VIEW_SEARCH_STATUS_UPDATE_ENTRY WHERE TASK_ASSIGNED_TO_EMP_ID='" & HiddenEmpID.Value & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        ddassignedby.DataSource = ds
        ddassignedby.DataTextField = "EMPNAME"
        ddassignedby.DataValueField = "TASK_ASSIGNED_BY_EMP_ID"
        ddassignedby.DataBind()
        Dim list2 As New ListItem
        list2.Text = "Assigned By"
        list2.Value = "-1"
        ddassignedby.Items.Insert(0, list2)

        ''Bsu
        sql_query = "SELECT DISTINCT CALLER_BSU_ID , BSU_SHORTNAME FROM VIEW_SEARCH_STATUS_UPDATE_ENTRY WHERE TASK_ASSIGNED_TO_EMP_ID='" & HiddenEmpID.Value & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        ddbsu.DataSource = ds
        ddbsu.DataTextField = "BSU_SHORTNAME"
        ddbsu.DataValueField = "CALLER_BSU_ID"
        ddbsu.DataBind()
        Dim list3 As New ListItem
        list3.Text = "Select BSU"
        list3.Value = "-1"
        ddbsu.Items.Insert(0, list3)



    End Sub

    Public Sub BindNewAssignedTask(Optional ByVal export As Boolean = False)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim pParms(12) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EMP_ID", HiddenEmpID.Value)

        If txtassignid.Text.Trim() <> "" Then
            pParms(1) = New SqlClient.SqlParameter("@TASK_LIST_ID", txtassignid.Text.Trim())
        End If
        If txtstartdate.Text.Trim() <> "" Then
            pParms(2) = New SqlClient.SqlParameter("@TASK_ASSIGN_START_DATE", txtstartdate.Text.Trim())
        End If
        If txttargetdate.Text.Trim() <> "" Then
            pParms(3) = New SqlClient.SqlParameter("@TASK_ASSIGN_END_DATE", txttargetdate.Text.Trim())
        End If
        If txtassigndate.Text.Trim() <> "" Then
            pParms(4) = New SqlClient.SqlParameter("@ENTRY_DATE", txtassigndate.Text.Trim())
        End If
        If txtkeyword.Text.Trim() <> "" Then
            pParms(5) = New SqlClient.SqlParameter("@KEYWORD", txtkeyword.Text.Trim())
        End If

        If ddcategory.SelectedIndex > 0 Then
            pParms(6) = New SqlClient.SqlParameter("@TASK_CATEGORY_ID", ddcategory.SelectedValue)
        End If

        If ddassignedby.SelectedIndex > 0 Then
            pParms(7) = New SqlClient.SqlParameter("@TASK_ASSIGNED_BY_EMP_ID", ddassignedby.SelectedValue)
        End If

        If ddpriority.SelectedIndex > 0 Then
            pParms(8) = New SqlClient.SqlParameter("@TASK_ASSIGN_PRIORITY", ddpriority.SelectedValue)
        End If

        If ddbsu.SelectedIndex > 0 Then
            pParms(9) = New SqlClient.SqlParameter("@CALLER_BSU_ID", ddbsu.SelectedValue)
        End If

        If ddstatus.SelectedIndex > 0 Then
            pParms(10) = New SqlClient.SqlParameter("@STATUS", ddstatus.SelectedValue)
        End If


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SEARCH_STATUS_UPDATE_ENTRY", pParms)


        GridTaskStatus.DataSource = ds
        GridTaskStatus.DataBind()

        For Each row As GridViewRow In GridTaskStatus.Rows
            Dim task_assign_id As String = DirectCast(row.FindControl("HiddenTaskAssignid"), HiddenField).Value
            Dim Sql_Query = "select TASK_STATUS  from dbo.TASK_LIST_MASTER where " & _
                            " TASK_LIST_ID=(select TASK_LIST_ID from dbo.TASK_ASSIGNED_LOG where TASK_ASSIGN_ID ='" & task_assign_id & "')"
            Dim status = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)

            If status = "6" Or status = "8" Then
                If status = "6" Then
                    DirectCast(row.FindControl("Image1"), Image).Visible = True
                End If
                If status = "8" Then
                    DirectCast(row.FindControl("Image2"), Image).Visible = True
                End If
                DirectCast(row.FindControl("lblmessage"), Label).Visible = False

                'DirectCast(row.FindControl("LinkUpdate"), LinkButton).Visible = False
                'DirectCast(row.FindControl("LinkReAssign"), LinkButton).Visible = False
                DirectCast(row.FindControl("ImageUpdate"), ImageButton).Visible = False
                DirectCast(row.FindControl("ImageReAssign"), ImageButton).Visible = False
                'Else
                '    DirectCast(row.FindControl("ImageUpdate"), ImageButton).Visible = False
                '    DirectCast(row.FindControl("ImageReAssign"), ImageButton).Visible = False
            End If
        Next

        If export Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dt As New DataTable
                dt = ds.Tables(0)
                dt.Columns.Remove("VisibleReassign")
                dt.Columns.Remove("hide")
                dt.Columns.Remove("tempview")
                dt.Columns.Remove("TASK_ASSIGN_DESCRIPTION")
                dt.Columns.Remove("TASK_ASSIGN_PRIORITY")
                dt.Columns.Remove("TASK_CATEGORY_ID")
                dt.Columns.Remove("CALLER_BSU_ID")
                dt.Columns.Remove("ENTER_STATUS")
                dt.Columns.Remove("VIEW_TASK")
                dt.Columns.Remove("VIEW_STATUS")
                dt.Columns.Remove("SHOWCHART")
                dt.Columns.Remove("REASSIGN")
                dt.Columns.Remove("HISTORY")
                dt.Columns.Remove("CURRENT_STATUS_ID")
                dt.Columns.Remove("FOLLOWUP")
                dt.Columns.Remove("TASK_ASSIGN_ID")
                dt.Columns.Remove("TASK_ASSIGNED_BY_EMP_ID")
                dt.Columns.Remove("TASK_ASSIGNED_TO_EMP_ID")
                dt.Columns.Remove("updateVisible")
                ExportExcel(dt)

            End If
        End If

    End Sub

    Public Sub ExportExcel(ByVal dt As DataTable)

        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, "A1", True)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")
        ef.SaveXls(Response.OutputStream)

    End Sub
    Protected Sub GridTaskStatus_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridTaskStatus.PageIndexChanging
        GridTaskStatus.PageIndex = e.NewPageIndex
        BindNewAssignedTask()
    End Sub


    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Session("sid") = ddstatus.SelectedValue
        BindNewAssignedTask()

    End Sub


    Protected Sub ddcategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcategory.SelectedIndexChanged
        BindNewAssignedTask()
    End Sub

    Protected Sub ddassignedby_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddassignedby.SelectedIndexChanged
        BindNewAssignedTask()
    End Sub

    Protected Sub ddpriority_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddpriority.SelectedIndexChanged
        BindNewAssignedTask()
    End Sub

    Protected Sub ddbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddbsu.SelectedIndexChanged
        BindNewAssignedTask()
    End Sub

    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        BindNewAssignedTask(True)
    End Sub
End Class
