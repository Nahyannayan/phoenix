Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI

Partial Class HelpDesk_UserControls_hdStatusDescriptionView
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Hiddenstatuslogid.Value = Request.QueryString("statuslogid")
            HiddenEmpID.Value = Session("EmployeeId")
            BindDetails()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Public Sub BindDetails()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT TASK_STATUS_DESCRIPTION  FROM TASK_STATUS_LOG where TASK_STATUS_LOG_ID='" & Hiddenstatuslogid.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            txtdetails.Content = ds.Tables(0).Rows(0).Item("TASK_STATUS_DESCRIPTION").ToString()
        End If


    End Sub


End Class
