﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI

Partial Class HelpDesk_UserControls_hdTaskCount
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        BindGrid()
    End Sub


    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@CLAIM_EMP_ID", Session("EmployeeId"))

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TASK_COUNT_FOR_OWNER", pParms)

        GridCount.DataSource = ds
        GridCount.DataBind()


    End Sub

End Class
