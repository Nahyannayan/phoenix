Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class HelpDesk_UserControls_hdTaskAssigning
    Inherits System.Web.UI.UserControl

    Public Sub BindEscalationLevels()
        Dim list As ListItem

        list = New ListItem
        list.Text = "Select Level"
        list.Value = "Select Level"
        ddEscalationLevel.Items.Insert(0, list)
        list = New ListItem
        list.Text = "Level 0"
        list.Value = "Level 0"
        ddEscalationLevel.Items.Insert(1, list)
        list = New ListItem
        list.Text = "Level 1"
        list.Value = "Level 1"
        ddEscalationLevel.Items.Insert(2, list)
        list = New ListItem
        list.Text = "Level 2"
        list.Value = "Level 2"
        ddEscalationLevel.Items.Insert(3, list)
        list = New ListItem
        list.Text = "Level 3"
        list.Value = "Level 3"
        ddEscalationLevel.Items.Insert(4, list)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            txttasklistid.Text = Session("Task_ID")
            HiddenEmpId.Value = Session("EmployeeId") ''"12581"
            BindAssignedTo()
            BindJobCategopry()
            BindPriority()
            BindStatus()
            BindEscalationLevels()
            SearchTaskList()
            Session("Task_ID") = ""
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnexport)

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Public Sub BindAssignedTo()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT * FROM ( SELECT DISTINCT EMP_ID, ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'') EMP_NAME FROM dbo.TASK_ASSIGNED_LOG AA " & _
                        " INNER JOIN OASIS.dbo.EMPLOYEE_M BB ON BB.EMP_ID=AA.TASK_ASSIGNED_TO_EMP_ID WHERE AA.TASK_ASSIGNED_BY_EMP_ID='" & Session("EmployeeId") & "' ) AAA ORDER BY  EMP_NAME "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddassignedto.DataSource = ds
        ddassignedto.DataTextField = "EMP_NAME"
        ddassignedto.DataValueField = "EMP_ID"
        ddassignedto.DataBind()

        Dim list As New ListItem
        list.Text = "Assigned To"
        list.Value = "-1"
        ddassignedto.Items.Insert(0, list)

    End Sub

    Public Sub BindPriority()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT * FROM TASK_PRIORITY_MASTER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddpriority.DataSource = ds
        ddpriority.DataTextField = "PRIORITY_DESCRIPTION"
        ddpriority.DataValueField = "RECORD_ID"
        ddpriority.DataBind()

        Dim list As New ListItem
        list.Text = "Select Priority"
        list.Value = "-1"
        ddpriority.Items.Insert(0, list)



    End Sub


    Public Sub BindStatus()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT * FROM TASK_STATUS_MASTER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddstatus.DataSource = ds
        ddstatus.DataTextField = "TASK_STATUS_DESCRIPTION"
        ddstatus.DataValueField = "STATUS_ID"
        ddstatus.DataBind()

        Dim list As New ListItem
        list.Text = "Select Status"
        list.Value = "-1"
        ddstatus.Items.Insert(0, list)

    End Sub


    Public Sub BindJobCategopry()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "SELECT DISTINCT A.TASK_CATEGORY_ID,( CATEGORY_DES+ ' - (' + MAIN_TAB_DESCRIPTION + '/'+ SUB_TAB_DESCRIPTION + ')' ) TASK_CATEGORY_DESCRIPTION,CATEGORY_DES AS CD " & _
                        " FROM dbo.TASK_CATEGORY_MASTER A  " & _
                        " INNER JOIN dbo.TASK_ROOTING_MASTER B  ON A.TASK_CATEGORY_ID=B.TASK_CATEGORY_ID   " & _
                        " INNER JOIN TASK_CATEGORY B1 ON A.CATEGORY_ID=B1.ID" & _
                        " INNER JOIN dbo.SUB_TAB_MASTER C ON C.SUB_TAB_ID = A.SUB_TAB_ID " & _
                        " INNER JOIN dbo.MAIN_TAB_MASTER D ON D.MAIN_TAB_ID = C.MAIN_TAB_ID " & _
                        " WHERE  B.TASK_CATEGORY_OWNER = 'TRUE' AND B.EMP_ID='" & HiddenEmpId.Value & "' " & _
                        " ORDER BY CD DESC "

        ''Bind Job Source
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddTaskCategory.DataSource = ds
        ddTaskCategory.DataTextField = "TASK_CATEGORY_DESCRIPTION"
        ddTaskCategory.DataValueField = "TASK_CATEGORY_ID"
        ddTaskCategory.DataBind()

        Dim list As New ListItem
        list.Text = "Task Category"
        list.Value = "-1"
        ddTaskCategory.Items.Insert(0, list)

    End Sub


    Protected Sub ddTaskCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddTaskCategory.SelectedIndexChanged
        lblmessage.Text = ""
        SearchTaskList()

    End Sub

    Public Sub SearchTaskList(Optional ByVal export As Boolean = False)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(12) As SqlClient.SqlParameter

        If ddTaskCategory.SelectedIndex > 0 Then
            pParms(0) = New SqlClient.SqlParameter("@TASK_CATEGORY", ddTaskCategory.SelectedValue)
        End If

        If txttasklistid.Text.Trim() <> "" Then
            pParms(1) = New SqlClient.SqlParameter("@TASK_LIST_ID", txttasklistid.Text.Trim())
        End If

        If txtreqdate.Text.Trim() <> "" Then
            pParms(2) = New SqlClient.SqlParameter("@TASK_TRAGET_DATE", txtreqdate.Text.Trim())
        End If

        If ddpriority.SelectedIndex > 0 Then
            pParms(3) = New SqlClient.SqlParameter("@TASK_PRIORITY", ddpriority.SelectedValue)
        End If

        If ddstatus.SelectedIndex > 0 Then
            pParms(4) = New SqlClient.SqlParameter("@TASK_STATUS", ddstatus.SelectedValue)
        End If

        If txtentryFromdate.Text.Trim() <> "" Then
            pParms(5) = New SqlClient.SqlParameter("@TASK_FROM_DATE", txtentryFromdate.Text.Trim())
        End If

        If txtentryTodate.Text.Trim() <> "" Then
            pParms(6) = New SqlClient.SqlParameter("@TASK_TO_DATE", txtentryTodate.Text.Trim())
        End If


        If txtkeyword.Text.Trim() <> "" Then
            pParms(7) = New SqlClient.SqlParameter("@KEYWORD", txtkeyword.Text.Trim())
        End If

        pParms(8) = New SqlClient.SqlParameter("@CLAIM_EMP_ID", HiddenEmpId.Value)

        If ddassignedto.SelectedValue <> "-1" Then
            pParms(9) = New SqlClient.SqlParameter("@TASK_ASSIGNED_TO", ddassignedto.SelectedValue)
        End If

        If ddEscalationLevel.SelectedIndex = 1 Then 'LEVEL 0
            pParms(11) = New SqlClient.SqlParameter("@ESCALATION_LEVEL", 0)
        ElseIf ddEscalationLevel.SelectedIndex = 2 Then 'LEVEL 1
            pParms(11) = New SqlClient.SqlParameter("@ESCALATION_LEVEL", 1)
        ElseIf ddEscalationLevel.SelectedIndex = 3 Then 'LEVEL 2
            pParms(11) = New SqlClient.SqlParameter("@ESCALATION_LEVEL", 2)
        ElseIf ddEscalationLevel.SelectedIndex = 4 Then 'LEVEL 0
            pParms(11) = New SqlClient.SqlParameter("@ESCALATION_LEVEL", 3)
        End If

        Dim ds As DataSet = Nothing

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TASK_SEARCH_FOR_TASK_CATEGORY", pParms)


        GridTaskList.DataSource = ds
        GridTaskList.DataBind()

        If export Then

            If ds.Tables(0).Rows.Count > 0 Then
                Dim dt As New DataTable
                dt = ds.Tables(0)
                dt.Columns.Remove("VIEW_TRACE_FLOW")
                dt.Columns.Remove("TASK_ID")
                dt.Columns.Remove("STATUS_COLOR")
                dt.Columns.Remove("VIEW_TASK")
                dt.Columns.Remove("IMAGE_PATH")
                dt.Columns.Remove("ASSIGN")
                dt.Columns.Remove("CloseVisible")
                dt.Columns.Remove("REASSIGN")
                dt.Columns.Remove("HISTORY")
                dt.Columns.Remove("CHART_COMPARE")
                dt.Columns.Remove("FOLLOWUP")
                dt.Columns.Remove("CLAIM_EMP_ID")
                ExportExcel(dt)
            End If

        End If

    End Sub


    Public Sub ExportExcel(ByVal dt As DataTable)

        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, "A1", True)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")
        ef.SaveXls(Response.OutputStream)

    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        lblmessage.Text = ""
        SearchTaskList()

    End Sub

    Protected Sub GridTaskList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridTaskList.PageIndexChanging
        GridTaskList.PageIndex = e.NewPageIndex
        SearchTaskList()

    End Sub

    Protected Sub GridTaskList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridTaskList.RowCommand
        lblmessage.Text = ""
        If e.CommandName = "claim" Then
            If HiddenEmpId.Value.Trim() <> "" Then
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
                Dim Sql_Query = "UPDATE TASK_LIST_MASTER SET CLAIM_EMP_ID='" & HiddenEmpId.Value & "' , CLAIM_DATE=GETDATE() WHERE TASK_LIST_ID='" & e.CommandArgument & "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, Sql_Query)
                lblmessage.Text = "You have successfully claimed the task."
                SearchTaskList()
            Else
                lblmessage.Text = "Please login again and claim this task."
            End If

        End If

    End Sub

    Protected Sub ddpriority_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddpriority.SelectedIndexChanged
        lblmessage.Text = ""
        SearchTaskList()
    End Sub

    Protected Sub ddstatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddstatus.SelectedIndexChanged
        lblmessage.Text = ""
        SearchTaskList()
    End Sub

    Protected Sub GridTaskList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridTaskList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim HiddenColor As HiddenField = DirectCast(e.Row.FindControl("HiddenColor"), HiddenField)
            If HiddenColor.Value <> "" Then
                e.Row.BackColor = Drawing.Color.FromName(HiddenColor.Value)  '(251, 204, 119)
            End If

        End If

        Dim row As DataRow
        If e.Row.RowType = DataControlRowType.DataRow Then
            row = CType(e.Row.DataItem, DataRowView).Row
            If row.Item("ESCALATION_LEVEL") = 3 Then
                e.Row.BackColor = Drawing.Color.FromArgb(255, 0, 0)
                e.Row.ForeColor = Drawing.Color.White
            ElseIf row.Item("ESCALATION_LEVEL") = 2 Then
                e.Row.BackColor = Drawing.Color.FromArgb(255, 165, 0)
            ElseIf row.Item("ESCALATION_LEVEL") = 1 Then
                e.Row.BackColor = Drawing.Color.FromArgb(255, 255, 0)
            Else

            End If
        End If
    End Sub

    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click

        SearchTaskList(True)

    End Sub
End Class
