﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
'Imports System.Data
'Imports System.Data.SqlClient
'Imports Microsoft.ApplicationBlocks.Data

Partial Class HelpDesk_UserControls_hdTaskEscalationLevelsMaster
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetFilter()
            BindEmployeeBsu()
            BindEmpName()
            BindBsuGrid()
            BindMainTabs()
            If Not Session("dtTaskEscalationLevelsMasterEmployeeExisting") Is Nothing Then
                Session.Remove("dtTaskEscalationLevelsMasterEmployeeExisting")
            End If
            Me.gvEMPNameExisting.DataSource = Nothing
            Me.gvEMPNameExisting.DataBind()
        Else
            If Not Session("dtTaskEscalationLevelsMasterEmployee") Is Nothing Then
                Dim dt As DataTable = CType(Session("dtTaskEscalationLevelsMasterEmployee"), DataTable)
                dt.Rows.Clear()
                Dim row As DataRow
                For Each grow As GridViewRow In Me.gvEMPName.Rows
                    row = dt.NewRow
                    row.Item("ID") = TryCast(grow.FindControl("lblEmpId"), Label).Text
                    row.Item("EMPNO") = TryCast(grow.FindControl("lblEmpNo"), Label).Text
                    row.Item("DESCR") = TryCast(grow.FindControl("lblEmpName"), Label).Text
                    row.Item("Level1") = TryCast(grow.FindControl("chkLevel1"), CheckBox).Checked
                    row.Item("Level2") = TryCast(grow.FindControl("chkLevel2"), CheckBox).Checked
                    row.Item("Level3") = TryCast(grow.FindControl("chkLevel3"), CheckBox).Checked
                    row.Item("CollectiveEmail") = TryCast(grow.FindControl("chkCollectiveEmail"), CheckBox).Checked
                    dt.Rows.Add(row)
                Next
                dt.AcceptChanges()
                Session("dtTaskEscalationLevelsMasterEmployee") = dt
            Else
                Dim dt As New DataTable
                dt.Columns.Add("ID", GetType(String))
                dt.Columns.Add("EMPNO", GetType(String))
                dt.Columns.Add("DESCR", GetType(String))
                dt.Columns.Add("Level1", GetType(Integer))
                dt.Columns.Add("Level2", GetType(Integer))
                dt.Columns.Add("Level3", GetType(Integer))
                dt.Columns.Add("CollectiveEmail", GetType(Integer))
                dt.AcceptChanges()
                Dim row As DataRow
                For Each grow As GridViewRow In Me.gvEMPName.Rows
                    row = dt.NewRow
                    row.Item("ID") = TryCast(grow.FindControl("lblEmpId"), Label).Text
                    row.Item("EMPNO") = TryCast(grow.FindControl("lblEmpNo"), Label).Text
                    row.Item("DESCR") = TryCast(grow.FindControl("lblEmpName"), Label).Text
                    row.Item("Level1") = TryCast(grow.FindControl("chkLevel1"), CheckBox).Checked
                    row.Item("Level2") = TryCast(grow.FindControl("chkLevel2"), CheckBox).Checked
                    row.Item("Level3") = TryCast(grow.FindControl("chkLevel3"), CheckBox).Checked
                    row.Item("CollectiveEmail") = TryCast(grow.FindControl("chkCollectiveEmail"), CheckBox).Checked
                    dt.Rows.Add(row)
                Next
                dt.AcceptChanges()
                Session("dtTaskEscalationLevelsMasterEmployee") = dt
            End If
        End If

        FillEmpNames(h_EMPID.Value)

        If Not Session("dtTaskEscalationLevelsMasterEmployeeExisting") Is Nothing Then
            Me.gvEMPNameExisting.DataSource = CType(Session("dtTaskEscalationLevelsMasterEmployeeExisting"), DataSet)
            Me.gvEMPNameExisting.DataBind()
        Else
            Me.gvEMPNameExisting.DataSource = Nothing
            Me.gvEMPNameExisting.DataBind()
        End If
    End Sub
    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub
    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet, dt As DataTable
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID, EMPNO as EMPNO, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR, 0 as Level1, 0 as Level2, 0 as Level3, 0 as CollectiveEmail from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If Not Session("dtTaskEscalationLevelsMasterEmployee") Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim row, tmprow As DataRow
            dt = Session("dtTaskEscalationLevelsMasterEmployee")
            For Each row In dt.Rows
                If ds.Tables(0).Select("ID='" & row.Item("ID") & "'").Length > 0 Then
                    tmprow = ds.Tables(0).Select("ID='" & row.Item("ID") & "'")(0)
                    tmprow.Item("Level1") = row.Item("Level1")
                    tmprow.Item("Level2") = row.Item("Level2")
                    tmprow.Item("Level3") = row.Item("Level3")
                Else

                End If
            Next

            For Each row In ds.Tables(0).Rows
                If dt.Select("ID = '" & row.Item("ID") & "'").Length <= 0 Then
                    tmprow = dt.NewRow
                    tmprow.Item("ID") = row.Item("ID")
                    tmprow.Item("EMPNO") = row.Item("EMPNO")
                    tmprow.Item("DESCR") = row.Item("DESCR")
                    tmprow.Item("Level1") = row.Item("Level1")
                    tmprow.Item("Level2") = row.Item("Level2")
                    tmprow.Item("Level3") = row.Item("Level3")
                    tmprow.Item("CollectiveEmail") = row.Item("CollectiveEmail")
                    dt.Rows.Add(tmprow)
                End If
            Next
            dt.AcceptChanges()
            Session("dtTaskEscalationLevelsMasterEmployee") = dt
        End If
        'gvEMPName.DataSource = ds
        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
            gvEMPName.DataSource = dt
        Else
            gvEMPName.DataSource = ds
        End If
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function
    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        Dim dt As DataTable, rowtoremove As DataRow
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            '    gvEMPName.PageIndex = gvEMPName.PageIndex
            '    FillEmpNames(h_EMPID.Value)

            If Not Session("dtTaskEscalationLevelsMasterEmployee") Is Nothing Then
                dt = Session("dtTaskEscalationLevelsMasterEmployee")
                If dt.Select("ID='" & lblEMPID.Text & "'").Length > 0 Then
                    rowtoremove = dt.Select("ID='" & lblEMPID.Text & "'")(0)
                    dt.Rows.Remove(rowtoremove)
                    dt.AcceptChanges()
                    Session("dtTaskEscalationLevelsMasterEmployee") = dt
                    h_EMPID.Value = ""
                    For Each row As DataRow In dt.Rows
                        h_EMPID.Value &= row.Item("ID") & "|"
                    Next
                    If h_EMPID.Value.EndsWith("|") Then
                        h_EMPID.Value.TrimEnd("|")
                    End If
                    gvEMPName.DataSource = dt
                    gvEMPName.DataBind()
                End If
            End If
        End If

        
    End Sub

    Protected Sub lnkbtngrdEMPExistingDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        Dim pParms(1) As SqlClient.SqlParameter
      
        Dim Con As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString())
        Try
            lblID = TryCast(sender.FindControl("lblNOTIFICATIONSID"), Label)
            If Not lblID Is Nothing Then
                pParms(0) = New SqlClient.SqlParameter("@NOTIFICATIONS_ID", lblID.Text)
                pParms(1) = New SqlClient.SqlParameter("@USR_ID", Session("sUsr_Id"))
                Con.Open()
                SqlHelper.ExecuteNonQuery(Con, CommandType.StoredProcedure, "DELETE_TASK_ESCALATION_LEVELS_NOTIFICATION_USERS", pParms)
            End If
            Me.ViewExistingSettings()
            Me.lblmessage.Text = "Data updated successfully"
        Catch ex As Exception
            Me.lblmessage.Text = "Error occured while deleting existing setting"
        Finally
            If Not Con Is Nothing Then If Not Con.State = ConnectionState.Closed Then Con.Close()
        End Try
        


    End Sub

    Public Sub GetFilter()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim Encr_decrData As New Encryption64
            Dim mednuid = "0"
            Try
                mednuid = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Catch ex As Exception

            End Try


            Dim Query = "SELECT * FROM ADD_ISSUE_CAT_FILTER WHERE MENU_ID='" & mednuid & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query)
            If ds.Tables(0).Rows.Count > 0 Then
                Hiddensubtabids.Value = ds.Tables(0).Rows(0).Item("SUB_TAB_IDS").ToString()
                Hiddentoplevelcatids.Value = ds.Tables(0).Rows(0).Item("TOP_LEVEL_CAT_IDS").ToString()
                Hiddentaskcatids.Value = ds.Tables(0).Rows(0).Item("TASK_CAT_IDS").ToString()
                HiddenType.Value = ds.Tables(0).Rows(0).Item("TYPE").ToString()

            End If


        Catch ex As Exception

        End Try

    End Sub

    Public Sub BindEmployeeBsu()
        Dim ds As New DataSet

        Dim query = " select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 " & _
                    " union select BSU_ID,BSU_NAME from dbo.BUSINESSUNIT_M WHERE BSU_CRM_NON_GEMS='True' order by BSU_NAME  "

        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, query)
        ddbsu.DataSource = ds
        ddbsu.DataTextField = "BSU_NAME"
        ddbsu.DataValueField = "BSU_ID"
        ddbsu.DataBind()
        ddbsu.SelectedValue = Session("sbsuid")
    End Sub


    Public Sub BindMainTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select * from  MAIN_TAB_MASTER WHERE ACTIVE='True'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddMaintab.DataSource = ds
        ddMaintab.DataTextField = "MAIN_TAB_DESCRIPTION"
        ddMaintab.DataValueField = "MAIN_TAB_ID"
        ddMaintab.DataBind()


        BindSubTabs()


    End Sub
    Public Sub BindSubTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select * from  SUB_TAB_MASTER where MAIN_TAB_ID='" & ddMaintab.SelectedValue & "' AND ACTIVE='True'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        Dim view As New DataView(ds.Tables(0))
        If Not Hiddensubtabids.Value = Nothing Then
            view.RowFilter = "SUB_TAB_ID not in (" & Hiddensubtabids.Value & ")"
        End If


        ddsubtab.DataSource = view
        ddsubtab.DataTextField = "SUB_TAB_DESCRIPTION"
        ddsubtab.DataValueField = "SUB_TAB_ID"
        ddsubtab.DataBind()

        BindJobCategory()
    End Sub

    Public Sub BindChildNodesLowlevelCategory(ByVal ParentNode As TreeNode)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = " select CONVERT(VARCHAR,LEVEL_ID) +'-'+ CONVERT(VARCHAR,A.TASK_CATEGORY_ID)TASK_CATEGORY_ID,CATEGORY_DES from dbo.TASK_MAIN_CATEGORY_LEVELS A  " & _
                        " INNER JOIN dbo.TASK_CATEGORY_MASTER B ON A.TASK_CATEGORY_ID=B.TASK_CATEGORY_ID  " & _
                        " INNER JOIN dbo.TASK_CATEGORY C ON B.CATEGORY_ID=C.ID WHERE A.MAIN_CATEGORY_ID='" & ParentNode.Value & "' "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        Dim view As New DataView(ds.Tables(0))
        If Not Hiddentaskcatids.Value = Nothing Then
            view.RowFilter = "TASK_CATEGORY_ID not in (" & Hiddentaskcatids.Value & ")"
        End If

        ds.Tables.Clear()
        ds.Tables.Add(view.ToTable())

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = "<font color='red'><strong>" + ds.Tables(0).Rows(i).Item("CATEGORY_DES").ToString() + "</strong></font>"
                ChildNode.Value = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString()
                ''ChildNode.ShowCheckBox = False
                ParentNode.ChildNodes.Add(ChildNode)
                ''ChildNode.NavigateUrl = GetNavigateDepartmentEdit(ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString())
            Next
        End If

    End Sub

    Private Function GetNavigateDepartmentEdit(ByVal pId As String) As String

        Return String.Format("javascript:var popup = window.showModalDialog('YearlyDepartmentsEdit.aspx?dep_id={0}', '','dialogHeight:500px;dialogWidth:600px;scroll:auto;resizable:yes;'); window.location.reload();", pId)

    End Function


    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = "Select MAIN_CATEGORY_ID,MAIN_CATEGORY_DESC from  TASK_MAIN_CATEGORY where PRIMARY_MAIN_CATEGORY_ID='" & childnodeValue & "' AND SUB_TAB_ID='" & ddsubtab.SelectedValue & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        Dim view As New DataView(ds.Tables(0))
        If Not Hiddentoplevelcatids.Value = Nothing Then
            view.RowFilter = "MAIN_CATEGORY_ID not in (" & Hiddentoplevelcatids.Value & ")"
        End If

        ds.Tables.Clear()
        ds.Tables.Add(view.ToTable())

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_DESC").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If

        For Each node As TreeNode In ParentNode.ChildNodes
            BindChildNodes(node)
        Next

        BindChildNodesLowlevelCategory(ParentNode)


    End Sub

    Public Sub BindJobCategory()
        TreeItemCategory.Nodes.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select MAIN_CATEGORY_ID,MAIN_CATEGORY_DESC from  TASK_MAIN_CATEGORY where PRIMARY_MAIN_CATEGORY_ID=0 AND SUB_TAB_ID='" & ddsubtab.SelectedValue & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        Dim view As New DataView(ds.Tables(0))
        If Not Hiddentoplevelcatids.Value = Nothing Then
            view.RowFilter = "MAIN_CATEGORY_ID not in (" & Hiddentoplevelcatids.Value & ")"
        End If

        ds.Tables.Clear()
        ds.Tables.Add(view.ToTable())


        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_DESC").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_ID").ToString()
                TreeItemCategory.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In TreeItemCategory.Nodes
            BindChildNodes(node)
        Next

        'TreeItemCategory.ExpandAll()


    End Sub


    Protected Sub ddsubtab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsubtab.SelectedIndexChanged
        lblmessage.Text = ""
        BindJobCategory()
    End Sub

    Protected Sub ddMaintab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddMaintab.SelectedIndexChanged
        BindSubTabs()
    End Sub

    Protected Sub ddbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddbsu.SelectedIndexChanged
        BindEmpName()
    End Sub

    Public Sub BindEmpName()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Dim Sql_Query = " SELECT  A.EMP_ID,isnull(EMP_FNAME,'') + ' ' + isnull(EMP_MNAME ,'') + ' ' +isnull(EMP_LNAME ,'') + ' - ' + isnull(EMPNO ,'') as EMP_NAME, EMD_CUR_MOBILE,EMD_EMAIL,'javascript:openWindow(''' + convert(varchar,A.EMP_ID) + '''); return false;' OPENW,'javascript:openAssignCategory(''' + convert(varchar,A.EMP_ID) + '''); return false;' OPENWC FROM  EMPLOYEE_M A " & _
                      " INNER JOIN EMPLOYEE_D B ON  A.EMP_ID= B.EMD_EMP_ID " & _
                      " WHERE EMP_BSU_ID='" & ddbsu.SelectedValue & "' and A.EMP_bACTIVE='True' and A.EMP_STATUS <> 4 ORDER BY EMP_FNAME "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddemp.DataSource = ds
        ddemp.DataTextField = "EMP_NAME"
        ddemp.DataValueField = "EMP_ID"
        ddemp.DataBind()


    End Sub

    Protected Sub ddemp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddemp.SelectedIndexChanged
        BindBsuGrid()
    End Sub

    Public Sub BindBsuGrid()
        Dim ds As New DataSet
        Dim qry = " select usr_name from dbo.USERS_M where USR_EMP_ID='" & ddemp.SelectedValue & "' AND USR_BDISABLE=0"
        Dim usr_name = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        Dim query = ""

        If Library.LibrarySuperAcess(Session("EmployeeId")) = "False" Then

            If usr_name <> "" Then
                query = " select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] ('" & usr_name & "') WHERE ISNULL(BSU_bSHOW,1)=1 " & _
                                 " union select BSU_ID,BSU_NAME from dbo.BUSINESSUNIT_M WHERE BSU_CRM_NON_GEMS='True' order by BSU_NAME  "

            Else
                query = " select BSU_ID,BSU_NAME from dbo.BUSINESSUNIT_M order by BSU_NAME "

            End If

        Else

            query = " select BSU_ID,BSU_NAME from dbo.BUSINESSUNIT_M order by BSU_NAME "

        End If


        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, query)
        GridRootingBsu.DataSource = ds
        GridRootingBsu.DataBind()

    End Sub


    Private Function ValidatePageData() As Boolean
        ValidatePageData = False
        'Business Unit
        If Me.ddbsu.SelectedIndex < 0 Then
            Me.lblmessage.Text = "Please select Business Unit"
            Exit Function
        End If
        'Main Tab
        If Me.ddMaintab.SelectedIndex < 0 Then
            Me.lblmessage.Text = "Please select Main Tab"
            Exit Function
        End If
        'Sub Tab (Departments)
        If Me.ddsubtab.SelectedIndex < 0 Then
            Me.lblmessage.Text = "Please select Department"
            Exit Function
        End If
        'Categories
        If TreeItemCategory.CheckedNodes.Count <= 0 Then
            Me.lblmessage.Text = "Please select atleast one Category"
            Exit Function
        End If
        'Level 1 Days
        If Not IsNumeric(Me.txtLevel1Days.Text) Or Val(Me.txtLevel1Days.Text) <= 0 Then
            Me.lblmessage.Text = "Please enter valid no for Level 1 Days. It should be numeric value and also not a decimal"
            Exit Function
        End If
        'Level 2 Days
        If Not IsNumeric(Me.txtLevel2Days.Text) Or Val(Me.txtLevel2Days.Text) <= 0 Then
            Me.lblmessage.Text = "Please enter valid no for Level 2 Days. It should be numeric value and also not a decimal"
            Exit Function
        End If
        'Level 3 Days
        If Not IsNumeric(Me.txtLevel3Days.Text) Or Val(Me.txtLevel3Days.Text) <= 0 Then
            Me.lblmessage.Text = "Please enter valid no for Level 3 Days. It should be numeric value and also not a decimal"
            Exit Function
        End If
        'Employee Grid
        If Me.gvEMPName.Rows.Count <= 0 Then
            Me.lblmessage.Text = "Please select atleast one employee who should be notified in the event of escalation of the task"
            Exit Function
        End If
        'For Each row As GridViewRow In Me.gvEMPName.Rows
        '    If Not DirectCast(row.FindControl("chkLevel1"), CheckBox).Checked Or Not DirectCast(row.FindControl("chkLevel2"), CheckBox).Checked Or Not DirectCast(row.FindControl("chkLevel3"), CheckBox).Checked Then
        '        Me.lblmessage.Text = "Please select atleast one escalation level for the selected employees"
        '        Exit Sub
        '    End If
        'Next
        If Not Session("dtTaskEscalationLevelsMasterEmployee") Is Nothing Then
            If CType(Session("dtTaskEscalationLevelsMasterEmployee"), DataTable).Rows.Count <= 0 Then
                Me.lblmessage.Text = "Please select atleast one escalation level for the selected employees"
                Exit Function
            End If
            For Each row As DataRow In CType(Session("dtTaskEscalationLevelsMasterEmployee"), DataTable).Rows
                If row.Item("Level1") = 0 And row.Item("Level2") = 0 And row.Item("Level3") = 0 Then
                    Me.lblmessage.Text = "Please select atleast one escalation level for the selected employees"
                    Exit Function
                End If
            Next
        End If

        Me.lblmessage.Text = ""
        Return True

    End Function

    Private Sub SavePageData()
        If Not Me.ValidatePageData() Then Exit Sub
        Dim Con As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString())
        Dim Tr As SqlTransaction
        Try
            Con.Open()
            Tr = Con.BeginTransaction
            'Task_Escalation_Levels_Master table
            For Each node As TreeNode In TreeItemCategory.CheckedNodes
                If node.Value.Contains("-") Then
                    Dim nval = node.Value.Split("-1")(1)
                    SaveEscalationLevels(Con, Tr, Me.ddbsu.SelectedValue, ddMaintab.SelectedValue, ddsubtab.SelectedValue, nval, Math.Ceiling(Val(Me.txtLevel1Days.Text)), Math.Ceiling(Val(Me.txtLevel2Days.Text)), Math.Ceiling(Val(Me.txtLevel3Days.Text)), Session("sUsr_Id"))
                End If
            Next
            'TASK_ESCALATION_LEVELS_NOTIFICATION_USERS table
            For Each node As TreeNode In TreeItemCategory.CheckedNodes
                If node.Value.Contains("-") Then
                    Dim nval = node.Value.Split("-1")(1)
                    If Not Session("dtTaskEscalationLevelsMasterEmployee") Is Nothing Then
                        For Each row As DataRow In CType(Session("dtTaskEscalationLevelsMasterEmployee"), DataTable).Rows
                            SaveEscalationLevelsNotificationsUsers(Con, Tr, Me.ddbsu.SelectedValue, ddMaintab.SelectedValue, ddsubtab.SelectedValue, nval, IIf(Val(row.Item("Level1")) > 0, 1, 0), IIf(Val(row.Item("Level2")) > 0, 1, 0), IIf(Val(row.Item("Level3")) > 0, 1, 0), row.Item("ID"), row.Item("CollectiveEmail"), Session("sUsr_Id"))
                        Next
                    End If
                End If
            Next
            Tr.Commit()
            Con.Close()
            lblmessage.Text = "Escalation Levels updated successfully."
            Me.h_EMPID.Value = ""
            Session.Remove("dtTaskEscalationLevelsMasterEmployee")
            Session.Remove("dtTaskEscalationLevelsMasterEmployeeExisting")
            Me.gvEMPName.DataSource = Nothing
            Me.gvEMPName.DataBind()
            Me.gvEMPNameExisting.DataSource = Nothing
            Me.gvEMPNameExisting.DataBind()
        Catch ex As Exception
            Tr.Rollback()
            lblmessage.Text = "There was an error setting escalation levels."
        Finally
            If Not Con.State = ConnectionState.Closed Then Con.Close()
        End Try

    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        Me.SavePageData()

        'Me.ValidatePageData()

        'Dim flag = 0

        'If TreeItemCategory.CheckedNodes.Count > 0 Then

        '    Dim node As TreeNode

        '    For Each node In TreeItemCategory.CheckedNodes

        '        If node.Value.Contains("-") Then
        '            Dim nval = node.Value.Split("-1")(1)

        '            For Each row As GridViewRow In GridRootingBsu.Rows

        '                If DirectCast(row.FindControl("Checkbsu"), CheckBox).Checked Then

        '                    Dim bsuid As String = DirectCast(row.FindControl("HiddenBsuid"), HiddenField).Value
        '                    Dim CheckOwner = CheckAccess.Items(0).Selected
        '                    Dim CheckMember = CheckAccess.Items(1).Selected
        '                    Dim CheckEmail = CheckAccess.Items(2).Selected
        '                    Dim CheckSMS = CheckAccess.Items(3).Selected
        '                    Dim reassign = CheckAccess.Items(4).Selected
        '                    SaveRooting(ddemp.SelectedValue, ddMaintab.SelectedValue, ddsubtab.SelectedValue, nval, bsuid, CheckOwner, CheckMember, CheckEmail, CheckSMS, reassign)
        '                    flag = 1
        '                End If

        '            Next
        '        End If

        '    Next

        '    If flag = 1 Then
        '        lblmessage.Text = "Successfully updated"
        '    Else
        '        lblmessage.Text = "Please select Business Unit"
        '    End If

        'Else

        '    lblmessage.Text = "Please select the categories."

        'End If

    End Sub


    Public Sub SaveRooting(ByVal emp_id As String, ByVal Main_Tab As String, ByVal Sub_Tab As String, ByVal Task_Category_ID As String, ByVal taskbsuid As String, ByVal CheckOwner As Boolean, ByVal CheckMember As Boolean, ByVal CheckEmail As Boolean, ByVal CheckSMS As Boolean, ByVal reassign As Boolean)


        Dim deleteflag = 0 '1-DELETE 0- NO DELETE

        If CheckOwner Or CheckMember Or CheckEmail Or CheckSMS Or reassign Then
            deleteflag = 0 ''No Delete
        Else
            deleteflag = 1 ''Delete
        End If

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(11) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@MAIN_TAB_ID", Main_Tab)
        pParms(1) = New SqlClient.SqlParameter("@SUB_TAB_ID", Sub_Tab)
        pParms(2) = New SqlClient.SqlParameter("@TASK_CATEGORY_ID", Task_Category_ID)
        pParms(3) = New SqlClient.SqlParameter("@TASK_CATEGORY_OWNER", CheckOwner)
        pParms(4) = New SqlClient.SqlParameter("@EMP_ID", emp_id)
        pParms(5) = New SqlClient.SqlParameter("@TASK_MEMBER", CheckMember)
        pParms(6) = New SqlClient.SqlParameter("@EMAIL_NOTIFY", CheckEmail)
        pParms(7) = New SqlClient.SqlParameter("@SMS_NOTIFY", CheckSMS)
        pParms(8) = New SqlClient.SqlParameter("@DELETE_FLAG", deleteflag)
        pParms(9) = New SqlClient.SqlParameter("@EMP_TASK_BSU_ID", taskbsuid)
        pParms(10) = New SqlClient.SqlParameter("@CAN_RESIGN", reassign)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_UPDATE_DELETE_TASK_ROOTING_MASTER", pParms)




    End Sub

    Public Sub SaveEscalationLevels(ByVal Con As SqlConnection, ByVal Tr As SqlTransaction, ByVal Bsu_Id As String, ByVal Main_Tab As String, ByVal Sub_Tab As String, ByVal Task_Category_Master_ID As String, ByVal Level1Days As Integer, ByVal Level2Days As Integer, ByVal Level3Days As Integer, ByVal Usr_Id As String)

        'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim cmd As New SqlCommand
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Bsu_Id)
        pParms(1) = New SqlClient.SqlParameter("@MAIN_TAB_ID", Main_Tab)
        pParms(2) = New SqlClient.SqlParameter("@SUB_TAB_ID", Sub_Tab)
        pParms(3) = New SqlClient.SqlParameter("@TASK_CATEGORY_MASTER_ID", Task_Category_Master_ID)

        pParms(4) = New SqlClient.SqlParameter("@LEVEL_1_DAYS", Level1Days)
        pParms(5) = New SqlClient.SqlParameter("@LEVEL_2_DAYS", Level2Days)
        pParms(6) = New SqlClient.SqlParameter("@LEVEL_3_DAYS", Level3Days)
        pParms(7) = New SqlClient.SqlParameter("@USR_ID", Usr_Id)

        'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SAVE_TASK_ESCALATION_LEVELS_MASTER", pParms)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "SAVE_TASK_ESCALATION_LEVELS_MASTER"
        cmd.Parameters.AddRange(pParms)
        cmd.Connection = Con
        cmd.Transaction = Tr
        cmd.ExecuteNonQuery()

    End Sub

    Public Sub SaveEscalationLevelsNotificationsUsers(ByVal Con As SqlConnection, ByVal Tr As SqlTransaction, ByVal Bsu_Id As String, ByVal Main_Tab As String, ByVal Sub_Tab As String, ByVal Task_Category_Master_ID As String, ByVal Level1 As Integer, ByVal Level2 As Integer, ByVal Level3 As Integer, ByVal Emp_Id As Integer, ByVal Collective_Email As Int32, ByVal Usr_Id As String)

        'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim cmd As New SqlCommand
        Dim pParms(9) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Bsu_Id)
        pParms(1) = New SqlClient.SqlParameter("@MAIN_TAB_ID", Main_Tab)
        pParms(2) = New SqlClient.SqlParameter("@SUB_TAB_ID", Sub_Tab)
        pParms(3) = New SqlClient.SqlParameter("@TASK_CATEGORY_MASTER_ID", Task_Category_Master_ID)

        pParms(4) = New SqlClient.SqlParameter("@LEVEL_1", Level1)
        pParms(5) = New SqlClient.SqlParameter("@LEVEL_2", Level2)
        pParms(6) = New SqlClient.SqlParameter("@LEVEL_3", Level3)
        pParms(7) = New SqlClient.SqlParameter("@EMP_ID", Emp_Id)
        pParms(8) = New SqlClient.SqlParameter("@COLLECTIVE_EMAIL", Collective_Email)
        pParms(9) = New SqlClient.SqlParameter("@USR_ID", Usr_Id)

        'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SAVE_TASK_ESCALATION_LEVELS_MASTER", pParms)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "SAVE_TASK_ESCALATION_LEVELS_NOTIFICATION_USERS"
        cmd.Parameters.AddRange(pParms)
        cmd.Connection = Con
        cmd.Transaction = Tr
        cmd.ExecuteNonQuery()

    End Sub

    Protected Sub gvEMPName_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvEMPName.RowUpdating
        Me.gvEMPName.Rows(e.RowIndex).Cells(1).Text = True
    End Sub

    Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        If Not Session("dtTaskEscalationLevelsMasterEmployee") Is Nothing Then
            Session.Remove("dtTaskEscalationLevelsMasterEmployee")
        End If
        'If Not Session("dtTaskEscalationLevelsMasterEmployeeExisting") Is Nothing Then
        '    Session.Remove("dtTaskEscalationLevelsMasterEmployeeExisting")
        'End If
    End Sub

    Protected Sub btnView_Click(sender As Object, e As EventArgs) Handles btnView.Click
        ViewExistingSettings()
    End Sub

    Private Sub ViewExistingSettings()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim con As New SqlConnection(str_conn)
        Dim cmd As New SqlCommand
        Dim nval, ids As String
        Dim ds As DataSet

        Dim pParms(3) As SqlClient.SqlParameter
        If Not TreeItemCategory.CheckedNodes.Count > 0 Then
            Me.lblmessage.Text = "Please select atleast one category to view the existing settings for it"
            Exit Sub
        Else
            Me.lblmessage.Text = Nothing
        End If
        Try
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Me.ddbsu.SelectedValue)
            pParms(1) = New SqlClient.SqlParameter("@MAIN_TAB_ID", Me.ddMaintab.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@SUB_TAB_ID", Me.ddsubtab.SelectedValue)

            For Each node As TreeNode In TreeItemCategory.CheckedNodes
                If node.Value.Contains("-") Then
                    nval = node.Value.Split("-1")(1)
                    ids &= nval & ","
                End If
            Next

            If ids.EndsWith(",") Then
                ids = ids.TrimEnd(",")
            End If

            pParms(3) = New SqlClient.SqlParameter("@TASK_CATEGORY_MASTER_IDs", ids)


            'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SAVE_TASK_ESCALATION_LEVELS_MASTER", pParms)

            con.Open()
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "GET_TASK_ESCALATION_LEVELS_NOTIFICATION_USERS", pParms)
            If Not Session("dtTaskEscalationLevelsMasterEmployeeExisting") Is Nothing Then
                Session.Remove("dtTaskEscalationLevelsMasterEmployeeExisting")
                Session("dtTaskEscalationLevelsMasterEmployeeExisting") = ds
            Else
                Session("dtTaskEscalationLevelsMasterEmployeeExisting") = ds
            End If
            Me.gvEMPNameExisting.DataSource = ds
            Me.gvEMPNameExisting.DataBind()
            Me.lblmessage.Text = Nothing
        Catch ex As Exception
            Me.lblmessage.Text = "Error occured while getting existing users for notifications"
        Finally
            If Not con Is Nothing Then If Not con.State = ConnectionState.Closed Then con.Close()
        End Try
    End Sub

    Protected Sub gvEMPNameExisting_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvEMPNameExisting.PageIndexChanging
        If Not Session("dtTaskEscalationLevelsMasterEmployeeExisting") Is Nothing Then
            gvEMPNameExisting.DataSource = CType(Session("dtTaskEscalationLevelsMasterEmployeeExisting"), DataSet)
        End If
        Me.gvEMPNameExisting.PageIndex = e.NewPageIndex
        gvEMPNameExisting.DataBind()
    End Sub
    Protected Sub h_EMPID_ValueChanged(sender As Object, e As EventArgs)
        FillEmpNames(h_EMPID.Value)
    End Sub


End Class
