Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class HelpDesk_UserControls_hdTaskHistory
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            HiddenTaskListID.Value = Request.QueryString("task_list_id").ToString()
            HiddenUserType.Value = Request.QueryString("Usertype").ToString()

            HiddenBsuid.Value = Session("sbsuid") ''"999998"
            HiddenEmpID.Value = Session("EmployeeId") ''"12581"

            BindNewAssignedTask()
            DisableTab()


        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub DisableTab()

        If HiddenUserType.Value = "MEMBER" Then
            T2.Enabled = False
            T3.Enabled = False
            Tab1.ActiveTabIndex = 0
        ElseIf HiddenUserType.Value = "CLIENT" Then
            T1.Enabled = False
            T3.Enabled = False
            Tab1.ActiveTabIndex = 1
        End If


    End Sub

    Public Sub BindNewAssignedTask()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", HiddenTaskListID.Value)
        pParms(1) = New SqlClient.SqlParameter("@ACTIVE", CheckActiveMembers.Checked)

        If HiddenUserType.Value = "MEMBER" Then
            pParms(2) = New SqlClient.SqlParameter("@TASK_ASSIGNED_BY_EMP_ID", HiddenEmpID.Value)
        End If

        Dim ds As DataSet = Nothing


        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TASK_SEARCH_GET_ASSIGNED_TASK_LIST", pParms)


        GridTaskStatus.DataSource = ds
        GridTaskStatus.DataBind()



    End Sub

    Protected Sub GridTaskStatus_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridTaskStatus.PageIndexChanging
        GridTaskStatus.PageIndex = e.NewPageIndex
        BindNewAssignedTask()
    End Sub

    Protected Sub CheckActiveMembers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckActiveMembers.CheckedChanged
        BindNewAssignedTask()
    End Sub

End Class
