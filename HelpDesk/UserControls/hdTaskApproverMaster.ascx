﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskApproverMaster.ascx.vb"
    Inherits="HelpDesk_UserControls_hdTaskApproverMaster" %>

<link type="text/css" href="../../cssfiles/sb-admin.css" rel="stylesheet" />
<link type="text/css" href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">

.gridheader_new 
{
	border-style: none;
        border-color: inherit;
        border-width: 0;
        font-family: Verdana, Arial, Helvetica, sans-serif; 
	background-image:url('../../Images/GRIDHEAD.gif') ;
	    background-repeat: repeat-x;font-size: 11px;
	    font-weight:bold;color:#1b80b6;
}
.inputbox 
{
	BACKGROUND: F5FED2; 
	BORDER-BOTTOM: #1B80B6 1px solid; 
	BORDER-LEFT: #1B80B6 1px solid; 
	BORDER-RIGHT: #1B80B6 1px solid; 
	BORDER-TOP: #1B80B6 1px solid; 
	COLOR: #555555; CURSOR: text; 
	FONT-FAMILY: verdana; 
	FONT-SIZE: 11px;
	WIDTH: 199px; 
	HEIGHT: 14px; 
	TEXT-DECORATION: none
}
    .button
    {}
</style>

<script language="javascript" type="text/javascript">

    <%--function GetEMPNAME(){
        var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 445px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;

        result = window.showModalDialog("../Payroll/Reports/Aspx/SelIDDESC.aspx?ID=EMP", "", sFeatures)

        if (result != '' && result != undefined) {
            document.getElementById('<%=h_EMPID.ClientID %>').value = result; //NameandCode[0];
            document.forms[0].submit();
        }
        else {
            return false;
        }

    }--%>

    function GetEMPNAME(){


            var oWnd = radopen("../Payroll/Reports/Aspx/SelIDDESC.aspx?ID=EMP", "pop_student");


    }



        function OnClientClose1(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                 document.getElementById('<%=h_EMPID.ClientID %>').value = arg.NameandCode;

 __doPostBack('<%=h_EMPID.ClientID%>', 'valueChanged');
            }
        }
  
function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    function OnTreeClick(evt) {
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
        if (isChkBoxClick) {
            var parentTable = GetParentByTagName("table", src);
            var nxtSibling = parentTable.nextSibling;
            if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
            {
                if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                {
                    //check or uncheck children at all levels
                    CheckUncheckChildren(parentTable.nextSibling, src.checked);
                }
            }
            //check or uncheck parents at all levels
            CheckUncheckParents(src, src.checked);
        }
    }

    function CheckUncheckChildren(childContainer, check) {
        var childChkBoxes = childContainer.getElementsByTagName("input");
        var childChkBoxCount = childChkBoxes.length;
        for (var i = 0; i < childChkBoxCount; i++) {
            childChkBoxes[i].checked = check;
        }
    }

    function CheckUncheckParents(srcChild, check) {
        var parentDiv = GetParentByTagName("div", srcChild);
        var parentNodeTable = parentDiv.previousSibling;

        if (parentNodeTable) {
            var checkUncheckSwitch;

            if (check) //checkbox checked
            {
                var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                if (isAllSiblingsChecked)
                    checkUncheckSwitch = true;
                else
                    return; //do not need to check parent if any child is not checked
            }
            else //checkbox unchecked
            {
                checkUncheckSwitch = false;
            }

            var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
            if (inpElemsInParentTable.length > 0) {
                var parentNodeChkBox = inpElemsInParentTable[0];
                parentNodeChkBox.checked = checkUncheckSwitch;
                //do the same recursively
                CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
            }
        }
    }

    function AreAllSiblingsChecked(chkBox) {
        var parentDiv = GetParentByTagName("div", chkBox);
        var childCount = parentDiv.childNodes.length;
        for (var i = 0; i < childCount; i++) {
            if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
            {
                if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                    var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                    //if any of sibling nodes are not checked, return false
                    if (!prevChkBox.checked) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    //utility function to get the container of an element by tagname
    function GetParentByTagName(parentTagName, childElementObj) {
        var parent = childElementObj.parentNode;
        while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
            parent = parent.parentNode;
        }
        return parent;
    }


    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("Checkbsu") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }


</script>
<telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-phone mr-3"></i>
Task Approver Master </div>
 <div class="card-body">
            <div class="table-responsive m-auto">

    <table align="center" width="100%">
    
        <tr id="tempdisp" visible="false" ><td align="left" width="25%"></td> <td align="left" width="25%"></td> <td align="left" width="25%"></td> <td align="left" width="25%"></td></tr>
      <tr>
        <td align="left" width="20%"> <span class="field-label">
            Business Unit</span> </td>
        <td colspan ="3" width="50%">
            <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>
          <td width="30%"></td>
    </tr >
    <tr id ="RowEmpName" runat = "server" visible ="false">
        <td align="left" width="20%"> <span class="field-label">
            Employee Name </span>
        </td>
        <td colspan ="3" width="80%">
            <asp:DropDownList ID="ddemp" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td align="left" width="20%"> <span class="field-label">
            Main Tab </span></td>
        <td width="30%">
<asp:DropDownList ID="ddMaintab" runat="server" AutoPostBack="True">
</asp:DropDownList>
        </td>

        <td align="left" width="20%"> <span class="field-label">
            Departments </span></td>
        <td width="30%">
<asp:DropDownList ID="ddsubtab" runat="server" AutoPostBack="True">
</asp:DropDownList>
        </td>

    </tr>
    <tr>
        <td align="left"> <span class="field-label">
            Categories </span>
        </td>
        <td colspan ="2" align="left">
            <div class="checkbox-list">
                            <asp:TreeView ID="TreeItemCategory" runat="server"  onclick="OnTreeClick(event);"  
                                 ImageSet="Msdn"  ShowCheckBoxes="All" ShowLines="True" >
                                
                                
                                <SelectedNodeStyle  HorizontalPadding="0px"
                                    VerticalPadding="0px" />
                                <NodeStyle  HorizontalPadding="0px"
                                    NodeSpacing="0px" VerticalPadding="0px" />
                            </asp:TreeView>
                </div>
        </td>
        <td></td>
    </tr>
    <tr id ="RowBusinessUnit" runat ="server" visible="false">
        <td align="left"> <span class="field-label">
            Business Unit (Access) </span>
        </td>
        <td colspan ="2">
            <asp:GridView ID="GridRootingBsu" runat="server" AutoGenerateColumns="false" EnableTheming="false" CssClass="table table-bordered table-row" Width="100%">
                <Columns>
                    <asp:TemplateField HeaderText="Owner">
                        <HeaderTemplate>
                            <center>
                        
                                <asp:CheckBox ID="chkAll1" runat="server" onclick="javascript:change_chk_stateg(this);"
                                    ToolTip="Click here to select/deselect all rows" />
                            </center>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:CheckBox ID="Checkbsu" runat="server"  />
                            </center>
                        </ItemTemplate>
                        <ItemStyle Width="60px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Business Unit">
                        <HeaderTemplate>
                            <span style="font-size: small">Business Unit</span>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span style="font-size: small">
                                <%#Eval("BSU_NAME")%></span>
                            <asp:HiddenField ID="HiddenBsuid" runat="server" Value='<%#Eval("BSU_ID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="gridheader_pop" Font-Size="Small" Height="20px" Wrap="False" />
                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <EditRowStyle Wrap="False" />
            </asp:GridView>
        </td>
        <td></td>
    </tr>
    <tr >
        <td colspan ="4" align="center">
        
            <asp:Button ID="btnView" runat="server" CssClass="button" Text="View Existing Approvers" Width="234px" Height="22px" />
        </td>
    </tr>
    <tr >
        
        <td></td>
        <td colspan="2">
                <asp:GridView ID="gvEMPNameExisting" runat="server" AutoGenerateColumns="False" Width="100%" PageSize="5" CssClass="table table-bordered table-row" EnableModelValidation="True" >
                    <Columns>
                        <asp:TemplateField HeaderText="ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblID" runat="server" Text='<%# Bind("TASK_APPROVER_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <ItemTemplate>
                                <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("CATEGORY") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EMP NO">
                            <ItemTemplate>
                                <asp:Label ID="lblEMPNO0" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCR" HeaderText="EMP NAME" >
                        <ItemStyle Width="35%" />
                        </asp:BoundField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdEMPDelete0" runat="server" OnClick="lnkbtngrdEMPExistingDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <%--<HeaderStyle CssClass="gridheader_new" />--%>
                </asp:GridView>
            </td>
        <td></td>
    </tr>
    <tr >
        <td > <span class="field-label">
            Task Approvers </span></td>
        <td colspan ="3">
                <asp:TextBox ID="txtEMPNAME" runat="server" Width="250px" CssClass="inputbox" 
                    ReadOnly="True"></asp:TextBox>
                <asp:LinkButton ID="lnlbtnAddEMPID" runat="server">Add</asp:LinkButton>
                <asp:ImageButton ID="imgGetEmp" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetEMPNAME();return false;" />
            </td>
    </tr>
    <tr >
        <td></td>
        <td colspan="2">
                <asp:GridView ID="gvEMPName" runat="server" AutoGenerateColumns="False"  Width="100%" PageSize="5" EnableModelValidation="True" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="EMP ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblEMPID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EMP NO">
                            <ItemTemplate>
                                <asp:Label ID="lblEMPNO" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCR" HeaderText="EMP NAME" >
                        <ItemStyle Width="60%" />
                        </asp:BoundField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Remove</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <%--<HeaderStyle CssClass="gridheader_new" />--%>
                </asp:GridView>
            </td>
        <td></td>
    </tr>
    <tr >
        
        <td colspan="4" align="center">
            <asp:Button ID="btnView0" runat="server" CssClass="button" Text="View Categories Requiring Document Uploading"  />
            </td>
    </tr>
    <tr >
       <td></td>
        <td colspan="2">
                <asp:GridView ID="gvCategories" runat="server" AutoGenerateColumns="False" 
                    Width="100%" PageSize="5" EnableModelValidation="True" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblUploadID" runat="server" Text='<%# Bind("ID")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <ItemTemplate>
                                <asp:Label ID="lblUploadCategory" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="83%" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdEMPDelete0" runat="server" OnClick="lnkbtngrdCategoriesDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <%--<HeaderStyle CssClass="gridheader_new" />--%>
                </asp:GridView>
            </td>
        <td></td>
    </tr>
    <tr >
        
        <td align="center" colspan="4" >
                <asp:CheckBox ID="chkUpload" runat="server" cssclass="field-label" Text="Upload Document Required While Member Closing the Task" />
            </td>
    </tr>
    <tr >
        
        <td colspan="4">
            <asp:CheckBoxList ID="CheckAccess" RepeatDirection="Horizontal" runat="server" 
                Visible="False">
                <asp:ListItem class="field-label"  Text="Owner" Value="O"></asp:ListItem>
                <asp:ListItem class="field-label" Text="Member" Value="M"></asp:ListItem>
                <asp:ListItem class="field-label" Text="Email (Owner)" Value="E"></asp:ListItem>
                <asp:ListItem class="field-label" Text="SMS (Owner)" Value="S"></asp:ListItem>
                <asp:ListItem class="field-label" Text="Reassign" Value="R"></asp:ListItem>
            </asp:CheckBoxList>
        </td>
    </tr>
    <tr>
        <td colspan="4" align="center">
            <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save"  /><br />
            <asp:Label ID="lblmessage0" runat="server" ForeColor="Red">Note: Any previous record with the same settings will be overwritten.</asp:Label>
                    
        </td>
    </tr>
    
    <tr>
        
        <td colspan="4" align="left">
            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
         </td>
    </tr>
</table>
          
          


    <asp:HiddenField ID="Hiddensubtabids" runat="server" />
    <asp:HiddenField ID="Hiddentoplevelcatids" runat="server" />
    <asp:HiddenField ID="Hiddentaskcatids" runat="server" />
    <asp:HiddenField ID="HiddenType" runat="server" />
    <asp:HiddenField ID="h_BSUID" runat="server" />
    <asp:HiddenField ID="h_EMPID" runat="server" OnValueChanged="h_EMPID_ValueChanged" />
                </div></div></div>
    
    