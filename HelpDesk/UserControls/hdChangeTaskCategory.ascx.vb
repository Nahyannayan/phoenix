Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class HelpDesk_UserControls_hdChangeTaskCategory
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenTaskListID.Value = Request.QueryString("task_list_id").ToString()
            BindMainTabs()
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)

        End If

    End Sub

    Public Sub BindMainTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select * from  MAIN_TAB_MASTER WHERE ACTIVE='True'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddMaintab.DataSource = ds
        ddMaintab.DataTextField = "MAIN_TAB_DESCRIPTION"
        ddMaintab.DataValueField = "MAIN_TAB_ID"
        ddMaintab.DataBind()


        BindSubTabs()


    End Sub
    Public Sub BindSubTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select * from  SUB_TAB_MASTER where MAIN_TAB_ID='" & ddMaintab.SelectedValue & "' AND ACTIVE='True'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddsubtab.DataSource = ds
        ddsubtab.DataTextField = "SUB_TAB_DESCRIPTION"
        ddsubtab.DataValueField = "SUB_TAB_ID"
        ddsubtab.DataBind()

        BindJobCategory()
    End Sub

    Protected Sub ddMaintab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddMaintab.SelectedIndexChanged
        BindSubTabs()
    End Sub

    Protected Sub ddsubtab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsubtab.SelectedIndexChanged
        BindJobCategory()
    End Sub


    Public Sub BindJobCategory()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = " select DISTINCT B.TASK_CATEGORY_ID as TASK_CATEGORY_ID ,CATEGORY_DES as TASK_CATEGORY_DESCRIPTION from " & _
                                       " dbo.TASK_CATEGORY_MASTER B " & _
                                       " INNER JOIN TASK_ROOTING_MASTER C ON C.TASK_CATEGORY_ID=B.TASK_CATEGORY_ID " & _
                                       " INNER JOIN TASK_CATEGORY C1 ON C1.ID=B.CATEGORY_ID  " & _
                                       " WHERE EMP_TASK_BSU_ID=(SELECT CALLER_BSU_ID FROM dbo.TASK_CONTACT_MASTER A INNER JOIN dbo.TASK_LIST_MASTER B ON A.TASK_ID= B.TASK_ID WHERE TASK_LIST_ID='" & HiddenTaskListID.Value & "') AND TASK_CATEGORY_OWNER='True' " & _
                                       " AND C.MAIN_TAB_ID='" & ddMaintab.SelectedValue & "' AND C.SUB_TAB_ID='" & ddsubtab.SelectedValue & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        RadioCategory.DataSource = ds
        RadioCategory.DataTextField = "TASK_CATEGORY_DESCRIPTION"
        RadioCategory.DataValueField = "TASK_CATEGORY_ID"
        RadioCategory.DataBind()

        Dim list As New ListItem
        list.Text = "General Category"
        list.Value = "-1"
        RadioCategory.Items.Insert(0, list)

        'If ds.Tables(0).Rows.Count > 0 Then
        '    btnupdate.Visible = True
        'Else
        '    btnupdate.Visible = False

        'End If

    End Sub


    Protected Sub btnupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim connection As New System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Dim transaction As System.Data.SqlClient.SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try
            Dim category = RadioCategory.SelectedValue
            If category <> "" Then
                Dim pParms(9) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TASK_CATEGORY", category)
                pParms(1) = New SqlClient.SqlParameter("@TASK_LIST_ID", HiddenTaskListID.Value)
                lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_TASK_CATEGORY", pParms)
                transaction.Commit()

                '' Send email
                SendEmailNotification()

                btnupdate.Visible = False
                btncancel.Text = " OK "
            Else
                lblmessage.Text = "Please Select a Category."
            End If


        Catch ex As Exception
            transaction.Rollback()
            lblmessage.Text = "Error occured while saving . " & ex.Message

        Finally
            connection.Close()

        End Try


    End Sub

    Public Sub SendEmailNotification()

    End Sub

End Class
