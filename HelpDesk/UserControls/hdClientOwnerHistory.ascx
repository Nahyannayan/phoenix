<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdClientOwnerHistory.ascx.vb"
    Inherits="HelpDesk_UserControls_hdClientOwnerHistory" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    <script type="text/javascript">
    
       function ViewCommmets(recordid)
        {
       
        window.showModalDialog('hdClientOwnerCommentsView.aspx?recordid=' + recordid  , '','dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        }
    
    </script>
    
<div>
    <asp:Panel ID="T5Panel1" runat="server">
        <table  width="100%">
            <tr>
                <td class="title-bg" >
                    Client-Owner Task History</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridClientHistory" AutoGenerateColumns="false" Width="920px" EmptyDataText="No Owner-Client interation made yet."
                        runat="server" AllowPaging="True">
                        <Columns>
                            <asp:TemplateField HeaderText="Entry By">
                                <HeaderTemplate>
                                    
                                                Entry By
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("EMPNAME") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type">
                                <HeaderTemplate>
                                    
                                                Type
                                            
                                </HeaderTemplate>
                                <ItemTemplate>
                                  <center> <%# Eval("LOGGER") %></center> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <HeaderTemplate>
                                   
                                                Status
                                </HeaderTemplate>
                                <ItemTemplate>
                                 <center>  <%# Eval("LOGGER_STATUS") %></center> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comments">
                                <HeaderTemplate>
                                   
                                                Comments
                                </HeaderTemplate>
                                <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkView" OnClientClick='<%#Eval("VIEW_COMMENTS")%>' runat="server">View</asp:LinkButton></center>
                                    <%--<asp:Label ID="T5lblview" runat="server" ForeColor="Red" Text="Show"></asp:Label>
                                    <asp:Panel ID="T5Panel1" runat="server" Height="50px">
                                        <asp:TextBox ID="txtdes" runat="server" EnableTheming="false" Text='<%# Eval("ENTRY_NOTES") %>'
                                            ReadOnly="true" TextMode="MultiLine" Height="220px" Width="350px"></asp:TextBox>
                                    </asp:Panel>
                                    <ajaxToolkit:CollapsiblePanelExtender ID="T5CollapsiblePanelExtender1" runat="server"
                                        AutoCollapse="False" AutoExpand="False" CollapseControlID="T5lblview" Collapsed="true"
                                        CollapsedSize="0" CollapsedText="Show" ExpandControlID="T5lblview" ExpandedSize="240"
                                        ExpandedText="Hide" ScrollContents="true" TargetControlID="T5Panel1" TextLabelID="T5lblview">
                                    </ajaxToolkit:CollapsiblePanelExtender>--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Entry Date">
                                <HeaderTemplate>
                                    
                                                Entry Date
                                </HeaderTemplate>
                                <ItemTemplate>
                                 <center><%# Eval("ENTRY_DATE") %></center> 
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                        <RowStyle CssClass="griditem"  Wrap="False" />
                        
                        <EmptyDataRowStyle Wrap="False" />
                        <EditRowStyle Wrap="False" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="HiddenTaskListID" runat="server" />
</div>
