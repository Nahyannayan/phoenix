Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class HelpDesk_UserControls_hdExternalTabs
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ActiveTab()
        End If
    End Sub

    Public Sub ActiveTab()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT * FROM SUB_TAB_MASTER WHERE MAIN_TAB_ID=2" ''EXTERNAL
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        Dim i = 0
        HT1.Enabled = False
        HT2.Enabled = False
        HT3.Enabled = False
        HT4.Enabled = False
        HT5.Enabled = False


        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim tab = ds.Tables(0).Rows(i).Item("TABLE_PREFIX").ToString().Trim()
            Dim flag = ds.Tables(0).Rows(i).Item("ACTIVE").ToString()

            If tab = "IT" And flag = "True" Then
                HT1.Enabled = True
            End If
            If tab = "HR" And flag = "True" Then
                HT2.Enabled = True
            End If
            If tab = "EDUCATION" And flag = "True" Then
                HT3.Enabled = True
            End If
            If tab = "FINANCE" And flag = "True" Then
                HT4.Enabled = True
            End If
            If tab = "STS" And flag = "True" Then
                HT5.Enabled = True
            End If

          
        Next


    End Sub
End Class
