<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdIT.ascx.vb" Inherits="HelpDesk_UserControls_hdIT" %>
<%@ Register Src="hdFileUpload.ascx" TagName="hdFileUpload" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript" >

function callblur()
{
 document.getElementById('<% = txtreqdate.ClientID %>').blur();
}

function OnTreeClick(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");

if(isChkBoxClick)
{
                        for(i=0; i<document.forms[0].elements.length; i++)
                           {
                               var currentid =document.forms[0].elements[i].id; 
                               if(document.forms[0].elements[i].type=="checkbox" && currentid != event.srcElement.id)
                                  {
                                    document.forms[0].elements[i].checked=false;
                                  }
                            }
}

}


</script>
<div class="matters">
  <center><asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></center> 
<asp:Panel ID="Panel1" runat="server" Width="700">
 <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="715" >
                        <tr>
                            <td class="subheader_img">
                                Basic Information</td>
                        </tr>
                        <tr>
                            <td >
<table>
    <tr>
        <td >
            Task&nbsp;Source</td>
        <td>
            :</td>
        <td >
            <asp:DropDownList ID="ddSource" runat="server">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td >
            Business&nbsp;Unit</td>
        <td>
            :</td>
        <td >
            <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td >
            Reported&nbsp;By</td>
        <td>
            :</td>
        <td >
            <asp:DropDownList ID="ddreportedby" runat="server"  AutoPostBack="True">
            </asp:DropDownList></td>
    </tr>
</table>
                                </td>
                    </tr>
               </table>
</asp:Panel>
<br />

<asp:Panel ID="Panel2" runat="server" Width="700" Visible="false">
 <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="715" >
                        <tr>
                            <td class="subheader_img">
                               Additional Information</td>
                        </tr>
                        <tr>
                            <td >
<table>
    <tr>
        <td >
            Name</td>
        <td >
            :</td>
        <td >
            <asp:TextBox ID="txtname" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td >
            Mobile</td>
        <td >
            :</td>
        <td >
            <asp:TextBox ID="txtmobile" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td >
            Email</td>
        <td >
            :</td>
        <td >
            <asp:TextBox ID="txtemail" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td >
            Fax</td>
        <td >
            :</td>
        <td >
            <asp:TextBox ID="txtfax" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td >
            Address</td>
        <td >
            :</td>
        <td >
            <asp:TextBox ID="txtaddress" EnableTheming="false" Width="155px" Height="100px"  runat="server"></asp:TextBox></td>
    </tr>
</table>
                                </td>
                    </tr>
               </table>
</asp:Panel>
<br />
<asp:Panel ID="Panel3" runat="server" Width="700">
               
               <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700" >
                        <tr>
                            <td class="subheader_img">
                                Task Information</td>
                        </tr>
                        <tr>
                            <td > 
               
               <table>
                   <tr>
                       <td>
                           Title</td>
                       <td>
                           :</td>
                       <td>
                           <asp:TextBox ID="txtTitle" runat="server" Width="446px"></asp:TextBox></td>
                   </tr>
                   <tr>
                       <td >
                           Task&nbsp;Category</td>
                       <td >
                           :</td>
                       <td >
                           <asp:DropDownList ID="ddJobCategory" runat="server">
                           </asp:DropDownList>
                           </td>
                   </tr>
                   <tr>
                       <td >
                           Priority</td>
                       <td >
                           :</td>
                       <td >
                           <asp:DropDownList ID="ddpriority" runat="server">
                           </asp:DropDownList></td>
                   </tr>
                   <tr>
                       <td >
                           Required&nbsp;Date</td>
                       <td >
                           :</td>
                       <td >
                           <asp:TextBox ID="txtreqdate" onfocus="javascript:callblur();return false;" runat="server"></asp:TextBox>
                           <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                   </tr>
                   <tr  >
                       <td colspan="3">
                           Task Description </td>
                   </tr>
                   <tr>
                       <td >
                           </td>
                       <td >
                           </td>
                       <td >
                       
                  <telerik:RadEditor ID="txtjobdes" Width="600px" EditModes="Design" ToolsFile="../xml/FullSetOfTools.xml" runat="server"></telerik:RadEditor>  
                 <%-- <telerik:RadEditor ID="txtaddnotes" Width="600px" EditModes="Design" ToolsFile="../xml/FullSetOfTools.xml" runat="server"></telerik:RadEditor>--%>

                         </td>
                   </tr>
                   <tr>
                       <td>
                       <br />
                           Attachments</td>
                       <td>
                       <br />
                           :</td>
                       <td>
                       <br />
                       
                           <uc1:hdFileUpload ID="HdFileUpload1" runat="server" />
                       </td>
                   </tr>
                   <tr>
                       <td align="center" colspan="3">
                           <br />
                           <br />
                    <asp:Button ID="btnsave" runat="server"  CssClass="button" Text="Save"  ValidationGroup="Save" Width="100px" /></td>
                   </tr>
               </table>
                                </td>
                    </tr>
               </table>
</asp:Panel>
 <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1" TargetControlID="txtreqdate"></ajaxToolkit:CalendarExtender>

    <asp:HiddenField ID="HiddenBsuid" runat="server" />
    <asp:HiddenField ID="HiddenUserId" runat="server" />
    <asp:HiddenField ID="HiddenMAIN_TAB_ID" runat="server" />
    <asp:HiddenField ID="HiddenSUB_TAB_ID" runat="server" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddreportedby"
        Display="None" ErrorMessage="Please Specify Reported Person" InitialValue="-1"
        SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator><br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTitle"
        Display="None" ErrorMessage="Please Enter Title" SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator><br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddJobCategory"
        Display="None" ErrorMessage="Please Select Task Category" InitialValue="0" SetFocusOnError="True"
        ValidationGroup="Save"></asp:RequiredFieldValidator><br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtjobdes"
                                    Display="None" ErrorMessage="Please Enter Job Description" ValidationGroup="Save" SetFocusOnError="True"></asp:RequiredFieldValidator></div><asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="Save" />


