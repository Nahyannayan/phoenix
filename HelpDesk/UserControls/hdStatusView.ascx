<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdStatusView.ascx.vb" Inherits="HelpDesk_UserControls_hdStatusView" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
    <script type="text/javascript" >
        function handleError() {

            return true;
        }
        window.onerror = handleError;
    function ViewStatus(statuslogid)
        {
       
      //  window.showModalDialog('hdStatusDescriptionView.aspx?statuslogid=' + statuslogid  , '','dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        var url = "hdStatusDescriptionView.aspx?statuslogid=" + statuslogid;
        var oWnd = radopen(url, "pop_DEDL");
        }
      
    function StatusChart(AssignedTaskid,emp_id)
        {
       
        window.showModalDialog('../FusionCharts/hdUserTaskProgressChart.aspx?emp_id=' + emp_id +'&AssignedTaskid='+ AssignedTaskid  , '','dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        var url = "../FusionCharts/hdUserTaskProgressChart.aspx?emp_id=" + emp_id +"&AssignedTaskid="+ AssignedTaskid;
        var oWnd = radopen(url, "pop_DEDL");
        }
    

    function OnClientCloseDEDL(oWnd, args) {
    }
    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }
    </script>
 <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
<br />
<br />

<telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_DEDL" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseDEDL"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        
    </telerik:RadWindowManager>
<div class="card mb-3">
       
        <div class="card-body">
            <div class="table-responsive">
<ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<asp:UpdatePanel id="UpdatePanel1" runat="server">
    <contenttemplate>
        
<table  cellpadding="5" cellspacing="0" width="100%">
    <tr>
        <td class="card-header letter-space">
           <i class="fa fa-book mr-3"></i> Task Progress </td>
    </tr>
    <tr>
        <td align="center">
                        <asp:GridView ID="GridStatus" runat="server" AutoGenerateColumns="false" EmptyDataText="Progress status not yet updated." Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                            <Columns>
                             <asp:TemplateField HeaderText="Date">
                                    <HeaderTemplate>
                                       
                                                   Date
                                                
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                      <center> <%#Eval("ENTRY_DATE")%> </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Percentage">
                                    <HeaderTemplate>
                                        
                                                    % Completed
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                    <center> <%#Eval("PERCENTAGE")%> </center>
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" />
                                </asp:TemplateField>

                                   <asp:TemplateField HeaderText="Exp Date">
                                    <HeaderTemplate>
                                      
                                                   Exp Date
                                                
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                    <center> <%#Eval("EXP_END_DATE")%> </center>
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status">
                                    <HeaderTemplate>
                                        
                                                   Status
                                                
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                      <center> 
                                         <asp:Image ID="Image1" ImageUrl='<%#Eval("IMAGE_PATH")%>' ToolTip='<%#Eval("STATUS_DESCRIPTION")%>' runat="server" />
                                   
                                     
                                      
                                       </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                               <asp:TemplateField HeaderText="View">
                                    <HeaderTemplate>
                                       
                                                    View
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                    <center> <asp:LinkButton ID="LinkView" OnClientClick='<%#Eval("VIEW_OPEN")%>' runat="server">View</asp:LinkButton></center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                            <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            <EmptyDataRowStyle Wrap="False" />
                            <EditRowStyle Wrap="False" />
                        </asp:GridView>
            <br />
            <asp:Button ID="btnchart" runat="server" CssClass="button"
                Text="Chart" ValidationGroup="s" Width="80px" />
            <asp:Button ID="btncancel" runat="server" CssClass="button" OnClientClick="javascript:window.close();"
                Text="Close" ValidationGroup="s" Width="80px" />
        </td>
    </tr>
</table></contenttemplate>
</asp:UpdatePanel>
<asp:HiddenField ID="HiddenAssignedTaskid" runat="server" />
<asp:HiddenField ID="HiddenEmpID" runat="server" />
</div>
            </div></div>