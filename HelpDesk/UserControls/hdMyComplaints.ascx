<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdMyComplaints.ascx.vb"
    Inherits="HelpDesk_UserControls_hdMyComplaints" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 140px;
    }
</style>
 <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Search
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
    <br /><br />
    <asp:LinkButton ID="lnkaddissue" Font-Size="Small" runat="server">Add Issues</asp:LinkButton>
    <table  cellpadding="5" cellspacing="0" width="100%">
       
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td>
                         <span class="field-label">   Task ID</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txttaskid" runat="server"></asp:TextBox>
                        </td>
                        <td class="style1">
                         <span class="field-label">   Entry Date</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtentrydate" runat="server"></asp:TextBox>
                        </td>
                        <td>
                         <span class="field-label">   Req Date</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtreqdate" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <span class="field-label">    Category</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddTaskCategory" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="style1">
                          <span class="field-label">  Priority</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddPriority" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>
                          <span class="field-label">  Keyword</span></td>
                        <td>
                            <asp:TextBox ID="txtkeyword" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <span class="field-label"> Status</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddstatus0" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="style1">
                           <span class="field-label"> Escalation Level</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddEscalationLevel" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" CssClass="button"
                                Text="Search" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table  cellpadding="5" cellspacing="0" width="100%">
        <tr>
            <td class="subheader_img">
                My Issues
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridTaskList" runat="server" AllowPaging="True" AutoGenerateColumns="false" OnRowDataBound="GridTaskList_RowDataBound"
                    PageSize="20" EmptyDataText="No Issues has been Registered. (or) Search query did not retrieve any results"
                    Width="100%" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Task List ID">
                            <HeaderTemplate>
                               
                                            Task ID
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_LIST_ID")%>
                                    <asp:HiddenField ID="HiddenTaskListId" runat="server" Value='<%#Eval("TASK_LIST_ID")%>' />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText=" Entry By">
                            <HeaderTemplate>
                               
                                            Entry By
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("ENTRY_EMP_NAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entry Date">
                            <HeaderTemplate>
                               
                                            Entry Date
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_DATE_TIME", "{0:dd/MMM/yyyy}")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tab">
                            <HeaderTemplate>
                                
                                            Tab
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("MAIN_TAB_DESCRIPTION")%>&nbsp;/&nbsp;<%#Eval("SUB_TAB_DESCRIPTION")%></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <HeaderTemplate>
                               
                                            Category
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("CATEGORY_DES")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <HeaderTemplate>
                                
                                          Subject
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkDes" runat="server" Text='<%#Eval("TASK_TITLE")%>' OnClientClick='<%#Eval("VIEW_TASK")%>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Req. Date">
                            <HeaderTemplate>
                               
                                            Req.Date
                                            <%-- <asp:LinkButton ID="Linksrd" CommandName="Sort" runat="server">Req.&nbsp;Date</asp:LinkButton>  --%>
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_TRAGET_DATE", "{0:dd/MMM/yyyy}")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Priority">
                            <HeaderTemplate>
                              
                                            Priority
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("PRIORITY_DESCRIPTION")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <HeaderTemplate>
                               
                                            Status
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:Image ID="Image1" ImageUrl='<%#Eval("IMAGE_PATH")%>' ToolTip='<%#Eval("TASK_STATUS_DESCRIPTION")%>'                    runat="server" />
                                    <br />
                                    <%#Eval("TASK_STATUS_DESCRIPTION")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="History">
                            <HeaderTemplate>
                               
                                            Details
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkHistory" runat="server" OnClientClick='<%#Eval("HISTORY")%>'>Details</asp:LinkButton>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--  <asp:TemplateField HeaderText="">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                          
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <asp:ImageButton ID="ImageMessage" ToolTip="Send Mail" OnClientClick='<%#Eval("SEND_MAIL")%>' ImageUrl="~/Images/Helpdesk/mail.png" runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Close">
                            <HeaderTemplate>
                               
                                            Status
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkClose" runat="server" Text='<%#Eval("CloseReopen")%>' OnClientClick='<%#Eval("CLIENT_CLOSE")%>'></asp:LinkButton></center>
                                <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" ConfirmText="Are you sure, Do you want continue?"
                                    TargetControlID="LinkClose">
                                </ajaxToolkit:ConfirmButtonExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                              <asp:TemplateField HeaderText="Category">
                            <HeaderTemplate>
                               
                                            Claimed by
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("CLAIM_EMPNAME")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Message">
                            <HeaderTemplate>
                               
                                            Follow Up
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:ImageButton ID="ImageFollowup" ImageUrl="~/Images/Helpdesk/followup.png" OnClientClick='<%#Eval("FOLLOWUP")%>'
                                        Visible='<%#Eval("FollowUpVisible")%>' ToolTip="Follow-Up, Correspondance with the Owner."
                                        Width="20px" Height="20px" runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <EditRowStyle Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
                </div>
            </div>
    <asp:HiddenField ID="HiddenEmpId" runat="server" />
    <asp:HiddenField ID="HiddenSort" Value="DESC" runat="server" />
    <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtentrydate"
        TargetControlID="txtentrydate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtreqdate"
        TargetControlID="txtreqdate">
    </ajaxToolkit:CalendarExtender>
</div>
