<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdExternalTabs.ascx.vb" Inherits="HelpDesk_UserControls_hdExternalTabs" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    
       <script type="text/javascript" >
   
    window.setTimeout('setpathExt()',100);
    
     function setpathExt()
        {
           var path = window.location.href
           var Rpath=path.substring(path.indexOf('?'),path.length)
           var objFrame
           
//           //IT
//           objFrame=document.getElementById("FEx1"); 
//           objFrame.src="Pages/hdITExt.aspx" + Rpath
//           
//            //HR
//           objFrame=document.getElementById("FEx2"); 
//           objFrame.src="Pages/hdHrExt.aspx" + Rpath
//           
//            //Education
//           objFrame=document.getElementById("FEx3"); 
//           objFrame.src="Pages/hdEducationExt.aspx" + Rpath
//           
//            //Finance
//           objFrame=document.getElementById("FEx4"); 
//           objFrame.src="Pages/hdFinanceExt.aspx" + Rpath
//           
//             //STS
//           objFrame=document.getElementById("FEx5"); 
//           objFrame.src="Pages/hdSTSExt.aspx" + Rpath
           
        }
   
   </script> 
    
<div>
    <div align="left" class="matters">
        <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
            <ajaxToolkit:TabPanel ID="HT1" runat="server">
                <ContentTemplate>
                   <iframe id="FEx1" height="1000" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="800"></iframe>
                 
                </ContentTemplate>
                <HeaderTemplate>
                  IT
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT2" runat="server">
                <ContentTemplate>
                     <iframe id="FEx2" height="1000" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="800"></iframe>
                   
                </ContentTemplate>
                <HeaderTemplate>
                HR
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
             <ajaxToolkit:TabPanel ID="HT3" runat="server">
                <ContentTemplate>
                    <iframe id="FEx3" height="1000" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="800"></iframe>
             
                </ContentTemplate>
                <HeaderTemplate>
                 Education
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT4" runat="server">
                <ContentTemplate>
                     <iframe id="FEx4" height="1000" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="800"></iframe>
                  
                
                </ContentTemplate>
                <HeaderTemplate>
                Finance
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT5" runat="server">
                <ContentTemplate>
                   
                  <iframe id="FEx5" height="1000" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="800"></iframe>
                  
                
                </ContentTemplate>
                <HeaderTemplate>
                STS
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>
        <asp:HiddenField ID="Hiddenempid" runat="server" />
        <asp:HiddenField ID="Hiddenbsuid" runat="server" />
        
    </div>
</div>
