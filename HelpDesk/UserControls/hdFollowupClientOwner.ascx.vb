Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI
Imports System.Data.SqlClient

Partial Class HelpDesk_UserControls_hdFollowupClientOwner
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

        If Not IsPostBack Then
            hiddenTaskListId.Value = Request.QueryString("Task_list_id")
            HiddenFrom.Value = Session("EmployeeId")
            HiddenTo.Value = Request.QueryString("to_emp_id")
            BindToDetails()
            BindGrid()
        End If


    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim sqlquery = " select ENTRY_DATE, " & _
                       " isnull(B.emp_displayname,ISNULL(B.EMP_FNAME, '') + ' ' + ISNULL(B.EMP_LNAME, ''))  AS FROM_EMPNAME, " & _
                       " isnull(C.emp_displayname,ISNULL(C.EMP_FNAME, '') + ' ' + ISNULL(C.EMP_LNAME, ''))  AS TO_EMPNAME, " & _
                       " SUBJECT,MESSAGE_TEXT,PRIORITY_HIGH,'javascript:ViewDes(''' + CONVERT(VARCHAR, A.RECORD_ID) + ''');return false;' AS ViewDes " & _
                       " from dbo.FOLLOW_UP_MESSAGE_LOG A " & _
                       " INNER JOIN OASIS.dbo.EMPLOYEE_M B ON B.EMP_ID=A.FROM_EMP_ID " & _
                       " INNER JOIN OASIS.dbo.EMPLOYEE_M C ON C.EMP_ID=A.TO_EMP_ID " & _
                       " WHERE  TASK_LIST_ID='" & hiddenTaskListId.Value & "' AND (FROM_EMP_ID = '" & HiddenFrom.Value & "' OR TO_EMP_ID='" & HiddenFrom.Value & "') ORDER BY RECORD_ID DESC"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sqlquery)

        GridTaskFollowup.DataSource = ds
        GridTaskFollowup.DataBind()


    End Sub
    Public Sub BindToDetails()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim sqlquery = " select  isnull(emp_displayname,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_LNAME, '')) EMPNAME,ISNULL(EMD_EMAIL,' (Email Id is missing. Email will not be sent)') EMD_EMAIL from OASIS.dbo.EMPLOYEE_M A " & _
                       " LEFT JOIN OASIS.dbo.EMPLOYEE_D B ON A.EMP_ID= B.EMD_EMP_ID " & _
                       " WHERE A.EMP_ID='" & HiddenTo.Value & "' "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sqlquery)

        If ds.Tables(0).Rows.Count > 0 Then
            lblFrom.Text = ds.Tables(0).Rows(0).Item("EMPNAME").ToString() & " - " & ds.Tables(0).Rows(0).Item("EMD_EMAIL").ToString()
        End If

    End Sub


  
    Protected Sub btnsend_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim commitflag = 0

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", hiddenTaskListId.Value)
            pParms(1) = New SqlClient.SqlParameter("@FROM_EMP_ID", HiddenFrom.Value)
            pParms(2) = New SqlClient.SqlParameter("@TO_EMP_ID", HiddenTo.Value)
            pParms(3) = New SqlClient.SqlParameter("@SUBJECT", txtsubject.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@MESSAGE_TEXT", txtdetails.Content)
            pParms(5) = New SqlClient.SqlParameter("@PRIORITY_HIGH", CheckHighPriority.Checked)

            If DirectCast(HdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value <> "" Then
                pParms(6) = New SqlClient.SqlParameter("@UPLOAD_IDS", DirectCast(HdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value)
            End If

            Dim MessageId = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_FOLLOW_UP_MESSAGE_LOG", pParms)

            transaction.Commit()
            commitflag = 1
            If commitflag = 1 Then
                ''Send Message

                Dim ds As DataSet
                ds = HelpDesk.GetCommunicationSettings()
                Dim username = ""
                Dim password = ""
                Dim port = ""
                Dim host = ""
                Dim fromemailid = ""
                If ds.Tables(0).Rows.Count > 0 Then

                    username = ds.Tables(0).Rows(0).Item("USERNAME").ToString()
                    password = ds.Tables(0).Rows(0).Item("PASSWORD").ToString()
                    port = ds.Tables(0).Rows(0).Item("PORT").ToString()
                    host = ds.Tables(0).Rows(0).Item("HOST").ToString()
                    fromemailid = ds.Tables(0).Rows(0).Item("FROMEMAIL").ToString()

                End If

                Dim sb As New Object
                Dim pParms1(4) As SqlClient.SqlParameter
                pParms1(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", hiddenTaskListId.Value)
                pParms1(1) = New SqlClient.SqlParameter("@MESSAGE_ID", MessageId)
                pParms1(2) = New SqlClient.SqlParameter("@OPTION", 9)
                sb = SqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString, CommandType.StoredProcedure, "HD_MAIL_TEXT", pParms1)
                Dim returnval = HelpDesk.SendPlainTextEmails(fromemailid, HelpDesk.GetEmployeeEmailId(HiddenTo.Value), txtsubject.Text, sb.ToString(), username, password, host, port, HelpDesk.GetuploadIds(MessageId, 4))


                Dim sql_query = "UPDATE FOLLOW_UP_MESSAGE_LOG SET EMAIL_STATUS='" & returnval & "' WHERE RECORD_ID='" & MessageId & "'"
                SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString, CommandType.Text, sql_query)

                txtsubject.Text = ""
                txtdetails.Content = ""
                CheckHighPriority.Checked = False
                DirectCast(HdFileUpload1.FindControl("GridUpload"), GridView).Controls.Clear()
                DirectCast(HdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value = ""
                BindGrid()
                lblmessage.Text = "Message Sent Successfully."

            End If

        Catch ex As Exception

            If commitflag = 0 Then
                transaction.Rollback()
            End If

            lblmessage.Text = "Error occured while saving . " & ex.Message

        End Try

    End Sub


    Protected Sub GridTaskFollowup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridTaskFollowup.PageIndexChanging
        GridTaskFollowup.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class
