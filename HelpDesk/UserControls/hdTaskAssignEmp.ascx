<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskAssignEmp.ascx.vb" Inherits="HelpDesk_UserControls_hdTaskAssignEmp" %>
<%@ Register Src="hdFileUploadViewAssign.ascx" TagName="hdFileUploadViewAssign" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
 <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
 
 <script type="text/javascript" >
 
 function openassign(empid)
 {
  var path = window.location.href
  var Rpath=path.substring(path.indexOf('?'),path.length)
  Rpath= Rpath + '&Emp_id='+ empid
  window.showModalDialog('../Pages/hdTaskAssignEmpEnter.aspx' + Rpath, '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;     
 }
 
 function NewOwner()
 {
 var cat= document.getElementById('<%=HiddenTaskCategory.ClientID %>').value;
 var task_list_id= document.getElementById('<%=HiddenTaskListID.ClientID %>').value;
 var Rpath='?task_category_id='+ cat + '&task_list_id=' + task_list_id 
 window.open('../Pages/hdChangeOwner.aspx'+ Rpath , '','Height=400px,Width=400px,scrollbars=yes,resizable=no,directories=no'); 
 window.close();
 return false;
 }
 
 function NewCategory()
 {
 var task_list_id= document.getElementById('<%=HiddenTaskListID.ClientID %>').value;
 var Rpath='?task_list_id=' + task_list_id 
 window.open('../Pages/hdChangeTaskCategory.aspx'+ Rpath , '','Height=400px,Width=400px,scrollbars=yes,resizable=no,directories=no'); 
 window.close();
 return false;
 }

 //Javascript Error Handling

 function handleError() {

     return true;
 }
 window.onerror = handleError;


 </script>
 
 <div class="matters" align="center" >
 <br />
 <br />
     <ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
     </ajaxToolkit:ToolkitScriptManager>
 <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700px" >
                        <tr>
                            <td class="subheader_img">
                                Task Details</td>
                        </tr>
                        <tr>
                            <td align="left">
<table>
    <tr>
        <td>
            Task ID</td>
        <td>
            :</td>
        <td>
            <asp:Label ID="Label1" runat="server"></asp:Label></td>
        <td>
            Route Tab</td>
        <td>
            :</td>
        <td>
            <asp:Label ID="Label2" runat="server"></asp:Label></td>
        <td>
            Priority</td>
        <td>
            :</td>
        <td>
            <asp:Label ID="Label5" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>
            Reported By</td>
        <td>
            :</td>
        <td>
            <asp:Label ID="Label9" runat="server"></asp:Label></td>
        <td>
            Task Category</td>
        <td>:
        </td>
        <td>
            <asp:Label ID="Label8" runat="server"></asp:Label></td>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Req. Date</td>
        <td>
            :</td>
        <td>
            <asp:Label ID="Label3" runat="server"></asp:Label></td>
        <td>
            Entry Date</td>
        <td>
            :</td>
        <td>
            <asp:Label ID="Label4" runat="server"></asp:Label></td>
        <td>
            Status</td>
        <td>
            :</td>
        <td>
            <asp:Label ID="Label6" runat="server"></asp:Label></td>
    </tr>
</table>
                                <br />
         <asp:Label ID="T5lblview" runat="server" ForeColor="Red" Text="Show"></asp:Label>
                                <asp:Panel ID="T5Panel1" runat="server" Height="50px">
                               
                                <uc1:hdFileUploadViewAssign ID="HdFileUploadViewAssign1" runat="server" />
                                    <br />
                                    <asp:Label ID="lbltxtdes" EnableTheming="true"  runat="server"></asp:Label>
                                
                              
                                </asp:Panel>
                                <br />
                                <ajaxToolkit:CollapsiblePanelExtender ID="T5CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T5lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText="Show" ExpandControlID="T5lblview"
                                    ExpandedSize="300" ExpandedText="Hide" ScrollContents="true" TargetControlID="T5Panel1"
                                    TextLabelID="T5lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>

 </td>
                    </tr>
               </table>
<br />
<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700px" >
    <tr>
        <td class="subheader_img">
            Task Members</td>
    </tr>
    <tr>
        <td >
            <asp:GridView ID="GridTaskMembers" AutoGenerateColumns="false"  EmptyDataText="No Members allocated for this category.Please assign Members and then proceed." Width="685px" runat="server" AllowPaging="True" PageSize="10">
            <Columns>
            <asp:TemplateField HeaderText="Name">
             <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           Name
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
            <ItemTemplate>
            <%#Eval("EMPNAME")%>
            <asp:HiddenField ID="HiddenEmpId" Value='<%#Eval("EMP_ID")%>' runat="server" />
            <asp:HiddenField ID="HiddenEmailid" Value='<%#Eval("EMD_EMAIL")%>' runat="server" />
            <asp:HiddenField ID="HiddenMobileNumber" Value='<%#Eval("EMD_CUR_MOBILE")%>' runat="server" />
            </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="BSU">
              <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           BSU
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
            <ItemTemplate>
            <center><%#Eval("BSU_SHORTNAME")%></center>
            </ItemTemplate>
            </asp:TemplateField>
                  <asp:TemplateField HeaderText="Assign">
              <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           Assign
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
            <ItemTemplate>
            <center>
                <asp:LinkButton ID="LinkAssign"  Visible='<%#Eval("DisableRow")%>' OnClientClick='<%#Eval("OpenAssign")%>' runat="server">Assign</asp:LinkButton>
                
            </center>
            </ItemTemplate>
            </asp:TemplateField>            
            </Columns>
             <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <EditRowStyle Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:LinkButton ID="lnkNewOwner" OnClientClick="javascript:NewOwner(); return false;" runat="server">Assign New Owner</asp:LinkButton>
            ||
            <asp:LinkButton ID="lnkChangeCategory" OnClientClick="javascript:NewCategory(); return false;" runat="server">Change Category</asp:LinkButton></td>
    </tr>
    <tr>
        <td align="center">
                <asp:Button ID="btnwcancel" runat="server" CssClass="button" OnClientClick="javascript:window.close();"
                    Text="Close" Width="100px" />
          
            </td>
    </tr>
</table>
<ajaxToolkit:ConfirmButtonExtender ID="CF1" ConfirmText="Assign new owner for this task ?" TargetControlID="lnkNewOwner" runat="server"></ajaxToolkit:ConfirmButtonExtender> 
<ajaxToolkit:ConfirmButtonExtender ID="CF2" ConfirmText="Assigning new cagegory will remove current ownership.Do you wish to continuie?" TargetControlID="lnkChangeCategory" runat="server"></ajaxToolkit:ConfirmButtonExtender> 

     <br />

<br />
<asp:HiddenField ID="HiddenSubTabID" runat="server" />
<asp:HiddenField ID="HiddenEmpID" runat="server" />
<asp:HiddenField ID="HiddenTaskCategory" runat="server" />
<asp:HiddenField ID="HiddenTaskListID" runat="server" />
<asp:HiddenField ID="HiddenBsuid" runat="server" />
<asp:HiddenField ID="HiddenPriorityId" runat="server" />
</div>