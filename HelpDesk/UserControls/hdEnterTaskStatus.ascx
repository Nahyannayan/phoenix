<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdEnterTaskStatus.ascx.vb"
    Inherits="HelpDesk_UserControls_hdEnterTaskStatus" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
     
 <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Search
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
    <table   width="100%" class="table table-bordered table-row">
       
       
                    <tr align="left">
                        <td align="left" width="16%">
                           <span class="field-label">    Task ID</span>
                        </td>
                       
                         <td align="left" width="16%">
                            <asp:TextBox ID="txtassignid" runat="server"></asp:TextBox>
                        </td>
                          <td align="left" width="16%">
                            <span class="field-label">  Status</span>
                        </td>
                       
                         <td align="left" width="16%">
                            <asp:DropDownList ID="ddstatus" runat="server">
                            </asp:DropDownList>
                        </td>
                         <td align="left" width="16%">
                            <span class="field-label">   Assigned By</span>
                        </td>
                        
                         <td align="left" width="16%">
                            <asp:DropDownList ID="ddassignedby" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left">
                        <td>
                            <span class="field-label">   Start Date</span>
                        </td>
                        
                        <td>
                            <asp:TextBox ID="txtstartdate" runat="server"></asp:TextBox>
                        </td>
                        <td>
                             <span class="field-label">  Target Date</span>
                        </td>
                        
                        <td>
                            <asp:TextBox ID="txttargetdate" runat="server"></asp:TextBox>
                        </td>
                        <td>
                           <span class="field-label">    Assign Date</span>
                        </td>
                        
                        <td>
                            <asp:TextBox ID="txtassigndate" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td>
                           <span class="field-label">    Keyword</span>
                        </td>
                        
                        <td>
                            <asp:TextBox ID="txtkeyword" runat="server"></asp:TextBox>
                        </td>
                        <td>
                          <span class="field-label">     Priority</span>
                        </td>
                       
                        <td>
                            <asp:DropDownList ID="ddpriority" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                           <span class="field-label">    BSU</span>
                        </td>
                        
                        <td>
                            <asp:DropDownList ID="ddbsu" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left">
                        <td>
                          <span class="field-label">     Task Category</span>
                        </td>
                        
                        <td colspan="3">
                            <asp:DropDownList ID="ddcategory" runat="server">
                            </asp:DropDownList>
                           
                        </td>
                        <td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:Button ID="btnsearch" runat="server" CssClass="button" Text="Search" />
                            <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Export" />
                        </td>
                    </tr>
               
    </table>
    <br />
    <table   width="100%">
        <tr>
            <td class="title-bg">
                Tasks
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridTaskStatus" runat="server" AutoGenerateColumns="false" PageSize="20"
                    Width="100%" EmptyDataText="No Task has been assigned to you. (or) Search query did not retrieve any results"
                    AllowPaging="True" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Task Assign ID.">
                            <HeaderTemplate>
                               
                                            Task ID
                                          
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_LIST_ID") %>
                                </center>
                                <asp:HiddenField ID="HiddenTaskAssignid" runat="server" Value='<%#Eval("TASK_ASSIGN_ID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Root Tab">
                            <HeaderTemplate>
                              
                                            Route Tab
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("RootTab") %>
                                    <br />
                                    <%#Eval("BSU_SHORTNAME") %>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <HeaderTemplate>
                               
                                            Task Category
                                            
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_CATEGORY_DESCRIPTION") %>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Assigned By">
                            <HeaderTemplate>
                                
                                            Assigned By

                                          
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("EMPNAME")%>
                                <br />
                                (<%#Eval("ASSIGN_BSU_CODE")%>)
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Task Description">
                            <HeaderTemplate>
                               
                                            Task
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkViewTask" runat="server" CommandName="ViewTask" Text='<%#Eval("TASK_TITLE")%>'
                                        OnClientClick='<%#Eval("VIEW_TASK")%>' CommandArgument='<%#Eval("TASK_ASSIGN_ID")%>'></asp:LinkButton></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Start Date">
                            <HeaderTemplate>
                               
                                            Start Date
                                          
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_ASSIGN_START_DATE") %>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Target Date">
                            <HeaderTemplate>
                               
                                            Target Date
                                          
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_ASSIGN_END_DATE") %>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Priority">
                            <HeaderTemplate>
                                
                                            Priority
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("PRIORITY_DESCRIPTION")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Assign Date">
                            <HeaderTemplate>
                              
                                            Assign Date
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("ENTRY_DATE")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Exp Date">
                            <HeaderTemplate>
                                
                                            Exp Date
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("EXP_END_DATE")%>
                                </center>
                            </ItemTemplate>
                            <ItemStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <HeaderTemplate>
                               
                                            Status
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("CURRENT_STATUS")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Update">
                            <HeaderTemplate>
                               
                                            Update
                                            
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%-- <asp:LinkButton ID="LinkUpdate" runat="server" OnClientClick='<%#Eval("ENTER_STATUS")%>'
                                                        Visible='<%#Eval("updateVisible")%>'>Update</asp:LinkButton>
                                    --%>
                                    <asp:ImageButton ID="ImageUpdate" OnClientClick='<%#Eval("ENTER_STATUS")%>' Visible='<%#Eval("updateVisible")%>'
                                        ToolTip="Click here to update your current progress of this task." Width="20px"
                                        Height="20px" ImageUrl="~/Images/Helpdesk/Update.png" runat="server" />
                                    <asp:Label ID="lblmessage" runat="server" Text='<%#Eval("labelmessage")%>'></asp:Label>
                                    <asp:Image ID="Image1" ImageUrl='~\Images\Helpdesk\status\Owner Closed.png' Visible="false"
                                        ToolTip='Owner Closed' runat="server" />
                                    <asp:Image ID="Image2" ImageUrl='~\Images\Helpdesk\status\Client Closed.png' Visible="false"
                                        ToolTip='Client Closed' runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reassign">
                            <HeaderTemplate>
                               
                                            Reassign
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%-- <asp:LinkButton ID="LinkReAssign" OnClientClick='<%#Eval("REASSIGN")%>'  Visible='<%#Eval("VisibleReassign")%>' runat="server">Reassign</asp:LinkButton>--%>
                                    <asp:ImageButton ID="ImageReAssign" OnClientClick='<%#Eval("REASSIGN")%>' Visible='<%#Eval("VisibleReassign")%>'
                                        ToolTip="Click here to Reassign task to other members." Width="20px" Height="20px"
                                        ImageUrl="~/Images/Helpdesk/Reassign.png" runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="History">
                            <HeaderTemplate>
                               
                                            Details
                                          
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%--<asp:LinkButton ID="LinkHistory" runat="server"  Visible='<%#Eval("VisibleReassign")%>'  OnClientClick='<%#Eval("HISTORY")%>'>Details</asp:LinkButton>--%>
                                    <asp:ImageButton ID="ImageHistory" OnClientClick='<%#Eval("HISTORY")%>' Visible='<%#Eval("VisibleReassign")%>'
                                        ToolTip="Click here to find more details of this task." Width="20px" Height="20px"
                                        ImageUrl="~/Images/Helpdesk/Details.png" runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <HeaderTemplate>
                               
                                            Progress
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%-- <asp:LinkButton ID="LinkView" runat="server" OnClientClick='<%#Eval("VIEW_STATUS")%>'>Progress</asp:LinkButton>--%>
                                    <asp:ImageButton ID="ImageView" OnClientClick='<%#Eval("VIEW_STATUS")%>' ToolTip="Click here to view your progress of this task."
                                        Width="20px" Height="20px" ImageUrl="~/Images/Helpdesk/Progress.png" runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Message">
                            <HeaderTemplate>
                             
                                            Follow Up
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:ImageButton ID="ImageFollowup" ImageUrl="~/Images/Helpdesk/followup.png" OnClientClick='<%#Eval("FOLLOWUP")%>'
                                        ToolTip="Follow-Up" Width="20px" Height="20px" runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="" Height="30px" Wrap="False" />
                    <RowStyle CssClass="" Height="25px" Wrap="False" />
                    <SelectedRowStyle CssClass="" Wrap="False" />
                    <AlternatingRowStyle CssClass="" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <EditRowStyle Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <asp:HiddenField ID="HiddenEmpID" runat="server" />
    <asp:HiddenField ID="HiddenBsuid" runat="server" />
    <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtstartdate"
        TargetControlID="txtstartdate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txttargetdate"
        TargetControlID="txttargetdate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CE3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtassigndate"
        TargetControlID="txtassigndate">
    </ajaxToolkit:CalendarExtender>
                </div>
            </div>
</div>
