Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI
Partial Class HelpDesk_UserControls_hdFollowupClientView
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindDetails(Request.QueryString("Record_id"))
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub BindDetails(ByVal record_id)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT SUBJECT,MESSAGE_TEXT,UPLOAD_IDS  FROM FOLLOW_UP_MESSAGE_LOG where RECORD_ID='" & record_id & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then

            lbltitle.Text = "Subject :       " & ds.Tables(0).Rows(0).Item("SUBJECT").ToString()
            DataView.InnerHtml = ds.Tables(0).Rows(0).Item("MESSAGE_TEXT").ToString()
            HdFileUploadViewAssign1.Uploadids = ds.Tables(0).Rows(0).Item("UPLOAD_IDS").ToString()
            HdFileUploadViewAssign1.ShowCheck = False
        End If


    End Sub
End Class
