Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class HelpDesk_UserControls_hdViewMessage
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            HiddenMessageid.Value = Request.QueryString("messageid")
            HiddenEmpid.Value = Request.QueryString("toemp_id")
            Dim type = Request.QueryString("Type")

            If type = "Outbox" Then
                lblcaption.Text = "To"
                btnreply.Visible = False
            Else
                lblcaption.Text = "From"
            End If
           
            BindPriority()
            SetReadFlag()

        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub
    Public Sub SetReadFlag()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim sqlquery = "UPDATE  MESSAGE_LOG SET MESSAGE_READ='True' , MESSAGE_READ_DATE=GETDATE() WHERE MESSAGE_ID='" & HiddenMessageid.Value & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, sqlquery)

    End Sub
    Public Sub BindPriority()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EMP_ID", HiddenEmpid.Value)
        pParms(1) = New SqlClient.SqlParameter("@MESSAGE_ID", HiddenMessageid.Value)
        Dim ds As New DataSet
        Dim sp As String
        If Request.QueryString("Type") = "Outbox" Then
            sp = "GET_OUTBOX_DATA"
        Else
            sp = "GET_INBOX_DATA"
        End If

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, sp, pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            lblFrom.Text = ds.Tables(0).Rows(0).Item("EMPNAME").ToString()
            lblsubject.Text = ds.Tables(0).Rows(0).Item("MESSAGE_SUBJECT").ToString()
            lblDate.Text = ds.Tables(0).Rows(0).Item("ENTRY_DATE").ToString()
            CheckHighPriority.Checked = ds.Tables(0).Rows(0).Item("HIGH_PRIORITY").ToString()
            'txtdetails.Content = ds.Tables(0).Rows(0).Item("MESSAGE_TEXT").ToString()
            DataView.InnerHtml = ds.Tables(0).Rows(0).Item("MESSAGE_TEXT").ToString()
            If Request.QueryString("Type") <> "Outbox" Then
                btnreply.Attributes.Add("OnClick", ds.Tables(0).Rows(0).Item("SEND_MAIL").ToString())
            End If
            HdFileUploadViewAssign1.Uploadids = ds.Tables(0).Rows(0).Item("UPLOAD_IDS").ToString()
            CheckHighPriority.Enabled = False
        End If



    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim smScriptManager As New ScriptManager
        smScriptManager = Me.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
End Class
