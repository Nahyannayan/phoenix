<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdChangeOwner.ascx.vb" Inherits="HelpDesk_UserControls_hdChangeOwner" %>
&nbsp;

<div class="matters" align="center"  >


<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="300">
    <tr>
        <td class="subheader_img">
            Owner Reassign</td>
    </tr>
    <tr>
        <td align="left"  >
<asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
<asp:RadioButtonList ID="RadioOwners" runat="server">
</asp:RadioButtonList></td>
    </tr>
    <tr>
        <td align="center">
<asp:Button ID="btnupdate" runat="server" Width="100px" CssClass="button" Text="Update" />
            <asp:Button ID="btncancel" runat="server" Width="100px" OnClientClick="javascript:window.close(); return false;" CssClass="button" Text="Close" /></td>
    </tr>
</table>

<asp:HiddenField ID="HiddenEmpID" runat="server" />
<asp:HiddenField ID="HiddenTaskCategory" runat="server" />
<asp:HiddenField ID="HiddenTaskListID" runat="server" />
<asp:HiddenField ID="HiddenBsuid" runat="server" />
</div>