<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskHistory.ascx.vb"
    Inherits="HelpDesk_UserControls_hdTaskHistory" %>
<%@ Register Src="hdClientOwnerHistory.ascx" TagName="hdClientOwnerHistory" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<script type="text/javascript">
      function handleError() 
    {
      
	    return true;
    }
    window.onerror = handleError;
    
    
        function openimage(fname)
        {
       
        window.showModalDialog('hdImageView.aspx?filename=' + fname +'&Filecount='+ '1'  , '','dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

        }
        
         function ViewTask(assignId)
        {
      
        window.open('hdTaskAssignView.aspx?AssignedTaskid=' + assignId  , '','dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        }
    
      function ViewTaskStatus(assignId)
        {
       
        window.showModalDialog('hdStatusView.aspx?AssignedTaskid=' + assignId  , '','dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        }
        
       function SendMail(TaskAssignid,FromEmpId,ToEmpid,Parent_Message_Id)
       {
         if (document.getElementById('<%= HiddenUserType.clientID %>').value == 'OWNER_AGENT')
            {
            //Help Desk Agent Send Message
             FromEmpId=document.getElementById('<%= HiddenEmpID.clientID %>').value
             
            }
       
         window.showModalDialog('hdEnterMessage.aspx?TaskAssignid=' + TaskAssignid + '&FromEmpId='+ FromEmpId + '&ToEmpid=' + ToEmpid + '&Parent_Message_Id=' + Parent_Message_Id , '','dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;


       }
    
</script>


<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 

<script type="text/javascript">
   
    window.setTimeout('setpath()',100);
    
     function setpath()
        {
           var path = window.location.href
           var Rpath=path.substring(path.indexOf('?'),path.length)
           var objFrame
           
           //Trace Flow
           objFrame=document.getElementById("F1"); 
           objFrame.src="../Pages/hdTraceFlow.aspx" + Rpath
        }
   
</script>


   

     <div class="card mb-3">
   <div class="card-body">
            <div class="table-responsive">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
        <ajaxToolkit:TabPanel ID="T1" runat="server">
            <HeaderTemplate>
                Task History
            </HeaderTemplate>
            <ContentTemplate>
                <table  width="100%" align="center">
                    <tr>
                        <td >
                            Task History
                            <asp:CheckBox ID="CheckActiveMembers" runat="server" AutoPostBack="True" Checked="True"
                                Text="Active Members" /></td>
                    </tr>
                    <tr>
                        <td >
                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label><asp:GridView
                                ID="GridTaskStatus" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Members allocated for this task. (or) Search Query does not retrive any results."
                                AllowPaging="True" CssClass="table table-bordered table-row">
                                <Columns>

                                    <asp:TemplateField HeaderText="Assigned By">
                                        <HeaderTemplate>
                                                        Assigned By
                                                    
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("EMPASSIGNBY")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Assigned To">
                                        <HeaderTemplate>
                                            
                                                        Assigned To
                                                    
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("EMPASSIGNTO")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reassigned From">
                                        <HeaderTemplate>
                                                        Reassigned From
                                                    
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("RE_ASSIGNED_ID_FROM")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reassigned To">
                                        <HeaderTemplate>
                                                        Reassigned To
                                                    
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("RE_ASSIGNED_ID_TO")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Task Description">
                                        <HeaderTemplate>
                                                        Description
                                                    
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="LinkViewTask" OnClientClick='<%#Eval("VIEW_TASK")%>' runat="server">View</asp:LinkButton></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Start Date">
                                        <HeaderTemplate>
                                                        Start Date
                                                    
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%#Eval("TASK_ASSIGN_START_DATE") %>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Target Date">
                                        <HeaderTemplate>
                                                        Target Date
                                                    
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%#Eval("TASK_ASSIGN_END_DATE") %>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Priority">
                                        <HeaderTemplate>
                                                        Priority
                                            
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%#Eval("PRIORITY_DESCRIPTION")%>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Assign Date">
                                        <HeaderTemplate>
                                                        Assign Date
                                                    
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <%#Eval("ENTRY_DATE")%>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="History">
                                        <HeaderTemplate>
                                                        Progress
                                            
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:LinkButton ID="LinkView" runat="server" OnClientClick='<%#Eval("TASK_HISTORY")%>'>View</asp:LinkButton>
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Active">
                                        <HeaderTemplate>
                                                        Active
                                            
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                <asp:Image ID="Image1" ToolTip="Active Member" Visible='<%#Eval("ACTIVE")%>' ImageUrl="~/Images/tick.gif"
                                                    runat="server" />
                                                <asp:Image ID="Image2" ToolTip="Inactive Member" Visible='<%#Eval("Showcross")%>'
                                                    ImageUrl="~/Images/cross.png" runat="server" />
                                                <asp:ImageButton ID="ImageMessage" ToolTip="Send Mail" OnClientClick='<%#Eval("SEND_MAIL")%>'
                                                    ImageUrl="~/Images/Helpdesk/mail.png" runat="server" />
                                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                <RowStyle CssClass="griditem"  Wrap="False" />
                                
                                <EmptyDataRowStyle Wrap="False" />
                                <EditRowStyle Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="T2" Height="600px" runat="server">
            <HeaderTemplate>
                Client - Owner History
            </HeaderTemplate>
            <ContentTemplate>
                <uc1:hdClientOwnerHistory ID="HdClientOwnerHistory1" runat="server" />
                
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="T3" Height="1500px" runat="server">
            <HeaderTemplate>
                <a onclick="javascript:setpath();">Trace Flow</a>
            </HeaderTemplate>
            <ContentTemplate>
                <iframe id="F1" height="1500" scrolling="auto" marginwidth="0px" frameborder="0"
                    width="800"></iframe>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <br />
    <br />
    <asp:Button ID="btnwcancel" runat="server" CssClass="button" OnClientClick="javascript:window.close();"
        Text="Close" Width="100px" />
    <asp:HiddenField ID="HiddenSubTabID" runat="server" />
    <asp:HiddenField ID="HiddenEmpID" runat="server" />
    <asp:HiddenField ID="HiddenTaskCategory" runat="server" />
    <asp:HiddenField ID="HiddenTaskListID" runat="server" />
    <asp:HiddenField ID="HiddenBsuid" runat="server" />
    <asp:HiddenField ID="HiddenUserType" runat="server" />
</div>
       </div>
         </div>
