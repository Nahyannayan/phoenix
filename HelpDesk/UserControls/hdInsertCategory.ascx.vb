Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration

Partial Class HelpDesk_UserControls_hdInsertCategory
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
             BindCategory()
        End If

    End Sub

    Public Sub BindCategory()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = " SELECT * FROM dbo.TASK_CATEGORY "
        Dim category = ""
        If GridCategory.Rows.Count > 0 Then
            category = DirectCast(GridCategory.HeaderRow.FindControl("Txt1"), TextBox).Text.Trim()
            If category <> "" Then
                Sql_Query &= " Where replace(CATEGORY_DES,' ','') like '%" & category & "%' "
            End If
        End If

        Sql_Query &= " ORDER BY CATEGORY_DES "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("ID")
            dt.Columns.Add("CATEGORY_DES")
            Dim dr As DataRow = dt.NewRow()
            dr("ID") = ""
            dr("CATEGORY_DES") = ""
            dt.Rows.Add(dr)
            GridCategory.DataSource = dt
            GridCategory.DataBind()
        Else
            GridCategory.DataSource = ds
            GridCategory.DataBind()

        End If

        If GridCategory.Rows.Count > 0 Then
            DirectCast(GridCategory.HeaderRow.FindControl("Txt1"), TextBox).Text = category
        End If


    End Sub

    Protected Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadd.Click
        lblmessage.Text = ""
        If TextCategory.Text.Trim() <> "" Then
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()

            Try
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@CATEGORY_DES", TextCategory.Text.Trim())
                pParms(1) = New SqlClient.SqlParameter("@OPTION", 1)

                lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_TASK_CATEGORY", pParms)
                transaction.Commit()
            Catch ex As Exception
                transaction.Rollback()
                lblmessage.Text = ex.Message
            Finally
                connection.Close()

            End Try


        End If

    End Sub


    Protected Sub GridCategory_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridCategory.RowCancelingEdit
        GridCategory.EditIndex = -1
        BindCategory()
    End Sub

    Protected Sub GridCategory_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridCategory.RowCommand

        If e.CommandName = "deleting" Then

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblmessage.Text = ""
            Try
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ID", e.CommandArgument)
                pParms(1) = New SqlClient.SqlParameter("@OPTION", 3)

                lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_TASK_CATEGORY", pParms)
                transaction.Commit()
                BindCategory()
            Catch ex As Exception
                lblmessage.Text = "Error occurred while transactions." & ex.Message
                transaction.Rollback()

            Finally
                connection.Close()

            End Try

        ElseIf e.CommandName = "search" Then
            lblmessage.Text = ""
            BindCategory()

        End If

    End Sub

    Protected Sub GridCategory_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridCategory.RowEditing
        GridCategory.EditIndex = e.NewEditIndex
        BindCategory()
    End Sub

    Protected Sub GridCategory_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridCategory.RowUpdating

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblmessage.Text = ""
        Try
            Dim Id = DirectCast(GridCategory.Rows(e.RowIndex).FindControl("HiddenDId"), HiddenField).Value
            Dim category = DirectCast(GridCategory.Rows(e.RowIndex).FindControl("txtDataEdit"), TextBox).Text
          
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ID", Id)
            pParms(1) = New SqlClient.SqlParameter("@CATEGORY_DES", category)
            pParms(5) = New SqlClient.SqlParameter("@OPTION", 2)

            lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_TASK_CATEGORY", pParms)
            transaction.Commit()
            GridCategory.EditIndex = "-1"
            BindCategory()
        Catch ex As Exception
            lblmessage.Text = "Error occurred while transactions. " & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()

        End Try

    End Sub

    Protected Sub GridCategory_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GridCategory.PageIndex = e.NewPageIndex
        BindCategory()
    End Sub
End Class
