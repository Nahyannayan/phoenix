﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskCount.ascx.vb"  Inherits="HelpDesk_UserControls_hdTaskCount" %>
<div align="left">

<table >
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
            <asp:GridView ID="GridCount" runat="server" ShowHeader="false"  BorderStyle="None" BorderWidth="0"  AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateField HeaderText="Status">
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Status
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            
                              You have  &nbsp; <span style="color:Red" ><b> <%#Eval("COUNT_C")%></b></span> &nbsp;   <%#Eval("TASK_STATUS_DESCRIPTION")%> /s  &nbsp;
                           
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField>
                        <ItemTemplate>
                            <center>
                                <asp:Image ID="Image1" ImageUrl='<%#Eval("IMAGE_PATH")%>' runat="server" />
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
                <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <EditRowStyle Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
</table>
</div>