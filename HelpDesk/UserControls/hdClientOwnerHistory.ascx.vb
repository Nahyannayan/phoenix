Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class HelpDesk_UserControls_hdClientOwnerHistory
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenTaskListID.Value = Request.QueryString("task_list_id").ToString()
            BindLogger()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub BindLogger()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "select LOGGER,LOGGER_STATUS,ENTRY_NOTES, " & _
                        " REPLACE(CONVERT(VARCHAR(11), ENTRY_DATE, 106), ' ', '/') AS ENTRY_DATE , " & _
                        " isnull(emp_displayname,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_LNAME, '')) EMPNAME , " & _
                        " 'javascript:ViewCommmets('''+ CONVERT(VARCHAR,RECORD_ID) +''');return false;' VIEW_COMMENTS " & _
                        " from dbo.TASK_LOGGER_MASTER " & _
                        " LEFT JOIN OASIS.dbo.EMPLOYEE_M H ON H.EMP_ID = ENTRY_EMP_ID " & _
                        " where TASK_LIST_ID='" & HiddenTaskListID.Value & "' ORDER BY RECORD_ID DESC "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        GridClientHistory.DataSource = ds
        GridClientHistory.DataBind()
    End Sub

    Protected Sub GridClientHistory_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridClientHistory.PageIndexChanging
        GridClientHistory.PageIndex = e.NewPageIndex
        BindLogger()
    End Sub
End Class
