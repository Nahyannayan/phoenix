﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class HelpDesk_UserControls_hdTaskMonitor
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.QueryString("tmsection") = 0 Then


            BindPriority()
            BindStatus()
            BindJobCategopry()
            SearchTaskList()
            BindDrops()
            cpindex.Value = 1

        Else

            If Request.QueryString("cpindex") <> Request.QueryString("tpage") Then
                cpindex.Value = Convert.ToInt16(Request.QueryString("cpindex")) + 1
                GridTaskList.PageIndex = Request.QueryString("cpindex")
            Else
                cpindex.Value = 0
                GridTaskList.PageIndex = Request.QueryString("cpindex")
            End If
            SearchTaskList()


        End If


    End Sub

    Public Sub BindDrops()

        '' Reported by
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = " SELECT DISTINCT REPORTED_EMP_ID AS EMP_ID , isnull(emp_displayname,ISNULL(C.EMP_FNAME, '') + ' ' + ISNULL(C.EMP_LNAME, ''))  AS EMPNAME ,emp_displayname " & _
                        " FROM  OASIS.dbo.EMPLOYEE_M C INNER JOIN dbo.TASK_CONTACT_MASTER CC ON CC.REPORTED_EMP_ID=C.EMP_ID ORDER BY emp_displayname "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddreportedby.DataSource = ds
        ddreportedby.DataTextField = "EMPNAME"
        ddreportedby.DataValueField = "EMP_ID"
        ddreportedby.DataBind()

        Dim list As New ListItem
        list.Text = "--"
        list.Value = "-1"
        ddreportedby.Items.Insert(0, list)


        '' Claimed by
        Sql_Query = " SELECT DISTINCT CLAIM_EMP_ID AS EMP_ID  , isnull(emp_displayname,ISNULL(C.EMP_FNAME, '') + ' ' + ISNULL(C.EMP_LNAME, ''))  AS EMPNAME ,emp_displayname " & _
                    " FROM  OASIS.dbo.EMPLOYEE_M C INNER JOIN dbo.TASK_LIST_MASTER CC ON CC.CLAIM_EMP_ID=C.EMP_ID ORDER BY emp_displayname "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddclaimedby.DataSource = ds
        ddclaimedby.DataTextField = "EMPNAME"
        ddclaimedby.DataValueField = "EMP_ID"
        ddclaimedby.DataBind()
        ddclaimedby.Items.Insert(0, list)


        ''Assigned by

        Sql_Query = " SELECT DISTINCT TASK_ASSIGNED_BY_EMP_ID AS EMP_ID  , isnull(emp_displayname,ISNULL(C.EMP_FNAME, '') + ' ' + ISNULL(C.EMP_LNAME, ''))  AS EMPNAME ,emp_displayname " & _
                   " FROM  OASIS.dbo.EMPLOYEE_M C INNER JOIN dbo.TASK_ASSIGNED_LOG CC ON CC.TASK_ASSIGNED_BY_EMP_ID=C.EMP_ID ORDER BY emp_displayname "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddassignedby.DataSource = ds
        ddassignedby.DataTextField = "EMPNAME"
        ddassignedby.DataValueField = "EMP_ID"
        ddassignedby.DataBind()
        ddassignedby.Items.Insert(0, list)


        ''Assigned to

        Sql_Query = " SELECT DISTINCT TASK_ASSIGNED_TO_EMP_ID AS EMP_ID  , isnull(emp_displayname,ISNULL(C.EMP_FNAME, '') + ' ' + ISNULL(C.EMP_LNAME, ''))  AS EMPNAME ,emp_displayname " & _
                   " FROM  OASIS.dbo.EMPLOYEE_M C INNER JOIN dbo.TASK_ASSIGNED_LOG CC ON CC.TASK_ASSIGNED_TO_EMP_ID=C.EMP_ID ORDER BY emp_displayname "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddassignedto.DataSource = ds
        ddassignedto.DataTextField = "EMPNAME"
        ddassignedto.DataValueField = "EMP_ID"
        ddassignedto.DataBind()

        ddassignedto.Items.Insert(0, list)





    End Sub

    Public Sub BindPriority()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT * FROM TASK_PRIORITY_MASTER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddpriority.DataSource = ds
        ddpriority.DataTextField = "PRIORITY_DESCRIPTION"
        ddpriority.DataValueField = "RECORD_ID"
        ddpriority.DataBind()

        Dim list As New ListItem
        list.Text = "Select Priority"
        list.Value = "-1"
        ddpriority.Items.Insert(0, list)



    End Sub


    Public Sub BindStatus()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT * FROM TASK_STATUS_MASTER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddstatus.DataSource = ds
        ddstatus.DataTextField = "TASK_STATUS_DESCRIPTION"
        ddstatus.DataValueField = "STATUS_ID"
        ddstatus.DataBind()

        Dim list As New ListItem
        list.Text = "Select Status"
        list.Value = "-1"
        ddstatus.Items.Insert(0, list)

    End Sub

    Public Sub BindJobCategopry()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "SELECT DISTINCT A.TASK_CATEGORY_ID,( CATEGORY_DES+ ' - (' + MAIN_TAB_DESCRIPTION + '/'+ SUB_TAB_DESCRIPTION + ')' ) TASK_CATEGORY_DESCRIPTION,CATEGORY_DES AS CD " & _
                        " FROM dbo.TASK_CATEGORY_MASTER A  " & _
                        " INNER JOIN dbo.TASK_ROOTING_MASTER B  ON A.TASK_CATEGORY_ID=B.TASK_CATEGORY_ID   " & _
                        " INNER JOIN TASK_CATEGORY B1 ON A.CATEGORY_ID=B1.ID" & _
                        " INNER JOIN dbo.SUB_TAB_MASTER C ON C.SUB_TAB_ID = A.SUB_TAB_ID " & _
                        " INNER JOIN dbo.MAIN_TAB_MASTER D ON D.MAIN_TAB_ID = C.MAIN_TAB_ID " & _
                        " WHERE  B.TASK_CATEGORY_OWNER = 'TRUE' " & _
                        " ORDER BY CD DESC "

        ''Bind Job Source
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddTaskCategory.DataSource = ds
        ddTaskCategory.DataTextField = "TASK_CATEGORY_DESCRIPTION"
        ddTaskCategory.DataValueField = "TASK_CATEGORY_ID"
        ddTaskCategory.DataBind()

        Dim list As New ListItem
        list.Text = "Task Category"
        list.Value = "-1"
        ddTaskCategory.Items.Insert(0, list)

    End Sub

    Protected Sub ddTaskCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddTaskCategory.SelectedIndexChanged
        lblmessage.Text = ""
        SearchTaskList()

    End Sub

    Public Sub SearchTaskList()
        Try

     
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim pParms(13) As SqlClient.SqlParameter

            If ddTaskCategory.SelectedIndex > 0 Then
                pParms(0) = New SqlClient.SqlParameter("@TASK_CATEGORY", ddTaskCategory.SelectedValue)
            End If

            If txttasklistid.Text.Trim() <> "" Then
                pParms(1) = New SqlClient.SqlParameter("@TASK_LIST_ID", txttasklistid.Text.Trim())
            End If

            If txtreqdate.Text.Trim() <> "" Then
                pParms(2) = New SqlClient.SqlParameter("@TASK_TRAGET_DATE", txtreqdate.Text.Trim())
            End If

            If ddpriority.SelectedIndex > 0 Then
                pParms(3) = New SqlClient.SqlParameter("@TASK_PRIORITY", ddpriority.SelectedValue)
            End If

            If ddstatus.SelectedIndex > 0 Then
                pParms(4) = New SqlClient.SqlParameter("@TASK_STATUS", ddstatus.SelectedValue)
            End If

            If txtentrydate.Text.Trim() <> "" Then
                pParms(5) = New SqlClient.SqlParameter("@TASK_DATE_TIME", txtentrydate.Text.Trim())
            End If

            If txtkeyword.Text.Trim() <> "" Then
                pParms(6) = New SqlClient.SqlParameter("@KEYWORD", txtkeyword.Text.Trim())
            End If

            If ddreportedby.SelectedIndex > 0 Then
                pParms(7) = New SqlClient.SqlParameter("@REPORTED_EMP_ID", ddreportedby.SelectedValue)
            End If

            If ddclaimedby.SelectedIndex > 0 Then
                pParms(8) = New SqlClient.SqlParameter("@CLAIM_EMP_ID", ddclaimedby.SelectedValue)
            End If

            If ddassignedby.SelectedIndex > 0 Then
                pParms(9) = New SqlClient.SqlParameter("@TASK_ASSIGNED_BY_EMP_ID", ddassignedby.SelectedValue)
            End If

            If ddassignedto.SelectedIndex > 0 Then
                pParms(10) = New SqlClient.SqlParameter("@TASK_ASSIGNED_TO_EMP_ID", ddassignedto.SelectedValue)
            End If

            If Request.QueryString("sBusper") = "False" Then
                pParms(11) = New SqlClient.SqlParameter("@OWNER_EMP_ID", Request.QueryString("EmployeeId"))
            End If

            pParms(12) = New SqlClient.SqlParameter("@ONLY_ASSIGNED", "True")

            Dim ds As DataSet = Nothing

            
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TASK_SEARCH_FOR_TASK_MONITOR", pParms)


            GridTaskList.DataSource = ds
            GridTaskList.DataBind()

            Dim pval As Double = ds.Tables(0).Rows.Count / GridTaskList.PageSize
            Dim pfval As Integer = 1
            If pval.ToString.Contains(".") Then

                If pval.ToString.Split(".")(1) > 0 Then
                    pfval = pval.ToString.Split(".")(0) + 1
                Else
                    pfval = pval.ToString.Split(".")(0)
                End If

            Else
                pfval = pval
            End If

            tpage.Value = pfval

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        lblmessage.Text = ""
        SearchTaskList()

    End Sub

    Protected Sub GridTaskList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridTaskList.PageIndexChanging
        GridTaskList.PageIndex = e.NewPageIndex
        SearchTaskList()

    End Sub


    Protected Sub ddpriority_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddpriority.SelectedIndexChanged
        lblmessage.Text = ""
        SearchTaskList()
    End Sub

    Protected Sub ddstatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddstatus.SelectedIndexChanged
        lblmessage.Text = ""
        SearchTaskList()
    End Sub


End Class
