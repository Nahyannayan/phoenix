Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Partial Class HelpDesk_UserControls_AddUserCategories
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenEmpid.Value = Request.QueryString("Emp_id")
            HiddenBsuid.Value = Session("sbsuid")
            BindMainTabs()
        End If

    End Sub
    Public Sub BindMainTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select * from  MAIN_TAB_MASTER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddMaintab.DataSource = ds
        ddMaintab.DataTextField = "MAIN_TAB_DESCRIPTION"
        ddMaintab.DataValueField = "MAIN_TAB_ID"
        ddMaintab.DataBind()


        BindSubTabs()


    End Sub
    Public Sub BindSubTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select * from  SUB_TAB_MASTER where MAIN_TAB_ID='" & ddMaintab.SelectedValue & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddsubtab.DataSource = ds
        ddsubtab.DataTextField = "SUB_TAB_DESCRIPTION"
        ddsubtab.DataValueField = "SUB_TAB_ID"
        ddsubtab.DataBind()

        BindJobCategory()
    End Sub

   

    Protected Sub ddsubtab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsubtab.SelectedIndexChanged
        lblmessage.Text = ""
        BindJobCategory()
    End Sub

    Protected Sub ddMaintab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddMaintab.SelectedIndexChanged
        BindSubTabs()
    End Sub

    Public Sub BindJobCategory()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim sql_query = " SELECT A.TASK_CATEGORY_ID,TASK_CATEGORY_DESCRIPTION,CASE ISNULL(EMP_ID,'') WHEN ''  THEN 'False' ELSE 'True' END CHECKED FROM  " & _
                        " (select DISTINCT  C.TASK_CATEGORY_ID,CATEGORY_DES AS TASK_CATEGORY_DESCRIPTION from dbo.SUB_TAB_MASTER A " & _
                        " INNER JOIN dbo.MAIN_TAB_MASTER B ON A.MAIN_TAB_ID= B.MAIN_TAB_ID " & _
                        " INNER JOIN dbo.TASK_CATEGORY_MASTER C ON C.SUB_TAB_ID = A.SUB_TAB_ID " & _
                        " INNER JOIN dbo.TASK_ROOTING_MASTER D ON A.MAIN_TAB_ID =D.MAIN_TAB_ID " & _
                        " INNER JOIN  TASK_CATEGORY D1 ON D1.ID=C.CATEGORY_ID " & _
                        " AND C.SUB_TAB_ID=D.SUB_TAB_ID AND C.TASK_CATEGORY_ID=D.TASK_CATEGORY_ID " & _
                        " WHERE TASK_CATEGORY_OWNER='True' AND " & _
                        " D.MAIN_TAB_ID ='" & ddMaintab.SelectedValue & "' AND " & _
                        " D.SUB_TAB_ID='" & ddsubtab.SelectedValue & "' )A " & _
                        " LEFT JOIN dbo.USER_TASK_CATEGORY E ON E.TASK_CATEGORY_ID=A.TASK_CATEGORY_ID AND EMP_ID='" & HiddenEmpid.Value & "' "
        Dim txtCategory As String
        If GrdCategory.Rows.Count > 0 Then
            txtCategory = DirectCast(GrdCategory.HeaderRow.FindControl("txtCategory"), TextBox).Text.Trim()
            If txtCategory.Trim() <> "" Then
                sql_query &= " Where CATEGORY_DES like '%" & txtCategory & "%' "
            End If
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
            If ds.Tables(0).Rows.Count = 0 Then
                Dim dt As New DataTable
                dt.Columns.Add("TASK_CATEGORY_ID")
                dt.Columns.Add("TASK_CATEGORY_DESCRIPTION")
                dt.Columns.Add("CHECKED")
                Dim dr As DataRow = dt.NewRow()
                dr("TASK_CATEGORY_ID") = ""
                dr("TASK_CATEGORY_DESCRIPTION") = ""
                dr("CHECKED") = "False"
            

                dt.Rows.Add(dr)
                GrdCategory.DataSource = dt
                GrdCategory.DataBind()
                btnsave.Visible = False
            Else
                GrdCategory.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
                GrdCategory.DataBind()
                btnsave.Visible = True
            End If
        Else
            GrdCategory.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
            GrdCategory.DataBind()
            btnsave.Visible = True

        End If

        If GrdCategory.Rows.Count > 0 Then

            DirectCast(GrdCategory.HeaderRow.FindControl("txtCategory"), TextBox).Text = txtCategory
        
        End If


    End Sub
    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click


        ''Delete the previous Categories
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()

        Try
            Dim sql_query = "DELETE USER_TASK_CATEGORY WHERE EMP_ID='" & HiddenEmpid.Value & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)

            For Each row As GridViewRow In GrdCategory.Rows
                Dim check As CheckBox = DirectCast(row.FindControl("Check"), CheckBox)
                If check.Checked Then

                    Dim Categoryid = DirectCast(row.FindControl("HiddenTaskCategory"), HiddenField).Value
                    Update(Categoryid, check.Checked, transaction)

                End If

            Next
            transaction.Commit()

            BindJobCategory()
            lblmessage.Text = "User Task Updated Successfully."
        Catch ex As Exception
            lblmessage.Text = "Error while transaction. Error :" & ex.Message
            transaction.Rollback()
        Finally
            connection.Close()
        End Try

       
      
    End Sub
  
    Public Sub Update(ByVal CategoryId As String, ByVal check As Boolean, ByVal transaction As SqlTransaction)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EMP_ID", HiddenEmpid.Value)
        pParms(1) = New SqlClient.SqlParameter("@TASK_CATEGORY_ID", CategoryId)
        pParms(2) = New SqlClient.SqlParameter("@CHECKED", check)

        SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_DELETE_USER_TASK_CATEGORY", pParms)

    End Sub


    Protected Sub GrdCategory_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdCategory.RowCommand

        If e.CommandName = "search" Then
            BindJobCategory()
        End If

    End Sub
End Class
