<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdViewMessage.ascx.vb"
    Inherits="HelpDesk_UserControls_hdViewMessage" %>
<%@ Register Src="hdFileUploadViewAssign.ascx" TagName="hdFileUploadViewAssign" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<script type="text/javascript">

function SendMail(TaskAssignid,FromEmpId,ToEmpid,Parent_Message_Id)
       {
       
         window.showModalDialog('hdEnterMessage.aspx?TaskAssignid=' + TaskAssignid + '&FromEmpId='+ FromEmpId + '&ToEmpid=' + ToEmpid + '&Parent_Message_Id=' + Parent_Message_Id , '','dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;


       }

</script>

<div align="center">
    <br />

    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="600">
                <tr>
                    <td class="subheader_img">
                        Message</td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="400px">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblcaption" runat="server"></asp:Label></td>
                                    <td>
                                        :</td>
                                    <td>
                                        <asp:Label ID="lblFrom" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                        Subject</td>
                                    <td>
                                        :</td>
                                    <td>
                                        <asp:Label ID="lblsubject" runat="server" Width="394px"></asp:Label>
                                        <asp:CheckBox ID="CheckHighPriority" runat="server" Text="High Priority" /></td>
                                </tr>
                                <tr>
                                    <td>
                                        Date</td>
                                    <td>
                                        :</td>
                                    <td>
                                        <asp:Label ID="lblDate" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <uc1:hdFileUploadViewAssign ID="HdFileUploadViewAssign1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 10px; background-color: silver">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" rowspan="2" id="DataView" align="left" runat="server">
                                        <%--   <telerik:RadEditor ID="txtdetails" runat="server" EditModes="Design" ToolsFile="../xml/NoTools.xml"
                Width="600px">
            </telerik:RadEditor>--%>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btnreply" runat="server" Text="Reply" CssClass="button" Width="80px" />
                        <asp:Button ID="btncancel" runat="server" CssClass="button" OnClientClick="javascript:window.close();"
                            Text="Close" ValidationGroup="s" Width="80px" /></td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    &nbsp; &nbsp; &nbsp; &nbsp;<asp:HiddenField ID="HiddenMessageid" runat="server" />
    <asp:HiddenField ID="HiddenEmpid" runat="server" />
</div>
