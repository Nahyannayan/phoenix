<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddUserCategories.ascx.vb" Inherits="HelpDesk_UserControls_AddUserCategories" %>
 
<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
    <tr>
        <td class="subheader_img">
            User Task Categories</td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        Main Tab</td>
                    <td>
                        <asp:DropDownList ID="ddMaintab" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        Sub Tab</td>
                    <td>
                        <asp:DropDownList ID="ddsubtab" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="GrdCategory" runat="server" AutoGenerateColumns="False" EmptyDataText="No record found for selected search" >
    <Columns>
        <asp:TemplateField HeaderText="Task Category">
            <HeaderTemplate>
                <table class="BlueTable" width="100%">
                    <tr class="matterswhite">
                        <td align="center" colspan="2">
                            Task Category
                            <br />
                            <asp:TextBox ID="txtCategory" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageSearch1" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:HiddenField ID="HiddenTaskCategory" runat="server" Value='<%#Eval("TASK_CATEGORY_ID")%>' />
               <%#Eval("TASK_CATEGORY_DESCRIPTION")%>
               
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Check">
            <HeaderTemplate>
                <table class="BlueTable" width="100%">
                    <tr class="matterswhite">
                        <td align="center" colspan="2">
                            <br />
                            Check
                            <br />
                            <br />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <br />
                    <asp:CheckBox ID="Check" Checked='<%#Eval("CHECKED")%>' runat="server" />
                </center>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
    <EmptyDataRowStyle Wrap="False" />
    <SelectedRowStyle CssClass="Green" Wrap="False" />
    <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
    <EditRowStyle Wrap="False" />
    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
</asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" Width="75px" /></td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenEmpid" runat="server" />
<asp:HiddenField ID="HiddenBsuid" runat="server" />
