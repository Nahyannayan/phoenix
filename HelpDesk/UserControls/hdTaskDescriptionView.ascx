<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskDescriptionView.ascx.vb" Inherits="HelpDesk_UserControls_hdTaskDescriptionView" %>
<%@ Register Src="hdFileUploadViewAssign.ascx" TagName="hdFileUploadViewAssign" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Task Description
        </div>
        <div class="card-body">
            <div class="table-responsive">


<br />


     <table  cellpadding="5" cellspacing="0" width="100%">
           
            <tr>
                <td align="left"  >
                 <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="100%" Height="400px">
                     <table width="100%">
                                <tr align="left">
                                    <td runat="server" align="left" colspan="3" style="background-color:ActiveBorder; height:20px">
                                        <asp:Label ID="lbltitle" runat="server"></asp:Label></td>
                                </tr>
                                <tr align="left" >
                                    <td colspan="3" id="DataView" align="left" runat="server">
                                      <%--  <telerik:RadEditor ID="txtdetails" runat="server" EditModes="Design" ToolsFile="../xml/NoTools.xml"
                                            Width="600px">
                                        </telerik:RadEditor>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="3">
                                        <uc1:hdFileUploadViewAssign ID="HdFileUploadViewAssign1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                 
                 </asp:Panel>
                        
                   
                </td>
            </tr>
         <tr>
             <td align="center">
                                        <asp:Button ID="btncancel" runat="server" CssClass="button" 
                                            OnClientClick="javascript:window.close();" Text="OK" ValidationGroup="s"
                                            Width="80px" /></td>
         </tr>
        </table>
</div>
            </div></div>
            <asp:HiddenField ID="HiddenEmpID" runat="server" />
                            <asp:HiddenField ID="HiddenTaskListId" runat="server" />