﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskRootingMasterV2.ascx.vb"
    Inherits="HelpDesk_UserControls_hdTaskRootingMasterV2" %>
<script type="text/javascript">
    function change_chk_stateg(chkThis, cvalu) {
        var vals = chkThis.id.split('_');
        var pserch = vals[2] + '_' + vals[3];
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(cvalu) != -1 && currentid.indexOf(pserch) != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }

    function ViewAssigned(main_tab, sub_tab, bsu_id, cat_id) {

        window.open('hdViewOwnerMembers.aspx?main_tab=' + main_tab + "&sub_tab=" + sub_tab + "&cat_id=" + cat_id + "&bsu_id=" + bsu_id, '', 'Height=650px,Width=750px,scrollbars=yes,resizable=no,directories=yes');
        return false;
    }
    
</script>
<div class="matters">
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="100%">
        <tr>
            <td class="subheader_img">
                Task Routing / Email / SMS Notification Master
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            Main Tab
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddMainTab" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Departments
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddSubTab" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Category</td>
                        <td>
                            :</td>
                        <td>
                <asp:DropDownList ID="ddsearchcategory" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                <br />
                <asp:GridView ID="GridInfo" runat="server" AllowPaging="True" PageSize="2" ShowFooter="true"
                    AutoGenerateColumns="false" EmptyDataText="Information not available." Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Involved
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:Image ID="Image1" ImageUrl='<%#Eval("Involved")%>' runat="server" />
                                </center>
                            </ItemTemplate>
                            <ItemStyle Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Category
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("CATEGORY_DES")%>
                                <asp:HiddenField ID="Hiddencatyid" Value='<%#Eval("TASK_CATEGORY_ID")%>' runat="server" />
                            </ItemTemplate>
                            <ItemStyle Width="120px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Settings
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div style="height: 300px; width: 100%; overflow: auto">
                                    <asp:GridView ID="GridRootingBsu" AutoGenerateColumns="false" EnableTheming="false"
                                        Width="95%" runat="server">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Business Unit">
                                                <HeaderTemplate>
                                                    <span style="font-size: small">Business Unit</span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <span style="font-size: small">
                                                        <%#Eval("BSU_NAME")%></span>
                                                    <asp:HiddenField ID="HiddenBsuid" Value='<%#Eval("BSU_ID") %>' runat="server" />
                                                </ItemTemplate>
                                                <%-- <ItemStyle Width="200px" />--%>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Owner">
                                                <HeaderTemplate>
                                                    <center>
                                                        <span style="font-size: small">Owner</span>
                                                        <br />
                                                        <asp:CheckBox ID="chkAll1" runat="server" onclick="javascript:change_chk_stateg(this,'CheckOwner');"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </center>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <asp:CheckBox ID="CheckOwner" Checked='<%#Eval("TASK_CATEGORY_OWNER") %>' runat="server" /></center>
                                                </ItemTemplate>
                                                <ItemStyle Width="60px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Member">
                                                <HeaderTemplate>
                                                    <center>
                                                        <span style="font-size: small">Member</span>
                                                        <br />
                                                        <asp:CheckBox ID="chkAll2" runat="server" onclick="javascript:change_chk_stateg(this,'CheckMember');"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </center>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <asp:CheckBox ID="CheckMember" Checked='<%#Eval("TASK_MEMBER") %>' runat="server" /></center>
                                                </ItemTemplate>
                                                <ItemStyle Width="70px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email">
                                                <HeaderTemplate>
                                                    <center>
                                                        <span style="font-size: small">Email</span>
                                                        <br />
                                                        <asp:CheckBox ID="chkAll3" runat="server" onclick="javascript:change_chk_stateg(this,'CheckEmail');"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </center>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <asp:CheckBox ID="CheckEmail" Checked='<%#Eval("EMAIL_NOTIFY") %>' runat="server" /></center>
                                                </ItemTemplate>
                                                <ItemStyle Width="60px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SMS">
                                                <HeaderTemplate>
                                                    <center>
                                                        <span style="font-size: small">SMS</span>
                                                        <br />
                                                        <asp:CheckBox ID="chkAll4" runat="server" onclick="javascript:change_chk_stateg(this,'CheckSMS');"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </center>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <asp:CheckBox ID="CheckSMS" Checked='<%#Eval("SMS_NOTIFY") %>' runat="server" /></center>
                                                </ItemTemplate>
                                                <ItemStyle Width="60px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Reassign">
                                                <HeaderTemplate>
                                                    <center>
                                                        <span style="font-size: small">Reassign</span>
                                                        <br />
                                                        <asp:CheckBox ID="chkAll5" runat="server" onclick="javascript:change_chk_stateg(this,'CheckReassign');"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </center>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <asp:CheckBox ID="CheckReassign" Checked='<%#Eval("CAN_RESIGN") %>' runat="server" /></center>
                                                </ItemTemplate>
                                                <ItemStyle Width="70px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="View">
                                            <HeaderTemplate>
                                            <center>View <br /></center>
                                            </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <asp:ImageButton ID="ImageReAssign" OnClientClick='<%#Eval("VIEW_ASSIGNED")%>' ToolTip="View other Owners and Members."
                                                            Width="20px" Height="20px" ImageUrl="~/Images/Helpdesk/Reassign.png" runat="server" />
                                                    </center>
                                                </ItemTemplate>
                                                <ItemStyle Width="60px" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle Height="20px" CssClass="gridheader_pop" Font-Size="Small" Wrap="False" />
                                        <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        <EmptyDataRowStyle Wrap="False" />
                                        <EditRowStyle Wrap="False" />
                                    </asp:GridView>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                <center>
                                    <asp:Button ID="btnSave" runat="server" CssClass="button" OnClick="SaveGrid" Text="Save"
                                        Width="100px" />
                                </center>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</div>
<asp:HiddenField ID="HiddenEmp_id" runat="server" Value="0" />
