Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI
Imports System.Data.SqlClient

Partial Class HelpDesk_UserControls_hdInbox
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenEmpid.Value = Session("EmployeeId")
            BindInbox()
        End If

    End Sub

    Public Sub BindInbox()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EMP_ID", HiddenEmpid.Value)

        Dim ds As New DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_INBOX_DATA", pParms)
        GridInbox.DataSource = ds
        GridInbox.DataBind()


    End Sub

    Protected Sub GridInbox_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInbox.PageIndexChanging

        GridInbox.PageIndex = e.NewPageIndex
        BindInbox()

    End Sub
End Class
