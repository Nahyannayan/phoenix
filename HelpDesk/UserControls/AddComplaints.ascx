<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddComplaints.ascx.vb" Inherits="HelpDesk_UserControls_AddComplaints" %>
<%@ Register Src="hdFileUpload.ascx" TagName="hdFileUpload" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<script type="text/javascript" >

function GetDateFunction(dateval)
{

var mySplitResult = dateval.split("/");
var result = 'unknown';

        switch (mySplitResult[1]) 
        {
        case 'Jan': result = '01'; break;
        case 'Feb': result = '02'; break;
        case 'Mar': result = '03'; break;
        case 'Apr': result = '04'; break;
        case 'May': result = '05'; break;
        case 'Jun': result = '06'; break;
        case 'Jul': result = '07'; break;
        case 'Aug': result = '08'; break;
        case 'Sep': result = '09'; break;
        case 'Oct': result = '10'; break;
        case 'Nov': result = '11'; break;
        case 'Dec': result = '12'; break;
        default: result = 'unknown';
        }


       if (result != 'unknown') 
       {
       
       result = mySplitResult[0] + '/' + result + '/' + mySplitResult[2] ;
       
       }
       else
       {
       alert('Error in Date');
       }
    return result
}


function ValidateDate()
{
var returnval= true
if (document.getElementById('<% = txtreqdate.ClientID %>').value != '')
{
     var SDate = new Date();    	   
     var EDate =  GetDateFunction(document.getElementById('<% = txtreqdate.ClientID %>').value);

     var endDate = new Date(EDate);    	    
     var startDate= new Date(SDate);
     
     
     if (endDate >  startDate)
     {
      
      returnval =  true;
     }
     else
     {
      alert('Required Date must be greater then Current Date');
      document.getElementById('<% = txtreqdate.ClientID %>').focus();
      returnval = false;
     }
}
   return  returnval
   
}


</script>
<div class="matters" align="center" >

<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
    <tr>
        <td align="right" >
            <asp:LinkButton ID="lnkmycomplanints" runat="server" EnableTheming="false" Font-Size="Large">My Issues</asp:LinkButton>
        </td>
    </tr>
    <tr>
        <td class="subheader_img">
            Task Information</td>
    </tr>
    <tr>
        <td align="left" >
            <table>
                <tr>
                    <td>
                        Title</td>
                    <td>
                        :</td>
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server" Width="446px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        Task&nbsp;Category</td>
                    <td>
                        :</td>
                    <td>
                        <asp:DropDownList ID="ddCategory" runat="server">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        Priority</td>
                    <td>
                        :</td>
                    <td>
                        <asp:DropDownList ID="ddpriority" runat="server">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        Required&nbsp;Date</td>
                    <td>
                        :</td>
                    <td>
                        <asp:TextBox ID="txtreqdate" runat="server"></asp:TextBox>
                        <asp:Image ID="Image1" runat="server" Visible="false"  ImageUrl="~/Images/calendar.gif" /></td>
                </tr>
                <tr>
                    <td colspan="3">
                        Task&nbsp;Description&nbsp;and&nbsp;Notes</td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <telerik:RadEditor ID="txtjobdes" runat="server" EditModes="Design" ToolsFile="xml/FullSetOfTools.xml"
                            Width="600px">
                        </telerik:RadEditor>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        Attachments</td>
                    <td>
                        :</td>
                    <td>
                        <uc1:hdFileUpload ID="HdFileUpload1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <asp:Button ID="btnsave" runat="server" CssClass="button" 
                            Text="Save" ValidationGroup="Save" Width="100px" />
                        <br />
                        <br />
                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenEmpId" runat="server" />
    <asp:HiddenField ID="HiddenBsuid" runat="server" />
    <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtreqdate"
        TargetControlID="txtreqdate">
    </ajaxToolkit:CalendarExtender>
</div>
<asp:RequiredFieldValidator
        ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTitle" Display="None"
        ErrorMessage="Please Enter Title" SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddCategory"
    Display="None" ErrorMessage="Please Select Task Category" InitialValue="0" SetFocusOnError="True"
    ValidationGroup="Save"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator
            ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtjobdes" Display="None"
            ErrorMessage="Please Enter Job Description" SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>&nbsp;<asp:ValidationSummary
                ID="ValidationSummary2" runat="server" ShowMessageBox="True" ShowSummary="False"
                ValidationGroup="Save" />
