Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class HelpDesk_UserControls_hdTaskRootingMaster
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            PLoad()
        End If

    End Sub

    Public Sub PLoad()
        HiddenEmp_id.Value = Request.QueryString("Emp_id").Trim()
        BindMainTab()


    End Sub


    Public Sub BindTreeUserData()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = " SELECT DISTINCT TASK_CATEGORY_ID FROM dbo.TASK_ROOTING_MASTER " & _
                        " WHERE MAIN_TAB_ID='" & ddMainTab.SelectedValue & "' AND  SUB_TAB_ID='" & ddSubTab.SelectedValue & "' AND EMP_ID='" & HiddenEmp_id.Value & "' "
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        Dim i = 0

        For i = 0 To ds.Tables(0).Rows.Count - 1

            Dim Aval = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString()

            For Each node As TreeNode In TreeJobCategory.Nodes

                If node.Value = Aval Then
                    node.Checked = True
                Else
                    SetNodeValue(node, Aval)
                End If

            Next

        Next


        Panel1.Visible = False
        btnassign.Visible = False
        btnEdit.Visible = True
        btnSave.Visible = False
    End Sub

    Public Function SetNodeValue(ByVal ParentNode As TreeNode, ByVal Aval As String) As String
        Dim val = ""

        For Each node As TreeNode In ParentNode.ChildNodes
            If node.Value = Aval Then

                node.Checked = True

            Else
                SetNodeValue(node, Aval)

            End If

        Next

        Return val

    End Function

    Public Sub BindMainTab()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select MAIN_TAB_ID,MAIN_TAB_DESCRIPTION from  MAIN_TAB_MASTER WHERE ACTIVE='True'"
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddMainTab.DataSource = ds
        ddMainTab.DataTextField = "MAIN_TAB_DESCRIPTION"
        ddMainTab.DataValueField = "MAIN_TAB_ID"
        ddMainTab.DataBind()
       
        BindSubTabs()


    End Sub

    Public Sub BindSubTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select SUB_TAB_ID,SUB_TAB_DESCRIPTION from  SUB_TAB_MASTER where MAIN_TAB_ID='" & ddMainTab.SelectedValue & "' AND Active='True'"
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddSubTab.DataSource = ds
        ddSubTab.DataTextField = "SUB_TAB_DESCRIPTION"
        ddSubTab.DataValueField = "SUB_TAB_ID"
        ddSubTab.DataBind()
        btnassign.Visible = False
        BindJobCategory()

    End Sub

    Public Function JGetNodeValue(ByVal ParentNode As TreeNode) As String
        Dim val = ""

        For Each node As TreeNode In ParentNode.ChildNodes
            If node.Checked Then
                val = node.Value

                Exit For
            Else
                val = GetNodeValue(node)

                If val <> "" Then
                    Exit For
                End If

            End If

        Next

        Return val

    End Function

    Public Sub BindRootingEmployeese()


        Dim Jval = ""
        For Each node As TreeNode In TreeJobCategory.Nodes
            If node.Checked Then
                Jval = node.Value

                Exit For
            Else
                Jval = JGetNodeValue(node)
                If Jval <> "" Then
                    Exit For
                End If
            End If
        Next


        If Jval <> "" Then

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

            Dim Sql_Query = " SELECT BSU_ID,BSU_NAME,ISNULL(TASK_CATEGORY_OWNER,'False')TASK_CATEGORY_OWNER,ISNULL(TASK_MEMBER,'False')TASK_MEMBER,ISNULL(EMAIL_NOTIFY,'False')EMAIL_NOTIFY,ISNULL(SMS_NOTIFY,'False')SMS_NOTIFY,ISNULL(CAN_RESIGN,'False')CAN_RESIGN ,'javascript:ViewAssigned(''' + convert(varchar," & ddMainTab.SelectedValue & ") + ''',''' +  convert(varchar," & ddSubTab.SelectedValue & ") + ''',''' + BSU_ID + ''',''' + convert(varchar," & Jval.ToString() & ") + ''' );return false;' AS VIEW_ASSIGNED FROM  OASIS.dbo.BUSINESSUNIT_M A " & _
                            " LEFT JOIN dbo.TASK_ROOTING_MASTER B ON A.BSU_ID=B.EMP_TASK_BSU_ID AND B.EMP_ID='" & HiddenEmp_id.Value & "' " & _
                            " AND MAIN_TAB_ID='" & ddMainTab.SelectedValue & "' AND SUB_TAB_ID='" & ddSubTab.SelectedValue & "' AND TASK_CATEGORY_ID='" & Jval & "' ORDER BY BSU_NAME"


            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

            GridRootingBsu.DataSource = ds
            GridRootingBsu.DataBind()
        Else

            lblmessage.Text = "Please select a Category"

        End If




    End Sub

    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = "Select A.TASK_CATEGORY_ID,CATEGORY_DES from TASK_CATEGORY_MASTER A INNER JOIN TASK_CATEGORY B ON B.ID=A.CATEGORY_ID where PARENT_TASK_CATEGORY_ID='" & childnodeValue & "' AND SUB_TAB_ID='" & ddSubTab.SelectedValue & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("CATEGORY_DES").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If


        For Each node As TreeNode In ParentNode.ChildNodes
            BindChildNodes(node)
        Next


    End Sub

    Public Sub BindJobCategory()
        lblmessage.Text = ""
        TreeJobCategory.Nodes.Clear()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select A.TASK_CATEGORY_ID,CATEGORY_DES from TASK_CATEGORY_MASTER A INNER JOIN TASK_CATEGORY B ON B.ID=A.CATEGORY_ID where PARENT_TASK_CATEGORY_ID=0 AND SUB_TAB_ID='" & ddSubTab.SelectedValue & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("CATEGORY_DES").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString()
                TreeJobCategory.Nodes.Add(ParentNode)
            Next
            For Each node As TreeNode In TreeJobCategory.Nodes
                BindChildNodes(node)
            Next

            TreeJobCategory.CollapseAll()

        Else

            btnSave.Visible = False
            TreeJobCategory.Nodes.Clear()

        End If

        If btnassign.Visible = False Then
            BindTreeUserData()
        End If

    End Sub


    Protected Sub ddMainTab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddMainTab.SelectedIndexChanged
        BindSubTabs()
        lblmessage.Text = ""
    End Sub

    Protected Sub ddSubTab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddSubTab.SelectedIndexChanged
        btnassign.Visible = False
        BindJobCategory()

        lblmessage.Text = ""
    End Sub



    Public Function GetNodeValue(ByVal ParentNode As TreeNode) As String
        Dim val = ""

        For Each node As TreeNode In ParentNode.ChildNodes
            If node.Checked Then
                val = node.Value
                'node.Checked = False
                Exit For
            Else
                val = GetNodeValue(node)

                If val <> "" Then
                    Exit For
                End If

            End If

        Next

        Return val

    End Function


    Public Function GetTaskCategory() As String
        Dim Task_Category_ID = ""


        For Each node As TreeNode In TreeJobCategory.Nodes

            If node.Checked Then
                Task_Category_ID = node.Value
                Exit For
            Else
                Task_Category_ID = GetNodeValue(node)
                If Task_Category_ID <> "" Then
                    Exit For
                End If
            End If

        Next

        If Task_Category_ID = "" Or Task_Category_ID = "-1" Then
            Task_Category_ID = "-1" ''General Category
        End If

        Return Task_Category_ID

    End Function



    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try

            Dim Main_Tab = ddMainTab.SelectedValue
            Dim Sub_Tab = ddSubTab.SelectedValue
            Dim Task_Category_ID = ""

            Task_Category_ID = GetTaskCategory()


            For Each row As GridViewRow In GridRootingBsu.Rows

                Dim deleteflag = 0 '1-DELETE 0- NO DELETE


                Dim taskbsuid = DirectCast(row.FindControl("HiddenBsuid"), HiddenField).Value

                Dim CheckOwner As New CheckBox
                CheckOwner = DirectCast(row.FindControl("CheckOwner"), CheckBox)
                Dim CheckMember As New CheckBox
                CheckMember = DirectCast(row.FindControl("CheckMember"), CheckBox)
                Dim CheckEmail As New CheckBox
                CheckEmail = DirectCast(row.FindControl("CheckEmail"), CheckBox)
                Dim CheckSMS As New CheckBox
                CheckSMS = DirectCast(row.FindControl("CheckSMS"), CheckBox)
                Dim reassign As New CheckBox
                reassign = DirectCast(row.FindControl("CheckReassign"), CheckBox)


                If CheckOwner.Checked Or CheckMember.Checked Or CheckEmail.Checked Or CheckSMS.Checked Or reassign.Checked Then
                    deleteflag = 0 ''No Delete
                Else
                    deleteflag = 1 ''Delete
                End If


                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
                Dim pParms(11) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@MAIN_TAB_ID", Main_Tab)
                pParms(1) = New SqlClient.SqlParameter("@SUB_TAB_ID", Sub_Tab)
                pParms(2) = New SqlClient.SqlParameter("@TASK_CATEGORY_ID", Task_Category_ID)
                pParms(3) = New SqlClient.SqlParameter("@TASK_CATEGORY_OWNER", CheckOwner.Checked)
                pParms(4) = New SqlClient.SqlParameter("@EMP_ID", HiddenEmp_id.Value)
                pParms(5) = New SqlClient.SqlParameter("@TASK_MEMBER", CheckMember.Checked)
                pParms(6) = New SqlClient.SqlParameter("@EMAIL_NOTIFY", CheckEmail.Checked)
                pParms(7) = New SqlClient.SqlParameter("@SMS_NOTIFY", CheckSMS.Checked)
                pParms(8) = New SqlClient.SqlParameter("@DELETE_FLAG", deleteflag)
                pParms(9) = New SqlClient.SqlParameter("@EMP_TASK_BSU_ID", taskbsuid)
                pParms(10) = New SqlClient.SqlParameter("@CAN_RESIGN", reassign.Checked)

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_UPDATE_DELETE_TASK_ROOTING_MASTER", pParms)

            Next
            lblmessage.Text = "Successfully updated"


        Catch ex As Exception
            lblmessage.Text = "Error : " & ex.Message
        End Try


    End Sub

    Public Sub SaveCatId(ByVal Category_id As String, ByVal deleteflag As Integer)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TASK_CATEGORY_ID", Category_id)
        pParms(1) = New SqlClient.SqlParameter("@EMP_ID", HiddenEmp_id.Value)
        pParms(2) = New SqlClient.SqlParameter("@DELETE_FLAG", deleteflag)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_TASK_EMP_CATEGORY_MASTER", pParms)

    End Sub

    Protected Sub btnassign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnassign.Click
        lblmessage.Text = ""
        BindRootingEmployeese()
        Panel1.Visible = True
        btnSave.Visible = True
    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        btnassign.Visible = True
        btnEdit.Visible = False
        btnSave.Visible = False
        BindJobCategory()
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreset.Click
        PLoad()
    End Sub

End Class
