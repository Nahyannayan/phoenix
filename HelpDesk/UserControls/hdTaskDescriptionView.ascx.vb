Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI
Partial Class HelpDesk_UserControls_hdTaskDescriptionView
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenTaskListId.Value = Request.QueryString("TaskListId")
            HiddenEmpID.Value = Session("EmployeeId")
            BindDetails()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindDetails()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT TASK_TITLE,TASK_DESCRIPTION,UPLOAD_IDS  FROM TASK_LIST_MASTER where TASK_LIST_ID='" & HiddenTaskListId.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            'txtdetails.Content = ds.Tables(0).Rows(0).Item("TASK_DESCRIPTION").ToString()
            lbltitle.Text = "Subject :       " & ds.Tables(0).Rows(0).Item("TASK_TITLE").ToString()
            DataView.InnerHtml = ds.Tables(0).Rows(0).Item("TASK_DESCRIPTION").ToString()
            HdFileUploadViewAssign1.Uploadids = ds.Tables(0).Rows(0).Item("UPLOAD_IDS").ToString()
            HdFileUploadViewAssign1.ShowCheck = False
        End If


    End Sub

End Class
