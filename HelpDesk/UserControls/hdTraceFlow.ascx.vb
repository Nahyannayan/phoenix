Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Partial Class HelpDesk_UserControls_hdTraceFlow
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenTaskListID.Value = Request.QueryString("task_list_id").ToString()
            BindGrid()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT * from TRACE_FLOW WHERE TASK_LIST_ID='" & HiddenTaskListID.Value & "' ORDER BY TASK_DATE_TIME DESC"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        GridTrace.DataSource = ds
        GridTrace.DataBind()

        'For Each row As GridViewRow In GridTrace.Rows
        '    Dim hiddenids As HiddenField = DirectCast(row.FindControl("HiddenUploads"), HiddenField)
        '    Dim HdFileUploadViewAssign1 As UserControl = DirectCast(row.FindControl("hdFileUploadViewAssign"), UserControl)

        '    'HdFileUploadViewAssign1.Uploadids = hiddenids
        'Next

    End Sub

    Protected Sub GridTrace_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridTrace.PageIndexChanging
        GridTrace.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class
