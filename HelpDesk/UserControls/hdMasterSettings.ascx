<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdMasterSettings.ascx.vb" Inherits="HelpDesk_UserControls_hdMasterSettings" %>
<%@ Register Src="hdInsertTaskCategory.ascx" TagName="hdInsertTaskCategory" TagPrefix="uc2" %>
<%@ Register Src="hdTaskRootingMaster.ascx" TagName="hdTaskRootingMaster" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<%--<script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />--%>
<link type="text/css" href="../../cssfiles/sb-admin.css" rel="stylesheet" />
<link type="text/css" href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
 <script type="text/javascript" >


   
    window.setTimeout('setpath()',100);
    
     function setpath()
        {
           var path = window.location.href
           var Rpath=path.substring(path.indexOf('?'),path.length)
           var objFrame
           
           //Rooting Master
           objFrame=document.getElementById("F1"); 
           objFrame.src="Pages/hdTaskRootingMasterList.aspx" + Rpath
           
            //Category Insert
           objFrame=document.getElementById("F2"); 
           objFrame.src="Pages/hdInsertTaskCategory.aspx" + Rpath
           
             //Category Insert
           objFrame=document.getElementById("F3"); 
           objFrame.src="Pages/hdInsertCategory.aspx" + Rpath
           
           
        }
   
   </script> 
<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-phone-square mr-3"></i>
            Master Settings
        </div>

<div class="card-body">
<div class="table-responsive m-auto">
<div>
    <div align="left" >
        <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
         
             <ajaxToolkit:TabPanel ID="T3" runat="server">
                <ContentTemplate>
                
                 <iframe id="F3" height="1000" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="100%"></iframe>

                </ContentTemplate>
                <HeaderTemplate>
                 Category Master
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
            <ajaxToolkit:TabPanel ID="T2" runat="server">
                <ContentTemplate>
                
                <iframe id="F2" height="1000" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="100%"></iframe>

                </ContentTemplate>
                <HeaderTemplate>
                    Insert Task Categories
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
          
             <ajaxToolkit:TabPanel ID="T1" runat="server">
                <ContentTemplate>
         
                 <iframe id="F1" height="1000" scrolling="auto"  marginwidth="0px"  frameborder="0"  width="100%"></iframe>

                </ContentTemplate>
                <HeaderTemplate>
                 Task Routing Master
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
   <%--      <ajaxToolkit:TabPanel ID="T4" runat="server">
                <ContentTemplate>
                </ContentTemplate>
                <HeaderTemplate>
                   Master Settings 4
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>--%>
        </ajaxToolkit:TabContainer>
        <asp:HiddenField ID="Hiddenempid" runat="server" />
        <asp:HiddenField ID="Hiddenbsuid" runat="server" />
    </div>
</div>
                </div></div></div>
