﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class HelpDesk_UserControls_hdTaskMonitor
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindBusinessUnit(ddbsu)
            BindPriority()
            BindStatus()
            BindJobCategopry()
            BindDrops()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnexport)

    End Sub

    Sub BindBusinessUnit(ByVal ddlBUnit As DropDownList)

        Try
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
            CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
            & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
            ddlBUnit.DataSource = ds
            ddlBUnit.DataTextField = "BSU_NAME"
            ddlBUnit.DataValueField = "BSU_ID"
            ddlBUnit.DataBind()
            Dim list As New ListItem
            list.Text = "Select Bussiness Unit"
            list.Value = "-1"
            ddlBUnit.Items.Insert(0, list)
            'If Not ddlBUnit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            '    ddlBUnit.Items.FindByValue(Session("sBsuid")).Selected = True
            'End If

            ddlBUnit.ClearSelection()
            ddlBUnit.Items.FindByValue(Session("sBsuid")).Selected = True

        Catch ex As Exception

        End Try


    End Sub

    Public Sub BindDrops()

        '' Reported by
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = " SELECT DISTINCT REPORTED_EMP_ID AS EMP_ID , isnull(emp_displayname,ISNULL(C.EMP_FNAME, '') + ' ' + ISNULL(C.EMP_LNAME, ''))  AS EMPNAME ,emp_displayname " & _
                        " FROM  OASIS.dbo.EMPLOYEE_M C INNER JOIN dbo.TASK_CONTACT_MASTER CC ON CC.REPORTED_EMP_ID=C.EMP_ID ORDER BY emp_displayname "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddreportedby.DataSource = ds
        ddreportedby.DataTextField = "EMPNAME"
        ddreportedby.DataValueField = "EMP_ID"
        ddreportedby.DataBind()

        Dim list As New ListItem
        list.Text = "--"
        list.Value = "-1"
        ddreportedby.Items.Insert(0, list)


        '' Claimed by
        Sql_Query = " SELECT DISTINCT CLAIM_EMP_ID AS EMP_ID  , isnull(emp_displayname,ISNULL(C.EMP_FNAME, '') + ' ' + ISNULL(C.EMP_LNAME, ''))  AS EMPNAME ,emp_displayname " & _
                    " FROM  OASIS.dbo.EMPLOYEE_M C INNER JOIN dbo.TASK_LIST_MASTER CC ON CC.CLAIM_EMP_ID=C.EMP_ID ORDER BY emp_displayname "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddclaimedby.DataSource = ds
        ddclaimedby.DataTextField = "EMPNAME"
        ddclaimedby.DataValueField = "EMP_ID"
        ddclaimedby.DataBind()
        ddclaimedby.Items.Insert(0, list)


        ''Assigned by

        Sql_Query = " SELECT DISTINCT TASK_ASSIGNED_BY_EMP_ID AS EMP_ID  , isnull(emp_displayname,ISNULL(C.EMP_FNAME, '') + ' ' + ISNULL(C.EMP_LNAME, ''))  AS EMPNAME ,emp_displayname " & _
                   " FROM  OASIS.dbo.EMPLOYEE_M C INNER JOIN dbo.TASK_ASSIGNED_LOG CC ON CC.TASK_ASSIGNED_BY_EMP_ID=C.EMP_ID ORDER BY emp_displayname "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddassignedby.DataSource = ds
        ddassignedby.DataTextField = "EMPNAME"
        ddassignedby.DataValueField = "EMP_ID"
        ddassignedby.DataBind()
        ddassignedby.Items.Insert(0, list)


        ''Assigned to

        Sql_Query = " SELECT DISTINCT TASK_ASSIGNED_TO_EMP_ID AS EMP_ID  , isnull(emp_displayname,ISNULL(C.EMP_FNAME, '') + ' ' + ISNULL(C.EMP_LNAME, ''))  AS EMPNAME ,emp_displayname " & _
                   " FROM  OASIS.dbo.EMPLOYEE_M C INNER JOIN dbo.TASK_ASSIGNED_LOG CC ON CC.TASK_ASSIGNED_TO_EMP_ID=C.EMP_ID ORDER BY emp_displayname "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddassignedto.DataSource = ds
        ddassignedto.DataTextField = "EMPNAME"
        ddassignedto.DataValueField = "EMP_ID"
        ddassignedto.DataBind()

        ddassignedto.Items.Insert(0, list)





    End Sub

    Public Sub BindPriority()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT * FROM TASK_PRIORITY_MASTER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddpriority.DataSource = ds
        ddpriority.DataTextField = "PRIORITY_DESCRIPTION"
        ddpriority.DataValueField = "RECORD_ID"
        ddpriority.DataBind()

        Dim list As New ListItem
        list.Text = "Select Priority"
        list.Value = "-1"
        ddpriority.Items.Insert(0, list)



    End Sub


    Public Sub BindStatus()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT * FROM TASK_STATUS_MASTER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddstatus.DataSource = ds
        ddstatus.DataTextField = "TASK_STATUS_DESCRIPTION"
        ddstatus.DataValueField = "STATUS_ID"
        ddstatus.DataBind()

        Dim list As New ListItem
        list.Text = "Select Status"
        list.Value = "-1"
        ddstatus.Items.Insert(0, list)

        ddstatus.ClearSelection()
        ddstatus.Items.FindByValue("1").Selected = True

    End Sub

    Public Sub BindJobCategopry()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "SELECT DISTINCT A.TASK_CATEGORY_ID,( CATEGORY_DES+ ' - (' + MAIN_TAB_DESCRIPTION + '/'+ SUB_TAB_DESCRIPTION + ')' ) TASK_CATEGORY_DESCRIPTION,CATEGORY_DES AS CD " & _
                        " FROM dbo.TASK_CATEGORY_MASTER A  " & _
                        " INNER JOIN dbo.TASK_ROOTING_MASTER B  ON A.TASK_CATEGORY_ID=B.TASK_CATEGORY_ID   " & _
                        " INNER JOIN TASK_CATEGORY B1 ON A.CATEGORY_ID=B1.ID" & _
                        " INNER JOIN dbo.SUB_TAB_MASTER C ON C.SUB_TAB_ID = A.SUB_TAB_ID " & _
                        " INNER JOIN dbo.MAIN_TAB_MASTER D ON D.MAIN_TAB_ID = C.MAIN_TAB_ID " & _
                        " WHERE  B.TASK_CATEGORY_OWNER = 'TRUE' " & _
                        " ORDER BY CATEGORY_DES DESC "

        ''Bind Job Source
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddTaskCategory.DataSource = ds
        ddTaskCategory.DataTextField = "TASK_CATEGORY_DESCRIPTION"
        ddTaskCategory.DataValueField = "TASK_CATEGORY_ID"
        ddTaskCategory.DataBind()

        Dim list As New ListItem
        list.Text = "Task Category"
        list.Value = "-1"
        ddTaskCategory.Items.Insert(0, list)

    End Sub

    Protected Sub ddTaskCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddTaskCategory.SelectedIndexChanged
        lblmessage.Text = ""
        SearchTaskList()

    End Sub

    Public Sub SearchTaskList(Optional ByVal export As Boolean = False)
        Try


            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim pParms(13) As SqlClient.SqlParameter

            If ddTaskCategory.SelectedIndex > 0 Then
                pParms(0) = New SqlClient.SqlParameter("@TASK_CATEGORY", ddTaskCategory.SelectedValue)
            End If

            If txttasklistid.Text.Trim() <> "" Then
                pParms(1) = New SqlClient.SqlParameter("@TASK_LIST_ID", txttasklistid.Text.Trim())
            End If

            If txtreqdate.Text.Trim() <> "" Then
                pParms(2) = New SqlClient.SqlParameter("@TASK_TRAGET_DATE", txtreqdate.Text.Trim())
            End If

            If ddpriority.SelectedIndex > 0 Then
                pParms(3) = New SqlClient.SqlParameter("@TASK_PRIORITY", ddpriority.SelectedValue)
            End If

            If ddstatus.SelectedIndex > 0 Then
                pParms(4) = New SqlClient.SqlParameter("@TASK_STATUS", ddstatus.SelectedValue)
            End If

            If txtentrydate.Text.Trim() <> "" Then
                pParms(5) = New SqlClient.SqlParameter("@TASK_DATE_TIME", txtentrydate.Text.Trim())
            End If

            If txtkeyword.Text.Trim() <> "" Then
                pParms(6) = New SqlClient.SqlParameter("@KEYWORD", txtkeyword.Text.Trim())
            End If

            If ddreportedby.SelectedIndex > 0 Then
                pParms(7) = New SqlClient.SqlParameter("@REPORTED_EMP_ID", ddreportedby.SelectedValue)
            End If

            If ddclaimedby.SelectedIndex > 0 Then
                pParms(8) = New SqlClient.SqlParameter("@CLAIM_EMP_ID", ddclaimedby.SelectedValue)
            End If

            If ddassignedby.SelectedIndex > 0 Then
                pParms(9) = New SqlClient.SqlParameter("@TASK_ASSIGNED_BY_EMP_ID", ddassignedby.SelectedValue)
            End If

            If ddassignedto.SelectedIndex > 0 Then
                pParms(10) = New SqlClient.SqlParameter("@TASK_ASSIGNED_TO_EMP_ID", ddassignedto.SelectedValue)
            End If


            If Library.LibrarySuperAcess(Session("EmployeeId")) Then
            Else
                pParms(11) = New SqlClient.SqlParameter("@OWNER_EMP_ID", Session("EmployeeId"))
            End If


            If ddbsu.SelectedValue <> "-1" Then
                pParms(12) = New SqlClient.SqlParameter("@TASK_BSU_ID", ddbsu.SelectedValue)
            End If


            Dim ds As DataSet = Nothing

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TASK_SEARCH_FOR_TASK_MONITOR", pParms)


            GridTaskList.DataSource = ds
            GridTaskList.DataBind()

            If export Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dt As New DataTable
                    dt = ds.Tables(0)
                    'dt.Columns.Remove("VIEW_TRACE_FLOW")
                    'dt.Columns.Remove("TASK_ID")
                    'dt.Columns.Remove("STATUS_COLOR")
                    'dt.Columns.Remove("VIEW_TASK")
                    'dt.Columns.Remove("IMAGE_PATH")
                    'dt.Columns.Remove("ASSIGN")
                    'dt.Columns.Remove("CloseVisible")
                    'dt.Columns.Remove("REASSIGN")
                    'dt.Columns.Remove("HISTORY")
                    'dt.Columns.Remove("CHART_COMPARE")
                    'dt.Columns.Remove("FOLLOWUP")
                    'dt.Columns.Remove("CLAIM_EMP_ID")
                    ExportExcel(dt)
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub

    Public Sub ExportExcel(ByVal dt As DataTable)

        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, "A1", True)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")
        ef.SaveXls(Response.OutputStream)

    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        lblmessage.Text = ""
        SearchTaskList()

    End Sub

    Protected Sub GridTaskList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridTaskList.PageIndexChanging
        GridTaskList.PageIndex = e.NewPageIndex
        SearchTaskList()

    End Sub


    Protected Sub ddpriority_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddpriority.SelectedIndexChanged
        lblmessage.Text = ""
        SearchTaskList()
    End Sub

    Protected Sub ddstatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddstatus.SelectedIndexChanged
        lblmessage.Text = ""
        SearchTaskList()
    End Sub


    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        SearchTaskList(True)
    End Sub
End Class
