﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdViewOwnerMembers.ascx.vb" Inherits="HelpDesk_UserControls_hdViewOwnerMembers" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
</style>




   <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
   <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 



<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-question-square mr-3"></i>
            Government Relations Report
        </div>
 <div class="card-body">
            <div class="table-responsive m-auto">

<table width="100%" align="center">
    
    <tr>
        <td width="20%"></td>
        <td width="30%"></td>
        <td width="20%"></td>
        <td width="30%"></td>
    </tr>
    <tr >
        <td class="table-bg" colspan="4">
            <span class="field-label">
            Search</span> </td> 
    </tr>
    
    <tr>
        <td width="20%"><span class="field-label">
                Main Tab</span></td>
        
        <td colspan="3">
                <asp:DropDownList ID="ddMainTab" runat="server" AutoPostBack="True">
                </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td width="20%">
            <span class="field-label">
            Departments </span></td>
        
        <td colspan="3">
                <asp:DropDownList ID="ddSubTab" runat="server" AutoPostBack="True">
                </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td  width="20%">
            <span class="field-label">
            Business Unit </span></td>
        
        <td colspan="3">
            <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td   width="20%"><span class="field-label">
            Task Category </span></td>
        
        <td colspan="3">
            <asp:DropDownList ID="ddJobCategory" runat="server">
            </asp:DropDownList>
        </td>
    </tr>

                    
                   

    <tr>
        <td class="table-bg">
            <span class="field-label">
            Owner and Members List
                </span>
                </td>
    </tr>
    <tr>
        <td colspan="4" align="center">
                    
                    <asp:GridView ID="GridMembers" EmptyDataText="No owners or members assigned for selected search."  AutoGenerateColumns="false" 
                      Width="100%"   runat="server" CssClass="table table-bordered table-row">
                    <Columns>
                    <asp:TemplateField HeaderText="Name">
                     <HeaderTemplate>
                                            <span class="field-label">
                                          Name </span>
                                
                            </HeaderTemplate>
                    <ItemTemplate>
                    <asp:Label ID="l1" Text='<% #Eval("EMP_NAME") %>' runat="server"></asp:Label>
                  
                  
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="BSU">
                     <HeaderTemplate>
                                            <span class="field-label">
                                          BSU </span>
                                
                            </HeaderTemplate>
                    <ItemTemplate>
                    
                    <%#Eval("BSU_SHORTNAME") %>
                    
                    </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Owner">
                     <HeaderTemplate>
                                
                                            <span class="field-label">
                                           Owner
                                                </span>
                                
                            </HeaderTemplate>
                    <ItemTemplate>
                    <center>
                    <img id="M1" src="~/Images/tick.gif" visible='<% #Eval("TASK_CATEGORY_OWNER") %>' runat="server" />
                        </center>
                    
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Member">
                     <HeaderTemplate>
                                            <span class="field-label">
                                           Member </span>
                                
                            </HeaderTemplate>
                    <ItemTemplate>
                    <center>
                       <img id="M2" src="~/Images/tick.gif" visible='<% #Eval("TASK_MEMBER") %>' runat="server" />
                    </center>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email">
                     <HeaderTemplate>
                                 <span class="field-label">
                                           Email </span>
                                
                            </HeaderTemplate>
                    <ItemTemplate>
                    <center>
                       <img id="M3" src="~/Images/tick.gif" visible='<% #Eval("EMAIL_NOTIFY") %>' runat="server" />
                    </center>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SMS">
                     <HeaderTemplate>
                                 <span class="field-label">
                                           SMS </span>
                                
                            </HeaderTemplate>
                    <ItemTemplate>
                        <center>
                    
                       <img id="M4" src="~/Images/tick.gif" visible='<% #Eval("SMS_NOTIFY") %>' runat="server" />
                    </center> 
                    </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Reassign">
                     <HeaderTemplate>
                                <span class="field-label">
                                           Reassign </span>
                                           
                            </HeaderTemplate>
                    <ItemTemplate>
                    <center>
                       <img id="M5" src="~/Images/tick.gif" visible='<% #Eval("CAN_RESIGN") %>' runat="server" />
                        </center>
                    
                    </ItemTemplate>
                    </asp:TemplateField>
                    </Columns>
                     <%--<HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                     <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                     <SelectedRowStyle CssClass="Green" Wrap="False" />
                     <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />--%>
                     <EmptyDataRowStyle Wrap="False" />
                     <EditRowStyle Wrap="False" />
                    </asp:GridView>
                    
        </td>
    </tr>
</table>
            <asp:Label ID="lblmessage" runat="server" align="left"></asp:Label>

</div></div></div>

                          
