<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskAssigning.ascx.vb"
    Inherits="HelpDesk_UserControls_hdTaskAssigning" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


 <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Assign Task
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
    <table cellpadding="5" cellspacing="0" width="100%">
        <tr>
            <td class="subheader_img">
                Search
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr align="left">
                        <td>
                          <span class="field-label">    Task ID</span>
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:TextBox ID="txttasklistid" runat="server" Width="100px"></asp:TextBox>
                        </td>
                        <td>
                             <span class="field-label"> Required Date</span>
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:TextBox ID="txtreqdate" runat="server" Width="150px"></asp:TextBox>&nbsp;</td>
                        <td>
                             <span class="field-label">   Keyword</span></td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:TextBox ID="txtkeyword" runat="server"></asp:TextBox>
                        </td>
                        <td>
                           <span class="field-label">   Priority</span>
                        </td>
                        <td>
                            :</td>
                        <td>
                            <asp:DropDownList ID="ddpriority" runat="server" Width="200px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left">
                        <td>
                            <span class="field-label">  Entry From Date</span></td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:TextBox ID="txtentryFromdate" Width="150px" runat="server"></asp:TextBox>
                              <ajaxToolkit:CalendarExtender ID="C3" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="txtentryFromdate" TargetControlID="txtentryFromdate">
                </ajaxToolkit:CalendarExtender>
                        </td>
                        <td>
                            &nbsp;  <span class="field-label">Entry To Date</span></td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:TextBox ID="txtentryTodate" Width="150px" runat="server"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="C2" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="txtentryTodate" TargetControlID="txtentryTodate">
                </ajaxToolkit:CalendarExtender>
                        </td>
                        <td>
                          <span class="field-label">    Status</span></td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddstatus" runat="server" />
                        </td>
                        <td>
                             <span class="field-label">    Assigned To</span></td>
                        <td>
                            :</td>
                        <td>
                            <asp:DropDownList ID="ddassignedto" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left">
                        <td>
                           <span class="field-label">   Task Category</span></td>
                        <td>
                            :
                        </td>
                        <td colspan="7">
                            <asp:DropDownList  ID="ddTaskCategory" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr align="left">
                        <td>
                             <span class="field-label"> Escalation Level</span></td>
                        <td>
                            &nbsp;</td>
                        <td colspan="7">
                            <asp:DropDownList ID="ddEscalationLevel" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="12">
                            <asp:Button ID="btnsearch" runat="server" CssClass="button" Text="Search" />
                            <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Export" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:FilteredTextBoxExtender ID="FT1" TargetControlID="txttasklistid" FilterType="Numbers"
                    runat="server">
                </ajaxToolkit:FilteredTextBoxExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="txtreqdate" TargetControlID="txtreqdate">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
    </table>
    <br />
    <table  cellpadding="5" cellspacing="0" width=100%">
        <tr>
            <td class="card-header letter-space">
              <i class="fa fa-book mr-3"></i> Tasks List</td>
        </tr>

       
        <tr>
            <td align="left">
                <asp:GridView ID="GridTaskList" AutoGenerateColumns="False" EmptyDataText="Search query did not retrieve any results. Please try with some other keyword(s)"
                    Width="100%" PageSize="20" runat="server" AllowPaging="True" OnRowDataBound="GridTaskList_RowDataBound" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Task ID">
                            <HeaderTemplate>
                              
                                            TaskID
                                          
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkTaskList" Text='<%#Eval("TASK_LIST_ID")%>' OnClientClick='<%#Eval("VIEW_TRACE_FLOW")%>'
                                        runat="server"></asp:LinkButton>
                                    <asp:HiddenField ID="HiddenTaskId" Value='<%#Eval("TASK_ID")%>' runat="server" />
                                    <asp:HiddenField ID="HiddenTaskListId" Value='<%#Eval("TASK_LIST_ID")%>' runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entry Date">
                            <HeaderTemplate>
                               
                                            Entry Date
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_DATE_TIME", "{0:dd/MMM/yyyy}")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Route Tab">
                            <HeaderTemplate>
                              
                                            Route Tab
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("ROOT_TAB")%>
                                    <br />
                                    <%#Eval("BSU_SHORTNAME")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <HeaderTemplate>
                               
                                            Category
                                            
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_CATEGORY_DESCRIPTION")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reported By">
                            <HeaderTemplate>
                               
                                            Reported By
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:HiddenField ID="HiddenColor" Value='<%#Eval("STATUS_COLOR")%>' runat="server" />
                                    <%#Eval("EMPNAME")%>
                                    <%#Eval("STUNAME")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <HeaderTemplate>
                              
                                            Title-Description
                                          
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkDes" OnClientClick='<%#Eval("VIEW_TASK")%>' Text='<%#Eval("TASK_TITLE")%>'
                                    Width="150px" runat="server" Style="white-space:pre-wrap;"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Priority">
                            <HeaderTemplate>
                            
                                            Priority
                                          
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("PRIORITY_DESCRIPTION")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <HeaderTemplate>
                               
                                            Status
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:Image ID="Image1" ImageUrl='<%#Eval("IMAGE_PATH")%>' ToolTip='<%#Eval("TASK_STATUS_DESCRIPTION")%>'
                                        runat="server" />
                                    <br />
                                    <span style="font-size: x-small">
                                        <%#Eval("TASK_STATUS_DESCRIPTION")%>
                                    </span>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Claim">
                            <HeaderTemplate>
                               
                                            Claim
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="lnkclaim" CommandName="claim" CommandArgument='<%#Eval("TASK_LIST_ID")%>' Visible='<%#Eval("Clinkvisible")%>' runat="server">Claim</asp:LinkButton>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Assign">
                            <HeaderTemplate>
                                
                                            Assign
                                            
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%-- <asp:LinkButton ID="LinkAssign" OnClientClick='<%#Eval("ASSIGN")%>' Visible='<%#Eval("CloseVisible")%>' runat="server">Assign</asp:LinkButton>--%>
                                    <asp:ImageButton ID="ImageAssign" OnClientClick='<%#Eval("ASSIGN")%>' Visible='<%#Eval("CloseVisible")%>'
                                        ToolTip="Click here to assign members to do the task." Width="20px" Height="20px"
                                        ImageUrl="~/Images/Helpdesk/Assign.png" runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reassign">
                            <HeaderTemplate>
                               
                                            Reassign
                                          
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%--<asp:LinkButton ID="LinkReAssign" OnClientClick='<%#Eval("REASSIGN")%>' Visible='<%#Eval("CloseVisible")%>' runat="server">Reassign</asp:LinkButton>--%>
                                    <asp:ImageButton ID="ImageReAssign" OnClientClick='<%#Eval("REASSIGN")%>' Visible='<%#Eval("CloseVisible")%>'
                                        ToolTip="Click here to Reassign task to other members." Width="20px" Height="20px"
                                        ImageUrl="~/Images/Helpdesk/Reassign.png" runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status History">
                            <HeaderTemplate>
                               
                                            Details
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%--<asp:LinkButton ID="LinkHistory" OnClientClick='<%#Eval("HISTORY")%>' runat="server">Details</asp:LinkButton>--%>
                                    <asp:ImageButton ID="ImageHistory" OnClientClick='<%#Eval("HISTORY")%>' ToolTip="View assigned and other transaction details"
                                        Width="20px" Height="20px" ImageUrl="~/Images/Helpdesk/Details.png" runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Chart">
                            <HeaderTemplate>
                              
                                            Chart
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%--<asp:LinkButton ID="LinkChart" OnClientClick='<%#Eval("CHART_COMPARE")%>' runat="server">Chart</asp:LinkButton>--%>
                                    <asp:ImageButton ID="ImageChart" OnClientClick='<%#Eval("CHART_COMPARE")%>' ToolTip="View progress chart of each members"
                                        Width="20px" Height="20px" ImageUrl="~/Images/Helpdesk/chart.png" runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Close">
                            <HeaderTemplate>
                                
                                            Completed
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%--<asp:LinkButton ID="LinkClose" CommandName="close" CommandArgument='<%#Eval("TASK_LIST_ID")%>' Visible='<%#Eval("CloseVisible")%>' runat="server">Completed</asp:LinkButton>--%>
                                    <asp:ImageButton ID="ImageClose" OnClientClick='<%#Eval("OWNER_CLOSE")%>'
                                        Visible='<%#Eval("CloseVisible")%>' ToolTip="Click here to complete the task."
                                        Width="20px" Height="20px" ImageUrl="~/Images/Helpdesk/Completed.png" runat="server" />
                                </center>
                                <ajaxToolkit:ConfirmButtonExtender ID="C1" ConfirmText="Are you sure, Do you want to close this task?"
                                    TargetControlID="ImageClose" runat="server">
                                </ajaxToolkit:ConfirmButtonExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Message">
                            <HeaderTemplate>
                              
                                            Follow Up
                                           
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:ImageButton ID="ImageFollowup" ImageUrl="~/Images/Helpdesk/followup.png" OnClientClick='<%#Eval("FOLLOWUP")%>'
                                        ToolTip="Follow-Up, Correspondance with the client." Width="20px" Height="20px"
                                        runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle Height="30px"  Wrap="False" />
                    <RowStyle  Height="25px" Wrap="False" />
                    <SelectedRowStyle  Wrap="False" />
                    <AlternatingRowStyle  Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <EditRowStyle Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
                </div>
            </div>
    <asp:HiddenField ID="HiddenEmpId" runat="server" />
   
</div>


