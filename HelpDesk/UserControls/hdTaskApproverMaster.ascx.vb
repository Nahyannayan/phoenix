﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
'Imports System.Data
'Imports System.Data.SqlClient
'Imports Microsoft.ApplicationBlocks.Data

Partial Class HelpDesk_UserControls_hdTaskApproverMaster
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetFilter()
            BindEmployeeBsu()
            BindEmpName()
            BindBsuGrid()
            BindMainTabs()
            If Not Session("dtTaskApproverMasterExisting") Is Nothing Then
                Session.Remove("dtTaskApproverMasterExisting")
            End If
            Me.gvEMPNameExisting.DataSource = Nothing
            Me.gvEMPNameExisting.DataBind()
            If Not Session("dtTaskUploadMaster") Is Nothing Then
                Session.Remove("dtTaskUploadMaster")
            End If
            Me.gvCategories.DataSource = Nothing
            Me.gvCategories.DataBind()
        Else
            If Not Session("dtTaskEscalationLevelsMasterApprover") Is Nothing Then
                Dim dt As DataTable = CType(Session("dtTaskEscalationLevelsMasterApprover"), DataTable)
                dt.Rows.Clear()
                Dim row As DataRow
                For Each grow As GridViewRow In Me.gvEMPName.Rows
                    row = dt.NewRow
                    row.Item("ID") = TryCast(grow.FindControl("lblEmpId"), Label).Text
                    'row.Item("Level1") = TryCast(grow.FindControl("chkLevel1"), CheckBox).Checked
                    'row.Item("Level2") = TryCast(grow.FindControl("chkLevel2"), CheckBox).Checked
                    'row.Item("Level3") = TryCast(grow.FindControl("chkLevel3"), CheckBox).Checked
                    dt.Rows.Add(row)
                Next
                dt.AcceptChanges()
                Session("dtTaskEscalationLevelsMasterApprover") = dt
            Else
                Dim dt As New DataTable
                dt.Columns.Add("ID", GetType(String))
                'dt.Columns.Add("Level1", GetType(Integer))
                'dt.Columns.Add("Level2", GetType(Integer))
                'dt.Columns.Add("Level3", GetType(Integer))
                dt.AcceptChanges()
                Dim row As DataRow
                For Each grow As GridViewRow In Me.gvEMPName.Rows
                    row = dt.NewRow
                    row.Item("ID") = TryCast(grow.FindControl("lblEmpId"), Label).Text
                    'row.Item("Level1") = TryCast(grow.FindControl("chkLevel1"), CheckBox).Checked
                    'row.Item("Level2") = TryCast(grow.FindControl("chkLevel2"), CheckBox).Checked
                    'row.Item("Level3") = TryCast(grow.FindControl("chkLevel3"), CheckBox).Checked
                    dt.Rows.Add(row)
                Next
                dt.AcceptChanges()
                Session("dtTaskEscalationLevelsMasterApprover") = dt
            End If
        End If

        FillEmpNames(h_EMPID.Value)

        If Not Session("dtTaskApproverMasterExisting") Is Nothing Then
            Me.gvEMPNameExisting.DataSource = CType(Session("dtTaskApproverMasterExisting"), DataSet)
            Me.gvEMPNameExisting.DataBind()
        Else
            Me.gvEMPNameExisting.DataSource = Nothing
            Me.gvEMPNameExisting.DataBind()
        End If
        If Not Session("dtTaskUploadMaster") Is Nothing Then
            Me.gvCategories.DataSource = CType(Session("dtTaskUploadMaster"), DataSet)
            Me.gvCategories.DataBind()
        Else
            Me.gvCategories.DataSource = Nothing
            Me.gvCategories.DataBind()
        End If
    End Sub
    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub
    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMP_ID as ID, EMPNO as EMPNO, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR, 0 as Level1, 0 as Level2, 0 as Level3 from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        'If Not Session("dtTaskEscalationLevelsMasterApprover") Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
        '    Dim row, tmprow As DataRow
        '    Dim dt As DataTable = Session("dtTaskEscalationLevelsMasterApprover")
        '    For Each row In dt.Rows
        '        If ds.Tables(0).Select("ID='" & row.Item("ID") & "'").Length >= 0 Then
        '            tmprow = ds.Tables(0).Select("ID='" & row.Item("ID") & "'")(0)
        '            tmprow.Item("Level1") = row.Item("Level1")
        '            tmprow.Item("Level2") = row.Item("Level2")
        '            tmprow.Item("Level3") = row.Item("Level3")
        '        End If
        '    Next
        'End If
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()

        If Me.gvEMPName.Rows.Count <= 0 Then
            txtEMPNAME.Visible = True
            lnlbtnAddEMPID.Visible = True
            imgGetEmp.Visible = True
        Else
            txtEMPNAME.Visible = False
            lnlbtnAddEMPID.Visible = False
            imgGetEmp.Visible = False
        End If

        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        
        Return True
    End Function
    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Public Sub GetFilter()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim Encr_decrData As New Encryption64
            Dim mednuid = "0"
            Try
                mednuid = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Catch ex As Exception

            End Try


            Dim Query = "SELECT * FROM ADD_ISSUE_CAT_FILTER WHERE MENU_ID='" & mednuid & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query)
            If ds.Tables(0).Rows.Count > 0 Then
                Hiddensubtabids.Value = ds.Tables(0).Rows(0).Item("SUB_TAB_IDS").ToString()
                Hiddentoplevelcatids.Value = ds.Tables(0).Rows(0).Item("TOP_LEVEL_CAT_IDS").ToString()
                Hiddentaskcatids.Value = ds.Tables(0).Rows(0).Item("TASK_CAT_IDS").ToString()
                HiddenType.Value = ds.Tables(0).Rows(0).Item("TYPE").ToString()

            End If


        Catch ex As Exception

        End Try

    End Sub

    Public Sub BindEmployeeBsu()
        Dim ds As New DataSet

        Dim query = " select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 " & _
                    " union select BSU_ID,BSU_NAME from dbo.BUSINESSUNIT_M WHERE BSU_CRM_NON_GEMS='True' order by BSU_NAME  "

        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, query)
        ddbsu.DataSource = ds
        ddbsu.DataTextField = "BSU_NAME"
        ddbsu.DataValueField = "BSU_ID"
        ddbsu.DataBind()
        ddbsu.SelectedValue = Session("sbsuid")
    End Sub


    Public Sub BindMainTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select * from  MAIN_TAB_MASTER WHERE ACTIVE='True'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddMaintab.DataSource = ds
        ddMaintab.DataTextField = "MAIN_TAB_DESCRIPTION"
        ddMaintab.DataValueField = "MAIN_TAB_ID"
        ddMaintab.DataBind()


        BindSubTabs()


    End Sub
    Public Sub BindSubTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select * from  SUB_TAB_MASTER where MAIN_TAB_ID='" & ddMaintab.SelectedValue & "' AND ACTIVE='True'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        Dim view As New DataView(ds.Tables(0))
        If Hiddensubtabids.Value <> "" Then
            view.RowFilter = "SUB_TAB_ID not in (" & Hiddensubtabids.Value & ")"
        End If


        ddsubtab.DataSource = view
        ddsubtab.DataTextField = "SUB_TAB_DESCRIPTION"
        ddsubtab.DataValueField = "SUB_TAB_ID"
        ddsubtab.DataBind()

        BindJobCategory()
    End Sub

    Public Sub BindChildNodesLowlevelCategory(ByVal ParentNode As TreeNode)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = " select CONVERT(VARCHAR,LEVEL_ID) +'-'+ CONVERT(VARCHAR,A.TASK_CATEGORY_ID)TASK_CATEGORY_ID,CATEGORY_DES from dbo.TASK_MAIN_CATEGORY_LEVELS A  " & _
                        " INNER JOIN dbo.TASK_CATEGORY_MASTER B ON A.TASK_CATEGORY_ID=B.TASK_CATEGORY_ID  " & _
                        " INNER JOIN dbo.TASK_CATEGORY C ON B.CATEGORY_ID=C.ID WHERE A.MAIN_CATEGORY_ID='" & ParentNode.Value & "' "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        Dim view As New DataView(ds.Tables(0))
        If Hiddentaskcatids.Value <> "" Then
            view.RowFilter = "TASK_CATEGORY_ID not in (" & Hiddentaskcatids.Value & ")"
        End If

        ds.Tables.Clear()
        ds.Tables.Add(view.ToTable())

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = "<font color='red'><strong>" + ds.Tables(0).Rows(i).Item("CATEGORY_DES").ToString() + "</strong></font>"
                ChildNode.Value = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString()
                ''ChildNode.ShowCheckBox = False
                ParentNode.ChildNodes.Add(ChildNode)
                ''ChildNode.NavigateUrl = GetNavigateDepartmentEdit(ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString())
            Next
        End If

    End Sub

    Private Function GetNavigateDepartmentEdit(ByVal pId As String) As String

        Return String.Format("javascript:var popup = window.showModalDialog('YearlyDepartmentsEdit.aspx?dep_id={0}', '','dialogHeight:500px;dialogWidth:600px;scroll:auto;resizable:yes;'); window.location.reload();", pId)

    End Function


    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = "Select MAIN_CATEGORY_ID,MAIN_CATEGORY_DESC from  TASK_MAIN_CATEGORY where PRIMARY_MAIN_CATEGORY_ID='" & childnodeValue & "' AND SUB_TAB_ID='" & ddsubtab.SelectedValue & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        Dim view As New DataView(ds.Tables(0))
        If Hiddentoplevelcatids.Value <> "" Then
            view.RowFilter = "MAIN_CATEGORY_ID not in (" & Hiddentoplevelcatids.Value & ")"
        End If

        ds.Tables.Clear()
        ds.Tables.Add(view.ToTable())

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_DESC").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If

        For Each node As TreeNode In ParentNode.ChildNodes
            BindChildNodes(node)
        Next

        BindChildNodesLowlevelCategory(ParentNode)


    End Sub

    Public Sub BindJobCategory()
        TreeItemCategory.Nodes.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select MAIN_CATEGORY_ID,MAIN_CATEGORY_DESC from  TASK_MAIN_CATEGORY where PRIMARY_MAIN_CATEGORY_ID=0 AND SUB_TAB_ID='" & ddsubtab.SelectedValue & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        Dim view As New DataView(ds.Tables(0))
        If Hiddentoplevelcatids.Value <> "" Then
            view.RowFilter = "MAIN_CATEGORY_ID not in (" & Hiddentoplevelcatids.Value & ")"
        End If

        ds.Tables.Clear()
        ds.Tables.Add(view.ToTable())


        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_DESC").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_ID").ToString()
                TreeItemCategory.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In TreeItemCategory.Nodes
            BindChildNodes(node)
        Next

        'TreeItemCategory.ExpandAll()


    End Sub


    Protected Sub ddsubtab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsubtab.SelectedIndexChanged
        lblmessage.Text = ""
        BindJobCategory()
    End Sub

    Protected Sub ddMaintab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddMaintab.SelectedIndexChanged
        BindSubTabs()
    End Sub

    Protected Sub ddbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddbsu.SelectedIndexChanged
        BindEmpName()
    End Sub

    Public Sub BindEmpName()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Dim Sql_Query = " SELECT  A.EMP_ID,isnull(EMP_FNAME,'') + ' ' + isnull(EMP_MNAME ,'') + ' ' +isnull(EMP_LNAME ,'') + ' - ' + isnull(EMPNO ,'') as EMP_NAME, EMD_CUR_MOBILE,EMD_EMAIL,'javascript:openWindow(''' + convert(varchar,A.EMP_ID) + '''); return false;' OPENW,'javascript:openAssignCategory(''' + convert(varchar,A.EMP_ID) + '''); return false;' OPENWC FROM  EMPLOYEE_M A " & _
                      " INNER JOIN EMPLOYEE_D B ON  A.EMP_ID= B.EMD_EMP_ID " & _
                      " WHERE EMP_BSU_ID='" & ddbsu.SelectedValue & "' and A.EMP_bACTIVE='True' and A.EMP_STATUS <> 4 ORDER BY EMP_FNAME "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddemp.DataSource = ds
        ddemp.DataTextField = "EMP_NAME"
        ddemp.DataValueField = "EMP_ID"
        ddemp.DataBind()


    End Sub

    Protected Sub ddemp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddemp.SelectedIndexChanged
        BindBsuGrid()
    End Sub

    Public Sub BindBsuGrid()
        Dim ds As New DataSet
        Dim qry = " select usr_name from dbo.USERS_M where USR_EMP_ID='" & ddemp.SelectedValue & "' AND USR_BDISABLE=0"
        Dim usr_name = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        Dim query = ""

        If Library.LibrarySuperAcess(Session("EmployeeId")) = "False" Then

            If usr_name <> "" Then
                query = " select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] ('" & usr_name & "') WHERE ISNULL(BSU_bSHOW,1)=1 " & _
                                 " union select BSU_ID,BSU_NAME from dbo.BUSINESSUNIT_M WHERE BSU_CRM_NON_GEMS='True' order by BSU_NAME  "

            Else
                query = " select BSU_ID,BSU_NAME from dbo.BUSINESSUNIT_M order by BSU_NAME "

            End If

        Else

            query = " select BSU_ID,BSU_NAME from dbo.BUSINESSUNIT_M order by BSU_NAME "

        End If


        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, query)
        GridRootingBsu.DataSource = ds
        GridRootingBsu.DataBind()

    End Sub


    Private Function ValidatePageData() As Boolean
        ValidatePageData = False
        'Business Unit
        If Me.ddbsu.SelectedIndex < 0 Then
            Me.lblmessage.Text = "Please select Business Unit"
            Exit Function
        End If
        'Main Tab
        If Me.ddMaintab.SelectedIndex < 0 Then
            Me.lblmessage.Text = "Please select Main Tab"
            Exit Function
        End If
        'Sub Tab (Departments)
        If Me.ddsubtab.SelectedIndex < 0 Then
            Me.lblmessage.Text = "Please select Department"
            Exit Function
        End If
        'Categories
        If TreeItemCategory.CheckedNodes.Count <= 0 Then
            Me.lblmessage.Text = "Please select atleast one Category"
            Exit Function
        End If
        'Employee Grid
        'If Me.gvEMPName.Rows.Count <= 0 Then
        '    Me.lblmessage.Text = "Please select an employee who would be the approver of this task"
        '    Exit Function
        'End If
        If Me.gvEMPName.Rows.Count > 1 Then
            Me.lblmessage.Text = "Please select only one employee who would be the approver of this task"
            Exit Function
        End If
        'If Not Session("dtTaskEscalationLevelsMasterApprover") Is Nothing Then
        '    If CType(Session("dtTaskEscalationLevelsMasterApprover"), DataTable).Rows.Count <= 0 Then
        '        Me.lblmessage.Text = "Please select an employee who would be the approver of this task"
        '        Exit Function
        '    End If
        'End If

        Me.lblmessage.Text = ""
        Return True

    End Function

    Private Sub SavePageData()
        If Not Me.ValidatePageData() Then Exit Sub
        Dim Con As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString())
        Dim Tr As SqlTransaction
        Try
            Con.Open()
            Tr = Con.BeginTransaction
            ''TASK_APPROVER_MASTER table
            'For Each node As TreeNode In TreeItemCategory.CheckedNodes
            '    If node.Value.Contains("-") Then
            '        Dim nval = node.Value.Split("-1")(1)
            '        SaveEscalationLevels(Con, Tr, Me.ddbsu.SelectedValue, ddMaintab.SelectedValue, ddsubtab.SelectedValue, nval, Math.Ceiling(Val(Me.txtLevel1Days.Text)), Math.Ceiling(Val(Me.txtLevel2Days.Text)), Math.Ceiling(Val(Me.txtLevel3Days.Text)))
            '    End If
            'Next

            'TASK_APPROVER_MASTER table
            For Each node As TreeNode In TreeItemCategory.CheckedNodes
                If node.Value.Contains("-") Then
                    Dim nval = node.Value.Split("-1")(1)
                    If Not Session("dtTaskEscalationLevelsMasterApprover") Is Nothing Then
                        If CType(Session("dtTaskEscalationLevelsMasterApprover"), DataTable).Rows.Count > 0 Then
                            For Each row As DataRow In CType(Session("dtTaskEscalationLevelsMasterApprover"), DataTable).Rows
                                SaveTaskApproverMaster(Con, Tr, Me.ddbsu.SelectedValue, ddMaintab.SelectedValue, ddsubtab.SelectedValue, nval, row.Item("ID"), Session("sUsr_Id"))
                            Next
                        End If
                    End If
                End If
            Next
            'TASK_UPLOAD_REQUIRE_MASTER
            For Each node As TreeNode In TreeItemCategory.CheckedNodes
                If node.Value.Contains("-") Then
                    Dim nval = node.Value.Split("-1")(1)
                    'If Not Session("dtTaskEscalationLevelsMasterApprover") Is Nothing Then
                    '    For Each row As DataRow In CType(Session("dtTaskEscalationLevelsMasterApprover"), DataTable).Rows
                    '        SaveTaskApproverMaster(Con, Tr, Me.ddbsu.SelectedValue, ddMaintab.SelectedValue, ddsubtab.SelectedValue, nval, row.Item("ID"), Session("sUsr_Id"))
                    '    Next
                    'End If
                    SaveTaskUploadRequireMaster(Con, Tr, Me.ddbsu.SelectedValue, ddMaintab.SelectedValue, ddsubtab.SelectedValue, nval, IIf(Me.chkUpload.Checked, 1, 0), Session("sUsr_Id"))
                End If
            Next
            Tr.Commit()
            Con.Close()
            lblmessage.Text = "Task Approvers updated successfully."
            Session.Remove("dtTaskEscalationLevelsMasterApprover")
            Me.h_EMPID.Value = ""
            Me.gvEMPName.DataSource = Nothing
            Me.gvEMPName.DataBind()
            Session.Remove("dtTaskApproverMasterExisting")
            Me.gvEMPNameExisting.DataSource = Nothing
            Me.gvEMPNameExisting.DataBind()
            Session.Remove("dtTaskUploadMaster")
            Me.gvCategories.DataSource = Nothing
            Me.gvCategories.DataBind()
        Catch ex As Exception
            Tr.Rollback()
            lblmessage.Text = "There was an error setting task approvers."
        Finally
            If Not Con.State = ConnectionState.Closed Then Con.Close()
        End Try
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        Me.SavePageData()

        'Me.ValidatePageData()

        'Dim flag = 0

        'If TreeItemCategory.CheckedNodes.Count > 0 Then

        '    Dim node As TreeNode

        '    For Each node In TreeItemCategory.CheckedNodes

        '        If node.Value.Contains("-") Then
        '            Dim nval = node.Value.Split("-1")(1)

        '            For Each row As GridViewRow In GridRootingBsu.Rows

        '                If DirectCast(row.FindControl("Checkbsu"), CheckBox).Checked Then

        '                    Dim bsuid As String = DirectCast(row.FindControl("HiddenBsuid"), HiddenField).Value
        '                    Dim CheckOwner = CheckAccess.Items(0).Selected
        '                    Dim CheckMember = CheckAccess.Items(1).Selected
        '                    Dim CheckEmail = CheckAccess.Items(2).Selected
        '                    Dim CheckSMS = CheckAccess.Items(3).Selected
        '                    Dim reassign = CheckAccess.Items(4).Selected
        '                    SaveRooting(ddemp.SelectedValue, ddMaintab.SelectedValue, ddsubtab.SelectedValue, nval, bsuid, CheckOwner, CheckMember, CheckEmail, CheckSMS, reassign)
        '                    flag = 1
        '                End If

        '            Next
        '        End If

        '    Next

        '    If flag = 1 Then
        '        lblmessage.Text = "Successfully updated"
        '    Else
        '        lblmessage.Text = "Please select Business Unit"
        '    End If

        'Else

        '    lblmessage.Text = "Please select the categories."

        'End If

    End Sub


    Public Sub SaveRooting(ByVal emp_id As String, ByVal Main_Tab As String, ByVal Sub_Tab As String, ByVal Task_Category_ID As String, ByVal taskbsuid As String, ByVal CheckOwner As Boolean, ByVal CheckMember As Boolean, ByVal CheckEmail As Boolean, ByVal CheckSMS As Boolean, ByVal reassign As Boolean)


        Dim deleteflag = 0 '1-DELETE 0- NO DELETE

        If CheckOwner Or CheckMember Or CheckEmail Or CheckSMS Or reassign Then
            deleteflag = 0 ''No Delete
        Else
            deleteflag = 1 ''Delete
        End If

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(11) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@MAIN_TAB_ID", Main_Tab)
        pParms(1) = New SqlClient.SqlParameter("@SUB_TAB_ID", Sub_Tab)
        pParms(2) = New SqlClient.SqlParameter("@TASK_CATEGORY_ID", Task_Category_ID)
        pParms(3) = New SqlClient.SqlParameter("@TASK_CATEGORY_OWNER", CheckOwner)
        pParms(4) = New SqlClient.SqlParameter("@EMP_ID", emp_id)
        pParms(5) = New SqlClient.SqlParameter("@TASK_MEMBER", CheckMember)
        pParms(6) = New SqlClient.SqlParameter("@EMAIL_NOTIFY", CheckEmail)
        pParms(7) = New SqlClient.SqlParameter("@SMS_NOTIFY", CheckSMS)
        pParms(8) = New SqlClient.SqlParameter("@DELETE_FLAG", deleteflag)
        pParms(9) = New SqlClient.SqlParameter("@EMP_TASK_BSU_ID", taskbsuid)
        pParms(10) = New SqlClient.SqlParameter("@CAN_RESIGN", reassign)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_UPDATE_DELETE_TASK_ROOTING_MASTER", pParms)




    End Sub

    Public Sub SaveEscalationLevels(ByVal Con As SqlConnection, ByVal Tr As SqlTransaction, ByVal Bsu_Id As String, ByVal Main_Tab As String, ByVal Sub_Tab As String, ByVal Task_Category_Master_ID As String, ByVal Level1Days As Integer, ByVal Level2Days As Integer, ByVal Level3Days As Integer)

        'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim cmd As New SqlCommand
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Bsu_Id)
        pParms(1) = New SqlClient.SqlParameter("@MAIN_TAB_ID", Main_Tab)
        pParms(2) = New SqlClient.SqlParameter("@SUB_TAB_ID", Sub_Tab)
        pParms(3) = New SqlClient.SqlParameter("@TASK_CATEGORY_MASTER_ID", Task_Category_Master_ID)

        pParms(4) = New SqlClient.SqlParameter("@LEVEL_1_DAYS", Level1Days)
        pParms(5) = New SqlClient.SqlParameter("@LEVEL_2_DAYS", Level2Days)
        pParms(6) = New SqlClient.SqlParameter("@LEVEL_3_DAYS", Level3Days)

        'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SAVE_TASK_ESCALATION_LEVELS_MASTER", pParms)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "SAVE_TASK_ESCALATION_LEVELS_MASTER"
        cmd.Parameters.AddRange(pParms)
        cmd.Connection = Con
        cmd.Transaction = Tr
        cmd.ExecuteNonQuery()

    End Sub

    Public Sub SaveEscalationLevelsNotificationsUsers(ByVal Con As SqlConnection, ByVal Tr As SqlTransaction, ByVal Bsu_Id As String, ByVal Main_Tab As String, ByVal Sub_Tab As String, ByVal Task_Category_Master_ID As String, ByVal Level1 As Integer, ByVal Level2 As Integer, ByVal Level3 As Integer, ByVal Emp_Id As Integer)

        'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim cmd As New SqlCommand
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Bsu_Id)
        pParms(1) = New SqlClient.SqlParameter("@MAIN_TAB_ID", Main_Tab)
        pParms(2) = New SqlClient.SqlParameter("@SUB_TAB_ID", Sub_Tab)
        pParms(3) = New SqlClient.SqlParameter("@TASK_CATEGORY_MASTER_ID", Task_Category_Master_ID)

        pParms(4) = New SqlClient.SqlParameter("@LEVEL_1", Level1)
        pParms(5) = New SqlClient.SqlParameter("@LEVEL_2", Level2)
        pParms(6) = New SqlClient.SqlParameter("@LEVEL_3", Level3)
        pParms(7) = New SqlClient.SqlParameter("@EMP_ID", Emp_Id)

        'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SAVE_TASK_ESCALATION_LEVELS_MASTER", pParms)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "SAVE_TASK_ESCALATION_LEVELS_NOTIFICATION_USERS"
        cmd.Parameters.AddRange(pParms)
        cmd.Connection = Con
        cmd.Transaction = Tr
        cmd.ExecuteNonQuery()

    End Sub

    Public Sub SaveTaskApproverMaster(ByVal Con As SqlConnection, ByVal Tr As SqlTransaction, ByVal Bsu_Id As String, ByVal Main_Tab As String, ByVal Sub_Tab As String, ByVal Task_Category_Master_ID As String, ByVal Emp_Id As Integer, ByVal Usr_Id As String)

        'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim cmd As New SqlCommand
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Bsu_Id)
        pParms(1) = New SqlClient.SqlParameter("@MAIN_TAB_ID", Main_Tab)
        pParms(2) = New SqlClient.SqlParameter("@SUB_TAB_ID", Sub_Tab)
        pParms(3) = New SqlClient.SqlParameter("@TASK_CATEGORY_MASTER_ID", Task_Category_Master_ID)

        pParms(4) = New SqlClient.SqlParameter("@EMP_ID", Emp_Id)
        pParms(5) = New SqlClient.SqlParameter("@USR_ID", Usr_Id)

        'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SAVE_TASK_ESCALATION_LEVELS_MASTER", pParms)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "SAVE_TASK_APPROVER_MASTER"
        cmd.Parameters.AddRange(pParms)
        cmd.Connection = Con
        cmd.Transaction = Tr
        cmd.ExecuteNonQuery()

    End Sub

    Public Sub SaveTaskUploadRequireMaster(ByVal Con As SqlConnection, ByVal Tr As SqlTransaction, ByVal Bsu_Id As String, ByVal Main_Tab As String, ByVal Sub_Tab As String, ByVal Task_Category_Master_ID As String, ByVal Upload_Required As Integer, ByVal Usr_Id As String)

        'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim cmd As New SqlCommand
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Bsu_Id)
        pParms(1) = New SqlClient.SqlParameter("@MAIN_TAB_ID", Main_Tab)
        pParms(2) = New SqlClient.SqlParameter("@SUB_TAB_ID", Sub_Tab)
        pParms(3) = New SqlClient.SqlParameter("@TASK_CATEGORY_MASTER_ID", Task_Category_Master_ID)

        pParms(4) = New SqlClient.SqlParameter("@UPLOAD_REQUIRED", Upload_Required)
        pParms(5) = New SqlClient.SqlParameter("@USR_ID", Usr_Id)

        'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SAVE_TASK_ESCALATION_LEVELS_MASTER", pParms)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "SAVE_TASK_UPLOAD_REQUIRE_MASTER"
        cmd.Parameters.AddRange(pParms)
        cmd.Connection = Con
        cmd.Transaction = Tr
        cmd.ExecuteNonQuery()

    End Sub

    Protected Sub gvEMPName_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvEMPName.RowUpdating
        Me.gvEMPName.Rows(e.RowIndex).Cells(1).Text = True
    End Sub

    Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        If Not Session("dtTaskEscalationLevelsMasterApprover") Is Nothing Then
            Session.Remove("dtTaskEscalationLevelsMasterApprover")
        End If
    End Sub

    Protected Sub btnView_Click(sender As Object, e As EventArgs) Handles btnView.Click
        ViewExistingApprovers()
    End Sub

    Private Sub ViewExistingApprovers()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim con As New SqlConnection(str_conn)
        Dim cmd As New SqlCommand
        Dim nval, ids As String
        Dim ds As DataSet

        Dim pParms(3) As SqlClient.SqlParameter
        If Not TreeItemCategory.CheckedNodes.Count > 0 Then
            Me.lblmessage.Text = "Please select atleast one category to view the existing approvers for it"
            Exit Sub
        Else
            Me.lblmessage.Text = Nothing
        End If
        Try
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Me.ddbsu.SelectedValue)
            pParms(1) = New SqlClient.SqlParameter("@MAIN_TAB_ID", Me.ddMaintab.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@SUB_TAB_ID", Me.ddsubtab.SelectedValue)

            For Each node As TreeNode In TreeItemCategory.CheckedNodes
                If node.Value.Contains("-") Then
                    nval = node.Value.Split("-1")(1)
                    ids &= nval & ","
                End If
            Next

            If ids.EndsWith(",") Then
                ids = ids.TrimEnd(",")
            End If

            pParms(3) = New SqlClient.SqlParameter("@TASK_CATEGORY_MASTER_IDs", ids)


            'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SAVE_TASK_ESCALATION_LEVELS_MASTER", pParms)

            con.Open()
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "GET_TASK_APPROVERS", pParms)
            If Not Session("dtTaskApproverMasterExisting") Is Nothing Then
                Session.Remove("dtTaskApproverMasterExisting")
                Session("dtTaskApproverMasterExisting") = ds
            Else
                Session("dtTaskApproverMasterExisting") = ds
            End If
            Me.gvEMPNameExisting.DataSource = ds
            Me.gvEMPNameExisting.DataBind()
            Me.lblmessage.Text = Nothing
        Catch ex As Exception
            Me.lblmessage.Text = "Error occured while getting existing approvers"
        Finally
            If Not con Is Nothing Then If Not con.State = ConnectionState.Closed Then con.Close()
        End Try
    End Sub

    Private Sub ViewCategoriesRequiringUploading()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim con As New SqlConnection(str_conn)
        Dim cmd As New SqlCommand
        Dim nval, ids As String
        Dim ds As DataSet

        Dim pParms(3) As SqlClient.SqlParameter
        If Not TreeItemCategory.CheckedNodes.Count > 0 Then
            Me.lblmessage.Text = "Please select atleast one category to view if it requires document uploading"
            Exit Sub
        Else
            Me.lblmessage.Text = Nothing
        End If
        Try
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Me.ddbsu.SelectedValue)
            pParms(1) = New SqlClient.SqlParameter("@MAIN_TAB_ID", Me.ddMaintab.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@SUB_TAB_ID", Me.ddsubtab.SelectedValue)

            For Each node As TreeNode In TreeItemCategory.CheckedNodes
                If node.Value.Contains("-") Then
                    nval = node.Value.Split("-1")(1)
                    ids &= nval & ","
                End If
            Next

            If ids.EndsWith(",") Then
                ids = ids.TrimEnd(",")
            End If

            pParms(3) = New SqlClient.SqlParameter("@TASK_CATEGORY_MASTER_IDs", ids)


            'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SAVE_TASK_ESCALATION_LEVELS_MASTER", pParms)

            con.Open()
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "GET_TASK_UPLOAD_REQUIRE", pParms)
            If Not Session("dtTaskUploadMaster") Is Nothing Then
                Session.Remove("dtTaskUploadMaster")
                Session("dtTaskUploadMaster") = ds
            Else
                Session("dtTaskUploadMaster") = ds
            End If
            Me.gvCategories.DataSource = ds
            Me.gvCategories.DataBind()
            Me.lblmessage.Text = Nothing
        Catch ex As Exception
            Me.lblmessage.Text = "Error occured while getting document upload require for the selected categories"
        Finally
            If Not con Is Nothing Then If Not con.State = ConnectionState.Closed Then con.Close()
        End Try
    End Sub

    Protected Sub lnkbtngrdEMPExistingDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        Dim pParms(1) As SqlClient.SqlParameter

        Dim Con As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString())
        Try
            lblID = TryCast(sender.FindControl("lblID"), Label)
            If Not lblID Is Nothing Then
                pParms(0) = New SqlClient.SqlParameter("@TASK_APPROVER_ID", lblID.Text)
                pParms(1) = New SqlClient.SqlParameter("@USR_ID", Session("sUsr_Id"))
                Con.Open()
                SqlHelper.ExecuteNonQuery(Con, CommandType.StoredProcedure, "DELETE_TASK_APPROVER", pParms)
            End If
            Me.ViewExistingApprovers()
            Me.lblmessage.Text = "Data updated successfully"
        Catch ex As Exception
            Me.lblmessage.Text = "Error occured while deleting existing approver"
        Finally
            If Not Con Is Nothing Then If Not Con.State = ConnectionState.Closed Then Con.Close()
        End Try



    End Sub

    Protected Sub lnkbtngrdCategoriesDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblID As New Label
        Dim pParms(1) As SqlClient.SqlParameter

        Dim Con As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString())
        Try
            lblID = TryCast(sender.FindControl("lblUploadID"), Label)
            If Not lblID Is Nothing Then
                pParms(0) = New SqlClient.SqlParameter("@ID", lblID.Text)
                pParms(1) = New SqlClient.SqlParameter("@USR_ID", Session("sUsr_Id"))
                Con.Open()
                SqlHelper.ExecuteNonQuery(Con, CommandType.StoredProcedure, "DELETE_TASK_UPLOAD_REQUIRE_MASTER", pParms)
            End If
            Me.ViewCategoriesRequiringUploading()
            Me.lblmessage.Text = "Data updated successfully"
        Catch ex As Exception
            Me.lblmessage.Text = "Error occured while deleting existing category for uploading"
        Finally
            If Not Con Is Nothing Then If Not Con.State = ConnectionState.Closed Then Con.Close()
        End Try



    End Sub

    Protected Sub gvEMPNameExisting_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvEMPNameExisting.PageIndexChanging
        If Not Session("dtTaskApproverMasterExisting") Is Nothing Then
            gvEMPNameExisting.DataSource = CType(Session("dtTaskApproverMasterExisting"), DataSet)
        End If
        Me.gvEMPNameExisting.PageIndex = e.NewPageIndex
        gvEMPNameExisting.DataBind()
    End Sub

    Protected Sub btnView0_Click(sender As Object, e As EventArgs) Handles btnView0.Click
        Me.ViewCategoriesRequiringUploading()
    End Sub
    Protected Sub h_EMPID_ValueChanged(sender As Object, e As EventArgs)
        FillEmpNames(h_EMPID.Value)
    End Sub
End Class
