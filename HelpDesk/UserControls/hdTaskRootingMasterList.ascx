<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskRootingMasterList.ascx.vb"
    Inherits="HelpDesk_UserControls_hdTaskRootingMasterList" %>
    
<%--<script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />--%>
<link type="text/css" href="../../cssfiles/sb-admin.css" rel="stylesheet" />
<link type="text/css" href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />

<script type="text/javascript" >
    
    function openWindow(empid)
    {
   
    window.open('hdTaskRootingMasterV2.aspx?Emp_id='+ empid  , '','Height=700px,Width=1000px,scrollbars=yes,resizable=no,directories=yes');
    return false;
    
    
    }
    
    function openAssignCategory(empid)
    {
   
    window.open('AddUserCategories.aspx?Emp_id='+ empid  , '','Height=600px,Width=750px,scrollbars=yes,resizable=no,directories=yes');
    return false;
    
    
    }
    
    </script>



<table width="100%" align="center">
    <tr>
        <td><span class="field-label">
            Select BSU </span>
            <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridRootingEmployees" runat="server" ShowFooter="true" AutoGenerateColumns="false"
                Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                <Columns>
                    <asp:TemplateField HeaderText="Name">
                        <HeaderTemplate>
                           
                                        Name
                                        <br />
                                        <asp:TextBox ID="Txt1" Width="75%" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />
                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("EMP_NAME")%>
                            <asp:HiddenField ID="HiddenEmpId" runat="server" Value='<%#Eval("EMP_ID") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                        <%--<center>
                        
                        </center>--%>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mobile Number">
                        <HeaderTemplate>
                            
                                        Mobile Number
                                        <br />
                                        <asp:TextBox ID="Txt2" Width="75%" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />
                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:TextBox ID="txtMobile" Text='<% #Eval("EMD_CUR_MOBILE") %>' MaxLength="20" runat="server"></asp:TextBox>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email ID">
                        <HeaderTemplate>
                           
                                        Email ID
                                        <br />
                                        <asp:TextBox ID="Txt3" Width="75%" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />
                                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:TextBox ID="txtEmail" Text='<% #Eval("EMD_EMAIL") %>' runat="server"></asp:TextBox>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Assign">
                        <HeaderTemplate>
                                        Assign
                                        
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="lnkAssign" OnClientClick='<% #Eval("OPENW") %>' runat="server">Assign</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                <%--    <asp:TemplateField HeaderText="Categories">
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        <br />
                                        Categories
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="lnkCategory" OnClientClick='<% #Eval("OPENWC") %>' runat="server">Assign</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
                <%--<HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                <RowStyle CssClass="griditem" Wrap="False" />--%>
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <%--<AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />--%>
                <EmptyDataRowStyle Wrap="False" />
                <EditRowStyle Wrap="False" />
            </asp:GridView>
            <tr>
                <td align="center">

<asp:Button ID="btnsave" runat="server" CssClass="button" CommandName="Save" Text="Save Changes" />
                </td>
            </tr>

            <asp:Label ID="lblMessage" runat="server" class="text-danger"></asp:Label></td>
    </tr>
</table>
                
