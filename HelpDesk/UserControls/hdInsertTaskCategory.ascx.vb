Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class HelpDesk_UserControls_hdInsertTaskCategory
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindMainTabs()
            BindCategory()
        End If

    End Sub

    Public Sub BindCategory()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = " SELECT * FROM dbo.TASK_CATEGORY A " & _
                        " LEFT JOIN dbo.TASK_CATEGORY_MASTER B ON A.ID = B.CATEGORY_ID  " & _
                        " WHERE isnull(SUB_TAB_ID,'') != " & ddsubtab.SelectedValue & " ORDER BY CATEGORY_DES "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        GridCategory.DataSource = ds
        GridCategory.DataBind()

    End Sub

    Public Sub BindMainTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select * from  MAIN_TAB_MASTER WHERE ACTIVE='True'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddMaintab.DataSource = ds
        ddMaintab.DataTextField = "MAIN_TAB_DESCRIPTION"
        ddMaintab.DataValueField = "MAIN_TAB_ID"
        ddMaintab.DataBind()


        BindSubTabs()


    End Sub
    Public Sub BindSubTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select * from  SUB_TAB_MASTER where MAIN_TAB_ID='" & ddMaintab.SelectedValue & "' AND ACTIVE='True'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddsubtab.DataSource = ds
        ddsubtab.DataTextField = "SUB_TAB_DESCRIPTION"
        ddsubtab.DataValueField = "SUB_TAB_ID"
        ddsubtab.DataBind()

        BindJobCategory()
    End Sub

    'Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

    '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

    '    Dim childnodeValue = ParentNode.Value
    '    Dim Sql_Query = " select TASK_CATEGORY_ID,CATEGORY_DES from dbo.TASK_CATEGORY_MASTER a " & _
    '                    " inner join dbo.TASK_CATEGORY b on a.CATEGORY_ID= convert(varchar, b.id) " & _
    '                    " where PARENT_TASK_CATEGORY_ID='" & childnodeValue & "' AND SUB_TAB_ID='" & ddsubtab.SelectedValue & "'"

    '    Dim ds As DataSet
    '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

    '    If ds.Tables(0).Rows.Count > 0 Then
    '        Dim i = 0
    '        For i = 0 To ds.Tables(0).Rows.Count - 1
    '            Dim ChildNode As New TreeNode
    '            ChildNode.Text = ds.Tables(0).Rows(i).Item("CATEGORY_DES").ToString()
    '            ChildNode.Value = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString()
    '            ParentNode.ChildNodes.Add(ChildNode)

    '        Next
    '    End If


    '    For Each node As TreeNode In ParentNode.ChildNodes
    '        BindChildNodes(node)
    '    Next


    'End Sub


    Public Sub BindChildNodesLowlevelCategory(ByVal ParentNode As TreeNode)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = " select CONVERT(VARCHAR,LEVEL_ID) +'-'+ CONVERT(VARCHAR,A.TASK_CATEGORY_ID)TASK_CATEGORY_ID,CATEGORY_DES from dbo.TASK_MAIN_CATEGORY_LEVELS A  " & _
                        " INNER JOIN dbo.TASK_CATEGORY_MASTER B ON A.TASK_CATEGORY_ID=B.TASK_CATEGORY_ID  " & _
                        " INNER JOIN dbo.TASK_CATEGORY C ON B.CATEGORY_ID=C.ID WHERE A.MAIN_CATEGORY_ID='" & ParentNode.Value & "' "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = "<font color='red'><strong>" + ds.Tables(0).Rows(i).Item("CATEGORY_DES").ToString() + "</strong></font>"
                ChildNode.Value = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString()
                ''ChildNode.ShowCheckBox = False
                ParentNode.ChildNodes.Add(ChildNode)
                ''ChildNode.NavigateUrl = GetNavigateDepartmentEdit(ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString())
            Next
        End If

    End Sub

    Private Function GetNavigateDepartmentEdit(ByVal pId As String) As String

        Return String.Format("javascript:var popup = window.showModalDialog('YearlyDepartmentsEdit.aspx?dep_id={0}', '','dialogHeight:500px;dialogWidth:600px;scroll:auto;resizable:yes;'); window.location.reload();", pId)

    End Function


    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = "Select MAIN_CATEGORY_ID,MAIN_CATEGORY_DESC from  TASK_MAIN_CATEGORY where PRIMARY_MAIN_CATEGORY_ID='" & childnodeValue & "' AND SUB_TAB_ID='" & ddsubtab.SelectedValue & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_DESC").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If

        For Each node As TreeNode In ParentNode.ChildNodes
            BindChildNodes(node)
        Next

        BindChildNodesLowlevelCategory(ParentNode)


    End Sub

    Public Sub BindJobCategory()
        TreeItemCategory.Nodes.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select MAIN_CATEGORY_ID,MAIN_CATEGORY_DESC from  TASK_MAIN_CATEGORY where PRIMARY_MAIN_CATEGORY_ID=0 AND SUB_TAB_ID='" & ddsubtab.SelectedValue & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_DESC").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("MAIN_CATEGORY_ID").ToString()
                TreeItemCategory.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In TreeItemCategory.Nodes
            BindChildNodes(node)
        Next

        TreeItemCategory.ExpandAll()


    End Sub


    Protected Sub ddsubtab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsubtab.SelectedIndexChanged
        lblmessage.Text = ""
        BindCategory()
        BindJobCategory()
    End Sub

    Protected Sub ddMaintab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddMaintab.SelectedIndexChanged
        BindSubTabs()
    End Sub

    Public Function GetNodeValue(ByVal ParentNode As TreeNode) As String
        Dim val = ""

        For Each node As TreeNode In ParentNode.ChildNodes
            If node.Checked Then
                val = node.Value
                node.Checked = False
                Exit For
            Else
                val = GetNodeValue(node)

                If val <> "" Then
                    Exit For
                End If

            End If

        Next

        Return val

    End Function

    Protected Sub addbasecategory(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim val = ""

        For Each node As TreeNode In TreeItemCategory.Nodes
            If node.Checked Then
                val = node.Value
                node.Checked = False
                Exit For
            Else
                val = GetNodeValue(node)
                If val <> "" Then
                    Exit For
                End If
            End If
        Next

        If val <> "" Then

            For Each row As GridViewRow In GridCategory.Rows
                Dim check As CheckBox = DirectCast(row.FindControl("CheckCatogory"), CheckBox)
                If check.Checked Then

                    Dim catid As HiddenField = DirectCast(row.FindControl("HiddenDId"), HiddenField)
                    Dim pParms(4) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@CATEGORY_ID", catid.Value)
                    pParms(1) = New SqlClient.SqlParameter("@PARENT_TASK_CATEGORY_ID", "0")
                    pParms(2) = New SqlClient.SqlParameter("@SUB_TAB_ID", ddsubtab.SelectedValue)
                    pParms(3) = New SqlClient.SqlParameter("@MAIN_CATEGORY_ID", val)
                    lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "INSERT_TASK_CATEGORY_MASTER", pParms)

                End If

            Next

        Else
            lblmessage.Text = "Please select a main category."

        End If


        BindCategory()
        BindJobCategory()

    End Sub

    'Protected Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
    '    lblmessage.Text = ""
    '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

    '    Dim val = ""
    '    For Each node As TreeNode In TreeJobCategory.Nodes
    '        If node.Checked Then
    '            val = node.Value
    '            node.Checked = False
    '            Exit For
    '        Else
    '            val = GetNodeValue(node)
    '            If val <> "" Then
    '                Exit For
    '            End If
    '        End If
    '    Next

    '    If val <> "" Then

    '        Dim pParms(1) As SqlClient.SqlParameter
    '        pParms(0) = New SqlClient.SqlParameter("@TASK_CATEGORY_ID", val)
    '        lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "DELETE_TASK_CATEGORY_MASTER", pParms)
    '        BindJobCategory()
    '    Else
    '        lblmessage.Text = "Please select a Task Category for deletion."

    '    End If


    'End Sub


    Protected Sub btnsave0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave0.Click
        If txtcategory.Text <> "" Then

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblmessage.Text = ""
            Try

                Dim val = ""
                For Each node As TreeNode In TreeItemCategory.Nodes
                    If node.Checked Then
                        val = node.Value
                        node.Checked = False
                        Exit For
                    Else
                        val = GetNodeValue(node)
                        If val <> "" Then
                            Exit For
                        End If
                    End If
                Next

                If val = "" Then
                    val = 0
                End If

                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@MAIN_CATEGORY_DESC", txtcategory.Text.Trim())
                pParms(1) = New SqlClient.SqlParameter("@PRIMARY_MAIN_CATEGORY_ID", val)
                pParms(2) = New SqlClient.SqlParameter("@SUB_TAB_ID", ddsubtab.SelectedValue)
                pParms(3) = New SqlClient.SqlParameter("@OPTIONS", 1)
                lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_TASK_MAIN_CATEGORY", pParms)
                txtcategory.Text = ""
                transaction.Commit()
                BindCategory()
                BindJobCategory()

            Catch ex As Exception
                transaction.Rollback()

            Finally
                connection.Close()

            End Try

        End If

    End Sub

    Protected Sub btnDelete0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete0.Click
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblmessage.Text = ""
        Try

            Dim val = ""
            For Each node As TreeNode In TreeItemCategory.Nodes
                If node.Checked Then
                    val = node.Value
                    node.Checked = False
                    Exit For
                Else
                    val = GetNodeValue(node)
                    If val <> "" Then
                        Exit For
                    End If
                End If
            Next

            Dim pParms(4) As SqlClient.SqlParameter

            If val.Contains("-") Then
                pParms(0) = New SqlClient.SqlParameter("@LEVEL_ID", val.Split("-")(0))
                pParms(1) = New SqlClient.SqlParameter("@TASK_CATEGORY_ID", val.Split("-")(1))
                lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "DELETE_TASK_CATEGORY_MASTER", pParms)
            Else
                pParms(0) = New SqlClient.SqlParameter("@MAIN_CATEGORY_ID ", val)
                pParms(1) = New SqlClient.SqlParameter("@OPTIONS", 3)
                lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_TASK_MAIN_CATEGORY", pParms)
            End If


            txtcategory.Text = ""
            transaction.Commit()
            BindCategory()
            BindJobCategory()

        Catch ex As Exception
            transaction.Rollback()

        Finally
            connection.Close()
        End Try

    End Sub

End Class
