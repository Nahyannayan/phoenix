Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI
Imports System.Data.SqlClient
Partial Class HelpDesk_UserControls_hdOutbox
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenEmpid.Value = Session("EmployeeId")
            BindOutbox()
        End If

    End Sub

    Public Sub BindOutbox()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EMP_ID", HiddenEmpid.Value)

        Dim ds As New DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_OUTBOX_DATA", pParms)
        GridOutbox.DataSource = ds
        GridOutbox.DataBind()


    End Sub

    Protected Sub GridOutbox_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridOutbox.PageIndexChanging

        GridOutbox.PageIndex = e.NewPageIndex
        BindOutbox()

    End Sub

End Class
