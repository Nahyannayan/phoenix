<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdChangeTaskCategory.ascx.vb" Inherits="HelpDesk_UserControls_hdChangeTaskCategory" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<div class="matters" align="center">
    <ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>

<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="200">
    <tr>
        <td class="subheader_img">
            Change Task Category</td>
    </tr>
    <tr>
        <td align="left">
            <table>
                <tr>
                    <td>
                        Main&nbsp;Tab</td>
                    <td>
                        <asp:DropDownList ID="ddMaintab" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        Sub&nbsp;Tab</td>
                    <td>
                        <asp:DropDownList ID="ddsubtab" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Panel ID="Panel1" runat="server" Height="200px" ScrollBars="Vertical" Width="200px">
                            <asp:RadioButtonList ID="RadioCategory" runat="server">
                            </asp:RadioButtonList></asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnupdate" runat="server" CssClass="button" Text="Update" Width="116px" OnClick="btnupdate_Click" /> <asp:Button ID="btncancel" runat="server" CssClass="button" OnClientClick="javascript:window.close(); return false;" Text="Close" Width="116px" /></td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenTaskListID" runat="server" /><ajaxToolkit:ConfirmButtonExtender id="CF2" runat="server" ConfirmText="Assigning new cagegory will remove current ownership.Do you wish to continuie?" TargetControlID="btnupdate"></ajaxToolkit:ConfirmButtonExtender>
</contenttemplate>
    </asp:UpdatePanel>&nbsp;&nbsp;

</div>