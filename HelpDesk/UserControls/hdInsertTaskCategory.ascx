<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdInsertTaskCategory.ascx.vb" Inherits="HelpDesk_UserControls_hdInsertTaskCategory" %>

<link type="text/css" href="../../cssfiles/sb-admin.css" rel="stylesheet" />
     <link type="text/css" href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />

<script type="text/javascript" >

function OnTreeClick(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");

if(isChkBoxClick)
{
                        for(i=0; i<document.forms[0].elements.length; i++)
                           {
                               var currentid =document.forms[0].elements[i].id; 
                               if(document.forms[0].elements[i].type=="checkbox" && currentid != event.srcElement.id)
                                  {
                                    document.forms[0].elements[i].checked=false;
                                  }
                            }
}

}
</script>

<div class="matters">

 <table  Width="100%" >
                        <tr>
                            <td class="title-bg">
                                Insert Task Category</td>
                        </tr>
                        <tr>
                            <td >

<table>
    <tr>
        <td  align="left" width="20%"><span class="field-label">
            Main Tab </span></td>
        <td align="left" width="30%">
<asp:DropDownList ID="ddMaintab" runat="server" AutoPostBack="True">
</asp:DropDownList>
            <br />
            Internal-Within GEMS Organization, External- Not in GEMS Organization eg:Parents</td>
        <td align="left" width="20%"><span class="field-label">
            Departments </span></td>
        <td align="left" width="30%"><asp:DropDownList ID="ddsubtab" runat="server" AutoPostBack="True">
</asp:DropDownList></td>
    </tr>
    
    <tr>
        <td valign="top" > <span class="field-label">
            Add Category </span></td>
        <td >
            
                <table>
                    <tr>
                        <td>
                            <div class="checkbox-list">
                            <asp:TreeView ID="TreeItemCategory" runat="server" height="100px"
                                 ImageSet="Arrows" onclick="OnTreeClick(event);" ShowCheckBoxes="All" >
                                
                                
                                <SelectedNodeStyle  HorizontalPadding="0px" VerticalPadding="0px" />
                                
                            </asp:TreeView>
                                </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtcategory" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnsave0" CssClass="button" runat="server" Width="80px" Text="Add" />
                            <asp:Button ID="btnUpdate" CssClass="button" runat="server" Width="80px" Visible="False" Text="Update" />
                            <asp:Button ID="btnDelete0" CssClass="button" runat="server" Width="80px" Text="Delete" />
                <ajaxToolkit:ConfirmButtonExtender ID="btnDelete0_ConfirmButtonExtender" 
                                ConfirmText="Do you wish to continue?" TargetControlID="btnDelete0" 
                                runat="server"></ajaxToolkit:ConfirmButtonExtender> 
               
                        </td>
                    </tr>
                    <tr>
                    <td>
                    
<asp:Label ID="lblmessage" runat="server" class="error "></asp:Label>
                    
                    </td>
                    </tr>
                    </table>
                    </td>
                    <td valign="top" align="left" colspan="2">
            <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Width="100%"  >
                <asp:GridView id="GridCategory" runat="server" Width="100%"  AutoGenerateColumns="false" AllowPaging ="true" PageSize="10" ShowFooter="True" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Check">
                            <HeaderTemplate>
                                Check
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:CheckBox ID="CheckCatogory" runat="server" />
                                </center>
                            </ItemTemplate>
                            <FooterTemplate>
                            <asp:Button ID="btnadd" CssClass="button" runat="server" OnClick="addbasecategory" Text="Add" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <HeaderTemplate>
                                Category
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddenDId" runat="server" Value='<%#Eval("ID")%>'  />
                                <%#Eval("CATEGORY_DES")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle Wrap="False"  />
                    <EmptyDataRowStyle Wrap="False"  />
                   <%-- <SelectedRowStyle CssClass="Green" Wrap="False"  />--%>
                    <HeaderStyle  Wrap="False"  />
                    <EditRowStyle Wrap="False"  />
                    <AlternatingRowStyle Wrap="False"  />
                </asp:GridView>
            </asp:Panel>
                    </td>
                
    </tr>
    
    </table>

 </td>
                    </tr>
               </table>
               
</div>