Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI
Imports System.Data.SqlClient

Partial Class HelpDesk_UserControls_hdEnterMessage
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenTaskAssignid.Value = Request.QueryString("TaskAssignid")
            HiddenFromEmpId.Value = Request.QueryString("FromEmpId")
            HiddenToEmpid.Value = Request.QueryString("ToEmpid")
            HiddenParent_Message_Id.Value = Request.QueryString("Parent_Message_Id")
            BindControls()
            BindToDetails()
            BindPreviousResponseDetails()
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub


    Public Sub BindPreviousResponseDetails()

        If HiddenTaskAssignid.Value <> "0" And HiddenParent_Message_Id.Value = "0" Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim Sql_Query = " SELECT ENTRY_DATE, isnull(emp_displayname,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_LNAME, '')) AS EMP_NAME,TASK_ASSIGN_DESCRIPTION,TASK_ASSIGNED_TO_EMP_ID,TASK_ASSIGN_START_DATE,TASK_ASSIGN_END_DATE,TASK_ASSIGN_PRIORITY,UPLOAD_IDS FROM TASK_ASSIGNED_LOG A " & _
                                   " LEFT JOIN OASIS.dbo.EMPLOYEE_M B ON A.TASK_ASSIGNED_TO_EMP_ID=B.EMP_ID " & _
                                   " WHERE TASK_ASSIGN_ID='" & HiddenTaskAssignid.Value & "'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

            If ds.Tables(0).Rows.Count > 0 Then

                txtdetails.Content = "<br><br><br><br><hr>-Assigned Message (Below)-<hr><br>" & _
                                                "<b>Assigned To :" & ds.Tables(0).Rows(0).Item("EMP_NAME").ToString() & _
                                                "<br>Date     :" & ds.Tables(0).Rows(0).Item("ENTRY_DATE").ToString() & _
                                                "</b><br><br>" & _
                                                ds.Tables(0).Rows(0).Item("TASK_ASSIGN_DESCRIPTION").ToString()
            End If


        Else
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim sqlquery = "Select MESSAGE_SUBJECT, isnull(emp_displayname,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_LNAME, '')) EMPNAME,MESSAGE_TEXT,CONVERT(VARCHAR(20), ENTRY_DATE, 100)ENTRY_DATE from MESSAGE_LOG A INNER JOIN OASIS.dbo.EMPLOYEE_M B ON A.FROM_EMP_ID=B.EMP_ID where MESSAGE_ID='" & HiddenParent_Message_Id.Value & "'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sqlquery)


            If ds.Tables(0).Rows.Count > 0 Then
                txtdetails.Content = " <br><br><br><br><hr>-Previous Message (Below)-<hr><br>" & _
                                                 "<b>Subject  :" & ds.Tables(0).Rows(0).Item("MESSAGE_SUBJECT").ToString() & _
                                                 "<br>From     :" & ds.Tables(0).Rows(0).Item("EMPNAME").ToString() & _
                                                 "<br>Date     :" & ds.Tables(0).Rows(0).Item("ENTRY_DATE").ToString() & _
                                                 "</b><br> <br>" & _
                                                  ds.Tables(0).Rows(0).Item("MESSAGE_TEXT").ToString()
            End If

        End If
        txtdetails.Focus()

    End Sub

    Public Sub BindToDetails()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim sqlquery = " select isnull(emp_displayname,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_LNAME, '')) EMPNAME,ISNULL(EMD_EMAIL,' (Email Id is missing. Email will not be sent)') EMD_EMAIL from OASIS.dbo.EMPLOYEE_M A " & _
                       " LEFT JOIN OASIS.dbo.EMPLOYEE_D B ON A.EMP_ID= B.EMD_EMP_ID " & _
                       " WHERE A.EMP_ID='" & HiddenToEmpid.Value & "' "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sqlquery)

        If ds.Tables(0).Rows.Count > 0 Then
            lblFrom.Text = ds.Tables(0).Rows(0).Item("EMPNAME").ToString() & " - " & ds.Tables(0).Rows(0).Item("EMD_EMAIL").ToString()
        End If




    End Sub
    Public Sub BindControls()

        If HiddenParent_Message_Id.Value = "0" Then
            lbltext.Text = "Compose"
        Else
            lbltext.Text = "Reply"
        End If
    End Sub


    Protected Sub btnsend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsend.Click
        Dim commitflag = 0

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FROM_EMP_ID", HiddenFromEmpId.Value)
            pParms(1) = New SqlClient.SqlParameter("@TO_EMP_ID", HiddenToEmpid.Value)

            If HiddenTaskAssignid.Value <> "0" Then
                pParms(2) = New SqlClient.SqlParameter("@TASK_ASSIGNED_ID", HiddenTaskAssignid.Value)
            End If

            pParms(3) = New SqlClient.SqlParameter("@MESSAGE_SUBJECT", txtsubject.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@MESSAGE_TEXT", txtdetails.Content)

            If HiddenParent_Message_Id.Value <> "0" Then
                pParms(5) = New SqlClient.SqlParameter("@PARENT_MESSAGE_ID", HiddenParent_Message_Id.Value)
            End If

            pParms(6) = New SqlClient.SqlParameter("@HIGH_PRIORITY", CheckHighPriority.Checked)

            If DirectCast(HdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value <> "" Then
                pParms(7) = New SqlClient.SqlParameter("@UPLOAD_IDS", DirectCast(HdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value)
            End If

            Dim MessageId = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_MESSAGE_LOG", pParms)

            transaction.Commit()
            commitflag = 1
            If commitflag = 1 Then

                ''Send Message


                Dim ds As DataSet
                ds = HelpDesk.GetCommunicationSettings()
                Dim username = ""
                Dim password = ""
                Dim port = ""
                Dim host = ""
                Dim fromemailid = ""
                If ds.Tables(0).Rows.Count > 0 Then

                    username = ds.Tables(0).Rows(0).Item("USERNAME").ToString()
                    password = ds.Tables(0).Rows(0).Item("PASSWORD").ToString()
                    port = ds.Tables(0).Rows(0).Item("PORT").ToString()
                    host = ds.Tables(0).Rows(0).Item("HOST").ToString()
                    fromemailid = ds.Tables(0).Rows(0).Item("FROMEMAIL").ToString()

                End If

                Dim sb As New Object
                Dim pParms1(3) As SqlClient.SqlParameter
                pParms1(0) = New SqlClient.SqlParameter("@TASK_ASSIGN_ID", HiddenTaskAssignid.Value)
                pParms1(1) = New SqlClient.SqlParameter("@MESSAGE_ID", MessageId)
                pParms1(2) = New SqlClient.SqlParameter("@OPTION", 5)
                sb = SqlHelper.ExecuteScalar(connection, CommandType.StoredProcedure, "HD_MAIL_TEXT", pParms1)


                HelpDesk.SendPlainTextEmails(fromemailid, HelpDesk.GetEmployeeEmailId(HiddenToEmpid.Value), txtsubject.Text, sb.ToString(), username, password, host, port, HelpDesk.GetuploadIds(MessageId, 3))
                txtsubject.Text = ""
                txtdetails.Content = ""
                CheckHighPriority.Checked = False
                DirectCast(HdFileUpload1.FindControl("GridUpload"), GridView).Controls.Clear()
                DirectCast(HdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value = ""
                lblmessage.Text = "Message Send Successfully."

            End If

        Catch ex As Exception

            If commitflag = 0 Then
                transaction.Rollback()
            End If

            lblmessage.Text = "Error occured while saving . " & ex.Message

        End Try

    End Sub


End Class
