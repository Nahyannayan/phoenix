<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskAssignEmpEnter.ascx.vb" Inherits="HelpDesk_UserControls_hdTaskAssignEmpEnter" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="hdFileUploadViewAssign.ascx" TagName="hdFileUploadViewAssign" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>

<div class="matters" >

<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
    <tr>
        <td class="subheader_img">
            Enter Assign Text</td>
    </tr>
    <tr>
        <td align="center" >
<telerik:radeditor id="txtdetails" runat="server" editmodes="Design" toolsfile="../xml/FullSetOfTools.xml"
    width="600px"></telerik:radeditor>
            <br />

<uc1:hdFileUploadViewAssign ID="HdFileUploadViewAssign1" ShowCheck="true"  runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Requested Date:
            <asp:TextBox ID="txtStartdate" runat="server" Width="100px"></asp:TextBox>&nbsp;
            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                OnClientClick="return false;" />
            &nbsp; &nbsp;&nbsp; Requested Completion Date: &nbsp;<asp:TextBox ID="txtEnddate" runat="server"
                Width="100px"></asp:TextBox>&nbsp;
            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif"
                OnClientClick="return false;" />
        </td>
    </tr>
    <tr>
        <td>
            Priority
            <asp:DropDownList ID="ddPriority" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
<asp:CheckBox ID="CheckSMS" runat="server" Text="Sms Notification" /></td>
    </tr>
    <tr>
        <td align="center">
<asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" Width="100px" /><asp:Button
    ID="btnwcancel" runat="server" CssClass="button" OnClientClick="javascript:window.close();"
    Text="Close" Width="100px" />
            <br />
<asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></td>
    </tr>
</table>
<br />
<asp:HiddenField ID="HiddenSubTabID" runat="server" />
<asp:HiddenField ID="HiddenEmpID" runat="server" />
<asp:HiddenField ID="HiddenTaskCategory" runat="server" />
<asp:HiddenField ID="HiddenTaskListID" runat="server" />
<asp:HiddenField ID="HiddenBsuid" runat="server" />
<asp:HiddenField ID="HiddenPriorityId" runat="server" />
<asp:HiddenField ID="HiddenTitle" runat="server" />
<ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"   Format="dd/MMM/yyyy"
    PopupButtonID="ImageButton1" TargetControlID="txtStartdate">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
    PopupButtonID="ImageButton2" TargetControlID="txtEnddate">
</ajaxToolkit:CalendarExtender>

</div>