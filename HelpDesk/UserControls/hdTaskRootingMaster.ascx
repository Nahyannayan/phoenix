<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskRootingMaster.ascx.vb"
    Inherits="HelpDesk_UserControls_hdTaskRootingMaster" %>

<link type="text/css" href="../../cssfiles/sb-admin.css" rel="stylesheet" />
<link type="text/css" href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />

<script type="text/javascript">


function OnTreeClick1(evt)
{ 
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");

if(isChkBoxClick)
{
                        for(i=0; i<document.forms[0].elements.length; i++)
                           {
                               var currentid =document.forms[0].elements[i].id;  
                               if (currentid.indexOf("TreeJobCategory") >-1)
                               {
                                if(document.forms[0].elements[i].type=="checkbox" && currentid != event.srcElement.id)
                                  {
                                 
                                    document.forms[0].elements[i].checked=false;

                                  }
                                }
                            }
                          
                            
}

document.getElementById('<% = Panel1.ClientID %>').style.visibility="hidden";
document.getElementById('<% = btnSave.ClientID %>').style.visibility="hidden";

}

function saveflagset()
{
document.getElementById('<% = HiddenSaveFlag.ClientID %>').value =1;
}

 function ViewAssigned(main_tab,sub_tab,bsu_id,cat_id)
 {
    window.open('hdViewOwnerMembers.aspx?main_tab='+ main_tab +"&sub_tab="+ sub_tab +"&cat_id=" +cat_id +"&bsu_id=" +bsu_id , '','Height=650px,Width=750px,scrollbars=yes,resizable=no,directories=yes');
    return false;
 }

</script>

 <script type="text/javascript" >
      function change_chk_stateg(chkThis,cvalu)
             {
                    var chk_state= ! chkThis.checked ;
                     for(i=0; i<document.forms[0].elements.length; i++)
                           {
                           var currentid =document.forms[0].elements[i].id; 
                           if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf(cvalu)!=-1)
                         {
                          
                                document.forms[0].elements[i].checked=chk_state;
                                 document.forms[0].elements[i].click();
                             }
                          }
              }
    
    </script>
    
<div class="matters">%
 <table  cellpadding="5" cellspacing="0" Width="700px" >
                        <tr>
                            <td class="title-bg">
                                Task Routing / Email / SMS Notification Master
                                <asp:Button ID="btnreset" runat="server" CssClass="button" Text="Reset" Width="100px" /></td>
                        </tr>
                        <tr>
                            <td >


    <table>
        <tr>
            <td>
                Main Tab</td>
            <td>
                :</td>
            <td>
                <asp:DropDownList ID="ddMainTab" runat="server" AutoPostBack="True">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td>
                Departments</td>
            <td>
                :</td>
            <td>
                <asp:DropDownList ID="ddSubTab" runat="server" AutoPostBack="True">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td>
                Task Category</td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                </td>
            <td>
                <asp:Panel ID="Panel2" runat="server" ScrollBars="Auto" Height="200px" >
                <asp:TreeView ID="TreeJobCategory" runat="server"  BorderColor="#404040" BorderStyle="Solid"
                    BorderWidth="1px" ImageSet="Arrows" onclick="OnTreeClick1(event);" ShowCheckBoxes="All"  >
                    <ParentNodeStyle Font-Bold="False" />
                    <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                    <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px"
                        VerticalPadding="0px" />
                    <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="5px"
                        NodeSpacing="0px" VerticalPadding="0px" />
                </asp:TreeView>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:Button ID="btnassign" runat="server" CssClass="button" Text="Set" Width="100px" />
                <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" Width="100px" /></td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" BorderStyle="Solid" Height="200px"
                    Width="100%" BorderColor="DarkGray">
                    
                    <asp:GridView ID="GridRootingBsu"  AutoGenerateColumns="false" Width="100%"  runat="server" CssClass="table table-bordered table-row">
                    <Columns>
                    <asp:TemplateField HeaderText="Business Unit">
                     <HeaderTemplate>
                               
                                           Business Unit
                                         
                            </HeaderTemplate>
                    <ItemTemplate>
                    <%#Eval("BSU_NAME")%>
                    <asp:HiddenField ID="HiddenBsuid" Value='<%#Eval("BSU_ID") %>' runat="server" />
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Owner">
                     <HeaderTemplate>
                               
                                           Owner
                                           <br />
                                           <asp:CheckBox ID="chkAll1"  runat="server"  onclick="javascript:change_chk_stateg(this,'CheckOwner');" ToolTip="Click here to select/deselect all rows" />
                                        
                            </HeaderTemplate>
                    <ItemTemplate>
                    <center><asp:CheckBox ID="CheckOwner" Checked='<%#Eval("TASK_CATEGORY_OWNER") %>' runat="server" /></center>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Member">
                     <HeaderTemplate>
                               
                                           Member
                                           <br />
                                           <asp:CheckBox ID="chkAll2"  runat="server"  onclick="javascript:change_chk_stateg(this,'CheckMember');" ToolTip="Click here to select/deselect all rows" />
                                        
                            </HeaderTemplate>
                    <ItemTemplate>
                    <center><asp:CheckBox ID="CheckMember" Checked='<%#Eval("TASK_MEMBER") %>' runat="server" /></center>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email">
                     <HeaderTemplate>
                               
                                           Email
                                           <br />
                                           <asp:CheckBox ID="chkAll3"  runat="server"  onclick="javascript:change_chk_stateg(this,'CheckEmail');" ToolTip="Click here to select/deselect all rows" />
                                        
                            </HeaderTemplate>
                    <ItemTemplate>
                    <center><asp:CheckBox ID="CheckEmail" Checked='<%#Eval("EMAIL_NOTIFY") %>' runat="server" /></center>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SMS">
                     <HeaderTemplate>
                              
                                           SMS
                                           <br />
                                           <asp:CheckBox ID="chkAll4"  runat="server"  onclick="javascript:change_chk_stateg(this,'CheckSMS');" ToolTip="Click here to select/deselect all rows" />
                                        
                            </HeaderTemplate>
                    <ItemTemplate>
                    <center><asp:CheckBox ID="CheckSMS" Checked='<%#Eval("SMS_NOTIFY") %>' runat="server" /></center>
                    </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Reassign">
                     <HeaderTemplate>
                              
                                           Reassign
                                           <br />
                                           <asp:CheckBox ID="chkAll5"  runat="server"  onclick="javascript:change_chk_stateg(this,'CheckReassign');" ToolTip="Click here to select/deselect all rows" />
                                       
                            </HeaderTemplate>
                    <ItemTemplate>
                    <center><asp:CheckBox ID="CheckReassign" Checked='<%#Eval("CAN_RESIGN") %>' runat="server" /></center>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="View">
                         <HeaderTemplate>
                               
                            </HeaderTemplate>
                    <ItemTemplate>
                    <center>
                      <asp:ImageButton ID="ImageReAssign" OnClientClick='<%#Eval("VIEW_ASSIGNED")%>' ToolTip="View other Owners and Members." Width="20px" Height="20px" ImageUrl="~/Images/Helpdesk/Reassign.png" runat="server" />
                     </center>
                    </ItemTemplate>
                    <ItemStyle Width="50px" />
                    </asp:TemplateField>
                    </Columns>
                     <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                     <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                     <SelectedRowStyle CssClass="Green" Wrap="False" />
                     <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                     <EmptyDataRowStyle Wrap="False" />
                     <EditRowStyle Wrap="False" />
                    </asp:GridView>
                    </asp:Panel>
                </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <asp:Button ID="btnSave" runat="server" CssClass="button" OnClientClick="javascript:saveflagset();" Text="Save" Width="100px" /></td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                &nbsp;<asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></td>
        </tr>
    </table>
    
    
 </td>
                    </tr>
               </table>
    
</div>
<asp:HiddenField ID="HiddenSaveFlag" Value="0" runat="server" /><asp:HiddenField ID="HiddenEmp_id" Value="0" runat="server" />

