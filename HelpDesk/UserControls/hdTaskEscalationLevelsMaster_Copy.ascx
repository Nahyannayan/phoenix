﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskEscalationLevelsMaster_Copy.ascx.vb"
    Inherits="HelpDesk_UserControls_hdTaskEscalationLevelsMaster_Copy" %>

<script language="javascript" type="text/javascript">

    function OnTreeClick(evt) {
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
        if (isChkBoxClick) {
            var parentTable = GetParentByTagName("table", src);
            var nxtSibling = parentTable.nextSibling;
            if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
            {
                if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                {
                    //check or uncheck children at all levels
                    CheckUncheckChildren(parentTable.nextSibling, src.checked);
                }
            }
            //check or uncheck parents at all levels
            CheckUncheckParents(src, src.checked);
        }
    }

    function CheckUncheckChildren(childContainer, check) {
        var childChkBoxes = childContainer.getElementsByTagName("input");
        var childChkBoxCount = childChkBoxes.length;
        for (var i = 0; i < childChkBoxCount; i++) {
            childChkBoxes[i].checked = check;
        }
    }

    function CheckUncheckParents(srcChild, check) {
        var parentDiv = GetParentByTagName("div", srcChild);
        var parentNodeTable = parentDiv.previousSibling;

        if (parentNodeTable) {
            var checkUncheckSwitch;

            if (check) //checkbox checked
            {
                var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                if (isAllSiblingsChecked)
                    checkUncheckSwitch = true;
                else
                    return; //do not need to check parent if any child is not checked
            }
            else //checkbox unchecked
            {
                checkUncheckSwitch = false;
            }

            var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
            if (inpElemsInParentTable.length > 0) {
                var parentNodeChkBox = inpElemsInParentTable[0];
                parentNodeChkBox.checked = checkUncheckSwitch;
                //do the same recursively
                CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
            }
        }
    }

    function AreAllSiblingsChecked(chkBox) {
        var parentDiv = GetParentByTagName("div", chkBox);
        var childCount = parentDiv.childNodes.length;
        for (var i = 0; i < childCount; i++) {
            if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
            {
                if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                    var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                    //if any of sibling nodes are not checked, return false
                    if (!prevChkBox.checked) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    //utility function to get the container of an element by tagname
    function GetParentByTagName(parentTagName, childElementObj) {
        var parent = childElementObj.parentNode;
        while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
            parent = parent.parentNode;
        }
        return parent;
    }


    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("Checkbsu") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }


</script>

    <div class="matters" >
    
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" 
    width="700">
    <tr>
        <td class="subheader_img">
         Task Routing Master
        </td>
    </tr>
    <tr>
        <td align="left">
          
          <table>
    <tr>
        <td>
            Business Unit
        </td>
        <td>
            :</td>
        <td>
            <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>
    </tr>
    <tr id="RowEmpName" runat = "server" visible = "false">
        <td>
            Employee Name
        </td>
        <td>
            :</td>
        <td>
            <asp:DropDownList ID="ddemp" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            Main Tab</td>
        <td>
            :</td>
        <td>
<asp:DropDownList ID="ddMaintab" runat="server" AutoPostBack="True">
</asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            Departments</td>
        <td>
            :</td>
        <td>
<asp:DropDownList ID="ddsubtab" runat="server" AutoPostBack="True">
</asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td valign="top">
            Categories
        </td>
        <td>
            &nbsp;
        </td>
        <td valign="top">
                            <asp:TreeView ID="TreeItemCategory" runat="server" BorderColor="#404040" onclick="OnTreeClick(event);"  BorderStyle="Solid"
                                BorderWidth="1px" ImageSet="Msdn"  ShowCheckBoxes="All" ShowLines="True" >
                                <ParentNodeStyle Font-Bold="False" />
                                <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                                <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px"
                                    VerticalPadding="0px" />
                                <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="5px"
                                    NodeSpacing="0px" VerticalPadding="0px" />
                            </asp:TreeView>
        </td>
    </tr>
    <tr id = "RowBusinessUnit" runat = "server" visible = "false">
        <td valign="top">
            Business Unit (Access)
        </td>
        <td>
            &nbsp;
        </td>
        <td valign="top">
            <asp:GridView ID="GridRootingBsu" runat="server" AutoGenerateColumns="false" EnableTheming="false"
                Width="95%">
                <Columns>
                    <asp:TemplateField HeaderText="Owner">
                        <HeaderTemplate>
                            <center>
                        
                                <asp:CheckBox ID="chkAll1" runat="server" onclick="javascript:change_chk_stateg(this);"
                                    ToolTip="Click here to select/deselect all rows" />
                            </center>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:CheckBox ID="Checkbsu" runat="server"  />
                            </center>
                        </ItemTemplate>
                        <ItemStyle Width="60px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Business Unit">
                        <HeaderTemplate>
                            <span style="font-size: small">Business Unit</span>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span style="font-size: small">
                                <%#Eval("BSU_NAME")%></span>
                            <asp:HiddenField ID="HiddenBsuid" runat="server" Value='<%#Eval("BSU_ID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="gridheader_pop" Font-Size="Small" Height="20px" Wrap="False" />
                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <EditRowStyle Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td>
            Escalation Levels</td>
        <td>
            &nbsp;</td>
        <td>
            Level 1&nbsp;
            <asp:TextBox ID="TextBox1" runat="server" Width="50px" BackColor="Yellow">7</asp:TextBox>
&nbsp;Days After Expiry of Completion Date</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            Level 2&nbsp;
            <asp:TextBox ID="TextBox2" runat="server" Width="50px" BackColor="#D39600">14</asp:TextBox>
&nbsp;Days After Expiry of Completion Date</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            Level 3&nbsp;
            <asp:TextBox ID="TextBox3" runat="server" Width="50px" BackColor="Red">30</asp:TextBox>
&nbsp;Days After Expiry of Completion Date</td>
    </tr>
    <tr id = "RowAccessLevels" runat = "server" visible ="false">
        <td>
            Access
            Levels
        <td>
            :
        </td>
        <td>
            <asp:CheckBoxList ID="CheckAccess" RepeatDirection="Horizontal" runat="server">
                <asp:ListItem Text="Owner" Value="O"></asp:ListItem>
                <asp:ListItem Text="Member" Value="M"></asp:ListItem>
                <asp:ListItem Text="Email (Owner)" Value="E"></asp:ListItem>
                <asp:ListItem Text="SMS (Owner)" Value="S"></asp:ListItem>
                <asp:ListItem Text="Reassign" Value="R"></asp:ListItem>
            </asp:CheckBoxList>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
        <td>
            <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" Width="100px" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
                    
<asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                    
        </td>
    </tr>
</table>
          
          </td>
    </tr>
</table>


    <asp:HiddenField ID="Hiddensubtabids" runat="server" />
    <asp:HiddenField ID="Hiddentoplevelcatids" runat="server" />
    <asp:HiddenField ID="Hiddentaskcatids" runat="server" />
    <asp:HiddenField ID="HiddenType" runat="server" />



  </div>