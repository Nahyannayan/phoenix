Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI

Partial Class HelpDesk_UserControls_hdStatusUpdate
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenAssignedTaskid.Value = Request.QueryString("AssignedTaskid")
            HiddenEmpID.Value = Session("EmployeeId")
            BindPercentage()
            BindStatus()
            BindFinerIssuesAndCheckUploadRequire()
            

        End If

        txtdetails.Focus()
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnok)

        Dim connection As New System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Dim dt As Object = SqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString, CommandType.Text, "Select  EXPECTED_COMPLETION_DATE From Task_List_Master Where Task_List_Id = (SELECT TASK_LIST_ID FROM dbo.TASK_ASSIGNED_LOG WHERE TASK_ASSIGN_ID = " & HiddenAssignedTaskid.Value & ")")
        If Not IsDBNull(dt) Then
            txtdate.Text = Format(dt, "dd/MMM/yyyy")
            txtdate.Enabled = False
            txtdate.ReadOnly = True
            imgAsOn.Visible = False
            lblAlert.Text = "Not editable"
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Public Sub BindPercentage()

        Dim i = 0

        For i = 1 To 99
            Dim list As New ListItem
            list.Value = i
            list.Text = i
            ddpercentage.Items.Insert(i - 1, i)
        Next
    End Sub

    Public Sub BindStatus()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT * FROM TASK_STATUS_MASTER where STATUS_ID NOT IN (1,2,6,7,8)"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddstatus.DataSource = ds
        ddstatus.DataTextField = "TASK_STATUS_DESCRIPTION"
        ddstatus.DataValueField = "STATUS_ID"
        ddstatus.DataBind()
    End Sub

    Public Sub BindFinerIssuesAndCheckUploadRequire()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim params(0) As SqlClient.SqlParameter
        params(0) = New SqlClient.SqlParameter("@TASK_ASSIGN_ID", HiddenAssignedTaskid.Value)
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_TASK_FINER_ISSUES", params(0))
            ddFinerIssues.DataSource = ds
            ddFinerIssues.DataTextField = "ISSUE_DESCR"
            ddFinerIssues.DataValueField = "FINER_ISSUE_ID"
            ddFinerIssues.DataBind()

            Dim tmpCount As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "CHECK_TASK_REQUIRE_UPLOAD", params(0))
            If tmpCount > 0 Then
                Me.chkUpload.Checked = True
            Else
                Me.chkUpload.Checked = False
            End If
            Me.btnok.Visible = True
        Catch ex As Exception
            Me.lblmessage.Text = "Error occured while loading the data"
            Me.btnok.Visible = False
        End Try
        
    End Sub

    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnok.Click

        If ValidateDate() Then

            Dim uploadids = ""
            Dim connection As New System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
            Dim transaction As System.Data.SqlClient.SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            Dim commit = 0
            Try
                Dim per = ""
                If ddstatus.SelectedValue = 5 Then
                    per = 100
                Else
                    per = ddpercentage.SelectedItem.Text
                End If
                Dim pParms(8) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TASK_ASSIGN_ID", HiddenAssignedTaskid.Value)
                pParms(1) = New SqlClient.SqlParameter("@TASK_STATUS_DESCRIPTION", txtdetails.Content)
                pParms(2) = New SqlClient.SqlParameter("@TASK_STATUS_ID", ddstatus.SelectedValue)
                pParms(3) = New SqlClient.SqlParameter("@TASK_EMP_ID", HiddenEmpID.Value)
                pParms(4) = New SqlClient.SqlParameter("@PERCENTAGE", per)
                If txtdate.Text <> "" Then
                    pParms(5) = New SqlClient.SqlParameter("@EXP_END_DATE", txtdate.Text)
                End If

                If DirectCast(hdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value <> "" Then
                    uploadids = DirectCast(hdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value
                    pParms(6) = New SqlClient.SqlParameter("@UPLOAD_IDS", uploadids)
                End If

                If Me.ddFinerIssues.SelectedIndex >= 0 Then
                    pParms(7) = New SqlClient.SqlParameter("@FINER_ISSUE_ID", Me.ddFinerIssues.SelectedValue)
                End If

                SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_TASK_STATUS_LOG", pParms)
                transaction.Commit()
                DirectCast(hdFileUpload1.FindControl("GridUpload"), GridView).Controls.Clear()
                DirectCast(hdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value = ""

                commit = 1

            Catch ex As Exception

                transaction.Rollback()

            Finally
                connection.Close()
            End Try

            If commit = 1 Then
                SendMailtoAssigner(uploadids)
            End If

        End If

    End Sub

    Public Function ValidateDate() As Boolean
        ValidateDate = False

        If txtdate.Text = "" Then
            lblmessage.Text = "Please enter expected completion date"
            Return False
        End If

        If Not IsDate(txtdate.Text) Then
            lblmessage.Text = "Please enter valid expected completion date"
            Return False
        End If

        If txtdate.Text <> "" And lblAlert.Text <> "Not editable" Then

            If Convert.ToDateTime(txtdate.Text) < Today.Date Then
                lblmessage.Text = "Date is past. Please enter a new date"
                Return False

            End If

        End If

        If rowFinerIssues.Visible = True Then
            If ddFinerIssues.Items.Count > 0 Then
                If ddFinerIssues.SelectedIndex <= 0 Then
                    lblmessage.Text = "Please select valid finer issue detail"
                    Return False
                End If
            End If
        End If

        'If rowFinerIssues.Visible = True Then
        If Me.chkUpload.Checked = True Then
            If DirectCast(hdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value = "" Then
                lblmessage.Text = "Please make sure to attach valid support document for closing the task"
                Return False
            End If
        End If

        Return True
    End Function

    Public Sub SendMailtoAssigner(ByVal uploadids As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim ds As DataSet
        ds = HelpDesk.GetCommunicationSettings()

        Dim username = ""
        Dim password = ""
        Dim port = ""
        Dim host = ""
        Dim fromemailid = ""

        If ds.Tables(0).Rows.Count > 0 Then
            username = ds.Tables(0).Rows(0).Item("USERNAME").ToString()
            password = ds.Tables(0).Rows(0).Item("PASSWORD").ToString()
            port = ds.Tables(0).Rows(0).Item("PORT").ToString()
            host = ds.Tables(0).Rows(0).Item("HOST").ToString()
            fromemailid = ds.Tables(0).Rows(0).Item("FROMEMAIL").ToString()
        End If

        Dim Sql_Query = " select DISTINCT TASK_ASSIGNED_BY_EMP_ID,A.TASK_LIST_ID,TASK_TITLE from dbo.TASK_ASSIGNED_LOG A  " & _
                        " INNER JOIN dbo.TASK_LIST_MASTER B ON A.TASK_LIST_ID = B.TASK_LIST_ID  " & _
                        " INNER JOIN OASIS.dbo.EMPLOYEE_D F ON TASK_ASSIGNED_TO_EMP_ID=F.EMD_EMP_ID  " & _
                        " where task_assign_id='" & HiddenAssignedTaskid.Value & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        Dim title = ""

        If ds.Tables(0).Rows.Count > 0 Then

            Dim email = HelpDesk.GetEmployeeEmailId(ds.Tables(0).Rows(0).Item("TASK_ASSIGNED_BY_EMP_ID").ToString())
            Dim TASK_LIST_ID = ds.Tables(0).Rows(0).Item("TASK_LIST_ID").ToString()
            Dim TASK_ASSIGN_ID = HiddenAssignedTaskid.Value
            title = ds.Tables(0).Rows(0).Item("TASK_TITLE").ToString()

            Dim sb As New Object

            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", TASK_LIST_ID)
            pParms(1) = New SqlClient.SqlParameter("@TASK_ASSIGN_ID", TASK_ASSIGN_ID)
            pParms(2) = New SqlClient.SqlParameter("@OPTION", 4)
            sb = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "HD_MAIL_TEXT", pParms)

            Dim uploadid = uploadids
            If email <> "" Then
                HelpDesk.SendPlainTextEmails(fromemailid, email, TASK_LIST_ID & " - " & title, sb.ToString(), username, password, host, port, uploadid)
            End If

            btnok.Visible = False
            lblmessage.Text = "Status updated successfully."

        End If

    End Sub

    Protected Sub ddstatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddstatus.SelectedIndexChanged
        If ddstatus.SelectedValue = 5 Then
            'ddpercentage.SelectedValue = 100
            'ddpercentage.Enabled = False
            ddpercentage.Enabled = False
            TR1.Visible = False
            TR2.Visible = False
            'rowFinerIssues.Visible = True
            rowFinerIssues.Visible = False
            Me.txtdate.Text = Format(Now.Date, "dd/MMM/yyyy")
        Else
            ddpercentage.SelectedValue = 1
            ddpercentage.Enabled = True
            TR1.Visible = True
            TR2.Visible = True
            rowFinerIssues.Visible = False
            'Me.txtdate.Text = ""
        End If
    End Sub
End Class

