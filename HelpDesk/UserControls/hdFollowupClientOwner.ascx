<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdFollowupClientOwner.ascx.vb" Inherits="HelpDesk_UserControls_hdFollowupClientOwner" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="hdFileUpload.ascx" TagName="hdFileUpload" TagPrefix="uc1" %>

<script type="text/javascript" >

  function ViewDes(record_id)
        {
       
       // window.open('../Pages/hdFollowupClientView.aspx?Record_id=' + record_id  , '','dialogHeight:750px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        var url = "../Pages/hdFollowupClientView.aspx?Record_id=" + record_id ;
        var oWnd = radopen(url, "pop_DEDL");
  }

  function OnClientCloseDEDL(oWnd, args) {
  }
  function autoSizeWithCalendar(oWindow) {
      var iframe = oWindow.get_contentFrame();
      var body = iframe.contentWindow.document.body;

      var height = body.scrollHeight;
      var width = body.scrollWidth;

      var iframeBounds = $telerik.getBounds(iframe);
      var heightDelta = height - iframeBounds.height;
      var widthDelta = width - iframeBounds.width;

      if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
      if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
      oWindow.center();
  }
        
</script>
 <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_DEDL" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseDEDL"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        
    </telerik:RadWindowManager>
<div class="matters" align="center" >
<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<br />
    <asp:LinkButton ID="LinkAdvanceSearch" runat="server" Font-Size="X-Large"   EnableTheming="false" OnClientClick="javascript:return false;">New Follow Up</asp:LinkButton><br />
    <asp:Panel ID="Panel1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="600">
    <tr>
        <td class="subheader_img">
            Message</td>
    </tr>
    <tr>
        <td align="left" >

<table>
    <tr>
        <td>
            To</td>
        <td>
            <asp:Label ID="lblFrom" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>
            Subject</td>
        <td>
            <asp:TextBox ID="txtsubject" runat="server" Width="364px"></asp:TextBox>
            <asp:CheckBox ID="CheckHighPriority" runat="server" Text="High Priority" /></td>
    </tr>
    <tr>
        <td>
            </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="2" rowspan="2">
            <telerik:RadEditor ID="txtdetails" runat="server" EditModes="Design" ToolsFile="../xml/FullSetOfTools.xml"
                Width="600px">
            </telerik:RadEditor>
        </td>
    </tr>
    <tr>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <uc1:hdFileUpload ID="HdFileUpload1" runat="server" />
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:Button ID="btnsend" runat="server" Text="Send" CssClass="button" Width="80px" OnClick="btnsend_Click"  />
            </td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></td>
    </tr>
</table>
        </td>
    </tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtsubject"
        Display="None" ErrorMessage="Plese Enter Subject" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdetails"
        Display="None" ErrorMessage="Please Enter Message" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
    
    </asp:Panel>
<br />
    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkAdvanceSearch"
        Collapsed="true" CollapsedSize="0" CollapsedText="New Follow Up" ExpandControlID="LinkAdvanceSearch"
        ExpandedSize="600" ExpandedText="Hide" ScrollContents="false" TargetControlID="Panel1"
        TextLabelID="LinkAdvanceSearch">
    </ajaxToolkit:CollapsiblePanelExtender>
<br />



    &nbsp;
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
        <tr>
            <td class="subheader_img">
                Follow Up Messages</td>
        </tr>
        <tr>
            <td>
<asp:GridView ID="GridTaskFollowup" runat="server" AllowPaging="True" AutoGenerateColumns="false"
    EmptyDataText="No Follow Up made yet."
    Width="680px">
    <Columns>
        <asp:TemplateField HeaderText="Entry Date">
            <HeaderTemplate>
                <table class="BlueTable" width="100%">
                    <tr class="matterswhite">
                        <td align="center" colspan="2">
                            Entry Date
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                </center>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="From">
            <HeaderTemplate>
                <table class="BlueTable" width="100%">
                    <tr class="matterswhite">
                        <td align="center" colspan="2">
                           From
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <%#Eval("FROM_EMPNAME")%>
                </center>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="To">
            <HeaderTemplate>
                <table class="BlueTable" width="100%">
                    <tr class="matterswhite">
                        <td align="center" colspan="2">
                            To
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <%#Eval("TO_EMPNAME")%>
                </center>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Subject">
            <HeaderTemplate>
                <table class="BlueTable" width="100%">
                    <tr class="matterswhite">
                        <td align="center" colspan="2">
                            Subject
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <asp:LinkButton ID="LinkDes" runat="server" OnClientClick='<%#Eval("ViewDes")%>' Text='<%#Eval("SUBJECT")%>' CausesValidation="false" ></asp:LinkButton></center>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Priority">
            <HeaderTemplate>
                <table class="BlueTable" width="100%">
                    <tr class="matterswhite">
                        <td align="center" colspan="2">
                          Priority
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                   <asp:Image ID="Image1" Visible='<%#Eval("PRIORITY_HIGH")%>' ImageUrl="~/Images/Helpdesk/warning.png" runat="server" />
                </center>
            </ItemTemplate>
        </asp:TemplateField>

        
    </Columns>
    <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
    <SelectedRowStyle CssClass="Green" Wrap="False" />
    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
    <EmptyDataRowStyle Wrap="False" />
    <EditRowStyle Wrap="False" />
</asp:GridView>
            </td>
        </tr>
    </table>
<br />
    &nbsp;<asp:Button ID="btncancel" runat="server" CssClass="button" OnClientClick="javascript:window.close();" Text="Close" ValidationGroup="s" Width="80px" />
<asp:HiddenField ID="hiddenTaskListId" runat="server" />
<asp:HiddenField ID="HiddenFrom" runat="server" />
<asp:HiddenField ID="HiddenTo" runat="server" />
</div>