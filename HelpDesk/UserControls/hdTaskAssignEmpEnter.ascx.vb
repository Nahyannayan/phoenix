Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI


Partial Class HelpDesk_UserControls_hdTaskAssignEmpEnter
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then


            HiddenTaskCategory.Value = Request.QueryString("task_category_id").ToString()
            HiddenTaskListID.Value = Request.QueryString("task_list_id").ToString()

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

            HiddenSubTabID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "SELECT SUB_TAB_ID FROM TASK_CATEGORY_MASTER WHERE TASK_CATEGORY_ID='" & HiddenTaskCategory.Value & "'")

            HiddenBsuid.Value = Session("sbsuid") ''"999998"
            HiddenEmpID.Value = Session("EmployeeId") ''"12581"
            BindControls()
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
            txtStartdate.Text = DateTime.Today.ToString("dd/MMM/yyyy")
            'txtEnddate.Text = DateTime.Today.ToString("dd/MMM/yyyy")
            txtEnddate.Text = Format(DateAdd(DateInterval.Day, 7, Now.Date), "dd/MMM/yyyy")

        End If

    End Sub


    Public Sub BindControls()

        HelpDesk.BindPriority(ddPriority)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = " SELECT A.TASK_DATE_TIME,TASK_TITLE,TASK_DESCRIPTION,isnull(emp_displayname,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_LNAME, '')) AS EMP_NAME,UPLOAD_IDS,TASK_PRIORITY  FROM dbo.TASK_LIST_MASTER A " & _
                        " LEFT JOIN dbo.TASK_CONTACT_MASTER B ON A.TASK_ID= B.TASK_ID " & _
                        " LEFT JOIN OASIS.dbo.EMPLOYEE_M C ON B.REPORTED_EMP_ID=C.EMP_ID " & _
                        " WHERE TASK_LIST_ID='" & HiddenTaskListID.Value & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            txtdetails.Content = "<br><br><br><br><hr>-Original Requirement Message (Below)-<hr><br>" & _
                                 "<b>Title  :" & ds.Tables(0).Rows(0).Item("TASK_TITLE").ToString() & _
                                 "<br>From     :" & ds.Tables(0).Rows(0).Item("EMP_NAME").ToString() & _
                                 "<br>Date     :" & ds.Tables(0).Rows(0).Item("TASK_DATE_TIME").ToString() & _
                                 "</b><br><br>" & _
                                 ds.Tables(0).Rows(0).Item("TASK_DESCRIPTION").ToString()
            HiddenTitle.value = ds.Tables(0).Rows(0).Item("TASK_TITLE").ToString()
            ddPriority.SelectedValue = ds.Tables(0).Rows(0).Item("TASK_PRIORITY").ToString()
            HdFileUploadViewAssign1.Uploadids = ds.Tables(0).Rows(0).Item("UPLOAD_IDS").ToString()
        Else
            btnSave.Visible = False
        End If

    End Sub

    Public Function BindCheckValidClaimer() As Boolean

        Dim returnvalue As Boolean = True

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = ""
        Dim ds As DataSet
        Sql_Query = "select CLAIM_EMP_ID from TASK_LIST_MASTER where CLAIM_EMP_ID IS NOT NULL AND TASK_LIST_ID='" & HiddenTaskListID.Value & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        If ds.Tables(0).Rows.Count > 0 Then

            Sql_Query = "select CLAIM_EMP_ID from TASK_LIST_MASTER where ISNULL(CLAIM_EMP_ID,'') != '" & HiddenEmpID.Value & "' AND TASK_LIST_ID='" & HiddenTaskListID.Value & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            If ds.Tables(0).Rows.Count > 0 Then
                returnvalue = False
                lblmessage.Text = "Another user already claimed for this task."
            End If

        End If

        If txtStartdate.Text <> "" And txtEnddate.Text <> "" Then

            If Convert.ToDateTime(txtStartdate.Text) <= Convert.ToDateTime(txtEnddate.Text) Then
                returnvalue = True
            Else
                returnvalue = False
                lblmessage.Text = "Date Range is not valid."
            End If

            If Convert.ToDateTime(txtStartdate.Text) < Date.Today Or Convert.ToDateTime(txtEnddate.Text) < Date.Today Then
                returnvalue = False
                lblmessage.Text = "Please do not enter past dates "
            End If
     
        End If

        If HiddenEmpID.Value.Trim() = "" Then
            returnvalue = False
            lblmessage.Text = "Please login again and assign task to employee."
        End If


        Return returnvalue

    End Function


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If BindCheckValidClaimer() Then

            Try

                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

                Dim ds As DataSet

                Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
                ds = HelpDesk.GetCommunicationSettings()
                Dim Sql_Query = ""

                Dim username = ""
                Dim password = ""
                Dim port = ""
                Dim host = ""
                Dim fromemailid = ""

                Dim reportedby = SqlHelper.ExecuteScalar(str_conn2, CommandType.Text, "SELECT isnull(emp_displayname,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_LNAME, '')) EMPNAME from OASIS.dbo.EMPLOYEE_M  where EMP_ID='" & HiddenEmpID.Value & "'")

                If ds.Tables(0).Rows.Count > 0 Then

                    username = ds.Tables(0).Rows(0).Item("USERNAME").ToString()
                    password = ds.Tables(0).Rows(0).Item("PASSWORD").ToString()
                    port = ds.Tables(0).Rows(0).Item("PORT").ToString()
                    host = ds.Tables(0).Rows(0).Item("HOST").ToString()
                    fromemailid = ds.Tables(0).Rows(0).Item("FROMEMAIL").ToString()

                End If

                Dim flag = 0

                Dim empid = Request.QueryString("Emp_id").ToString()

                Dim Q = " SELECT EMD_CUR_MOBILE,EMD_EMAIL FROM OASIS.dbo.EMPLOYEE_M A " & _
                        " INNER JOIN OASIS.dbo.EMPLOYEE_D B ON B.EMD_EMP_ID = A.EMP_ID " & _
                        " WHERE A.EMP_ID ='" & empid & "' "

                Dim ds2 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Q)
                Dim emailid As String
                Dim mobilenumber As String
                If ds.Tables(0).Rows.Count > 0 Then

                    emailid = ds2.Tables(0).Rows(0).Item("EMD_EMAIL").ToString()
                    mobilenumber = ds2.Tables(0).Rows(0).Item("EMD_CUR_MOBILE").ToString()

                End If

                Dim taskdetails = txtdetails.Content
                Dim startdate = txtStartdate.Text
                Dim enddate = txtEnddate.Text
                Dim priority = ddPriority.SelectedItem.Text
                Dim priorityid = ddPriority.SelectedValue
                Dim TASK_ASSIGN_ID As String = ""


                Dim connection As New System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
                Dim transaction As System.Data.SqlClient.SqlTransaction
                connection.Open()
                transaction = connection.BeginTransaction()
                Dim commitflag = 0
                Try
                    Dim pParms(9) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", HiddenTaskListID.Value)
                    pParms(1) = New SqlClient.SqlParameter("@TASK_ASSIGNED_TO_EMP_ID", empid)
                    pParms(2) = New SqlClient.SqlParameter("@TASK_ASSIGN_DESCRIPTION", txtdetails.Content.ToString())
                    If startdate <> "" Then
                        pParms(3) = New SqlClient.SqlParameter("@TASK_ASSIGN_START_DATE", Convert.ToDateTime(startdate))
                    End If
                    If enddate <> "" Then
                        pParms(4) = New SqlClient.SqlParameter("@TASK_ASSIGN_END_DATE", Convert.ToDateTime(enddate))
                    End If
                    pParms(5) = New SqlClient.SqlParameter("@TASK_ASSIGN_PRIORITY", priorityid)
                    pParms(6) = New SqlClient.SqlParameter("@TASK_ASSIGNED_BY_EMP_ID", HiddenEmpID.Value)
                    pParms(7) = New SqlClient.SqlParameter("@SUB_TAB_ID", HiddenSubTabID.Value)

                    ''Get attachements
                    Dim grid As GridView = DirectCast(HdFileUploadViewAssign1.FindControl("GridUpload"), GridView)
                    Dim val
                    Dim uploadid = ""
                    For Each row As GridViewRow In grid.Rows
                        Dim check As CheckBox = DirectCast(row.FindControl("CheckUpload"), CheckBox)
                        If check.Checked Then
                            val = DirectCast(row.FindControl("HiddenUploadValue"), HiddenField).Value
                            If uploadid = "" Then
                                uploadid = val
                            Else
                                uploadid = uploadid & "," & val
                            End If

                        End If

                    Next
                    If uploadid <> "" Then

                        pParms(8) = New SqlClient.SqlParameter("@UPLOAD_IDS", uploadid)

                    End If


                    TASK_ASSIGN_ID = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_TASK_ASSIGNED_LOG", pParms)

                    'check if this task is suppose to go through approval process and if yes then start approval process for it. 
                    SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "CHECK_AND_SEND_TASK_FOR_APPROVAL", New SqlClient.SqlParameter("@TASK_LIST_MASTER_ID", HiddenTaskListID.Value))

                    transaction.Commit()
                    commitflag = 1

                Catch ex As Exception

                    If commitflag = 0 Then
                        transaction.Rollback()
                    End If

                    lblmessage.Text = "Error occured while saving . " & ex.Message
                Finally
                    connection.Close()
                End Try

                If commitflag = 1 And TASK_ASSIGN_ID <> "" Then

                    ''Sent Email
                    Try

                        If ds.Tables(0).Rows.Count > 0 Then
                            Dim textdetails As String = taskdetails

                            Dim sb As New Object
                            Dim pParms(23) As SqlClient.SqlParameter
                            pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", HiddenTaskListID.Value)
                            pParms(1) = New SqlClient.SqlParameter("@TASK_ASSIGN_ID", TASK_ASSIGN_ID)
                            pParms(2) = New SqlClient.SqlParameter("@OPTION", 3)
                            sb = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "HD_MAIL_TEXT", pParms)

                            Dim replytoemailid = HelpDesk.ReplytoEmailid(HiddenEmpID.Value, empid)

                            Dim status = HelpDesk.SendPlainTextEmails(fromemailid, emailid, HiddenTaskListID.Value & " - " & HiddenTitle.Value, sb.ToString(), username, password, host, port, HelpDesk.GetuploadIds(TASK_ASSIGN_ID, 2), replytoemailid).Replace("'", " ")
                            ''Update Email Notification Status
                            Sql_Query = "UPDATE TASK_ASSIGNED_LOG SET TASK_EMAIL_NOTIFICATION='" & status & "' WHERE TASK_ASSIGN_ID='" & TASK_ASSIGN_ID & "'"
                            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, Sql_Query)

                        End If


                        ''Sent SMS
                        If CheckSMS.Checked Then
                            Dim message = "New Task Assigned ID : " & TASK_ASSIGN_ID & " " & _
                                          ", Title : " & HiddenTitle.Value & _
                                          ", Reported By : " & reportedby & _
                                          ", Priority :" & priority & _
                                          ".GEMS-Helpdesk"
                            Dim status = SmsService.sms.SendMessage(mobilenumber, message, "Helpdesk", Web.Configuration.WebConfigurationManager.AppSettings("smsUsername").ToString(), Web.Configuration.WebConfigurationManager.AppSettings("smspwd").ToString())
                            ''Update Sms Notification Status
                            Sql_Query = "UPDATE TASK_ASSIGNED_LOG SET TASK_SMS_NOTIFICATION='" & status & "' WHERE TASK_ASSIGN_ID='" & TASK_ASSIGN_ID & "'"
                            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, Sql_Query)

                        End If

                        lblmessage.Text = "Task assigned successfully."
                        btnSave.Visible = False


                    Catch ex As Exception

                        If commitflag = 1 Then
                            lblmessage.Text = "Task assigned successfully.But error in sending email."
                            btnSave.Visible = False
                        End If

                    End Try

                Else

                    lblmessage.Text = "Task already assigned for this user. Current transactions will not taken in to effect."

                End If


            Catch ex As Exception

                lblmessage.Text = "Error :" & ex.Message

            End Try


        End If



    End Sub


End Class
