Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI

Partial Class HelpDesk_UserControls_hdTaskAssignEmp
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then


            HiddenTaskCategory.Value = Request.QueryString("task_category_id").ToString()
            HiddenTaskListID.Value = Request.QueryString("task_list_id").ToString()

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

            HiddenSubTabID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "SELECT SUB_TAB_ID FROM TASK_CATEGORY_MASTER WHERE TASK_CATEGORY_ID='" & HiddenTaskCategory.Value & "'")

            HiddenBsuid.Value = Session("sbsuid") ''"999998"
            HiddenEmpID.Value = Session("EmployeeId") ''"12581"

            BindTaskDetails()


        End If

        BindTaskMembers()

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    
    Public Sub BindTaskMembers()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT DISTINCT 'javascript:openassign('''+ A.EMP_ID +''');location.reload();' OpenAssign,A.EMP_ID,EMD_CUR_MOBILE,EMD_EMAIL,ISNULL(EMP_FNAME,'') +' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'') EMPNAME, BSU_SHORTNAME , " & _
                        " (CASE ACTIVE WHEN 'TRUE' THEN 'False' ELSE 'True' END) DisableRow " & _
                        " from dbo.TASK_ROOTING_MASTER A " & _
                        " INNER JOIN OASIS.dbo.EMPLOYEE_M B ON  B.EMP_ID=A.EMP_ID " & _
                        " INNER JOIN OASIS.dbo.EMPLOYEE_D D ON D.EMD_EMP_ID = A.EMP_ID " & _
                        " INNER JOIN OASIS.dbo.BUSINESSUNIT_M C ON A.EMP_BSU_ID= C. BSU_ID " & _
                        " LEFT JOIN dbo.TASK_ASSIGNED_LOG ON TASK_LIST_ID='" & HiddenTaskListID.Value & "' AND TASK_ASSIGNED_TO_EMP_ID=A.EMP_ID  AND ACTIVE='TRUE' AND A.SUB_TAB_ID='" & HiddenSubTabID.Value & "'" & _
                        " WHERE  A.TASK_CATEGORY_ID='" & HiddenTaskCategory.Value & "'AND A.TASK_MEMBER='True' " & _
                        " AND A.EMP_ID IN (SELECT EMP_ID FROM TASK_ROOTING_MASTER WHERE EMP_TASK_BSU_ID=(SELECT CALLER_BSU_ID FROM TASK_CONTACT_MASTER WHERE TASK_ID=(SELECT TASK_ID FROM TASK_LIST_MASTER WHERE TASK_LIST_ID='" & HiddenTaskListID.Value & "') ))"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        GridTaskMembers.DataSource = ds
        GridTaskMembers.DataBind()


    End Sub

    Public Sub BindTaskDetails()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim ds As New DataSet

        Dim sql_query = " SELECT A.TASK_PRIORITY,CLIPBOARD_IMAGE,'javascript:openimage(''' + CLIPBOARD_IMAGE + ''');return false;'imagefile, (case isnull(CLIPBOARD_IMAGE,'') when '' then 'False' else 'True' end )Visible,(case isnull(TASK_STATUS,'') when '6' then 'False' else 'True' end )CloseVisible, " & _
                        " (MAIN_TAB_DESCRIPTION + '/' + SUB_TAB_DESCRIPTION)ROOT_TAB,A.TASK_ID,TASK_LIST_ID,PRIORITY_DESCRIPTION,TASK_TRAGET_DATE,A.TASK_DATE_TIME,TASK_STATUS_DESCRIPTION,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,('</br><span style=''color: red;font-weight:bold''> more... </span>')tempview,TASK_DESCRIPTION,A.TASK_DATE_TIME , " & _
                        " CATEGORY_DES AS TASK_CATEGORY_DESCRIPTION,ISNULL(EMP_FNAME,'') +' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'') EMPNAME,NAME,TASK_NOTES " & _
                        " ,ISNULL(STU_FIRSTNAME,'') +' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'') STUNAME, " & _
                        " BSU_SHORTNAME,UPLOAD_IDS " & _
                        " FROM dbo.TASK_LIST_MASTER A " & _
                        " INNER JOIN dbo.TASK_STATUS_MASTER B ON A.TASK_STATUS =B.STATUS_ID " & _
                        " INNER JOIN dbo.TASK_PRIORITY_MASTER C ON C.RECORD_ID=A.TASK_PRIORITY " & _
                        " INNER JOIN dbo.TASK_CATEGORY_MASTER D ON D.TASK_CATEGORY_ID = A.TASK_CATEGORY " & _
                        " INNER JOIN dbo.TASK_CATEGORY D1 ON D1.ID=D.CATEGORY_ID " & _
                        " INNER JOIN dbo.SUB_TAB_MASTER E ON  E.SUB_TAB_ID = D.SUB_TAB_ID " & _
                        " INNER JOIN dbo.MAIN_TAB_MASTER F ON F.MAIN_TAB_ID = E.MAIN_TAB_ID " & _
                        " INNER JOIN dbo.TASK_CONTACT_MASTER G ON G.TASK_ID=A.TASK_ID " & _
                        " INNER JOIN OASIS.dbo.BUSINESSUNIT_M G1 ON G1.BSU_ID=G.CALLER_BSU_ID " & _
                        " LEFT JOIN OASIS.dbo.EMPLOYEE_M H ON H.EMP_ID = G.REPORTED_EMP_ID " & _
                        " LEFT JOIN OASIS.dbo.STUDENT_M I ON I.STU_ID = G.REPORTED_STU_ID " & _
                        " WHERE A.TASK_CATEGORY='" & HiddenTaskCategory.Value & "' AND A.TASK_LIST_ID='" & HiddenTaskListID.Value & "' "


        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)


        If ds.Tables(0).Rows.Count > 0 Then
            Label1.Text = ds.Tables(0).Rows(0).Item("TASK_LIST_ID").ToString()
            Label2.Text = ds.Tables(0).Rows(0).Item("ROOT_TAB").ToString()
            If ds.Tables(0).Rows(0).Item("TASK_TRAGET_DATE").ToString() <> "" Then
                Label3.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("TASK_TRAGET_DATE").ToString()).ToString("dd/MMM/yyyy")
            End If
            Label4.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("TASK_DATE_TIME").ToString()).ToString("dd/MMM/yyyy")
            Label5.Text = ds.Tables(0).Rows(0).Item("PRIORITY_DESCRIPTION").ToString()
            Label6.Text = ds.Tables(0).Rows(0).Item("TASK_STATUS_DESCRIPTION").ToString()
            'txtdes.Content = ds.Tables(0).Rows(0).Item("TASK_DESCRIPTION").ToString()
            lbltxtdes.Text = ds.Tables(0).Rows(0).Item("TASK_DESCRIPTION").ToString()
            'txtnotes.Content = ds.Tables(0).Rows(0).Item("TASK_NOTES").ToString()
            Label8.Text = ds.Tables(0).Rows(0).Item("TASK_CATEGORY_DESCRIPTION").ToString()

            Label9.Text = ds.Tables(0).Rows(0).Item("EMPNAME").ToString() & "" & ds.Tables(0).Rows(0).Item("NAME").ToString() & "" & ds.Tables(0).Rows(0).Item("STUNAME").ToString()
            HiddenPriorityId.Value = ds.Tables(0).Rows(0).Item("TASK_PRIORITY").ToString()
            HdFileUploadViewAssign1.Uploadids = ds.Tables(0).Rows(0).Item("UPLOAD_IDS").ToString()

            If Label6.Text.Trim() = "Closed" Then

                GridTaskMembers.Enabled = False
            Else

                GridTaskMembers.Enabled = True
            End If
        End If


    End Sub


    Protected Sub GridTaskMembers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridTaskMembers.PageIndexChanging
        GridTaskMembers.PageIndex = e.NewPageIndex
        BindTaskMembers()
    End Sub

End Class
