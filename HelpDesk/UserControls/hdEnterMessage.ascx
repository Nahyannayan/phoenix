<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdEnterMessage.ascx.vb" Inherits="HelpDesk_UserControls_hdEnterMessage" %>
<%@ Register Src="hdFileUpload.ascx" TagName="hdFileUpload" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

 <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Message <asp:Label ID="lbltext" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

<br />

<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
         
        <table  width="80%" class="table table-bordered table-row">
    
   
    <tr>
        <td>
            To</td>
        <td>
            <asp:Label ID="lblFrom" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>
            Subject</td>
        <td>
            <asp:TextBox ID="txtsubject" runat="server" Width="364px"></asp:TextBox>
            <asp:CheckBox ID="CheckHighPriority" runat="server" Text="High Priority" /></td>
    </tr>
    <tr>
        <td>
            </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <telerik:RadEditor ID="txtdetails" runat="server" EditModes="Design" ToolsFile="../xml/FullSetOfTools.xml"
                Width="600px">
            </telerik:RadEditor>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <uc1:hdFileUpload ID="HdFileUpload1" runat="server" />
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:Button ID="btnsend" runat="server" Text="Send" CssClass="button" Width="80px" />
            <asp:Button ID="btncancel" runat="server" CssClass="button" OnClientClick="javascript:window.close();"
                Text="Close" ValidationGroup="s" Width="80px" /></td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></td>
    </tr>
</table>
        
    </ContentTemplate>
</asp:UpdatePanel>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtsubject"
        Display="None" ErrorMessage="Plese Enter Subject" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdetails"
        Display="None" ErrorMessage="Please Enter Message" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
        ShowSummary="False" />
<br />
<asp:HiddenField ID="HiddenTaskAssignid" runat="server" />
<asp:HiddenField ID="HiddenFromEmpId" runat="server" />
<asp:HiddenField ID="HiddenToEmpid" runat="server" />
<asp:HiddenField ID="HiddenParent_Message_Id" runat="server" />
</div>
            </div></div>