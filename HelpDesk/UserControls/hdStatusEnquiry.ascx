<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdStatusEnquiry.ascx.vb" Inherits="HelpDesk_UserControls_hdStatusEnquiry" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>    
     <script type="text/javascript" >
    

        function openimage(fname)
        {
            // window.showModalDialog('hdImageView.aspx?filename=' + fname +'&Filecount='+ '1'  , '','dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;
            var  url = "hdImageView.aspx?filename='" + fname +"'&Filecount=1";
            var oWnd = radopen(url, "pop_DEDL");

        }
        
        function ViewTaskDes(TaskListId)
        {
       
          //  window.showModalDialog('Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;
            var url = "Pages/hdTaskDescriptionView.aspx?TaskListId=" + TaskListId
            var oWnd = radopen(url, "pop_DEDL");

        }
         function History(task_list_id)
        {

        // window.showModalDialog('Pages/hdTaskHistory.aspx?task_list_id=' + task_list_id +'&Usertype=OWNER_AGENT' , '','dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;
             var url = "Pages/hdTaskHistory.aspx?task_list_id=" + task_list_id + "&Usertype=OWNER_AGENT"
             var oWnd = radopen(url, "pop_DEDL");
        }
        
        function SendMail(TaskListid,FromEmpId,ToEmpid,Parent_Message_Id)
       {
       
         //Help Desk Agent Send Message
            FromEmpId = document.getElementById('<%= HiddenEmpId.clientID %>').value
            var url = "hdEnterMessage.aspx?TaskListid='" + TaskListid + "&FromEmpId="+ FromEmpId + "&ToEmpid=" + ToEmpid + "&Parent_Message_Id=" + Parent_Message_Id 
            var oWnd = radopen(url, "pop_DEDL");
        //window.showModalDialog('hdEnterMessage.aspx?TaskListid=' + TaskListid + '&FromEmpId='+ FromEmpId + '&ToEmpid=' + ToEmpid + '&Parent_Message_Id=' + Parent_Message_Id , '','dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

       }
        
         function OnClientCloseDEDL(oWnd, args) {
         }
         function autoSizeWithCalendar(oWindow) {
             var iframe = oWindow.get_contentFrame();
             var body = iframe.contentWindow.document.body;

             var height = body.scrollHeight;
             var width = body.scrollWidth;

             var iframeBounds = $telerik.getBounds(iframe);
             var heightDelta = height - iframeBounds.height;
             var widthDelta = width - iframeBounds.width;

             if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
             if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
             oWindow.center();
         }
    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_DEDL" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseDEDL"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        
    </telerik:RadWindowManager>
<div class="matters">
 
    <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>

<table>
    <tr>
        <td >
            Enter Reference Task ID</td>
        <td >
            <asp:TextBox ID="txttaskid" runat="server"></asp:TextBox></td>
        <td >
            <asp:Button ID="btnstatus" CssClass="button" runat="server" Text="Status" /></td>
    </tr>
</table><hr />
    <br />
    <asp:LinkButton ID="LinkCloseJobs" OnClientClick="javascript:return false;" runat="server">Closed Jobs (Owner)</asp:LinkButton>
    <br />
    <asp:Panel ID="Panel2" runat="server">
     <table  cellpadding="5" cellspacing="0" Width="700" >
                        <tr>
                            <td class="subheader_img">
                                Owner Closed Jobs Lists</td>
                        </tr>
                        <tr>
                            <td >
        <asp:GridView ID="GridOwnerClose" runat="server" AllowPaging="True" AutoGenerateColumns="false"
            EmptyDataText="No closed tasks by owner"
            Width="100%" PageSize="5" CssClass="table table-bordered table-row">
            <Columns>
                <asp:TemplateField HeaderText="Task ID">
                    <HeaderTemplate>
                       
                                    Task ID
                               
                    </HeaderTemplate>
                    <ItemTemplate>
                        <center>
                            <%#Eval("TASK_ID")%>
                            <asp:HiddenField ID="HiddenTaskId" runat="server" Value='<%#Eval("TASK_ID")%>' />
                        </center>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Task List ID">
                    <HeaderTemplate>
                       
                                    Task List ID
                              
                    </HeaderTemplate>
                    <ItemTemplate>
                        <center>
                            <%#Eval("TASK_LIST_ID")%>
                            <asp:HiddenField ID="HiddenTaskListId" runat="server" Value='<%#Eval("TASK_LIST_ID")%>' />
                        </center>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Root Tab">
                    <HeaderTemplate>
                      
                                    Root Tab
                               
                    </HeaderTemplate>
                    <ItemTemplate>
                        <center>
                            <%#Eval("ROOT_TAB")%>
                        </center>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Reported By">
                    <HeaderTemplate>
                     
                                    Reported By
                               
                    </HeaderTemplate>
                    <ItemTemplate>
                        <center>
                            <%#Eval("EMPNAME")%>
                            <%#Eval("NAME")%>
                            <%#Eval("STUNAME")%>
                        </center>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Priority">
                    <HeaderTemplate>
                      
                                    Priority
                             
                    </HeaderTemplate>
                    <ItemTemplate>
                        <center>
                            <%#Eval("PRIORITY_DESCRIPTION")%>
                        </center>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <HeaderTemplate>
                       
                                    Description
                               
                    </HeaderTemplate>
                    <ItemTemplate>
                    
                       <center><asp:LinkButton ID="LinkDes" OnClientClick='<%#Eval("VIEW_TASK")%>' runat="server">View</asp:LinkButton></center>

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Entry Date">
                    <HeaderTemplate>
                      
                                    Entry Date
                               
                    </HeaderTemplate>
                    <ItemTemplate>
                        <center>
                            <%#Eval("TASK_DATE_TIME", "{0:dd/MMM/yyyy}")%>
                        </center>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Select">
                    <HeaderTemplate>
                       
                                    Select
                               
                    </HeaderTemplate>
                    <ItemTemplate>
                        <center>
                            <asp:LinkButton ID="LinkSelect" CommandName="Taskselect" CommandArgument='<%#Eval("TASK_ID")%>' runat="server">Select</asp:LinkButton></center>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
            <RowStyle CssClass="" Height="25px" Wrap="False" />
            <SelectedRowStyle CssClass="" Wrap="False" />
            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
            <EmptyDataRowStyle Wrap="False" />
            <EditRowStyle Wrap="False" />
        </asp:GridView>
    
 </td>
                    </tr>
               </table>
    
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkCloseJobs"
        Collapsed="true"  CollapsedSize="0" CollapsedText="Closed Jobs (Owner)" ExpandControlID="LinkCloseJobs"
        ExpandedSize="240"   ExpandedText="Hide-Closed Jobs (Owner)" ScrollContents="true"  TargetControlID="Panel2"
        TextLabelID="LinkCloseJobs">
    </ajaxToolkit:CollapsiblePanelExtender>
    
    <br />
<hr />
    <br />
    <asp:Panel ID="Panel1" runat="server" >
     <table  cellpadding="5" cellspacing="0" Width="100%" >
                        <tr>
                            <td class="subheader_img">
                                Contact Information</td>
                        </tr>
                        <tr>
                            <td >
        <table>
            <tr>
                <td>
                   <span class="field-label">   Task ID</span></td>
                <td>
                    :</td>
                <td>
                    <asp:Label ID="lbltaskid" runat="server" Text=""></asp:Label></td>
                <td>
                      <span class="field-label">Task Source</span></td>
                <td>
                    :</td>
                <td>
                    <asp:Label ID="lbltasksource" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td>
                     <span class="field-label"> Name</span></td>
                <td>
                    :</td>
                <td>
                    <asp:Label ID="lblname" runat="server" Text=""></asp:Label></td>
                <td>
                     <span class="field-label"> BSU</span></td>
                <td>
                    :</td>
                <td>
                    <asp:Label ID="lblbsu" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td >
                    <span class="field-label">  Contact Number</span></td>
                <td>
                    :</td>
                <td >
                    <asp:Label ID="lblcontactnumber" runat="server" Text=""></asp:Label></td>
                <td>
                    <span class="field-label">  Email</span></td>
                <td>
                    :</td>
                <td>
                    <asp:Label ID="lblemail" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td >
                      <span class="field-label">Address</span></td>
                <td>
                    :</td>
                <td colspan="4">
                    <asp:Label ID="lbladdress" runat="server" Text=""></asp:Label></td>
            </tr>
        </table>
        
 </td>
                    </tr>
               </table>
        <br />
    <table  cellpadding="5" cellspacing="0" width="100%">
        <tr>
            <td class="subheader_img">
                Task Lists</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridTaskList" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                    EmptyDataText="Search query did not retrieve any results. Please try with some other keyword(s)"
                    Width="100%" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Task List ID">
                         <HeaderTemplate>
                               
                                           Task List ID
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_LIST_ID")%>
                                    <asp:HiddenField ID="HiddenTaskListId" runat="server" Value='<%#Eval("TASK_LIST_ID")%>' />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                  
                        <asp:TemplateField HeaderText="Priority">
                         <HeaderTemplate>
                               
                                           Priority
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("PRIORITY_DESCRIPTION")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                         <HeaderTemplate>
                               
                                           Description
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                            
                             <center><asp:LinkButton ID="LinkDes" OnClientClick='<%#Eval("VIEW_TASK")%>' runat="server">View</asp:LinkButton></center>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Req. Date">
                         <HeaderTemplate>
                               
                                           Req. Date
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_TRAGET_DATE", "{0:dd/MMM/yyyy}")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entry Date">
                         <HeaderTemplate>
                              
                                           Entry Date
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_DATE_TIME", "{0:dd/MMM/yyyy}")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Status">
                         <HeaderTemplate>
                               
                                           Status
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_STATUS_DESCRIPTION")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="History">
                         <HeaderTemplate>
                              
                                           History
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                              <center>
                                    <asp:LinkButton ID="LinkHistory" OnClientClick='<%#Eval("HISTORY")%>' runat="server">History</asp:LinkButton>
                                    
                              </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                      <%--  <asp:TemplateField HeaderText="">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                          
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <asp:ImageButton ID="ImageMessage" ToolTip="Send Mail" OnClientClick='<%#Eval("SEND_MAIL")%>' ImageUrl="~/Images/Helpdesk/mail.png" runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        
                           <asp:TemplateField HeaderText="Close">
                            <HeaderTemplate>
                               
                                           Close
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkClose" CommandName="close" Visible='<%#Eval("VisibleClose")%>' CommandArgument='<%#Eval("TASK_LIST_ID")%>' runat="server">Close</asp:LinkButton></center>
                                    <ajaxToolkit:ConfirmButtonExtender ID="C1" ConfirmText="Are you sure, Do you want to update this task client status?" TargetControlID="LinkClose" runat="server"></ajaxToolkit:ConfirmButtonExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                    <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <EditRowStyle Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
        <asp:Label ID="Label3" runat="server"></asp:Label><asp:Panel ID="PanelStatusupdate"
            runat="server" BackColor="white" CssClass="modalPopup" Style="display: none"
            Width="620">
            <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="240">
                <tr>
                    <td class="subheader_img">
                       Update Status</td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <telerik:RadEditor ID="txtdetails" runat="server" EditModes="Design" ToolsFile="xml/FullSetOfTools.xml"
                                                Width="100%">
                                            </telerik:RadEditor>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="3">
                                            <asp:DropDownList ID="ddstatus" runat="server">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="3">
                                            <asp:Button ID="btnok" runat="server" CssClass="button" OnClick="btnok_Click" Text="Ok"
                                                ValidationGroup="s" Width="80px" /><asp:Button ID="btncancel3" runat="server" CssClass="button"
                                                    Text="Cancel" Width="80px" /></td>
                                    </tr>
                                </table>
                                <asp:HiddenField ID="HiddenTasklistid" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        &nbsp;</asp:Panel>
    <asp:HiddenField ID="HiddenEmpId" runat="server" />
    <ajaxToolkit:ModalPopupExtender ID="MO3" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btncancel3" DropShadow="true" PopupControlID="PanelStatusupdate"
        RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="Label3">
    </ajaxToolkit:ModalPopupExtender>
    
</div>
