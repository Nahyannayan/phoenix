Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports EmailService
Imports SmsService
Partial Class HelpDesk_UserControls_hdHr
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            ''HR Internal
            HiddenMAIN_TAB_ID.Value = "1"
            HiddenSUB_TAB_ID.Value = "2"


            BindControls()

            HiddenBsuid.Value = Session("sbsuid") ''"999998"
            HiddenUserId.Value = Session("EmployeeId") ''"12581"

            CreateDatatable()


        End If

    End Sub
    Public Sub CreateDatatable()
        ''Create a Datatable
        Dim dt As New DataTable
        dt.Columns.Add("TASK_CATEGORY")
        dt.Columns.Add("TASK_CATEGORY_ID")
        dt.Columns.Add("PRIORITY")
        dt.Columns.Add("PRIORITY_ID")
        dt.Columns.Add("REQ_DATE")
        dt.Columns.Add("TASK_DESCRIPTION")
        dt.Columns.Add("TASK_NOTES")

        Session("TASKlistHR") = dt

    End Sub


    Public Sub BindControls()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select * from  TASK_SOURCE_MASTER order by TASK_SOURCE_ID"

        ''Bind Job Source
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddSource.DataSource = ds
        ddSource.DataTextField = "TASK_SOURCE_DESCRIPTION"
        ddSource.DataValueField = "TASK_SOURCE_ID"
        ddSource.DataBind()

        ''Bind Priority
        Sql_Query = "SELECT RECORD_ID,PRIORITY_DESCRIPTION FROM TASK_PRIORITY_MASTER"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddpriority.DataSource = ds
        ddpriority.DataTextField = "PRIORITY_DESCRIPTION"
        ddpriority.DataValueField = "RECORD_ID"
        ddpriority.DataBind()


        ''Bind Bsu
        str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Sql_Query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M"
        Dim dsBsu As DataSet
        If ds.Tables(0).Rows.Count > 0 Then
            dsBsu = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            ddbsu.DataSource = dsBsu
            ddbsu.DataTextField = "BSU_NAME"
            ddbsu.DataValueField = "BSU_ID"
            ddbsu.DataBind()
            ddbsu.SelectedValue = "999998" 'Corporate Offices' 

        End If

        BindJobCategory()

        BindEmpName()

    End Sub

    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = "Select TASK_CATEGORY_ID,TASK_CATEGORY_DESCRIPTION from  TASK_CATEGORY_MASTER where PARENT_TASK_CATEGORY_ID='" & childnodeValue & "' AND SUB_TAB_ID='" & HiddenSUB_TAB_ID.Value & "'"  '(SUB_TAB_ID = 2 FOR HR)

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_DESCRIPTION").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If


        For Each node As TreeNode In ParentNode.ChildNodes
            BindChildNodes(node)
        Next


    End Sub

    Public Sub BindJobCategory()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select TASK_CATEGORY_ID,TASK_CATEGORY_DESCRIPTION from  TASK_CATEGORY_MASTER where PARENT_TASK_CATEGORY_ID=0 AND SUB_TAB_ID='" & HiddenSUB_TAB_ID.Value & "'"  '(SUB_TAB_ID = 2 FOR HR)

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_DESCRIPTION").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("TASK_CATEGORY_ID").ToString()
                TreeJobCategory.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In TreeJobCategory.Nodes
            BindChildNodes(node)
        Next

        Dim GNode As New TreeNode
        GNode.Text = "General Category"
        GNode.Value = "-1"
        TreeJobCategory.Nodes.Add(GNode)

        TreeJobCategory.CollapseAll()

    End Sub


    Public Sub BindEmpName()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

        Dim Sql_Query = "select EMP_ID,EMP_FNAME, isnull(EMP_FNAME,'') + ' ' +  isnull(EMP_MNAME ,'') + ' ' + isnull(EMP_LNAME ,'') as EMP_NAME from dbo.EMPLOYEE_M " & _
                        " where EMP_BSU_ID='" & ddbsu.SelectedValue & "' and EMP_bACTIVE='True'  order by EMP_FNAME  "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddreportedby.DataSource = ds
            ddreportedby.DataTextField = "EMP_NAME"
            ddreportedby.DataValueField = "EMP_ID"
            ddreportedby.DataBind()
        End If

        Dim list As New ListItem
        list.Text = "Reported By"
        list.Value = "-1"
        ddreportedby.Items.Insert(0, list)

        Dim list2 As New ListItem
        list2.Text = "Others"
        list2.Value = "0"
        ddreportedby.Items.Insert(1, list2)

        lblmessage.Text = ""



    End Sub

    Protected Sub ddbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddbsu.SelectedIndexChanged
        BindEmpName()
    End Sub


    Public Function GetNodeValue(ByVal ParentNode As TreeNode) As String
        Dim val = ""

        For Each node As TreeNode In ParentNode.ChildNodes
            If node.Checked Then
                val = node.Value
                node.Checked = False
                Exit For
            Else
                val = GetNodeValue(node)

                If val <> "" Then
                    Exit For
                End If

            End If

        Next

        Return val

    End Function

    Protected Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadd.Click
        Dim val = ""
        Dim jcatogory = ""

        For Each node As TreeNode In TreeJobCategory.Nodes
            If node.Checked Then
                val = node.Value
                node.Checked = False
                Exit For
            Else
                val = GetNodeValue(node)
                If val <> "" Then
                    Exit For
                End If
            End If
        Next

        If val = "" Or val = "-1" Then
            val = "-1" ''General Category
            jcatogory = "General Category"
        Else

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim Sql_Query = "Select TASK_CATEGORY_DESCRIPTION from  TASK_CATEGORY_MASTER where TASK_CATEGORY_ID='" & val & "'"
            jcatogory = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)

        End If

        Dim dt As DataTable = Session("TASKlistHR")

        Dim dr As DataRow = dt.NewRow()
        dr("TASK_CATEGORY") = jcatogory
        dr("TASK_CATEGORY_ID") = val
        dr("PRIORITY") = ddpriority.SelectedItem.Text
        dr("PRIORITY_ID") = ddpriority.SelectedValue
        dr("REQ_DATE") = txtreqdate.Text
        dr("TASK_DESCRIPTION") = txtjobdes.Text
        dr("TASK_NOTES") = txtaddnotes.Text

        dt.Rows.Add(dr)

        Session("TASKlistHR") = dt
        Gridjobs.DataSource = dt
        Gridjobs.DataBind()

        If Gridjobs.Rows.Count > 0 Then
            Panel4.Visible = True
            Panel1.Enabled = False
        Else
            Panel4.Visible = False
            Panel1.Enabled = True
        End If

        ''Clear Controls
        TreeJobCategory.CollapseAll()
        txtreqdate.Text = ""
        txtjobdes.Text = ""
        txtaddnotes.Text = ""
        ddpriority.SelectedIndex = 0

        lblmessage.Text = ""


    End Sub

    Protected Sub ddreportedby_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddreportedby.SelectedIndexChanged
        If ddreportedby.SelectedValue = "0" Then
            Panel2.Visible = True
        Else
            Panel2.Visible = False
            txtname.Text = ""
            txtmobile.Text = ""
            txtemail.Text = ""
            txtfax.Text = ""
            txtaddress.Text = ""
        End If
        lblmessage.Text = ""

    End Sub


    Protected Sub Gridjobs_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Gridjobs.RowDeleting
        Dim val = e.RowIndex

        Dim dt As DataTable = Session("TASKlistHR")
        dt.Rows(val).Delete()
        Session("TASKlistHR") = dt
        Gridjobs.DataSource = dt

        Gridjobs.DataBind()

        If Gridjobs.Rows.Count > 0 Then
            Panel4.Visible = True
            Panel1.Enabled = False
        Else
            Panel4.Visible = False
            Panel1.Enabled = True
        End If

    End Sub

    Public Sub SaveEmailNotificationLog(ByVal Task_list_id As String, ByVal emp_id As String, ByVal status As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", Task_list_id)
        pParms(1) = New SqlClient.SqlParameter("@EMP_ID", emp_id)
        pParms(2) = New SqlClient.SqlParameter("@STATUS", status)
        pParms(3) = New SqlClient.SqlParameter("@SUB_TAB_ID", HiddenSUB_TAB_ID.Value)
        pParms(4) = New SqlClient.SqlParameter("@MAIN_TAB_ID", HiddenMAIN_TAB_ID.Value) '' 1 Internal , 2 External
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_TASK_EMAIL_NOTIFICATION_LOG", pParms)

    End Sub

    Public Sub SaveSmsNotificationLog(ByVal Task_list_id As String, ByVal emp_id As String, ByVal status As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", Task_list_id)
        pParms(1) = New SqlClient.SqlParameter("@EMP_ID", emp_id)
        pParms(2) = New SqlClient.SqlParameter("@STATUS", status)
        pParms(3) = New SqlClient.SqlParameter("@SUB_TAB_ID", HiddenSUB_TAB_ID.Value)
        pParms(4) = New SqlClient.SqlParameter("@MAIN_TAB_ID", HiddenMAIN_TAB_ID.Value) '' 1 Internal , 2 External
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_TASK_SMS_NOTIFICATION_LOG", pParms)

    End Sub

    Public Sub SendEmailSmsNotification(ByVal TASK_LIST_ID As String, ByVal TASK_CATEGORY_ID As String, ByVal TASK_CATEGORY As String, ByVal PRIORITY As String, ByVal REQ_DATE As String, ByVal TASK_DESCRIPTION As String, ByVal TASK_NOTES As String)

        Try

            Dim ds As DataSet

            Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

            Dim Sql_Query = "select * from BSU_COMMUNICATION_M where BSC_BSU_ID='" & HiddenBsuid.Value & "' and BSC_TYPE='HELPDESK' "

            ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)

            Dim username = ""
            Dim password = ""
            Dim port = ""
            Dim host = ""
            Dim fromemailid = ""

            If ds.Tables(0).Rows.Count > 0 Then
                username = ds.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
                password = ds.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
                port = ds.Tables(0).Rows(0).Item("BSC_PORT").ToString()
                host = ds.Tables(0).Rows(0).Item("BSC_HOST").ToString()
                fromemailid = ds.Tables(0).Rows(0).Item("BSC_FROMEMAIL").ToString()
            End If

            Dim ds1 As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

            Sql_Query = "select A.EMP_ID,EMD_CUR_MOBILE,EMD_PERM_MOBILE, ISNULL(EMAIL_NOTIFY,'False')EMAIL_NOTIFY, ISNULL(SMS_NOTIFY,'False')SMS_NOTIFY, EMD_EMAIL " & _
                            " from dbo.EMPLOYEE_M  A " & _
                            " inner join OASIS_HELP_DESK.dbo.TASK_ROOTING_MASTER B on A.EMP_ID=B.EMP_ID " & _
                            " inner join dbo.EMPLOYEE_D C on A.EMP_ID = C.EMD_EMP_ID " & _
                            " where  TASK_CATEGORY_ID='" & TASK_CATEGORY_ID & "' AND (EMAIL_NOTIFY ='True' or  SMS_NOTIFY='True') " & _
                            " AND EMP_bACTIVE='True' AND MAIN_TAB_ID='" & HiddenMAIN_TAB_ID.Value & "' AND SUB_TAB_ID='" & HiddenSUB_TAB_ID.Value & "' AND EMP_TASK_BSU_ID='" & ddbsu.SelectedValue & "' "


            ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

            If ds1.Tables(0).Rows.Count > 0 Then
                Dim i = 0
                For i = 0 To ds1.Tables(0).Rows.Count - 1

                    Dim ToEmailId = ds1.Tables(0).Rows(i).Item("EMD_EMAIL").ToString()
                    Dim emp_id = ds1.Tables(0).Rows(i).Item("EMP_ID").ToString()
                    Dim email = ds1.Tables(0).Rows(i).Item("EMAIL_NOTIFY")
                    Dim sms = ds1.Tables(0).Rows(i).Item("SMS_NOTIFY")
                    Dim mobileNumber = ds1.Tables(0).Rows(i).Item("EMD_CUR_MOBILE").ToString()


                    If ds.Tables(0).Rows.Count > 0 Then
                        If email Then

                            Dim status = EmailService.email.SendPlainTextEmails(fromemailid, ToEmailId, TASK_CATEGORY, MailBody(TASK_LIST_ID, TASK_CATEGORY, PRIORITY, REQ_DATE, TASK_DESCRIPTION, TASK_NOTES), username, password, host, port, 0, False)
                            SaveEmailNotificationLog(TASK_LIST_ID, emp_id, status)

                        End If
                    End If


                    If sms Then
                        Dim message = "New Task List ID : " & TASK_LIST_ID & " , Category : " & TASK_CATEGORY & _
                                      ", Reported By : " & ddreportedby.SelectedItem.Text & _
                                      ", Priority :" & PRIORITY & _
                                      ".                               GEMS-Helpdesk"
                        Dim status = SmsService.sms.SendMessage(mobileNumber, message, "Helpdesk", "gemseducation", "manoj")
                        SaveSmsNotificationLog(TASK_LIST_ID, emp_id, status)

                    End If


                Next

            End If


        Catch ex As Exception

            lblmessage.Text = "Error: " & ex.Message

        End Try

    End Sub

    Public Function MailBody(ByVal TASK_LIST_ID As String, ByVal TASK_CATEGORY As String, ByVal PRIORITY As String, ByVal REQ_DATE As String, ByVal TASK_DESCRIPTION As String, ByVal TASK_NOTES As String) As String

        Dim sb As New StringBuilder

        sb.Append("<table border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>")
        sb.Append("<tr><td colspan='3' style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>GEMS (HelpDesk) - New Task</td></tr>")
        sb.Append("<tr><td>Task List ID.</td><td>:</td><td>" & TASK_LIST_ID & "</td></tr>")
        sb.Append("<tr><td>Reported By</td><td>:</td><td>" & ddreportedby.SelectedItem.Text & "</td></tr>")
        sb.Append("<tr><td>BSU</td><td>:</td><td>" & ddbsu.SelectedItem.Text & "</td></tr>")
        sb.Append("<tr><td>Task Category</td><td>:</td><td>" & TASK_CATEGORY & "</td></tr>")
        sb.Append("<tr><td>Priority</td><td>:</td><td>" & PRIORITY & "</td></tr>")
        sb.Append("<tr><td>Reported Date</td><td>:</td><td>" & Convert.ToDateTime(Date.Now()).ToString("dd/MMM/yyyy") & "</td></tr>")
        sb.Append("<tr><td>Require Date</td><td>:</td><td>" & REQ_DATE & "</td></tr>")
        sb.Append("<tr><td>Task Description</td><td>:</td><td>" & TASK_DESCRIPTION & "</td></tr>")
        sb.Append("<tr><td>Additional Notes</td><td>:</td><td>" & TASK_NOTES & "</td></tr>")
        sb.Append("<tr><td colspan='3' rowspan='3'><br /><br />From<br /><br />GEMS- Help Desk</td>")
        sb.Append("</tr><tr></tr><tr></tr>")
        sb.Append("</table>")

        Dim val As String = sb.ToString()

        Return val

    End Function


    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try


            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim pParms(12) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@TASK_SOURCE_ID", ddSource.SelectedValue)
            pParms(1) = New SqlClient.SqlParameter("@CALLER_BSU_ID", ddbsu.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@TO_BSU_ID", HiddenBsuid.Value)
            pParms(3) = New SqlClient.SqlParameter("@REPORTED_EMP_ID", ddreportedby.SelectedValue)

            If ddreportedby.SelectedValue = "0" Then
                pParms(4) = New SqlClient.SqlParameter("@NAME", txtname.Text.Trim)
                pParms(5) = New SqlClient.SqlParameter("@MOBILE", txtmobile.Text.Trim)
                pParms(6) = New SqlClient.SqlParameter("@EMAIL", txtemail.Text.Trim)
                pParms(7) = New SqlClient.SqlParameter("@FAX", txtfax.Text.Trim)
                pParms(8) = New SqlClient.SqlParameter("@ADDRESS", txtaddress.Text.Trim)
            End If

            pParms(9) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", HiddenUserId.Value)
            pParms(10) = New SqlClient.SqlParameter("@MAIN_TAB_ID", HiddenMAIN_TAB_ID.Value) '' 1 Internal , 2 External
            pParms(11) = New SqlClient.SqlParameter("@SUB_TAB_ID", HiddenSUB_TAB_ID.Value) '' HR 

            Dim TASK_ID = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "INSERT_TASK_CONTACT_MASTER", pParms)



            Dim dt As DataTable = Session("TASKlistIT")


            For Each row As GridViewRow In Gridjobs.Rows

                Dim categoryid = DirectCast(row.FindControl("HiddenCategoryid"), HiddenField).Value
                Dim priorityid = DirectCast(row.FindControl("HiddenPriorityid"), HiddenField).Value
                Dim category = DirectCast(row.FindControl("lblcategory"), Label).Text
                Dim priority = DirectCast(row.FindControl("lblpriority"), Label).Text
                Dim reqdate = DirectCast(row.FindControl("lblreqdate"), Label).Text
                Dim taskdes = DirectCast(row.FindControl("txtdetails"), TextBox).Text
                Dim txtnotes = DirectCast(row.FindControl("txtnotes"), TextBox).Text

                Dim pParms2(6) As SqlClient.SqlParameter
                pParms2(0) = New SqlClient.SqlParameter("@TASK_ID", TASK_ID)
                pParms2(1) = New SqlClient.SqlParameter("@TASK_CATEGORY", categoryid)
                pParms2(2) = New SqlClient.SqlParameter("@TASK_PRIORITY", priorityid)
                pParms2(3) = New SqlClient.SqlParameter("@TASK_DESCRIPTION", taskdes)
                If reqdate.ToString().Trim() <> "" Then
                    pParms2(4) = New SqlClient.SqlParameter("@TASK_TRAGET_DATE", Convert.ToDateTime(reqdate.ToString().Trim()))
                End If
                pParms2(5) = New SqlClient.SqlParameter("@TASK_NOTES", txtnotes)
                Dim TASK_LIST_ID = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "INSERT_TASK_LIST_MASTER", pParms2)

                'TASK_LIST_ID changed to TASK_ID

                SendEmailSmsNotification(TASK_ID, categoryid, category, priority, reqdate, taskdes, txtnotes)

            Next


            ''Clear all controls after save
            ddSource.SelectedIndex = 0
            ddbsu.SelectedValue = "999998"
            ddreportedby.SelectedIndex = 0


            txtname.Text = ""
            txtmobile.Text = ""
            txtemail.Text = ""
            txtfax.Text = ""
            txtaddress.Text = ""

            TreeJobCategory.CollapseAll()
            ddpriority.SelectedIndex = 0
            txtreqdate.Text = ""
            txtjobdes.Text = ""
            txtaddnotes.Text = ""

            dt.Rows.Clear()
            Session("TASKlistHR") = dt

            Gridjobs.DataSource = Nothing

            Gridjobs.DataBind()

            ''CreateDatatable()

            If Gridjobs.Rows.Count > 0 Then
                Panel4.Visible = True
                Panel1.Enabled = False
            Else
                Panel4.Visible = False
                Panel1.Enabled = True
            End If

            lblmessage.Text = "Information(s) Saved. <br><b>Reference Task Number :" + TASK_ID.ToString() + "</b>"

        Catch ex As Exception

            lblmessage.Text = "Error: " & ex.Message

        End Try

    End Sub

End Class
