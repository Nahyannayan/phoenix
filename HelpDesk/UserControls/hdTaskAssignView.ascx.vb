Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI
Partial Class HelpDesk_UserControls_hdTaskAssignView
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            HiddenAssignedTaskid.Value = Request.QueryString("AssignedTaskid")
            HiddenEmpID.Value = Session("EmployeeId")
            BindDetails()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Public Sub BindDetails()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = "SELECT TASK_ASSIGN_DESCRIPTION,UPLOAD_IDS  FROM TASK_ASSIGNED_LOG where TASK_ASSIGN_ID='" & HiddenAssignedTaskid.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            'txtdetails.Content = ds.Tables(0).Rows(0).Item("TASK_ASSIGN_DESCRIPTION").ToString()
            DataView.InnerHtml = ds.Tables(0).Rows(0).Item("TASK_ASSIGN_DESCRIPTION").ToString()
            HdFileUploadViewAssign1.Uploadids = ds.Tables(0).Rows(0).Item("UPLOAD_IDS").ToString()
            HdFileUploadViewAssign1.ShowCheck = False
        End If


    End Sub

End Class
