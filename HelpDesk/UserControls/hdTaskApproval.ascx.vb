﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient


Partial Class HelpDesk_Version2_Usercontrols_hdTaskApproval
    Inherits System.Web.UI.UserControl

    Public Enum ApprovalStatus As Integer
        Approved
        OnHold
        Rejected
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            HiddenBsuid.Value = Session("sbsuid") ''"999998"
            HiddenUserId.Value = Session("EmployeeId") ''"12581"

            GetFilter()
            ''IT Internal
            BindMainTab()

            'txtreqdate.Text = DateTime.Today.ToString("dd/MMM/yyyy")

            BindControls()
            Me.optAwaitingApproval.Checked = True
            SearchAndBindGrid_All_Pending()
        End If

    End Sub


    Public Sub GetFilter()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim Encr_decrData As New Encryption64
            Dim mednuid = "0"
            Try
                mednuid = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Catch ex As Exception

            End Try
            Dim Query = ""
            Dim ds As DataSet

            Query = "SELECT * FROM ADD_ISSUE_CAT_FILTER WHERE MENU_ID='" & mednuid & "' and BSU_ID='" & HiddenBsuid.Value & "'"
            If SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query).Tables(0).Rows.Count = 0 Then
                Query = "SELECT * FROM ADD_ISSUE_CAT_FILTER WHERE MENU_ID='" & mednuid & "'"
            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query)
            If ds.Tables(0).Rows.Count > 0 Then
                Hiddensubtabids.Value = ds.Tables(0).Rows(0).Item("SUB_TAB_IDS").ToString()
                Hiddentoplevelcatids.Value = ds.Tables(0).Rows(0).Item("TOP_LEVEL_CAT_IDS").ToString()
                Hiddentaskcatids.Value = ds.Tables(0).Rows(0).Item("TASK_CAT_IDS").ToString()
                HiddenType.Value = ds.Tables(0).Rows(0).Item("TYPE").ToString()

            End If


        Catch ex As Exception

        End Try

    End Sub
    Public Sub Bindtoplevelcat()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select '[' + CONVERT(VARCHAR,MAIN_CATEGORY_ID) + ']' as MAIN_CATEGORY_ID,MAIN_CATEGORY_DESC from  TASK_MAIN_CATEGORY where PRIMARY_MAIN_CATEGORY_ID=0 AND SUB_TAB_ID='" & ddsubtab.SelectedValue & "' ORDER BY MAIN_CATEGORY_DESC"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        Dim view As New DataView(ds.Tables(0))
        If Hiddentoplevelcatids.Value <> "" Then
            view.RowFilter = "MAIN_CATEGORY_ID not in (" & Hiddentoplevelcatids.Value & ")"
        End If


        ddtoplevelcat.DataSource = view

        ddtoplevelcat.DataTextField = "MAIN_CATEGORY_DESC"
        ddtoplevelcat.DataValueField = "MAIN_CATEGORY_ID"
        ddtoplevelcat.DataBind()

        'Dim list0 As New ListItem
        'list0.Text = "Select Top Level Category"
        'list0.Value = "-1"
        'ddtoplevelcat.Items.Insert(0, list0)

    End Sub

    Public Sub BindControls()

        ''Bind Priority

        HelpDesk.BindPriority(ddpriority)

        If Not Session("Task_Approval_Priority") Is Nothing Then
            Session.Remove("Task_Approval_Priority")
        End If
        Session.Add("Task_Approval_Priority", ddpriority.DataSource)

        ''Bind Bsu

        HelpDesk.BindBsu(ddbsu)
        BindBusinessUnit(ddbsu)
        'ddbsu.SelectedValue = HiddenBsuid.Value

        Bindtoplevelcat()
        BindJobCategory()
        BindEmpName()

    End Sub

    Sub BindBusinessUnit(ByVal ddlBUnit As DropDownList)

        Try
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
            CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
            & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
            ddlBUnit.DataSource = ds
            ddlBUnit.DataTextField = "BSU_NAME"
            ddlBUnit.DataValueField = "BSU_ID"
            ddlBUnit.DataBind()
            If Not ddlBUnit.Items.FindByValue(Session("sBsuid")) Is Nothing Then
                ddlBUnit.Items.FindByValue(Session("sBsuid")).Selected = True
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Function GetCategory(ByVal Main_Tab_id As String, ByVal Sub_Tab_id As String, ByVal bsu_id As String, ByVal toplevelid As String) As DataSet
        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim str_query = " select DISTINCT C.TASK_CATEGORY_ID,CATEGORY_DES AS TASK_CATEGORY_DESCRIPTION from dbo.SUB_TAB_MASTER A " & _
                        " INNER JOIN dbo.MAIN_TAB_MASTER B ON A.MAIN_TAB_ID= B.MAIN_TAB_ID " & _
                        " INNER JOIN dbo.TASK_CATEGORY_MASTER C ON C.SUB_TAB_ID = A.SUB_TAB_ID " & _
                        " INNER JOIN TASK_CATEGORY C1 ON C1.ID=C.CATEGORY_ID " & _
                        " INNER JOIN dbo.TASK_ROOTING_MASTER D ON A.MAIN_TAB_ID =D.MAIN_TAB_ID " & _
                        " INNER JOIN (SELECT isnull([dbo].[DepartmentLevelsID] (TASK_CATEGORY_ID),'') AS IDS,TASK_CATEGORY_ID FROM dbo.TASK_CATEGORY_MASTER) TT ON C.TASK_CATEGORY_ID=TT.TASK_CATEGORY_ID " & _
                        " WHERE TASK_CATEGORY_OWNER='True' AND " & _
                        " C.SUB_TAB_ID=D.SUB_TAB_ID AND C.TASK_CATEGORY_ID=D.TASK_CATEGORY_ID AND" & _
                        " D.MAIN_TAB_ID ='" & Main_Tab_id & "' AND " & _
                        " D.SUB_TAB_ID='" & Sub_Tab_id & "' AND " & _
                        " EMP_TASK_BSU_ID='" & bsu_id & "' "
        If ddtoplevelcat.SelectedValue <> "-1" Then
            str_query &= " AND charindex('" & ddtoplevelcat.SelectedValue & "',TT.IDS )>0"
        End If
        str_query &= " ORDER BY CATEGORY_DES "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Return ds
    End Function
    Public Sub BindJobCategory()

        ddJobCategory.Items.Clear()
        Dim ds As DataSet = GetCategory(ddmaintab.SelectedValue, ddsubtab.SelectedValue, ddbsu.SelectedValue, ddtoplevelcat.SelectedValue)

        Dim view As New DataView(ds.Tables(0))
        If Hiddentaskcatids.Value <> "" Then
            view.RowFilter = "TASK_CATEGORY_ID not in (" & Hiddentaskcatids.Value & ")"
        End If

        ddJobCategory.DataSource = view

        ddJobCategory.DataTextField = "TASK_CATEGORY_DESCRIPTION"
        ddJobCategory.DataValueField = "TASK_CATEGORY_ID"
        ddJobCategory.DataBind()

        Dim list0 As New ListItem
        list0.Text = "Select Task Category"
        list0.Value = "0"
        ddJobCategory.Items.Insert(0, list0)

        'Dim list As New ListItem
        'list.Text = "General Category"
        'list.Value = "-1"
        'ddJobCategory.Items.Insert(1, list)
    End Sub

    Public Sub BindEmpName()
        ddreportedby.Items.Clear()
        'HelpDesk.BindEmp(ddbsu.SelectedValue, ddreportedby)
        HelpDesk.BindEmp(Me.ddbsu.SelectedValue, ddreportedby)

        Dim list As New ListItem
        list.Text = "    --    "
        list.Value = "-1"
        ddreportedby.Items.Insert(0, list)

        Try
            Dim val = ddreportedby.Items.FindByValue(HiddenUserId.Value).Text

            If val <> "" Then
                ddreportedby.SelectedValue = HiddenUserId.Value
            End If
        Catch ex As Exception

        End Try

    End Sub

    Public Sub BindPriority(ByRef ddPriority As DropDownList)
        ddPriority.Items.Clear()
        'HelpDesk.BindEmp(ddbsu.SelectedValue, ddreportedby)
        HelpDesk.BindPriority(ddPriority)

        'Dim list As New ListItem
        'list.Text = "    --    "
        'list.Value = "-1"
        'ddPriority.Items.Insert(0, list)

        'Try
        '    Dim val = ddreportedby.Items.FindByValue(HiddenUserId.Value).Text

        '    If val <> "" Then
        '        ddreportedby.SelectedValue = HiddenUserId.Value
        '    End If
        'Catch ex As Exception

        'End Try

    End Sub



    Public Sub BindMainTab()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "select * from MAIN_TAB_MASTER WHERE ACTIVE='True'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddmaintab.DataSource = ds
        ddmaintab.DataTextField = "MAIN_TAB_DESCRIPTION"
        ddmaintab.DataValueField = "MAIN_TAB_ID"
        ddmaintab.DataBind()

        BindSubTab()

    End Sub


    Public Sub BindSubTab()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "select * from SUB_TAB_MASTER where MAIN_TAB_ID='" & ddmaintab.SelectedValue & "' AND ACTIVE='True'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        Dim view As New DataView(ds.Tables(0))
        If Hiddensubtabids.Value <> "" Then
            view.RowFilter = "SUB_TAB_ID not in (" & Hiddensubtabids.Value & ")"
        End If

        'For Each row As DataRowView In view
        '    row.Delete()
        'Next


        ddsubtab.DataSource = view
        ddsubtab.DataTextField = "SUB_TAB_DESCRIPTION"
        ddsubtab.DataValueField = "SUB_TAB_ID"
        ddsubtab.DataBind()


    End Sub



    Protected Sub ddmaintab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddmaintab.SelectedIndexChanged
        BindSubTab()
    End Sub

    Protected Sub ddbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddbsu.SelectedIndexChanged
        BindEmpName()
        BindJobCategory()
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        Dim commitflag = 0

        Try

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()

            Try
                Dim pParms(12) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TASK_SOURCE_ID", "6")
                pParms(1) = New SqlClient.SqlParameter("@CALLER_BSU_ID", ddbsu.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@TO_BSU_ID", HiddenBsuid.Value)
                pParms(3) = New SqlClient.SqlParameter("@REPORTED_EMP_ID", ddreportedby.SelectedValue)
                pParms(9) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", HiddenUserId.Value)
                pParms(10) = New SqlClient.SqlParameter("@MAIN_TAB_ID", ddmaintab.SelectedValue) '' 1 Internal , 2 External
                pParms(11) = New SqlClient.SqlParameter("@SUB_TAB_ID", ddsubtab.SelectedValue) '' 1 IT

                Dim TASK_ID = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_TASK_CONTACT_MASTER", pParms)


                Dim categoryid = ddJobCategory.SelectedValue

                If categoryid = "" Then

                    categoryid = "-1"

                End If

                Dim jcatogory = ddJobCategory.SelectedItem.Text

                Dim category = jcatogory
                Dim priority = ddpriority.SelectedItem.Text
                Dim reqdate = txtreqdate.Text.Trim()

                Dim pParms2(10) As SqlClient.SqlParameter
                pParms2(0) = New SqlClient.SqlParameter("@TASK_ID", TASK_ID)
                pParms2(1) = New SqlClient.SqlParameter("@TASK_CATEGORY", categoryid)
                pParms2(2) = New SqlClient.SqlParameter("@TASK_PRIORITY", ddpriority.SelectedValue)
                pParms2(3) = New SqlClient.SqlParameter("@TASK_DESCRIPTION", txtjobdes.Content)
                If reqdate.ToString().Trim() <> "" Then
                    pParms2(4) = New SqlClient.SqlParameter("@TASK_TRAGET_DATE", Convert.ToDateTime(reqdate.ToString().Trim()))
                End If
                pParms2(5) = New SqlClient.SqlParameter("@TASK_NOTES", "")
                pParms2(6) = New SqlClient.SqlParameter("@TASK_TITLE", txtTitle.Text.Trim())

                If DirectCast(hdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value <> "" Then
                    pParms2(7) = New SqlClient.SqlParameter("@UPLOAD_IDS", DirectCast(hdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value)
                End If

                pParms2(8) = New SqlClient.SqlParameter("@BSU_ID", Me.ddbsu.SelectedValue)
                pParms2(9) = New SqlClient.SqlParameter("@MAIN_TAB_ID", 1)
                pParms2(10) = New SqlClient.SqlParameter("@SUB_TAB_ID", ddsubtab.SelectedValue)

                Dim TASK_LIST_ID = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_TASK_LIST_MASTER", pParms2)

                'check if this task is suppose to go through approval process and if yes then send start approval process for it. 
                SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "[CHECK_AND_SEND_TASK_FOR_APPROVAL]", New SqlClient.SqlParameter("@TASK_LIST_MASTER_ID", TASK_LIST_ID))

                Dim reportedEmpText = ""
                If ddreportedby.SelectedValue > 0 Then
                    reportedEmpText = ddreportedby.SelectedItem.Text
                End If

                transaction.Commit()
                commitflag = 1

                lblmessage.Text = "Task Submitted Successfully. <br><b>Reference Task ID :" + TASK_LIST_ID.ToString() + "</b>"

                HelpDesk.SendEmailSmsNotification(ddmaintab.SelectedValue, ddsubtab.SelectedValue, HiddenBsuid.Value, ddbsu.SelectedValue, ddbsu.SelectedItem.Text, reportedEmpText, TASK_LIST_ID, categoryid, category, priority, reqdate, txtjobdes.Content, txtTitle.Text.Trim())

            Catch ex As Exception

                If commitflag = 0 Then
                    transaction.Rollback()
                    lblmessage.Text = "Error occured while saving . " & ex.Message
                End If

            Finally

                connection.Close()

            End Try

            ''Clear all controls after save

            ddbsu.SelectedValue = HiddenBsuid.Value
            BindEmpName()
            txtTitle.Text = ""

            ddpriority.SelectedIndex = 0
            txtreqdate.Text = DateTime.Today.ToString("dd/MMM/yyyy")
            txtjobdes.Content = ""
            DirectCast(hdFileUpload1.FindControl("GridUpload"), GridView).Controls.Clear()
            DirectCast(hdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value = ""


        Catch ex As Exception

            lblmessage.Text = "Error: " & ex.Message

        End Try

    End Sub

    Protected Sub ddsubtab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsubtab.SelectedIndexChanged
        Bindtoplevelcat()
        BindJobCategory()
    End Sub

    Protected Sub ddtoplevelcat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddtoplevelcat.SelectedIndexChanged
        BindJobCategory()
    End Sub

    Protected Sub optAwaitingApproval_CheckedChanged(sender As Object, e As System.EventArgs) Handles optAwaitingApproval.CheckedChanged
        Me.SearchAndBindGrid()
    End Sub

    Private Sub SearchAndBindGrid()
        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Dim params(4) As SqlParameter
        Try
            params(0) = New SqlClient.SqlParameter("@BSU_ID", IIf(Me.ddbsu.SelectedIndex <= 0, 0, Me.ddbsu.SelectedValue))
            params(1) = New SqlClient.SqlParameter("@MAIN_TAB_ID", 1)
            params(2) = New SqlClient.SqlParameter("@SUB_TAB_ID", IIf(Me.ddsubtab.SelectedIndex <= 0, 0, Me.ddsubtab.SelectedValue))
            params(3) = New SqlClient.SqlParameter("@@TASK_CATEGORY", IIf(Me.ddJobCategory.SelectedIndex <= 0, 0, Me.ddJobCategory.SelectedValue))

            If Me.optAwaitingApproval.Checked Then
                params(4) = New SqlClient.SqlParameter("@TASK_STATUS", "Awaiting Approval")
            ElseIf Me.optApproved.Checked Then
                params(4) = New SqlClient.SqlParameter("@TASK_STATUS", "Approved")
            ElseIf Me.optOnHold.Checked Then
                params(4) = New SqlClient.SqlParameter("@TASK_STATUS", "On Hold")
            ElseIf Me.optRejected.Checked Then
                params(4) = New SqlClient.SqlParameter("@TASK_STATUS", "Rejected")
            End If

            con.Open()

            Dim ds As DataSet = SqlHelper.ExecuteDataset(con, "GET_TASK_APPROVAL", params)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                Me.gvTaskList.DataSource = ds
                Me.gvTaskList.DataBind()
            End If

            If Me.optAwaitingApproval.Checked Then
                Me.btnApprove.Visible = True
                Me.btnOnHold.Visible = True
                Me.btnReject.Visible = True
                Me.gvTaskList.Columns(9).Visible = True 'Approve
                Me.gvTaskList.Columns(10).Visible = True 'on Hold
                Me.gvTaskList.Columns(11).Visible = True 'Reject
            ElseIf Me.optApproved.Checked Then
                Me.btnApprove.Visible = False
                Me.btnReject.Visible = False
                Me.btnOnHold.Visible = False
                Me.gvTaskList.Columns(9).Visible = False 'Approve
                Me.gvTaskList.Columns(10).Visible = False 'on Hold
                Me.gvTaskList.Columns(11).Visible = False 'Reject
            ElseIf Me.optOnHold.Checked Then
                Me.btnApprove.Visible = True
                Me.btnOnHold.Visible = False
                Me.btnReject.Visible = True
                Me.gvTaskList.Columns(9).Visible = True 'Approve
                Me.gvTaskList.Columns(10).Visible = False 'on Hold
                Me.gvTaskList.Columns(11).Visible = True 'Reject
            ElseIf Me.optRejected.Checked Then
                Me.btnApprove.Visible = False
                Me.btnReject.Visible = False
                Me.btnOnHold.Visible = False
                Me.gvTaskList.Columns(9).Visible = False 'Approve
                Me.gvTaskList.Columns(10).Visible = False 'on Hold
                Me.gvTaskList.Columns(11).Visible = False 'Reject
            End If
            Me.lblmessage.Text = ""
        Catch ex As Exception
            Me.lblmessage.Text = "There was an error getting task list."
        Finally
            If Not con.State = ConnectionState.Closed Then con.Close()
        End Try

    End Sub

    Private Sub SearchAndBindGrid_All_Pending()
        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Try
            con.Open()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(con, "GET_TASK_APPROVAL_ALL_PENDING", Nothing)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                Me.gvTaskList.DataSource = ds
                Me.gvTaskList.DataBind()
            End If

            If Me.optAwaitingApproval.Checked Then
                Me.btnApprove.Visible = True
                Me.btnOnHold.Visible = True
                Me.btnReject.Visible = True
                Me.gvTaskList.Columns(9).Visible = True 'Approve
                Me.gvTaskList.Columns(10).Visible = True 'on Hold
                Me.gvTaskList.Columns(11).Visible = True 'Reject
            ElseIf Me.optApproved.Checked Then
                Me.btnApprove.Visible = False
                Me.btnReject.Visible = False
                Me.btnOnHold.Visible = False
                Me.gvTaskList.Columns(9).Visible = False 'Approve
                Me.gvTaskList.Columns(10).Visible = False 'on Hold
                Me.gvTaskList.Columns(11).Visible = False 'Reject
            ElseIf Me.optOnHold.Checked Then
                Me.btnApprove.Visible = True
                Me.btnOnHold.Visible = False
                Me.btnReject.Visible = True
                Me.gvTaskList.Columns(9).Visible = True 'Approve
                Me.gvTaskList.Columns(10).Visible = False 'on Hold
                Me.gvTaskList.Columns(11).Visible = True 'Reject
            ElseIf Me.optRejected.Checked Then
                Me.btnApprove.Visible = False
                Me.btnReject.Visible = False
                Me.btnOnHold.Visible = False
                Me.gvTaskList.Columns(9).Visible = False 'Approve
                Me.gvTaskList.Columns(10).Visible = False 'on Hold
                Me.gvTaskList.Columns(11).Visible = False 'Reject
            End If
            Me.lblmessage.Text = ""
        Catch ex As Exception
            Me.lblmessage.Text = "There was an error getting pending task list awaiting approval."
        Finally
            If Not con.State = ConnectionState.Closed Then con.Close()
        End Try

    End Sub

    Protected Sub optApproved_CheckedChanged(sender As Object, e As System.EventArgs) Handles optApproved.CheckedChanged
        Me.SearchAndBindGrid()
    End Sub

    Protected Sub optOnHold_CheckedChanged(sender As Object, e As System.EventArgs) Handles optOnHold.CheckedChanged
        Me.SearchAndBindGrid()
    End Sub

    Protected Sub optRejected_CheckedChanged(sender As Object, e As System.EventArgs) Handles optRejected.CheckedChanged
        Me.SearchAndBindGrid()
    End Sub

    Protected Sub ddJobCategory_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddJobCategory.SelectedIndexChanged
        Me.SearchAndBindGrid()
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As System.EventArgs) Handles btnApprove.Click
        Me.SavePageData(ApprovalStatus.Approved)
    End Sub

    Protected Sub btnOnHold_Click(sender As Object, e As System.EventArgs) Handles btnOnHold.Click
        Me.SavePageData(ApprovalStatus.OnHold)
    End Sub

    Protected Sub btnReject_Click(sender As Object, e As System.EventArgs) Handles btnReject.Click
        Me.SavePageData(ApprovalStatus.Rejected)
    End Sub

    Private Sub SavePageData(ByVal Status As ApprovalStatus)
        Dim i As Integer
        Dim params(1) As SqlParameter
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Dim transaction As SqlTransaction
        Try
            connection.Open()
            transaction = connection.BeginTransaction()
            For Each row As GridViewRow In Me.gvTaskList.Rows
                If TryCast(row.FindControl("chkAction"), CheckBox).Checked Then
                    params(0) = New SqlParameter("@TASK_LIST_MASTER_ID", TryCast(row.FindControl("lblTaskListMasterId"), Label).Text)
                    Select Case Status
                        Case ApprovalStatus.Approved
                            params(1) = New SqlParameter("@STATUS", "Approved")
                        Case ApprovalStatus.OnHold
                            params(1) = New SqlParameter("@STATUS", "On Hold")
                        Case ApprovalStatus.Rejected
                            params(1) = New SqlParameter("@STATUS", "Rejected")
                    End Select
                    SqlHelper.ExecuteNonQuery(transaction, "TASK_APPROVAL_PROCESS", params)
                End If
            Next
            transaction.Commit()
            Me.SearchAndBindGrid()
            Me.lblmessage.Text = "Approval process done successfully"
        Catch ex As Exception
            transaction.Rollback()
            Me.lblmessage.Text = "There was an error performing approval process"
        Finally
            If Not connection.State = ConnectionState.Closed Then connection.Close()
        End Try

    End Sub

    Private Sub SavePageDataIndividually(ByVal Status As ApprovalStatus)
        Dim i As Integer
        Dim params(1) As SqlParameter
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Dim transaction As SqlTransaction
        Try
            connection.Open()
            transaction = connection.BeginTransaction()
            For Each row As GridViewRow In Me.gvTaskList.Rows
                If TryCast(row.FindControl("chkAction"), CheckBox).Checked Then
                    params(0) = New SqlParameter("@TASK_LIST_MASTER_ID", TryCast(row.FindControl("lblTaskListMasterId"), Label).Text)
                    Select Case Status
                        Case ApprovalStatus.Approved
                            params(1) = New SqlParameter("@STATUS", "Approved")
                        Case ApprovalStatus.OnHold
                            params(1) = New SqlParameter("@STATUS", "On Hold")
                        Case ApprovalStatus.Rejected
                            params(1) = New SqlParameter("@STATUS", "Rejected")
                    End Select
                    SqlHelper.ExecuteNonQuery(transaction, "TASK_APPROVAL_PROCESS", params)
                End If
            Next
            transaction.Commit()
            Me.SearchAndBindGrid()
            Me.lblmessage.Text = "Approval process done successfully"
        Catch ex As Exception
            If Not transaction Is Nothing Then
                transaction.Rollback()
            End If
            Me.lblmessage.Text = "There was an error performing approval process"
        Finally
            If Not connection.State = ConnectionState.Closed Then connection.Close()
        End Try

    End Sub

    Protected Sub ApproveTask(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbl As Label = TryCast(sender.FindControl("lblTaskListMasterId"), Label) 'CType(Me.gvIssueList.SelectedRow.FindControl("lblIssueID"), Label)
        Dim ddl As DropDownList = TryCast(sender.FindControl("ddpriority"), DropDownList) 'CType(Me.gvIssueList.SelectedRow.FindControl("lblIssueID"), Label)
        Dim txt As TextBox = TryCast(sender.FindControl("txtRemarks"), TextBox) 'CType(Me.gvIssueList.SelectedRow.FindControl("lblIssueID"), Label)
        Dim params(3) As SqlParameter
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Try
            params(0) = New SqlParameter("@TASK_LIST_MASTER_ID", lbl.Text)
            params(1) = New SqlParameter("@STATUS", "Approved")
            params(2) = New SqlParameter("@TASK_PRIORITY", ddl.SelectedValue)
            params(3) = New SqlParameter("@TASK_APPROVAL_REMARKS", IIf(txt.Text = "", "", txt.Text))

            connection.Open()
            SqlHelper.ExecuteNonQuery(connection, "TASK_APPROVAL_PROCESS", params)
            Me.SearchAndBindGrid()
            Me.lblmessage.Text = "Approval process done successfully"
        Catch ex As Exception
            Me.lblmessage.Text = "There was an error performing approval process"
        Finally
            If Not connection.State = ConnectionState.Closed Then connection.Close()
        End Try

    End Sub

    Protected Sub HoldTask(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim i As Integer = 0
        Dim lbl As Label = TryCast(sender.FindControl("lblTaskListMasterId"), Label) 'CType(Me.gvIssueList.SelectedRow.FindControl("lblIssueID"), Label)
        Dim ddl As DropDownList = TryCast(sender.FindControl("ddpriority"), DropDownList) 'CType(Me.gvIssueList.SelectedRow.FindControl("lblIssueID"), Label)
        Dim txt As TextBox = TryCast(sender.FindControl("txtRemarks"), TextBox) 'CType(Me.gvIssueList.SelectedRow.FindControl("lblIssueID"), Label)
        Dim params(3) As SqlParameter
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Try
            params(0) = New SqlParameter("@TASK_LIST_MASTER_ID", lbl.Text)
            params(1) = New SqlParameter("@STATUS", "On Hold")
            params(2) = New SqlParameter("@TASK_PRIORITY", ddl.SelectedValue)
            params(3) = New SqlParameter("@TASK_APPROVAL_REMARKS", IIf(txt.Text = "", "", txt.Text))

            connection.Open()
            SqlHelper.ExecuteNonQuery(connection, "TASK_APPROVAL_PROCESS", params)
            Me.SearchAndBindGrid()
            Me.lblmessage.Text = "Approval process done successfully"
        Catch ex As Exception
            Me.lblmessage.Text = "There was an error performing approval process"
        Finally
            If Not connection.State = ConnectionState.Closed Then connection.Close()
        End Try

    End Sub

    Protected Sub RejectTask(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim i As Integer = 0
        Dim lbl As Label = TryCast(sender.FindControl("lblTaskListMasterId"), Label) 'CType(Me.gvIssueList.SelectedRow.FindControl("lblIssueID"), Label)
        Dim ddl As DropDownList = TryCast(sender.FindControl("ddpriority"), DropDownList) 'CType(Me.gvIssueList.SelectedRow.FindControl("lblIssueID"), Label)
        Dim txt As TextBox = TryCast(sender.FindControl("txtRemarks"), TextBox) 'CType(Me.gvIssueList.SelectedRow.FindControl("lblIssueID"), Label)
        Dim params(3) As SqlParameter
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Try
            params(0) = New SqlParameter("@TASK_LIST_MASTER_ID", lbl.Text)
            params(1) = New SqlParameter("@STATUS", "Rejected")
            params(2) = New SqlParameter("@TASK_PRIORITY", ddl.SelectedValue)
            params(3) = New SqlParameter("@TASK_APPROVAL_REMARKS", IIf(txt.Text = "", "", txt.Text))

            connection.Open()
            SqlHelper.ExecuteNonQuery(connection, "TASK_APPROVAL_PROCESS", params)
            Me.SearchAndBindGrid()
            Me.lblmessage.Text = "Approval process done successfully"
        Catch ex As Exception
            Me.lblmessage.Text = "There was an error performing approval process"
        Finally
            If Not connection.State = ConnectionState.Closed Then connection.Close()
        End Try
    End Sub

    Protected Sub gvTaskList_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTaskList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then 'AndAlso (e.Row.RowState And DataControlRowState.Edit) = DataControlRowState.Edit Then
            Dim ddl As DropDownList = TryCast(e.Row.FindControl("ddPriority"), DropDownList)
            If ddl IsNot Nothing Then
                If Not Session("Task_Approval_Priority") Is Nothing Then
                    ddl.DataSource = Session("Task_Approval_Priority")
                    ddl.DataTextField = "PRIORITY_DESCRIPTION"
                    ddl.DataValueField = "RECORD_ID"
                    ddl.DataBind()
                End If
            End If
            Dim lbl As Label = e.Row.FindControl("lblPriority")
            ddl = e.Row.FindControl("ddpriority")
            If Not ddl Is Nothing AndAlso Not lbl Is Nothing Then
                ddl.SelectedValue = lbl.Text
            End If
        End If

    End Sub

    
End Class
