﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class HelpDesk_UserControls_hdViewOwnerMembers
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindMainTab()
            BindBsu()
            BindTaskCategory()
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub


    Public Sub BindMainTab()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select MAIN_TAB_ID,MAIN_TAB_DESCRIPTION from  MAIN_TAB_MASTER WHERE ACTIVE='True'"
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddMainTab.DataSource = ds
        ddMainTab.DataTextField = "MAIN_TAB_DESCRIPTION"
        ddMainTab.DataValueField = "MAIN_TAB_ID"
        ddMainTab.DataBind()

        If Request.QueryString("main_tab") <> "" Then
            ddMainTab.SelectedValue = Request.QueryString("main_tab")
        End If

        BindSubTabs()


    End Sub

    Public Sub BindSubTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select SUB_TAB_ID,SUB_TAB_DESCRIPTION from  SUB_TAB_MASTER where MAIN_TAB_ID='" & ddMainTab.SelectedValue & "' AND ACTIVE='True'"
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddSubTab.DataSource = ds
        ddSubTab.DataTextField = "SUB_TAB_DESCRIPTION"
        ddSubTab.DataValueField = "SUB_TAB_ID"
        ddSubTab.DataBind()

        If Request.QueryString("sub_tab") <> "" Then
            ddSubTab.SelectedValue = Request.QueryString("sub_tab")
        End If


    End Sub

    Public Sub BindBsu()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Dim Sql_Query = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M ORDER BY BSU_NAME"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        If ds.Tables(0).Rows.Count > 0 Then
            ddbsu.DataSource = ds
            ddbsu.DataTextField = "BSU_NAME"
            ddbsu.DataValueField = "BSU_ID"
            ddbsu.DataBind()
        End If

        If Request.QueryString("bsu_id") <> "" Then
            ddbsu.SelectedValue = Request.QueryString("bsu_id")
        End If


    End Sub

    Public Sub BindTaskCategory()

        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim str_query = " select DISTINCT C.TASK_CATEGORY_ID,CATEGORY_DES AS TASK_CATEGORY_DESCRIPTION from dbo.SUB_TAB_MASTER A " & _
                        " INNER JOIN dbo.MAIN_TAB_MASTER B ON A.MAIN_TAB_ID= B.MAIN_TAB_ID " & _
                        " INNER JOIN dbo.TASK_CATEGORY_MASTER C ON C.SUB_TAB_ID = A.SUB_TAB_ID " & _
                        " INNER JOIN TASK_CATEGORY C1 ON C1.ID=C.CATEGORY_ID " & _
                        " INNER JOIN dbo.TASK_ROOTING_MASTER D ON A.MAIN_TAB_ID =D.MAIN_TAB_ID " & _
                        " AND C.SUB_TAB_ID=D.SUB_TAB_ID AND C.TASK_CATEGORY_ID=D.TASK_CATEGORY_ID " & _
                        " WHERE D.MAIN_TAB_ID ='" & ddMainTab.SelectedValue & "' AND " & _
                        " D.SUB_TAB_ID='" & ddSubTab.SelectedValue & "' AND " & _
                        " EMP_TASK_BSU_ID='" & ddbsu.SelectedValue & "' "

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddJobCategory.DataSource = ds
        ddJobCategory.DataTextField = "TASK_CATEGORY_DESCRIPTION"
        ddJobCategory.DataValueField = "TASK_CATEGORY_ID"
        ddJobCategory.DataBind()

        Dim list As New ListItem
        list.Text = "Select Category"
        list.Value = "-1"
        ddJobCategory.Items.Insert(0, list)


        If Request.QueryString("cat_id") <> "" Then
            ddJobCategory.SelectedValue = Request.QueryString("cat_id")
        End If


        BindGrid()

    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = " SELECT isnull(EMP_FNAME,'') + ' ' +  isnull(EMP_MNAME ,'') + ' ' + isnull(EMP_LNAME ,'') as EMP_NAME,BSU_SHORTNAME,TASK_CATEGORY_OWNER,TASK_MEMBER,EMAIL_NOTIFY,SMS_NOTIFY,CAN_RESIGN FROM dbo.TASK_ROOTING_MASTER A " & _
                        " INNER JOIN OASIS.dbo.EMPLOYEE_M B ON  A.EMP_ID= B.EMP_ID " & _
                        " INNER JOIN OASIS.dbo.BUSINESSUNIT_M C ON C.BSU_ID = A.EMP_BSU_ID " & _
                        " WHERE A.MAIN_TAB_ID='" & ddMainTab.SelectedValue & "' AND A.SUB_TAB_ID='" & ddSubTab.SelectedValue & "' AND  A.EMP_TASK_BSU_ID='" & ddbsu.SelectedValue & "' AND A.TASK_CATEGORY_ID='" & ddJobCategory.SelectedValue & "' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        GridMembers.DataSource = ds
        GridMembers.DataBind()



    End Sub

    Protected Sub ddMainTab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddMainTab.SelectedIndexChanged
        BindSubTabs()
        lblmessage.Text = ""
    End Sub

    Protected Sub ddSubTab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddSubTab.SelectedIndexChanged
        BindTaskCategory()
        lblmessage.Text = ""
    End Sub

    Protected Sub ddbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddbsu.SelectedIndexChanged
        BindTaskCategory()
        lblmessage.Text = ""
    End Sub

    Protected Sub ddJobCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddJobCategory.SelectedIndexChanged
        BindGrid()
        lblmessage.Text = ""
    End Sub


End Class
