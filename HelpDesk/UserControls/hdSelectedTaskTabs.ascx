<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdSelectedTaskTabs.ascx.vb" Inherits="HelpDesk_UserControls_hdSelectedTaskTabs" %>
<%@ Register Src="hdClientOwnerHistory.ascx" TagName="hdClientOwnerHistory" TagPrefix="uc4" %>
<%@ Register Src="hdTaskHistory.ascx" TagName="hdTaskHistory" TagPrefix="uc3" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="hdTaskReassign.ascx" TagName="hdTaskReassign" TagPrefix="uc2" %>
<%@ Register Src="hdTaskAssignEmp.ascx" TagName="hdTaskAssignEmp" TagPrefix="uc1" %>
<div>
    <div align="left" class="matters">
     <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="1010px" >
                        <tr>
                            <td class="subheader_img">
                               Selected Task </td>
                        </tr>
                        <tr>
                            <td >

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
        <ajaxToolkit:TabContainer ID="Tab1" runat="server" AutoPostBack="true" ActiveTabIndex="0">
            <ajaxToolkit:TabPanel ID="T1" runat="server">
                <ContentTemplate>
                    <uc1:hdTaskAssignEmp ID="HdTaskAssignEmp1" runat="server" />
                
                </ContentTemplate>
                <HeaderTemplate>
                 Assign Task
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="T3" runat="server">
                <ContentTemplate>
                    <uc2:hdTaskReassign ID="HdTaskReassign1" runat="server" />
                </ContentTemplate>
                <HeaderTemplate>
                 Reassign Task
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
             <ajaxToolkit:TabPanel ID="T4" runat="server">
                <ContentTemplate>
                    <uc3:hdTaskHistory id="HdTaskHistory1" runat="server">
                    </uc3:hdTaskHistory>
                    <br />
                  
               <uc4:hdClientOwnerHistory ID="HdClientOwnerHistory2" runat="server" />
                </ContentTemplate>
                <HeaderTemplate>
                Task Status
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>

        </ajaxToolkit:TabContainer>    
            
        <asp:HiddenField ID="Hiddenempid" runat="server" />
        <asp:HiddenField ID="Hiddenbsuid" runat="server" />
        
            </ContentTemplate>
        </asp:UpdatePanel>
     


 </td>
                    </tr>
               </table>
    </div>
</div>

