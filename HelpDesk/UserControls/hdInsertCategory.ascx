<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdInsertCategory.ascx.vb" Inherits="HelpDesk_UserControls_hdInsertCategory" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server"> </ajaxToolkit:ToolkitScriptManager>

    <link type="text/css" href="../../cssfiles/sb-admin.css" rel="stylesheet" />
     <link type="text/css" href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />

<asp:UpdatePanel id="UpdatePanel1" runat="server"> 
    <contenttemplate>


 <table width="100%"  >
                        <tr>
                            <td class="title-bg" colspan="4" >
                                Category </td>
                        </tr>
                        <tr>
                            <td  align="left" width="20%" ><span class="field-label">
                                Enter Category</span></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="TextCategory" runat="server" Width="150px"></asp:TextBox>
                                </td>
                            <td align="left" width="20%"></td><td align="left" width="30%"></td>
                        </tr>
     <tr>
         <td align="center" colspan="4">
                            
                <asp:Button ID="btnadd"  class="button" runat="server"  Text="Save" />
             </td>
         </tr>
     <tr>
         <td align="center" colspan ="4">
                                
                                <asp:Label ID="lblmessage" runat="server" cssclass="text-danger"></asp:Label>
<asp:GridView ID="GridCategory" runat="server" AutoGenerateColumns="false"
    Width="100%" AllowPaging="True" OnPageIndexChanging="GridCategory_PageIndexChanging" CssClass="table table-bordered table-row">
    <Columns>
        <asp:TemplateField HeaderText="Category">
            <HeaderTemplate>
               Category
                            <br />
                                        <asp:TextBox ID="Txt1" Width="75%" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search"
                                            ImageUrl="~/Images/forum_search.gif" />
                        
            </HeaderTemplate>
            <ItemTemplate>
              
                    <%#Eval("CATEGORY_DES")%>
                
            </ItemTemplate>
            <EditItemTemplate>
                <asp:HiddenField ID="HiddenDId" runat="server" Value='<%#Eval("ID")%>' />
                <center>
                    <asp:TextBox ID="txtDataEdit" runat="server" Text='<%#Eval("CATEGORY_DES")%>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorE1" runat="server" ControlToValidate="txtDataEdit"
                        Display="None" ErrorMessage="Please Enter Category" SetFocusOnError="True"
                        ValidationGroup="Edit"></asp:RequiredFieldValidator>
                </center>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <HeaderTemplate> 
                            Edit 
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <asp:LinkButton ID="LinkEdit" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ID")%>'
                        CommandName="Edit">Edit</asp:LinkButton>
                </center>
            </ItemTemplate>
            <EditItemTemplate>
                <center>
                    <asp:LinkButton ID="LinkUpdate" runat="server" CommandArgument='<%#Eval("ID")%>'
                        CommandName="Update" ValidationGroup="Edit">Update</asp:LinkButton>
                    <asp:LinkButton ID="LinkCancel" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ID")%>'
                        CommandName="Cancel">Cancel</asp:LinkButton>
                </center>
            </EditItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <HeaderTemplate>
                            Delete
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <asp:LinkButton ID="LinkDelete" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ID")%>'
                        CommandName="deleting"> Delete</asp:LinkButton>
                    <ajaxToolkit:ConfirmButtonExtender ID="CF1" runat="server" ConfirmText="Are you sure, Do you want this record to be deleted ?"
                        TargetControlID="LinkDelete">
                    </ajaxToolkit:ConfirmButtonExtender>
                </center>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <RowStyle CssClass="griditem"  Wrap="False" />
    <EmptyDataRowStyle Wrap="False" />
    <%--<SelectedRowStyle CssClass="Green" Wrap="False" />--%>
    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
    <EditRowStyle Wrap="False" />
    <%--<AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />--%>
</asp:GridView>



 </td>
                    </tr>
               </table>
</contenttemplate>
</asp:UpdatePanel>

