﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdFinerIssues.ascx.vb" Inherits="HelpDesk_Usercontrols_hdFinerIssues" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="hdFileUpload.ascx" TagName="hdFileUpload" TagPrefix="uc1" %>


<script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />


<script lang="javascript" type="text/javascript">

    function CheckAllRows() {
        var frm = document.forms['hdTaskApproval'];

        for (var i = 0; i < document.forms[0].length; i++) {
            if (document.forms[0].elements[i].id.indexOf('chkAction') != -1) {

                //                    document.forms[0].elements[i].checked = true;

                if (document.forms[0].elements[i].checked == true) {
                    document.forms[0].elements[i].checked = false;
                }
                else if (document.forms[0].elements[i].checked == false) {
                    document.forms[0].elements[i].checked = true;
                }

            }
        }

        for (var i = 0; i < document.forms[0].length; i++) {
            if (document.forms[0].elements[i].id.indexOf('chkAction_Header') != -1) {

                //                    document.forms[0].elements[i].checked = true;

                if (document.forms[0].elements[i].checked == true) {
                    document.forms[0].elements[i].checked = false;
                }
                else if (document.forms[0].elements[i].checked == false) {
                    document.forms[0].elements[i].checked = true;
                }

            }
        }

        return false;
    }



</script>


<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-phone-square mr-3"></i>
            Finer Issues Master
        </div>

 <div class="card-body">
            <div class="table-responsive m-auto">
    <table width="100%" align="center">
       <tr>
                       
                        <td align="left" colspan="4">
                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr id="Tem" visible="false" >
                        <td></td><td></td><td></td><td></td>
                    </tr>
                    <tr id="rowBusinessUnit" runat="server" visible="false">
                        
                        <td><span class="field-label"> Business Unit </span>
                        </td>
                        
                        <td colspan="3">
                            <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        
                    </tr>
                    <tr id="rowReportedBy" runat="server" visible="false">
                        <td><span class="field-label"> Reported By/On behalf of </span>
                        </td>
                        
                        <td colspan ="3">
                            <asp:DropDownList ID="ddreportedby" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td > <span class="field-label">Category </span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddsubtab" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                        <asp:DropDownList ID="ddtoplevelcat" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>

                        <td align="left">
                        <asp:DropDownList ID="ddJobCategory" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddmaintab" runat="server" Visible="false" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        
                    </tr>
                    <tr>
                        <td> <span class="field-label">Finer Issue Description </span></td>
                        
                        <td colspan="3">
                            <asp:TextBox ID="txtFinerIssueDescr" runat="server" Width="446px"></asp:TextBox>
                            &nbsp;<asp:Button ID="btnAddNew" runat="server" CssClass="button" Text="Add" ValidationGroup="Save"
                                Width="50px" />
                        </td>
                    </tr>
                    <tr id="rowSubject" runat="server" visible="false">
                        <td><span class="field-label"> Subject </span>
                        </td>
                        
                        <td colspan="3">
                            <asp:TextBox ID="txtTitle" runat="server" Width="446px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="rowPriority" runat="server" visible="false">
                        <td> <span class="field-label">Priority </span>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="ddpriority" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="rowRequiredDate" runat="server" visible="false">
                        <td><span class="field-label"> Required Date </span>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtreqdate" runat="server"></asp:TextBox>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />
                        </td>
                    </tr>
                    <tr id="rowTaskDescription" runat="server" visible="false">
                        <td colspan="4"> <span class="field-label">Task Description </span>
                        </td>
                    </tr>
                    <tr id="rowDetailedDescription" runat="server" visible="false">
                        <td></td>
                        
                        <td colspan="3">
                            <telerik:RadEditor ID="txtjobdes" runat="server" EditModes="Design" ToolsFile="~/HelpDesk/xml/FullSetOfTools.xml"
                                Width="600px">
                            </telerik:RadEditor>
                        </td>
                    </tr>
                    <tr id="rowAttachments" runat="server" visible="false">
                        <td> <span class="field-label">
                                                       Attachments </span>
                        </td>
                        
                        <td colspan="3">
                            <br />
                            <uc1:hdFileUpload ID="hdFileUpload1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        
                        <td colspan="4" align="center" >
                            <asp:GridView ID="gvIssueList" runat="server" AutoGenerateColumns="False"
                                Width="78%" PageSize="5"
                                EnableModelValidation="True" SkinID="GridViewView" AllowPaging="True" CssClass="table table-bordered table-row" >
                                <Columns>
                                    <asp:TemplateField HeaderText="Issue ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIssueID" runat="server" Text='<%# Bind("FINER_ISSUE_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Task Category">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaskCategory0" runat="server"
                                                Text='<%# Bind("TASK_CATEGORY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="45%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Finer Issue Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIssueDescription" runat="server"
                                                Text='<%# Bind("ISSUE_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="45%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server"
                                                OnClick='DeleteFinerIssue' Text="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    
                    <tr id="rowOptions" runat="server" visible="false">
                        
                        <td align="left" colspan="4">
                            <asp:RadioButton ID="optAwaitingApproval" runat="server"
                                Text="Awaiting Approval" AutoPostBack="True" GroupName="1" />
                            <asp:RadioButton ID="optApproved" runat="server" Text="Approved"
                                AutoPostBack="True" GroupName="1" />
                            <asp:RadioButton ID="optOnHold" runat="server" Text="On Hold"
                                AutoPostBack="True" GroupName="1" />
                            <asp:RadioButton ID="optRejected" runat="server" Text="Rejected"
                                AutoPostBack="True" GroupName="1" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4"></td>
                    </tr>
                    <tr id="rowGrid" runat="server" visible="false">
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvTaskList" runat="server" AutoGenerateColumns="False"
                                Width="100%" PageSize="5"
                                EnableModelValidation="True" SkinID="GridViewView" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAction_Header" runat="server" OnClick="CheckAllRows();" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkAction" runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle Width="25px" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Task Ref ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaskListMasterId" runat="server"
                                                Text='<%# Bind("TASK_LIST_MASTER_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Task Category">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaskCategory" runat="server"
                                                Text='<%# Bind("TASK_CATEGORY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Title">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkDes" runat="server"
                                                OnClientClick='<%# Eval("VIEW_TASK") %>' Text='<%# Eval("TITLE") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="30%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Priority">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPriority" runat="server" Text='<%# Bind("PRIORITY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Initiated By">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInitiatedBy" runat="server"
                                                Text='<%# Bind("INITIATED_BY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Initiated Date/Time">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInitiatedDate" runat="server"
                                                Text='<%# Bind("INITIATED_DATE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="rowButtons" runat="server" visible="false">
                        <td align="center" colspan="4">
                        
                            <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve"
                                Width="100px" />
                            &nbsp;<asp:Button ID="btnOnHold" runat="server" CssClass="button" Text="On Hold"
                                Width="100px" />
                            &nbsp;<asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject"
                                Width="100px" />
                            <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" ValidationGroup="Save"
                                Width="100px" Visible="False" />
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
        TargetControlID="txtreqdate">
    </ajaxToolkit:CalendarExtender>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddreportedby"
        Display="None" ErrorMessage="Please Specify Reported Person" InitialValue="-1"
        SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTitle"
        Display="None" ErrorMessage="Please Enter Title" SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddJobCategory"
        Display="None" ErrorMessage="Please Select Task Category" InitialValue="0" SetFocusOnError="True"
        ValidationGroup="Save"></asp:RequiredFieldValidator>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtreqdate"
        Display="None" ErrorMessage="Please Enter Required Date" SetFocusOnError="True"
        ValidationGroup="Save"></asp:RequiredFieldValidator>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtjobdes"
        Display="None" ErrorMessage="Please Enter Job Description" SetFocusOnError="True"
        ValidationGroup="Save"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="Save" />
    <asp:HiddenField ID="HiddenBsuid" runat="server" />
    <asp:HiddenField ID="HiddenUserId" runat="server" />
    <asp:HiddenField ID="Hiddensubtabids" runat="server" />
    <asp:HiddenField ID="Hiddentoplevelcatids" runat="server" />
    <asp:HiddenField ID="Hiddentaskcatids" runat="server" />
    <asp:HiddenField ID="HiddenType" runat="server" />
</div>
     </div></div>
