Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI


Partial Class HelpDesk_UserControls_hdChangeOwner
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenTaskCategory.Value = Request.QueryString("task_category_id").ToString()
            HiddenTaskListID.Value = Request.QueryString("task_list_id").ToString()
            HiddenBsuid.Value = Session("sbsuid") ''"999998"
            HiddenEmpID.Value = Session("EmployeeId") ''"12581"
            BindOwners()
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)

        End If


    End Sub


    Public Sub BindOwners()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim Sql_Query = " SELECT AA.EMP_ID,ISNULL(EMP_FNAME,'') +' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'') EMPNAME FROM dbo.TASK_ROOTING_MASTER AA " & _
                        " INNER JOIN OASIS.dbo.EMPLOYEE_M BB ON  BB.EMP_ID=AA.EMP_ID " & _
                        " WHERE TASK_CATEGORY_ID='" & HiddenTaskCategory.Value & "' AND TASK_CATEGORY_OWNER='True' AND " & _
                        " EMP_TASK_BSU_ID=(SELECT CALLER_BSU_ID FROM dbo.TASK_CONTACT_MASTER A INNER JOIN dbo.TASK_LIST_MASTER B ON A.TASK_ID=B.TASK_ID WHERE TASK_LIST_ID='" & HiddenTaskListID.Value & "') " & _
                        " AND AA.EMP_ID !=(SELECT CLAIM_EMP_ID FROM dbo.TASK_LIST_MASTER WHERE TASK_LIST_ID='" & HiddenTaskListID.Value & "') " & _
                        " AND SUB_TAB_ID=(SELECT SUB_TAB_ID FROM dbo.TASK_CONTACT_MASTER A INNER JOIN dbo.TASK_LIST_MASTER B ON A.TASK_ID=B.TASK_ID WHERE TASK_LIST_ID='" & HiddenTaskListID.Value & "')"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            RadioOwners.DataSource = ds
            RadioOwners.DataTextField = "EMPNAME"
            RadioOwners.DataValueField = "EMP_ID"
            RadioOwners.DataBind()
        Else
            lblmessage.Text = "<b>No other Owners has been assigned for this Category. Please assign owners and then proceed.</b>"
            btnupdate.Visible = False
        End If




    End Sub


    Protected Sub btnupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Dim connection As New System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
        Dim transaction As System.Data.SqlClient.SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try
            Dim empid = RadioOwners.SelectedValue
            If empid <> "" Then
                Dim pParms(9) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", HiddenTaskListID.Value)
                pParms(1) = New SqlClient.SqlParameter("@NEW_CLAIM_EMP_ID", empid)
                lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_TASK_OWNERS", pParms)
                transaction.Commit()

                '' Send email
                SendEmailNotification(empid)

                btnupdate.Visible = False
                btncancel.Text = " OK "
            Else
                lblmessage.Text = "Please Select an Owner."
            End If


        Catch ex As Exception
            transaction.Rollback()
            lblmessage.Text = "Error occured while saving . " & ex.Message

        Finally
            connection.Close()

        End Try


    End Sub

    Public Sub SendEmailNotification(ByVal empid As String)

    End Sub

    Public Function MailBody(ByVal rootBsuText As String, ByVal ReportedEmpText As String, ByVal TASK_LIST_ID As String, ByVal TASK_CATEGORY As String, ByVal PRIORITY As String, ByVal REQ_DATE As String, ByVal TASK_DESCRIPTION As String, ByVal TASK_NOTES As String) As String

        Dim sb As New StringBuilder

        sb.Append("<table border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>")
        sb.Append("<tr><td colspan='3' style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>GEMS (HelpDesk) - New Task- Owner Reassigned.</td></tr>")
        sb.Append("<tr><td>Task List ID.</td><td>:</td><td>" & TASK_LIST_ID & "</td></tr>")
        sb.Append("<tr><td>Reported By</td><td>:</td><td>" & ReportedEmpText & "</td></tr>")
        sb.Append("<tr><td>BSU</td><td>:</td><td>" & rootBsuText & "</td></tr>")
        sb.Append("<tr><td>Task Category</td><td>:</td><td>" & TASK_CATEGORY & "</td></tr>")
        sb.Append("<tr><td>Priority</td><td>:</td><td>" & PRIORITY & "</td></tr>")
        sb.Append("<tr><td>Require Date</td><td>:</td><td>" & REQ_DATE & "</td></tr>")
        sb.Append("<tr><td>Task Description</td><td>:</td><td>" & TASK_DESCRIPTION & "</td></tr>")
        sb.Append("<tr><td>Previous Owner Was</td><td>:</td><td>" & HelpDesk.GetEmployeeName(HiddenEmpID.Value) & "</td></tr>")
        sb.Append("<tr><td colspan='3' rowspan='3'><br /><br />From<br /><br />GEMS- Help Desk</td>")
        sb.Append("</tr><tr></tr><tr></tr>")
        sb.Append("</table>")

        Dim val As String = sb.ToString()

        Return val

    End Function


End Class
