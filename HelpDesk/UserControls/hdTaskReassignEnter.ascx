<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskReassignEnter.ascx.vb" Inherits="HelpDesk_UserControls_hdTaskReassignEnter" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="hdFileUploadViewAssign.ascx" TagName="hdFileUploadViewAssign" TagPrefix="uc1" %>
<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<script type="text/javascript" >

function WindowClose()
{

window.returnValue =1 
window.close();

}

</script>
<div class="matters">

 <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="240">
        <tr>
            <td class="subheader_img">
                Reassign to</td>
        </tr>
        <tr>
            <td align="left">
                        <table>
                            <tr>
                                <td colspan="3">
                                    Select a Member</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    <asp:RadioButtonList ID="RadioTaskMembers" runat="server">
                                    </asp:RadioButtonList></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    Description</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                   
                                  <telerik:RadEditor ID="txtdetails" Width="600px" EditModes="Design" ToolsFile="../xml/FullSetOfTools.xml" runat="server"></telerik:RadEditor>
                                        
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Attachments</td>
                                <td>
                                    :</td>
                                <td>
                                    <uc1:hdFileUploadViewAssign ID="HdFileUploadViewAssign1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Start Date</td>
                                <td>
                                    :</td>
                                <td>
                                    <asp:TextBox ID="txtstartdate" runat="server"></asp:TextBox>
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                            </tr>
                            <tr>
                                <td>
                                    End Date</td>
                                <td>
                                    :</td>
                                <td>
                                    <asp:TextBox ID="txtenddate" runat="server"></asp:TextBox>
                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                            </tr>
                            <tr>
                                <td>
                                    Priority</td>
                                <td>
                                    :</td>
                                <td>
                                    <asp:DropDownList ID="ddpriority" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                                <asp:CheckBox ID="CheckSMS" runat="server" Text="Sms Notification" /></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:CheckBox ID="checkRelease" runat="server" Text="Task Release" />- (Check if
                                    the current member no longer assigned to this task)</td>
                            </tr>
                            <tr>
                                <td align="center" colspan="3">
                         <asp:Button ID="btnok" CssClass="button" runat="server" Text="Ok" Width="80px" ValidationGroup="s"  /><asp:Button ID="btncancel" CssClass="button" runat="server" OnClientClick="javascript:WindowClose();" Text="Close" Width="80px" /><br />
                                    <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></td>
                            </tr>
                        </table>
                    <asp:HiddenField ID="HiddenAssignedTaskid" runat="server" />
                        <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
                            TargetControlID="txtstartdate">
                        </ajaxToolkit:CalendarExtender>
                        <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy"
                            PopupButtonID="Image2" TargetControlID="txtenddate">
                        </ajaxToolkit:CalendarExtender>
                   
            </td>
        </tr>
    </table>
<asp:HiddenField ID="HiddenSubTabID" runat="server" />
<asp:HiddenField ID="HiddenEmpID" runat="server" />
<asp:HiddenField ID="HiddenTaskCategory" runat="server" />
<asp:HiddenField ID="HiddenTaskListID" runat="server" />
<asp:HiddenField ID="HiddenBsuid" runat="server" />
<asp:HiddenField ID="HiddenUserType" runat="server" />
</div>