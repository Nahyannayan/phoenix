<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdInbox.ascx.vb" Inherits="HelpDesk_UserControls_hdInbox" %>
   
<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Inbox
        </div>
        <div class="card-body">
            <div class="table-responsive">
<table  cellpadding="5" cellspacing="0" width="100%">
   
    <tr>
        <td>
            <asp:GridView ID="GridInbox" runat="server" AutoGenerateColumns="false" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                <Columns>
                 <asp:TemplateField HeaderText="From">
                        <HeaderTemplate>
                           
                                       From
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("EMPNAME") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Subject">
                        <HeaderTemplate>
                           
                                       Subject
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("MESSAGE_SUBJECT") %> <asp:Image ID="Image2" Visible='<%#Eval("MESSAGE_READ")%>' ImageUrl="~/Images/Helpdesk/newmail.png" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Message">
                        <HeaderTemplate>
                            
                                       Message
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                        <center>
                            <asp:LinkButton ID="LinkView" OnClientClick='<%#Eval("VIEW_MESSAGE")%>' runat="server">View</asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Entry Date">
                        <HeaderTemplate>
                           
                                        Entry Date
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center><%#Eval("ENTRY_DATE")%></center>  
                        </ItemTemplate>
                    </asp:TemplateField>
                   <asp:TemplateField HeaderText="Priority">
                        <HeaderTemplate>
                          
                                        Priority
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:Image ID="Image1" Visible='<%#Eval("HIGH_PRIORITY")%>' ImageUrl="~/Images/Helpdesk/warning.png" runat="server" />
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reply">
                        <HeaderTemplate>
                            
                                        Reply
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                 <asp:ImageButton ID="ImageMessage" ToolTip="Send Mail" OnClientClick='<%#Eval("SEND_MAIL")%>' ImageUrl="~/Images/Helpdesk/mail.png" runat="server" />
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
              
                <SelectedRowStyle CssClass="Green" Wrap="False" />
              
                <EmptyDataRowStyle Wrap="False" />
                <EditRowStyle Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenEmpid" runat="server" />
                </div>
            </div></div>