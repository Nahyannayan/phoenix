<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdFileUpload.ascx.vb" Inherits="HelpDesk_UserControls_hdFileUpload" %>

<div class="matters">


<asp:FileUpload ID="FileUploadHd" runat="server" />
<asp:Button ID="btnadd" runat="server" CssClass="button" CausesValidation="false" Text="ADD" /><br />
<br />
<asp:GridView ID="GridUpload" runat="server" AutoGenerateColumns="false"  ShowHeader="False">
<Columns>
<asp:TemplateField HeaderText="File">
<ItemTemplate>
<center>
<%#Eval("FILE_NAME") %>
</center>
</ItemTemplate>
<ItemStyle Width="200px" />
</asp:TemplateField>
<asp:TemplateField HeaderText="Delete" >
<ItemTemplate>
<center>
<asp:LinkButton ID="lnkdelete" CommandName="deleting" CommandArgument='<%#Eval("UPLOAD_ID") %>' CausesValidation="false" runat="server">Delete</asp:LinkButton>
</center>
</ItemTemplate>
<ItemStyle Width="70px" />
</asp:TemplateField>

</Columns>
</asp:GridView>
<asp:HiddenField ID="HiddenUploadid" runat="server" />

</div>