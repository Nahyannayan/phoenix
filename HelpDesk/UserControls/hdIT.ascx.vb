Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient


Partial Class HelpDesk_UserControls_hdIT
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            ''IT Internal
            HiddenMAIN_TAB_ID.Value = "1"
            HiddenSUB_TAB_ID.Value = "1"

            HiddenBsuid.Value = Session("sbsuid") ''"999998"
            HiddenUserId.Value = Session("EmployeeId") ''"12581"

            BindControls()

        End If

    End Sub

    Public Sub BindControls()
      
        ''Bind Job Source

        HelpDesk.BindTaskSource(ddSource)

        ''Bind Priority

        HelpDesk.BindPriority(ddpriority)

        ''Bind Bsu

        HelpDesk.BindBsu(ddbsu)
        ddbsu.SelectedValue = HiddenBsuid.Value


        BindJobCategory()

        BindEmpName()

    End Sub

   

    Public Sub BindJobCategory()


        Dim ds As DataSet = HelpDesk.GetOwnerAssignedTaskCategory(HiddenMAIN_TAB_ID.Value, HiddenSUB_TAB_ID.Value, ddbsu.SelectedValue)
        ddJobCategory.DataSource = ds
        ddJobCategory.DataTextField = "TASK_CATEGORY_DESCRIPTION"
        ddJobCategory.DataValueField = "TASK_CATEGORY_ID"
        ddJobCategory.DataBind()

        Dim list0 As New ListItem
        list0.Text = "Select Task Category"
        list0.Value = "0"
        ddJobCategory.Items.Insert(0, list0)

        Dim list As New ListItem
        list.Text = "General Category"
        list.Value = "-1"
        ddJobCategory.Items.Insert(1, list)

    End Sub


    Public Sub BindEmpName()
        ddreportedby.Items.Clear()
        HelpDesk.BindEmp(ddbsu.SelectedValue, ddreportedby)

        Dim list As New ListItem
        list.Text = "Reported By"
        list.Value = "-1"
        ddreportedby.Items.Insert(0, list)

        Dim list2 As New ListItem
        list2.Text = "Others"
        list2.Value = "0"
        ddreportedby.Items.Insert(1, list2)

        lblmessage.Text = ""



    End Sub

    Protected Sub ddbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddbsu.SelectedIndexChanged
        BindEmpName()
        BindJobCategory()
    End Sub



    Protected Sub ddreportedby_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddreportedby.SelectedIndexChanged
        If ddreportedby.SelectedValue = "0" Then
            Panel2.Visible = True
        Else
            Panel2.Visible = False
            txtname.Text = ""
            txtmobile.Text = ""
            txtemail.Text = ""
            txtfax.Text = ""
            txtaddress.Text = ""
        End If
        lblmessage.Text = ""

    End Sub




    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        Dim commitflag = 0

        Try

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()

            Try
                Dim pParms(12) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TASK_SOURCE_ID", ddSource.SelectedValue)
                pParms(1) = New SqlClient.SqlParameter("@CALLER_BSU_ID", ddbsu.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@TO_BSU_ID", HiddenBsuid.Value)
                pParms(3) = New SqlClient.SqlParameter("@REPORTED_EMP_ID", ddreportedby.SelectedValue)

                If ddreportedby.SelectedValue = "0" Then
                    pParms(4) = New SqlClient.SqlParameter("@NAME", txtname.Text.Trim)
                    pParms(5) = New SqlClient.SqlParameter("@MOBILE", txtmobile.Text.Trim)
                    pParms(6) = New SqlClient.SqlParameter("@EMAIL", txtemail.Text.Trim)
                    pParms(7) = New SqlClient.SqlParameter("@FAX", txtfax.Text.Trim)
                    pParms(8) = New SqlClient.SqlParameter("@ADDRESS", txtaddress.Text.Trim)
                End If

                pParms(9) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", HiddenUserId.Value)
                pParms(10) = New SqlClient.SqlParameter("@MAIN_TAB_ID", HiddenMAIN_TAB_ID.Value) '' 1 Internal , 2 External
                pParms(11) = New SqlClient.SqlParameter("@SUB_TAB_ID", HiddenSUB_TAB_ID.Value) '' 1 IT

                Dim TASK_ID = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_TASK_CONTACT_MASTER", pParms)


                Dim categoryid = ddJobCategory.SelectedValue

                If categoryid = "" Then

                    categoryid = "-1"

                End If

                Dim jcatogory = ddJobCategory.SelectedItem.Text

                Dim category = jcatogory
                Dim priority = ddpriority.SelectedItem.Text
                Dim reqdate = txtreqdate.Text.Trim()

                Dim pParms2(8) As SqlClient.SqlParameter
                pParms2(0) = New SqlClient.SqlParameter("@TASK_ID", TASK_ID)
                pParms2(1) = New SqlClient.SqlParameter("@TASK_CATEGORY", categoryid)
                pParms2(2) = New SqlClient.SqlParameter("@TASK_PRIORITY", ddpriority.SelectedValue)
                pParms2(3) = New SqlClient.SqlParameter("@TASK_DESCRIPTION", txtjobdes.Content)
                If reqdate.ToString().Trim() <> "" Then
                    pParms2(4) = New SqlClient.SqlParameter("@TASK_TRAGET_DATE", Convert.ToDateTime(reqdate.ToString().Trim()))
                End If
                pParms2(5) = New SqlClient.SqlParameter("@TASK_NOTES", "") 'txtaddnotes.Content
                pParms2(6) = New SqlClient.SqlParameter("@TASK_TITLE", txtTitle.Text.Trim())

                If DirectCast(HdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value <> "" Then
                    pParms2(7) = New SqlClient.SqlParameter("@UPLOAD_IDS", DirectCast(HdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value)
                End If

                Dim TASK_LIST_ID = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_TASK_LIST_MASTER", pParms2)

                Dim reportedEmpText = ""
                If ddreportedby.SelectedValue > 0 Then
                    reportedEmpText = ddreportedby.SelectedItem.Text
                Else
                    reportedEmpText = txtname.Text
                End If

                transaction.Commit()
                commitflag = 1
                HelpDesk.SendEmailSmsNotification(HiddenMAIN_TAB_ID.Value, HiddenSUB_TAB_ID.Value, HiddenBsuid.Value, ddbsu.SelectedValue, ddbsu.SelectedItem.Text, reportedEmpText, TASK_LIST_ID, categoryid, category, priority, reqdate, txtjobdes.Content, "")

                lblmessage.Text = "Task Submitted Successfully. <br><b>Reference Task ID :" + TASK_LIST_ID.ToString() + "</b>"

            Catch ex As Exception

                If commitflag = 0 Then
                    transaction.Rollback()
                    lblmessage.Text = "Error occured while saving . " & ex.Message
                End If

            Finally

                connection.Close()

            End Try




            ''Clear all controls after save
            ddSource.SelectedIndex = 0
            ddbsu.SelectedValue = HiddenBsuid.Value
            ddreportedby.SelectedIndex = 0


            txtname.Text = ""
            txtmobile.Text = ""
            txtemail.Text = ""
            txtfax.Text = ""
            txtaddress.Text = ""
            txtTitle.Text = ""

            ddpriority.SelectedIndex = 0
            txtreqdate.Text = ""
            txtjobdes.Content = ""
            'txtaddnotes.Content = ""
            DirectCast(HdFileUpload1.FindControl("GridUpload"), GridView).Controls.Clear()
            DirectCast(HdFileUpload1.FindControl("HiddenUploadid"), HiddenField).Value = ""


        Catch ex As Exception

            lblmessage.Text = "Error: " & ex.Message

        End Try

    End Sub

  
End Class
