﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskMonitor.ascx.vb"
    Inherits="HelpDesk_UserControls_hdTaskMonitor" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<script type="text/javascript">

    function callblur() {
        document.getElementById('<% = txtreqdate.ClientID %>').blur();
    }

    function Assign(task_list_id, task_category_id) {

        //window.open('Pages/hdTaskAssignEmp.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id, '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;
        Popup('Pages/hdTaskAssignEmp.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id)
    }
    function ReAssign(task_list_id, task_category_id) {

        //window.showModalDialog('Pages/hdTaskReassign.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id + '&Usertype=OWNER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;
        Popup('Pages/hdTaskReassign.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id + '&Usertype=OWNER')
    }

    function History(task_list_id, task_category_id) {

        //window.showModalDialog('Pages/hdTaskHistory.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id + '&Usertype=OWNER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;
        Popup('Pages/hdTaskHistory.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id + '&Usertype=OWNER')
    }


    function openimage(fname) {
        //window.showModalDialog('hdImageView.aspx?filename=' + fname + '&Filecount=' + '1', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;
        Popup('hdImageView.aspx?filename=' + fname + '&Filecount=' + '1')
    }

    function ViewTaskDes(TaskListId) {

        //window.open('Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;
        Popup('Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId)
    }

    function ChartComp(TaskListid) {

        //window.showModalDialog('FusionCharts/hdTaskProgressCompare.aspx?TaskListid=' + TaskListid, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;
        Popup('FusionCharts/hdTaskProgressCompare.aspx?TaskListid=' + TaskListid)
    }

    function Followup(task_list_id, to_emp_id) {

        //window.showModalDialog('Pages/hdFollowupClientOwner.aspx?Task_list_id=' + task_list_id + '&to_emp_id=' + to_emp_id + '&Usertype=OWNER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;
        Popup('Pages/hdFollowupClientOwner.aspx?Task_list_id=' + task_list_id + '&to_emp_id=' + to_emp_id + '&Usertype=OWNER')
    }

    function ViewTraceFlow(task_list_id) {
        //window.showModalDialog('Pages/hdTraceFlow.aspx?task_list_id=' + task_list_id, '', 'dialogHeight:600px;dialogWidth:800px;scroll:yes;resizable:yes;'); return false;
        Popup('Pages/hdTraceFlow.aspx?task_list_id=' + task_list_id)
    }

    function ViewAssigned(main_tab, sub_tab, bsu_id, cat_id) {
//        window.open('Pages/hdViewOwnerMembers.aspx?main_tab=' + main_tab + "&sub_tab=" + sub_tab + "&cat_id=" + cat_id + "&bsu_id=" + bsu_id, '', 'Height=650px,Width=750px,scrollbars=yes,resizable=no,directories=yes');
  //      return false;
        Popup('Pages/hdViewOwnerMembers.aspx?main_tab=' + main_tab + "&sub_tab=" + sub_tab + "&cat_id=" + cat_id + "&bsu_id=" + bsu_id)
    }

</script>

<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 




<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-phone-square mr-3"></i>
            Task Summary Report
        </div>

 <div class="card-body">
            <div class="table-responsive m-auto">
    
    
    <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>


    <table  width="100%" align="center">
        
        <tr>

            <td width = "20%" align="left">
                <span class="field-label">
                Task BSU </span>
            </td>

            <td width = "100%" colspan="3" >
                <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
            </td>

            
            </tr>

        <tr>
            <td width = "20%" align="left">
                <span class="field-label">
                Task ID </span>
            </td>

            <td width = "30%" align="left">
                <asp:TextBox ID="txttasklistid" runat="server" Width="100px"></asp:TextBox>
            </td>

            <td width = "20%" align="left">
                <span class="field-label">
                Task Category </span></td>
                        

            <td width = "30%" align="left"> 
                <asp:DropDownList ID="ddTaskCategory" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
            </td>

            </tr>

                <tr>
            <td width = "20%" align="left">
                <span class="field-label">
                Required Date
                    </span>
            </td>

            <td width = "30%" align="left">
                <asp:TextBox ID="txtreqdate" runat="server" Width="100px"></asp:TextBox>
                <%--<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />--%>
            </td>

            <td width = "20%" align="left">
                <span class="field-label">
                Entry Date </span>
            </td>

            <td width = "30%" align="left"> 
                <asp:TextBox ID="txtentrydate" runat="server" Width="100px"></asp:TextBox>
                <%--<asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />--%>
            </td>

            </tr>


                <tr>
            <td width = "20%" align="left">
                <span class="field-label">
                Priority </span>
            </td>

            <td width = "30%" align="left">
                <asp:DropDownList ID="ddpriority" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
            </td>

            <td width = "20%" align="left">
                <span class="field-label">
                Status </span>
            </td>

            <td width = "30%" align="left"> 
                <asp:DropDownList ID="ddstatus" runat="server" AutoPostBack="True" />
            </td>

            </tr>


                <tr>
            <td width = "20%" align="left">
                <span class="field-label">
                Reported By </span>
            </td>

            <td width = "30%" align="left">
                <asp:DropDownList ID="ddreportedby" runat="server">
                            </asp:DropDownList>
            </td>

            <td width = "20%" align="left">
                
                <span class="field-label"> Claimed By </span>
            </td>

            <td width = "30%" align="left"> 
                <asp:DropDownList ID="ddclaimedby" runat="server">
                            </asp:DropDownList>
            </td>

            </tr>

                <tr>
            <td width = "20%" align="left">
                <span class="field-label">
                Assigned By </span>
            </td>

            <td width = "30%" align="left">
                <asp:DropDownList ID="ddassignedby" runat="server">
                            </asp:DropDownList>
            </td>

            <td width = "20%" align="left">
                <span class="field-label">
                Assigned To </span>
            </td>

            <td width = "30%" align="left"> 
                <asp:DropDownList ID="ddassignedto" runat="server">
                            </asp:DropDownList>
            </td>

            </tr>

         <tr>
            <td width = "20%" align="left">
                <span class="field-label">
                Keyword </span>
            </td>

            <td width = "100%"  colspan="3" >
                <asp:TextBox ID="txtkeyword" runat="server"></asp:TextBox>
            </td>

           

            </tr>


            
                  
                  
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnsearch" runat="server" CssClass="button" Text="Search" 
                                Width="92px" />
                            <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Export" 
                                Width="92px" />
                        </td>
                    </tr>
                </table>
            

        <tr>
            <td colspan="4" align="center" width="100%"> 
                <asp:GridView ID="GridTaskList" runat="server" AllowPaging="True" PageSize="20" AutoGenerateColumns="False"
                    EmptyDataText="Search query did not retrieve any results. Please try with some other keyword(s)"
                    Width="100%" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Task ID">
                            <HeaderTemplate>
                                            Task ID
                                            <br />
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkTaskList" runat="server" OnClientClick='<%#Eval("VIEW_TRACE_FLOW")%>'
                                        Text='<%#Eval("TASK_LIST_ID")%>'></asp:LinkButton>
                                    <asp:HiddenField ID="HiddenTaskId" runat="server" Value='<%#Eval("TASK_ID")%>' />
                                    <asp:HiddenField ID="HiddenTaskListId" runat="server" Value='<%#Eval("TASK_LIST_ID")%>' />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Entry Date">
                            <HeaderTemplate>
                                            Entry Date
                                            <br />
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_DATE_TIME", "{0:dd/MMM/yyyy}")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Route Tab">
                            <HeaderTemplate>
                                
                                            Route Tab
                                            <br />
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("ROOT_TAB")%>
                                    <br />
                                    <%#Eval("BSU_SHORTNAME")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <HeaderTemplate>
                                            Category
                                            <br />
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TASK_CATEGORY_DESCRIPTION")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reported By">
                            <HeaderTemplate>
                                            Reported By
                                            <br />
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("EMPNAME")%>
                                    <%#Eval("STUNAME")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <HeaderTemplate>
                                            Title-Description
                                            <br />
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkDes" runat="server" OnClientClick='<%#Eval("VIEW_TASK")%>'
                                    Text='<%#Eval("TASK_TITLE")%>' Width="150px"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Priority">
                            <HeaderTemplate>
                                
                                            Priority
                                            <br />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("PRIORITY_DESCRIPTION")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Claimed By">
                            <HeaderTemplate>
                                            Claimed By
                                            <br />
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("CLAIM_EMPNAME")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <HeaderTemplate>
                                            Status
                                            <br />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%#Eval("IMAGE_PATH")%>' ToolTip='<%#Eval("TASK_STATUS_DESCRIPTION")%>' />
                                    <br />
                                    <span style="font-size: x-small">
                                        <%#Eval("TASK_STATUS_DESCRIPTION")%>
                                    </span>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status History">
                            <HeaderTemplate>
                                            Details
                                            <br />
                                        
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%--<asp:LinkButton ID="LinkHistory" OnClientClick='<%#Eval("HISTORY")%>' runat="server">Details</asp:LinkButton>--%>
                                    <asp:ImageButton ID="ImageHistory" runat="server" Height="20px" ImageUrl="~/Images/Helpdesk/Details.png"
                                        OnClientClick='<%#Eval("HISTORY")%>' ToolTip="View assigned and other transaction details"
                                        Width="20px" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Chart">
                            <HeaderTemplate>
                                            Chart
                                            <br />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%--<asp:LinkButton ID="LinkChart" OnClientClick='<%#Eval("CHART_COMPARE")%>' runat="server">Chart</asp:LinkButton>--%>
                                    <asp:ImageButton ID="ImageChart" runat="server" Height="20px" ImageUrl="~/Images/Helpdesk/chart.png"
                                        OnClientClick='<%#Eval("CHART_COMPARE")%>' ToolTip="View progress chart of each members"
                                        Width="20px" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="View">
                            <HeaderTemplate>
                                
                                            Members
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:ImageButton ID="ImageReAssign" OnClientClick='<%#Eval("VIEW_ASSIGNED")%>' ToolTip="View other Owners and Members."
                                        Width="20px" Height="20px" ImageUrl="~/Images/Helpdesk/Reassign.png" runat="server" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <EditRowStyle Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    
    <asp:HiddenField ID="HiddenEmpId" runat="server" />
    <ajaxToolkit:CalendarExtender id="CL1" TargetControlID="txtreqdate" PopupButtonId="txtreqdate"  Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender id="CL2" TargetControlID="txtentrydate" PopupButtonId="txtentrydate"  Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
</div></div></div>
