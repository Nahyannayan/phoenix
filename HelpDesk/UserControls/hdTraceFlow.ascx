<%@ Control Language="VB" AutoEventWireup="false" EnableTheming="false" CodeFile="hdTraceFlow.ascx.vb" Inherits="HelpDesk_UserControls_hdTraceFlow" %>
<%@ Register Src="hdFileUploadViewAssign.ascx" TagName="hdFileUploadViewAssign" TagPrefix="uc1" %>

<div align="center"  class="nottheamed">

<asp:GridView ID="GridTrace" AutoGenerateColumns="false"  ShowHeader="false" AllowPaging="false"  ShowFooter="true" runat="server">
<Columns>
<asp:TemplateField>
<ItemTemplate>
<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
    <tr>
        <td class="subheader_img">
                        Action :<%#Eval("TAB")%>   &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;Date: <%#Eval("TASK_DATE_TIME")%> </td>
    </tr>
    <tr>
        <td align="left" >
          <b>  <table>
                <tr>
                    <td >
                        From
                    </td>
                    <td >
                        :</td>
                    <td colspan="4"  style="text-transform: capitalize;">
                    <%#Eval("MFROM")%>
                    </td>
                </tr>
                <tr runat="server" visible='<%#Eval("SHOW_MTO")%>' >
                    <td style="height: 21px">
                        To</td>
                    <td style="height: 21px">
                        :</td>
                    <td colspan="4" style="text-transform: capitalize;">
                    <%#Eval("MTO")%>
                    </td>
                </tr>
                <tr runat="server" visible='<%#Eval("SHOW_TASK_TITLE")%>'>
                    <td >
                        Title</td>
                    <td >
                        :</td>
                    <td colspan="4" >
                    <%#Eval("TASK_TITLE")%>
                    </td>
                </tr>
                <tr runat="server" visible='<%#Eval("SHOW_UPLOAD_IDS")%>'>
                    <td>
                        Attachments</td>
                    <td>
                    </td>
                    <td colspan="4">
                        <asp:HiddenField ID="HiddenUploads" Value='<%#Eval("UPLOAD_IDS")%>' runat="server" />
                        <uc1:hdFileUploadViewAssign ID="HdFileUploadViewAssign1" ShowCheck="false" Uploadids='<%#Eval("UPLOAD_IDS")%>'  runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="6" rowspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                </tr>
                <tr>
                    <td colspan="6">
                        Message</td>
                </tr>
            </table></b>
            <div style="border-bottom: #999966 thin solid; border-left: #999966 thin solid; border-top: #999966 thin solid; border-right: #999966 thin solid;"> <%#Eval("TASK_DESCRIPTION")%></div>
        </td>
    </tr>
</table>


</ItemTemplate>
</asp:TemplateField>
</Columns>
</asp:GridView>
  




</div>


<asp:HiddenField ID="HiddenTaskListID" runat="server" />
