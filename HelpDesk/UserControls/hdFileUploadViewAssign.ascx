<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdFileUploadViewAssign.ascx.vb" Inherits="HelpDesk_UserControls_hdFileUploadViewAssign" %>

<asp:GridView id="GridUpload" CellPadding="0" CellSpacing="0" GridLines="None" runat="server" ShowHeader="False" AutoGenerateColumns="false" CssClass="table table-bordered table-row">
<Columns>
<asp:TemplateField HeaderText="Check" >
<ItemTemplate>
<center>
<asp:CheckBox ID="CheckUpload" Checked="true" Visible='<%#Eval("Scheck") %>' runat="server" />
<asp:HiddenField ID="HiddenUploadValue" Value='<%#Eval("UPLOAD_ID") %>' runat="server" />
</center>
</ItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="File">
<ItemTemplate>
    
    <asp:LinkButton ID="lnkFileName" Text='<%#Eval("FILE_NAME") %>' CommandName="View" CommandArgument='<%#Eval("UPLOAD_ID") %>' runat="server"></asp:LinkButton>

</ItemTemplate>
<ItemStyle Width="200px"  />
</asp:TemplateField>

</Columns>
</asp:GridView>