Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class HelpDesk_UserControls_hdClaimTask
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenUserId.Value = Session("EmployeeId")
            BindMainTab()
            Panel1.Visible = False
        End If
    End Sub

    Public Sub BindMainTab()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "select * from MAIN_TAB_MASTER WHERE ACTIVE='True'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddmaintab.DataSource = ds
        ddmaintab.DataTextField = "MAIN_TAB_DESCRIPTION"
        ddmaintab.DataValueField = "MAIN_TAB_ID"
        ddmaintab.DataBind()

        Dim list As New ListItem
        list.Text = "Select Main Tab"
        list.Value = "-1"
        ddmaintab.Items.Insert(0, list)

        BindSubTab()

    End Sub


    Public Sub BindSubTab()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "select * from SUB_TAB_MASTER where MAIN_TAB_ID='" & ddmaintab.SelectedValue & "' AND ACTIVE='True'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddsubtab.DataSource = ds
        ddsubtab.DataTextField = "SUB_TAB_DESCRIPTION"
        ddsubtab.DataValueField = "SUB_TAB_ID"
        ddsubtab.DataBind()

        Dim list As New ListItem
        list.Text = "Select Sub Tab"
        list.Value = "-1"
        ddsubtab.Items.Insert(0, list)


    End Sub

    Public Sub BindTaskDetails()

        If ddsubtab.SelectedIndex > 0 Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

            Dim Sql_Query = " SELECT A.TASK_TITLE,CLIPBOARD_IMAGE,BSU_SHORTNAME,'javascript:openimage(''' + CLIPBOARD_IMAGE + ''');return false;'imagefile, (case isnull(CLIPBOARD_IMAGE,'') when '' then 'False' else 'True' end )Visible,A.TASK_ID,TASK_LIST_ID,PRIORITY_DESCRIPTION,TASK_TRAGET_DATE,A.TASK_DATE_TIME,TASK_STATUS_DESCRIPTION,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(TASK_DESCRIPTION,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview,TASK_DESCRIPTION,A.TASK_DATE_TIME  " & _
                                              " ,ISNULL(EMP_FNAME,'') +' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'') EMPNAME " & _
                                              " ,G.NAME,ISNULL(STU_FIRSTNAME,'') +' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'') STUNAME,B.IMAGE_PATH,'javascript:ViewTaskDes('''+ CONVERT(VARCHAR,TASK_LIST_ID) +''');return false;' VIEW_TASK " & _
                                              " FROM dbo.TASK_LIST_MASTER A " & _
                                              " INNER JOIN dbo.TASK_STATUS_MASTER B ON A.TASK_STATUS =B.STATUS_ID " & _
                                              " INNER JOIN dbo.TASK_PRIORITY_MASTER C ON C.RECORD_ID=A.TASK_PRIORITY " & _
                                              " LEFT JOIN dbo.TASK_CATEGORY_MASTER D ON D.TASK_CATEGORY_ID = A.TASK_CATEGORY " & _
                                              " INNER JOIN dbo.TASK_CONTACT_MASTER G ON G.TASK_ID=A.TASK_ID " & _
                                              " INNER JOIN OASIS.dbo.BUSINESSUNIT_M G1 ON G1.BSU_ID=G.CALLER_BSU_ID " & _
                                              " LEFT JOIN OASIS.dbo.EMPLOYEE_M H ON H.EMP_ID = G.REPORTED_EMP_ID " & _
                                              " LEFT JOIN OASIS.dbo.STUDENT_M J ON J.STU_ID = G.REPORTED_STU_ID " & _
                                              " INNER JOIN dbo.TASK_CONTACT_MASTER I ON I.TASK_ID = A.TASK_ID " & _
                                              " WHERE A.TASK_CATEGORY='-1' AND I.MAIN_TAB_ID='" & ddmaintab.SelectedValue & "' AND I.SUB_TAB_ID='" & ddsubtab.SelectedValue & "' ORDER BY A.TASK_LIST_ID DESC "

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

            If ds.Tables(0).Rows.Count > 0 Then
                GridTaskList.DataSource = ds
                GridTaskList.DataBind()
                Panel1.Visible = True
            Else
                Panel1.Visible = False
                lblmessage.Text = "There is no general category under " + ddsubtab.SelectedItem.Text
            End If
        Else
            lblmessage.Text = "There is no general category under " + ddsubtab.SelectedItem.Text
            Panel1.Visible = False

        End If
       


    End Sub



    Protected Sub ddsubtab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsubtab.SelectedIndexChanged
        lblmessage.Text = ""

        BindTaskDetails()


    End Sub

    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim val = ddcategory.SelectedValue
      
        'For Each node As TreeNode In TreeJobCategory.Nodes
        '    If node.Checked Then
        '        val = node.Value
        '        node.Checked = False
        '        Exit For
        '    Else
        '        val = GetNodeValue(node)
        '        If val <> "" Then
        '            Exit For
        '        End If
        '    End If
        'Next

        If val <> "" Then

            Dim pParms2(3) As SqlClient.SqlParameter
            pParms2(0) = New SqlClient.SqlParameter("@TASK_CATEGORY", val)
            pParms2(1) = New SqlClient.SqlParameter("@TASK_LIST_ID", HiddenTasklistid.Value)
            pParms2(2) = New SqlClient.SqlParameter("@CLAIM_EMP_ID", HiddenUserId.Value)

            lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "UPDATE_CLAIM_TASK_CATEGORY", pParms2)

            BindTaskDetails()

        Else

            lblmessage.Text = "Please select a Task Category."

        End If

    End Sub

    Protected Sub GridTaskList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridTaskList.RowCommand

        If e.CommandName = "claim" Then
            HiddenTasklistid.Value = e.CommandArgument
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim Sql_Query = "select DISTINCT B.TASK_CATEGORY_ID,( CATEGORY_DES+ ' - (' + MAIN_TAB_DESCRIPTION + '/'+ SUB_TAB_DESCRIPTION + ')' ) TASK_CATEGORY_DESCRIPTION FROM dbo.TASK_CATEGORY_MASTER B " & _
                            " INNER JOIN TASK_ROOTING_MASTER C ON C.TASK_CATEGORY_ID=B.TASK_CATEGORY_ID " & _
                            " INNER JOIN TASK_CATEGORY C1 ON C1.ID=B.CATEGORY_ID  " & _
                            " INNER JOIN dbo.SUB_TAB_MASTER C2 ON C2.SUB_TAB_ID = B.SUB_TAB_ID " & _
                            " INNER JOIN dbo.MAIN_TAB_MASTER D2 ON D2.MAIN_TAB_ID = C2.MAIN_TAB_ID " & _
                            " WHERE  EMP_TASK_BSU_ID=(SELECT CALLER_BSU_ID FROM dbo.TASK_CONTACT_MASTER AA " & _
                            " INNER JOIN dbo.TASK_LIST_MASTER BB ON AA.TASK_ID=BB.TASK_ID " & _
                            " WHERE TASK_LIST_ID='" & HiddenTasklistid.Value & "') " & _
                            " AND TASK_CATEGORY_OWNER='True' "
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            ddcategory.DataSource = ds
            ddcategory.DataTextField = "TASK_CATEGORY_DESCRIPTION"
            ddcategory.DataValueField = "TASK_CATEGORY_ID"
            ddcategory.DataBind()

            MO1.Show()
        End If

    End Sub

    Protected Sub ddmaintab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddmaintab.SelectedIndexChanged

        BindSubTab()

        If ddsubtab.Items.Count > 0 Then
            BindTaskDetails()
        Else
            Panel1.Visible = False
        End If

        lblmessage.Text = ""

    End Sub

End Class
