<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdITExt.ascx.vb" Inherits="HelpDesk_UserControls_hdITExt" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript" >

function callblur()
{
 document.getElementById('<% = txtreqdate.ClientID %>').blur();
}

function OnTreeClick(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");

if(isChkBoxClick)
{
                        for(i=0; i<document.forms[0].elements.length; i++)
                           {
                               var currentid =document.forms[0].elements[i].id; 
                               if(document.forms[0].elements[i].type=="checkbox" && currentid != event.srcElement.id)
                                  {
                                    document.forms[0].elements[i].checked=false;
                                  }
                            }
}

}

function Validate()
{
if (document.getElementById('<% = ddreportedby.ClientID %>').value == 0)
{
//alert('Hi')
}

}


</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<div class="matters">
  <center><asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></center> 
<asp:Panel ID="Panel1" runat="server" Width="700">
 <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700" >
                        <tr>
                            <td class="subheader_img">
                                Basic Information</td>
                        </tr>
                        <tr>
                            <td >
<table>
    <tr>
        <td >
            Task Source</td>
        <td>
            :</td>
        <td >
            <asp:DropDownList ID="ddSource" runat="server">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td >
            Business Unit</td>
        <td>
            :</td>
        <td >
            <asp:DropDownList ID="ddbsu" runat="server">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td >
            Reported By</td>
        <td>
            :</td>
        <td >
            <asp:DropDownList ID="ddreportedby" runat="server" AutoPostBack="True">
            </asp:DropDownList></td>
    </tr>
</table>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddreportedby"
                                    Display="None" ErrorMessage="Please Specify Reported Person" InitialValue="-1"
                                    SetFocusOnError="True" ValidationGroup="SaveITExt"></asp:RequiredFieldValidator></td>
                    </tr>
               </table>
</asp:Panel>
<br />

<asp:Panel ID="Panel2" runat="server" Width="700" >
 <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700" >
                        <tr>
                            <td class="subheader_img">
                               Additional Information</td>
                        </tr>
                        <tr>
                            <td >
<table>
    <tr>
        <td >
            Name</td>
        <td >
            :</td>
        <td >
            <asp:TextBox ID="txtname" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td >
            Mobile</td>
        <td >
            :</td>
        <td >
            <asp:TextBox ID="txtmobile" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td >
            Email</td>
        <td >
            :</td>
        <td >
            <asp:TextBox ID="txtemail" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td >
            Fax</td>
        <td >
            :</td>
        <td >
            <asp:TextBox ID="txtfax" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td >
            Address</td>
        <td >
            :</td>
        <td >
            <asp:TextBox ID="txtaddress" EnableTheming="false" Width="155px" Height="100px"  runat="server"></asp:TextBox></td>
    </tr>
</table>
                                </td>
                    </tr>
               </table>
</asp:Panel>
<br />
<asp:Panel ID="Panel3" runat="server" Width="700">
               
               <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700" >
                        <tr>
                            <td class="subheader_img">
                                Task Information</td>
                        </tr>
                        <tr>
                            <td > 
               
               <table>
                   <tr>
                       <td >
                           Task Category</td>
                       <td >
                           :</td>
                       <td >
                           <asp:TreeView ID="TreeJobCategory" runat="server"  BorderColor="#404040" onclick="OnTreeClick(event);" BorderStyle="Solid" BorderWidth="1px" ShowCheckBoxes="All" ImageSet="Arrows">
                               <ParentNodeStyle Font-Bold="False" />
                               <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                               <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px"
                                   VerticalPadding="0px" />
                               <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="5px"
                                   NodeSpacing="0px" VerticalPadding="0px" />
                           </asp:TreeView>
                           </td>
                   </tr>
                   <tr>
                       <td >
                           Priority</td>
                       <td >
                           :</td>
                       <td >
                           <asp:DropDownList ID="ddpriority" runat="server">
                           </asp:DropDownList></td>
                   </tr>
                   <tr>
                       <td >
                           Required Date</td>
                       <td >
                           :</td>
                       <td >
                           <asp:TextBox ID="txtreqdate" onfocus="javascript:callblur();return false;" runat="server"></asp:TextBox>
                           <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                   </tr>
                                    <tr  >
                       <td colspan="3">
                           Task Description and Notes</td>
                   </tr>
                   <tr>
                       <td >
                           </td>
                       <td >
                           </td>
                       <td >
                       
                  <telerik:RadEditor ID="txtjobdes" Width="600px" EditModes="Design" ToolsFile="../xml/FullSetOfTools.xml" runat="server"></telerik:RadEditor>  
                  <telerik:RadEditor ID="txtaddnotes" Width="600px" EditModes="Design" ToolsFile="../xml/FullSetOfTools.xml" runat="server"></telerik:RadEditor>

                         </td>
                   </tr>
                   <tr>
                       <td align="center" colspan="3">
                           <asp:Button ID="btnadd" runat="server" CssClass="button" Text="Add" ValidationGroup="AddITExt" Width="100px" /></td>
                   </tr>
               </table>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtjobdes"
                                    Display="None" ErrorMessage="Please Enter Job Description" ValidationGroup="AddITExt" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
                    </tr>
               </table>
</asp:Panel>
<br />
    <asp:Panel ID="Panel4" runat="server" Visible="False" Width="700">
     <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700" >
                        <tr>
                            <td class="subheader_img">
                                Task Lists</td>
                        </tr>
                        <tr>
                            <td >

        <table width="100%">
            <tr>
                <td >
                    <asp:GridView ID="Gridjobs" runat="server" AutoGenerateColumns="false" Width="100%">
                    <Columns>
                    <asp:TemplateField HeaderText="Task Category">
                     <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           Task Category
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                    <ItemTemplate>
                    <asp:Label ID="lblcategory" runat="server" Text='<%#Eval("TASK_CATEGORY") %>'></asp:Label>
                    <asp:HiddenField id ="HiddenCategoryid" Value='<%#Eval("TASK_CATEGORY_ID") %>' runat="server" />
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Priority">
                     <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                         Priority
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                    <ItemTemplate>
                    <asp:Label ID="lblpriority" runat="server" Text='<%#Eval("PRIORITY") %>'></asp:Label>
                    <asp:HiddenField id ="HiddenPriorityid" Value='<%#Eval("PRIORITY_ID") %>' runat="server" />
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Required Date">
                     <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                          Required Date
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                    <ItemTemplate>
                    <asp:Label ID="lblreqdate" runat="server" Text='<%#Eval("REQ_DATE") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Task Description">
                     <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           Task Description
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                    <ItemTemplate>
                     <asp:Label ID="T5lblview" runat="server" ForeColor="Red" Text="Show"></asp:Label>
                                <asp:Panel ID="T5Panel1" runat="server" Height="50px">
                               <asp:Label ID="txtdetails" EnableTheming="false" runat="server" Text='<%#Eval("TASK_DESCRIPTION")%>'></asp:Label>
                                
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T5CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T5lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText="Show" ExpandControlID="T5lblview"
                                    ExpandedSize="240" ExpandedText="Hide" ScrollContents="true" TargetControlID="T5Panel1"
                                    TextLabelID="T5lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                     
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Task Notes">
                     <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           Task Notes
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                    <ItemTemplate>
                      <asp:Label ID="T5lblview1" runat="server" ForeColor="Red" Text="Show"></asp:Label>
                                <asp:Panel ID="T5Panel11" runat="server" Height="50px">
                               <asp:Label ID="txtnotes" EnableTheming="false" runat="server" Text='<%#Eval("TASK_NOTES")%>'></asp:Label>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T5CollapsiblePanelExtender11" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T5lblview1" Collapsed="true"
                                    CollapsedSize="0" CollapsedText="Show" ExpandControlID="T5lblview1"
                                    ExpandedSize="240" ExpandedText="Hide" ScrollContents="true" TargetControlID="T5Panel11"
                                    TextLabelID="T5lblview1">
                                </ajaxToolkit:CollapsiblePanelExtender>
                    </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Delete">
                      <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           Delete
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                    <ItemTemplate>
                    <center><asp:Button ID="btnDelete" runat="server" CommandName="delete" CssClass="button" Text="Delete" /></center>
                    </ItemTemplate>
                    </asp:TemplateField>
                    
                    </Columns>
                     <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                     <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                     <SelectedRowStyle CssClass="Green" Wrap="False" />
                     <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                     <EmptyDataRowStyle Wrap="False" />
                     <EditRowStyle Wrap="False" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center" >
                    <asp:Button ID="btnsave" runat="server"  CssClass="button" Text="Save" OnClientClick="javascript:Validate();" OnClick="btnsave_Click" ValidationGroup="SaveITExt" Width="100px" /><br />
                </td>
            </tr>
        </table>
        
 </td>
                    </tr>
               </table>
    </asp:Panel>
 
<%--<ajaxToolkit:RoundedCornersExtender ID="rce1" runat="server" TargetControlID="Panel1" Radius="6" Color="LightBlue"  BorderColor="Black"    Corners="All" />
<ajaxToolkit:RoundedCornersExtender ID="rce2" runat="server" TargetControlID="Panel2" Radius="6" Color="LightBlue" BorderColor="Black"    Corners="All" />
<ajaxToolkit:RoundedCornersExtender ID="rce3" runat="server" TargetControlID="Panel3" Radius="6" Color="LightBlue" BorderColor="Black"    Corners="All" />
<ajaxToolkit:RoundedCornersExtender ID="rce4" runat="server" TargetControlID="Panel4" Radius="6" Color="LightBlue" BorderColor="Black"    Corners="All" />
--%><ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1" TargetControlID="txtreqdate"></ajaxToolkit:CalendarExtender>

    <asp:HiddenField ID="HiddenBsuid" runat="server" />
    <asp:HiddenField ID="HiddenUserId" runat="server" />
    <asp:HiddenField ID="HiddenMAIN_TAB_ID" runat="server" />
    <asp:HiddenField ID="HiddenSUB_TAB_ID" runat="server" />
    </div>
</ContentTemplate>
</asp:UpdatePanel>


<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
    ShowSummary="False" ValidationGroup="SaveITExt" />
<asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
    ShowSummary="False" ValidationGroup="AddITExt" />
