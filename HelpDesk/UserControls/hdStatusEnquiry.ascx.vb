Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class HelpDesk_UserControls_hdStatusEnquiry
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindStatus()
            HiddenEmpId.Value = Session("EmployeeId")
            Panel1.Visible = False
            BindOwnerClosedTask()
        End If
    End Sub


    Public Sub BindOwnerClosedTask()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_OWNER_CLOSED_TASK")
        GridOwnerClose.DataSource = ds
        GridOwnerClose.DataBind()
    End Sub

    Public Sub BindStatus()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "select * from  TASK_STATUS_MASTER where STATUS_ID in (6,7)"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        ddstatus.DataSource = ds
        ddstatus.DataTextField = "TASK_STATUS_DESCRIPTION"
        ddstatus.DataValueField = "STATUS_ID"
        ddstatus.DataBind()


    End Sub
    Public Sub BindContactDetails()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "SELECT TASK_ID,TASK_SOURCE_DESCRIPTION,MOBILE,EMAIL,FAX,ADDRESS,BSU_NAME, " & _
                        " REPORTED_EMP_ID,NAME,ISNULL(EMP_FNAME,'') +' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'') EMPNAME, " & _
                        " EMD_CUR_MOBILE,EMD_EMAIL,EMD_PERM_ADDRESS " & _
                        " ,ISNULL(STU_FIRSTNAME,'') +' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'') STUNAME " & _
                        " ,STS_FMOBILE,STS_FEMAIL,STS_FFAX, " & _
                        " STS_MMOBILE,STS_MEMAIL,STS_MFAX, " & _
                        " STS_GMOBILE,STS_GEMAIL,STS_GFAX " & _
                        " STS_FPRMADDR1,STS_MPRMADDR1,STS_GPRMADDR1 " & _
                        " from dbo.TASK_CONTACT_MASTER A  " & _
                        " LEFT JOIN OASIS.dbo.EMPLOYEE_D B ON A.REPORTED_EMP_ID = B.EMD_EMP_ID  " & _
                        " LEFT JOIN OASIS.dbo.BUSINESSUNIT_M C ON A.CALLER_BSU_ID=C.BSU_ID " & _
                        " LEFT JOIN OASIS.dbo.STUDENT_M I ON I.STU_ID = A.REPORTED_STU_ID " & _
                        " LEFT JOIN OASIS.dbo.STUDENT_D J ON J.STS_STU_ID = I.STU_SIBLING_ID " & _
                        " INNER JOIN dbo.TASK_SOURCE_MASTER D ON D.TASK_SOURCE_ID = A.TASK_SOURCE_ID " & _
                        " LEFT JOIN OASIS.dbo.EMPLOYEE_M E ON E.EMP_ID=A.REPORTED_EMP_ID " & _
                        " where TASK_ID='" & txttaskid.Text.Trim() & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)


        If ds.Tables(0).Rows.Count > 0 Then
            lbltaskid.Text = ds.Tables(0).Rows(0).Item("TASK_ID").ToString()
            lbltasksource.Text = ds.Tables(0).Rows(0).Item("TASK_SOURCE_DESCRIPTION").ToString()
            lblname.Text = ds.Tables(0).Rows(0).Item("EMPNAME").ToString() & "" & ds.Tables(0).Rows(0).Item("NAME").ToString() & "" & ds.Tables(0).Rows(0).Item("STUNAME").ToString()
            lblcontactnumber.Text = ds.Tables(0).Rows(0).Item("EMD_CUR_MOBILE").ToString() & "<br>" & ds.Tables(0).Rows(0).Item("MOBILE").ToString() & "<br>" & ds.Tables(0).Rows(0).Item("STS_FMOBILE").ToString() & "<br>" & ds.Tables(0).Rows(0).Item("STS_MMOBILE").ToString() & "<br>" & ds.Tables(0).Rows(0).Item("STS_GMOBILE").ToString()
            lblemail.Text = ds.Tables(0).Rows(0).Item("EMD_EMAIL").ToString() & "<br>" & ds.Tables(0).Rows(0).Item("EMAIL").ToString() & "<br>" & ds.Tables(0).Rows(0).Item("STS_FEMAIL").ToString() & "<br>" & ds.Tables(0).Rows(0).Item("STS_MEMAIL").ToString() & "<br>" & ds.Tables(0).Rows(0).Item("STS_GEMAIL").ToString()
            lblbsu.Text = ds.Tables(0).Rows(0).Item("BSU_NAME").ToString()
            lbladdress.Text = ds.Tables(0).Rows(0).Item("EMD_PERM_ADDRESS").ToString() & "<br>" & ds.Tables(0).Rows(0).Item("ADDRESS").ToString() & "<br>" & ds.Tables(0).Rows(0).Item("STS_FPRMADDR1").ToString() & "<br>" & ds.Tables(0).Rows(0).Item("STS_MPRMADDR1").ToString() & "<br>" & ds.Tables(0).Rows(0).Item("STS_GPRMADDR1").ToString()



        End If





    End Sub


    Public Sub BindTaskDetails()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

     
        Dim Sql_Query = " SELECT CLIPBOARD_IMAGE,'javascript:openimage(''' + CLIPBOARD_IMAGE + ''');return false;'imagefile,'javascript:ViewTaskDes(''' + convert(varchar,TASK_LIST_ID) + ''');return false;' VIEW_TASK, (case isnull(CLIPBOARD_IMAGE,'') when '' then 'False' else 'True' end )Visible,A.TASK_ID,TASK_LIST_ID,PRIORITY_DESCRIPTION,TASK_TRAGET_DATE,A.TASK_DATE_TIME,TASK_STATUS_DESCRIPTION,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(TASK_DESCRIPTION,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview,TASK_DESCRIPTION,A.TASK_DATE_TIME , " & _
                        " 'javascript:History('''+ CONVERT(VARCHAR,TASK_LIST_ID) +''');return false;' HISTORY, " & _
                              " ISNULL(EMP_FNAME,'') +' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'') EMPNAME, " & _
                              " (case isnull(TASK_STATUS,'') when '8' then 'False' else 'True' end )VisibleClose " & _
                              " FROM dbo.TASK_LIST_MASTER A " & _
                              " INNER JOIN dbo.TASK_STATUS_MASTER B ON A.TASK_STATUS =B.STATUS_ID " & _
                              " INNER JOIN dbo.TASK_PRIORITY_MASTER C ON C.RECORD_ID=A.TASK_PRIORITY " & _
                              " LEFT JOIN dbo.TASK_CATEGORY_MASTER D ON D.TASK_CATEGORY_ID = A.TASK_CATEGORY " & _
                              " INNER JOIN dbo.TASK_CONTACT_MASTER G ON G.TASK_ID=A.TASK_ID " & _
                              " LEFT JOIN OASIS.dbo.EMPLOYEE_M H ON H.EMP_ID = G.REPORTED_EMP_ID " & _
                              " WHERE A.TASK_ID='" & txttaskid.Text.Trim() & "' ORDER BY A.TASK_LIST_ID "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Panel1.Visible = True
            GridTaskList.DataSource = ds
            GridTaskList.DataBind()
            lblmessage.Text = ""

        Else
            Panel1.Visible = False
            lblmessage.Text = "No Record exists"
        End If





    End Sub

    Protected Sub btnstatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnstatus.Click
        BindContactDetails()
        BindTaskDetails()
    End Sub

  
    Protected Sub GridTaskList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridTaskList.PageIndexChanging
        GridTaskList.PageIndex = e.NewPageIndex
        BindTaskDetails()
    End Sub

  

    Protected Sub GridTaskList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridTaskList.RowCommand

       If e.CommandName = "close" Then
            HiddenTasklistid.Value = e.CommandArgument
            MO3.Show()
      
        End If

    End Sub
  
    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim ownerstatus As Boolean
        Dim clientstatus As Boolean
        Dim LOGGER_STATUS As String = ""

        If ddstatus.SelectedValue = 6 Then
            clientstatus = True
            LOGGER_STATUS = "CLOSED"
        Else
            clientstatus = False
            ownerstatus = False
            LOGGER_STATUS = "REOPENED"
        End If

        Dim pParms(9) As SqlClient.SqlParameter
        If ownerstatus.ToString() <> "" Then
            pParms(0) = New SqlClient.SqlParameter("@OWNER_STATUS_CLOSED", ownerstatus)
        End If

        pParms(1) = New SqlClient.SqlParameter("@CLIENT_STATUS_CLOSED", clientstatus)
        pParms(2) = New SqlClient.SqlParameter("@TASK_LIST_ID", HiddenTasklistid.Value)
        pParms(3) = New SqlClient.SqlParameter("@TASK_STATUS_ID", ddstatus.SelectedValue)
        pParms(4) = New SqlClient.SqlParameter("@ENTRY_NOTES", txtdetails.Content)
        pParms(5) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", HiddenEmpId.Value)
        pParms(6) = New SqlClient.SqlParameter("@LOGGER", "CLIENT")
        pParms(7) = New SqlClient.SqlParameter("@LOGGER_STATUS", LOGGER_STATUS)

        Dim val As String = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "UPDATE_TASK_LOGGER_MASTER", pParms)

        Dim loggerid = Val.Split("_")(0)
        lblmessage.Text = Val.Split("_")(1)

        HelpDesk.SendClosingReopeningEmails(HiddenTasklistid.Value, txtdetails.Content, LOGGER_STATUS, HiddenEmpId.Value, "CLIENT", loggerid, "")

        BindTaskDetails()
        BindOwnerClosedTask()
        Panel1.Visible = False

    End Sub


    Protected Sub GridOwnerClose_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridOwnerClose.PageIndexChanging
        GridOwnerClose.PageIndex = e.NewPageIndex
        BindOwnerClosedTask()
    End Sub

    Protected Sub GridOwnerClose_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridOwnerClose.RowCommand
        txttaskid.Text = e.CommandArgument
        BindContactDetails()
        BindTaskDetails()
    End Sub
End Class
