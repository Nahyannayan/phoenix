<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdClaimTask.ascx.vb" Inherits="HelpDesk_UserControls_hdClaimTask" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<script type="text/javascript" >


function OnTreeClick(evt)
{
var src = window.event != window.undefined ? window.event.srcElement : evt.target;
var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");

if(isChkBoxClick)
{
                        for(i=0; i<document.forms[0].elements.length; i++)
                           {
                               var currentid =document.forms[0].elements[i].id; 
                               if(document.forms[0].elements[i].type=="checkbox" && currentid != event.srcElement.id)
                                  {
                                    document.forms[0].elements[i].checked=false;
                                  }
                            }
}

}

 function ViewTaskDes(TaskListId)
 {
       
  window.open('Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId  , '','Height:700px;Width:800px;scroll:auto;resizable:yes;'); return false;

 }
        

</script>

<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
    <tr>
        <td class="subheader_img">
            Please Select Tab Options</td>
    </tr>
    <tr>
        <td>
<table>
    <tr align="left" >
        <td align="left" >
            Main Tab</td>
        <td>
            :</td>
        <td>
            <asp:DropDownList ID="ddmaintab" runat="server" AutoPostBack="True">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td>
            Departments</td>
        <td>
            :</td>
        <td>
            <asp:DropDownList ID="ddsubtab" runat="server" AutoPostBack="True">
            </asp:DropDownList></td>
    </tr>
</table>

        </td>
    </tr>
</table>
<asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label><br />
<asp:Panel ID="Panel1" runat="server" >

<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
    <tr>
        <td class="subheader_img">
            Task Lists-General Category</td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridTaskList" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                EmptyDataText="Search query did not retrieve any results. Please try with some other keyword(s)"
                Width="680px">
                <Columns>
                    <asp:TemplateField HeaderText="Task List ID">
                     <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                          Task&nbsp;List&nbsp;ID
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("TASK_LIST_ID")%>
                                <asp:HiddenField ID="HiddenTaskListId" runat="server" Value='<%#Eval("TASK_LIST_ID")%>' />
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Entry Date">
                     <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           Entry&nbsp;Date
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("TASK_DATE_TIME", "{0:dd/MMM/yyyy}")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reported By">
                     <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           Reported&nbsp;By
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("EMPNAME")%> <%#Eval("NAME")%> <%#Eval("STUNAME")%>
                                <br />
                               (<%#Eval("BSU_SHORTNAME")%>)
                                
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Priority">
                     <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           Priority
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("PRIORITY_DESCRIPTION")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                     <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           Description
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                        <ItemTemplate>
                            <center><asp:LinkButton ID="LinkDes" OnClientClick='<%#Eval("VIEW_TASK")%>' Text='<%#Eval("TASK_TITLE")%>' runat="server"></asp:LinkButton></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Req. Date">
                     <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           Req.&nbsp;Date
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("TASK_TRAGET_DATE", "{0:dd/MMM/yyyy}")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                   
                    <asp:TemplateField HeaderText="Status">
                     <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           Status
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                               <asp:Image ID="Image1" ImageUrl='<%#Eval("IMAGE_PATH")%>' ToolTip='<%#Eval("TASK_STATUS_DESCRIPTION")%>' runat="server" />   
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Claim">
                     <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                           Claim
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LinkClaim" runat="server" CommandArgument='<%#Eval("TASK_LIST_ID")%>'
                                    CommandName="claim">Claim</asp:LinkButton></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <EditRowStyle Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
</table>
</asp:Panel>
<asp:Label ID="Label1" runat="server"></asp:Label><asp:Panel ID="PanelStatusupdate"
    runat="server" BackColor="white" CssClass="modalPopup" Width="290px" Style="display: none">
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="250">
        <tr>
            <td class="subheader_img">
                                    Select a Category</td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
                        <table>
                            <tr>
                                <td colspan="3">
                                    &nbsp;<asp:DropDownList ID="ddcategory" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="3">
                         <asp:Button ID="btnok" CssClass="button" runat="server" Text="Ok" Width="80px" ValidationGroup="s" OnClick="btnok_Click" /><asp:Button ID="btncancel" CssClass="button" runat="server" Text="Cancel" Width="80px" /></td>
                            </tr>
                        </table>
                    <asp:HiddenField ID="HiddenTasklistid" runat="server" />
                    </contenttemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="btncancel" DropShadow="true" PopupControlID="PanelStatusupdate"
    RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="Label1">
</ajaxToolkit:ModalPopupExtender>
<asp:HiddenField ID="HiddenUserId" runat="server" />
