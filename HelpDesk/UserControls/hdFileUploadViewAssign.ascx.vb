Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class HelpDesk_UserControls_hdFileUploadViewAssign
    Inherits System.Web.UI.UserControl

    Dim mID As String
    Dim Scheck As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            bindgrid()
        End If

        'For Each row As GridViewRow In GridUpload.Rows
        '    Dim link1 As LinkButton = DirectCast(row.FindControl("lnkFileName"), LinkButton)
        '    ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(link1)
        'Next

    End Sub


    Public Property Uploadids() As String
        Get
            Return mID
        End Get
        Set(ByVal value As String)
            mID = value
        End Set
    End Property

    Public Property ShowCheck() As Boolean
        Get
            Return Scheck
        End Get
        Set(ByVal value As Boolean)
            Scheck = value
        End Set
    End Property


    Public Sub bindgrid()

        If mID <> "" Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim sql_query = "select *,'" & Scheck.ToString() & "' Scheck from FILE_UPLOAD_MASTER where UPLOAD_ID in (" + mID + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
            GridUpload.DataSource = ds
            GridUpload.DataBind()
        End If


    End Sub

    Private Function GetExtension(ByVal FileName As String) As String

        Dim Extension As String

        Dim split As String() = FileName.Split(".")
        Extension = split(split.Length - 1)

        Return Extension


    End Function

    Protected Sub GridUpload_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridUpload.RowCommand

        Try
            If e.CommandName = "View" Then
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

                Dim serverpath As String = WebConfigurationManager.AppSettings("HelpdeskAttachments").ToString

                Dim sql_query = "select * from FILE_UPLOAD_MASTER where UPLOAD_ID ='" & e.CommandArgument & "'"

                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
                If ds.Tables(0).Rows.Count > 0 Then

                    Dim path = serverpath & e.CommandArgument & "." & GetExtension(ds.Tables(0).Rows(0).Item("FILE_NAME").ToString())

                    Dim bytes() As Byte = File.ReadAllBytes(path)
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/octect-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                    Response.BinaryWrite(bytes)

                    Response.Flush()

                    Response.End()
                    'HttpContext.Current.Response.ContentType = "application/octect-stream"
                    'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                    'HttpContext.Current.Response.Clear()
                    'HttpContext.Current.Response.WriteFile(path)
                    'HttpContext.Current.Response.End()

                End If

            End If
        Catch ex As Exception

        End Try


    End Sub
End Class
