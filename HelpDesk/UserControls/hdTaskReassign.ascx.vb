Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI

Partial Class HelpDesk_UserControls_hdTaskReassign
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then


            HiddenTaskCategory.Value = Request.QueryString("task_category_id").ToString()
            HiddenTaskListID.Value = Request.QueryString("task_list_id").ToString()
            HiddenUserType.Value = Request.QueryString("Usertype").ToString()

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

            HiddenSubTabID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "SELECT SUB_TAB_ID FROM TASK_CATEGORY_MASTER WHERE TASK_CATEGORY_ID='" & HiddenTaskCategory.Value & "'")



            HiddenBsuid.Value = Session("sbsuid") ''"999998"
            HiddenEmpID.Value = Session("EmployeeId") ''"12581"

            BindNewAssignedTask()
            CheckClosed()

            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)

        End If

    End Sub

    Public Sub CheckClosed()
        '' Check if the task has been closed
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "select TASK_STATUS from TASK_LIST_MASTER where TASK_LIST_ID='" & HiddenTaskListID.Value & "'"

        Dim status As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)
        If status = "6" Or status = "8" Then
            GridTaskStatus.Visible = False
        Else
            GridTaskStatus.Visible = True
        End If
    End Sub

    Public Sub BindNewAssignedTask()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()


        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TASK_LIST_ID", HiddenTaskListID.Value)

        Dim ds As DataSet = Nothing

        If HiddenUserType.Value = "MEMBER" Then
            pParms(1) = New SqlClient.SqlParameter("@TASK_ASSIGNED_TO_EMP_ID", HiddenEmpID.Value)
        End If

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TASK_SEARCH_GET_ASSIGNED_TASK_LIST", pParms)

        GridTaskStatus.DataSource = ds
        GridTaskStatus.DataBind()


    End Sub


    Protected Sub GridTaskStatus_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridTaskStatus.PageIndexChanging
        GridTaskStatus.PageIndex = e.NewPageIndex
        BindNewAssignedTask()
    End Sub
End Class
