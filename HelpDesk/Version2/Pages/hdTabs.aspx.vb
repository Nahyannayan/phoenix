﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Partial Class HelpDesk_Version2_Pages_hdTabs
    Inherits BasePage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            lnkhome.Visible = False
            Dim url = Request.Url

            Dim tab = ""
            Try
                If Request.Url.ToString().Contains("hdTabs.aspx?at=1") Then
                    tab = 1 - 1
                    Session("hdv2l") = 1
                    lnkhome.Visible = True
                ElseIf Request.Url.ToString().Contains("hdTabs.aspx?at=2") Then
                    tab = 2 - 1
                    Session("hdv2l") = 1
                    lnkhome.Visible = True
                Else
                    tab = Session("ActiveTab") - 1
                End If
            Catch ex As Exception
                tab = 0
            End Try

            Tab1.ActiveTabIndex = tab
            HiddenTabid.Value = tab

            Try
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
                Dim Query = " select top 1 RECORD_ID from dbo.TASK_ROOTING_MASTER where EMP_ID='" & Session("EmployeeId") & "' and TASK_CATEGORY_OWNER=1 "
                If SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query).Tables(0).Rows.Count = 0 Then
                    HT3.Enabled = False
                    HT3.Visible = False
                End If
                Query = " select top 1 RECORD_ID from dbo.TASK_ROOTING_MASTER where EMP_ID='" & Session("EmployeeId") & "' and TASK_MEMBER=1 "
                If SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query).Tables(0).Rows.Count = 0 Then
                    HT4.Enabled = False
                    HT4.Visible = False
                End If


            Catch ex As Exception

            End Try

        End If


    End Sub

    Protected Sub lnkhome_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkhome.Click
        Response.Redirect("~/Modulelogin.aspx")
    End Sub

   


End Class
