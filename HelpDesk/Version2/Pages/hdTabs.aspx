﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdTabs.aspx.vb" Inherits="HelpDesk_Version2_Pages_hdTabs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
       <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <title>Help Desk</title>
     <script type="text/javascript">



         window.setTimeout('first()', 1000);

         function first() {
             var c = parseInt(document.getElementById('HiddenTabid').value) + 1;
             setpathExt();
         }
         function setpathExt() {

             var path = window.location.href
             var Rpath = ''
             if (path.indexOf('?') != '-1') {
                 Rpath = path.substring(path.indexOf('?'), path.length)
             }
             var objFrame

             objFrame = document.getElementById("FEx1");
             objFrame.src = "../TabsPages/hdMyComplaints.aspx" + Rpath



             objFrame = document.getElementById("FEx2");
             objFrame.src = "../TabsPages/hdAddIssues.aspx" + Rpath



             objFrame = document.getElementById("FEx3");
             if (objFrame != null)
             {
                 objFrame.src = "../TabsPages/hdAssignTask2Members.aspx" + Rpath
             }
           



             objFrame = document.getElementById("FEx4");
             if (objFrame != null) {
                 objFrame.src = "../TabsPages/hdEnterTaskStatus.aspx" + Rpath
             }


             objFrame = document.getElementById("FEx5");
             objFrame.src = "../TabsPages/hdInbox.aspx" + Rpath


             objFrame = document.getElementById("FEx6");
             objFrame.src = "../TabsPages/hdOutbox.aspx" + Rpath





         }

    </script>
</head>
<body style="margin-top: 0; margin-left: 0; margin-right: 0;">
    <form id="form1" style="width: 100%" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div style="height: 40px; width: 100%; font-size: large; background-color: #8DC24C;
        font-family: Tahoma; color: #FFFFFF; font-weight: bold; vertical-align: middle">
        <table style="height: 100%; width: 100%">
            <tr>
                <td valign="middle">
                    &nbsp;&nbsp;&nbsp;PHOENIX Helpdesk &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                    <asp:LinkButton ID="lnkhome" runat="server" ForeColor="White" Font-Size="Small">Home</asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
    <br />
         <div class="card mb-3">
   <div class="card-body">
            <div class="table-responsive">
        <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
            <ajaxToolkit:TabPanel ID="HT1" runat="server">
                <ContentTemplate>
                    <iframe id="FEx1" height="1000" scrolling="auto" marginwidth="0px" frameborder="0"
                        width="100%"></iframe>
                </ContentTemplate>
                <HeaderTemplate>
                    My Issues
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT2" runat="server">
                <ContentTemplate>
                    <iframe id="FEx2" height="1000" scrolling="auto" marginwidth="0px" frameborder="0"
                        width="100%"></iframe>
                </ContentTemplate>
                <HeaderTemplate>
                   Add Issue
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT3" runat="server">
                <ContentTemplate>
                    <iframe id="FEx3" height="1000" scrolling="auto" marginwidth="0px" frameborder="0"
                        width="100%"></iframe>
                </ContentTemplate>
                <HeaderTemplate>
                  Assign Task to Members/Task Progress Details
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT4" runat="server">
                <ContentTemplate>
                    <iframe id="FEx4" height="1000" scrolling="auto" marginwidth="0px" frameborder="0"
                        width="100%"></iframe>
                </ContentTemplate>
                <HeaderTemplate>
                    Update Task Progress
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT5" Width="800" runat="server">
                <ContentTemplate>
                    <iframe id="FEx5" height="1000" scrolling="auto" marginwidth="0px" frameborder="0"
                        width="100%"></iframe>
                </ContentTemplate>
                <HeaderTemplate>
                   Inbox
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT6" Width="800" runat="server">
                <ContentTemplate>
                    <iframe id="FEx6" height="1000" scrolling="auto" marginwidth="0px" frameborder="0"
                        width="100%"></iframe>
                </ContentTemplate>
                <HeaderTemplate>
                Outbox
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>
        <asp:HiddenField ID="Hiddenempid" runat="server" />
        <asp:HiddenField ID="Hiddenbsuid" runat="server" />
        <asp:HiddenField ID="HiddenTabid" runat="server" />
    </div>
       </div>
       </div>
    </form>
</body>
</html>
