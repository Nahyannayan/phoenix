﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdAuthenticate.aspx.vb"
    Inherits="HelpDesk_Version2_Pages_hdAuthenticate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>OASIS Helpdesk Login</title>
    <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" defaultbutton="btnlogin" runat="server">
    <div style="background-image: url(../../../Images/Helpdesk/diamonds_bg.jpg); height: 500px;
        width: 1000px; margin-right: auto; margin-left: auto; background-repeat: no-repeat;">
        <table width="100%" style="height: 100%">
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td valign="bottom">
                    <table>
                        <tr>
                            <td style="font-size: x-large; font-family: Verdana; color: #003399">
                                Username
                            </td>
                            <td style="font-size: x-large; font-family: Verdana; color: #003399">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddUser" EnableTheming="false" Font-Size="X-Large" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: x-large; font-family: Verdana; color: #003399">
                             
                                Password</td>
                            <td style="font-size: x-large; font-family: Verdana; color: #003399">
                             
                                :</td>
                            <td>
                                <asp:TextBox ID="txtpassword" runat="server" Font-Size="X-Large" EnableTheming="false"
                                    Width="300px" Height="30px" TextMode="Password"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnlogin" runat="server" CssClass="button" Text="Login" Width="175px"
                                    Height="30px" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                &nbsp;
                                <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField ID="HiddenActiveTab" runat="server" />
    <asp:HiddenField ID="HiddenTaskId" runat="server" />
    </form>
</body>
</html>
