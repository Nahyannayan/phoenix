﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI
Imports System.Data.SqlClient

Partial Class HelpDesk_Version2_Pages_hdAuthenticate
    Inherits BasePage

    Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Dim str_query = " SELECT  upper(USR_NAME)USR_NAME,A.EMP_ID,isnull(emp_displayname,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_LNAME, '')) EMP_NAME,USR_PASSWORD,EMP_BSU_ID,USR_bSuper from OASIS.dbo.EMPLOYEE_M  A " & _
                            " INNER JOIN dbo.USERS_M B ON A.EMP_ID=B.USR_EMP_ID WHERE A.EMP_ID='" & Request.QueryString("emid") & "'"

            'Request.QueryString("emid") "9232"

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            btnlogin.Visible = False

            If ds.Tables(0).Rows.Count > 0 Then

                btnlogin.Visible = True
                ddUser.DataSource = ds
                ddUser.DataTextField = "USR_NAME"
                ddUser.DataValueField = "EMP_ID"
                ddUser.DataBind()

            End If
            txtpassword.Focus()
            HiddenActiveTab.Value = Request.QueryString("atab")
            HiddenTaskId.Value = Request.QueryString("tlid")
        End If


    End Sub

    Protected Sub btnlogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlogin.Click
        Dim Encr_decrData As New Encryption64
        Dim password As String = Encr_decrData.Encrypt(txtpassword.Text)

        Dim str_query = " SELECT  upper(USR_NAME)USR_NAME,A.EMP_ID,isnull(emp_displayname,ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_LNAME, '')) EMP_NAME,USR_PASSWORD,EMP_BSU_ID,USR_bSuper from OASIS.dbo.EMPLOYEE_M  A " & _
                        " INNER JOIN dbo.USERS_M B ON A.EMP_ID=B.USR_EMP_ID WHERE A.EMP_ID='" & ddUser.SelectedValue & "' AND USR_NAME='" & ddUser.SelectedItem.Text & "' AND USR_PASSWORD='" & password & "'"
 

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            Session("hdv2") = "1"
            Session("EmployeeId") = ds.Tables(0).Rows(0).Item("EMP_ID").ToString()
            Session("sbsuid") = ds.Tables(0).Rows(0).Item("EMP_BSU_ID").ToString()
            Session("sBusper") = ds.Tables(0).Rows(0).Item("USR_bSuper").ToString()
            Session("ActiveTab") = HiddenActiveTab.value
            Session("Task_ID") = HiddenTaskId.Value
            Response.Redirect("hdTabs.aspx")
        Else
            lblmessage.Text = "Login Failed..Please try again..(If you are not the valid user displayed, please close this window.)"

        End If




    End Sub

End Class
