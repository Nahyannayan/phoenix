﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="uctlHdMenu.ascx.vb" Inherits="HelpDesk_UserControls_uctlHdMenu" %>
 <div style="height: 40px; width: 100%; font-size: large; background-color: #3B5998;
        font-family: Tahoma; color: #FFFFFF; font-weight: bold; vertical-align: middle">
        <table style="height: 100%; width: 100%">
            <tr>
                <td valign="middle">
                    &nbsp;&nbsp;&nbsp;OASIS Helpdesk &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                    <asp:LinkButton ID="lnkhome" runat="server" ForeColor="White" Font-Size="Small">Home</asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
    <br />
<div align="left" class="matters" style="direction:none;">
     <asp:LinkButton ID="lbtnStep1" runat="server" CssClass="" PostBackUrl="../TabsPages/hdMyComplaints.aspx">
                
                 <div class="cssStepContainer">           
                             <div class="cssStepInfo">My Issues</div>     
                 </div>
                    
                
        </asp:LinkButton>
 <asp:LinkButton ID="LinkButton1" runat="server" CssClass="" PostBackUrl="../TabsPages/hdAddIssues.aspx">
                
                 <div class="cssStepContainer">           
                             <div class="cssStepInfo">Add Issue</div>     
                 </div>
                    
                
        </asp:LinkButton>
         <asp:LinkButton ID="LinkButton2" runat="server" CssClass="" PostBackUrl="../TabsPages/hdAssignTask2Members.aspx">
                
                 <div class="cssStepContainer1">           
                             <div class="cssStepInfo">Assign Task to Members/Task Progress Details</div>     
                 </div>
                    
                
        </asp:LinkButton>
         <asp:LinkButton ID="LinkButton3" runat="server" CssClass="" PostBackUrl="../TabsPages/hdEnterTaskStatus.aspx">
                
                 <div class="cssStepContainer1">           
                             <div class="cssStepInfo">Update Task Progress</div>     
                 </div>
                    
                
        </asp:LinkButton>
         <asp:LinkButton ID="LinkButton4" runat="server" CssClass="" PostBackUrl="../TabsPages/hdInbox.aspx">
                
                 <div class="cssStepContainer">           
                             <div class="cssStepInfo">Inbox</div>     
                 </div>
                    
                
        </asp:LinkButton>

            <asp:LinkButton ID="LinkButton5" runat="server" CssClass="" PostBackUrl="../TabsPages/hdOutbox.aspx">
               
                 <div class="cssStepContainer">           
                             <div class="cssStepInfo">Outbox</div>     
                 </div>
                    
                
        </asp:LinkButton>

        </div>