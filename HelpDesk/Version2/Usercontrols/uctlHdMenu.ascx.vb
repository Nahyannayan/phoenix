﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class HelpDesk_UserControls_uctlHdMenu
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then


            Try
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
                Dim Query = " select top 1 RECORD_ID from dbo.TASK_ROOTING_MASTER where EMP_ID='" & Session("EmployeeId") & "' and TASK_CATEGORY_OWNER=1 "
                If SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query).Tables(0).Rows.Count = 0 Then
                    LinkButton2.Visible = False
                End If
                Query = " select top 1 RECORD_ID from dbo.TASK_ROOTING_MASTER where EMP_ID='" & Session("EmployeeId") & "' and TASK_MEMBER=1 "
                If SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Query).Tables(0).Rows.Count = 0 Then
                    LinkButton3.Visible = False
                End If


            Catch ex As Exception

            End Try


            

        End If
        Dim currentpage As String = Path.GetFileName(Page.Request.Path).ToString

        If currentpage = "hdMyComplaints.aspx" Then
            lbtnStep1.Attributes.Add("class", "")
            lbtnStep1.Attributes.Add("class", "cssStepBtnActive")
            LinkButton1.Attributes.Add("class", "cssStepBtnInActive")
            LinkButton2.Attributes.Add("class", "cssStepBtnInActive1")
            LinkButton3.Attributes.Add("class", "cssStepBtnInActive2")
            LinkButton4.Attributes.Add("class", "cssStepBtnInActive")
            LinkButton5.Attributes.Add("class", "cssStepBtnInActive")
        ElseIf currentpage = "hdAddIssues.aspx" Then
            LinkButton1.Attributes.Add("class", "")
            LinkButton1.Attributes.Add("class", "cssStepBtnActive")
            lbtnStep1.Attributes.Add("class", "cssStepBtnInActive")
            LinkButton2.Attributes.Add("class", "cssStepBtnInActive1")
            LinkButton3.Attributes.Add("class", "cssStepBtnInActive2")
            LinkButton4.Attributes.Add("class", "cssStepBtnInActive")
            LinkButton5.Attributes.Add("class", "cssStepBtnInActive")
        ElseIf currentpage = "hdAssignTask2Members.aspx" Then
            LinkButton2.Attributes.Add("class", "")
            LinkButton2.Attributes.Add("class", "cssStepBtnActive1")
            lbtnStep1.Attributes.Add("class", "cssStepBtnInActive")
            LinkButton1.Attributes.Add("class", "cssStepBtnInActive")
            LinkButton3.Attributes.Add("class", "cssStepBtnInActive2")
            LinkButton4.Attributes.Add("class", "cssStepBtnInActive")
            LinkButton5.Attributes.Add("class", "cssStepBtnInActive")
        ElseIf currentpage = "hdEnterTaskStatus.aspx" Then
            LinkButton3.Attributes.Add("class", "")
            LinkButton3.Attributes.Add("class", "cssStepBtnActive2")
            lbtnStep1.Attributes.Add("class", "cssStepBtnInActive")
            LinkButton2.Attributes.Add("class", "cssStepBtnInActive1")
            LinkButton1.Attributes.Add("class", "cssStepBtnInActive")
            LinkButton4.Attributes.Add("class", "cssStepBtnInActive")
            LinkButton5.Attributes.Add("class", "cssStepBtnInActive")
        ElseIf currentpage = "hdInbox.aspx" Then
            LinkButton4.Attributes.Add("class", "")
            LinkButton4.Attributes.Add("class", "cssStepBtnActive")
            lbtnStep1.Attributes.Add("class", "cssStepBtnInActive")
            LinkButton2.Attributes.Add("class", "cssStepBtnInActive1")
            LinkButton3.Attributes.Add("class", "cssStepBtnInActive2")
            LinkButton1.Attributes.Add("class", "cssStepBtnInActive")
            LinkButton5.Attributes.Add("class", "cssStepBtnInActive")
        ElseIf currentpage = "hdOutbox.aspx" Then
            LinkButton5.Attributes.Add("class", "")
            LinkButton5.Attributes.Add("class", "cssStepBtnActive")
            lbtnStep1.Attributes.Add("class", "cssStepBtnInActive")
            LinkButton2.Attributes.Add("class", "cssStepBtnInActive1")
            LinkButton3.Attributes.Add("class", "cssStepBtnInActive2")
            LinkButton4.Attributes.Add("class", "cssStepBtnInActive")
            LinkButton1.Attributes.Add("class", "cssStepBtnInActive")

        End If

    End Sub
    Protected Sub lnkhome_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkhome.Click
        Response.Redirect("~/Modulelogin.aspx")
    End Sub
End Class
