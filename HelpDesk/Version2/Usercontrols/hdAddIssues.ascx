﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdAddIssues.ascx.vb"
    Inherits="HelpDesk_Version2_Usercontrols_hdAddIssues" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../../UserControls/hdFileUpload.ascx" TagName="hdFileUpload" TagPrefix="uc1" %>
<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Add Issues
        </div>
        <div class="card-body">
            <div class="table-responsive">
 
                <table  width="100%" align="center">
                    <tr>
                        <td width="20%" align="left">
                          <span class="field-label">     Business&nbsp;Unit</span>
                        </td>
                      
                        <td width="30%" align="left">
                            <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    
                        <td width="20%" align="left">
                            <span class="field-label">   Reported&nbsp;By/
                            <br />
                            On&nbsp;behalf&nbsp;of </span>
                        </td>
                      
                        <td width="30%" align="left">
                            <asp:DropDownList ID="ddreportedby" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                          <span class="field-label">     Category</span>
                        </td>
                       
                        <td colspan="3">
                            <table width="100%">
                                <tr>
                                      
                                    <td align="left" width="25%" class="p-0">
                                        <asp:DropDownList ID="ddsubtab" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                                    </td>
                                    <td align="left" width="25%" class="p-0">
                                        <asp:DropDownList ID="ddtoplevelcat" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                                    </td>
                                    <td align="left" width="25%" class="p-0">
                                        <asp:DropDownList ID="ddJobCategory" runat="server">
                            </asp:DropDownList>
                                    </td>
                                    <!--we have moved this section  below  -->
                                    <td align="left" width="25%" class="p-0">
                                      <asp:DropDownList ID="ddmaintab" runat="server" Visible="false" AutoPostBack="True">
                            </asp:DropDownList>
                         </td>
                                </tr>
                            </table>
                            
                           
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <span class="field-label">       Subject</span>
                        </td>
                      
                        <td>
                            <asp:TextBox ID="txtTitle" runat="server" Width="446px"></asp:TextBox>
                        </td>
                    
                        <td>
                           <span class="field-label">    Priority</span>
                        </td>
                       
                        <td>
                            <asp:DropDownList ID="ddpriority" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <span class="field-label">    Required&nbsp;Date</span>
                        </td>
                       
                        <td>
                            <asp:TextBox ID="txtreqdate" runat="server"></asp:TextBox>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" />
                        </td>
                    </tr>
                   
                    <tr>
                        <td>
                            <span class="field-label">   Task Description</span>
                        </td>
                       
                        <td colspan="3" align="left">
                            <telerik:radeditor id="txtjobdes" runat="server" editmodes="Design" toolsfile="~/HelpDesk/xml/FullSetOfTools.xml"
                                width="600px"></telerik:radeditor>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           
                            <span class="field-label">   Attachments</span>
                        </td>
                        
                        <td colspan="3">
                           
                            <uc1:hdFileUpload ID="hdFileUpload1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                          
                            <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" ValidationGroup="Save"
                                Width="100px" />
                            <br />
                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
            
    <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
        TargetControlID="txtreqdate">
    </ajaxToolkit:CalendarExtender>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddreportedby"
        Display="None" ErrorMessage="Please Specify Reported Person" InitialValue="-1"
        SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTitle"
        Display="None" ErrorMessage="Please Enter Title" SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddJobCategory"
        Display="None" ErrorMessage="Please Select Task Category" InitialValue="0" SetFocusOnError="True"
        ValidationGroup="Save"></asp:RequiredFieldValidator>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtreqdate"
        Display="None" ErrorMessage="Please Enter Required Date" SetFocusOnError="True"
        ValidationGroup="Save"></asp:RequiredFieldValidator>
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtjobdes"
        Display="None" ErrorMessage="Please Enter Job Description" SetFocusOnError="True"
        ValidationGroup="Save"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="Save" />
    <asp:HiddenField ID="HiddenBsuid" runat="server" />
    <asp:HiddenField ID="HiddenUserId" runat="server" />
    <asp:HiddenField ID="Hiddensubtabids" runat="server" />
    <asp:HiddenField ID="Hiddentoplevelcatids" runat="server" />
    <asp:HiddenField ID="Hiddentaskcatids" runat="server" />
    <asp:HiddenField ID="HiddenType" runat="server" />
</div>
            </div>
    </div>
