﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdMyComplaints.aspx.vb" Inherits="HelpDesk_Version2_TabsPages_hdMyComplaints" %>
 
<%@ Register src="../../UserControls/hdMyComplaints.ascx" tagname="hdMyComplaints" tagprefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>My Complaints</title>
     <link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function ViewTaskDes(TaskListId) {

            // window.open('../../Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId, '', 'Height:768px;Width:1024px;scroll:auto;resizable:yes;'); return false;

            var url = "../../Pages/hdTaskDescriptionView.aspx?TaskListId=" + TaskListId;
            var oWnd = radopen(url, "pop_DEDL");

        }
        function History(task_list_id) {

           // window.showModalDialog('../../Pages/hdTaskHistory.aspx?task_list_id=' + task_list_id + '&Usertype=CLIENT', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

            var url = "../../Pages/hdTaskHistory.aspx?task_list_id=" + task_list_id + "&Usertype=CLIENT";
            var oWnd = radopen(url, "pop_DEDL");

        }
        function Followup(task_list_id, to_emp_id) {

          //  window.showModalDialog('../../Pages/hdFollowupClientOwner.aspx?Task_list_id=' + task_list_id + '&to_emp_id=' + to_emp_id + '&Usertype=OWNER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;
            var url = "../../Pages/hdFollowupClientOwner.aspx?Task_list_id=" + task_list_id + "&to_emp_id=" + to_emp_id + "&Usertype=OWNER";
            var oWnd = radopen(url, "pop_DEDL");
        }



        function ownerclosejob(TaskListId) {
          //  window.showModalDialog("../../Pages/hdClientTaskClosing.aspx?tasklistid=" + taslistit, "", "dialogWidth:700px; dialogHeight:600px; center:yes");
            var url = "../../Pages/hdClientTaskClosing.aspx?tasklistid=" + TaskListId;
            var oWnd = radopen(url, "pop_DEDL");
        }

        function OnClientCloseDEDL(oWnd, args) {
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

</script>


</head>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_DEDL" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseDEDL"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        
    </telerik:RadWindowManager>
<body>
    <form id="form1" runat="server">
    <div>
    
<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
    
  
        <uc1:hdMyComplaints ID="hdMyComplaints1" runat="server" />
    
    
    </div>
    </form>
</body>
</html>
