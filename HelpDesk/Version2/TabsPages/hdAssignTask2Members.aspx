﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdAssignTask2Members.aspx.vb" Inherits="HelpDesk_Version2_TabsPages_hdAssignTask2Members" %>

<%@ Register src="../../UserControls/hdTaskAssigning.ascx" tagname="hdTaskAssigning" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Assign Task to Members</title>
    
</head>
<body>
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <div>
    <script type="text/javascript">


        function Assign(task_list_id, task_category_id) {

            window.showModalDialog('../../Pages/hdTaskAssignEmp.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id, '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

        }
        function ReAssign(task_list_id, task_category_id) {

            window.showModalDialog('../../Pages/hdTaskReassign.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id + '&Usertype=OWNER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

        }

        function History(task_list_id, task_category_id) {

            window.showModalDialog('../../Pages/hdTaskHistory.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id + '&Usertype=OWNER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

        }


        function openimage(fname) {
            window.showModalDialog('hdImageView.aspx?filename=' + fname + '&Filecount=' + '1', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

        }

        function ViewTaskDes(TaskListId) {

            window.open('../../Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        }

        function ChartComp(TaskListid) {

            window.showModalDialog('../../FusionCharts/hdTaskProgressCompare.aspx?TaskListid=' + TaskListid, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        }

        function Followup(task_list_id, to_emp_id) {

            window.showModalDialog('../../Pages/hdFollowupClientOwner.aspx?Task_list_id=' + task_list_id + '&to_emp_id=' + to_emp_id + '&Usertype=OWNER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

        }

        function ViewTraceFlow(task_list_id) {
            window.showModalDialog('../../Pages/hdTraceFlow.aspx?task_list_id=' + task_list_id, '', 'dialogHeight:600px;dialogWidth:800px;scroll:yes;resizable:yes;'); return false;

        }


        function ownerclosejob(taslistit) {
            window.showModalDialog("../../Pages/hdOwnerTaskClosing.aspx?tasklistid=" + taslistit, "", "dialogWidth:700px; dialogHeight:600px; center:yes");
        }
        
</script>

        <uc1:hdTaskAssigning ID="hdTaskAssigning1" runat="server" />
    
    </div>
    </form>
</body>
</html>
