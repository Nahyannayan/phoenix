﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdEnterTaskStatus.aspx.vb" Inherits="HelpDesk_Version2_TabsPages_hdEnterTaskStatus" %>

<%@ Register src="../../UserControls/hdEnterTaskStatus.ascx" tagname="hdEnterTaskStatus" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Enter Task Status</title>
      <link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function handleError() {

            return true;
        }
        window.onerror = handleError;


        function EnterStatus(assignId) {

            window.showModalDialog('../../Pages/hdStatusUpdate.aspx?AssignedTaskid=' + assignId, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        }

        function ViewTask(assignId) {

            window.open('../../Pages/hdTaskAssignView.aspx?AssignedTaskid=' + assignId, '', 'Height:700px;Width:800px;scroll:auto;resizable:yes;'); return false;

        }

        function ViewTaskStatus(assignId) {

            window.showModalDialog('../../Pages/hdStatusView.aspx?AssignedTaskid=' + assignId, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        }
        function StatusChart(AssignedTaskid, emp_id) {

            window.showModalDialog('../../FusionCharts/hdUserTaskProgressChart.aspx?emp_id=' + emp_id + '&AssignedTaskid=' + AssignedTaskid, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        }

        function ReAssign(task_list_id, task_category_id) {

            window.showModalDialog('../../Pages/hdTaskReassign.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id + '&Usertype=MEMBER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

        }

        function History(task_list_id, task_category_id) {

            window.showModalDialog('../../Pages/hdTaskHistory.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id + '&Usertype=MEMBER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

        }

        function SendMail(TaskAssignid, FromEmpId, ToEmpid, Parent_Message_Id) {

            window.showModalDialog('../../Pages/hdEnterMessage.aspx?TaskAssignid=' + TaskAssignid + '&FromEmpId=' + FromEmpId + '&ToEmpid=' + ToEmpid + '&Parent_Message_Id=' + Parent_Message_Id, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;


        }
        
</script>


</head>
<body>
    <form id="form1" runat="server">
    <div>
      <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
 
        <uc1:hdEnterTaskStatus ID="hdEnterTaskStatus1" runat="server" />
    
    </div>
  
    </form>
</body>
</html>
