﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdInbox.aspx.vb" Inherits="HelpDesk_Version2_TabsPages_hdInbox" %>

<%@ Register src="../../UserControls/hdInbox.ascx" tagname="hdInbox" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Inbox</title>
      <link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />

     <script type="text/javascript">
         function SendMail(TaskAssignid, FromEmpId, ToEmpid, Parent_Message_Id) {

             window.showModalDialog('../../Pages/hdEnterMessage.aspx?TaskAssignid=' + TaskAssignid + '&FromEmpId=' + FromEmpId + '&ToEmpid=' + ToEmpid + '&Parent_Message_Id=' + Parent_Message_Id, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;


         }
         function ViewMessage(messageid, toemp_id) {

             window.open('../../Pages/hdViewMessage.aspx?messageid=' + messageid + '&toemp_id=' + toemp_id + '&Type=Inbox', '', 'Height:650px;Width:800px;scrollbars:auto;resizable:yes;'); return false;


         }
 </script>
 

</head>
<body>
    <form id="form1" runat="server">
    <div>
     
        <uc1:hdInbox ID="hdInbox1" runat="server" />
    
    </div>
    </form>
</body>
</html>
