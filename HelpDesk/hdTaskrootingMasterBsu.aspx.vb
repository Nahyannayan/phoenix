﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class HelpDesk_hdTaskrootingMasterBsu
    Inherits BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenEmp_id.Value = Request.QueryString("Emp_id").Trim()
            BindMainTab()

        End If

    End Sub

    Public Sub BindMainTab()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select MAIN_TAB_ID,MAIN_TAB_DESCRIPTION from  MAIN_TAB_MASTER WHERE ACTIVE='True'"
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddMainTab.DataSource = ds
        ddMainTab.DataTextField = "MAIN_TAB_DESCRIPTION"
        ddMainTab.DataValueField = "MAIN_TAB_ID"
        ddMainTab.DataBind()

        BindSubTabs()


    End Sub

    Public Sub BindSubTabs()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = "Select SUB_TAB_ID,SUB_TAB_DESCRIPTION from  SUB_TAB_MASTER where MAIN_TAB_ID='" & ddMainTab.SelectedValue & "' AND Active='True'"
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddSubTab.DataSource = ds
        ddSubTab.DataTextField = "SUB_TAB_DESCRIPTION"
        ddSubTab.DataValueField = "SUB_TAB_ID"
        ddSubTab.DataBind()
        Dim list1 As New ListItem
        list1.Text = "Please select"
        list1.Value = "-1"
        ddSubTab.Items.Insert(0, list1)
        BindSearchCategory()
        BindJobCategory()
    
    End Sub

    Public Sub BindSearchCategory()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = " Select A.TASK_CATEGORY_ID,CATEGORY_DES from TASK_CATEGORY_MASTER A " & _
                        " INNER JOIN TASK_CATEGORY B ON B.ID=A.CATEGORY_ID " & _
                        " where PARENT_TASK_CATEGORY_ID=0 AND SUB_TAB_ID='" & ddSubTab.SelectedValue & "' AND A.TASK_CATEGORY_ID IN (SELECT DISTINCT TASK_CATEGORY_ID FROM dbo.TASK_ROOTING_MASTER WHERE EMP_ID='" & HiddenEmp_id.Value & "')"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        ddsearchcategory.DataSource = ds
        ddsearchcategory.DataTextField = "CATEGORY_DES"
        ddsearchcategory.DataValueField = "TASK_CATEGORY_ID"
        ddsearchcategory.DataBind()
        Dim list As New ListItem
        list.Text = "Search Category"
        list.Value = "-1"
        ddsearchcategory.Items.Insert(0, list)

    End Sub
    Public Sub BindJobCategory()

        lblmessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = " Select A.TASK_CATEGORY_ID,CATEGORY_DES,CASE WHEN ISNULL(C.TASK_CATEGORY_ID,'')='' THEN '~/Images/cross.png' else '~/Images/tick.gif' END Involved from TASK_CATEGORY_MASTER A " & _
                        " INNER JOIN TASK_CATEGORY B ON B.ID=A.CATEGORY_ID inner join ( " & _
                        " SELECT DISTINCT TASK_CATEGORY_ID FROM dbo.TASK_ROOTING_MASTER " & _
                        " WHERE MAIN_TAB_ID='" & ddMainTab.SelectedValue & "' AND  SUB_TAB_ID='" & ddSubTab.SelectedValue & "' AND EMP_ID='" & HiddenEmp_id.Value & "' " & _
                        " ) C on C.TASK_CATEGORY_ID=A.TASK_CATEGORY_ID " & _
                        " where PARENT_TASK_CATEGORY_ID=0 AND SUB_TAB_ID='" & ddSubTab.SelectedValue & "' "

        If ddsearchcategory.SelectedValue <> "-1" Then
            Sql_Query &= " AND A.TASK_CATEGORY_ID='" & ddsearchcategory.SelectedValue & "'"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        GridInfo.DataSource = ds
        GridInfo.DataBind()


        For Each row As GridViewRow In GridInfo.Rows

            Dim grid As GridView = DirectCast(row.FindControl("GridRootingBsu"), GridView)
            Dim catyid = DirectCast(row.FindControl("Hiddencatyid"), HiddenField).Value
            BindSettings(grid, catyid)

        Next


    End Sub


    Public Sub BindSettings(ByVal Grid As GridView, ByVal category As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()

        Dim Sql_Query = " SELECT BSU_ID,BSU_NAME,ISNULL(TASK_CATEGORY_OWNER,'False')TASK_CATEGORY_OWNER,ISNULL(TASK_MEMBER,'False')TASK_MEMBER,ISNULL(EMAIL_NOTIFY,'False')EMAIL_NOTIFY,ISNULL(SMS_NOTIFY,'False')SMS_NOTIFY,ISNULL(CAN_RESIGN,'False')CAN_RESIGN ,'javascript:ViewAssigned(''' + convert(varchar," & ddMainTab.SelectedValue & ") + ''',''' +  convert(varchar," & ddSubTab.SelectedValue & ") + ''',''' + BSU_ID + ''',''' + convert(varchar," & category & ") + ''' );return false;' AS VIEW_ASSIGNED FROM  OASIS.dbo.BUSINESSUNIT_M A " & _
                        " inner JOIN dbo.TASK_ROOTING_MASTER B ON A.BSU_ID=B.EMP_TASK_BSU_ID AND B.EMP_ID='" & HiddenEmp_id.Value & "' " & _
                        " AND MAIN_TAB_ID='" & ddMainTab.SelectedValue & "' AND SUB_TAB_ID='" & ddSubTab.SelectedValue & "' AND TASK_CATEGORY_ID='" & category & "' ORDER BY BSU_NAME"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Grid.DataSource = ds
                Grid.DataBind()
                GridInfo.Visible = True
            Else
                GridInfo.Visible = False
                lblmessage.Text = "sorry, Staff is not linked to this category."
                lblmessage.Style.Add("text-align", "center")
            End If
        End If



    End Sub

    Protected Sub ddSubTab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddSubTab.SelectedIndexChanged
        BindJobCategory()
        BindSearchCategory()

    End Sub

    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging
        GridInfo.PageIndex = e.NewPageIndex
        BindJobCategory()
    End Sub

    Public Sub SaveGrid(ByVal sender As Object, ByVal e As System.EventArgs)

        For Each row As GridViewRow In GridInfo.Rows

            Dim grid As GridView = DirectCast(row.FindControl("GridRootingBsu"), GridView)
            Dim Task_Category_ID As String = DirectCast(row.FindControl("Hiddencatyid"), HiddenField).Value

            savedata(grid, Task_Category_ID)

        Next
        BindJobCategory()
        lblmessage.Text = "Successfully updated"

    End Sub

    Public Sub savedata(ByVal grid As GridView, ByVal Task_Category_ID As String)
        Try

            Dim Main_Tab = ddMainTab.SelectedValue
            Dim Sub_Tab = ddSubTab.SelectedValue

            For Each row As GridViewRow In grid.Rows

                Dim deleteflag = 0 '1-DELETE 0- NO DELETE


                Dim taskbsuid = DirectCast(row.FindControl("HiddenBsuid"), HiddenField).Value

                Dim CheckOwner As New CheckBox
                CheckOwner = DirectCast(row.FindControl("CheckOwner"), CheckBox)
                Dim CheckMember As New CheckBox
                CheckMember = DirectCast(row.FindControl("CheckMember"), CheckBox)
                Dim CheckEmail As New CheckBox
                CheckEmail = DirectCast(row.FindControl("CheckEmail"), CheckBox)
                Dim CheckSMS As New CheckBox
                CheckSMS = DirectCast(row.FindControl("CheckSMS"), CheckBox)
                Dim reassign As New CheckBox
                reassign = DirectCast(row.FindControl("CheckReassign"), CheckBox)


                If CheckOwner.Checked Or CheckMember.Checked Or CheckEmail.Checked Or CheckSMS.Checked Or reassign.Checked Then
                    deleteflag = 0 ''No Delete
                Else
                    deleteflag = 1 ''Delete
                End If


                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
                Dim pParms(11) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@MAIN_TAB_ID", Main_Tab)
                pParms(1) = New SqlClient.SqlParameter("@SUB_TAB_ID", Sub_Tab)
                pParms(2) = New SqlClient.SqlParameter("@TASK_CATEGORY_ID", Task_Category_ID)
                pParms(3) = New SqlClient.SqlParameter("@TASK_CATEGORY_OWNER", CheckOwner.Checked)
                pParms(4) = New SqlClient.SqlParameter("@EMP_ID", HiddenEmp_id.Value)
                pParms(5) = New SqlClient.SqlParameter("@TASK_MEMBER", CheckMember.Checked)
                pParms(6) = New SqlClient.SqlParameter("@EMAIL_NOTIFY", CheckEmail.Checked)
                pParms(7) = New SqlClient.SqlParameter("@SMS_NOTIFY", CheckSMS.Checked)
                pParms(8) = New SqlClient.SqlParameter("@DELETE_FLAG", deleteflag)
                pParms(9) = New SqlClient.SqlParameter("@EMP_TASK_BSU_ID", taskbsuid)
                pParms(10) = New SqlClient.SqlParameter("@CAN_RESIGN", reassign.Checked)

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_UPDATE_DELETE_TASK_ROOTING_MASTER", pParms)

            Next



        Catch ex As Exception
            lblmessage.Text = "Error : " & ex.Message
        End Try

    End Sub

    Protected Sub ddsearchcategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsearchcategory.SelectedIndexChanged
        BindJobCategory()
    End Sub
End Class
