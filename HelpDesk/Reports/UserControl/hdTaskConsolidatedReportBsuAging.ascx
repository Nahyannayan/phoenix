﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskConsolidatedReportBsuAging.ascx.vb" Inherits="HelpDesk_Reports_UserControl_hdTaskConsolidatedReportBsuAging" %>

<script type="text/javascript">

    function openW(a, b, c) {
        var bsu = c;
        var fdate = document.getElementById("<%=txtFromdate.ClientID %>").value;
        var tdate = document.getElementById("<%=txtToDate.ClientID %>").value;
        var val = "?fdays=" + a + "&tdays=" + b + "&clmbsuid=" + bsu + "&fromdate=" + fdate + "&todate=" + tdate;

        window.open("hdTaskConsolidatedReportBsuAgingView.aspx" + val, "", "dialogWidth:1024px; dialogHeight:800px; center:yes");

    }


</script> 
<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-phone-square mr-3"></i>
            Summary Report Member Pending Issues-Ageing  
        </div>

 <div class="card-body">
            <div class="table-responsive m-auto">

<table  width="100%" align="center">
                <tr>

                <td  width = "20%" align="left" >
                             <span class="field-label">
                            From Date</span>
                        </td>
                    <td>
                        <asp:TextBox ID="txtFromdate" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtFromdate"
                            TargetControlID="txtFromdate">
                        </ajaxToolkit:CalendarExtender>
                    </td>
                        <td  width = "20%" align="left">
                           <span class="field-label"> To Date</span>
                        </td>
                        
                        <td  width = "30%">
                        
                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtToDate"
                            TargetControlID="txtToDate">
                        </ajaxToolkit:CalendarExtender>
                    </td>

                </tr>



                 <tr>
                    <td colspan="4" align="center">
                        <asp:Button ID="btnview" runat="server" CssClass="button" Width="100px" Text="View" />
                            <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Export" 
                                Width="92px" />            
                    </td>
                </tr>



    
    <tr>
        <td align="centet" colspan="4">
            <asp:GridView ID="GridInfo" runat="server" AutoGenerateColumns="false" 
                EmptyDataText="Information not available." Width="100%" CssClass="table table-bordered table-row" >
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        <asp:LinkButton ID="Linkbsu" CommandName="1"  CommandArgument="0" runat="server">BSU</asp:LinkButton>  
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                  <asp:LinkButton ID="LK0" CommandName="2" CommandArgument='<%# Eval("CLAME_BSU_ID")%>' runat="server"><%# Eval("BSU_SHORTNAME")%></asp:LinkButton>  
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        <asp:LinkButton ID="lnk11" CommandName="1"  CommandArgument="1" runat="server"> 7 Days</asp:LinkButton>   
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LK1" runat="server" OnClientClick='<%# Eval("T11")%>'><%# Eval("SEVENDAYS")%></asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                       <asp:LinkButton ID="lnk12" CommandName="1"  CommandArgument="2" runat="server">  14 Days</asp:LinkButton>   
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LK2" runat="server" OnClientClick='<%# Eval("T12")%>'><%# Eval("FORTEENDAYS")%></asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                     <asp:LinkButton ID="lnk13" CommandName="1"  CommandArgument="3" runat="server"> 21 Days</asp:LinkButton>   
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LK3" runat="server" OnClientClick='<%# Eval("T13")%>'><%# Eval("TWENTYONEDAYS")%></asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                      <asp:LinkButton ID="lnk14" CommandName="1"  CommandArgument="4" runat="server">  30 Days </asp:LinkButton>    
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LK4" runat="server" OnClientClick='<%# Eval("T14")%>'><%# Eval("THIRYDAYS")%></asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                       <asp:LinkButton ID="lnk15" CommandName="1"  CommandArgument="5" runat="server">  45 Days </asp:LinkButton>  
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LK5" runat="server" OnClientClick='<%# Eval("T15")%>'><%# Eval("FOURTYFIVEDAYS")%></asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                     <asp:LinkButton ID="lnk16" CommandName="1"  CommandArgument="6" runat="server">  > 45 Days </asp:LinkButton>   
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LK6" runat="server" OnClientClick='<%# Eval("T16")%>'><%# Eval("EXEEDFOURTYFIVEDAYS")%></asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                <EditRowStyle Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
            </asp:GridView>
            
        </td>
    </tr>
</table>

</div></div>
<p>
                            </p>


