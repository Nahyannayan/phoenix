﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdTaskConsolidatedReportBsu.ascx.vb"
    Inherits="HelpDesk_Reports_UserControl_hdTaskConsolidatedReportBsu" %>

<script type="text/javascript">

    function openW(a, b, c) {
        //alert(a)
        //alert(b)
        var bsu = c;
        var fdate = document.getElementById("<%=txtFromdate.ClientID %>").value;
        var tdate = document.getElementById("<%=txtToDate.ClientID %>").value;

        var val = "?fromdate=" + fdate + "&todate=" + tdate + "&clmbsuid=" + bsu + "&callbsuid=" + b + "&option=" + a;

        window.open("hdTaskConsolidatedReportBsuView.aspx" + val, "", "dialogWidth:1024px; dialogHeight:800px; center:yes");

    }
    </script> 


<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-phone-square mr-3"></i>
            Task Summary Report
        </div>

 <div class="card-body">
            <div class="table-responsive m-auto">

<table  width="100%" align="center">
    <tr>
        <td  width = "20%" align="left" >
                             <span class="field-label">
                            From Date</span>
                        </td>
                    <td width = "30%" align="left">
                        <asp:TextBox ID="txtFromdate" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtFromdate"
                            TargetControlID="txtFromdate">
                        </ajaxToolkit:CalendarExtender>
                    </td>
                        <td  width = "20%" align="left">
                           <span class="field-label"> To Date</span>
                        </td>
                        
                        <td  width = "30%">
                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtToDate"
                            TargetControlID="txtToDate">
                        </ajaxToolkit:CalendarExtender>
                    </td>

                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <asp:Button ID="btnview" runat="server" CssClass="button" Width="100px" Text="View" />
                                    <asp:Button ID="btnexport" runat="server" Width="100px" CssClass="button" 
                                         Text="Export" />
                                    
                    </td>
                </tr>
            
</table>
                

<table width="100%" align="center">
    <tr>
        <td class= "table-bg" colspan="4">
            Summary Report</td>
    </tr>
    <tr>
        <td align="center" colspan="4">
            <asp:GridView ID="GridInfo0" runat="server" AutoGenerateColumns="false"  CssClass="table table-bordered table-row" EmptyDataText="Information not available."
                Width="100%">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        <asp:LinkButton ID="lnkbsu" CommandName="1" CommandArgument="1" runat="server">BSU</asp:LinkButton> 
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                               
                                 <asp:LinkButton ID="LK1" CommandName="view" CommandArgument='<%# Eval("CLAME_BSU_ID")%>' runat="server"><%# Eval("BSU_SHORTNAME")%></asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                         <asp:LinkButton ID="lnk11" CommandName="1" CommandArgument="11" runat="server">Total Issue</asp:LinkButton>  
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("TOTAL_ISSUE")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                              <asp:LinkButton ID="lnk12" CommandName="1" CommandArgument="12" runat="server">Assigned</asp:LinkButton>  
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                              <%# Eval("ASSIGNED_COUNT")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                       <asp:LinkButton ID="lnk13" CommandName="1" CommandArgument="13" runat="server">Member Completed</asp:LinkButton>  
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                               <%# Eval("MEMBER_COMPLETED_COUNT")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        <asp:LinkButton ID="lnk14" CommandName="1" CommandArgument="14" runat="server">Member Pending</asp:LinkButton>   
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("MEMBER_PENDING_COUNT")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                     <asp:LinkButton ID="lnk15" CommandName="1" CommandArgument="15" runat="server">Member Pending Future Dates</asp:LinkButton>    
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("MEMBER_PENDING_FUTURE_DATES")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                <EditRowStyle Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
</table>

<asp:HiddenField ID="Hiddenclaimid" runat="server" />

<asp:Label ID="Label1" runat="server"></asp:Label><br />
<asp:Panel ID="PanelStatusupdate" runat="server" BackColor="white" CssClass="modalPopup" Height="500" ScrollBars="Auto" width="700" Style="display: none">
   <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
    <tr>
        <td class="subheader_img">
            Summary Report-BSU</td>
    </tr>
    <tr>
        <td align="left">
            <asp:GridView ID="GridInfo" runat="server" AutoGenerateColumns="false" EmptyDataText="Information not available."
                Width="100%">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        <asp:LinkButton ID="lnkbsu" CommandName="1" CommandArgument="20" runat="server">BSU</asp:LinkButton>   
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="lnk100" CommandName="100"  CommandArgument='<%#Eval("CALLER_BSU_ID")%>' runat="server"><%#Eval("BSU_SHORTNAME")%></asp:LinkButton>  

                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                         <asp:LinkButton ID="lnk11" CommandName="1" CommandArgument="11" runat="server">Total Issue</asp:LinkButton>    
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LK1" OnClientClick='<%# Eval("T11")%>' runat="server"><%# Eval("TOTAL_ISSUE")%></asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        <asp:LinkButton ID="lnk12" CommandName="1" CommandArgument="12" runat="server"> Assigned</asp:LinkButton>    
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LK2" OnClientClick='<%# Eval("T12")%>' runat="server"><%# Eval("ASSIGNED_COUNT")%></asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                      <asp:LinkButton ID="lnk13" CommandName="1" CommandArgument="13" runat="server"> Member Completed</asp:LinkButton>    
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LK3" OnClientClick='<%# Eval("T13")%>' runat="server"><%# Eval("MEMBER_COMPLETED_COUNT")%></asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                       <asp:LinkButton ID="lnk14" CommandName="1" CommandArgument="14" runat="server">  Member Pending</asp:LinkButton>   
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LK4" OnClientClick='<%# Eval("T14")%>' runat="server"><%# Eval("MEMBER_PENDING_COUNT")%></asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        <asp:LinkButton ID="lnk15" CommandName="1" CommandArgument="15" runat="server"> Member Pending Future Dates</asp:LinkButton>   
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:LinkButton ID="LK5" OnClientClick='<%# Eval("T15")%>' runat="server"><%# Eval("MEMBER_PENDING_FUTURE_DATES")%></asp:LinkButton>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                <EditRowStyle Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
            </asp:GridView>
            <center>
            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Close" />
            <asp:Button ID="btnexport0" runat="server" Width="100px" CssClass="button" 
                                         Text="Export" />
            </center>
        </td>
    </tr>
</table>

</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
    DropShadow="true" PopupControlID="PanelStatusupdate"
    RepositionMode="RepositionOnWindowResizeAndScroll" CancelControlID="btncancel"  TargetControlID="Label1">
</ajaxToolkit:ModalPopupExtender>
<asp:HiddenField ID="Hiddenclaimbsuid" runat="server" />

                </div></div></div>
