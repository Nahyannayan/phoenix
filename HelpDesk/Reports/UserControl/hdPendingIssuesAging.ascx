﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="hdPendingIssuesAging.ascx.vb"
    Inherits="HelpDesk_UserControls_hdPendingIssuesAging" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Charting" tagprefix="telerik" %>

<%--<script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>--%>
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/cssfiles/BSUstyles.css" rel="stylesheet" />
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">


<style type="text/css">

.gridheader_new 
{
	border-style: none;
        border-color: inherit;
        border-width: 0;
        font-family: Verdana, Arial, Helvetica, sans-serif; 
	background-image:url('../../Images/GRIDHEAD.gif') ;
	    background-repeat: repeat-x;font-size: 11px;
	    font-weight:bold;color:#1b80b6;
}
.inputbox 
{
	BACKGROUND: F5FED2; 
	BORDER-BOTTOM: #1B80B6 1px solid; 
	BORDER-LEFT: #1B80B6 1px solid; 
	BORDER-RIGHT: #1B80B6 1px solid; 
	BORDER-TOP: #1B80B6 1px solid; 
	COLOR: #555555; CURSOR: text; 
	FONT-FAMILY: verdana; 
	FONT-SIZE: 11px;
	WIDTH: 199px; 
	HEIGHT: 14px; 
	TEXT-DECORATION: none
}
    .button
    {}
</style>

<script language="javascript" type="text/javascript">

 




  

    function GetEMPNAME(){
        var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 445px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;

                result = window.showModalDialog("../Payroll/Reports/Aspx/SelIDDESC.aspx?ID=EMP", "", sFeatures)

        if (result != '' && result != undefined) {
            document.getElementById('<%=h_EMPID.ClientID %>').value = result; //NameandCode[0];
            document.forms[0].submit();
        }
        else {
            return false;
        }

    }

    function OnTreeClick(evt) {
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
        if (isChkBoxClick) {
            var parentTable = GetParentByTagName("table", src);
            var nxtSibling = parentTable.nextSibling;
            if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
            {
                if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                {
                    //check or uncheck children at all levels
                    CheckUncheckChildren(parentTable.nextSibling, src.checked);
                }
            }
            //check or uncheck parents at all levels
            CheckUncheckParents(src, src.checked);
        }
    }

    function CheckUncheckChildren(childContainer, check) {
        var childChkBoxes = childContainer.getElementsByTagName("input");
        var childChkBoxCount = childChkBoxes.length;
        for (var i = 0; i < childChkBoxCount; i++) {
            childChkBoxes[i].checked = check;
        }
    }

    function CheckUncheckParents(srcChild, check) {
        var parentDiv = GetParentByTagName("div", srcChild);
        var parentNodeTable = parentDiv.previousSibling;

        if (parentNodeTable) {
            var checkUncheckSwitch;

            if (check) //checkbox checked
            {
                var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                if (isAllSiblingsChecked)
                    checkUncheckSwitch = true;
                else
                    return; //do not need to check parent if any child is not checked
            }
            else //checkbox unchecked
            {
                checkUncheckSwitch = false;
            }

            var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
            if (inpElemsInParentTable.length > 0) {
                var parentNodeChkBox = inpElemsInParentTable[0];
                parentNodeChkBox.checked = checkUncheckSwitch;
                //do the same recursively
                CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
            }
        }
    }

    function AreAllSiblingsChecked(chkBox) {
        var parentDiv = GetParentByTagName("div", chkBox);
        var childCount = parentDiv.childNodes.length;
        for (var i = 0; i < childCount; i++) {
            if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
            {
                if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                    var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                    //if any of sibling nodes are not checked, return false
                    if (!prevChkBox.checked) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    //utility function to get the container of an element by tagname
    function GetParentByTagName(parentTagName, childElementObj) {
        var parent = childElementObj.parentNode;
        while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
            parent = parent.parentNode;
        }
        return parent;
    }


    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("Checkbsu") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }


</script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-phone-square mr-3"></i>
            Pending Issues Aging - Dashboard
        </div>
 <div class="card-body">
            <div class="table-responsive m-auto">
    
    <table width="100%" align="center">
    
    <%--<tr>
        <td align="left">--%>
          
          <table style="width: 100%">
    <tr>
        <td width = "20%" align="left"><span class="field-label">
            Business Unit </span></td>
        
        <td width = "30%" align="left"> 
            <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>

        <td width = "20%" align="left">

        </td>
        <td width = "30%" align="left">

        </td>
    </tr>
    <tr id ="RowEmpName" runat = "server" visible ="false">
        <td width = "20%" align="left"><span class="field-label">
            Employee Name </span>
        </td>
        
        <td width = "30%" align="left">
            <asp:DropDownList ID="ddemp" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>

        <td width = "20%" align="left">

        </td>
        <td width = "30%" align="left">

        </td>
    </tr>
    <tr>
        <td width = "20%" align="left"><span class="field-label">
            Main Tab </span> </td>
        
        <td width = "30%" align="left">
<asp:DropDownList ID="ddMaintab" runat="server" AutoPostBack="True">
</asp:DropDownList>
        </td>
        
        <td width = "20%" align="left"><span class="field-label">
            Departments</span>  </td>
        
        <td width = "30%" align="left">
<asp:DropDownList ID="ddsubtab" runat="server" AutoPostBack="True">
</asp:DropDownList>
        </td>
    </tr>
    
    <tr>
        <td width = "20%" align="left"><span class="field-label">
            Categories </span> 
        </td>
        
        <td width = "30%" align="left">
            <div class="checkbox-list">
                            <asp:TreeView ID="TreeItemCategory" runat="server" BorderColor="#404040" 
                                onclick="OnTreeClick(event);"  BorderStyle="Solid"
                                BorderWidth="1px" ImageSet="Msdn"  ShowCheckBoxes="All" ShowLines="True" 
                                ExpandDepth="0" >
                                <ParentNodeStyle Font-Bold="False" />
                                <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                                <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px"
                                    VerticalPadding="0px" />
                                <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="5px"
                                    NodeSpacing="0px" VerticalPadding="0px" />
                            </asp:TreeView>
                </div>
        </td>
                <td width = "20%" align="left">

        </td>
        <td width = "30%" align="left">

        </td>

    </tr>
    <tr id ="RowBusinessUnit" runat ="server" visible="false">
        <td width = "20%" align="left"><span class="field-label">
            Business Unit (Access) </span>
        </td>
        
        <td valign="top">
            <div class="checkbox-list">
            <asp:GridView ID="GridRootingBsu" runat="server" AutoGenerateColumns="false" 
                Width="100%">
                <Columns>
                    <asp:TemplateField HeaderText="Owner">
                        <HeaderTemplate>
                            <center>
                        
                                <asp:CheckBox ID="chkAll1" runat="server" onclick="javascript:change_chk_stateg(this);"
                                    ToolTip="Click here to select/deselect all rows" />
                            </center>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:CheckBox ID="Checkbsu" runat="server"  />
                            </center>
                        </ItemTemplate>
                        <ItemStyle Width="60px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Business Unit">
                        <HeaderTemplate>
                            <span style="font-size: small">Business Unit</span>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span style="font-size: small">
                                <%#Eval("BSU_NAME")%></span>
                            <asp:HiddenField ID="HiddenBsuid" runat="server" Value='<%#Eval("BSU_ID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="gridheader_pop" Font-Size="Small" Height="20px" Wrap="False" />
                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <EditRowStyle Wrap="False" />
            </asp:GridView>
                </div>
        </td>
                <td width = "20%" align="left">

        </td>
        <td width = "30%" align="left">

        </td>

    </tr>
    
    
    
    <tr  >
        
        <td colspan ="4" width="100%">
            <asp:RadioButton ID="optOption1" runat="server" Text="Option 1" 
                GroupName="ReportOptions" Checked="True" AutoPostBack="True" />
            &nbsp;(Business Units &amp; Task Escalations)<asp:CheckBoxList ID="CheckAccess" RepeatDirection="Horizontal" runat="server" 
                Visible="False">
                <asp:ListItem Text="Owner" Value="O"></asp:ListItem>
                <asp:ListItem Text="Member" Value="M"></asp:ListItem>
                <asp:ListItem Text="Email (Owner)" Value="E"></asp:ListItem>
                <asp:ListItem Text="SMS (Owner)" Value="S"></asp:ListItem>
                <asp:ListItem Text="Reassign" Value="R"></asp:ListItem>
            </asp:CheckBoxList>
        </td>
    </tr>
    
    
    
    <tr  >
        <td colspan ="4" width="100%">
            
            <asp:RadioButton ID="optOption2" runat="server" Text="Option 2" 
                GroupName="ReportOptions" AutoPostBack="True" />
                    
        (Business Units, Task Categories, Task Escalation Days &amp; Task Escalations)</td>
    </tr>
    
    
    
    <tr  >
        
        <td colspan ="4" width="100%">
            <asp:RadioButton ID="optOption3" runat="server" Text="Option 3" 
                GroupName="ReportOptions" AutoPostBack="True" />
                    
        (Graphical)</td>
    </tr>
    <tr>
        <td colspan="4" width="100%">
            <asp:Button ID="btnDashboard" runat="server" CssClass="button" 
                Text="Dashboard" Width="122px" />
                    
        
            <asp:Button ID="btnPrint" runat="server" CssClass="button" 
                Text="Print" Width="122px" />
                    
        </td>
    </tr>
    <tr id="rowDashboardGrid" runat="server" visible="false"  >
        <td colspan="4" width="100%">
            
        
            
            <asp:GridView ID="gvDashboard1" runat="server" AutoGenerateColumns="False" EnableTheming="False"
                Width="100%" EnableModelValidation="True"  CssClass="table table-bordered table-row">
                
                <Columns>
                    <asp:BoundField DataField="BSU_NAME" HeaderText="Business Unit">
                    <ItemStyle Width="20%" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Category_Des" HeaderText="Task Category">
                    <ItemStyle Width="15%" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LEVEL0" HeaderText="Level 0">
                    <ItemStyle ForeColor="Black" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LEVEL1" HeaderText="Level 1">
                    <ItemStyle BackColor="Yellow" ForeColor="Black" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Level_1_Days" HeaderText="Level 1 Days">
                    <ItemStyle BackColor="Yellow" ForeColor="Black" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LEVEL2" HeaderText="Level 2">
                    <ItemStyle BackColor="Orange" ForeColor="Black" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Level_2_Days" HeaderText="Level 2 Days">
                    <ItemStyle BackColor="Orange" ForeColor="Black" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LEVEL3" HeaderText="Level 3">
                    <ItemStyle BackColor="Red" ForeColor="Black" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Level_3_Days" HeaderText="Level 3 Days">
                    <ItemStyle BackColor="Red" ForeColor="Black" HorizontalAlign="Center" />
                    </asp:BoundField>
                </Columns>
                
            </asp:GridView>
        </td>
    </tr>
    <tr>
        
        <td align="left" colspan="4">
                    
<asp:Label ID="lblmessage" runat="server" cssclass="error"></asp:Label>
                    
        </td>
    </tr>
</table>
          
          <%--</td>
    </tr>--%>
</table>

                </div></div>


    <asp:HiddenField ID="Hiddensubtabids" runat="server" />
    <asp:HiddenField ID="Hiddentoplevelcatids" runat="server" />
    <asp:HiddenField ID="Hiddentaskcatids" runat="server" />
    <asp:HiddenField ID="HiddenType" runat="server" />



    <asp:HiddenField ID="h_EMPID" runat="server" />



  </div>
    <asp:HiddenField ID="h_BSUID" runat="server" />
    