﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class HelpDesk_Reports_UserControl_hdTaskConsolidatedReportBsuAging
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnexport)
        If GridInfo.Rows.Count > 0 Then
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("Linkbsu"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk11"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk12"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk13"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk14"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk15"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk16"), LinkButton))
        End If
        For Each row As GridViewRow In GridInfo.Rows
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(row.FindControl("LK0"), LinkButton))
        Next
    End Sub


    Public Sub BindGrid(Optional ByVal export As Boolean = False)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
        If Library.LibrarySuperAcess(Session("EmployeeId")) Then
        Else
            pParms(1) = New SqlClient.SqlParameter("@LOGIN_EMP_ID", Session("EmployeeId"))
        End If
        pParms(2) = New SqlClient.SqlParameter("@FROMDATE", txtFromdate.Text.Trim())
        pParms(3) = New SqlClient.SqlParameter("@TODATE", txtToDate.Text.Trim())

        Dim ds As New DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "REPORT_TASK_CONSOLIDATED_BSU_AGING", pParms)

        GridInfo.DataSource = ds
        GridInfo.DataBind()

        If GridInfo.Rows.Count > 0 Then
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("Linkbsu"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk11"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk12"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk13"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk14"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk15"), LinkButton))
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GridInfo.HeaderRow.FindControl("lnk16"), LinkButton))
        End If
        For Each row As GridViewRow In GridInfo.Rows
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(row.FindControl("LK0"), LinkButton))
        Next
        If export Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dt As New DataTable
                dt = ds.Tables(0)
                dt.Columns.Remove("T11")
                dt.Columns.Remove("T12")
                dt.Columns.Remove("T13")
                dt.Columns.Remove("T14")
                dt.Columns.Remove("T15")
                dt.Columns.Remove("T16")

                ExportExcel(dt)
            End If
        End If
    End Sub

    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        BindGrid(True)
    End Sub

    Public Sub ExportExcel(ByVal dt As DataTable)

        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        ' ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "Data.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

    End Sub

    Protected Sub GridInfo_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridInfo.RowCommand

        If e.CommandName = "1" Then

            If e.CommandArgument = "0" Then
                ShowTask("", "", "")
            End If
            If e.CommandArgument = "1" Then
                ShowTask("-1", "7", "")
            End If
            If e.CommandArgument = "2" Then
                ShowTask("7", "14", "")
            End If

            If e.CommandArgument = "3" Then
                ShowTask("14", "21", "")
            End If

            If e.CommandArgument = "4" Then
                ShowTask("21", "30", "")
            End If

            If e.CommandArgument = "5" Then
                ShowTask("30", "45", "")
            End If

            If e.CommandArgument = "6" Then
                ShowTask("45", "10000", "")
            End If

        End If
        If e.CommandName = "2" Then
            ShowTask("", "", e.CommandArgument)
        End If

    End Sub

    Public Sub ShowTask(ByVal fdays As String, ByVal tdays As String, ByVal claimbsuid As String)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(8) As SqlClient.SqlParameter

        If fdays <> "" Then
            pParms(0) = New SqlClient.SqlParameter("@FROM_DAYS", fdays)
        End If

        If tdays <> "" Then
            pParms(1) = New SqlClient.SqlParameter("@TO_DAYS", tdays)
        End If

        If claimbsuid <> "" Then
            pParms(2) = New SqlClient.SqlParameter("@CLAME_BSU_ID", claimbsuid)
        End If

        pParms(4) = New SqlClient.SqlParameter("@OPTION", 2)

        If Library.LibrarySuperAcess(Session("EmployeeId")) Then
        Else
            pParms(5) = New SqlClient.SqlParameter("@LOGIN_EMP_ID", Session("EmployeeId"))
        End If

        pParms(6) = New SqlClient.SqlParameter("@FROMDATE", txtFromdate.Text.Trim())
        pParms(7) = New SqlClient.SqlParameter("@TODATE", txtToDate.Text.Trim())

        Dim ds As New DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "REPORT_TASK_CONSOLIDATED_BSU_AGING", pParms)


        If ds.Tables(0).Rows.Count > 0 Then

            Dim dt As New DataTable
            dt = ds.Tables(0)
            dt.Columns.Remove("VIEW_TRACE_FLOW")
            dt.Columns.Remove("TASK_ID")
            dt.Columns.Remove("VIEW_TASK")
            ExportExcel(dt)

        End If

    End Sub

    Protected Sub btnview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnview.Click
        BindGrid()
    End Sub
End Class
