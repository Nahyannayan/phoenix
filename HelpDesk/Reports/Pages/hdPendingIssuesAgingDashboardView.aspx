﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdPendingIssuesAgingDashboardView.aspx.vb"
    Inherits="HelpDesk_Reports_Pages_hdPendingIssuesAgingDashboardView" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/cssfiles/BSUstyles.css" rel="stylesheet" />
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

<head runat="server">
    <title></title>
    <base  target="_self" /> 
    <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function ViewTraceFlow(task_list_id) {
            window.showModalDialog('../../Pages/hdTraceFlow.aspx?task_list_id=' + task_list_id, '', 'dialogHeight:600px;dialogWidth:800px;scroll:yes;resizable:yes;'); return false;

        }

        function ViewTaskDes(TaskListId) {

            window.open('../../Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-phone-square mr-3"></i>
            Pending Issues Aging - Dashboard
        </div>
 <div class="card-body">
            <div class="table-responsive m-auto">
    
    





        <table  width="100%" align="center" >
            <tr>
                <td class="title-bg">
                    <asp:Label ID="lblTitle" runat="server">Business Units Task Escalations</asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblBsu" runat="server">Selected Business Unit(s): All</asp:Label>
                &nbsp;</td>
            </tr>
            <tr>
                <td align="center">
            <asp:GridView ID="gvDashboard1" runat="server" AutoGenerateColumns="False" 
                Width="100%" CssClass="table table-bordered table-row">
                <%--<HeaderStyle CssClass="gridheader_pop" Font-Size="Small" Height="20px" Wrap="False" />
                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />--%>
                <Columns>
                    <asp:BoundField DataField="BSU_NAME" HeaderText="Business Unit">
                    <ItemStyle Width="20%" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Category_Des" HeaderText="Task Category">
                    <ItemStyle Width="15%" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LEVEL0" HeaderText="Level 0">
                    <ItemStyle ForeColor="Black" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LEVEL1" HeaderText="Level 1">
                    <ItemStyle BackColor="Yellow" ForeColor="Black" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Level_1_Days" HeaderText="Level 1 Days">
                    <ItemStyle BackColor="Yellow" ForeColor="Black" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LEVEL2" HeaderText="Level 2">
                    <ItemStyle BackColor="Orange" ForeColor="Black" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Level_2_Days" HeaderText="Level 2 Days">
                    <ItemStyle BackColor="Orange" ForeColor="Black" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LEVEL3" HeaderText="Level 3">
                    <ItemStyle BackColor="Red" ForeColor="Black" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Level_3_Days" HeaderText="Level 3 Days">
                    <ItemStyle BackColor="Red" ForeColor="Black" HorizontalAlign="Center" />
                    </asp:BoundField>
                </Columns>
                <%--<EditRowStyle Wrap="False" />--%>
            </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Visible="False"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
     </div></div>
    <asp:LinkButton ID="LinkExport" runat="server">Export</asp:LinkButton>
    </form>
</body>
</html>
