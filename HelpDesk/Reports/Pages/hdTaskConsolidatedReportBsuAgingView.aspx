﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdTaskConsolidatedReportBsuAgingView.aspx.vb"
    Inherits="HelpDesk_Reports_Pages_hdTaskConsolidatedReportBsuView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <base  target="_self" /> 
    <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function ViewTraceFlow(task_list_id) {
            window.showModalDialog('../../Pages/hdTraceFlow.aspx?task_list_id=' + task_list_id, '', 'dialogHeight:600px;dialogWidth:800px;scroll:yes;resizable:yes;'); return false;

        }

        function ViewTaskDes(TaskListId) {

            window.open('../../Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="100%">
            <tr>
                <td class="subheader_img">
                    List
                    -<asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridTaskList" runat="server"  AutoGenerateColumns="False"
                        EmptyDataText="Search query did not retrieve any results. Please try with some other keyword(s)"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Task ID">
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Task&nbsp;ID
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <asp:LinkButton ID="LinkTaskList" runat="server" OnClientClick='<%#Eval("VIEW_TRACE_FLOW")%>'
                                            Text='<%#Eval("TASK_LIST_ID")%>'></asp:LinkButton>
                                        <asp:HiddenField ID="HiddenTaskId" runat="server" Value='<%#Eval("TASK_ID")%>' />
                                        <asp:HiddenField ID="HiddenTaskListId" runat="server" Value='<%#Eval("TASK_LIST_ID")%>' />
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BSU">
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                BSU
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%# Eval("BSU_SHORTNAME")%>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Priority">
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Priority
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%#Eval("PRIORITY_DESCRIPTION")%>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category">
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Category
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("ROOT_TAB")%>
                                    <br />
                                    <%# Eval("MAIN_CATEGORY_DESC")%>
                                    <br />
                                    <%#Eval("TASK_CATEGORY_DESCRIPTION")%>
                                </ItemTemplate>
                                <ItemStyle Width="200px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description">
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Title-Description
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkDes" runat="server" OnClientClick='<%#Eval("VIEW_TASK")%>'
                                        Text='<%#Eval("TASK_TITLE")%>' Width="150px"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="200px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reported By">
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Reported&nbsp;by
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("EMPNAME")%>
                                    <%#Eval("STUNAME")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Claimed By">
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Claimed&nbsp;by
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("CLAIM_EMPNAME")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">
                                                Status
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("TASK_STATUS_DESCRIPTION")%>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                        <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <EditRowStyle Wrap="False" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
<asp:LinkButton ID="LinkExport" runat="server">Export</asp:LinkButton>
    </div>
 
    

    </form>
</body>
</html>
