﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdTaskConsolidatedReportBsuView.aspx.vb"
    Inherits="HelpDesk_Reports_Pages_hdTaskConsolidatedReportBsuView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">

    
<head runat="server">
    <title></title>
    <base  target="_self" /> 
    

    

     <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/cssfiles/BSUstyles.css" rel="stylesheet" />
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <%--<script>
       
    </script>--%>

    <script type="text/javascript">

        function ViewTraceFlow(task_list_id) {
            //window.showModalDialog('../../Pages/hdTraceFlow.aspx?task_list_id=' + task_list_id, '', 'dialogHeight:600px;dialogWidth:800px;scroll:yes;resizable:yes;'); return false;
            
            Popup('../../Pages/hdTraceFlow.aspx?task_list_id=' + task_list_id, '', 'dialogHeight:600px;dialogWidth:800px;scroll:yes;resizable:yes;')

        }

        function ViewTaskDes(TaskListId) {

            //window.open('../../Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;
            Popup('../../Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;')
        }

        function Popup(url) {
            $.fancybox({
                'width': '80%',
                'height': '40%',
                'autoScale': true,
                'fitToView': false,
                'autoSize': false,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url
            });
        };
    
    </script>
</head>

    





<body>
    <form id="form1" runat="server">
    <div>
        <table  width="100%">
            <tr>
                <td class="subheader_img">
                    List
                    <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridTaskList" runat="server"  AutoGenerateColumns="False"
                        EmptyDataText="Search query did not retrieve any results. Please try with some other keyword(s)"
                        Width="100%" CssClass="table table-bordered table-row">
                        <Columns>
                            <asp:TemplateField HeaderText="Task ID">
                                <HeaderTemplate>
                                    Task ID
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <asp:LinkButton ID="LinkTaskList" runat="server" OnClientClick='<%#Eval("VIEW_TRACE_FLOW")%>'
                                            Text='<%#Eval("TASK_LIST_ID")%>'></asp:LinkButton>
                                        <asp:HiddenField ID="HiddenTaskId" runat="server" Value='<%#Eval("TASK_ID")%>' />
                                        <asp:HiddenField ID="HiddenTaskListId" runat="server" Value='<%#Eval("TASK_LIST_ID")%>' />
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BSU">
                                <HeaderTemplate>
                                    BSU
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%# Eval("BSU_SHORTNAME")%>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Priority">
                                <HeaderTemplate>
                                    Priority
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%#Eval("PRIORITY_DESCRIPTION")%>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category">
                                <HeaderTemplate>
                                     Category
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("ROOT_TAB")%>
                                    <br />
                                    <%# Eval("MAIN_CATEGORY_DESC")%>
                                    <br />
                                    <%#Eval("TASK_CATEGORY_DESCRIPTION")%>
                                </ItemTemplate>
                                <ItemStyle Width="200px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description">
                                <HeaderTemplate>
                                    Title-Description
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkDes" runat="server" OnClientClick='<%#Eval("VIEW_TASK")%>'
                                        Text='<%#Eval("TASK_TITLE")%>' Width="150px"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="200px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reported By">
                                <HeaderTemplate>
                                     Reported by
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("EMPNAME")%>
                                    <%#Eval("STUNAME")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Claimed By">
                                <HeaderTemplate>
                                    Claimed by
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("CLAIM_EMPNAME")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="Status">
                                <HeaderTemplate>
                                    Status
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("TASK_STATUS_DESCRIPTION")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <%--<HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                        <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />--%>
                        <EmptyDataRowStyle Wrap="False" />
                        <EditRowStyle Wrap="False" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <asp:LinkButton ID="LinkExport" runat="server">Export</asp:LinkButton>
    </form>
</body>
                
</html>
