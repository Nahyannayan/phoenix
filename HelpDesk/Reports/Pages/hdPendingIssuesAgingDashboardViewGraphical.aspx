﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdPendingIssuesAgingDashboardViewGraphical.aspx.vb"
    Inherits="HelpDesk_Reports_Pages_hdPendingIssuesAgingDashboardViewGraphical" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Charting" tagprefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <base  target="_self" /> 
    <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />

    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/cssfiles/BSUstyles.css" rel="stylesheet" />
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">


    <script type="text/javascript">

        function ViewTraceFlow(task_list_id) {
            //window.showModalDialog('../../Pages/hdTraceFlow.aspx?task_list_id=' + task_list_id, '', 'dialogHeight:600px;dialogWidth:800px;scroll:yes;resizable:yes;'); return false;
            Popup('../../Pages/hdTraceFlow.aspx?task_list_id=' + task_list_id)
        }

        function ViewTaskDes(TaskListId) {

            //'window.open('../../Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;
            Popup('../../Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId)
        }
    
    </script>
</head>
    
<body>
    <form id="form1" runat="server">
    <%--<div>
        
    </div>--%>
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" 
        style="width: 100%; height: 100%">
            <tr>
                <td class= "table-bg">
                    <asp:Label ID="lblTitle" runat="server">Business Units &amp; Task Escalations</asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblBsu" runat="server">Selected Business Unit(s): All</asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    
            <telerik:RadChart ID="chart" runat="server" Skin="SkyBlue" Width="650px" Height="700px">
                <Series>
<telerik:ChartSeries Name="Escalation Level 0">
    <Appearance>
        <FillStyle MainColor="LightGreen" filltype="Solid">
            <FillSettings>
                <ComplexGradient>
                    <telerik:GradientElement Color="213, 247, 255" />
                    <telerik:GradientElement Color="193, 239, 252" Position="0.5" />
                    <telerik:GradientElement Color="157, 217, 238" Position="1" />
                </ComplexGradient>
            </FillSettings>
        </FillStyle>
        <TextAppearance TextProperties-Color="51, 51, 51">
        </TextAppearance>
    </Appearance>
                    </telerik:ChartSeries>
<telerik:ChartSeries Name="Escalation Level 1">
    <Appearance>
        <FillStyle MainColor="Yellow" filltype="Solid">
            <FillSettings>
                <ComplexGradient>
                    <telerik:GradientElement Color="218, 254, 122" />
                    <telerik:GradientElement Color="198, 244, 80" Position="0.5" />
                    <telerik:GradientElement Color="153, 205, 46" Position="1" />
                </ComplexGradient>
            </FillSettings>
        </FillStyle>
        <TextAppearance TextProperties-Color="51, 51, 51">
        </TextAppearance>
        <Border Color="111, 174, 12" />
    </Appearance>
                    </telerik:ChartSeries>
                    <telerik:ChartSeries Name="Escalation Level 2">
                        <Appearance>
                            <FillStyle MainColor="Orange" filltype="Solid">
                                <FillSettings>
                                    <ComplexGradient>
                                        <telerik:GradientElement Color="136, 221, 246" />
                                        <telerik:GradientElement Color="97, 203, 234" Position="0.5" />
                                        <telerik:GradientElement Color="59, 161, 197" Position="1" />
                                    </ComplexGradient>
                                </FillSettings>
                            </FillStyle>
                            <TextAppearance TextProperties-Color="51, 51, 51">
                            </TextAppearance>
                            <Border Color="67, 181, 229" />
                        </Appearance>
                    </telerik:ChartSeries>
                    <telerik:ChartSeries Name="Escalation Level 3">
                        <Appearance>
                            <FillStyle MainColor="Red" filltype="Solid">
                                <FillSettings>
                                    <ComplexGradient>
                                        <telerik:GradientElement Color="163, 222, 78" />
                                        <telerik:GradientElement Color="132, 207, 27" Position="0.5" />
                                        <telerik:GradientElement Color="102, 181, 3" Position="1" />
                                    </ComplexGradient>
                                </FillSettings>
                            </FillStyle>
                            <TextAppearance TextProperties-Color="51, 51, 51">
                            </TextAppearance>
                            <Border Color="94, 160, 0" />
                        </Appearance>
                    </telerik:ChartSeries>
</Series>
                <PlotArea>
                    <EmptySeriesMessage>
                        <TextBlock Text="">
                        </TextBlock>
                    </EmptySeriesMessage>
                    <XAxis>
                        <Appearance Color="180, 210, 236" MajorTick-Color="206, 222, 235">
                            <MajorGridLines Color="206, 222, 235" PenStyle="Solid" />
                            <LabelAppearance Position-AlignedPosition="TopLeft">
                            </LabelAppearance>
                            <TextAppearance Position-Auto="False" Position-X="0" Position-Y="0" 
                                TextProperties-Color="51, 51, 51">
                            </TextAppearance>
                        </Appearance>
                        <AxisLabel>
                            <TextBlock>
                                <Appearance AutoTextWrap="False" TextProperties-Color="51, 51, 51">
                                </Appearance>
                            </TextBlock>
                        </AxisLabel>
                    </XAxis>
                    <YAxis>
                        <Appearance Color="180, 210, 236" MajorTick-Color="206, 222, 235" 
                            MinorTick-Color="206, 222, 235">
                            <MajorGridLines Color="206, 222, 235" />
                            <MinorGridLines Color="206, 222, 235" PenStyle="Dash" />
                            <TextAppearance TextProperties-Color="51, 51, 51">
                            </TextAppearance>
                        </Appearance>
                        <AxisLabel>
                            <TextBlock>
                                <Appearance TextProperties-Color="51, 51, 51">
                                </Appearance>
                            </TextBlock>
                        </AxisLabel>
                    </YAxis>
                    <Appearance Dimensions-Margins="18%, 100px, 12%, 8%">
                        <FillStyle FillType="Solid" MainColor="White">
                        </FillStyle>
                        <Border Color="180, 210, 236" />
                    </Appearance>
                </PlotArea>
                <Appearance>
                    <FillStyle MainColor="226, 247, 255">
                    </FillStyle>
                    <Border Color="82, 160, 226" />
                </Appearance>
                <ChartTitle>
                    <Appearance Dimensions-Margins="3%, 10px, 14px, 6%">
                        <FillStyle MainColor="Transparent">
                        </FillStyle>
                        <Border Color="Transparent" />
                    </Appearance>
                    <TextBlock Text="Business Units &amp; Task Escalations">
                        <Appearance TextProperties-Color="19, 111, 182" 
                            TextProperties-Font="Arial, 18pt">
                        </Appearance>
                    </TextBlock>
                </ChartTitle>
                <Legend>
                    <Appearance Dimensions-Margins="15%, 2%, 1px, 1px" 
                        Position-AlignedPosition="TopRight">
                        <ItemTextAppearance TextProperties-Font="Verdana, 8pt">
                        </ItemTextAppearance>
                        <FillStyle MainColor="Transparent">
                        </FillStyle>
                        <Border Color="Transparent" />
                    </Appearance>
                </Legend>
            </telerik:RadChart>
                    
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblmessage" runat="server" ForeColor="Red" Visible="False"></asp:Label>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
