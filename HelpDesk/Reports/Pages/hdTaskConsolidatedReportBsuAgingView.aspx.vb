﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class HelpDesk_Reports_Pages_hdTaskConsolidatedReportBsuView
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindGrid()
        End If

        If Request.QueryString("fdays") = "-1" And Request.QueryString("tdays") = "7" Then
            lblmessage.Text = "7 Days"
        End If
        If Request.QueryString("fdays") = "7" And Request.QueryString("tdays") = "14" Then
            lblmessage.Text = "14 Days"
        End If
        If Request.QueryString("fdays") = "14" And Request.QueryString("tdays") = "21" Then
            lblmessage.Text = "21 Days"
        End If
        If Request.QueryString("fdays") = "21" And Request.QueryString("tdays") = "30" Then
            lblmessage.Text = "30 Days"
        End If
        If Request.QueryString("fdays") = "30" And Request.QueryString("tdays") = "45" Then
            lblmessage.Text = "45 Days"
        End If
        If Request.QueryString("fdays") = "45" And Request.QueryString("tdays") = "10000" Then
            lblmessage.Text = "> 45 Days"
        End If


    End Sub

    Public Sub BindGrid(Optional ByVal export As Boolean = False)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FROM_DAYS", Request.QueryString("fdays"))
        pParms(1) = New SqlClient.SqlParameter("@TO_DAYS", Request.QueryString("tdays"))
        pParms(2) = New SqlClient.SqlParameter("@CLAME_BSU_ID", Request.QueryString("clmbsuid"))
        pParms(4) = New SqlClient.SqlParameter("@OPTION", 2)
        If Library.LibrarySuperAcess(Session("EmployeeId")) Then
        Else
            pParms(5) = New SqlClient.SqlParameter("@LOGIN_EMP_ID", Session("EmployeeId"))
        End If
        pParms(6) = New SqlClient.SqlParameter("@FROMDATE", Request.QueryString("fromdate"))
        pParms(7) = New SqlClient.SqlParameter("@TODATE", Request.QueryString("todate"))

        Dim ds As New DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "REPORT_TASK_CONSOLIDATED_BSU_AGING", pParms)

        GridTaskList.DataSource = ds
        GridTaskList.DataBind()

        If export Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dt As New DataTable

                dt = ds.Tables(0)
                dt.Columns.Remove("VIEW_TRACE_FLOW")
                dt.Columns.Remove("TASK_ID")
                dt.Columns.Remove("VIEW_TASK")


                ExportExcel(dt)
            End If

        End If

    End Sub

    Public Sub ExportExcel(ByVal dt As DataTable)

        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        ' ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "Data.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()

    End Sub

    Protected Sub LinkExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkExport.Click
        BindGrid(True)
    End Sub
End Class
