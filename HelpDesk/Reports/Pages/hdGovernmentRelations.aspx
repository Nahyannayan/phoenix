﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="hdGovernmentRelations.aspx.vb" Inherits="HelpDesk_Reports_Pages_hdGovernmentRelations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
 <style>
        .GridPager a, .GridPager span
        {
            display: block;
            height: 15px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            text-decoration: none;
        }
        .GridPager a
        {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }
        .GridPager span
        {
            background-color: #A1DCF2;
            color: #000;
            border: 1px solid #3AC0F2;
        }
    </style>
<%--    <link rel="stylesheet" type="text/css" href="../../../Scripts/jQuery-ui-1.10.3.css" />
    <script src="../../../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
   
    <script type="text/javascript" src="../../../Scripts/jquery-ui.js"></script>    --%>
 
       
    <script type="text/javascript">

 

        $(function () {

            $(".datepicker").datepicker({
                dateFormat: 'dd/M/yy'
            });
        }
    );


        function governmentRelationView(claimEMP, subCat,fromdate,todate,option) {
           
            var  url;
            //sFeatures = "dialogWidth: 800px; ";
            //sFeatures += "dialogHeight: 500px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: yes; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            //var NameandCode;
            //var result;
            var val = "?fromdate=" + fromdate + "&todate=" + todate + "&claimEMP=" + claimEMP + "&subCat=" + subCat + "&option=" + option;

            
            url = "hdTaskConsolidatedReportBsuView.aspx" + val;
            Popup(url)
            //result = window.showModalDialog(url, "", sFeatures);

            //if (result == '' || result == undefined) {
            //    return false;
            //}

            //return true;
        }   
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-question-square mr-3"></i>
            Government Relations Report
        </div>
 <div class="card-body">
            <div class="table-responsive m-auto">

    <table  width = "100%" align="center" >
        <%--<tr>
            <td class="subheader_img" colspan="4" align="center" width="100%>
                Government Relations Report
            </td>
            

        </tr>--%>



        <tr>
            <td align="left">
                <table width="100%" align="center">
                    <tr>
                        <td  width = "20%" align="left" >
                             <span class="field-label">
                           From Date</span>
                        </td>
                        
                        <td align="left" width = "30%">
                           
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputbox" Width="83px"></asp:TextBox>&nbsp;
                            <asp:ImageButton      ID="imgFromDate" runat="server" CausesValidation="False" CssClass="ajax_calendar" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                                
                        </td>
                        <td  width = "20%" align="left">
                           <span class="field-label"> To Date</span>
                        </td>
                        
                        <td  width = "30%">
                           
                             <asp:TextBox ID="txtToDate" runat="server" CssClass="inputbox" Width="83px"></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="imgToDate" runat="server" CausesValidation="False" CssClass="ajax_calendar" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" />
                        </td>
                    </tr>
                    <tr>
                        <td  width = "20%" align="left" > 
                           <span class="field-label"> Category</span>
                        </td>
                        
                        <td  width = "30%">
                            <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td  width = "20%" align="left">
                           <span class="field-label"> Sub Category</span>
                        </td>
                        
                        <td  width = "30%">
                            <asp:DropDownList ID="ddlSubCategory" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr >
                    <td colspan="4" align="center" width="100%">
                    <asp:Button ID="btnGovernmentRelations" runat="server" Text="SEARCH" CssClass="button" />
                    </td>
                    </tr>
                </table>
                
            </td>
        </tr>
    </table>
    <br />
    <table class="BlueTableView" width="100%" border="0">
    <tr id="trTitle" runat="server" visible="false">
            <td class="title-bg">
                Government Relations Report
            </td>
        </tr>
                <tr id="trError" runat="server" visible="false">
                <td><asp:Label ID="lblError" runat="server" CssClass="error" ></asp:Label></td>
                </tr>
                    <tr id="trGrid" runat="server" visible="false">
                        <td>
                            <asp:GridView ID="gvGovernementRelation" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" OnPageIndexChanging="gvGovernementRelation_PageIndexChanging" Width="100%" OnRowDataBound="gvGovernementRelation_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Assigned To">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAssignedto" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="All ">
                                        <ItemTemplate>
                                          <asp:LinkButton ID="lbtnGR" runat="server" Text='<%# Bind("COL_ALL") %>' ></asp:LinkButton>
                                          <asp:Label ID="lblClameBSU" Visible="false" runat="server" Text='<%# Bind("ROW_ID") %>'></asp:Label>
                                         <asp:Label ID="lblCALBSU" Visible="false" runat="server" Text='<%# Bind("CAL_BSU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NEW">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNEW" runat="server" Text='<%# Bind("COL_NEW") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ASSIGNED">
                                        <ItemTemplate>
                                            <asp:Label ID="lblASSIGNED" runat="server" Text='<%# Bind("COL_ASSIGNED") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="IN PROGRESS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPROGRESS" runat="server" Text='<%# Bind("COL_PROGRESS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ON HOLD">
                                        <ItemTemplate>
                                            <asp:Label ID="lblHOLD" runat="server" Text='<%# Bind("COL_HOLD") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="MEMBER COMPLETED">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMCT" runat="server" Text='<%# Bind("COL_MCT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                           <asp:TemplateField HeaderText="APPROVED">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAPP" runat="server" Text='<%# Bind("COL_APPROVED") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                           <asp:TemplateField HeaderText="COMPLETED">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCOMPLETED" runat="server" Text='<%# Bind("COL_COMPLETED") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="OVER DUE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOVERDUE" runat="server" Text='<%# Bind("COL_OVERDUE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <%--<RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                   <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />--%>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

                </div></div></div>
                
                 <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    
    
      <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    
    
</asp:Content>
