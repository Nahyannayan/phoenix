﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class HelpDesk_Reports_Pages_hdGovernmentRelations
    Inherits BasePage
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "HD02024") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    bindCategory()
                    txtFromDate.Text = "01/JAN/2013"
                    txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    ''calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                  
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights                   


                  
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try

        End If

    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Try
            bindSubCategory(Convert.ToInt32(ddlCategory.SelectedValue))
        Catch ex As Exception
            '' lblError.InnerHtml = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvGovernementRelation_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvGovernementRelation.PageIndex = e.NewPageIndex
        BindGovernementRelations()
    End Sub

    Protected Sub btnGovernmentRelations_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGovernmentRelations.Click
        Try
            BindGovernementRelations()
        Catch ex As Exception
            lblError.Text = "ERROR WHILE RETRIEVING DATA"
        End Try
    End Sub

    Protected Sub gvGovernementRelation_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblClameBSU As Label = DirectCast(e.Row.FindControl("lblClameBSU"), Label)
                Dim lblCALBSU As Label = DirectCast(e.Row.FindControl("lblCALBSU"), Label)

                Dim lbtnGR As LinkButton = DirectCast(e.Row.FindControl("lbtnGR"), LinkButton)

                lbtnGR.OnClientClick = [String].Format("javascript:governmentRelationView('{0}', '{1}','{2}','{3}','{4}');", lblClameBSU.Text, ddlSubCategory.SelectedItem.Value, txtFromDate.Text, txtToDate.Text, 25)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub bindCategory()
        Try

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim PARAM(2) As SqlParameter

            PARAM(0) = New SqlParameter("@subtabId", 29)


            Using CategoryReader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GR.GET_CATEGORY_LIST", PARAM)
                ddlCategory.Items.Clear()


                If CategoryReader.HasRows = True Then

                    ddlCategory.DataSource = CategoryReader
                    ddlCategory.DataTextField = "MAIN_CATEGORY_DESC"
                    ddlCategory.DataValueField = "MAIN_CATEGORY_ID"
                    ddlCategory.DataBind()
                    ddlCategory.Items.Insert(0, New ListItem("-All-", "0"))
                Else
                    ddlCategory.Items.Insert(0, New ListItem("-No category Exists-", "0"))

                End If
            End Using
            ddlCategory_SelectedIndexChanged(ddlCategory, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub bindSubCategory(ByVal CategoryID As Integer)
        Try

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim PARAM(2) As SqlParameter

            PARAM(0) = New SqlParameter("@CategoryId", CategoryID)


            Using CategoryReader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GR.GET_SUBCATEGORY_BY_CATEGORYID", PARAM)
                ddlSubCategory.Items.Clear()


                If CategoryReader.HasRows = True Then

                    ddlSubCategory.DataSource = CategoryReader
                    ddlSubCategory.DataTextField = "category_des"
                    ddlSubCategory.DataValueField = "ID"
                    ddlSubCategory.DataBind()
                    ddlSubCategory.Items.Insert(0, New ListItem("-All-", "0"))
                Else
                    ddlSubCategory.Items.Insert(0, New ListItem("-No subcategory Exists-", "0"))

                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub BindGovernementRelations()

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionStringHelpDesk").ConnectionString()
            Dim selectedbsu As String = ""
            Dim param(4) As SqlClient.SqlParameter
            Dim ds As DataSet

            param(1) = New SqlClient.SqlParameter("@SUB_CATEGORY_ID", ddlSubCategory.SelectedItem.Value)
            param(2) = New SqlClient.SqlParameter("@FROMDATE", txtFromDate.Text)
            param(3) = New SqlClient.SqlParameter("@TODATE", txtToDate.Text)
            

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GR.GET_GOVERNMENT_RELATION_REPORT", param)

            If ds.Tables(0).Rows.Count > 0 Then

                gvGovernementRelation.DataSource = ds.Tables(0)
                gvGovernementRelation.DataBind()
                trTitle.Visible = True

                trGrid.Visible = True
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvGovernementRelation.DataSource = ds.Tables(0)
                Try
                    gvGovernementRelation.DataBind()
                    trTitle.Visible = True
                    trGrid.Visible = True
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvGovernementRelation.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvGovernementRelation.Rows(0).Cells.Clear()
                gvGovernementRelation.Rows(0).Cells.Add(New TableCell)
                gvGovernementRelation.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGovernementRelation.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGovernementRelation.Rows(0).Cells(0).Text = "No record found."
            End If
        Catch ex As Exception
            trError.Visible = True
            lblError.Text = "ERROR WHILE RETRIEVING DATA"
        End Try


    End Sub
End Class
