﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class HelpDesk_Reports_Pages_hdPendingIssuesAgingDashboardView
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'If Not IsPostBack Then
        '    If Request.QueryString("option") = "11" Then
        '        lblmessage.Text = "Total Issue"
        '    End If
        '    If Request.QueryString("option") = "12" Then
        '        lblmessage.Text = "Assigned"
        '    End If
        '    If Request.QueryString("option") = "13" Then
        '        lblmessage.Text = "Member Completed"
        '    End If
        '    If Request.QueryString("option") = "14" Then
        '        lblmessage.Text = "Member Pending"
        '    End If
        '    If Request.QueryString("option") = "15" Then
        '        lblmessage.Text = "Member Pending Future Dates"
        '    End If


        '    BindGrid()
        'End If

        Try
            If Not Request.QueryString("BsuName") Is Nothing Then
                If Request.QueryString("BsuName") = "SELECT" Then
                    Me.lblBsu.Text = "Selected Business Unit(s): All"
                Else
                    Me.lblBsu.Text = "Selected Business Unit(s): " & Request.QueryString("BsuName")
                End If
            End If
            If Not Request.QueryString("Option") Is Nothing Then
                If Request.QueryString("Option") = "1" Then
                    Me.lblTitle.Text = "Business Units & Task Escalations"
                    Me.gvDashboard1.Columns(1).Visible = False
                    Me.gvDashboard1.Columns(4).Visible = False
                    Me.gvDashboard1.Columns(6).Visible = False
                    Me.gvDashboard1.Columns(8).Visible = False
                Else
                    Me.lblTitle.Text = "Business Units, Task Categories, Task Escalation Days & Task Escalations"
                    Me.gvDashboard1.Columns(1).Visible = True
                    Me.gvDashboard1.Columns(4).Visible = True
                    Me.gvDashboard1.Columns(6).Visible = True
                    Me.gvDashboard1.Columns(8).Visible = True
                End If
            End If

            If Not Session("dsPendingIssuesAging") Is Nothing Then
                gvDashboard1.DataSource = CType(Session("dsPendingIssuesAging"), DataSet)
                gvDashboard1.DataBind()
            End If
            Me.lblmessage.Visible = False
        Catch ex As Exception
            Me.lblmessage.Text = "Error occured getting the data"
            Me.lblmessage.Visible = True
        End Try



    End Sub

    Protected Sub gvDashboard_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDashboard1.PageIndexChanging
        Try
            If Not Session("dsPendingIssuesAging") Is Nothing Then
                gvDashboard1.DataSource = CType(Session("dsPendingIssuesAging"), DataSet)
            End If
            Me.gvDashboard1.PageIndex = e.NewPageIndex
            gvDashboard1.DataBind()
            Me.lblmessage.Visible = False
        Catch ex As Exception
            Me.lblmessage.Text = "Error occured getting the data"
            Me.lblmessage.Visible = True
        End Try

    End Sub



    Public Sub ExportExcel(ByVal dt As DataTable)


        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        ' ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xlsx")
        ''ef.SaveXls(Response.OutputStream)
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)
        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "Data.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()


    End Sub


    Protected Sub LinkExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkExport.Click
        Dim ds As DataSet
        If Not Session("dsPendingIssuesAging") Is Nothing Then
            ds = CType(Session("dsPendingIssuesAging"), DataSet)
        End If
        If Not ds Is Nothing Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dt As New DataTable

                dt = ds.Tables(0)



                ExportExcel(dt)
            End If
        End If

    End Sub
End Class
