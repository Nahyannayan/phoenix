<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdMyComplaints.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="HelpDesk_hdMyComplaints" %>

<%@ Register Src="UserControls/hdMyComplaints.ascx" TagName="hdMyComplaints" TagPrefix="uc1" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
<script type="text/javascript">

    function ViewTaskDes(TaskListId) {

        window.open('Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId, '', 'Height:768px;Width:1024px;scroll:auto;resizable:yes;'); return false;

    }
    function History(task_list_id) {

        window.showModalDialog('Pages/hdTaskHistory.aspx?task_list_id=' + task_list_id + '&Usertype=CLIENT', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

    }
    function Followup(task_list_id, to_emp_id) {

        window.showModalDialog('Pages/hdFollowupClientOwner.aspx?Task_list_id=' + task_list_id + '&to_emp_id=' + to_emp_id + '&Usertype=OWNER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

    }        

 
    function ownerclosejob(taslistit) {
        window.showModalDialog("Pages/hdClientTaskClosing.aspx?tasklistid=" + taslistit, "", "dialogWidth:700px; dialogHeight:600px; center:yes");
    }

 


</script>
 <div>
        <uc1:hdMyComplaints id="HdMyComplaints1" runat="server">
        </uc1:hdMyComplaints></div>

</asp:Content> 

