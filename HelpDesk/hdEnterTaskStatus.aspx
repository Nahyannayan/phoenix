<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdEnterTaskStatus.aspx.vb"  MasterPageFile="~/mainMasterPage.master"  Inherits="HelpDesk_hdEnterTaskStatus" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="UserControls/hdEnterTaskStatus.ascx" TagName="hdEnterTaskStatus"
    TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
<script type="text/javascript">
    function handleError() {

        return true;
    }
    window.onerror = handleError;


    function EnterStatus(assignId) {

        //window.showModalDialog('Pages/hdStatusUpdate.aspx?AssignedTaskid=' + assignId, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        var url = "Pages/hdStatusUpdate.aspx?AssignedTaskid=" + assignId;
        var oWnd = radopen(url, "pop_DEDL");

    }

    function ViewTask(assignId) {

      //  window.open('Pages/hdTaskAssignView.aspx?AssignedTaskid=' + assignId, '', 'Height:700px;Width:800px;scroll:auto;resizable:yes;'); return false;
        var url = "Pages/hdStatusUpdate.aspx?AssignedTaskid=" + assignId;
        var oWnd = radopen(url, "pop_DEDL");
    }

    function ViewTaskStatus(assignId) {

       // window.showModalDialog('Pages/hdStatusView.aspx?AssignedTaskid=' + assignId, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        var url = "Pages/hdStatusView.aspx?AssignedTaskid=" + assignId;
        var oWnd = radopen(url, "pop_DEDL");


    }
    function StatusChart(AssignedTaskid, emp_id) {

       // window.showModalDialog('FusionCharts/hdUserTaskProgressChart.aspx?emp_id=' + emp_id + '&AssignedTaskid=' + AssignedTaskid, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        var url = "FusionCharts/hdUserTaskProgressChart.aspx?emp_id=" + emp_id ;
        var oWnd = radopen(url, "pop_DEDL");

    }

    function ReAssign(task_list_id, task_category_id) {

        //window.showModalDialog('Pages/hdTaskReassign.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id + '&Usertype=MEMBER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

        var url = "Pages/hdTaskReassign.aspx?task_list_id=" + task_list_id + "&task_category_id=" + task_category_id + "&Usertype=MEMBER";
        var oWnd = radopen(url, "pop_DEDL");
    }

    function History(task_list_id, task_category_id) {

        // window.showModalDialog('Pages/hdTaskHistory.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id + '&Usertype=MEMBER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

        var url = "Pages/hdTaskHistory.aspx?task_list_id=" + task_list_id + "&task_category_id=" + task_category_id + "&Usertype=MEMBER";
        var oWnd = radopen(url, "pop_DEDL");

    }
    function SendMail(TaskAssignid, FromEmpId, ToEmpid, Parent_Message_Id) {
  
       // window.showModalDialog('Pages/hdEnterMessage.aspx?TaskAssignid=' + TaskAssignid + '&FromEmpId=' + FromEmpId + '&ToEmpid=' + ToEmpid + '&Parent_Message_Id=' + Parent_Message_Id, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

        var url = "Pages/hdEnterMessage.aspx?TaskAssignid=" + TaskAssignid + "&FromEmpId=" + FromEmpId + "&ToEmpid=" + ToEmpid + "&Parent_Message_Id=" + Parent_Message_Id;
        var oWnd = radopen(url, "pop_DEDL");

    }
    function OnClientCloseDEDL(oWnd, args) {
    }
    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }
</script>
   <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_DEDL" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseDEDL"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        
    </telerik:RadWindowManager>
<div class="matters">
     <uc1:hdEnterTaskStatus ID="HdEnterTaskStatus1" runat="server" />
</div>
</asp:Content>

