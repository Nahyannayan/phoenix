<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdTaskAssigning.aspx.vb" MasterPageFile="~/mainMasterPage.master"   Inherits="HelpDesk_hdTaskAssigning" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Src="UserControls/hdTaskAssigning.ascx" TagName="hdTaskAssigning" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server" >
<script type="text/javascript">


    function Assign(task_list_id, task_category_id) {

        //  window.showModalDialog('Pages/hdTaskAssignEmp.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id, '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;
        var url = "Pages/hdTaskAssignEmp.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id"
        var oWnd = radopen(url, "pop_DEDL");

    }
    function ReAssign(task_list_id, task_category_id) {

        window.showModalDialog('Pages/hdTaskReassign.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id + '&Usertype=OWNER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

    }

    function History(task_list_id, task_category_id) {

        window.showModalDialog('Pages/hdTaskHistory.aspx?task_list_id=' + task_list_id + '&task_category_id=' + task_category_id + '&Usertype=OWNER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

    }


    function openimage(fname) {
        window.showModalDialog('hdImageView.aspx?filename=' + fname + '&Filecount=' + '1', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

    }

    function ViewTaskDes(TaskListId) {

        window.open('Pages/hdTaskDescriptionView.aspx?TaskListId=' + TaskListId, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

    }

    function ChartComp(TaskListid) {

        window.showModalDialog('FusionCharts/hdTaskProgressCompare.aspx?TaskListid=' + TaskListid, '', 'dialogHeight:600px;dialogWidth:800px;scroll:auto;resizable:yes;'); return false;

    }

    function Followup(task_list_id, to_emp_id) {

        window.showModalDialog('Pages/hdFollowupClientOwner.aspx?Task_list_id=' + task_list_id + '&to_emp_id=' + to_emp_id + '&Usertype=OWNER', '', 'dialogHeight:768px;dialogWidth:1024px;scroll:auto;resizable:yes;'); return false;

    }

    function ViewTraceFlow(task_list_id) {
        window.showModalDialog('Pages/hdTraceFlow.aspx?task_list_id=' + task_list_id, '', 'dialogHeight:600px;dialogWidth:800px;scroll:yes;resizable:yes;'); return false;

    }
        


    function ownerclosejob(taslistit) {
        window.showModalDialog("Pages/hdOwnerTaskClosing.aspx?tasklistid=" + taslistit, "", "dialogWidth:700px; dialogHeight:600px; center:yes");
    }

    function OnClientCloseDEDL(oWnd, args) {
    }


    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }
        
</script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_DEDL" runat="server" Behaviors="Close,Move" OnClientClose="OnClientCloseDEDL"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        
    </telerik:RadWindowManager>
<div class="matters">
<table width="100%"> 
<tr>
<td align="left" >


</td>
</tr>
</table>
 <uc1:hdTaskAssigning ID="HdTaskAssigning1" runat="server" />
</div>

</asp:Content>  

