<%@ Page Language="VB" AutoEventWireup="false" CodeFile="hdHelpdesk.aspx.vb"  MaintainScrollPositionOnPostback="true"  MasterPageFile="~/mainMasterPage.master" Inherits="HelpDesk_hdHelpdesk" %>

<%@ Register Src="UserControls/hdInternalTabs.ascx" TagName="hdInternalTabs" TagPrefix="uc1" %>
<%@ Register Src="UserControls/hdStatusEnquiry.ascx" TagName="hdStatusEnquiry" TagPrefix="uc2" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage"  runat="server" >
 <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>PHOENIX HELP DESK
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table width="100%">
<tr>
<td align="left" >
 <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
                <ajaxToolkit:TabPanel ID="T1" runat="server">
                    <ContentTemplate>
                        <uc1:hdInternalTabs ID="HdInternalTabs1" runat="server" />
                    </ContentTemplate>
                    <HeaderTemplate>
                       Add Issues
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>
                  <ajaxToolkit:TabPanel ID="T3" runat="server">
                    <ContentTemplate>
                        <uc2:hdStatusEnquiry ID="HdStatusEnquiry1" runat="server" />
                     
                    </ContentTemplate>
                    <HeaderTemplate>
                  Task Enquiry
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>

            <asp:HiddenField ID="Hiddenempid" runat="server" />
            <asp:HiddenField ID="Hiddenbsuid" runat="server" />


</td>
</tr>
</table>

      </div>
            </div>     

</div>
</asp:Content> 

