Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports UtilityObj
Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic

Partial Class loginMain
    Inherits System.Web.UI.Page
    Dim SessionFlag As Integer
    Private m_bIsTerminating As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim Encr_decrData As New Encryption64

            ''Dim str As String = Encr_decrData.Decrypt("+5hNx/lBy6Y64Uy4F3tq6g==")
            ClientScript.RegisterStartupScript(Me.GetType(), _
           "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            If Page.IsPostBack = False Then
                ViewState("attempt") = "1"
                If Not Request.Cookies("OASIS_SETTINGS") Is Nothing Then
                    txtUsername.Text = Request.Cookies("OASIS_SETTINGS")("UsrName")
                    txtPassword.Focus()
                Else
                    txtUsername.Focus()
                End If
                If Request.QueryString("logoff") <> "" Then
                    'call the class to clear all the lock----not completed
                    Dim jhd_sub_id As String = Session("Sub_ID")
                    Dim Bsuid As String = Session("sBsuid")
                    Dim JHD_fyear As Integer = Session("F_YEAR")
                    Dim Doctype As String = String.Empty
                    Dim DocNo As String = String.Empty
                    Dim SessionCode As String = String.Empty
                    Dim JHD_user As String = Session("sUsr_name")
                    Dim JHD_Timestamp As Byte

                    Dim clearlocks As Integer = AccessRoleUser.ClearLocks(jhd_sub_id, Bsuid, JHD_fyear, Doctype, DocNo, SessionCode, JHD_user, JHD_Timestamp)
                    If clearlocks <> 0 Then
                        Throw New ArgumentException("Error while clearing the locks")
                    End If
                    Dim auditFlag As Integer = UtilityObj.operOnAudiTable("Logout", "logout", "Logout", Page.User.Identity.Name.ToString)
                    If auditFlag <> 0 Then
                        Throw New ArgumentException("Unable to track your request")
                    End If
                    SessionFlag = AccessRoleUser.UpdateSessionID(Session("sUsr_name").ToString, "")
                    If SessionFlag <> 0 Then
                        Throw New ArgumentException("Unable to track your request")
                    End If
                    Session.RemoveAll()
                End If
            End If
        Catch myex As ArgumentException
            lblResult.Text = myex.Message
            UtilityObj.Errorlog(myex.Message, "Logoff Process")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Logoff Process")
        End Try
    End Sub

    Private Sub getSettings(ByVal BusUnit As String)
        Try
            If Not ((Session("sroleid") = "46") Or (Session("sroleid") = "47")) Then
                Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(BusUnit)
                    While BSUInformation.Read
                        Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                        Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                        Session("BSU_COUNTRY_ID") = Trim(Convert.ToString(BSUInformation("BSU_COUNTRY_ID")))
                        Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                        Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                        Session("CollectBank") = Convert.ToString(BSUInformation("CollectBank"))
                        Session("Collect_name") = Convert.ToString(BSUInformation("Collect_name"))
                        Session("BSU_ROUNDOFF") = Convert.ToInt32(BSUInformation("BSU_ROUNDOFF"))
                        Session("BSU_CURRENCY") = Convert.ToString(BSUInformation("BSU_CURRENCY"))
                        Session("BSU_PAYYEAR") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Session("F_YEAR"), BSUInformation("BSU_PAYYEAR")))
                        Session("BSU_PAYMONTH") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Month(Date.Today), BSUInformation("BSU_PAYMONTH")))
                        Session("BSU_IsOnDAX") = Convert.ToInt32(BSUInformation("BSU_IsOnDAX"))
                        Session("BSU_IsHROnDAX") = Convert.ToInt32(BSUInformation("BSU_IsHROnDAX"))
                        Dim strformat As String = "0."
                        For i As Integer = 1 To CInt(Session("BSU_ROUNDOFF"))
                            strformat = strformat & "0"
                        Next
                        Session("BSU_DataFormatString") = strformat
                    End While
                End Using
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim SQLStr As String
                SQLStr = "SELECT FYR_ID,FYR_DESCR,FYR_FROMDT  ,FYR_TODT from FINANCIALYEAR_S WHERE  bDefault=1"
                Dim dtFinYear As DataTable
                dtFinYear = Mainclass.getDataTable(SQLStr, str_conn)
                If dtFinYear.Rows.Count > 0 Then
                    Session("F_YEAR") = dtFinYear.Rows(0).Item("FYR_ID")
                    Session("F_Descr") = dtFinYear.Rows(0).Item("FYR_DESCR")
                    Session("F_TODT") = AccessRoleUser.GetFinancialYearDate(dtFinYear.Rows(0).Item("FYR_ID"))
                End If
            End If

            Dim OasisSettingsCookie As HttpCookie
            If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
                OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
            Else
                OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
            End If
            OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
            Response.Cookies.Add(OasisSettingsCookie)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Business Unit Selection")
        End Try
    End Sub


    Public Shared Function GetUserDetails(ByVal p_USR_NAME As String, ByVal p_USR_PASSWORD As String, _
   ByVal p_AUD_HOST As String, ByVal p_AUD_WINUSER As String, ByRef dtUserData As DataTable, _
    ByRef USR_TRYCOUNT As Integer, ByVal stTrans As SqlTransaction) As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'EXEC	@return_value = [dbo].[GetUserDetails]
        '@USR_NAME = N'guru',
        '@USR_PASSWORD = N'wr92h2HbWq4=',
        '@USR_TRYCOUNT = @USR_TRYCOUNT OUTPUT,
        '@AUD_HOST = N'x',
        '@AUD_WINUSER = N'xx'

        Try

            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar, 20)
            pParms(0).Value = p_USR_NAME
            pParms(1) = New SqlClient.SqlParameter("@USR_PASSWORD", SqlDbType.VarChar, 100)
            pParms(1).Value = p_USR_PASSWORD
            pParms(2) = New SqlClient.SqlParameter("@USR_TRYCOUNT", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.Output

            pParms(3) = New SqlClient.SqlParameter("@AUD_HOST", SqlDbType.VarChar, 100)
            pParms(3).Value = p_AUD_HOST
            pParms(4) = New SqlClient.SqlParameter("@AUD_WINUSER", SqlDbType.VarChar, 100)
            pParms(4).Value = p_AUD_WINUSER
            pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            'Dim stTrans As SqlTransaction = objConn.BeginTransaction

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(stTrans, CommandType.StoredProcedure, "GetUserDetails_For_GemsIT", pParms)
            'retval = SqlHelper.ExecuteNonQuery(objConn, CommandType.StoredProcedure, "GetUserDetails", pParms)
            If pParms(5).Value = "0" Then
                If ds.Tables.Count > 0 Then
                    dtUserData = ds.Tables(0)
                    USR_TRYCOUNT = pParms(2).Value
                End If
            End If
            USR_TRYCOUNT = pParms(2).Value
            GetUserDetails = pParms(5).Value

        Catch ex As Exception
            Errorlog(ex.Message)
            GetUserDetails = "1000"

        End Try
    End Function
    Protected Overloads Overrides Sub RaisePostBackEvent(ByVal sourceControl As IPostBackEventHandler, ByVal eventArgument As String)
        If m_bIsTerminating = False Then
            MyBase.RaisePostBackEvent(sourceControl, eventArgument)
        End If
    End Sub

    Private Sub DisableUserLogin()
        Dim stTrans As SqlTransaction
        Try
            ' If Loging attempt is exceeded than maximum alooted then 
            '[DisableUserLogin]
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()

            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar, 50)
            pParms(0).Value = txtUsername.Text.Trim()

            Dim IntCnt As Integer
            stTrans = objConn.BeginTransaction
            IntCnt = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "DisableUserLogin", pParms)
            'retval 
            stTrans.Commit()
        Catch ex As Exception
            stTrans.Rollback()
        End Try
    End Sub

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        If m_bIsTerminating = False Then
            MyBase.Render(writer)
        End If
    End Sub
    Protected Sub Lnkfpassword_Click(sender As Object, e As EventArgs) Handles lnkfpassword.Click
        'Response.Redirect("https://selfreset.gemseducation.com")
        Dim strConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim retval As Integer
        Try
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar, 20)
            pParms(0).Value = "%" & LTrim(Trim(txtUsername.Text)) & "%"
            pParms(1) = New SqlClient.SqlParameter("OPTION", SqlDbType.Int)
            pParms(1).Value = 2
            pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            retval = SqlHelper.ExecuteNonQuery(strConn, CommandType.StoredProcedure, "TestUserDomain", pParms)
            If pParms(2).Value = "1" Then
                lblResult.Text = "Please click on Others Link"
            Else
                lblResult.Text = ""
                Response.Redirect("https://selfreset.gemseducation.com")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub LnkfSpassword_Click(sender As Object, e As EventArgs) Handles lnkfSpassword.Click
        'ClientScript.RegisterStartupScript(Page.GetType(), "ForgotPassword", "ForgotPassword();", True)
        Dim strConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim retval As Integer
        Try
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar, 30)
            pParms(0).Value = "%" & LTrim(Trim(txtUsername.Text)) & "%"
            pParms(1) = New SqlClient.SqlParameter("OPTION", SqlDbType.Int)
            pParms(1).Value = 1
            pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            retval = SqlHelper.ExecuteNonQuery(strConn, CommandType.StoredProcedure, "TestUserDomain", pParms)
            If pParms(2).Value = "1" Then
                lblResult.Text = "Please click Gems Staff Link"
            Else
                lblResult.Text = ""
                ClientScript.RegisterStartupScript(Page.GetType(), "ForgotPassword", "ForgotPassword();", True)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub





    Protected Sub imgBtnLogin_Click(sender As Object, e As EventArgs) Handles imgBtnLogin.Click

        'check whether user is expired
        Dim str_conn2 As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", txtUsername.Text)
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn2, CommandType.StoredProcedure, "Get_USR_NAME_ACTIVE", pParms)
            While reader.Read
                Session("sUsr_name") = txtUsername.Text
                Dim lstrActive = Convert.ToString(reader("CURRSTATUS"))
                If lstrActive = "EXPIRED" Then
                    hfExpired.Value = "expired"
                    Exit Sub
                End If
            End While
        End Using




        'use to send the Encrypted password
        If ViewState("attempt") Is Nothing Then
            ViewState("attempt") = 1
        End If
        If CInt(ViewState("attempt")) > OASISConstants.MAX_PASSWORDTRY Then
            Response.Redirect("common/loginwarning.aspx?un=" & txtUsername.Text.Trim(), False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
            m_bIsTerminating = True
            ' Disbale User Login
            DisableUserLogin()
            Exit Sub
        End If
        hfExpired.Value = ""
        Dim passwordEncr As New Encryption64
        Dim username As String = txtUsername.Text
        Dim password As String = passwordEncr.Encrypt(txtPassword.Text)
        Dim tblusername As String = ""
        Dim tblpassword As String = ""
        Dim tblroleid As Integer = 0
        Dim tblbsuper As Boolean = False
        Dim tblbITSupport As Boolean = False
        Dim tblbsuid As String = ""
        Dim tblbUsr_id As String = ""
        Dim tblDisplay_usr As String = ""
        Dim bCount As Integer = 0
        Dim var_F_Year As String = ""
        Dim var_F_Year_ID As String = ""
        Dim var_F_Date As String = ""
        Dim bUSR_MIS As Integer = 0
        Dim USR_bShowMISonLogin As Integer = 0
        Dim tblModuleAccess As String = ""
        Dim USR_FCM_ID As String = ""
        Dim tblReqRole As String = ""
        Dim tblDays As Integer = 0

        Dim tblBSU_Name As String = ""
        Dim tblEMPID As String = ""
        Dim tblCOSTEMP As String = ""
        Dim tblCOSTSTU As String = ""
        Dim tblCOSTOTH As String = ""
        Dim tbldef_module As String = ""
        Dim tblUSR_ForcePwdChange As Integer = 0

        Session("tot_bsu_count") = "0"
        'Session("sUsr_name") = txtUsername.Text
        Try
            Dim dt As New DataTable
            Dim trycount As Integer
            '''''''''''
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn) '
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retval As String = "1000"
                retval = GetUserDetails(username, password, "", "", dt, trycount, stTrans)
                '  retval = AccessRoleUser.GetUserDetails(username, password, "", "", dt, trycount, stTrans)
                stTrans.Commit()
                If retval = "0" Then
                    If dt.Rows.Count > 0 Then
                        tblusername = Convert.ToString(dt.Rows(0)("usr_name"))
                        tblDisplay_usr = Convert.ToString(dt.Rows(0)("Display_Name"))
                        tblpassword = passwordEncr.Decrypt(Convert.ToString(dt.Rows(0)("usr_password")).Replace(" ", "+"))
                        tblroleid = Convert.ToInt32(dt.Rows(0)("usr_rol_id"))
                        tblbsuper = IIf(IsDBNull(dt.Rows(0)("usr_bsuper")), False, dt.Rows(0)("usr_bsuper"))
                        tblbITSupport = IIf(IsDBNull(dt.Rows(0)("USR_bITSupport")), False, dt.Rows(0)("USR_bITSupport"))
                        tblbsuid = Convert.ToString(dt.Rows(0)("usr_bsu_id"))
                        tblbUsr_id = Convert.ToString(dt.Rows(0)("usr_id"))
                        tblModuleAccess = Convert.ToString(dt.Rows(0)("USR_MODULEACCESS"))
                        tblReqRole = Convert.ToString(dt.Rows(0)("USR_REQROLE"))
                        tblEMPID = Convert.ToString(dt.Rows(0)("USR_EMP_ID"))
                        USR_FCM_ID = Convert.ToString(dt.Rows(0)("USR_FCM_ID"))
                        tblBSU_Name = Convert.ToString(dt.Rows(0)("BSU_NAME"))
                        tblCOSTEMP = dt.Rows(0)("COSTcenterEMP").ToString()
                        tblCOSTSTU = dt.Rows(0)("COSTcenterSTU").ToString()
                        tblCOSTOTH = dt.Rows(0)("COSTcenterOTH").ToString()
                        tbldef_module = dt.Rows(0)("USR_MODULEACCESS").ToString()
                        tblUSR_ForcePwdChange = dt.Rows(0)("USR_ForcePwdChange")
                        If IsDBNull(dt.Rows(0)("USR_MIS")) Then
                            bUSR_MIS = 0
                        Else
                            bUSR_MIS = Convert.ToInt32(dt.Rows(0)("USR_MIS"))
                        End If
                        If IsDBNull(dt.Rows(0)("USR_bShowMISonLogin")) Then
                            USR_bShowMISonLogin = 0
                        Else
                            USR_bShowMISonLogin = Convert.ToInt32(dt.Rows(0)("USR_bShowMISonLogin"))
                        End If

                        tblDays = CInt(dt.Rows(0)("Days"))
                        Session("lastlogtime") = Convert.ToString(dt.Rows(0)("lastlogtime"))
                    End If
                ElseIf retval = "560" Then
                    'Response.Redirect("common/loginwarning.aspx?un=" & txtUsername.Text.Trim(), False)
                    'HttpContext.Current.ApplicationInstance.CompleteRequest()
                    'm_bIsTerminating = True
                    Session("sUsr_name") = username
                    hfExpired.Value = "expired"
                    'DisableUserLogin()
                    Exit Sub
                ElseIf retval = "2600" Then
                    Dim RedirectUsername As String = txtUsername.Text
                    Dim RedirectPassword As String = txtPassword.Text
                    Dim LogTime As String = Now.ToString("dd-MMM-yyyyHH")
                    Dim RedirectQueryStr As String
                    RedirectQueryStr = RedirectUsername & "|" & RedirectPassword & "|" & LogTime
                    RedirectQueryStr = passwordEncr.Encrypt(RedirectQueryStr)
                    Dim URL As String
                    URL = WebConfigurationManager.AppSettings("OldOasisURL") & "/loginRedirect.aspx?OASISLogin=" & RedirectQueryStr
                    Response.Redirect(URL, False)
                Else
                    lblResult.Text = getErrorMessage(retval)
                    Select Case trycount
                        Case -1
                            'mail here
                            Try
                                Dim str_data As String = GetDataFromSQL("SELECT     SYS_EMAIL_HOST + '|' + LTRIM(SYS_EMAIL_PORT) + '|' + SYS_ADMIN_EMAIL FROM SYSINFO_S", _
                                WebConfigurationManager.ConnectionStrings("maindb").ConnectionString)
                                UtilityObj.SendEmail(str_data.Split("|")(2), "This user is disabled : " & txtUsername.Text, _
                                 str_data.Split("|")(0), str_data.Split("|")(1), txtUsername.Text, _
                                 "<html><Body>Hi, <br />  This is an Automated mail from GEMS OASIS regarding suspicious login attempts.<br/>  " _
                                 & "User Name : " & txtUsername.Text & "<br />" _
                                 & "IP Address : " & Request.UserHostAddress.ToString() & "<br />" _
                                 & "<br /><br /><br /><br />Regards,<br />GEMS OASIS TEAM  </Body></html>", "system@gemseducation.com")
                            Catch ex As Exception
                            End Try
                            Response.Redirect("common/loginwarning.aspx?un=" & txtUsername.Text.Trim(), False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                            m_bIsTerminating = True
                            Exit Sub
                        Case 0
                            ViewState("attempt") = CInt(ViewState("attempt")) + 1
                            If OASISConstants.MAX_PASSWORDTRY = ViewState("attempt") Then
                                lblResult.Text = lblResult.Text & "<br />Careful!!! Last Attempt"
                            ElseIf ViewState("attempt") < OASISConstants.MAX_PASSWORDTRY Then
                                lblResult.Text = lblResult.Text & "<br />Attempt #" & ViewState("attempt")
                            ElseIf ViewState("attempt") > OASISConstants.MAX_PASSWORDTRY Then
                                Response.Redirect("common/loginwarning.aspx?un=" & txtUsername.Text.Trim(), False)
                                HttpContext.Current.ApplicationInstance.CompleteRequest()
                                m_bIsTerminating = True
                                DisableUserLogin()
                                Exit Sub
                            Else
                                lblResult.Text = lblResult.Text & "<br />Attempt #" & CInt(ViewState("attempt"))
                            End If
                        Case Else
                            ViewState("attempt") = CInt(ViewState("attempt")) + 1
                            If OASISConstants.MAX_PASSWORDTRY = ViewState("attempt") Then
                                lblResult.Text = lblResult.Text & "<br />Careful!!!Last Attempt"
                            ElseIf ViewState("attempt") < OASISConstants.MAX_PASSWORDTRY Then
                                lblResult.Text = lblResult.Text & "<br />Attempt #" & CInt(ViewState("attempt"))
                            ElseIf ViewState("attempt") > OASISConstants.MAX_PASSWORDTRY Then
                                Response.Redirect("common/loginwarning.aspx?un=" & txtUsername.Text.Trim(), False)
                                HttpContext.Current.ApplicationInstance.CompleteRequest()
                                m_bIsTerminating = True
                                DisableUserLogin()
                                Exit Sub
                            Else
                                lblResult.Text = lblResult.Text & "<br />Attempt #" & CInt(ViewState("attempt"))
                            End If
                    End Select
                End If
            Catch ex As Exception
                stTrans.Rollback()
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try

            password = passwordEncr.Decrypt(password.Replace(" ", "+"))
            'check for  password matches
            If password = tblpassword Then
                'if valid hold the required information into the session
                Session("sUsr_id") = tblbUsr_id
                'to used for all the page
                Session("sUsr_name") = tblusername
                Session("sUsr_Display_Name") = tblDisplay_usr
                Session("sroleid") = tblroleid
                Session("sBusper") = tblbsuper
                Session("sBsuid") = tblbsuid
                Session("sBITSupport") = tblbITSupport
                Session("BSU_Name") = tblBSU_Name
                Session("sModuleAccess") = tblModuleAccess
                Session("sReqRole") = tblReqRole
                Session("EmployeeId") = tblEMPID
                Session("counterId") = USR_FCM_ID

                Session("CostEMP") = tblCOSTEMP
                Session("CostSTU") = tblCOSTSTU
                Session("CostOTH") = tblCOSTOTH
                Session("sModule") = tbldef_module
                'insert the session id of the cuurently logged in user
                Session("Sub_ID") = "007"

                Session("ForcePwdChange") = tblUSR_ForcePwdChange
                If Session("ForcePwdChange") = 1 Then
                    Response.Redirect("Forcepasswordchange.aspx")
                End If


                If Request.QueryString("hd") = 1 Then
                    Session("hdv2") = "1"
                    Session("ActiveTab") = Request.QueryString("atab")
                    Session("Task_ID") = Request.QueryString("tlid")
                    Response.Redirect("~\HelpDesk\Version2\Pages\hdTabs.aspx")

                End If
                If Request.QueryString("ss") = 1 Then
                    Dim url As String
                    url = String.Format("homepageSS.aspx")
                    Session("sModule") = "SS"
                    getSettings(Session("sBsuid"))
                    Response.Redirect(url)
                End If
                Using RoleReader As SqlDataReader = AccessRoleUser.GetRoles_modules(tblroleid)
                    While RoleReader.Read
                        Session("ROL_MODULE_ACCESS") = Convert.ToString(RoleReader("ROL_MODULE_ACCESS"))
                    End While
                End Using

                'if success write into the table users_m put the sessionID in  usr_session
                Using collectReader As SqlDataReader = AccessRoleUser.getCollectBank(tblbsuid)
                    While collectReader.Read
                        Session("CollectBank") = Convert.ToString(collectReader("CollectBank"))
                        Session("Collect_name") = Convert.ToString(collectReader("Collect_name"))
                    End While
                End Using
                Using CurriculumReader As SqlDataReader = AccessStudentClass.GetCURRICULUM_M_ID(tblbsuid)
                    While CurriculumReader.Read
                        Session("CLM") = Convert.ToString(CurriculumReader("BSU_CLM_ID"))
                    End While
                End Using
                SessionFlag = AccessRoleUser.UpdateSessionID(tblusername, Session.SessionID)
                If SessionFlag <> 0 Then
                    Throw New ArgumentException("Unable to track your request")
                End If

                Using UserreaderFINANCIALYEAR As SqlDataReader = AccessRoleUser.GetFINANCIALYEAR()
                    While UserreaderFINANCIALYEAR.Read
                        'handle the null value returned from the reader incase  convert.tostring
                        var_F_Year_ID = Convert.ToString(UserreaderFINANCIALYEAR("FYR_ID"))
                        var_F_Year = Convert.ToString(UserreaderFINANCIALYEAR("FYR_Descr"))
                        var_F_Date = Convert.ToString(UserreaderFINANCIALYEAR("FYR_TODT"))
                    End While
                    'clear of the resource end using
                End Using
                Session("F_YEAR") = var_F_Year_ID
                Session("F_Descr") = var_F_Year
                Session("F_TODT") = Format(var_F_Date, OASISConstants.DateFormat)
                Dim OasisSettingsCookie As HttpCookie
                If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
                    OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
                Else
                    OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
                End If
                OasisSettingsCookie.Values("UsrName") = txtUsername.Text
                OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
                Response.Cookies.Add(OasisSettingsCookie)
                If tblDays <= 0 Then
                    hfExpired.Value = "expired"
                    Exit Sub
                Else
                    hfExpired.Value = ""
                End If

                Dim auditFlag As Integer = UtilityObj.operOnAudiTable("Login", "login", "Login", Page.User.Identity.Name.ToString)
                If auditFlag <> 0 Then
                    Throw New ArgumentException("Unable to track your request")
                End If
                'if super admin allow all businessunit access
                Session("USR_MIS") = bUSR_MIS
                Session("USR_bShowMISonLogin") = USR_bShowMISonLogin

                If USR_bShowMISonLogin = 1 Then
                    Response.Redirect("~/Management/empManagementMain.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    m_bIsTerminating = True
                    Exit Sub
                ElseIf USR_bShowMISonLogin = 2 Then
                    Response.Redirect("~/Management/empManagementPrincipal.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    m_bIsTerminating = True
                    Exit Sub
                End If
                If tblbsuper = True Then

                    ' Response.Redirect("BusinessUnit.aspx")
                    Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(tblbsuid)
                        While BSUInformation.Read
                            Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                            Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                            Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                            Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                            Session("BSU_bMOE_TC") = Convert.ToString(BSUInformation("BSU_bMOE_TC"))
                            Session("BSU_bVATEnabled") = Convert.ToBoolean(BSUInformation("BSU_bVATEnabled"))
                        End While
                    End Using
                    Session("tot_bsu_count") = "1"
                    getSettings(Session("sBsuid"))
                    Response.Redirect("modulelogin.aspx", False)
                    'HttpContext.Current.ApplicationInstance.CompleteRequest()
                    'm_bIsTerminating = True
                    Exit Sub

                Else
                    'if user with single business unit direct the user to module login page else
                    'allow the user to select the business unit
                    Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                        While BUnitreader.Read
                            bCount += 1
                            If bCount = 1 Then
                                Session("BSU_ROUNDOFF") = Convert.ToInt32(BUnitreader("Roundoff"))
                                Session("BSU_CURRENCY") = Convert.ToString(BUnitreader("BSU_CURRENCY"))
                                Session("BSU_PAYYEAR") = Convert.ToInt32(IIf(IsDBNull(BUnitreader("BSU_PAYYEAR")) = True, Session("F_YEAR"), BUnitreader("BSU_PAYYEAR")))
                                Session("BSU_PAYMONTH") = Convert.ToInt32(IIf(IsDBNull(BUnitreader("BSU_PAYYEAR")) = True, Month(Date.Today), BUnitreader("BSU_PAYMONTH")))
                                Session("BSU_bVATEnabled") = Convert.ToBoolean(BUnitreader("BSU_bVATEnabled"))
                                Session("BSU_IsOnDAX") = Convert.ToInt32(BUnitreader("BSU_IsOnDAX"))
                                Session("BSU_IsHROnDAX") = Convert.ToInt32(BUnitreader("BSU_IsHROnDAX"))
                            End If
                        End While
                    End Using

                    Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(tblbsuid)
                        While BSUInformation.Read
                            Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                            Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                            Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                            Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                            Session("BSU_bMOE_TC") = Convert.ToString(BSUInformation("BSU_bMOE_TC"))
                        End While
                    End Using
                End If

                getSettings(Session("sBsuid"))
                Response.Redirect("Modulelogin.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                m_bIsTerminating = True
                Exit Sub
                'check the number of business unit for the current user
                'If bCount > 1 Then
                '    Session("tot_bsu_count") = "1"
                '    Response.Redirect("BusinessUnit.aspx")
                '    HttpContext.Current.ApplicationInstance.CompleteRequest()
                '    m_bIsTerminating = True
                '    Exit Sub
                'Else
                '    getSettings(Session("sBsuid"))
                '    'Dim auditFlag As Integer = UtilityObj.operOnAudiTable("Login", "login", "Login", Page.User.Identity.Name.ToString)
                '    'If auditFlag <> 0 Then
                '    '    Throw New ArgumentException("Unable to track your request")
                '    'End If
                '    Session("tot_bsu_count") = "0"
                '    If ((Session("sroleid") = "46") Or (Session("sroleid") = "47")) Then
                '        Session("sModule") = "E0"
                '        Response.Redirect("~\eduShield\eduShield_Home.aspx", False)
                '    Else
                '        Response.Redirect("Modulelogin.aspx", False)
                '    End If

                '    HttpContext.Current.ApplicationInstance.CompleteRequest()
                '    m_bIsTerminating = True
                '    Exit Sub
                'End If
            Else
                If lblResult.Text = "" Then
                    lblResult.Text = "Invalid username or password"
                End If
            End If
        Catch sqlEx As SqlException
            lblResult.Text = "Unable to process your request"
            UtilityObj.Errorlog(sqlEx.Message, "Login1")
        Catch generalEx As Exception
            lblResult.Text = "Invalid username or password"
            UtilityObj.Errorlog(generalEx.Message, "Login2")
        End Try
    End Sub

   
End Class