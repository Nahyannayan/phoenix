<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptGetMISPnL.aspx.vb" Inherits="Reports_ASPX_Report_rptGetMISPnL" Title="Untitled Page" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Label"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="dayBook" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" width="30%">
                            <div class="checkbox-list">
                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                            </div>
                        </td>
                        <td align="left" width="20%"><span class="field-label">To Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="dayBook">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>

                    <tr runat="server" id="tr_CompareDate" visible="false">
                        <td align="left" width="20%"><span class="field-label">Compare </span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCompareDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgCompareDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%">
                            <asp:CheckBox ID="chkTrend" runat="server" Text="Trend Report" CssClass="field-label" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Express In</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddExpressedin" runat="server">
                                <asp:ListItem Value="-1">Actual</asp:ListItem>
                                <asp:ListItem>0</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>100</asp:ListItem>
                                <asp:ListItem>1000</asp:ListItem>
                                <asp:ListItem>1000000</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%">
                            <asp:DropDownList ID="ddGroupby" Width="100%" runat="server" Visible="False">
                                <asp:ListItem Value="BSU">Group by Business Unit</asp:ListItem>
                                <asp:ListItem Value="SEG">Group by Segment</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="30%">
                            <asp:CheckBox ID="chkExcludeSpecialAccounts" runat="server"
                                Text="Exclude Special Accounts" Visible="False" CssClass="field-label"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <asp:Label ID="lblType" runat="server" Text="Type" Visible="False" CssClass="field-label"></asp:Label>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddType" runat="server"
                                Visible="False">
                                <asp:ListItem Value="MP">P&amp;L</asp:ListItem>
                                <asp:ListItem Value="MB">BS</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="dayBook" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtToDate" PopupButtonID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate3" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgCompareDate" TargetControlID="txtCompareDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate4" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtCompareDate" PopupButtonID="txtCompareDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

