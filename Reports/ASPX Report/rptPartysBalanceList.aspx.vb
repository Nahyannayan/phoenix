Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_PartysBalanceList
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64 
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Page.Title = OASISConstants.Gemstitle
                UtilityObj.NoOpen(Me.Header)
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                txtFromDate.Text = UtilityObj.GetDiplayDate()

                txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
                radSummary.Attributes.Add("onclick", "EnableControls();")
                radConsolidation.Attributes.Add("onclick", "EnableControls();")
            End If

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            If MainMnu_code <> "A550014" And MainMnu_code <> "A550022" Then
                Response.Redirect("..\..\noAccess.aspx")
            Else
                Select Case MainMnu_code
                    Case "A550014"
                        lblrptCaption.Text = "Creditors Balance List"
                        h_Mode.Value = "s"
                    Case "A550022"
                        lblrptCaption.Text = "Debtors Balance List"
                        h_Mode.Value = "C"
                End Select
            End If
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            If h_BSUID.Value = Nothing Or h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
                h_BSUID.Value = Session("sBsuid")
            End If
            FillBSUNames(h_BSUID.Value)
            If h_ACTIDs.Value <> Nothing And h_ACTIDs.Value <> "" And h_ACTIDs.Value <> "undefined" Then
                FillACTIDs(h_ACTIDs.Value)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Try
            Dim IDs As String() = ACTIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdACTDetails.DataSource = ds
            grdACTDetails.DataBind()
            'dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            'txtBankNames.Text = ""
            'Dim bval As Boolean = dr.Read
            'While (bval)
            '    txtBankNames.Text += dr(0).ToString()
            '    bval = dr.Read()
            '    If bval Then
            '        txtBankNames.Text += "||"
            '    End If
            'End While
            'txtbankCodes.Text = ACTIDs
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.ACTName
                elements(0) = "ACT_DETAILS"
                elements(1) = "ACT_DETAIL"
                elements(2) = "ACT_ID"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                XMLEBSUID = xmlDoc.CreateElement(elements(2))
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Select Case MainMnu_code
            Case "A550014"
                If radConsolidation.Checked Then
                    h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
                    Dim strXMLBSUNames = GenerateXML(h_BSUID.Value, XMLType.BSUName) 'txtBSUNames.Text)
                    GenerateAgingConsolidation(strXMLBSUNames)
                Else
                    GetPartyBalanceList()
                End If
            Case "A550022"
                If radConsolidation.Checked Then
                    h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
                    Dim strXMLBSUNames = GenerateXML(h_BSUID.Value, XMLType.BSUName) 'txtBSUNames.Text)
                    GenerateAgingConsolidation(strXMLBSUNames)
                Else
                    GetPartyBalanceList()
                End If
        End Select
    End Sub

    Private Sub GenerateAgingConsolidation(ByVal strXMLBSUNames As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand

        'cmd = New SqlCommand("RPT_AGING_CONSOLIDATED", objConn)
        Select Case MainMnu_code
            Case "A550014"
                cmd = New SqlCommand("RPT_AGING_CONSOLIDATED")
                cmd.CommandType = CommandType.StoredProcedure
            Case "A550022"
                cmd = New SqlCommand("RPT_DEBITOR_AGING_CONSOLIDATED")
                cmd.CommandType = CommandType.StoredProcedure
        End Select

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@TRN_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        sqlpACT_IDs.Value = GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
        cmd.Parameters.Add(sqlpACT_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        Dim sqlpVouchDT As New SqlParameter("@bVouchDt", SqlDbType.Bit)
        sqlpVouchDT.Value = False
        cmd.Parameters.Add(sqlpVouchDT)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("AsOnDate") = txtFromDate.Text
        Select Case MainMnu_code
            Case "A550014"
                params("RptTitle") = "CREDITORS BALANCE LIST"
            Case "A550022"
                params("RptTitle") = "DEBTORS BALANCE LIST"
        End Select
        repSource.Parameter = params
        repSource.Command = cmd
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        If radFormat1.Checked Then
            repSource.ResourceName = "../RPT_Files/rptAgingConsolidationReport.rpt"
        Else
            repSource.ResourceName = "../RPT_Files/rptAgingConsolidationReport_Format2.rpt"
        End If

        Session("ReportSource") = repSource
        'Context.Items.Add("ReportSource", repSource)
        objConn.Close()
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("rptviewer.aspx?isExport=true", True)
        Else
            'Response.Redirect("rptviewer.aspx", True)
            ReportLoadSelection()
        End If
        'Else
        'lblError.Text = "No Records with specified condition"
        'End If
        objConn.Close()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub GetPartyBalanceList()
        Try
            Dim str_sql As String = String.Empty
            Dim fromDate As String
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim strFilter As String = String.Empty

            If (txtFromDate.Text <> "") And (txtFromDate.Text <> String.Empty) Then
                fromDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", txtFromDate.Text)
            Else
                fromDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", DateTime.MinValue)
            End If

            ''SELECT * FROM vw_OSA_JOURNAL where FYEAR, DOCTYPE, DOCNO, DOCDT
            strFilter = " TRN_DOCDT <= '" & fromDate & "'"
            Dim strAND As String = String.Empty
            h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
            If h_BSUID.Value <> "" Then
                strFilter += " AND TRN_BSU_ID in ("

                Dim comma As String = String.Empty
                Dim strArrayBSUID As String() = h_BSUID.Value.Split("||")
                Dim strBSUID As String = String.Empty
                For count As Integer = 0 To strArrayBSUID.Length - 1
                    If strArrayBSUID(count) <> "" Then
                        strBSUID += comma
                        strBSUID += "'" & strArrayBSUID(count) & "'"
                    End If
                    comma = ","
                Next
                strFilter += strBSUID + ")"
                strAND = " AND "
            End If
            If h_ACTIDs.Value <> "" Then
                strFilter += strAND + " ACT_ID in ("
                Dim strArrayACTID As String() = h_ACTIDs.Value.Split("||")
                Dim strACTID As String = String.Empty
                For count As Integer = 0 To strArrayACTID.Length - 1
                    If count <> 0 And Not strACTID.EndsWith(", ") Then
                        strACTID += ", "
                    End If
                    If strArrayACTID(count) <> "" Then
                        strACTID += "'" + strArrayACTID(count) + "'"
                    End If
                Next
                strFilter += strACTID + ")"
            End If
            Select Case MainMnu_code
                Case "A550014"
                    str_sql = "select * from vw_PArtysBalanceList WHERE " + strFilter
                Case "A550022"
                    str_sql = "select * from vw_DebitorsBalanceList WHERE " + strFilter
            End Select
            'str_sql = "select * from vw_PArtysBalanceList WHERE " + strFilter
            ' TRN_BSU_ID IN('125016') AND ACT_ID IN('06101002') ORDER BY TRN_DOCDT"
            'str_Sql = "SELECT * FROM vw_OSA_VOUCHER where VHH_TYPE = 'P' AND VHH_bPosted = 'true' " + strFilter
            'Dim ds As New DataSet
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            'If SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_sql, Nothing) IsNot Nothing Then
            If 1 = 1 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_sql
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("GroupCurrency") = chkGroupCurrency.Checked
                'params("fromDate") = txtFromDate.Text
                params("fromDate") = fromDate
                'params("Month") = "January"
                'params("reportHeading") = "Disbursement Account"
                'params("toDate") = toDate
                params("decimal") = Session("BSU_ROUNDOFF")
                Select Case MainMnu_code
                    Case "A550014"
                        params("RptTitle") = "CREDITORS BALANCE LIST"
                    Case "A550022"
                        params("RptTitle") = "DEBTORS BALANCE LIST"
                End Select
                repSource.Parameter = params
                repSource.Command = cmd
                If radSummary.Checked Then
                    repSource.ResourceName = "../RPT_Files/rptPartyBalanceList_Summary.rpt"
                Else
                    repSource.ResourceName = "../RPT_Files/rptPartyBalanceList.rpt"
                End If
                Session("ReportSource") = repSource
                If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                    Response.Redirect("rptviewer.aspx?isExport=true", True)
                Else
                    'Response.Redirect("rptviewer.aspx", True)
                    ReportLoadSelection()
                End If
            Else
                lblError.Text = "No Records with specified condition"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
        str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        Else
            If ds.Tables(0).Rows.Count > 1 Then
                chkGroupCurrency.Checked = True
            Else
                chkGroupCurrency.Checked = False
            End If
        End If
        'grdBSU.DataSource = ds
        'grdBSU.DataBind()
        'txtBSUName.Text = ""
        'Dim bval As Boolean = dr.Read
        'While (bval)
        '    txtBSUName.Text += dr(0).ToString()
        '    bval = dr.Read()
        '    If bval Then
        '        txtBSUName.Text += "||"
        '    End If
        'End While
        Return True
    End Function

    'Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
    '    h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
    '    FillBSUNames(h_BSUID.Value)
    'End Sub

    'Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
    '    grdBSU.PageIndex = e.NewPageIndex
    '    FillBSUNames(h_BSUID.Value)
    'End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            'grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(h_ACTIDs.Value)
        End If

    End Sub

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdACTDetails.PageIndexChanging
        grdACTDetails.PageIndex = e.NewPageIndex
        FillBSUNames(h_ACTIDs.Value)
    End Sub

    Protected Sub lblAddActID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddActID.Click
         h_ACTIDs.Value += "||" + txtBankNames.Text.Replace(",", "||")
         FillACTIDs(h_ACTIDs.Value)
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)

    End Sub
End Class