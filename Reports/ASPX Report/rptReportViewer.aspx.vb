Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.IO
Imports system.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections
Imports System.Web.Configuration

Partial Class Reports_ASPX_Report_rptReportViewer
    Inherits System.Web.UI.Page

    Dim rs As New ReportDocument

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Dim smScriptManager As New ScriptManager
        'smScriptManager = Master.FindControl("ScriptManager1")

        'smScriptManager.EnablePartialRendering = False


        Try
            'rs.CacheDuration = 1
            'rs.EnableCaching = False
            'If Request.Browser.Type = "InternetExplorer10" Then
            '    'If Request.Browser.Version = "10" Then
            '    Dim meta As New HtmlMeta
            '    meta.HttpEquiv = "X-UA-Compatible"
            '    meta.Content = "IE=EmulateIE9"
            '    Page.Header.Controls.AddAt(1, meta)
            '    Page.Header.Controls.RemoveAt(2)
            '    Page.Header.Controls.RemoveAt(3)
            '    'End If
            'End If
            LoadReports(sender, e)
        Catch ex As Exception
        End Try
    End Sub


    'Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
    'Using msOur As New System.IO.MemoryStream()
    '    Using swOur As New System.IO.StreamWriter(msOur)
    '        Dim ourWriter As New HtmlTextWriter(swOur)
    '        MyBase.Render(ourWriter)
    '        ourWriter.Flush()
    '        msOur.Position = 0
    '        Using oReader As New System.IO.StreamReader(msOur)
    '            Dim sTxt As String = oReader.ReadToEnd()
    '            If Web.Configuration.WebConfigurationManager.AppSettings("AlwaysSSL").ToLower = "true" Then
    '                sTxt = sTxt.Replace(Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString(), _
    '                Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString().Replace("http://", "https://"))
    '            End If
    '            Response.Write(sTxt)
    '            oReader.Close()
    '        End Using
    '    End Using
    'End Using
    '  End Sub




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Try
        '    'rs.CacheDuration = 1
        '    'rs.EnableCaching = False
        '    UtilityObj.Errorlog("reportpageload--")
        '    LoadReports(sender, e)
        'Catch ex As Exception
        'End Try


    End Sub

    Sub LoadReports(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rptClass As New rptClass
        Try
            '' UtilityObj.Errorlog("LoadReports1--")
            '  rs = New ReportDocument


            Dim iRpt As New DictionaryEntry
            Dim i As Integer
            Dim newWindow As String

            Try
                newWindow = Request.QueryString("newWindow")
            Catch ex As Exception

            End Try
            Dim rptStr As String = ""
            If Not newWindow Is Nothing Then
                rptStr = "rptClass" + newWindow
            Else
                rptStr = "rptClass"
            End If


            ' rptClass = Session("rptClass")
            rptClass = Session.Item(rptStr)

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues

            With rptClass

                rs.Load(.reportPath)
                ' rs.DataSourceConnections(0).SetConnection(.crInstanceName, .crDatabase, .crUser, .crPassword)
                ' crv.ReportSource = rs
                ''   UtilityObj.Errorlog("LoadReports2--")


                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword

                Try
                    ''  UtilityObj.Errorlog("LoadReports3--")
                    SetDBLogonForSubreports(myConnectionInfo, rs, .reportParameters)
                    SetDBLogonForReport(myConnectionInfo, rs, .reportParameters)
                Catch ex As Exception
                    UtilityObj.Errorlog("1--" & (ex.Message))
                    'lblRepInfo.Text = "1--" & (ex.Message)
                End Try




                Try
                    crParameterFieldDefinitions = rs.DataDefinition.ParameterFields
                    If .reportParameters.Count <> 0 Then
                        ''  UtilityObj.Errorlog("LoadReports4--")
                        For Each iRpt In .reportParameters
                            crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                            crParameterValues = crParameterFieldLocation.CurrentValues
                            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                            crParameterDiscreteValue.Value = iRpt.Value
                            crParameterValues.Add(crParameterDiscreteValue)
                            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                        Next
                    End If

                    ChangeFont(rs)
                Catch ex As Exception
                    UtilityObj.Errorlog("2--" & (ex.Message))
                    'lblRepInfo.Text = "2--" & (ex.Message)
                End Try

                If .selectionFormula <> "" Then

                    rs.RecordSelectionFormula = .selectionFormula
                End If


            End With


        Catch ex As Exception
            UtilityObj.Errorlog("load--" & rptClass.reportPath & "," & (ex.Message))
            'lblRepInfo.Text = "load--" & (ex.Message)

        Finally
            crv.ReportSource = rs

        End Try
    End Sub

    Sub ChangeFont(rs As ReportDocument)

        Dim crSection As CrystalDecisions.CrystalReports.Engine.Section
        Dim crObject As CrystalDecisions.CrystalReports.Engine.ReportObject

        For Each crSection In rs.ReportDefinition.Sections
            For Each crObject In crSection.ReportObjects
                If crObject.Kind = ReportObjectKind.TextObject Then
                    Dim txtObj As CrystalDecisions.CrystalReports.Engine.TextObject = CType(crObject, CrystalDecisions.CrystalReports.Engine.TextObject)
                    txtObj.ApplyFont(New Drawing.Font(txtObj.Font.Name, txtObj.Font.Size - 1, txtObj.Font.Style))
                ElseIf crObject.Kind = ReportObjectKind.FieldObject Then
                    Dim fobj As CrystalDecisions.CrystalReports.Engine.FieldObject = CType(crObject, CrystalDecisions.CrystalReports.Engine.FieldObject)
                    fobj.ApplyFont(New Drawing.Font(fobj.Font.Name, fobj.Font.Size - 1, fobj.Font.Style))
                ElseIf crObject.Kind = ReportObjectKind.CrossTabObject Then
                    Dim ctobj As CrystalDecisions.CrystalReports.Engine.CrossTabObject = CType(crObject, CrystalDecisions.CrystalReports.Engine.CrossTabObject)
                    ctobj.ObjectFormat.EnableCanGrow = True

                End If

            Next
        Next
    End Sub


    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            Dim crParameterDiscreteValue As ParameterDiscreteValue
            'Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            'Dim crParameterFieldLocation As ParameterFieldDefinition
            'Dim crParameterValues As ParameterValues
            'Dim iRpt As New DictionaryEntry

            ' Dim myTableLogonInfo As TableLogOnInfo
            ' myTableLogonInfo = New TableLogOnInfo
            'myTableLogonInfo.ConnectionInfo = myConnectionInfo

            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

            Next


            'crParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
            'If reportParameters.Count <> 0 Then
            '    For Each iRpt In reportParameters
            '        Try
            '            crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
            '            crParameterValues = crParameterFieldLocation.CurrentValues
            '            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
            '            crParameterDiscreteValue.Value = iRpt.Value
            '            crParameterValues.Add(crParameterDiscreteValue)
            '            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
            '        Catch ex As Exception
            '        End Try
            '    Next
            'End If

            'myReportDocument.DataSourceConnections(0).SetConnection(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
            'myReportDocument.SetDatabaseLogon(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '
            If myConnectionInfo.ServerName.ToString <> "172.25.29.51" Then
                Try
                    myReportDocument.VerifyDatabase()
                Catch ex As Exception
                    UtilityObj.Errorlog("verifydb--" & ex.Message)
                    'lblRepInfo.Text = "verifydb--" & (ex.Message)
                End Try

            End If

        Catch ex As Exception

            UtilityObj.Errorlog("final--" & (ex.Message))
            lblRepInfo.Text = "final--" & (ex.Message)
        End Try

    End Sub
    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

                    Select Case subReportDocument.Name
                        Case "rptTransportStudPhoto.rpt"
                            Dim dS As New dsImageForTransport
                            '<add name="EmpFilepath" connectionString="\\172.16.1.11\OASISPHOTOS\OASIS_HR\ApplicantPhoto"/>
                            Dim str_noimg As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
                            str_noimg = str_noimg + "\NOIMG\no_image.jpg"
                            If Session("transportFileName") = "" Or Session("transportFileName").Contains("no_image") Then
                                Session("transportFileName") = str_noimg
                            End If
                            Dim fs As New FileStream(Session("transportFileName"), System.IO.FileMode.Open, System.IO.FileAccess.Read)
                            Dim Image As Byte() = New Byte(fs.Length - 1) {}
                            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
                            fs.Close()
                            Dim dr As dsImageForTransport.PHOTOFORTRANSPORTRow = dS.PHOTOFORTRANSPORT.NewPHOTOFORTRANSPORTRow
                            dr.Image = Image
                            'Add the new row to the dataset
                            dS.PHOTOFORTRANSPORT.Rows.Add(dr)
                            subReportDocument.SetDataSource(dS)
                        Case "studphotos"
                            Dim dS As New dsImageRpt
                            Dim fs As New FileStream(Session("StudentPhotoPath"), System.IO.FileMode.Open, System.IO.FileAccess.Read)
                            Dim Image As Byte() = New Byte(fs.Length - 1) {}
                            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
                            fs.Close()
                            Dim dr As dsImageRpt.StudPhotoRow = dS.StudPhoto.NewStudPhotoRow
                            dr.Photo = Image
                            'Add the new row to the dataset
                            dS.StudPhoto.Rows.Add(dr)
                            subReportDocument.SetDataSource(dS)
                        Case "rptSubPhoto.rpt"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetPhotoToReport(subReportDocument, vrptClass.Photos)
                            End If
                        Case "rptGWAElmtSub", "rptWSOInterimSub", "rptCISImageSub", "rptWINImageSub", "rptWSSImageSub", "rptWISImageSub"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetGWAPhotoToReport(subReportDocument, vrptClass.Photos, reportParameters)
                            End If
                        Case Else
                            SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    End Select

                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub

    Private Sub SetPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos)
        Dim arrphotos As ArrayList = vPhotos.IDs
        vPhotos = UpdatePhotoPath(vPhotos)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt64(fs.Length))
            Catch ex As Exception
                Dim ss As String = ex.Message
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.DSPhotosRow = dS.DSPhotos.NewDSPhotosRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.DSPhotos.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub

    Function UpdatePhotoPath(ByVal vPhotos As OASISPhotos) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = Web.Configuration.WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString()  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStud_photoPath(vPhotos.IDs) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        If Session("sbsuid") = "125017" Then
                            If Not Session("noimg") Is Nothing Then
                                vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                            Else
                                vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/NOIMG/no_img_gwa.jpg")
                            End If

                        ElseIf Session("sbsuid") = "114003" Then
                            vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                        Else
                            vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + "/NOIMG/no_image.jpg"
                        End If
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function

    Private Function GETStud_photoPath(ByVal arrSTU_IDs As ArrayList) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FILE_PATH,STU_ID FROM STUDENT_M where  STU_ID in(" & strStudIDs & ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function



    Function UpdateGWAPhotoPath(ByVal vPhotos As OASISPhotos, ByVal RPF_ID As String) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = ConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStudGWA_photoPath(vPhotos.IDs, RPF_ID) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function


    Private Function GETStudGWA_photoPath(ByVal arrSTU_IDs As ArrayList, ByVal RPF_ID As String) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        'Dim sqlString As String = "SELECT '/125017/'+SFU_GRD_ID+'_'+RIGHT(ACY_DESCR,2)+'_'+CONVERT(VARCHAR(100),SFU_RPF_ID)+'\'+SFU_FILEPATH AS FILE_PATH,SFU_STU_ID AS STU_ID" _
        '                                & " FROM CURR.STUDENT_FILEUPLOAD INNER JOIN VW_ACADEMICYEAR_D ON SFU_ACD_ID=ACD_ID " _
        '                                & " INNER JOIN VW_ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID WHERE SFU_STU_ID IN " _
        '                                & "(" + strStudIDs + ") AND SFU_RPF_ID=" + RPF_ID


        Dim sqlString As String = "SELECT CASE WHEN ISNULL(SFU_FILEPATH,'')='' THEN '' ELSE '\" + Session("sbsuid") + "\'+SFU_GRD_ID+'_'+RIGHT(ACY_DESCR,2)+'_'+CONVERT(VARCHAR(100),SFU_RPF_ID)+'\'+SFU_FILEPATH END AS FILE_PATH " _
                                    & " ,STU_ID" _
                                    & " FROM STUDENT_M LEFT OUTER JOIN CURR.STUDENT_FILEUPLOAD ON SFU_STU_ID=STU_ID AND SFU_RPF_ID=" + RPF_ID _
                                    & " LEFT OUTER JOIN VW_ACADEMICYEAR_D ON SFU_ACD_ID=ACD_ID " _
                                    & " INNER JOIN VW_ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID WHERE STU_ID IN " _
                                    & "(" + strStudIDs + ")"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function


    Private Sub SetGWAPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos, ByVal param As Hashtable)
        Dim arrphotos As ArrayList = vPhotos.IDs
        Dim RPF_ID As String = param.Item("@RPF_ID")
        vPhotos = UpdateGWAPhotoPath(vPhotos, RPF_ID)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch ex As Exception
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.dsGWAImageRow = dS.dsGWAImage.NewdsGWAImageRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.dsGWAImage.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub


    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        'Try
        '    'crv.Dispose()
        '    'crv = Nothing
        '    'rs.Dispose()
        '    ' rs = Nothing
        '    'GC.Collect()

        '    Try
        '        If Not rs Is Nothing Then
        '            rs.Dispose()
        '        End If
        '    Catch ex As Exception
        '        UtilityObj.Errorlog("load reports finally dispose--" & (ex.Message))
        '    End Try
        'Catch ex As Exception
        'End Try
    End Sub

    Protected Sub crv_Unload(sender As Object, e As EventArgs) Handles crv.Unload
        Try

            Try
                For Each table As Table In rs.Database.Tables
                    table.Dispose()
                Next
                rs.Database.Dispose()
            Catch ex As Exception
                UtilityObj.Errorlog("crv_Unload--", ex.Message)
            End Try

            rs.Close()

            rs.Dispose()

            crv.Dispose()
            'GC.WaitForPendingFinalizers()
            'GC.Collect()


        Catch ex As Exception

        End Try
    End Sub
End Class
