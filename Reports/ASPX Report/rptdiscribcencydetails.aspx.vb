Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO


Partial Class Students_Reports_ASPX_rptdiscribcencydetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.Title = "Discrepancy Details"
        describcencydetails()

    End Sub
    Protected Sub describcencydetails()
        Dim TRN_ID As String = IIf(Request.QueryString("TRN_ID") <> Nothing, Request.QueryString("TRN_ID"), String.Empty)
        Dim BSU_ID As String = IIf(Request.QueryString("BSU_ID") <> Nothing, Request.QueryString("BSU_ID"), String.Empty)

        ViewState("TRN_ID") = TRN_ID
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim parm(8) As SqlClient.SqlParameter

        parm(0) = New SqlClient.SqlParameter("@STA_BSU_ID", BSU_ID)
        parm(1) = New SqlClient.SqlParameter("@FromDate", Session("rptdesFROMDATE"))
        parm(2) = New SqlClient.SqlParameter("@ToDate", Session("rptdesTODATE"))
        parm(3) = New SqlClient.SqlParameter("@STA_TRN_ID", TRN_ID)

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "rptdiscribcency_details", parm)

        gvdiscribcency.DataSource = ds
        gvdiscribcency.DataBind()

    End Sub

    Protected Sub gvdiscribcency_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvdiscribcency.PageIndexChanging

        gvdiscribcency.PageIndex = e.NewPageIndex
        describcencydetails()

    End Sub
 
End Class
