Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_rptPrepaymentSummary
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If
        Session("user") = Session("sUsr_name")
        Page.Title = OASISConstants.Gemstitle
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        UsrBSUnits1.MenuCode = MainMnu_code
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            lblError.Text = ""
        End If
        txtExpActID.Attributes.Add("ReadOnly", "ReadOnly")
        txtPrActID.Attributes.Add("ReadOnly", "ReadOnly")
        txtAccountName.Attributes.Add("ReadOnly", "ReadOnly")
        txtBankName.Attributes.Add("ReadOnly", "ReadOnly")
        'txtToDate.Attributes.Add("onBlur", "checkdate(this)")
        'txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
        If Not IsPostBack Then
            UtilityObj.NoOpen(Me.Header)
            h_BSUID.Value = IIf(Session("SBSUID") <> "", Session("SBSUID").ToString(), String.Empty)
            txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
            txtToDate.Attributes.Add("onBlur", "checkdate(this)")
        End If
        If h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
            h_BSUID.Value = Session("sBsuid")
        End If
        FillBSUNames(h_BSUID.Value)
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        'grdBSU.DataSource = ds
        'grdBSU.DataBind()
        Return True
    End Function

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = Session("sBSUID")
            End If
            'grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim str_Sql, strFilter As String
        strFilter = String.Empty
        If ((txtFromDate.Text <> "") And (txtFromDate.Text <> String.Empty)) Or _
        ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then
            InitializeFilter(strFilter)
            strFilter += " DOCDT BETWEEN '" + String.Format("{0:dd/MMM/yyyy}", CDate(txtFromDate.Text)) + "' AND '" + String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text)) + "'"
        End If
        InitializeFilter(strFilter)
        Dim rStatus As Status
        rStatus = GetReportStatus()
        Select Case rStatus
            Case Status.ALL
                strFilter += " bDELETED = 'FALSE'"
            Case Status.Deleted
                strFilter += " bDELETED = 'TRUE'"
            Case Status.Open
                strFilter += " bDELETED = 'FALSE' AND bPOSTED = 'FALSE'"
            Case Status.Posted
                strFilter += " bPOSTED = 'TRUE'"
        End Select

        If (txtBankName.Text <> "") And (txtBankName.Text <> String.Empty) Then
            InitializeFilter(strFilter)
            strFilter += " PreAcc = '" + txtPrActID.Text + "'"
        End If
        If (txtAccountName.Text <> "") And (txtAccountName.Text <> String.Empty) Then
            InitializeFilter(strFilter)
            strFilter += " ExpAcc = '" + txtExpActID.Text + "'"
        End If
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        If (h_BSUID.Value <> "") And (h_BSUID.Value <> String.Empty) Then
            InitializeFilter(strFilter)
            strFilter += " BSU_ID IN( "
            Dim comma As String = String.Empty
            Dim strArrayBSUID As String() = h_BSUID.Value.Split("||")
            Dim strBSUID As String = String.Empty
            For count As Integer = 0 To strArrayBSUID.Length - 1
                'If count <> 0 And Not strBSUID.EndsWith(", ") Then
                '    strBSUID += ", "
                'End If
                If strArrayBSUID(count) <> "" Then
                    strBSUID += comma
                    strBSUID += "'" & strArrayBSUID(count) & "'"
                End If
                comma = ","
            Next
            strFilter += strBSUID + ")"
        End If

        str_Sql = "SELECT * FROM vw_OSA_PREPAYMENTSUMMARY " + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        'Dim ds As New DataSet
        'SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
        'If SqlHelper.ExecuteScalar(str_conn, CommandType.Text, cmd.CommandText, Nothing) IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("UserName") = Session("sUsr_name")
            params("fromDate") = Format(CDate(txtFromDate.Text), "dd/MMM/yyyy")
            params("toDate") = Format(CDate(txtToDate.Text), "dd/MMM/yyyy")
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptPrepaymentSummary.rpt"
            Dim subRep(1) As MyReportClass
            subRep(0) = New MyReportClass
            Dim subcmd As New SqlCommand
            subcmd.CommandText = "SELECT * FROM PREPAYMENTS_D"
            subcmd.CommandType = Data.CommandType.Text
            subcmd.Connection = New SqlConnection(str_conn)
            subRep(0).Command = subcmd
            repSource.SubReport = subRep
            Session("ReportSource") = repSource
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                Response.Redirect("rptviewer.aspx", True)
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
    End Sub

    Private Sub InitializeFilter(ByRef filter As String)
        If filter IsNot String.Empty Or filter IsNot "" Then
            If Not filter.StartsWith(" WHERE") Then
                filter = filter.Insert(0, " WHERE")
            End If
            If Not filter.EndsWith("WHERE") Then
                filter = filter.Insert(filter.Length, " AND")
            End If
        End If
    End Sub

    Private Function GetReportStatus() As Status
        Dim rstatus As Status
        If radStatusAll.Checked = True Then
            rstatus = Status.ALL
        ElseIf radStatusOpen.Checked = True Then
            rstatus = Status.Open
        ElseIf radStatusPosted.Checked = True Then
            rstatus = Status.Posted
        ElseIf radStatusDeleted.Checked = True Then
            rstatus = Status.Deleted
        End If
        Return rstatus
    End Function

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

End Class
