Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_rptViewPostingDetails
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
         
        Page.Title = OASISConstants.Gemstitle
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If
        If Not IsPostBack Then

            PopulateDocType()
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            lblError.Text = ""
        End If

    End Sub

    Private Sub PopulateDocType()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            Dim dsDoc As New DataSet
            str_Sql = "SELECT distinct DOC_ID, DOC_NAME FROM DOCUMENT_M"
            dsDoc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If dsDoc Is Nothing Then
                Throw New Exception("DsDoc is null Throws ")
            ElseIf dsDoc.Tables.Count > 0 Then
                
                cmbDocType.DataSource = dsDoc.Tables(0)
                cmbDocType.DataValueField = "DOC_ID"
                cmbDocType.DataTextField = "DOC_NAME"
                cmbDocType.DataBind()
                cmbDocType.SelectedValue = "ALL"
            Else
                Throw New Exception("DsDoc.Table is less than 0 Throws Error in DayBook.cs function GetDataSourceDocType()")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim cmd As New SqlCommand
        Dim params As New Hashtable
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet

        Dim strFilter As String = String.Empty
        Dim strAND As String = String.Empty
        Dim strWhere As String = " WHERE "
        If cmbDocType.SelectedItem.Value <> "ALL" Then
            strFilter += strWhere + " JOURNAL_D.JNL_DOCTYPE ='" & cmbDocType.SelectedItem.Value & "'"
            strAND = " AND"
            strWhere = String.Empty
        End If
        strFilter += strWhere + strAND + " JOURNAL_D.JNL_DOCNO ='" & txtDocNo.Text & "'" & " AND JNL_BSU_ID = '" & Session("sBSUID") & "'"
        
        cmd.CommandText = "SELECT ACCOUNTS_M.ACT_ID, ACCOUNTS_M.ACT_NAME, JOURNAL_D.JNL_CHQNO, JOURNAL_D.JNL_CHQDT," & _
        " JOURNAL_D.JNL_CREDIT, JOURNAL_D.JNL_DEBIT, vw_OSO_BUSINESSUNIT_M.BSU_NAME, JOURNAL_D.JNL_DOCTYPE, " & _
        "JOURNAL_D.JNL_DOCDT, JOURNAL_D.JNL_NARRATION, JOURNAL_H.JHD_DOCNO" & _
        " FROM JOURNAL_D RIGHT OUTER JOIN JOURNAL_H ON JOURNAL_D.JNL_BSU_ID = JOURNAL_H.JHD_BSU_ID AND " & _
        "JOURNAL_D.JNL_FYEAR = JOURNAL_H.JHD_FYEAR AND JOURNAL_D.JNL_DOCTYPE = JOURNAL_H.JHD_DOCTYPE AND " & _
        "JOURNAL_D.JNL_DOCNO = JOURNAL_H.JHD_DOCNO AND JOURNAL_D.JNL_SUB_ID = JOURNAL_H.JHD_SUB_ID LEFT OUTER JOIN " & _
        "ACCOUNTS_M ON JOURNAL_D.JNL_ACT_ID = ACCOUNTS_M.ACT_ID LEFT OUTER JOIN vw_OSO_BUSINESSUNIT_M ON " & _
        "JOURNAL_H.JHD_BSU_ID = vw_OSO_BUSINESSUNIT_M.BSU_ID " & strFilter _
        & " ORDER BY JOURNAL_D.JNL_ID"
        
        cmd.CommandType = Data.CommandType.Text 
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            params("UserName") = Session("sUsr_name")
            params("DocType") = cmbDocType.SelectedItem.Text
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptViewPostDetails.rpt"
            Session("ReportSource") = repSource
            ' Response.Redirect("rptviewer.aspx", True)
            ReportLoadSelection()
        Else
            lblError.Text = "No Records with specified condition"
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub InitializeFilter(ByRef filter As String)
        If filter IsNot String.Empty Or filter IsNot "" Then
            If Not filter.StartsWith(" WHERE") Then
                filter = filter.Insert(0, " WHERE")
            End If
            If Not filter.EndsWith("WHERE") Then
                filter = filter.Insert(filter.Length, " AND")
            End If
        End If
    End Sub
End Class
