<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CorpDrillDown_N.aspx.vb" Inherits="Reports_ASPX_Report_CorpDrillDown_N"  %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server" >
    <title>Untitled Page</title>
    <base target="_self">
</head>
<body>
    <form id="form1" runat="server" >
    <div>
        &nbsp;<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
    <telerik:RadGrid ID="gv_BsuDrilldown" runat="server" AllowPaging="True" 
            PageSize="25" AllowSorting="True" 
                AutoGenerateColumns="False" CellSpacing="0" EnableTheming="False" 
                GridLines="None" Skin="Office2007" >
                <ClientSettings AllowDragToGroup="True">
                </ClientSettings>
                <MasterTableView>
                 <DetailTables>
                <telerik:GridTableView DataKeyNames="BSU_Name" Name="World" Width="100%">
                    <DetailTables>
                        <telerik:GridTableView DataKeyNames="BSU_Name" Name="UAE" Width="100%">
                                <DetailTables>
                                <telerik:GridTableView DataKeyNames="BSU_Name" Name="DUBAI" Width="100%">
                                                                  <DetailTables>
                                <telerik:GridTableView DataKeyNames="BSU_Name" Name="INTERNATIONAL" Width="100%">
                                    <Columns>
                                      <telerik:GridBoundColumn DataField="BSU_Name" ItemStyle-Width="360px"
                                          HeaderText="School" 
                                         UniqueName="column">
                                     </telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn DataField="DR_EN_1112" 
                                          HeaderText="2011-2012 EN" 
                                         UniqueName="column">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_TC_1112" 
                                          HeaderText="2011-2012 TC" 
                                         UniqueName="column1">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_EN_11213" 
                                          HeaderText="2012-2013 EN" 
                                         UniqueName="column2">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_TC_11213" 
                                          HeaderText="2012-2013 TC" 
                                         UniqueName="column3">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_EN_1314" 
                                          HeaderText="2013-2014 EN" 
                                         UniqueName="column4">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_TC_1314" 
                                          HeaderText="2013-2014 TC" 
                                         UniqueName="column5">
                                     </telerik:GridBoundColumn>
                                    </Columns>
                                </telerik:GridTableView>
                            </DetailTables>
                                    <Columns>
                                      <telerik:GridBoundColumn DataField="BSU_Name" 
                                          HeaderText="Curriculum"  ItemStyle-Width="360px"
                                         UniqueName="column">
                                     </telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn DataField="DR_EN_1112" 
                                          HeaderText="2011-2012 EN" 
                                         UniqueName="column">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_TC_1112" 
                                          HeaderText="2011-2012 TC" 
                                         UniqueName="column1">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_EN_11213" 
                                          HeaderText="2012-2013 EN" 
                                         UniqueName="column2">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_TC_11213" 
                                          HeaderText="2012-2013 TC" 
                                         UniqueName="column3">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_EN_1314" 
                                          HeaderText="2013-2014 EN" 
                                         UniqueName="column4">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_TC_1314" 
                                          HeaderText="2013-2014 TC" 
                                         UniqueName="column5">
                                     </telerik:GridBoundColumn>
                                    </Columns>
                                </telerik:GridTableView>
                            </DetailTables>
                            <Columns>
                              <telerik:GridBoundColumn DataField="BSU_Name" 
                                          HeaderText="City" ItemStyle-Width="380px"
                                         UniqueName="column">
                                     </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="DR_EN_1112" 
                                      HeaderText="2011-2012 EN" 
                                     UniqueName="column">
                                 </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="DR_TC_1112" 
                                      HeaderText="2011-2012 TC" 
                                     UniqueName="column1">
                                 </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="DR_EN_11213" 
                                      HeaderText="2012-2013 EN" 
                                     UniqueName="column2">
                                 </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="DR_TC_11213" 
                                      HeaderText="2012-2013 TC" 
                                     UniqueName="column3">
                                 </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="DR_EN_1314" 
                                      HeaderText="2013-2014 EN" 
                                     UniqueName="column4">
                                 </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="DR_TC_1314" 
                                      HeaderText="2013-2014 TC" 
                                     UniqueName="column5">
                                 </telerik:GridBoundColumn>
                            </Columns>
                        </telerik:GridTableView>
                    </DetailTables>
                    <Columns>
                      <telerik:GridBoundColumn DataField="BSU_Name" 
                                          HeaderText="Country" ItemStyle-Width="400px"
                                         UniqueName="column">
                                     </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="DR_EN_1112" 
                                          HeaderText="2011-2012 EN" 
                                         UniqueName="column">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_TC_1112" 
                                          HeaderText="2011-2012 TC" 
                                         UniqueName="column1">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_EN_11213" 
                                          HeaderText="2012-2013 EN" 
                                         UniqueName="column2">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_TC_11213" 
                                          HeaderText="2012-2013 TC" 
                                         UniqueName="column3">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_EN_1314" 
                                          HeaderText="2013-2014 EN" 
                                         UniqueName="column4">
                                     </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DR_TC_1314" 
                                          HeaderText="2013-2014 TC" 
                                         UniqueName="column5">
                                     </telerik:GridBoundColumn>
                    </Columns>
                </telerik:GridTableView>
            </DetailTables>   
                    <Columns> 
           
                         <telerik:GridBoundColumn DataField="BSU_Name" AllowFiltering="false" 
                            HeaderText="GEMS" ItemStyle-Width="420px"
                            UniqueName="BSU_NAME">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText="Edit"  Visible="false" Groupable="false"
                            UniqueName="TemplateColumn_VERIFIED">
                            <ItemTemplate>                             
                                                             
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                         <telerik:GridBoundColumn DataField="DR_EN_1112" 
                              HeaderText="2011-2012 EN" 
                             UniqueName="column">
                         </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn DataField="DR_TC_1112" 
                              HeaderText="2011-2012 TC" 
                             UniqueName="column1">
                         </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn DataField="DR_EN_11213" 
                              HeaderText="2012-2013 EN" 
                             UniqueName="column2">
                         </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn DataField="DR_TC_11213" 
                              HeaderText="2012-2013 TC" 
                             UniqueName="column3">
                         </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn DataField="DR_EN_1314" 
                              HeaderText="2013-2014 EN" 
                             UniqueName="column4">
                         </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn DataField="DR_TC_1314" 
                              HeaderText="2013-2014 TC" 
                             UniqueName="column5">
                         </telerik:GridBoundColumn>
                         
                    </Columns>                   
                    <EditFormSettings>
                        <EditColumn>
                        </EditColumn>
                    </EditFormSettings>
                </MasterTableView>
                <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                <ItemStyle Font-Bold="true" HorizontalAlign="Left" />
                <AlternatingItemStyle Font-Bold="true" HorizontalAlign="Left" />
                <FilterMenu>
                </FilterMenu>
                <PagerStyle mode="NextPrevNumericAndAdvanced"></PagerStyle>
            </telerik:RadGrid>
    </div>
    </form>
</body>
</html>
