Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptAging
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared MainMnu_code As String
    Shared bconsolidation As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If

        'Add the readOnly attribute to the Date textBox So that the Text property value is accessible in the form
        'It is not giving the values if you make the ReadOnly property to TRUE
        'So you have to do this in page_Load()
        txtToDate.Attributes.Add("ReadOnly", "Readonly")
        txtFromDate.Attributes.Add("ReadOnly", "Readonly")
        txtBSUName.Attributes.Add("ReadOnly", "Readonly")
        Page.Title = OASISConstants.Gemstitle
        'checks whether the BSUnit is selected if it is selected h_BSUID.Value will be having
        'the IDs of the BS Unit. "FillBSUNames()" function will set the BSU names
        If h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            FillBSUNames(h_BSUID.Value)
            'h_BSUID.Value = ""
        Else
            h_BSUID.Value = Session("sBsuid")
            txtBSUName.Text = Session("BSU_Name")
            'FillBSUNames(Session("businessunit"))
        End If

        If Not IsPostBack Then
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
           
            txtToDate.Text = UtilityObj.GetDiplayDate()
            txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))

        End If

        If bconsolidation Then
            chkGroupCurrency.Enabled = True
        Else
            chkGroupCurrency.Enabled = False
            chkGroupCurrency.Checked = False
        End If
        lblError.Text = ""

    End Sub

    Private Function GetDataSourceDocType() As Hashtable
        ' Type-0(By Account),-1(By Ctrl Account),-2(By SgrpCode),-3(By Grpcode)
        Dim ht As New Hashtable
        ht("0") = "By Account"
        ht("1") = "By Ctrl Account"
        ht("2") = "By SgrpCode"
        ht("3") = "By Grpcode"
        Return ht
    End Function
    Private Function GetDataSourceRptType() As Hashtable
        ' Type-0(By Account),-1(By Ctrl Account),-2(By SgrpCode),-3(By Grpcode)
        Dim ht As New Hashtable
        'ht("0") = "By Account"
        'ht("1") = "By Ctrl Account"
        'ht("2") = "By SgrpCode"
        'ht("3") = "By Grpcode"
        ht("6") = "By Income/Expense"
        ht("7") = "Expense Contribution by Account"
        ht("8") = "Income Contribution by Account"
        Return ht
    End Function

    Private Sub FillBSUNames(ByVal BSUIDs As String)
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim dr As SqlDataReader
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"

        dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        txtBSUName.Text = ""
        bconsolidation = True
        Dim bval As Boolean = dr.Read
        While (bval)
            txtBSUName.Text += dr(0).ToString()
            bval = dr.Read()
            If bval Then
                bconsolidation = False
                txtBSUName.Text += "||"
            End If
        End While
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        Dim strXMLBSUNames As String
        'Generate the XML in the form <BSU_DETAILS><BSU_ID>IDhere</BSU_ID></BSU_DETAILS>
        strXMLBSUNames = GenerateBSUXML(h_BSUID.Value) 'txtBSUName.Text)

        
        GenerateTrialbalanceReport(strXMLBSUNames)

       

    End Sub

    Private Sub GenerateExpenseChart()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTExpenseChart", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        'Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.Xml)
        'sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        'cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        'Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        'sqlpJHD_FYEAR.Value = Session("financialyear")
        'cmd.Parameters.Add(sqlpJHD_FYEAR)

        'Dim sqlpJHD_DOCTYPE As New SqlParameter("@Type", SqlDbType.VarChar, 20)
        'sqlpJHD_DOCTYPE.Value = cmbGroupType.SelectedItem.Value
        'cmd.Parameters.Add(sqlpJHD_DOCTYPE)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@TODOCDT", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        'Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        'sqlpConsolidation.Value = chkConsolidation.Checked
        'cmd.Parameters.Add(sqlpConsolidation)

        'Dim sqlpGroupCurrency As New SqlParameter("@bShowinGrpCurrency", SqlDbType.Bit)
        'sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        'cmd.Parameters.Add(sqlpGroupCurrency)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/chrtExpense.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            response.redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Private Sub GenerateExpenseIncomeChart(ByVal strXMLBSUNames As String)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTGETTPnLDATA", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpJHD_DOCTYPE As New SqlParameter("@Type", SqlDbType.VarChar, 2)
        sqlpJHD_DOCTYPE.Value = cmbGroupType.SelectedItem.Value
        cmd.Parameters.Add(sqlpJHD_DOCTYPE)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpID As New SqlParameter("@ID", SqlDbType.VarChar)
        sqlpID.Value = ""
        cmd.Parameters.Add(sqlpID)

        Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        sqlpConsolidation.Value = bconsolidation
        cmd.Parameters.Add(sqlpConsolidation)

        Dim sqlpGroupCurrency As New SqlParameter("@bShowinGrpCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            params("rptCaption") = GetReportCaption(cmbGroupType.SelectedItem.Value)
            'params("bConsolidation") = bconsolidation
            'If bconsolidation Then
            '    params("BSUNames") = txtBSUName.Text.Replace("||", ",")
            'Else
            '    params("BSUNames") = ""
            'End If
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/chrtExpense_Income.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            response.redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Private Function GetReportCaption(ByVal type As Integer) As String
        Select Case type
            Case 6
                Return "P\L Contribution"
            Case 7
                Return "Expense Contribution"
            Case 8
                Return "Income Contribution"
            Case Else
                Return ""
        End Select
    End Function

    Private Sub GeneratePLStatement(ByVal strXMLBSUNames As String)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTRETURNFINALACCOUNTS", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        'Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        'sqlpJHD_FYEAR.Value = Session("financialyear")
        'cmd.Parameters.Add(sqlpJHD_FYEAR)

        Dim sqlpJHD_DOCTYPE As New SqlParameter("@Typ", SqlDbType.VarChar, 2)
        sqlpJHD_DOCTYPE.Value = "P"
        cmd.Parameters.Add(sqlpJHD_DOCTYPE)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        sqlpConsolidation.Value = bconsolidation
        cmd.Parameters.Add(sqlpConsolidation)

        Dim sqlpGroupCurrency As New SqlParameter("@bGrpCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            params("bConsolidation") = bconsolidation
            If bconsolidation Then
                params("BSUNames") = txtBSUName.Text.Replace("||", ",")
            Else
                params("BSUNames") = ""
            End If
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptPLStatement.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            response.redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Private Sub GenerateTrialbalanceReport(ByVal strXMLBSUNames As String)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("Aging", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@TRN_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        'Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        'sqlpJHD_FYEAR.Value = Session("financialyear")
        'cmd.Parameters.Add(sqlpJHD_FYEAR)



        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)



        Dim sqlpGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            Session("BSUNames") = strXMLBSUNames
            Session("rptFromDate") = txtFromDate.Text
            Session("rptToDate") = txtToDate.Text
            Session("rptConsolidation") = chkConsolidation.Checked
            Session("rptGroupCurrency") = chkGroupCurrency.Checked

            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            params("ReportHeading") = ""
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptAgingSum.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                Response.Redirect("rptviewer.aspx", True)
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
            objConn.Close()
    End Sub
    'Generates the XML for BSUnit
    Private Function GenerateBSUXML(ByVal BSUIDs As String) As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Try
            BSUDetails = xmlDoc.CreateElement("BSU_DETAILS")
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement("BSU_DETAIL")
                XMLEBSUID = xmlDoc.CreateElement("BSU_ID")
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub
End Class
