Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_rptAccountMasterList
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
        'If Page.IsPostBack = False Then
        '    If isPageExpired() Then
        '        Response.Redirect("expired.htm")
        '    Else
        '        Session("TimeStamp") = Now.ToString
        '        ViewState("TimeStamp") = Now.ToString
        '    End If
        'End If

        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        UsrBSUnits1.MenuCode = MainMnu_code
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            lblError.Text = ""
        End If
    End Sub

    Private Sub GenerateDayendMonthend()
        Dim params As New Hashtable
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim cmd As New SqlCommand("GetDayMonthendStatus", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_IDs As New SqlParameter("@BSU_IDS", SqlDbType.VarChar, 600)
        sqlpBSU_IDs.Value = UsrBSUnits1.GetSelectedNode("|")
        cmd.Parameters.Add(sqlpBSU_IDs)

        'Dim retData As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, cmd.CommandText, Nothing)
        'If Not retData IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            params("UserName") = Session("sUsr_name")
            params("OnDate") = Now.Date
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptGetDayMonthendStatus.rpt"
            Session("ReportSource") = repSource
            Response.Redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Select Case MainMnu_code
            Case "A850035"
                GenerateDayendMonthend()
        End Select
    End Sub

End Class
