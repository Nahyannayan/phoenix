Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Partial Class Reports_ASPX_Report_PickCostObject
    Inherits System.Web.UI.Page
    Shared liUserList As List(Of String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            Response.Write("} </script>" & vbCrLf)
        End If
        If Page.IsPostBack = False Then
            If Session("Menu_text") <> "" And Session("Menu_text") <> String.Empty Then
                Dim menu_arr() As String = Session("Menu_text").Split("|")
                Session("temp") = menu_arr(menu_arr.Length - 1)
            End If
            Try
                Dim ds As New DataSet
                Dim moduleDecr As New Encryption64
                Dim connStr As String = ConfigurationManager.ConnectionStrings("maindb").ConnectionString
                Dim str_sql As String
                Using conn As SqlConnection = New SqlConnection(connStr)
                    str_sql = "SELECT CCS_ID,  CCS_DESCR, " _
                    & " CCS_PARENT_CCS_ID FROM COSTCENTER_S " _
                    & " WHERE (CCS_CCT_ID <> '9999')  "
                    Dim da As SqlDataAdapter = New SqlDataAdapter(str_sql, conn)
                    da.Fill(ds)
                    da.Dispose()
                End Using
                ds.DataSetName = "COSTCENTER"
                ds.Tables(0).TableName = "COSTCENTERTABLE"
                Dim relation As DataRelation = New DataRelation("ParentChild", _
                ds.Tables("COSTCENTERTABLE").Columns("CCS_ID"), _
                ds.Tables("COSTCENTERTABLE").Columns("CCS_PARENT_CCS_ID"), True)
                relation.Nested = True
                ds.Relations.Add(relation)
                If ds.Tables(0).Rows.Count > 0 Then
                    XmlDataSource1.Data = ds.GetXml()
                Else
                    Response.Redirect("~/Modulelogin.aspx")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
        SetChk(Me.Page)
    End Sub

    Private Sub bind_previous_ids()
        liUserList = New List(Of String)
        If Session("dtCostChild") IsNot System.DBNull.Value Then
            'Dim dv As New DataView
            ' ''For icheck As Integer = 0 To Session("dtCostChild").rows.count - 1
            ' ''    If Request.QueryString("ccsmode") <> "others" Then
            ' ''        If Session("dtCostChild").Rows(icheck)("VoucherId") = Request.QueryString("vid") And Session("dtCostChild").Rows(icheck)("CostCenter") = Request.QueryString("ccsmode") Then
            ' ''            list_add(Session("dtCostChild").Rows(icheck)("MemberId"))
            ' ''        End If
            ' ''    Else
            ' ''        If Session("dtCostChild").Rows(icheck)("VoucherId") = Request.QueryString("vid") And Session("dtCostChild").Rows(icheck)("Memberid") & "" = "" Then
            ' ''            list_add(Session("dtCostChild").Rows(icheck)("CostCenter"))
            ' ''        End If
            ' ''    End If

            ' ''Next 'dv = DirectCast(Session("dtCostChild"), DataTable).DefaultView
        Else

        End If
    End Sub

    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code, str_filter_name As String
            Dim str_filter_column1, str_filter_column2, str_filter_column3, str_mode As String
            Dim str_txtCode, str_txtName, str_txtcolumn1, str_txtcolumn2, str_txtcolumn3 As String
            str_filter_code = ""
            str_filter_name = ""
            str_filter_column1 = ""
            str_filter_column2 = ""
            str_filter_column3 = ""
            str_mode = ""

            str_txtCode = ""
            str_txtName = ""
            str_txtcolumn1 = ""
            str_txtcolumn2 = ""
            str_txtcolumn3 = ""
            Dim str_query_header As String = get_Query()
            str_Sql = str_query_header.Split("||")(0)

            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
          
            str_Sql = "SELECT  DISTINCT  JDS_CODE, JDS_CCS_ID,  JDS_DESCR , JDS_BSU_ID" _
               & " FROM         JOURNAL_D_S AS JDS" _
               & " WHERE (JDS_CODE IS NOT NULL)" _
               & "AND JDS_CCS_ID='" & Request.QueryString("ccsmode") & "'"
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            Dim ds As New DataSet

            If Request.QueryString("ccsmode") = "OTH" Then
                str_Sql = "SELECT  DISTINCT  JDS.JDS_CCS_ID,   JDS.JDS_BSU_ID," _
                  & " JDS.JDS_CCS_ID AS JDS_CODE, " _
                  & " CCS.CCS_DESCR AS JDS_DESCR" _
                  & " FROM JOURNAL_D_S AS JDS INNER JOIN" _
                  & " COSTCENTER_S AS CCS ON" _
                  & " JDS.JDS_CCS_ID = CCS.CCS_ID" _
                  & " WHERE     (JDS.JDS_CODE IS NULL)"
            End If
            str_Sql = str_Sql & str_filter_code & str_filter_name & str_filter_column1 & str_filter_column2 & str_filter_column3
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            SetChk(Me.Page)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Function get_Query() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql, str_ccs As String
            str_ccs = Request.QueryString("ccsmode")
            str_Sql = "SELECT CCS_ID ,CCS_DESCR ,CCS_QUERY, CCS_COLUMNS" _
            & " FROM COSTCENTER_S" _
            & " WHERE  CCS_CCT_ID='9999'" _
            & " AND CCS_ID='" & str_ccs & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("CCS_QUERY") & "|" & ds.Tables(0).Rows(0)("CCS_COLUMNS")
            Else
                Return ("")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return ("")
        End Try
        Return ""
    End Function
     
    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If liUserList.Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If liUserList.Contains(p_userid) Then
            Return False
        Else
            liUserList.Add(p_userid)
            Return False
        End If
    End Function

    Protected Sub TreeView1_SelectedNodeChanged(ByVal sender As Object, _
        ByVal e As System.EventArgs) Handles tvGroup.SelectedNodeChanged
        Try
            h_Selectedid.Value = tvGroup.SelectedNode.Value
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        SetChk(Me.Page)
        h_SelectedId.Value = ""
        For Each node As TreeNode In tvGroup.CheckedNodes
            '''''  check whether some child node is selected
            Dim boolHasCilld As Boolean = False
            For Each cnode As TreeNode In node.ChildNodes
                If cnode.Checked = True Then
                    boolHasCilld = True
                    lblError.Text = "<br>Multiple Selection Occured in the Same Heirarchy <br>(Parent as well as Child Unit Got Selected in Some Cost Units)"
                    Exit Sub
                End If
            Next
            If boolHasCilld = False Then
                If h_SelectedId.Value = "" Then
                    h_SelectedId.Value = node.Value
                Else
                    h_SelectedId.Value = h_SelectedId.Value & "||" & node.Value
                End If
            End If
        Next
        If h_SelectedId.Value.ToString.EndsWith("||") Then
            h_SelectedId.Value = h_SelectedId.Value.Substring(0, h_SelectedId.Value.Length - 2)
        End If
        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        Response.Write("window.close();")
        Response.Write("} </script>")
    End Sub

End Class
