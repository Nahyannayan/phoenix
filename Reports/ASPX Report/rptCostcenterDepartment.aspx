﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptCostcenterDepartment.aspx.vb" Inherits="Reports_ASPX_Report_rptCostcenterDepartment" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked;


            if (CheckBoxIDs != null) {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                    ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }
        function GetAccounts() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?type=exp&multiSelect=true&forrpt=1", "", sFeatures)

            if (result != "") {
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();

            }
        }

        function getFilter(NameObj, IdObj, frm) {

            var bsuId = '<%=Session("sBsuid")%>'
            var sFeatures;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 650px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("ShowCostCenterMulti.aspx", "", sFeatures)

            if (result == '' || result == undefined) {
                return false;
            }

            NameandCode = result.split('__');

            document.getElementById(IdObj).value = NameandCode[0];
            document.getElementById(NameObj).value = NameandCode[1];
            return true;

        }
        function ChkBox(ObjTwo, ObjThree) {
            document.getElementById(ObjTwo).checked = false;
            document.getElementById(ObjThree).checked = false;
        }
    </script>

    <table align="center" cellpadding="5" border="1" bordercolor="#1b80b6" cellspacing="0" style="border-collapse: collapse;
        width: 70%;">
        <tr class="subheader_img">
            <td align="left" colspan="4" style="height: 19px" valign="middle">
                <asp:Label ID="lblCaption" runat="server" Text="Sub Ledger Report"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters">
                From Date
            </td>
            <td align="left" class="matters" style="width: 189px; height: 1px; text-align: left">
                <asp:TextBox ID="txtFromDate" runat="server" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif"
                    CausesValidation="False" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
            </td>
            <td align="left" class="matters" style="color: #1b80b6; height: 1px">
                To Date
            </td>
            <td align="left" class="matters" style="height: 1px; text-align: left">
                <asp:TextBox ID="txtToDate" runat="server" Width="122px"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr style="color: #1b80b6" id="trBSUName" runat="server">
            <td align="left" valign="top" class="matters">
                Business Unit
            </td>
            <td align="left" class="matters" colspan="3" valign="top">
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" valign="top">
                <asp:Label ID="lblBankCash" runat="server" Text="Account"></asp:Label>
            </td>
            <td align="left" class="matters" colspan="3" valign="top">
                <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the ACT ID you want to Add to the Search and click on Add)"
                    Width="377px"></asp:Label>
                <br />
                <asp:TextBox ID="txtBankNames" runat="server" CssClass="inputbox" Height="18px" Width="330px"></asp:TextBox>
                <asp:LinkButton ID="lblAddActID" runat="server">Add</asp:LinkButton>
                <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetAccounts()" /><br />
                <asp:GridView ID="grdACTDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    PageSize="5" Width="377px">
                    <Columns>
                        <asp:TemplateField HeaderText="ACT ID">
                            <ItemTemplate>
                                <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ACT_Name" HeaderText="ACT Name" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_small" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters">
                Department
            </td>
            <td align="left" class="matters" colspan="3">
                <asp:TextBox ID="txtDepartment" runat="server" Width="292px" OnTextChanged="txtDepartment_TextChanged"
                    AutoPostBack="True"></asp:TextBox>
                <asp:ImageButton ID="imgPickDpt" runat="server" ImageUrl="~/Images/forum_search.gif" /><br />
                <asp:GridView ID="gvDptdetails" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    SkinID="GridViewPickDetails" Width="96%" OnPageIndexChanging="gvDptdetails_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="Department">
                            <ItemTemplate>
                                <asp:Label ID="lblStudentName" runat="server" Text='<%# bind("CCT_DESCR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters">
                Show By 
            </td>
            <td align="left" class="matters" colspan="3">
                <asp:DropDownList ID="ddAccountGroup" runat="server" Width="151px" AutoPostBack="True">
                    <asp:ListItem Value="ACC">By Account</asp:ListItem> 
                    <asp:ListItem Value="BSU">By Business Unit</asp:ListItem>
                    <asp:ListItem Value="CON">Consolidated</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="4" style="text-align: right">
                &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table>
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
    <asp:HiddenField ID="h_ACTIDs" runat="server" />
    <asp:HiddenField ID="H_COSTIDS" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <asp:HiddenField ID="h_Departmentid" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
</asp:Content>
