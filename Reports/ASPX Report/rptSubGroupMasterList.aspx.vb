Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_rptSubGroupMasterList
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Page.Title = OASISConstants.Gemstitle
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
        End If

        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            lblError.Text = ""
        End If

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim cmd As New SqlCommand
        Dim params As New Hashtable
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet

        cmd.CommandText = "SELECT dbo.ACCGRP_M.GPM_DESCR, dbo.ACCGRP_M.GPM_ID, dbo.ACCSGRP_M.SGP_DESCR, " _
        & "dbo.ACCSGRP_M.SGP_TYPE FROM dbo.ACCSGRP_M LEFT OUTER JOIN " _
        & "dbo.ACCGRP_M ON dbo.ACCSGRP_M.SGP_GPM_ID = dbo.ACCGRP_M.GPM_ID"
        cmd.CommandType = Data.CommandType.Text

        'SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
        'If SqlHelper.ExecuteScalar(str_conn, CommandType.Text, cmd.CommandText, Nothing) IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            params("UserName") = Session("sUsr_name")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptSubGrpMaster.rpt"
            Session("ReportSource") = repSource
            Response.Redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
    End Sub

    Private Sub InitializeFilter(ByRef filter As String)
        If filter IsNot String.Empty Or filter IsNot "" Then
            If Not filter.StartsWith(" WHERE") Then
                filter = filter.Insert(0, " WHERE")
            End If
            If Not filter.EndsWith("WHERE") Then
                filter = filter.Insert(filter.Length, " AND")
            End If
        End If
    End Sub
End Class
