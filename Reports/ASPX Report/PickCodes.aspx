<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PickCodes.aspx.vb" Inherits="Accounts_PickCodes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Add Cost Object</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
     <base target="_self" />
    <script language="javascript" type="text/javascript" src="../../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
    function ChangeCheckBoxState(id, checkState)
        {
            var cb = document.getElementById(id);
            if (cb != null)
               cb.checked = checkState;
        }
        
        function ChangeAllCheckBoxStates(checkState)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked; 
           
            
            if (CheckBoxIDs != null)
            {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                   ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }
       
      function menu_click(val,mid)
                {
                //alert(val);
                var path;
                if (val=='LI')
                {
                path='../images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../images/operations/notendswith.gif';
                }
               if (mid==1)
                 {
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value=val+'__'+path;
                 document.getElementById('<%=getid1() %>').src = path;
                 }
                 else  if (mid==2)
                 {
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value=val+'__'+path;
                 document.getElementById('<%=getid2() %>').src = path;
                 }
                  else  if (mid==3)
                 {
                document.getElementById("<%=h_selected_menu_3.ClientID %>").value=val+'__'+path;
                 document.getElementById('<%=getid3() %>').src = path;
                 }
                  else  if (mid==4)
                 {
                document.getElementById("<%=h_selected_menu_4.ClientID %>").value=val+'__'+path;
                 document.getElementById('<%=getid4() %>').src = path;
                 }
                  else  if (mid==5)
                 {
                document.getElementById("<%=h_selected_menu_5.ClientID %>").value=val+'__'+path;
                 document.getElementById('<%=getid5() %>').src = path;
                 }
                 
                }//end fn
         </script>
</head>
<body  onload="listen_window();" topmargin="0" leftmargin="0">
<!--1st drop down menu -->                                                   
<div id="dropmenu1" class="dropmenudiv" style="width: 110px;">
<a href="javascript:menu_click('LI',1);"><img class="img_left" alt="Any where" src= "../../images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:menu_click('NLI',1);"><img class="img_left" alt="Not In" src= "../../images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:menu_click('SW',1);"><img class="img_left" alt="Starts With" src= "../../images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:menu_click('NSW',1);"><img class="img_left" alt="Like" src= "../../images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:menu_click('EW',1);"><img class="img_left" alt="Like" src= "../../images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:menu_click('NEW',1);"><img class="img_left" alt="Like" src= "../../images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>
<!--2nd drop down menu -->                                                
<div id="dropmenu2" class="dropmenudiv" style="width: 110px;">
<a href="javascript:menu_click('LI',2);"><img class="img_left" alt="Like" src= "../../images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:menu_click('NLI',2);"><img class="img_left" alt="Like" src= "../../images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:menu_click('SW',2);"><img class="img_left" alt="Like" src= "../../images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:menu_click('NSW',2);"><img class="img_left" alt="Like" src= "../../images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:menu_click('EW',2);"><img class="img_left" alt="Like" src= "../images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:menu_click('NEW',2);"><img class="img_left" alt="Like" src= "../../images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>
<!--3rd drop down menu -->                                                   
<div id="dropmenu3" class="dropmenudiv" style="width: 150px;">
<a href="javascript:menu_click('LI',3);"><img class="img_left" alt="Like" src= "../../images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:menu_click('NLI',3);"><img class="img_left" alt="Like" src= "../../images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:menu_click('SW',3);"><img class="img_left" alt="Like" src= "../../images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:menu_click('NSW',3);"><img class="img_left" alt="Like" src= "../../images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:menu_click('EW',3);"><img class="img_left" alt="Like" src= "../../images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:menu_click('NEW',3);"><img class="img_left" alt="Like" src= "../../images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>
<!--4th drop down menu -->                                                   
<div id="dropmenu4" class="dropmenudiv" style="width: 150px;">
<a href="javascript:menu_click('LI',4);"><img class="img_left" alt="Like" src= "../../images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:menu_click('NLI',4);"><img class="img_left" alt="Like" src= "../../images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:menu_click('SW',4);"><img class="img_left" alt="Like" src= "../../images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:menu_click('NSW',4);"><img class="img_left" alt="Like" src= "../../images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:menu_click('EW',4);"><img class="img_left" alt="Like" src= "../../images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:menu_click('NEW',4);"><img class="img_left" alt="Like" src= "../../images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>
<!--5th drop down menu -->                                                   
<div id="dropmenu5" class="dropmenudiv" style="width: 150px;">
<a href="javascript:menu_click('LI',5);"><img class="img_left" alt="Like" src= "../../images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:menu_click('NLI',5);"><img class="img_left" alt="Like" src= "../../images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:menu_click('SW',5);"><img class="img_left" alt="Like" src= "../../images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:menu_click('NSW',5);"><img class="img_left" alt="Like" src= "../../images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:menu_click('EW',5);"><img class="img_left" alt="Like" src= "../../images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:menu_click('NEW',5);"><img class="img_left" alt="Like" src= "../../images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>
    <form id="form1" runat="server">
   <table id="tbl" width="95%" align="center">
                    <tr>
                        <td class="column_show_acc_3" colspan="6" style="height: 14px">
                        <input id="h_SelectedId" runat="server" type="hidden" value="" />
                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr> 
                    <tr style="font-size: 12pt">
                        <td colspan="7">
                            <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                                EmptyDataText="No Data" Width="100%" AllowPaging="True" SkinID="GridViewView" PageSize="15">
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                 <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Select" SortExpression="ID">
                                        <ItemTemplate>
                                            &nbsp;<input id="chkControl" runat="server" type="checkbox" value='<%# Bind("ID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="40px" />
                                        <HeaderTemplate>
                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;
                                                height: 100%">
                                               <tr><td></td></tr>
                                                <tr>
                                                    <td style="width: 100px" valign="middle" class="gridheader_text" >Select
                                            <input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                                    </td>
                                                </tr>
                                                <tr><td>&nbsp;</td></tr> 
                                            </table>
                                        </HeaderTemplate>
                                        <HeaderStyle Width="40px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Id" SortExpression="ID">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <table style="width: 100%; height: 100%" >
                                                <tr>
                                                    <td align="center" style="width: 100px">
                                                        <asp:Label ID="lblID" runat="server" CssClass="gridheader_text"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100px">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 100px; height: 12px">
                                                                    <div id="chromemenu1" class="chromestyle">
                                                                        <ul>
                                                                            <li><a href="#" rel="dropmenu1">
                                                                                <img id="mnu_1_img" runat="server" align="middle" alt="Menu" border="0" src="../../images/operations/like.gif" /><span
                                                                                    style="color: #0000ff; text-decoration: none">&nbsp;</span></a> </li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                                <td style="width: 100px; height: 12px">
                                                                    <asp:TextBox ID="txtCode" runat="server" Width="72px"></asp:TextBox></td>
                                                                <td style="width: 100px; height: 12px" valign="middle">
                                                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../../images/forum_search.gif" OnClick="btnCodeSearch_Click"
                                                                         />&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name" SortExpression="NAME">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <table style="width: 100%; height: 100%" >
                                                <tr>
                                                    <td align="center" style="width: 100px">
                                                        <asp:Label ID="lblName" runat="server" CssClass="gridheader_text"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100px">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 100px; height: 12px">
                                                                    <div id="chromemenu2" class="chromestyle">
                                                                        <ul>
                                                                            <li><a href="#" rel="dropmenu2">
                                                                                <img id="mnu_2_img" runat="server" align="middle" alt="Menu" border="0" src="../../images/operations/like.gif" /><span
                                                                                    style="color: #0000ff; text-decoration: none">&nbsp;</span></a> </li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                                <td style="width: 100px; height: 12px">
                                                                    <asp:TextBox ID="txtName" runat="server" Width="72px"></asp:TextBox></td>
                                                                <td style="width: 100px; height: 12px" valign="middle">
                                                                    <asp:ImageButton ID="btnNameSearch" runat="server" ImageAlign="Top" ImageUrl="../../images/forum_search.gif" OnClick="btnNameSearch_Click"
                                                                         />&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView> 
                        </td>
                    </tr>
                    <tr style="font-size: 12pt">
                        <td style="height: 21px" align="center" colspan="10">
                            &nbsp;<asp:Button ID="btnFinish" runat="server" Text="Finish" CssClass="button" /></td>
                    </tr>
                </table>
            <script type="text/javascript">
                cssdropdown.startchrome("chromemenu1");
                cssdropdown.startchrome("chromemenu2");
                
            </script>
    </form>
</body>
</html>
