<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptReconciliations.aspx.vb" Inherits="Reports_ASPX_Report_rptRecociliations"
    Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx?dt=" + document.getElementById('<%=txtfromDate.ClientID %>').value, "", sFeatures);
            if (result != '' && result != undefined) {
                switch (txtControl) {
                    case 0:
                        document.getElementById('<%=txtfromDate.ClientID %>').value = result;
                        break;
                }
            }
            return false;
        }

        function GetBSUName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var mode = document.getElementById('<%=h_Mode.ClientID %>').value;
            if (mode == "PDC" || mode == "party_aging" || mode == "prepayment") {
                result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx", "", sFeatures)
            }
            else {
                result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx?multiSelect=false", "", sFeatures)
            }
            if (result != '' && result != undefined) {

                document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }
        }

        function GetAccounts() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var ActType;
            var mode = document.getElementById('<%=h_Mode.ClientID %>').value;
            if (mode == "party_aging") {
                ActType = document.getElementById('<%=h_ACTTYPE.ClientID %>').value;
                if (ActType == 's') {
                    //result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=s&forrpt=1", "", sFeatures)
                    result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=s&forrpt=1", "pop_up2")
                }
                if (ActType == 'C') {
                    //result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=C&forrpt=1", "", sFeatures)
                    result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=C&forrpt=1", "pop_up2")
                }
            }
            else if (mode == "PDC") {
                //result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?bankcash=b&multiSelect=false&forrpt=1", "", sFeatures)
                result = radopen("../../Accounts/accjvshowaccount.aspx?bankcash=b&multiSelect=false&forrpt=1", "pop_up2")
            }
            else if (mode == "Reconciliations") {
                //result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?bankcash=b&multiSelect=true&forrpt=1", "", sFeatures)
                result = radopen("../../Accounts/accjvshowaccount.aspx?bankcash=b&multiSelect=true&forrpt=1", "pop_up2")
            }
            else {
                //result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?multiSelect=false&forrpt=1", "", sFeatures)
                result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=false&forrpt=1", "pop_up2")
            }
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = result;
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_ACTIDs.ClientID%>').value = arg.NameandCode;
                document.forms[0].submit();
                //__doPostBack('<%= h_ACTIDs.ClientID%>', 'ValueChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>

     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
       
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
       
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Reconciliations Statement"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="dayBook" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr id="trMultiSelect" runat="server">
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <div class="checkbox-list">
                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr id="trSingleSelect" runat="server">
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlBSUnit" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="trasOnDate" runat="server">
                        <td align="left" width="20%"><span class="field-label">As On Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server">    </asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="trFilter" runat="server">
                        <td align="left" width="20%"><span class="field-label">Filter Type</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddFilterType" runat="server" OnSelectedIndexChanged="ddFilterType_SelectedIndexChanged"
                                AutoPostBack="True">
                                <asp:ListItem Value="0">As On Date</asp:ListItem>
                                <asp:ListItem Value="1">Voucher Date</asp:ListItem>
                                <asp:ListItem Value="2">Cheque Date</asp:ListItem>
                            </asp:DropDownList></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="trFilterDt" runat="server">
                        <td align="left" width="20%">
                            <asp:Label ID="lblFilterName" runat="server" Text="As On Date" CssClass="field-label"></asp:Label></td>
                        <td align="left" colspan="3">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:TextBox ID="txtDTFrom" runat="server">
                                        </asp:TextBox><asp:ImageButton ID="imgDTFrom" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtFromDate"
                                            ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revDTFrom" runat="server" ControlToValidate="txtDTFrom"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                            ValidationGroup="dayBook">*</asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:TextBox ID="txtDTTO" runat="server">
                                        </asp:TextBox><asp:ImageButton ID="imgDTTO" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtDTTO"
                                            ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revDTTO" runat="server" ControlToValidate="txtDTTO"
                                            Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                            ValidationGroup="dayBook">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="Trbank" runat="server">
                        <td align="left" width="20%">
                            <asp:Label ID="lblAccountType" runat="server" Text="Account" CssClass="field-label"></asp:Label></td>
                        <td align="left" colspan="3">
                            <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the ACT ID you want to Add to the Search and click on Add)"></asp:Label>
                            <asp:TextBox ID="txtBankNames" runat="server">
                            </asp:TextBox>
                            <asp:LinkButton ID="lblAddActID" runat="server">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick=" GetAccounts(); return false;" />
                            <asp:GridView ID="grdACTDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                PageSize="5" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="ACT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ACT_Name" HeaderText="ACT Name" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trcheckboxes" runat="server">
                        <td align="left" width="20%"></td>
                        <td align="left" colspan="3">
                            <asp:CheckBox ID="chkConsolidation" runat="server" Text="Consolidation" CssClass="field-label" />

                            <asp:CheckBox ID="chkGroupCurrency" runat="server" Text="View in Group Currency" CssClass="field-label" /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                ValidationGroup="dayBook" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />

                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_Mode" runat="server" />
                <asp:HiddenField ID="h_ACTIDs" runat="server" />
                <asp:HiddenField ID="h_ACTTYPE" runat="server"></asp:HiddenField>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>

                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgDTFrom" TargetControlID="txtDTFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtDTFrom">
                </ajaxToolkit:CalendarExtender>

                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgDTTO" TargetControlID="txtDTTO">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtDTTO">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>

</asp:Content>
