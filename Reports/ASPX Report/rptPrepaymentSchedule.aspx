<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPrepaymentSchedule.aspx.vb" Inherits="Reports_ASPX_Report_rptPrepaymentSchedule" 
%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">
       
    </script>
            
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary
        ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
    <br />
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0"
        style="width: 65%;">
        <tr class ="subheader_img">
            <td align="left" colspan="9" style="height: 19px"> Prepayment Schedule
                </td>
        </tr>
        <tr>
            <td align="left" class="matters" >
                As on Date</td>
            <td class="matters">
                :</td>
            <td align="left" class="matters" colspan="2"  >
                <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputbox" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                        ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
            <td align="left" class="matters"  >
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
    </table>
    <ajaxtoolkit:calendarextender id="calFromDate1" runat="server" format="dd/MMM/yyyy"
        popupbuttonid="imgFromDate" targetcontrolid="txtFromDate">
    </ajaxtoolkit:calendarextender>
    &nbsp; &nbsp; &nbsp;
</asp:Content>

