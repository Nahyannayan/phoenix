<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptSubGroupMasterList.aspx.vb" Inherits="Reports_ASPX_Report_rptSubGroupMasterList"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">
       function GetBSUName()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx","", sFeatures)
            if(result != "" || result != 'undefined')
            {
                document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
        }
    </script> 
    <table  align="center" class="BlueTable" cellpadding="5" cellspacing="0" style="width: 80%; height: 174px">
        <tr class="subheader_img">
            <td align="left" colspan="8" style="height: 19px" valign="middle">
             <asp:Label ID="lblrptCaption" runat="server" Text="SubGroup  Master List"></asp:Label> 
            </td>
        </tr>
        <tr>
            <td align="left" class="matters"   valign="top" style="width: 126px">
                Group</td>
            <td class="matters" style="width: 18px" valign="top">
                :</td>
            <td align="right" class="matters" colspan="6" style="text-align: left" valign="top">
                <asp:DropDownList ID="DropDownList1" runat="server" Width="318px">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 126px">
                Sub Group</td>
            <td class="matters" style="width: 18px">
                :</td>
            <td align="left" class="matters" colspan="6" style="height: 1px; text-align: left">
                <asp:DropDownList ID="DropDownList2" runat="server" Width="318px">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 126px">
                Control Account</td>
            <td class="matters" style="width: 18px">
                :</td>
            <td align="left" class="matters" colspan="6" style="height: 1px; text-align: left">
                <asp:DropDownList ID="DropDownList3" runat="server" Width="318px">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="text-align: right; height: 33px;">
                &nbsp;
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="h_BSUID" runat="server" />
    <asp:HiddenField ID="h_mode" runat="server" />
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
</asp:Content>

