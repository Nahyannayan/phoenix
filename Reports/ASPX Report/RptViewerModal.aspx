<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RptViewerModal.aspx.vb" Inherits="Reports_ASPX_Report_RptViewerModal" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="Expires" CONTENT="-1">
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
          <base target="_self" />
          <script type="text/javascript" language="javascript">
           window.onload = CheckForPrint
                function CheckForPrint()  
                     {
                     var ctrl=document.getElementById('<%= h_print.ClientID %>' ).value; 
                      document.getElementById('<%= h_print.ClientID %>' ).value='';
                      if  (ctrl=='completed') CloseWindow()
                      else if (ctrl!='')
                       {
                         document.getElementById(ctrl).click();
                        }
                     } 
                     function CloseWindow()
                        {
                        //if (confirm('Close the window?'))
                            window.close();
                     } 
          </script>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
</head>
<body topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">
    <form id="form1" runat="server">
    <table align="left" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr width = 100% height = 100%>
            <td align="left" valign = "top">
           <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
       ToolPanelView="None" Height="50px" Width="350px" PrintMode="ActiveX" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="True" HasGotoPageButton="True" HasSearchButton="False" HasToggleGroupTreeButton="False"  HasZoomFactorList="False" /> 
                <asp:HiddenField ID="h_print" runat="server" />
    </td>
        </tr>
    </table>
    </form>
</body>
</html>
