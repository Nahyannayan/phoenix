Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Reports_ASPX_Report_rptPDCVoucher
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared MainMnu_code As String
    Shared bconsolidation As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If
        Page.Title = OASISConstants.Gemstitle        
        'Add the readOnly attribute to the Date textBox So that the Text property value is accessible in the form
        'It is not giving the values if you make the ReadOnly property to TRUE
        'So you have to do this in page_Load()

        'txtFromDate.Attributes.Add("ReadOnly", "Readonly")

        'checks whether the BSUnit is selected if it is selected h_BSUID.Value will be having
        'the IDs of the BS Unit. "FillBSUNames()" function will set the BSU names
        If h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
            h_BSUID.Value = Session("sBsuid")
        End If
        FillBSUNames(h_BSUID.Value)

        If Not IsPostBack Then
            UtilityObj.NoOpen(Me.Header)
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Select Case MainMnu_code
                Case "A750010" 'Trial Balance'
                    lblCaption.Text = "PDC Voucher"
                Case "A750015" 'Trial Balance'A750015
                    lblCaption.Text = "PDC Receivable"
                Case "A750011" 'Trial Balance'A750015
                    lblCaption.Text = "PDC Cancellation"
                    lblFromdate.Text = "From Date"
                    TrTodate.Visible = True
                    TrTodate1.Visible = True
                    TrTodate2.Visible = True
                    trChkBoxes.Visible = False

            End Select

            txtFromDate.Text = UtilityObj.GetDiplayDate()
            txtToDate.Text = UtilityObj.GetDiplayDate()

        End If

        If bconsolidation Then
            chkGroupCurrency.Enabled = True
        Else
            chkGroupCurrency.Enabled = False
            chkGroupCurrency.Checked = True
        End If
        lblError.Text = ""

    End Sub




    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
        str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        bconsolidation = IIf(ds.Tables(0).Rows.Count = 1, True, False)
        'grdBSU.DataSource = ds
        'grdBSU.DataBind()
        Return True
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        Dim strfDate As String = txtFromDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtFromDate.Text = strfDate
        End If

        Dim strXMLBSUNames As String
        'Generate the XML in the form <BSU_DETAILS><BSU_ID>IDhere</BSU_ID></BSU_DETAILS>
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        strXMLBSUNames = GenerateBSUXML(h_BSUID.Value) 'txtBSUNames.Text)

        Select Case MainMnu_code
            Case "A750010" 'Trial Balance
                GenerateTrialbalanceReport(strXMLBSUNames)
            Case "A750015" 'Trial Balance
                GenerateTrialbalanceReport(strXMLBSUNames, "RPTPDCRECEIVABLEVOUCHER")
            Case "A750011" 'Trial Balance
                PdcCancellationReport()
        End Select

    End Sub 
 
     
    ''' <summary>
    ''' ''''''
    ''' </summary>
    ''' <param name="strXMLBSUNames"></param>
    ''' <remarks></remarks>
    Private Sub GenerateTrialbalanceReport(ByVal strXMLBSUNames As String, Optional ByVal p_receivable As String = "")

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand
        If p_receivable = "" Then
            cmd = New SqlCommand("RPTPDCPAYABLEVOUCHER", objConn)
        Else
            cmd = New SqlCommand("RPTPDCRECEIVABLEVOUCHER", objConn)
        End If

        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        'Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        'sqlpJHD_FYEAR.Value = Session("financialyear")
        'cmd.Parameters.Add(sqlpJHD_FYEAR)


        Dim sqlpFROMDOCDT As New SqlParameter("@DOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        sqlpConsolidation.Value = chkConsolidation.Checked
        cmd.Parameters.Add(sqlpConsolidation)

        Dim sqlpGroupCurrency As New SqlParameter("@bShowinGrpCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            'Session("BSUNames") = strXMLBSUNames
            'Session("rptFromDate") = txtFromDate.Text
            ''Session("rptToDate") = txtToDate.Text
            'Session("rptConsolidation") = chkConsolidation.Checked
            'Session("rptGroupCurrency") = chkGroupCurrency.Checked

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            ''params("ToDate") = txtToDate.Text
            If p_receivable = "" Then
                params("header") = "PDC Payable Voucher Report"
            Else
                params("header") = "PDC Receivable Voucher Report"
            End If

            'params("parent") = ""
            'params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptPDCPayableVoucher.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                Response.Redirect("rptviewer.aspx", True)
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Private Sub PdcCancellationReport()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand
        
        cmd = New SqlCommand("[PDCVOUCHER_CANCEL]", objConn)


        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = UsrBSUnits1.GetSelectedNode("|")
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDT", SqlDbType.VarChar)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@TODT", SqlDbType.VarChar)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
   
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("ReportHead") = "PDC CANCELLATION DETAILS FOR THE PERIOD :" & txtFromDate.Text & " TO " & txtToDate.Text
          
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/PdcVouceherCancelReport.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                Response.Redirect("rptviewer.aspx", True)
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub
    'Generates the XML for BSUnit
    Private Function GenerateBSUXML(ByVal BSUIDs As String) As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Try
            BSUDetails = xmlDoc.CreateElement("BSU_DETAILS")
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement("BSU_DETAIL")
                XMLEBSUID = xmlDoc.CreateElement("BSU_ID")
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    'Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
    '    h_BSUID.Value += "||" + txtBSUNames.Text.Replace(",", "||")
    '    FillBSUNames(h_BSUID.Value)
    'End Sub

    'Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
    '    grdBSU.PageIndex = e.NewPageIndex
    '    FillBSUNames(h_BSUID.Value)
    'End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = Session("sBSUID")
            End If
            ' grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub
End Class
