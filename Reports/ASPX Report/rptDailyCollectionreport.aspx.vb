Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Reports_ASPX_Report_rptDailyreport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim accountID, bankID As String
    Dim fromDate, toDate As String
    Shared MainMnu_code As String

    Shared bconsolidation As Boolean



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If
        Page.Title = OASISConstants.Gemstitle
        'Add the readOnly attribute to the Date textBox So that the Text property value is accessible in the form
        'It is not giving the values if you make the ReadOnly property to TRUE
        'So you have to do this in page_Load()
        'txtToDate.Attributes.Add("ReadOnly", "Readonly")
        'txtFromDate.Attributes.Add("ReadOnly", "Readonly")

        'checks whether the BSUnit is selected if it is selected h_BSUID.Value will be having
        'the IDs of the BS Unit. "FillBSUNames()" function will set the BSU names
        If h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
            h_BSUID.Value = Session("sBsuid")
        End If
        FillBSUNames(h_BSUID.Value)

        If Not IsPostBack Then
            UtilityObj.NoOpen(Me.Header)
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Select Case MainMnu_code
                Case "A350040" ' '
                    'tr_expenceincome.Visible = False A650045
                    lblCaption.Text = "Periodic Collection Analysis"
                Case "A650045" ' '
                    'tr_expenceincome.Visible = False A650045
                    lblCaption.Text = "Periodic Collection Analysis"

            End Select
            txtToDate.Text = UtilityObj.GetDiplayDate()
            txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))

        End If

        If bconsolidation Then
            chkGroupCurrency.Enabled = True
        Else
            chkGroupCurrency.Enabled = False
            chkGroupCurrency.Checked = True
        End If
        lblError.Text = ""

    End Sub




    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
        str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        bconsolidation = IIf(ds.Tables(0).Rows.Count = 1, True, False)
        'grdBSU.DataSource = ds
        'grdBSU.DataBind()
        Return True
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim strfDate As String = txtFromDate.Text.Trim
        Dim strtDate As String = txtToDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        str_err = str_err & DateFunctions.checkdate(strtDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtFromDate.Text = strfDate
            txtToDate.Text = strtDate
        End If
        Dim strXMLBSUNames As String
        'Generate the XML in the form <BSU_DETAILS><BSU_ID>IDhere</BSU_ID></BSU_DETAILS>
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        strXMLBSUNames = GenerateBSUXML(h_BSUID.Value) 'txtBSUNames.Text)
        'MainMnu_code = "A650015"
        Select Case MainMnu_code
            Case "A350040" 'day
                GenerateTrialbalanceReport(strXMLBSUNames)

            Case "A650045" 'day
                GenerateTrialbalanceReport(strXMLBSUNames)

        End Select

    End Sub


    ''' <summary>
    ''' ''''''
    ''' </summary>
    ''' <param name="strXMLBSUNames"></param>
    ''' <remarks></remarks>
    Private Sub GenerateTrialbalanceReport(ByVal strXMLBSUNames As String) ', ByVal p_mode As String

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand
        'If p_receivable = "" Then
        cmd = New SqlCommand("RPTCollectionSummaryForaPeriod", objConn)
        'Else
        '    cmd = New SqlCommand("RPTGETTPnLMovement", objConn)
        'End If

        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@JHD_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpDTTO As New SqlParameter("@TODOCDT", SqlDbType.VarChar)
        sqlpDTTO.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpDTTO)

        ''Dim sqlpID As New SqlParameter("@ID", SqlDbType.VarChar)
        ''sqlpID.Value = ""
        ''cmd.Parameters.Add(sqlpID)

        ''Dim sqlpType As New SqlParameter("@Type", SqlDbType.VarChar)
        ''sqlpType.Value = p_mode
        ''cmd.Parameters.Add(sqlpType)

        Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        sqlpConsolidation.Value = chkConsolidation.Checked
        cmd.Parameters.Add(sqlpConsolidation)

        Dim sqlpGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        adpt.SelectCommand = cmd


        objConn.Open()
        adpt.Fill(ds)
        If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            'Session("BSUNames") = strXMLBSUNames
            'Session("rptFromDate") = txtFromDate.Text
            ''Session("rptToDate") = txtToDate.Text
            'Session("rptConsolidation") = chkConsolidation.Checked
            'Session("rptGroupCurrency") = chkGroupCurrency.Checked

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text

            params("toDate") = txtToDate.Text

            Select Case MainMnu_code
                Case "A350040" 'p/l
                    repSource.ResourceName = "../RPT_Files/rptCollectionsummaryForaPeriod.rpt"
                    params("header") = "Periodic Collection Analysis"
                Case "A650045" 'p/l
                    repSource.ResourceName = "../RPT_Files/rptchartCollectionsummaryForaPeriod.rpt"
                    params("header") = "Periodic Collection Analysis - Contribution By Collection Mode"
                    ' ''Case "A650040" 'Trial Balance'income/exp
                    ' ''    repSource.ResourceName = "../RPT_Files/rptChartMonthlyincome.rpt"
                    ' ''    params("header") = "Income Movement"
                    ' ''    params("subheader") = "Income"
            End Select
            'repSource.GetDataSourceFromCommand = False
            repSource.Parameter = params
            repSource.Command = cmd
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                Response.Redirect("rptviewer.aspx", True)
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub
    'Generates the XML for BSUnit
    Private Function GenerateBSUXML(ByVal BSUIDs As String) As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Try
            BSUDetails = xmlDoc.CreateElement("BSU_DETAILS")
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement("BSU_DETAIL")
                XMLEBSUID = xmlDoc.CreateElement("BSU_ID")
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    'Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
    '    h_BSUID.Value += "||" + txtBSUNames.Text.Replace(",", "||")
    '    FillBSUNames(h_BSUID.Value)
    'End Sub

    'Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
    '    grdBSU.PageIndex = e.NewPageIndex
    '    FillBSUNames(h_BSUID.Value)
    'End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = Session("sBSUID")
            End If
            'grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub
End Class
