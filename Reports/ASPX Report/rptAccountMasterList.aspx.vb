Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_rptAccountMasterList
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
        'If Page.IsPostBack = False Then
        '    If isPageExpired() Then
        '        Response.Redirect("expired.htm")
        '    Else
        '        Session("TimeStamp") = Now.ToString
        '        ViewState("TimeStamp") = Now.ToString
        '    End If
        'End If

        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            lblError.Text = ""
        End If

        If Not IsPostBack Then
            Page.Title = OASISConstants.Gemstitle
            BindGroupMaster()
            BindControlAccount()
            BindType()
            Select Case MainMnu_code
                Case "A850010"
                    lblrptCaption.Text = "Account Master List"
                Case "A850015"
                    lblrptCaption.Text = "Supplier Account List"
                Case "A850025" 'Group
                    GenerateGroupList()
                Case "A850020" 'Sub Group
                    lblrptCaption.Text = "Sub Group List"
                    trSubGroup.Visible = False
                    trCntrlAcc.Visible = False
                    trType.Visible = False
                Case "A850030" 'Control Account
                    lblrptCaption.Text = "Control Account List"
                    trCntrlAcc.Visible = False
                    trType.Visible = False
                Case "A850035" 'Day/Month
                    GenerateDayendMonthend()
                Case "A850040" 'Users online
                    LISTONLINEUSERS()
            End Select
        End If
    End Sub

    Private Sub LISTONLINEUSERS()
        Dim cmd As New SqlCommand
        Dim params As New Hashtable
        cmd.CommandText = "exec fees.LISTONLINEUSERS"
        cmd.CommandType = Data.CommandType.Text
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
            Dim repSource As New MyReportClass
            params("UserName") = Session("sUsr_name")
            params("OnDate") = Now
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptGetOnlineUsers.rpt"
            Session("ReportSource") = repSource
            Response.Redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
    End Sub

    Private Sub GenerateDayendMonthend()
        Dim cmd As New SqlCommand
        Dim params As New Hashtable
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        'Dim ds As New DataSet
        cmd.CommandText = "exec GetDayMonthendStatus"
        cmd.CommandType = Data.CommandType.Text

        'Dim retData As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, cmd.CommandText, Nothing)
        'If Not retData IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            params("UserName") = Session("sUsr_name")
            params("OnDate") = Now.Date
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptGetDayMonthendStatus.rpt"
            Session("ReportSource") = repSource
            Response.Redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
    End Sub

    Private Sub BindGroupMaster()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String = "SELECT GPM_ID, GPM_DESCR FROM ACCGRP_M"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If (Not ds Is Nothing) And (Not ds.Tables(0) Is Nothing) Then
            Dim drow As DataRow = ds.Tables(0).NewRow()
            drow(0) = "ALL"
            drow(1) = "ALL"
            ds.Tables(0).Rows.Add(drow)
        End If
        cmbGroup.Items.Clear()
        cmbGroup.DataSource = ds.Tables(0)
        cmbGroup.DataTextField = "GPM_DESCR"
        cmbGroup.DataValueField = "GPM_ID"
        cmbGroup.DataBind()
        cmbGroup.SelectedValue = "ALL"
        BindSubGroupMaster()
    End Sub

    Private Sub BindSubGroupMaster()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String = String.Empty
        If cmbGroup.SelectedItem.Value = "ALL" Then
            str_Sql = "SELECT SGP_ID, SGP_DESCR FROM ACCSGRP_M"
        Else
            str_Sql = "SELECT SGP_ID, SGP_DESCR FROM ACCSGRP_M WHERE SGP_GPM_ID = '" + cmbGroup.SelectedItem.Value + "'"
        End If
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If (Not ds Is Nothing) And (Not ds.Tables(0) Is Nothing) Then
            Dim drow As DataRow = ds.Tables(0).NewRow()
            drow(0) = "ALL"
            drow(1) = "ALL"
            ds.Tables(0).Rows.Add(drow)
        End If
        cmbSubGroup.Items.Clear()
        cmbSubGroup.DataSource = ds.Tables(0)
        cmbSubGroup.DataTextField = "SGP_DESCR"
        cmbSubGroup.DataValueField = "SGP_ID"
        cmbSubGroup.SelectedValue = "ALL"
        cmbSubGroup.DataBind()
    End Sub

    Private Sub BindControlAccount()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String = "SELECT ACT_ID, ACT_NAME FROM ACCOUNTS_M WHERE ACT_Bctrlac = 1"

        If cmbSubGroup.SelectedItem.Value = "ALL" Then
            str_Sql = "SELECT ACT_ID, ACT_NAME FROM ACCOUNTS_M WHERE ACT_Bctrlac = 1"
        Else
            str_Sql = "SELECT ACT_ID, ACT_NAME FROM ACCOUNTS_M WHERE ACT_Bctrlac = 1 and ACT_SGP_ID = '" + cmbSubGroup.SelectedItem.Value + "'"
        End If

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If (Not ds Is Nothing) And (Not ds.Tables(0) Is Nothing) Then
            Dim drow As DataRow = ds.Tables(0).NewRow()
            drow(0) = "ALL"
            drow(1) = "ALL"
            ds.Tables(0).Rows.Add(drow)
        End If
        cmbCtrlAccount.Items.Clear()
        cmbCtrlAccount.DataSource = ds.Tables(0)
        cmbCtrlAccount.DataTextField = "ACT_NAME"
        cmbCtrlAccount.DataValueField = "ACT_ID"
        cmbCtrlAccount.DataBind()
        cmbCtrlAccount.SelectedValue = "ALL"
    End Sub

    Sub BindType()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT DISTINCT ACT_TYPE FROM ACCOUNTS_M "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If (Not ds Is Nothing) And (Not ds.Tables(0) Is Nothing) Then
                Dim drow As DataRow = ds.Tables(0).NewRow()
                drow(0) = "ALL"
                ds.Tables(0).Rows.Add(drow)
            End If
            cmbType.DataSource = ds.Tables(0)
            cmbType.DataTextField = "ACT_TYPE"
            cmbType.DataValueField = "ACT_TYPE"
            cmbType.DataBind()
            cmbType.SelectedValue = "ALL"

        Catch ex As Exception

        End Try
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Select Case MainMnu_code
            Case "A850010", "A850015"
                GenerateAccountMastersList()
            Case "A850025", "A850020"
                GenerateSubGroupMastersList()
            Case "A850030" 'Control Account
                GenerateAccountMastersList()
        End Select
    End Sub

    Private Sub GenerateGroupList()
        Dim cmd As New SqlCommand
        Dim params As New Hashtable
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        'Dim ds As New DataSet
        cmd.CommandText = "select GPM_ID, GPM_DESCR from dbo.ACCGRP_M"
        cmd.CommandType = Data.CommandType.Text

        'Dim retData As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, cmd.CommandText, Nothing)
        'If Not retData IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            params("UserName") = Session("sUsr_name")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptGrpMaster.rpt"
            Session("ReportSource") = repSource
            Response.Redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
    End Sub
    Private Sub GenerateSubGroupMastersList()
        Dim cmd As New SqlCommand
        Dim params As New Hashtable
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet

        Dim strFilter As String = String.Empty
        If cmbGroup.SelectedItem.Value <> "ALL" Then
            strFilter += " AND ACCGRP_M.GPM_ID ='" & cmbGroup.SelectedItem.Value & "'"
        End If

        If cmbSubGroup.Items.Count > 0 Then
            If cmbSubGroup.SelectedItem.Value <> "ALL" Then
                strFilter += " AND ACCSGRP_M.SGP_ID ='" & cmbSubGroup.SelectedItem.Value & "'"
            End If
        End If

        If cmbType.Items.Count > 0 Then
            If cmbType.SelectedItem.Value <> "ALL" Then
                strFilter += " AND ACT_TYPE ='" & cmbType.SelectedItem.Text & "'"
            End If
        End If

        cmd.CommandText = "SELECT ACCGRP_M.GPM_DESCR, ACCGRP_M.GPM_ID, ACCSGRP_M.SGP_DESCR, " _
        & "ACCSGRP_M.SGP_TYPE FROM ACCSGRP_M LEFT OUTER JOIN " _
        & " ACCGRP_M ON ACCSGRP_M.SGP_GPM_ID = ACCGRP_M.GPM_ID"
        cmd.CommandType = Data.CommandType.Text

        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            params("UserName") = Session("sUsr_name")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptSubGrpMaster.rpt"
            Session("ReportSource") = repSource
            Response.Redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
    End Sub
    Private Sub GenerateAccountMastersList()
        Dim cmd As New SqlCommand
        Dim params As New Hashtable
        Dim strFilter As String = String.Empty
        If cmbGroup.SelectedItem.Value <> "ALL" Then
            strFilter += " AND GPM_ID ='" & cmbGroup.SelectedItem.Value & "'"
        End If

        If cmbSubGroup.Items.Count > 0 Then
            If cmbSubGroup.SelectedItem.Value <> "ALL" Then
                strFilter += " AND SGP_ID ='" & cmbSubGroup.SelectedItem.Value & "'"
            End If
        End If

        If cmbCtrlAccount.Visible = True Then
            If cmbCtrlAccount.Items.Count > 0 Then
                If cmbCtrlAccount.SelectedItem.Value <> "ALL" Then
                    strFilter += " AND CTRLACC ='" & cmbCtrlAccount.SelectedItem.Text & "'"
                End If
            End If
        End If

        If cmbType.Visible = True Then
            If cmbType.Items.Count > 0 Then
                If cmbType.SelectedItem.Value <> "ALL" Then
                    strFilter += " AND ACT_TYPE ='" & cmbType.SelectedItem.Text & "'"
                End If
            End If
        End If

        Select Case MainMnu_code
            Case "A850010"
                cmd.CommandText = "SELECT * FROM vw_OSA_ChartofAccounts WHERE ACT_FLAG <> 'S'" & strFilter
                params("ReportCaption") = "Account Master List"
            Case "A850015"
                cmd.CommandText = "SELECT * FROM vw_OSA_ChartofAccounts WHERE ACT_FLAG = 'S'" & strFilter
                params("ReportCaption") = "Supplier Account List"
            Case "A850030" 'Control Account
                cmd.CommandText = "SELECT * FROM vw_OSA_ChartofAccounts where act_bctrlac = 'true' " & strFilter
                params("ReportCaption") = "Control Account List"
        End Select
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        'Dim ds As New DataSet
        'SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        'If SqlHelper.ExecuteScalar(str_conn, CommandType.Text, cmd.CommandText, Nothing) IsNot Nothing Then
        'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            params("UserName") = Session("sUsr_name")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptAccountMasterList.rpt"
            Session("ReportSource") = repSource
            'Response.Redirect("rptviewer.aspx", True)
            ReportLoadSelection()
        Else
            lblError.Text = "No Records with specified condition"
        End If
    End Sub
    Private Sub InitializeFilter(ByRef filter As String)
        If filter IsNot String.Empty Or filter IsNot "" Then
            If Not filter.StartsWith(" WHERE") Then
                filter = filter.Insert(0, " WHERE")
            End If
            If Not filter.EndsWith("WHERE") Then
                filter = filter.Insert(filter.Length, " AND")
            End If
        End If
    End Sub

    Protected Sub cmbGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbGroup.SelectedIndexChanged
        BindSubGroupMaster()
    End Sub

    Protected Sub cmbSubGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSubGroup.SelectedIndexChanged
        BindControlAccount()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
