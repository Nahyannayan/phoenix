<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptViewPostingDetails.aspx.vb" Inherits="Reports_ASPX_Report_rptViewPostingDetails" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function GetDocNo() {

            var result;
            var type = document.getElementById('<%=cmbDocType.ClientID %>').value;
            result = radopen("../../Accounts/selDocNo.aspx?type=" + type, "pop_up");
           <%-- if(result != "" && result != undefined)
            {
                document.getElementById('<%=txtDocNo.ClientID %>').value = result;//NameandCode[0];
            }
            return false;--%>
       }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(NameandCode);
                document.getElementById('<%=txtDocNo.ClientID %>').value = arg.NameandCode;//NameandCode[0];
                __doPostBack('<%= txtDocNo.ClientID%>', 'TextChanged');
            }
        }
        Report


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="View Posting Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" style="width: 100%;">

                    <tr>
                        <td align="left" valign="top" style="width: 20%">
                            <span class="field-label">Document Type</span></td>

                        <td align="left" style="text-align: left" valign="top" width="30%">
                            <asp:DropDownList ID="cmbDocType" runat="server">
                            </asp:DropDownList></td>
                  
                        <td align="left" style="width: 20%">
                            <span class="field-label">Document No</span></td>

                        <td align="left" style="text-align: left" width="20%">
                            <asp:TextBox ID="txtDocNo" runat="server"></asp:TextBox>
                            <asp:ImageButton
                                ID="ImageButton1" OnClientClick="GetDocNo(); return false;" runat="server" ImageUrl="~/Images/forum_search.gif" /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" style="text-align: center;">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_mode" runat="server" />
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>

