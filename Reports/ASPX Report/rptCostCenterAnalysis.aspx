<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptCostCenterAnalysis.aspx.vb" Inherits="Reports_ASPX_Report_rptCostCenterAnalysis" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked;


            if (CheckBoxIDs != null) {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                    ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }



        function GetAccounts() {

            var NameandCode;
            var result;
            var mode = 'general';
            if (mode == 'bank') {
                result = radopen("../../Accounts/accjvshowaccount.aspx?bankcash=b&multiSelect=true&forrpt=1", "pop_up");
            }
            else if (mode == 'cash') {
                result = radopen("../../Accounts/accjvshowaccount.aspx?bankcash=c&multiSelect=true&forrpt=1", "pop_up");
            }
            else if (mode == 'party') {
                result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=s&forrpt=1", "pop_up");
            }
            else if (mode == 'general') {
                result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=g&forrpt=1", "pop_up");
            }
            else //if(mode == 'general')
            {
                result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&forrpt=1", "pop_up");
            }
           <%-- if (result != "") {
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();

            }--%>
        }



        
        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = arg.NameandCode;//NameandCode[0];
                document.forms[0].submit();
            }
        }


        function getOtherCostCenter() {


            <%--            if ('<%=ddCostcenter.Selecteditem.value %>' == 'others') {
              
            }
            else {
              
            }--%>

            var NameandCode;
            var result;
            var url = 'PickCostObject.aspx?ccsmode=' + '<%=ddCostcenter.Selecteditem.value %>' + '&vid=' + '<%=Request.QueryString("vid") %>';
            url = url + '&dt=' + '12-sep-2007';
            //alert(url);
            result = radopen(url, "pop_up1");
          <%--  document.getElementById('<%=h_Memberids.ClientID %>').value = result;
            document.forms[0].submit();--%>
        }

        function OnClientClose1(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Memberids.ClientID %>').value = arg.NameandCode;
            document.forms[0].submit();
            }
        }


        function getTEST() {



            var urlName = "PickCodes.aspx"
            var OthSession = '<%=Session("CostOTH")%>'
            var FromDate = document.getElementById('<%=txtFromDate.ClientID %>').value
            var costCenter = document.getElementById('<%=ddCostcenter.ClientID %>').value
            if (costCenter == OthSession)
                urlName = "OthercostcenterShow.aspx"


            var NameandCode;
            var result;
            var url = urlName + '?ccsmode=' + costCenter + '&vid=' + '<%=Request.QueryString("vid") %>';
            url = url + '&dt=' + FromDate;
            //alert(url);
            result = radopen(url, "pop_up2");

          <%--  if (result == '' || result == undefined) {
                return false;
            }

            document.getElementById('<%=h_Memberids.ClientID %>').value = result;

            document.forms[0].submit();--%>

        }
        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Memberids.ClientID %>').value = arg.NameandCode;
                document.forms[0].submit();
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Sub Ledger Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" style="width: 100%;">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">From Date</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
                        <td align="left" width="20%"><span class="field-label">To Date</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Drill by</span></td>

                        <td align="left" width="30%">
                            <asp:RadioButton ID="rbAccount" runat="server" GroupName="cc" Text="Account" EnableTheming="True" Checked="True" />
                            <asp:RadioButton ID="rbCostcenter" runat="server" GroupName="cc" Text="Cost Center" Enabled="False" /></td>
                    </tr>
                    <tr id="trBSUName" runat="server">
                        <td align="left" width="20%" valign="top"><span class="field-label">Business Unit</span></td>

                        <td align="left" colspan="3" valign="top">
                            <div class="checkbox-list">
                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%" valign="top">
                            <asp:Label ID="lblBankCash" runat="server" Text="Account" CssClass="field-label"></asp:Label></td>

                        <td align="left" colspan="2" valign="top">
                            <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the ACT ID you want to Add to the Search and click on Add)"></asp:Label>
                            <asp:TextBox ID="txtBankNames" runat="server"></asp:TextBox>
                            <asp:LinkButton ID="lblAddActID" runat="server">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetAccounts(); return false;" /><br />
                            <asp:GridView ID="grdACTDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                PageSize="5">
                                <Columns>
                                    <asp:TemplateField HeaderText="ACT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ACT_Name" HeaderText="ACT Name" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_small" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="tr_CostCenter" runat="server">
                        <td align="left" width="20%"><span class="field-label">Cost Center</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddCostcenter" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:ImageButton ID="imgPickOther" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getOtherCostCenter(); return false;" />
                            <asp:ImageButton ID="imgPickcost" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getTEST(); return false;" /></td>
                    </tr>
                    <tr id="showBy" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Show By</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddAccountGroup" runat="server" Width="151px" AutoPostBack="True">
                                <asp:ListItem Value="ACC">By Account</asp:ListItem>
                                <asp:ListItem Value="CTRL">By Control Account</asp:ListItem>
                                <asp:ListItem Value="SUB">By Sub Group</asp:ListItem>
                                <asp:ListItem Value="GRP">By Group</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="trChkBoxes" runat="server">
                        <td align="left" colspan="4">
                            <asp:CheckBox ID="chkGroupCurrency" runat="server" Text="View in Group Currency" />
                            <asp:CheckBox ID="chkConsolidation" runat="server" Text="Consolidation" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:HiddenField ID="h_ACTIDs" runat="server" />
                <asp:HiddenField ID="H_COSTIDS" runat="server" />
                <asp:HiddenField ID="h_Mode" runat="server" />
                <asp:HiddenField ID="h_Memberids" runat="server" />
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

