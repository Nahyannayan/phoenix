<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPiandlemovement.aspx.vb" Inherits="Reports_ASPX_Report_rptPiandlemovement"  %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">


    <script language="javascript" type="text/javascript"> 
           
           function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 229px; ";
            sFeatures+="dialogHeight: 234px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx?dt="+ document.getElementById('<%=txtfromDate.ClientID %>').value,"", sFeatures);
            if (result=='' || result==undefined)
            {
                return false;
            }
            switch(txtControl)
            {
              case 0:
                document.getElementById('<%=txtfromDate.ClientID %>').value=result;
                break;
              
            }    
           }
   
         function GetBSUName()
          {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 450px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx","", sFeatures)
            if(result != "")
            {
              document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
              document.forms[0].submit();
            }
         }
    </script>
        
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
        
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0" >
        <tr  class="subheader_img">
            <td align="left" colspan="8" valign="middle" style="height: 19px">                
                        <asp:Label ID="lblCaption" runat="server"></asp:Label> 
            </td>
        </tr>
        <tr>
            <td align="left" class="matters">
                As On</td>
            <td class="matters" >
                :</td>
            <td align="left" class="matters" colspan="6" style="width: 189px; height: 1px; text-align: left">
                <asp:TextBox ID="txtFromDate" runat="server" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                <asp:HiddenField ID="h_BSUID" runat="server" />
</td>
              
        </tr>
       
        <tr id="tr_expenceincome" runat="server"> 
        <td align="left" class="matters"> Group by</td>
        <td class="matters"> :</td>
        <td colspan="6" align="left"> &nbsp;<asp:DropDownList ID="ddGroupby" runat="server" SkinID="DropDownListNormal">
            <asp:ListItem Value="5">Control Account</asp:ListItem>
            <asp:ListItem Value="4">Sub Group</asp:ListItem>
            </asp:DropDownList></td>
        
        </tr>
        <tr style="color: #1b80b6" id = "trBSUName" runat = "server">
            <td align="left" valign = "top" class="matters" >
                Business Unit</td>
            <td class="matters" valign = "top">
                :</td>
            <td align="left" class="matters" colspan="6" valign="top">
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
            </td>
        </tr>
        <tr style="color: #1b80b6" id = "trChkBoxes" runat = "server">
            <td align="left" class="matters" colspan="8" style="height: 7px">
                <asp:CheckBox ID="chkGroupCurrency" runat="server" Text="View in Group Currency" /><br />
                <asp:CheckBox ID="chkConsolidation" runat="server" Text="Consolidation" /></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="text-align: right">
                &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table> 
     <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
         Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
     </ajaxToolkit:CalendarExtender>
     <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
         Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
     </ajaxToolkit:CalendarExtender>
</asp:Content>
