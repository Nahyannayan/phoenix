Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class Reports_ASPX_Report_rptTreasurytransfer
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim accountID, bankID As String
    Dim fromDate, toDate As String
    Dim MainMnu_code As String
    Dim MainObj As Mainclass = New Mainclass()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnSubmit)

        If Not IsPostBack Then
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Session("user") = Session("sUsr_name")

            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If


            UtilityObj.NoOpen(Me.Header)

            drpBSUnit.DataSource = UtilityObj.GetBusinessUnits(Session("sUsr_name"))
            drpBSUnit.DataTextField = "BSU_NAME"
            drpBSUnit.DataValueField = "BSU_ID"
            drpBSUnit.DataBind()
            drpBSUnit.Items.Insert(0, " ")
            drpBSUnit.Items(0).Value = "0"
            drpBSUnit.SelectedValue = Session("sbsuid")

            drpUnitTo.DataSource = UtilityObj.GetBusinessUnits(Session("sUsr_name"))
            drpUnitTo.DataTextField = "BSU_NAME"
            drpUnitTo.DataValueField = "BSU_ID"
            drpUnitTo.DataBind()
            drpUnitTo.Items.Insert(0, " ")
            drpUnitTo.Items(0).Value = "0"

            lblRepCaption.Text = "Treasury Transfer Details"
            h_BSUID.Value = IIf(Session("SBSUID") <> "", Session("SBSUID").ToString(), String.Empty)
            'txtBSUID.Text = IIf(Session("BSU_Name") IsNot Nothing, Session("BSU_Name"), String.Empty)
            If Cache("fromDate") IsNot Nothing And Cache("toDate") IsNot Nothing Then
                txtfromDate.Text = Cache("fromDate")
                txtToDate.Text = Cache("toDate")
            Else
                txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtfromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            End If

            txtfromDate.Attributes.Add("onBlur", "checkdate(this)")
            txtToDate.Attributes.Add("onBlur", "checkdate(this)")
            radPaid.Visible = False
            radReceivable.Visible = False
            radUnitTypeall.Visible = False



        End If
    End Sub

    

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        txtfromDate.Text = Format("dd/MMM/yyyy", txtfromDate.Text)
        txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
        getData()
        Session("FDate") = txtfromDate.Text
        Session("TDate") = txtToDate.Text
        Session("selBSUID") = drpBSUnit.SelectedValue
        

        
    End Sub
    Private Sub getData()

        Try

        
            Dim delStatus As Int32 = 1
            Dim PostStatus As String = "ALL"
            Dim UniteType As String = "ALL"
            Dim SQLQUERY As String = ""
            Dim selectUnit As String = drpBSUnit.SelectedValue.ToString()
            Dim selectBank As String = drpBank.SelectedValue.ToString()

            
            SQLQUERY = " SELECT INTERUNIT_TRANSFER.ITF_DATE, INTERUNIT_TRANSFER.ITF_DOCNO AS IRF_ID, vw_OSO_BUSINESSUNIT_M.BSU_NAME AS FROMUNIT, " _
                        & " ACCOUNTS_M.ACT_NAME AS FROMBANK, vw_OSO_BUSINESSUNIT_M_1.BSU_NAME AS TOUNIT, ACCOUNTS_M_1.ACT_NAME AS TOBANK, " _
                        & " INTERUNIT_TRANSFER.ITF_AMOUNT, INTERUNIT_TRANSFER.ITF_NARRATION FROM INTERUNIT_TRANSFER INNER JOIN" _
                        & " vw_OSO_BUSINESSUNIT_M ON INTERUNIT_TRANSFER.ITF_CRBSU_ID = vw_OSO_BUSINESSUNIT_M.BSU_ID INNER JOIN" _
                        & " ACCOUNTS_M ON INTERUNIT_TRANSFER.ITF_CR_BANK_ACT_ID = ACCOUNTS_M.ACT_ID INNER JOIN" _
                        & " vw_OSO_BUSINESSUNIT_M AS vw_OSO_BUSINESSUNIT_M_1 ON " _
                        & " INTERUNIT_TRANSFER.ITF_DRBSU_ID = vw_OSO_BUSINESSUNIT_M_1.BSU_ID INNER JOIN" _
                        & " ACCOUNTS_M AS ACCOUNTS_M_1 ON INTERUNIT_TRANSFER.ITF_DR_BANK_ACT_ID = ACCOUNTS_M_1.ACT_ID" _
                        & " WHERE INTERUNIT_TRANSFER.ITF_DATE >= '" & txtfromDate.Text & "' AND INTERUNIT_TRANSFER.ITF_DATE <= '" & txtToDate.Text & "' "
            '& " WHERE INTERUNIT_TRANSFER.ITF_DATE BETWEEN DATEADD(DAY,-1,'" & txtfromDate.Text & "')  AND DATEADD(DAY,1,'" & txtToDate.Text & "') "

            If radStatusAll.Checked Then
                SQLQUERY += " AND ITF_bDELETED = 0 "
            ElseIf radStatusOpen.Checked Then
                SQLQUERY += " AND ITF_bDELETED = 0  AND ITF_bPOSTED = 0 "
            ElseIf radStatusPosted.Checked Then
                SQLQUERY += " AND ITF_bDELETED = 0  AND ITF_bPOSTED = 1 "
            ElseIf radStatusDeleted.Checked Then
                SQLQUERY += " AND ITF_bDELETED = 1 "
            End If
            

            If Not drpUnitTo.SelectedValue.Equals("0") Then
                SQLQUERY += " AND INTERUNIT_TRANSFER.ITF_DRBSU_ID = '" & drpUnitTo.SelectedValue.ToString() & "' "
                If Not drpBankTo.SelectedValue.Equals("0") And Not drpBankTo.SelectedValue.Equals("") Then
                    SQLQUERY += " AND ( INTERUNIT_TRANSFER.ITF_DR_BANK_ACT_ID = '" & drpBankTo.SelectedValue.ToString() & "'  )"
                End If
            End If

            If Not drpBSUnit.SelectedValue.Equals("0") Then
                SQLQUERY += " AND INTERUNIT_TRANSFER.ITF_CRBSU_ID = '" & drpBSUnit.SelectedValue.ToString() & "' "
                If Not selectBank.Equals("0") And selectBank <> "" Then
                    SQLQUERY += " AND ( INTERUNIT_TRANSFER.ITF_CR_BANK_ACT_ID = '" & selectBank & "'  )"
                End If
            End If

            SQLQUERY += " ORDER BY " & drpFillter.SelectedValue.ToString()


            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim cmd As New SqlCommand
            'cmd.CommandText = "RPTTreasuryTransfer"
            cmd.CommandText = SQLQUERY
            cmd.Connection = New SqlConnection(str_conn)
            'cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandType = CommandType.Text
            Dim repSource As New MyReportClass
            Dim params As New Hashtable


            params("userName") = Session("sUsr_name")
            params("ReportHead") = "Interunit Treasury Transfer Date From :" & txtfromDate.Text & ".To." & txtToDate.Text
            repSource.Parameter = params
            repSource.VoucherName = "TreasuryTransfer"
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            repSource.ResourceName = "../RPT_Files/TreasurytransferReport.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                Response.Redirect("rptviewer.aspx", True)
            End If
            'End If
            'End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    



    Public Sub compareDate(ByVal sender As Object, ByVal value As ServerValidateEventArgs)
        Dim dtfrom, dtto As DateTime
        If ((txtfromDate.Text <> "") And (txtfromDate.Text <> String.Empty)) Or _
        ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then
            dtfrom = Convert.ToDateTime(txtfromDate.Text)
            dtto = Convert.ToDateTime(txtToDate.Text)
            If dtfrom < dtto Then
                value.IsValid = True
            Else
                value.IsValid = False
            End If
        End If
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnSubmit_Click(sender, e)
    End Sub



    
    Protected Sub drpBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim SqlString As String
        SqlString = "SELECT ACT_ID ,ACT_NAME FROM ACCOUNTS_M WHERE ACT_BSU_ID Like   '%" & drpBSUnit.SelectedValue.ToString() & "%' AND ACT_BANKCASH='B' AND ACT_Bctrlac='False' AND isnull(ACT_BACTIVE,0)=1 ORDER BY ACT_NAME"
        Dim _table As DataTable = MainObj.ListRecords(SqlString, "MainDb")
        drpBank.DataSource = _table
        drpBank.DataTextField = "ACT_NAME"
        drpBank.DataValueField = "ACT_ID"
        drpBank.DataBind()
        drpBank.Items.Insert(0, " ")
        drpBank.Items(0).Value = "0"
        drpBank.SelectedValue = "0"

    End Sub

    Protected Sub drpBankTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim SqlString As String
        SqlString = "SELECT ACT_ID ,ACT_NAME FROM ACCOUNTS_M WHERE ACT_BSU_ID Like   '%" & drpBankTo.SelectedValue.ToString() & "%' AND ACT_BANKCASH='B' AND ACT_Bctrlac='False' AND isnull(ACT_BACTIVE,0)=1 ORDER BY ACT_NAME"
        Dim _table As DataTable = MainObj.ListRecords(SqlString, "MainDb")
        drpBankTo.DataSource = _table
        drpBankTo.DataTextField = "ACT_NAME"
        drpBankTo.DataValueField = "ACT_ID"
        drpBankTo.DataBind()
        drpBankTo.Items.Insert(0, " ")
        drpBankTo.Items(0).Value = "0"
        drpBankTo.SelectedValue = "0"
    End Sub
End Class

