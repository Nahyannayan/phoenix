<%@ Page Language="VB" AutoEventWireup="false" CodeFile="rptReportViewer_Popup.aspx.vb" Inherits="rptReportViewer_Popup" title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


   


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
  <link href="../../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/example.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
<table style="width:700px;height:100%">
        <tr style="width:700px;height:100%">
            <td align="left" valign = "top" style="width: 696px"> 
     <CR:CrystalReportViewer ID="crv" runat="server" AutoDataBind="true" ToolPanelView="None"
        EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False" ReportSourceID="rs"
        ReuseParameterValuesOnRefresh="True" PrintMode="ActiveX" Width="350px" Height="50px" ShowAllPageIds="True" HasToggleGroupTreeButton="False" />
   
       &nbsp;<CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>
      </td>
        </tr>
    </table>
</div>
</form>
</body>
</html>


