<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptGetMisPLMonthly.aspx.vb" Inherits="Reports_ASPX_Report_rptGetMisPLMonthly" title="Untitled Page" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">

    </script>
   <table align="center" style="width: 80%;" cellpadding="0" cellspacing="0">  
       <tr align=left >
           <td><asp:ValidationSummary   ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="dayBook" />
               <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
           </td>
       </tr>
    </table>
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0" style="width: 80%;" >
        <tr  class="subheader_img">
            <td align="left" colspan="3" style="height: 19px" valign="middle">                 
                        <asp:Label ID="lblrptCaption" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr >
            <td align="left" class="matters" width="20%">
                Business Unit</td>
            <td class="matters" valign="top" width="1%">
                :</td>
            <td align="right" class="matters" style="text-align: left" valign="top">
                <asp:DropDownList id="ddlBSUnit" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="matters" >
                &nbsp;Date</td>
            <td class="matters"  >
                :</td>
            <td align="left" class="matters">
                <asp:TextBox ID="txtFromDate" runat="server" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate" EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
        </tr>
         <tr>
            <td align="left" class="matters" style="width: 88px">
                Express In</td>
            <td class="matters" style="width: 18px">
                :</td>
            <td align="left" class="matters" colspan="1" style="height: 1px; text-align: left">
                <asp:DropDownList ID="ddExpressedin" runat="server">
                    <asp:ListItem Value="-1">Actual</asp:ListItem>
                    <asp:ListItem>0</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>100</asp:ListItem>
                    <asp:ListItem>1000</asp:ListItem>
                    <asp:ListItem>1000000</asp:ListItem>
                </asp:DropDownList>
                <asp:CheckBox ID="chkExcludeSpecialAccounts" runat="server" 
                    Text="Exclude Special Accounts" Visible="False" />
                </td>
        </tr>
        <tr>
            <td align="center" class="matters" colspan="3">
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="dayBook" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
            </td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender> 
</asp:Content>


