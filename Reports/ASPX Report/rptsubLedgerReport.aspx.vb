Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports System.Xml
Partial Class Reports_ASPX_Report_rptsubLedgerReport
    Inherits System.Web.UI.Page

 
    Dim Encr_decrData As New Encryption64
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = OASISConstants.Gemstitle
 
        txtToDate.Attributes.Add("ReadOnly", "Readonly")
        txtFromDate.Attributes.Add("ReadOnly", "Readonly")

        'checks whether the BSUnit is selected if it is selected h_BSUID.Value will be having
        'the IDs of the BS Unit. "FillBSUNames()" function will set the BSU names
        If h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
            h_BSUID.Value = Session("sBsuid")
        End If

        'If h_Memberids.Value <> Nothing And h_Memberids.Value <> "" And h_Memberids.Value <> "undefined" Then
        '    Bind_CostunitGrid(h_Memberids.Value)
        'End If

        If h_ACTIDs.Value <> Nothing And h_ACTIDs.Value <> "" And h_ACTIDs.Value <> "undefined" Then
            FillACTIDs(h_ACTIDs.Value)
            'h_BSUID.Value = ""
        End If
        FillBSUNames(h_BSUID.Value)
       
        If Not IsPostBack Then
            BindType()
            imgPickOther.Visible = False
            imgPickcost.Visible = True
            'Bind_CostunitGrid()
            Session("liUserList") = New List(Of String)
            If (Request.QueryString("MainMnu_code") <> "") Then
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                ViewState("MainMnu_code") = "A450020"
            End If
            Select Case ViewState("MainMnu_code").ToString
                Case "A450020" 'Sub ledger rpt
                    'Bind the Data with the DropDown
                    lblCaption.Text = "Sub Ledger Report"
            End Select
            UsrBSUnits1.MenuCode = ViewState("MainMnu_code").ToString
            txtToDate.Text = UtilityObj.GetDiplayDate()
            txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))

        End If

        If chkConsolidation.Checked Then
            chkGroupCurrency.Enabled = True
        Else
            chkGroupCurrency.Enabled = False
            chkGroupCurrency.Checked = True
        End If
        lblError.Text = ""
        
        If Request.QueryString("costid") <> "" Then
            ddCostcenter.SelectedItem.Value = "OTH"
            GenerateCostcenterReport("")

        End If

        SetChk(Me.Page)
    End Sub
 


    Private Function GetDataSourceDocType() As Hashtable
        ' Type-0(By Account),-1(By Ctrl Account),-2(By SgrpCode),-3(By Grpcode)
        Dim ht As New Hashtable
        ht("0") = "By Account"
        ht("1") = "By Ctrl Account"
        ht("2") = "By SgrpCode"
        ht("3") = "By Grpcode"
        Return ht
    End Function


    Private Function GetDataSourceRptType() As Hashtable
        ' Type-0(By Account),-1(By Ctrl Account),-2(By SgrpCode),-3(By Grpcode)
        Dim ht As New Hashtable
        'ht("0") = "By Account"
        'ht("1") = "By Ctrl Account"
        'ht("2") = "By SgrpCode"
        'ht("3") = "By Grpcode"
        ht("6") = "By Income/Expense"
        ht("7") = "Expense Contribution by Account"
        ht("8") = "Income Contribution by Account"
        Return ht
    End Function


    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        If h_Mode.Value = "cashFlow" Then
            chkConsolidation.Checked = True
        Else
            chkConsolidation.Checked = IIf(ds.Tables(0).Rows.Count = 1, False, True)
        End If
        'grdBSU.DataSource = ds
        'grdBSU.DataBind()
        Return True
    End Function


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Session("liUserList") Is Nothing Then
            lblError.Text = "Please Pick atleast one cost object"
            Exit Sub
        End If
        If grdACTDetails.Rows.Count = 0 Then
            lblError.Text = "Please Pick atleast one one account"
            Exit Sub
        End If

        Dim strXMLBSUNames As String
        'Generate the XML in the form <BSU_DETAILS><BSU_ID>IDhere</BSU_ID></BSU_DETAILS>
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        strXMLBSUNames = GenerateBSUXML(h_BSUID.Value) 'txtBSUNames.Text)
        txtfromDate.Text = Format("dd/MMM/yyyy", txtfromDate.Text)
        txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)

        Select Case ViewState("MainMnu_code")
            Case "A450020" 'Trial Balance
                GenerateCostcenterReport(strXMLBSUNames)
            Case Else
                GenerateCostcenterReport(strXMLBSUNames)
        End Select

    End Sub


    Private Function GetReportCaption(ByVal type As Integer) As String
        Select Case type
            Case 6
                Return "P\L Contribution"
            Case 7
                Return "Expense Contribution"
            Case 8
                Return "Income Contribution"
            Case Else
                Return ""
        End Select
    End Function


    Sub BindType()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT CCS_ID,CCS_DESCR" _
            & " FROM COSTCENTER_S WHERE CCS_CCT_ID='9999' UNION ALL SELECT 'OTH' as CCS_ID,'OTHER COST CENTERS' as CCS_DESCR "
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddCostcenter.DataSource = ds.Tables(0)
            ddCostcenter.DataTextField = "CCS_DESCR"
            ddCostcenter.DataValueField = "CCS_ID"
            ddCostcenter.DataBind()
            ddCostcenter.SelectedIndex = 0
        Catch ex As Exception
            Errorlog(ex.Message)

        End Try
    End Sub


    Private Sub GenerateCostcenterReport(ByVal strXMLBSUNames As String)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet 
        Dim cmd As SqlCommand

        If ddCostcenter.SelectedItem.Value <> "OTH" Then
            cmd = New SqlCommand("RPTSubLedgerReport", objConn)
        Else
            cmd = New SqlCommand("RPTSubLedgerReportOthers", objConn)
        End If

        cmd.CommandType = CommandType.StoredProcedure
        ''@BSUIDs xml,
        ''@ACT_IDs xml,
        ''@FROMDT varchar(12),
        ''@TODT varchar(12),
        ''@bConsolidation bit=0,
        ''@bShowinGrpCurrency bit 
        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        If Request.QueryString("costid") = "" Then
            sqlpJHD_BSU_IDs.Value = strXMLBSUNames
            Session("BSUIDs") = strXMLBSUNames
        Else
            sqlpJHD_BSU_IDs.Value = Session("BSUIDs")
        End If
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        'Dim sqlpCCS_ID As New SqlParameter("@CCS_ID", SqlDbType.VarChar, 20)
        'sqlpCCS_ID.Value = ddCostcenter.SelectedItem.Value
        'cmd.Parameters.Add(sqlpCCS_ID)

        Dim sqlpJHD_DOCTYPE As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        If Request.QueryString("costid") = "" Then
            sqlpJHD_DOCTYPE.Value = GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
            Session("ACT_IDs") = sqlpJHD_DOCTYPE.Value
        Else
            sqlpJHD_DOCTYPE.Value = Session("ACT_IDs")
        End If
        cmd.Parameters.Add(sqlpJHD_DOCTYPE)

        Dim sqlpCOST_IDs As New SqlParameter("@COST_IDs", SqlDbType.Xml)
        If Request.QueryString("costid") = "" Then
            If ddCostcenter.SelectedItem.Value = "OTH" Then
                sqlpCOST_IDs.Value = FillCOSTOthers()
            Else
                sqlpCOST_IDs.Value = FillCOST()
            End If

        Else
            sqlpCOST_IDs.Value = "<COST_DETAILS><COST_DETAIL><COST_ID>" & Request.QueryString("costid") & "</COST_ID></COST_DETAIL></COST_DETAILS>"
        End If
        cmd.Parameters.Add(sqlpCOST_IDs)

        If ddCostcenter.SelectedItem.Value <> "OTH" Then
            Dim sqlpFROMDOCDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
            sqlpFROMDOCDT.Value = txtFromDate.Text
            cmd.Parameters.Add(sqlpFROMDOCDT)

            Dim sqlpTODOCDT As New SqlParameter("@TODT", SqlDbType.DateTime)
            sqlpTODOCDT.Value = txtToDate.Text
            cmd.Parameters.Add(sqlpTODOCDT)

            Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
            sqlpConsolidation.Value = chkConsolidation.Checked
            cmd.Parameters.Add(sqlpConsolidation)

            Dim sqlpGroupCurrency As New SqlParameter("@bShowinGrpCurrency", SqlDbType.Bit)
            sqlpGroupCurrency.Value = chkGroupCurrency.Checked
            cmd.Parameters.Add(sqlpGroupCurrency)
        Else

        End If


        adpt.SelectCommand = cmd
        objConn.Close()
        objConn.Open()
        adpt.Fill(ds)
        If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            Session("BSUNames") = strXMLBSUNames
            Session("rptFromDate") = txtFromDate.Text
            Session("rptToDate") = txtToDate.Text
            Session("rptConsolidation") = chkConsolidation.Checked
            Session("rptGroupCurrency") = chkGroupCurrency.Checked

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            'params("userName") = Session("sUsr_name")
            'params("FromDate") = txtFromDate.Text
            'params("ToDate") = txtToDate.Text
            'params("Type") = ""
            'params("parent") = ""
            repSource.Parameter = params
            repSource.Command = cmd
            If rbAccount.Checked = True Then
                repSource.ResourceName = "../RPT_Files/rptSubLedgerReport_Account.rpt"
            Else
                repSource.ResourceName = "../RPT_Files/rptSubLedgerReport_CostCenter.rpt"
            End If
            If ddCostcenter.SelectedItem.Value = "OTH" Then
                If rbAccount.Checked = True Then
                    repSource.ResourceName = "../RPT_Files/rptSubLedgerReport_OthersAccount.rpt"
                Else
                    repSource.ResourceName = "../RPT_Files/rptSubLedgerReport_OthersCostCenter.rpt"
                End If
            End If
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            response.redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub


    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(h_ACTIDs.Value)
        End If

    End Sub


    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Dim IDs As String() = ACTIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim str_Sql As String
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        grdACTDetails.DataSource = ds
        grdACTDetails.DataBind()
       
    End Sub





    Private Function FillCOST() As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Try
            BSUDetails = xmlDoc.CreateElement("COST_DETAILS")
            xmlDoc.AppendChild(BSUDetails)
            'Dim IDs As String() = h_Memberids.Value.Split("||")
            'Dim i As Integer

            For i As Integer = 0 To Session("liUserListCost").Count - 1
                If (Session("liUserListCost")(i).ToString <> "") Then
                    XMLEBSUDetail = xmlDoc.CreateElement("COST_DETAIL")
                    XMLEBSUID = xmlDoc.CreateElement("COST_ID")
                    XMLEBSUID.InnerText = Session("liUserListCost")(i).ToString()
                    XMLEBSUDetail.AppendChild(XMLEBSUID)
                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
            Next 
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Function FillCOSTOthers() As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Try
            BSUDetails = xmlDoc.CreateElement("COST_DETAILS")
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = h_Memberids.Value.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If IDs(i) <> "" Then
                    XMLEBSUDetail = xmlDoc.CreateElement("COST_DETAIL")
                    XMLEBSUID = xmlDoc.CreateElement("COST_ID")
                    XMLEBSUID.InnerText = IDs(i)
                    XMLEBSUDetail.AppendChild(XMLEBSUID)
                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
            Next

            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

   
    'Generates the XML for BSUnit
    Private Function GenerateBSUXML(ByVal BSUIDs As String) As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Try
            BSUDetails = xmlDoc.CreateElement("BSU_DETAILS")
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement("BSU_DETAIL")
                XMLEBSUID = xmlDoc.CreateElement("BSU_ID")
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function


    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.ACTName
                elements(0) = "ACT_DETAILS"
                elements(1) = "ACT_DETAIL"
                elements(2) = "ACT_ID"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                XMLEBSUID = xmlDoc.CreateElement(elements(2))
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function


    'Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
    '    If h_Mode.Value = "cashflow" Then
    '        h_BSUID.Value = txtBSUNames.Text
    '    Else
    '        h_BSUID.Value += "||" + txtBSUNames.Text.Replace(",", "||")
    '    End If
    '    FillBSUNames(h_BSUID.Value)
    'End Sub


    'Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
    '    grdBSU.PageIndex = e.NewPageIndex
    '    FillBSUNames(h_BSUID.Value)
    'End Sub


    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = Session("sBSUID")
            End If
            'grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub


    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdACTDetails.PageIndexChanging
        grdACTDetails.PageIndex = e.NewPageIndex
        FillBSUNames(h_ACTIDs.Value)
    End Sub

    Protected Sub ddCostcenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddCostcenter.SelectedIndexChanged
        'Bind_CostunitGrid()
        If ddCostcenter.SelectedItem.Value = "OTH" Then
            imgPickOther.Visible = True
            imgPickcost.Visible = False
        Else
            imgPickOther.Visible = False
            imgPickcost.Visible = True
        End If
    End Sub

    'Protected Sub gvCostcenter_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCostcenter.PageIndexChanging


    '    gvCostcenter.PageIndex = e.NewPageIndex
    '    Bind_CostunitGrid()
    '    SetChk(Me.Page)
    'End Sub

    Protected Sub lnkbtngrdcOST(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            H_COSTIDS.Value = H_COSTIDS.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(H_COSTIDS.Value)
        End If
    End Sub



    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource = Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function
    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub


    
End Class
