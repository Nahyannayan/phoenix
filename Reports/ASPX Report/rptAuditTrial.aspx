<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptAuditTrial.aspx.vb" Inherits="Reports_ASPX_Report_rptAuditTrial" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
     <script language="javascript" type="text/javascript">
 
 function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 229px; ";
            sFeatures+="dialogHeight: 234px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx","", sFeatures);
            if (result=='' || result==undefined)
            {
                return false;
            }
            switch(txtControl)
            {
              case 0:
                document.getElementById('<%=txtfromDate.ClientID %>').value=result;
                break;
              case 1:  
                document.getElementById('<%=txtToDate.ClientID %>').value=result;
                break;
            }    
           }
 
 /*
     function checkdate(objName) {
                    var datefield = objName;
                    if (chkdate(objName) == false) {
                    datefield.select();
                    alert("That date is invalid.  Please try again.");
                    datefield.focus();
                    return false;
                    }
                    else {
                    return true;
                       }
                    }
       function chkdate(objName) {
                    var strDatestyle = "US"; //United States date style
                    //var strDatestyle = "EU";  //European date style
                    var strDate;
                    var strDateArray;
                    var strDay;
                    var strMonth;
                    var strYear;
                    var intday;
                    var intMonth;
                    var intYear;
                    var booFound = false;
                    var datefield = objName;
                    var strSeparatorArray = new Array("-"," ","/",".");
                    var intElementNr;
                    var err = 0;
                    var strMonthArray = new Array(12);
                    strMonthArray[0] = "Jan";
                    strMonthArray[1] = "Feb";
                    strMonthArray[2] = "Mar";
                    strMonthArray[3] = "Apr";
                    strMonthArray[4] = "May";
                    strMonthArray[5] = "Jun";
                    strMonthArray[6] = "Jul";
                    strMonthArray[7] = "Aug";
                    strMonthArray[8] = "Sep";
                    strMonthArray[9] = "Oct";
                    strMonthArray[10] = "Nov";
                    strMonthArray[11] = "Dec";
                    strDate = datefield.value;
                    if (strDate.length < 1) {
                    return true;
                    }
                    for (intElementNr = 0; intElementNr < strSeparatorArray.length; intElementNr++) {
                    if (strDate.indexOf(strSeparatorArray[intElementNr]) != -1) {
                    strDateArray = strDate.split(strSeparatorArray[intElementNr]);
                    if (strDateArray.length != 3) {
                    err = 1;
                    return false;
                    }
                    else {
                    strDay = strDateArray[0];
                    strMonth = strDateArray[1];
                    strYear = strDateArray[2];
                    }
                    booFound = true;
                       }
                    }
                    if (booFound == false) {
                    if (strDate.length>5) {
                    strDay = strDate.substr(0, 2);
                    strMonth = strDate.substr(2, 2);
                    strYear = strDate.substr(4);
                       }
                    }
                    if (strYear.length == 2) {
                    strYear = '20' + strYear;
                    }
                    // US style my change
                    if (strDatestyle == "US") {
                    //strTemp = strDay;
                    //strDay = strMonth;
                    //strMonth = strTemp;
                    }
                    intday = parseInt(strDay, 10);
                    if (isNaN(intday)) {
                    err = 2;
                    return false;
                    }
                    intMonth = parseInt(strMonth, 10);
                    if (isNaN(intMonth)) {
                    for (i = 0;i<12;i++) {
                    if (strMonth.toUpperCase() == strMonthArray[i].toUpperCase()) {
                    intMonth = i+1;
                    strMonth = strMonthArray[i];
                    i = 12;
                       }
                    }
                    if (isNaN(intMonth)) {
                    err = 3;
                    return false;
                       }
                    }
                    intYear = parseInt(strYear, 10);
                    if (isNaN(intYear)) {
                    err = 4;
                    return false;
                    }
                    if (intMonth>12 || intMonth<1) {
                    err = 5;
                    return false;
                    }
                    if ((intMonth == 1 || intMonth == 3 || intMonth == 5 || intMonth == 7 || intMonth == 8 || intMonth == 10 || intMonth == 12) && (intday > 31 || intday < 1)) {
                    err = 6;
                    return false;
                    }
                    if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intday > 30 || intday < 1)) {
                    err = 7;
                    return false;
                    }
                    if (intMonth == 2) {
                    if (intday < 1) {
                    err = 8;
                    return false;
                    }
                    if (LeapYear(intYear) == true) {
                    if (intday > 29) {
                    err = 9;
                    return false;
                    }
                    }
                    else {
                    if (intday > 28) {
                    err = 10;
                    return false;
                    }
                    }
                    }
                    if (strDatestyle == "US") {
                    //datefield.value = strMonthArray[intMonth-1] + " " + intday+" " + strYear;
                    datefield.value = intday + " " + strMonthArray[intMonth-1] + " " + strYear;
                    
                    }
                    else {
                    datefield.value = intday + " " + strMonthArray[intMonth-1] + " " + strYear;
                    }
                    return true;
                    }
                    function LeapYear(intYear) {
                    if (intYear % 100 == 0) {
                    if (intYear % 400 == 0) { return true; }
                    }
                    else {
                    if ((intYear % 4) == 0) { return true; }
                    }
                    return false;
                    }
                    function doDateCheck(from, to) {
                    if (Date.parse(from.value) <= Date.parse(to.value)) {
                    alert("The dates are valid.");
                    }
                    else {
                    if (from.value == "" || to.value == "") 
                    alert("Both dates must be entered.");
                    else 
                    alert("To date must occur after the from date.");
                       }
                    }
*/
 
 
 function getComAudit(mode) 
 {     
            var sFeatures;
            sFeatures="dialogWidth: 310px; ";
            sFeatures+="dialogHeight: 400px; ";
            
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            var optcode; 
            url='rptShowcomAudit.aspx?id='+mode;
       
             if (mode=='M')
             {          
            result = window.showModalDialog(url,"", sFeatures);
                                 
            if (result=='' || result==undefined)
            {    return false;      }   
             NameandCode = result.split('___');  
            document.getElementById("<%=txtModule.ClientID %>").value=NameandCode[0];
           // document.forms[0].submit();
            
             }
             else if(mode=='L')
             {
            
              result = window.showModalDialog(url,"", sFeatures);
                                 
            if (result=='' || result==undefined)
            {    return false;      } 
            
            NameandCode = result.split('___');  
            document.getElementById("<%=txtLoginUser.ClientID %>").value=NameandCode[0];
           
             }
             
             else if(mode=='F')
             {
             optcode='&opt='+ document.getElementById('<%=txtModule.ClientID %>').value ;
             url=url+optcode;
            
              result = window.showModalDialog(url,"", sFeatures);
                                 
            if (result=='' || result==undefined)
            {    return false;      } 
            
            NameandCode = result.split('___');  
            document.getElementById("<%=txtForm.ClientID %>").value=NameandCode[0];
           
             }
             else if(mode=='D')
             {
             optcode='&opt='+ document.getElementById('<%=txtForm.ClientID %>').value ;
             url=url+optcode;
              result = window.showModalDialog(url,"", sFeatures);
                                 
            if (result=='' || result==undefined)
            {    return false;      } 
                             NameandCode = result.split('___');  
            document.getElementById("<%=txtDocNo.ClientID %>").value=NameandCode[0];
              
             }
             else if(mode=='A')
             {
              result = window.showModalDialog(url,"", sFeatures);
                                 
            if (result=='' || result==undefined)
            {    return false;      } 
            
            NameandCode = result.split('___');  
            document.getElementById("<%=txtAction.ClientID %>").value=NameandCode[0];
           
             }
             
             else if(mode=='T')
             {
              result = window.showModalDialog(url,"", sFeatures);
                                 
            if (result=='' || result==undefined)
            {    return false;      } 
            
            NameandCode = result.split('___');  
            document.getElementById("<%=txtTerminal.ClientID %>").value=NameandCode[0];
           
             }
           else if(mode=='W')
             {
              result = window.showModalDialog(url,"", sFeatures);
                                 
            if (result=='' || result==undefined)
            {    return false;      } 
            
            NameandCode = result.split('___');  
            document.getElementById("<%=txtWindow.ClientID %>").value=NameandCode[0];
           
             }
                     
           }
           
           </script> 
 
 
  <table width="75%">
  <tr>
 
 <td style="width: 571px" align="left"> 
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><br />
     <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="bankPayment" CssClass="error" ForeColor="" />
 </td>
 </tr>
 <tr>
 
 <td align="center" valign="top" style="height: 538px">
   <table align="center" class="BlueTable" cellpadding="5" cellspacing="0" style="width: 754px">
        <tr class="subheader_img">
            <td colspan="6" style="height: 19px">Audit Trail
            </td>
        </tr>
        <tr>
            <td class="matters" style="width: 88px; height: 16px; text-align: left">
                From Date</td>
            <td class="matters" style="width: 8px; height: 16px" align="center">
                :</td>
            <td class="matters" style="width: 180px; height: 16px; text-align: left">
                <asp:TextBox ID="txtFromDate" runat="server" Width="50%"></asp:TextBox>
                <img onclick="getDate(550, 310, 0)" src="../../Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="valFromDate" runat="server" ControlToValidate="txtfromDate"
                    ErrorMessage="From Date Required" ValidationGroup="bankPayment" EnableViewState="False" CssClass="error" Display="Dynamic" ForeColor="">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revFromDate" runat="server" ControlToValidate="txtFromDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$" ValidationGroup="bankPayment" CssClass="error" ForeColor="">*</asp:RegularExpressionValidator>
                <asp:CustomValidator ID="cvDateFrom" runat="server" Display="Dynamic" EnableViewState="False"
                    ErrorMessage="From Date entered is not a valid date" ValidationGroup="bankPayment" CssClass="error" ForeColor="">*</asp:CustomValidator></td>
            <td class="matters" style="width: 65px; color: #1b80b6; height: 16px; text-align: left">
                To Date</td>
            <td class="matters" style="width: 1px; height: 16px">
                :</td>
            <td class="matters" style="width: 163px; height: 16px; text-align: left">
                <asp:TextBox ID="txtToDate" runat="server" Width="60%"></asp:TextBox>
                <img onclick="getDate(820, 310, 1)" src="../../Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="valToDate" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date Required" ValidationGroup="bankPayment" EnableViewState="False">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revTodate" runat="server" ControlToValidate="txtToDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$" ValidationGroup="bankPayment">*</asp:RegularExpressionValidator>
                <asp:CustomValidator ID="cvTodate" runat="server" Display="Dynamic" EnableViewState="False"
                    ErrorMessage="To Date entered is not a valid date and must be greater than From date"
                    ValidationGroup="bankPayment">*</asp:CustomValidator></td>
        </tr>
        <!-- Grade & Section -->
        <!-- Name -->
        <!-- Student Id -->
        <tr runat="server" style="color: #1b80b6">
            <td class="matters" style="width: 88px; height: 13px; text-align: left">
                Login User</td>
            <td align="center" class="matters" style="width: 8px; height: 13px">
                :</td>
            <td class="matters" colspan="4" style="height: 13px; text-align: left" align="center">
                <asp:TextBox ID="txtLoginUser" runat="server" TextMode="MultiLine" Width="40%"></asp:TextBox>
                &nbsp;&nbsp;<img onclick="getComAudit('L');" src="../../Images/forum_search.gif" align="middle" /></td>
        </tr>
        <tr id="trBankRow" runat="server" style="color: #1b80b6">
            <td class="matters" style="width: 88px; height: 13px; text-align: left">
                Module</td>
            <td class="matters" style="width: 8px; height: 13px" align="center">
                :</td>
            <td class="matters" colspan="4" style="height: 13px; text-align: left">
                <asp:TextBox ID="txtModule" runat="server" TextMode="MultiLine" Width="40%" AutoPostBack="True"></asp:TextBox>
                &nbsp;
                <img onclick="getComAudit('M');" src="../../Images/forum_search.gif" align="middle" /></td>
        </tr>
        <tr id="trAccountRow" runat="server">
            <td class="matters" style="width: 88px; height: 19px; text-align: left">
                Form Name</td>
            <td class="matters" style="width: 8px; height: 19px" align="center">
                :</td>
            <td class="matters" colspan="4" style="height: 19px; text-align: left">
                <asp:TextBox ID="txtForm" runat="server" Width="40%" TextMode="MultiLine"></asp:TextBox>
                &nbsp;
                <img onclick="getComAudit('F');" src="../../Images/forum_search.gif" align="middle" id="IMG1" /></td>
        </tr>
        <tr runat="server">
            <td class="matters" style="width: 88px; height: 19px; text-align: left">
                Doc. Code.</td>
            <td align="center" class="matters" style="width: 8px; height: 19px">
                :</td>
            <td class="matters" colspan="4" style="height: 19px; text-align: left">
                <asp:TextBox ID="txtDocNo" runat="server" Width="40%" TextMode="MultiLine"></asp:TextBox> &nbsp;
                <img onclick="getComAudit('D');" src="../../Images/forum_search.gif" align="middle" /></td>
        </tr>
        <tr>
            <td class="matters" style="width: 88px; height: 20px; text-align: left">
                Action</td>
            <td class="matters" style="width: 8px; height: 20px" align="center">
                :</td>
            <td class="matters" colspan="4" style="height: 20px; text-align: left">
                <asp:TextBox ID="txtAction" runat="server" Width="40%" TextMode="MultiLine"></asp:TextBox> &nbsp;
                <img onclick="getComAudit('A');" src="../../Images/forum_search.gif" align="middle" /></td>
        </tr>
        <tr>
            <td class="matters" style="width: 88px; height: 20px; text-align: left">
                Terminal</td>
            <td align="center" class="matters" style="width: 8px; height: 20px">
                :</td>
            <td class="matters" colspan="4" style="height: 20px; text-align: left">
                <asp:TextBox ID="txtTerminal" runat="server" Width="40%" TextMode="MultiLine" Columns="30"></asp:TextBox> &nbsp;
                <img onclick="getComAudit('T');" src="../../Images/forum_search.gif" align="middle" /></td>
        </tr>
        <tr>
            <td class="matters" style="width: 88px; height: 20px; text-align: left">
                Window
                User</td>
            <td align="center" class="matters" style="width: 8px; height: 20px">
                :</td>
            <td class="matters" colspan="4" style="height: 20px; text-align: left">
                <asp:TextBox ID="txtWindow" runat="server" Width="40%" TextMode="MultiLine"></asp:TextBox> &nbsp;
                <img onclick="getComAudit('W');" src="../../Images/forum_search.gif" align="middle" /></td>
        </tr>
        <tr>
            <td class="matters" colspan="6" style="height: 15px; text-align: right">
                &nbsp;
                &nbsp;<asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Generate Report"
                    ValidationGroup="bankPayment" Width="134px" />
                <asp:Button ID="btnClear" runat="server" CssClass="button" Text="Clear" UseSubmitBehavior="False"
                    Width="72px" />
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" UseSubmitBehavior="False"
                    Width="72px" /></td>
        </tr>
        <!-- ===================================== APPLICANT 6 ======================================= -->
    </table>
     
    <asp:HiddenField ID="hfID" runat="server" />
    <asp:HiddenField ID="hfDescr" runat="server" />
    
    </td>
     </tr>
      
    </table>
    
</asp:Content>

