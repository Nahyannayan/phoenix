﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports System.Xml
Partial Class Reports_ASPX_Report_rptCostcenterDepartment
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If h_ACTIDs.Value <> Nothing And h_ACTIDs.Value <> "" And h_ACTIDs.Value <> "undefined" Then
            FillACTIDs(h_ACTIDs.Value)
        End If
        If Not IsPostBack Then
            Page.Title = OASISConstants.Gemstitle
            'txtToDate.Attributes.Add("ReadOnly", "Readonly")
            'txtFromDate.Attributes.Add("ReadOnly", "Readonly")
            imgPickDpt.Attributes.Add("onclick", "return getFilter('" & txtDepartment.ClientID & "','" & h_Departmentid.ClientID & "','DPT')")

            Session("liUserList") = New List(Of String)
            gvDptdetails.Attributes.Add("bordercolor", "#1b80b6")
            grdACTDetails.Attributes.Add("bordercolor", "#1b80b6")

            If (Request.QueryString("MainMnu_code") <> "") Then
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                ViewState("MainMnu_code") = ""
            End If
            UsrBSUnits1.MenuCode = ViewState("MainMnu_code")
            Select Case ViewState("MainMnu_code").ToString
                Case "A350060"
                    lblCaption.Text = "Cost Center Analysis"
                    'tr_CostCenter.Visible = False
            End Select
            txtToDate.Text = UtilityObj.GetDiplayDate()
            txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))
        End If

    End Sub 

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Session("liUserList") Is Nothing Then
            lblError.Text = "Please Pick atleast one cost object"
            Exit Sub
        End If
        If grdACTDetails.Rows.Count = 0 Then
            lblError.Text = "Please Pick atleast one one account"
            Exit Sub
        End If 
        txtFromDate.Text = Format("dd/MMM/yyyy", txtFromDate.Text)
        txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)

        Select Case ViewState("MainMnu_code").ToString
            Case "A350060"
                lblCaption.Text = "Cost Center Analysis"
                RPTGetCostCenterNew()
        End Select

    End Sub 

    Private Sub RPTGetCostCenterNew()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As SqlCommand
        cmd = New SqlCommand("RPTGetCostCenterNew", objConn)

        cmd.CommandType = CommandType.StoredProcedure  

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = UsrBSUnits1.GetSelectedNode().Replace("||", "|")
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpJHD_DOCTYPE As New SqlParameter("@ACT_IDs", SqlDbType.VarChar) 
        sqlpJHD_DOCTYPE.Value = h_ACTIDs.Value 
        cmd.Parameters.Add(sqlpJHD_DOCTYPE)

        Dim sqlpCOST_IDs As New SqlParameter("@CCT_IDS", SqlDbType.VarChar) 
        sqlpCOST_IDs.Value = h_Departmentid.Value.Replace("||", "|") 
        cmd.Parameters.Add(sqlpCOST_IDs) 

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        adpt.SelectCommand = cmd
        objConn.Close()
        objConn.Open()
        adpt.Fill(ds)
        If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            Dim str_bsu_Segment As String = ""
            Dim str_bsu_shortname As String = ""
            Dim str_bsu_CURRENCY As String = ""
            getBsuSegmentSplit(sqlpJHD_BSU_IDs.Value, str_bsu_Segment, _
            str_bsu_shortname, str_bsu_CURRENCY)
            params("Bsu_Shortnames") = str_bsu_shortname
            params("Bsu_Segments") = str_bsu_Segment
            Select Case ddAccountGroup.SelectedItem.Value
                Case "ACC"
                    repSource.ResourceName = "../RPT_Files/rptCostCenterAnalysis_AccountNew.rpt"
                    params("ReportHeader") = "Cost Center Analysis by Account"
                Case "BSU"
                    repSource.ResourceName = "../RPT_Files/rptCostCenterAnalysis_Business.rpt"
                    params("ReportHeader") = "Cost Center Analysis by Business Unit"
                Case "CON"
                    repSource.ResourceName = "../RPT_Files/rptCostCenterAnalysis_Consolidated.rpt"
                    params("ReportHeader") = "Cost Center Analysis - Consolidated"
            End Select
            repSource.Parameter = params
            repSource.Command = cmd

            Session("ReportSource") = repSource
            objConn.Close()
            Response.Redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(h_ACTIDs.Value)
        End If
    End Sub

    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Dim IDs As String() = ACTIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim str_Sql As String
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")" 
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        grdACTDetails.DataSource = ds
        grdACTDetails.DataBind() 
    End Sub
     
    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdACTDetails.PageIndexChanging
        grdACTDetails.PageIndex = e.NewPageIndex
    End Sub

    Protected Sub lnkbtngrdcOST(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            H_COSTIDS.Value = H_COSTIDS.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(H_COSTIDS.Value)
        End If
    End Sub

    Protected Sub txtDepartment_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDepartment.TextChanged
        txtDepartment.Text = ""
        FillDepartment(h_Departmentid.Value)
    End Sub

    Private Sub FillDepartment(ByVal STU_IDs As String)
        gvDptdetails.DataSource = Nothing
        gvDptdetails.DataBind()
        Dim IDs As String() = STU_IDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = " SELECT CCT_ID, CCT_DESCR FROM COSTCENTER_M WHERE CCT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_Sql)
        gvDptdetails.DataSource = ds
        gvDptdetails.DataBind() 
    End Sub

    Protected Sub gvDptdetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvDptdetails.PageIndex = e.NewPageIndex()
        FillDepartment(h_Departmentid.Value)
    End Sub

End Class
