<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptDayBook.aspx.vb" Inherits="Reports_ASPX_Report_rptDayBook" title="Untitled Page" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">
           function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 250px; ";
            sFeatures+="dialogHeight: 270px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx","", sFeatures);
            if(result != '' && result != undefined)
            {
                switch(txtControl)
                {
                  case 0:
                    document.getElementById('<%=txtfromDate.ClientID %>').value=result;
                    break;
                  case 1:  
                    document.getElementById('<%=txtToDate.ClientID %>').value=result;
                    break;
                }
            }
            return false;
           }
   
       function GetBSUName()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx","", sFeatures);
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else
            {
                return false;
            }
        }

    </script>    

    <table align="center" style="width: 70%" border="0" cellpadding="0" cellspacing="0">
        <tr align="left">
            <td>
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="dayBook" />
            </td>
        </tr>
    </table>
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0"
        style="width: 70%; height: 174px">
        <tr class="subheader_img">
            <td align="left" colspan="8" valign="middle" style="height: 19px">
                <asp:Label ID="lblRptCaption" runat="server" Text="Day Book Report"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" class="matters" style=" height: 25px; width: 294px;">
                From Date&nbsp;</td>
            <td class="matters" style="height: 25px; width: 10px;">
                :</td>
            <td  align="left" class="matters" colspan="2">
                <asp:TextBox ID="txtFromDate" runat="server" Width="123px" CssClass="inputbox"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False"/>&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook" Display="Dynamic">*</asp:RegularExpressionValidator>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp;</td>
            <td align="left" class="matters" style="height: 25px; width: 294px;">
                To Date &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
            </td>
            <td align="left" class="matters" style="height: 25px; width: 9px;">
                :</td>
            <td align="left" class="matters" colspan="2">
                <asp:TextBox ID="txtToDate" runat="server" Width="122px" CssClass="inputbox"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr style="color: #1b80b6">
            <td align="left" valign = "top" class="matters" style="height: 16px; width: 294px;">
                Business Unit</td>
            <td class="matters" valign = "top" style="width: 10px; height: 16px;">
                :</td>
            <td align="left" class="matters" colspan="6" valign="top">
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
            </td>
        </tr>
        <tr style="color: #1b80b6" id = "trDocType" runat = "server">
            <td align="left" valign = "top" class="matters" style=" height: 13px; width: 294px;">
                Doc Type</td>
            <td class="matters" valign = "top" style="height: 13px; width: 10px;">
                :</td>
            <td align="left" class="matters" colspan="6">
                <asp:DropDownList ID="cmbDocType" runat="server">
                </asp:DropDownList>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="text-align: right">
                <asp:LinkButton id="lnkExporttoexcel" runat="server" onclick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="dayBook" /> 
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /> 
            </td>
        </tr>
    </table>
                <asp:HiddenField ID="h_BSUID" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
</asp:Content>

