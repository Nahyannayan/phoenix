Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Reports_ASPX_Report_rptSettlement
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblError.Text = ""
        Page.Title = OASISConstants.Gemstitle
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If

        'Add the readOnly attribute to the Date textBox So that the Text property value is accessible in the form
        'It is not giving the values if you make the ReadOnly property to TRUE
        'So you have to do this in page_Load()
        'txtToDate.Attributes.Add("ReadOnly", "Readonly")
        'txtFromDate.Attributes.Add("ReadOnly", "Readonly")
        'txtBSUName.Attributes.Add("ReadOnly", "Readonly")
        'txtBSUCode.Attributes.Add("ReadOnly", "Readonly")
        txtbankCodes.Attributes.Add("ReadOnly", "Readonly")
        txtBankNames.Attributes.Add("ReadOnly", "Readonly")
        'checks whether the BSUnit is selected if it is selected h_BSUID.Value will be having
        'the IDs of the BS Unit. "FillBSUNames()" function will set the BSU names



        If h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            'FillBSUNames(h_BSUID.Value)
            'h_BSUID.Value = ""
        Else
            h_BSUID.Value = Session("sBsuid")
            'ddlBSUnit.SelectedValue = h_BSUID.Value
            'txtBSUCode.Text = Session("sBsuid")
            'txtBSUName.Text = Session("BSU_Name")
            'FillBSUNames(Session("businessunit"))
            'h_BSUID.Value = Session("businessunit")
        End If

        If Not IsPostBack Then
            ddlBSUnit.DataSource = UtilityObj.GetBusinessUnits(Session("sUsr_name"))
            ddlBSUnit.DataTextField = "BSU_NAME"
            ddlBSUnit.DataValueField = "BSU_ID"
            ddlBSUnit.DataBind()
            ddlBSUnit.SelectedValue = Session("sBsuid")

            txtToDate.Text = UtilityObj.GetDiplayDate()
            txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))


        End If

    End Sub

     
    Private Function GetDataSourceDocType() As DataTable
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            Dim dsDoc As New DataSet
            str_Sql = "SELECT DOC_ID, DOC_NAME FROM DOCUMENT_M"
            dsDoc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If dsDoc Is Nothing Then
                Throw New Exception("DsDoc is null Throws Error in DayBook.cs function GetDataSourceDocType()")
            ElseIf dsDoc.Tables.Count > 0 Then
                Dim row As DataRow
                row = dsDoc.Tables(0).NewRow()
                row(0) = "ALL"
                row(1) = "ALL"
                dsDoc.Tables(0).Rows.Add(row)
                Return dsDoc.Tables(0)
            Else
                Throw New Exception("DsDoc.Table is less than 0 Throws Error in DayBook.cs function GetDataSourceDocType()")
            End If
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        'Dim strXMLBSUNames As String
        'Genera.tte the XML in the form <BSU_DETAILS><BSU_ID>IDhere</BSU_ID></BSU_DETAILS>
        'strXMLBSUNames = GenerateBSUXML(h_BSUID.Value) 'txtBSUNames.Text)
        Dim strfDate As String = txtFromDate.Text.Trim
        Dim strtDate As String = txtToDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdates(strfDate, strtDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        End If

        
        txtFromDate.Text = strfDate
        txtToDate.Text = strtDate

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim str_sql As String
        

        Dim str_filter As String
        If rbDoc.Checked = True Then
            str_filter = " and SETTLE_D.SET_DOCDT between '" & strfDate & "' and '" & strtDate & "'"
        Else
            str_filter = " and SETTLE_D.SET_REFDOCDT between '" & strfDate & "' and '" & strtDate & "'"

        End If


        str_sql = "SELECT     SETTLE_D.GUID, SETTLE_D.SET_SUB_ID, " _
            & " SETTLE_D.SET_BSU_ID, SETTLE_D.SET_FYEAR," _
            & " SETTLE_D.SET_DOCTYPE, SETTLE_D.SET_DOCNO," _
            & " SETTLE_D.SET_DOCDT, SETTLE_D.SET_ACCTCODE," _
            & " SETTLE_D.SET_SETTLEDAMT, SETTLE_D.SET_CURRENCY," _
            & " SETTLE_D.SET_EXGRATE1, SETTLE_D.SET_EXGRATE2," _
            & " SETTLE_D.SET_REFDOCTYPE, SETTLE_D.SET_REFDOCNO," _
            & " SETTLE_D.SET_REFLINEID, SETTLE_D.SET_REFSETTLEDAMT," _
            & " SETTLE_D.SET_REFCURRENCY, SETTLE_D.SET_REFEXGRATE1," _
            & " SETTLE_D.SET_REFEXGRATE2, SETTLE_D.SET_REFDOCDT," _
            & " SETTLE_D.SET_REFYEAR, TRANHDR_D.TRN_AMOUNT AS Amount," _
            & " TRANHDR_D_1.TRN_AMOUNT AS RefAmount" _
            & " FROM TRANHDR_D INNER JOIN SETTLE_D" _
            & " ON TRANHDR_D.TRN_SUB_ID = SETTLE_D.SET_SUB_ID" _
            & " AND TRANHDR_D.TRN_BSU_ID = SETTLE_D.SET_BSU_ID" _
            & " AND  TRANHDR_D.TRN_FYEAR = SETTLE_D.SET_FYEAR" _
            & " AND TRANHDR_D.TRN_DOCTYPE = SETTLE_D.SET_DOCTYPE " _
            & " AND TRANHDR_D.TRN_DOCNO = SETTLE_D.SET_DOCNO " _
            & " AND TRANHDR_D.TRN_ACT_ID = SETTLE_D.SET_ACCTCODE" _
            & " AND TRANHDR_D.TRN_LINEID = SETTLE_D.SET_LINEID" _
            & " INNER JOIN TRANHDR_D AS TRANHDR_D_1 ON" _
            & " SETTLE_D.SET_BSU_ID = TRANHDR_D_1.TRN_BSU_ID" _
            & " AND SETTLE_D.SET_REFYEAR = TRANHDR_D_1.TRN_FYEAR" _
            & " AND SETTLE_D.SET_REFDOCTYPE = TRANHDR_D_1.TRN_DOCTYPE " _
            & " AND SETTLE_D.SET_REFDOCNO = TRANHDR_D_1.TRN_DOCNO" _
            & " AND SETTLE_D.SET_ACCTCODE = TRANHDR_D_1.TRN_ACT_ID" _
            & " AND SETTLE_D.SET_CURRENCY = TRANHDR_D_1.TRN_CUR_ID" _
            & " AND SETTLE_D.SET_REFLINEID = TRANHDR_D_1.TRN_LINEID" _
            & " WHERE (SETTLE_D.SET_BSU_ID = '" & ddlBSUnit.SelectedValue & "')" _
            & " AND (SETTLE_D.SET_ACCTCODE = '" & txtbankCodes.Text & "')" _
            & str_filter
        Dim cmd As New SqlCommand("RPTSETTLE", objConn)
        cmd.CommandType = CommandType.StoredProcedure
        '@ACT_ID varchar(20),
        '@BSU_ID varchar(20),
        ' 
        '@FROMDOCDT varchar(30),
        '@TODOCDT  varchar(30) ,
        '@bGroupCurrency bit
        Dim sqlpACT_ID As New SqlParameter("@ACT_ID", SqlDbType.VarChar, 20)
        sqlpACT_ID.Value = txtbankCodes.Text
        cmd.Parameters.Add(sqlpACT_ID)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = strfDate
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@TODOCDT", SqlDbType.DateTime)
        sqlpTODOCDT.Value = strtDate
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlbGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlbGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlbGroupCurrency)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            'params("p_bsuname") = txtBSUName.Text.Trim
            params("p_bsuname") = ddlBSUnit.SelectedItem.Text
            params("p_actname_code") = txtbankCodes.Text.Trim & " - " & txtBankNames.Text.Trim
            params("P_FROMDATE") = strfDate
            params("P_TODATE") = strtDate
            params("UserName") = Session("sUsr_name")
            'params("FromDate") = txtFromDate.Text
            'params("ToDate") = txtToDate.Text
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptSettle.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            response.redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    'Generates the XML for BSUnit
    Private Function GenerateBSUXML(ByVal BSUIDs As String) As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Try
            BSUDetails = xmlDoc.CreateElement("BSU_DETAILS")
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement("BSU_DETAIL")
                XMLEBSUID = xmlDoc.CreateElement("BSU_ID")
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    


End Class
