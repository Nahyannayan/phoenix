Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports Crystaldecisions.reportsource
Imports Crystaldecisions.shared
Imports CrystalDecisions.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration


Partial Class ReportViewer
    Inherits System.Web.UI.Page

    Shared MainMnu_code As String
    Dim Encr_decrData As New Encryption64
    Dim dataMode As String = "none"
    Shared repSource As MyReportClass
    Dim repClassVal As New RepClass
    Shared content As ContentPlaceHolder

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    
        Try
            'Get report source object 
            repSource = DirectCast(Context.Items("ReportSource"), MyReportClass)

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            repSource.Command.Connection = objConn
            objConn.Open()
            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = repSource.Command
            adpt.Fill(ds)

            repClassVal = New RepClass
            repClassVal.ResourceName = repSource.ResourceName
            repClassVal.SetDataSource(ds.Tables(0))
            CrystalReportViewer1.ReportSource = repClassVal
            Dim ienum As IDictionaryEnumerator = repSource.Parameter.GetEnumerator()
            While ienum.MoveNext()
                repClassVal.SetParameterValue(ienum.Key, ienum.Value)
            End While
            objConn.Close()
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Page.Title = OASISConstants.Gemstitle
            'For checking the Rights of the user
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            End If
            Dim menu_rights As Integer = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
            content = Page.Master.FindControl("cphMasterpage")
            Call AccessRight.setpage(content, menu_rights, dataMode)
        
        End If

        'if the repSource object is not available in the Context redirect to the prevoius page
        If repSource Is Nothing Then
            If Not Request.UrlReferrer Is Nothing Then
                Response.Redirect(Request.UrlReferrer.ToString())
            Else
                Response.Redirect("~\noAccess.aspx")
            End If
        End If

    
    End Sub

    Protected Sub btnPrintReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If repClassVal IsNot Nothing Then
            CrystalReportViewer1.PrintMode = PrintMode.ActiveX
            repClassVal.PrintToPrinter(1, False, 1, 1)
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        CrystalReportViewer1.Dispose()
        CrystalReportViewer1 = Nothing
        'GC.Collect()
    End Sub
End Class
