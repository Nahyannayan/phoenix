Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Reports_ASPX_Report_PdcSchedule
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            UtilityObj.NoOpen(Me.Header)

            If h_ACTIDs.Value <> Nothing And h_ACTIDs.Value <> "" And h_ACTIDs.Value <> "undefined" Then
                FillACTIDs(h_ACTIDs.Value)
                'h_BSUID.Value = ""
            End If
            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                txtToDate.Text = UtilityObj.GetDiplayDate()
                txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))

                txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
                txtToDate.Attributes.Add("onBlur", "checkdate(this)")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Try
            Dim IDs As String() = ACTIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdACTDetails.DataSource = ds
            grdACTDetails.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            txtFromDate.Text = Format("dd/MMM/yyyy", txtFromDate.Text)
            txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
           
            Session("Fdate") = txtFromDate.Text
            Session("Tdate") = txtToDate.Text
            Session("selACTID") = h_ACTIDs.Value
            getSummary()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub getSummary()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim AcId As String = h_ACTIDs.Value
        AcId = AcId.Replace("||", "@")
        Dim Mode As String = "NORMAL"
        Dim rptFile As String = "../RPT_Files/PDCSchedule.rpt"
        Dim rptSubhead As String = "PDC Schedule Summary Date From .."
        'AcId = "02101103@64101001@063N0021@04101101@04101105"
        'AcId = "24601207@24602315@24602311@24601023@24602301@24601702@24601008@24602312"

        If RdUnite.Checked Then
            Mode = "BSU"
            rptFile = "../RPT_Files/PDCScheduleByunit.rpt"
            rptSubhead = "PDC Schedule Business Unitewise Summary Date From .."

        End If
        If RdDetails.Checked Then
            Mode = "DETAILS"
            rptFile = "../RPT_Files/PDCScheduleDetails.rpt"
            rptSubhead = "PDC Schedule Business Unitewise Details Date From .."

        End If

        Dim cmd As New SqlCommand("[rptPDCSchedule]", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpACCOUNTID As New SqlParameter("@ACCOUNTID", SqlDbType.VarChar)
        sqlpACCOUNTID.Value = AcId.ToString()
        cmd.Parameters.Add(sqlpACCOUNTID)

       

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpMODE As New SqlParameter("@MODE", SqlDbType.VarChar)
        sqlpMODE.Value = Mode
        cmd.Parameters.Add(sqlpMODE)



        'adpt.SelectCommand = cmd
        'objConn.Close()
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)

            Session("rptFromDate") = txtFromDate.Text
            Session("rptToDate") = txtToDate.Text
            'Session("rptConsolidation") = chkConsolidation.Checked
            'Session("rptGroupCurrency") = chkGroupCurrency.Checked

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("ReportHead") = "PDC SCHEDULE"
            params("SubHead") = rptSubhead & txtFromDate.Text.ToString() & " ..To...  " & txtToDate.Text.ToString()
            ' params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = rptFile
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            repSource.IncludeBSUImage = True
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                Response.Redirect("rptviewer.aspx", False)
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(h_ACTIDs.Value)
        End If

    End Sub

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdACTDetails.PageIndexChanging
        grdACTDetails.PageIndex = e.NewPageIndex
        FillACTIDs(h_ACTIDs.Value)
    End Sub

    Protected Sub lblAddActID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddActID.Click
        'If h_Mode.Value = "party" Then
        h_ACTIDs.Value += "||" + txtBankNames.Text.Replace(",", "||")
        'ElseIf FillACTIDs(txtBSUName.Text) Then
        h_ACTIDs.Value = txtBankNames.Text
        'End If
        FillACTIDs(h_ACTIDs.Value)
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
            txtFromDate.Text = Session("FDate")
            txtToDate.Text = Session("TDate")
            FillACTIDs(Session("selACTID"))
            
        End If
    End Sub

End Class