<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptSupplierBillsOS.aspx.vb" Inherits="Reports_ASPX_Report_rptSupplierBillsOS" title="Untitled Page" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">
           function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 250px; ";
            sFeatures+="dialogHeight: 270px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx","", sFeatures);
            if(result != '' && result != undefined)
            {
                switch(txtControl)
                {
                  case 0:
                      document.getElementById('<%=txtAsonDate.ClientID %>').value = result;
                    break;
                }    
            }
            return false;    
           }
   
       function GetBSUName()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx","", sFeatures)
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else
            {
                return false;
            }
        }

       function GetAccounts()
        {
            var sFeatures;
            sFeatures="dialogWidth: 820px; ";
            sFeatures+="dialogHeight: 450px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var ActType;
            
            ActType = document.getElementById('<%=h_Mode.ClientID %>').value
            if (ActType=='s')
            {
            result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=s&forrpt=1","", sFeatures)
            }
            if (ActType=='C')
            {
             result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=C&forrpt=1","", sFeatures)
            }
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else
            {
                return false;            
            }
        }  
    </script>

    <table align="center" style="width: 55%">
        <tr align="left">
            <td>
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="balanceList" />
            </td>
        </tr>
        <tr align="left">
            <td>
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
    </table>
    
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0"
        style="width:55%; height: 174px" runat =server id="tblMain">
        <tr  class="subheader_img">
            <td align="left" colspan="3" style="height: 19px" valign="middle">                 
                        <asp:Label ID="lblrptCaption" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" valign = "top" class="matters" style="width: 68px" >
                Business Unit</td>
            <td class="matters" valign = "top" style="width: 18px">
                :</td>
            <td align="left" valign = "top" class="matters"> 
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                </td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 68px" >
                As On Date</td>
            <td class="matters" style="width: 18px"  >
                :</td>
            <td align="left" class="matters" style="height: 1px; text-align: left">
                <asp:RadioButtonList ID="rblIorD" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="I">InvoiceDate</asp:ListItem>
                    <asp:ListItem Value="D">DocDate</asp:ListItem>
                </asp:RadioButtonList>
                <asp:TextBox ID="txtAsonDate" runat="server" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAsonDate"
                    ErrorMessage="From Date required" ValidationGroup="balanceList">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtAsonDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="balanceList">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr style="color: #1b80b6" id = "Trbank">
            <td align="left" valign ="top" class="matters" style="width: 68px">
                <asp:Label ID="lblBankCash" runat="server" Text="Creditor"></asp:Label></td>
            <td class="matters" valign = "top" style="width: 18px">
                :<br />
            </td>
            <td align="left" class="matters" style="height: 17px">
                <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the ACT ID you want to Add to the Search and click on Add)" Width="377px"></asp:Label>
                <asp:TextBox ID="txtBankNames" runat="server" Height="18px"
                    Width="330px" CssClass="inputbox"></asp:TextBox>
                <asp:LinkButton ID="lblAddActID" runat="server">Add</asp:LinkButton>
                <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="return GetAccounts()" /><br />
                <asp:GridView ID="grdACTDetails" runat="server" AutoGenerateColumns="False" Width="377px" AllowPaging="True" PageSize="5">
                        <Columns>
                            <asp:TemplateField HeaderText="ACT ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ACT_Name" HeaderText="ACT Name" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                             <HeaderStyle CssClass="gridheader_small" />
                    </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="3" style=" text-align: right">
                &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="balanceList" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp;</td>
        </tr>
    </table>
    <asp:HiddenField ID="h_ACTIDs" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
    <asp:HiddenField ID="h_Mode" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtAsonDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtAsonDate">
    </ajaxToolkit:CalendarExtender>
</asp:Content>

