﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReportBuilderpopup.aspx.vb" Inherits="Reports_ASPX_Report_ReportBuilderpopup" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server"> 
   <base target="_self" />
  <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
 <script language="javascript" type="text/javascript" src="../../chromejs/chrome.js">
</script> 
<script type ="text/javascript" language ="javascript" >
    function getValue(retval,NameVal)
    {
    
    window.returnValue = retval +"||"+ NameVal ;
    window.close();      
    }
    function MouseOver(ObjId)
    {
    document.getElementById(ObjId).style.color ="red" ;
    }
    function MouseLeave(ObjId)
    {
    document.getElementById(ObjId).style.color ="#1b80b6" ;
    }
    
    function CheckAll()
    {
    var chk_state=document.getElementById("ChkSelAll").checked; 
     for(i=0; i<document.forms[0].elements.length; i++)
     {     
      if (document.forms[0].elements[i].name.search(/ChkBox/) > 0)
      document.forms[0].elements[i].checked = chk_state;
     }
    }   
    
    function getMultiValue()
    {
    var ChkValues = "";
    for(i=0; i<document.forms[0].elements.length; i++)
     {     
          if (document.forms[0].elements[i].name.search(/ChkBox/) > 0)
          {
              if (document.forms[0].elements[i].checked == true)
              {
              ChkValues = ChkValues + document.forms[0].elements[i].value + "@";
              }
          }
     }
    ChkValues = ChkValues.substring(0,ChkValues.length -1);
    getValue(ChkValues,'Multi','Multi')
    }
    </script>
</head>
<body class="matter" leftmargin="0" topmargin="0" bottommargin="0" rightmargin="0">
  <form id="form1" runat="server">
           

 <table width="98%" align="center" border="0" cellpadding="0" cellspacing="0">
            
      <tr>
        <td colspan="2" style ="cursor:hand; " >
        <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"  Width="100%" EmptyDataText="No Data" AllowPaging="True" PageSize="17" SkinID="GridViewView">
          <Columns>
            <asp:TemplateField HeaderText="Select" HeaderStyle-HorizontalAlign ="Left" >
            <HeaderTemplate>
              <input ID="ChkSelAll" type="checkbox" onclick ="CheckAll()" />
            </HeaderTemplate>
            <ItemTemplate>
              <input ID="ChkBox" type="checkbox"  runat="server"  value='<%# Bind("SelectId") %>' style ="cursor:hand; " />
            </ItemTemplate>
            </asp:TemplateField>
              <asp:BoundField DataField="ViewCode" HeaderText="Code"  />
              <asp:BoundField DataField="ViewName" HeaderText="Name" />
          </Columns>
        </asp:GridView>
        </td>
        <td>
        </td>
      </tr>      
   <tr>
     
     <td>
     <asp:Button ID="btnFinish" runat="server" CssClass="button" Text="Finish" OnClientClick ="getMultiValue()"  /></td>
   </tr>
    </table>  
 
</form>
</body>
</html>
