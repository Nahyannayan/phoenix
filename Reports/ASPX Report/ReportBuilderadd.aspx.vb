﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Reports_ASPX_Report_ReportBuilderadd
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim AegAmount As Double
    Dim SetAmount As Double

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            InitialiseCompnents()

        End If
    End Sub

    Public Function dbNames(ByVal connStr As String) As DataTable
        Dim sqlString As String = "SELECT db_name(database_id) as dbName FROM sys.master_files WHERE db_name(database_id) LIKE 'OA%' GROUP BY database_id order by db_name(database_id)"
        Dim _table As DataSet = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sqlString)
        Return _table.Tables(0)
    End Function
    Public Function viewNames(ByVal connStr As String, ByVal dbName As String) As DataTable
        Dim sqlString As String = "SELECT TABLE_NAME FROM " & dbName & ".INFORMATION_SCHEMA.VIEWS ORDER BY TABLE_NAME"
        Dim _table As DataSet = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sqlString)
        Return _table.Tables(0)
    End Function
    Public Function bindFields(ByVal connStr As String, ByVal dbName As String, ByVal viewName As String) As DataTable
        Dim sqlString As String = "SELECT * FROM " & dbName & ".." & viewName & " WHERE 1=2 ORDER BY 1"
        Dim _table As DataSet = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sqlString)
        Return _table.Tables(0)
    End Function
    
    Private Sub FilldbName()
        ddlDbName.DataSource = dbNames(ConnectionManger.GetOASISFINConnectionString)
        ddlDbName.DataTextField = "dbName"
        ddlDbName.DataValueField = "dbName"
        ddlDbName.DataBind()
    End Sub
    Private Sub FillViews()
        ddlDataView.Items.Clear()
        ddlDataView.DataSource = viewNames(ConnectionManger.GetOASISFINConnectionString, ddlDbName.SelectedItem.Text)
        ddlDataView.DataTextField = "TABLE_NAME"
        ddlDataView.DataValueField = "TABLE_NAME"
        ddlDataView.DataBind()
        ddlDataView.Items.Insert(0, " ")
        gvStudentDetails.DataSource = Nothing
        gvStudentDetails.DataBind()
        txtdbName.Text = ddlDbName.SelectedItem.Text
    End Sub

    Sub InitialiseCompnents()
        gvStudentDetails.Attributes.Add("bordercolor", "#1b80b6")
        FilldbName()
        FillViews()
        txtdbName.Text = ddlDbName.SelectedItem.Text
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Dim Name As New DataColumn("Name", System.Type.GetType("System.String"))
        Dim NameValue As New DataColumn("NameValue", System.Type.GetType("System.String"))
        dtDt.Columns.Add(Name)
        dtDt.Columns.Add(NameValue)
        Return dtDt
    End Function
    Private Function getDataTable(ByVal ds As DataTable) As DataTable
        Dim dt As New DataTable
        dt = CreateDataTable()

        For x As Integer = 0 To ds.Columns.Count - 1
            Dim dr As DataRow
            dr = dt.NewRow
            dr("Name") = ds.Columns(x).ColumnName
            dr("NameValue") = ds.Columns(x).ColumnName
            dt.Rows.Add(dr)
        Next
        Return dt
    End Function

    Protected Sub gvStudentDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim ImgSelect As New Image
        Dim txtQuery As New TextBox
        Dim h_seletId As New HiddenField
        If e.Row.RowType = DataControlRowType.DataRow Then
           
            ImgSelect = e.Row.FindControl("ImgSelect")
            txtQuery = e.Row.FindControl("txtQuery")
            h_seletId = e.Row.FindControl("h_seletId")
            ImgSelect.Attributes.Add("OnClick", "return   getWindow( '" + txtQuery.ClientID + "','" + h_seletId.ClientID + "')")
            ImgSelect.Style("cursor") = "hand"
        End If
    End Sub

    Protected Sub ddlDataView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDataView.SelectedIndexChanged
        bindGrid()
    End Sub

    Protected Sub ddlDbName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDbName.SelectedIndexChanged
        FillViews()

    End Sub
    Sub bindGrid()
        Dim _table As DataTable = bindFields(ConnectionManger.GetOASISFINConnectionString, ddlDbName.SelectedItem.Text, ddlDataView.SelectedItem.Text)
        _table = getDataTable(_table)
        gvStudentDetails.DataSource = _table
        gvStudentDetails.DataBind()
        txtViewname.Text = ddlDataView.SelectedItem.Text
    End Sub
    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        Try
            bindGrid()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            doInsert()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub doInsert()
        If txtdbName.Text = "" Then
            Throw New Exception("Invalid Database Name..!")
        End If
        If txtViewname.Text = "" Then
            Throw New Exception("Invalid View Name..!")
        End If
        If gvStudentDetails.Rows.Count = 0 Then
            Throw New Exception("Invalid Selection..!")
        End If
        Dim NewRVHId As String = ""
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim retval As Integer
        Try
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@RVH_dbDISCRIPTION", SqlDbType.VarChar)
            pParms(0).Value = txtdbName.Text
            pParms(1) = New SqlClient.SqlParameter("@RVH_dbNAME", SqlDbType.VarChar)
            pParms(1).Value = ddlDbName.SelectedItem.Text
            pParms(2) = New SqlClient.SqlParameter("@RVS_ViewName", SqlDbType.VarChar)
            pParms(2).Value = ddlDataView.SelectedItem.Text
            pParms(3) = New SqlClient.SqlParameter("@RVS_ViewDiscription", SqlDbType.VarChar)
            pParms(3).Value = txtViewname.Text
            pParms(4) = New SqlClient.SqlParameter("@RVS_RVH_ID", SqlDbType.BigInt)
            pParms(4).Direction = ParameterDirection.Output
            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
           
            retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "RB_InsertREPORTVIEW_H", pParms)
            NewRVHId = pParms(4).Value
            If pParms(5).Value <> 0 Or NewRVHId = "" Then
                stTrans.Rollback()
                objConn.Close()
                Throw New Exception(getErrorMessage(pParms(4).ToString()))
            End If

            For Each grow As GridViewRow In gvStudentDetails.Rows

                Dim ChkSelect As HtmlInputCheckBox = CType(grow.FindControl("ChkSelect"), HtmlInputCheckBox)
                Dim txtFieldName As TextBox = CType(grow.FindControl("txtFieldName"), TextBox)
                Dim txtQuery As TextBox = CType(grow.FindControl("txtQuery"), TextBox)
                Dim ChkFilter As HtmlInputCheckBox = CType(grow.FindControl("ChkFilter"), HtmlInputCheckBox)
                Dim h_seletId As HiddenField = CType(grow.FindControl("h_seletId"), HiddenField)

                If ChkSelect IsNot Nothing And ChkSelect.Checked Then
                    Dim pParms1(6) As SqlClient.SqlParameter
                    pParms1(0) = New SqlClient.SqlParameter("@RVD_RVS_ID", SqlDbType.BigInt)
                    pParms1(0).Value = NewRVHId
                    pParms1(1) = New SqlClient.SqlParameter("@RVD_FNAME", SqlDbType.VarChar)
                    pParms1(1).Value = grow.Cells(1).Text
                    pParms1(2) = New SqlClient.SqlParameter("@RVD_FDISCRIPTION", SqlDbType.VarChar)
                    pParms1(2).Value = txtFieldName.Text
                    pParms1(3) = New SqlClient.SqlParameter("@RVD_bFilter", SqlDbType.Bit)
                    pParms1(3).Value = ChkFilter.Checked
                    pParms1(4) = New SqlClient.SqlParameter("@RVD_FQR_ID", SqlDbType.VarChar)
                    pParms1(4).Value = h_seletId.Value
                    pParms1(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms1(5).Direction = ParameterDirection.ReturnValue
                    retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "RB_InsertREPORTVIEW_D", pParms1)
                    If pParms1(5).Value <> 0 Then
                        stTrans.Rollback()
                        objConn.Close()
                        Throw New Exception(getErrorMessage(pParms(5).ToString()))
                    End If
                End If

            Next
        Catch ex As Exception
            stTrans.Rollback()
            objConn.Close()
            Throw New Exception(ex.Message)
        End Try
        stTrans.Commit()
        objConn.Close()
        lblError.Text = getErrorMessage("0")
    End Sub
End Class
