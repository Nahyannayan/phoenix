Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.IO
Imports system.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections

Partial Class Reports_ASPX_Report_rptReportViewer
    Inherits System.Web.UI.Page
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Dim smScriptManager As New ScriptManager
        'smScriptManager = Master.FindControl("ScriptManager1")

        'smScriptManager.EnablePartialRendering = False


        Try
            rs.CacheDuration = 1
            rs.EnableCaching = False
            LoadReports(sender, e)
        Catch ex As Exception
        End Try
    End Sub
    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Using msOur As New System.IO.MemoryStream()
            Using swOur As New System.IO.StreamWriter(msOur)
                Dim ourWriter As New HtmlTextWriter(swOur)
                MyBase.Render(ourWriter)
                ourWriter.Flush()
                msOur.Position = 0
                Using oReader As New System.IO.StreamReader(msOur)
                    Dim sTxt As String = oReader.ReadToEnd()
                    If Web.Configuration.WebConfigurationManager.AppSettings("AlwaysSSL").ToLower = "true" Then
                        sTxt = sTxt.Replace(Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString(), _
                        Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString().Replace("http://", "https://"))
                    End If
                    Response.Write(sTxt)
                    oReader.Close()
                End Using
            End Using
        End Using
    End Sub




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Try
        '    rs.CacheDuration = 1
        '    rs.EnableCaching = False
        '    LoadReports(sender, e)
        'Catch ex As Exception
        'End Try
    End Sub

    Sub LoadReports(ByVal sender As Object, ByVal e As System.EventArgs)
        Try



            Dim rptClass As New rptClass
            Dim iRpt As New DictionaryEntry
            Dim i As Integer
            Dim newWindow As String

            Try
                newWindow = Request.QueryString("newWindow")
            Catch ex As Exception

            End Try
            Dim rptStr As String = ""
            If Not newWindow Is Nothing Then
                rptStr = "rptClass" + newWindow
            Else
                rptStr = "rptClass"
            End If


            ' rptClass = Session("rptClass")
            rptClass = Session.Item(rptStr)

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues

            With rptClass




                'If Not Session("prevClass") Is Nothing And newWindow Is Nothing Then
                '    Session("rptClass") = Session("prevClass")
                '    rptClass = Session("prevClass")
                '    Session("prevClass") = Nothing
                '    Page_Load(sender, e)
                '    Exit Sub
                'End If

                rs.ReportDocument.Load(.reportPath)
                'rs.ReportDocument.DataSourceConnections(0).SetConnection(.crInstanceName, .crDatabase, .crUser, .crPassword) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
                'rs.ReportDocument.SetDatabaseLogon(.crUser, .crPassword, .crInstanceName, .crDatabase, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '
                'rs.ReportDocument.VerifyDatabase()

                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword


                SetDBLogonForSubreports(myConnectionInfo, rs.ReportDocument, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs.ReportDocument, .reportParameters)


                'If .subReportCount <> 0 Then
                '    For i = 0 To .subReportCount - 1
                '        rs.ReportDocument.Subreports(i).VerifyDatabase()
                '    Next
                'End If


                crParameterFieldDefinitions = rs.ReportDocument.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If

                '  rs.ReportDocument.VerifyDatabase()


                If .selectionFormula <> "" Then
                    rs.ReportDocument.RecordSelectionFormula = .selectionFormula
                End If


            End With
        Catch ex As Exception


        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue
        'Dim crParameterFieldDefinitions As ParameterFieldDefinitions
        'Dim crParameterFieldLocation As ParameterFieldDefinition
        'Dim crParameterValues As ParameterValues
        'Dim iRpt As New DictionaryEntry

        ' Dim myTableLogonInfo As TableLogOnInfo
        ' myTableLogonInfo = New TableLogOnInfo
        'myTableLogonInfo.ConnectionInfo = myConnectionInfo

        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

        Next


        'crParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        'If reportParameters.Count <> 0 Then
        '    For Each iRpt In reportParameters
        '        Try
        '            crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
        '            crParameterValues = crParameterFieldLocation.CurrentValues
        '            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
        '            crParameterDiscreteValue.Value = iRpt.Value
        '            crParameterValues.Add(crParameterDiscreteValue)
        '            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
        '        Catch ex As Exception
        '        End Try
        '    Next
        'End If

        'myReportDocument.DataSourceConnections(0).SetConnection(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
        'myReportDocument.SetDatabaseLogon(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '

        myReportDocument.VerifyDatabase()


    End Sub
    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

                    If subReportDocument.Name <> "studphotos" Then
                        SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    Else
                        Try

                        
                            Dim dS As New dsImageRpt
                            Dim fs As New FileStream(Session("StudentPhotoPath"), System.IO.FileMode.Open, System.IO.FileAccess.Read)
                            Dim Image As Byte() = New Byte(fs.Length - 1) {}
                            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
                            fs.Close()
                            Dim dr As dsImageRpt.StudPhotoRow = dS.StudPhoto.NewStudPhotoRow
                            dr.Photo = Image
                            'Add the new row to the dataset
                            dS.StudPhoto.Rows.Add(dr)
                            subReportDocument.SetDataSource(dS)
                        Catch ex As Exception
                            UtilityObj.Errorlog(ex.Message)
                        End Try
                    End If

                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload

        'crv.Dispose()
        'crv = Nothing
        ' rs.Dispose()
        ' rs = Nothing

    End Sub
End Class
