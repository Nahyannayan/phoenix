Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_BankBook
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            h_MAINMENU.Value = MainMnu_code
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle

            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            UtilityObj.NoOpen(Me.Header)
            trBSUMulti.Visible = False
            trBSUSingle.Visible = False
            TrMultiYear.Visible = False
            TrDOC.Visible = False
            Select Case MainMnu_code
                Case "A350078" 'Multiyear Columnar
                    TrMultiYear.Visible = True
                    TrDOC.Visible = True
                    trBSUMulti.Visible = True
                    chkGroupCurrency.Visible = False
                    chkSummary.Visible = False
                    ddlReport.Visible = True
                    If Not IsPostBack Then
                        ddlReport.DataSource = SqlHelper.ExecuteDataset(WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString, CommandType.Text, "select cast(RPT as varchar)+';'+RPT_ID RPT_ID, RPT_DESCR from VW_REPORTBAK order by RPT")
                        ddlReport.DataTextField = "RPT_DESCR"
                        ddlReport.DataValueField = "RPT_ID"
                        ddlReport.DataBind()
                    End If
                    chkConsolidation.Text = "Ignore year end P&L entry"
                    lblrptCaption.Text = "Multiyear Columnar Report"
                    h_Mode.Value = "general"
                    lblBankCash.Text = "Account"
                Case "A450005" '"General Ledger"
                    'Trbank.Visible = True
                    lblrptCaption.Text = "General Ledger Report"
                    lblBankCash.Text = "Account"
                    h_Mode.Value = "general"
                    TrDOC.Visible = True
                    trBSUMulti.Visible = True
                    chkSummary.Visible = True
                Case "A750005" '"BankBook"
                    'Trbank.Visible = True
                    lblrptCaption.Text = "Bank Book Report"
                    h_Mode.Value = "bank"
                    lblBankCash.Text = "Bank"
                    trBSUSingle.Visible = True
                Case "A450015" '"CashBook"
                    'Trbank.Visible = False
                    lblrptCaption.Text = "Cash Book Report"
                    h_Mode.Value = "cash"
                    lblBankCash.Text = "Cash"
                    trBSUSingle.Visible = True
                Case "A550005" 'Party Ledger
                    lblrptCaption.Text = "Creditors Ledger"
                    chkConsolidation.Visible = False
                    chkSummary.Visible = True
                    h_Mode.Value = "party"
                    trBSUMulti.Visible = True
                    lblBankCash.Text = "Creditor"
                    h_ACTTYPE.Value = "s"
                    TrDOC.Visible = True
                Case "A550015" 'Party Statement
                    lblrptCaption.Text = "Creditors Statement"
                    chkConsolidation.Visible = False
                    h_Mode.Value = "party"
                    lblBankCash.Text = "Creditor"
                    trBSUSingle.Visible = True
                    h_ACTTYPE.Value = "s"
                Case "A550016"
                    lblrptCaption.Text = "Creditors Trial Balance"
                    lblBankCash.Text = "Creditor"
                    h_Mode.Value = "party"
                    chkSummary.Visible = False
                    'trBSUSingle.Visible = True
                    trBSUMulti.Visible = True
                    h_ACTTYPE.Value = "s"
                Case "A750040" '"BankBook Summary" 
                    lblrptCaption.Text = "Bank Book Summary"
                    h_Mode.Value = "bank"
                    lblBankCash.Text = "Bank"
                    chkSummary.Visible = True
                    trBSUSingle.Visible = True
                Case "A750042" '"BankBook MIS" 
                    lblrptCaption.Text = "Bank Book MIS"
                    h_Mode.Value = "bankmis"
                    lblBankCash.Text = "Bank"
                    chkSummary.Visible = True
                    trBSUSingle.Visible = False
                    trBSUMulti.Visible = True
                Case "A450025" '"CashBook Summary" 
                    lblrptCaption.Text = "Cash Book Summary"
                    h_Mode.Value = "cash"
                    lblBankCash.Text = "Cash"
                    chkSummary.Visible = True
                    trBSUSingle.Visible = True
                Case "A550021"  'Debtors Trial Balance
                    lblrptCaption.Text = "Debtors Trial Balance"
                    lblBankCash.Text = "Debtor"
                    h_Mode.Value = "party"
                    chkSummary.Visible = False
                    trBSUSingle.Visible = True
                    h_ACTTYPE.Value = "C"
                Case "A550020"  'Debtors Statement
                    lblrptCaption.Text = "Debtors Statement"
                    chkConsolidation.Visible = False
                    h_Mode.Value = "party"
                    lblBankCash.Text = "Debtor"
                    trBSUSingle.Visible = True
                    h_ACTTYPE.Value = "C"
                Case "A550004"
                    lblrptCaption.Text = "Debtors Ledger"
                    chkConsolidation.Visible = False
                    chkSummary.Visible = True
                    h_Mode.Value = "party"
                    trBSUMulti.Visible = True
                    lblBankCash.Text = "Debtor"
                    h_ACTTYPE.Value = "C"
            End Select
            'txtToDate.Attributes.Add("ReadOnly", "Readonly")
            'txtFromDate.Attributes.Add("ReadOnly", "Readonly")

            If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
                If h_Mode.Value <> "party" Then
                    h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
                    h_BSUID.Value = h_BSUID.Value.Split("___")(0)
                End If
            Else
                h_BSUID.Value = ddlBSUnit.SelectedValue
                ' h_BSUID.Value = Session("sBsuid")
            End If
            FillBSUNames(h_BSUID.Value)
            If h_ACTIDs.Value <> Nothing And h_ACTIDs.Value <> "" And h_ACTIDs.Value <> "undefined" Then
                FillACTIDs(h_ACTIDs.Value)
                'h_BSUID.Value = ""
            End If
            If h_DOCIDs.Value <> Nothing And h_DOCIDs.Value <> "" And h_DOCIDs.Value <> "undefined" Then
                FillDOCIDs(h_DOCIDs.Value)
                'h_BSUID.Value = ""
            End If
            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ddlBSUnit.DataSource = UtilityObj.GetBusinessUnits(Session("sUsr_name"))
                ddlBSUnit.DataTextField = "BSU_NAME"
                ddlBSUnit.DataValueField = "BSU_ID"
                ddlBSUnit.DataBind()
                ddlBSUnit.SelectedValue = Session("sbsuid")
                txtToDate.Text = UtilityObj.GetDiplayDate()
                txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))

                txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
                txtToDate.Attributes.Add("onBlur", "checkdate(this)")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function


    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Try
            Dim IDs As String() = ACTIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdACTDetails.DataSource = ds
            grdACTDetails.DataBind()
            'dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            'txtBankNames.Text = ""
            'Dim bval As Boolean = dr.Read
            'While (bval)
            '    txtBankNames.Text += dr(0).ToString()
            '    bval = dr.Read()
            '    If bval Then
            '        txtBankNames.Text += "||"
            '    End If
            'End While
            'txtbankCodes.Text = ACTIDs
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            txtFromDate.Text = Format("dd/MMM/yyyy", txtFromDate.Text)
            txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
            Dim strXMLBSUNames As String
            'Generate the XML in the form <BSU_DETAILS><BSU_ID>IDhere</BSU_ID></BSU_DETAILS>
            If trBSUMulti.Visible = True Then
                h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
            Else
                h_BSUID.Value = ddlBSUnit.SelectedValue
            End If

            Session("Fdate") = txtFromDate.Text
            Session("Tdate") = txtToDate.Text
            Session("selBSUID") = h_BSUID.Value
            Session("selACTID") = h_ACTIDs.Value

            strXMLBSUNames = GenerateXML(h_BSUID.Value, XMLType.BSUName) 'txtBSUNames.Text)

            Select Case MainMnu_code
                Case "A350078"
                    MultiYearColumnar()
                Case "A450005", "A450015", "A750005" '"General Ledger" "BankBook" "CashBook"
                    GenerateCashBankLedger(strXMLBSUNames)
                Case "A550005", "A550004" 'Party Ledger
                    GeneratePartyLedger(strXMLBSUNames)
                Case "A550015", "A550020" 'Creditors or Debtors Party Ledger
                    GeneratePartyStatement(strXMLBSUNames)
                Case "A550016", "A550021" 'Creditors or Debtors Trial Balance
                    GeneratePartyTrialbalanceReport(strXMLBSUNames)
                Case "A750040", "A450025", "A750042"
                    RPTLEDGERBANKBOOKSUMMARY(strXMLBSUNames)
            End Select

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub GeneratePartyTrialbalanceReport(ByVal strXMLBSUNames As String)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand
        Select Case MainMnu_code
            Case "A550016"
                cmd = New SqlCommand("[RPTGETPARTYTRIALBALANCE]")
            Case "A550021"
                cmd = New SqlCommand("[RPTGETPARTYDEBITORTRIALBALANCE]")
        End Select
        'Dim cmd As New SqlCommand("[RPTGETPARTYTRIALBALANCE]", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        sqlpACT_IDs.Value = UtilityObj.GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
        cmd.Parameters.Add(sqlpACT_IDs)

        'Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        'sqlpJHD_FYEAR.Value = Session("financialyear")
        'cmd.Parameters.Add(sqlpJHD_FYEAR)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        sqlpConsolidation.Value = chkConsolidation.Checked
        cmd.Parameters.Add(sqlpConsolidation)

        Dim sqlpGroupCurrency As New SqlParameter("@bShowinGrpCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        'adpt.SelectCommand = cmd
        'objConn.Close()
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Session("BSUNames") = strXMLBSUNames
            Session("rptFromDate") = txtFromDate.Text
            Session("rptToDate") = txtToDate.Text
            Session("rptConsolidation") = chkConsolidation.Checked
            Session("rptGroupCurrency") = chkGroupCurrency.Checked

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            params("Type") = 0 'cmbGroupType.SelectedItem.Value
            params("parent") = ""
            Select Case MainMnu_code
                Case "A550016"
                    params("RPT_CAPTION") = "Creditors Trial Balance"
                Case "A550021"
                    params("RPT_CAPTION") = "Debtors Trial Balance"
            End Select

            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptTrailBalance.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                ' Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Private Sub GeneratePartyStatement(ByVal strXMLBSUNames As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand

        Select Case MainMnu_code
            Case "A550015"
                cmd = New SqlCommand("RPTPARTYsStatement")
            Case "A550020"
                cmd = New SqlCommand("RPTPARTYsDebitorStatement")
        End Select
        'Dim cmd As New SqlCommand("RPTPARTYsStatement", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@TRN_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        sqlpACT_IDs.Value = GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
        cmd.Parameters.Add(sqlpACT_IDs)

        Dim sqlpJHD_FYEAR As New SqlParameter("@TRN_FYEAR", SqlDbType.Int)
        sqlpJHD_FYEAR.Value = Session("F_YEAR")
        cmd.Parameters.Add(sqlpJHD_FYEAR)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@TODOCDT", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        'adpt.SelectCommand = cmd
        objConn.Close()
        objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("UserName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            Select Case MainMnu_code
                Case "A550015"
                    params("ReportCaption") = "Creditors Statement"
                Case "A550020"
                    params("ReportCaption") = "Debtors Statement"
            End Select
            params("partyAging") = False
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptPartyStatement.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                ' Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub


    Private Sub GeneratePartyLedger(ByVal strXMLBSUNames As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand

        Select Case MainMnu_code
            Case "A550005"
                cmd = New SqlCommand("RPTPARTYsLEDGER")
            Case "A550004"
                cmd = New SqlCommand("RPTPARTYsDEBITORsLEDGER")
        End Select
        'Dim cmd As New SqlCommand("RPTPARTYsLEDGER", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        If MainMnu_code = "A550005" Then
            Dim sqlpDOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar)
            sqlpDOCTYPE.Value = h_DOCIDs.Value.Replace("|", ",")
            cmd.Parameters.Add(sqlpDOCTYPE)
        End If

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@TRN_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        sqlpACT_IDs.Value = GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
        cmd.Parameters.Add(sqlpACT_IDs)

        Dim sqlpJHD_FYEAR As New SqlParameter("@TRN_FYEAR", SqlDbType.Int)
        sqlpJHD_FYEAR.Value = Session("F_YEAR")
        cmd.Parameters.Add(sqlpJHD_FYEAR)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@TODOCDT", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        'adpt.SelectCommand = cmd
        'objConn.Open()
        'adpt.Fill(ds)
        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("UserName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            Select Case MainMnu_code
                Case "A550005"
                    params("ReportCaption") = "Creditors Ledger"
                Case "A550004"
                    params("ReportCaption") = "Debtors Ledger"
            End Select

            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            If chkSummary.Checked Then
                repSource.ResourceName = "../RPT_Files/rptPartysLedgerSummary.rpt"
            Else
                repSource.ResourceName = "../RPT_Files/rptPartysLedgerwithAging.rpt"
            End If
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                ' Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub


    Private Sub GenerateCashBankLedger(ByVal strXMLBSUNames As String)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTLEDGER", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        If MainMnu_code = "A450005" Then
            Dim sqlpDOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar)
            sqlpDOCTYPE.Value = h_DOCIDs.Value.Replace("|", ",")
            cmd.Parameters.Add(sqlpDOCTYPE)
        End If

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@JHD_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        sqlpACT_IDs.Value = GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
        cmd.Parameters.Add(sqlpACT_IDs)

        Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        sqlpJHD_FYEAR.Value = Session("F_YEAR")
        cmd.Parameters.Add(sqlpJHD_FYEAR)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@TODOCDT", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        'Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        'sqlpConsolidation.Value = chkConsolidation.Checked
        'cmd.Parameters.Add(sqlpConsolidation)

        Dim sqlpGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)
        '''''''''''''
        Dim param As SqlParameterCollection = cmd.Parameters

        ''''''''''
        'adpt.SelectCommand = cmd
        'objConn.Close()
        'cmd.CommandTimeout = 0
        'objConn.Open()
        'adpt.Fill(ds)
        'objConn.Close()
        ''ds = SqlHelper.ExecuteDataset(str_conn, cmd.CommandText, param)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("UserName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            params("ReportCaption") = "Bank Book"
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            Select Case MainMnu_code
                Case "A450005" '"General Ledger
                    repSource.ResourceName = "../RPT_Files/rptCashBook.rpt"
                    params("ReportCaption") = "General Ledger"
                    If chkSummary.Checked Then
                        Session("BSUNames") = strXMLBSUNames
                        Session("rptFromDate") = txtFromDate.Text
                        Session("rptToDate") = txtToDate.Text
                        Session("rptGroupCurrency") = chkGroupCurrency.Checked
                    End If
                    params("summary") = chkSummary.Checked
                Case "A750005" '"BankBook"
                    repSource.ResourceName = "../RPT_Files/rptBankBook.rpt"
                    params("ReportCaption") = "Bank Book"
                Case "A450015" '"CashBook"
                    repSource.ResourceName = "../RPT_Files/rptCashBook.rpt"
                    params("summary") = False
                    params("ReportCaption") = "Cash Book"
            End Select
            'repSource.ResourceName = "../RPT_Files/rptBankBook.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                '  Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()

    End Sub
    'Generates the XML for BSUnit

    Private Sub RPTLEDGERBANKBOOKSUMMARY(ByVal strXMLBSUNames As String)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTLEDGERBANKBOOKSUMMARY", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@JHD_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        sqlpACT_IDs.Value = GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
        cmd.Parameters.Add(sqlpACT_IDs)

        Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        If MainMnu_code = "A750042" Then
            sqlpJHD_FYEAR.Value = "0000"
        Else
            sqlpJHD_FYEAR.Value = Session("F_YEAR")
        End If
        cmd.Parameters.Add(sqlpJHD_FYEAR)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@TODOCDT", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpbSummary As New SqlParameter("@bSummary", SqlDbType.Bit)
        sqlpbSummary.Value = chkSummary.Checked
        cmd.Parameters.Add(sqlpbSummary)

        Dim sqlpGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)
        '''''''''''''
        Dim param As SqlParameterCollection = cmd.Parameters

        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("UserName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            params("ReportCaption") = "Bank Book"
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            Select Case MainMnu_code
                Case "A750040", "A750042" '"BankBook summary"
                    repSource.ResourceName = "../RPT_Files/rptBankBook_Summary.rpt"
                    params("ReportCaption") = "Bank Book Summary"
                Case "A450025" '"cash Book  summary"
                    repSource.ResourceName = "../RPT_Files/rptBankBook_Summary.rpt"
                    params("ReportCaption") = "Cash Book Summary"
            End Select

            Session("ReportSource") = repSource
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                ReportLoadSelection()
                'Response.Redirect("rptviewer.aspx", True)
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()

    End Sub

    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.ACTName
                elements(0) = "ACT_DETAILS"
                elements(1) = "ACT_DETAIL"
                elements(2) = "ACT_ID"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                XMLEBSUID = xmlDoc.CreateElement(elements(2))
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function


    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub


    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        'grdBSU.DataSource = ds
        'grdBSU.DataBind()
        'txtBSUName.Text = ""
        'Dim bval As Boolean = dr.Read
        'While (bval)
        '    txtBSUName.Text += dr(0).ToString()
        '    bval = dr.Read()
        '    If bval Then
        '        txtBSUName.Text += "||"
        '    End If
        'End While
        Return True
    End Function


    'Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
    '    If h_Mode.Value = "party" Then
    '        h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
    '    ElseIf FillBSUNames(txtBSUName.Text) Then
    '        h_BSUID.Value = txtBSUName.Text
    '    End If
    '    FillBSUNames(h_BSUID.Value)
    'End Sub


    'Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
    '    grdBSU.PageIndex = e.NewPageIndex
    '    FillBSUNames(h_BSUID.Value)
    'End Sub


    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            'grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub


    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(h_ACTIDs.Value)
        End If

    End Sub


    Protected Sub lnkbtngrdDOCDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDOCID As New Label
        lblDOCID = TryCast(sender.FindControl("lblDOCID"), Label)
        If Not lblDOCID Is Nothing Then
            h_DOCIDs.Value = h_DOCIDs.Value.Replace(lblDOCID.Text, "").Replace("||||", "||")
            grdDOCDetails.PageIndex = grdDOCDetails.PageIndex
            FillDOCIDs(h_DOCIDs.Value)
        End If

    End Sub

    Private Sub FillDOCIDs(ByVal DOCIDs As String)
        Try
            Dim IDs As String() = DOCIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            str_Sql = "select DOC_ID, DOC_NAME from DOCUMENT_M where DOC_FYR_ID IN (SELECT max(DOC_FYR_ID) from DOCUMENT_M) AND DOC_ID IN('" + DOCIDs.Replace("|", "','") + "')"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdDOCDetails.DataSource = ds
            grdDOCDetails.DataBind()
            'dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            'txtBankNames.Text = ""
            'Dim bval As Boolean = dr.Read
            'While (bval)
            '    txtBankNames.Text += dr(0).ToString()
            '    bval = dr.Read()
            '    If bval Then
            '        txtBankNames.Text += "||"
            '    End If
            'End While
            'txtbankCodes.Text = DOCIDs
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub grdDOCDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdACTDetails.PageIndexChanging
        grdDOCDetails.PageIndex = e.NewPageIndex
        FillBSUNames(h_DOCIDs.Value)
    End Sub

    Protected Sub lblAddDocID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddDocID.Click
        'If h_Mode.Value = "party" Then
        h_DOCIDs.Value += "||" + txtDOCNames.Text.Replace(",", "||")
        'ElseIf FillACTIDs(txtBSUName.Text) Then
        h_DOCIDs.Value = txtDOCNames.Text
        'End If
        FillDOCIDs(h_DOCIDs.Value)
    End Sub

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdACTDetails.PageIndexChanging
        grdACTDetails.PageIndex = e.NewPageIndex
        FillBSUNames(h_ACTIDs.Value)
    End Sub


    Protected Sub lblAddActID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddActID.Click
        'If h_Mode.Value = "party" Then
        h_ACTIDs.Value += "||" + txtBankNames.Text.Replace(",", "||")
        'ElseIf FillACTIDs(txtBSUName.Text) Then
        h_ACTIDs.Value = txtBankNames.Text
        'End If
        FillACTIDs(h_ACTIDs.Value)
    End Sub


    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
            txtFromDate.Text = Session("FDate")
            txtToDate.Text = Session("TDate")
            FillACTIDs(Session("selACTID"))
            If trBSUSingle.Visible = True Then
                ddlBSUnit.SelectedValue = Session("selBSUID")
            Else
                UsrBSUnits1.SetSelectedNodes(Session("selBSUID"))
            End If
        End If
    End Sub

    Private Sub MultiYearColumnar()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("rptMultiYearColumnarBak", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpConsolidation As New SqlParameter("@multiyear", SqlDbType.Int)
        sqlpConsolidation.Value = IIf(chkConsolidation.Checked, 1, 0)
        cmd.Parameters.Add(sqlpConsolidation)

        If h_ACTIDs.Value <> "" Then
            Dim sqlpJHD_ACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
            sqlpJHD_ACT_IDs.Value = UtilityObj.GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
            cmd.Parameters.Add(sqlpJHD_ACT_IDs)
        End If

        Dim sqlReport As New SqlParameter("@Report", SqlDbType.VarChar)
        sqlReport.Value = ddlReport.SelectedValue.ToString.Split(";")(0)
        cmd.Parameters.Add(sqlReport)

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = UtilityObj.GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("reportCaption") = "MultiYear Columnar Report"
            params("reportDate") = "" & txtFromDate.Text & " To " & txtToDate.Text
            params("BSUName") = ""

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/" & ddlReport.SelectedValue.ToString.Split(";")(1)
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

End Class