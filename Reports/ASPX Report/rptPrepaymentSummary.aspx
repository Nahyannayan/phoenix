<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPrepaymentSummary.aspx.vb" Inherits="Reports_ASPX_Report_rptPrepaymentSummary" title="Untitled Page" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">
           function getDate(left,top,txtControl) 
           {
            var sFeatures; 
            sFeatures="dialogWidth: 250px; ";
            sFeatures+="dialogHeight: 270px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx","", sFeatures);
            if(result != '' && result != undefined)
            {
                switch(txtControl)
                {
                  case 0:
                    document.getElementById('<%=txtfromDate.ClientID %>').value=result;
                    break;
                  case 1:  
                    document.getElementById('<%=txtToDate.ClientID %>').value=result;
                    break;
                }    
             }
             return false;   
           }
   
       function GetBSUName()
       {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx","", sFeatures)
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else
            {
                return false;            
            }
        }
        
      function GetBankName(id)
       {     
            var sFeatures;
            sFeatures="dialogWidth: 820px; ";
            sFeatures+="dialogHeight: 450px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var mode;
            var result;
            switch(id)
            {
                case 0:
                   result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?type=exp&multiSelect=false&forrpt=1","", sFeatures)
                   break;
                case 1:
                   result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?type=pre&multiSelect=false&forrpt=1","", sFeatures)
                   break;
            }
            if (result=='' || result==undefined)
            {
                return false;
            }
            NameandCode = result.split('___');
            switch(id)
            {
                case 1:
                   document.getElementById('<%=txtPrActID.ClientID %>').value=NameandCode[0]; 
                   document.getElementById('<%=txtBankName.ClientID %>').value=NameandCode[1]; 
                break;
                case 0:
                    document.getElementById('<%=txtExpActID.ClientID %>').value=NameandCode[0]; 
                    document.getElementById('<%=txtAccountName.ClientID %>').value=NameandCode[1]; 
                break;
            }
           return false;
      }  

    </script> 

    <table align="center" style="width: 83%">
        <tr align="left">
            <td>
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="PPSummary" />
            </td>
        </tr>
        <tr align="left">
            <td>
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
    </table>
    <table align="center"class="BlueTable" cellpadding="5" cellspacing="0" style="width: 80%; height: 174px">
        <tr class="subheader_img">
            <td align="left" colspan="8" style="height: 19px" valign="middle"> 
                        <asp:Label ID="lblrptCaption" runat="server" Text="Prepayment Summary Report"></asp:Label> 
            </td>
        </tr>
        <tr>
            <td align="left" class="matters"   valign="top" style="width: 68px">
                Business Unit</td>
            <td class="matters" style="width: 18px" valign="top">
                :</td>
            <td align="left" class="matters" colspan="6" valign="top">
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 68px">
                Prepaid Account</td>
            <td class="matters" style="width: 18px">
                :</td>
            <td align="left" class="matters" colspan="6" style="height: 1px; text-align: left">
                <asp:TextBox ID="txtBankName" runat="server" CssClass="inputbox" Height="20px" Width="379px"></asp:TextBox>
                <img
                    onclick="return GetBankName(1)" src="../../Images/forum_search.gif" style="width: 23px;
                    height: 20px" />
                <asp:TextBox ID="txtPrActID" runat="server" CssClass="inputbox" Height="20px"
                        Width="142px"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 68px">
                Expense Account</td>
            <td class="matters" style="width: 18px">
                :</td>
            <td align="left" class="matters" colspan="6" style="height: 1px; text-align: left">
                <asp:TextBox ID="txtAccountName" runat="server" CssClass="inputbox" Width="379px"></asp:TextBox>
                <img onclick="return GetBankName(0)" src="../../Images/forum_search.gif" style="width: 22px;
                    height: 21px" />
                <asp:TextBox ID="txtExpActID" runat="server" CssClass="inputbox" Width="144px"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 68px"  >
                From Date</td>
            <td class="matters" style="width: 18px">
                :</td>
            <td align="left" class="matters" colspan="2" style="height: 1px; text-align: left">
                <asp:TextBox ID="txtFromDate" runat="server" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="PPSummary">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="PPSummary">*</asp:RegularExpressionValidator></td>
            <td align="left" class="matters" style="color: #1b80b6">
                To Date</td>
            <td align="left" class="matters" style="height: 1px">
                :</td>
            <td align="left" class="matters" colspan="2" style="height: 1px; text-align: left">
                <asp:TextBox ID="txtToDate" runat="server" Width="122px"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="PPSummary">*</asp:RequiredFieldValidator>&nbsp;<asp:RegularExpressionValidator
                        ID="revToDate" runat="server" ControlToValidate="txtToDate" Display="Dynamic"
                        EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                        ValidationGroup="PPSummary">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 68px">
                Status</td>
            <td class="matters" style="width: 18px">
                :</td>
            <td align="left" class="matters" colspan="6" style="height: 1px; text-align: left">
                &nbsp;
                <asp:RadioButton ID="radStatusAll" runat="server" Checked="True" GroupName="chkStatus"
                    Text="All" />
                &nbsp;
                <asp:RadioButton ID="radStatusOpen" runat="server" GroupName="chkStatus" Text="Open" />
                &nbsp;
                <asp:RadioButton ID="radStatusPosted" runat="server" GroupName="chkStatus" Text="Posted" />
                &nbsp;
                <asp:RadioButton ID="radStatusDeleted" runat="server" GroupName="chkStatus" Text="Deleted" /></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="text-align: right">
                <asp:LinkButton id="lnkExporttoexcel" runat="server" onclick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>&nbsp;
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="PPSummary" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="h_BSUID" runat="server" />
    <asp:HiddenField ID="h_mode" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
</asp:Content>

