Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports Crystaldecisions.reportsource
Imports Crystaldecisions.shared
Imports CrystalDecisions.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration

Partial Class Reports_ASPX_Report_printReport
    Inherits System.Web.UI.Page

    Dim repSource As MyReportClass
    Dim repClassVal As New RepClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
        End If
        If Not IsPostBack Then
            repSource = DirectCast(Session("ReportSource"), MyReportClass)
        Else
            Response.Redirect(Request.UrlReferrer.ToString())
        End If

        If Not repSource Is Nothing Then
            If repSource.GetDataSourceFromCommand Then
                'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                'Dim objConn As New SqlConnection(str_conn)
                Dim objConn As SqlConnection = repSource.Command.Connection
                'repSource.Command.Connection = objConn
                objConn.Close()
                objConn.Open()
                Dim ds As New DataSet
                Dim adpt As New SqlDataAdapter
                adpt.SelectCommand = repSource.Command
                adpt.Fill(ds)
                objConn.Close()
                repClassVal = New RepClass
                repClassVal.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                repClassVal.ResourceName = repSource.ResourceName
                repClassVal.SetDataSource(ds.Tables(0))
                SubReport(repSource, repClassVal)
                CrystalReportViewer1.ReportSource = repClassVal
                If repSource.Parameter IsNot Nothing Then
                    Dim ienum As IDictionaryEnumerator = repSource.Parameter.GetEnumerator()
                    While ienum.MoveNext()
                        repClassVal.SetParameterValue(ienum.Key, ienum.Value)
                    End While
                End If
                objConn.Close()
            End If
        End If
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub SubReport(ByVal subRep As MyReportClass, ByRef repClassVal As RepClass)
        If subRep.SubReport IsNot Nothing Then
            For i As Integer = 0 To subRep.SubReport.Length - 1
                Dim myrep As MyReportClass = subRep.SubReport(i)
                If myrep IsNot Nothing Then
                    If myrep.SubReport IsNot Nothing Then
                        SubReport(subRep, repClassVal)
                    Else
                        Dim objConn As SqlConnection = repSource.Command.Connection
                        'objConn.Close()
                        objConn.Open()
                        Dim ds As New DataSet
                        Dim adpt As New SqlDataAdapter
                        adpt.SelectCommand = myrep.Command
                        adpt.Fill(ds)
                        objConn.Close()
                        repClassVal.Subreports(i).SetDataSource(ds.Tables(0))
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        CrystalReportViewer1.Dispose()
        CrystalReportViewer1 = Nothing
        'GC.Collect()
    End Sub
End Class
