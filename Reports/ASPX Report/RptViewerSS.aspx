<%@ Page Language="VB" MasterPageFile="~/mainMasterPageSS.master" AutoEventWireup="false"
    CodeFile="RptViewerSS.aspx.vb" Inherits="Reports_ASPX_Report_RptViewerSS" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <table width="100%" >
        <tr id ="trYearSelect" runat="server"> 
            <td align="center" width="100%" class="matters">
                
                <asp:Button ID="btnPrintReport" runat="server" Height="18px" Text="Print Report"
                    Visible="False" Width="95px" />&nbsp;
                &nbsp;
                </td>
        </tr>
        <tr id="TrCalendarYear" runat="server">
            <td align="left" class="matters" width="100%" style="height: 32px" valign="middle">
                Select Calendar Year&nbsp;&nbsp;
                <asp:DropDownList ID="ddlYear" runat="server" Width="60px" 
                    Height="20px">
                </asp:DropDownList>
                &nbsp;
                <asp:Button ID="btnGO" runat="server" CssClass="button" Text="View" Height="21px" 
                    Width="67px" /></td>
        </tr>
        <tr >
            <td align="left" valign="top" width="100%" style="vertical-align: top;">
                <div style="width:100%; height:800px; vertical-align:top">
                    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
                         EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False"
                        ReuseParameterValuesOnRefresh="True" PrintMode="ActiveX" Height="50px" ShowAllPageIds="True"
                        HasToggleGroupTreeButton="False" BestFitPage="true" Width="100%" />
                </div>
            </td>
        </tr>
    </table>
    &nbsp;
</asp:Content>
