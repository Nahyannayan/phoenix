<%@ Page Language="VB" AutoEventWireup="false" CodeFile="rptdiscribcencydetails.aspx.vb" Inherits="Students_Reports_ASPX_rptdiscribcencydetails" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <base target="_self" />
    <%--<link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
</head>
<body>

    <form id="form1" runat="server">
        <div>
            <center>
                <table>
                    <tr>
                        <td align="left" class="title-bg" colspan="3">Discrepancy Details</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="3"></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="3">
                            <asp:GridView ID="gvdiscribcency" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" AllowPaging="True" PageSize="15" EmptyDataText="No Records To  Display">
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <RowStyle CssClass="griditem" />
                                <HeaderStyle />
                                <Columns>
                                    <asp:BoundField DataField="STU_NO" HeaderText="Student No" />
                                    <asp:BoundField DataField="STU_NAME" HeaderText="Student Name" />
                                    <asp:BoundField DataField="ACY_DESCR" HeaderText="Academic Year" />
                                    <asp:BoundField DataField="GRD_ID" HeaderText="Grade" />
                                    <asp:BoundField DataField="STA_ORG_DATE" HeaderText="Date" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" ></td>
                        <td align="left" ></td>
                        <td align="left" ></td>
                    </tr>
                </table>
            </center>
            &nbsp;
        </div>
    </form>
</body>
</html>
