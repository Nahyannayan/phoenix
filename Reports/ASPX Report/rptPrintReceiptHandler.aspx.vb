Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class Reports_ASPX_Report_rptPrintReceipt
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Label1.Text = Session("sBsuid") & "---" & Session("STU_NAME")
        Select Case Request.QueryString("type")
            Case "REC"
                If Request.QueryString("id") <> "" Then
                    PrintReceipt(Request.QueryString("id"))
                End If
        End Select
    End Sub

    Protected Sub PrintReceipt(ByVal p_Receiptno As String)
        Dim str_Sql, strFilter As String
        Dim STR_RECEIPT As String = "FEES.VW_OSO_FEES_RECEIPT"
        
        strFilter = "  FCL_RECNO='" & p_Receiptno & "' AND FCL_BSU_ID='" & Session("sBsuid") & "' "
        str_Sql = "select * FROM " & STR_RECEIPT & " WHERE " + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
 
            params("STU_TYPE") = "Student #"

            params("UserName") = Session("STU_NAME")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = True

            Dim repSourceSubRep(2) As MyReportClass
            repSourceSubRep(0) = New MyReportClass
            repSourceSubRep(1) = New MyReportClass
            repSourceSubRep(2) = New MyReportClass
            Dim cmdSubColln As New SqlCommand

            ''SUBREPORT1
            cmdSubColln.CommandText = "select * FROM FEES.VW_OSO_FEES_FEECOLLECTED where " & strFilter
            cmdSubColln.Connection = New SqlConnection(str_conn)
            cmdSubColln.CommandType = CommandType.Text
            repSourceSubRep(0).Command = cmdSubColln

            ''''SUBREPORT2
            Dim cmdSubPayments As New SqlCommand
            cmdSubPayments.CommandText = "select * FROM   FEES.VW_OSO_FEES_PAYMENTS where " & strFilter
            cmdSubPayments.Connection = New SqlConnection(str_conn)
            cmdSubPayments.CommandType = CommandType.Text
            repSourceSubRep(1).Command = cmdSubPayments

            ''''SUBREPORT3
            Dim cmdSubSettle As New SqlCommand("[FEES].[F_GetCollectionSettlement]", New SqlConnection(str_conn))
            cmdSubSettle.Parameters.AddWithValue("@BSU_ID", Session("sBsuid"))
            cmdSubSettle.Parameters.AddWithValue("@FCL_RECNO", p_Receiptno)
            cmdSubSettle.CommandType = CommandType.StoredProcedure
            repSourceSubRep(2).Command = cmdSubSettle
            'repSource.HeaderBSUID = Session("sBsuid")
            repSource.SubReport = repSourceSubRep
            repSource.ResourceName = "../../fees/Reports/RPT/rptFeeCollectionReceipt.rpt"
            Session("ReportSource") = repSource
            ' Response.Redirect("../ASPX Report/rptviewermodal.aspx", True)
        End If
    End Sub
End Class
'Dim cmd As New SqlCommand
'cmd.CommandText = "SELECT * FROM vw_OSA_VOUCHER where VHH_TYPE = 'P'"
'cmd.CommandType = Data.CommandType.Text
'Page.Title = OASISConstants.Gemstitle
'' check whether Data Exits
'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
'Dim ds As New DataSet
'SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
'If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
'    'lblError.Text = "No Records with specified condition"
'Else
'    cmd.Connection = New SqlConnection(str_conn)
'    Dim repSource As New MyReportClass
'    Dim params As New Hashtable
'    params("Month") = "Jan"
'    params("decimal") = Session("BSU_ROUNDOFF")
'    repSource.Parameter = params
'    repSource.Command = cmd
'    repSource.ResourceName = "../RPT_Files/RPTDISBURSEMENT.rpt"
'    Session("ReportSource") = repSource
'    Response.Redirect("rptviewer.aspx", True)
'End If