<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTreasurytransfer.aspx.vb" Inherits="Reports_ASPX_Report_rptTreasurytransfer"  %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">
      
           function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 250px; ";
            sFeatures+="dialogHeight: 270px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx","", sFeatures);
            if(result != '' && result != undefined)
            {
                switch(txtControl)
                {
                  case 0:
                    document.getElementById('<%=txtfromDate.ClientID %>').value=result;
                    break;
                  case 1:  
                    document.getElementById('<%=txtToDate.ClientID %>').value=result;
                    break;
                }    
            }
            return false;
           }

         

         
    </script> 
    <table align="center" style="width: 754px" cellpadding="0" cellspacing="0">
        <tr align="left">
            <td><asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="bankPayment" />
            </td>
        </tr>
    </table>
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0" style="width: 754px">
        <tr class="subheader_img">
            <td colspan="6" style="height: 19px" >
                <div align="left"><asp:Label ID="lblRepCaption" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td class="matters" style="width: 65px; height: 16px; text-align: left">
                From Date</td>
            <td class="matters" style="width: 2px; height: 16px">
                &nbsp;
                :</td>
            <td class="matters" style="width: 180px; height: 16px; text-align: left">
                <asp:TextBox ID="txtfromDate" runat="server" Width="131px" CssClass="inputbox"></asp:TextBox>
                <img onclick = "getDate(550, 310, 0)" src="../../Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="valFromDate" runat="server" ControlToValidate="txtfromDate"
                    ErrorMessage="From Date Required" ValidationGroup="bankPayment">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                        EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                        ValidationGroup="bankPayment">*</asp:RegularExpressionValidator></td>
            <td class="matters" style="width: 65px; height: 16px; text-align: left">
                To Date</td>
            <td class="matters" style="width: 1px; height: 16px">
                :</td>
            <td class="matters" style="width: 170px; height: 16px; text-align: left">
                <asp:TextBox ID="txtToDate" runat="server" Width="100px" CssClass="inputbox"></asp:TextBox>
                <img onclick = "getDate(820, 310, 1)" src="../../Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="valToDate" runat="server" ErrorMessage="To Date Required" ControlToValidate="txtToDate" ValidationGroup="bankPayment">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="bankPayment">*</asp:RegularExpressionValidator>
                </td>
        </tr>
        <!-- Grade & Section -->
        <!-- Name -->
        <!-- Student Id -->
        <tr runat = "server" id = "trFilterBy">
            <td class="matters" style="width: 16%; height: 12px; text-align: left">
                Filtered By</td>
            <td class="matters" style="width: 16px; height: 12px">
                :</td>
            <td class="matters" colspan="4" style="height: 12px; text-align: left">
                <asp:DropDownList ID="drpFillter" runat="server"
                    DataTextField="COL_DESCR" DataValueField="COL_ID" SkinID="DropDownListNormal" Width="135px">
                    <asp:ListItem Value="ITF_DATE">Date</asp:ListItem>
                    <asp:ListItem Value="IRF_ID">Doc No</asp:ListItem>
                    <asp:ListItem Value="ITF_BSU_ID">Business Unit</asp:ListItem>
                </asp:DropDownList>
                &nbsp; &nbsp; &nbsp;
                <asp:RadioButton ID="radReceivable" runat="server" GroupName="chkUnit" Text="Receivable" Checked="True" />&nbsp; &nbsp;<asp:RadioButton ID="radPaid" runat="server" GroupName="chkUnit" Text="Payable" Checked="True" />&nbsp; &nbsp;<asp:RadioButton ID="radUnitTypeall" runat="server" GroupName="chkUnit" Text="All" Checked="True" /></td>
        </tr>
        <tr runat = "server" id = "trBankRow">
            <td class="matters" style="width: 16%; height: 13px; text-align: left">
                From
                Business Unit</td>
            <td class="matters" style="width: 16px; height: 13px">
                :</td>
            <td class="matters" colspan="4" style="height: 13px; text-align: left"><asp:DropDownList id="drpBSUnit" runat="server" Width="483px" AutoPostBack="True" OnSelectedIndexChanged="drpBSUnit_SelectedIndexChanged">
            </asp:DropDownList></td>
        </tr>
        <tr runat="server">
            <td class="matters" style="width: 16%; height: 13px; text-align: left">
                From Bank</td>
            <td class="matters" style="width: 16px; height: 13px">
            </td>
            <td class="matters" colspan="4" style="height: 13px; text-align: left">
                <asp:DropDownList id="drpBank" runat="server" Width="483px">
                </asp:DropDownList></td>
        </tr>
        <tr runat="server">
            <td class="matters" style="width: 16%; height: 13px; text-align: left">
                To Business Unit</td>
            <td class="matters" style="width: 16px; height: 13px">
                :</td>
            <td class="matters" colspan="4" style="height: 13px; text-align: left">
                <asp:DropDownList id="drpUnitTo" runat="server" Width="483px" AutoPostBack="True">
                </asp:DropDownList></td>
        </tr>
        <tr runat="server">
            <td class="matters" style="width: 16%; height: 13px; text-align: left">
                To
                Bank</td>
            <td class="matters" style="width: 16px; height: 13px">
                :</td>
            <td class="matters" colspan="4" style="height: 13px; text-align: left">
                <asp:DropDownList id="drpBankTo" runat="server" Width="483px" OnSelectedIndexChanged="drpBankTo_SelectedIndexChanged">
            </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="matters" style="width: 16%; height: 17px; text-align: left">
                Status</td>
            <td class="matters" style="width: 16px; height: 17px">
                :</td>
            <td class="matters" style="height: 17px; text-align: left;" colspan="4">
                &nbsp;&nbsp;
                            <asp:RadioButton ID="radStatusAll" runat="server" GroupName="chkStatus" Text="All" Checked="True" />
                                &nbsp; &nbsp; <asp:RadioButton ID="radStatusOpen" runat="server" GroupName="chkStatus" Text="Open" />
                                &nbsp; &nbsp; <asp:RadioButton ID="radStatusPosted" runat="server" GroupName="chkStatus" Text="Posted" />
                                &nbsp; &nbsp; <asp:RadioButton ID="radStatusDeleted" runat="server" GroupName="chkStatus" Text="Deleted" /></td>
        </tr>
        <tr>
            <td class="matters" style="height: 15px; text-align: right;" colspan="6">
                <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Generate Report" ValidationGroup="bankPayment" CssClass="button" />
                <asp:Button ID="Button1" runat="server" Text="Cancel" UseSubmitBehavior="False" CssClass="button" Width="72px" /></td>
        </tr>
        
    </table>
    <asp:HiddenField ID="h_BSUID" runat="server" />
    <asp:HiddenField ID="h_mode" runat="server" />
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp;&nbsp;
</asp:Content>

