<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptCheckListProcesses.aspx.vb" Inherits="rptCheckListProcesses" title="Untitled Page" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">            
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary
        ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
    <br />
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0"
       width = "60%" >
        <tr class ="subheader_img">
            <td align="left" colspan="12" style="height: 19px">
                <asp:Label ID="lblReportCaption" runat="server"></asp:Label></td>
        </tr>
        <tr runat="server" id ="trAsonDate">
            <td align="left" class="matters" valign="top">
                As On Date</td>
            <td class="matters" valign="top">
                :</td>
            <td align="left" class="matters" colspan="9" valign="top">
                <asp:TextBox ID="txtAsOnDate" runat="server" CssClass="inputbox" Width="122px">
                </asp:TextBox>&nbsp;<asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False"
                    ImageUrl="~/Images/calendar.gif" OnClientClick="return false" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAsOnDate"
                    ErrorMessage="To Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAsOnDate"
                        Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td align="left" class="matters" valign="top">
                Business Unit</td>
            <td class="matters" valign="top">
                :</td>
            <td align="left" class="matters" colspan="9" valign="top">
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
            </td>
        </tr>
        <tr runat ="server" id ="trProcesses">
            <td align="left" class="matters" valign="top">
                Processes</td>
            <td class="matters" valign="top">
                :</td>
            <td align="left" class="matters" colspan="9" valign="top">
                <asp:TreeView ID="tvCheckList" runat="server" onclick="client_OnTreeNodeChecked();"
                    ShowCheckBoxes="All">
                    <NodeStyle CssClass="treenode" />
                </asp:TreeView>
            </td>
        </tr>
        <tr runat="server">
            <td align="left" class="matters" valign="top">
                Process</td>
            <td class="matters" valign="top">
                :</td>
            <td align="left" class="matters" colspan="9" valign="top">
                <asp:DropDownList ID="ddlProcess" runat="server">
                    <asp:ListItem Value="D">Day End</asp:ListItem>
                    <asp:ListItem Value="M">Month End</asp:ListItem>
                    <asp:ListItem Value="Y">Year End</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="12" style=" text-align:right">
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
        </tr>
    </table>
    <ajaxtoolkit:calendarextender id="calAsOnDate1" runat="server" format="dd/MMM/yyyy"
        popupbuttonid="txtAsOnDate" targetcontrolid="txtAsOnDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxtoolkit:calendarextender id="calAsOnDate2" runat="server" format="dd/MMM/yyyy" targetcontrolid="txtAsOnDate">
    </ajaxToolkit:CalendarExtender>
</asp:Content>

