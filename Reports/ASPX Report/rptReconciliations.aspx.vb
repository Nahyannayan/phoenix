Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_rptRecociliations
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Page.Title = OASISConstants.Gemstitle
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            lblError.Text = ""
        End If



        If h_BSUID.Value = Nothing Or h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
            If h_Mode.Value = "Reconciliations" Then
                'h_BSUID.Value = Session("sBsuid")
                h_BSUID.Value = ddlBSUnit.SelectedValue
            Else
                h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
            End If
        End If
        'FillBSUName(h_BSUID.Value)

        If h_ACTIDs.Value <> Nothing Or h_ACTIDs.Value <> "" Or h_ACTIDs.Value <> "undefined" Then
            If h_Mode.Value = "PDC" Or h_Mode.Value = "Reconciliations" Then
                h_ACTIDs.Value = h_ACTIDs.Value.Split("____")(0)
            End If
            FillACTIDs(h_ACTIDs.Value)
        End If

        If Not IsPostBack Then
            UtilityObj.NoOpen(Me.Header)
            ddlBSUnit.DataSource = UtilityObj.GetBusinessUnits(Session("sUsr_name"))
            ddlBSUnit.DataTextField = "BSU_NAME"
            ddlBSUnit.DataValueField = "BSU_ID"
            ddlBSUnit.DataBind()
            ddlBSUnit.SelectedValue = Session("sbsuid")

            txtFromDate.Text = Format(CDate(UtilityObj.GetDiplayDate()), OASISConstants.DateFormat)
            txtDTFrom.Text = Format(DateAdd(DateInterval.Month, -1, CDate(UtilityObj.GetDiplayDate())), OASISConstants.DateFormat)
            txtDTTO.Text = Format(CDate(UtilityObj.GetDiplayDate()), OASISConstants.DateFormat)
            txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
            'txtFromDate.Attributes.Add("ReadOnly", "Readonly")
            UsrBSUnits1.MenuCode = ViewState("MainMnu_code").ToString
            trFilterDt.Visible = False
            trFilter.Visible = False
            Select Case ViewState("MainMnu_code").ToString
                Case "A750020" '
                    lblrptCaption.Text = "Reconciliations Statement"
                    lblAccountType.Text = "Account"
                    h_Mode.Value = "Reconciliations"
                    trcheckboxes.Visible = False
                    trSingleSelect.Visible = True
                    trMultiSelect.Visible = False
                Case "A750025" '
                    lblrptCaption.Text = "PDC Report"
                    lblAccountType.Text = "Bank"
                    h_Mode.Value = "PDC"
                    trMultiSelect.Visible = True
                    trSingleSelect.Visible = False
                Case "A550010" '
                    lblrptCaption.Text = "Creditors Statement of account with Ageing"
                    lblAccountType.Text = "Account"
                    h_Mode.Value = "party_aging"
                    chkConsolidation.Visible = False
                    trMultiSelect.Visible = True
                    trSingleSelect.Visible = False
                    h_ACTTYPE.Value = "s"
                Case "A250070" 'Prepayment Schedule
                    lblrptCaption.Text = "Prepayment Allocation"
                    h_Mode.Value = "prepayment"
                    Trbank.Visible = False
                    trMultiSelect.Visible = True
                    trSingleSelect.Visible = False
                Case "A550017"
                    lblrptCaption.Text = "Debtors Statement of account with Ageing"
                    lblAccountType.Text = "Account"
                    h_Mode.Value = "party_aging"
                    chkConsolidation.Visible = False
                    trMultiSelect.Visible = True
                    trSingleSelect.Visible = False
                    h_ACTTYPE.Value = "C"
                Case "A350065"
                    lblrptCaption.Text = "Cheque Register Report"
                    lblAccountType.Text = "Bank"
                    h_Mode.Value = "PDC"
                    trMultiSelect.Visible = True
                    trSingleSelect.Visible = False
                    Trbank.Visible = False
                    trcheckboxes.Visible = False
                    trasOnDate.Visible = False
                    trFilter.Visible = True
                    trFilterDt.Visible = True
                    txtDTTO.Visible = False
                    imgDTTO.Visible = False
            End Select
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        txtFromDate.Text = Format("dd/MMM/yyyy", txtFromDate.Text)
        If trMultiSelect.Visible = True Then
            h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        Else
            h_BSUID.Value = ddlBSUnit.SelectedValue
        End If
        Select Case ViewState("MainMnu_code").ToString
            Case "A750020" 'Reconciliations
                GenerateReconciliation()
            Case "A750025" 'PDC
                GeneratePDCReport()
            Case "A550010", "A550017" 'Partys Statement account with Ageing
                GeneratePartyStatementAccountWithAging()
            Case "A250070"
                GeneratePrepaymentSchedule()
            Case "A350065"
                rptChequeRgister(h_BSUID.Value)
        End Select
    End Sub

    Private Sub rptChequeRgister(ByVal strXMLBSUNames As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("rptChequeRgister", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.VarChar, 2000)
        sqlpJHD_BSU_IDs.Value = UtilityObj.GetBSUnitWithSeperator(UsrBSUnits1.GetSelectedNode, "|")
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtDTFrom.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpFilterType As New SqlParameter("@Typ", SqlDbType.TinyInt)
        sqlpFilterType.Value = ddFilterType.SelectedValue
        cmd.Parameters.Add(sqlpFilterType)

        Dim sqlpTODOCDT As New SqlParameter("@AsonDate", SqlDbType.DateTime)
        If ddFilterType.SelectedValue = 0 Then
            sqlpTODOCDT.Value = txtDTFrom.Text
        Else
            sqlpTODOCDT.Value = txtDTTO.Text
        End If

        cmd.Parameters.Add(sqlpTODOCDT)

        'adpt.SelectCommand = cmd
        'objConn.Close()
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("Date") = txtFromDate.Text
            If ddFilterType.SelectedValue = 0 Then
                params("DateCaption") = " As On " & Format(CDate(txtDTFrom.Text), "dd/MMM/yyyy")
            Else
                params("DateCaption") = " Date Range from " & Format(CDate(txtDTFrom.Text), "dd/MMM/yyyy") & " to " & Format(CDate(txtDTTO.Text), "dd/MMM/yyyy")
            End If
            'params("ReportHeading") = "CASH FLOW REPORT" 
            repSource.Parameter = params
            repSource.Command = cmd


            repSource.ResourceName = "../RPT_Files/rptChequeRgister.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Private Sub GeneratePrepaymentSchedule()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTPREPAYMENTALLOCATION", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        sqlpBSU_ID.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpDOCDT As New SqlParameter("@DOCDT", SqlDbType.DateTime)
        sqlpDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpDOCDT)

        Dim sqlpbGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpbGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpbGroupCurrency)

        Dim sqlpbConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        sqlpbConsolidation.Value = chkConsolidation.Checked
        cmd.Parameters.Add(sqlpbConsolidation)

        'adpt.SelectCommand = cmd
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then

        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("DocDate") = txtFromDate.Text
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptPrepaymentSchedule.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Private Sub GeneratePartyStatementAccountWithAging()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand

        Select Case ViewState("MainMnu_code").ToString
            Case "A550010"
                cmd = New SqlCommand("RPTPARTYsStatementWithAging")
            Case "A550017"
                cmd = New SqlCommand("RPTPARTYsDebitorStatementWithAging")
        End Select

        'Dim cmd As New SqlCommand("RPTPARTYsStatementWithAging", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@TRN_BSU_IDs", SqlDbType.Xml)
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        sqlpJHD_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        sqlpACT_IDs.Value = GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
        cmd.Parameters.Add(sqlpACT_IDs)

        Dim sqlpDOCDT As New SqlParameter("@DOCDT", SqlDbType.DateTime)
        sqlpDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpDOCDT)


        Dim sqlpTRN_FYEAR As New SqlParameter("@TRN_FYEAR", SqlDbType.Int)
        sqlpTRN_FYEAR.Value = Session("F_YEAR")
        cmd.Parameters.Add(sqlpTRN_FYEAR)

        Dim sqlpbGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.VarChar, 20)
        sqlpbGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpbGroupCurrency)

        'adpt.SelectCommand = cmd
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then

        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("UserName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtFromDate.Text
            params("partyAging") = True
            Select Case ViewState("MainMnu_code").ToString
                Case "A550010"
                    params("ReportCaption") = "Creditors Statement of Account with Ageing"
                Case "A550017"
                    params("ReportCaption") = "Debtors Statement of Account with Ageing"
            End Select
            'params("ReportCaption") = "Creditors Statement of Account with Ageing"
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.SubReport = GetSubReport()
            repSource.ResourceName = "../RPT_Files/rptpartysLedger.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub


    Private Function GetSubReport() As MyReportClass()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        Dim cmd As New SqlCommand("Aging", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@TRN_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        sqlpACT_IDs.Value = GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
        cmd.Parameters.Add(sqlpACT_IDs)

        Dim sqlpDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpDOCDT)

        Dim sqlpbGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.VarChar, 20)
        sqlpbGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpbGroupCurrency)

        Dim subRepSource(1) As MyReportClass
        subRepSource(0) = New MyReportClass
        subRepSource(0).Command = cmd

        Return subRepSource
    End Function

    Private Sub GeneratePDCReport()

        Response.Redirect("../../underconstruction.aspx")
        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        'Dim objConn As New SqlConnection(str_conn)
        'Dim adpt As New SqlDataAdapter
        'Dim ds As New DataSet

        'Dim cmd As New SqlCommand("RPTRECONCILIATIONSTMT", objConn)
        'cmd.CommandType = CommandType.StoredProcedure

        'Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        'sqlpBSU_ID.Value = h_BSUID.Value
        'cmd.Parameters.Add(sqlpBSU_ID)

        'Dim sqlpDOCDT As New SqlParameter("@DOCDT", SqlDbType.DateTime)
        'sqlpDOCDT.Value = txtFromDate.Text
        'cmd.Parameters.Add(sqlpDOCDT)

        'Dim sqlpACT_ID As New SqlParameter("@ACT_ID", SqlDbType.VarChar, 20)
        'sqlpACT_ID.Value = h_Mode.Value
        'cmd.Parameters.Add(sqlpACT_ID)

        'adpt.SelectCommand = cmd
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        '    cmd.Connection = New SqlConnection(str_conn)
        '    Dim repSource As New MyReportClass
        '    Dim params As New Hashtable
        '    params("userName") = Session("sUsr_name")
        '    params("Date") = txtFromDate.Text
        '    repSource.Parameter = params
        '    repSource.Command = cmd
        '    repSource.ResourceName = "../RPT_Files/RPTRECONCILIATIONS.rpt"
        '    Session("ReportSource") = repSource
        '    'Context.Items.Add("ReportSource", repSource)
        '    objConn.Close()
        '    response.redirect("rptviewer.aspx", True)
        'Else
        '    lblError.Text = "No Records with specified condition"
        'End If
        'objConn.Close()
    End Sub

    Private Sub GenerateReconciliation()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTRECONCILIATIONSTMT", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = h_BSUID.Value
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpDOCDT As New SqlParameter("@DOCDT", SqlDbType.DateTime)
        sqlpDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpDOCDT)

        Dim sqlpACT_ID As New SqlParameter("@ACT_ID", SqlDbType.VarChar, 20)
        sqlpACT_ID.Value = h_ACTIDs.Value
        cmd.Parameters.Add(sqlpACT_ID)

        'adpt.SelectCommand = cmd
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then

        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("Date") = txtFromDate.Text
            params("decimal") = Session("BSU_ROUNDOFF")
            params("bsuname") = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "select bsu_name from oasis..businessunit_m where bsu_id='" & Session("sbsuid") & "'")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/RPTRECONCILIATIONS.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptViewer.aspx?isExport=true", True)
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    'Private Sub FillBSUName(ByVal bsuValue As String)
    '    If h_Mode.Value = "PDC" Or h_Mode.Value = "party_aging" Then
    '        FillBSUNames(h_BSUID.Value)
    '    Else
    '        Dim str As String() = bsuValue.Split("___")
    '        txtBSUName.Text = GetBSUName(str(0))
    '        h_BSUID.Value = str(0)
    '    End If
    'End Sub

    Private Function GetBSUName(ByVal BSUName As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
    End Function

    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        'grdBSU.DataSource = ds
        'grdBSU.DataBind()
        Return True
    End Function

    Private Function FillACTIDs(ByVal ACTIDs As String) As Boolean
        Dim IDs As String() = ACTIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim str_Sql As String
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        grdACTDetails.DataSource = ds
        grdACTDetails.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        Else
            Return True
        End If
        'dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        'txtBankNames.Text = ""
        'Dim bval As Boolean = dr.Read
        'While (bval)
        '    txtBankNames.Text += dr(0).ToString()
        '    bval = dr.Read()
        '    If bval Then
        '        txtBankNames.Text += "||"
        '    End If
        'End While
        'txtbankCodes.Text = ACTIDs
    End Function

    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.ACTName
                elements(0) = "ACT_DETAILS"
                elements(1) = "ACT_DETAIL"
                elements(2) = "ACT_ID"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                XMLEBSUID = xmlDoc.CreateElement(elements(2))
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub lblAddActID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddActID.Click
        If h_Mode.Value = "PDC" Or h_Mode.Value = "party_aging" Then
            h_ACTIDs.Value += "||" + txtBankNames.Text.Replace(",", "||")
        ElseIf FillACTIDs(txtBankNames.Text) Then
            h_ACTIDs.Value = txtBankNames.Text
        End If
        FillACTIDs(h_ACTIDs.Value)
    End Sub

    'Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
    '    If h_Mode.Value = "PDC" Or h_Mode.Value = "party_aging" Or h_Mode.Value = "prepayment" Then
    '        h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
    '    ElseIf FillBSUNames(txtBSUName.Text) Then
    '        h_BSUID.Value = txtBSUName.Text
    '    End If
    '    FillBSUNames(h_BSUID.Value)
    'End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = Session("sBSUID")
            End If
            ' grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

    'Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
    '    grdBSU.PageIndex = e.NewPageIndex
    '    FillBSUNames(h_BSUID.Value)
    'End Sub

    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(h_ACTIDs.Value)
        End If
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub


    Protected Sub ddFilterType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddFilterType.SelectedItem IsNot Nothing Then
            If ddFilterType.SelectedValue = 0 Then
                txtDTTO.Visible = False
                imgDTTO.Visible = False
            Else
                txtDTTO.Visible = True
                imgDTTO.Visible = True
                txtDTTO.Text = Format(CDate(UtilityObj.GetDiplayDate()), OASISConstants.DateFormat)
            End If
            lblFilterName.Text = ddFilterType.SelectedItem.Text
        End If

    End Sub
End Class
