Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports System.Data.SqlTypes
Imports UtilityObj
Partial Class Reports_ASPX_Report_rptInterUnitAdj
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnitsRow.MenuCode = MainMnu_code
            UsrBSUnitsCol.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            UsrBSUnitsRow.ExpandFirstNodeOnStart = True
            UsrBSUnitsCol.ExpandFirstNodeOnStart = True

            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim sqlStr As String
                sqlStr = "SELECT FYR_FROMDT,FYR_TODT FROM  FINANCIALYEAR_S where FYR_ID='" & Session("F_YEAR") & "'"
                Dim mtable As DataTable
                mtable = Mainclass.getDataTable(sqlStr, ConnectionManger.GetOASISConnectionString)
                If mtable.Rows.Count > 0 Then
                    txtFromDate.Text = CDate(mtable.Rows(0).Item("FYR_FROMDT")).ToString("dd/MMM/yyyy")
                    txtToDate.Text = CDate(mtable.Rows(0).Item("FYR_TODT")).ToString("dd/MMM/yyyy")
                    txtAsOnDate.Text = CDate(mtable.Rows(0).Item("FYR_TODT")).ToString("dd/MMM/yyyy")
                End If
                txtToDate.Attributes.Add("onBlur", "checkdate(this)")
                txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
                Select Case MainMnu_code
                    Case "A753081"
                        trAsOnDate.Visible = False
                        lblrptCaption.Text = "Inter Unit Adjustment"
                    Case "A753089"
                        trAsOnDate.Visible = True
                        trDateRange.Visible = False
                        trType.Visible = False
                        trGroupBy.Visible = False
                        lblrptCaption.Text = "Inter Unit Balance After Knock Off"
                End Select
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            Select Case MainMnu_code
                Case "A753081"
                    txtFromDate.Text = Format("dd/MMM/yyyy", txtFromDate.Text)
                    txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
                    RPTGetInterUnitAdjustment()
                Case "A753089"
                    txtAsOnDate.Text = Format("dd/MMM/yyyy", txtAsOnDate.Text)
                    RPTInterUnitMonthEnd()
            End Select
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub RPTInterUnitMonthEnd()
        Dim strSelectedNodes1 As String = UsrBSUnitsRow.GetSelectedNode("|")
        Dim strSelectedNodes2 As String = UsrBSUnitsCol.GetSelectedNode("|")
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes1, str_bsu_Segment, str_bsu_shortname, str_bsu_CURRENCY)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim cmd As New SqlCommand
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = objConn
        cmd.CommandText = "RPTInterUnitMonthEnd"
        Dim sqlParam(2) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", strSelectedNodes1, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlParam(0))
        sqlParam(1) = Mainclass.CreateSqlParameter("@OPP_BSU_ID", strSelectedNodes2, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlParam(1))
        sqlParam(2) = Mainclass.CreateSqlParameter("@ASONDATE", txtAsOnDate.Text, SqlDbType.DateTime)
        cmd.Parameters.Add(sqlParam(2))
        cmd.Connection = objConn

        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("ReportCaption") = "Inter Unit Balance After Knock Off"
        params("AsOnDate") = Format("dd/MMM/yyyy", txtAsOnDate.Text)
        params("UserName") = Session("sUsr_name")
        repSource.ResourceName = "../RPT_Files/rptInterUnitMonthEnd.rpt"
        repSource.Parameter = params
        repSource.Command = cmd

        repSource.DisplayGroupTree = False
        Session("ReportSource") = repSource
        objConn.Close()
        Response.Redirect("rptviewer.aspx", True)
    End Sub

    Private Sub RPTGetInterUnitAdjustment()
        Dim strSelectedNodes1 As String = UsrBSUnitsRow.GetSelectedNode("|")
        Dim strSelectedNodes2 As String = UsrBSUnitsCol.GetSelectedNode("|")
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes1, str_bsu_Segment, str_bsu_shortname, str_bsu_CURRENCY)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim cmd As New SqlCommand
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = objConn
        cmd.CommandText = "RPTInterUnitAdjustment"
        Dim sqlParam(4) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", strSelectedNodes1, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlParam(0))
        sqlParam(1) = Mainclass.CreateSqlParameter("@OPP_BSU_ID", strSelectedNodes2, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlParam(1))
        sqlParam(2) = Mainclass.CreateSqlParameter("@DTFROM", txtFromDate.Text, SqlDbType.DateTime)
        cmd.Parameters.Add(sqlParam(2))
        sqlParam(3) = Mainclass.CreateSqlParameter("@DTTO", txtToDate.Text, SqlDbType.DateTime)
        cmd.Parameters.Add(sqlParam(3))
        sqlParam(4) = Mainclass.CreateSqlParameter("@TYPE", ddType.SelectedValue, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlParam(4))
        cmd.Connection = objConn

        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("ReportCaption") = "Inter Unit Adjustment"
        params("FromDate") = Format("dd/MMM/yyyy", txtFromDate.Text)
        params("ToDate") = Format("dd/MMM/yyyy", txtToDate.Text)
        params("UserName") = Session("sUsr_name")
        Dim formulas As New Hashtable
        Dim c As New SqlConnection
        Select Case ddGroupby.SelectedValue
            Case "ACT"
                formulas("GroupByField") = "{VW_RPT_InterUnitAdjustment.ACT_NAME}"
                formulas("GroupByID") = "{VW_RPT_InterUnitAdjustment.JAL_ACT_ID}"
                repSource.ResourceName = "../RPT_Files/rptInterUnitAdj_Account.rpt" 'No subtotal needed for summary by account
            Case "RSS"
                formulas("GroupByField") = "{VW_RPT_InterUnitAdjustment.RSS_DESCR}"
                formulas("GroupByID") = "{VW_RPT_InterUnitAdjustment.JAL_RSS_ID}"
                repSource.ResourceName = "../RPT_Files/rptInterUnitAdj.rpt"
            Case Else
                formulas("GroupByField") = "{VW_RPT_InterUnitAdjustment.ACT_NAME}"
                formulas("GroupByID") = "{VW_RPT_InterUnitAdjustment.JAL_ACT_ID}"
                repSource.ResourceName = "../RPT_Files/rptInterUnitAdj.rpt"
        End Select
        repSource.Parameter = params
        repSource.Formulas = formulas
        repSource.Command = cmd

        repSource.DisplayGroupTree = False
        Session("ReportSource") = repSource
        objConn.Close()
        Response.Redirect("rptviewer.aspx", True)
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub
    Protected Sub btnSetSelected_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSetSelected.Click
        Try
            Dim tvBusinessunitCol, tvBusinessunitRow As New TreeView
            tvBusinessunitCol = UsrBSUnitsCol.FindControl("tvBusinessunit")
            tvBusinessunitRow = UsrBSUnitsRow.FindControl("tvBusinessunit")
            If tvBusinessunitCol Is Nothing Or tvBusinessunitCol Is Nothing Then Exit Sub
            SetSelectedNodes(tvBusinessunitCol, GetSelectedNode(tvBusinessunitRow, "|"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Function GetSelectedNode(ByVal tvBusinessunit As TreeView, Optional ByVal seperator As String = "||") As String
        Try
            Dim strBSUnits As New StringBuilder
            For Each node As TreeNode In tvBusinessunit.CheckedNodes
                If node.Value.Length > 0 Then
                    strBSUnits.Append(node.Value)
                    strBSUnits.Append(seperator)
                End If
            Next
            Return strBSUnits.ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function
    Public Function SetSelectedNodes(ByVal tvBusinessunit As TreeView, ByVal strBSUid As String) As Boolean
        Try
            UncheckAllNodes(tvBusinessunit.Nodes)
            Dim IDs As String() = strBSUid.Split("||")
            Dim condition As String = String.Empty
            For i As Integer = 0 To IDs.Length - 1
                CheckSelectedNode(IDs(i), tvBusinessunit.Nodes)
            Next
            Return True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function
    Private Sub UncheckAllNodes(ByVal vNodes As TreeNodeCollection)
        Try
            Dim ienum As IEnumerator = vNodes.GetEnumerator()
            Dim trNode As TreeNode
            While (ienum.MoveNext())
                trNode = ienum.Current
                trNode.Checked = False
                If trNode.ChildNodes.Count > 0 Then
                    UncheckAllNodes(trNode.ChildNodes)
                End If
            End While
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub CheckSelectedNode(ByVal vBSU_ID As String, ByVal vNodes As TreeNodeCollection)
        Try
            Dim ienum As IEnumerator = vNodes.GetEnumerator()
            Dim trNode As TreeNode
            While (ienum.MoveNext())
                trNode = ienum.Current
                If trNode.ChildNodes.Count > 0 Then
                    If trNode.Value = vBSU_ID Then
                        trNode.Checked = True
                    End If
                    CheckSelectedNode(vBSU_ID, trNode.ChildNodes)
                ElseIf trNode.Value = vBSU_ID Then
                    trNode.Checked = True
                End If
            End While
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

End Class
