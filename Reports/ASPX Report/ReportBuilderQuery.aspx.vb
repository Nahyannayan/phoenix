﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports Microsoft.VisualBasic
Partial Class Reports_ASPX_Report_ReportBuilderQuery
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                gvGroup.Attributes.Add("bordercolor", "#1b80b6")
                bindData()
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If

    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        bindData()
    End Sub

    Protected Sub gvGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGroup.RowDataBound
        
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", "return getValue ('" & e.Row.Cells(0).Text & "','" & e.Row.Cells(1).Text.Replace("'", " ") & "')")
            'e.Row.Attributes.Add("onmouseover", "return MouseOver('" & e.Row.ClientID & "')")
            'e.Row.Attributes.Add("onmouseleave", "return MouseLeave('" & e.Row.ClientID & "')")
        End If
        If e.Row.Cells.Count > 1 Then
            e.Row.Cells(0).Style("display") = "none"

        End If
    End Sub
    Sub bindData()
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FQR_Name", txtFind.Text)
        Dim _table As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "RB_FillQUERY", pParms)
        gvGroup.DataSource = _table.Tables(0)
        gvGroup.DataBind()
    End Sub
    Function CheckQuery(ByVal query As String) As String
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FQR_Query", query)
        pParms(1) = New SqlClient.SqlParameter("@RETMESAGE", SqlDbType.VarChar, 1000)
        pParms(1).Direction = ParameterDirection.Output
        Dim retval As Integer = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "RB_QueryValidate", pParms)
        Return pParms(1).Value
    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim retval As Integer
        Try
            If txtQueryName.Text = "" Then
                Throw New Exception("Invalid Query name..!")
            End If

            If txtQuery.Text = "" Then
                Throw New Exception("Invalid Query...!")
            End If

            If txtQuery.Text.ToUpper().IndexOf("SELECT") < 0 Then
                Throw New Exception("Invalid Statement...Kyword SELECT Missing..!")
            End If

            lblError.Text = CheckQuery(txtQuery.Text)
            If lblError.Text <> "" Then
                Throw New Exception(lblError.Text)
            End If
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FQR_Name", txtQueryName.Text)
            pParms(1) = New SqlClient.SqlParameter("@FQR_Query", txtQuery.Text.ToUpper())
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            pParms(3) = New SqlClient.SqlParameter("@RETMESAGE", SqlDbType.VarChar, 1000)
            pParms(3).Direction = ParameterDirection.Output
            retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "RB_FillQUERYInsert", pParms)

            If pParms(2).Value <> 0 Then
                stTrans.Rollback()
                objConn.Close()
                lblError.Text = pParms(3).Value.ToString()
                Exit Sub
            Else
                stTrans.Commit()
                objConn.Close()
                lblError.Text = pParms(3).Value.ToString()
                doClear()
                bindData()
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
            stTrans.Rollback()
            objConn.Close()
            Exit Sub
        End Try
        
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        bindData()
    End Sub
    Sub doClear()
        txtQuery.Text = ""
        txtQueryName.Text = ""
        txtFind.Text = ""
    End Sub
End Class
