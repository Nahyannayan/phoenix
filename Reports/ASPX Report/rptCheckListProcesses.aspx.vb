Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptCheckListProcesses
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTAsOnDate") = txtAsOnDate.Text
        Select Case MainMnu_code
            Case OASISConstants.MNU_ACC_REP_PROCESSCHECKLIST
                GenerateCheckListReport()
        End Select
    End Sub

    Private Sub GenerateCheckListReport()
        Dim str_conn As String = ConnectionManger.GetOASISFINConnectionString
        Dim cmd As New SqlCommand("[RPTCheckListProcessReport]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.Xml)
        sqlpBSU_IDs.Value = UtilityObj.GenerateXML(UsrBSUnits1.GetSelectedNode(), XMLType.BSUName)
        cmd.Parameters.Add(sqlpBSU_IDs)

        'Dim sqlpCHKLST_IDs As New SqlParameter("@CHKLST_IDs", SqlDbType.Xml)
        'sqlpCHKLST_IDs.Value = UtilityObj.GenerateXML(GetSelectedNode(), XMLType.CheckList)
        'cmd.Parameters.Add(sqlpCHKLST_IDs)

        Dim sqlpPROCESS As New SqlParameter("@PROCESS", SqlDbType.VarChar, 2)
        sqlpPROCESS.Value = ddlProcess.SelectedValue
        cmd.Parameters.Add(sqlpPROCESS)

        Dim sqlpAsOnDT As New SqlParameter("@AsOnDT", SqlDbType.DateTime)
        sqlpAsOnDT.Value = CDate(txtAsOnDate.Text)
        cmd.Parameters.Add(sqlpAsOnDT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("ASONDATE") = txtAsOnDate.Text

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        params("RPT_CAPTION") = " PROCESS CHECK LIST REPORT"
        repSource.ResourceName = "../RPT_Files/rptCheckList.rpt"
        Session("ReportSource") = repSource
        Response.Redirect("Rptviewer.aspx", True)
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If

            Select Case MainMnu_code
                Case OASISConstants.MNU_ACC_REP_PROCESSCHECKLIST
                    lblReportCaption.Text = "Check List Report"
                    trAsonDate.Visible = True
                    trProcesses.Visible = False
                    PopulateNodes()
            End Select

            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If bNoData Then
                txtAsOnDate.Text = Session("RPTAsOnDate")
            Else
                txtAsOnDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            End If
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            lblError.Text = ""
        End If
    End Sub

    Private Sub PopulateNodes()
        Dim str_query As String = " SELECT CHK_ID,CHK_DESCRIPTION FROM CHECKLIST_M "
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_query)
        While (dr.Read())
            Dim tn As New TreeNode()
            tn.Text = dr("CHK_DESCRIPTION").ToString()
            tn.Value = dr("CHK_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            tvCheckList.Nodes.Add(tn)
        End While
    End Sub

    Public Function GetSelectedNode()
        Dim strBSUnits As New StringBuilder
        For Each node As TreeNode In tvCheckList.CheckedNodes
            strBSUnits.Append(node.Value)
            strBSUnits.Append("||")
        Next
        Return strBSUnits.ToString()
    End Function


End Class
