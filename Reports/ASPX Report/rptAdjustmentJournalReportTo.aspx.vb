﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Reports_ASPX_Report_rptAdjustmentJournalReportTo
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            usrBSUnitsTo1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            UtilityObj.NoOpen(Me.Header)
            Select Case MainMnu_code 
                Case "A753083" 'Adjustment Journal PL - Summary"
                    lblrptCaption.Text = "Adjustment Journal - PL - Summary Opposite"
                    lblBankCash.Text = "Account"
                    'chkConsolidation.Visible = True
            End Select
            Session("selACTID") = h_ACTIDs.Value
            If h_ACTIDs.Value <> Nothing And h_ACTIDs.Value <> "" And h_ACTIDs.Value <> "undefined" Then
                FillACTIDs(h_ACTIDs.Value)
            End If
            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'ddlBSUnit.DataSource = UtilityObj.GetBusinessUnits(Session("sUsr_name"))
                'ddlBSUnit.DataTextField = "BSU_NAME"
                'ddlBSUnit.DataValueField = "BSU_ID"
                'ddlBSUnit.DataBind()

                'Dim ITEM As New ListItem("", "")
                'ddlBSUnit.Items.Insert(0, ITEM)

                txtToDate.Text = UtilityObj.GetDiplayDate()
                txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))

                txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
                txtToDate.Attributes.Add("onBlur", "checkdate(this)")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Try
            Dim IDs As String() = ACTIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_Sql)
            grdACTDetails.DataSource = ds
            grdACTDetails.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            txtFromDate.Text = Format("dd/MMM/yyyy", txtFromDate.Text)
            txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
            h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
            h_BSUIDTO.Value = usrBSUnitsTo1.GetSelectedNode()
            Session("Fdate") = txtFromDate.Text
            Session("Tdate") = txtToDate.Text
            Session("selBSUID") = h_BSUID.Value
            Session("selACTID") = h_ACTIDs.Value
            Select Case MainMnu_code 
                Case "A753083" 'Adjustment Journal - Pl - opposite
                    RPTAdjustmentJOURNALLIST_Opposite()
            End Select
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub  

    Private Sub RPTAdjustmentJOURNALLIST_Opposite()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand

        cmd = New SqlCommand("[RPTAdjustmentJOURNALLIST_Opposite]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = h_BSUID.Value.Replace("||", "|")
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.VarChar)
        sqlpACT_IDs.Value = h_ACTIDs.Value.Replace("||", "|")
        cmd.Parameters.Add(sqlpACT_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpOppBSU_ID As New SqlParameter("@OppBSU_IDs", SqlDbType.VarChar)
        sqlpOppBSU_ID.Value = h_BSUIDTO.Value.Replace("||", "|")
        cmd.Parameters.Add(sqlpOppBSU_ID) 

        objConn.Close()
        objConn.Open()

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text


        repSource.ResourceName = "../RPT_Files/rptAdjustmentJOURNALLIST_PL_Consolidation.rpt"
        params("RPT_CAPTION") = "Adjustment Journal - Summary"
        Dim Misparams As New MisParameters
        Misparams.BSU_IDs = sqlpJHD_BSU_IDs.Value
        Misparams.BSUShortnames = ""
        Misparams.BSUSegments = ""
        Misparams.ToDate = txtToDate.Text
        Misparams.FromDate = txtFromDate.Text
        Session("Misparams") = Misparams
    

        repSource.Parameter = params
        repSource.Command = cmd

        Session("ReportSource") = repSource
        'Context.Items.Add("ReportSource", repSource)
        objConn.Close()
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("rptviewer.aspx?isExport=true", True)
        Else
            Response.Redirect("rptviewer.aspx", True)
        End If
        objConn.Close()
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "|")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(h_ACTIDs.Value)
        End If
    End Sub

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdACTDetails.PageIndexChanging
        grdACTDetails.PageIndex = e.NewPageIndex
    End Sub

    Protected Sub lblAddActID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddActID.Click
        h_ACTIDs.Value += "||" + txtBankNames.Text.Replace(",", "||")
        h_ACTIDs.Value = txtBankNames.Text
        FillACTIDs(h_ACTIDs.Value)
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
            txtFromDate.Text = Session("FDate")
            txtToDate.Text = Session("TDate")
            FillACTIDs(Session("selACTID"))
            'UsrBSUnits1.SetSelectedNodes(Session("sBsu_id"))
        End If
    End Sub

End Class