<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptAccountMasterList.aspx.vb" Inherits="Reports_ASPX_Report_rptAccountMasterList" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Account Master List"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">

                    <tr id="trGroup" runat="server">
                        <td align="left" width="20%"><span class="field-label">Group</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="cmbGroup" runat="server" Width="318px" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="trSubGroup" runat="server">
                        <td align="left" width="20%"><span class="field-label">Sub Group</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="cmbSubGroup" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="trCntrlAcc" runat="server">
                        <td align="left" width="20%"><span class="field-label">Control Account</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="cmbCtrlAccount" runat="server" Width="318px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="trType" runat="server">
                        <td align="left" width="20%"><span class="field-label">Type</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="cmbType" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />

                        </td>
                    </tr>
                </table>
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>

