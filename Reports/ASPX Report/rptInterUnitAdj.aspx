<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptInterUnitAdj.aspx.vb" Inherits="Reports_ASPX_Report_rptInterUnitAdj"
    Title="Untitled Page" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>

    <table align="center" style="width: 80%;">
        <tr align="left">
            <td>
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="dayBook" />
            </td>
        </tr>
    </table>
    <table align="center" bordercolor="#1B80B6" border="1" cellpadding="5" cellspacing="0"
        style="width: 80%; border-collapse: collapse;">
        <tr class="subheader_img">
            <td align="left" colspan="2" style="height: 19px" valign="middle">
                <asp:Label ID="lblrptCaption" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" valign="middle" class="matters" style="width: 88px">
                Business Unit
            </td>
            <td align="left" valign="middle" class="matters" colspan="1">
                &nbsp;<table style="width: 100%;">
                    <tr>
                        <td style="width: 282px">
                            <uc1:usrBSUnits ID="UsrBSUnitsRow" runat="server" />
                        </td>
                        <td align="center" width="5%">
                            <asp:Button ID="btnSetSelected" runat="server" CssClass="button" Text="&gt;" />
                        </td>
                        <td>
                            <uc1:usrBSUnits ID="UsrBSUnitsCol" runat="server" />
                        </td>
                    </tr>
                </table>
                &nbsp;
            </td>
        </tr>
        <tr id="trDateRange" runat="server">
            <td align="left" class="matters" style="width: 88px">
                Date Range
            </td>
            <td align="left" class="matters" colspan="1" style="height: 1px; text-align: left;">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 180px">
                            <asp:TextBox ID="txtFromDate" runat="server" Width="122px" Height="22px"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="txtFromDate_CalendarExtender" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate"
                                BehaviorID="txtFromDate_CalendarExtender">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="txtToDate_CalendarExtender" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" TargetControlID="txtToDate" PopupButtonID="txtToDate" BehaviorID="txtToDate_CalendarExtender">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                Height="16px" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFromDate"
                                ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revFromDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="dayBook">*</asp:RegularExpressionValidator>
                        </td>
                        <td style="width: 39px">
                            To
                        </td>
                        <td>
                            <asp:TextBox ID="txtToDate" runat="server" Width="122px"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" TargetControlID="txtToDate" PopupButtonID="txtToDate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="dayBook">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trAsOnDate" runat="server">
            <td align="left" class="matters" style="width: 88px">
                As On Date
            </td>
            <td align="left" class="matters" colspan="1" style="height: 1px; text-align: left;">
                <asp:TextBox ID="txtAsOnDate" runat="server" Width="122px" Height="22px"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="txtAsOnDate_CalendarExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgAsOnDate" TargetControlID="txtAsOnDate"
                    BehaviorID="txtAsOnDate_CalendarExtender">
                </ajaxToolkit:CalendarExtender>
                <asp:ImageButton ID="imgAsOnDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    Height="16px" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAsOnDate"
                    ErrorMessage="As On Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAsOnDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr id="trType" runat="server">
            <td align="left" class="matters" style="width: 88px">
                Type
            </td>
            <td align="left" class="matters" colspan="1" style="height: 1px; text-align: left;">
                <asp:DropDownList ID="ddType" runat="server" Height="21px" Width="56px">
                    <asp:ListItem Value="ALL">All</asp:ListItem>
                    <asp:ListItem Value="MB">MB</asp:ListItem>
                    <asp:ListItem Value="MP">MP</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trGroupBy" runat="server">
            <td align="left" class="matters" style="width: 88px">
                Group By
            </td>
            <td align="left" class="matters" colspan="1" style="height: 1px; text-align: left;">
                <asp:DropDownList ID="ddGroupby" runat="server">
                    <asp:ListItem Value="ACT">Account Name</asp:ListItem>
                    <asp:ListItem Value="RSS">Account Summary</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="center" class="matters" colspan="2">
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                    ValidationGroup="dayBook" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
            </td>
        </tr>
    </table>
</asp:Content>
