Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports System.Data.SqlTypes
Imports UtilityObj
Partial Class Reports_ASPX_Report_rptGetMISPnL
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle 
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            End If

            If Not IsPostBack Then

                Select Case MainMnu_code
                    Case OASISConstants.ReportMISPnLColumnar
                        lblrptCaption.Text = "P/L Report (Columnar) "
                        ddGroupby.Visible = True
                        chkExcludeSpecialAccounts.Visible = True
                    Case OASISConstants.ReportMISBudgetvsActual
                        lblrptCaption.Text = "Budget vs Actual"
                    Case OASISConstants.ReportMISBalanceSheet
                        lblrptCaption.Text = "Balance Sheet"
                        chkExcludeSpecialAccounts.Visible = True
                        tr_CompareDate.Visible = True
                        chkTrend.Text = "Comparison Report"
                        txtCompareDate.Text = UtilityObj.GetDataFromSQL("SELECT dateadd(year,-1,FYR_toDT) FROM FINANCIALYEAR_S " _
                           & " WHERE '" & UtilityObj.GetDiplayDate() & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
                           WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
                        txtCompareDate.Text = Format(CDate(txtCompareDate.Text), OASISConstants.DateFormat)
                    Case OASISConstants.ReportMISBalanceSheetColumnar
                        lblrptCaption.Text = "Balance Sheet Columnar"
                        ddGroupby.Visible = True
                        chkExcludeSpecialAccounts.Visible = True
                    Case OASISConstants.ReportMISBalanceSheetSchedule
                        lblrptCaption.Text = "Balance Sheet Schedule"
                        chkExcludeSpecialAccounts.Visible = True
                        tr_CompareDate.Visible = True
                        chkTrend.Text = "Balance Sheet Schedule - TB"
                        If Not IsDate(txtCompareDate.Text) Then
                            txtCompareDate.Text = UtilityObj.GetDataFromSQL("SELECT dateadd(year,-1,FYR_toDT) FROM FINANCIALYEAR_S " _
                              & " WHERE '" & UtilityObj.GetDiplayDate() & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
                              WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
                        End If
                        txtCompareDate.Text = Format(CDate(txtCompareDate.Text), OASISConstants.DateFormat)
                    Case OASISConstants.ReportMISCashFlow
                        lblrptCaption.Text = "Cash Flow"
                    Case OASISConstants.ReportMISPnL
                        lblrptCaption.Text = "Income Statements"
                        chkExcludeSpecialAccounts.Visible = True
                    Case OASISConstants.ReportBvsActualColumnar
                        lblrptCaption.Text = "Budget vs Actual Columnar Report"
                    Case OASISConstants.ReportCashFlowColumnar
                        lblrptCaption.Text = "Cash Flow Columnar Report"
                    Case "A753025"
                        lblrptCaption.Text = "Income Statement Schedule"
                    Case "A753085"
                        lblrptCaption.Text = "P/ L Statement - Comparison/Trend"
                        tr_CompareDate.Visible = True
                    Case "A753087"
                        lblrptCaption.Text = "MIS Statement - Trend (Imported)"
                        tr_CompareDate.Visible = True
                        chkTrend.Enabled = False
                        chkTrend.Checked = True
                        lblType.Visible = True
                        ddType.Visible = True
                    Case "A753980"
                        lblrptCaption.Text = "MIS Consolidation - Balance Sheet"
                        ddGroupby.Visible = False
                        chkExcludeSpecialAccounts.Visible = True
                        'Case "A753985"
                        '    lblrptCaption.Text = "MIS Consolidation - Cash Flow"
                        '    ddGroupby.Visible = False
                        '    chkExcludeSpecialAccounts.Visible = True
                End Select
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                txtToDate.Text = UtilityObj.GetDiplayDate()
                txtToDate.Attributes.Add("onBlur", "checkdate(this)")
                If Not IsDate(txtCompareDate.Text) Then
                    txtCompareDate.Text = UtilityObj.GetDiplayDate()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
            Select Case MainMnu_code
                Case OASISConstants.ReportMISBalanceSheet
                    GenerateRPTGetMISBS()
                Case OASISConstants.ReportMISBalanceSheetSchedule
                    If chkTrend.Checked Then
                        RPTGetMISBSScedule_TB()
                    Else
                        GenerateRPTGetMISBSScedule()
                    End If
                Case OASISConstants.ReportMISCashFlow
                    GenerateRPTGetCashFlow()
                Case OASISConstants.ReportMISPnL
                    RPTGetMISDrillDown()
                Case OASISConstants.ReportMISBudgetvsActual
                    rptMISBudgetvsActual()
                Case OASISConstants.ReportMISPnLColumnar
                    rptMISPnLColumnar()
                Case OASISConstants.ReportMISBalanceSheetColumnar
                    RptBalanceSheetColumnar()
                Case OASISConstants.ReportBvsActualColumnar
                    rptMISPnLColumnar_BudgetActual()
                Case OASISConstants.ReportCashFlowColumnar
                    rptMISPnLColumnar_CashFlow()
                Case "A753085"
                    If chkTrend.Checked Then
                        RPTGetMISPnL_Trend()
                    Else
                        RPTGetMISPnL_Compare()
                    End If
                Case "A753087"
                    RPTGetMISPnL_Imported_Trend()
				   Case "A753980"
                    RptMISConsolidation_BalanceSheet()
            End Select
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub RPTGetMISPnL_Imported_Trend()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)

        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")

        Dim cmd As New SqlCommand("RPTGetMISPnL_Trend_Imported", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpCompDate As New SqlParameter("@CompDate", SqlDbType.DateTime)
        sqlpCompDate.Value = txtCompareDate.Text
        cmd.Parameters.Add(sqlpCompDate)

        Dim sqlpType As New SqlParameter("@Type", SqlDbType.VarChar)
        sqlpType.Value = ddType.SelectedValue
        cmd.Parameters.Add(sqlpType)


        objConn.Close()
        objConn.Open()
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        If ddType.SelectedValue = "MP" Then
            params("ReportCaption") = "P/L Statement - Trend (Imported)"
        Else
            params("ReportCaption") = "B/S Statement - Trend (Imported)"
        End If
        ' params("ReportCaption") = "MIS Statement - Trend (Imported)"
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        params("ToDate") = txtToDate.Text
        params("fromDate") = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        params("fromDate") = Format(CDate(params("fromDate")), OASISConstants.DateFormat)
        params("CompareDate") = txtCompareDate.Text

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/RPTGetMISPnL_Imported_Trend.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        '  Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub


    Private Sub RPTGetMISPnL_Trend() 
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)

        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")

        Dim cmd As New SqlCommand("RPTGetMISPnL_Trend", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpCompDate As New SqlParameter("@CompDate", SqlDbType.DateTime)
        sqlpCompDate.Value = txtCompareDate.Text
        cmd.Parameters.Add(sqlpCompDate)

        objConn.Close()
        objConn.Open()
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        params("ReportCaption") = "Income Statements - Trend Report"
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        params("ToDate") = txtToDate.Text
        params("fromDate") = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        params("fromDate") = Format(CDate(params("fromDate")), OASISConstants.DateFormat)
        params("CompareDate") = txtCompareDate.Text

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/RPTGetMISPnL_Trend.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        ' Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Private Sub RPTGetMISPnL_Compare()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim Misparams As New MisParameters
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")

        Dim cmd As New SqlCommand("RPTGetMISPnL", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", SqlDbType.Bit)
        sqlpBSpecial.Value = chkExcludeSpecialAccounts.Checked
        cmd.Parameters.Add(sqlpBSpecial)

        Dim sqlpCompDate As New SqlParameter("@CompDate", SqlDbType.DateTime)
        sqlpCompDate.Value = txtCompareDate.Text
        cmd.Parameters.Add(sqlpCompDate)

        objConn.Close()
        objConn.Open()
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        If chkExcludeSpecialAccounts.Checked Then
            params("ReportCaption") = "Income Statements (Excluding Special Accounts)"
        Else
            params("ReportCaption") = "Income Statements"
        End If
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        Misparams.BSU_IDs = strSelectedNodes
        Misparams.BSUShortnames = str_bsu_shortname
        Misparams.BSUSegments = str_bsu_Segment
        Misparams.ToDate = txtToDate.Text
        Misparams.FromDate = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Misparams.FromDate = Format(CDate(Misparams.FromDate), OASISConstants.DateFormat)
        params("ToDate") = txtToDate.Text
        params("fromDate") = Misparams.FromDate
        params("CompareDate") = txtCompareDate.Text
        Misparams.Currency = str_bsu_CURRENCY
        Misparams.ExpressedIn = ddExpressedin.SelectedItem.Value
        Misparams.ExcludeSpecialAccounts = chkExcludeSpecialAccounts.Checked
        Session("Misparams") = Misparams
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptGetMISPnLBudget_Compare.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        '  Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Private Sub rptMISPnLColumnar_CashFlow()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")
        Dim str_report_sp As String = "[RPTGetMISCAshflow_Columnar]"
        Dim bSegment As Boolean = False
        If ddGroupby.SelectedItem.Value = "SEG" Then
            bSegment = True
        End If
        Dim cmd As New SqlCommand(str_report_sp, objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs) 

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpDTTO) 
      
        objConn.Close()
        objConn.Open() 

        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        params("ReportCaption") = "Cash Flow Columnar Report"
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        params("ToDate") = txtToDate.Text
        params("fromDate") = Format(CDate(sqlpFROMDOCDT.Value), OASISConstants.DateFormat)
        params("DrillDownMode") = ""
        repSource.Parameter = params
        repSource.Command = cmd


        Dim Misparams As New MisParameters

        Misparams.BSU_IDs = strSelectedNodes
        Misparams.BSUShortnames = str_bsu_shortname
        Misparams.BSUSegments = str_bsu_Segment
        Misparams.ToDate = txtToDate.Text
        Misparams.FromDate = Format(CDate(txtToDate.Text), OASISConstants.DateFormat)

        Misparams.Currency = str_bsu_CURRENCY
        Misparams.ExpressedIn = ddExpressedin.SelectedItem.Value
        Misparams.ExcludeSpecialAccounts = chkExcludeSpecialAccounts.Checked
        Session("Misparams") = Misparams


        repSource.ResourceName = "../RPT_Files/rptMISPnLColumnar.rpt"
        Session("ReportSource") = repSource 
        objConn.Close()
        ' Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Private Sub RptBalanceSheetColumnar()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")
        Dim str_report_sp As String = "[RPTGetMISBS_Columnar]"
        Dim bSegment As Boolean = False
           Dim Misparams As New MisParameters
        If ddGroupby.SelectedItem.Value = "SEG" Then
            bSegment = True
        End If
        Dim cmd As New SqlCommand(str_report_sp, objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpbSegment As New SqlParameter("@bSegment", SqlDbType.Bit)
        sqlpbSegment.Value = bSegment
        cmd.Parameters.Add(sqlpbSegment)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", SqlDbType.Bit)
        sqlpBSpecial.Value = chkExcludeSpecialAccounts.Checked
        cmd.Parameters.Add(sqlpBSpecial)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpDTTO)

        objConn.Close()
        objConn.Open()

        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        If chkExcludeSpecialAccounts.Checked Then
            params("ReportCaption") = "Balance Sheet Columnar Report (Excluding Special Accounts)"
        Else
            params("ReportCaption") = "Balance Sheet Columnar Report"
        End If
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        params("ToDate") = txtToDate.Text
        params("fromDate") = Format(CDate(sqlpFROMDOCDT.Value), OASISConstants.DateFormat)
        params("DrillDownMode") = "BSCOLUMNAR"
        repSource.Parameter = params
        repSource.Command = cmd
        Misparams.BSU_IDs = strSelectedNodes
        Misparams.BSUShortnames = str_bsu_shortname
        Misparams.BSUSegments = str_bsu_Segment
        Misparams.ToDate = txtToDate.Text
        Misparams.FromDate = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Misparams.FromDate = Format(CDate(Misparams.FromDate), OASISConstants.DateFormat)
        params("ToDate") = txtToDate.Text
        params("fromDate") = Misparams.FromDate
        Misparams.Currency = str_bsu_CURRENCY
        Misparams.ExpressedIn = ddExpressedin.SelectedItem.Value
        Misparams.ExcludeSpecialAccounts = chkExcludeSpecialAccounts.Checked
        Session("Misparams") = Misparams
        repSource.ResourceName = "../RPT_Files/rptMISPnLColumnar.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        ' Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Private Sub RptMISConsolidation_BalanceSheet()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")
        Dim str_report_sp As String = "[RPTGetMISBS_Columnar_consolidation]"
        Dim bSegment As Boolean = False
        Dim Misparams As New MisParameters

        Dim cmd As New SqlCommand(str_report_sp, objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", SqlDbType.Bit)
        sqlpBSpecial.Value = chkExcludeSpecialAccounts.Checked
        cmd.Parameters.Add(sqlpBSpecial)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpDTTO)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        cmd.Parameters.Add(sqlpFROMDOCDT)

        objConn.Close()
        objConn.Open()

        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        If chkExcludeSpecialAccounts.Checked Then
            params("ReportCaption") = "MIS Consolidation Balance Sheet Report(Excluding Special Accounts)"
        Else
            params("ReportCaption") = "MIS Consolidation Balance Sheet Report"
        End If
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        params("ToDate") = txtToDate.Text
        params("fromDate") = Format(CDate(sqlpFROMDOCDT.Value), OASISConstants.DateFormat)
        params("DrillDownMode") = "BSCOLUMNAR"
        repSource.Parameter = params
        repSource.Command = cmd
        Misparams.BSU_IDs = strSelectedNodes
        Misparams.BSUShortnames = str_bsu_shortname
        Misparams.BSUSegments = str_bsu_Segment
        Misparams.ToDate = txtToDate.Text
        Misparams.FromDate = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Misparams.FromDate = Format(CDate(Misparams.FromDate), OASISConstants.DateFormat)
        params("ToDate") = txtToDate.Text
        params("fromDate") = Misparams.FromDate
        Misparams.Currency = str_bsu_CURRENCY
        Misparams.ExpressedIn = ddExpressedin.SelectedItem.Value
        Misparams.ExcludeSpecialAccounts = chkExcludeSpecialAccounts.Checked
        Session("Misparams") = Misparams
        repSource.ResourceName = "../RPT_Files/rptMISPnLColumnar.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        'Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Private Sub rptMISPnLColumnar_BudgetActual()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")
        Dim str_report_sp As String = "rptMISPnLColumnar"
        If ddGroupby.SelectedItem.Value = "SEG" Then
            str_report_sp = "rptMISPnLColumnar_Segment"
        End If
        Dim cmd As New SqlCommand(str_report_sp, objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpDTTO)

        objConn.Close()
        objConn.Open()
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        params("ReportCaption") = "Budget vs Actual Columnar Report"
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        params("ToDate") = txtToDate.Text
        params("fromDate") = Format(CDate(sqlpFROMDOCDT.Value), OASISConstants.DateFormat)
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptMISPnLColumnar_BudgetvsActual.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        ' Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Private Sub rptMISPnLColumnar()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim Misparams As New MisParameters
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")
         Dim str_report_sp As String = "rptMISPnLColumnar"
        If ddGroupby.SelectedItem.Value = "SEG" Then
            str_report_sp = "rptMISPnLColumnar_Segment"
        End If
        Dim cmd As New SqlCommand(str_report_sp, objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpDTTO)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", SqlDbType.Bit)
        sqlpBSpecial.Value = chkExcludeSpecialAccounts.Checked
        cmd.Parameters.Add(sqlpBSpecial)

        objConn.Close()
        objConn.Open()
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        If chkExcludeSpecialAccounts.Checked Then
            params("ReportCaption") = "Income Statement for the period ending YTD (Excluding Special Accounts)"
        Else
            params("ReportCaption") = "Income Statement for the period ending YTD"
        End If
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        params("ToDate") = txtToDate.Text
        params("fromDate") = Format(CDate(sqlpFROMDOCDT.Value), OASISConstants.DateFormat)
        params("DrillDownMode") = "PLCOLUMNAR"
        repSource.Parameter = params
        repSource.Command = cmd
        Misparams.BSU_IDs = strSelectedNodes
        Misparams.BSUShortnames = str_bsu_shortname
        Misparams.BSUSegments = str_bsu_Segment
        Misparams.ToDate = txtToDate.Text
        Misparams.FromDate = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Misparams.FromDate = Format(CDate(Misparams.FromDate), OASISConstants.DateFormat)
        params("ToDate") = txtToDate.Text
        params("fromDate") = Misparams.FromDate
        Misparams.Currency = str_bsu_CURRENCY
        Misparams.ExpressedIn = ddExpressedin.SelectedItem.Value
        Misparams.ExcludeSpecialAccounts = chkExcludeSpecialAccounts.Checked
        Session("Misparams") = Misparams
        repSource.ResourceName = "../RPT_Files/rptMISPnLColumnar.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        'Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Private Sub rptMISBudgetvsActual()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")
         Dim cmd As New SqlCommand("rptMISBudgetvsActual", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpDTTO)

        Dim sqlpRSS_TYP As New SqlParameter("@RSS_TYP", SqlDbType.VarChar, 20)
        sqlpRSS_TYP.Value = "MP"
        cmd.Parameters.Add(sqlpRSS_TYP)
        objConn.Close()
        objConn.Open()
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        params("ReportCaption") = "Budget vs Actual"
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        params("ToDate") = txtToDate.Text
        params("fromDate") = Format(CDate(sqlpFROMDOCDT.Value), OASISConstants.DateFormat)
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptMISBudgetvsActual.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        '   Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Private Sub RPTGetMISDrillDown()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim Misparams As New MisParameters
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")

        Dim cmd As New SqlCommand("RPTGetMISPnL", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", SqlDbType.Bit)
        sqlpBSpecial.Value = chkExcludeSpecialAccounts.Checked
        cmd.Parameters.Add(sqlpBSpecial)

        objConn.Close()
        objConn.Open()
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        If chkExcludeSpecialAccounts.Checked Then
            params("ReportCaption") = "Income Statements (Excluding Special Accounts)"
        Else
            params("ReportCaption") = "Income Statements"
        End If
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        Misparams.BSU_IDs = strSelectedNodes
        Misparams.BSUShortnames = str_bsu_shortname
        Misparams.BSUSegments = str_bsu_Segment
        Misparams.ToDate = txtToDate.Text
        Misparams.FromDate = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Misparams.FromDate = Format(CDate(Misparams.FromDate), OASISConstants.DateFormat)
        params("ToDate") = txtToDate.Text
        params("fromDate") = Misparams.FromDate
        Misparams.Currency = str_bsu_CURRENCY
        Misparams.ExpressedIn = ddExpressedin.SelectedItem.Value
        Misparams.ExcludeSpecialAccounts = chkExcludeSpecialAccounts.Checked
        Session("Misparams") = Misparams
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptGetMISPnLBudget.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        '  Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Private Sub GenerateRPTGetMISPnL()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")

        Dim cmd As New SqlCommand("RPTGetMISPnL", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        objConn.Close()
        objConn.Open()
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        params("ToDate") = txtToDate.Text
        params("ReportCaption") = "Income Statement"
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptGetMISPnL.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        ' Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Private Sub GenerateRPTGetMISBS()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
       
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")

        Dim cmd As SqlCommand
        If chkTrend.Checked And IsDate(txtCompareDate.Text) Then
            cmd = New SqlCommand("RPTGetMISBS_Compare", objConn)
            Dim sqlpCompareDT As New SqlParameter("@CompareDT", SqlDbType.DateTime)
            sqlpCompareDT.Value = txtCompareDate.Text
            cmd.Parameters.Add(sqlpCompareDT)
        Else
            cmd = New SqlCommand("RPTGetMISBS", objConn)
        End If
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", SqlDbType.Bit)
        sqlpBSpecial.Value = chkExcludeSpecialAccounts.Checked
        cmd.Parameters.Add(sqlpBSpecial)

        objConn.Close()
        objConn.Open()
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        params("ToDate") = txtToDate.Text
        If chkExcludeSpecialAccounts.Checked Then
            params("ReportCaption") = "Balance Sheet (Excluding Special Accounts)"
        Else
            params("ReportCaption") = "Balance Sheet"
        End If
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        ''''''
        Dim Misparams As New MisParameters
        Misparams.BSU_IDs = strSelectedNodes
        Misparams.BSUShortnames = str_bsu_shortname
        Misparams.BSUSegments = str_bsu_Segment
        Misparams.ToDate = txtToDate.Text
        Misparams.FromDate = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Misparams.FromDate = Format(CDate(Misparams.FromDate), OASISConstants.DateFormat)
        Misparams.Currency = str_bsu_CURRENCY
        Misparams.ExpressedIn = ddExpressedin.SelectedItem.Value
        Misparams.ExcludeSpecialAccounts = chkExcludeSpecialAccounts.Checked
        Session("Misparams") = Misparams
        ''''
        repSource.Parameter = params
        repSource.Command = cmd
        If chkTrend.Checked And IsDate(txtCompareDate.Text) Then
            repSource.ResourceName = "../RPT_Files/RPTGetMISBS_Compare.rpt"
            params("CompareDate") = txtCompareDate.Text
        Else
            repSource.ResourceName = "../RPT_Files/RPTGetMISBS.rpt"
        End If
        Session("ReportSource") = repSource
        objConn.Close()
        ' Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Private Sub GenerateRPTGetCashFlow()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
    
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")

        Dim cmd As New SqlCommand("RPTGetMISCAshflow", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        objConn.Close()
        objConn.Open()

        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        params("ToDate") = txtToDate.Text
        params("ReportCaption") = "Cash Flow"
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value

        Dim Misparams As New MisParameters

        Misparams.BSU_IDs = strSelectedNodes
        Misparams.BSUShortnames = str_bsu_shortname
        Misparams.BSUSegments = str_bsu_Segment
        Misparams.ToDate = txtToDate.Text
        Misparams.FromDate = Format(CDate(txtToDate.Text), OASISConstants.DateFormat)

        Misparams.Currency = str_bsu_CURRENCY
        Misparams.ExpressedIn = ddExpressedin.SelectedItem.Value
        Misparams.ExcludeSpecialAccounts = chkExcludeSpecialAccounts.Checked
        Session("Misparams") = Misparams
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/RPTGetMISCF.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        '  Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub


    Private Sub GenerateRPTGetMISBSScedule()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
       
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")

        Dim cmd As New SqlCommand("RPTGetMISBSScedule", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", SqlDbType.Bit)
        sqlpBSpecial.Value = chkExcludeSpecialAccounts.Checked
        cmd.Parameters.Add(sqlpBSpecial)

        objConn.Close()
        objConn.Open()

        Dim str_bsu_Segment As String = ""
        Dim str_bsu_CURRENCY As String = ""
        Dim str_bsu_shortname As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        params("ToDate") = txtToDate.Text
        If chkExcludeSpecialAccounts.Checked Then
            params("ReportCaption") = "Balance Sheet Schedule (Excluding Special Accounts)"
        Else
            params("ReportCaption") = "Balance Sheet Schedule"
        End If
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/RPTGetMISBSSchedule.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        ' Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Private Sub RPTGetMISBSScedule_TB()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)

        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")

        Dim cmd As New SqlCommand("RPTGetMISBSScedule_TB", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpCompareDT As New SqlParameter("@CompareDT", SqlDbType.DateTime)
        sqlpCompareDT.Value = txtCompareDate.Text
        cmd.Parameters.Add(sqlpCompareDT)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", SqlDbType.Bit)
        sqlpBSpecial.Value = chkExcludeSpecialAccounts.Checked
        cmd.Parameters.Add(sqlpBSpecial)

        objConn.Close()
        objConn.Open()

        Dim str_bsu_Segment As String = ""
        Dim str_bsu_CURRENCY As String = ""
        Dim str_bsu_shortname As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        params("ToDate") = txtToDate.Text
        params("CompareDT") = txtCompareDate.Text
        If chkExcludeSpecialAccounts.Checked Then
            params("ReportCaption") = "Balance Sheet Schedule - TB (Excluding Special Accounts)"
        Else
            params("ReportCaption") = "Balance Sheet Schedule - TB"
        End If
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/RPTGetMISBSSchedule_TB.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        '  Response.Redirect("rptviewer.aspx", True)
        ReportLoadSelection()

    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    'Private Sub RPTGetMISCAshflow_Columnar_Consolidation()
    '    Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
    '    Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")
    '    Dim str_report_sp As String = "[RPTGetMISCAshflow_Columnar_Consolidation]"
    '    Dim bSegment As Boolean = False
    '    If ddGroupby.SelectedItem.Value = "SEG" Then
    '        bSegment = True
    '    End If
    '    Dim cmd As New SqlCommand(str_report_sp, objConn)
    '    cmd.CommandType = CommandType.StoredProcedure

    '    Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
    '    sqlpJHD_BSU_IDs.Value = strSelectedNodes
    '    cmd.Parameters.Add(sqlpJHD_BSU_IDs)

    '    Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
    '    sqlpFROMDOCDT.Value = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
    '    & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
    '    WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
    '    'cmd.Parameters.Add(sqlpFROMDOCDT)

    '    Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
    '    sqlpDTTO.Value = txtToDate.Text
    '    cmd.Parameters.Add(sqlpDTTO)

    '    objConn.Close()
    '    objConn.Open()

    '    Dim str_bsu_Segment As String = ""
    '    Dim str_bsu_shortname As String = ""
    '    Dim str_bsu_CURRENCY As String = ""
    '    getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
    '    str_bsu_shortname, str_bsu_CURRENCY)

    '    cmd.Connection = objConn
    '    Dim repSource As New MyReportClass
    '    Dim params As New Hashtable
    '    params("UserName") = Session("sUsr_name")
    '    params("Bsu_Shortnames") = str_bsu_shortname
    '    params("Bsu_Segments") = str_bsu_Segment
    '    params("ReportCaption") = "Cash Flow Columnar Report"
    '    params("BSU_CURRENCY") = str_bsu_CURRENCY
    '    params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
    '    params("ToDate") = txtToDate.Text
    '    params("fromDate") = Format(CDate(sqlpFROMDOCDT.Value), OASISConstants.DateFormat)
    '    params("DrillDownMode") = "MCCOLUMNARNEW"
    '    repSource.Parameter = params
    '    repSource.Command = cmd
    '    ''''
    '    Dim Misparams As New MisParameters
    '    Misparams.BSU_IDs = strSelectedNodes
    '    Misparams.BSUShortnames = str_bsu_shortname
    '    Misparams.BSUSegments = str_bsu_Segment
    '    Misparams.ToDate = txtToDate.Text
    '    Misparams.FromDate = params("fromDate")
    '    Misparams.Currency = str_bsu_CURRENCY
    '    Misparams.ExpressedIn = ddExpressedin.SelectedItem.Value
    '    Misparams.ExcludeSpecialAccounts = chkExcludeSpecialAccounts.Checked
    '    Session("Misparams") = Misparams
    '    ''''
    '    repSource.ResourceName = "../RPT_Files/rptMISPnLColumnar.rpt"
    '    Session("ReportSource") = repSource
    '    objConn.Close()
    '    Response.Redirect("rptviewer.aspx", True)
    'End Sub

End Class
