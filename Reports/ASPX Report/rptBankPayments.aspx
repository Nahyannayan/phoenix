<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptBankPayments.aspx.vb" Inherits="rptBankPayments" Title="Bank Payments" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
      
           <%--function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 250px; ";
            sFeatures+="dialogHeight: 270px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx","", sFeatures);
            if(result != '' && result != undefined)
            {
                switch(txtControl)
                {
                  case 0:
                    document.getElementById('<%=txtfromDate.ClientID %>').value=result;
                    break;
                  case 1:  
                    document.getElementById('<%=txtToDate.ClientID %>').value=result;
                    break;
                }    
            }
            return false;
           }--%>


        function GetBankName(id) {
           
            var NameandCode;
            var mode;
            var result;
            document.getElementById('<%=h_id.ClientID%>').value = id;
            if(id == 0) {
                mode = document.getElementById('<%=h_mode.ClientID %>').value;
                if (mode == 'bank') {
                    result = radopen("../../Accounts/accjvshowaccount.aspx?bankcash=b&multiSelect=false&forrpt=1", "pop_up");
                }
                else if (mode == 'cash') {
                    result = radopen("../../Accounts/accjvshowaccount.aspx?bankcash=c&multiSelect=false&forrpt=1", "pop_up");
                }
                // result = window.showModalDialog("NewWorkArea/showAccount.aspx?filter=Bank","", sFeatures)

            }
            else if(id==1){
                result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=false&forrpt=1", "pop_up");
                // result = window.showModalDialog("NewWorkArea/showAccount.aspx?filter=Account","", sFeatures)
            }
        }
                <%--if (result=='' || result==undefined)
                {
                    return false;
                }
                NameandCode = result.split('___');
                switch(id)
                {
                    case 0:
                       document.getElementById('<%=txtbankID.ClientID %>').value=NameandCode[0]; 
                       document.getElementById('<%=txtBankName.ClientID %>').value=NameandCode[1]; 
                    break;
                    case 1:
                        document.getElementById('<%=txtAccountID.ClientID %>').value=NameandCode[0]; 
                        document.getElementById('<%=txtAccountName.ClientID %>').value=NameandCode[1]; 
                    break;
                }
               return false;--%>
        


        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var id;
            id = document.getElementById('<%=h_id.ClientID%>').value;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
               if(id==0){
                        document.getElementById('<%=txtbankID.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%=txtBankName.ClientID %>').value = NameandCode[1];
                        __doPostBack('<%= txtBankName.ClientID%>', 'TextChanged');
               }
               else if(id == 1)
               {
                        document.getElementById('<%=txtAccountID.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%=txtAccountName.ClientID %>').value = NameandCode[1];
                        __doPostBack('<%= txtAccountName.ClientID%>', 'TextChanged');
                    }
                }
            }
        


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>


    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblRepCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="bankPayment" />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">From Date</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtfromDate" runat="server"></asp:TextBox>
                            <img id="imgFrom" runat="server" src="../../Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgFrom" TargetControlID="txtfromDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtfromDate" TargetControlID="txtfromDate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="valFromDate" runat="server" ControlToValidate="txtfromDate"
                                ErrorMessage="From Date Required" ValidationGroup="bankPayment">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                    EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="bankPayment">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%">
                            <span class="field-label">To Date</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                            <img id="imgToDate" runat="server" src="../../Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="imgToDate" TargetControlID="txtToDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                                PopupButtonID="txtToDate" TargetControlID="txtToDate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="valToDate" runat="server" ErrorMessage="To Date Required" ControlToValidate="txtToDate" ValidationGroup="bankPayment">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="bankPayment">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <!-- Grade & Section -->
                    <!-- Name -->
                    <!-- Student Id -->
                    <tr runat="server" id="trFilterBy">
                        <td align="left">
                            <span class="field-label">Filtered By</span></td>

                        <td align="left">
                            <asp:RadioButton ID="radVoucherDate" runat="server" CssClass="field-label" Checked="True" GroupName="dateType"
                                Text="Voucher Date" />

                            <asp:RadioButton ID="radChequeDate" runat="server" CssClass="field-label" GroupName="dateType" Text="Cheque Date" /></td>
                        <td colspan="2"></td>

                    </tr>
                    <tr runat="server" id="trBankRow">
                        <td align="left">
                            <asp:Label ID="lblType" runat="server" CssClass="field-label" Text="Bank"></asp:Label></td>

                        <td colspan="2" align="left">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtBankName" runat="server" Width="92%"></asp:TextBox>
                                        <img src="../../Images/forum_search.gif" onclick="GetBankName(0); return false;" />

                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtBankID" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td></td>

                    </tr>
                    <tr runat="server" id="trCollectionRow">
                        <td align="left">Collection Type</td>

                        <td align="left">
                            <asp:DropDownList ID="ddCollection" runat="server"
                                DataTextField="COL_DESCR" DataValueField="COL_ID" SkinID="DropDownListNormal">
                            </asp:DropDownList><asp:SqlDataSource ID="collection" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                                SelectCommand="SELECT COL_ID, COL_DESCR FROM COLLECTION_M"></asp:SqlDataSource>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr runat="server" id="trAccountRow">
                        <td align="left">
                            <span class="field-label">Account</span></td>

                        <td colspan="2" align="left">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtAccountName" runat="server" Width="92%"></asp:TextBox>
                                        <img src="../../Images/forum_search.gif" onclick="GetBankName(1); return false;" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAccountID" runat="server"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr runat="server" id="trStatus">
                        <td align="left">
                            <span class="field-label">Status</span></td>

                        <td align="left">

                            <asp:RadioButton ID="radStatusAll" runat="server" CssClass="field-label" GroupName="chkStatus" Text="All" Checked="True" />
                            <asp:RadioButton ID="radStatusOpen" runat="server" CssClass="field-label" GroupName="chkStatus" Text="Open" />
                            <asp:RadioButton ID="radStatusPosted" runat="server" CssClass="field-label" GroupName="chkStatus" Text="Posted" />
                            <asp:RadioButton ID="radStatusDeleted" runat="server" CssClass="field-label" GroupName="chkStatus" Text="Deleted" /></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="field-label">Business Unit</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddlBSUnit" runat="server"></asp:DropDownList></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr runat="server" id="trSummary">
                        <td colspan="4" align="left">
                            <asp:CheckBox ID="chkSummary" CssClass="field-label" runat="server" Text="View Summary" /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                            <asp:Button ID="btnSubmit" runat="server" Text="Generate Report" ValidationGroup="bankPayment" CssClass="button" />
                            <asp:Button ID="Button1" runat="server" Text="Cancel" UseSubmitBehavior="False" CssClass="button" /></td>
                    </tr>
                    <!-- ===================================== APPLICANT 6 ======================================= -->
                </table>
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_mode" runat="server" />
                <asp:HiddenField ID="h_id" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>

