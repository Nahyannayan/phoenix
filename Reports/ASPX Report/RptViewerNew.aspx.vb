Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports UtilityObj
Imports System.Drawing.Printing
Imports System.IO

Partial Class Reports_ASPX_Report_RptViewernew
    Inherits System.Web.UI.Page

    Dim repSource As MyReportClass
    Dim repClassVal As New RepClass
    Dim Encr_decrData As New Encryption64
    Shared rnd As New Random()
    'Dim rs As ReportDocument
    Dim rptClass As New rptClass

    'Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
    '    Using msOur As New System.IO.MemoryStream()
    '        Dim swOur As New System.IO.StreamWriter(msOur)
    '        Dim ourWriter As New HtmlTextWriter(swOur)
    '        MyBase.Render(ourWriter)
    '        ourWriter.Flush()
    '        msOur.Position = 0
    '        Using oReader As New System.IO.StreamReader(msOur)
    '            Dim sTxt As String = oReader.ReadToEnd()
    '            If Web.Configuration.WebConfigurationManager.AppSettings("AlwaysSSL").ToLower = "true" Then
    '                sTxt = sTxt.Replace(Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString(), _
    '                Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString().Replace("http://", "https://"))
    '            End If
    '            Response.Write(sTxt)
    '            oReader.Close()
    '        End Using
    '        End Using
    '    End Using
    'End Sub


    Protected Sub btnPrintReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintReport.Click
        Try
            If repClassVal IsNot Nothing Then
                'Dim printDocument As New System.Drawing.Printing.PrintDocument()
                'Response.Write(Drawing.Printing.PrinterSettings.InstalledPrinters.Count)
                'repClassVal.PrintOptions.PrinterName = ""
                'repClassVal.PrintToPrinter(1, False, 0, 0)
                'Response.Redirect("printReport.aspx", True)
                'CrystalReportViewer1.RefreshReport()


            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        Dim ds As New DataSet

        ' Page.Title = OASISConstants.Gemstitle

        If Not IsPostBack Then
            repSource = DirectCast(Session("ReportSource"), MyReportClass)
            Session("TempReportSource" & ViewState("h_uniqueID")) = repSource
        Else
            repSource = DirectCast(Session("TempReportSource" & ViewState("h_uniqueID")), MyReportClass)
        End If

        If Cache.Item("reportDS" & ViewState("h_uniqueID")) Is Nothing Then
            Cache.Remove("reportDS" & ViewState("h_uniqueID"))
            ViewState("h_uniqueID") = Session.SessionID & rnd.Next()
            'Session("TempReportSource" & ViewState("h_uniqueID")) = repSource
        End If
        'repSource.Parameter.Add("@IMG_BSU_ID", Session("sbsuid"))
        'repSource.Parameter.Add("@IMG_TYPE", "LOGO")
        If Not repSource Is Nothing Then
            Try
                Errorlog("Not repSource Is Nothing", "1")
                If repSource.GetDataSourceFromCommand Then

                    If Cache("reportDS" & ViewState("h_uniqueID")) Is Nothing Then
                        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                        'Dim objConn As New SqlConnection(str_conn)
                        Dim objConn As SqlConnection = repSource.Command.Connection
                        'repSource.Command.Connection = objConn
                        objConn.Close()
                        objConn.Open()
                        Dim adpt As New SqlDataAdapter
                        adpt.SelectCommand = repSource.Command
                        'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey
                        adpt.SelectCommand.CommandTimeout = 0
                        ds.Clear()
                        adpt.Fill(ds)
                        If ds Is Nothing OrElse ds.Tables.Count <= 0 OrElse ds.Tables(0).Rows.Count <= 0 Then
                            If Not Request.UrlReferrer Is Nothing Then
                                If Request.UrlReferrer.ToString() <> "" And "/" & Request.UrlReferrer.GetComponents(UriComponents.Path, UriFormat.SafeUnescaped) <> Request.CurrentExecutionFilePath Then
                                    Dim urlstring As String = IIf(Request.UrlReferrer.ToString().Contains("?"), "&nodata=true", "?nodata=true")
                                    'Response.Redirect(Request.UrlReferrer.ToString() & urlstring)
                                End If
                            Else
                                'Exit Sub
                            End If
                        End If
                        Dim isCircular = IIf(Request.QueryString("isCircular") Is Nothing, False, Request.QueryString("isCircular"))
                        If isCircular Then
                            UpdatePassword(ds.Tables(0))
                        End If
                        objConn.Close()
                        Cache.Insert("reportDS" & ViewState("h_uniqueID"), ds, Nothing, DateTime.Now.AddMinutes(10), Nothing)
                    End If
                    repClassVal = New RepClass
                    repClassVal.ResourceName = repSource.ResourceName
                    ds = DirectCast(Cache.Item("reportDS" & ViewState("h_uniqueID")), DataSet)
                    repClassVal.SetDataSource(ds.Tables(0))


                    If repClassVal.Subreports("rptHeader.rpt") IsNot Nothing Or repClassVal.Subreports("ReportHeader_SubReport_Payroll.rpt") IsNot Nothing Or repClassVal.Subreports("ReportHeader_SubReport.rpt") IsNot Nothing Or repClassVal.Subreports("ReportHeader_SubReport_Curr.rpt") IsNot Nothing Or repClassVal.Subreports("rptHeader_Transport.rpt") IsNot Nothing Or repClassVal.Subreports("rptHeader_Portrait.rpt") IsNot Nothing Or repClassVal.Subreports("ReportHeader_Sub_Landscape.rpt") IsNot Nothing Then
                        repSource.IncludeBSUImage = True
                    End If


                    If repSource.IncludeBSUImage Then
                        '    If repSource.HeaderBSUID Is Nothing Then
                        IncludeBSUmage(repClassVal)
                        '    Else
                        '        IncludeBSUmage(repClassVal, repSource.HeaderBSUID)
                        '    End If

                    End If
                    With rptClass
                        Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                        SubReport(repSource, repClassVal)
                        'repClassVal.SetDatabaseLogon(.crUser, .crPassword, .crInstanceName, .crOasisdb)
                        'repClassVal.SetDatabaseLogon(.crUser, .crPassword, .crInstanceName, .crDatabase)
                    End With
                    If Session("ReportSel").ToString <> "PDF" And Session("ReportSel").ToString <> "EXCEL" Then
                        CrystalReportViewer1.ReportSource = repClassVal
                    End If


                    repClassVal.PrintOptions.PaperOrientation = repClassVal.PrintOptions.PaperOrientation
                    'repClassVal.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                    Dim ienum As IDictionaryEnumerator = repSource.Parameter.GetEnumerator()
                    While ienum.MoveNext()
                        Try
                            If ienum.Key = "userName" Then
                                If Not Session("lastMnuCode") Is Nothing Then
                                    repClassVal.SetParameterValue(ienum.Key, ienum.Value + " [Ref. - " + Session("lastMnuCode") + "]")
                                Else

                                    repClassVal.SetParameterValue(ienum.Key, ienum.Value + " [Ref. - ")
                                End If
                            Else
                                repClassVal.SetParameterValue(ienum.Key, ienum.Value)
                            End If
                        Catch ex As Exception
                            Errorlog("rptviewer-while", ex.Message)
                        End Try

                    End While
                    Try
                        If Not repSource Is Nothing Then
                            If Not repSource.Formulas Is Nothing Then
                                If Not repSource.Formulas.GetEnumerator() Is Nothing Then
                                    Dim iformula As IDictionaryEnumerator = repSource.Formulas.GetEnumerator()
                                    While iformula.MoveNext()
                                        If Not repClassVal.DataDefinition.FormulaFields(iformula.Key) Is Nothing Then
                                            repClassVal.DataDefinition.FormulaFields(iformula.Key).text = iformula.Value
                                        End If
                                    End While
                                End If
                            End If
                        End If


                        If Session("ReportSel") = "PDF" Then
                            'Session("ReportSel") = ""
                            repClassVal.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "Exported Report")
                        ElseIf Session("ReportSel") = "EXCEL" Then
                            repClassVal.ExportToHttpResponse(ExportFormatType.ExcelWorkbook, Response, False, "Exported Report")
                        End If
                    Catch ex As Exception
                        Dim exm As String = ex.Message
                    End Try

                    ''commented by nahyan on 2feb2015 obsolete
                    'CrystalReportViewer1.DisplayGroupTree = repSource.DisplayGroupTree
                    'objConn.Close()
                Else
                    Dim dt As DataTable
                    If Cache("reportDS" & ViewState("h_uniqueID")) Is Nothing Then
                        'Dim objConn As New SqlConnection(str_conn)
                        Dim objConn As SqlConnection = repSource.Command.Connection
                        'repSource.Command.Connection = objConn
                        objConn.Close()
                        objConn.Open()
                        'Dim ds As New DataSet
                        Dim adpt As New SqlDataAdapter
                        adpt.SelectCommand = repSource.Command
                        adpt.Fill(ds)
                        '' '
                        Dim fromDate As Date
                        If repSource.Parameter IsNot Nothing Then
                            fromDate = CDate(repSource.Parameter("FromDate"))
                        End If
                        dt = TransformDatasettoTable.TransformDataset(ds.Tables(0), fromDate)
                        Cache.Insert("reportDS" & ViewState("h_uniqueID"), dt, Nothing, DateTime.Now.AddMinutes(30), Nothing)
                        objConn.Close()
                    End If
                    repClassVal = New RepClass
                    repClassVal.ResourceName = repSource.ResourceName

                    dt = Cache("reportDS" & ViewState("h_uniqueID"))
                    repClassVal.SetDataSource(dt)

                    '' ''
                    '' '
                    CrystalReportViewer1.ReportSource = repClassVal
                    Dim ienum As IDictionaryEnumerator = repSource.Parameter.GetEnumerator()
                    While ienum.MoveNext()
                        repClassVal.SetParameterValue(ienum.Key, ienum.Value)
                    End While
                    If Not repSource Is Nothing Then
                        If Not repSource.Formulas Is Nothing Then
                            If Not repSource.Formulas.GetEnumerator() Is Nothing Then
                                Dim iformula As IDictionaryEnumerator = repSource.Formulas.GetEnumerator()
                                While iformula.MoveNext()
                                    If Not repClassVal.DataDefinition.FormulaFields(iformula.Key) Is Nothing Then
                                        repClassVal.DataDefinition.FormulaFields(iformula.Key).text = iformula.Value
                                    End If
                                End While
                            End If
                        End If
                    End If
                End If

            Catch ex As Exception
                Errorlog("rptViewer-init", ex.Message)
            End Try

            If Session("PrintClicked") IsNot Nothing And Session("PrintClicked") = True Then
                CheckForDuplicate()
            End If
            Try
                Dim btnClick As Object = PrinterFunctions.GetPostBackControl(Me.Page)
                If btnClick IsNot Nothing Then
                    Dim btnClickstatus As String = btnClick.CommandName
                    If btnClickstatus = "Export" Or btnClickstatus = "Print" Then
                        'IncreamentPrinterCount()
                    Else
                        Session("PrintClicked") = False
                    End If
                End If
            Catch ex As Exception
                Session("PrintClicked") = False
            End Try
            repSource = Nothing
            'crv_Unload(Nothing, Nothing)
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim isExport = IIf(Request.QueryString("isExport") Is Nothing, False, Request.QueryString("isExport"))
            If isExport Then
                ExportReportToExcel()
            End If
        End If
        If Cache.Item("reportDS" & ViewState("h_uniqueID")) Is Nothing Then
            ' Cache.Remove("reportDS" & ViewState("h_uniqueID"))
            ViewState("h_uniqueID") = Session.SessionID & rnd.Next()
            Session("TempReportSource" & ViewState("h_uniqueID")) = repSource
        End If

    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table

            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

            Next

        Catch ex As Exception

            UtilityObj.Errorlog("final--" & (ex.Message))
        End Try

    End Sub

    Private Sub UpdatePassword(ByVal dt As DataTable)
        For Each dtRow As DataRow In dt.Rows
            dtRow("PASSWORD") = Encr_decrData.Decrypt(dtRow("OLU_PASSWORD"))
        Next

    End Sub


    Private Sub ExportReportToExcel()
        Dim ds As New DataSet
        repSource = DirectCast(Session("ReportSource"), MyReportClass)
        Dim objConn As SqlConnection = repSource.Command.Connection
        Try
            'repSource.Command.Connection = objConn
            objConn.Close()
            objConn.Open()
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = repSource.Command
            'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey
            adpt.SelectCommand.CommandTimeout = 0
            ds.Clear()
            adpt.Fill(ds)
            If (Not ds Is Nothing) AndAlso (Not ds.Tables(0).Rows.Count <= 0) Then
                Dim strFName As String = String.Empty

                If repSource.VoucherName <> "" Then
                    strFName = repSource.VoucherName
                Else
                    If Not repSource.Parameter Is Nothing Then
                        strFName = repSource.Parameter("VoucherName")
                    End If
                End If
                If strFName = "" Then
                    strFName = "report" + Session("sUsr_name") + DateTime.Now.ToFileTime.ToString()
                End If

                DataSetToExcel.Convert(ds, Response, Server.MapPath("ExcelFormat.xsl"), strFName)
                'DataSetToExcel.Convert(ds, Response)
                'Dim strDirPath As String = Server.MapPath("..\..\Temp")
                'Dim strFName As String = String.Empty

                'If repSource.VoucherName <> "" Then
                '    strFName = repSource.VoucherName
                'Else
                '    If Not repSource.Parameter Is Nothing Then
                '        strFName = repSource.Parameter("VoucherName")
                '    End If
                'End If
                'If strFName = "" Then
                '    strFName = "report" + Session("sUsr_name") + DateTime.Now.ToFileTime.ToString()
                'End If
                'Dim strFileName As String = DataSetToExcel.Convert(ds, strDirPath, strFName)
                'Response.Clear()
                'Response.Charset = ""
                ''set the response mime type for excel
                'Response.ContentType = "application/vnd.ms-excel"
                'Response.AddHeader("Content-Disposition", "attachment; filename=" & strFName & ".xls")
                'Dim fStream As New System.IO.FileStream(strFileName, IO.FileMode.Open, IO.FileAccess.Read)
                ''     Dim bw As New System.IO.BinaryReader(fStream)
                'Dim byt As Byte() = New Byte(fStream.Length - 1) {}
                'fStream.Read(byt, 0, fStream.Length - 1)
                ''    byt = bw.ReadBytes(CInt(fStream.Length - 1))
                'fStream.Close()
                ''bw.Close()
                'Response.BinaryWrite(byt)
                ''Response.End()
                ''Response.Flush()

                ''Response.WriteFile(strFileName)

                'System.IO.File.Delete(strFileName)

            Else
                If Request.UrlReferrer <> Nothing AndAlso Request.UrlReferrer.ToString() <> "" Then
                    Dim urlstring As String = IIf(Request.UrlReferrer.ToString().Contains("?"), "&nodata=true", "?nodata=true")
                    Response.Redirect(Request.UrlReferrer.ToString() & urlstring)
                End If
            End If
        Catch ex As Exception


        Finally
            objConn.Close()
        End Try

    End Sub

    Private Sub IncreamentPrinterCount()
        If Session("PrintUpdate") IsNot Nothing Then
            Dim docType As String = Session("PrintDocType")
            Dim docNo As String = Session("PrintDocNo")
            Dim str_lock As String = lock()
            If str_lock <> "" Then
                Dim str_sql As String = "update VOUCHER_H set VHH_Count = isnull(VHH_Count,0) + 1 where VHH_BSU_ID='" & Session("sBsuid") & _
                "' and VHH_FYEAR = " & Session("F_YEAR") & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & docNo & "'"
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim i As Integer = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_sql)
                If i < 1 Then
                    UtilityObj.Errorlog("Error in updating Print count")
                End If
                unlock()
                Session("PrintClicked") = True
            Else
                UtilityObj.Errorlog("Error in updating Print count")
            End If
        End If
    End Sub
    Private Sub CheckForDuplicate()
        If Session("PrintUpdate") IsNot Nothing Then
            repSource = DirectCast(Session("TempReportSource" & ViewState("h_uniqueID")), MyReportClass)
            repSource.Parameter.Item("Duplicated") = False
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_sql As String = "Select VHH_Count FROM VOUCHER_H where VHH_BSU_ID='" & Session("sBsuid") & _
            "' and VHH_FYEAR = " & Session("F_YEAR") & " and VHH_DOCTYPE = '" & Session("PrintDocType") & "' and VHH_DOCNO = '" & Session("PrintDocNo") & "'"
            str_conn = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_sql)
            repSource.Parameter.Item("Duplicated") = False
            If Not TypeOf count Is DBNull Then
                If count > 0 Then
                    repSource.Parameter.Item("Duplicated") = True
                End If
            End If
            Session("TempReportSource" & ViewState("h_uniqueID")) = repSource
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub SubReport(ByVal subRep As MyReportClass, ByRef repClassVal As RepClass)
        If subRep.SubReport IsNot Nothing Then
            Dim ii As Integer = 0
            If subRep.IncludeBSUImage Then
                ii = 1
            End If
            For i As Integer = 0 To subRep.SubReport.Length - 1
                Dim myrep As MyReportClass = subRep.SubReport(i)
                If myrep IsNot Nothing Then
                    If myrep.SubReport IsNot Nothing Then
                        SubReport(subRep, repClassVal)
                    Else
                        Dim objConn As SqlConnection = repSource.Command.Connection
                        'objConn.Close()
                        objConn.Open()
                        Dim ds As New DataSet
                        Dim adpt As New SqlDataAdapter
                        Try
                            adpt.SelectCommand = myrep.Command
                            adpt.Fill(ds)
                        Catch ex As Exception
                            UtilityObj.Errorlog(ex.Message, "Report Viewer(SUB_FIN)")
                            Exit Sub
                        Finally
                            objConn.Close()
                        End Try
                        If String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Transport.rpt", True) <> 0 Or _
                       String.Compare(repClassVal.Subreports(i + ii).Name, "ReportHeader_SubReport_Payroll.rpt", True) <> 0 Or _
                       String.Compare(repClassVal.Subreports(i + ii).Name, "ReportHeader_SubReport.rpt", True) <> 0 Or _
                         String.Compare(repClassVal.Subreports(i + ii).Name, "ReportHeader_SubReport_Curr.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Portrait.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "ReportHeader_Sub_Landscape.rpt", True) <> 0 Then
                            repClassVal.Subreports(i + ii).SetDataSource(ds.Tables(0))
                        Else
                            ViewState("reportHeader") = repClassVal.Subreports(i + ii).Name
                        End If
                    End If
                End If
            Next
        End If
    End Sub


    Private Sub IncludeBSUmage(ByRef repClassVal As RepClass, Optional ByVal BSUID As String = "")
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(lstrConn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        'Dim cmd As New SqlCommand("getBsuInFoWithImage", objConn)
        Dim cmd As New SqlCommand("ReportHeader_Subreport", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpIMG_BSU_ID As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 100)
        If BSUID = "" Then
            sqlpIMG_BSU_ID.Value = Session("sbSUID")
        Else
            sqlpIMG_BSU_ID.Value = BSUID
        End If

        cmd.Parameters.Add(sqlpIMG_BSU_ID)

        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmd.Parameters.Add(sqlpIMG_TYPE)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        objConn.Close()
        If Not repClassVal.Subreports("rptHeader.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("ReportHeader_Sub_Landscape.rpt") Is Nothing Then
            repClassVal.Subreports("ReportHeader_Sub_Landscape.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Transport.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Transport.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Portrait.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Portrait.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("ReportHeader_SubReport_Payroll.rpt") Is Nothing Then
            repClassVal.Subreports("ReportHeader_SubReport_Payroll.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("ReportHeader_SubReport.rpt") Is Nothing Then
            repClassVal.Subreports("ReportHeader_SubReport.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("ReportHeader_SubReport_Curr.rpt") Is Nothing Then
            repClassVal.Subreports("ReportHeader_SubReport_Curr.rpt").SetDataSource(ds.Tables(0))
        End If
        'repClassVal.Subreports("bsuimage").SetDataSource(dtDt)

        'repClassVal.Subreports("BSUIMAGE").SetDataSource(dtDt)
    End Sub

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")
    '    smScriptManager.EnablePartialRendering = False
    '    'If Session("sModule") = "SS" Then
    '    '    Me.MasterPageFile = "../../mainMasterPageSS.master"
    '    'Else
    '    '    Me.MasterPageFile = "../../mainMasterPage.master"
    '    'End If
    'End Sub


    'Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload

    '    'CrystalReportViewer1.Dispose()
    '    'CrystalReportViewer1 = Nothing
    '    If Not repClassVal Is Nothing Then
    '        repClassVal.Close()
    '        repClassVal.Dispose()
    '    End If
    '    'GC.Collect()

    'End Sub
    Protected Sub crv_Unload(sender As Object, e As EventArgs) Handles CrystalReportViewer1.Unload
        Try
            Try
                For Each table As Table In repClassVal.Database.Tables
                    table.Dispose()
                Next
                repClassVal.Database.Dispose()
                Cache.Remove("reportDS" & ViewState("h_uniqueID"))
                Session("TempReportSource" & ViewState("h_uniqueID")) = Nothing
            Catch ex As Exception
                UtilityObj.Errorlog("crv_Unload--", ex.Message)
            End Try
            
        Catch ex As Exception

        Finally
            repClassVal.Close()
            repClassVal.Dispose()
            CrystalReportViewer1.Dispose()
        End Try
    End Sub
    Private Function lock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim docNo As String = Session("PrintDocNo")
            Dim docType As String = Session("PrintDocType")
            Dim subID As String = Session("PrintSUBID")
            Dim objConn As New SqlConnection(str_conn)
            Try
                objConn.Open()
                Dim cmd As New SqlCommand("LockVOUCHER_H", objConn)
                cmd.CommandType = CommandType.StoredProcedure


                Dim sqlpJHD_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                sqlpJHD_SUB_ID.Value = subID
                cmd.Parameters.Add(sqlpJHD_SUB_ID)

                Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpJHD_BSU_ID.Value = Session("sBsuid") & ""
                cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                sqlpJHD_FYEAR.Value = Session("F_YEAR") & ""
                cmd.Parameters.Add(sqlpJHD_FYEAR)

                Dim sqlpJHD_DOCTYPE As New SqlParameter("@VHH_DOCTYPE", SqlDbType.VarChar, 20)
                sqlpJHD_DOCTYPE.Value = docType
                cmd.Parameters.Add(sqlpJHD_DOCTYPE)

                Dim sqlpJHD_DOCNO As New SqlParameter("@VHH_DOCNO", SqlDbType.VarChar, 20)
                sqlpJHD_DOCNO.Value = docNo
                cmd.Parameters.Add(sqlpJHD_DOCNO)

                Dim sqlpJHD_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                sqlpJHD_CUR_ID.Value = Session.SessionID
                cmd.Parameters.Add(sqlpJHD_CUR_ID)

                Dim sqlpJHD_USER As New SqlParameter("@VHH_USER", SqlDbType.VarChar, 50)
                sqlpJHD_USER.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpJHD_USER)

                Dim sqlopJHD_TIMESTAMP As New SqlParameter("@VHH_TIMESTAMP", SqlDbType.Timestamp, 8)
                cmd.Parameters.Add(sqlopJHD_TIMESTAMP)
                cmd.Parameters("@VHH_TIMESTAMP").Direction = ParameterDirection.Output

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()
                Session("str_timestamp") = sqlopJHD_TIMESTAMP.Value
                iReturnvalue = retValParam.Value
                If iReturnvalue <> 0 Then
                    Throw New Exception("")
                End If
                Return iReturnvalue
            Catch ex As Exception

            Finally
                objConn.Close()
            End Try
            '''''''
            Return " | | "
        Catch ex As Exception
            Return " | | "
        End Try
        Return True
    End Function

    Private Function unlock() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            Dim objConn As New SqlConnection(str_conn)

            Dim docNo As String = Session("PrintDocNo")
            Dim docType As String = Session("PrintDocType")
            Dim subID As String = Session("PrintSUBID")

            Try
                objConn.Open()
                Dim cmd As New SqlCommand("ClearAllLocks", objConn)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpVHH_SUB_ID As New SqlParameter("@JHD_SUB_ID", SqlDbType.VarChar, 20)
                sqlpVHH_SUB_ID.Value = subID
                cmd.Parameters.Add(sqlpVHH_SUB_ID)

                Dim sqlpsqlpBSUID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
                sqlpsqlpBSUID.Value = Session("sBSUId")
                cmd.Parameters.Add(sqlpsqlpBSUID)

                Dim sqlpVHH_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
                sqlpVHH_FYEAR.Value = Session("F_YEAR")
                cmd.Parameters.Add(sqlpVHH_FYEAR)

                Dim sqlpVHH_DOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                sqlpVHH_DOCTYPE.Value = docType
                cmd.Parameters.Add(sqlpVHH_DOCTYPE)

                Dim sqlpVHH_DOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                sqlpVHH_DOCNO.Value = docNo
                cmd.Parameters.Add(sqlpVHH_DOCNO)

                Dim sqlpVHH_CUR_ID As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
                sqlpVHH_CUR_ID.Value = Session.SessionID
                cmd.Parameters.Add(sqlpVHH_CUR_ID)

                Dim sqlpVHH_USER As New SqlParameter("@JHD_USER", SqlDbType.VarChar, 50)
                sqlpVHH_USER.Value = Session("sUsr_name")
                cmd.Parameters.Add(sqlpVHH_USER)

                Dim sqlopVHH_TIMESTAMP As New SqlParameter("@JHD_TIMESTAMP", SqlDbType.Timestamp, 8)
                sqlopVHH_TIMESTAMP.Value = Session("str_timestamp")
                cmd.Parameters.Add(sqlopVHH_TIMESTAMP)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()

                iReturnvalue = retValParam.Value
                If iReturnvalue <> 0 Then
                    Throw New Exception(iReturnvalue)
                End If
                Return iReturnvalue
            Catch ex As Exception
                'handle error
            Finally
                objConn.Close()
            End Try
            Return " | | "
        Catch ex As Exception
            'handle error
            Return " | | "
        End Try
        Return True
    End Function

End Class
