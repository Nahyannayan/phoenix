<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="CostcenterDepartment.aspx.vb" Inherits="Reports_ASPX_Report_CostcenterDepartment"  %>
<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript" >
     function getFilter(NameObj,IdObj,frm)
   {
  
  var bsuId  = '<%=Session("sBsuid")%>'
    var sFeatures;
            sFeatures="dialogWidth: 600px; ";
            sFeatures+="dialogHeight: 650px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
             //result = window.showModalDialog("ReportsFilter.aspx?FROM="+frm+"&BSUID="+bsuId,"", sFeatures)
             result = window.showModalDialog("../../Accounts/ShowDepartmentMulti.aspx","", sFeatures)
        
            if (result=='' || result==undefined)
            {
            return false;
            }
           
           NameandCode = result.split('__');  
           
           document.getElementById(IdObj).value=NameandCode[0]; 
           document.getElementById(NameObj).value=NameandCode[1]; 
           return true;
            
   }
   
   function ChkBox(ObjTwo,ObjThree)
   {
   document.getElementById(ObjTwo).checked = false;
   document.getElementById(ObjThree).checked = false;
   }
       
    </script>
      
    <table align="center"class="BlueTable" cellpadding="5" cellspacing="0"
        style="width: 70%;">
        <tr class="subheader_img">
            <td align="left" colspan="8" style="height: 19px" valign="middle">            
            <asp:Label ID="lblCaption" runat="server" Text="Cost Center Analysis by Department"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" class="matters">From Date</td>
            <td class="matters">:</td>
            <td align="left" class="matters" colspan="2" style="width: 189px; height: 1px; text-align: left">
                <asp:TextBox ID="txtFromDate" runat="server" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
            <td align="left" class="matters" style="color: #1b80b6; height: 1px">
                To Date</td>
            <td align="left" class="matters">
                :</td>
            <td align="left" class="matters" colspan="2" style="height: 1px; text-align: left">
                <asp:TextBox ID="txtToDate" runat="server" Width="122px"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr runat="server" style="color: #1b80b6">
            <td align="left" class="matters" valign="top">
                Business Unit</td>
            <td class="matters" valign="top">
                :</td>
            <td align="left" class="matters" colspan="6" valign="middle"><span class ="error" ><u>
                <asp:Label id="labBsunit" runat="server" CssClass="error" style="cursor: hand" Text="Select Business Unit"
                    ToolTip="Click for Business Unit"></asp:Label></u></span></td>
        </tr>
        <tr id="tr_CostCenter" runat="server" >
            <td align="left" class="matters" >
                Department</td>
            <td class="matters">
                :</td>
            <td align="left" class="matters" colspan="6" >
                <asp:TextBox id="txtDepartment" runat="server" Width="292px" OnTextChanged="txtDepartment_TextChanged" AutoPostBack="True"></asp:TextBox>
            <asp:ImageButton ID="imgPickDpt" runat="server" ImageUrl="~/Images/forum_search.gif" /><br />
                <asp:GridView id="gvDptdetails" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    SkinID="GridViewPickDetails" Width="96%" 
                    OnPageIndexChanging="gvDptdetails_PageIndexChanging"                     >
                    <columns>
<asp:TemplateField HeaderText="Department"><ItemTemplate>
                                <asp:Label ID="lblStudentName" runat="server" Text='<%# bind("DPT_DESCR") %>'></asp:Label>
                            
</ItemTemplate>
</asp:TemplateField>
</columns>
                </asp:GridView></td>
        </tr>
        <tr runat="server">
            <td align="left" class="matters">
            </td>
            <td class="matters">
            </td>
            <td align="left" class="matters" colspan="6">
                <asp:CheckBox id="ChkDetails" runat="server" Text="Details">
                </asp:CheckBox>&nbsp;
                <asp:CheckBox id="ChkMonthly" runat="server" Text="Monthly">
                </asp:CheckBox>&nbsp;
                <asp:CheckBox id="ChkMonthAc" runat="server" Text="Monthly Accountwise">
                </asp:CheckBox></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="text-align: right; height: 33px;">
                &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table> <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
   <asp:HiddenField ID="h_BSUID" runat="server" /><asp:HiddenField ID="h_Mode" runat="server" />
    <asp:HiddenField ID="h_Departmentid" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    
    <table ><tr> 
       <td align="left"  >
     
         <asp:Panel ID="Panel2" runat="server" CssClass ="matters" >
        
        <table align ="left" class ="BlueTable" >
        
        <tr align ="left"   >
        
        <td style="background-color: lemonchiffon; border:1px solid #646464;color: red;width:150px  ">
            
        <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                  
                
       </td></tr>
       
       </table>
        
    </asp:Panel>
               </td></tr>
</table>
    <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" runat="server" CommitProperty="value"
        CommitScript="" PopupControlID="Panel2" Position="Bottom" TargetControlID="labBsunit">
    </ajaxToolkit:PopupControlExtender>
    <br />
</asp:Content>

