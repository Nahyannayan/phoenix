﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptLastVoucher.aspx.vb" Inherits="Reports_ASPX_Report_rptLastVoucher" %>

<%@ Register src="../../UserControls/usrBSUnits.ascx" tagname="usrBSUnits" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
</script> 
<script type="text/javascript" language="javascript">
      
  
    </script>                                
 
    <table align="center" border="0" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="98%">
            <tr valign="top">
                <td class="matters" valign="top" align="left" colspan="2">
                    -<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label> 
                    </td> 
            </tr> 
        </table>  
   
          <table align="center" bordercolor="#1B80B6" border="1" cellpadding="5" cellspacing="0" style="width: 80%; border-collapse:collapse;">
    
            <tr class="subheader_BlueTableView" valign="top" >
                <th align="left" colspan="2" valign="middle" style="height: 19px">
                    <asp:Label ID="lblHead" runat="server"></asp:Label></th>
            </tr>
            <tr valign="top">
            <td align="left" valign="middle" class="matters" >
                    Select Business Unit</td>
                        <td align="left" class="matters" valign="middle">
                        <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                </td>
        </tr>
        <tr>
            <td align="left" class="matters" >
                Date</td>
            <td class="matters" align="left" >
            <asp:TextBox ID="txtToDate" runat="server" Width="122px"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>&nbsp;
                <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
             
        </tr>
        <tr>
            <td align="center" class="matters" colspan="2" > 
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="dayBook" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
            </td>
        </tr>
    </table> 
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    </asp:Content>
