
Imports System.IO

Partial Class Payroll_empOrganizationChartView
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                If Not Session("PrintCtrl") Is Nothing Then
                    Me.Title = Session("PrintTitle")
                    PrintWebControl(Session("PrintCtrl"))
                End If
            End If
        Catch ex As Exception
        Finally
            '  Response.Write("window.close();")
        End Try
    End Sub
    Public Shared Sub PrintWebControl(ByVal ControlToPrint As Control)
        Dim stringWrite As New StringWriter()
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        If TypeOf ControlToPrint Is WebControl Then
            Dim w As New Unit(100, UnitType.Percentage)
            DirectCast(ControlToPrint, WebControl).Width = w
        End If
        Dim pg As New Page()
        pg.EnableEventValidation = False
        Dim frm As New HtmlForm()
        pg.Controls.Add(frm)
        frm.Attributes.Add("runat", "server")
        frm.Controls.Add(ControlToPrint)
        pg.DesignerInitialize()
        pg.RenderControl(htmlWrite)
        Dim strHTML As String = stringWrite.ToString()
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.Write(strHTML)
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session("PrintCtrl") = Nothing
    End Sub
End Class
