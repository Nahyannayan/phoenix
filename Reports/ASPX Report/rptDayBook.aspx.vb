Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_rptDayBook
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
         
        Page.Title = OASISConstants.Gemstitle
        lblError.Text = ""
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If

        'Add the readOnly attribute to the Date textBox So that the Text property value is accessible in the form
        'It is not giving the values if you make the ReadOnly property to TRUE
        'So you have to do this in page_Load()
        'txtToDate.Attributes.Add("ReadOnly", "Readonly")
        'txtFromDate.Attributes.Add("ReadOnly", "Readonly")

        'checks whether the BSUnit is selected if it is selected h_BSUID.Value will be having
        'the IDs of the BS Unit. "FillBSUNames()" function will set the BSU names
        If h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
            h_BSUID.Value = Session("sBsuid")
        End If
        FillBSUNames(h_BSUID.Value)
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        UsrBSUnits1.MenuCode = MainMnu_code
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            lblError.Text = ""
        End If
        If Not IsPostBack Then
            UtilityObj.NoOpen(Me.Header)
            txtToDate.Text = UtilityObj.GetDiplayDate()
            txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))

            Select Case MainMnu_code
                Case "A250005"
                    lblRptCaption.Text = "Day Book Report"
                    'Bind the Data with the DropDown
                    cmbDocType.DataSource = GetDataSourceDocType()
                    cmbDocType.DataValueField = "DOC_ID"
                    cmbDocType.DataTextField = "DOC_NAME"
                    cmbDocType.DataBind()
                    cmbDocType.SelectedValue = "ALL"
                Case "A250031"
                    lblRptCaption.Text = "Interunit Journal Voucher"
                    trDocType.Visible = False
                Case "A250095"
                    lblRptCaption.Text = "Disbursement Account"
                    trDocType.Visible = False
            End Select
            txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
            txtToDate.Attributes.Add("onBlur", "checkdate(this)")
        End If
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
        str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        'grdBSU.DataSource = ds
        'grdBSU.DataBind()
        Return True
    End Function
    Private Function GetDataSourceDocType() As DataTable
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            Dim dsDoc As New DataSet
            str_Sql = "SELECT distinct DOC_ID, DOC_NAME FROM DOCUMENT_M"
            dsDoc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If dsDoc Is Nothing Then
                Throw New Exception("DsDoc is null Throws Error in DayBook.cs function GetDataSourceDocType()")
            ElseIf dsDoc.Tables.Count > 0 Then
                Dim row As DataRow
                row = dsDoc.Tables(0).NewRow()
                row(0) = "ALL"
                row(1) = "ALL"
                dsDoc.Tables(0).Rows.Add(row)
                Return dsDoc.Tables(0)
            Else
                Throw New Exception("DsDoc.Table is less than 0 Throws Error in DayBook.cs function GetDataSourceDocType()")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return Nothing
        End Try
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        txtFromDate.Text = Format("dd/MMM/yyyy", txtFromDate.Text)
        txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
        Dim strXMLBSUNames As String
        'Generate the XML in the form <BSU_DETAILS><BSU_ID>IDhere</BSU_ID></BSU_DETAILS>
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        strXMLBSUNames = GenerateBSUXML(h_BSUID.Value) 'txtBSUNames.Text)
        Session("FDate") = txtFromDate.Text
        Session("TDate") = txtToDate.Text
        Session("selBSUID") = h_BSUID.Value
        Session("DocType") = cmbDocType.SelectedValue
        Select Case MainMnu_code
            Case "A250005"
                GenerateDayBookReport(strXMLBSUNames)
            Case "A250031"
                GenerateIJVSummaryReport(h_BSUID.Value)
            Case "A250095"
                GenerateDisbursementAccount()
        End Select

    End Sub

    Private Sub GenerateDisbursementAccount()
        Dim fromDate, toDate As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        Dim strFilter As String = String.Empty
        If (txtFromDate.Text <> "") And (txtFromDate.Text <> String.Empty) Then
            fromDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", txtFromDate.Text)
        Else
            fromDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", DateTime.MinValue)
        End If
        If (txtToDate.Text <> "") And (txtToDate.Text <> String.Empty) Then
            toDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", txtToDate.Text)
        Else
            toDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", DateTime.Now)
        End If

        'SELECT * FROM vw_OSA_JOURNAL where FYEAR, DOCTYPE, DOCNO, DOCDT
        strFilter = "AND VHH_DOCDT between '" & fromDate & "' and '" & toDate & "'"
        strFilter += " AND VHH_BSU_ID in ("
        Dim comma As String = String.Empty
        Dim strArrayBSUID As String() = h_BSUID.Value.Split("||")
        Dim strBSUID As String = String.Empty
        For count As Integer = 0 To strArrayBSUID.Length - 1
            'If count <> 0 And Not strBSUID.EndsWith(", ") Then
            '    strBSUID += ", "
            'End If
            If strArrayBSUID(count) <> "" Then
                strBSUID += comma
                strBSUID += "'" & strArrayBSUID(count) & "'"
            End If
            comma = ","
        Next
        strFilter += strBSUID + ")"
        str_Sql = "SELECT * FROM vw_OSA_VOUCHER where VHH_TYPE = 'P' AND VHH_bPosted = 'true' " + strFilter
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.Text

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            'params("Month") = "January"
            'params("reportHeading") = "Disbursement Account"
            params("fromDate") = txtFromDate.Text
            params("toDate") = txtToDate.Text
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.VoucherName = "Disbursement Account"
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/RPTDISBURSEMENT.rpt"
            Session("ReportSource") = repSource
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                Response.Redirect("rptviewer.aspx", True)
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
    End Sub

    Private Sub GenerateIJVSummaryReport(ByVal strBSUIDs As String)
        Dim fromDate, toDate As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        Dim strFilter As String = String.Empty
        If (txtFromDate.Text <> "") And (txtFromDate.Text <> String.Empty) Then
            fromDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", txtFromDate.Text)
        Else
            fromDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", DateTime.MinValue)
        End If
        If (txtToDate.Text <> "") And (txtToDate.Text <> String.Empty) Then
            toDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", txtToDate.Text)
        Else
            toDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", DateTime.Now)
        End If

        'SELECT * FROM vw_OSA_JOURNAL where FYEAR, DOCTYPE, DOCNO, DOCDT
        strFilter = " WHERE IJOURNAL_H.IJH_DOCDT between '" & fromDate & "' and '" & toDate & "'"
        strFilter += " AND isnull(IJH_bDeleted,0)=0 and  (IJOURNAL_H.IJH_CR_BSU_ID in ("

        Dim comma As String = String.Empty
        Dim strArrayBSUID As String() = h_BSUID.Value.Split("||")
        Dim strBSUID As String = String.Empty
        For count As Integer = 0 To strArrayBSUID.Length - 1
            'If count <> 0 And Not strBSUID.EndsWith(", ") Then
            '    strBSUID += ", "
            'End If
            If strArrayBSUID(count) <> "" Then
                strBSUID += comma
                strBSUID += "'" + strArrayBSUID(count) + "'"
            End If
            comma = ","
        Next
        strFilter += strBSUID + ") or IJOURNAL_H.IJH_DR_BSU_ID in (" + strBSUID + "))"

        str_Sql = "SELECT     dbo.IJOURNAL_H.IJH_DOCNO, dbo.IJOURNAL_H.IJH_DOCDT, dbo.CURRENCY_M.CUR_DESCR, dbo.IJOURNAL_H.IJH_EXGRATE2, " & _
                              " dbo.IJOURNAL_H.IJH_EXGRATE1, vw_OSO_BUSINESSUNIT_M_1.BSU_NAME AS DR_BSU_NAME, " & _
                      " dbo.vw_OSO_BUSINESSUNIT_M.BSU_NAME AS CR_BSU_NAME, dbo.IJOURNAL_H.IJH_NARRATION, dbo.IJOURNAL_H.IJH_CR_BSU_ID, " & _
                      " dbo.IJOURNAL_H.IJH_bMIRROR, dbo.IJOURNAL_H.IJH_DIFFAMT, dbo.IJOURNAL_H.IJH_DR_BSU_ID, SUM(dbo.IJOURNAL_D.IJL_DEBIT) AS DEBIT_TOTAL, " & _
                      " SUM(dbo.IJOURNAL_D.IJL_CREDIT) AS CREDIT_TOTAL FROM dbo.IJOURNAL_H INNER JOIN" & _
                      " dbo.IJOURNAL_D ON dbo.IJOURNAL_H.IJH_DOCTYPE = dbo.IJOURNAL_D.IJL_DOCTYPE AND " & _
                      " dbo.IJOURNAL_H.IJH_FYEAR = dbo.IJOURNAL_D.IJL_FYEAR AND dbo.IJOURNAL_H.IJH_DOCNO = dbo.IJOURNAL_D.IJL_DOCNO LEFT OUTER JOIN" & _
                      " dbo.CURRENCY_M ON dbo.IJOURNAL_H.IJH_CUR_ID = dbo.CURRENCY_M.CUR_ID LEFT OUTER JOIN" & _
                      " dbo.vw_OSO_BUSINESSUNIT_M AS vw_OSO_BUSINESSUNIT_M_1 ON" & _
                      " dbo.IJOURNAL_H.IJH_DR_BSU_ID = vw_OSO_BUSINESSUNIT_M_1.BSU_ID LEFT OUTER JOIN" & _
                      " dbo.vw_OSO_BUSINESSUNIT_M ON dbo.IJOURNAL_H.IJH_CR_BSU_ID = dbo.vw_OSO_BUSINESSUNIT_M.BSU_ID"

        'str_Sql = "SELECT dbo.IJOURNAL_H.IJH_DOCNO, dbo.IJOURNAL_H.IJH_DOCDT, dbo.CURRENCY_M.CUR_DESCR, dbo.IJOURNAL_H.IJH_EXGRATE2, " & _
        '"dbo.IJOURNAL_H.IJH_EXGRATE1, vw_OSO_BUSINESSUNIT_M_1.BSU_NAME AS DR_BSU_NAME, " & _
        '"dbo.vw_OSO_BUSINESSUNIT_M.BSU_NAME AS CR_BSU_NAME, dbo.IJOURNAL_H.IJH_NARRATION, dbo.IJOURNAL_H.IJH_CR_BSU_ID, " & _
        '"dbo.IJOURNAL_H.IJH_bMIRROR, dbo.IJOURNAL_H.IJH_DIFFAMT, dbo.IJOURNAL_H.IJH_DR_BSU_ID " & _
        '"FROM         dbo.IJOURNAL_H LEFT OUTER JOIN" & _
        '" dbo.CURRENCY_M ON dbo.IJOURNAL_H.IJH_CUR_ID = dbo.CURRENCY_M.CUR_ID LEFT OUTER JOIN " & _
        '"dbo.vw_OSO_BUSINESSUNIT_M AS vw_OSO_BUSINESSUNIT_M_1 ON " & _
        '"dbo.IJOURNAL_H.IJH_DR_BSU_ID = vw_OSO_BUSINESSUNIT_M_1.BSU_ID LEFT OUTER JOIN " & _
        '"dbo.vw_OSO_BUSINESSUNIT_M ON dbo.IJOURNAL_H.IJH_CR_BSU_ID = dbo.vw_OSO_BUSINESSUNIT_M.BSU_ID"
        str_Sql += strFilter

        str_Sql += "GROUP BY dbo.IJOURNAL_H.IJH_DOCNO, dbo.IJOURNAL_H.IJH_DOCDT, dbo.CURRENCY_M.CUR_DESCR, dbo.IJOURNAL_H.IJH_EXGRATE2, " & _
        " dbo.IJOURNAL_H.IJH_EXGRATE1, vw_OSO_BUSINESSUNIT_M_1.BSU_NAME, dbo.vw_OSO_BUSINESSUNIT_M.BSU_NAME, " & _
        " dbo.IJOURNAL_H.IJH_NARRATION, dbo.IJOURNAL_H.IJH_CR_BSU_ID, dbo.IJOURNAL_H.IJH_bMIRROR, dbo.IJOURNAL_H.IJH_DIFFAMT, " & _
        "dbo.IJOURNAL_H.IJH_DR_BSU_ID"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.Text

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("VoucherName") = "InterUnit Journal Voucher Summary"
            params("reportHeading") = "InterUnit Journal Voucher Summary"
            params("fromDate") = Format("dd/MMM/yyyy", CDate(fromDate))
            params("toDate") = Format("dd/MMM/yyyy", CDate(toDate))
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.VoucherName = "InterUnit Journal Voucher Summary"
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptIJVSummaryReport.rpt"
            Session("ReportSource") = repSource
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                Response.Redirect("rptviewer.aspx", True)
            End If
        End If

    End Sub

    Private Sub GenerateDayBookReport(ByVal strXMLBSUNames As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        'Dim adpt As New SqlDataAdapter
        'Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTJOURNALLIST", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@JHD_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        sqlpJHD_FYEAR.Value = Session("F_YEAR")
        cmd.Parameters.Add(sqlpJHD_FYEAR)

        Dim sqlpJHD_DOCTYPE As New SqlParameter("@JHD_DOCTYPE", SqlDbType.VarChar, 20)
        If cmbDocType.SelectedValue = "ALL" Then
            sqlpJHD_DOCTYPE.Value = String.Empty
        Else
            sqlpJHD_DOCTYPE.Value = cmbDocType.SelectedItem.Value
        End If
        cmd.Parameters.Add(sqlpJHD_DOCTYPE)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@TODOCDT", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        'adpt.SelectCommand = cmd
        'objConn.Open()
        'adpt.Fill(ds)
        objConn.Close()
        objConn.Open()
        'Dim retType As Object = cmd.ExecuteScalar()
        'If retType IsNot Nothing Then
        If 1 = 1 Then
            'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("UserName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptDayBook.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                Response.Redirect("rptviewer.aspx", True)
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub
    'Generates the XML for BSUnit
    Private Function GenerateBSUXML(ByVal BSUIDs As String) As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Try
            BSUDetails = xmlDoc.CreateElement("BSU_DETAILS")
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement("BSU_DETAIL")
                XMLEBSUID = xmlDoc.CreateElement("BSU_ID")
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    'Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
    '    h_BSUID.Value += "||" + txtBSUNames.Text.Replace(",", "||")
    '    FillBSUNames(h_BSUID.Value)
    'End Sub

    'Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
    '    grdBSU.PageIndex = e.NewPageIndex
    '    FillBSUNames(h_BSUID.Value)
    'End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = Session("sBSUID")
            End If
            'grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click

    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            txtFromDate.Text = Session("FDate")
            txtToDate.Text = Session("TDate")
            UsrBSUnits1.SetSelectedNodes(Session("selBSUID"))
            cmbDocType.SelectedValue = Session("DocType")
        Else
            lblError.Text = ""
        End If
    End Sub
End Class
