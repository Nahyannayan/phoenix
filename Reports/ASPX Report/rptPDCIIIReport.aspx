<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPDCIIIReport.aspx.vb" Inherits="Reports_ASPX_Report_rptPDCIIIReport" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function SetControls(src) { //alert(src);
            if (src.checked == true) {
                document.getElementById('td_fromdate').style.visibility = 'hidden';
                document.getElementById('td_fromdatecaption').style.visibility = 'hidden';
            }
            else {
                document.getElementById('td_fromdate').style.display = 'table-cell';
                document.getElementById('td_fromdatecaption').style.display = 'table-cell';
                document.getElementById('td_fromdate').style.visibility = 'visible';
                document.getElementById('td_fromdatecaption').style.visibility = 'visible';
            }
        }
        function GetAccounts() {
            var sFeatures;
            sFeatures = "dialogWidth: 820px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var ActType;

            //result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?multiSelect=true&forrpt=1", "", sFeatures)
            result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&forrpt=1", "pop_up2")
           <%-- if (result != '' && result != undefined) {
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_ACTIDs.ClientID%>').value = arg.NameandCode;
                document.forms[0].submit();

                //__doPostBack('<%= h_ACTIDs.ClientID%>', 'ValueChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">

                    <tr>
                        <td align="left" id="td_fromdatecaption" width="20%"><span class="field-label">Date From</span>
                        </td>
                        <td align="left" id="td_fromdate" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Date To </span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtTodate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImgTo" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" /><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFromDate"
                                ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                        </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                            <td align="left" colspan="3">
                                <div class="checkbox-list">
                                    <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"><span class="field-label">Account</span></td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the ACT ID you want to Add to the Search and click on Add)"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtBankNames" runat="server"></asp:TextBox>
                                <asp:LinkButton ID="lblAddActID" runat="server">Add</asp:LinkButton>
                                <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetAccounts(); return false;" /><br />
                                <asp:GridView ID="grdACTDetails" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ACT ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ACT_Name" HeaderText="ACT Name" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%"></td>
                            <td align="left" colspan="3">
                                <asp:CheckBox ID="chkConsolidation" CssClass="field-label" runat="server" Text="Consolidation" AutoPostBack="True" OnCheckedChanged="chkConsolidation_CheckedChanged" />
                                <asp:CheckBox ID="chkSummary" CssClass="field-label" runat="server" Text="Summary"></asp:CheckBox>
                                <asp:CheckBox ID="ChkDetails" CssClass="field-label" runat="server" Text="Detail View" onclick="SetControls(this)"></asp:CheckBox></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                                <asp:HiddenField ID="h_ACTIDs" runat="server" />
                            </td>
                        </tr>
                </table>
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>&nbsp;
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="ImgTo" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <script>
                    window.onload = function () { SetControls(document.getElementById('<%=ChkDetails.ClientID %>')); }
                </script>

            </div>
        </div>
    </div>
</asp:Content>

