<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptCostCenters.aspx.vb" Inherits="Reports_ASPX_Report_rptCostCenters"
    Title="Untitled Page" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function getSubLedger() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var ACTID;
            ACTID = document.getElementById('<%=h_ACTIDs.ClientID %>').value;
            pMode = "CCSUBLEDGER"
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&ACT_ID=" + ACTID + "&MultiSelect=true"; 
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%=h_SubLedgers.ClientID %>').value = NameandCode[0];
        }
        function getCostCenter() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            sFeatures = "dialogWidth: 600px; ";     
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var ACTID;
            ACTID = document.getElementById('<%=h_ACTIDs.ClientID %>').value;
            pMode = "COSTCENTER"
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&MultiSelect=true"; 
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%=h_CostCenters.ClientID %>').value = NameandCode[0];
        }
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx", "", sFeatures);

            if (result != '' && result != undefined) {
                switch (txtControl) {
                    case 0:
                        document.getElementById('<%=txtfromDate.ClientID %>').value = result;
                        break;
                    case 1:
                        document.getElementById('<%=txtToDate.ClientID %>').value = result;
                        break;
                }
            }
            return false;
        }

        function GetBSUName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            type = document.getElementById('<%=h_Mode.ClientID %>').value;
            if (type == "party" || type == "general") {
                result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx", "", sFeatures)
            }
            else {
                result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx?multiSelect=false", "", sFeatures)
            }

            if (result != '' && result != undefined) {
                document.getElementById('<%=h_BSUID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }
        }

        function GetAccounts() {
            var sFeatures;
            sFeatures = "dialogWidth: 820px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var ActType;
            var mode = document.getElementById('<%=h_Mode.ClientID %>').value;
            result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?type=EXPINC&multiSelect=true", "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }
        }  

    </script>

    <table align="center" style="width: 80%;" cellpadding="0" cellspacing="0">
        <tr align="left">
            <td>
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="dayBook" />
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
    </table>
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0" style="width: 80%;
        height: 174px" runat="server" id="tblMain">
        <tr class="subheader_img">
            <td align="left" colspan="6" style="height: 19px" valign="middle">
                <asp:Label ID="lblrptCaption" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr runat="server" id="trBSUMulti">
            <td align="left" valign="top" class="matters" style="width: 15%">
                Business Unit
            </td>
            <td class="matters" valign="top" width="1%">
                :
            </td>
            <td align="left" valign="top" class="matters" colspan="4">
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left" class="matters">
                From Date
            </td>
            <td class="matters">
                :
            </td>
            <td align="left" class="matters" style="height: 1px; text-align: left;">
                <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate"
                    EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator>
            </td>
            <td align="left" class="matters" style="color: #1b80b6;">
                To Date
            </td>
            <td align="left" class="matters" style="height: 1px" width="1%">
                :
            </td>
            <td align="left" class="matters" style="height: 1px; text-align: left;">
                <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>&nbsp;
                <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr style="color: #1b80b6" id="Trbank">
            <td align="left" valign="top" class="matters">
                Account
            </td>
            <td class="matters" valign="top">
                :<br />
            </td>
            <td align="left" class="matters" colspan="4" style="height: 17px">
                <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the ACT ID you want to Add to the Search and click on Add)"></asp:Label>
                <br />
                <asp:TextBox ID="txtBankNames" runat="server" Height="18px" CssClass="inputbox" Width="274px"></asp:TextBox>
                <asp:LinkButton ID="lblAddActID" runat="server">Add</asp:LinkButton>
                <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="return GetAccounts()" /><br />
                <asp:GridView ID="grdACTDetails" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                    PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="ACT ID">
                            <ItemTemplate>
                                <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ACT_Name" HeaderText="ACT Name" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
         <tr id="trCostCenter" runat="server">
            <td align="left" class="matters">
                Cost Center
            </td>
            <td class="matters">
                :
            </td>
            <td align="left" class="matters" colspan="4" style="height: 17px">
                <asp:TextBox ID="txtAddCostCenter" runat="server" Height="18px" CssClass="inputbox" Width="274px"></asp:TextBox>
                <asp:LinkButton ID="lnkAddCostCenter" runat="server">Add</asp:LinkButton>
                <asp:ImageButton ID="imgAddCostCenter" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="getCostCenter()" /><br />
                <asp:GridView ID="grdCostCenter" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                    PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="Cost Center ID">
                            <ItemTemplate>
                                <asp:Label ID="lblCostCenter" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCR" HeaderText="Cost Center Name" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdCostCenterDelete" runat="server" OnClick="lnkbtngrdCostCenterDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
        <tr id="trSubLedger" runat="server">
            <td align="left" class="matters">
                Sub Ledger
            </td>
            <td class="matters">
                :
            </td>
            <td align="left" class="matters" colspan="4" style="height: 17px">
                <asp:TextBox ID="txtSubLedger" runat="server" Height="18px" CssClass="inputbox" Width="274px"></asp:TextBox>
                <asp:LinkButton ID="lblAddSubLed" runat="server">Add</asp:LinkButton>
                <asp:ImageButton ID="imgSubLedger" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="getSubLedger()" /><br />
                <asp:GridView ID="grdSubLedger" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                    PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="Sub Ledger ID">
                            <ItemTemplate>
                                <asp:Label ID="lblSubLedger" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCR" HeaderText="Sub Ledger" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdACTDelete0" runat="server" OnClick="lnkbtngrdSubLedDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
        <tr id="trGroupBy" runat="server">
            <td align="left" class="matters">
                View By
            </td>
            <td class="matters">
                :
            </td>
            <td align="left" class="matters" colspan="4" style="height: 17px">
                <table width="70%">
                    <tr>
                        <td width="30%">
                            <asp:ListBox ID="lstGroupByBox" runat="server" Width="116px" 
                                CssClass="itemArea">
                                <asp:ListItem Value="ACT">Account</asp:ListItem>
                                <asp:ListItem Value="CCT">Cost Center</asp:ListItem>
                                <asp:ListItem Value="SLP">Sub Ledger Parent</asp:ListItem>
                                <asp:ListItem Value="SUB">Sub Ledger</asp:ListItem>
                            </asp:ListBox>
                        </td>
                        <td width="10%">
                <asp:Button ID="btnLstAddAll" runat="server" CssClass="button" Text="&gt;&gt;" 
                                Width="31px" />
                            <br />
                <asp:Button ID="btnLstAdd" runat="server" CssClass="button" Text="&gt;" Width="31px" 
                                height="24px" />
                            <br />
                <asp:Button ID="btnLstRemove" runat="server" CssClass="button" Text="&lt;" Width="31px" 
                                height="24px" />
                            <br />
                <asp:Button ID="btnLstRemoveAll" runat="server" CssClass="button" Text="&lt;&lt;" 
                                Width="31px" height="24px" />
                        </td>
                        <td width="45%">
                            <asp:ListBox ID="lstSelGroupByBox" runat="server" Width="116px" 
                                CssClass="itemArea"></asp:ListBox>
                        </td>
                    </tr>
                </table>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="6" style="text-align: right">
                &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                    ValidationGroup="dayBook" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="h_ACTIDs" runat="server" />
    <asp:HiddenField ID="h_BSUID" runat="server" />
    <asp:HiddenField ID="h_Mode" runat="server" />
    <asp:HiddenField ID="h_ACTTYPE" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="h_SubLedgers" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="h_CostCenters" runat="server"></asp:HiddenField>
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
</asp:Content>
