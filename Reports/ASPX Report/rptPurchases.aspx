<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPurchases.aspx.vb" Inherits="Reports_ASPX_Report_rptPurchases" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <style>
        table td input[type=text]{
            min-width : 20%  !important;
        }
    </style>
    <script language="javascript" type="text/javascript">


        function GetBankName(id) {
            var NameandCode;
            var mode;
            var result;
            document.getElementById('<%=hd_id.ClientID%>').value = id;
            switch (id) {
                case 0:
                    result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=false&mode=s&forrpt=1", "pop_up")
                    break;
                case 1:
                    result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=false&forrpt=1", "pop_up")
                    break;
            }
            <%-- if (result == '' || result == undefined) {
              return false;
          }
          NameandCode = result.split('___');
          switch (id) {
              case 0:
                  document.getElementById('<%=txtbankID.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%=txtBankName.ClientID %>').value = NameandCode[1];
                        break;
                    case 1:
                        document.getElementById('<%=txtAccountID.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%=txtAccountName.ClientID %>').value = NameandCode[1];
                        break;
                }
                return false;
            }--%>
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                var id = document.getElementById('<%=hd_id.ClientID%>').value;
                if (id == 0) {
                    document.getElementById('<%=txtbankID.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%=txtBankName.ClientID %>').value = NameandCode[1];
                }
                else if (id == 1) {
                    document.getElementById('<%=txtAccountID.ClientID %>').value = NameandCode[0];
                       document.getElementById('<%=txtAccountName.ClientID %>').value = NameandCode[1];

                   }
               return false;
           }
       }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <asp:HiddenField ID="hd_id" runat="server" />
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            Purchases 
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table style="width: 100%">

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">From Date</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtfromDate" runat="server"></asp:TextBox>
                            <img onclick="getDate(550, 310, 0)" src="../../Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="valFromDate" runat="server" ControlToValidate="txtfromDate"
                                ErrorMessage="From Date Required" ValidationGroup="bankPayment">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate"
                                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="bankPayment">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%">
                            <span class="field-label">To Date</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                            <img onclick="getDate(820, 310, 1)" src="../../Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="valToDate" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date Required" ValidationGroup="bankPayment">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="bankPayment">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr id="trBankRow" runat="server">
                        <td align="left" width="20%">
                            <span class="field-label">Creditor</span></td>

                        <td colspan="3" style="text-align: left">
                            <asp:TextBox ID="txtBankName" runat="server" Width="40%"></asp:TextBox>
                            <img onclick="GetBankName(0); return false;" src="../../Images/forum_search.gif" />
                            <asp:TextBox ID="txtBankID" runat="server" Width="30%"></asp:TextBox></td>
                    </tr>
                    <tr id="trAccountRow" runat="server">
                        <td align="left" width="20%">
                            <span class="field-label">Account</span></td>

                        <td colspan="3" style="text-align: left">
                            <asp:TextBox ID="txtAccountName" runat="server" Width="40%"></asp:TextBox>
                            <img onclick="GetBankName(1); return false;" src="../../Images/forum_search.gif" />
                            <asp:TextBox ID="txtAccountID" runat="server" Width="30%"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Business Unit</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlBSUnit" runat="server"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Generate Report"
                                ValidationGroup="bankPayment" />
                            <asp:Button ID="Button1" runat="server" CssClass="button" Text="Cancel" UseSubmitBehavior="False" /></td>
                    </tr>
                </table>
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                <asp:ValidationSummary
                    ID="errorList" runat="server" Font-Overline="False" HeaderText="There was an error submitting your form.&#13;&#10;            Please check the following:"
                    ShowSummary="true" Style="list-style-position: inside; list-style-type: circle"
                    ValidationGroup="bankPayment" />
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>

