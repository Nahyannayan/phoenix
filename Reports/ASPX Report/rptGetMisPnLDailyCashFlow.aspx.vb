Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports System.Data.SqlTypes
Imports UtilityObj
Partial Class Reports_ASPX_Report_rptGetMisPnLDailyCashFlow
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            Select Case MainMnu_code
                Case OASISConstants.ReportMISDailyCashFlow
                    lblrptCaption.Text = "Daily Cash Flow"
                Case "A753975"
                    lblrptCaption.Text = "MIS Consolidation - P/L"
                    chkExcludeSpecialAccounts.Visible = True
                Case "A753056"
                    lblrptCaption.Text = "Budget vs Actual P/L for period (Columnar)"
            End Select
            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                txtToDate.Text = UtilityObj.GetDiplayDate()
                txtFromDate.Text = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
                & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", ConnectionManger.GetOASISConnectionString)
                txtFromDate.Text = Format(CDate(txtFromDate.Text), OASISConstants.DateFormat) 
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
            Select Case MainMnu_code
                Case OASISConstants.ReportMISDailyCashFlow
                    RPTGetMISCashflowNew()
                Case "A753975"
                    rptMISPnLColumnar_consolidation()
                Case "A753056"
                    rptMISPnLColumnar_period()
            End Select
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub rptMISPnLColumnar_period()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")
        Dim str_report_sp As String = "rptMISPnLColumnar_period"
        Dim Misparams As New MisParameters
        Dim cmd As New SqlCommand(str_report_sp, objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar, 400)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)


        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpDTTO)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        objConn.Close()
        objConn.Open()
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        params("ReportCaption") = "Budget vs Actual P/L for period (Columnar)"
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        params("ToDate") = txtToDate.Text
        params("fromDate") = Format(CDate(sqlpFROMDOCDT.Value), OASISConstants.DateFormat)

        Misparams.BSU_IDs = strSelectedNodes
        Misparams.BSUShortnames = str_bsu_shortname
        Misparams.BSUSegments = str_bsu_Segment
        Misparams.ToDate = txtToDate.Text
        Misparams.FromDate = txtFromDate.Text
        Misparams.FromDate = Format(CDate(Misparams.FromDate), OASISConstants.DateFormat)
        params("ToDate") = txtToDate.Text
        params("fromDate") = Misparams.FromDate
        Misparams.Currency = str_bsu_CURRENCY
        Misparams.ExpressedIn = ddExpressedin.SelectedItem.Value
        Misparams.ExcludeSpecialAccounts = chkExcludeSpecialAccounts.Checked
        Session("Misparams") = Misparams

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptMISPnLColumnar_BudgetvsActual_Period.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        Response.Redirect("rptviewer.aspx", True)
    End Sub

    Private Sub rptMISPnLColumnar_consolidation()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim Misparams As New MisParameters
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")
        Dim str_report_sp As String = "rptMISPnLColumnar_consolidation"
       
        Dim cmd As New SqlCommand(str_report_sp, objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar, 400)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpDTTO)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", SqlDbType.Bit)
        sqlpBSpecial.Value = chkExcludeSpecialAccounts.Checked
        cmd.Parameters.Add(sqlpBSpecial)

        objConn.Close()
        objConn.Open()
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        If chkExcludeSpecialAccounts.Checked Then
            params("ReportCaption") = "MIS Consolidation - P/L (Excluding Special Accounts)"
        Else
            params("ReportCaption") = "MIS Consolidation - P/L"
        End If
        params("BSU_CURRENCY") = str_bsu_CURRENCY
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        params("ToDate") = txtToDate.Text
        params("fromDate") = Format(CDate(sqlpFROMDOCDT.Value), OASISConstants.DateFormat)
        params("DrillDownMode") = "PLCOLUMNAR"
        repSource.Parameter = params
        repSource.Command = cmd
        Misparams.BSU_IDs = strSelectedNodes
        Misparams.BSUShortnames = str_bsu_shortname
        Misparams.BSUSegments = str_bsu_Segment
        Misparams.ToDate = txtToDate.Text
        Misparams.FromDate = txtFromDate.Text
        Misparams.FromDate = Format(CDate(Misparams.FromDate), OASISConstants.DateFormat)
        params("ToDate") = txtToDate.Text
        params("fromDate") = Misparams.FromDate
        Misparams.Currency = str_bsu_CURRENCY
        Misparams.ExpressedIn = ddExpressedin.SelectedItem.Value
        Misparams.ExcludeSpecialAccounts = chkExcludeSpecialAccounts.Checked
        Session("Misparams") = Misparams
        repSource.ResourceName = "../RPT_Files/rptMISPnLColumnar_consolidation.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        Response.Redirect("rptviewer.aspx", True)
    End Sub

    Private Sub RPTGetMISCashflowNew()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        '@return_value = [dbo].[RPTGetMISPnL]
        '@BSU_ID = N'999999|01101000|125016|125003|125016',
        '@DTTO = N'31/mar/2008'
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")

        Dim cmd As New SqlCommand("RPTGetMISCashflowNew", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 400)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpDTFROM As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpDTFROM.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpDTFROM)
        objConn.Close()
        objConn.Open()
        Dim Misparams As New MisParameters
        If 1 = 1 Then
            Dim str_bsu_Segment As String = ""
            Dim str_bsu_shortname As String = ""
            Dim str_bsu_CURRENCY As String = ""
            getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
            str_bsu_shortname, str_bsu_CURRENCY)

            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("UserName") = Session("sUsr_name")
            params("Bsu_Shortnames") = str_bsu_shortname
            params("Bsu_Segments") = str_bsu_Segment
            params("ToDate") = txtToDate.Text
            params("FromDate") = txtFromDate.Text
            params("ReportCaption") = "Daily Cash Flow"
            params("BSU_CURRENCY") = str_bsu_CURRENCY
            params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value

            Misparams.BSU_IDs = strSelectedNodes
            Misparams.BSUShortnames = str_bsu_shortname
            Misparams.BSUSegments = str_bsu_Segment
            Misparams.ToDate = txtToDate.Text
            'Misparams.FromDate = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
            '& " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
            'WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
            'Misparams.FromDate = Format(CDate(Misparams.FromDate), OASISConstants.DateFormat)
            Misparams.FromDate = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            params("fromDate") = Misparams.FromDate
            Misparams.Currency = str_bsu_CURRENCY
            Misparams.ExpressedIn = ddExpressedin.SelectedItem.Value
            Session("Misparams") = Misparams

            Dim repSourceSubRep() As MyReportClass
            repSourceSubRep = repSource.SubReport
            Dim subReportLength As Integer = 1
            If repSource.SubReport Is Nothing Then
                ReDim repSourceSubRep(1)
            Else
                subReportLength = repSourceSubRep.Length + 1
                ReDim Preserve repSourceSubRep(subReportLength)
            End If
            repSource.SubReport = repSourceSubRep

            repSourceSubRep(subReportLength - 1) = New MyReportClass
            Dim cmdRecieptInfo As New SqlCommand("RptLedgerSummaryForCashflow", objConn)
            cmdRecieptInfo.CommandType = CommandType.StoredProcedure

            Dim sqlpsubBSU_IDs As New SqlParameter("@BSUID", SqlDbType.VarChar, 200)
            sqlpsubBSU_IDs.Value = strSelectedNodes
            cmdRecieptInfo.Parameters.Add(sqlpsubBSU_IDs)

            Dim sqlpsubDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
            sqlpsubDTTO.Value = txtToDate.Text
            cmdRecieptInfo.Parameters.Add(sqlpsubDTTO)

            Dim sqlpsubTyp As New SqlParameter("@Typ", SqlDbType.VarChar, 2)
            sqlpsubTyp.Value = "R"
            cmdRecieptInfo.Parameters.Add(sqlpsubTyp)
            repSourceSubRep(subReportLength - 1).Command = cmdRecieptInfo

            Dim cmdPayment As SqlCommand = cmdRecieptInfo.Clone
            cmdPayment.Parameters(2).Value = "P"
            repSourceSubRep(subReportLength) = New MyReportClass
            repSourceSubRep(subReportLength).Command = cmdPayment

            repSource.SubReport = repSourceSubRep

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptGetMISPnLDailycashflow.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            Response.Redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub
     
End Class
