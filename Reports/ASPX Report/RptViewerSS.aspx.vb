Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports Crystaldecisions.reportsource
Imports Crystaldecisions.shared
Imports CrystalDecisions.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports UtilityObj
Imports System.drawing.Printing

Partial Class Reports_ASPX_Report_RptViewerSS
    Inherits System.Web.UI.Page

    Dim repSource As MyReportClass
    Dim repClassVal As New RepClass
    Dim Encr_decrData As New Encryption64
    Shared rnd As New Random()


    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Using msOur As New System.IO.MemoryStream()
            Using swOur As New System.IO.StreamWriter(msOur)
                Dim ourWriter As New HtmlTextWriter(swOur)
                MyBase.Render(ourWriter)
                ourWriter.Flush()
                msOur.Position = 0
                Using oReader As New System.IO.StreamReader(msOur)
                    Dim sTxt As String = oReader.ReadToEnd()
                    If Web.Configuration.WebConfigurationManager.AppSettings("AlwaysSSL").ToLower = "true" Then
                        sTxt = sTxt.Replace(Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString(), _
                        Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString().Replace("http://", "https://"))
                    End If
                    Response.Write(sTxt)
                    oReader.Close()
                End Using
            End Using
        End Using
    End Sub
    Private Sub FillYear()
        Try
            Dim strSql As String
            Dim EMP_ID As String
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            EMP_ID = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
            strSql = "SELECT DISTINCT YEAR(ELS_DTFROM) mYear FROM EMPLEAVESCHEDULE_D WHERE ELS_BSU_ID = '" & Session("sBsuid") & "' AND  ELS_EMP_ID='" & EMP_ID & "' order by YEAR(ELS_DTFROM) "
            fillDropdown(ddlYear, strSql, "mYear", "mYear", str_conn, True)
            If Not Request.QueryString("SelValue") Is Nothing Then
                If ddlYear.Items.FindByValue(Request.QueryString("SelValue").ToString) IsNot Nothing Then
                    ddlYear.SelectedValue = Request.QueryString("SelValue").ToString
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal connStr As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connStr)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            drpObj.Items.Insert(0, " ")
            drpObj.Items(0).Value = "0"
            drpObj.SelectedValue = "0"
        End If
    End Sub

    Protected Sub btnPrintReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintReport.Click
        Try
            If repClassVal IsNot Nothing Then
                'Dim printDocument As New System.Drawing.Printing.PrintDocument()
                'Response.Write(Drawing.Printing.PrinterSettings.InstalledPrinters.Count)
                'repClassVal.PrintOptions.PrinterName = ""
                'repClassVal.PrintToPrinter(1, False, 0, 0)
                'Response.Redirect("printReport.aspx", True)
                'CrystalReportViewer1.RefreshReport()


            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim isExport = IIf(Request.QueryString("isExport") Is Nothing, False, Request.QueryString("isExport"))
            If isExport Then
                ExportReportToExcel()
            End If
            FillYear()
            If Request.UrlReferrer IsNot Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
        End If
        Dim ds As New DataSet
        Page.Title = OASISConstants.Gemstitle
        If Cache.Item("reportDS" & ViewState("h_uniqueID")) Is Nothing Then
            Cache.Remove("reportDS" & ViewState("h_uniqueID"))
            ViewState("h_uniqueID") = Session.SessionID & rnd.Next()
            Session("TempReportSource" & ViewState("h_uniqueID")) = repSource
        End If
        If Not IsPostBack Then
            repSource = DirectCast(Session("ReportSource"), MyReportClass)
            Session("TempReportSource" & ViewState("h_uniqueID")) = repSource
        Else
            repSource = DirectCast(Session("TempReportSource" & ViewState("h_uniqueID")), MyReportClass)
        End If

        If Not repSource Is Nothing Then
            If repSource.GetDataSourceFromCommand Then
                If Cache("reportDS" & ViewState("h_uniqueID")) Is Nothing Then
                    'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                    'Dim objConn As New SqlConnection(str_conn)
                    Dim objConn As SqlConnection = repSource.Command.Connection
                    'repSource.Command.Connection = objConn
                    objConn.Close()
                    objConn.Open()
                    Dim adpt As New SqlDataAdapter
                    adpt.SelectCommand = repSource.Command
                    'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey
                    adpt.SelectCommand.CommandTimeout = 0
                    ds.Clear()
                    adpt.Fill(ds)
                    If ds Is Nothing Then ' OrElse ds.Tables.Count <= 0 OrElse ds.Tables(0).Rows.Count <= 0 Then
                        If Not Request.UrlReferrer Is Nothing Then
                            If Request.UrlReferrer.ToString() <> "" And "/" & Request.UrlReferrer.GetComponents(UriComponents.Path, UriFormat.SafeUnescaped) <> Request.CurrentExecutionFilePath Then
                                Dim urlstring As String = IIf(Request.UrlReferrer.ToString().Contains("?"), "&nodata=true", "?nodata=true")
                                Response.Redirect(Request.UrlReferrer.ToString() & urlstring)
                            End If
                        Else
                            Exit Sub
                        End If
                    End If
                    Dim isCircular = IIf(Request.QueryString("isCircular") Is Nothing, False, Request.QueryString("isCircular"))
                    If isCircular Then
                        UpdatePassword(ds.Tables(0))
                    End If
                    objConn.Close()
                    Cache.Insert("reportDS" & ViewState("h_uniqueID"), ds, Nothing, DateTime.Now.AddMinutes(30), Nothing)
                End If
                repClassVal = New RepClass
                repClassVal.ResourceName = repSource.ResourceName
                ds = DirectCast(Cache.Item("reportDS" & ViewState("h_uniqueID")), DataSet)
                repClassVal.SetDataSource(ds.Tables(0))
                'repSource.IncludeBSUImage = False
                If repSource.IncludeBSUImage Then
                    If repSource.HeaderBSUID Is Nothing Then
                        IncludeBSUmage(repClassVal)
                    Else
                        IncludeBSUmage(repClassVal, repSource.HeaderBSUID)
                    End If

                End If
                SubReport(repSource, repClassVal)
                CrystalReportViewer1.ReportSource = repClassVal
                repClassVal.PrintOptions.PaperOrientation = repClassVal.PrintOptions.PaperOrientation
                'repClassVal.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                Dim ienum As IDictionaryEnumerator = repSource.Parameter.GetEnumerator()
                While ienum.MoveNext()
                    If ienum.Key = "userName" Then
                        If Not Session("lastMnuCode") Is Nothing Then
                            repClassVal.SetParameterValue(ienum.Key, ienum.Value + " [Ref. - " + Session("lastMnuCode") + "]")
                        Else
                            repClassVal.SetParameterValue(ienum.Key, ienum.Value)
                        End If
                    Else
                        Try
                            repClassVal.SetParameterValue(ienum.Key, ienum.Value)
                        Catch ex As Exception

                        End Try

                    End If
                End While
                Try
                    If Not repSource.Formulas.GetEnumerator() Is Nothing Then
                        Dim iformula As IDictionaryEnumerator = repSource.Formulas.GetEnumerator()
                        While iformula.MoveNext()
                            If Not repClassVal.DataDefinition.FormulaFields(iformula.Key) Is Nothing Then
                                repClassVal.DataDefinition.FormulaFields(iformula.Key).text = iformula.Value
                            End If
                        End While
                    End If

                Catch ex As Exception
                End Try
                CrystalReportViewer1.DisplayGroupTree = repSource.DisplayGroupTree
                'objConn.Close()
            Else
                Dim dt As DataTable
                If Cache("reportDS" & ViewState("h_uniqueID")) Is Nothing Then
                    'Dim objConn As New SqlConnection(str_conn)
                    Dim objConn As SqlConnection = repSource.Command.Connection
                    'repSource.Command.Connection = objConn
                    objConn.Close()
                    objConn.Open()
                    'Dim ds As New DataSet
                    Dim adpt As New SqlDataAdapter
                    adpt.SelectCommand = repSource.Command
                    adpt.Fill(ds)
                    '' '
                    Dim fromDate As Date
                    If repSource.Parameter IsNot Nothing Then
                        fromDate = CDate(repSource.Parameter("FromDate"))
                    End If
                    dt = TransformDatasettoTable.TransformDataset(ds.Tables(0), fromDate)
                    Cache.Insert("reportDS" & ViewState("h_uniqueID"), dt, Nothing, DateTime.Now.AddMinutes(30), Nothing)
                    objConn.Close()
                End If
                repClassVal = New RepClass
                repClassVal.ResourceName = repSource.ResourceName

                dt = Cache("reportDS" & ViewState("h_uniqueID"))
                repClassVal.SetDataSource(dt)

                '' ''
                '' '
                CrystalReportViewer1.ReportSource = repClassVal
                Dim ienum As IDictionaryEnumerator = repSource.Parameter.GetEnumerator()
                While ienum.MoveNext()
                    repClassVal.SetParameterValue(ienum.Key, ienum.Value)
                End While

                If Not repSource.Formulas.GetEnumerator() Is Nothing Then
                    Dim iformula As IDictionaryEnumerator = repSource.Formulas.GetEnumerator()
                    While iformula.MoveNext()
                        If Not repClassVal.DataDefinition.FormulaFields(iformula.Key) Is Nothing Then
                            repClassVal.DataDefinition.FormulaFields(iformula.Key).text = iformula.Value
                        End If
                    End While
                End If
            End If

        End If
      
        Try
            Dim btnClick = PrinterFunctions.GetPostBackControl(Me.Page)
            If btnClick IsNot Nothing Then
                Dim btnClickstatus As String '= btnClick.CommandName
                If btnClickstatus = "Export" Or btnClickstatus = "Print" Then
                    'IncreamentPrinterCount()
                Else
                    Session("PrintClicked") = False
                End If
            End If
        Catch ex As Exception
            Session("PrintClicked") = False
        End Try
        repSource = Nothing
    End Sub

    Private Sub UpdatePassword(ByVal dt As DataTable)
        For Each dtRow As DataRow In dt.Rows
            dtRow("PASSWORD") = Encr_decrData.Decrypt(dtRow("OLU_PASSWORD"))
        Next

    End Sub


    Private Sub ExportReportToExcel()
        Dim ds As New DataSet
        repSource = DirectCast(Session("ReportSource"), MyReportClass)
        Dim objConn As SqlConnection = repSource.Command.Connection
        Try
            'repSource.Command.Connection = objConn
            objConn.Close()
            objConn.Open()
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = repSource.Command
            'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey
            adpt.SelectCommand.CommandTimeout = 0
            ds.Clear()
            adpt.Fill(ds)
            If (Not ds Is Nothing) AndAlso (Not ds.Tables(0).Rows.Count <= 0) Then
                Dim strFName As String = String.Empty

                If repSource.VoucherName <> "" Then
                    strFName = repSource.VoucherName
                Else
                    If Not repSource.Parameter Is Nothing Then
                        strFName = repSource.Parameter("VoucherName")
                    End If
                End If
                If strFName = "" Then
                    strFName = "report" + Session("sUsr_name") + DateTime.Now.ToFileTime.ToString()
                End If

                DataSetToExcel.Convert(ds, Response, Server.MapPath("ExcelFormat.xsl"), strFName)
                'DataSetToExcel.Convert(ds, Response)
                'Dim strDirPath As String = Server.MapPath("..\..\Temp")
                'Dim strFName As String = String.Empty

                'If repSource.VoucherName <> "" Then
                '    strFName = repSource.VoucherName
                'Else
                '    If Not repSource.Parameter Is Nothing Then
                '        strFName = repSource.Parameter("VoucherName")
                '    End If
                'End If
                'If strFName = "" Then
                '    strFName = "report" + Session("sUsr_name") + DateTime.Now.ToFileTime.ToString()
                'End If
                'Dim strFileName As String = DataSetToExcel.Convert(ds, strDirPath, strFName)
                'Response.Clear()
                'Response.Charset = ""
                ''set the response mime type for excel
                'Response.ContentType = "application/vnd.ms-excel"
                'Response.AddHeader("Content-Disposition", "attachment; filename=" & strFName & ".xls")
                'Dim fStream As New System.IO.FileStream(strFileName, IO.FileMode.Open, IO.FileAccess.Read)
                ''     Dim bw As New System.IO.BinaryReader(fStream)
                'Dim byt As Byte() = New Byte(fStream.Length - 1) {}
                'fStream.Read(byt, 0, fStream.Length - 1)
                ''    byt = bw.ReadBytes(CInt(fStream.Length - 1))
                'fStream.Close()
                ''bw.Close()
                'Response.BinaryWrite(byt)
                ''Response.End()
                ''Response.Flush()

                ''Response.WriteFile(strFileName)

                'System.IO.File.Delete(strFileName)

            Else
                If Request.UrlReferrer <> Nothing AndAlso Request.UrlReferrer.ToString() <> "" Then
                    Dim urlstring As String = IIf(Request.UrlReferrer.ToString().Contains("?"), "&nodata=true", "?nodata=true")
                    Response.Redirect(Request.UrlReferrer.ToString() & urlstring)
                End If
            End If
        Catch ex As Exception


        Finally
            objConn.Close()
        End Try

    End Sub

     
     
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub SubReport(ByVal subRep As MyReportClass, ByRef repClassVal As RepClass)
        If subRep.SubReport IsNot Nothing Then
            Dim ii As Integer = 0
            If subRep.IncludeBSUImage Then
                ii = 1
            End If
            For i As Integer = 0 To subRep.SubReport.Length - 1
                Dim myrep As MyReportClass = subRep.SubReport(i)
                If myrep IsNot Nothing Then
                    If myrep.SubReport IsNot Nothing Then
                        SubReport(subRep, repClassVal)
                    Else
                        Dim objConn As SqlConnection = repSource.Command.Connection
                        'objConn.Close()
                        objConn.Open()
                        Dim ds As New DataSet
                        Dim adpt As New SqlDataAdapter
                        Try
                            adpt.SelectCommand = myrep.Command
                            adpt.Fill(ds)
                        Catch ex As Exception
                            UtilityObj.Errorlog(ex.Message, "Report Viewer(SUB_FIN)")
                            Exit Sub
                        Finally
                            objConn.Close()
                        End Try
                        If String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Transport.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Portrait.rpt", True) <> 0 Then
                            repClassVal.Subreports(i + ii).SetDataSource(ds.Tables(0))
                        Else
                            ViewState("reportHeader") = repClassVal.Subreports(i + ii).Name
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub IncludeBSUmage(ByRef repClassVal As RepClass, Optional ByVal BSUID As String = "")
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(lstrConn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("getBsuInFoWithImage", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpIMG_BSU_ID As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 100)
        If BSUID = "" Then
            sqlpIMG_BSU_ID.Value = Session("sbSUID")
        Else
            sqlpIMG_BSU_ID.Value = BSUID
        End If

        cmd.Parameters.Add(sqlpIMG_BSU_ID)

        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmd.Parameters.Add(sqlpIMG_TYPE)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        objConn.Close()
        If Not repClassVal.Subreports("rptHeader.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Transport.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Transport.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Portrait.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Portrait.rpt").SetDataSource(ds.Tables(0))
        End If
        'repClassVal.Subreports("bsuimage").SetDataSource(dtDt)

        'repClassVal.Subreports("BSUIMAGE").SetDataSource(dtDt)
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
        'If Session("sModule") = "SS" Then
        '    Me.MasterPageFile = "../../mainMasterPageSS.master"
        'Else
        '    Me.MasterPageFile = "../../mainMasterPage.master"
        'End If
    End Sub


    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload

        'CrystalReportViewer1.Dispose()
        'CrystalReportViewer1 = Nothing
        repClassVal.Close()
        repClassVal.Dispose()
        'GC.Collect()

    End Sub

    Protected Sub btnGO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGO.Click
        Try
            If ddlYear.SelectedItem Is Nothing Then Exit Sub
            Dim urlstring As String
            urlstring = "../../Payroll/EOSReports.aspx?MainMnu_code=" & Encr_decrData.Encrypt("U000084")
            urlstring = urlstring & "&lYear=" & ddlYear.SelectedValue
            Response.Redirect(urlstring)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
End Class
