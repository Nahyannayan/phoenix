Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Partial Class Reports_ASPX_Report_rptCostCenters
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String
    Dim MainObj As Mainclass = New Mainclass()



    'ModifiedOn         By          Description
    '----------         --          -----------

    '05/nov/2012        Jacob       To include subreport in cost center report.


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle

            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            'UtilityObj.NoOpen(Me.Header)
            lblrptCaption.Text = Mainclass.GetMenuCaption(MainMnu_code)
            Select Case MainMnu_code
                Case "A450005"
                    trBSUMulti.Visible = True
            End Select
            h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
            If h_ACTIDs.Value <> Nothing And h_ACTIDs.Value <> "" And h_ACTIDs.Value <> "undefined" Then
                FillACTIDs(h_ACTIDs.Value)
            End If
            If h_SubLedgers.Value <> Nothing And h_SubLedgers.Value <> "" And h_SubLedgers.Value <> "undefined" Then
                FillSubLedgers(h_SubLedgers.Value)
            End If
            If h_CostCenters.Value <> Nothing And h_CostCenters.Value <> "" And h_CostCenters.Value <> "undefined" Then
                FillCostCenters(h_CostCenters.Value)
            End If
            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                txtToDate.Text = UtilityObj.GetDiplayDate()
                txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))
                txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
                txtToDate.Attributes.Add("onBlur", "checkdate(this)")
                filldrp()
                FillHistoryValues()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Private Sub FillCostCenters(ByVal CCTIDs As String)
        Try
            Dim IDs As String() = CCTIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
            Next
            str_Sql = "select CCT_ID ID,CCT_DESCR DESCR from COSTCENTER_M  WHERE CCT_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdCostCenter.DataSource = ds
            grdCostCenter.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Try
            Dim IDs As String() = ACTIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
            Next
            str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdACTDetails.DataSource = ds
            grdACTDetails.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub FillSubLedgers(ByVal SubLedgerIDs As String)
        Try
            Dim IDs As String() = SubLedgerIDs.Split("|")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
            Next
            str_Sql = "SELECT distinct ASM_NAME DESCR,ASM_ID ID FROM VW_ACCOUNTS_SUB_ASM WHERE ASM_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdSubLedger.DataSource = ds
            grdSubLedger.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            txtFromDate.Text = Format("dd/MMM/yyyy", txtFromDate.Text)
            txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
            Dim strXMLBSUNames As String
            h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
            Session("Fdate") = txtFromDate.Text
            Session("Tdate") = txtToDate.Text
            Session("selBSUID") = h_BSUID.Value
            Session("selACTID") = h_ACTIDs.Value
            Session("selSubLedgersID") = h_SubLedgers.Value
            Session("selCostCenterID") = h_CostCenters.Value
            Dim SelGroupList As String = ""
            Dim lstItem As ListItem
            For Each lstItem In lstSelGroupByBox.Items
                SelGroupList &= IIf(SelGroupList = "", "", "|") & lstItem.Value
            Next
            Session("SelGroupList") = SelGroupList
            strXMLBSUNames = GenerateXML(h_BSUID.Value, XMLType.BSUName) 'txtBSUNames.Text)
            Select Case MainMnu_code
                Case "A350046" 'Cost Center Analysis
                    GenerateCostCenterLedgerReport()
            End Select

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GenerateCostCenterLedgerReport()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand

        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "SP_COSTCENTER_LEDGER"
        Dim cmdSub As New SqlCommand("[SP_COSTCENTER_LEDGERBALANCE]")
        cmdSub.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = h_BSUID.Value
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)
        Dim subrepParam1 As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        subrepParam1.Value = h_BSUID.Value.Replace("||", "|")
        cmdSub.Parameters.Add(subrepParam1)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.VarChar)
        sqlpACT_IDs.Value = h_ACTIDs.Value
        cmd.Parameters.Add(sqlpACT_IDs)
        Dim subrepParam2 As New SqlParameter("@ACT_IDs", SqlDbType.VarChar)
        subrepParam2.Value = h_ACTIDs.Value.Replace("||", "|")
        cmdSub.Parameters.Add(subrepParam2)

        Dim sqlpCCT_IDs As New SqlParameter("@CCT_IDs", SqlDbType.VarChar)
        sqlpCCT_IDs.Value = h_CostCenters.Value
        cmd.Parameters.Add(sqlpCCT_IDs)


        Dim sqlpASM_IDs As New SqlParameter("@ASM_IDs", SqlDbType.VarChar)
        sqlpASM_IDs.Value = h_SubLedgers.Value
        cmd.Parameters.Add(sqlpASM_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)
        Dim subrepParam3 As New SqlParameter("@DTFROM", SqlDbType.VarChar)
        subrepParam3.Value = txtFromDate.Text
        cmdSub.Parameters.Add(subrepParam3)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)
        Dim subrepParam4 As New SqlParameter("@DTTO", SqlDbType.VarChar)
        subrepParam4.Value = txtToDate.Text
        cmdSub.Parameters.Add(subrepParam4)

        objConn.Close()
        objConn.Open()
        cmd.Connection = New SqlConnection(str_conn)
        cmdSub.Connection = New SqlConnection(str_conn)
        Dim repSourceSubRep(1) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub
        Session("BSUNames") = ""
        Session("rptFromDate") = txtFromDate.Text
        Session("rptToDate") = txtToDate.Text

        Dim repSource As New MyReportClass
        Dim formulas As New Hashtable
        Dim ij As Int16
        Dim GroupByTitle As String = ""
        Dim params As New Hashtable
        For ij = 1 To 4
            If lstSelGroupByBox.Items.Count >= ij Then
                formulas("GroupBy_" & ij.ToString) = getGroupByFieldName(lstSelGroupByBox.Items(ij - 1).Value)
                params("GroupID_" & ij.ToString) = (lstSelGroupByBox.Items(ij - 1).Value)
                GroupByTitle &= IIf(GroupByTitle = "", "", ",") & lstSelGroupByBox.Items(ij - 1).Text
            Else
                formulas("GroupBy_" & ij.ToString) = getGroupByFieldName(lstGroupByBox.Items(ij - lstSelGroupByBox.Items.Count - 1).Value)
                params("GroupID_" & ij.ToString) = (lstGroupByBox.Items(ij - lstSelGroupByBox.Items.Count - 1).Value)
                If lstSelGroupByBox.Items.Count = 0 Then
                    GroupByTitle &= IIf(GroupByTitle = "", "", ",") & lstGroupByBox.Items(ij - lstSelGroupByBox.Items.Count - 1).Text
                End If
            End If
        Next
        repSource.Formulas = formulas

        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        params("ReportCaption") = Mainclass.GetMenuCaption(MainMnu_code) & "(View By - " & GroupByTitle & ")"
        params("BSUNAME") = Mainclass.GetBSUName(Session("sBsuid"))
        repSource.Parameter = params
        repSource.Command = cmd

        repSource.SubReport = repSourceSubRep
        Dim GroupByCount As Int16
        GroupByCount = lstSelGroupByBox.Items.Count
        If GroupByCount = 0 Then GroupByCount = 4

        repSource.ResourceName = "../RPT_Files/rptCostCenterCrossTab_" & GroupByCount.ToString & ".rpt"

        Session("ReportSource") = repSource
        objConn.Close()
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("rptviewer.aspx?isExport=true", True)
        Else
            Response.Redirect("rptviewer.aspx", True)
        End If
        objConn.Close()
    End Sub
    Private Function getGroupByFieldName(ByVal iValue As String) As String
        Select Case iValue
            Case "ACT"
                Return "{SP_COSTCENTER_LEDGER;1.ACT_NAME}"
            Case "CCT"
                Return "{SP_COSTCENTER_LEDGER;1.CCT_DESCR}"
            Case "SLP"
                Return "{SP_COSTCENTER_LEDGER;1.ParentName}"
            Case "SUB"
                Return "{SP_COSTCENTER_LEDGER;1.ASM_NAME}"
            Case Else
                Return "{SP_COSTCENTER_LEDGER;1.ASM_NAME}"
        End Select
    End Function
    Private Function getGroupByFieldID(ByVal iValue As String) As String
        Select Case iValue
            Case "ACT"
                Return "SP_COSTCENTER_LEDGER;1.JNL_ACT_ID"
            Case "CCT"
                Return "{SP_COSTCENTER_LEDGER;1.CCT_ID}"
            Case "SLP"
                Return "{SP_COSTCENTER_LEDGER;1.ParentID}"
            Case "SUB"
                Return "{SP_COSTCENTER_LEDGER;1.ASM_ID}"
            Case Else
                Return "{SP_COSTCENTER_LEDGER;1.ASM_ID}"
        End Select
    End Function
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal dbName As String, ByVal addValue As Boolean)
        drpObj.DataSource = MainObj.getRecords(sqlQuery, dbName)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            drpObj.Items.Insert(0, " ")
            drpObj.Items(0).Value = "0"
            drpObj.SelectedValue = "0"
        End If
    End Sub
    Public Sub filldrp()
        Try
            Dim Query As String
            Query = "SELECT TypeId,TypeName FROM vw_GetCostCenterGroupType   ORDER BY TypeID "
            '    fillDropdown(ddType, Query, "TypeName", "TypeId", "MainDB", False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.ACTName
                elements(0) = "ACT_DETAILS"
                elements(1) = "ACT_DETAIL"
                elements(2) = "ACT_ID"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                XMLEBSUID = xmlDoc.CreateElement(elements(2))
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function


    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub
    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            Session("selACTID") = h_ACTIDs.Value
            FillACTIDs(h_ACTIDs.Value)
        End If
    End Sub
    Protected Sub lnkbtngrdSubLedDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSubLedger As New Label
        lblSubLedger = TryCast(sender.FindControl("lblSubLedger"), Label)
        If Not lblSubLedger Is Nothing Then
            h_SubLedgers.Value = ("|" & h_SubLedgers.Value & "|").Replace("|" & lblSubLedger.Text & "|", "|").Replace("||||", "||")
            Session("selSubLedgersID") = h_SubLedgers.Value
            FillSubLedgers(h_SubLedgers.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdCostCenterDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCostCenter As New Label
        lblCostCenter = TryCast(sender.FindControl("lblCostCenter"), Label)
        If Not lblCostCenter Is Nothing Then
            h_CostCenters.Value = ("|" & h_CostCenters.Value & "|").Replace("|" & lblCostCenter.Text & "|", "|").Replace("||||", "||")
            Session("selCostCenterID") = h_CostCenters.Value
            FillCostCenters(h_CostCenters.Value)
        End If
    End Sub

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdACTDetails.PageIndexChanging
        grdACTDetails.PageIndex = e.NewPageIndex
        FillACTIDs(h_ACTIDs.Value)
    End Sub
    Protected Sub grdSubLedDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdSubLedger.PageIndexChanging
        grdSubLedger.PageIndex = e.NewPageIndex
        FillSubLedgers(h_SubLedgers.Value)
    End Sub
    Protected Sub grdCostCenter_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdCostCenter.PageIndexChanging
        grdCostCenter.PageIndex = e.NewPageIndex
        FillCostCenters(h_CostCenters.Value)
    End Sub
    Protected Sub lblAddActID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddActID.Click
        h_ACTIDs.Value += "||" + txtBankNames.Text.Replace(",", "||")
        FillACTIDs(h_ACTIDs.Value)
    End Sub
    Protected Sub lblAddSubLed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddSubLed.Click
        h_SubLedgers.Value += "||" + txtSubLedger.Text.Replace(",", "||")
        FillSubLedgers(h_SubLedgers.Value)
    End Sub
    Protected Sub lblAddCostCenter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddCostCenter.Click
        h_CostCenters.Value += "||" + txtAddCostCenter.Text.Replace(",", "||")
        FillCostCenters(h_CostCenters.Value)
    End Sub
    Private Sub FillHistoryValues()
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
            txtFromDate.Text = Session("FDate")
            txtToDate.Text = Session("TDate")
            h_ACTIDs.Value = Session("selACTID")
            h_SubLedgers.Value = Session("selSubLedgersID")
            h_CostCenters.Value = Session("selCostCenterID")
            FillACTIDs(Session("selACTID"))
            FillSubLedgers(Session("selSubLedgersID"))
            FillCostCenters(Session("selCostCenterID"))
            UsrBSUnits1.SetSelectedNodes(Session("selBSUID"))
            If Session("SelGroupList") IsNot Nothing Then
                Dim SelGroupList(), iStr As String
                SelGroupList = Session("SelGroupList").ToString.Split("|")
                Dim lstItem As ListItem
                For Each iStr In SelGroupList
                    lstItem = lstGroupByBox.Items.FindByValue(iStr)
                    If lstItem IsNot Nothing And lstSelGroupByBox.Items.FindByValue(iStr) Is Nothing Then
                        lstGroupByBox.Items.Remove(lstItem)
                        Dim lstSelItems As ListItem
                        For Each lstSelItems In lstSelGroupByBox.Items
                            lstSelItems.Selected = False
                        Next
                        lstSelGroupByBox.Items.Add(lstItem)
                    End If
                Next
            End If


        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        'If nodata And 1 = 2 Then
        '    lblError.Text = "No Records with specified condition"
        '    txtFromDate.Text = Session("FDate")
        '    txtToDate.Text = Session("TDate")
        '    FillACTIDs(Session("selACTID"))
        '    FillSubLedgers(Session("selSubLedgersID"))
        '    FillCostCenters(Session("selCostCenterID"))
        '    UsrBSUnits1.SetSelectedNodes(Session("selBSUID"))
        'Else
        '    FillACTIDs(h_ACTIDs.Value)
        '    FillSubLedgers(h_SubLedgers.Value)
        '    FillCostCenters(h_CostCenters.Value)
        'End If
    End Sub

    Protected Sub btnLstAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLstAdd.Click
        Try
            If lstGroupByBox.SelectedItem IsNot Nothing Then
                Dim lstItem As ListItem
                lstItem = lstGroupByBox.SelectedItem
                lstGroupByBox.Items.Remove(lstItem)
                Dim lstSelItems As ListItem
                For Each lstSelItems In lstSelGroupByBox.Items
                    lstSelItems.Selected = False
                Next
                lstSelGroupByBox.Items.Add(lstItem)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnLstRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLstRemove.Click
        Try
            If lstSelGroupByBox.SelectedItem IsNot Nothing Then
                Dim lstItem As ListItem
                lstItem = lstSelGroupByBox.SelectedItem
                lstSelGroupByBox.Items.Remove(lstItem)
                Dim lstSelItems As ListItem
                For Each lstSelItems In lstGroupByBox.Items
                    lstSelItems.Selected = False
                Next
                lstGroupByBox.Items.Add(lstItem)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnLstAddAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLstAddAll.Click
        Try
            Dim lstItem As ListItem
            Dim i As Int16
            For i = lstGroupByBox.Items.Count - 1 To 0 Step -1
                lstItem = lstGroupByBox.Items(i)
                Dim lstSelItems As ListItem
                For Each lstSelItems In lstSelGroupByBox.Items
                    lstSelItems.Selected = False
                Next
                lstSelGroupByBox.Items.Insert(0, lstItem)
                lstGroupByBox.Items.Remove(lstItem)
            Next
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnLstRemoveAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLstRemoveAll.Click
        Try
            Dim lstItem As ListItem
            Dim i As Int16
            For i = lstSelGroupByBox.Items.Count - 1 To 0 Step -1
                lstItem = lstSelGroupByBox.Items(i)
                Dim lstSelItems As ListItem
                For Each lstSelItems In lstGroupByBox.Items
                    lstSelItems.Selected = False
                Next
                lstGroupByBox.Items.Insert(0, lstItem)
                lstSelGroupByBox.Items.Remove(lstItem)
            Next
        Catch ex As Exception

        End Try
    End Sub
End Class