Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports System.Xml
Partial Class Reports_ASPX_Report_rptCostCenterAnalysisEmployee
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ddCostcenter.DataBind()
            Page.Title = OASISConstants.Gemstitle
            txtToDate.Attributes.Add("ReadOnly", "Readonly")
            txtFromDate.Attributes.Add("ReadOnly", "Readonly")
            Session("liUserList") = New List(Of String)
            Session("liUserListCost") = New List(Of String)
            If (Request.QueryString("MainMnu_code") <> "") Then
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                ViewState("MainMnu_code") = "A450020"
            End If
            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.ReportSubLedgerAnalysis
                    lblCaption.Text = "Sub Ledger Analysis"
            End Select
            UsrBSUnits1.MenuCode = ViewState("MainMnu_code").ToString
            txtToDate.Text = UtilityObj.GetDiplayDate()
            txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))
        End If
        If chkConsolidation.Checked Then
            chkGroupCurrency.Enabled = True
        Else
            chkGroupCurrency.Enabled = False
            chkGroupCurrency.Checked = True
        End If

    End Sub 

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Session("liUserList") Is Nothing Then
            lblError.Text = "Please Pick atleast one cost object"
            Exit Sub
        End If
        txtFromDate.Text = Format("dd/MMM/yyyy", txtFromDate.Text)
        txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
        Select Case ViewState("MainMnu_code").ToString
            Case OASISConstants.ReportSubLedgerAnalysis
                RPTSubLedgerAnalysis()
        End Select
    End Sub

    Private Sub RPTSubLedgerAnalysis()
        '[dbo].[RPTSubLedgerAnalysis]
        '	@BSU_ID varchar(200) , 
        '	@FROMDT datetime ,
        '	@DTTO datetime ,
        '	@COST_IDs varchar(1000) ,
        '	@SUBCOST_IDs varchar(1000)
        '@CCS_ID
        Dim str_selectedBSU As String = UsrBSUnits1.GetSelectedNode("|")
        If str_selectedBSU = "" Then
            lblError.Text = "Please Select Businessunit"
            Exit Sub
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        'Dim adpt As New SqlDataAdapter
        'Dim ds As New DataSet
        Dim cmd As SqlCommand
        cmd = New SqlCommand("RPTSubLedgerAnalysis", objConn)

        cmd.CommandType = CommandType.StoredProcedure
        '[dbo].[RPTGetCostCenterAnalysis](@BSU_ID varchar(200), @DTTO datetime)

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 500)
        sqlpJHD_BSU_IDs.Value = str_selectedBSU
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpCOST_IDs As New SqlParameter("@SUBCOST_IDs", SqlDbType.VarChar)
        sqlpCOST_IDs.Value = FillCOSTOthers(False)
        cmd.Parameters.Add(sqlpCOST_IDs)

        Dim sqlpSUBCOST_IDs As New SqlParameter("@COST_IDs", SqlDbType.VarChar)
        sqlpSUBCOST_IDs.Value = FillCOSTOthers(True)
        cmd.Parameters.Add(sqlpSUBCOST_IDs)

        Dim sqlpCCS_ID As New SqlParameter("@CCS_ID", SqlDbType.VarChar)
        sqlpCCS_ID.Value = ddCostcenter.SelectedItem.Value
        cmd.Parameters.Add(sqlpCCS_ID)

        Dim str_costids As String = ""
        Dim str_Employeelist As String = get_Query()
        For Index As Integer = 0 To Session("liUserListCost").Count - 1
            str_costids = str_costids & "'" & Session("liUserListCost")(Index).ToString & "',"
        Next
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, _
        str_Employeelist & " and id in (" & str_costids & " '0')")
        str_costids = "* "
        If ds.Tables.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                str_costids = str_costids & ds.Tables(0).Rows(i)("Name").ToString & ","
            Next
        End If
        If str_costids = "* " Then
            str_costids = "* All"
        End If
        'adpt.SelectCommand = cmd
        'objConn.Close()
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        params("CostCenterNames") = str_costids
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptCostCenterAnalysis_Employee.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        Response.Redirect("rptviewer.aspx", True)
        'Else
        'lblError.Text = "No Records with specified condition"
        'End If
        objConn.Close()
    End Sub

    Function FillCOSTOthers(ByVal IsOther As Boolean) As String
        Dim str_costids As String = ""
        If IsOther Then
            For Index As Integer = 0 To Session("liUserList").Count - 1
                If str_costids = "" Then
                    str_costids = Session("liUserList")(Index).ToString
                Else
                    str_costids = str_costids & "|" & Session("liUserList")(Index).ToString
                End If
            Next
        Else
            For Index As Integer = 0 To Session("liUserListCost").Count - 1
                If str_costids = "" Then
                    str_costids = Session("liUserListCost")(Index).ToString
                Else
                    str_costids = str_costids & "|" & Session("liUserListCost")(Index).ToString
                End If
            Next
        End If
        Return str_costids
    End Function

    Function get_Query() As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql, str_query As String
            str_Sql = "SELECT CCS_ID ,CCS_DESCR ,CCS_QUERY, CCS_COLUMNS" _
            & " FROM COSTCENTER_S WHERE  CCS_CCT_ID='9999' AND CCS_ID='" & ddCostcenter.SelectedItem.Value & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                str_query = ds.Tables(0).Rows(0)("CCS_QUERY")
                str_query = str_query.Replace("###", Session("sBsuid"))
                str_query = str_query.Replace("@@@", txtToDate.Text)
                Return str_query
            Else
                Return ("")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ("")
        End Try

        Return ""

    End Function

    Protected Sub ddCostcenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("liUserListCost") = Nothing
        Session("liUserListCost") = New List(Of String)
    End Sub

End Class
