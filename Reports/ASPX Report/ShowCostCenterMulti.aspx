<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowCostCenterMulti.aspx.vb" Inherits="Reports_ShowCostCenterMulti" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server"> 
   <base target="_self" />
  <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
 <script language="javascript" type="text/javascript" src="../../chromejs/chrome.js">
</script> 
<script language="javascript" type="text/javascript">
   function test(val)
    {
    var path;
    if (val=='LI')
    {
    path='../../Images/operations/like.gif';
    }else if (val=='NLI')
    {
    path='../../Images/operations/notlike.gif';
    }else if (val=='SW')
    {
    path='../../Images/operations/startswith.gif';
    }else if (val=='NSW')
    {
    path='../../Images/operations/notstartwith.gif';
    }else if (val=='EW')
    {
    path='../../Images/operations/endswith.gif';
    }else if (val=='NEW')
    {
    path='../../Images/operations/notendswith.gif';
    }
    document.getElementById("<%=getid() %>").src = path;
    document.getElementById("<%=h_selected_menu_1.ClientID %>").value=val+'__'+path;
    }
  function test1(val)
    {
    var path;
    if (val=='LI')
    {
    path='../../Images/operations/like.gif';
    }else if (val=='NLI')
    {
    path='../../Images/operations/notlike.gif';
    }else if (val=='SW')
    {
    path='../../Images/operations/startswith.gif';
    }else if (val=='NSW')
    {
    path='../../Images/operations/notstartwith.gif';
    }else if (val=='EW')
    {
    path='../../Images/operations/endswith.gif';
    }else if (val=='NEW')
    {
    path='../../Images/operations/notendswith.gif';
    }
    document.getElementById("<%=getid1() %>").src = path;
    document.getElementById("<%=h_selected_menu_2.ClientID %>").value=val+'__'+path;
    }
  function test2(val)
    {
    var path;
    if (val=='LI')
    {
    path='../../Images/operations/like.gif';
    }else if (val=='NLI')
    {
    path='../../Images/operations/notlike.gif';
    }else if (val=='SW')
    {
    path='../../Images/operations/startswith.gif';
    }else if (val=='NSW')
    {
    path='../../Images/operations/notstartwith.gif';
    }else if (val=='EW')
    {
    path='../../Images/operations/endswith.gif';
    }else if (val=='NEW')
    {
    path='../../Images/operations/notendswith.gif';
    }
    document.getElementById("<%=getid2() %>").src = path;
    document.getElementById("<%=h_selected_menu_3.ClientID %>").value=val+'__'+path;
    }
   function test3(val)
    {
    var path;
    if (val=='LI')
    {
    path='../../Images/operations/like.gif';
    }else if (val=='NLI')
    {
    path='../../Images/operations/notlike.gif';
    }else if (val=='SW')
    {
    path='../../Images/operations/startswith.gif';
    }else if (val=='NSW')
    {
    path='../../Images/operations/notstartwith.gif';
    }else if (val=='EW')
    {
    path='../../Images/operations/endswith.gif';
    }else if (val=='NEW')
    {
    path='../../Images/operations/notendswith.gif';
    }
    document.getElementById("<%=getid3() %>").src = path;
    document.getElementById("<%=h_selected_menu_4.ClientID %>").value=val+'__'+path;
    }

  function test4(val)
    {
    var path;
    if (val=='LI')
    {
    path='../../Images/operations/like.gif';
    }else if (val=='NLI')
    {
    path='../../Images/operations/notlike.gif';
    }else if (val=='SW')
    {
    path='../../Images/operations/startswith.gif';
    }else if (val=='NSW')
    {
    path='../../Images/operations/notstartwith.gif';
    }else if (val=='EW')
    {
    path='../../Images/operations/endswith.gif';
    }else if (val=='NEW')
    {
    path='../../Images/operations/notendswith.gif';
    }
    document.getElementById("<%=getid4() %>").src = path;
    document.getElementById("<%=h_selected_menu_5.ClientID %>").value=val+'__'+path;
    }

   function ChangeCheckBoxState(id, checkState)
    {
    var cb = document.getElementById(id);
    if (cb != null)
     cb.checked = checkState;
    }
    
    function ChangeAllCheckBoxStates(checkState)
    {
    // Toggles through all of the checkboxes defined in the CheckBoxIDs array
    // and updates their value to the checkState input parameter
     // var lstrChk = document.getElementById("chkAL").checked;
    var chk_state=document.getElementById("chkAL").checked; 
     for(i=0; i<document.forms[0].elements.length; i++)
     {
     if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
      if (document.forms[0].elements[i].type=='checkbox')
        {
        document.forms[0].elements[i].checked=chk_state;
        }
     }
    }   
  </script>
</head>
<body class="matter" onload="listen_window();" leftmargin="0" topmargin="0" bottommargin="0" rightmargin="0">
  <form id="form1" runat="server">
  <!--1st drop down menu -->                 
<div id="dropmenu1" class="dropmenudiv" style="width: 110px;">
<a href="javascript:test('LI');"><img class="img_left" alt="Any where" src= "../../Images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:test('NLI');"><img class="img_left" alt="Not In" src= "../../Images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:test('SW');"><img class="img_left" alt="Starts With" src= "../../Images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:test('NSW');"><img class="img_left" alt="Like" src= "../../Images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:test('EW');"><img class="img_left" alt="Like" src= "../../Images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:test('NEW');"><img class="img_left" alt="Like" src= "../../Images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>


 <table width="98%" align="center" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="2">
        <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
        <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
        <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
        <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
        </td>
      </tr>
      
      <tr>
        <td colspan="2">
        <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"  Width="100%" EmptyDataText="No Data" AllowPaging="True" PageSize="17" SkinID="GridViewView">
          <Columns>
            <asp:TemplateField HeaderText="Select">
            <HeaderTemplate>
              <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                value="Check All" />
            </HeaderTemplate>
            <ItemTemplate>
              <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("CCT_ID") %>' />
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Department"> 
            <HeaderTemplate>
              <table style="width: 100%; height: 100%">
                <tr>
                <td align="center" class="gridheader_text">
                    Department</td>
                </tr>
                <tr>
                <td >
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                    <td >
                      <div id="chromemenu1" class="chromestyle">
                        <ul>
                        <li><a href="#" rel="dropmenu1">
                          <img id="mnu_1_img" alt="Menu" runat="server" align="middle"  border="0" src="../../Images/operations/like.gif" /></a> </li>
                        </ul>
                      </div>
                    </td>
                    <td >
                      <asp:TextBox ID="txtCode" runat="server" Width="72px"></asp:TextBox></td>
                    <td  valign="middle">
                      <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif" OnClick="btnSearchName_Click" />&nbsp;</td>
                    </tr>
                  </table>
                </td>
                </tr>
              </table>
            </HeaderTemplate>
            <ItemTemplate>
            <asp:LinkButton ID="lbClose" runat="server" OnClick="LinkButton1_Click" Text='<%# Bind("CCT_DESCR") %>'></asp:LinkButton>
              
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DptId" Visible="False">
            <ItemTemplate>
              <asp:Label ID="lblCCT_ID" runat="server" Text='<%# Bind("CCT_ID") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
          </Columns>
        </asp:GridView>
        </td>
        <td>
        </td>
      </tr>      
   <tr>
     <td width="45%">
      <asp:CheckBox ID="chkSelAll" runat="server" AutoPostBack="True" CssClass="radiobutton"
        Text="Select All" Width="76px" />
     </td>
     <td>
     <asp:Button ID="btnFinish" runat="server" CssClass="button" Text="Finish"/></td>
   </tr>
    </table>  
 <script type="text/javascript">
  
  cssdropdown.startchrome("chromemenu1")
  
 </script>
</form>
</body>
</html>
