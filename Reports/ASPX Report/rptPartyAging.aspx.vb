Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptPartyAging
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64 
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Page.Title = OASISConstants.Gemstitle
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            'Response.Cache.SetExpires(Now.AddSeconds(-1))
            'Response.Cache.SetNoStore()
            'Response.AppendHeader("Pragma", "no-cache")
            'If Page.IsPostBack = False Then
            '    If isPageExpired() Then
            '        Response.Redirect("expired.htm")
            '    Else
            '        Session("TimeStamp") = Now.ToString
            '        ViewState("TimeStamp") = Now.ToString
            '    End If
            'End If
            UtilityObj.NoOpen(Me.Header)
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Select Case MainMnu_code
                Case "A650050" 'Party Statement
                    lblrptCaption.Text = "Creditors Ageing Analysis"
                    h_Mode.Value = "party"
                    lblBankCash.Text = "Creditor"
                    trReportType.Visible = False
                Case "A550011" 'Party Statement
                    lblrptCaption.Text = "Creditors Ageing Summary"
                    h_Mode.Value = "party"
                    lblBankCash.Text = "Creditor"
                    h_ACTTYPE.Value = "s"
                    trReportType.Visible = True
                Case "A550012" 'Party Statement
                    lblrptCaption.Text = "Creditors Ageing By Document"
                    h_Mode.Value = "party"
                    lblBankCash.Text = "Creditor"
                    trReportType.Visible = False
                    h_ACTTYPE.Value = "s"
                Case "A550019"  'Debtors Ageing By Document
                    lblrptCaption.Text = "Debtors Ageing By Document"
                    h_Mode.Value = "party"
                    lblBankCash.Text = "Debtor"
                    trReportType.Visible = False
                    h_ACTTYPE.Value = "C"
                Case "A550018"  'Debtors Ageing Summary
                    lblrptCaption.Text = "Debtors Ageing Summary"
                    h_Mode.Value = "party"
                    lblBankCash.Text = "Debtor"
                    h_ACTTYPE.Value = "C"
                    trReportType.Visible = True
            End Select

            'txtFromDate.Attributes.Add("ReadOnly", "Readonly")

            If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
                If h_Mode.Value <> "party" Then
                    h_BSUID.Value = h_BSUID.Value.Split("___")(0)
                End If
            Else
                h_BSUID.Value = Session("sBsuid")
            End If
            FillBSUNames(h_BSUID.Value)
            If h_ACTIDs.Value <> Nothing And h_ACTIDs.Value <> "" And h_ACTIDs.Value <> "undefined" Then
                FillACTIDs(h_ACTIDs.Value)
                'h_BSUID.Value = ""
            End If
            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                txtFromDate.Text = UtilityObj.GetDiplayDate()
                'If MainMnu_code = "A550011" Then
                '    radDefault.Checked = True
                'End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Try
            Dim IDs As String() = ACTIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdACTDetails.DataSource = ds
            grdACTDetails.DataBind()
            'dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            'txtBankNames.Text = ""
            'Dim bval As Boolean = dr.Read
            'While (bval)
            '    txtBankNames.Text += dr(0).ToString()
            '    bval = dr.Read()
            '    If bval Then
            '        txtBankNames.Text += "||"
            '    End If
            'End While
            'txtbankCodes.Text = ACTIDs
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            txtFromDate.Text = Format("dd/MMM/yyyy", txtFromDate.Text)
 
            Dim strXMLBSUNames As String
            'Generate the XML in the form <BSU_DETAILS><BSU_ID>IDhere</BSU_ID></BSU_DETAILS>
            h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
            strXMLBSUNames = GenerateXML(h_BSUID.Value, XMLType.BSUName) 'txtBSUNames.Text)

            Select Case MainMnu_code
                Case "A650050" 'Party Ledger
                    GeneratePartyLedger(strXMLBSUNames)
                Case "A550011", "A550012", "A550019", "A550018" 'Party Ledger
                    GenerateAgingSum(strXMLBSUNames)
            End Select

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    'Private Sub GenerateAgingConsolidation(ByVal strXMLBSUNames As String)
    '    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
    '    Dim objConn As New SqlConnection(str_conn)
    '    Dim adpt As New SqlDataAdapter
    '    Dim ds As New DataSet

    '    Dim cmd As New SqlCommand("[RPT_AGING_CONSOLIDATED]", objConn)
    '    cmd.CommandType = CommandType.StoredProcedure

    '    Dim sqlpJHD_BSU_IDs As New SqlParameter("@TRN_BSU_IDs", SqlDbType.Xml)
    '    sqlpJHD_BSU_IDs.Value = strXMLBSUNames
    '    cmd.Parameters.Add(sqlpJHD_BSU_IDs)

    '    Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
    '    sqlpACT_IDs.Value = GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
    '    cmd.Parameters.Add(sqlpACT_IDs)

    '     Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
    '    sqlpFROMDOCDT.Value = txtFromDate.Text
    '    cmd.Parameters.Add(sqlpFROMDOCDT)

    '     Dim sqlpGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
    '    sqlpGroupCurrency.Value = chkGroupCurrency.Checked
    '    cmd.Parameters.Add(sqlpGroupCurrency)


    '    Dim sqlpVouchDT As New SqlParameter("@bVouchDt", SqlDbType.Bit)
    '    sqlpVouchDT.Value = chkVoucherDate.Checked
    '    cmd.Parameters.Add(sqlpVouchDT)

    '    cmd.Connection = objConn
    '    Dim repSource As New MyReportClass
    '    Dim params As New Hashtable
    '    params("UserName") = Session("sUsr_name")
    '    params("AsOnDate") = txtFromDate.Text
    '    repSource.Parameter = params
    '    repSource.Command = cmd
    '    MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

    '    Select Case MainMnu_code
    '        Case "A550011" 'Party Ledger
    '            If radFormat1.Checked Then
    '                repSource.ResourceName = "../RPT_Files/rptAgingConsolidationReport.rpt"
    '            Else
    '                repSource.ResourceName = "../RPT_Files/rptAgingConsolidationReport_Format2.rpt"
    '            End If
    '    End Select

    '    Session("ReportSource") = repSource
    '    'Context.Items.Add("ReportSource", repSource)
    '    objConn.Close()
    '    If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
    '        Response.Redirect("rptviewer.aspx?isExport=true", True)
    '    Else
    '        Response.Redirect("rptviewer.aspx", True)
    '    End If
    '    'Else
    '    'lblError.Text = "No Records with specified condition"
    '    'End If
    '    objConn.Close()
    'End Sub

    Private Sub GeneratePartyLedger(ByVal strXMLBSUNames As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        'Dim adpt As New SqlDataAdapter
        'Dim ds As New DataSet

        Dim cmd As New SqlCommand("AGINGCHART", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@TRN_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        sqlpACT_IDs.Value = GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
        cmd.Parameters.Add(sqlpACT_IDs)

        cmd.Parameters.AddWithValue("@Bkt1", txtBkt1.Text)
        cmd.Parameters.AddWithValue("@Bkt2", txtBkt2.Text)
        cmd.Parameters.AddWithValue("@Bkt3", txtBkt3.Text)
        cmd.Parameters.AddWithValue("@Bkt4", txtBkt4.Text)
        cmd.Parameters.AddWithValue("@Bkt5", txtBkt5.Text)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        'Dim sqlpVouchDT As New SqlParameter("@bVouchDt", SqlDbType.Bit)
        'sqlpVouchDT.Value = chkVoucherDate.Checked
        'cmd.Parameters.Add(sqlpVouchDT)

        'adpt.SelectCommand = cmd
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")


        params("lblAsOnDate") = "(As on " & txtFromDate.Text & ")"
        'params("decimal") = Session("BSU_ROUNDOFF")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptAgingChart.rpt"
        Session("ReportSource") = repSource
        'Context.Items.Add("ReportSource", repSource)
        objConn.Close()
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("rptviewer.aspx?isExport=true", True)
        Else
            'Response.Redirect("rptviewer.aspx", True)
            ReportLoadSelection()
        End If
        'Else
        'lblError.Text = "No Records with specified condition"
        'End If
        objConn.Close()
    End Sub

    Private Sub GenerateAgingSum(ByVal strXMLBSUNames As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim RepType As Integer
        Dim cmd As SqlCommand
        If radDefault.Checked = True Then
            RepType = 1
        ElseIf radByUnit.Checked = True Then
            RepType = 2
        ElseIf radByParty.Checked = True Then
            RepType = 3
        Else
            RepType = 0
        End If

        If RepType = 1 Or RepType = 0 Then  'Default Statement
            Select Case MainMnu_code
                Case "A550012", "A550011"  'Creditors Default Ageing Summary 
                    If ViewState("isExport") = True Then
                        cmd = New SqlCommand("AGINGSummaryExcel")
                    Else
                        If MainMnu_code = "A550012" Then
                            cmd = New SqlCommand("AGINGDETAIL")
                        Else
                            cmd = New SqlCommand("AGINGSUMMARY")
                        End If

                    End If

                Case "A550019", "A550018"  'Debtors Default Ageing Summary
                    cmd = New SqlCommand("AGINGSUMMARYDebitor")
            End Select
            'cmd = New SqlCommand("AGINGSUMMARY")
            cmd.CommandType = CommandType.StoredProcedure
        End If

        If RepType = 2 Or RepType = 3 Then  'By Unit or By Pary Statement
            Select Case MainMnu_code
                Case "A550011"  'Creditors Ageing Sumary By Unit or By Party
                    cmd = New SqlCommand("AGINGSUMMARYByUnit")
                Case "A550018"  'Debtors Ageing Sumary By Unit or By Party
                    cmd = New SqlCommand("AGINGSUMMARYDebitorByUnit")
            End Select
            'cmd = New SqlCommand("AGINGSUMMARYByUnit")
            cmd.CommandType = CommandType.StoredProcedure
        End If

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@TRN_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        sqlpACT_IDs.Value = GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
        cmd.Parameters.Add(sqlpACT_IDs)

        cmd.Parameters.AddWithValue("@Bkt1", txtBkt1.Text)
        cmd.Parameters.AddWithValue("@Bkt2", txtBkt2.Text)
        cmd.Parameters.AddWithValue("@Bkt3", txtBkt3.Text)
        cmd.Parameters.AddWithValue("@Bkt4", txtBkt4.Text)
        cmd.Parameters.AddWithValue("@Bkt5", txtBkt5.Text)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        Dim sqlpVouchDT As New SqlParameter("@bVouchDt", SqlDbType.Bit)
        sqlpVouchDT.Value = chkVoucherDate.Checked
        cmd.Parameters.Add(sqlpVouchDT)
        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bkt1") = txtBkt1.Text
        params("Bkt2") = txtBkt2.Text
        params("Bkt3") = txtBkt3.Text
        params("Bkt4") = txtBkt4.Text
        params("Bkt5") = txtBkt5.Text
        Select Case MainMnu_code
            Case "A550012"
                params("RptTitle") = "CREDITORS AGEING ANALYSIS"
            Case "A550019"
                params("RptTitle") = "DEBTORS AGEING ANALYSIS"
            Case "A550011"
                If RepType = 1 Or RepType = 0 Then
                    params("RptTitle") = "CREDITORS AGEING ANALYSIS"
                ElseIf RepType = 2 Then
                    params("RptTitle") = "CREDITORS AGEING ANALYSIS BY UNIT"
                ElseIf RepType = 3 Then
                    params("RptTitle") = "CREDITORS AGEING ANALYSIS BY PARTY"
                End If
            Case "A550018"
                If RepType = 1 Or RepType = 0 Then
                    params("RptTitle") = "DEBTORS AGEING ANALYSIS"
                ElseIf RepType = 2 Then
                    params("RptTitle") = "DEBTORS AGEING ANALYSIS BY UNIT"
                ElseIf RepType = 3 Then
                    params("RptTitle") = "DEBTORS AGEING ANALYSIS BY PARTY"
                End If
        End Select
        params("lblAsOnDate") = "As on " & txtFromDate.Text & ""
        'params("decimal") = Session("BSU_ROUNDOFF")
        repSource.Parameter = params
        repSource.Command = cmd
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        Select Case MainMnu_code
            Case "A550011", "A550018" 'Party Ledger
                If RepType = 1 Or RepType = 0 Then
                    'repSource.ResourceName = "../RPT_Files/rptAgingSummary.rpt"
                    repSource.ResourceName = "../RPT_Files/rptAgingSummary.rpt"
                ElseIf RepType = 2 Then
                    repSource.ResourceName = "../RPT_Files/rptAgingSummaryByUnit.rpt"
                ElseIf RepType = 3 Then
                    repSource.ResourceName = "../RPT_Files/rptAgingSummaryByParty.rpt"
                End If

            Case "A550012", "A550019" 'Party Ledger
                repSource.ResourceName = "../RPT_Files/rptAgingDetail.rpt"
        End Select

        Session("ReportSource") = repSource
        'Context.Items.Add("ReportSource", repSource)
        'objConn.Close()
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("rptviewer.aspx?isExport=true", True)
        Else
            'Response.Redirect("rptviewer.aspx", True)
            ReportLoadSelection()
        End If
    End Sub


    'Generates the XML for BSUnit
    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case Type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.ACTName
                elements(0) = "ACT_DETAILS"
                elements(1) = "ACT_DETAIL"
                elements(2) = "ACT_ID"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                XMLEBSUID = xmlDoc.CreateElement(elements(2))
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        'grdBSU.DataSource = ds
        'grdBSU.DataBind()
        'txtBSUName.Text = ""
        'Dim bval As Boolean = dr.Read
        'While (bval)
        '    txtBSUName.Text += dr(0).ToString()
        '    bval = dr.Read()
        '    If bval Then
        '        txtBSUName.Text += "||"
        '    End If
        'End While
        Return True
    End Function

    'Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
    '    If h_Mode.Value = "party" Then
    '        h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
    '    ElseIf FillBSUNames(txtBSUName.Text) Then
    '        h_BSUID.Value = txtBSUName.Text
    '    End If
    '    FillBSUNames(h_BSUID.Value)
    'End Sub

    'Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
    '    grdBSU.PageIndex = e.NewPageIndex
    '    FillBSUNames(h_BSUID.Value)
    'End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            'grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(h_ACTIDs.Value)
        End If

    End Sub

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdACTDetails.PageIndexChanging
        grdACTDetails.PageIndex = e.NewPageIndex
        FillBSUNames(h_ACTIDs.Value)
    End Sub

    Protected Sub lblAddActID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddActID.Click
        'If h_Mode.Value = "party" Then
        h_ACTIDs.Value += "||" + txtBankNames.Text.Replace(",", "||")
        'ElseIf FillACTIDs(txtBSUName.Text) Then
        h_ACTIDs.Value = txtBankNames.Text
        'End If
        FillACTIDs(h_ACTIDs.Value)
    End Sub
    Private Sub ExportReportToExcel()
        'Dim ds As New DataSet
        'repSource = DirectCast(Session("ReportSource"), MyReportClass)
        'Dim objConn As SqlConnection = repSource.Command.Connection
        'Try
        '    'repSource.Command.Connection = objConn
        '    objConn.Close()
        '    objConn.Open()
        '    Dim adpt As New SqlDataAdapter
        '    adpt.SelectCommand = repSource.Command
        '    'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey
        '    adpt.SelectCommand.CommandTimeout = 0
        '    ds.Clear()
        '    adpt.Fill(ds)
        '    If (Not ds Is Nothing) AndAlso (Not ds.Tables(0).Rows.Count <= 0) Then
        '        Dim strFName As String = String.Empty

        '        If repSource.VoucherName <> "" Then
        '            strFName = repSource.VoucherName
        '        Else
        '            If Not repSource.Parameter Is Nothing Then
        '                strFName = repSource.Parameter("VoucherName")
        '            End If
        '        End If
        '        If strFName = "" Then
        '            strFName = "report" + Session("sUsr_name") + DateTime.Now.ToFileTime.ToString()
        '        End If

        '        DataSetToExcel.Convert(ds, Response, Server.MapPath("ExcelFormat.xsl"), strFName)
        '        'DataSetToExcel.Convert(ds, Response)
        '        'Dim strDirPath As String = Server.MapPath("..\..\Temp")
        '        'Dim strFName As String = String.Empty

        '        'If repSource.VoucherName <> "" Then
        '        '    strFName = repSource.VoucherName
        '        'Else
        '        '    If Not repSource.Parameter Is Nothing Then
        '        '        strFName = repSource.Parameter("VoucherName")
        '        '    End If
        '        'End If
        '        'If strFName = "" Then
        '        '    strFName = "report" + Session("sUsr_name") + DateTime.Now.ToFileTime.ToString()
        '        'End If
        '        'Dim strFileName As String = DataSetToExcel.Convert(ds, strDirPath, strFName)
        '        'Response.Clear()
        '        'Response.Charset = ""
        '        ''set the response mime type for excel
        '        'Response.ContentType = "application/vnd.ms-excel"
        '        'Response.AddHeader("Content-Disposition", "attachment; filename=" & strFName & ".xls")
        '        'Dim fStream As New System.IO.FileStream(strFileName, IO.FileMode.Open, IO.FileAccess.Read)
        '        ''     Dim bw As New System.IO.BinaryReader(fStream)
        '        'Dim byt As Byte() = New Byte(fStream.Length - 1) {}
        '        'fStream.Read(byt, 0, fStream.Length - 1)
        '        ''    byt = bw.ReadBytes(CInt(fStream.Length - 1))
        '        'fStream.Close()
        '        ''bw.Close()
        '        'Response.BinaryWrite(byt)
        '        ''Response.End()
        '        ''Response.Flush()

        '        ''Response.WriteFile(strFileName)

        '        'System.IO.File.Delete(strFileName)

        '    Else
        '        If Request.UrlReferrer <> Nothing AndAlso Request.UrlReferrer.ToString() <> "" Then
        '            Dim urlstring As String = IIf(Request.UrlReferrer.ToString().Contains("?"), "&nodata=true", "?nodata=true")
        '            Response.Redirect(Request.UrlReferrer.ToString() & urlstring)
        '        End If
        '    End If
        'Catch ex As Exception


        'Finally
        '    objConn.Close()
        'End Try

    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

    Protected Sub radByUnit_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        radDefault.Checked = False
        radByParty.Checked = False
    End Sub

    Protected Sub radByParty_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        radDefault.Checked = False
        radByUnit.Checked = False
    End Sub

    Protected Sub radDefault_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        radByUnit.Checked = False
        radByParty.Checked = False
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class