<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptChqStatusReport.aspx.vb" Inherits="Accounts_accChqCancellation" title="Untitled Page" %>
<%@ MasterType  virtualPath="~/mainMasterPage.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
 function CallPopup() {
        var sFeatures;
     
        sFeatures="dialogWidth:750px; ";
        sFeatures+="dialogHeight: 475px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var NameandCode;
        var result;
        var lstrVal;
        var bsu_id =document.getElementById('<%=ddlBSUnit.Clientid%>').value;
        if (bsu_id=="")
         {
             alert("Please Select The Business Unit");
             return false;
         }
        result = window.showModalDialog("../../Accounts/PopUp.aspx?ShowType=BANKBSU&codeorname="+document.getElementById('<%=txtBankCode.ClientId %>').value + "&BSU_ID=" + bsu_id,"", sFeatures);                                 
        if (result=='' || result==undefined)
        {    return false;      } 
        else
        {
            lstrVal=result.split('||');     
            document.getElementById('<%=txtBankCode.ClientId %>').value=lstrVal[0];
            document.getElementById('<%=txtBankDescr.ClientId %>').value=lstrVal[1];  
        }
        document.getElementById('<%=txtChqBook.ClientID %>').value='';
        document.getElementById('<%=txtChqNo.ClientID %>').value='';
        return false;
    }   
function getCheque()
    { 
        var sFeatures;
        sFeatures="dialogWidth: 750px; ";
        sFeatures+="dialogHeight: 475px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
    
      if (document.getElementById('<%=txtBankCode.ClientId %>').value=="")
       {
           alert("Please Select The Bank");
           return false;
       } 
      var bsu_id =document.getElementById('<%=ddlBSUnit.Clientid%>').value;
       result = window.showModalDialog("../../Accounts/ShowChqsPDC.aspx?ShowType=PDCCHQBOOK&BankCode="+document.getElementById('<%=txtBankCode.ClientId %>').value +"&MonthsReq=1&docno=1&BSU_ID=" + bsu_id,"", sFeatures);
       if (result=='' || result==undefined)
       { return false; } 
       lstrVal=result.split('||');     
       document.getElementById('<%=txtChqBook.ClientID %>').value=lstrVal[0];
       document.getElementById('<%=txtChqNo.ClientID %>').value=lstrVal[1];
//       document.getElementById(ctrl).focus();
    }
</script>

    &nbsp;
      <table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 90%;">
        <tr >
            <td align="left" colspan="8">
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="CHQ_CANCEL" />
               
            </td>
        </tr>
        </table> 
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0"
        style="width: 90%;">
        <tr class="subheader_img">
            <td align="left" colspan="8" style="height: 19px">
                <asp:Label ID="lblrptCaption" runat="server" Text="Cheque Cancellation"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" valign="top">
                Business Unit</td>
            <td align="left" class="matters" valign="top">
                :</td>
            <td align="left" class="matters">
                <asp:DropDownList id="ddlBSUnit" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="matters" valign="top">
                Bank</td>
            <td align="left" class="matters" valign="top">
                :</td>
            <td align="left" class="matters">
                <asp:TextBox ID="txtBankCode" runat="server" Width="115px"></asp:TextBox>
                <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" 
                   OnClientClick="CallPopup();return false;" />
                <asp:TextBox ID="txtBankDescr" runat="server" Width="417px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBankDescr"
                    ErrorMessage="Bank Required" ValidationGroup="CHQ_CANCEL">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td align="left" class="matters" valign="top">
                Cheque Lot</td>
            <td align="left" class="matters" valign="top">
                :</td>
            <td align="left" class="matters">
                <asp:TextBox ID="txtChqBook" runat="server" Style="left: -6px; top: 0px" Width="30%"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgCHQAllote" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getCheque();" />
                <asp:TextBox ID="txtChqNo" runat="server" Style="left: -6px; top: 0px" Width="20%"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtChqNo"
                    ErrorMessage="Cheque Lot Required" ValidationGroup="CHQ_CANCEL">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="text-align: right">
                &nbsp; &nbsp;&nbsp;
                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Generate Report"
                    ValidationGroup="bankPayment" />&nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="button"
                        Text="Cancel" UseSubmitBehavior="False" Width="72px" /></td>
        </tr>
    </table>
          <asp:HiddenField ID="hCheqBook" runat="server" />    
</asp:Content>

