Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class Reports_ASPX_Report_rptOasisAudit
    Inherits System.Web.UI.Page
    Shared bsu_ID As String
    Shared TxtDate As String
    Shared MainMnu_code As String
    Shared datamode As String = "none"
    Shared menu_rights As Integer
    Dim Encr_decrData As New Encryption64
    Shared str_SqlTD As String
    Shared str_SqlSUM As String

   
    Protected Sub cvDateFrom_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvDateFrom.ServerValidate
        Dim sflag As Boolean = False
        Dim DateTime1 As Date
        Try
            'convert the date into the required format so that it can be validate by Isdate function
            DateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date
            If IsDate(DateTime1) Then
                sflag = True
            End If

            If sflag Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        Catch
            'catch when format string through an error
            args.IsValid = False
        End Try

    End Sub

    Protected Sub cvTodate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvTodate.ServerValidate
        Dim sflag As Boolean = False
        Dim dateTime1 As Date
        Dim DateTime2 As Date
        Try
            dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            DateTime2 = Date.ParseExact(txtToDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

            'check for the leap year date

            If IsDate(DateTime2) Then

                If DateTime2 < dateTime1 Then
                    sflag = False
                Else
                    sflag = True
                End If
            End If
            If sflag Then
                args.IsValid = True
            Else
                'CustomValidator1.Text = ""
                args.IsValid = False
            End If
        Catch
            args.IsValid = False
        End Try
    End Sub

   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'hardcode the menu code
            If USR_NAME = "" Or MainMnu_code <> "D050035" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else

                'menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                Dim str1 As String = String.Empty
                Dim str2 As String = String.Empty

                bsu_ID = Session("sBsuid")

                txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                Using UserreaderDocName As SqlDataReader = AccessRoleUser.GetDocName_Audit()
                    If UserreaderDocName.HasRows Then
                        While UserreaderDocName.Read
                            str1 = Convert.ToString(UserreaderDocName("Dname"))
                            str2 = Convert.ToString(UserreaderDocName("Dtype"))

                            chkDocNo.Items.Add(New ListItem(str1, str2))

                        End While

                    End If

                End Using
                TABLE2.Visible = False
                TABLE3.Visible = False
                TABLE4.Visible = False
            End If

        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        

        If Page.IsValid Then

           
            Call gridbindgvDocType()


        End If
    End Sub
    Private Sub gridbindgvDocType()
        Dim fromDate As Date
        Dim todate As Date
        Dim chkFlag As Boolean
        Dim strfilter As String = String.Empty
        Try
            If (txtFromDate.Text <> "") And (txtFromDate.Text <> String.Empty) Then
                fromDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", txtFromDate.Text)

            Else
                fromDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", DateTime.MinValue)
            End If
            If (txtToDate.Text <> "") And (txtToDate.Text <> String.Empty) Then
                Dim Tdate As Date = CType(txtToDate.Text, Date)
                Tdate = Tdate.AddDays(1)
                todate = CType(Tdate, String)
                '  todate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", todate)
            Else
                todate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", DateTime.Now)
            End If

            If ((txtFromDate.Text <> "") And (txtFromDate.Text <> String.Empty)) Or _
           ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then

                strfilter = " AND AUD_DT BETWEEN '" + fromDate + "' AND '" + todate + "'"

                TxtDate = strfilter
            End If

            For Each item As ListItem In chkDocNo.Items
                If (item.Selected) Then
                    chkFlag = True
                End If
            Next

            If chkFlag = True Then
                strfilter = strfilter + " and vw_OSA_DOCUMENT_M.DOC_ID IN ( "
                Dim strAddtxt As String = String.Empty

                For Each item As ListItem In chkDocNo.Items
                    If (item.Selected) Then
                        strAddtxt = strAddtxt + "'" + item.Value + "'" + ","

                    End If
                Next
                If Right(strAddtxt, 1) = "," Then
                    strAddtxt = Left(strAddtxt, Len(strAddtxt) - 1)
                End If

                If Left(strAddtxt, 1) = "," Then
                    strAddtxt = Mid(strAddtxt, 2)
                End If

                strfilter += strAddtxt + ")"
            End If


            Using str_conn As SqlConnection = ConnectionManger.GetOASISAuditConnection


                Try

                    Dim ds As New DataSet

                    Dim str_sql As String = " SELECT   vw_OSA_DOCUMENT_M.DOC_NAME as Doc_name, COUNT(*) AS Modified,vw_OSA_DOCUMENT_M.DOC_ID as DOC_ID from " & _
        " AUDITDATA INNER JOIN  vw_OSA_DOCUMENT_M ON AUDITDATA.AUD_DOCTYPE = vw_OSA_DOCUMENT_M.DOC_ID " & _
        " where AUDITDATA.Aud_BSU_ID='" & bsu_ID & "'" + strfilter + " GROUP BY  vw_OSA_DOCUMENT_M.DOC_NAME,vw_OSA_DOCUMENT_M.DOC_ID"
                    '''''''''


                    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
                    gvDocType.DataSource = ds.Tables(0)
                    If ds.Tables(0).Rows.Count > 0 Then

                        gvDocType.DataBind()
                    Else
                        ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                        gvDocType.DataBind()
                        Dim columnCount As Integer = gvDocType.Rows(0).Cells.Count
                        'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                        gvDocType.Rows(0).Cells.Clear()
                        gvDocType.Rows(0).Cells.Add(New TableCell)
                        gvDocType.Rows(0).Cells(0).ColumnSpan = columnCount
                        gvDocType.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                        gvDocType.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                    End If
                    TABLE2.Visible = True
                    TABLE3.Visible = False
                    TABLE4.Visible = False
                    gvDoctypeDetail.Visible = False
                    gvSummary.Visible = False
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, "oasisAuditpage")

                    lblError.Text = UtilityObj.getErrorMessage("1000")

                End Try

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "oasisAuditpage")
            lblError.Text = UtilityObj.getErrorMessage("1000")

        End Try
    End Sub
    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)



        Try
            TABLE3.Visible = True
            gvDoctypeDetail.Visible = True





            Dim lblDocNo As New Label
            gvDocType.SelectedIndex = sender.parent.parent.RowIndex

            '        Dim lblGrpCode As New Label
            lblDocNo = TryCast(sender.FindControl("lblDocNo"), Label)




            str_SqlTD = "SELECT COUNT(*) AS Modified, vw_OSA_DOCUMENT_M.DOC_NAME as Doc_name ,AUDITDATA.AUD_DOCNO as DOCNO FROM AUDITDATA INNER JOIN vw_OSA_DOCUMENT_M ON AUDITDATA.AUD_DOCTYPE = vw_OSA_DOCUMENT_M.DOC_ID where vw_OSA_DOCUMENT_M.DOC_ID='" & lblDocNo.Text & "' " & _
    " and AUDITDATA.Aud_BSU_ID='" & bsu_ID & "'" + TxtDate + "GROUP BY  vw_OSA_DOCUMENT_M.DOC_NAME, AUDITDATA.AUD_DOCNO order by vw_OSA_DOCUMENT_M.DOC_NAME "

            gridbindgvDocTypeDetail()
            
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "oasisAuditpage")

            lblError.Text = UtilityObj.getErrorMessage("1000")

        End Try


    End Sub
    Private Sub gridbindgvDocTypeDetail()
        Using str_conn As SqlConnection = ConnectionManger.GetOASISAuditConnection
            Try

            
                Dim ds As New DataSet

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_SqlTD)
                gvDoctypeDetail.DataSource = ds.Tables(0)
                If ds.Tables(0).Rows.Count > 0 Then

                    gvDoctypeDetail.DataBind()
                Else
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                    gvDoctypeDetail.DataBind()
                    Dim columnCount As Integer = gvDoctypeDetail.Rows(0).Cells.Count
                    'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                    gvDoctypeDetail.Rows(0).Cells.Clear()
                    gvDoctypeDetail.Rows(0).Cells.Add(New TableCell)
                    gvDoctypeDetail.Rows(0).Cells(0).ColumnSpan = columnCount
                    gvDocType.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                    gvDoctypeDetail.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "oasisAuditpage")

                lblError.Text = UtilityObj.getErrorMessage("1000")

            End Try
        End Using


    End Sub

    Protected Sub lbDocDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Try
            TABLE4.Visible = True
            gvSummary.Visible = True

            Dim lblDocNo As New Label
            gvDoctypeDetail.SelectedIndex = sender.parent.parent.RowIndex

            '        Dim lblGrpCode As New Label
            lblDocNo = TryCast(sender.FindControl("lblDocNo"), Label)


            str_SqlSUM = "SELECT  AUDITDATA.AUD_DOCTYPE as DOCTYPE,vw_OSA_DOCUMENT_M.DOC_NAME as Doc_name,AUDITDATA.AUD_ID as ID, AUDITDATA.AUD_DOCNO as DocNo, AUDITDATA.AUD_LOGDT as LogDT,AUDITDATA.AUD_DT as AudDT, AUDITDATA.AUD_USER as AudUser, " & _
                "  AUDITDATA.AUD_REMARKS as Remark FROM AUDITDATA INNER JOIN vw_OSA_DOCUMENT_M ON AUDITDATA.AUD_DOCTYPE = vw_OSA_DOCUMENT_M.DOC_ID " & _
" WHERE    AUDITDATA.Aud_BSU_ID='" & bsu_ID & "'" + TxtDate + "AND AUDITDATA.AUD_DOCNO = '" & lblDocNo.Text & "'  ORDER BY AUDITDATA.AUD_ID DESC"



            gridbindgvDocTypeSummary()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "oasisAuditpage")

            lblError.Text = UtilityObj.getErrorMessage("1000")

        End Try


    End Sub
    Private Sub gridbindgvDocTypeSummary()
        Using str_conn As SqlConnection = ConnectionManger.GetOASISAuditConnection

            Try

            

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_SqlSUM)
                gvSummary.DataSource = ds.Tables(0)
                If ds.Tables(0).Rows.Count > 0 Then

                    gvSummary.DataBind()
                Else
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                    gvSummary.DataBind()
                    Dim columnCount As Integer = gvSummary.Rows(0).Cells.Count
                    'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                    gvSummary.Rows(0).Cells.Clear()
                    gvSummary.Rows(0).Cells.Add(New TableCell)
                    gvSummary.Rows(0).Cells(0).ColumnSpan = columnCount
                    gvSummary.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                    gvSummary.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                End If
            Catch ex As Exception

                UtilityObj.Errorlog(ex.Message, "oasisAuditpage")

                lblError.Text = UtilityObj.getErrorMessage("1000")

            End Try
        End Using
    End Sub
    
    Protected Sub gvDocType_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDocType.PageIndexChanging
        gvDocType.PageIndex = e.NewPageIndex
        gridbindgvDocType()
    End Sub

    Protected Sub gvDoctypeDetail_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDoctypeDetail.PageIndexChanging
        gvDoctypeDetail.PageIndex = e.NewPageIndex
        gridbindgvDocTypeDetail()
    End Sub

    Protected Sub gvSummary_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSummary.PageIndexChanging
        gvSummary.PageIndex = e.NewPageIndex
        gridbindgvDocTypeSummary()
    End Sub

    Protected Sub lbAfterChange_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblID As New Label
            Dim lblDocType As New Label
            Dim lblDocNo As New Label
            Dim lblDocName As New Label
            Dim url As String = String.Empty
            ' Dim viewid As String
            Dim Aud_ID As String = String.Empty
            Dim DType As String = String.Empty
            Dim DocNo As String = String.Empty
            Dim DocName As String = String.Empty
            Dim Sel_Flag As String = String.Empty

            lblID = TryCast(sender.FindControl("lblID"), Label)
            lblDocType = TryCast(sender.FindControl("lblDocType"), Label)
            lblDocNo = TryCast(sender.FindControl("lblDocNo"), Label)
            lblDocName = TryCast(sender.findControl("lblDocName"), Label)
            Aud_ID = lblID.Text
            DType = lblDocType.Text
            DocNo = lblDocNo.Text
            DocName = lblDocName.Text


            'define the datamode to view if view is clicked
            'datamode = "view"
            'Encrypt the data that needs to be send through Query String

            MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)

            Aud_ID = Encr_decrData.Encrypt(Aud_ID)
            DType = Encr_decrData.Encrypt(DType)
            Sel_Flag = Encr_decrData.Encrypt("A")
            DocName = Encr_decrData.Encrypt(DocName)
            DocNo = Encr_decrData.Encrypt(DocNo)
            Select Case Encr_decrData.Decrypt(DType)
                Case "CN", "BP", "BR", "CC", "CR", "CP", "CD", "DN"
                    url = String.Format("oasisAudit\rptDisplayAudit.aspx?MainMnu_code={0}&Aud_ID={1}&Dtype={2}&Sel_Flag={3}&DocNo={4}&DocName={5}", MainMnu_code, Aud_ID, DType, Sel_Flag, DocNo, DocName)
                Case "JV"
                    url = String.Format("oasisAudit\rptDisplayAuditJ.aspx?MainMnu_code={0}&Aud_ID={1}&Dtype={2}&Sel_Flag={3}&DocNo={4}&DocName={5}", MainMnu_code, Aud_ID, DType, Sel_Flag, DocNo, DocName)
            End Select

            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub lbBefore_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblID As New Label
            Dim lblDocType As New Label
            Dim lblDocNo As New Label
            Dim lblDocName As New Label
            Dim url As String = String.Empty
            ' Dim viewid As String
            Dim Aud_ID As String = String.Empty
            Dim DType As String = String.Empty
            Dim DocNo As String = String.Empty
            Dim DocName As String = String.Empty
            Dim Sel_Flag As String = String.Empty

            lblID = TryCast(sender.FindControl("lblID"), Label)
            lblDocType = TryCast(sender.FindControl("lblDocType"), Label)
            lblDocNo = TryCast(sender.FindControl("lblDocNo"), Label)
            lblDocName = TryCast(sender.findControl("lblDocName"), Label)
            Aud_ID = lblID.Text
            DType = lblDocType.Text
            DocNo = lblDocNo.Text
            DocName = lblDocName.Text


            'define the datamode to view if view is clicked
            'datamode = "view"
            'Encrypt the data that needs to be send through Query String

            MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)

            Aud_ID = Encr_decrData.Encrypt(Aud_ID)
            DType = Encr_decrData.Encrypt(DType)
            Sel_Flag = Encr_decrData.Encrypt("B")
            DocNo = Encr_decrData.Encrypt(DocNo)
            DocName = Encr_decrData.Encrypt(DocName)
            Select Case Encr_decrData.Decrypt(DType)
                Case "CN", "BP", "BR", "CC", "CR", "CP", "CD", "DN"
                    url = String.Format("oasisAudit\rptDisplayAudit.aspx?MainMnu_code={0}&Aud_ID={1}&Dtype={2}&Sel_Flag={3}&DocNo={4}&DocName={5}", MainMnu_code, Aud_ID, DType, Sel_Flag, DocNo, DocName)
                Case "JV"
                    url = String.Format("oasisAudit\rptDisplayAuditJ.aspx?MainMnu_code={0}&Aud_ID={1}&Dtype={2}&Sel_Flag={3}&DocNo={4}&DocName={5}", MainMnu_code, Aud_ID, DType, Sel_Flag, DocNo, DocName)
            End Select

            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
End Class
