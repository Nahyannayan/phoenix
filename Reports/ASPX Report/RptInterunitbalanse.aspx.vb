Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports System.Data.SqlTypes
Imports UtilityObj
Partial Class Reports_ASPX_Report_RptInterunitbalanse
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("MainMnu_code") = MainMnu_code
            Page.Title = OASISConstants.Gemstitle

            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            End If


            lblrptCaption.Text = "Inter Unit Balances"
            If MainMnu_code = "A200365" Then
                lblrptCaption.Text = "Inter Unit Reconciliation"
            End If
            'A200360
            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                txtToDate.Text = UtilityObj.GetDiplayDate()
                'txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))
                PopulateTree()
                'txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
                txtToDate.Attributes.Add("onBlur", "checkdate(this)")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub PopulateRootLevel()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        tvBusinessunit.Nodes.Clear()
        str_Sql = "SELECT  0 AS BSU_ID,'All' AS BSU_NAME,  COUNT (*)  AS childnodecount   FROM BUSSEGMENT_M BSG"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), tvBusinessunit.Nodes)
    End Sub


    Private Sub PopulateNodes(ByVal dt As DataTable, _
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("BSU_NAME").ToString()
            tn.Value = dr("BSU_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            If tn.Value = Session("sBsuid") Then
                tn.Checked = True
            End If
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub


    Private Sub PopulateTree() 'Generate Tree
        PopulateRootLevel()
        tvBusinessunit.DataBind()
        tvBusinessunit.CollapseAll()
    End Sub


    Protected Sub tvBusinessunit_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvBusinessunit.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateSubLevel(str, e.Node)
    End Sub


    Private Sub PopulateSubLevel(ByVal parentid As String, _
        ByVal parentNode As TreeNode)
        PopulateNodes(GetBsuTree_WithRights(Session("sBusper"), Session("sUsr_id"), parentid, ViewState("MainMnu_code")), parentNode.ChildNodes)

        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim str_Sql As String
        'If parentid = "0" Then
        '    If Session("sBusper") = True Then
        '        str_Sql = " SELECT     BSG_ID AS BSU_ID,'� � �' +BSG_DESCR+'� � �' AS BSU_NAME,BSG_ID, " _
        '                        & " (SELECT COUNT (*) FROM BUSINESSUNIT_M WHERE BUS_BSG_ID=  BSG.BSG_ID) AS childnodecount " _
        '                        & " FROM BUSSEGMENT_M BSG "
        '    Else
        '        str_Sql = "SELECT '� � �' + BSG.BSG_DESCR + '� � �' AS BSU_NAME, COUNT(*) AS childnodecount, BSG.BSG_ID AS BSU_ID " _
        '          & " FROM BUSINESSUNIT_M AS BSU INNER JOIN BUSSEGMENT_M AS BSG ON BSU.BUS_BSG_ID = BSG.BSG_ID " _
        '          & " and BSU_ID IN(SELECT USA.USA_BSU_ID AS BSU_ID FROM  USERACCESS_S AS USA INNER JOIN" _
        '          & " BUSINESSUNIT_M AS BSU ON USA.USA_BSU_ID = BSU.BSU_ID " _
        '          & " WHERE (USA.USA_USR_ID = '" & Session("sUsr_id") & "')  union SELECT  USR.USR_BSU_ID AS BSU_ID " _
        '          & " FROM USERS_M AS USR INNER JOIN BUSINESSUNIT_M " _
        '          & " AS BSU ON USR.USR_BSU_ID = BSU.BSU_ID WHERE (USR.USR_ID = '" & Session("sUsr_id") & "') )" _
        '          & " GROUP BY BSG.BSG_DESCR, BSG.BSG_ID"
        '    End If
        'Else
        '    If Session("sBusper") = True Then
        '        str_Sql = "SELECT  BSU_ID, BSU_NAME AS BSU_NAME,  0 AS childnodecount " _
        '                & " FROM BUSINESSUNIT_M WHERE BUS_BSG_ID =  '" & parentid & "'"
        '    Else
        '        str_Sql = "SELECT  BSU_ID, BSU_NAME AS BSU_NAME,  0 AS childnodecount " _
        '               & " FROM BUSINESSUNIT_M WHERE BUS_BSG_ID =  '" & parentid & "'" _
        '          & " and BSU_ID IN(SELECT USA.USA_BSU_ID AS BSU_ID FROM  USERACCESS_S AS USA INNER JOIN" _
        '          & " BUSINESSUNIT_M AS BSU ON USA.USA_BSU_ID = BSU.BSU_ID " _
        '          & " WHERE (USA.USA_USR_ID = '" & Session("sUsr_id") & "')  union SELECT  USR.USR_BSU_ID AS BSU_ID " _
        '          & " FROM USERS_M AS USR INNER JOIN BUSINESSUNIT_M " _
        '          & " AS BSU ON USR.USR_BSU_ID = BSU.BSU_ID WHERE (USR.USR_ID = '" & Session("sUsr_id") & "') )"
        '    End If
        'End If
        'Dim ds As New DataSet
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        'PopulateNodes(ds.Tables(0), parentNode.ChildNodes)
    End Sub


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
            If MainMnu_code = "A200365" Then
                Reconciliation()
            Else
                GenerateRPT()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub GenerateRPT()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        Dim str_bsu_ids As New StringBuilder

        For Each node As TreeNode In tvBusinessunit.CheckedNodes
            If node.Value.Length > 2 Then
                str_bsu_ids.Append(node.Value)
                str_bsu_ids.Append("|")
            End If
        Next

        Dim cmd As New SqlCommand("RptGetMISInterunitData", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 400)
        sqlpJHD_BSU_IDs.Value = str_bsu_ids.ToString
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        'adpt.SelectCommand = cmd
        objConn.Close()
        objConn.Open()

        If 1 = 1 Then
            Dim str_bsu_Segment As String = ""
            Dim str_bsu_shortname As String = ""
            Dim str_bsu_CURRENCY As String = ""
            getBsuSegmentSplit(str_bsu_ids.ToString, str_bsu_Segment, _
            str_bsu_shortname, str_bsu_CURRENCY)

            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("ReportHeading") = "INTER UNIT BALANCES (Columnar) " & txtToDate.Text.ToString()
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/InterunitBal.rpt"
            Session("ReportSource") = repSource

            objConn.Close()
            Response.Redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub
    Private Sub Reconciliation()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        Dim str_bsu_ids As New StringBuilder

        For Each node As TreeNode In tvBusinessunit.CheckedNodes
            If node.Value.Length > 2 Then
                str_bsu_ids.Append(node.Value)
                str_bsu_ids.Append("|")
            End If
        Next

        Dim cmd As New SqlCommand("RptGetInterunitRecon", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 400)
        sqlpJHD_BSU_IDs.Value = str_bsu_ids.ToString
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        'adpt.SelectCommand = cmd
        objConn.Close()
        objConn.Open()

        If 1 = 1 Then
            Dim str_bsu_Segment As String = ""
            Dim str_bsu_shortname As String = ""
            Dim str_bsu_CURRENCY As String = ""
            getBsuSegmentSplit(str_bsu_ids.ToString, str_bsu_Segment, _
            str_bsu_shortname, str_bsu_CURRENCY)
            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("ReportHead") = "Inter Unit Reconciliation As on " & txtToDate.Text.ToString()
            params("UserName") = Session("sUsr_name")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/RptGetInterunitRecon.rpt"
            Session("ReportSource") = repSource

                objConn.Close()
                Response.Redirect("rptviewer.aspx", True)
            Else
                lblError.Text = "No Records with specified condition"
        End If

        objConn.Close()
    End Sub


    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub


End Class
