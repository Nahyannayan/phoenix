Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports System.Xml
Partial Class Reports_ASPX_Report_CostcenterDepartment
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = OASISConstants.Gemstitle
        If IsPostBack = False Then
            'txtToDate.Attributes.Add("ReadOnly", "Readonly")
            'txtFromDate.Attributes.Add("ReadOnly", "Readonly")
            gvDptdetails.Attributes.Add("bordercolor", "#1b80b6")
            imgPickDpt.Attributes.Add("onclick", "return getFilter('" & txtDepartment.ClientID & "','" & h_Departmentid.ClientID & "','DPT')")
            ChkDetails.Attributes.Add("onclick", "return ChkBox('" & ChkMonthAc.ClientID & "','" & ChkMonthly.ClientID & "')")
            ChkMonthAc.Attributes.Add("onclick", "return ChkBox('" & ChkDetails.ClientID & "','" & ChkMonthly.ClientID & "')")
            ChkMonthly.Attributes.Add("onclick", "return ChkBox('" & ChkDetails.ClientID & "','" & ChkMonthAc.ClientID & "')")
            If (Request.QueryString("MainMnu_code") <> "") Then
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                ViewState("MainMnu_code") = "A350050"
            End If
            UsrBSUnits1.MenuCode = ViewState("MainMnu_code")
            txtToDate.Text = UtilityObj.GetDiplayDate()
            txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If h_Departmentid.Value = "" Then
            lblError.Text = "Please Select Department..!"
            Exit Sub
        End If
        If ChkMonthly.Checked Then
            RPTGetCostCenterAnalysisMonthly()
        ElseIf ChkMonthAc.Checked Then
            RPTGetCostCenterAnalysisMonthly()
        Else
            RPTGetCostCenterAnalysis()
        End If
    End Sub

    Private Sub RPTGetCostCenterAnalysisMonthly()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As SqlCommand
        Dim level As Integer = 0
        Dim Misparams As New MisParameters
        Try
            Dim bsuUnits As String = UsrBSUnits1.GetSelectedNode()
            bsuUnits = bsuUnits.Replace("||", "|")

            If ChkMonthAc.Checked Then
                level = 1
            End If
            cmd = New SqlCommand("RPTGetCostCenterByDEpartmentMonthly", objConn)
            cmd.CommandType = CommandType.StoredProcedure


            Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
            sqlpJHD_BSU_IDs.Value = bsuUnits
            cmd.Parameters.Add(sqlpJHD_BSU_IDs)

            Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
            sqlpFROMDOCDT.Value = txtFromDate.Text
            cmd.Parameters.Add(sqlpFROMDOCDT)

            Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
            sqlpTODOCDT.Value = txtToDate.Text
            cmd.Parameters.Add(sqlpTODOCDT)

            Dim sqlpDTP_ID As New SqlParameter("@DTP_ID", SqlDbType.VarChar)
            sqlpDTP_ID.Value = h_Departmentid.Value.ToString().Replace("||", "|") 'h_Departmentid.Value ' "105|124|" '
            cmd.Parameters.Add(sqlpDTP_ID)

            Dim sqlpLevel As New SqlParameter("@Lvl", SqlDbType.TinyInt)
            sqlpLevel.Value = level
            cmd.Parameters.Add(sqlpLevel)

            adpt.SelectCommand = cmd
            objConn.Close()
            objConn.Open()
            adpt.Fill(ds)
            'Exit Sub

            If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
                cmd.Connection = New SqlConnection(str_conn)

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("ToDate") = txtToDate.Text.ToString()
                'params("DTTO") = txtToDate.Text.ToString()
                params("ReportHeading") = "Monthly Cost Analysis By Department for the period  " + txtFromDate.Text.ToString() + "..To.." + txtToDate.Text.ToString()
                repSource.ResourceName = "../RPT_Files/CostcenterDepartmentReportMonth.rpt"

                If ChkMonthAc.Checked Then
                    params("ReportHeading") = "Monthly Cost Analysis By Account for the period  " + txtFromDate.Text.ToString() + "..To.." + txtToDate.Text.ToString()
                    repSource.ResourceName = "../RPT_Files/CostcenterDepartmentReportMonthlevel2.rpt"
                End If
                repSource.Parameter = params
                repSource.Command = cmd
                Session("ReportSource") = repSource
                objConn.Close()
                Response.Redirect("rptviewer.aspx", True)
            Else
                lblError.Text = "No Records with specified condition"
            End If
            objConn.Close()
        Catch ex As Exception
            objConn.Close()
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub RPTGetCostCenterAnalysis()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As SqlCommand
        Dim level As Integer = 1
        Dim Misparams As New MisParameters
        Try
            Dim bsuUnits As String = UsrBSUnits1.GetSelectedNode()
            bsuUnits = bsuUnits.Replace("||", "|")

            If ChkDetails.Checked Then
                level = 2
            End If
            cmd = New SqlCommand("RPTGetCostCenterByDEpartment", objConn)
            cmd.CommandType = CommandType.StoredProcedure


            Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 500)
            sqlpJHD_BSU_IDs.Value = bsuUnits
            cmd.Parameters.Add(sqlpJHD_BSU_IDs)

            Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
            sqlpFROMDOCDT.Value = txtFromDate.Text
            cmd.Parameters.Add(sqlpFROMDOCDT)

            Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
            sqlpTODOCDT.Value = txtToDate.Text
            cmd.Parameters.Add(sqlpTODOCDT)

            Dim sqlpDTP_ID As New SqlParameter("@DTP_ID", SqlDbType.VarChar)
            sqlpDTP_ID.Value = h_Departmentid.Value.ToString().Replace("||", "|")   '
            cmd.Parameters.Add(sqlpDTP_ID)

            Dim sqlpLevel As New SqlParameter("@Level", SqlDbType.TinyInt)
            sqlpLevel.Value = level
            cmd.Parameters.Add(sqlpLevel)

            Dim sqlpRSS_CODE As New SqlParameter("@RSS_CODE", SqlDbType.VarChar, 20)
            sqlpRSS_CODE.Value = ""
            cmd.Parameters.Add(sqlpRSS_CODE)

            Dim sqlpRSB_ID As New SqlParameter("@RSB_ID", SqlDbType.VarChar, 20)
            sqlpRSB_ID.Value = DBNull.Value
            cmd.Parameters.Add(sqlpRSB_ID)

            adpt.SelectCommand = cmd
            objConn.Close()
            objConn.Open()
            adpt.Fill(ds)
            If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
                cmd.Connection = New SqlConnection(str_conn)

                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                params("VoucherName") = ""
                params("ReportHeading") = "Cost Summary for the month of " & Convert.ToDateTime(txtToDate.Text).ToString("MMMM-yyyy")
                repSource.ResourceName = "../RPT_Files/CostcenterDepartmentReport.rpt"
                If ChkDetails.Checked Then
                    repSource.ResourceName = "../RPT_Files/CostcenterDepartmentReportlevel2.rpt"
                    params("ReportHeading") = "Cost Detail for the month of " & Convert.ToDateTime(txtToDate.Text).ToString("MMMM-yyyy")
                End If
                repSource.Parameter = params
                repSource.Command = cmd

                Misparams.FromDate = txtFromDate.Text
                Misparams.ToDate = txtToDate.Text
                Misparams.RSSPARENT = h_Departmentid.Value
                Misparams.BSU_IDs = bsuUnits.ToString()

                Session("Misparams") = Misparams
                Session("ReportSource") = repSource

                objConn.Close()
                Response.Redirect("rptviewer.aspx", True)
            Else
                lblError.Text = "No Records with specified condition"
            End If
            objConn.Close()
        Catch ex As Exception
            objConn.Close()
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        UsrBSUnits1.ExpandOne(1)
    End Sub

    Protected Sub txtDepartment_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtDepartment.Text = ""
        FillDepartment(h_Departmentid.Value)
    End Sub

    Private Sub FillDepartment(ByVal STU_IDs As String)
        gvDptdetails.DataSource = Nothing
        gvDptdetails.DataBind()
        Dim IDs As String() = STU_IDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = " SELECT DPT_ID, DPT_DESCR FROM  OASIS.dbo.DEPARTMENT_M WHERE DPT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDptdetails.DataSource = ds
        gvDptdetails.DataBind()

    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub gvDptdetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvDptdetails.PageIndex = e.NewPageIndex()
        FillDepartment(h_Departmentid.Value)
    End Sub

End Class
