<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPDCVoucher.aspx.vb" Inherits="Reports_ASPX_Report_rptPDCVoucher" title="Untitled Page" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">
      
           function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 229px; ";
            sFeatures+="dialogHeight: 234px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx","", sFeatures);
            if (result=='' || result==undefined)
            {
                return false;
            }
            switch(txtControl)
            {
              case 0:
                document.getElementById('<%=txtfromDate.ClientID %>').value=result;
                break;
              
            }    
           }
   
         function GetBSUName()
          {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 450px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx","", sFeatures)
            if(result != "")
            {
              document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
              document.forms[0].submit();
            }
         }
    </script>
        
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0"
        style="width: 70%" >
        <tr class="subheader_img">
            <td align="left" colspan="19" style="height: 19px" valign="middle">                
              <asp:Label ID="lblCaption" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters">
                <asp:Label runat="server" Text="As On" id = "lblFromdate"></asp:Label></td>
            <td class="matters" >
                :</td>
            <td align="left" class="matters" colspan="6" style="width: 189px; height: 1px; text-align: left">
                <asp:TextBox ID="txtFromDate" runat="server" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
              
              
              <td align="left" class="matters" runat="server" id ="TrTodate" visible="false" >
                <asp:Label runat="server" Text="To Dtae" id = "lblTodate"></asp:Label></td>
            <td class="matters" runat="server" id ="TrTodate1" visible="false">
                :</td>
            <td align="left" class="matters" colspan="6" runat="server" id ="TrTodate2"  visible="false" style="width: 189px; height: 1px; text-align: left">
                <asp:TextBox ID="txtToDate" runat="server" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
                    
        </tr>
        <tr style="color: #1b80b6" id = "trBSUName" runat = "server">
            <td align="left" valign = "top" class="matters" >
                Business Unit</td>
            <td class="matters" valign = "top">
                :</td>
            <td align="left" class="matters" colspan="15" valign="top">
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
            </td>
        </tr>
        <tr style="color: #1b80b6" id = "trChkBoxes" runat = "server">
            <td align="left" class="matters" colspan="19" style="height: 7px">
                <asp:CheckBox ID="chkGroupCurrency" runat="server" Text="View in Group Currency" /><br />
                <asp:CheckBox ID="chkConsolidation" runat="server" Text="Consolidation" /></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="19" style="text-align: right">
                <asp:LinkButton id="lnkExporttoexcel" runat="server" onclick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table> <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:HiddenField ID="h_BSUID" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtToDate" PopupButtonID="imgToDate">
    </ajaxToolkit:CalendarExtender>
</asp:Content>

