<%@ Page Language="VB" MasterPageFile="~/Management/Management.master" AutoEventWireup="false" CodeFile="rptReportViewerMIS.aspx.vb" Inherits="Reports_ASPX_Report_rptReportViewer" title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

   

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<table style="width:700px;height:100%">
        <tr style="width:700px;height:100%">
            <td align="left" valign = "top" style="width: 696px"> 
        <CR:CrystalReportViewer ID="crv" runat="server" AutoDataBind="true" ToolPanelView="None"
        EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False" ReportSourceID="rs"
        ReuseParameterValuesOnRefresh="True" PrintMode="ActiveX" Width="350px" Height="50px" ShowAllPageIds="True" />
    &nbsp;<CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>
      </td>
        </tr>
    </table>
</asp:Content>

