<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPartysBalanceList.aspx.vb" Inherits="Reports_ASPX_Report_PartysBalanceList" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function GetAccounts() {
            //alert(1);
            var NameandCode;
            var result;
            var ActType;
            //alert(document.getElementById('<%=h_Mode.ClientID %>').value);
            ActType = document.getElementById('<%=h_Mode.ClientID %>').value
            if (ActType == 's') {
                result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=s&forrpt=1", "pop_up");
            }
            if (ActType == 'C') {
                result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=C&forrpt=1", "pop_up");
            }
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }
        function EnableControls() {
            var bConsolidation;
            bConsolidation = document.getElementById('<%=radConsolidation.ClientID %>').checked;
            document.getElementById('<%=radFormat1.ClientID %>').disabled = !bConsolidation;
            document.getElementById('<%=radFormat2.ClientID %>').disabled = !bConsolidation;
        }
        window.onload = EnableControls



        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = arg.NameandCode;//NameandCode[0];
                document.forms[0].submit();
            }
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Creditors Balance List"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" style="width: 100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="balanceList" />
                        </td>
                    </tr>
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                </table>

                <table align="center" width="100%" runat="server" id="tblMain">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                        <td align="left" colspan="3">
                            <div class="checkbox-list">
                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">As On Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                ErrorMessage="From Date required" ValidationGroup="balanceList">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="balanceList">*</asp:RegularExpressionValidator>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr id="Trbank">
                        <td align="left" valign="top" width="20%">
                            <asp:Label ID="lblBankCash" runat="server" Text="Creditor" CssClass="field-label"></asp:Label></td>
                        <td align="left" colspan="3">
                            <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the ACT ID you want to Add to the Search and click on Add)"></asp:Label>
                            <asp:TextBox ID="txtBankNames" runat="server"></asp:TextBox>
                            <asp:LinkButton ID="lblAddActID" runat="server">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetAccounts(); return false;" /><br />
                            <asp:GridView ID="grdACTDetails" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="ACT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ACT_Name" HeaderText="ACT Name" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_small" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"></td>
                        <td align="left" colspan="3">
                            <asp:CheckBox ID="chkGroupCurrency" runat="server" Text="View in Group Currency" CssClass="field-label" />
                            <asp:RadioButton ID="radSummary" runat="server" Text="Summary" GroupName="SUM_CONS" CssClass="field-label" />
                            <asp:RadioButton
                                ID="radConsolidation" runat="server" GroupName="SUM_CONS" Text="Consolidation" CssClass="field-label"></asp:RadioButton>

                            <asp:RadioButton ID="radFormat1" runat="server" Checked="True" GroupName="format" CssClass="field-label"
                                Text="Format 1"></asp:RadioButton>
                            <asp:RadioButton ID="radFormat2" runat="server" GroupName="format" CssClass="field-label"
                                Text="Format 2"></asp:RadioButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="balanceList" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />

                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_ACTIDs" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_Mode" runat="server" />
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

