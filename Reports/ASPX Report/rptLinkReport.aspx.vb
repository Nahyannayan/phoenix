Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.OleDb.OleDbDataAdapter
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Reports_ASPX_Report_rptLinkReport
    Inherits System.Web.UI.Page


    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim strQuery As String
        Dim param As New Hashtable
        ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")


        If ViewState("MainMnu_code") = "C860005" Or ViewState("MainMnu_code") = "C370005" Then
            param.Add("CurrYear", Request.QueryString("CurrYear"))
            param.Add("PrevYear1", Request.QueryString("PrevYear1"))
            param.Add("PrevYear2", Request.QueryString("PrevYear2"))
            param.Add("CurriCulum", Request.QueryString("CurriCulum"))

            If Request.QueryString("RES_STREAM") = "S" Then
                param.Add("Stream", "Science Stream")
            ElseIf Request.QueryString("RES_STREAM") = "C" Then
                param.Add("Stream", "Commerce Stream")
            ElseIf Request.QueryString("RES_STREAM") = "H" Then
                param.Add("Stream", "Humanities Stream")
            End If

            param.Add("@ACD_LOGIN_ID", Request.QueryString("ACD_ID"))
            param.Add("@GRD_ID", Request.QueryString("GRD_ID"))
            param.Add("@BSU_ID", Request.QueryString("BSU_ID"))

            strQuery = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID='" + Request.QueryString("BSU_ID") + "'"
            Dim BSU As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)

            param.Add("BSU_NAME", BSU)

            param.Add("@RES_STREAM", Request.QueryString("RES_STREAM"))

        End If

        If ViewState("MainMnu_code") = "C250005" Then
            param.Add("@IMG_BSU_ID", Session("sbsuid"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("@BSU_ID", Session("sbsuid"))
            param.Add("@Grade", Request.QueryString("Grade"))

            param.Add("@ACCYEAR", Request.QueryString("ACCYEAR"))

            param.Add("Acad_Year", Request.QueryString("Acad_Year"))
            param.Add("Grade_Dis", Request.QueryString("Grade_Dis"))

            param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
            param.Add("UserName", Session("sUsr_name"))

            param.Add("YEAR", Request.QueryString("YEAR"))

            param.Add("EXAM", Request.QueryString("EXAM"))
            param.Add("BSUNAME", Request.QueryString("BSUNAME"))
        End If

        If ViewState("MainMnu_code") = "T100270" Or ViewState("MainMnu_code") = "T100270-1" Or ViewState("MainMnu_code") = "T100270-2" Then
            param.Add("@IMG_BSU_ID", Session("SBSUID"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("UserName", Session("sUsr_name"))
            param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
            If Session("sbsuid") = "900501" Then
                param.Add("showH1", "true")
            Else
                param.Add("showH1", "false")
            End If
            param.Add("@ACY_ID", Request.QueryString("ACY_ID"))
            param.Add("@BSU_XML", getSeats_Bsu_XML)
            param.Add("Area", Request.QueryString("Area"))

            If ViewState("MainMnu_code") = "T100270-1" Then
                param.Add("@TRP_JOURNEY", Request.QueryString("Journey"))
                param.Add("@SBL_ID", Request.QueryString("SBL_ID"))
                param.Add("@PNT_ID", DBNull.Value)
            ElseIf ViewState("MainMnu_code") = "T100270-2" Then
                param.Add("@TRP_JOURNEY", Request.QueryString("Journey"))
                param.Add("@SBL_ID", DBNull.Value)
                param.Add("@PNT_ID", Request.QueryString("PNT_ID"))

            Else
                param.Add("@SBL_ID", Request.QueryString("SBL_ID"))
            End If
            If ViewState("MainMnu_code") = "T100270-1" Then
                param.Add("@bTOTAL", "FALSE")
            End If
        End If

        If ViewState("MainMnu_code") = "T100270-3" Or ViewState("MainMnu_code") = "T100270-4" Then
            param.Add("@IMG_BSU_ID", Session("SBSUID"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("UserName", Session("sUsr_name"))
            param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
            If Session("sbsuid") = "900501" Then
                param.Add("showH1", "true")
            Else
                param.Add("showH1", "false")
            End If
            param.Add("@ACY_ID", Request.QueryString("ACY_ID"))
            param.Add("@BSU_ID", Request.QueryString("BSU_ID"))
            param.Add("@SHF_ID", Request.QueryString("SHF_ID"))

            If ViewState("MainMnu_code") = "T100270-3" Then
                param.Add("@PNT_ID", Request.QueryString("PNT_ID"))
                param.Add("@TRD_ID", DBNull.Value)
                param.Add("Area", Request.QueryString("Area"))
                param.Add("shift", Request.QueryString("shift"))
            Else
                param.Add("@PNT_ID", DBNull.Value)
                param.Add("@TRD_ID", Request.QueryString("TRD_ID"))
                param.Add("Area", "")
            End If
        End If

        If ViewState("MainMnu_code") = "T100310" Then
            Dim str_query As String = "SELECT TRD_DRIVER_EMP_ID,VEH_REGNO,TRP_DESCR,BNO_DESCR=''," _
                          & " CON_NAME,DRV_NAME,TRP_JOURNEY,SHF_DESCR='Extra Trips'," _
                          & " PICKUPPOINTS=''," _
                          & " TRL_STARTTIME,TRL_ENDTIME,TRL_ENDKM,TRL_STARTKM,BSU_NAME,BSU_ID,TRL_TRIPDATE,PSE_DESCR,BSU_SHORT_RESULT " _
                          & " FROM TRANSPORT.VV_EXTRATRIPS_D AS B INNER JOIN TRANSPORT.TRIPLOG_S AS A ON B.TRD_ID=A.TRL_TRD_ID " _
                          & " INNER JOIN BUSINESSUNIT_M AS C ON B.TRD_BSU_ID=C.BSU_ID " _
                          & " WHERE TRP_ID=" + Request.QueryString("trpid")
            Dim dtfrom As String = Request.QueryString("from")
            Dim dtto As String = Request.QueryString("to")
            If dtfrom <> "" Then
                str_query += " AND TRL_TRIPDATE>='" + Format(Date.Parse(dtfrom), "yyyy-MM-dd") + "'"
            End If

            If dtto <> "" Then
                str_query += " AND TRL_TRIPDATE<='" + Format(Date.Parse(dtto), "yyyy-MM-dd") + "'"
            End If


            param.Add("@IMG_BSU_ID", Session("SBSUID"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("UserName", Session("sUsr_name"))
            param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
            param.Add("@SEARCHQUERY", str_query)

            param.Add("from", dtfrom)
            param.Add("to", dtto)
            If Session("sbsuid") = "900501" Then
                param.Add("showH1", "true")
            Else
                param.Add("showH1", "false")
            End If

        End If


        If ViewState("MainMnu_code") = "C300120" Then
            param.Add("@ACD_ID", Request.QueryString("Acdid"))
            param.Add("@GRD_ID", Request.QueryString("GrdId"))
            param.Add("@SBG_DESCR", Request.QueryString("Subject"))
            param.Add("@OPT_DESCR", Request.QueryString("Option"))
            param.Add("@TYPE", Request.QueryString("Type"))
            param.Add("accYear", Request.QueryString("accYear"))
            param.Add("grade", Request.QueryString("grade"))
            param.Add("type", Request.QueryString("Type"))
        End If


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            If ViewState("MainMnu_code") = "C860005" Then
                .reportPath = Server.MapPath("../../ResAnalysis/Reports/RPT/rptSubject.rpt")
                Session("rptClass1") = rptClass
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx?newwindow=1", True)
                .previousClass = Session("rptClass")
            ElseIf ViewState("MainMnu_code") = "C370005" Then
                .reportPath = Server.MapPath("../../ResAnalysis/Reports/RPT/rptSubject-Gender.rpt")
                .previousClass = Session("rptClass")
                Session("rptClass2") = rptClass
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx?newwindow=2", True)
            ElseIf ViewState("MainMnu_code") = "C250005" Then 'student details
                .reportPath = Server.MapPath("../../ResAnalysis/Reports/RPT/rptResultSummary.rpt")
                .previousClass = Session("rptClass")
                Session("rptClass3") = rptClass
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx?newwindow=3", True)
            ElseIf ViewState("MainMnu_code") = "T100270" Then
                .crDatabase = "Oasis_Transport"
                .reportPath = Server.MapPath("../../Transport/Reports/RPT/rptSeatCapacity_SubPickups.rpt")
                .previousClass = Session("rptClass")
                Session("rptClass4") = rptClass
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx?newwindow=4", True)
            ElseIf ViewState("MainMnu_code") = "T100270-1" Then
                .crDatabase = "Oasis_Transport"
                .reportPath = Server.MapPath("../../Transport/Reports/RPT/rptRegularTripSummary_Area.rpt")
                Session("rptClass5") = rptClass
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx?newwindow=5", True)
            ElseIf ViewState("MainMnu_code") = "T100270-2" Then
                .crDatabase = "Oasis_Transport"
                .reportPath = Server.MapPath("../../Transport/Reports/RPT/rptRegularTripSummary_Area.rpt")
                Session("rptClass6") = rptClass
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx?newwindow=6", True)
            ElseIf ViewState("MainMnu_code") = "T100270-3" Then
                .crDatabase = "Oasis_Transport"
                .reportPath = Server.MapPath("../../Transport/Reports/RPT/rptStudList_PickUps.rpt")
                Session("rptClass7") = rptClass
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx?newwindow=7", True)
            ElseIf ViewState("MainMnu_code") = "T100270-4" Then
                .crDatabase = "Oasis_Transport"
                .reportPath = Server.MapPath("../../Transport/Reports/RPT/rptStudList_PickUps.rpt")
                Session("rptClass8") = rptClass
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx?newwindow=8", True)
            ElseIf ViewState("MainMnu_code") = "T100310" Then
                .crDatabase = "Oasis_Transport"
                .reportPath = Server.MapPath("../../Transport/Reports/RPT/rptExtraTripSummary_Sub.rpt")
                Session("rptClass9") = rptClass
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx?newwindow=9", True)
            ElseIf ViewState("MainMnu_code") = "C300120" Then
                .crDatabase = "Oasis_Curriculum"
                .reportPath = Server.MapPath("../../Curriculum/Reports/RPT/rptOptionRequestCount_Sub.rpt")
                Session("rptClassPopup") = rptClass
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_Popup.aspx", True)
            End If

        End With
       
    End Sub

    Function getSeats_Bsu_XML() As String
        Dim str As String = String.Empty
        Dim bsu_id As String = Request.QueryString("BSU_ID")
        Dim shf_id As String = Request.QueryString("SHF_ID")
        If shf_id <> "0" Then
            str = "<ID><BSU_ID>" + bsu_id + "</BSU_ID><SHF_ID>" + shf_id + "</SHF_ID></ID>"
        Else
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim str_query As String = "SELECT SHF_ID FROM vv_SHIFTS_M WHERE SHF_BSU_ID='" + bsu_id + "'"
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While reader.Read
                str += "<ID><BSU_ID>" + bsu_id + "</BSU_ID><SHF_ID>" + reader.GetValue(0).ToString + "</SHF_ID></ID>"
            End While
            reader.Close()
        End If
        Return "<IDS>" + str + "</IDS>"
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            CallReport()
        End If
    End Sub
End Class
