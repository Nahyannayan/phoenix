Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data

Partial Class rptBankPayments
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim accountID, bankID As String
    Dim fromDate, toDate As String
    Dim MainMnu_code As String
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
        'If Page.IsPostBack = False Then
        '    If isPageExpired() Then

        '        Response.Redirect("expired.htm")
        '    Else
        '        trCollectionRow.Visible = False
        '        Session("TimeStamp") = Now.ToString
        '        ViewState("TimeStamp") = Now.ToString
        '    End If
        'End If
        Page.Title = OASISConstants.Gemstitle
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If
        Session("user") = Session("sUsr_name")

        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        'txtBSUID.Attributes.Add("ReadOnly", "ReadOnly")
        txtAccountID.Attributes.Add("ReadOnly", "ReadOnly")
        txtBankID.Attributes.Add("ReadOnly", "ReadOnly")
        txtAccountName.Attributes.Add("ReadOnly", "ReadOnly")
        txtBankName.Attributes.Add("ReadOnly", "ReadOnly")

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            lblError.Text = ""
        End If

        If Not IsPostBack Then
            UtilityObj.NoOpen(Me.Header)

            ddlBSUnit.DataSource = UtilityObj.GetBusinessUnits(Session("sUsr_name"))
            ddlBSUnit.DataTextField = "BSU_NAME"
            ddlBSUnit.DataValueField = "BSU_ID"
            ddlBSUnit.DataBind()
            ddlBSUnit.SelectedValue = Session("sbsuid")

            h_BSUID.Value = IIf(Session("SBSUID") <> "", Session("SBSUID").ToString(), String.Empty)
            'txtBSUID.Text = IIf(Session("BSU_Name") IsNot Nothing, Session("BSU_Name"), String.Empty)
            If Cache("fromDate") IsNot Nothing And Cache("toDate") IsNot Nothing Then
                txtfromDate.Text = Cache("fromDate")
                txtToDate.Text = Cache("toDate")
            Else
                txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtfromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            End If

            txtfromDate.Attributes.Add("onBlur", "checkdate(this)")
            txtToDate.Attributes.Add("onBlur", "checkdate(this)")
            trCollectionRow.Visible = False
            Select Case MainMnu_code
                Case "A250020", "A250025" 'Bank Payments"
                    Select Case MainMnu_code
                        Case "A250020"
                            lblRepCaption.Text = "Bank Payments"
                        Case "A250025"
                            lblRepCaption.Text = "Bank Receipts"
                    End Select
                    lblType.Text = "Bank"
                    h_mode.Value = "bank"
                Case "A250030", "A250035", OASISConstants.MenuREPORTSJV, "A250034"  ' journal & Recurring Vouchers
                    trSummary.Visible = False
                    Select Case MainMnu_code
                        Case "A250030"
                            trSummary.Visible = True
                            chkSummary.Text = "New Format"
                            lblRepCaption.Text = "Journal Vouchers"
                        Case "A250035"
                            lblRepCaption.Text = "Recurring Vouchers"
                        Case OASISConstants.MenuREPORTSJV
                            lblRepCaption.Text = "Self Reversing Journal Voucher "
                        Case "A250034"
                            lblRepCaption.Text = "Year End Adjustments"
                    End Select
                    trBankRow.Visible = False
                    trAccountRow.Visible = False
                    trFilterBy.Visible = False
                Case "A250097", "FD00086"
                    trSummary.Visible = False
                    lblRepCaption.Text = "Utilities Report"
                    trStatus.Visible = False
                    trBankRow.Visible = False
                    trAccountRow.Visible = False
                    trFilterBy.Visible = False

                Case "A250010", "A250015", "A250033" 'cash
                    Select Case MainMnu_code
                        Case "A250010"
                            lblRepCaption.Text = "Cash Payments"
                            trCollectionRow.Visible = False
                        Case "A250015"
                            lblRepCaption.Text = "Cash Receipts"
                            trCollectionRow.Visible = True
                        Case "A250033"
                            lblRepCaption.Text = "Internet Collection"
                            trCollectionRow.Visible = True
                    End Select
                    trBankRow.Visible = False
                    h_mode.Value = "cash"
                    BindCollection()
                    trFilterBy.Visible = False
                Case "A250050", "A250055"
                    Select Case MainMnu_code
                        Case "A250050"
                            lblRepCaption.Text = "Debit Notes"
                        Case "A250055"
                            lblRepCaption.Text = "Credit Notes"
                    End Select
                    trBankRow.Visible = False
                    trFilterBy.Visible = False
                Case "A250075"
                    lblRepCaption.Text = "Credit Card"
                    trFilterBy.Visible = False
                    trBankRow.Visible = False
                    h_mode.Value = "cash"
                    trCollectionRow.Visible = True
                    BindCollection()
                    'trBankRow.Visible = False
            End Select
        End If
    End Sub

    Private Sub BindCollection()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim str_sql As String = "SELECT [COL_ID], [COL_DESCR] FROM [COLLECTION_M]"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        If Not ds Is Nothing And Not ds.Tables(0) Is Nothing Then
            Dim drow As DataRow = ds.Tables(0).NewRow()
            drow(0) = "-1"
            drow(1) = "ALL"
            ds.Tables(0).Rows.Add(drow)
        End If
        ddCollection.DataSource = ds
        ddCollection.DataBind()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        txtfromDate.Text = Format("dd/MMM/yyyy", txtfromDate.Text)
        txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)

        Session("FDate") = txtfromDate.Text
        Session("TDate") = txtToDate.Text
        Session("selBSUID") = ddlBSUnit.SelectedValue

        If (txtfromDate.Text <> "") And (txtfromDate.Text <> String.Empty) Then
            fromDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", txtfromDate.Text)
        Else
            fromDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", DateTime.MinValue)
        End If
        If (txtToDate.Text <> "") And (txtToDate.Text <> String.Empty) Then
            toDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", txtToDate.Text)
        Else
            toDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", DateTime.Now)
        End If
        If Not Page.IsValid Then
            Exit Sub
        End If
        Cache("fromDate") = txtfromDate.Text
        Cache("toDate") = txtToDate.Text

        Select Case MainMnu_code
            Case "A250020" '"bank payment"
                GenerateBankCashPaymentReport("bankpay")
            Case "A250010" 'cash payment
                GenerateBankCashPaymentReport("cashpay")
            Case "A250030" '"journal"
                GenerateJournalVoucherReport()
            Case OASISConstants.MenuREPORTSJV
                GenerateSJVReport()
            Case "A250035" 'Recurring Vouchers
                GenerateRecurringVoucherReport()
            Case "A250025" 'bank reciept
                GenerateBankCashPaymentReport("bankrec")
            Case "A250015" 'cash reciept
                GenerateBankCashPaymentReport("cashrec")
            Case "A250050" ' Debit Notes
                GenerateBankCashPaymentReport("debitnote")
            Case "A250055" 'Credit Notes
                GenerateBankCashPaymentReport("creditnote")
            Case "A250075" 'cerdit card reciept
                GenerateBankCashPaymentReport("creditcard")
            Case "A250033"
                GenerateBankCashPaymentReport("internetCollection")
            Case "A250034"
                GenerateYJVReport()
            Case "A250097", "FD00086"
                GenerateUtilititesReport()
        End Select
    End Sub

    Private Sub GenerateUtilititesReport()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim cmd As New SqlCommand
        cmd.CommandText = "rptUtilities"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = New SqlConnection(str_conn)
        Dim sqlParam(3) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@FROM_DATE", txtfromDate.Text, SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@TO_DATE", txtToDate.Text, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlParam(0))
        cmd.Parameters.Add(sqlParam(1))
        cmd.Parameters.Add(sqlParam(2))

        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("reportCaption") = "Utilities Report"
        params("@BSU_ID") = Session("sbsuid")
        params("reportDate") = txtToDate.Text
        repSource.Parameter = params
        repSource.VoucherName = "Utilities Report"
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptUtilities.rpt"
        Session("ReportSource") = repSource
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            'Response.Redirect("rptviewer.aspx?isExport=true", True)
            ReportLoadSelection_export()
        Else
            'Response.Redirect("rptviewer.aspx", True)
            ReportLoadSelection()
        End If

    End Sub

    Private Sub GenerateJournalVoucherReport()
        Dim strFilter As String = String.Empty
        If ((txtfromDate.Text <> "") And (txtfromDate.Text <> String.Empty)) Or _
        ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then
            InitializeFilter(strFilter)
            strFilter += " DOCDT BETWEEN '" + fromDate + "' AND '" + toDate + "'"
        End If
        InitializeFilter(strFilter)
        Dim rStatus As Status
        rStatus = GetReportStatus()
        Session("Status") = rStatus
        Select Case rStatus
            Case Status.ALL
                strFilter += " JHD_bDELETED = 'FALSE'"
            Case Status.Deleted
                strFilter += " JHD_bDELETED = 'TRUE'"
            Case Status.Open
                strFilter += " JHD_bDELETED = 'FALSE' AND JHD_bPOSTED = 'FALSE'"
            Case Status.Posted
                strFilter += " JHD_bPOSTED = 'TRUE'"
        End Select

        'If (txtBSUID.Text <> "") And (txtBSUID.Text <> String.Empty) Then
        If (ddlBSUnit.SelectedValue <> "") And (ddlBSUnit.SelectedValue <> String.Empty) Then
            InitializeFilter(strFilter)
            strFilter += " BSU_ID IN( "
            Dim strArrayBSUID As String() = h_BSUID.Value.Split("||")
            Dim strBSUID As String = String.Empty
            For count As Integer = 0 To strArrayBSUID.Length - 1
                If count <> 0 Then
                    strBSUID += ", "
                End If
                strBSUID += "'" + strArrayBSUID(count) + "'"
            Next
            strFilter += strBSUID + ")"
        End If
        InitializeFilter(strFilter)
        strFilter += " DOCTYPE = 'JV'"

        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT * FROM vw_OSA_JOURNAL" + strFilter
        cmd.CommandType = Data.CommandType.Text

        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        'Dim ds As New DataSet
        'SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        'If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
        'If SqlHelper.ExecuteScalar(str_conn, CommandType.Text, cmd.CommandText, Nothing) Is Nothing Then
        '    lblError.Text = "No Records with specified condition"
        'Else
        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("reportHeading") = "Journal Voucher"
        params("VoucherName") = "Journal Voucher"
        params("fromDate") = fromDate
        params("toDate") = toDate
        params("decimal") = Session("BSU_ROUNDOFF")
        repSource.Parameter = params
        repSource.VoucherName = "Journal Voucher"
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If chkSummary.Checked Then
            repSource.ResourceName = "../RPT_Files/rptJournalVoucherSummaryReportNext.rpt"
        Else
            repSource.ResourceName = "../RPT_Files/rptJournalVoucherSummaryReport.rpt"
        End If

        Session("ReportSource") = repSource
        'Context.Items.Add("ReportSource", repSource)
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            'Response.Redirect("rptviewer.aspx?isExport=true", True)
            ReportLoadSelection_export()
        Else
            'Response.Redirect("rptviewer.aspx", True)
            ReportLoadSelection()
        End If
        'End If
    End Sub

    Private Sub GenerateYJVReport()
        Dim strFilter As String = String.Empty
        If ((txtfromDate.Text <> "") And (txtfromDate.Text <> String.Empty)) Or _
        ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then
            InitializeFilter(strFilter)
            strFilter += " SHD_DOCDT BETWEEN '" + fromDate + "' AND '" + toDate + "'"
        End If
        InitializeFilter(strFilter)
        Dim rStatus As Status
        rStatus = GetReportStatus()
        Session("Status") = rStatus
        Select Case rStatus
            Case Status.ALL
                strFilter += " SHD_bDELETED = 'FALSE'"
            Case Status.Deleted
                strFilter += " SHD_bDELETED = 'TRUE'"
            Case Status.Open
                strFilter += " SHD_bDELETED = 'FALSE' AND SHD_bPOSTED = 'FALSE'"
            Case Status.Posted
                strFilter += " SHD_bPOSTED = 'TRUE'"
        End Select

        InitializeFilter(strFilter)
        strFilter += " BSU_ID IN( "
        Dim strArrayBSUID As String() = h_BSUID.Value.Split("||")
        Dim strBSUID As String = String.Empty
        For count As Integer = 0 To strArrayBSUID.Length - 1
            If count <> 0 Then
                strBSUID += ", "
            End If
            strBSUID += "'" + strArrayBSUID(count) + "'"
        Next
        strFilter += strBSUID + ")"

        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT * FROM vw_OSA_YJOURNAL" + strFilter
        cmd.CommandType = Data.CommandType.Text

        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        'Dim ds As New DataSet
        'SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        'If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
        'If SqlHelper.ExecuteScalar(str_conn, CommandType.Text, cmd.CommandText, Nothing) Is Nothing Then
        '    lblError.Text = "No Records with specified condition"
        'Else
        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("reportHeading") = "YEAR END ADJUSTMENTS"
        params("VoucherName") = "YEAR END ADJUSTMENTS"
        params("fromDate") = fromDate
        params("toDate") = toDate
        params("decimal") = Session("BSU_ROUNDOFF")
        repSource.Parameter = params
        repSource.VoucherName = "YEAR END ADJUSTMENTS"
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptYJVSummaryReport.rpt"
        Session("ReportSource") = repSource
        'Context.Items.Add("ReportSource", repSource)
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            'Response.Redirect("rptviewer.aspx?isExport=true", True)
            ReportLoadSelection_export()
        Else
            'Response.Redirect("rptviewer.aspx", True)
            ReportLoadSelection_export()
        End If
        'End If
    End Sub

    Private Sub GenerateSJVReport()
        Dim strFilter As String = String.Empty
        If ((txtfromDate.Text <> "") And (txtfromDate.Text <> String.Empty)) Or _
        ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then
            InitializeFilter(strFilter)
            strFilter += " SHD_DOCDT BETWEEN '" + fromDate + "' AND '" + toDate + "'"
        End If
        InitializeFilter(strFilter)
        Dim rStatus As Status
        rStatus = GetReportStatus()
        Session("Status") = rStatus
        Select Case rStatus
            Case Status.ALL
                strFilter += " SHD_bDELETED = 'FALSE'"
            Case Status.Deleted
                strFilter += " SHD_bDELETED = 'TRUE'"
            Case Status.Open
                strFilter += " SHD_bDELETED = 'FALSE' AND SHD_bPOSTED = 'FALSE'"
            Case Status.Posted
                strFilter += " SHD_bPOSTED = 'TRUE'"
        End Select

        InitializeFilter(strFilter)
        strFilter += " BSU_ID IN( "
        Dim strArrayBSUID As String() = h_BSUID.Value.Split("||")
        Dim strBSUID As String = String.Empty
        For count As Integer = 0 To strArrayBSUID.Length - 1
            If count <> 0 Then
                strBSUID += ", "
            End If
            strBSUID += "'" + strArrayBSUID(count) + "'"
        Next
        strFilter += strBSUID + ")"

        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT * FROM vw_OSA_SJOURNAL" + strFilter
        cmd.CommandType = Data.CommandType.Text

        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        'Dim ds As New DataSet
        'SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        'If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
        'If SqlHelper.ExecuteScalar(str_conn, CommandType.Text, cmd.CommandText, Nothing) Is Nothing Then
        '    lblError.Text = "No Records with specified condition"
        'Else
        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("reportHeading") = "Self Reversing Journal Voucher"
        params("VoucherName") = "Self Reversing Journal Voucher"
        params("fromDate") = fromDate
        params("toDate") = toDate
        params("decimal") = Session("BSU_ROUNDOFF")
        repSource.Parameter = params
        repSource.VoucherName = "Self Reversing Journal Voucher"
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptSJVSummaryReport.rpt"
        Session("ReportSource") = repSource
        'Context.Items.Add("ReportSource", repSource)
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            'Response.Redirect("rptviewer.aspx?isExport=true", True)
            ReportLoadSelection_export()
        Else
            'Response.Redirect("rptviewer.aspx", True)
            ReportLoadSelection()
        End If
        'End If
    End Sub

    Private Sub GenerateRecurringVoucherReport()
        Dim strFilter As String = String.Empty
        If ((txtfromDate.Text <> "") And (txtfromDate.Text <> String.Empty)) Or _
        ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then
            InitializeFilter(strFilter)
            strFilter += " DOCDT BETWEEN '" + fromDate + "' AND '" + toDate + "'"
        End If
        InitializeFilter(strFilter)
        Dim rStatus As Status
        rStatus = GetReportStatus()
        Session("Status") = rStatus
        Select Case rStatus
            Case Status.ALL
                strFilter += " bDELETED = 'FALSE'"
            Case Status.Deleted
                strFilter += " bDELETED = 'TRUE'"
            Case Status.Open
                strFilter += " bDELETED = 'FALSE' AND bPOSTED = 'FALSE'"
            Case Status.Posted
                strFilter += " bPOSTED = 'TRUE'"
        End Select

        'If (txtBSUID.Text <> "") And (txtBSUID.Text <> String.Empty) Then
        If (ddlBSUnit.SelectedValue <> "") And (ddlBSUnit.SelectedValue <> String.Empty) Then
            InitializeFilter(strFilter)
            strFilter += " BSU_ID IN( "
            Dim strArrayBSUID As String() = h_BSUID.Value.Split("||")
            Dim strBSUID As String = String.Empty
            For count As Integer = 0 To strArrayBSUID.Length - 1
                If count <> 0 Then
                    strBSUID += ", "
                End If
                strBSUID += "'" + strArrayBSUID(count) + "'"
            Next
            strFilter += strBSUID + ")"
        End If
        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT * FROM vw_OSA_RJOURNALFULL" + strFilter
        cmd.CommandType = Data.CommandType.Text

        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        'Dim ds As New DataSet
        'SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        'If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
        'If SqlHelper.ExecuteScalar(str_conn, CommandType.Text, cmd.CommandText, Nothing) Is Nothing Then
        '    lblError.Text = "No Records with specified condition"
        'Else
        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        'params("Summary") = chkSummary.Checked
        params("Duplicated") = False
        params("VoucherName") = "Recurring Voucher"
        params("reportHeading") = "Recurring Voucher"
        params("FromDate") = fromDate
        params("ToDate") = toDate
        params("decimal") = Session("BSU_ROUNDOFF")
        repSource.Parameter = params
        repSource.VoucherName = "Recurring Voucher"
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptJournalVoucherSummaryReport.rpt"
        Session("ReportSource") = repSource
        'Context.Items.Add("ReportSource", repSource)
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            'Response.Redirect("rptviewer.aspx?isExport=true", True)
            ReportLoadSelection_export()
        Else
            Response.Redirect("rptviewer.aspx", True)
        End If
        'Response.Redirect("reportviewer.aspx")
        'End If
    End Sub

    Private Sub GenerateBankCashPaymentReport(ByVal strMode As String)
        Dim strFilter As String = String.Empty
        InitializeFilter(strFilter)
        Select Case strMode
            Case "bankpay", "bankrec"
                Select Case strMode
                    Case "bankpay"
                        strFilter += " VHH_DOCTYPE = 'BP'"
                    Case "bankrec"
                        strFilter += " VHH_DOCTYPE = 'BR'"
                End Select
                If ((txtfromDate.Text <> "") And (txtfromDate.Text <> String.Empty)) Or _
                ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then
                    InitializeFilter(strFilter)
                    Dim chkdDateType As DateType
                    chkdDateType = GetDateType()
                    Session("DateType") = chkdDateType
                    Select Case chkdDateType
                        Case DateType.ChequeDate
                            strFilter += " VHD_CHQDT BETWEEN '" + fromDate + "' AND '" + toDate + "'"
                            Exit Select
                        Case DateType.VoucherDate
                            strFilter += " VHH_DOCDT BETWEEN '" + fromDate + "' AND '" + toDate + "'"
                            Exit Select
                    End Select
                End If

            Case "creditnote", "debitnote"
                Select Case strMode
                    Case "creditnote"
                        strFilter += " VHH_DOCTYPE = 'CN'"
                    Case "debitnote"
                        strFilter += " VHH_DOCTYPE = 'DN'"
                End Select
                If ((txtfromDate.Text <> "") And (txtfromDate.Text <> String.Empty)) Or _
                ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then
                    InitializeFilter(strFilter)
                    strFilter += " VHH_DOCDT BETWEEN '" + fromDate + "' AND '" + toDate + "'"
                End If

            Case "cashpay", "cashrec", "internetCollection"
                Select Case strMode
                    Case "cashpay"
                        strFilter += " VHH_DOCTYPE = 'CP'"
                    Case "cashrec"
                        strFilter += " VHH_DOCTYPE = 'CR'"
                    Case "internetCollection"
                        strFilter += " VHH_DOCTYPE = 'IC'"
                End Select
                If ((txtfromDate.Text <> "") And (txtfromDate.Text <> String.Empty)) Or _
                ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then
                    InitializeFilter(strFilter)
                    strFilter += " VHH_DOCDT BETWEEN '" + fromDate + "' AND '" + toDate + "'"
                End If
            Case "creditcard"
                strFilter += " VHH_DOCTYPE = 'CC'"
                If ((txtfromDate.Text <> "") And (txtfromDate.Text <> String.Empty)) Or _
                              ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then
                    InitializeFilter(strFilter)
                    strFilter += " VHH_DOCDT BETWEEN '" + fromDate + "' AND '" + toDate + "'"
                End If
        End Select
        If (txtBankName.Text <> "") And (txtBankName.Text <> String.Empty) Then
            accountID = txtBankID.Text
            Session("BankID") = txtBankID.Text
            Session("BankName") = txtBankName.Text
            InitializeFilter(strFilter)
            strFilter += " VHH_ACT_ID = '" + accountID + "'"
        End If
        If (txtAccountName.Text <> "") And (txtAccountName.Text <> String.Empty) Then
            bankID = txtAccountID.Text
            Session("AccountID") = txtAccountID.Text
            Session("AccountName") = txtAccountName.Text
            InitializeFilter(strFilter)
            Select Case strMode
                Case "debitnote", "creditnote" ', "creditcard"
                    strFilter += " VHH_ACT_ID = '" + bankID + "'"
                Case "creditcard"
                    strFilter += " VHD_ACT_ID = '" + bankID + "'"
                Case Else
                    strFilter += " VHD_ACT_ID = '" + bankID + "'" '" AND VHH_COL_ID='" & ddCollection.SelectedItem.Value & "'"
            End Select
        End If

        Select Case strMode
            Case "creditcard", "cashrec", "internetCollection"
                If ddCollection.SelectedItem.Text <> "ALL" Then
                    strFilter += " AND VHH_COL_ID='" & ddCollection.SelectedItem.Value & "'"
                End If
                Session("Collection") = ddCollection.SelectedValue
        End Select

        InitializeFilter(strFilter)
        Dim rStatus As Status
        rStatus = GetReportStatus()
        Session("Status") = rStatus
        Select Case rStatus
            Case Status.ALL
                strFilter += " VHH_bDELETED = 'FALSE'"
            Case Status.Deleted
                strFilter += " VHH_bDELETED = 'TRUE'"
            Case Status.Open
                strFilter += " VHH_bDELETED = 'FALSE' AND VHH_bPOSTED = 'FALSE'"
            Case Status.Posted
                strFilter += " VHH_bPOSTED = 'TRUE'"
        End Select

        'If (txtBSUID.Text <> "") And (txtBSUID.Text <> String.Empty) Then
        If (ddlBSUnit.SelectedValue <> "") And (ddlBSUnit.SelectedValue <> String.Empty) Then
            InitializeFilter(strFilter)
            strFilter += " VHH_BSU_ID IN( "
            Dim strArrayBSUID As String() = h_BSUID.Value.Split("||")
            Dim strBSUID As String = String.Empty
            For count As Integer = 0 To strArrayBSUID.Length - 1
                If count <> 0 Then
                    strBSUID += ", "
                End If
                strBSUID += "'" & strArrayBSUID(count) & "'"
            Next
            strFilter += strBSUID + ")"
        End If
        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT * FROM vw_OSA_VOUCHER" + strFilter
        cmd.CommandType = Data.CommandType.Text

        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        'Dim ds As New DataSet
        'SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        'If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
        'If SqlHelper.ExecuteScalar(str_conn, CommandType.Text, cmd.CommandText, Nothing) Is Nothing Then
        '    lblError.Text = "No Records with specified condition"
        'Else
        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Summary") = chkSummary.Checked
        Select Case strMode
            Case "cashpay"
                params("voucherName") = "CASH PAYMENT VOUCHER"
                repSource.VoucherName = "CASH PAYMENT VOUCHER"
            Case "bankpay"
                repSource.VoucherName = "BANK PAYMENT VOUCHER"
                params("voucherName") = "BANK PAYMENT VOUCHER"
            Case "internetCollection"
                params("voucherName") = "INTERNET COLLECTION"
                repSource.VoucherName = "INTERNET COLLECTION"
            Case "cashrec"
                params("voucherName") = "CASH RECEIPT VOUCHER"
                repSource.VoucherName = "CASH RECEIPT VOUCHER"
            Case "bankrec"
                repSource.VoucherName = "BANK RECEIPT VOUCHER"
                params("voucherName") = "BANK RECEIPT VOUCHER"
            Case "debitnote"
                repSource.VoucherName = "DEBIT NOTE"
                params("voucherName") = "DEBIT NOTE"
            Case "creditnote"
                repSource.VoucherName = "CREDIT NOTE"
                params("voucherName") = "CREDIT NOTE"
            Case "creditcard" 'creditcard
                repSource.VoucherName = "CREDIT CARD RECEIPTS"
                params("voucherName") = "CREDIT CARD RECEIPTS"
        End Select
        params("FromDate") = fromDate
        params("ToDate") = toDate
        params("decimal") = Session("BSU_ROUNDOFF")
        repSource.Parameter = params
        repSource.Command = cmd
        Select Case strMode
            Case "cashpay", "creditnote", "debitnote"
                repSource.ResourceName = "../RPT_Files/CashPaymentSummaryReport.rpt"
            Case "cashrec", "internetCollection"
                repSource.ResourceName = "../RPT_Files/CashReceiptSummaryReport.rpt"
            Case "bankpay", "bankrec"
                If chkSummary.Checked Then
                    repSource.ResourceName = "../RPT_Files/CashPaymentSummaryReport.rpt"
                Else
                    repSource.ResourceName = "../RPT_Files/BankPaymentSummaryReport.rpt"
                End If
                'repSource.ResourceName = "../RPT_Files/BankPaymentReport.rpt"
            Case "creditcard"
                repSource.ResourceName = "../RPT_Files/CreditCardReceiptReport.rpt"
        End Select
        Session("ReportSource") = repSource
        'Context.Items.Add("ReportSource", repSource)
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            'Response.Redirect("rptviewer.aspx?isExport=true", True)
            ReportLoadSelection_export()
        Else
            'Response.Redirect("rptviewer.aspx", True)
            ReportLoadSelection()
        End If
        'Response.Redirect("reportviewer.aspx")
        'End If
    End Sub
    Private Sub InitializeFilter(ByRef filter As String)
        If filter IsNot String.Empty Or filter IsNot "" Then
            If Not filter.StartsWith(" WHERE") Then
                filter = filter.Insert(0, " WHERE")
            End If
            If Not filter.EndsWith("WHERE") Then
                filter = filter.Insert(filter.Length, " AND")
            End If
        End If
    End Sub

    Private Function GetDateType() As DateType
        Dim chkdDateType As DateType
        If radVoucherDate.Checked = True Then
            chkdDateType = DateType.VoucherDate
        ElseIf radChequeDate.Checked = True Then
            chkdDateType = DateType.ChequeDate
        End If
        Return chkdDateType
    End Function

    Private Function GetReportStatus() As Status
        Dim rstatus As Status
        If radStatusAll.Checked = True Then
            rstatus = Status.ALL
        ElseIf radStatusOpen.Checked = True Then
            rstatus = Status.Open
        ElseIf radStatusPosted.Checked = True Then
            rstatus = Status.Posted
        ElseIf radStatusDeleted.Checked = True Then
            rstatus = Status.Deleted
        End If
        Return rstatus
    End Function

    Public Sub compareDate(ByVal sender As Object, ByVal value As ServerValidateEventArgs)
        Dim dtfrom, dtto As DateTime
        If ((txtfromDate.Text <> "") And (txtfromDate.Text <> String.Empty)) Or _
        ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then
            dtfrom = Convert.ToDateTime(txtfromDate.Text)
            dtto = Convert.ToDateTime(txtToDate.Text)
            If dtfrom < dtto Then
                value.IsValid = True
            Else
                value.IsValid = False
            End If
        End If
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnSubmit_Click(sender, e)
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            txtfromDate.Text = Session("FDate")
            txtToDate.Text = Session("TDate")
            ddlBSUnit.SelectedValue = Session("selBSUID")
            txtAccountName.Text = Session("AccountName")
            txtAccountID.Text = Session("AccountID")
            If Session("DateType") = DateType.VoucherDate Then
                radVoucherDate.Checked = True
            Else
                radChequeDate.Checked = True
            End If
            If Session("Status") = Status.ALL Then
                radStatusAll.Checked = True
            ElseIf Session("Status") = Status.Open Then
                radStatusOpen.Checked = True
            ElseIf Session("Status") = Status.Deleted Then
                radStatusDeleted.Checked = True
            ElseIf Session("Status") = Status.Posted Then
                radStatusPosted.Checked = True
            End If
            'Select Case MainMnu_code
            '    Case "A250025" 'bank reciept

            txtBankName.Text = Session("BankName")
            txtBankID.Text = Session("BankID")
            'Case "A250015" 'Cash reciept
            ddCollection.SelectedValue = Session("Collection")
            'End Select
        End If
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub

    Sub ReportLoadSelection_export()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx?isExport=true');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx?isExport=true','_blank');", True)
        End If
    End Sub
End Class
