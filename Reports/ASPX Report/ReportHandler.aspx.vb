Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports System.Net

Partial Class Reports_ASPX_Report_ReportHandler
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim params As New Hashtable
            Dim BSUID As String = IIf(Request.QueryString("BSUID") <> Nothing, Request.QueryString("BSUID"), String.Empty)
            Dim ACTID As String = IIf(Request.QueryString("ID") <> Nothing, Request.QueryString("ID"), String.Empty)
            Dim groupType As Integer = IIf(Request.QueryString("Type") <> Nothing, Request.QueryString("Type"), 0)
            Select Case (groupType)
                'For Lijo.....(student Attendance)
                Case 10
                    GenerateMonthWiseDetails()
                Case 12
                    GenerateDailyAttDetails()
                Case 14
                    GenerateStud_AttDetails()
                Case 16
                    GenerateSchool_GradeDetails()
                Case 18
                    GenerateSchool_SectionDetails()
                Case 22
                    GenerateEdushield_Details()
                Case 20
                    Dim docType As String = IIf(Request.QueryString("doctype") <> Nothing, Request.QueryString("doctype"), String.Empty)
                    Dim DOCNO As String = IIf(Request.QueryString("DOCNO") <> Nothing, Request.QueryString("DOCNO"), 0)
                    Dim str_sql As String = "SELECT FSH_REF_ID FROM FEES.FEESCHEDULE WHERE FSH_ID = " & DOCNO
                    Select Case docType
                        Case "ADJ"
                            'DOCNO
                            DOCNO = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_sql)
                            ViewAdjustmentTransport(DOCNO)
                        Case "CONC"

                            DOCNO = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str_sql)
                            ViewConcessionDetailsTransport(DOCNO, BSUID)
                        Case "COLL"
                            ViewTransportFeeCollectionDetails(DOCNO, BSUID)
                    End Select
                Case 30
                    Dim docType As String = IIf(Request.QueryString("doctype") <> Nothing, Request.QueryString("doctype"), String.Empty)
                    Dim DOCNO As String = IIf(Request.QueryString("DOCNO") <> Nothing, Request.QueryString("DOCNO"), 0)
                    Dim vACD_ID As String = IIf(Request.QueryString("ACDID") <> Nothing, Request.QueryString("ACDID"), 0)
                    Select Case docType
                        Case "ADJ"
                            'DOCNO
                            Dim str_sql As String = "SELECT FSH_REF_ID FROM FEES.FEESCHEDULE WHERE FSH_DOCNO = '" & DOCNO & "'"
                            DOCNO = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_sql)
                            ViewAdjustment(DOCNO)
                        Case "CONC"
                            Dim str_sql As String = "SELECT FCH_ID FROM FEES.FEESCHEDULE INNER JOIN " & _
                            " FEES.FEE_CONCESSION_H ON FEES.FEESCHEDULE.FSH_DOCNO = FEES.FEE_CONCESSION_H.FCH_RECNO " & _
                            " WHERE FSH_DOCNO = '" & DOCNO & "' AND FCH_ACD_ID =" & vACD_ID
                            DOCNO = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_sql)
                            ViewConcessionDetails(DOCNO, BSUID)
                        Case "COLL"
                            Dim vSTU_TYPE As String = IIf(Request.QueryString("STU_TYPE") <> Nothing, Request.QueryString("STU_TYPE"), 0)
                            ViewFeeCollectionDetails(DOCNO, vSTU_TYPE)
                    End Select
                Case 3 '"By Grpcode"
                    'nothing need to be changed
                    BSUID = Session("BSUNames")
                Case 2, 1, 0 '"By SgrpCode"
                    'Pass the GroupID
                    BSUID = GenerateXML(BSUID, XMLType.BSUName)
                    'Case 1 '"By Ctrl Account"
                    '    BSUID = GenerateXML(BSUID, XMLType.BSUName)
                    'Case 0 '"By Account"
                    '    BSUID = GenerateXML(BSUID, XMLType.BSUName)
                Case -1
                    Dim ACTName As String = IIf(Request.QueryString("ACTName") <> Nothing, Request.QueryString("ACTName"), String.Empty)
                    GetLedger(BSUID, ACTID)
                    params("Type") = -2
                    Exit Sub
                Case -2
                    Dim docType As String = IIf(Request.QueryString("doctype") <> Nothing, Request.QueryString("doctype"), String.Empty)
                    If docType <> "" Then
                        GetLedger(docType)
                        'params("Type") = -3
                        Exit Sub
                    End If
                Case -3
                    GeneratePartyLedger()
                    Exit Sub
                Case -4
                    GeneratePartyLedger(GenerateXML(BSUID, XMLType.BSUName))
                    Exit Sub
                Case -5
                    GeneratePartyLedger(GenerateXML(BSUID, XMLType.BSUName), ACTID)
                    Exit Sub
                Case 40 ' Case No 40 Done By Sajesh For Getting Sibling Information...

                    GetSiblingInformation()
                    Exit Sub
                Case 41 'case no 41 done by arun.g for School statistics report for grade wise
                    corp_currentwise_strength()
                    Exit Sub
                Case 1401 'case no 1401 done by Prem for ENQ_REG_ENR  report for grade wise
                    Corp_BSU_ENQREGENR()
                    Exit Sub
                Case 1402 'case no 1401 done by Prem for ENQ_REG_ENR  report for grade wise
                    Corp_BSU_ENQREGENR_Details()
                    Exit Sub
                Case 42 'case no 42 done by arun.g for SURVEY
                    SURVEY_REPORT("42")
                    Exit Sub
                Case 43 'case no 42 done by arun.g for SURVEY
                    SURVEY_REPORT("43")
                    Exit Sub
                Case 44 'case no 42 done by arun.g for SURVEY
                    SURVEY_REPORT("44")
                    Exit Sub
                Case 50 'case no 50 done by arun.g for describcency report
                    describcencydetails()
                    Exit Sub
                Case 51 'case no 51 done by rajesh for school statitics report
                    GenerateSchool_statistics()
                    Exit Sub
            End Select

            'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            'Dim objConn As New SqlConnection(str_conn)
            'Dim adpt As New SqlDataAdapter
            'Dim ds As New DataSet

            'Dim cmd As New SqlCommand("RPTGETTBSUBDETAILS", objConn)
            'cmd.CommandType = CommandType.StoredProcedure

            'Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.Xml)
            'sqlpJHD_BSU_IDs.Value = BSUID
            'cmd.Parameters.Add(sqlpJHD_BSU_IDs)

            'Dim sqlpID As New SqlParameter("@ID", SqlDbType.VarChar, 20)
            'sqlpID.Value = ACTID
            'cmd.Parameters.Add(sqlpID)

            'Dim sqlpJHD_DOCTYPE As New SqlParameter("@Type", SqlDbType.VarChar, 20)
            'sqlpJHD_DOCTYPE.Value = groupType
            'cmd.Parameters.Add(sqlpJHD_DOCTYPE)

            'Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
            'sqlpFROMDOCDT.Value = Session("rptFromDate")
            'cmd.Parameters.Add(sqlpFROMDOCDT)

            'Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
            'sqlpTODOCDT.Value = Session("rptToDate")
            'cmd.Parameters.Add(sqlpTODOCDT)

            'Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
            'sqlpConsolidation.Value = Session("rptConsolidation")
            'cmd.Parameters.Add(sqlpConsolidation)

            'Dim sqlpGroupCurrency As New SqlParameter("@bShowinGrpCurrency", SqlDbType.Bit)
            'sqlpGroupCurrency.Value = Session("rptGroupCurrency")
            'cmd.Parameters.Add(sqlpGroupCurrency)

            'adpt.SelectCommand = cmd
            'objConn.Open()
            'adpt.Fill(ds)
            'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            '    Dim repSource As New MyReportClass

            '    Dim path As String = String.Empty
            '    If Not Session("ReportSource") Is Nothing Then
            '        repSource = DirectCast(Session("ReportSource"), MyReportClass)
            '        path = repSource.Parameter.Item("parent") + " ->"
            '    End If
            '    params("userName") = Session("sUsr_name")
            '    params("FromDate") = Session("rptFromDate")
            '    params("ToDate") = Session("rptToDate")
            '    params("Type") = groupType
            '    params("parent") = path + Request.QueryString("ACTName") 'GetACTName(ACTID)
            '    params("decimal") = Session("BSU_ROUNDOFF")
            '    repSource.Parameter = params
            '    repSource.Command = cmd
            '    repSource.ResourceName = "../RPT_Files/rptTrailBalance.rpt"
            '    objConn.Close()
            '    Session("ReportSource") = repSource
            'End If
            'objConn.Close()
            'Response.Redirect("rptviewer.aspx?newwindow=true", True)
            'Response.Write("<script>window.open('rptviewer.aspx')</script>")

            'Response.Redirect(Request.UrlReferrer.ToString(), True)
            'write in Error log
            ' "No Records with specified condition"
        End If
    End Sub
    Private Sub GenerateSchool_statistics()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Bsuid As String = IIf(Request.QueryString("Bsuid") <> Nothing, Request.QueryString("Bsuid"), String.Empty)
        Dim ACDID As String = IIf(Request.QueryString("ACDID") <> Nothing, Request.QueryString("ACDID"), String.Empty)
        Dim CLMID As String = IIf(Request.QueryString("CLMID") <> Nothing, Request.QueryString("CLMID"), String.Empty)
        Dim ToDT As String = IIf(Request.QueryString("ToDT") <> Nothing, Request.QueryString("ToDT"), String.Empty)
        Dim FROMDT As String = IIf(Request.QueryString("FROMDT") <> Nothing, Request.QueryString("FROMDT"), String.Empty)


        Dim FromTo_Date As String = ""
        Dim i As Integer = 0

        Dim str_bsu_ids As New StringBuilder



        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Bsuid)
        param.Add("@IMG_TYPE", "LOGO")
        'param.Add("@ToDT", Format(Date.Parse(txtToDate.Text), "dd-MM-yyyy"))
        'param.Add("@FROMDT", Format(Date.Parse(txtFromDate.Text), "dd-MM-yyyy"))


        param.Add("@ToDT", Format(Date.Parse(ToDT), "yyyy-MM-dd"))
        param.Add("@FROMDT", Format(Date.Parse(FROMDT), "yyyy-MM-dd"))

        param.Add("@ACD_ID", ACDID)
        param.Add("@ACD_CLM_ID", CLMID)

        param.Add("@SHF_ID", Nothing)



        param.Add("Todt", Format(Date.Parse(ToDT), "dd/MMM/yyyy"))
        param.Add("Fromdt", Format(Date.Parse(FROMDT), "dd/MMM/yyyy"))


        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        'param.Add("To_Date", txtToDate.Text)
        'param.Add("From_Date", txtFromDate.Text)
        param.Add("UserName", Session("sUsr_name"))

        ''code added for describceny report in the report footer
        param.Add("@STA_BSU_ID", Bsuid)
        param.Add("@FROMDATE", Format(Date.Parse(ToDT), "dd/MMM/yyyy"))
        param.Add("@TODATE", Format(Date.Parse(ToDT), "dd/MMM/yyyy"))
        Session("rptdesFROMDATE") = Format(Date.Parse(ToDT), "dd/MMM/yyyy")
        Session("rptdesTODATE") = Format(Date.Parse(ToDT), "dd/MMM/yyyy")
        ''end

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../../Students/Reports/RPT/rptStud_SchoolStat.rpt")


        End With
        Session("rptClass2") = rptClass
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "ShowStatistics", "ShowStatistics();", True)
        ' Response.Redirect("rptReportViewer.aspx?newwindow=2", True)
    End Sub
    Protected Sub SURVEY_REPORT(ByVal type As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim SURVEY_ID As String = IIf(Request.QueryString("SURVEY_ID") <> Nothing, Request.QueryString("SURVEY_ID"), 0)
        Dim BSU_ID As String = IIf(Request.QueryString("BSU_ID") <> Nothing, Request.QueryString("BSU_ID"), 0)
        Dim str_bsu_ids As New StringBuilder
        Dim bsu_xml As String = ""
        Dim param As New Hashtable
        param.Add("@BSU_ID", BSU_ID.ToString)
        param.Add("@SURVEY_ID", SURVEY_ID.ToString)
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_COMM"
            .reportParameters = param
            If type = "42" Then
                .reportPath = Server.MapPath("~/Survey/Reports/RPT/rptSuvery_PSI_Report.rpt")
            ElseIf type = "43" Then
                .reportPath = Server.MapPath("~/Survey/Reports/RPT/rptSurveyStandard.rpt")
            ElseIf type = "44" Then
                .reportPath = Server.MapPath("~/Survey/Reports/RPT/rptSurveyComments.rpt")
            End If

        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewerNew.aspx")
    End Sub

    Protected Sub corp_currentwise_strength()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim From_Date As String = IIf(Request.QueryString("FROMDATE") <> Nothing, Request.QueryString("FROMDATE"), 0)
        Dim To_Date As String = IIf(Request.QueryString("TODATE") <> Nothing, Request.QueryString("TODATE"), 0)

        Dim Bsu_id As String = IIf(Request.QueryString("BSUID") <> Nothing, Request.QueryString("BSUID"), 0)
        Dim ACD_ID As String = IIf(Request.QueryString("ACD") <> Nothing, Request.QueryString("ACD"), 0)

        Dim str_bsu_ids As New StringBuilder
        Dim bsu_xml As String = ""
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", "999998")
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ACD_ID.ToString)
        param.Add("@BSU_ID", Bsu_id.ToString)
        param.Add("@ToDT", Format(Date.Parse(To_Date.ToString), "yyyy-MMM-dd"))
        param.Add("@FROMDT", Format(Date.Parse(From_Date.ToString), "yyyy-MMM-dd"))
        param.Add("Todt", Format(Date.Parse(To_Date.ToString), "dd/MMM/yyyy"))
        param.Add("Fromdt", Format(Date.Parse(From_Date.ToString), "dd/MMM/yyyy"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))

        param.Add("UserName", Session("sUsr_name"))

        ''code added for describceny report in the report footer
        'param.Add("@STA_BSU_ID", str_bsu_ids.ToString)
        'param.Add("@FROMDATE", Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy"))
        'param.Add("@TODATE", Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy"))
        'param.Add("@SHF_ID", Nothing)
        'Session("rptdesFROMDATE") = Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy")
        'Session("rptdesTODATE") = Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy")
        ''end
        h_newWindow.Value = "true"
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../../Students/Reports/RPT/rptCorpo_SchoolCurr_Wise_Stat_GRADE.rpt")
            '.reportPath = Server.MapPath("../RPT/rptCorpo_SchoolCurr_Wise_Stat.rpt")

        End With
        Session("rptClass2") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("rptReportViewer.aspx?newwindow=2", True)
    End Sub
    Protected Sub Corp_BSU_ENQREGENR_Details()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim From_Date As String = IIf(Request.QueryString("FROMDATE") <> Nothing, Request.QueryString("FROMDATE"), 0)
        Dim To_Date As String = IIf(Request.QueryString("TODATE") <> Nothing, Request.QueryString("TODATE"), 0)
        Dim Bsu_id As String = IIf(Request.QueryString("BSUID") <> Nothing, Request.QueryString("BSUID"), 0)
        Dim CLM As String = IIf(Request.QueryString("CLM") <> Nothing, Request.QueryString("CLM"), 0)
        Dim ENQ_TYPE As String = IIf(Request.QueryString("ENQ_TYPE") <> Nothing, Request.QueryString("ENQ_TYPE"), 0)


        Dim str_bsu_ids As New StringBuilder
        Dim bsu_xml As String = ""
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", "999998")
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_XML", Session("Corp_ACD_XML"))
        param.Add("@BSU_ID", Bsu_id.ToString)
        param.Add("@CLM_DESCR", CLM.ToString)
        param.Add("@ENQ_TYPE", ENQ_TYPE.ToString)

        param.Add("@DATE2", Format(Date.Parse(To_Date.ToString), "dd/MMM/yyyy"))
        param.Add("@DATE1", Format(Date.Parse(From_Date.ToString), "dd/MMM/yyyy"))
        param.Add("DATE2", Format(Date.Parse(To_Date.ToString), "dd/MMM/yyyy"))
        param.Add("DATE1", Format(Date.Parse(From_Date.ToString), "dd/MMM/yyyy"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))

        ''code added for describceny report in the report footer
        'param.Add("@STA_BSU_ID", str_bsu_ids.ToString)
        'param.Add("@FROMDATE", Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy"))
        'param.Add("@TODATE", Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy"))
        'param.Add("@SHF_ID", Nothing)
        'Session("rptdesFROMDATE") = Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy")
        'Session("rptdesTODATE") = Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy")
        ''end
        h_newWindow.Value = "true"
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../../Students/Reports/RPT/rptCorp_ENQREGENR_Details.rpt")
            '.reportPath = Server.MapPath("../RPT/rptCorpo_SchoolCurr_Wise_Stat.rpt")

        End With
        Session("rptClass2") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("rptReportViewer.aspx?newwindow=2", True)
    End Sub


    Protected Sub Corp_BSU_ENQREGENR()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim From_Date As String = IIf(Request.QueryString("FROMDATE") <> Nothing, Request.QueryString("FROMDATE"), 0)
        Dim To_Date As String = IIf(Request.QueryString("TODATE") <> Nothing, Request.QueryString("TODATE"), 0)
        Dim Bsu_id As String = IIf(Request.QueryString("BSUID") <> Nothing, Request.QueryString("BSUID"), 0)
        Dim CLM As String = IIf(Request.QueryString("CLM") <> Nothing, Request.QueryString("CLM"), 0)


        Dim str_bsu_ids As New StringBuilder
        Dim bsu_xml As String = ""
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", "999998")
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_XML", Session("Corp_ACD_XML"))
        param.Add("@BSU_ID", Bsu_id.ToString)
        param.Add("@CLM_DESCR", CLM.ToString)

        param.Add("@DATE2", Format(Date.Parse(To_Date.ToString), "dd/MMM/yyyy"))
        param.Add("@DATE1", Format(Date.Parse(From_Date.ToString), "dd/MMM/yyyy"))
        param.Add("DATE2", Format(Date.Parse(To_Date.ToString), "dd/MMM/yyyy"))
        param.Add("DATE1", Format(Date.Parse(From_Date.ToString), "dd/MMM/yyyy"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))

        ''code added for describceny report in the report footer
        'param.Add("@STA_BSU_ID", str_bsu_ids.ToString)
        'param.Add("@FROMDATE", Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy"))
        'param.Add("@TODATE", Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy"))
        'param.Add("@SHF_ID", Nothing)
        'Session("rptdesFROMDATE") = Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy")
        'Session("rptdesTODATE") = Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy")
        ''end
        h_newWindow.Value = "true"
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../../Students/Reports/RPT/rptCorp_ENQREGENR_Grade.rpt")
            '.reportPath = Server.MapPath("../RPT/rptCorpo_SchoolCurr_Wise_Stat.rpt")

        End With
        Session("rptClass2") = rptClass
        ReportLoadSelection()
        'Response.Redirect("rptReportViewer.aspx?newwindow=2", True)
    End Sub
    Protected Sub describcencydetails()
        'h_trn_id.Value = IIf(Request.QueryString("TRN_ID") <> Nothing, Request.QueryString("TRN_ID"), String.Empty)
        'h_bsu_id.Value = IIf(Request.QueryString("BSU_ID") <> Nothing, Request.QueryString("BSU_ID"), String.Empty)
        'h_newWindow.Value = "true"
    End Sub

    Protected Sub ViewTransportFeeCollectionDetails(ByVal p_NEW_FCL_RECNO As String, ByVal p_BSU_ID As String)
        Session("ReportSource") = FeeCollectionTransport.PrintReceipt(p_NEW_FCL_RECNO, Session("sUsr_name"), p_BSU_ID, Session("sBsuid"))
        Response.Redirect("rptviewernew.aspx", True)
    End Sub

    Protected Sub ViewFeeCollectionDetails(ByVal p_Receiptno As String, ByVal p_STU_TYPE As String)
        Dim IsEnquiry As Boolean = False
        If p_STU_TYPE = "E" Then
            IsEnquiry = True
        End If
        Session("ReportSource") = FeeCollection.PrintReceipt(p_Receiptno, Session("sBSUID"), IsEnquiry, Session("sUsr_name"), False)
        Response.Redirect("rptviewernew.aspx", True)
    End Sub

    Private Sub ViewAdjustmentTransport(ByVal doc_id As Integer)
        ViewState("datamode") = Encr_decrData.Encrypt("view")
        Dim mnucode As String = OASISConstants.MNU_FEE_ADJUSTMENTS_TRANSPORT
        Response.Redirect("../../Fees/FeeTransportAdjustment.aspx?FAH_ID=" & Encr_decrData.Encrypt(doc_id) & _
        "&MainMnu_code=" & Encr_decrData.Encrypt(mnucode) & "&datamode=" & ViewState("datamode"))
    End Sub

    Private Sub ViewConcessionDetailsTransport(ByVal doc_id As Integer, ByVal vbsu_id As String)
        ViewState("datamode") = Encr_decrData.Encrypt("view")

        Dim mnucode As String = OASISConstants.MNU_FEE_TRANSPORT_FEE_CONCESSION
        Response.Redirect("../../Fees/FeeConcessionTransBB.aspx?FCH_ID=" & Encr_decrData.Encrypt(doc_id) & _
                      "&MainMnu_code=" & Encr_decrData.Encrypt(mnucode) & _
                      "&datamode=" & ViewState("datamode") & "&bsu=" & vbsu_id)
    End Sub

    Private Sub ViewAdjustment(ByVal doc_id As Integer)
        ViewState("datamode") = Encr_decrData.Encrypt("view")
        Dim mnucode As String = OASISConstants.MNU_FEE_ADJUSTMENTS
        Response.Redirect("../../Fees/FeeAdjustment.aspx?FAH_ID=" & Encr_decrData.Encrypt(doc_id) & _
        "&MainMnu_code=" & Encr_decrData.Encrypt(mnucode) & "&datamode=" & ViewState("datamode"))
    End Sub

    Private Sub ViewConcessionDetails(ByVal doc_id As Integer, ByVal vbsu_id As String)

        ViewState("datamode") = Encr_decrData.Encrypt("view")
        Dim mnucode As String = OASISConstants.MNU_FEE_CONCESSION_TRANS
        Response.Redirect("../../Fees/FeeConcessionTrans.aspx?FCH_ID=" & Encr_decrData.Encrypt(doc_id) & _
                      "&MainMnu_code=" & Encr_decrData.Encrypt(mnucode) & _
                      "&datamode=" & ViewState("datamode") & "&bsu=" & vbsu_id)
    End Sub

    Private Sub GenerateSchool_GradeDetails()
        'COLLECT QUERY STRING
        Dim ACD As String = IIf(Request.QueryString("ACD") <> Nothing, Request.QueryString("ACD"), String.Empty)
        Dim GRD As String = IIf(Request.QueryString("GRD") <> Nothing, Request.QueryString("GRD"), String.Empty)
        Dim BSUID As String = IIf(Request.QueryString("BSUID") <> Nothing, Request.QueryString("BSUID"), String.Empty)
        Dim ASDATE As String = IIf(Request.QueryString("ASDATE") <> Nothing, Request.QueryString("ASDATE"), String.Empty)
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ACD)
        param.Add("@GRD_ID", GRD)
        param.Add("@BSU_ID", BSUID)
        param.Add("@AsOnDate", Format(Date.Parse(ASDATE), "dd/MMM/yyyy"))
        param.Add("@Choice", "1")
        param.Add("BSU_NAME", StrConv(GetBSU_NAME(BSUID), VbStrConv.ProperCase))
        param.Add("AsOnDate", Format(Date.Parse(ASDATE), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../../Students/Reports/RPT/rptCorpo_SchoolGrade_Wise.rpt")
            '.reportPath = Server.MapPath("../RPT_Files/rptStudAttMonth.rpt")
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        'Response.Redirect("rptReportViewer.aspx?newwindow=1", True)
    End Sub

    Private Function GetBSU_NAME(ByVal BSUID As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_sql As String = " Select BSU_NAME FROM   BUSINESSUNIT_M WHERE    BSU_ID = '" & BSUID & "'"
        Return SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_sql)
    End Function

    Private Sub GenerateEdushield_Details()
        'COLLECT QUERY STRING
        Dim ACD As String = IIf(Request.QueryString("ACD") <> Nothing, Request.QueryString("ACD"), String.Empty)
        Dim BSUID As String = IIf(Request.QueryString("BSUID") <> Nothing, Request.QueryString("BSUID"), String.Empty)
        Dim FDATE As String = IIf(Request.QueryString("FDATE") <> Nothing, Request.QueryString("FDATE"), String.Empty)
        Dim TDATE As String = IIf(Request.QueryString("TDATE") <> Nothing, Request.QueryString("TDATE"), String.Empty)
        Dim BSUNAME As String = IIf(Request.QueryString("BSUNAME") <> Nothing, Request.QueryString("BSUNAME"), String.Empty)
        Dim ReportType As String = IIf(Request.QueryString("ReportType") <> Nothing, Request.QueryString("ReportType"), String.Empty)

        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tempFdate As Date = FDATE
        Dim tempTdate As Date = TDATE
        Dim FROMTO As String = String.Empty
        FROMTO = " From " & String.Format("{0:" & OASISConstants.DateFormat & "}", tempFdate) & " to " & String.Format("{0:" & OASISConstants.DateFormat & "}", tempTdate)

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", BSUID)
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ACD)
        param.Add("@FDATE", Format(Date.Parse(FDATE), "dd/MMM/yyyy"))
        param.Add("@TDATE", Format(Date.Parse(TDATE), "dd/MMM/yyyy"))
        param.Add("@reportType", ReportType)
        param.Add("@BSU_ID", BSUID)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("FROMTO", FROMTO)
        param.Add("BSUNAME", BSUNAME)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../../Students/Reports/RPT/rptEDU_SHEILD_SUB.rpt")
            '.reportPath = Server.MapPath("../RPT_Files/rptStudAttMonth.rpt")
        End With
        Session("rptClass2") = rptClass
        ReportLoadSelection()
        '   Response.Redirect("rptReportViewer.aspx?newwindow=2", True)
    End Sub

    Private Sub GenerateSchool_SectionDetails()
        'COLLECT QUERY STRING
        Dim ACD As String = IIf(Request.QueryString("ACD") <> Nothing, Request.QueryString("ACD"), String.Empty)
        Dim GRD As String = IIf(Request.QueryString("GRD") <> Nothing, Request.QueryString("GRD"), String.Empty)
        Dim BSUID As String = IIf(Request.QueryString("BSUID") <> Nothing, Request.QueryString("BSUID"), String.Empty)
        Dim ASDATE As String = IIf(Request.QueryString("ASDATE") <> Nothing, Request.QueryString("ASDATE"), String.Empty)
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ACD)
        param.Add("@GRD_ID", GRD)
        param.Add("@BSU_ID", BSUID)
        param.Add("@AsOnDate", Format(Date.Parse(ASDATE), "dd/MMM/yyyy"))
        param.Add("@Choice", "2")
        param.Add("BSU_NAME", StrConv(GetBSU_NAME(BSUID), VbStrConv.ProperCase))
        param.Add("AsOnDate", Format(Date.Parse(ASDATE), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../../Students/Reports/RPT/rptCorpo_SchoolSection_Wise.rpt")
            '.reportPath = Server.MapPath("../RPT_Files/rptStudAttMonth.rpt")
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("rptReportViewer.aspx?newwindow=2", True)
    End Sub

    Function GetGrade_Section_Report(ByVal ACD As String, ByVal GRD As String, ByVal SCT As String) As String
        Try
            Dim Section As String = String.Empty
            Dim Grade As String = String.Empty
            Using Grade_reader As SqlDataReader = AccessStudentClass.GETGRADE_SECTION_Report(ACD, GRD, SCT)
                If Grade_reader.HasRows = True Then
                    While Grade_reader.Read
                        Section = Grade_reader("SCT")
                        Grade = Grade_reader("GRD")
                    End While
                End If
            End Using
            GetGrade_Section_Report = "Grade : " & Grade & "   Section : " & Section
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetGetGrade_info()")
        End Try
    End Function

    Private Sub GenerateStud_AttDetails()
        'COLLECT QUERY STRING
        Dim ACD As String = IIf(Request.QueryString("ACD") <> Nothing, Request.QueryString("ACD"), String.Empty)
        Dim GRD As String = IIf(Request.QueryString("GRD") <> Nothing, Request.QueryString("GRD"), String.Empty)
        Dim SCT As String = IIf(Request.QueryString("SCT") <> Nothing, Request.QueryString("SCT"), String.Empty)
        Dim SDATE As String = IIf(Request.QueryString("SDATE") <> Nothing, Request.QueryString("SDATE"), String.Empty)
        Dim EDATE As String = IIf(Request.QueryString("EDATE") <> Nothing, Request.QueryString("EDATE"), String.Empty)
        Dim W1 As String = IIf(Request.QueryString("W1") <> Nothing, Request.QueryString("W1"), String.Empty)
        Dim W2 As String = IIf(Request.QueryString("W2") <> Nothing, Request.QueryString("W2"), String.Empty)
        Dim STUD As String = IIf(Request.QueryString("STUD") <> Nothing, Request.QueryString("STUD"), String.Empty)


        Dim startdt As String = String.Empty
        Dim enddt As String = String.Empty
        Call getStartdt_enddt(ACD, startdt, enddt)


        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim Ason_Date As String = ""
        Dim i As Integer = 0

        Dim SDT As String = startdt 'Format(Date.Parse(SDATE), "dd/MMM/yyyy")
        Dim EDT As String = enddt ' Format(Date.Parse(EDATE), "dd/MMM/yyyy")
        Dim CurrentDate As String = Format(Date.Parse(Now.Date), "dd/MMM/yyyy")
        Dim Current_Year As String = GetCurrentACY_DESCR(ACD)

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ACD)
        param.Add("@GRD_ID", GRD)
        param.Add("@SCT_ID", CInt(SCT))
        'param.Add("@STARTDATE", Format(Date.Parse(SDATE)), "dd/MMM/yyyy"))
        ' param.Add("@ENDDATE", Format(Date.Parse(EDATE), "dd/MMM/yyyy"))
        param.Add("@STARTDATE", startdt) 'Format(Date.Parse(SDATE), "dd/MMM/yyyy"))
        param.Add("@ENDDATE", enddt) 'Format(Date.Parse(EDATE), "dd/MMM/yyyy"))
        param.Add("@WEEKEND1", W1)
        param.Add("@WEEKEND2", W2)
        param.Add("@STU_ID", STUD)
        'param.Add("@filepath", TransferPath(STUD))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("Acad_Year", Current_Year)
        param.Add("TWORKDAY", GETTotal_WorkingDay(ACD, GRD, SCT, SDT, EDT, W1, W2))
        param.Add("CWORKDAY", GETTotal_WorkingDay_TillDate(ACD, GRD, SCT, SDT, CurrentDate, W1, W2))
        Session("noimg") = "yes"

        Dim rptClass As New rptClass
        With rptClass
            .Photos = GetPhotoClass(STUD & "|")
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../../Students/Reports/RPT/rptStudAtt_Detail.rpt")
            '.reportPath = Server.MapPath("../RPT_Files/rptStudAttMonth.rpt")
        End With
        'Session("rptClass3") = rptClass
        'Response.Redirect("rptReportViewer.aspx?newwindow=3", True)
        Session("rptClass") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("rptReportViewer.aspx?newwindow=3", True)
        'Response.Redirect("rptReportViewer.aspx?")

    End Sub
    Function GetPhotoClass(ByVal stud As String) As OASISPhotos
        Dim vPhoto As New OASISPhotos
        vPhoto.BSU_ID = Session("sBSUID")
        vPhoto.PhotoType = OASISPhotoType.STUDENT_PHOTO
        Dim arrList As ArrayList = New ArrayList(stud.Split("|"))
        For Each vVal As Object In arrList
            If vVal.ToString <> "" Then
                vPhoto.IDs.Add(vVal)
            End If
        Next
        Return vPhoto
    End Function
    Private Sub LoadImage(ByVal objDataRow As DataRow, ByVal strImageField As String, ByVal FilePath As String)
        Try
            Dim fs As New FileStream(FilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
            Dim Image As Byte() = New Byte(fs.Length - 1) {}
            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            fs.Close()
            objDataRow(strImageField) = Image
        Catch ex As Exception
            Response.Write("<font color=red>" & ex.Message & "</font>")
        End Try
    End Sub

    Function TransferPath(ByVal STU_ID As String) As String
        Dim FilePath As String = GETStud_photoPath(STU_ID) 'get the image
        Dim Virtual_Path As String = Web.Configuration.WebConfigurationManager.AppSettings("StudentPhotoPath").ToString()  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
        If FilePath.Trim = "" Then
            TransferPath = Virtual_Path + "/NOIMG/no_image.jpg"
            ' TransferPath = "C:\no_image.jpg"
        Else
            'TransferPath = "C:\no_image.jpg"
            TransferPath = Virtual_Path + FilePath
        End If
    End Function

    Private Function GETStud_photoPath(ByVal STU_ID As String) As String
        STU_ID = CStr(CInt(STU_ID))
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FROM STUDENT_M where  STU_ID='" & STU_ID & " '"
        Dim RESULT As String
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            RESULT = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return RESULT
    End Function
    Private Sub getStartdt_enddt(ByVal acd_id As String, ByRef startdt As String, ByRef enddt As String)

        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@acd_id", acd_id)
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "ATT.GETSTARTDT_ENDDT", param)
            While datareader.Read
                startdt = Convert.ToString(datareader("ACD_STARTDT"))
                enddt = Convert.ToString(datareader("ACD_ENDDT"))

            End While
        End Using


    End Sub

    Private Sub GenerateMonthWiseDetails()
        'COLLECT QUERY STRING
        Dim ACD As String = IIf(Request.QueryString("ACD") <> Nothing, Request.QueryString("ACD"), String.Empty)
        Dim GRD As String = IIf(Request.QueryString("GRD") <> Nothing, Request.QueryString("GRD"), String.Empty)
        Dim SCT As String = IIf(Request.QueryString("SCT") <> Nothing, Request.QueryString("SCT"), String.Empty)
        Dim SDATE As String = IIf(Request.QueryString("SDATE") <> Nothing, Request.QueryString("SDATE"), String.Empty)
        Dim EDATE As String = IIf(Request.QueryString("EDATE") <> Nothing, Request.QueryString("EDATE"), String.Empty)
        Dim W1 As String = IIf(Request.QueryString("W1") <> Nothing, Request.QueryString("W1"), String.Empty)
        Dim W2 As String = IIf(Request.QueryString("W2") <> Nothing, Request.QueryString("W2"), String.Empty)
        Dim FMONTH As String = IIf(Request.QueryString("FMONTH") <> Nothing, Request.QueryString("FMONTH"), String.Empty)
        Dim startdt As String = String.Empty
        Dim enddt As String = String.Empty
        Call getStartdt_enddt(ACD, startdt, enddt)

        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim Ason_Date As String = ""
        Dim i As Integer = 0
        Dim ARS_PRE_DESC_S1 As String = String.Empty
        Dim ARS_PRE_DISP_S1 As String = String.Empty
        Dim ARS_PRE_DESC_S2 As String = String.Empty
        Dim ARS_PRE_DISP_S2 As String = String.Empty
        Dim ARS_ABS_DESC As String = String.Empty
        Dim ARS_ABS_DISP As String = String.Empty
        Dim ARS_UNMARK_DESC As String = String.Empty
        Dim ARS_UNMARK_DISP As String = String.Empty
        Dim ARS_LATE_DESC As String = String.Empty
        Dim ARS_LATE_DISP As String = String.Empty
        Dim ARS_LEAVE_DESC As String = String.Empty
        Dim ARS_LEAVE_DISP As String = String.Empty
        Dim ARS_S1_DESC As String = String.Empty
        Dim ARS_S1_DISP As String = String.Empty
        Dim ARS_S2_DESC As String = String.Empty
        Dim ARS_S2_DISP As String = String.Empty
        Dim PRS_S1_DISP As String = String.Empty
        Dim PRS_S2_DISP As String = String.Empty
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ACD)
        param.Add("@GRD_ID", GRD)
        param.Add("@SCT_ID", CInt(SCT))
        Dim SDT As String = startdt ' Format(Date.Parse(SDATE), "dd/MMM/yyyy") 'Format(Date.Parse("01/JUN/2008"), "dd/MMM/yyyy") 
        Dim EDT As String = enddt 'Format(Date.Parse(EDATE), "dd/MMM/yyyy") 'Format(Date.Parse("31/MAY/2009"), "dd/MMM/yyyy") 
        Dim TYEAR As String = GETYEAR_ONLY(ACD, GRD, SCT, SDT, EDT, W1, W2, FMONTH)
        Dim Month_stdt As String = "01/" + FMONTH + "/" + TYEAR

        param.Add("@STARTDATE", startdt) ' Format(Date.Parse(SDATE), "dd/MMM/yyyy"))
        param.Add("@ENDDATE", enddt) 'Format(Date.Parse(EDATE), "dd/MMM/yyyy"))

        param.Add("@WEEKEND1", W1)
        param.Add("@WEEKEND2", W2)
        param.Add("@FORMONTH", FMONTH)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("GradeSect", GetGrade_Section_Report(ACD, GRD, CStr(CInt(SCT))))
        param.Add("MONTH_STDT", Format(Date.Parse(Month_stdt), "dd/MMM/yyyy"))
        
        Dim MonthYear As String = FMONTH & " " & TYEAR
        param.Add("MonthYear", MonthYear)

        Dim TDATE As Date = Format(Date.Parse(GETREPORT_YEAR(ACD, GRD, CInt(SCT), SDT, EDT, W1, W2, FMONTH)), "dd/MMM/yyyy")
        Dim LastDayofTDATE As Date = DateAdd(DateInterval.Second, -3, DateAdd(DateInterval.Month, DateDiff("m", Date.MinValue, TDATE) + 1, Date.MinValue))

        Dim TDay As Integer = DatePart(DateInterval.Day, LastDayofTDATE)
        param.Add("TOTDAY", TDay)
        Dim Current_Year As String = GetCurrentACY_DESCR(ACD)
        param.Add("Acad_Year", Current_Year)

        Using readerAttendance_Param As SqlDataReader = AccessStudentClass.GetAttendance_Param_NEW(ACD)
            If readerAttendance_Param.HasRows = True Then
                While readerAttendance_Param.Read
                    If Convert.ToString(readerAttendance_Param("ARP_DESCR")) = "SESSION1" Then
                        ARS_S1_DESC = Convert.ToString(readerAttendance_Param("ARP_DESCR"))
                        ARS_S1_DISP = Convert.ToString(readerAttendance_Param("ARP_DISP"))
                    End If
                    If Convert.ToString(readerAttendance_Param("ARP_DESCR")) = "SESSION2" Then
                        ARS_S2_DESC = Convert.ToString(readerAttendance_Param("ARP_DESCR"))
                        ARS_S2_DISP = Convert.ToString(readerAttendance_Param("ARP_DISP"))
                    End If

                    If Convert.ToString(readerAttendance_Param("ARP_DESCR")) = "PRESENT" Then
                        PRS_S1_DISP = Convert.ToString(readerAttendance_Param("ARP_DISP"))
                    End If
                    If Convert.ToString(readerAttendance_Param("ARP_DESCR")) = "PRESENT2" Then
                        PRS_S2_DISP = Convert.ToString(readerAttendance_Param("ARP_DISP"))

                    End If
                End While
            End If
        End Using
        param.Add("PRS_S1_DISP", PRS_S1_DISP)
        param.Add("PRS_S2_DISP", PRS_S2_DISP)
        param.Add("ARS_S1_DESC", ARS_S1_DESC)
        param.Add("ARS_S1_DISP", ARS_S1_DISP)
        param.Add("ARS_S2_DESC", ARS_S2_DESC)
        param.Add("ARS_S2_DISP", ARS_S2_DISP)
        Dim iCOUNT As Integer = GETSESSION_COUNT(ACD, GRD, FMONTH)
        Dim rptClass As New rptClass
        With rptClass
            .previousClass = Session("rptClass")
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            If iCOUNT = 2 Then
                .reportPath = Server.MapPath("../../Students/Reports/RPT/rptStudAttMonth.rpt")
            Else
                .reportPath = Server.MapPath("../../Students/Reports/RPT/rptStudAttMonth_S1.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("rptReportViewer.aspx?newwindow=4", True)
    End Sub

    Private Function GETSESSION_COUNT(ByVal ACD As String, ByVal GRD As String, ByVal FMONTH As String) As Integer
        Dim ATT_Types As String = String.Empty
        Dim separator As String() = New String() {"|"}
        Using readerSTUD_ATT_SESS As SqlDataReader = AccessStudentClass.GetAtt_SESSION_COUNT(ACD, GRD, FMONTH)
            While readerSTUD_ATT_SESS.Read
                ATT_Types = ATT_Types + Convert.ToString(readerSTUD_ATT_SESS("SES_LIST")) + "|"
            End While
        End Using
        Dim strSplitArr As String() = ATT_Types.Split(separator, StringSplitOptions.RemoveEmptyEntries)
        Dim iCOUNT As Integer
        For Each arrStr As String In strSplitArr

            If Array.IndexOf(strSplitArr, "Ses1&Ses2") >= 0 Then
                iCOUNT = 2
                Exit For
            Else
                iCOUNT = iCOUNT + 1
            End If
        Next
        GETSESSION_COUNT = iCOUNT
    End Function

    Private Function GETYEAR_ONLY(ByVal ACD As String, ByVal GRD As String, ByVal SCT As Integer, ByVal SDT As String, ByVal EDT As String, ByVal W1 As String, ByVal W2 As String, ByVal FMONTH As String) As String
        Try
            Dim TYEAR As String = String.Empty
            Using readerDate As SqlDataReader = AccessStudentClass.GETREPORT_YEAR_ACD(ACD, GRD, SCT, SDT, EDT, W1, W2, FMONTH)
                If readerDate.HasRows = True Then
                    While readerDate.Read
                        TYEAR = Convert.ToString(readerDate("TYEAR"))
                    End While
                End If
            End Using
            GETYEAR_ONLY = TYEAR
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function

    Private Function GETREPORT_YEAR(ByVal ACD As String, ByVal GRD As String, ByVal SCT As Integer, ByVal SDT As String, ByVal EDT As String, ByVal W1 As String, ByVal W2 As String, ByVal FMONTH As String) As String
        Try
            Dim TYEAR As String = String.Empty
            Using readerDate As SqlDataReader = AccessStudentClass.GETREPORT_YEAR_ACD(ACD, GRD, SCT, SDT, EDT, W1, W2, FMONTH)
                If readerDate.HasRows = True Then
                    While readerDate.Read
                        TYEAR = Convert.ToString(readerDate("TYEAR"))
                    End While
                End If
            End Using
            GETREPORT_YEAR = "1" & "/" & FMONTH & "/" & TYEAR
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function

    Private Function GETTotal_WorkingDay(ByVal ACD As String, ByVal GRD As String, ByVal SCT As Integer, ByVal SDT As String, ByVal EDT As String, ByVal W1 As String, ByVal W2 As String) As String
        Dim sqlString As String = "select count(WDATE)  from fn_TWORKINGDAYS('" & ACD & "','" & GRD & "','" & SCT & "','" & SDT & "','" & EDT & "','" & W1 & "','" & W2 & "')"
        Dim result As Object
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            result = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return CStr(result)
    End Function

    Private Function GETTotal_WorkingDay_TillDate(ByVal ACD As String, ByVal GRD As String, ByVal SCT As Integer, ByVal SDT As String, ByVal CurrentDate As String, ByVal W1 As String, ByVal W2 As String) As String
        Dim sqlString As String = "select count(WDATE)  from fn_TWORKINGDAYS('" & ACD & "','" & GRD & "','" & SCT & "','" & SDT & "','" & CurrentDate & "','" & W1 & "','" & W2 & "')"
        Dim result As Object
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            result = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return CStr(result)
    End Function

    Private Sub GenerateDailyAttDetails()
        'COLLECT QUERY STRING
        Dim ACD As String = IIf(Request.QueryString("ACD") <> Nothing, Request.QueryString("ACD"), String.Empty)
        Dim GRD As String = IIf(Request.QueryString("GRD") <> Nothing, Request.QueryString("GRD"), String.Empty)
        Dim SCT As String = IIf(Request.QueryString("SCT") <> Nothing, Request.QueryString("SCT"), String.Empty)
        Dim SDATE As String = IIf(Request.QueryString("SDATE") <> Nothing, Request.QueryString("SDATE"), String.Empty)
        Dim EDATE As String = IIf(Request.QueryString("EDATE") <> Nothing, Request.QueryString("EDATE"), String.Empty)
        Dim W1 As String = IIf(Request.QueryString("W1") <> Nothing, Request.QueryString("W1"), String.Empty)
        Dim W2 As String = IIf(Request.QueryString("W2") <> Nothing, Request.QueryString("W2"), String.Empty)
        Dim TDAY As String = IIf(Request.QueryString("TDAY") <> Nothing, Request.QueryString("TDAY"), String.Empty)
        Dim TMONTH As String = IIf(Request.QueryString("TMONTH") <> Nothing, Request.QueryString("TMONTH"), String.Empty)
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim Ason_Date As String = ""
        Dim i As Integer = 0
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ACD)
        param.Add("@GRD_ID", GRD)
        param.Add("@SCT_ID", CInt(SCT))

        Dim SDT As String = Format(Date.Parse(SDATE), "dd/MMM/yyyy")
        Dim EDT As String = Format(Date.Parse(EDATE), "dd/MMM/yyyy")
        param.Add("@STARTDATE", Format(Date.Parse(SDATE), "dd/MMM/yyyy"))
        param.Add("@ENDDATE", Format(Date.Parse(EDATE), "dd/MMM/yyyy"))
        'param.Add("@STARTDATE", Format(Date.Parse("01/JUN/2008"), "dd/MMM/yyyy"))
        'param.Add("@ENDDATE", Format(Date.Parse("31/MAY/2009"), "dd/MMM/yyyy"))
        param.Add("@WEEKEND1", W1)
        param.Add("@WEEKEND2", W2)
        param.Add("@TDAY", TDAY) '"16")
        param.Add("@TMONTH", TMONTH) '"Jul") 
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("GradeSect", GetGrade_Section_Report(ACD, GRD, CStr(CInt(SCT))))

        Dim TYEAR As String = GETYEAR_ONLY(ACD, GRD, SCT, SDT, EDT, W1, W2, TMONTH)
        Dim DayMonthYear As String = TDAY & "/" & TMONTH & "/" & TYEAR

        param.Add("RDate", DayMonthYear)
        Dim Current_Year As String = GetCurrentACY_DESCR(ACD)
        param.Add("Acad_Year", Current_Year)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../../Students/Reports/RPT/rptStudAttDaily.rpt")
            '.reportPath = Server.MapPath("../RPT/rptStudAttDaily.rpt")
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        '    Response.Redirect("rptReportViewer.aspx?newwindow=5", True)
    End Sub

    Private Function GetCurrentACY_DESCR(ByVal ACD_ID As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_sql As String = "SELECT  [ACY_DESCR]  FROM [ACADEMICYEAR_M] where ACY_ID=(SELECT [ACD_ACY_ID]   FROM [ACADEMICYEAR_D] where ACD_ID='" & ACD_ID & "')"
        Return SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_sql)
    End Function

    Private Sub GeneratePartyLedger()
        GeneratePartyLedger(Session("BSUNames"))
    End Sub

    Private Sub GeneratePartyLedger(ByVal BSUIDs As String, Optional ByVal ACT_CTRLACC As String = "06301000")
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim sql_query As String = "SELECT ACT_ID FROM ACCOUNTS_M WHERE ACT_CTRLACC = '" & ACT_CTRLACC & "' "

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, sql_query)
        Dim accounts As String = String.Empty
        While (dr.Read())
            accounts += dr(0).ToString() + "||"
        End While
        Dim cmd As New SqlCommand("RPTPARTYsLEDGER", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@TRN_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = BSUIDs
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        sqlpACT_IDs.Value = GenerateXML(accounts, XMLType.ACTName)
        cmd.Parameters.Add(sqlpACT_IDs)

        Dim sqlpJHD_FYEAR As New SqlParameter("@TRN_FYEAR", SqlDbType.Int)
        sqlpJHD_FYEAR.Value = Session("F_YEAR")
        cmd.Parameters.Add(sqlpJHD_FYEAR)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = Session("rptFromDate")
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@TODOCDT", SqlDbType.DateTime)
        sqlpTODOCDT.Value = Session("rptToDate")
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = Session("rptGroupCurrency")
        cmd.Parameters.Add(sqlpGroupCurrency)

        cmd.CommandTimeout = 120
        adpt.SelectCommand = cmd
        objConn.Close()
        objConn.Open()
        adpt.Fill(ds)
        If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("UserName") = Session("sUsr_name")
            params("FromDate") = Session("rptFromDate")
            params("ToDate") = Session("rptToDate")
            params("ReportCaption") = "Creditors Ledger"
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            'If chkSummary.Checked Then
            '    repSource.ResourceName = "../RPT_Files/rptPartysLedgerSummary.rpt"
            'Else
            repSource.ResourceName = "../RPT_Files/rptPartysLedgerSummary.rpt"
            'repSource.ResourceName = "../RPT_Files/rptPartysLedgerwithAging.rpt"
            'End If
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
        End If
        objConn.Close()
        Response.Redirect("rptviewernew.aspx?newwindow=true", True)
        'Response.Write("<script>window.open('rptviewer.aspx')</script>")
    End Sub

    Private Sub GetSiblingInformation()
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("@bsu_id") = Session("sbsuID")
        params("@accyear") = IIf(Request.QueryString("ACD") <> Nothing, Request.QueryString("ACD"), String.Empty)
        params("@Grade") = String.Empty
        params("@sib_Count") = String.Empty
        params("@STUID") = Request.QueryString("STUID").Replace(",", "") 'IIf(Request.QueryString("STUID") <> Nothing, Request.QueryString("STUID"), String.Empty)
        repSource.Parameter = params

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = params
            .reportPath = Server.MapPath("../RPT_Files/rptSiblingDetails.rpt")
        End With
        Session("rptClass5") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("rptReportViewer.aspx?newwindow=5", True)

    End Sub

    Private Sub GetLedger(ByVal docType As String)
        Dim Encr_decrData As New Encryption64
        Dim MainMnu_code As String
        Dim datamode As String = Encr_decrData.Encrypt("view")
        Dim BSUID As String = IIf(Request.QueryString("BSUID") <> Nothing, Request.QueryString("BSUID"), String.Empty)
        Select Case docType.ToUpper()
            Case "JV"
                MainMnu_code = Encr_decrData.Encrypt("A150020")
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim strDocNO As String = Request.QueryString("DOCNO")
                Dim strQuery As String = "select GUID from JOURNAL_H where JHD_FYEAR =" & Session("F_YEAR") & "and JHD_DOCTYPE = '" & docType & "' and JHD_DOCNO = '" & strDocNO & "' and JHD_BSU_ID = '" & BSUID & "'" ' & Session("BSUNames") & "'"
                Dim guid As String = Convert.ToString(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery))
                If guid <> String.Empty And guid <> "" Then
                    Response.Redirect("../../Accounts/accjvjournalvoucher.aspx?viewid=" & guid & "&MainMnu_code=" & MainMnu_code & "&datamode=" & datamode & "&BSUID=" & BSUID)
                End If
            Case "SJV"
                MainMnu_code = Encr_decrData.Encrypt("A150080")
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim strDocNO As String = Request.QueryString("DOCNO")
                Dim strQuery As String = "select GUID from SJOURNAL_H where SHD_FYEAR =" & Session("F_YEAR") & "and SHD_DOCTYPE = '" & docType & "' and SHD_DOCNO = '" & strDocNO & "' and SHD_BSU_ID = '" & BSUID & "'" ' & Session("BSUNames") & "'"
                Dim guid As String = Convert.ToString(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery))
                If guid <> String.Empty And guid <> "" Then
                    Response.Redirect("../../Accounts/accsjvSJournalvoucher.aspx?viewid=" & guid & "&MainMnu_code=" & MainMnu_code & "&datamode=" & datamode & "&BSUID=" & BSUID)
                End If
            Case "PP"
                MainMnu_code = Encr_decrData.Encrypt("A150030")
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim strDocNO As String = Request.QueryString("DOCNO")
                strDocNO = strDocNO.Substring(0, strDocNO.LastIndexOf("-"))
                Dim strQuery As String = "select GUID from PREPAYMENTS_H where PRP_FYEAR =" & Session("F_YEAR") & " and PRP_ID = '" & strDocNO & "' and PRP_BSU_ID = '" & BSUID & "'" ' & Session("BSUNames") & "'"
                Dim guid As String = Convert.ToString(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery))
                If guid <> String.Empty And guid <> "" Then
                    'Response.Redirect("../../Accounts/accjvjournalvoucher.aspx?viewid=" & guid & "&MainMnu_code=" & MainMnu_code & "&datamode=" & datamode & "&BSUID=" & BSUID)
                    guid = Encr_decrData.Encrypt(guid)
                    Response.Redirect(String.Format("~\Accounts\accPrepaymentsAddEdit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, datamode, guid))
                End If
            Case "IJV"
                MainMnu_code = Encr_decrData.Encrypt("A150025")
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim strDocNO As String = Request.QueryString("DOCNO")
                Dim strQuery As String = "select GUID from IJOURNAL_H where IJH_FYEAR =" & Session("F_YEAR") & " and IJH_DOCTYPE = '" & docType & "' and IJH_DOCNO = '" & strDocNO & "' and (IJH_CR_BSU_ID = '" & BSUID & "' OR IJH_DR_BSU_ID = '" & BSUID & "') " ' & Session("BSUNames") & "'"
                Dim guid As String = Convert.ToString(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery))
                If guid <> String.Empty And guid <> "" Then
                    Response.Redirect("../../Accounts/acciujournalvoucher.aspx?viewid=" & guid & "&MainMnu_code=" & MainMnu_code & "&datamode=" & datamode & "&BSUID=" & BSUID)
                End If
            Case "CP"
                MainMnu_code = Encr_decrData.Encrypt("A150001")
                'Get the GUID of VOUCHER_H from JOURNAL_H
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim strDocNO As String = Request.QueryString("DOCNO")
                Dim strQuery As String = "select GUID from VOUCHER_H where VHH_FYEAR =" & Session("F_YEAR") & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & strDocNO & "' and VHH_BSU_ID = '" & BSUID & "'" ' & Session("BSUNames") & "'" 
                Dim guid As String = Convert.ToString(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery))
                If guid <> String.Empty And guid <> "" Then
                    Response.Redirect("../../Accounts/acccpCashPayments.aspx?viewid=" & guid & "&MainMnu_code=" & MainMnu_code & "&datamode=" & datamode & "&BSUID=" & BSUID)
                End If
            Case "BR"
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim strDocNO As String = Request.QueryString("DOCNO")
                Dim strQuery As String = "select GUID from VOUCHER_H where VHH_FYEAR =" & Session("F_YEAR") & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & strDocNO & "' and VHH_BSU_ID = '" & BSUID & "'" ' & Session("BSUNames") & "'" 
                Dim guid As String = Convert.ToString(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery))
                MainMnu_code = Encr_decrData.Encrypt("A150015")
                If guid <> String.Empty And guid <> "" Then
                    guid = Encr_decrData.Encrypt(guid)
                    Response.Redirect(String.Format("../../Accounts/AccBankR.aspx?MainMnu_code={0}&datamode={1}&Eid={2}&BSUID={3}", MainMnu_code, datamode, guid, BSUID))
                End If
            Case "PJ"
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim strDocNO As String = Request.QueryString("DOCNO")
                Dim strQuery As String = "select GUID from PURCHASE_H where PUH_FYEAR =" & Session("F_YEAR") & " and PUH_DOCTYPE = '" & docType & "' and PUH_DOCNO = '" & strDocNO & "' and PUH_BSU_ID = '" & BSUID & "'" ' & Session("BSUNames") & "'" 
                Dim guid As String = Convert.ToString(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery))
                MainMnu_code = Encr_decrData.Encrypt("A150021")
                If guid <> String.Empty And guid <> "" Then
                    guid = Encr_decrData.Encrypt(guid)
                    Response.Redirect(String.Format("../../Accounts/AccPJ.aspx?MainMnu_code={0}&datamode={1}&Eid={2}&BSUID={3}", MainMnu_code, datamode, guid, BSUID))
                End If
            Case "BP"
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim strDocNO As String = Request.QueryString("DOCNO")
                Dim strQuery As String = "select GUID from VOUCHER_H where VHH_FYEAR =" & Session("F_YEAR") & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & strDocNO & "' and VHH_BSU_ID = '" & BSUID & "'" ' & Session("BSUNames") & "'" 
                Dim guid As String = Convert.ToString(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery))
                If guid <> String.Empty And guid <> "" Then
                    strQuery = "select VHH_bPDC from VOUCHER_H where VHH_FYEAR =" & Session("F_YEAR") & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & strDocNO & "' and VHH_BSU_ID = '" & BSUID & "'" ' & Session("BSUNames") & "'" 
                    Dim retObj = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)
                    Dim bPDC As Boolean = IIf(retObj Is Nothing, False, retObj)
                    guid = Encr_decrData.Encrypt(guid)
                    If bPDC Then
                        MainMnu_code = Encr_decrData.Encrypt("A150013")
                        Response.Redirect(String.Format("../../Accounts/AccAddPDC.aspx?MainMnu_code={0}&datamode={1}&Eid={2}&BSUID={3}", MainMnu_code, datamode, guid, BSUID))
                    Else
                        MainMnu_code = Encr_decrData.Encrypt("A150010")
                        Response.Redirect(String.Format("../../Accounts/AccAddBankTran.aspx?MainMnu_code={0}&datamode={1}&Eid={2}&BSUID={3}", MainMnu_code, datamode, guid, BSUID))
                    End If
                End If
            Case "CR"
                MainMnu_code = Encr_decrData.Encrypt("A150005")
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim strDocNO As String = Request.QueryString("DOCNO")
                Dim strQuery As String = "select GUID from VOUCHER_H where VHH_FYEAR =" & Session("F_YEAR") & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & strDocNO & "' and VHH_BSU_ID = '" & BSUID & "'" ' & Session("BSUNames") & "'"
                Dim guid As String = Convert.ToString(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery))
                If guid <> String.Empty And guid <> "" Then
                    Response.Redirect("../../Accounts/acccrCashReceipt.aspx?viewid=" & guid & "&MainMnu_code=" & MainMnu_code & "&datamode=" & datamode & "&BSUID=" & BSUID)
                End If
            Case "DN"
                MainMnu_code = Encr_decrData.Encrypt("A150040")
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim strDocNO As String = Request.QueryString("DOCNO")
                Dim strQuery As String = "select GUID from VOUCHER_H where VHH_FYEAR =" & Session("F_YEAR") & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & strDocNO & "' and VHH_BSU_ID = '" & BSUID & "'" ' & Session("BSUNames") & "'"
                Dim guid As String = Convert.ToString(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery))
                If guid <> String.Empty And guid <> "" Then
                    Response.Redirect("../../Accounts/acccnDebitNote.aspx?viewid=" & guid & "&MainMnu_code=" & MainMnu_code & "&datamode=" & datamode & "&BSUID=" & BSUID)
                End If
            Case "CN"
                MainMnu_code = Encr_decrData.Encrypt("A150045")
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                Dim strDocNO As String = Request.QueryString("DOCNO")
                Dim strQuery As String = "select GUID from VOUCHER_H where VHH_FYEAR =" & Session("F_YEAR") & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & strDocNO & "' and VHH_BSU_ID = '" & BSUID & "'" ' & Session("BSUNames") & "'"
                Dim guid As String = Convert.ToString(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery))
                If guid <> String.Empty And guid <> "" Then
                    Response.Redirect("../../Accounts/acccnDebitNote.aspx?viewid=" & guid & "&MainMnu_code=" & MainMnu_code & "&datamode=" & datamode & "&BSUID=" & BSUID)
                End If
            Case Else
                Response.Redirect("rptviewernew.aspx?newwindow=true", True)
                'Response.Write("<script>window.open('rptviewer.aspx')</script>")
        End Select
        Response.Redirect("rptviewernew.aspx?newwindow=true", True)
    End Sub

    Private Sub GetLedger(ByVal BSUName As String, ByVal ACTName As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTLEDGER", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@JHD_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = GenerateXML(BSUName, XMLType.BSUName)
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        sqlpACT_IDs.Value = GenerateXML(ACTName, XMLType.ACTName)
        cmd.Parameters.Add(sqlpACT_IDs)

        Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        sqlpJHD_FYEAR.Value = Session("F_YEAR")
        cmd.Parameters.Add(sqlpJHD_FYEAR)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = Session("rptFromDate")
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@TODOCDT", SqlDbType.DateTime)
        sqlpTODOCDT.Value = Session("rptToDate")
        cmd.Parameters.Add(sqlpTODOCDT)

        'Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        'sqlpConsolidation.Value = chkConsolidation.Checked
        'cmd.Parameters.Add(sqlpConsolidation)

        Dim sqlpGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = Session("rptGroupCurrency")
        cmd.Parameters.Add(sqlpGroupCurrency)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("UserName") = Session("sUsr_name")
            params("FromDate") = Session("rptFromDate")
            params("ToDate") = Session("rptToDate")
            params("ReportCaption") = "General Ledger"
            params("summary") = False
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptCashBook.rpt"
            Session("ReportSource") = repSource
            objConn.Close()
        End If
        objConn.Close()
        Response.Redirect("rptviewernew.aspx?newwindow=true", True)
        'Response.Write("<script>window.open('rptviewer.aspx')</script>")
        'Response.Redirect(Request.UrlReferrer.ToString(), True)

    End Sub

    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.ACTName
                elements(0) = "ACT_DETAILS"
                elements(1) = "ACT_DETAIL"
                elements(2) = "ACT_ID"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                XMLEBSUID = xmlDoc.CreateElement(elements(2))
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Function GetBSUID(ByVal BSUName As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT BSU_ID FROM BUSINESSUNIT_M WHERE BSU_NAME IN('" + BSUName + "')"
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
    End Function

    Private Function GetACTName(ByVal ACTID As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT ACT_NAME FROM ACCOUNTS_M WHERE ACT_ID IN('" + ACTID + "')"
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
    End Function

    Sub ReportLoadSelection()
        Session("ReportSel") = "NT"
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
