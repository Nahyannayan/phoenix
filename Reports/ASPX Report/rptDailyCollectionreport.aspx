<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptDailyCollectionreport.aspx.vb" Inherits="Reports_ASPX_Report_rptDailyreport"  %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">
           function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 239px; ";
            sFeatures+="dialogHeight: 264px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
             switch(txtControl)
            {
              case 0:
                url="../../Accounts/calendar.aspx?dt="+document.getElementById('<%=txtfromDate.ClientID %>').value;
                break;
               case 1:
                url="../../Accounts/calendar.aspx?dt="+document.getElementById('<%=txtToDate.ClientID %>').value;
                break;
            }    
            result = window.showModalDialog(url,"", sFeatures);
            if (result=='' || result==undefined)
            {
                return false;
            }
            switch(txtControl)
            {
              case 0:
                document.getElementById('<%=txtfromDate.ClientID %>').value=result;
                break;
               case 1:
                document.getElementById('<%=txtToDate.ClientID %>').value=result;
                break;
            }    
           }
   
         function GetBSUName()
          {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 450px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx","", sFeatures)
            if(result != "")
            {
              document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
              document.forms[0].submit();
            }
         }
    </script>
        
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><br />
        
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0" >
        <tr  class="subheader_img">
            <td align="left" colspan="8" valign="middle" style="height: 19px">                
                        <asp:Label ID="lblCaption" runat="server"></asp:Label> 
            </td>
        </tr>
        <tr>
            <td align="left" class="matters">
                From Date</td>
            <td class="matters" style="height: 1px">
                :</td>
            <td align="left" class="matters" colspan="2" >
                <asp:TextBox ID="txtFromDate" runat="server" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
            <td align="left" class="matters">
                To Date</td>
            <td align="left" class="matters" style="height: 1px">
                :</td>
            <td align="left" class="matters" colspan="2" style="height: 1px; text-align: left">
                <asp:TextBox ID="txtToDate" runat="server" Width="122px"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>&nbsp;
            </td>
            
        </tr>
        <tr style="color: #1b80b6" id = "trBSUName" runat = "server">
            <td align="left" valign = "top" class="matters" style="height: 19px" >
                Business Unit</td>
            <td class="matters" valign = "top" style="height: 19px">
                :</td>
            <td align="left" class="matters" colspan="6" valign="top">
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                </td>
        </tr>
        <tr style="color: #1b80b6" id = "trChkBoxes" runat = "server">
            <td align="left" class="matters" colspan="8">
                <asp:CheckBox ID="chkGroupCurrency" runat="server" Text="View in Group Currency" /><br />
                <asp:CheckBox ID="chkConsolidation" runat="server" Text="Consolidation" /></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="text-align: right">
                <asp:LinkButton id="lnkExporttoexcel"
                    runat="server" onclick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" /> 
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
            </td>
        </tr>
    </table> 
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_BSUID" runat="server" />
</asp:Content>