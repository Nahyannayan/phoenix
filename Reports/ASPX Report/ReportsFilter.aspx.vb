Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Partial Class Reports_ASPX_Report_ReportsFilter
    Inherits System.Web.UI.Page

    Public ControlNa As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load


        If IsPostBack = False Then
            Try
                hidFrom.Value = Request.QueryString("FROM")
                getList()
            Catch ex As Exception
                MsgLable.Text = ex.Message
            End Try

        End If

    End Sub
    Private Sub getList()

        Dim MainObj As Mainclass = New Mainclass()

        Try


            Dim Query As String = ""
            Dim dbName As String = "MainDB"
            Dim FromReq As String = hidFrom.Value.ToString()
            Dim likString As String = "%" & txtFind.Text.ToString() & "%"
            Dim BsuUnit As String = ""

            

            If FromReq.Equals("DPT") Then

                Query = " SELECT DPT_ID, DPT_DESCR FROM  OASIS.dbo.DEPARTMENT_M WHERE DPT_DESCR LIKE '" & likString & "'  ORDER BY DPT_DESCR "
                labHead.Text = "Department"
                dbName = "MainDBO"

            End If
            Session("QUERY") = Query

            Dim table As DataTable = MainObj.getRecords(Query, dbName)

            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            gvJournal.DataSource = table
            gvJournal.DataBind()

            If gvJournal.Rows.Count = 0 Then
                Exit Sub
            End If

            Dim TxtName1 As New TextBox
            Dim TxtCode1 As New TextBox

            TxtCode1.Width = Unit.Pixel(100)
            TxtName1.Width = Unit.Parse("90%")
            TxtName1.CssClass = "inputbox"
            TxtName1.TabIndex = 1

            gvJournal.HeaderRow.Cells(0).Controls.Add(TxtCode1)
            gvJournal.HeaderRow.Cells(1).Controls.Add(TxtName1)

            gvJournal.HeaderRow.Cells(0).Style("display") = "none"
            'gvJournal.HeaderRow.Cells(1).Style("display") = "none"

            'gvJournal.HeaderRow.Cells(1).Attributes.Add("onclick", "sortList( '" & gvJournal.Rows.Count() & "','" & txtColom.ClientID & "','" & txtHidden.ClientID & "')")

            TxtName1.Attributes.Add("onkeypress", "postItem('" & TxtCode1.ClientID & "','" & TxtName1.ClientID & "')")
            TxtCode1.Attributes.Add("onkeypress", "postItem('" & TxtCode1.ClientID & "','" & TxtName1.ClientID & "')")

            'TxtName1.Attributes.Add("onkeyup", "return FilterName('" & TxtName1.ClientID & "','Name','" & gvJournal.Rows.Count() & "','" & txtColom.ClientID & "','" & txtHidden.ClientID & "')")
            'TxtCode1.Attributes.Add("onkeyup", "return FilterName('" & TxtCode1.ClientID & "','Code','" & gvJournal.Rows.Count() & "','" & txtColom.ClientID & "','" & txtHidden.ClientID & "')")

            TxtCode1.Attributes.Add("onkeydown", "return selectName('" & gvJournal.Rows.Count() & "','" & TxtName1.ClientID & "','" & TxtCode1.ClientID & "','" & txtHidden.ClientID & "')")
            TxtName1.Attributes.Add("onkeydown", "return selectName('" & gvJournal.Rows.Count() & "','" & TxtName1.ClientID & "','" & TxtCode1.ClientID & "','" & txtHidden.ClientID & "')")




            txtColom.Text = table.Columns.Count.ToString()
            txtRow.Text = table.Rows.Count.ToString()
            Me.Page.Title = labHead.Text
            TxtName1.Focus()
        Catch ex As Exception

            MsgLable.Text = ex.Message
        End Try
    End Sub
    Public x As Integer
    Public y As Integer

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound


        'Dim Name As String

        If Not e.Row.RowIndex = -1 Then

            Dim ht As New HtmlInputHidden()
            ht.ID = ("Cell" & e.Row.RowIndex) + x.ToString()
            ht.Value = e.Row.Cells(0).Text.ToString()
            e.Row.ID = "TR" + x.ToString()
            e.Row.Cells(0).ID = "CODE"
            e.Row.Cells(1).ID = "NAME"
            e.Row.Cells(0).Width = Unit.Parse("25%")

            e.Row.Attributes.Add("onClick", "getValueIn('" & e.Row.Cells(0).Text & "','" & e.Row.Cells(1).Text.ToString().Replace("'", "") & "','" & e.Row.Cells(0).ClientID & "')")
            'e.Row.Attributes.Add("onClick", "getValueIn('" & e.Row.Cells(0).ClientID & "','" & e.Row.Cells(1).ClientID & "','" & e.Row.Cells(0).ClientID & "')")
            e.Row.Attributes.Add("onmouseover", "return  Mouse_Move('" + e.Row.ClientID + "')")
            e.Row.Attributes.Add("onmouseout", "return  Mouse_Out('" + e.Row.ClientID + "')")
            e.Row.ToolTip = "Click for selection..!"
            x = x + 1
            txtHidden.Text = x.ToString()
            e.Row.Cells(0).Style("display") = "none"
        End If
    End Sub



    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        getList()

    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        getList()
    End Sub
End Class
