<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="RPTSubLedgerAnalysis.aspx.vb" Inherits="Reports_ASPX_Report_rptCostCenterAnalysisEmployee" title="Untitled Page" %>
<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
 <script language="javascript" type="text/javascript">     
 function getOtherCostCenter(other) 
    {    
    var sFeatures;     
    sFeatures="dialogWidth: 729px; ";
    sFeatures+="dialogHeight: 634px; "; 
    sFeatures+="help: no; ";
    sFeatures+="resizable: no; ";
    sFeatures+="scroll: yes; ";
    sFeatures+="status: yes; ";
    sFeatures+="unadorned: no; ";
    var urlName = "PickCodes.aspx"
    var OthSession = '<%=Session("CostOTH")%>'
    var FromDate = document.getElementById('<%=txtFromDate.ClientID %>' ).value
    var costCenter = document.getElementById('<%=ddCostcenter.ClientID %>' ).value
    if ((costCenter == OthSession)|| (other=='1'))
    urlName = "OthercostcenterShow.aspx"   
    var NameandCode;
    var result;
    var url= urlName+ '?ccsmode=' + costCenter+'&vid='+'<%=Request.QueryString("vid") %>'; 
    url=url+'&dt='+FromDate ;
     //alert(url);
    result = window.showModalDialog(url,window, sFeatures)   
    if (result=='' || result==undefined)
            {
            return false;
            }  
    }       
    </script>
    <table border="0" cellpadding="0" cellspacing="0" width="70%" align="center">
        <tr>
            <td align="left">
            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
    </table>
    <table align="center"class="BlueTable" cellpadding="5" cellspacing="0"
        style="width: 70%;">
        <tr class="subheader_img">
            <td align="left" colspan="8" style="height: 19px" valign="middle">            
            <asp:Label ID="lblCaption" runat="server" Text="Sub Ledger Report"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" class="matters">From Date</td>
            <td class="matters">:</td>
            <td align="left" class="matters" colspan="2" style="width: 189px; height: 1px; text-align: left">
                <asp:TextBox ID="txtFromDate" runat="server" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
            <td align="left" class="matters" style="color: #1b80b6; height: 1px">
                To Date</td>
            <td align="left" class="matters">
                :</td>
            <td align="left" class="matters" colspan="2" style="height: 1px; text-align: left">
                <asp:TextBox ID="txtToDate" runat="server" Width="122px"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr style="color: #1b80b6" id = "trBSUName" runat = "server">
            <td align="left" valign = "top" class="matters" >
                Business Unit</td>
            <td class="matters" valign = "top">
                :</td>
            <td align="left" class="matters" colspan="6" valign="top">
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
            </td>
        </tr>
        <tr id="tr_CostCenter" runat="server" >
            <td align="left" class="matters" >
                Cost Center</td>
            <td class="matters">
                :</td>
            <td align="left" class="matters" colspan="6" >
                <asp:DropDownList ID="ddCostcenter" runat="server" Width="151px" AutoPostBack="True" OnSelectedIndexChanged="ddCostcenter_SelectedIndexChanged" DataSourceID="odsCostCenter" DataTextField="CCS_DESCR" DataValueField="CCS_ID">
                </asp:DropDownList>
                <asp:ImageButton ID="imgPickcost" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getOtherCostCenter('0');" /></td>
        </tr>
        <tr runat="server">
            <td align="left" class="matters">
                Sub Ledger</td>
            <td class="matters">
                :</td>
            <td align="left" class="matters" colspan="6">
            <asp:ImageButton ID="imgPickOther" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getOtherCostCenter('1')" /></td>
        </tr>
        <tr id = "trChkBoxes" runat = "server" style="display: none; color: #1b80b6">
            <td align="left" class="matters" colspan="8" style="height: 8px">
                <asp:CheckBox ID="chkGroupCurrency" runat="server" Text="View in Group Currency" /><br />
                <asp:CheckBox ID="chkConsolidation" runat="server" Text="Consolidation" />
                </td>
        </tr>       
        <tr>
            <td align="left" class="matters" colspan="8" style="text-align: right">
                &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp;&nbsp;
            </td>
        </tr>
    </table>
    <asp:ObjectDataSource id="odsCostCenter" runat="server" SelectMethod="GetCostCenter"
        TypeName="AccountFunctions"></asp:ObjectDataSource>
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender> 
</asp:Content>
