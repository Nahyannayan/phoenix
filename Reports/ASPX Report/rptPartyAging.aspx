<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPartyAging.aspx.vb" Inherits="rptPartyAging" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx", "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            switch (txtControl) {
                case 0:
                    document.getElementById('<%=txtfromDate.ClientID %>').value = result;
                break;
        }
    }

    function GetBSUName() {
        var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 445px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var type;
        type = document.getElementById('<%=h_Mode.ClientID %>').value;
        if (type == "party" || type == "general") {
            result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx", "", sFeatures)
        }
        else {
            result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx?multiSelect=false", "", sFeatures)
        }
        if (result != "" && result != "undefined") {
            document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
            document.forms[0].submit();
        }
    }

    function GetAccounts() {
        var sFeatures;
        sFeatures = "dialogWidth: 820px; ";
        sFeatures += "dialogHeight: 450px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var ActType;
        var mode = document.getElementById('<%=h_Mode.ClientID %>').value;
        if (mode == 'bank') {
            //result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?bankcash=b&multiSelect=true&forrpt=1", "", sFeatures)
            var oWnd = radopen("../../Accounts/accjvshowaccount.aspx?bankcash=b&multiSelect=true&forrpt=1", "pop_up2");
        }
        else if (mode == 'cash') {
            //result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?bankcash=c&multiSelect=true&forrpt=1", "", sFeatures)
            var oWnd = radopen("../../Accounts/accjvshowaccount.aspx?bankcash=c&multiSelect=true&forrpt=1", "pop_up2");
        }
        else if (mode == 'party') {
            ActType = document.getElementById('<%=h_ACTTYPE.ClientID %>').value;
            if (ActType == 's') {
                //result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=s&forrpt=1", "", sFeatures)
                var oWnd = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=s&forrpt=1", "pop_up2");
            }
            if (ActType == 'C') {
                //result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=C&forrpt=1", "", sFeatures)
                var oWnd = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=C&forrpt=1", "pop_up2");
            }
        }
        else if (mode == 'general') {
            //result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=g&forrpt=1", "", sFeatures)
            var oWnd = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=g&forrpt=1", "pop_up2");
        }
        else //if(mode == 'general')
        {
            //result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?multiSelect=true&forrpt=1", "", sFeatures)
            var oWnd = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&forrpt=1", "pop_up2");
        }
   <%-- if (result != "") {
        document.getElementById('<%=h_ACTIDs.ClientID %>').value = result;//NameandCode[0];
        document.forms[0].submit();

    }--%>
    }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                //NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = arg.NameandCode;
        document.forms[0].submit();
        //__doPostBack('<%= h_ACTIDs.ClientID%>', 'ValueChanged');
    }
}
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Label"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <div class="checkbox-list-full">
                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">As On Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
                        <td align="left" colspan="2">
                            <asp:CheckBox ID="chkVoucherDate" runat="server" Text="Show Ageing Based On Invoice Date" CssClass="field-label" /></td>
                    </tr>
                    <tr id="Trbank">
                        <td align="left" width="20%">
                            <asp:Label ID="lblBankCash" runat="server" Text="Bank" CssClass="field-label"></asp:Label></td>

                        <td align="left" colspan="3">
                            <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the ACT ID you want to Add to the Search and click on Add)"></asp:Label>
                            <asp:TextBox ID="txtBankNames" runat="server"></asp:TextBox>
                            <asp:LinkButton ID="lblAddActID" runat="server">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick=" GetAccounts(); return false;" /><br />
                            <asp:GridView ID="grdACTDetails" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="ACT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ACT_Name" HeaderText="ACT Name" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Ageing Buckets</span> </td>
                        <td align="left" colspan="3">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:TextBox ID="txtBkt1" runat="server" MaxLength="3">30</asp:TextBox>
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:TextBox ID="txtBkt2" runat="server" MaxLength="3">60</asp:TextBox>
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:TextBox ID="txtBkt3" runat="server" MaxLength="3">90</asp:TextBox>
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:TextBox ID="txtBkt4" runat="server" MaxLength="3">120</asp:TextBox>
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:TextBox ID="txtBkt5" runat="server" MaxLength="3">180</asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr runat="server" id="trReportType">
                        <td align="left" width="20%"></td>
                        <td align="left" colspan="3">
                            <asp:RadioButton ID="radDefault" runat="server" CssClass="field-label" Text="Default" AutoPostBack="True" OnCheckedChanged="radDefault_CheckedChanged" Checked="True"></asp:RadioButton>
                            <asp:RadioButton ID="radByUnit" runat="server" CssClass="field-label" Text="Summary By Unit" AutoPostBack="True" OnCheckedChanged="radByUnit_CheckedChanged"></asp:RadioButton>
                            <asp:RadioButton ID="radByParty" runat="server" CssClass="field-label" Text="Summary by Party" AutoPostBack="True" OnCheckedChanged="radByParty_CheckedChanged"></asp:RadioButton></td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"></td>
                        <td align="left" colspan="3">
                            <asp:CheckBox ID="chkGroupCurrency" runat="server" Text="View in Group Currency" CssClass="field-label" /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:HiddenField ID="h_ACTIDs" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_Mode" runat="server" />
                <asp:HiddenField ID="h_ACTTYPE" runat="server"></asp:HiddenField>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

