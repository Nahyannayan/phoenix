Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class Reports_ASPX_Report_rptPDCIIIReport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkExporttoexcel)
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If
        If h_ACTIDs.Value <> Nothing And h_ACTIDs.Value <> "" And h_ACTIDs.Value <> "undefined" Then
            FillACTIDs(h_ACTIDs.Value)
            'h_BSUID.Value = ""
        End If
        Page.Title = OASISConstants.Gemstitle
        If Not IsPostBack Then
            UtilityObj.NoOpen(Me.Header)
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = ViewState("MainMnu_code")
            Select Case ViewState("MainMnu_code").ToString
                Case "A750045"
                    lblCaption.Text = "PDC Form-III Report"
            End Select
            txtFromDate.Text = UtilityObj.GetDiplayDate()
            txtTodate.Text = UtilityObj.GetDiplayDate()
            chkSummary.Visible = False
        End If
    End Sub
     
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim strfDate As String = txtFromDate.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtFromDate.Text = strfDate
        End If
        Select Case ViewState("MainMnu_code").ToString
            Case "A750045"
                If ChkDetails.Checked Then
                    PDCFormatIIIdetails()
                Else
                    Dim strXMLBSUNames As String
                    'Generate the XML in the form <BSU_DETAILS><BSU_ID>IDhere</BSU_ID></BSU_DETAILS>
                    strXMLBSUNames = UsrBSUnits1.GetSelectedNode()
                    rptPDCFormatIII(UtilityObj.GenerateXML(strXMLBSUNames, XMLType.BSUName))
                End If
        End Select
    End Sub

    Private Sub rptPDCFormatIII(ByVal strXMLBSUNames As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim isExcel As Boolean = False
        If ViewState("isExport") = True Then
            isExcel = True
        End If
        Dim cmd As New SqlCommand
        cmd = New SqlCommand("rptPDCFormatIII", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.Xml)
        sqlpBSU_ID.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSUB_ID As New SqlParameter("@SUB_ID", SqlDbType.VarChar, 20)
        sqlpSUB_ID.Value = Session("SUB_ID")
        cmd.Parameters.Add(sqlpSUB_ID)

        Dim sqlpFROMDOCDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DT", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtTodate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpbGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpbGroupCurrency.Value = chkConsolidation.Checked
        cmd.Parameters.Add(sqlpbGroupCurrency)

        Dim sqlpExcel As New SqlParameter("@Excel", SqlDbType.Bit)
        sqlpExcel.Value = isExcel
        cmd.Parameters.Add(sqlpExcel)

        Dim sqlpPARTY As New SqlParameter("@PARTY", SqlDbType.VarChar )
        sqlpPARTY.Value = h_ACTIDs.Value.Replace("||", "|")
        cmd.Parameters.Add(sqlpPARTY)

        objConn.Open()
        cmd.Connection = objConn

        Dim repSourceSubRep(1) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        Dim cmdSubColln As New SqlCommand

        cmdSubColln.CommandText = "[RPTLEDGER_CHQISSUED]"
        cmdSubColln.Connection = objConn
        cmdSubColln.CommandType = CommandType.StoredProcedure
        cmdSubColln.Parameters.AddWithValue("@BSU_IDs", strXMLBSUNames) 
        cmdSubColln.Parameters.AddWithValue("@TODOCDT", txtFromDate.Text)
        cmdSubColln.Parameters.AddWithValue("@bGroupCurrency", chkConsolidation.Checked)
        repSourceSubRep(0).Command = cmdSubColln

        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
       
        params("header") = "PDC Form-III Report"
        params("SubHead") = "Date Period FROM " & txtFromDate.Text & "  TO " & txtTodate.Text

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.SubReport = repSourceSubRep
        If chkSummary.Checked Then
            repSource.ResourceName = "../RPT_Files/rptPDCFormatIIISummary.rpt"
        Else
            repSource.ResourceName = "../RPT_Files/rptPDCFormatIII.rpt"
        End If
        Session("ReportSource") = repSource
        objConn.Close()
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("rptviewer.aspx?isExport=true", True)
            Exit Sub
        Else
            'Response.Redirect("rptviewer.aspx", True)
            ReportLoadSelection()
        End If
        objConn.Close()
    End Sub

    Private Sub PDCFormatIIIdetails()
        Dim str_conn As String = ConnectionManger.GetOASISFINConnectionString
        Dim objConn As New SqlConnection(str_conn)

        Dim cmd As New SqlCommand
        cmd = New SqlCommand("rptPDCFormatIII_Excel", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.Xml)
        sqlpBSU_ID.Value = UtilityObj.GenerateXML(UsrBSUnits1.GetSelectedNode(), XMLType.BSUName)
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSUB_ID As New SqlParameter("@SUB_ID", SqlDbType.VarChar, 20)
        sqlpSUB_ID.Value = Session("SUB_ID")
        cmd.Parameters.Add(sqlpSUB_ID)

        Dim sqlpFROMDOCDT As New SqlParameter("@FRMDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text 'Not using
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DT", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtTodate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpbGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpbGroupCurrency.Value = True
        cmd.Parameters.Add(sqlpbGroupCurrency)

        Dim sqlMode As New SqlParameter("@MODE", SqlDbType.Bit)
        sqlMode.Value = True
        cmd.Parameters.Add(sqlMode)

        Dim sqlpPARTY As New SqlParameter("@PARTY", SqlDbType.VarChar)
        sqlpPARTY.Value = h_ACTIDs.Value.Replace("||", "|")
        cmd.Parameters.Add(sqlpPARTY)

        objConn.Open()
        cmd.Connection = New SqlConnection(str_conn)

        Dim repSourceSubRep(1) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        Dim cmdSubColln As New SqlCommand

        cmdSubColln.CommandText = "[RPTLEDGER_CHQISSUED]"
        cmdSubColln.Connection = New SqlConnection(str_conn)
        cmdSubColln.CommandType = CommandType.StoredProcedure
        cmdSubColln.Parameters.AddWithValue("@BSU_IDs", UtilityObj.GenerateXML(UsrBSUnits1.GetSelectedNode(), XMLType.BSUName))
        cmdSubColln.Parameters.AddWithValue("@TODOCDT", txtTodate.Text) 
        cmdSubColln.Parameters.AddWithValue("@bGroupCurrency", chkConsolidation.Checked)

        repSourceSubRep(0).Command = cmdSubColln

        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtTodate.Text
        params("header") = "PDC III FORMAT DETAILS "
        params("SubHead") = "As on " & txtTodate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.SubReport = repSourceSubRep
        repSource.ResourceName = "../RPT_Files/rptPDCFormatIIINextl.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("rptviewer.aspx?isExport=true", True)
        Else
            'Response.Redirect("rptviewer.aspx", True)
            ReportLoadSelection()
        End If
    End Sub

    Protected Sub chkConsolidation_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkConsolidation.Checked Then
            chkSummary.Visible = True
        Else
            chkSummary.Visible = False
        End If
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExporttoexcel.Click
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Try
            Dim IDs As String() = ACTIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdACTDetails.DataSource = ds
            grdACTDetails.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(h_ACTIDs.Value)
        End If 
    End Sub  

    Protected Sub lblAddActID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddActID.Click
        h_ACTIDs.Value += "||" + txtBankNames.Text.Replace(",", "||")
        h_ACTIDs.Value = txtBankNames.Text
        FillACTIDs(h_ACTIDs.Value)
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
