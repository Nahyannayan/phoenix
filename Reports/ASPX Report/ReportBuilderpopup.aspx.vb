﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports Microsoft.VisualBasic
Partial Class Reports_ASPX_Report_ReportBuilderpopup
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                gvGroup.Attributes.Add("bordercolor", "#1b80b6")
                ViewState("QryID") = Request.QueryString("QRY")
                'Session("QRY") = Request.QueryString("QRY")
                bindData(ViewState("QryID"))
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
       
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        bindData(ViewState("QryID"))
    End Sub

    Protected Sub gvGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGroup.RowDataBound
        Dim ChkBox As New HtmlInputCheckBox
        If e.Row.RowType = DataControlRowType.DataRow Then
            ChkBox = e.Row.FindControl("ChkBox")
            e.Row.Cells(1).Attributes.Add("onclick", "return getValue ('" & ChkBox.Value & "','" & e.Row.Cells(2).Text.Replace("'", " ") & "')")
            e.Row.Cells(2).Attributes.Add("onclick", "return getValue ('" & ChkBox.Value & "','" & e.Row.Cells(2).Text.Replace("'", " ") & "')")
            e.Row.Attributes.Add("onmouseover", "return MouseOver('" & e.Row.ClientID & "')")
            e.Row.Attributes.Add("onmouseleave", "return MouseLeave('" & e.Row.ClientID & "')")
        End If
    End Sub
    Sub bindData(ByVal Qry As String)
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@QuryId", Qry)
        Dim _table As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.StoredProcedure, "RB_expopQUERY", pParms)
        gvGroup.DataSource = _table
        gvGroup.DataBind()
    End Sub
End Class
