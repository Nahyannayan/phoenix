<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptAging.aspx.vb" Inherits="rptAging"  %>
<%@ OutputCache Duration="1" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">
           function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 229px; ";
            sFeatures+="dialogHeight: 234px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx","", sFeatures);
            if (result=='' || result==undefined)
            {
                return false;
            }
            switch(txtControl)
            {
              case 0:
                document.getElementById('<%=txtfromDate.ClientID %>').value=result;
                break;
              case 1:  
                document.getElementById('<%=txtToDate.ClientID %>').value=result;
                break;
            }    
           }
   
         function GetBSUName()
          {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 450px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx","", sFeatures)
            if(result != "")
            {
                //NameandCode = result.split('____');
                //document.getElementById('<%=txtBSUName.ClientID %>').value = result;//NameandCode[1];
                document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
                //document.getElementById('<%=h_BSUID.ClientID %>').value='';
            }
            
         }
    </script>
        
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0"
        style="width: 70%; height: 174px">
        <tr class="subheader_img" >
            <td align="left" colspan="8" style="height: 19px" valign="middle">
                        <asp:Label ID="lblCaption" runat="server" Text="Trail Balance Report"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 468px; height: 1px">
                From Date</td>
            <td class="matters" style="width: 150px; height: 1px">
                :</td>
            <td align="left" class="matters" colspan="2" style="width: 338px; height: 1px; text-align: left">
                <asp:TextBox ID="txtFromDate" runat="server" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
            <td align="left" class="matters" style="width: 98px; color: #1b80b6; height: 1px">
                To Date</td>
            <td align="left" class="matters" style="height: 1px">
                :</td>
            <td align="left" class="matters" colspan="2" style="width: 200px; height: 1px; text-align: left">
                <asp:TextBox ID="txtToDate" runat="server" Width="122px"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                &nbsp;&nbsp;
            </td>
        </tr>
       <tr>
            <td align="left" class="matters" style=" height: 5px; width: 557px;">
                Business Unit</td>
            <td class="matters" style=" height: 5px;">
                :</td>
            <td align="right" class="matters" colspan="6" style="height: 5px; text-align: left">
                <asp:TextBox ID="txtBSUName" runat="server" Height="23px" Width="359px" TextMode="MultiLine"></asp:TextBox>
                <asp:ImageButton ID="imgGetBSUName" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetBSUName()" />&nbsp;</td>
        </tr>
        <tr style="color: #1b80b6" runat = "server" id = "trGroupType">
            <td align="left" class="matters" style="width: 468px; height: 8px">
                Group Type</td>
            <td class="matters" style="width: 150px; height: 8px">
                :</td>
            <td align="left" class="matters" colspan="6" style="height: 8px">
                <asp:DropDownList ID="cmbGroupType" runat="server" Width="151px">
                </asp:DropDownList>&nbsp;
            </td>
        </tr>
        <tr style="color: #1b80b6" id = "trChkBoxes" runat = "server">
            <td align="left" class="matters" colspan="8" style="height: 8px">
                <asp:CheckBox ID="chkGroupCurrency" runat="server" Text="View in Group Currency" /><br />
                <asp:CheckBox ID="chkConsolidation" runat="server" Text="Consolidation" /></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="height: 22px; text-align: right">
                <asp:LinkButton id="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table>
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:HiddenField ID="h_BSUID" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
</asp:Content>

