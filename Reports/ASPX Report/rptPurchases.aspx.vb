Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data

Partial Class Reports_ASPX_Report_rptPurchases
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IIf(Request.QueryString("nodata") Is Nothing, False, True) Then
            lblError.Text = "No Records with specified condition"
        End If
        If Not IsPostBack Then
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            txtAccountID.Attributes.Add("ReadOnly", "ReadOnly")
            txtBankID.Attributes.Add("ReadOnly", "ReadOnly")
            txtAccountName.Attributes.Add("ReadOnly", "ReadOnly")
            txtBankName.Attributes.Add("ReadOnly", "ReadOnly")
            txtToDate.Attributes.Add("onBlur", "checkdate(this)")
            txtfromDate.Attributes.Add("onBlur", "checkdate(this)")
            'h_BSUID.Value = IIf(Session("SBSUID") <> "", Session("SBSUID").ToString(), String.Empty)
            ddlBSUnit.DataSource = UtilityObj.GetBusinessUnits(Session("sUsr_name"), True)
            ddlBSUnit.DataTextField = "BSU_NAME"
            ddlBSUnit.DataValueField = "BSU_ID"
            ddlBSUnit.DataBind()
            ddlBSUnit.SelectedValue = Session("sbsuid")
            txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            txtfromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim fromDate, toDate As String
        If (txtfromDate.Text <> "") And (txtfromDate.Text <> String.Empty) Then
            fromDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", txtfromDate.Text)
        Else
            fromDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", DateTime.MinValue)
        End If
        If (txtToDate.Text <> "") And (txtToDate.Text <> String.Empty) Then
            toDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", txtToDate.Text)
        Else
            toDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", DateTime.Now)
        End If
        If Not Page.IsValid Then
            Exit Sub
        End If

        Dim strFilter As String = String.Empty
        If ((txtfromDate.Text <> "") And (txtfromDate.Text <> String.Empty)) Or _
        ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then
            InitializeFilter(strFilter)
            strFilter += " PUH_DOCDT BETWEEN '" + fromDate + "' AND '" + toDate + "'"
        End If
        'If (txtBSUID.Text <> "") And (txtBSUID.Text <> String.Empty) Then
        If (ddlBSUnit.SelectedValue <> "") And (ddlBSUnit.SelectedValue <> String.Empty) Then
            InitializeFilter(strFilter)
            strFilter += " BSU_ID IN( "
            'Dim strArrayBSUID As String() = h_BSUID.Value.Split("||")
            Dim strArrayBSUID As String() = ddlBSUnit.SelectedValue.Split("||")
            Dim strBSUID As String = String.Empty
            For count As Integer = 0 To strArrayBSUID.Length - 1
                If count <> 0 And Not strBSUID.EndsWith(", ") Then
                    strBSUID += ", "
                End If
                strBSUID += "'" + strArrayBSUID(count) + "'"
            Next
            strFilter += strBSUID + ")"
        End If
        If txtBankName.Text <> "" And (txtBankName.Text <> String.Empty) Then
            InitializeFilter(strFilter)
            strFilter += " PUH_PARTY_ACT_ID = '" + txtBankID.Text + "'"
        End If
        If txtAccountName.Text <> "" And (txtAccountName.Text <> String.Empty) Then
            InitializeFilter(strFilter)
            strFilter += " PUD_DEBIT_ACT_ID = '" + txtAccountID.Text + "'"
        End If

        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT * FROM vw_OSA_PURCHASE" + strFilter
        cmd.CommandType = Data.CommandType.Text

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("ReportHead") = "Purchase Report Period of : " & txtfromDate.Text.ToString() & " . To. " & txtToDate.Text.ToString()
        params("VoucherName") = "Purchase Report"
        'params("FromDate") = fromDate
        'params("ToDate") = toDate
        params("decimal") = Session("BSU_ROUNDOFF")
        repSource.Parameter = params
        repSource.VoucherName = "Purchase Report"
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/PurchaseSummaryReportNext.rpt"
        Session("ReportSource") = repSource
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("rptviewer.aspx?isExport=true", True)
        Else
            ' Response.Redirect("rptviewer.aspx", True)
            ReportLoadSelection()
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub InitializeFilter(ByRef filter As String)
        If filter IsNot String.Empty Or filter IsNot "" Then
            If Not filter.StartsWith(" WHERE") Then
                filter = filter.Insert(0, " WHERE")
            End If
            If Not filter.EndsWith("WHERE") Then
                filter = filter.Insert(filter.Length, " AND")
            End If
        End If
    End Sub

    Public Sub compareDate(ByVal sender As Object, ByVal value As ServerValidateEventArgs)
        Dim dtfrom, dtto As DateTime
        If ((txtfromDate.Text <> "") And (txtfromDate.Text <> String.Empty)) Or _
        ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then
            dtfrom = Convert.ToDateTime(txtfromDate.Text)
            dtto = Convert.ToDateTime(txtToDate.Text)
            If dtfrom < dtto Then
                value.IsValid = True
            Else
                value.IsValid = False
            End If
        End If
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnSubmit_Click(sender, e)
    End Sub

End Class
