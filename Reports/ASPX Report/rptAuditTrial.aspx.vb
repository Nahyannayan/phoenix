Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class Reports_ASPX_Report_rptAuditTrial
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim accountID, bankID As String
    Dim fromDate, toDate As String
    Dim MainMnu_code As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            txtAction.Attributes.Add("readonly", "readonly")
            'txtDocNo.Attributes.Add("readonly", "readonly")
            txtForm.Attributes.Add("readonly", "readonly")
            txtLoginUser.Attributes.Add("readonly", "readonly")
            txtModule.Attributes.Add("readonly", "readonly")
            txtTerminal.Attributes.Add("readonly", "readonly")
            txtWindow.Attributes.Add("readonly", "readonly")
            txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            ' txtToDate.Attributes.Add("onBlur", "checkdate(this)")
            ' txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
            'txtAction.MaxLength = 30
            'txtDocNo.MaxLength = 30
            'txtForm.MaxLength = 30
            'txtLoginUser.MaxLength = 30
            'txtModule.MaxLength = 30
            'txtTerminal.MaxLength = 30
            'txtWindow.MaxLength = 30
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Page.IsValid Then
            If (txtFromDate.Text <> "") And (txtFromDate.Text <> String.Empty) Then
                fromDate = Format(OASISConstants.DataBaseDateFormat, CType(txtFromDate.Text, Date))

            Else
                fromDate = Format(OASISConstants.DataBaseDateFormat, DateTime.MinValue)
            End If
            If (txtToDate.Text <> "") And (txtToDate.Text <> String.Empty) Then
                Dim Tdate As Date = CType(txtToDate.Text, Date)
                Tdate = Tdate.AddDays(1)
                toDate = CType(Tdate, String)


                toDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", toDate)


            Else
                toDate = String.Format("{0:" & OASISConstants.DataBaseDateFormat & "}", DateTime.Now)
            End If
            If Not Page.IsValid Then
                Exit Sub
            End If
            Dim strFilter As String = String.Empty

            Dim strLogNull As String = String.Empty
            Dim strModNull As String = String.Empty
            Dim strArrayTxt() As String
            Dim strAddTxt As String = String.Empty

            Dim usrB_ID As String = Session("sBsuid")
            strFilter = " WHERE AUD_USER<>''  "
            'filter condition on date
            If ((txtFromDate.Text <> "") And (txtFromDate.Text <> String.Empty)) Or _
            ((txtToDate.Text <> "") And (txtToDate.Text <> String.Empty)) Then
                InitializeFilter(strFilter)
                strFilter += " AUD_LOGDT BETWEEN '" + fromDate + "' AND '" + toDate + "'"
            End If

            'filter condition on loginUser
            If (txtLoginUser.Text <> "") And (txtLoginUser.Text <> String.Empty) Then
                strArrayTxt = txtLoginUser.Text.Split("|")
                If (strArrayTxt.Length - 1) = 0 And LCase(strArrayTxt(0)) = "anonymous" Then



                    strFilter += " or aud_user is null"
                Else
                    InitializeFilter(strFilter)
                    strFilter += " AUD_USER IN( "


                    For count As Integer = 0 To strArrayTxt.Length - 1
                        If count <> 0 Then
                            If LCase(strArrayTxt(count)) <> "anonymous" Then
                                strAddTxt += ","
                            End If

                        End If

                        If LCase(strArrayTxt(count)) = "anonymous" Then
                            strModNull = " or aud_user is null"

                        Else 'If LCase(strArrayTxt(count)) <> "anonymous" Then
                            strAddTxt += "'" + strArrayTxt(count) + "'"

                        End If
                    Next

                    If Right(strAddTxt, 1) = "," Then
                        strAddTxt = Left(strAddTxt, Len(strAddTxt) - 1)
                    End If

                    If Left(strAddTxt, 1) = "," Then
                        strAddTxt = Mid(strAddTxt, 2)
                    End If

                    strFilter += strAddTxt + ")" + strModNull
                End If
            End If


            'filter condition on DocNo

            If (txtDocNo.Text <> "") And (txtDocNo.Text <> String.Empty) Then
                InitializeFilter(strFilter)
                strFilter += " AUD_DOCNO IN( "
                strArrayTxt = txtDocNo.Text.Split("|")
                strAddTxt = String.Empty
                For count As Integer = 0 To strArrayTxt.Length - 1
                    If count <> 0 Then
                        strAddTxt += ", "
                    End If
                    strAddTxt += "'" + strArrayTxt(count) + "'"

                Next
                strFilter += strAddTxt + ")"
            End If


            'filter condition on Action
            If (txtAction.Text <> "") And (txtAction.Text <> String.Empty) Then
                InitializeFilter(strFilter)
                strFilter += " AUD_ACTION IN( "
                strArrayTxt = txtAction.Text.Split("|")
                strAddTxt = String.Empty
                For count As Integer = 0 To strArrayTxt.Length - 1
                    If count <> 0 Then
                        strAddTxt += ", "
                    End If
                    strAddTxt += "'" + strArrayTxt(count) + "'"
                Next
                strFilter += strAddTxt + ")"
            End If

            'filter condtion on Form
            If (txtForm.Text <> "") And (txtForm.Text <> String.Empty) Then
                InitializeFilter(strFilter)
                strFilter += " AUD_Form IN( "
                strArrayTxt = txtForm.Text.Split("|")
                strAddTxt = String.Empty
                For count As Integer = 0 To strArrayTxt.Length - 1
                    If count <> 0 Then
                        strAddTxt += ", "
                    End If
                    strAddTxt += "'" + strArrayTxt(count) + "'"
                Next
                strFilter += strAddTxt + ")"
            End If

            'filter condition on Terminal

            If (txtTerminal.Text <> "") And (txtTerminal.Text <> String.Empty) Then
                InitializeFilter(strFilter)
                strFilter += " AUD_HOST IN( "
                strArrayTxt = txtTerminal.Text.Split("|")
                strAddTxt = String.Empty
                For count As Integer = 0 To strArrayTxt.Length - 1
                    If count <> 0 Then
                        strAddTxt += ", "
                    End If
                    strAddTxt += "'" + strArrayTxt(count) + "'"
                Next
                strFilter += strAddTxt + ")"
            End If

            'filter condition on Window User

            If (txtWindow.Text <> "") And (txtWindow.Text <> String.Empty) Then
                InitializeFilter(strFilter)
                strFilter += " AUD_WinUser IN( "
                strArrayTxt = txtWindow.Text.Split("|")
                strAddTxt = String.Empty
                For count As Integer = 0 To strArrayTxt.Length - 1
                    If count <> 0 Then
                        strAddTxt += ", "
                    End If
                    strAddTxt += "'" + strArrayTxt(count) + "'"
                Next
                strFilter += strAddTxt + ")"

            End If

            'filter condition on modules
            If (txtModule.Text <> "") And (txtModule.Text <> String.Empty) Then


                strArrayTxt = txtModule.Text.Split("|")

                If (strArrayTxt.Length - 1) = 0 And LCase(strArrayTxt(0)) = "general" Then



                    strFilter += " or AUD_Module is null"
                Else
                    InitializeFilter(strFilter)
                    strFilter = strFilter + " AUD_Module IN( "
                    strAddTxt = String.Empty

                    For count As Integer = 0 To strArrayTxt.Length - 1

                        If count <> 0 Then
                            If LCase(strArrayTxt(count)) <> "general" Then
                                strAddTxt += ","

                            End If

                        End If

                        Select Case LCase(strArrayTxt(count))

                            Case "accounts"
                                strAddTxt += "'" + "A0" + "'"
                            Case "payroll"
                                strAddTxt += "'" + "P0" + "'"

                            Case "master"
                                strAddTxt += "'" + "D0" + "'"

                            Case "inventory"
                                strAddTxt += "'" + "I0" + "'"

                            Case "fees"
                                strAddTxt += "'" + "F0" + "'"

                        End Select

                        If LCase(strArrayTxt(count)) = "general" Then
                            strModNull = "  or AUD_Module is null"
                        End If
                    Next

                    If Right(strAddTxt, 1) = "," Then
                        strAddTxt = Left(strAddTxt, Len(strAddTxt) - 1)
                    End If

                    If Left(strAddTxt, 1) = "," Then
                        strAddTxt = Mid(strAddTxt, 2)
                    End If

                    strFilter += strAddTxt + ")" + strModNull

                End If


            End If

            Dim cmd As New SqlCommand
            cmd.CommandText = "SELECT  top 200   AUD_LOGDT, AUD_USER, CASE  WHEN isnull(AUD_MODULE, '') = '' THEN 'General' WHEN isnull(AUD_MODULE, '')='A0' THEN 'Accounts'" & _
     " WHEN isnull(AUD_MODULE, '') = 'P0' THEN 'Payroll'  WHEN isnull(AUD_MODULE, '') = 'D0' THEN 'Master'  WHEN isnull(AUD_MODULE, '') = 'I0' THEN 'Inventory'" & _
     " WHEN isnull(AUD_MODULE, '') = 'F0' THEN 'Fees' END AS Module , AUD_FORM, AUD_DOCNO, AUD_ACTION,AUD_REMARKS, AUD_HOST, AUD_WINUSER FROM AUDITTRAIL" + strFilter + " AND AUD_BSU_ID='" & usrB_ID & "' order by AUD_LOGDT desc,Module,AUD_FORM,AUD_USER,AUD_ACTION"
            cmd.CommandType = Data.CommandType.Text
            Dim tblbsuid As String = Session("sBsuid")
            Dim bsuName As String = String.Empty
            Using bsuName_reader As SqlDataReader = AccessRoleUser.GetBUnitImage(tblbsuid)
                While bsuName_reader.Read

                    bsuName = Convert.ToString(bsuName_reader("BSU_NAME"))

                End While

            End Using



            ' check whether Data Exits
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As New DataSet
            cmd.Connection = New SqlConnection(str_conn)

            SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
            If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
                lblError.Text = "No Records with specified condition"
            Else
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("userName") = Session("sUsr_name")
                'params("Summary") = chkSummary.Checked
                params("Bus_unit") = bsuName
                params("FromDate") = txtFromDate.Text
                params("ToDate") = txtToDate.Text
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.ResourceName = "../RPT_Files/rptAuditTrialReport.rpt"
                'Context.Items.Add("ReportSource", repSource)
                Session("ReportSource") = repSource
                response.redirect("rptviewer.aspx", True)
            End If
        End If

    End Sub


    Private Sub InitializeFilter(ByRef filter As String)


        If filter IsNot String.Empty Or filter IsNot "" Then

            'using the filter option to add WHERE clause before adding condition
            If Not filter.StartsWith(" WHERE") Then

                filter = filter.Insert(0, " WHERE")
            End If
            'using the filter option to add AND clause after adding each condition
            If Not filter.EndsWith("WHERE") Then
                filter = filter.Insert(filter.Length, " AND")
            End If
        End If
    End Sub

    
    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtAction.Text = ""
        txtDocNo.Text = ""
        txtForm.Text = ""
        txtLoginUser.Text = ""
        txtModule.Text = ""
        txtTerminal.Text = ""
        txtWindow.Text = ""
       
    End Sub

    
    Protected Sub cvDateFrom_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvDateFrom.ServerValidate
        Dim sflag As Boolean = False
        Dim DateTime1 As Date
        Try
            'convert the date into the required format so that it can be validate by Isdate function
            DateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date
            If IsDate(DateTime1) Then
                sflag = True
            End If

            If sflag Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        Catch
            'catch when format string through an error
            args.IsValid = False
        End Try
    End Sub

    Protected Sub cvTodate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvTodate.ServerValidate
        Dim sflag As Boolean = False
        Dim dateTime1 As Date
        Dim DateTime2 As Date
        Try
            dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            DateTime2 = Date.ParseExact(txtToDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

            'check for the leap year date

            If IsDate(DateTime2) Then

                If DateTime2 < dateTime1 Then
                    sflag = False
                Else
                    sflag = True
                End If
            End If
            If sflag Then
                args.IsValid = True
            Else
                'CustomValidator1.Text = ""
                args.IsValid = False
            End If
        Catch
            args.IsValid = False
        End Try
    End Sub

    
End Class
