<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptDayEndMonthEndDetails.aspx.vb" Inherits="Reports_ASPX_Report_rptAccountMasterList" title="Untitled Page" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
     <table align="center" class="BlueTable" cellpadding="5" cellspacing="0">
        <tr class="subheader_img">
            <td align="left" colspan="8" valign="middle" style="height: 19px">
                        <asp:Label ID="lblrptCaption" runat="server" Text="Day End Month End Details..."></asp:Label> 
            </td>
        </tr>
        <tr id ="trGroup" runat = "server">
            <td align="left" class="matters"   valign="middle">
                Business Unit</td>
            <td class="matters" valign="middle">
                :</td>
            <td align="right" class="matters" colspan="6" style="text-align: left" valign="middle" width="300">
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="text-align: right; height: 33px;">
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table>
    &nbsp;
    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
</asp:Content>

