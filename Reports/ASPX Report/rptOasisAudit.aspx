<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptOasisAudit.aspx.vb" Inherits="Reports_ASPX_Report_rptOasisAudit"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
   
   <script language="javascript" type="text/javascript">
   
   function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 229px; ";
            sFeatures+="dialogHeight: 234px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
            var commVar;
            result = window.showModalDialog("../../Accounts/calendar.aspx?dt="+ document.getElementById('<%=txtfromDate.ClientID %>').value,"", sFeatures);
            if (result=='' || result==undefined)
            {
                return false;
            }
            switch(txtControl)
            {
              case 0:
                document.getElementById('<%=txtfromDate.ClientID %>').value=result;
                
                break;
              case 1:  
                document.getElementById('<%=txtToDate.ClientID %>').value=result;
                
                break;
            }    
           }
           </script>
           
    <table width="70%">
        <tr>
            <td align="left" style="width: 571px">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><br />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="bankPayment" CssClass="error" ForeColor="" />
            </td>
        </tr>
        <tr>
            <td align="center" style="height: 185px" valign="top">
                <table id="TABLE1" runat="server" align="center" border="1" bordercolor="#1b80b6"
                    cellpadding="5" cellspacing="0" style="width: 100%">
                    <tr class="subheader_img">
                        <td colspan="6" style="height: 3px">
                        OASIS Audit Trail 
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" style="width: 106px; height: 16px; text-align: left">
                            From Date</td>
                        <td align="center" class="matters" style="width: 8px; height: 16px">
                            :</td>
                        <td class="matters" style="width: 180px; height: 16px; text-align: left">
                            <asp:TextBox ID="txtFromDate" runat="server" Width="50%"></asp:TextBox>
                            <img onclick="getDate(550, 310, 0)" src="../../Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="valFromDate" runat="server" ControlToValidate="txtfromDate"
                                ErrorMessage="From Date Required" ValidationGroup="bankPayment" CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revFromDate" runat="server" ControlToValidate="txtFromDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$" CssClass="error" ForeColor="">*</asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="cvDateFrom" runat="server" Display="Dynamic" EnableViewState="False"
                                ErrorMessage="From Date entered is not a valid date" ValidationGroup="Group1" CssClass="error" ForeColor="">*</asp:CustomValidator></td>
                        <td class="matters" style="width: 65px; color: #1b80b6; height: 16px; text-align: left">
                            To Date</td>
                        <td class="matters" style="width: 1px; height: 16px">
                            :</td>
                        <td class="matters" style="width: 163px; height: 16px; text-align: left">
                            <asp:TextBox ID="txtToDate" runat="server" Width="60%"></asp:TextBox>
                            <img onclick="getDate(820, 310, 1)" src="../../Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="valToDate" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date Required" ValidationGroup="bankPayment" CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revTodate" runat="server" ControlToValidate="txtToDate"
                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$" CssClass="error" ForeColor="">*</asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="cvTodate" runat="server" Display="Dynamic" EnableViewState="False"
                                ErrorMessage="To Date entered is not a valid date and must be greater than From date"
                                ValidationGroup="Group1" CssClass="error" ForeColor="">*</asp:CustomValidator></td>
                    </tr>
                    <!-- Grade & Section -->
                    <!-- Name -->
                    <!-- Student Id -->
                    <tr id="Tr1" runat="server" style="color: #1b80b6">
                        <td class="matters" style="width: 106px; height: 13px; text-align: left">
                            Document Type.</td>
                        <td align="center" class="matters" style="width: 8px; height: 13px">
                            :</td>
                        <td align="left" class="matters" colspan="4" valign="top">
                           
                            <asp:Panel ID="plDocNo" runat="server" Height="30%" Width="60%" ScrollBars="Vertical" BorderColor="#1B80B6" BorderStyle="Solid" BorderWidth="1px">
                           
                            <asp:CheckBoxList ID="chkDocNo" runat="server">
                            </asp:CheckBoxList> </asp:Panel>
                            
                            </td>
                    </tr>
                    
                   
                    <tr>
                        <td class="matters" colspan="6" align="center">
                            &nbsp; &nbsp;<asp:Button ID="btnSubmit" runat="server" CssClass="button"
                                Text="Generate View" ValidationGroup="bankPayment" Width="100px" />
                            <asp:Button ID="btnClear" runat="server" CssClass="button" Text="Clear" UseSubmitBehavior="False"
                                Width="72px" />
                            <asp:Button ID="Button1" runat="server" CssClass="button" Text="Cancel" UseSubmitBehavior="False"
                                Width="72px" /></td>
                    </tr>
                    <!-- ===================================== APPLICANT 6 ======================================= -->
                </table>
                
            </td>
        </tr>
        <tr>
            <td align="center" style="height: 104px" valign="top" class="gridheader_text">
                <table id="TABLE2" runat="server" align="center" border="1" bordercolor="#1b80b6"
                    cellpadding="5" cellspacing="0" style="width: 100%">
                    <tr class="subheader_img">
                        <td colspan="6" style="height: 3px">
                        Selected Document 
                        </td>
                    </tr>
                    <tr>
                    <td valign="top" style="height: 197px">
                
                
                <asp:GridView ID="gvDocType" runat="server" AutoGenerateColumns="False" Width="60%" CssClass="gridstyle" AllowPaging="True" PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="Doc ID" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("DocNo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemStyle Wrap="False" />
                            <ItemTemplate>
                                <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("DOC_ID") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="gridheader_text" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="DOC_NAME" HeaderText="Doc Name" />
                        <asp:BoundField DataField="Modified" HeaderText="Modified" />
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemStyle Wrap="False" />
                            <ItemTemplate>
                                &nbsp;&nbsp;
                                <asp:LinkButton ID="lbView" runat="server" OnClick="lbView_Click" >View</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="Wheat" />
                    <HeaderStyle CssClass="gridheader_new" Height="25px" />
                    <AlternatingRowStyle BackColor="#DFE0E5" CssClass="griditem_alternative" />
                    <RowStyle CssClass="griditem" Height="25px" />
                </asp:GridView>
          
          </td>
                    </tr>
                    </table>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top">
            </td>
        </tr>
        <tr>
            <td align="center" style="height: 100px" valign="top" class="gridheader_text">
               
                <table id="TABLE3" runat="server" align="center" border="1" bordercolor="#1b80b6"
                    cellpadding="5" cellspacing="0" style="width: 100%">
                    <tr class="subheader_img">
                        <td colspan="6" style="height: 3px">
                            Document Type Detail 
                        </td>
                    </tr>
                    <tr>
                    <td valign="top" style="height: 197px">
                <asp:GridView ID="gvDoctypeDetail" runat="server" AutoGenerateColumns="False" Width="60%" CssClass="gridstyle" AllowPaging="True" PageSize="5">
                    <Columns>
                        <asp:BoundField DataField="Doc_name" HeaderText="Doc_name" Visible="False" />
                        <asp:TemplateField HeaderText="Doc No">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("DocNo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemStyle Wrap="False"/>
                            <ItemTemplate>
                                <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("DocNo") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="griditem_text" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Modified" HeaderText="Modified" />
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemStyle Wrap="False" />
                            <ItemTemplate>
                                &nbsp;&nbsp;
                                <asp:LinkButton ID="lbDocDetail" runat="server" OnClick="lbDocDetail_Click" >View</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="Wheat" />
                    <HeaderStyle CssClass="gridheader_new" Height="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <AlternatingRowStyle BackColor="#DFE0E5" CssClass="griditem_alternative" />
                    <RowStyle CssClass="griditem" Height="25px" />
                </asp:GridView>
                
                  </td>
                    </tr>
                    </table>
                
            </td>
        </tr>
        <tr>
            <td align="center" style="height: 12px" valign="top">
            </td>
        </tr>
        <tr>
       <td align="center" style="height: 99px" valign="top" class="gridheader_text">
               
                <table id="TABLE4" runat="server" align="center" border="1" bordercolor="#1b80b6"
                    cellpadding="5" cellspacing="0" style="width: 100%">
                    <tr class="subheader_img">
                        <td colspan="6" style="height: 3px"> Document
                                    Summary 
                        </td>
                    </tr>
                    <tr>
                    <td valign="top" style="height: 197px">
                <asp:GridView ID="gvSummary" runat="server" AutoGenerateColumns="False" CssClass="gridstyle" Width="100%" AllowPaging="True" PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("ID") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemStyle Wrap="False" />
                            <ItemTemplate>
                                <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DocType" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("DocType") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDoctype" runat="server" Text='<%# Bind("DocType") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DocName" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Doc_name") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDocName" runat="server" Text='<%# Bind("Doc_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Doc No">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("DocNo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemStyle Wrap="False" />
                            <ItemTemplate>
                                <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("DocNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="LogDT" HeaderText="Logged Date" DataFormatString="{0:dd/MMM/yyyy -- HH:mm:ss}" HtmlEncode="False" ConvertEmptyStringToNull="False" />
                        <asp:BoundField DataField="AudDT" HeaderText="Audit Date" DataFormatString="{0:dd/MMM/yyyy -- HH:mm:ss}" HtmlEncode="False" Visible="False" />
                        <asp:BoundField DataField="AudUser" HeaderText="User" />
                        <asp:BoundField DataField="Remark" HeaderText="Remarks" />
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemStyle Wrap="False" />
                            <ItemTemplate>
                                &nbsp;&nbsp;
                                <asp:LinkButton ID="lbAfter" runat="server" OnClick="lbAfterChange_Click" CausesValidation="False" >After Change</asp:LinkButton>
                                /<asp:LinkButton ID="lbBefore" runat="server" OnClick="lbBefore_Click" CausesValidation="False" >Before Change</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="Wheat" />
                    <HeaderStyle CssClass="gridheader_new" Height="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <AlternatingRowStyle BackColor="#DFE0E5" CssClass="griditem_alternative" />
                    <RowStyle CssClass="griditem" Height="25px" />
                </asp:GridView>
                
                </td>
                    </tr>
                    </table>
           
        </tr>
        <tr>
            <td align="center" valign="top">
            </td>
        </tr>
    </table>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfDescr" runat="server" />
</asp:Content>

