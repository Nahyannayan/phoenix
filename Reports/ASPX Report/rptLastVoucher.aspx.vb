﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.IO
Imports UtilityObj
Partial Class Reports_ASPX_Report_rptLastVoucher
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            Page.Title = OASISConstants.Gemstitle

            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            End If
            Select Case ViewState("MainMnu_code").ToString
                Case "A250096"
                    lblHead.Text = "Last Voucher"
            End Select
            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                txtToDate.Text = UtilityObj.GetDiplayDate()
                txtToDate.Attributes.Add("onBlur", "checkdate(this)")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
            Select Case ViewState("MainMnu_code").ToString
                Case "A250096"
                    RPTLastVoucherAudit()

            End Select
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub RPTLastVoucherAudit()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        '@return_value = [dbo].[RPTGetMISPnL]
        '@BSU_ID = N'999999|01101000|125016|125003|125016',
        '@DTTO = N'31/mar/2008'
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")

        Dim cmd As New SqlCommand("[RPTLastVoucherAudit]", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 400)
        sqlpJHD_BSU_IDs.Value = strSelectedNodes
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)
        objConn.Close()
        objConn.Open()
        If 1 = 1 Then
            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("UserName") = Session("sUsr_name")

            params("ToDate") = "As On " & txtToDate.Text
            params("ReportHeading") = "Last VoucherAudit"

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptLastVoucherAudit.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            Response.Redirect("rptviewer.aspx", True)
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

End Class
