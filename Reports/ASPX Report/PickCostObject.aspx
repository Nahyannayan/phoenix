<%@ OutputCache Duration="1" VaryByParam="none" %> 
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PickCostObject.aspx.vb" Inherits="Reports_ASPX_Report_PickCostObject" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Add Cost Object</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
     <base target="_self" />
    <script language="javascript" type="text/javascript" src="../../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
    function ChangeCheckBoxState(id, checkState)
        {
            var cb = document.getElementById(id);
            if (cb != null)
               cb.checked = checkState;
        }
        
        function ChangeAllCheckBoxStates(checkState)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked; 
           
            
            if (CheckBoxIDs != null)
            {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                   ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }
       function change_chk_state(src)
         {
                if (src==0)
                    var chk_state=document.getElementById('chkSelectall').checked;
                else
                    var chk_state=!(document.getElementById('chkSelectall').checked);

               for(i=0; i<document.forms[0].elements.length; i++)
               {
                if (document.forms[0].elements[i].type=='checkbox')
                    {
                    document.forms[0].elements[i].checked=chk_state;
                    }
               }
          }     
    
         </script>
</head>
<body  onload="listen_window();" topmargin="0" leftmargin="0">
 
    <form id="form1" runat="server">
     <table id="Table1" width="100%" align="center" border="1" cellpadding="0" cellspacing="0" bordercolor="#1b80b6">
      <tr class="subheader_img" valign="middle">
                        <td colspan="3"> &nbsp;Pick Cost units<input id="h_SelectedId" runat="server" type="hidden" value="=" />&nbsp;
                        </td>
                    </tr> 
     <tr>
     <td>
    <table id="tbl" width="96%" align="center" border="0" cellpadding="0" cellspacing="0">
                   
                    <tr>
                        <td class="matters" valign="middle" align="left" colspan="3">
                            &nbsp;<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="left" colspan="2">
                            <asp:TreeView ID="tvGroup" runat="server" ShowLines="True" ExpandDepth="0" DataSourceID="XmlDataSource1" ShowCheckBoxes="All">
                                <DataBindings>
                                    <asp:TreeNodeBinding DataMember="COSTCENTERItem" TextField="Text" ValueField="valuefield" />
                                </DataBindings>
                            </asp:TreeView>
                            <asp:XmlDataSource ID="XmlDataSource1" runat="server" TransformFile="~/XSLTFileCostCenter.xsl"
                                XPath="COSTCENTERItems/COSTCENTERItem"></asp:XmlDataSource>
                            &nbsp;
                            &nbsp;<!--
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                                SelectCommand="SELECT * FROM [vw_OSO_EMPLOYEE_M]"></asp:SqlDataSource>-->&nbsp;
                        </td>
                         
                    </tr>
                    <tr style="font-size: 12pt">
                        <td style="height: 19px">
                        </td>
                        <td align="center" style="height: 19px">
                            <asp:Button ID="Button1" runat="server" CssClass="button" Text="Finish" OnClick="Button1_Click" />&nbsp;</td>
                    </tr>
                </table>
        </td>
     </tr>
     </table>    
    </form>
</body>
</html>
