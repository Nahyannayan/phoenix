﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ReportBuilderadd.aspx.vb" Inherits="Reports_ASPX_Report_ReportBuilderadd" title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server" EnableViewState="false" >
<script type="text/javascript" language="javascript">

 function getWindow(NameId,ValId)
      {     
      
        var sFeatures;
        sFeatures="dialogWidth: 750px; ";
        sFeatures+="dialogHeight: 675px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var NameandCode;
        var result;       
          url= "ReportBuilderQuery.aspx" ;
        result = window.showModalDialog(url,"", sFeatures)
        if(result != '' && result != undefined)
        {
            NameandCode = result.split('||');
            document.getElementById(ValId).value=NameandCode[0];
            document.getElementById(NameId).value=NameandCode[1];
            return true;
        }
        else
        {
            return false;
        }
      }
 
</script>
 <table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
    <tr >
        <td  colspan="4" align="left">
        
        <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False" CssClass="error"></asp:Label>&nbsp;
            </td>
    </tr>
</table>
<table class="BlueTable" align="center" width="90%">            
    <tr class="subheader_img">
        <td colspan="5" style="height: 19px" align="left">
            Reporter Add Builder</td>                
    </tr>
    
    <tr>
        <td align="left" class="matters">
            Database</td>
        <td align="left" class="matters"  >
            <asp:DropDownList ID="ddlDbName" runat="server" AutoPostBack="True">
            </asp:DropDownList>
            </td>
        <td align="left" class="matters"  >
            Database Views
            </td>
        <td align="left" class="matters"  >
            <asp:DropDownList ID="ddlDataView" runat="server" AutoPostBack="True">
            </asp:DropDownList>
            </td>
        <td align="center" class="matters" rowspan="2"  >
         <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Find" CausesValidation="False" Width="65px"  />
            </td>
    </tr>
    <tr>
        <td align="left" class="matters">
            Database Name</td>
        <td align="left" class="matters"  >
         <asp:TextBox ID="txtdbName" runat="server" Width="226px"></asp:TextBox>
             </td>
        <td align="left" class="matters"  >
            View Name
             </td>
        <td align="left" class="matters"  >
         <asp:TextBox ID="txtViewname" runat="server" Width="226px"></asp:TextBox>
             </td>
    </tr>
</table>
<br />
  
         <table class="BlueTable_simple" align="center" width="90%" runat ="server" id ="TbNew">   
     <tr class="gridheader_pop" ><td align="left" >
         Report Fields</td></tr>
          <tr> 
            <td class="matters" align="center" ><span style="font-family: Verdana; text-decoration: underline; font-size:8pt" > </span>
                <asp:GridView ID="gvStudentDetails" runat="server" Width="98%" 
                    EmptyDataText="No Details Added" SkinID="GridViewPickDetails" 
                    OnRowDataBound="gvStudentDetails_RowDataBound" 
                    AutoGenerateColumns="False">
                    <Columns>
                        
<asp:TemplateField HeaderStyle-Width ="10px" >
    <ItemTemplate >
        <input ID="ChkSelect" type="checkbox" runat ="server" style ="cursor:hand "  />
    </ItemTemplate>

<HeaderStyle Width="20px"></HeaderStyle>
</asp:TemplateField>
                        <asp:BoundField DataField="NAME" HeaderText="Field Name" />
                       
                        <asp:TemplateField HeaderText ="Discription">
                            <ItemTemplate>
                                <asp:TextBox ID="txtFieldName" runat="server" Text ='<%# Bind("NAME") %>' Width="90%"></asp:TextBox>
                                
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width ="10px" HeaderText ="Filter" >
    <ItemTemplate >
        <input ID="ChkFilter" type="checkbox" runat ="server" style ="cursor:hand "  checked ="checked"  />
    </ItemTemplate>

<HeaderStyle Width="20px" ></HeaderStyle>
</asp:TemplateField>

<asp:TemplateField HeaderText ="Query" >
                            <ItemTemplate >
                            <asp:HiddenField ID="h_seletId" runat="server" />
                                <asp:TextBox ID="txtQuery" runat="server"  Width="250px"  Height ="40px" ReadOnly ="true"   ></asp:TextBox>
                                &nbsp;<asp:Image ID="ImgSelect" runat="server" ImageAlign="Top"  
                                    ImageUrl="~/Images/pick.GIF"  />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
</Columns>
            </asp:GridView>
        </td >
        </tr>     
     <tr>
         <td align="center" class="matters" valign="middle" >
         <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" 
                 CausesValidation="False" Width="65px"  />
             &nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" Width="65px" />
         </td>
     </tr>
    </table> 
     
</asp:Content>

