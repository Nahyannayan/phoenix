Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_MISBudgetColumnar
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared MainMnu_code As String
    Shared bconsolidation As Boolean

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = OASISConstants.Gemstitle
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If
        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            lblError.Text = ""
        End If
        'Add the readOnly attribute to the Date textBox So that the Text property value is accessible in the form
        'It is not giving the values if you make the ReadOnly property to TRUE
        'So you have to do this in page_Load()
        'txtToDate.Attributes.Add("ReadOnly", "Readonly")
        'txtFromDate.Attributes.Add("ReadOnly", "Readonly")

        'checks whether the BSUnit is selected if it is selected h_BSUID.Value will be having
        'the IDs of the BS Unit. "FillBSUNames()" function will set the BSU names
        If h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
            h_BSUID.Value = Session("sBsuid")
        End If

        If Not IsPostBack Then
            UtilityObj.NoOpen(Me.Header)
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Select Case MainMnu_code
                Case "A753981"
                    lblCaption.Text = "MIS Budget Columnar"
            End Select
            txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(Date.Now))

            txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
        End If
        If h_ACTIDs.Value <> "" Then
            FillACTIDs(h_ACTIDs.Value)
        End If

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        'Generate the XML in the form <BSU_DETAILS><BSU_ID>IDhere</BSU_ID></BSU_DETAILS>
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        txtFromDate.Text = Format("dd/MMM/yyyy", txtFromDate.Text)
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        'ViewState("fromDate") = txtFromDate.Text
        'ViewState("toDate") = txtToDate.Text
        'ViewState("selBSUIDs") = h_BSUID.Value
        Session("FDate") = txtFromDate.Text
        Session("selBSUID") = h_BSUID.Value
        Session("selACTIDs") = h_ACTIDs.Value
        Select Case MainMnu_code
            Case "A753981"
                GenerateMISBudgetColumnar()
        End Select

    End Sub

    Private Sub GenerateMISBudgetColumnar()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("[RptGetACCVsBudgetBalanceForColumnar]", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlpBSU_IDs.Value = UtilityObj.GetBSUnitWithSeperator(h_BSUID.Value, "|")
        cmd.Parameters.Add(sqlpBSU_IDs)

        Dim sqlpCTRLACS As New SqlParameter("@CTRLACS", SqlDbType.VarChar)
        sqlpCTRLACS.Value = UtilityObj.GetBSUnitWithSeperator(h_ACTIDs.Value, "|")
        cmd.Parameters.Add(sqlpCTRLACS)

        Dim sqlpDT As New SqlParameter("@Dt", SqlDbType.DateTime)
        sqlpDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpDT)

        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("DT") = Format("dd\MMM\yyyy", CDate(txtFromDate.Text))
            'params("bConsolidation") = bconsolidation
            'params("reportCaption") = "P/L Statement"
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/GetMISBudgetColumnar.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                Response.Redirect("rptviewer.aspx", True)
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
                'txtFromDate.Text = ViewState("fromDate")
                'txtToDate.Text = ViewState("toDate")
                'UsrBSUnits1.SetSelectedNodes(ViewState("selBSUIDs"))
                txtFromDate.Text = Session("FDate")
                UsrBSUnits1.SetSelectedNodes(Session("selBSUID"))
                h_ACTIDs.Value = Session("selACTIDs")
                FillACTIDs(Session("selACTIDs"))

            Else
                lblError.Text = ""
            End If
        End If
    End Sub

    Protected Sub lblAddActID_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'If h_Mode.Value = "party" Then
        h_ACTIDs.Value += "||" + txtBankNames.Text.Replace(",", "||")
        'ElseIf FillACTIDs(txtBSUName.Text) Then
        h_ACTIDs.Value = txtBankNames.Text
        'End If
        FillACTIDs(h_ACTIDs.Value)
    End Sub

    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(h_ACTIDs.Value)
        End If

    End Sub

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdACTDetails.PageIndexChanging
        grdACTDetails.PageIndex = e.NewPageIndex
        FillACTIDs(h_ACTIDs.Value)
    End Sub

    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Try
            Dim IDs As String() = ACTIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdACTDetails.DataSource = ds
            grdACTDetails.DataBind()
            'dr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            'txtBankNames.Text = ""
            'Dim bval As Boolean = dr.Read
            'While (bval)
            '    txtBankNames.Text += dr(0).ToString()
            '    bval = dr.Read()
            '    If bval Then
            '        txtBankNames.Text += "||"
            '    End If
            'End While
            'txtbankCodes.Text = ACTIDs
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

End Class
