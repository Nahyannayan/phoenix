<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptSettlement.aspx.vb" Inherits="Reports_ASPX_Report_rptSettlement"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">
           function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 229px; ";
            sFeatures+="dialogHeight: 254px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            switch(txtControl)
            {
              case 0:
                url="../../Accounts/calendar.aspx?dt="+document.getElementById('<%=txtfromDate.ClientID %>').value;
                break;
              case 1:  
                url="../../Accounts/calendar.aspx?dt="+document.getElementById('<%=txtToDate.ClientID %>').value;
                break;
            }    
            var NameandCode;
            var result;
            result = window.showModalDialog(url,"", sFeatures);
            if (result=='' || result==undefined)
            {
                return false;
            }
            switch(txtControl)
            {
              case 0:
                document.getElementById('<%=txtfromDate.ClientID %>').value=result;
                break;
              case 1:  
                document.getElementById('<%=txtToDate.ClientID %>').value=result;
                break;
            }    
           }
   
       

       function GetAccounts()
        {
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 450px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var mode = document.getElementById('<%=h_Mode.ClientID %>').value;
             
                result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?mode=S&multiSelect=false&forrpt=1","", sFeatures)
             
            if(result != "")
            {
                //alert(result);
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = result;//NameandCode[0];
                  document.getElementById('<%=txtBankNames.ClientID %>').value = result.split("___")[1];
                  document.getElementById('<%=txtbankCodes.ClientID %>').value = result.split("___")[0];
            }
        }  

    </script>
    
    <table align="center"class="BlueTable" cellpadding="5" cellspacing="0"
        style="width: 75%;">
         <tr class="subheader_img">
            <td align="left" colspan="8" valign="middle" style="height: 19px">Settlement Report 
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" style=" height: 5px; width: 557px;">
                Business Unit</td>
            <td class="matters" style=" height: 5px;">
                :</td>
            <td align="right" class="matters" colspan="6" style="height: 5px; text-align: left">
                <asp:DropDownList id="ddlBSUnit" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="matters" >
                From Date</td>
            <td class="matters" style=" height: 1px;">
                :</td>
            <td align="left" class="matters" colspan="2"><asp:TextBox ID="txtFromDate" runat="server" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
            <td align="left" class="matters">
                To Date</td>
            <td align="left" class="matters" style="height: 1px">
                :</td>
            <td align="left" class="matters" colspan="1"> <asp:TextBox ID="txtToDate" runat="server" Width="122px"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" /><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
                <td align="left">
                    <asp:RadioButton ID="rbDoc" runat="server" Checked="True" CssClass="radiobutton"
                        GroupName="dt" Text="Doc Date" />
                    <asp:RadioButton ID="rbRefdoc" runat="server" CssClass="radiobutton" GroupName="dt"
                        Text="Ref Doc Date" /></td>
        </tr>
        <tr style="color: #1b80b6" id = "Trbank">
            <td align="left" class="matters" style=" height: 17px; width: 557px;">
                Creditor</td>
            <td class="matters" style="height: 17px;">
                :</td>
            <td align="left" class="matters" colspan="6" style="height: 17px">
                <asp:TextBox ID="txtbankCodes" runat="server" Width="72px"></asp:TextBox>
                <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetAccounts()" Width="17px" />
                <asp:TextBox ID="txtBankNames" runat="server"
                    Width="359px"></asp:TextBox>&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="height: 22px; text-align: left">
                <asp:CheckBox ID="chkGroupCurrency" runat="server" Text="View in Group Currency" /><br />
                </td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="height: 22px; text-align: right">
                &nbsp;
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
      
    </table>   
    <asp:HiddenField ID="h_ACTIDs" runat="server" />
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:HiddenField ID="h_BSUID" runat="server" />
    <asp:HiddenField ID="h_Mode" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
  
</asp:Content>

