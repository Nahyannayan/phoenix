Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Accounts_accChqCancellation
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                txtBankDescr.Attributes.Add("ReadOnly", "ReadOnly")
                txtChqBook.Attributes.Add("ReadOnly", "ReadOnly")
                txtChqNo.Attributes.Add("ReadOnly", "ReadOnly")
                ddlBSUnit.DataSource = UtilityObj.GetBusinessUnits(Session("sUsr_name"))
                ddlBSUnit.DataTextField = "BSU_NAME"
                ddlBSUnit.DataValueField = "BSU_ID"
                ddlBSUnit.DataBind()
                ddlBSUnit.SelectedValue = Session("sbsuid")

                'txtBSUID.Text = Session("sbsuid")
                'txtBSUName.Text = Session("BSU_Name")
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Private Sub GenerateChequeStatus()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            'Dim str_Sql As String = " SELECT CHD_ID, CHB_ID,CHD_No, ACT_NAME," _
            '& " CHB_LOTNO,CHB_PREFIX ,CHB_FROM ,CHB_TO,CHB_NEXTNo,AvlNos" _
            '& " FROM vw_OSA_CHQBOOK_M AM WHERE CHD_ALLOTED = 0 AND CHB_BSU_ID ='" _
            '& Session("sBSUID") & "' AND CHB_ACT_ID= '" & txtBankCode.Text & _
            '"' AND CHB_LOTNO='" & txtChqBook.Text & "'"
            Dim str_Condition As String
            Dim str_Count_sql As String = String.Empty
            Dim str_Sql As String = "SELECT vw_OSA_CHQBOOK_M.CHB_ID, vw_OSA_CHQBOOK_M.CHB_ACT_ID, " & _
            "vw_OSA_CHQBOOK_M.ACT_NAME, " & _
            "vw_OSA_CHQBOOK_M.CHB_BSU_ID, vw_OSA_CHQBOOK_M.CHB_LOTNO, vw_OSA_CHQBOOK_M.CHB_PREFIX, " & _
            "vw_OSA_CHQBOOK_M.CHB_FROM, vw_OSA_CHQBOOK_M.CHB_TO, vw_OSA_CHQBOOK_M.CHB_NEXTNO, " & _
            "vw_OSA_CHQBOOK_M.AvlNos, vw_OSA_CHQBOOK_M.CHD_NO, vw_OSA_CHQBOOK_M.CHD_ALLOTED, " & _
            "vw_OSA_CHQBOOK_M.CHD_DOCNO, vw_OSA_CHQBOOK_M.CHB_PREV_CHB_ID, vw_OSA_CHQBOOK_M.CHD_ID, " & _
            "vw_OSO_BUSINESSUNIT_M.BSU_NAME FROM vw_OSA_CHQBOOK_M INNER JOIN " & _
            "vw_OSO_BUSINESSUNIT_M ON vw_OSA_CHQBOOK_M.CHB_BSU_ID = vw_OSO_BUSINESSUNIT_M.BSU_ID "
            str_Condition = " WHERE CHB_BSU_ID ='" & ddlBSUnit.SelectedValue & "' AND CHB_ACT_ID= '" & txtBankCode.Text & "'"
            If txtChqBook.Text <> "" Then
                str_Condition += " AND CHB_LOTNO='" & txtChqBook.Text & "'"
            End If
            str_Count_sql = "Select count(*) from vw_OSA_CHQBOOK_M " & str_Condition
            Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Count_sql)
            If count > 0 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = str_Sql & str_Condition
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.Text
                Dim repSource As New MyReportClass
                Dim params As New Hashtable
                params("RPT_CAPTION") = "CHEQUE STATUS REPORT"
                params("userName") = Session("sUsr_name")
                repSource.Parameter = params
                repSource.Command = cmd
                repSource.ResourceName = "../RPT_Files/rptCheqStatus.rpt"
                Session("ReportSource") = repSource
                If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                    Response.Redirect("rptviewer.aspx?isExport=true", True)
                Else
                    Response.Redirect("rptviewer.aspx", True)
                End If
            Else
                lblError.Text = "No Data with specific condition"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        GenerateChequeStatus()
    End Sub

End Class
