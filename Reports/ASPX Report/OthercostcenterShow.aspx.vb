Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Reports_ASPX_Report_OthercostcenterShow
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
        If Not IsPostBack Then
            PopulateTree()
            bind_previous_ids()
        End If
    End Sub

    Public Function GetSelectedNode()
        Dim strOthcost As New StringBuilder
        For Each node As TreeNode In tvCostcenter.CheckedNodes
            If node.Value.Length > 1 Then
                list_add(node.Value)
            End If
        Next
        Return strOthcost.ToString()
    End Function

    Protected Sub tvCostcenter_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvCostcenter.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateSubLevel(str, e.Node)
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As String, _
      ByVal parentNode As TreeNode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = " SELECT CCS_ID,CCS_DESCR FROM COSTCENTER_S  WHERE CCS_PARENT_CCS_ID = '" & parentid & "' " _
        & " GROUP BY CCS_ID,CCS_DESCR ORDER BY CCS_ID "
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), parentNode.ChildNodes)
    End Sub

    Private Sub PopulateRootLevel()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MainDB").ConnectionString
        Dim str_Sql As String
        tvCostcenter.Nodes.Clear()
        str_Sql = "SELECT CCS_ID,CCS_DESCR FROM COSTCENTER_S  WHERE (/*CCS_PARENT_CCS_ID ='0010'  OR */CCS_PARENT_CCS_ID IS NULL AND (CCS_CCT_ID <> '9999' OR CCS_ID = '0010' ) ) GROUP BY CCS_ID,CCS_DESCR ORDER BY CCS_ID"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), tvCostcenter.Nodes)
    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, _
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("CCS_DESCR").ToString()
            tn.Value = dr("CCS_ID").ToString().Trim()
            If Session("liUserList").Contains(tn.Value) Then
                tn.Checked = True
            End If
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = True
            'tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
            ' PopulateSubLevel(dr("CCS_ID").ToString(), tn)
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Private Sub bind_previous_ids()
        If Session("liUserListCost") Is Nothing Then
            Session("liUserListCost") = New List(Of String)
        End If
    End Sub

    Private Sub PopulateTree() 'Generate Tree
        tvCostcenter.Nodes.Clear()
        PopulateRootLevel()
        tvCostcenter.DataBind()
        'tvCostcenter.CollapseAll()
    End Sub

    Protected Sub btnDone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDone.Click
        h_SelectedId.Value = ""
        GetSelectedNode()
        Dim javaSr As String = "<script language=javascript>;function listen_window(){window.returnValue = 'done';window.close()}</script>"
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)
    End Sub

End Class


'Public Function SetSelectedNodes(ByVal strBSUid As String) As Boolean
'    For i As Integer = 0 To tvCostcenter.CheckedNodes.Count - 1
'        tvCostcenter.CheckedNodes(i).Checked = False
'    Next
'    Dim IDs As String() = strBSUid.Split("||")
'    Dim condition As String = String.Empty
'    For i As Integer = 0 To IDs.Length - 1
'        CheckSelectedNode(IDs(i), tvCostcenter.Nodes)
'    Next
'    Return True
'End Function

'Private Sub CheckSelectedNode(ByVal vBSU_ID As String, ByVal vNodes As TreeNodeCollection)
'    Dim ienum As IEnumerator = vNodes.GetEnumerator()
'    Dim trNode As TreeNode
'    While (ienum.MoveNext())
'        trNode = ienum.Current
'        If trNode.ChildNodes.Count > 0 Then
'            CheckSelectedNode(vBSU_ID, trNode.ChildNodes)
'        ElseIf trNode.Value = vBSU_ID Then
'            trNode.Checked = True

'        End If

'    End While

'End Sub

'Private Sub RePopulateValues() 
'    If Session("liUserList") IsNot Nothing AndAlso Session("liUserList").Count > 0 Then
'        For Each node As TreeNode In tvCostcenter.Nodes
'            Dim lstrIndex As String = node.Value.ToString()
'            If Session("liUserList").Contains(lstrIndex) Then
'                node.Checked = True
'            End If
'        Next
'    End If
'End Sub
