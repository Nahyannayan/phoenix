<%@ Page Language="VB" MasterPageFile="~/mainMasterPageSS.master" AutoEventWireup="false"
    CodeFile="rptReportViewerSS.aspx.vb" Inherits="Reports_ASPX_Report_rptReportViewer"
    Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function handleError() {

            return true;
        }
        window.onerror = handleError;
    </script>

    <table style="width: 100%; height: 100%">
        <tr style="width: 100%; height: 100%">
            <td align="left" valign="top" width="100%" style="vertical-align: top;">
                <div style="width: 900px; height: 1200px; overflow: auto; vertical-align: top">
  <CR:CrystalReportViewer ID="crv" runat="server" AutoDataBind="true" ToolPanelView="None"
                        EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False" ReportSourceID="rs"
                        ReuseParameterValuesOnRefresh="True" PrintMode="ActiveX" Width="700px" Height="50px"
                        ShowAllPageIds="True"  BestFitPage="true" HasToggleGroupTreeButton="False" />
                    &nbsp;<CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                    </CR:CrystalReportSource>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
