Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports UtilityObj
Imports System.Drawing.Printing
Partial Class Reports_ASPX_Report_RptViewerModalview
    Inherits System.Web.UI.Page

    Dim repSource As MyReportClass
    Dim repClassVal As New RepClass
    Shared rnd As New Random()


    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Using msOur As New System.IO.MemoryStream()
            Using swOur As New System.IO.StreamWriter(msOur)
                Dim ourWriter As New HtmlTextWriter(swOur)
                MyBase.Render(ourWriter)
                ourWriter.Flush()
                msOur.Position = 0
                Using oReader As New System.IO.StreamReader(msOur)
                    Dim sTxt As String = oReader.ReadToEnd()
                    If Web.Configuration.WebConfigurationManager.AppSettings("AlwaysSSL").ToLower = "true" Then
                        sTxt = sTxt.Replace(Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString(), _
                        Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString().Replace("http://", "https://"))
                    End If
                    Response.Write(sTxt)
                    oReader.Close()
                End Using
            End Using
        End Using
    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New DataSet
        'If Session("sBsuid") = "900500" Then
        '    'Page.Title = OASISConstants.Transporttitle
        'Else
        Page.Title = OASISConstants.Gemstitle
        'End If

        If Cache.Item("reportDS" & ViewState("h_uniqueID")) Is Nothing Then
            Cache.Remove("reportDS" & ViewState("h_uniqueID"))
            ViewState("h_uniqueID") = Session.SessionID & rnd.Next()
        End If
        If Not IsPostBack Then
            repSource = DirectCast(Session("ReportSource"), MyReportClass)
            Session("TempReportSource" & ViewState("h_uniqueID")) = repSource
        Else
            repSource = DirectCast(Session("TempReportSource" & ViewState("h_uniqueID")), MyReportClass)
        End If

        If Not repSource Is Nothing Then
            If repSource.GetDataSourceFromCommand Then
                If Cache("reportDS" & ViewState("h_uniqueID")) Is Nothing Then
                    'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
                    'Dim objConn As New SqlConnection(str_conn)
                    Dim objConn As SqlConnection = repSource.Command.Connection
                    'repSource.Command.Connection = objConn
                    objConn.Close()
                    objConn.Open()
                    Dim adpt As New SqlDataAdapter
                    adpt.SelectCommand = repSource.Command
                    'adpt.MissingSchemaAction = MissingSchemaAction.AddWithKey
                    adpt.SelectCommand.CommandTimeout = 0
                    ds.Clear()
                    adpt.Fill(ds)
                    If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
                        If Request.UrlReferrer <> Nothing AndAlso Request.UrlReferrer.ToString() <> "" Then
                            Dim urlstring As String = IIf(Request.UrlReferrer.ToString().Contains("?"), "&nodata=true", "?nodata=true")
                            Response.Redirect(Request.UrlReferrer.ToString() & urlstring)
                        End If
                    End If
                    objConn.Close()
                    Cache.Insert("reportDS" & ViewState("h_uniqueID"), ds, Nothing, DateTime.Now.AddMinutes(30), Nothing)
                End If
                repClassVal = New RepClass
                repClassVal.ResourceName = repSource.ResourceName
                ds = DirectCast(Cache.Item("reportDS" & ViewState("h_uniqueID")), DataSet)
                repClassVal.SetDataSource(ds.Tables(0))
                'repSource.IncludeBSUImage = False
                If repSource.IncludeBSUImage Then
                    If repSource.HeaderBSUID Is Nothing Then
                        IncludeBSUmage(repClassVal)
                    Else
                        IncludeBSUmage(repClassVal, repSource.HeaderBSUID)
                    End If
                End If
                SubReport(repSource, repClassVal)
                CrystalReportViewer1.ReportSource = repClassVal
                repClassVal.PrintOptions.PaperOrientation = repClassVal.PrintOptions.PaperOrientation
                'repClassVal.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                Dim ienum As IDictionaryEnumerator = repSource.Parameter.GetEnumerator()
                While ienum.MoveNext()
                    repClassVal.SetParameterValue(ienum.Key, ienum.Value)
                End While
                'objConn.Close()
            Else
                'Dim objConn As New SqlConnection(str_conn)
                Dim objConn As SqlConnection = repSource.Command.Connection
                'repSource.Command.Connection = objConn
                objConn.Close()
                objConn.Open()
                'Dim ds As New DataSet
                Dim adpt As New SqlDataAdapter
                adpt.SelectCommand = repSource.Command
                adpt.Fill(ds)
                repClassVal = New RepClass
                repClassVal.ResourceName = repSource.ResourceName
                '' '
                Dim fromDate As Date
                If repSource.Parameter IsNot Nothing Then
                    fromDate = CDate(repSource.Parameter("FromDate"))
                End If
                Dim dt As DataTable
                dt = TransformDatasettoTable.TransformDataset(ds.Tables(0), fromDate)

                repClassVal.SetDataSource(dt)
                CrystalReportViewer1.ReportSource = repClassVal
                Dim ienum As IDictionaryEnumerator = repSource.Parameter.GetEnumerator()
                While ienum.MoveNext()
                    repClassVal.SetParameterValue(ienum.Key, ienum.Value)
                End While
                objConn.Close()
            End If
        End If

        Try
            'For showing print dialog on page load
            Dim btnToClick = GetPrintButton(Me.Page)
            If btnToClick IsNot Nothing Then
                Dim btn As New System.Web.UI.WebControls.ImageButton
                h_print.Value = btnToClick.ClientID
            End If
            'For avoiding showing print dialog on eveyr page load
            Dim btnClicked As Object = PrinterFunctions.GetPostBackControl(Me.Page)

            If btnClicked IsNot Nothing Then
                Dim btnClickstatus As String = btnClicked.CommandName
                If btnClickstatus = "Export" Or btnClickstatus = "Print" Then
                    h_print.Value = ""
                End If
            End If
        Catch ex As Exception
        End Try

        repSource = Nothing
    End Sub


    Public Function GetPrintButton(ByVal p_Controls As Control) As System.Web.UI.Control
        Dim ctrlStr As String = [String].Empty
        For Each ctl As Control In p_Controls.Controls
            If ctl.HasControls() Then
                Dim PrintControl As New Control
                PrintControl = GetPrintButton(ctl)
                If Not PrintControl Is Nothing Then
                    Return PrintControl
                End If
            End If
            ' handle ImageButton controls 
            If ctl.ToString.EndsWith(".x") OrElse ctl.ToString.EndsWith(".y") Then
                ctrlStr = ctl.ToString.Substring(0, ctl.ToString.Length - 2)
            End If
            If TypeOf ctl Is System.Web.UI.WebControls.ImageButton Then
                Response.Write(ctl.ID)
                Dim btn As New System.Web.UI.WebControls.ImageButton
                btn = ctl
                If btn.CommandName = "Print" Then
                    Return ctl
                End If
            End If
        Next
        Return Nothing
    End Function


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Sub SubReport(ByVal subRep As MyReportClass, ByRef repClassVal As RepClass)
        If subRep.SubReport IsNot Nothing Then
            Dim ii As Integer = 0
            If subRep.IncludeBSUImage Then
                ii = 1
            End If
            For i As Integer = 0 To subRep.SubReport.Length - 1
                Dim myrep As MyReportClass = subRep.SubReport(i)
                If myrep IsNot Nothing Then
                    If myrep.SubReport IsNot Nothing Then
                        SubReport(subRep, repClassVal)
                    Else
                        Dim objConn As SqlConnection = repSource.Command.Connection
                        'objConn.Close()
                        objConn.Open()
                        Dim ds As New DataSet
                        Dim adpt As New SqlDataAdapter
                        adpt.SelectCommand = myrep.Command
                        adpt.Fill(ds)
                        objConn.Close()

                        If String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader.rpt", True) <> 0 Or _
                            String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Transport.rpt", True) <> 0 Or _
                            String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Portrait.rpt", True) <> 0 Or _
                            String.Compare(repClassVal.Subreports(i + ii).Name, "ReportHeader_SubReport_Payroll.rpt", True) <> 0 Or _
                            String.Compare(repClassVal.Subreports(i + ii).Name, "ReportHeader_SubReport.rpt", True) <> 0 Or _
                            String.Compare(repClassVal.Subreports(i + ii).Name, "ReportHeader_SubReport_Curr.rpt", True) <> 0 Or _
                            String.Compare(repClassVal.Subreports(i + ii).Name, "ReportHeader_SubReport.rpt", True) <> 0 Or _
                            String.Compare(repClassVal.Subreports(i + ii).Name, "ReportHeader_Sub_Landscape.rpt", True) <> 0 Then
                            repClassVal.Subreports(i + ii).SetDataSource(ds.Tables(0))
                        End If

                    End If
                End If
            Next
        End If
    End Sub


    Private Sub IncludeBSUmage(ByRef repClassVal As RepClass, Optional ByVal BSUID As String = "")
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(lstrConn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        'Dim cmd As New SqlCommand("getBsuInFoWithImage", objConn)
        Dim cmd As New SqlCommand("ReportHeader_Subreport", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpIMG_BSU_ID As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 100)
        If BSUID = "" Then
            sqlpIMG_BSU_ID.Value = Session("sbSUID")
        Else
            sqlpIMG_BSU_ID.Value = BSUID
        End If

        cmd.Parameters.Add(sqlpIMG_BSU_ID)

        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmd.Parameters.Add(sqlpIMG_TYPE)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        objConn.Close()
        If Not repClassVal.Subreports("rptHeader.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("ReportHeader_Sub_Landscape.rpt") Is Nothing Then
            repClassVal.Subreports("ReportHeader_Sub_Landscape.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Transport.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Transport.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Portrait.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Portrait.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("ReportHeader_SubReport_Payroll.rpt") Is Nothing Then
            repClassVal.Subreports("ReportHeader_SubReport_Payroll.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("ReportHeader_SubReport.rpt") Is Nothing Then
            repClassVal.Subreports("ReportHeader_SubReport.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("ReportHeader_SubReport_Curr.rpt") Is Nothing Then
            repClassVal.Subreports("ReportHeader_SubReport_Curr.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("ReportHeader_SubReport.rpt") Is Nothing Then
            repClassVal.Subreports("ReportHeader_SubReport.rpt").SetDataSource(ds.Tables(0))
        End If
        'repClassVal.Subreports("bsuimage").SetDataSource(dtDt)

        'repClassVal.Subreports("BSUIMAGE").SetDataSource(dtDt)
    End Sub


    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        'CrystalReportViewer1.Dispose()
        'CrystalReportViewer1 = Nothing
        repClassVal.Close()
        repClassVal.Dispose()
        'GC.Collect()
    End Sub


End Class
