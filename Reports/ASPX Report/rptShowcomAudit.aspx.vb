Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj

Partial Class Accounts_acccpPickCodes
    Inherits System.Web.UI.Page
    Shared SearchMode As String
    Shared liUserList As List(Of String)



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            h_Selected_menu_1.Value = "LI__../../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../../Images/operations/like.gif"


            SearchMode = Request.QueryString("id")


            If SearchMode = "M" Then
                Page.Title = "Module Info"
            ElseIf SearchMode = "L" Then
                Page.Title = "Login User Info"

            ElseIf SearchMode = "F" Then
                Page.Title = "Form Info"
            ElseIf SearchMode = "D" Then
                Page.Title = "Document Info"
            ElseIf SearchMode = "A" Then
                Page.Title = "Action Info"
            ElseIf SearchMode = "T" Then
                Page.Title = "Terminal Info"

            ElseIf SearchMode = "W" Then
                Page.Title = "Window User Info"

            End If



            liUserList = New List(Of String)
            gridbind()
            'content = Page.Master.FindControl("cphMasterpage")

        End If
        For Each gvr As GridViewRow In gvGroup.Rows
            'Get a programmatic reference to the CheckBox control
            Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
            If cb IsNot Nothing Then
                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
            End If
        Next
        ' reg_clientScript()
        set_Menu_Img()
        SetChk(Me.Page)
    End Sub


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))


    End Sub


    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code, str_filter_name As String
            Dim str_mode, str_BSUName As String
            Dim str_txtCode, str_txtName As String
            str_filter_code = ""
            str_filter_name = ""
            Dim strModNull As String = String.Empty

            Dim strAddTxt As String = String.Empty
            Dim strFilter As String = String.Empty
            Dim optSearch As String = String.Empty
            Dim tblbUsr_id As String = Session("sUsr_id")
            Dim tblbUSuper As Boolean = Session("sBusper")
            ' Dim BUnitreaderSuper As SqlDataReader
            str_mode = ""

            str_txtCode = ""
            str_txtName = ""
            str_BSUName = ""
            Dim str_query_header As String = String.Empty

            optSearch = Request.QueryString("opt")
            Dim strArrayTxt() As String

            If SearchMode = "M" Then
                str_query_header = "Select * from(SELECT distinct CASE WHEN isnull(AUD_MODULE, '') = '' THEN 'General'" & _
               " WHEN isnull(AUD_MODULE, '')='A0' THEN 'Accounts' WHEN isnull(AUD_MODULE, '') = 'P0' THEN 'Payroll' WHEN isnull(AUD_MODULE, '') = 'D0' THEN 'Master' " & _
               " WHEN isnull(AUD_MODULE, '') = 'I0' THEN 'Inventory' WHEN isnull(AUD_MODULE, '') = 'F0' THEN 'Fees' " & _
                " END as BSU_ID,'' as BSU_NAME from  AUDITTRAIL)a where a.BSU_ID<>''" & "|" & "Modules"
                '   str_query_header = "Select * from(Select distinct isnull(aud_user,'Anonymous') as BSU_ID,'' as BSU_NAME  from AUDITTRAIL )a where a.BSU_ID<>'' " & "|" & "Users|BSU Name"
                gvGroup.Columns(2).Visible = False
            ElseIf SearchMode = "L" Then

                str_query_header = "Select * from(Select distinct isnull(aud_user,'Anonymous') as BSU_ID,'' as BSU_NAME  from AUDITTRAIL )a where a.BSU_ID<>''" & "|" & "Login Users"

                gvGroup.Columns(2).Visible = False

            ElseIf SearchMode = "F" Then
                If (optSearch <> "") And (optSearch <> String.Empty) Then
                    ' InitializeFilter(strFilter)

                    strFilter += " a.filter IN( "
                    strArrayTxt = optSearch.Split("|")
                    strAddTxt = String.Empty


                    For count As Integer = 0 To strArrayTxt.Length - 1

                        If count <> 0 Then
                            If LCase(strArrayTxt(count)) <> "general" Then
                                strAddTxt += ","

                            End If

                        End If
                        Select Case LCase(strArrayTxt(count))

                            Case "accounts"
                                strAddTxt += "'" + "A0" + "'"

                            Case "payroll"
                                strAddTxt += "'" + "P0" + "'"

                            Case "master"
                                strAddTxt += "'" + "D0" + "'"

                            Case "inventory"
                                strAddTxt += "'" + "I0" + "'"

                            Case "fees"
                                strAddTxt += "'" + "F0" + "'"

                        End Select
                        If LCase(strArrayTxt(count)) = "general" Then
                            strModNull = "  or a.filter is null"
                        End If
                    Next
                    If Right(strAddTxt, 1) = "," Then
                        strAddTxt = Left(strAddTxt, Len(strAddTxt) - 1)
                    End If

                    If Left(strAddTxt, 1) = "," Then
                        strAddTxt = Mid(strAddTxt, 2)
                    End If
                    strFilter += strAddTxt + ")" + strModNull
                    str_query_header = "Select distinct BSU_ID,BSU_NAME from(Select  AUD_FORM as BSU_ID,'' as BSU_NAME ,AUD_MODULE as filter from AUDITTRAIL )a where " & strFilter & "|" & "Form Name"



                Else
                    str_query_header = "Select distinct BSU_ID,BSU_NAME from(Select  AUD_FORM as BSU_ID,'' as BSU_NAME  from AUDITTRAIL)a where a.BSU_ID<>''" & "|" & "Form Name"
                End If


                gvGroup.Columns(2).Visible = False
            ElseIf SearchMode = "D" Then

                If (optSearch <> "") And (optSearch <> String.Empty) Then

                    strFilter += " a.filter IN( "
                    strArrayTxt = optSearch.Split("|")
                    strAddTxt = String.Empty

                    For count As Integer = 0 To strArrayTxt.Length - 1
                        If count <> 0 Then
                            strAddTxt += ", "
                        End If
                        strAddTxt += "'" + strArrayTxt(count) + "'"
                    Next

                    If Right(strAddTxt, 1) = "," Then
                        strAddTxt = Left(strAddTxt, Len(strAddTxt) - 1)
                    End If

                    If Left(strAddTxt, 1) = "," Then
                        strAddTxt = Mid(strAddTxt, 2)
                    End If
                    strFilter += strAddTxt + ")"

                    str_query_header = "Select distinct BSU_ID,BSU_NAME from(Select   AUD_DOCNO as BSU_ID,'' as BSU_NAME ,AUD_Form as filter from AUDITTRAIL)a where " & strFilter & "|" & "Document Code"

                Else
                    str_query_header = "Select distinct BSU_ID,BSU_NAME from(Select   AUD_DOCNO as BSU_ID,'' as BSU_NAME  from AUDITTRAIL )a where a.BSU_ID<>''" & "|" & "Document Code"
                End If


                gvGroup.Columns(2).Visible = False
            ElseIf SearchMode = "A" Then

                str_query_header = "Select distinct BSU_ID,BSU_NAME from(Select  AUD_ACTION as BSU_ID,'' as BSU_NAME  from AUDITTRAIL )a where a.BSU_ID<>'' " & "|" & "Action"
            ElseIf SearchMode = "T" Then

                str_query_header = "Select distinct BSU_ID,BSU_NAME  from(Select  AUD_HOST as BSU_ID,'' as BSU_NAME  from AUDITTRAIL )a where a.BSU_ID<>''" & "|" & "Terminals"

            ElseIf SearchMode = "W" Then

                str_query_header = "Select distinct BSU_ID,BSU_NAME from(Select AUD_WINUSER as BSU_ID,'' as BSU_NAME  from AUDITTRAIL )a where a.BSU_ID<>''" & "|" & "Window Users"

            End If



            str_Sql = str_query_header.Split("|")(0)

            Dim str_headers As String()
            'str_query_header = str_query_header.Split("__")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_Sql.Replace("###", Session("sBsuid"))
            Dim lblheader As New Label

            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("BSU_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("BSU_NAME", str_Sid_search(0), str_txtName)


            End If

            Dim ds As New DataSet


            str_Sql = str_Sql & str_filter_code & str_filter_name & " order by a.BSU_ID"
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
                gvGroup.Columns(2).Visible = False

            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If

            ' Next
            set_Menu_Img()

            SetChk(Me.Page)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Private Sub InitializeFilter(ByRef filter As String)


        If filter IsNot String.Empty Or filter IsNot "" Then

            'using the filter option to add WHERE clause before adding condition
            If Not filter.StartsWith(" WHERE") Then

                filter = filter.Insert(0, " WHERE")
            End If
            'using the filter option to add AND clause after adding each condition
            If Not filter.EndsWith("WHERE") Then
                filter = filter.Insert(filter.Length, " AND")
            End If
        End If
    End Sub


    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Sub reg_clientScript()
        '& "xWin.form1.h_Memberids.value=xWin.form1.h_Memberids.value +'%' + document.getElementById('h_SelectedId').value;" _
        '       & "xWin.form1.submit();window.close();" _
        Dim scr As String = "<script>" & Chr(13) & "" & Chr(10) _
        & "function Done()" & Chr(13) _
        & "" & Chr(10) & "" & Chr(9) _
        & "" & Chr(9) & "" & Chr(9) _
        & "{  " & Chr(9) & "" & Chr(9) _
        & "" & Chr(9) & "" & Chr(9) & "" & Chr(9) _
        & "" & Chr(9) & "" & Chr(9) & "" & Chr(9) _
        & "" & Chr(9) & "" & Chr(9) & " " _
        & Chr(9) & "" & Chr(9) & "" & Chr(9) _
        & "" & Chr(9) & "" & Chr(9) & "" _
        & Chr(9) & "" & Chr(9) & "" & Chr(9) _
        & "" & Chr(9) & " " & Chr(13) & "" _
        & Chr(10) & "" & Chr(9) & "" & Chr(9) _
        & "" & Chr(9) _
        & " var xWin=window.dialogArguments; " _
        & " xWin.update(document.getElementById('h_SelectedId').value); " _
        & " window.close();}" _
        & Chr(9) & "  " & Chr(13) & "" & Chr(10) _
 & Chr(9) & "</script>"
        'Page.RegisterClientScriptBlock("done", scr)
        'Dim scr As String = "<script>" & "function Done() {alert ('client');window.opener.update('fName');//window.close(); }</script>"
        Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "done", scr)
    End Sub

    'pass the page control to the function to find the checkbox control
    Private Sub SetChk(ByVal Page As Control)
        'loop through all the control in that page
        For Each ctrl As Control In Page.Controls
            'if ctrl is of type checkbox then check the status of checkbox
            If TypeOf ctrl Is HtmlInputCheckBox Then


                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)

                'if chk are checked  then add to the listitem through the function list_add
                If chk.Checked = True Then


                    'h_SelectedId.Value = h_SelectedId.Value & "%" & chk.Value.ToString

                    ' Response.Write(chk.Value.ToString & "->")

                    ' list_add return false then  make the chkbox checked; which is checked through contains method in listitem
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If


            Else
                'check the total number checkbox in that page control and keep loop till the last checkbox
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next

    End Sub


    Private Function list_exist(ByVal p_userid As String) As Boolean
        If liUserList.Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Function list_add(ByVal p_userid As String) As Boolean
        If liUserList.Contains(p_userid) Then
            Return False
        Else
            liUserList.Add(p_userid)
            DropDownList1.DataSource = liUserList
            DropDownList1.DataBind()
            Return False
        End If
    End Function


    Private Sub list_remove(ByVal p_userid As String)
        If liUserList.Contains(p_userid) Then
            liUserList.Remove(p_userid)
        End If
    End Sub


    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    'when clicking on the image(mirror) button in the search the record based on the code
    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    'when clicking on the image(mirror) button in the search the record based on the name
    Protected Sub btnNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        SetChk(Me.Page)
        h_SelectedId.Value = ""
        For i As Integer = 0 To liUserList.Count - 1
            If h_SelectedId.Value <> "" Then
                h_SelectedId.Value += "|"
            End If
            h_SelectedId.Value += liUserList(i).ToString
        Next

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        Response.Write("window.close();")
        Response.Write("} </script>")

    End Sub


End Class
