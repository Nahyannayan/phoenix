<%@ OutputCache Duration="1" VaryByParam="none" %> 
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="rptShowcomAudit.aspx.vb" Inherits="Accounts_acccpPickCodes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Add Sub Cost Center</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    
     <base target="_self" />
    <script language="javascript" type="text/javascript" src="../../cssfiles/chromejs/chrome.js">
    
    
    </script>
    <script language="javascript" type="text/javascript">
    function ChangeCheckBoxState(id, checkState)
        {
            var cb = document.getElementById(id);
            if (cb != null)
               cb.checked = checkState;
        }
        
        function ChangeAllCheckBoxStates(checkState)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked; 
           
            
            if (CheckBoxIDs != null)
            {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                   ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }
       
      function menu_click(val,mid)
                {
                //alert(val);
                var path;
                if (val=='LI')
                {
                path='../../images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../../images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../../images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../../images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../../images/operations/notendswith.gif';
                }
               if (mid==1)
                 {
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value=val+'__'+path;
                 document.getElementById('<%=getid1() %>').src = path;
                 }
                 else  if (mid==2)
                 {
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value=val+'__'+path;
                 document.getElementById('<%=getid2() %>').src = path;
                 }
                
                 
                }//end fn
         </script>
</head>
<body  onload="listen_window();" topmargin="0" leftmargin="0" bottommargin="0" rightmargin="0">
<!--1st drop down menu -->                                                   
<div id="dropmenu1" class="dropmenudiv" style="width: 110px;">
<a href="javascript:menu_click('LI',1);"><img class="img_left" alt="Any where" src= "../../images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:menu_click('NLI',1);"><img class="img_left" alt="Not In" src= "../../images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:menu_click('SW',1);"><img class="img_left" alt="Starts With" src= "../../images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:menu_click('NSW',1);"><img class="img_left" alt="Like" src= "../../images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:menu_click('EW',1);"><img class="img_left" alt="Like" src= "../../images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:menu_click('NEW',1);"><img class="img_left" alt="Like" src= "../../images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>
<!--2nd drop down menu -->                                                
<div id="dropmenu2" class="dropmenudiv" style="width: 110px;">
<a href="javascript:menu_click('LI',2);"><img class="img_left" alt="Like" src= "../../images/operations/like.gif" />&nbsp;Any Where</a>
<a href="javascript:menu_click('NLI',2);"><img class="img_left" alt="Like" src= "../../images/operations/notlike.gif" />&nbsp;Not In</a>
<a href="javascript:menu_click('SW',2);"><img class="img_left" alt="Like" src= "../../images/operations/startswith.gif" />&nbsp;Starts With</a>
<a href="javascript:menu_click('NSW',2);"><img class="img_left" alt="Like" src= "../../images/operations/notstartwith.gif" />&nbsp;Not Start With</a>
<a href="javascript:menu_click('EW',2);"><img class="img_left" alt="Like" src= "../../images/operations/endswith.gif" />&nbsp;Ends With</a>
<a href="javascript:menu_click('NEW',2);"><img class="img_left" alt="Like" src= "../../images/operations/notendswith.gif" />&nbsp;Not Ends With</a>
</div>

    <form id="form1" runat="server">     
    <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="1%">
                &nbsp;</td>
            <td  width="98%">
             <input id="h_SelectedId" runat="server" type="hidden" value="" />
            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
            <input id="h_selected_menu_2" runat="server" type="hidden" value="=" /></td>
            <td  width="1%">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="center" valign="top">
                <asp:GridView ID="gvGroup" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                EmptyDataText="No Data" HeaderStyle-CssClass="gridheader" Width="98%" CssClass="gridstyle">
                                <Columns>
                                    <asp:TemplateField HeaderText="Select" SortExpression="ID">
                                        <HeaderTemplate>
                                            Select
                                            <input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            &nbsp;<input id="chkControl" runat="server" type="checkbox" value='<%# Bind("BSU_ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Id" SortExpression="ID">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <table style="width: 100%; height: 100%">
                                                <tr>
                                                    <td align="center" style="width: 100px">
                                                        <asp:Label ID="lblID" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="10pt" ForeColor="#1B80B6"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100px">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 100px; height: 12px">
                                                                    <div id="chromemenu1" class="chromestyle">
                                                                        <ul>
                                                                            <li><a href="#" rel="dropmenu1">
                                                                                <img id="mnu_1_img" runat="server" align="middle" alt="Menu" border="0" src="../../Images/operations/like.gif" /><span
                                                                                    style="color: #0000ff; text-decoration: none">&nbsp;</span></a> </li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                                <td style="width: 100px; height: 12px">
                                                                    <asp:TextBox ID="txtCode" runat="server" Width="72px"></asp:TextBox></td>
                                                                <td style="width: 100px; height: 12px" valign="middle">
                                                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                                        OnClick="btnCodeSearch_Click" />&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("BSU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name" SortExpression="NAME" Visible="False">
                                        <HeaderTemplate>
                                            <table style="width: 100%; height: 100%">
                                                <tr>
                                                    <td align="center" style="width: 100px">
                                                        <asp:Label ID="lblName" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="10pt" ForeColor="#1B80B6"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100px">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 100px; height: 12px">
                                                                    <div id="chromemenu2" class="chromestyle">
                                                                        <ul>
                                                                            <li><a href="#" rel="dropmenu2">
                                                                                <img id="mnu_2_img" runat="server" align="middle" alt="Menu" border="0" src="../../Images/operations/like.gif" /><span
                                                                                    style="color: #0000ff; text-decoration: none">&nbsp;</span></a> </li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                                <td style="width: 100px; height: 12px">
                                                                    <asp:TextBox ID="txtName" runat="server" Width="72px"></asp:TextBox></td>
                                                                <td style="width: 100px; height: 12px" valign="middle">
                                                                    <asp:ImageButton ID="btnNameSearch" runat="server" ImageAlign="Top" ImageUrl="../../Images/forum_search.gif"
                                                                        OnClick="btnNameSearch_Click" />&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>&nbsp;<br />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle BackColor="LightCyan" CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="center">
                <strong><span style="font-size: 8pt; color: #800000; font-family: Verdana">&nbsp;</span></strong>
                            &nbsp;<asp:DropDownList ID="DropDownList1" runat="server" CssClass="listbox" Height="16px" Width="112px">
                                </asp:DropDownList>&nbsp;
                            <asp:Button ID="btnFinish" runat="server" Text="Finish" CssClass="button" /></td>
            <td>       <span id="sp_message" runat="server" class="error"></span>
            </td>
        </tr>
        
    </table>
    <script type="text/javascript">    
                cssdropdown.startchrome("chromemenu1");
            //    cssdropdown.startchrome("chromemenu2");
            </script>
    </form>
</body>
</html>
