<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PdcSchedule.aspx.vb" 
Inherits="Reports_ASPX_Report_PdcSchedule" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">
           function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 250px; ";
            sFeatures+="dialogHeight: 270px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx","", sFeatures);
            
            if(result != '' && result != undefined)
            {
                switch(txtControl)
                {
                  case 0:
                    document.getElementById('<%=txtfromDate.ClientID %>').value=result;
                    break;
                  case 1:  
                    document.getElementById('<%=txtToDate.ClientID %>').value=result;
                    break;
                }    
            }
            return false;
           }
   
       
       function GetAccounts()
        {
            var sFeatures;
            sFeatures="dialogWidth: 820px; ";
            sFeatures+="dialogHeight: 450px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
           
                result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=g&forrpt=1","", sFeatures)
           
            if(result != '' && result != undefined)
            {
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else
            {
                return false;
            }
        }  

    </script>
   <table align="center" style="width: 80%;" cellpadding="0" cellspacing="0">  
       <tr align=left >
           <td><asp:ValidationSummary   ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="dayBook" />
               <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
           </td>
       </tr>
    </table>
  
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0" style="width: 80%; height: 174px" runat =server id="tblMain">
        <tr  class="subheader_img">
            <td align="left" colspan="8" style="height: 19px" valign="middle">                 
                        <asp:Label ID="lblrptCaption" runat="server" Text="PDC Schedule"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 68px" >
                From Date</td>
            <td class="matters" style="width: 18px"  >
                :</td>
            <td align="left" class="matters" colspan="2" style="height: 1px; text-align: left;">
                <asp:TextBox ID="txtFromDate" runat="server" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate" EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
            <td align="left" class="matters" style="color: #1b80b6;">
                To Date</td>
            <td align="left" class="matters" style="height: 1px">
                :</td>
            <td align="left" class="matters" colspan="2" style="height: 1px; text-align: left;">
                <asp:TextBox ID="txtToDate" runat="server" Width="122px"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>&nbsp;
                <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr style="color: #1b80b6" id = "Trbank">
            <td align="left" valign ="top" class="matters" style="width: 68px">
                <asp:Label ID="lblBankCash" runat="server" Text="Bank"></asp:Label></td>
            <td class="matters" valign = "top" style="width: 18px">
                :<br />
            </td>
            <td align="left" class="matters" colspan="6" style="height: 17px">
                <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the ACT ID you want to Add to the Search and click on Add)" Width="377px"></asp:Label>
                <asp:TextBox ID="txtBankNames" runat="server" Height="18px"
                    Width="330px" CssClass="inputbox"></asp:TextBox>
                <asp:LinkButton ID="lblAddActID" runat="server">Add</asp:LinkButton>
                <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="return GetAccounts()" /><br />
                <asp:GridView ID="grdACTDetails" runat="server" AutoGenerateColumns="False" Width="95%" AllowPaging="True" PageSize="5">
                        <Columns>
                            <asp:TemplateField HeaderText="ACT ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ACT_Name" HeaderText="ACT Name" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                             <HeaderStyle CssClass="gridheader_new" />
                    </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style=" text-align: left">
                <asp:RadioButton ID="RdSummary" runat="server" Checked="True" GroupName="RD" Text="Summary" />
                &nbsp;&nbsp;<asp:RadioButton ID="RdUnite" runat="server" GroupName="RD" Text="By Unit" />&nbsp;<asp:RadioButton
                    ID="RdDetails" runat="server" GroupName="RD" Text="Details" /></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style=" text-align: right">
                <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>&nbsp;
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="dayBook" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="h_ACTIDs" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
    <asp:HiddenField ID="h_Mode" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
</asp:Content>

