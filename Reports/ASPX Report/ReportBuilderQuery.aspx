﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReportBuilderQuery.aspx.vb" Inherits="Reports_ASPX_Report_ReportBuilderQuery" %>

   <%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server"> 
   <base target="_self" />
  <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
 <script language="javascript" type="text/javascript" src="../../chromejs/chrome.js">
</script> 
<script type ="text/javascript" language ="javascript" >
    function getValue(retval,NameVal)
    {
    
    window.returnValue = retval +"||"+ NameVal ;
    window.close();      
    }
    function MouseOver(ObjId)
    {
    document.getElementById(ObjId).style.color ="red" ;
    }
    function MouseLeave(ObjId)
    {
    document.getElementById(ObjId).style.color ="#1b80b6" ;
    }
    
  
    
    
    </script>
</head>
<body class="matter" leftmargin="0" topmargin="0" bottommargin="0" rightmargin="0">
  <form id="form1" runat="server">
           <br />
<table class="BlueTable" align="center" width="90%">
<tr><td colspan="2" class="subheader_img" >Add Query &nbsp;
<asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False" CssClass="error"></asp:Label>
</td></tr>
<tr class="matters" ><td width ="100px">Query Name</td><td>
                       <asp:TextBox ID="txtQueryName" runat="server" Width ="200px"></asp:TextBox>
                       &nbsp;<asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" 
                           Width="78px"  />
                       </td></tr>
<tr class="matters"><td>Example</td><td>
                       SELECT BSU_ID AS SelectId,BSU_ID as ViewCode,BSU_NAME as ViewName FROM 
                       BUSINESS_UNIT WHRE 1=1</td></tr>
<tr class="matters"><td>Query </td><td>
                       <asp:TextBox ID="txtQuery" runat="server" Width ="90%" 
                           TextMode ="MultiLine" Height="80px" ></asp:TextBox>
                       </td></tr>
</table> 
<br />
 <table width="98%" align="center" border="0" cellpadding="0" cellspacing="0">
 <tr class="matters"><td align="center" >
 <table class="BlueTable" align="center" width="100%"><tr class="matters"><td>
 Query Name&nbsp;
                <asp:TextBox ID="txtFind" runat="server" Width ="55%"></asp:TextBox>&nbsp;<asp:Button 
                    ID="btnFind" runat="server" CssClass="button" Text="Find" Width="70px"  />
 </td></tr></table> 
            
                    
                    </td></tr>
      <tr>
        <td style ="cursor:hand; " >
        <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"  Width="100%" EmptyDataText="No Data" AllowPaging="True" PageSize="10" SkinID="GridViewView">
          <Columns>
            
              <asp:BoundField DataField="FQR_ID" HeaderText="id"  />
              <asp:BoundField DataField="FQR_Name" HeaderText="Query Name"  />
              <asp:BoundField DataField="FQR_Query" HeaderText="Query " />
          </Columns>
        </asp:GridView>
        </td>
      </tr>      
   
    </table>  
 
</form>
</body>
</html>