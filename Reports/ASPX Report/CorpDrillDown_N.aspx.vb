Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports Telerik.Web.UI
Partial Class Reports_ASPX_Report_CorpDrillDown_N
    Inherits System.Web.UI.Page
    Private Sub Bind_BsuDrilldown()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@ACT_LVL", 1)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "rpt_Corp_DrillDown", parm)
            gv_BsuDrilldown.DataSource = ds
            gv_BsuDrilldown.DataBind()
        Catch ex As Exception
            ' lbldeletemsg.Text = ex.Message
        End Try
    End Sub
    Protected Sub gv_BsuDrilldown_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gv_BsuDrilldown.NeedDataSource
        Bind_BsuDrilldown()
    End Sub
    Public Function GetDataTable(ByVal query As String) As DataTable
        Dim ConnString As String = ConfigurationManager.ConnectionStrings("NorthwindConnectionString").ConnectionString
        Dim connection1 As New SqlConnection(ConnString)
        Dim adapter1 As New SqlDataAdapter
        adapter1.SelectCommand = New SqlCommand(query, connection1)
        Dim table1 As New DataTable
        connection1.Open()
        Try
            adapter1.Fill(table1)
        Finally
            connection1.Close()
        End Try
        Return table1
    End Function
    Protected Sub gv_BsuDrilldown_DetailTableDataBind(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridDetailTableDataBindEventArgs) Handles gv_BsuDrilldown.DetailTableDataBind
        Dim dataItem As GridDataItem = CType(e.DetailTableView.ParentItem, GridDataItem)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim CustomerID As String = dataItem.GetDataKeyValue("BSU_Name").ToString()
        Select Case e.DetailTableView.Name
            Case "World"
                Dim parm(4) As SqlClient.SqlParameter
                parm(0) = New System.Data.SqlClient.SqlParameter("@ACT_LVL", 2)
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "rpt_Corp_DrillDown", parm)
                e.DetailTableView.DataSource = ds.Tables(0)
            Case "UAE"
                Dim BSU_Name As String = dataItem.GetDataKeyValue("BSU_Name").ToString()
                Dim parm(4) As SqlClient.SqlParameter
                parm(0) = New System.Data.SqlClient.SqlParameter("@ACT_LVL", 3)
                parm(1) = New System.Data.SqlClient.SqlParameter("@LVL1", BSU_Name)
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "rpt_Corp_DrillDown", parm)
                e.DetailTableView.DataSource = ds.Tables(0)
            Case "DUBAI"
                Dim BSU_Name As String = dataItem.GetDataKeyValue("BSU_Name").ToString()
                Dim parm(4) As SqlClient.SqlParameter
                parm(0) = New System.Data.SqlClient.SqlParameter("@ACT_LVL", 4)
                parm(1) = New System.Data.SqlClient.SqlParameter("@LVL1", BSU_Name)
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "rpt_Corp_DrillDown", parm)
                e.DetailTableView.DataSource = ds.Tables(0)
            Case "INTERNATIONAL"
                Dim BSU_Name As String = dataItem.GetDataKeyValue("BSU_Name").ToString()
                Dim parm(4) As SqlClient.SqlParameter
                parm(0) = New System.Data.SqlClient.SqlParameter("@ACT_LVL", 5)
                parm(1) = New System.Data.SqlClient.SqlParameter("@LVL1", BSU_Name)
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "rpt_Corp_DrillDown", parm)
                e.DetailTableView.DataSource = ds.Tables(0)
        End Select
    End Sub
    Protected Sub gv_BsuDrilldown_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv_BsuDrilldown.PreRender
        'If Not Page.IsPostBack Then
        '    gv_BsuDrilldown.MasterTableView.Items(0).Expanded = True
        '    gv_BsuDrilldown.MasterTableView.Items(0).ChildItem.NestedTableViews(0).Items(0).Expanded = True
        'End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Bind_BsuDrilldown()
        End If
    End Sub
End Class


