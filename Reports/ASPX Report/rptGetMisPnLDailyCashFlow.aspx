<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptGetMisPnLDailyCashFlow.aspx.vb" Inherits="Reports_ASPX_Report_rptGetMisPnLDailyCashFlow" title="Untitled Page" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server"> 
    <script language="javascript" type="text/javascript">
     function change_chk_state(src)
         {
               var chk_state=(src.checked);
               for(i=0; i<document.forms[0].elements.length; i++)
               {
                if (document.forms[0].elements[i].type=='checkbox')
                    {
                    document.forms[0].elements[i].checked=chk_state;
                    }
               }
          }     
        function client_OnTreeNodeChecked( )
        { 
                var obj = window.event.srcElement;
                var treeNodeFound = false;
                var checkedState;
                if (obj.tagName == "INPUT" && obj.type == "checkbox") {
//                //
//                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
//                change_chk_state(obj); }
//                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                    } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                    if (numTables >= 1)
                    {
                        for (i=0; i < numTables; i++)
                        {
                            if (tables[i] == obj)
                            {
                                treeNodeFound = true;
                                i++;
                                if (i == numTables)
                                    {
                                    return;
                                    }
                            }
                            if (treeNodeFound == true)
                            {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel)
                                {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                                }
                            else
                                {
                                return;
                                }
                            }
                        }
                    }
                }
        }
    </script>
    
   <table align="center" style="width: 80%;" > 
       <tr align =left >
           <td>
               <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
    <asp:ValidationSummary   ID="ValidationSummary2" runat="server" EnableViewState="False"
                    HeaderText="Following condition required" ValidationGroup="dayBook" />
           </td>
       </tr>
    </table>
    
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0" style="width: 80%;">
        <tr  class="subheader_img">
            <td align="left" colspan="4" style="height: 19px" valign="middle">                 
                        <asp:Label ID="lblrptCaption" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" valign = "middle" class="matters" style="width: 88px" >
                Business Unit</td>
            <td align="left" valign = "middle" class="matters" colspan="3" > 
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                </td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 88px" >
                From Date</td>
            <td align="left" class="matters" colspan="1" style="height: 1px; text-align: left;">
                <asp:TextBox ID="txtFromDate" runat="server" Width="122px"></asp:TextBox>
                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>&nbsp;
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFromDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
            <td align="left" class="matters" colspan="1" style="height: 1px; text-align: left">
                To Date</td>
            <td align="left" class="matters" colspan="1" style="height: 1px; text-align: left">
                <asp:TextBox ID="txtToDate" runat="server" Width="122px"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>&nbsp;
                <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
             
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 88px">
                Express In</td>
            <td align="left" class="matters" colspan="3" 
                style="height: 1px; text-align: left">
                <asp:DropDownList ID="ddExpressedin" runat="server">
                    <asp:ListItem Value="-1">Actual</asp:ListItem>
                    <asp:ListItem>0</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>100</asp:ListItem>
                    <asp:ListItem>1000</asp:ListItem>
                </asp:DropDownList>
                <asp:CheckBox ID="chkExcludeSpecialAccounts" runat="server" 
                    Text="Exclude Special Accounts" Visible="False" />
            </td>
        </tr>
        <tr>
            <td align="center" class="matters" colspan="4" >
                &nbsp;&nbsp;
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="dayBook" /><asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table> 
    
     <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="ImageButton1" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
     <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="txtFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender> 
 <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
     <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="txtToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>

</asp:Content>
