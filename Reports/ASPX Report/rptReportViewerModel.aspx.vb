Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.IO
Imports system.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections

Partial Class Reports_ASPX_Report_rptReportViewerx
    Inherits System.Web.UI.Page
    Dim rs As New ReportDocument
 

    'Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
    '    Using msOur As New System.IO.MemoryStream()
    '        Using swOur As New System.IO.StreamWriter(msOur)
    '            Dim ourWriter As New HtmlTextWriter(swOur)
    '            MyBase.Render(ourWriter)
    '            ourWriter.Flush()
    '            msOur.Position = 0
    '            Using oReader As New System.IO.StreamReader(msOur)
    '                Dim sTxt As String = oReader.ReadToEnd()
    '                If Web.Configuration.WebConfigurationManager.AppSettings("AlwaysSSL").ToLower = "true" Then
    '                    sTxt = sTxt.Replace(Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString(), _
    '                    Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString().Replace("http://", "https://"))
    '                End If
    '                Response.Write(sTxt)
    '                oReader.Close()
    '            End Using
    '        End Using
    '    End Using
    'End Sub




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Try
        '    'rs.CacheDuration = 1
        '    ' rs.EnableCaching = False
        '    LoadReports(sender, e)
        '    Try
        '        'For showing print dialog on page load
        '        Dim btnToClick = GetPrintButton(Me.Page)
        '        If btnToClick IsNot Nothing Then
        '            Dim btn As New System.Web.UI.WebControls.ImageButton
        '            h_print.Value = btnToClick.ClientID
        '        End If
        '        Dim btnClicked As Object = PrinterFunctions.GetPostBackControl(Me.Page)

        '        If btnClicked IsNot Nothing Then
        '            Dim btnClickstatus As String = btnClicked.CommandName
        '            If btnClickstatus = "Export" Or btnClickstatus = "Print" Then
        '                h_print.Value = "completed"
        '            End If
        '        End If

        '    Catch ex As Exception
        '    End Try


        'Catch ex As Exception
        'End Try
    End Sub

    Public Function GetPrintButton(ByVal p_Controls As Control) As System.Web.UI.Control
        Dim ctrlStr As String = [String].Empty
        For Each ctl As Control In p_Controls.Controls
            If ctl.HasControls() Then
                Dim PrintControl As New Control
                PrintControl = GetPrintButton(ctl)
                If Not PrintControl Is Nothing Then
                    Return PrintControl
                End If
            End If
            ' handle ImageButton controls 
            If ctl.ToString.EndsWith(".x") OrElse ctl.ToString.EndsWith(".y") Then
                ctrlStr = ctl.ToString.Substring(0, ctl.ToString.Length - 2)
            End If
            If TypeOf ctl Is System.Web.UI.WebControls.ImageButton Then
                Response.Write(ctl.ID)
                Dim btn As New System.Web.UI.WebControls.ImageButton
                btn = ctl
                If btn.CommandName = "Print" Then
                    Return ctl
                End If
            End If
        Next
        Return Nothing
    End Function

    Sub LoadReports(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rptClass As New rptClass
        Try


            Dim iRpt As New DictionaryEntry
            Dim i As Integer
            Dim newWindow As String

            Try
                newWindow = Request.QueryString("newWindow")
            Catch ex As Exception

            End Try
            Dim rptStr As String = ""
            If Not newWindow Is Nothing Then
                rptStr = "rptClass" + newWindow
            Else
                rptStr = "rptClass"
            End If


            ' rptClass = Session("rptClass")
            rptClass = Session.Item(rptStr)

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues

            With rptClass




                'If Not Session("prevClass") Is Nothing And newWindow Is Nothing Then
                '    Session("rptClass") = Session("prevClass")
                '    rptClass = Session("prevClass")
                '    Session("prevClass") = Nothing
                '    Page_Load(sender, e)
                '    Exit Sub
                'End If

                ' rs.ReportDocument.Load(.reportPath)
                rs.Load(.reportPath)
                'rs.ReportDocument.DataSourceConnections(0).SetConnection(.crInstanceName, .crDatabase, .crUser, .crPassword) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
                'rs.ReportDocument.SetDatabaseLogon(.crUser, .crPassword, .crInstanceName, .crDatabase, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '
                'rs.ReportDocument.VerifyDatabase()

                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword


                SetDBLogonForSubreports(myConnectionInfo, rs, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs, .reportParameters)


                'If .subReportCount <> 0 Then
                '    For i = 0 To .subReportCount - 1
                '        rs.ReportDocument.Subreports(i).VerifyDatabase()
                '    Next
                'End If


                crParameterFieldDefinitions = rs.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If

                '  rs.ReportDocument.VerifyDatabase()


                If .selectionFormula <> "" Then
                    rs.RecordSelectionFormula = .selectionFormula
                End If


            End With
        Catch ex As Exception
        Finally
            crv.ReportSource = rs

        End Try
       

    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue
        'Dim crParameterFieldDefinitions As ParameterFieldDefinitions
        'Dim crParameterFieldLocation As ParameterFieldDefinition
        'Dim crParameterValues As ParameterValues
        'Dim iRpt As New DictionaryEntry

        ' Dim myTableLogonInfo As TableLogOnInfo
        ' myTableLogonInfo = New TableLogOnInfo
        'myTableLogonInfo.ConnectionInfo = myConnectionInfo

        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

        Next


        'crParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        'If reportParameters.Count <> 0 Then
        '    For Each iRpt In reportParameters
        '        Try
        '            crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
        '            crParameterValues = crParameterFieldLocation.CurrentValues
        '            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
        '            crParameterDiscreteValue.Value = iRpt.Value
        '            crParameterValues.Add(crParameterDiscreteValue)
        '            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
        '        Catch ex As Exception
        '        End Try
        '    Next
        'End If

        'myReportDocument.DataSourceConnections(0).SetConnection(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
        'myReportDocument.SetDatabaseLogon(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '

        myReportDocument.VerifyDatabase()


    End Sub
    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

                    Select Case subReportDocument.Name
                        Case "studphotos"
                            Dim dS As New dsImageRpt
                            Dim fs As New FileStream(Session("StudentPhotoPath"), System.IO.FileMode.Open, System.IO.FileAccess.Read)
                            Dim Image As Byte() = New Byte(fs.Length - 1) {}
                            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
                            fs.Close()
                            Dim dr As dsImageRpt.StudPhotoRow = dS.StudPhoto.NewStudPhotoRow
                            dr.Photo = Image
                            'Add the new row to the dataset
                            dS.StudPhoto.Rows.Add(dr)
                            subReportDocument.SetDataSource(dS)
                        Case "rptSubPhoto.rpt"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetPhotoToReport(subReportDocument, vrptClass.Photos)
                            End If
                        Case Else
                            SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    End Select

                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub

    Private Sub SetPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos)
        Dim arrphotos As ArrayList = vPhotos.IDs
        vPhotos = UpdatePhotoPath(vPhotos)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch
            Finally
                fs.Close()
            End Try
            Dim dr As dsImageRpt.DSPhotosRow = dS.DSPhotos.NewDSPhotosRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.DSPhotos.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub

    Function UpdatePhotoPath(ByVal vPhotos As OASISPhotos) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = Web.Configuration.WebConfigurationManager.AppSettings("StudentPhotoPath").ToString()  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStud_photoPath(vPhotos.IDs) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) orelse(dr("FILE_PATH") ="") Then
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + "/NOIMG/no_image.jpg"
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function

    Private Function GETStud_photoPath(ByVal arrSTU_IDs As ArrayList) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FILE_PATH,STU_ID FROM STUDENT_M where  STU_ID in(" & strStudIDs & ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Try
           
            LoadReports(sender, e)
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub crv_Unload(sender As Object, e As EventArgs) Handles crv.Unload
        Try

            Try
                For Each table As Table In rs.Database.Tables
                    table.Dispose()
                Next
                rs.Database.Dispose()
            Catch ex As Exception
                UtilityObj.Errorlog("crv_Unload--", ex.Message)
            End Try

            rs.Close()

            rs.Dispose()

            crv.Dispose()
            'GC.WaitForPendingFinalizers()
            'GC.Collect()


        Catch ex As Exception

        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload

        'crv.Dispose()
        'crv = Nothing
        ' rs.Dispose()
        ' rs = Nothing

    End Sub
End Class
