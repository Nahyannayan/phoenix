Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports System.Xml
Partial Class Reports_ASPX_Report_rptCostCenterAnalysis
    Inherits System.Web.UI.Page


    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = OASISConstants.Gemstitle

        txtToDate.Attributes.Add("ReadOnly", "Readonly")
        txtFromDate.Attributes.Add("ReadOnly", "Readonly")

        'checks whether the BSUnit is selected if it is selected h_BSUID.Value will be having
        'the IDs of the BS Unit. "FillBSUNames()" function will set the BSU names
       

        'If h_Memberids.Value <> Nothing And h_Memberids.Value <> "" And h_Memberids.Value <> "undefined" Then
        '    Bind_CostunitGrid(h_Memberids.Value)
        'End If

        If h_ACTIDs.Value <> Nothing And h_ACTIDs.Value <> "" And h_ACTIDs.Value <> "undefined" Then
            FillACTIDs(h_ACTIDs.Value)
            'h_BSUID.Value = ""
        End If 
        If Not IsPostBack Then
            BindType()
            imgPickOther.Visible = False
            imgPickcost.Visible = True
            'Bind_CostunitGrid()
            Session("liUserList") = New List(Of String)
            If (Request.QueryString("MainMnu_code") <> "") Then
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                ViewState("MainMnu_code") = "A450020"
            End If
            UsrBSUnits1.MenuCode = ViewState("MainMnu_code")
            Select Case ViewState("MainMnu_code").ToString
                Case OASISConstants.ReportCostcenterAnalysis
                    lblCaption.Text = "Cost Center Analysis"
                    'tr_CostCenter.Visible = False
            End Select
            txtToDate.Text = UtilityObj.GetDiplayDate()
            txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))

        End If

        If chkConsolidation.Checked Then
            chkGroupCurrency.Enabled = True
        Else
            chkGroupCurrency.Enabled = False
            chkGroupCurrency.Checked = True
        End If 

    End Sub



    Private Function GetDataSourceDocType() As Hashtable
        ' Type-0(By Account),-1(By Ctrl Account),-2(By SgrpCode),-3(By Grpcode)
        Dim ht As New Hashtable
        ht("0") = "By Account"
        ht("1") = "By Ctrl Account"
        ht("2") = "By SgrpCode"
        ht("3") = "By Grpcode"
        Return ht
    End Function


    Private Function GetDataSourceRptType() As Hashtable
        ' Type-0(By Account),-1(By Ctrl Account),-2(By SgrpCode),-3(By Grpcode)
        Dim ht As New Hashtable
        'ht("0") = "By Account"
        'ht("1") = "By Ctrl Account"
        'ht("2") = "By SgrpCode"
        'ht("3") = "By Grpcode"
        ht("6") = "By Income/Expense"
        ht("7") = "Expense Contribution by Account"
        ht("8") = "Income Contribution by Account"
        Return ht
    End Function


     


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        'If Session("liUserList") Is Nothing Then
        '    lblError.Text = "Please Pick atleast one cost object"
        '    Exit Sub
        'End If
        'If grdACTDetails.Rows.Count = 0 Then
        '    lblError.Text = "Please Pick atleast one one account"
        '    Exit Sub
        'End If

       
        txtFromDate.Text = Format("dd/MMM/yyyy", txtFromDate.Text)
        txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)

        Select Case ViewState("MainMnu_code").ToString
            Case OASISConstants.ReportCostcenterAnalysis
                RPTGetCostCenterAnalysis()
        End Select

    End Sub 


    Sub BindType()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT ECT_ID,ECT_DESCR FROM oasis..EMPCATEGORY_M union all SELECT 0, 'ALL STAFF' order by ECT_ID "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddCostcenter.DataSource = ds.Tables(0)
            ddCostcenter.DataTextField = "ECT_DESCR"
            ddCostcenter.DataValueField = "ECT_ID"
            ddCostcenter.DataBind()
            ddCostcenter.SelectedIndex = 0
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub 


    Private Sub RPTGetCostCenterAnalysis()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As SqlCommand

        If ddCostcenter.SelectedItem.Value <> "OTH" Then
            cmd = New SqlCommand("RPTGetCostCenterAnalysis", objConn)
        Else
            cmd = New SqlCommand("RPTSubLedgerReportOthers", objConn)
        End If

        cmd.CommandType = CommandType.StoredProcedure
        '[dbo].[RPTGetCostCenterAnalysis](@BSU_ID varchar(200), @DTTO datetime)

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 500)
        sqlpJHD_BSU_IDs.Value = UsrBSUnits1.GetSelectedNode("|")
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpCCS_ID As New SqlParameter("@CCS_ID", SqlDbType.VarChar, 20)
        sqlpCCS_ID.Value = ddCostcenter.SelectedItem.Value
        cmd.Parameters.Add(sqlpCCS_ID)

        Dim sqlpJHD_DOCTYPE As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        If Request.QueryString("costid") = "" Then
            sqlpJHD_DOCTYPE.Value = GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
            Session("ACT_IDs") = sqlpJHD_DOCTYPE.Value
        Else
            sqlpJHD_DOCTYPE.Value = Session("ACT_IDs")
        End If
        cmd.Parameters.Add(sqlpJHD_DOCTYPE)

        Dim sqlpCOST_IDs As New SqlParameter("@COST_IDs", SqlDbType.Xml)
        If Request.QueryString("costid") = "" Then
            If ddCostcenter.SelectedItem.Value = "OTH" Then
                sqlpCOST_IDs.Value = FillCOSTOthers()
            Else
                sqlpCOST_IDs.Value = FillCOST()
            End If

        Else
            sqlpCOST_IDs.Value = "<COST_DETAILS><COST_DETAIL><COST_ID>" & Request.QueryString("costid") & "</COST_ID></COST_DETAIL></COST_DETAILS>"
        End If
        cmd.Parameters.Add(sqlpCOST_IDs)


        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)
        'If ddCostcenter.SelectedItem.Value <> "OTH" Then
        '    Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        '    sqlpConsolidation.Value = chkConsolidation.Checked
        '    cmd.Parameters.Add(sqlpConsolidation)

        '    Dim sqlpGroupCurrency As New SqlParameter("@bShowinGrpCurrency", SqlDbType.Bit)
        '    sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        '    cmd.Parameters.Add(sqlpGroupCurrency) 
        'End If


        adpt.SelectCommand = cmd
        objConn.Close()
        objConn.Open()
        adpt.Fill(ds)
        If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            'params("Type") = ""
            'params("parent") = ""
            repSource.Parameter = params
            repSource.Command = cmd
            'If rbAccount.Checked = True Then
            '    repSource.ResourceName = "../RPT_Files/rptCostCenterAnalysis.rpt"
            'Else
            '    repSource.ResourceName = "../RPT_Files/rptCostCenterAnalysis.rpt"
            'End If
            'If ddCostcenter.SelectedItem.Value = "OTH" Then
            '    If rbAccount.Checked = True Then
            '        repSource.ResourceName = "../RPT_Files/rptCostCenterAnalysis.rpt"
            '    Else
            '        repSource.ResourceName = "../RPT_Files/rptCostCenterAnalysis.rpt"
            '    End If
            'End If
            Select Case ddAccountGroup.SelectedItem.Value
                Case "ACC"
                    repSource.ResourceName = "../RPT_Files/rptCostCenterAnalysis_Account.rpt"
                Case "CTRL"
                    repSource.ResourceName = "../RPT_Files/rptCostCenterAnalysis_CtrlAcc.rpt"
                Case "SUB"
                    repSource.ResourceName = "../RPT_Files/rptCostCenterAnalysis_SubGroup.rpt"
                Case "GRP"
                    repSource.ResourceName = "../RPT_Files/rptCostCenterAnalysis_Group.rpt"
            End Select
            repSource.ResourceName = "../RPT_Files/rptCostCenterAnalysis.rpt"

            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            ' Response.Redirect("rptviewer.aspx", True)
            ReportLoadSelection()
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(h_ACTIDs.Value)
        End If

    End Sub


    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Dim IDs As String() = ACTIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim str_Sql As String
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        grdACTDetails.DataSource = ds
        grdACTDetails.DataBind()

    End Sub





    Private Function FillCOST() As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Try
            BSUDetails = xmlDoc.CreateElement("COST_DETAILS")
            xmlDoc.AppendChild(BSUDetails)
            'Dim IDs As String() = h_Memberids.Value.Split("||")
            'Dim i As Integer

            For i As Integer = 0 To Session("liUserListCost").Count - 1
                If (Session("liUserListCost")(i).ToString <> "") Then
                    XMLEBSUDetail = xmlDoc.CreateElement("COST_DETAIL")
                    XMLEBSUID = xmlDoc.CreateElement("COST_ID")
                    XMLEBSUID.InnerText = Session("liUserListCost")(i).ToString()
                    XMLEBSUDetail.AppendChild(XMLEBSUID)
                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Function FillCOSTOthers() As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Try
            BSUDetails = xmlDoc.CreateElement("COST_DETAILS")
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = h_Memberids.Value.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If IDs(i) <> "" Then
                    XMLEBSUDetail = xmlDoc.CreateElement("COST_DETAIL")
                    XMLEBSUID = xmlDoc.CreateElement("COST_ID")
                    XMLEBSUID.InnerText = IDs(i)
                    XMLEBSUDetail.AppendChild(XMLEBSUID)
                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
            Next

            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
 
  


    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdACTDetails.PageIndexChanging
        grdACTDetails.PageIndex = e.NewPageIndex
        FillACTIDs(h_ACTIDs.Value)
    End Sub

    Protected Sub ddCostcenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddCostcenter.SelectedIndexChanged
        'Bind_CostunitGrid()
        If ddCostcenter.SelectedItem.Value = "OTH" Then
            imgPickOther.Visible = True
            imgPickcost.Visible = False
        Else
            imgPickOther.Visible = False
            imgPickcost.Visible = True
        End If
    End Sub 

    Protected Sub lnkbtngrdcOST(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            H_COSTIDS.Value = H_COSTIDS.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(H_COSTIDS.Value)
        End If
    End Sub  

End Class
