<%@ Page Language="VB" AutoEventWireup="false" CodeFile="OthercostcenterShow.aspx.vb" Inherits="Reports_ASPX_Report_OthercostcenterShow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
 <base target="_self" />
    <title>Other Costcenter</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
     
     <script language="javascript" type="text/javascript">
     function change_chk_state(src)
         {
               var chk_state=(src.checked);
               for(i=0; i<document.forms[0].elements.length; i++)
               {
                if (document.forms[0].elements[i].type=='checkbox')
                    {
                    document.forms[0].elements[i].checked=chk_state;
                    }
               }
          }     
        function client_OnTreeNodeChecked( )
        { 
                var obj = window.event.srcElement;
                var treeNodeFound = false;
                var checkedState;
                if (obj.tagName == "INPUT" && obj.type == "checkbox") {
//                //
//                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
//                change_chk_state(obj); }
//                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                    } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                    if (numTables >= 1)
                    {
                        for (i=0; i < numTables; i++)
                        {
                            if (tables[i] == obj)
                            {
                                treeNodeFound = true;
                                i++;
                                if (i == numTables)
                                    {
                                    return;
                                    }
                            }
                            if (treeNodeFound == true)
                            {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel)
                                {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                                }
                            else
                                {
                                return;
                                }
                            }
                        }
                    }
                }
        }
        function listen_window()
        {
        }
    </script>
</head>
<body onload="listen_window();">
    <form id="form2" runat="server">
    <br />
    <div >
    <table class ="BlueTable"  width ="85%" align="center" >
    <tr><td>
    <asp:Button ID ="btnDone" runat ="server" CssClass ="button" Text ="Done" Width ="100px"    />&nbsp;
    </td></tr>
    <tr class ="matters"><td>
    
    <asp:TreeView ID="tvCostcenter" runat="server" ShowCheckBoxes="All" >
                    <NodeStyle CssClass="treenode" />
                </asp:TreeView>
    </td></tr></table>
    <input id="h_SelectedId" runat="server" type="hidden" value="" />
    </div>
    </form>
</body>
</html>
