Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Partial Class Reports_ASPX_Report_rptPrepaymentSchedule
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ClientScript.RegisterStartupScript(Me.GetType(), _
        "script", "<script language='javascript'>  CheckOnPostback(); </script>")
       
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
           
        End If

       

    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
       
        PrepaymentSchedule()

    End Sub





    Private Sub PrepaymentSchedule()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MainDB").ConnectionString
        Dim cmd As New SqlCommand("Prepayment_Schedule")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@PRP_BSU_ID", SqlDbType.VarChar, 10)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)



        Dim sqlpSUBID As New SqlParameter("@PRP_SUB_ID", SqlDbType.VarChar, 10)
        sqlpSUBID.Value = Session("Sub_ID")
        cmd.Parameters.Add(sqlpSUBID)

        Dim sqlpFromDT As New SqlParameter("@PRD_DOCDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)




        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("ReportHead") = "Prepayment Schedule As on " & txtFromDate.Text.ToString()
        repSource.Parameter = params
        repSource.IncludeBSUImage = True
        repSource.Command = cmd



        repSource.ResourceName = "../RPT_Files/PrepaymentSchedule.rpt"
        Session("ReportSource") = repSource
        Response.Redirect("rptviewer.aspx", True)

    End Sub

    



    



    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    

End Class

