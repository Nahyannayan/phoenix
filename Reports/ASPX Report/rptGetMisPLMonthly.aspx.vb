Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_rptGetMisPLMonthly
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle

            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If

            Select Case ViewState("MainMnu_code").ToString
                Case "A753070" 'MIS Monthly
                    lblrptCaption.Text = "MIS Monthly"
                    chkExcludeSpecialAccounts.Visible = True
                Case "A753075" 'MIS Monthly
                    lblrptCaption.Text = "MIS Monthly - Budget"
                    chkExcludeSpecialAccounts.Visible = True
            End Select

            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ddlBSUnit.DataSource = UtilityObj.GetBusinessUnits(Session("sUsr_name"))
                ddlBSUnit.DataTextField = "BSU_NAME"
                ddlBSUnit.DataValueField = "BSU_ID"
                ddlBSUnit.DataBind()
                ddlBSUnit.SelectedValue = Session("sbsuid")
                txtFromDate.Text = UtilityObj.GetDiplayDate()

                txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

     Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            txtFromDate.Text = Format("dd/MMM/yyyy", txtFromDate.Text)
            Session("Fdate") = txtFromDate.Text
            Select Case ViewState("MainMnu_code").ToString
                Case "A753070"  'MIS Monthly 
                    RPTGetMISPnL_Abbraj()
                Case "A753075"  'MIS Monthly 
                    RPTGetMISPnL_Abbraj_Budget()
            End Select
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
 
    Private Sub RPTGetMISPnL_Abbraj()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim cmd As New SqlCommand("RPTGetMISPnL_Abbraj", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = ddlBSUnit.SelectedItem.Value
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", SqlDbType.Bit)
        sqlpBSpecial.Value = chkExcludeSpecialAccounts.Checked
        cmd.Parameters.Add(sqlpBSpecial)

        Dim param As SqlParameterCollection = cmd.Parameters
        objConn.Close()
        objConn.Open()
        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("toDate") = txtFromDate.Text
        params("FYR_FROMDT") = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtFromDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        ConnectionManger.GetOASISConnectionString)
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        repSource.Parameter = params
        repSource.Command = cmd
        Select Case ViewState("MainMnu_code").ToString
            Case "A753070" '"BankBook"
                repSource.ResourceName = "../RPT_Files/rptMisPLAbbrajrpt.rpt"
                If chkExcludeSpecialAccounts.Checked Then
                    params("ReportCaption") = ddlBSUnit.SelectedItem.Text
                Else
                    params("ReportCaption") = ddlBSUnit.SelectedItem.Text
                End If
        End Select
        Session("ReportSource") = repSource
        objConn.Close()
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("rptviewer.aspx?isExport=true", True)
        Else
            Response.Redirect("rptviewer.aspx", True)
        End If
    End Sub

    Private Sub RPTGetMISPnL_Abbraj_Budget()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim cmd As New SqlCommand("RPTGetMISPnL_Abbraj_Budget", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = ddlBSUnit.SelectedItem.Value
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", SqlDbType.Bit)
        sqlpBSpecial.Value = chkExcludeSpecialAccounts.Checked
        cmd.Parameters.Add(sqlpBSpecial)

        Dim param As SqlParameterCollection = cmd.Parameters
        objConn.Close()
        objConn.Open()
        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("toDate") = txtFromDate.Text
        params("FYR_FROMDT") = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtFromDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        ConnectionManger.GetOASISConnectionString)
        params("EXPRESSED_IN") = ddExpressedin.SelectedItem.Value
        repSource.Parameter = params
        repSource.Command = cmd
        Select Case ViewState("MainMnu_code").ToString
            Case "A753075" '"BankBook"
                repSource.ResourceName = "../RPT_Files/rptMisPLAbbrajrpt_Budget.rpt"
                If chkExcludeSpecialAccounts.Checked Then
                    params("ReportCaption") = ddlBSUnit.SelectedItem.Text
                Else
                    params("ReportCaption") = ddlBSUnit.SelectedItem.Text
                End If
        End Select
        Session("ReportSource") = repSource
        objConn.Close()
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("rptviewer.aspx?isExport=true", True)
        Else
            Response.Redirect("rptviewer.aspx", True)
        End If
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub 
  
End Class