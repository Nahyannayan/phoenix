<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptBankBook.aspx.vb" Inherits="Reports_ASPX_Report_BankBook" Title="Untitled Page" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../Accounts/calendar.aspx", "", sFeatures);

            if (result != '' && result != undefined) {
                switch (txtControl) {
                    case 0:
                        document.getElementById('<%=txtfromDate.ClientID %>').value = result;
                        break;
                    case 1:
                        document.getElementById('<%=txtToDate.ClientID %>').value = result;
                        break;
                }
            }
            return false;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function GetBSUName() {

            var NameandCode;
            var result;
            var type;
            type = document.getElementById('<%=h_Mode.ClientID %>').value;
            if (type == "party" || type == "general") {
                result = radopen("../../Accounts/selBussinessUnit.aspx", "pop_up");
            }
            else {
                result = radopen("../../Accounts/selBussinessUnit.aspx?multiSelect=false", "pop_up");
            }

            <%-- if (result != '' && result != undefined) {
                document.getElementById('<%=h_BSUID.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_BSUID.ClientID %>').value = arg.NameandCode;
                document.forms[0].submit();
                //__doPostBack('<%= h_BSUID.ClientID%>', 'ValueChanged');
            }
        }


        function GetAccounts() {

            var NameandCode;
            var result;
            var ActType;
            var mode = document.getElementById('<%=h_Mode.ClientID %>').value;

            if (mode == 'bankmis') {
                result = radopen("../../Accounts/accjvshowaccount.aspx?bankmis=y&bankcash=b&multiSelect=true", "pop_up2")
            }
            else if (mode == 'bank') {
                result = radopen("../../Accounts/accjvshowaccount.aspx?bankcash=b&multiSelect=true", "pop_up2")
            }
            else if (mode == 'cash') {
                result = radopen("../../Accounts/accjvshowaccount.aspx?bankcash=c&multiSelect=true&forrpt=1", "pop_up2")
            }
            else if (mode == 'party') {
                ActType = document.getElementById('<%=h_ACTTYPE.ClientID %>').value;
                if (ActType == "s") {
                    result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=s&forrpt=1", "pop_up2")
                }

                if (ActType == "C") {
                    result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=C&forrpt=1", "pop_up2")
                }
            }
            else if (mode == 'general') {
                result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=g&forrpt=1", "pop_up2")
            }
            else //if(mode == 'general')
            {
                result = radopen("../../Accounts/accjvshowaccount.aspx?multiSelect=true&forrpt=1", "pop_up2")
            }

  <%--  if (result != '' && result != undefined) {
        document.getElementById('<%=h_ACTIDs.ClientID %>').value = result; //NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }--%>
        }

        function OnClientClose2(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = arg.NameandCode;
                document.forms[0].submit();
                //__doPostBack('<%= h_ACTIDs.ClientID%>', 'ValueChanged');
            }
        }

        function GetDocTypes() {

            var NameandCode;
            var result;
            pMode = "FINDOCTYPE"
            url = "../../common/PopupSelect.aspx?id=" + pMode + "&MultiSelect=1";
            result = radopen(url, "pop_up3");
           <%-- if (result == '' || result == undefined) return false;
            document.getElementById("<%=h_DOCIDs.ClientID %>").value = result;
            document.forms[0].submit();--%>

        }

        function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_DOCIDs.ClientID%>').value = arg.NameandCode;
                document.forms[0].submit();
                <%--' __doPostBack('<%= h_DOCIDs.ClientID%>', 'ValueChanged');--%>
            }
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator  mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Label"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" style="width: 100%;" cellpadding="0" cellspacing="0">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>

                <table align="center" cellpadding="5" cellspacing="0" style="width: 100%;" runat="server" id="tblMain">

                    <tr runat="server" id="trBSUMulti">
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <div class="checkbox-list">
                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr id="trBSUSingle" runat="server">
                        <td align="left" width="20%" valign="top"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <asp:DropDownList ID="ddlBSUnit" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">From Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate" EnableViewState="False" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%"><span class="field-label">To Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>&nbsp;
                <asp:RegularExpressionValidator ID="revToDate" runat="server" ControlToValidate="txtToDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="dayBook">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr id="TrMultiYear" runat="server" visible="false">
                        <td align="left" width="20%">
                            <asp:Label ID="lblMultiYear" runat="server" Text="Select Report" CssClass="field-label"></asp:Label></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReport" runat="server" Visible="false"></asp:DropDownList></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="Trbank" runat="server">
                        <td align="left" width="20%">
                            <asp:Label ID="lblBankCash" runat="server" Text="Bank" CssClass="field-label"></asp:Label></td>

                        <td align="left" width="30%">
                            <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the ACT ID you want to Add to the Search)" Width="377px"></asp:Label><br>
                            <asp:TextBox ID="txtBankNames" runat="server" Height="18px"></asp:TextBox>
                            <asp:LinkButton ID="lblAddActID" runat="server">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetAccounts(); return false;" /><br />
                        </td>
                        <td align="left" colspan="2">
                            <asp:GridView ID="grdACTDetails" Width="100%" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="ACT ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ACT_Name" HeaderText="ACT Name" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="TrDOC" visible="false" runat="server">
                        <td align="left" width="20%">
                            <asp:Label ID="lblDOC" runat="server" Text="Document Type" CssClass="field-label"></asp:Label></td>

                        <td align="left" width="30%">
                            <asp:Label ID="lblDOCIDCaption" runat="server" Text=""></asp:Label>
                            <asp:TextBox ID="txtDOCNames" runat="server"></asp:TextBox>
                            <asp:LinkButton ID="lblAddDocID" runat="server">Add</asp:LinkButton>
                            <asp:ImageButton ID="imgDocSel" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetDocTypes(); return false;" /><br />
                        </td>
                        <td align="left" colspan="2">
                            <asp:GridView ID="grdDOCDetails" runat="server" Width="100%" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="DOC ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDOCID" runat="server" Text='<%# Bind("DOC_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DOC_Name" HeaderText="ACT Name" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtngrdDOCDelete" runat="server" OnClick="lnkbtngrdDOCDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"></td>
                        <td align="left" colspan="3">
                            <asp:CheckBox ID="chkGroupCurrency" runat="server" Text="View in Group Currency" CssClass="field-label" />
                            <asp:CheckBox ID="chkConsolidation" runat="server" Text="Consolidation" CssClass="field-label" />
                            <asp:CheckBox ID="chkSummary" runat="server" Text="View Summary" Visible="False" CssClass="field-label" /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="dayBook" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_ACTIDs" runat="server" />
                <asp:HiddenField ID="h_DOCIDs" runat="server" />
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_MAINMENU" runat="server" />
                <asp:HiddenField ID="h_Mode" runat="server" />
                <asp:HiddenField ID="h_ACTTYPE" runat="server"></asp:HiddenField>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>
