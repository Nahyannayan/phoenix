Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class Reports_ASPX_Report_rptTrialBalance
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared MainMnu_code As String
    Shared bconsolidation As Boolean

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = OASISConstants.Gemstitle
        If Request.QueryString("MainMnu_code") = "" Then
            Response.Redirect("..\..\noAccess.aspx")
        End If

        'Add the readOnly attribute to the Date textBox So that the Text property value is accessible in the form
        'It is not giving the values if you make the ReadOnly property to TRUE
        'So you have to do this in page_Load()
        'txtToDate.Attributes.Add("ReadOnly", "Readonly")
        'txtFromDate.Attributes.Add("ReadOnly", "Readonly")

        'checks whether the BSUnit is selected if it is selected h_BSUID.Value will be having
        'the IDs of the BS Unit. "FillBSUNames()" function will set the BSU names
        If h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
            h_BSUID.Value = Session("sBsuid")
        End If

        If Not IsPostBack Then
            UtilityObj.NoOpen(Me.Header)
            trACT_ID.Visible = False
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Select Case MainMnu_code
                Case "A350078" 'Multiyear Columnar
                    trGroupType.Visible = False
                    trACT_ID.Visible = True
                    chkGroupCurrency.Visible = False
                    chkConsolidation.Text = "Ignore year end P&L entry"
                    lblCaption.Text = "Multiyear Columnar Report"
                    h_Mode.Value = "general"
                Case "A350005" 'Trial Balance
                    'Bind the Data with the DropDown
                    trACT_ID.Visible = False
                    cmbGroupType.DataSource = GetDataSourceDocType()
                    cmbGroupType.DataValueField = "Key"
                    cmbGroupType.DataTextField = "Value"
                    cmbGroupType.DataBind()
                    lblCaption.Text = "Trial Balance Report"
                    chkMultiYear.Visible = True
                    h_Mode.Value = "general"
                Case "A350006" 'Trial Balance - DAX COA
                    'Bind the Data with the DropDown
                    trACT_ID.Visible = False
                    trGroupType.Visible = False
                    lblCaption.Text = "Trial Balance Report - DAX COA"
                    chkMultiYear.Visible = False
                    h_Mode.Value = "general"
                    chkConsolidation.Text = "Discrepancy report"
                Case "A350010" 'P\L Statements
                    lblCaption.Text = "Income Statements"
                    trGroupType.Visible = False
                    chkConsolidation.Visible = False
                    chkMultiYear.Visible = False
                Case "A350015" 'P\L Schedule
                    lblCaption.Text = "Income Statement Schedule"
                    trGroupType.Visible = False
                    chkConsolidation.Visible = False
                    chkMultiYear.Visible = True
                Case "A350020" 'P\L Schedule columnar
                    lblCaption.Text = "Income Statement Schedule Columnar"
                    trGroupType.Visible = False
                    chkConsolidation.Visible = False
                Case "A650005"
                    lblCaption.Text = "Expense\Income"
                    cmbGroupType.DataSource = GetDataSourceRptType()
                    cmbGroupType.DataValueField = "Key"
                    cmbGroupType.DataTextField = "Value"
                    cmbGroupType.DataBind()
                Case "A650010"
                    lblCaption.Text = "Expense Chart"
                    trGroupType.Visible = False
                    trChkBoxes.Visible = False
                    trBSUName.Visible = False
                    'cmbGroupType.DataSource = GetDataSourceRptType()
                    'cmbGroupType.DataValueField = "Key"
                    'cmbGroupType.DataTextField = "Value"
                    'cmbGroupType.DataBind()
                Case "A350030"
                    lblCaption.Text = "Cash Flow"
                    trGroupType.Visible = False
                    chkConsolidation.Visible = False
                    chkMultiYear.Visible = False
                    bconsolidation = True
                    h_Mode.Value = "cashFlow"
                Case "A450030"      'Selective Group TB
                    cmbGroupType.DataSource = GetDataSourceDocType()
                    cmbGroupType.DataValueField = "Key"
                    cmbGroupType.DataTextField = "Value"
                    cmbGroupType.DataBind()
                    trACT_ID.Visible = True
                    h_Mode.Value = "general"
                    lblCaption.Text = "Selective Group TB"
                Case "A350055"
                    lblCaption.Text = "Cash/Bank Status"
                    trACT_ID.Style("display") = "none"
                    trChkBoxes.Style("display") = "none"
                    trGroupType.Style("display") = "none"
                    TdFd1.Style("display") = "none"
                    'TdFd2.Style("display") = "none"
                    TdFd3.Style("display") = "none"

                Case "A350077"
                    lblCaption.Text = "Cash/Bank Position"
                    trACT_ID.Style("display") = "none"
                    trChkBoxes.Style("display") = "none"
                    trGroupType.Style("display") = "none"
                    TdFd1.Style("display") = "none"
                    'TdFd2.Style("display") = "none"
                    TdFd3.Style("display") = "none"
            End Select
            txtToDate.Text = UtilityObj.GetDiplayDate()

            txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(txtToDate.Text).AddMonths(-2))

            txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
            txtToDate.Attributes.Add("onBlur", "checkdate(this)")
        End If

        'If bconsolidation Then
        '    chkGroupCurrency.Enabled = True
        'Else
        '    chkGroupCurrency.Enabled = False
        '    chkGroupCurrency.Checked = True
        'End If
        If h_ACTIDs.Value <> "" Then
            FillACTIDs(h_ACTIDs.Value)
        End If

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function GetDataSourceDocType() As Hashtable
        ' Type-0(By Account),-1(By Ctrl Account),-2(By SgrpCode),-3(By Grpcode)
        Dim ht As New Hashtable
        ht("0") = "By Account"
        ht("1") = "By Ctrl Account"
        ht("2") = "By SgrpCode"
        ht("3") = "By Grpcode"
        Return ht
    End Function

    Private Function GetDataSourceRptType() As Hashtable
        ' Type-0(By Account),-1(By Ctrl Account),-2(By SgrpCode),-3(By Grpcode)
        Dim ht As New Hashtable
        'ht("0") = "By Account"
        'ht("1") = "By Ctrl Account"
        'ht("2") = "By SgrpCode"
        'ht("3") = "By Grpcode"
        ht("6") = "By Income/Expense"
        ht("7") = "Expense Contribution by Account"
        ht("8") = "Income Contribution by Account"
        Return ht
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        Dim strXMLBSUNames As String
        'Generate the XML in the form <BSU_DETAILS><BSU_ID>IDhere</BSU_ID></BSU_DETAILS>
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        strXMLBSUNames = GenerateBSUXML(h_BSUID.Value) 'txtBSUNames.Text)
        txtfromDate.Text = Format("dd/MMM/yyyy", txtfromDate.Text)
        txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        'ViewState("fromDate") = txtFromDate.Text
        'ViewState("toDate") = txtToDate.Text
        'ViewState("selBSUIDs") = h_BSUID.Value
        Session("FDate") = txtFromDate.Text
        Session("TDate") = txtToDate.Text
        Session("selBSUID") = h_BSUID.Value
        Session("GrpType") = cmbGroupType.SelectedValue
        Session("selACTIDs") = h_ACTIDs.Value
        Select Case MainMnu_code
            Case "A350005", "A350006", "A450030" 'Trial Balance
                GenerateTrialbalanceReport(strXMLBSUNames)
            Case "A350010" 'P\L Statements
                GeneratePLStatement(strXMLBSUNames)
            Case "A350015" 'P\L Schedule
                GeneratePLSchedule(strXMLBSUNames)
            Case "A350020" 'P\L Schedule Columnar
                GeneratePLScheduleCTB(strXMLBSUNames)
            Case "A650005" 'P\L Statements
                GenerateExpenseIncomeChart(strXMLBSUNames)
            Case "A650010"
                GenerateExpenseChart()
            Case "A350030"
                GenerateCashFlow(strXMLBSUNames)
            Case "A350055"
                CashBankStatus()
            Case "A350077"
                CashBankPosition()
            Case "A350078"
                MultiYearColumnar()
        End Select

    End Sub

    Private Sub GenerateCashFlow(ByVal strXMLBSUNames As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTGetCashflowDBP", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.VarChar, 200)
        sqlpJHD_BSU_IDs.Value = UtilityObj.GetBSUnitWithSeperator(UsrBSUnits1.GetSelectedNode, "|")
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        'Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        'sqlpJHD_FYEAR.Value = Session("financialyear")
        'cmd.Parameters.Add(sqlpJHD_FYEAR)

        'Dim sqlpJHD_DOCTYPE As New SqlParameter("@Type", SqlDbType.VarChar, 20)
        'sqlpJHD_DOCTYPE.Value = cmbGroupType.SelectedItem.Value
        'cmd.Parameters.Add(sqlpJHD_DOCTYPE)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpGroupCurrency As New SqlParameter("@bGrpCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        sqlpConsolidation.Value = False
        cmd.Parameters.Add(sqlpConsolidation)

        'adpt.SelectCommand = cmd
        'objConn.Close()
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            params("ReportHeading") = "CASH FLOW REPORT"
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd

            Dim repSourceSubRep() As MyReportClass
            repSourceSubRep = repSource.SubReport
            Dim subReportLength As Integer = 1
            If repSource.SubReport Is Nothing Then
                ReDim repSourceSubRep(1)
            Else
                subReportLength = repSourceSubRep.Length + 1
                ReDim Preserve repSourceSubRep(subReportLength)
            End If
            repSource.SubReport = repSourceSubRep

            repSourceSubRep(subReportLength - 1) = New MyReportClass
            Dim cmdRecieptInfo As New SqlCommand("RptLedgerSummaryForCashflow", objConn)
            cmdRecieptInfo.CommandType = CommandType.StoredProcedure

            Dim sqlpsubBSU_IDs As New SqlParameter("@BSUID", SqlDbType.VarChar, 200)
            sqlpsubBSU_IDs.Value = UtilityObj.GetBSUnitWithSeperator(UsrBSUnits1.GetSelectedNode, "|")
            cmdRecieptInfo.Parameters.Add(sqlpsubBSU_IDs)

            Dim sqlpsubDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
            sqlpsubDTTO.Value = txtToDate.Text
            cmdRecieptInfo.Parameters.Add(sqlpsubDTTO)

            Dim sqlpsubTyp As New SqlParameter("@Typ", SqlDbType.VarChar, 2)
            sqlpsubTyp.Value = "R"
            cmdRecieptInfo.Parameters.Add(sqlpsubTyp)
            repSourceSubRep(subReportLength - 1).Command = cmdRecieptInfo

            Dim cmdPayment As SqlCommand = cmdRecieptInfo.Clone
            cmdPayment.Parameters(2).Value = "P"
            repSourceSubRep(subReportLength) = New MyReportClass
            repSourceSubRep(subReportLength).Command = cmdPayment

            repSource.SubReport = repSourceSubRep

            repSource.ResourceName = "../RPT_Files/rptCashFlow.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub
    Private Sub CashBankPosition()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("[ADHOC].[GetCashbankConsolidation]", objConn)
        cmd.CommandType = CommandType.StoredProcedure



        Dim sqlpFROMDOCDT As New SqlParameter("@DT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpsubBSU_IDs As New SqlParameter("@BSU_IDS", SqlDbType.VarChar, 200)
        sqlpsubBSU_IDs.Value = UtilityObj.GetBSUnitWithSeperator(UsrBSUnits1.GetSelectedNode, "|")
        cmd.Parameters.Add(sqlpsubBSU_IDs)

        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("ReportHeading") = "Cash / Bank Position As On :" & txtToDate.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/CashbankConsolidation.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub
    Private Sub CashBankStatus()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("[ADHOC].[GetCashbankConsolidation]", objConn)
        cmd.CommandType = CommandType.StoredProcedure



        Dim sqlpFROMDOCDT As New SqlParameter("@DT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpsubBSU_IDs As New SqlParameter("@BSU_IDS", SqlDbType.VarChar, 200)
        sqlpsubBSU_IDs.Value = UtilityObj.GetBSUnitWithSeperator(UsrBSUnits1.GetSelectedNode, "|")
        cmd.Parameters.Add(sqlpsubBSU_IDs)

        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("ReportHeading") = "Cash / Bank Status As On :" & txtToDate.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/ConsolidateCashBankBalance.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub
    Private Sub GenerateExpenseChart()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTExpenseChart", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        'Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.Xml)
        'sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        'cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        'Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        'sqlpJHD_FYEAR.Value = Session("financialyear")
        'cmd.Parameters.Add(sqlpJHD_FYEAR)

        'Dim sqlpJHD_DOCTYPE As New SqlParameter("@Type", SqlDbType.VarChar, 20)
        'sqlpJHD_DOCTYPE.Value = cmbGroupType.SelectedItem.Value
        'cmd.Parameters.Add(sqlpJHD_DOCTYPE)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@TODOCDT", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        'Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        'sqlpConsolidation.Value = chkConsolidation.Checked
        'cmd.Parameters.Add(sqlpConsolidation)

        'Dim sqlpGroupCurrency As New SqlParameter("@bShowinGrpCurrency", SqlDbType.Bit)
        'sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        'cmd.Parameters.Add(sqlpGroupCurrency)

        'adpt.SelectCommand = cmd
        'objConn.Close()
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/chrtExpense.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Private Sub GenerateExpenseIncomeChart(ByVal strXMLBSUNames As String)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTGETTPnLDATA", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpJHD_DOCTYPE As New SqlParameter("@Type", SqlDbType.VarChar, 2)
        sqlpJHD_DOCTYPE.Value = cmbGroupType.SelectedItem.Value
        cmd.Parameters.Add(sqlpJHD_DOCTYPE)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpID As New SqlParameter("@ID", SqlDbType.VarChar)
        sqlpID.Value = ""
        cmd.Parameters.Add(sqlpID)

        Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        sqlpConsolidation.Value = bconsolidation
        cmd.Parameters.Add(sqlpConsolidation)

        Dim sqlpGroupCurrency As New SqlParameter("@bShowinGrpCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        'adpt.SelectCommand = cmd
        'objConn.Close()
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            params("rptCaption") = GetReportCaption(cmbGroupType.SelectedItem.Value)
            'params("bConsolidation") = bconsolidation
            'If bconsolidation Then
            '    params("BSUNames") = txtBSUNames.Text.Replace("||", ",")
            'Else
            '    params("BSUNames") = ""
            'End If
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/chrtExpense_Income.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Private Function GetReportCaption(ByVal type As Integer) As String
        Select Case type
            Case 6
                Return "P\L Contribution"
            Case 7
                Return "Expense Contribution"
            Case 8
                Return "Income Contribution"
            Case Else
                Return ""
        End Select
    End Function

    Private Sub GeneratePLStatement(ByVal strXMLBSUNames As String)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTRETURNFINALACCOUNTS", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        'Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        'sqlpJHD_FYEAR.Value = Session("financialyear")
        'cmd.Parameters.Add(sqlpJHD_FYEAR)

        Dim sqlpJHD_DOCTYPE As New SqlParameter("@Typ", SqlDbType.VarChar, 2)
        sqlpJHD_DOCTYPE.Value = "P"
        cmd.Parameters.Add(sqlpJHD_DOCTYPE)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        sqlpConsolidation.Value = bconsolidation
        cmd.Parameters.Add(sqlpConsolidation)

        Dim sqlpGroupCurrency As New SqlParameter("@bGrpCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        'adpt.SelectCommand = cmd
        'objConn.Close()
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = Format("dd\MMM\yyyy", CDate(txtFromDate.Text))
            params("ToDate") = Format("dd\MMM\yyyy", CDate(txtToDate.Text))
            params("bConsolidation") = bconsolidation
            params("reportCaption") = "Income Statements"
            If bconsolidation Then
                'params("BSUNames") = txtBSUNames.Text.Replace("||", ",")
            Else
                params("BSUNames") = ""
            End If
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptPLStatement.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Private Sub GeneratePLSchedule(ByVal strXMLBSUNames As String)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTRETURNFINALACCOUNTSSCHEDULE", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        'Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        'sqlpJHD_FYEAR.Value = Session("financialyear")
        'cmd.Parameters.Add(sqlpJHD_FYEAR)

        Dim sqlpJHD_DOCTYPE As New SqlParameter("@Typ", SqlDbType.VarChar, 2)
        sqlpJHD_DOCTYPE.Value = "P"
        cmd.Parameters.Add(sqlpJHD_DOCTYPE)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        sqlpConsolidation.Value = bconsolidation
        cmd.Parameters.Add(sqlpConsolidation)

        Dim sqlpGroupCurrency As New SqlParameter("@bGrpCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        Dim sqlpMultiYear As New SqlParameter("@bMultiYear", SqlDbType.Bit)
        sqlpMultiYear.Value = chkMultiYear.Checked
        cmd.Parameters.Add(sqlpMultiYear)

        'adpt.SelectCommand = cmd
        'objConn.Close()
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            params("bConsolidation") = bconsolidation
            params("reportCaption") = "Income Statement Schedule"
            If bconsolidation Then
                'params("BSUNames") = txtBSUNames.Text.Replace("||", ",")
            Else
                params("BSUNames") = ""
            End If
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptPLSchedule.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Private Sub GeneratePLScheduleCTB(ByVal strXMLBSUNames As String)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTRETURNFINALACCOUNTSSCHEDULECTB", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        'Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        'sqlpJHD_FYEAR.Value = Session("financialyear")
        'cmd.Parameters.Add(sqlpJHD_FYEAR)

        Dim sqlpJHD_DOCTYPE As New SqlParameter("@Typ", SqlDbType.VarChar, 2)
        sqlpJHD_DOCTYPE.Value = "P"
        cmd.Parameters.Add(sqlpJHD_DOCTYPE)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        sqlpConsolidation.Value = bconsolidation
        cmd.Parameters.Add(sqlpConsolidation)

        Dim sqlpGroupCurrency As New SqlParameter("@bGrpCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        Dim sqlpMultiYear As New SqlParameter("@bMultiYear", SqlDbType.Bit)
        sqlpMultiYear.Value = chkMultiYear.Checked
        cmd.Parameters.Add(sqlpMultiYear)

        'adpt.SelectCommand = cmd
        'objConn.Close()
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            params("bConsolidation") = bconsolidation
            params("reportCaption") = "Income Statement Schedule"
            If bconsolidation Then
                'params("BSUNames") = txtBSUNames.Text.Replace("||", ",")
            Else
                params("BSUNames") = ""
            End If
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptPLSchedule.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub

    Private Sub GenerateTrialbalanceReport(ByVal strXMLBSUNames As String)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("RPTGETTRIALBALANCE", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        'Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        'sqlpJHD_FYEAR.Value = Session("financialyear")
        'cmd.Parameters.Add(sqlpJHD_FYEAR)

        If h_ACTIDs.Value <> "" Then
            Dim sqlpJHD_ACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
            sqlpJHD_ACT_IDs.Value = UtilityObj.GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
            cmd.Parameters.Add(sqlpJHD_ACT_IDs)
        End If

        If MainMnu_code = "A350006" Then
            Dim sqlpJHD_DOCTYPE As New SqlParameter("@Type", SqlDbType.VarChar, 20)
            sqlpJHD_DOCTYPE.Value = 4
            cmd.Parameters.Add(sqlpJHD_DOCTYPE)
            Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
            sqlpConsolidation.Value = chkConsolidation.Checked
            cmd.Parameters.Add(sqlpConsolidation)
        Else
            Dim sqlpJHD_DOCTYPE As New SqlParameter("@Type", SqlDbType.VarChar, 20)
            sqlpJHD_DOCTYPE.Value = cmbGroupType.SelectedItem.Value
            cmd.Parameters.Add(sqlpJHD_DOCTYPE)
            Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
            sqlpConsolidation.Value = chkConsolidation.Checked
            cmd.Parameters.Add(sqlpConsolidation)
            Dim sqlpMultiYear As New SqlParameter("@bMultiYear", SqlDbType.Bit)
            sqlpMultiYear.Value = chkMultiYear.Checked
            cmd.Parameters.Add(sqlpMultiYear)
        End If

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpGroupCurrency As New SqlParameter("@bShowinGrpCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = chkGroupCurrency.Checked
        cmd.Parameters.Add(sqlpGroupCurrency)

        'adpt.SelectCommand = cmd
        'objConn.Close()
        'objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Session("BSUNames") = strXMLBSUNames
            Session("rptFromDate") = txtFromDate.Text
            Session("rptToDate") = txtToDate.Text
            Session("rptConsolidation") = chkConsolidation.Checked
            Session("rptGroupCurrency") = chkGroupCurrency.Checked
            Session("rptMultiyear") = chkMultiYear.Checked

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("FromDate") = txtFromDate.Text
            params("ToDate") = txtToDate.Text
            If MainMnu_code = "A350006" Then
                params("Type") = 4
                params("RPT_CAPTION") = "Trial Balance - DAX COA"
            Else
                params("Type") = cmbGroupType.SelectedItem.Value
                params("RPT_CAPTION") = "Trial Balance"
            End If
            params("parent") = ""
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            If MainMnu_code = "A350006" Then
                repSource.ResourceName = "../RPT_Files/rptTrailBalanceDAX.rpt"
            Else
                repSource.ResourceName = "../RPT_Files/rptTrailBalance.rpt"
            End If
            If ChkExclude.Checked Then
                params("RPT_CAPTION") = "Trial Balance ( Exclude Revenue Accounts Opening ) "
                repSource.ResourceName = "../RPT_Files/rptTrailBalanceExcludeOpening.rpt"
            End If
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                'Response.Redirect("rptviewer.aspx?isExport=true", True)
                ReportLoadSelection_Export()
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub
    'Generates the XML for BSUnit
    Private Function GenerateBSUXML(ByVal BSUIDs As String) As String
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Try
            BSUDetails = xmlDoc.CreateElement("BSU_DETAILS")
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                XMLEBSUDetail = xmlDoc.CreateElement("BSU_DETAIL")
                XMLEBSUID = xmlDoc.CreateElement("BSU_ID")
                XMLEBSUID.InnerText = IDs(i)
                XMLEBSUDetail.AppendChild(XMLEBSUID)
                xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
                'txtFromDate.Text = ViewState("fromDate")
                'txtToDate.Text = ViewState("toDate")
                'UsrBSUnits1.SetSelectedNodes(ViewState("selBSUIDs"))
                txtFromDate.Text = Session("FDate")
                txtToDate.Text = Session("TDate")
                UsrBSUnits1.SetSelectedNodes(Session("selBSUID"))
                cmbGroupType.SelectedValue = Session("GrpType")
                h_ACTIDs.Value = Session("selACTIDs")
                FillACTIDs(Session("selACTIDs"))

            Else
                lblError.Text = ""
            End If
        End If
    End Sub

    Protected Sub lblAddActID_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'If h_Mode.Value = "party" Then
        h_ACTIDs.Value += "||" + txtBankNames.Text.Replace(",", "||")
        'ElseIf FillACTIDs(txtBSUName.Text) Then
        h_ACTIDs.Value = txtBankNames.Text
        'End If
        FillACTIDs(h_ACTIDs.Value)
    End Sub

    Protected Sub lnkbtngrdACTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblACTID As New Label
        lblACTID = TryCast(sender.FindControl("lblACTID"), Label)
        If Not lblACTID Is Nothing Then
            h_ACTIDs.Value = h_ACTIDs.Value.Replace(lblACTID.Text, "").Replace("||||", "||")
            grdACTDetails.PageIndex = grdACTDetails.PageIndex
            FillACTIDs(h_ACTIDs.Value)
        End If

    End Sub

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdACTDetails.PageIndexChanging
        grdACTDetails.PageIndex = e.NewPageIndex
        FillACTIDs(h_ACTIDs.Value)
    End Sub

    Private Sub FillACTIDs(ByVal ACTIDs As String)
        Try
            Dim IDs As String() = ACTIDs.Split("||")
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
            Dim str_Sql As String
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            str_Sql = "SELECT ACT_NAME, ACT_ID FROM ACCOUNTS_M WHERE ACT_ID IN(" + condition + ")"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            grdACTDetails.DataSource = ds
            grdACTDetails.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub MultiYearColumnar()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("rptMultiYearColumnar", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpConsolidation As New SqlParameter("@multiyear", SqlDbType.Int)
        sqlpConsolidation.Value = IIf(chkConsolidation.Checked, 1, 0)
        cmd.Parameters.Add(sqlpConsolidation)

        If h_ACTIDs.Value <> "" Then
            Dim sqlpJHD_ACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
            sqlpJHD_ACT_IDs.Value = UtilityObj.GenerateXML(h_ACTIDs.Value, XMLType.ACTName)
            cmd.Parameters.Add(sqlpJHD_ACT_IDs)
        End If

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = UtilityObj.GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = Session("sUsr_name")
            params("reportCaption") = "MultiYear Columnar Report :" & txtFromDate.Text & " - " & txtToDate.Text
            params("reportDate") = ""
            params("BSUName") = ""

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/rptMultiYearColumnar.rpt"
            Session("ReportSource") = repSource
            'Context.Items.Add("ReportSource", repSource)
            objConn.Close()
            If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
                Response.Redirect("rptviewer.aspx?isExport=true", True)
            Else
                'Response.Redirect("rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()
    End Sub


    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub

    Sub ReportLoadSelection_Export()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx?isExport=true');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx?isExport=true','_blank');", True)
        End If
    End Sub
End Class
