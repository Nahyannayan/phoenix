Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data

Partial Class Reports_ASPX_Report_rptDisbursement
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT * FROM vw_OSA_VOUCHER where VHH_TYPE = 'P'"
        cmd.CommandType = Data.CommandType.Text
        Page.Title = OASISConstants.Gemstitle
        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            'lblError.Text = "No Records with specified condition"
        Else
            cmd.Connection = New SqlConnection(str_conn)
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("Month") = "Jan"
            params("decimal") = Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/RPTDISBURSEMENT.rpt"
            Session("ReportSource") = repSource
            response.redirect("rptviewer.aspx", True)
        End If

    End Sub
End Class
