Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Partial Class Reports_ASPX_Report_oasisAudit_rptDisplayAuditJ
    Inherits System.Web.UI.Page

    'Shared dtJournal As DataTable
    'Shared idJournal, iDeleteCount As Integer
    ' Shared str_timestamp() As Byte
    'Shared str_editData As String
    ''
    'Shared MainMnu_code As String
    ' Shared menu_rights As String
    ' Shared datamode As String = "none"
    Dim Encr_decrData As New Encryption64

    Sub initialize_components()
        txtHDocdate.Text = UtilityObj.GetDiplayDate()

        gvJournal.Attributes.Add("bordercolor", "#1b80b6")
        txtHNarration.Attributes.Add("onblur", "javascript:CopyDetails()")

        Session("dtJournal") = CreateDataTable()
        Session("dtCostChild") = CreateDataTableCostcenter()
        ViewState("idJournal") = 0
        Session("idCostChild") = 0
        'btnCancel.Visible = False
       
    End Sub


    Private Sub setModifyHeader(ByVal Aud_ID As String)
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM JOURNAL_H where JHD_AUD_ID='" & Aud_ID & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then

                txtHDocdate.Text = Format("{0:dd/MMM/yyyy}", CDate(ds.Tables(0).Rows(0)("JHD_DOCDT")))
                txtHNarration.Text = ds.Tables(0).Rows(0)("JHD_NARRATION")
                txtHDocno.Text = ds.Tables(0).Rows(0)("JHD_DOCNO")
                bind_Currency()
                setModifyDetails(Aud_ID)
            Else
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub bind_Currency() 'bind the currency combo according to selected date
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            ' str_Sql = " SELECT A.EXG_ID,A.EXG_CUR_ID,A.EXG_RATE,B.EXG_RATE LOCAL_RATE,( str(B.EXG_RATE)+'__'+ str(a.EXG_RATE) ) as RATES" _

            str_Sql = "SELECT A.EXG_ID,A.EXG_CUR_ID," _
                        & " A.EXG_RATE,B.EXG_RATE LOCAL_RATE," _
                        & " (ltrim(str(B.EXG_RATE))+'__'+" _
                        & " ltrim(str(a.EXG_RATE) ))+'__'+" _
                        & " ltrim(str(a.EXG_ID)) as RATES " _
                        & " FROM EXGRATE_S A,EXGRATE_S B" _
                        & " Where A.EXG_BSU_ID='" & Session("sBsuid") & "'" _
                        & " AND '" & txtHDocdate.Text & "' BETWEEN A.EXG_FDATE AND ISNULL(A.EXG_TDATE , GETDATE())" _
                        & " AND " _
                        & " B.EXG_BSU_ID='" & Session("sBsuid") & "' AND B.EXG_TDATE IS NULL" _
                        & " AND " _
                        & " B.EXG_CUR_ID='DHS'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            DDCurrency.Items.Clear()
            DDCurrency.DataSource = ds.Tables(0)
            DDCurrency.DataTextField = "EXG_CUR_ID"
            DDCurrency.DataValueField = "RATES"
            DDCurrency.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then

                If set_default_currency() <> True Then

                    DDCurrency.SelectedIndex = 0

                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim

                    txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim

                End If

            Else

            End If
            'getnextdocid()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "DisplayAuditJ_currency")
        End Try
    End Sub

    Private Function set_default_currency() As Boolean

        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

            Dim str_Sql As String

            str_Sql = "SELECT     BSU_ID," _
            & " BSU_SUB_ID, BSU_CURRENCY" _
            & " FROM vw_OSO_BUSINESSUNIT_M" _
            & " WHERE (BSU_ID = '" & Session("sBsuid") & "')"



            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)



            If ds.Tables(0).Rows.Count > 0 Then

                For Each item As ListItem In DDCurrency.Items

                    If item.Text = ds.Tables(0).Rows(0)("BSU_CURRENCY") Then

                        item.Selected = True

                        txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim

                        txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim

                        Return True

                        Exit For

                    End If

                Next

            Else

            End If

            Return False

        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message)

            Return False

        End Try

    End Function

    Private Sub setViewData()

        
        gvJournal.Columns(6).Visible = False
        gvJournal.Columns(7).Visible = False
        gvJournal.Columns(9).Visible = False

        ' btnHdate.Enabled = False
        DDCurrency.Enabled = False
        txtHDocdate.Attributes.Add("readonly", "readonly")
        txtHNarration.Attributes.Add("readonly", "readonly")

    End Sub

    Private Sub setModifyDetails(ByVal Aud_ID As String)
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM JOURNAL_D where JNL_AUD_ID='" & Aud_ID & "'and JNL_BDELETED='False' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    'JNL_DEBIT, JNL_CREDIT, JNL_NARRATION, 
                    Dim rDt As DataRow
                    'Dim i As Integer
                    Dim str_actname_cost_mand As String = getAccountname(ds.Tables(0).Rows(iIndex)("JNL_ACT_ID"))
                    rDt = Session("dtJournal").NewRow
                    rDt("GUID") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Id") = ds.Tables(0).Rows(iIndex)("JNL_SLNO")

                    ViewState("idJournal") = ViewState("idJournal") + 1
                    rDt("Accountid") = ds.Tables(0).Rows(iIndex)("JNL_ACT_ID")
                    rDt("Accountname") = str_actname_cost_mand.Split("|")(0)
                    rDt("Narration") = ds.Tables(0).Rows(iIndex)("JNL_NARRATION")

                    rDt("Credit") = ds.Tables(0).Rows(iIndex)("JNL_CREDIT")
                    rDt("Debit") = ds.Tables(0).Rows(iIndex)("JNL_DEBIT")

                    rDt("CostCenter") = str_actname_cost_mand.Split("|")(1)
                    rDt("Required") = Convert.ToBoolean(str_actname_cost_mand.Split("|")(2))
                    rDt("Status") = "Normal"
                    Session("dtJournal").Rows.Add(rDt)
                Next
                setModifyCost(Aud_ID)
            Else

            End If
            ViewState("idJournal") = ViewState("idJournal") + 1

            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub setModifyCost(ByVal Aud_ID As String)
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString
            Dim str_Sql As String


            str_Sql = "SELECT JOURNAL_D_S.GUID, JOURNAL_D_S.JDS_DOCTYPE," _
            & " JOURNAL_D_S.JDS_DOCNO," _
            & " case isnull(JOURNAL_D_S.JDS_CODE,'')" _
            & " when '' then  vw_OSA_COSTCENTER_S.CCS_DESCR else" _
            & " JOURNAL_D_S.JDS_DESCR end JDS_DESCR ," _
            & " JOURNAL_D_S.JDS_ACT_ID, JOURNAL_D_S.JDS_CCS_ID," _
            & " JOURNAL_D_S.JDS_CODE, JOURNAL_D_S.JDS_AMOUNT," _
            & " JOURNAL_D_S.JDS_SLNO FROM JOURNAL_D_S INNER JOIN" _
            & " vw_OSA_COSTCENTER_S ON JOURNAL_D_S.JDS_CCS_ID = vw_OSA_COSTCENTER_S.CCS_ID" _
            & " WHERE (JOURNAL_D_S.JDS_AUD_ID = '" & Aud_ID & "')" _
            & " AND (JOURNAL_D_S.JDS_bDELETED = 'False')" _
            & " and (JOURNAL_D_S.JDS_DOCTYPE='JV')"



            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim rDt As DataRow
                   
                    rDt = Session("dtCostChild").NewRow
                    rDt("GUID") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Id") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Name") = ds.Tables(0).Rows(iIndex)("JDS_DESCR") & ""
                    rDt("Memberid") = ds.Tables(0).Rows(iIndex)("JDS_CODE")
                    rDt("VoucherId") = ds.Tables(0).Rows(iIndex)("JDS_SLNO")
                    rDt("Costcenter") = ds.Tables(0).Rows(iIndex)("JDS_CCS_ID")
                    'rDt("Name") = ds.Tables(0).Rows(iIndex)("JDS_CCS_ID")
                    rDt("Amount") = ds.Tables(0).Rows(iIndex)("JDS_AMOUNT")
                    rDt("Status") = "Normal"
                    'idCostChild = idCostChild + 1
                    Session("dtCostChild").Rows.Add(rDt)
                Next
            Else
            End If
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Function getAccountname(ByVal Aud_ID As String) As String
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT ACT_ID,ACT_NAME FROM ACCOUNTS_M where ACT_ID='" & Aud_ID & "' "

            str_Sql = "SELECT ACT_ID,ACT_NAME,ACT_PLY_ID, " _
            & " isnull(PM.PLY_COSTCENTER,'AST') PLY_COSTCENTER ,PM.PLY_BMANDATORY" _
            & " FROM vw_OSA_ACCOUNTS_M AM, vw_OSA_POLICY_M PM  WHERE" _
            & " ACT_Bctrlac='FALSE' AND PM.PLY_ID = AM.ACT_PLY_ID" _
            & " AND ACT_ID='" & Aud_ID & "'" _
            & " AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%'"

            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ACT_NAME") & "|" _
                & ds.Tables(0).Rows(0)("PLY_COSTCENTER") & "|" _
                & ds.Tables(0).Rows(0)("PLY_BMANDATORY")
            Else

            End If
            Return " | | "
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return " | | "
        End Try
    End Function

    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarration As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cCredit As New DataColumn("Credit", System.Type.GetType("System.Decimal"))
            Dim cDebit As New DataColumn("Debit", System.Type.GetType("System.Decimal"))
            Dim cCostcenter As New DataColumn("Costcenter", System.Type.GetType("System.String"))
            Dim cCCRequired As New DataColumn("Required", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cNarration)
            dtDt.Columns.Add(cCredit)
            dtDt.Columns.Add(cDebit)

            dtDt.Columns.Add(cCostcenter)
            dtDt.Columns.Add(cCCRequired)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)

            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function


    Private Function CreateDataTableCostcenter() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cVoucherid As New DataColumn("VoucherId", System.Type.GetType("System.String"))
            Dim cCostcenter As New DataColumn("Costcenter", System.Type.GetType("System.String"))
            Dim cMember As New DataColumn("Memberid", System.Type.GetType("System.String"))
            Dim cName As New DataColumn("Name", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cVoucherid)
            dtDt.Columns.Add(cCostcenter)

            dtDt.Columns.Add(cMember)
            dtDt.Columns.Add(cName)
            dtDt.Columns.Add(cAmount)

            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)

            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function


    Protected Sub DDCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDCurrency.SelectedIndexChanged
        txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub

    Private Sub gridbind()
        Try

            Dim i As Integer
            Dim dtTempjournal As New DataTable
            dtTempjournal = CreateDataTable()
            Dim dDebit As Double = 0
            Dim dCredit As Double = 0
            'Dim dTotAmount As Double = 0
            Dim dAllocate As Double = 0
            If Session("dtJournal").Rows.Count > 0 Then
                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Status") & "" <> "Deleted" Then
                        Dim rDt As DataRow
                        rDt = dtTempjournal.NewRow
                        For j As Integer = 0 To Session("dtJournal").Columns.Count - 1
                            rDt.Item(j) = Session("dtJournal").Rows(i)(j)
                        Next
                        If Session("dtJournal").Rows(i)("Credit") <> 0 Then
                            dCredit = dCredit + Session("dtJournal").Rows(i)("Credit")
                        Else
                            dDebit = dDebit + Session("dtJournal").Rows(i)("Debit")
                        End If
                        ' dTotAmount = dTotAmount + session("dtCostChild").Rows(i)("Amount")
                        dtTempjournal.Rows.Add(rDt)
                    Else
                    End If
                Next
            End If
            gvJournal.DataSource = dtTempjournal
            gvJournal.DataBind()
            txtTDotalDebit.Text = dDebit
            txtTotalCredit.Text = dCredit
            txtDifference.Text = dDebit - dCredit
            
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function set_viewdata(ByVal Aud_ID As String) As String
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM JOURNAL_H WHERE" _
            & " JHD_AUD_ID='" & Aud_ID & "'" _
            & " AND JHD_BSU_ID='" & Session("sBsuid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocdate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0)("JHD_DOCDT"))
                txtHDocno.Text = ds.Tables(0).Rows(0)("JHD_DOCNO")
                txtHNarration.Text = ds.Tables(0).Rows(0)("JHD_NARRATION")

                ViewState("str_editData") = ds.Tables(0).Rows(0)("JHD_DOCNO") & "|" _
                & ds.Tables(0).Rows(0)("JHD_SUB_ID") & "|" _
                & ds.Tables(0).Rows(0)("JHD_DOCDT") & "|" _
                & ds.Tables(0).Rows(0)("JHD_FYEAR") & "|"
                Return ""
            Else
                ViewState("str_editData") = ""
                'datamode = Encr_decrData.Encrypt("view")

            End If
            Return ""
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            ' datamode = Encr_decrData.Encrypt("view")

        End Try
        Return True
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                ' 

              

                Dim Aud_ID As String = Encr_decrData.Decrypt(Request.QueryString("Aud_ID").Replace(" ", "+"))
                Dim DType As String = Encr_decrData.Decrypt(Request.QueryString("DType").Replace(" ", "+"))
                Dim Sel_Flag As String = Request.QueryString("Sel_Flag")
                Dim DocNo As String = Encr_decrData.Decrypt(Request.QueryString("DocNo").Replace(" ", "+"))
                Dim DocName As String = Encr_decrData.Decrypt(Request.QueryString("DocName").Replace(" ", "+"))
                'MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'lblHeader.Text = DocName
                Select Case DType.ToUpper
                    Case "JV"

                         If Sel_Flag = "B" Then
                            Using AudID_reader As SqlDataReader = AccessRoleUser.GetAud_ID_Voucher(Aud_ID, DocNo, Sel_Flag)
                                If AudID_reader.HasRows = True Then
                                    While AudID_reader.Read
                                        Aud_ID = Convert.ToString(AudID_reader("ID"))
                                    End While
                                End If

                            End Using

                        End If
                        initialize_components()
                        set_viewdata(Aud_ID)

                        setViewData()
                        setModifyHeader(Aud_ID)
                        bind_Currency()
                End Select



            Catch ex As Exception
                UtilityObj.Errorlog("OASISAUDIT", ex.Message)
            End Try
        End If

    End Sub


End Class
