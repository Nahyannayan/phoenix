<%@ Page Language="VB" AutoEventWireup="false" CodeFile="rptDisplayAuditJ.aspx.vb" Inherits="Reports_ASPX_Report_oasisAudit_rptDisplayAuditJ" Title="::::GEMS OASIS:::: Online Student Administration System::::"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
   <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../../cssfiles/example.css" rel="stylesheet" type="text/css" />
    
    <base target="_self" />
    </head>
 <body  class="no_margin"> 
    <form id="form1" runat="server">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">
        <tr>
            <td align="left">
                &nbsp;<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <table id="tbl_test" runat="server" align="center" border="1" bordercolor="#1b80b6"
                    cellpadding="5" cellspacing="0" width="100%">
                    <tr class="subheader_img">
                        <td align="left" colspan="9" valign="middle">
                            <strong><font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">Jounal Voucher
                                &nbsp; &nbsp; &nbsp; </font></strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" width="16%">
                            <strong>Doc No</strong></td>
                        <td class="matters" style="font-weight: bold" width="2%">
                            :</td>
                        <td align="left" class="matters" colspan="3" style="font-weight: bold" width="46%">
                            &nbsp;<asp:TextBox ID="txtHDocno" runat="server" CssClass="inputbox" ReadOnly="True"
                                Width="224px"></asp:TextBox></td>
                        <td align="left" class="matters" width="6%">
                            <strong>Doc Date</strong></td>
                        <td class="matters" width="2%">
                            <strong>&nbsp;:</strong></td>
                        <td align="left" class="matters" colspan="2" style="font-weight: bold" width="25%">
                            <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="True" CssClass="inputbox"
                                Width="200px"></asp:TextBox>&nbsp;
                            </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" width="16%">
                            <strong>Currency</strong></td>
                        <td class="matters" width="2%">
                            <strong>:</strong></td>
                        <td align="left" class="matters" style="width: 16%">
                            &nbsp;<asp:DropDownList ID="DDCurrency" runat="server" AutoPostBack="True" CssClass="listbox"
                                Width="72px">
                            </asp:DropDownList></td>
                        <td align="left" class="matters" colspan="2" width="7%">
                            <asp:TextBox ID="txtHExchRate" runat="server" CssClass="inputbox" ReadOnly="True"
                                Width="128px"></asp:TextBox></td>
                        <td align="left" class="matters" width="16%">
                            <strong>Local Rate</strong></td>
                        <td class="matters" width="6%">
                            &nbsp;<strong>:</strong></td>
                        <td align="left" class="matters" colspan="2" width="2%">
                            <asp:TextBox ID="txtHLocalRate" runat="server" CssClass="inputbox" ReadOnly="True"
                                Width="200px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" style="height: 33px" width="16%">
                            <strong>Narration</strong></td>
                        <td class="matters" style="height: 33px" width="2%">
                            <strong>:</strong></td>
                        <td align="left" class="matters" colspan="7" style="height: 33px" width="77%">
                            &nbsp;<asp:TextBox ID="txtHNarration" runat="server" CssClass="inputbox" Width="304px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtHNarration"
                                ErrorMessage="Voucher Narration Cannot be blank" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                    </tr>
                </table>
                <table id="Table1" align="center" border="1" bordercolor="#1b80b6" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="center" class="matters" colspan="9" width="95%">
                            <br />
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                EmptyDataText="No Transaction details added yet." Width="98%" CssClass="gridstyle">
                                <Columns>
                                    <asp:TemplateField HeaderText="id" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Accountid" HeaderText="Account Code" ReadOnly="True" />
                                    <asp:TemplateField HeaderText="Account Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Accountname") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Narration" HeaderText="Narration" />
                                    <asp:BoundField DataField="Debit" DataFormatString="{0:0.00}" HeaderText="Debit"
                                        HtmlEncode="False" SortExpression="Debit">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Credit" DataFormatString="{0:0.00}" HeaderText="Credit"
                                        HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edits"
                                                Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Edit
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="CostCenter" HeaderText="TEST" />
                                    <asp:TemplateField HeaderText="Allocate" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnAlloca" runat="server" CausesValidation="false" CommandName=""
                                                Text="Allocate"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cost Center" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRequired" runat="server" Text='<%# Bind("Required") %>' Visible="False"></asp:Label>
                                            <asp:Image ID="imgAllocate" runat="server" Visible="False" />&nbsp;
                                            <asp:LinkButton ID="lbAllocate" runat="server"  Visible="False">Allocate</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" />
                                <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                            </asp:GridView>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="9" style="height: 62px">
                            Debit Total :
                            <asp:TextBox ID="txtTDotalDebit" runat="server" CssClass="inputbox" ReadOnly="True"
                                Width="144px"></asp:TextBox>&nbsp; Credit&nbsp; Total : &nbsp;<asp:TextBox ID="txtTotalCredit"
                                    runat="server" CssClass="inputbox" ReadOnly="True" Width="144px"></asp:TextBox>
                            &nbsp; &nbsp;Difference : &nbsp;<asp:TextBox ID="txtDifference" runat="server" CssClass="inputbox"
                                ReadOnly="True" Width="144px"></asp:TextBox>
                            &nbsp; &nbsp; 
                            <br />
                            <br />
                            <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close();"
                                Text="Close" Width="47px" />
                            &nbsp;&nbsp;</td>
                    </tr>
                    <tr id="tr_SaveButtons" runat="server">
                        <td align="center" class="matters" colspan="9" style="height: 54px">
                            &nbsp; &nbsp; &nbsp;
                            <br />
                            <input id="h_Memberids" runat="server" type="hidden" />
                            <input id="h_mode" runat="server" type="hidden" />
                            <input id="h_editorview" runat="server" type="hidden" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>
</body>
</html>


