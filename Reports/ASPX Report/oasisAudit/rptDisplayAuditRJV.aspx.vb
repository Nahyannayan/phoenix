Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Partial Class Reports_ASPX_Report_oasisAudit_rptDisplayAuditRJV
    Inherits System.Web.UI.Page
    Shared MainMnu_code As String
    Shared menu_rights As Integer
    Shared datamode As String = "none"
    Dim Encr_decrData As New Encryption64
    Shared content As ContentPlaceHolder
    Shared Eid As String


    Shared dtDTL As DataTable
    Shared lstrErrMsg As String
    Shared gintGridLine, gintEditLine As Integer
    Shared gDtlDataMode As String
    Shared idJournal, idCostChild, iDeleteCount As Integer
    Shared str_timestamp() As Byte

    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarrn As New DataColumn("Narration", System.Type.GetType("System.String"))

            Dim cDebit As New DataColumn("Debit", System.Type.GetType("System.Decimal"))
            Dim cCredit As New DataColumn("Credit", System.Type.GetType("System.Decimal"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            Dim cPly As New DataColumn("Ply", System.Type.GetType("System.String"))
            Dim cCostReqd As New DataColumn("CostReqd", System.Type.GetType("System.Boolean"))


            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cNarrn)


            dtDt.Columns.Add(cDebit)
            dtDt.Columns.Add(cCredit)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)

            dtDt.Columns.Add(cPly)
            dtDt.Columns.Add(cCostReqd)



            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
    End Function
    Private Function CreateSubDataTable() As DataTable

        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cVoucherid As New DataColumn("VoucherId", System.Type.GetType("System.String"))
            Dim cCostcenter As New DataColumn("Costcenter", System.Type.GetType("System.String"))
            Dim cMember As New DataColumn("Memberid", System.Type.GetType("System.String"))
            Dim cName As New DataColumn("Name", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            Session("gdtSub").Columns.Add(cId)
            Session("gdtSub").Columns.Add(cVoucherid)
            Session("gdtSub").Columns.Add(cCostcenter)

            Session("gdtSub").Columns.Add(cMember)
            Session("gdtSub").Columns.Add(cName)
            Session("gdtSub").Columns.Add(cAmount)

            Session("gdtSub").Columns.Add(cStatus)
            Session("gdtSub").Columns.Add(cGuid)




            Return Session("gdtSub")
        Catch ex As Exception
            Return Session("gdtSub")
        End Try
    End Function
    Private Sub bind_Currency() 'bind the currency combo according to selected date
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            ' str_Sql = " SELECT A.EXG_ID,A.EXG_CUR_ID,A.EXG_RATE,B.EXG_RATE LOCAL_RATE,( str(B.EXG_RATE)+'__'+ str(a.EXG_RATE) ) as RATES" _

            str_Sql = "SELECT A.EXG_ID,A.EXG_CUR_ID," _
                        & " A.EXG_RATE,B.EXG_RATE LOCAL_RATE," _
                        & " (ltrim(str(B.EXG_RATE))+'__'+" _
                        & " ltrim(str(a.EXG_RATE) ))+'__'+" _
                        & " ltrim(str(a.EXG_ID)) as RATES " _
                        & " FROM EXGRATE_S A,EXGRATE_S B" _
                        & " Where A.EXG_BSU_ID='" & Session("sBsuid") & "'" _
                        & " AND '" & txtDocDate.Text & "' BETWEEN A.EXG_FDATE AND ISNULL(A.EXG_TDATE , GETDATE())" _
                        & " AND " _
                        & " B.EXG_BSU_ID='" & Session("sBsuid") & "' AND B.EXG_TDATE IS NULL" _
                        & " AND " _
                        & " B.EXG_CUR_ID='DHS'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            cmbCurrency.Items.Clear()
            cmbCurrency.DataSource = ds.Tables(0)
            cmbCurrency.DataTextField = "EXG_CUR_ID"
            cmbCurrency.DataValueField = "RATES"
            cmbCurrency.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then

                If set_default_currency() <> True Then

                    cmbCurrency.SelectedIndex = 0

                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim

                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim

                End If

            Else

            End If
            'getnextdocid()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "rptDisplayPJ-Bind_currency")
        End Try
    End Sub
    Private Sub LockControls()
        txtDocNo.Attributes.Add("readonly", "readonly")
        txtDocDate.Attributes.Add("readonly", "readonly")

        txtExchRate.Attributes.Add("readonly", "readonly")
        txtLocalRate.Attributes.Add("readonly", "readonly")
       

    End Sub

    Private Function set_default_currency() As Boolean

        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

            Dim str_Sql As String

            str_Sql = "SELECT     BSU_ID," _
            & " BSU_SUB_ID, BSU_CURRENCY" _
            & " FROM vw_OSO_BUSINESSUNIT_M" _
            & " WHERE (BSU_ID = '" & Session("sBsuid") & "')"

            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)



            If ds.Tables(0).Rows.Count > 0 Then

                For Each item As ListItem In cmbCurrency.Items

                    If item.Text = ds.Tables(0).Rows(0)("BSU_CURRENCY") Then

                        item.Selected = True

                        txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim

                        txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim

                        Return True

                        Exit For

                    End If

                Next

            Else

            End If

            Return False

        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message)

            Return False

        End Try

    End Function

    Protected Sub cmbCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCurrency.SelectedIndexChanged
        txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                

                Dim Aud_ID As String = Encr_decrData.Decrypt(Request.QueryString("Aud_ID").Replace(" ", "+"))
                Dim DType As String = Encr_decrData.Decrypt(Request.QueryString("DType").Replace(" ", "+"))
                Dim Sel_Flag As String = Encr_decrData.Decrypt(Request.QueryString("Sel_Flag").Replace(" ", "+"))
                Dim DocNo As String = Encr_decrData.Decrypt(Request.QueryString("DocNo").Replace(" ", "+"))
                Dim DocName As String = Encr_decrData.Decrypt(Request.QueryString("DocName").Replace(" ", "+"))
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                Select Case DType.ToUpper
                    Case "RJV"

                        If Sel_Flag = "B" Then
                            Using AudID_reader As SqlDataReader = AccessRoleUser.GetAud_ID_Voucher(Aud_ID, DocNo, Sel_Flag)
                                If AudID_reader.HasRows = True Then
                                    While AudID_reader.Read
                                        Aud_ID = Convert.ToString(AudID_reader("ID"))
                                    End While
                                End If

                            End Using

                        End If
                        Session("BANKTRAN") = "RJV"
                        Eid = Aud_ID
                        LockControls()
                        FillValues()
                        bind_Currency()
                End Select



            Catch ex As Exception
                UtilityObj.Errorlog("OASISAUDIT", ex.Message)
            End Try
        End If
    End Sub
    Private Sub FillValues()
        '   --- Check Whether This Account Has Got Transactions
        Try


            Dim lstrConn As String = ConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString
            Dim lstrSQL, lstrSQL2 As String

            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim i As Integer

            lstrSQL = "SELECT A.*,REPLACE(CONVERT(VARCHAR(11), A.RJH_DOCDT, 106), ' ', '/') as DocDate  FROM RJOURNAL_H A  " _
                       & " WHERE A.RJH_AUD_ID='" & Eid & "' "
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)

            If ds.Tables(0).Rows.Count > 0 Then
                txtDocNo.Text = ds.Tables(0).Rows(0)("RJH_DOCNO")
                txtDocDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0)("DocDate"))

                bind_Currency()
                For i = 0 To cmbCurrency.Items.Count - 1
                    If cmbCurrency.Items(i).Value = ds.Tables(0).Rows(0)("RJH_CUR_ID") Then
                        cmbCurrency.SelectedIndex = i
                        Exit For
                    End If
                Next
                txtExchRate.Text = ds.Tables(0).Rows(0)("RJH_EXGRATE1")
                txtLocalRate.Text = ds.Tables(0).Rows(0)("RJH_EXGRATE2")
                txtNarrn.Text = ds.Tables(0).Rows(0)("RJH_NARRATION")
                txtMonths.Text = ds.Tables(0).Rows(0)("RJH_MONTHS")


                '   --- Initialize The Grid With The Data From The Detail Table
                lstrSQL2 = "SELECT Convert(VarChar,A.RJL_SLNO) as Id,A.RJL_ACT_ID as AccountId,C.ACT_NAME as AccountName,A.RJL_NARRATION as Narration,A.RJL_DEBIT as Debit,A.RJL_CREDIT as Credit,(A.RJL_DEBIT+A.RJL_CREDIT) as Amount, " _
                            & " '' as Status,A.GUID FROM RJOURNAL_D A  " _
                            & " INNER JOIN vw_OSA_ACCOUNTS_M C ON A.RJL_ACT_ID=C.ACT_ID" _
                            & " WHERE A.RJL_SUB_ID='" & ds.Tables(0).Rows(0)("RJH_SUB_ID") & "'  AND RJL_BSU_ID='" & ds.Tables(0).Rows(0)("RJH_BSU_ID") & "'   AND A.RJL_AUD_ID='" & Eid & "'  "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)

                dtDTL = CreateDataTable()
                dtDTL = ds2.Tables(0)


                Dim dDebit As Double = 0
                Dim dCredit As Double = 0

                Dim dAllocate As Double = 0
                If dtDTL.Rows.Count > 0 Then
                    For i = 0 To dtDTL.Rows.Count - 1

                        Dim rDt As DataRow
                        rDt = dtDTL.NewRow
                        For j As Integer = 0 To dtDTL.Columns.Count - 1
                            rDt.Item(j) = dtDTL.Rows(i)(j)
                        Next
                        If dtDTL.Rows(i)("Credit") <> 0 Then
                            dCredit = dCredit + dtDTL.Rows(i)("Credit")
                        Else
                            dDebit = dDebit + dtDTL.Rows(i)("Debit")
                        End If

                      

                    Next
                End If

                txtDebit.Text = dDebit
                txtCredit.Text = dCredit






                gvDTL.DataSource = dtDTL
                gvDTL.DataBind()

               

               


            Else
                lblError.Text = "Record Not Found !!!"
                UtilityObj.Errorlog("AuditDisplayRJV", "Record Not Found !!!")
            End If




        Catch ex As Exception

            UtilityObj.Errorlog("AuditDisplayRJV", ex.Message)

        End Try
    End Sub
End Class
