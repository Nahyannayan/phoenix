Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Partial Class Reports_ASPX_Report_oasisAudit_rptDisplayAuditPJ
    Inherits System.Web.UI.Page
    'Shared MainMnu_code As String

    'Shared datamode As String = "none"
    Dim Encr_decrData As New Encryption64

    'Shared Eid As String


    ' Shared dtDTL As DataTable
    'Shared lstrErrMsg As String
    'Shared gintGridLine, gintEditLine As Integer
    'Shared gDtlDataMode As String
    'Shared idJournal, idCostChild, iDeleteCount As Integer
    'Shared str_timestamp() As Byte


    Private Sub FillValues(ByVal Aud_ID As String)
        '   --- Check Whether This Account Has Got Transactions
        Try
            Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString
            Dim lstrSQL, lstrSQL2 As String
            ' Dim Eid As Guid = System.Guid

            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim i As Integer
            'code to be corrected 01 06 2008

            lstrSQL = "SELECT A.*,REPLACE(CONVERT(VARCHAR(11), A.PUH_DOCDT, 106), ' ', '/') as DocDate  FROM PURCHASE_H A  " _
                       & " WHERE A.PUH_AUD_ID='" & Aud_ID & "' "
            ds = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL)

            If ds.Tables(0).Rows.Count > 0 Then
                txtDocNo.Text = ds.Tables(0).Rows(0)("PUH_DOCNO")
                txtDocDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0)("PUH_DOCDT"))
                txtPartyCode.Text = ds.Tables(0).Rows(0)("PUH_PARTY_ACT_ID")
                txtPartyDescr.Text = GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("PUH_PARTY_ACT_ID"))
                txtDrCode.Text = ds.Tables(0).Rows(0)("PUH_DEBIT_ACT_ID")
                txtDrDescr.Text = GetDescr("ACCOUNTS_M", "ACT_Name", "ACT_ID", ds.Tables(0).Rows(0)("PUH_DEBIT_ACT_ID"))
                bind_Currency()
                For i = 0 To cmbCurrency.Items.Count - 1
                    If cmbCurrency.Items(i).Value = ds.Tables(0).Rows(0)("PUH_CUR_ID") Then
                        cmbCurrency.SelectedIndex = i
                        Exit For
                    End If
                Next
                txtExchRate.Text = ds.Tables(0).Rows(0)("PUH_EXGRATE1")
                txtLocalRate.Text = ds.Tables(0).Rows(0)("PUH_EXGRATE2")
                txtNarrn.Text = ds.Tables(0).Rows(0)("PUH_NARRATION")
                txtLPO.Text = ds.Tables(0).Rows(0)("PUH_LPO")
                txtInvoiceNo.Text = ds.Tables(0).Rows(0)("PUH_INVOICENO")



                '   --- Initialize The Grid With The Data From The Detail Table
                lstrSQL2 = "SELECT Convert(VarChar,A.PUD_LINENO) as Id,A.PUD_DEBIT_ACT_ID as AccountId,C.ACT_NAME as AccountName,A.PUD_ITEM as Item,A.PUD_QTY as Qty,A.PUD_Rate as Rate,(A.PUD_QTY*A.PUD_Rate) as Amount, " _
                            & " '' as Status,A.GUID,isNULL(PLY_COSTCENTER,'OTH') as PLY,PLY_BMANDATORY as CostReqd FROM PURCHASE_D A  " _
                            & " INNER JOIN vw_OSA_ACCOUNTS_M C ON A.PUD_DEBIT_ACT_ID=C.ACT_ID" _
                            & " WHERE A.PUD_SUB_ID='" & ds.Tables(0).Rows(0)("PUH_SUB_ID") & "'  AND PUD_BSU_ID='" & ds.Tables(0).Rows(0)("PUH_BSU_ID") & "'   AND A.PUD_DOCNO='" & ds.Tables(0).Rows(0)("PUH_DOCNO") & "'  "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)

                Session("dtDTL") = CreateDataTable()
                Session("dtDTL") = ds2.Tables(0)
                gvDTL.DataSource = ds2
                gvDTL.DataBind()
                lstrSQL2 = "SELECT MAx(PUD_LINENO) as Id FROM PURCHASE_D A " _
                           & " WHERE A.PUD_SUB_ID='" & ds.Tables(0).Rows(0)("PUH_SUB_ID") & "'  AND PUD_BSU_ID='" & ds.Tables(0).Rows(0)("PUH_BSU_ID") & "'   AND A.PUD_DOCNO='" & ds.Tables(0).Rows(0)("PUH_DOCNO") & "'  "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)
                ViewState("gintGridLine") = ds2.Tables(0).Rows(0)("Id") + 1


                '   ----  Initalize the Cost Center Grid
                Session("gdtSub") = CreateSubDataTable()
                lstrSQL2 = "SELECT Convert(VarChar,A.VDS_ID) as Id,A.VDS_SLNO as VoucherId,A.VDS_CCS_ID as Costcenter,A.VDS_CODE as Memberid,A.VDS_DESCr as Name,A.VDS_AMOUNT as Amount," _
                            & " '' as Status,A.GUID FROM VOUCHER_D_S A" _
                            & " WHERE A.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("PUH_SUB_ID") & "'  AND VDS_BSU_ID='" & ds.Tables(0).Rows(0)("PUH_BSU_ID") & "'   AND A.VDS_DOCNO='" & ds.Tables(0).Rows(0)("PUH_DOCNO") & "' AND VDS_CODE IS NOT NULL " _
                            & " UNION ALL SELECT Convert(VarChar,A.VDS_ID) as Id,A.VDS_SLNO as VoucherId,'OTH' as Costcenter,A.VDS_CCS_ID as Memberid,A.VDS_DESCr as Name,A.VDS_AMOUNT as Amount," _
                            & " '' as Status,A.GUID FROM VOUCHER_D_S A" _
                            & " WHERE A.VDS_SUB_ID='" & ds.Tables(0).Rows(0)("PUH_SUB_ID") & "'  AND VDS_BSU_ID='" & ds.Tables(0).Rows(0)("PUH_BSU_ID") & "'   AND A.VDS_DOCNO='" & ds.Tables(0).Rows(0)("PUH_DOCNO") & "' AND VDS_CODE IS NULL "
                ds2 = SqlHelper.ExecuteDataset(lstrConn, CommandType.Text, lstrSQL2)


                Session("gdtSub") = ds2.Tables(0)


            Else
                lblError.Text = "Record Not Found !!!"
            End If
        Catch
            lblError.Text = "Record Not Found !!! "
        End Try
    End Sub
    Private Sub bind_Currency() 'bind the currency combo according to selected date
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            ' str_Sql = " SELECT A.EXG_ID,A.EXG_CUR_ID,A.EXG_RATE,B.EXG_RATE LOCAL_RATE,( str(B.EXG_RATE)+'__'+ str(a.EXG_RATE) ) as RATES" _

            str_Sql = "SELECT A.EXG_ID,A.EXG_CUR_ID," _
                        & " A.EXG_RATE,B.EXG_RATE LOCAL_RATE," _
                        & " (ltrim(str(B.EXG_RATE))+'__'+" _
                        & " ltrim(str(a.EXG_RATE) ))+'__'+" _
                        & " ltrim(str(a.EXG_ID)) as RATES " _
                        & " FROM EXGRATE_S A,EXGRATE_S B" _
                        & " Where A.EXG_BSU_ID='" & Session("sBsuid") & "'" _
                        & " AND '" & txtDocDate.Text & "' BETWEEN A.EXG_FDATE AND ISNULL(A.EXG_TDATE , GETDATE())" _
                        & " AND " _
                        & " B.EXG_BSU_ID='" & Session("sBsuid") & "' AND B.EXG_TDATE IS NULL" _
                        & " AND " _
                        & " B.EXG_CUR_ID='DHS'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            cmbCurrency.Items.Clear()
            cmbCurrency.DataSource = ds.Tables(0)
            cmbCurrency.DataTextField = "EXG_CUR_ID"
            cmbCurrency.DataValueField = "RATES"
            cmbCurrency.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then

                If set_default_currency() <> True Then

                    cmbCurrency.SelectedIndex = 0

                    txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim

                    txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim

                End If

            Else

            End If
            'getnextdocid()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "rptDisplayPJ-Bind_currency")
        End Try
    End Sub
    Private Sub LockControls()
        txtDocNo.Attributes.Add("readonly", "readonly")
        txtDocDate.Attributes.Add("readonly", "readonly")
        txtDrCode.Attributes.Add("readonly", "readonly")
        txtDrDescr.Attributes.Add("readonly", "readonly")
        txtExchRate.Attributes.Add("readonly", "readonly")
        txtLocalRate.Attributes.Add("readonly", "readonly")
        txtPartyCode.Attributes.Add("readonly", "readonly")
        txtPartyDescr.Attributes.Add("readonly", "readonly")
       


    End Sub

    Private Function set_default_currency() As Boolean

        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

            Dim str_Sql As String

            str_Sql = "SELECT     BSU_ID," _
            & " BSU_SUB_ID, BSU_CURRENCY" _
            & " FROM vw_OSO_BUSINESSUNIT_M" _
            & " WHERE (BSU_ID = '" & Session("sBsuid") & "')"

            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)



            If ds.Tables(0).Rows.Count > 0 Then

                For Each item As ListItem In cmbCurrency.Items

                    If item.Text = ds.Tables(0).Rows(0)("BSU_CURRENCY") Then

                        item.Selected = True

                        txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim

                        txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim

                        Return True

                        Exit For

                    End If

                Next

            Else

            End If

            Return False

        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message)

            Return False

        End Try

    End Function
    Public Function GetDescr(ByVal pTableName As String, ByVal pFieldName As String, ByVal pWhere As String, ByVal pVal As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As New DataSet

        Dim str_Sql As String = "SELECT  [" & pFieldName & "] From " & pTableName & " WHERE " & pWhere & "='" & pVal & "'"


        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)(0) & ""
        Else
            Return "--"
        End If

    End Function
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cItem As New DataColumn("Item", System.Type.GetType("System.String"))
            Dim cQty As New DataColumn("Qty", System.Type.GetType("System.Decimal"))
            Dim cRate As New DataColumn("Rate", System.Type.GetType("System.Decimal"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            Dim cPly As New DataColumn("Ply", System.Type.GetType("System.String"))
            Dim cCostReqd As New DataColumn("CostReqd", System.Type.GetType("System.Boolean"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cItem)
            dtDt.Columns.Add(cQty)
            dtDt.Columns.Add(cRate)
            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)

            dtDt.Columns.Add(cPly)
            dtDt.Columns.Add(cCostReqd)

            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
    End Function
    Private Function CreateSubDataTable() As DataTable

        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cVoucherid As New DataColumn("VoucherId", System.Type.GetType("System.String"))
            Dim cCostcenter As New DataColumn("Costcenter", System.Type.GetType("System.String"))
            Dim cMember As New DataColumn("Memberid", System.Type.GetType("System.String"))
            Dim cName As New DataColumn("Name", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            Session("gdtSub").Columns.Add(cId)
            Session("gdtSub").Columns.Add(cVoucherid)
            Session("gdtSub").Columns.Add(cCostcenter)

            Session("gdtSub").Columns.Add(cMember)
            Session("gdtSub").Columns.Add(cName)
            Session("gdtSub").Columns.Add(cAmount)

            Session("gdtSub").Columns.Add(cStatus)
            Session("gdtSub").Columns.Add(cGuid)




            Return Session("gdtSub")
        Catch ex As Exception
            Return Session("gdtSub")
        End Try
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                

                Dim Aud_ID As String = Encr_decrData.Decrypt(Request.QueryString("Aud_ID").Replace(" ", "+"))
                Dim DType As String = Encr_decrData.Decrypt(Request.QueryString("DType").Replace(" ", "+"))
                Dim Sel_Flag As String = Request.QueryString("Sel_Flag")
                Dim DocNo As String = Encr_decrData.Decrypt(Request.QueryString("DocNo").Replace(" ", "+"))
                Dim DocName As String = Encr_decrData.Decrypt(Request.QueryString("DocName").Replace(" ", "+"))
                'MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                Select Case DType.ToUpper
                    Case "PJ"

                        If Sel_Flag = "B" Then
                            Using AudID_reader As SqlDataReader = AccessRoleUser.GetAud_ID_Voucher(Aud_ID, DocNo, Sel_Flag)
                                If AudID_reader.HasRows = True Then
                                    While AudID_reader.Read
                                        Aud_ID = Convert.ToString(AudID_reader("ID"))
                                    End While
                                End If

                            End Using

                        End If
                        LockControls()
                        FillValues(Aud_ID)
                        bind_Currency()
                End Select



            Catch ex As Exception
                UtilityObj.Errorlog("OASISAUDIT", ex.Message)
            End Try
        End If

    End Sub

    Protected Sub cmbCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCurrency.SelectedIndexChanged
        txtExchRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtLocalRate.Text = cmbCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub
End Class
