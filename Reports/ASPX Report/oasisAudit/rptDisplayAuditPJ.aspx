<%@ Page Language="VB"  AutoEventWireup="false" CodeFile="rptDisplayAuditPJ.aspx.vb" Inherits="Reports_ASPX_Report_oasisAudit_rptDisplayAuditPJ" Title="::::GEMS OASIS:::: Online Student Administration System::::"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
   <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../../cssfiles/example.css" rel="stylesheet" type="text/css" />
   <base target="_self" />
    </head>
   
  
    <script language="javascript" type="text/javascript">
function getDate(pId) 
   {     
            var sFeatures;
            sFeatures="dialogWidth: 229px; ";
            sFeatures+="dialogHeight: 234px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("calendar.aspx","", sFeatures)
            if (result=='' || result==undefined)
            {
//            document.getElementById("txtDate").value=''; 
            return false;
            }
           if (pId==1)
          {  
           document.getElementById('<%=txtDocDate.ClientID %>').value=result;
          } 
         
           return true;
    }      
 
    </script>
<body  class="no_margin"> 
    <form id="form1" runat="server">
    <table align="center" border="0" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
        width="90%">
        <tbody>
            <tr id="tr_errLNE" runat="server">
                <td align="left">
                    <asp:Label ID="lblError" runat="server" SkinID="Error"></asp:Label>
                    <asp:Label ID="lblSuccess" runat="server" SkinID="Error"></asp:Label></td>
            </tr>
        </tbody>
    </table>
    <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
        width="90%">
        <tbody>
            <tr class="subheader_img">
                <td align="left" class="matters" colspan="6">
                    <strong><font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">Purchase Journals
                                &nbsp; &nbsp; &nbsp; </font></strong></td>
            </tr>
        </tbody>
    </table>
    <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
        width="90%">
        <tr>
            <td align="left" class="matters" width="15%">
                Doc No</td>
            <td class="matters" width="2%">
                :</td>
            <td align="left" class="matters" width="25%">
                <asp:TextBox ID="txtDocNo" runat="server" Style="left: -6px; position: relative;
                    top: 0px" Width="69%"></asp:TextBox></td>
            <td align="left" class="matters" width="15%">
                Doc Date</td>
            <td class="matters" width="2%">
                :</td>
            <td align="left" class="matters" style="width: 25%">
                <asp:TextBox ID="txtDocDate" runat="server" Style="left: -2px; position: relative;
                    top: 0px" Width="66%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" width="15%">
                Party Account</td>
            <td class="matters" width="2%">
                :</td>
            <td align="left" class="matters" colspan="4">
                <asp:TextBox ID="txtPartyCode" runat="server" Style="left: -6px; top: 0px" Width="20%"></asp:TextBox>&nbsp;<a
                    href="#" onclick="popUp('660','600','ALLACC','<%=txtPartyCode.ClientId %>','<%=txtPartyDescr.ClientId %>')"></a>
                <asp:TextBox ID="txtPartyDescr" runat="server" Style="left: 8px; position: relative;
                    top: 0px" Width="69%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" width="15%">
                Debit Account</td>
            <td class="matters" width="2%">
                :</td>
            <td align="left" class="matters" colspan="4">
                <asp:TextBox ID="txtDrCode" runat="server" Style="left: -6px; top: 0px" Width="20%"></asp:TextBox>&nbsp;<a
                    href="#" onclick="popUp('460','400','ALLACC','<%=txtDrCode.ClientId %>','<%=txtDrDescr.ClientId %>')"></a>
                <asp:TextBox ID="txtDrDescr" runat="server" Style="left: 8px; position: relative;
                    top: 0px" Width="69%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" width="15%" style="height: 36px">
                LPO No</td>
            <td class="matters" width="2%" style="height: 36px">
                :</td>
            <td align="left" class="matters" width="25%" style="height: 36px">
                <asp:TextBox ID="txtLPO" runat="server" Style="left: -6px; position: relative; top: 0px"
                    Width="69%"></asp:TextBox><a href="javascript:show_calendar('document.checkout.TCDate',%20document.checkout.TCDate.value);"></a></td>
            <td align="left" class="matters" width="15%" style="height: 36px">
                Invoice No</td>
            <td class="matters" width="2%" style="height: 36px">
                :</td>
            <td align="left" class="matters" style="width: 25%; height: 36px;">
                <asp:TextBox ID="txtInvoiceNo" runat="server" Style="left: -2px; position: relative;
                    top: 0px" Width="66%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" width="15%">
                Currency</td>
            <td class="matters" width="2%">
                :</td>
            <td align="left" class="matters" width="25%">
                <asp:DropDownList ID="cmbCurrency" runat="server" AutoPostBack="True" Style="left: -6px;
                    position: relative; top: 0px">
                </asp:DropDownList>
                &nbsp;
                <asp:TextBox ID="txtExchRate" runat="server" Style="left: -6px; top: 0px" Width="20%"></asp:TextBox></td>
            <td align="left" class="matters" width="15%">
                Local Rate</td>
            <td class="matters" width="2%">
                :</td>
            <td align="left" class="matters" style="width: 25%">
                <asp:TextBox ID="txtLocalRate" runat="server" Style="left: -6px; top: 0px" Width="20%"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
        width="90%">
        <tr>
            <td align="left" class="matters" width="18%">
                Narration</td>
            <td class="matters" width="2%">
                :</td>
            <td align="left" class="matters" colspan="4" style="width: 576px">
                <asp:TextBox ID="txtNarrn" runat="server" Style="left: -6px; top: 0px" Width="95%"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
        width="90%">
        <asp:HiddenField ID="hPLY" runat="server" />
        <asp:HiddenField ID="hCostReqd" runat="server" Value="1" />
        <tr>
            <td align="center" class="matters" colspan="6">
                <asp:GridView ID="gvDTL" runat="server" AutoGenerateColumns="False" BorderColor="#1B80B6"
                    DataKeyNames="id" EmptyDataText="No Rransaction details added yet." Font-Names="Verdana"
                    Font-Size="8pt" ForeColor="#1B80B6" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Id">
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="1%" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Accountid" HeaderText="Acount Code" ReadOnly="True">
                            <ItemStyle Width="10%" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Account Name">
                            <ItemTemplate>
                                <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="15%" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Item" HeaderText="Item">
                            <ItemStyle Width="25%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Qty" DataFormatString="{0:0.00}" HeaderText="Qty" HtmlEncode="False"
                            SortExpression="Qty">
                            <ItemStyle HorizontalAlign="Right" Width="5%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Rate" DataFormatString="{0:0.00}" HeaderText="Rate" HtmlEncode="False">
                            <ItemStyle HorizontalAlign="Right" Width="5%" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Amount" SortExpression="Amount" Visible="True">
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" SkinID="Grid" Text='<%# string.format("{0:0.00}", Eval("Amount")) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="GUID" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ply" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblPLY" runat="server" Text='<%# Bind("Ply") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CostReqd" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblCostReqd" runat="server" Text='<%# Bind("CostReqd") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edits"
                                   Text="Edit"> </asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                                Edit
                            </HeaderTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:LinkButton ID="DeleteBtn" runat="server" CommandArgument='<%# Eval("id") %>'
                                    CommandName="Delete">
         Delete</asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cost Center">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="8%" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hlAllocate" runat="server" Text="Allocate"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Height="23px" />
                    <SelectedRowStyle BackColor="Aqua" />
                    <HeaderStyle CssClass="gridheader_new" Height="25px" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                    <EmptyDataRowStyle CssClass="gridheader" Wrap="True" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="center" class="matters" colspan="6">
                <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close();"
                    Text="Close" Width="47px" />&nbsp;</td>
        </tr>
    </table>
    </form>
</body>
</html>

