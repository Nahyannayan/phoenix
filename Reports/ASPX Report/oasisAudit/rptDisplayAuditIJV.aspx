<%@ Page Language="VB"  AutoEventWireup="false" CodeFile="rptDisplayAuditIJV.aspx.vb" Inherits="Reports_ASPX_Report_oasisAudit_rptDisplayAuditIJV" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Accounts</title>
   <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../../cssfiles/example.css" rel="stylesheet" type="text/css" />
   <base target="_self" />
   
    </head>
   
   <body  class="no_margin"> 
    <form id="form1" runat="server">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">
        <tr valign="top">
            <td style="width: 782px; height: 821px">
                <table id="tbl_test" runat="server" align="center" border="1" bordercolor="#1b80b6"
                    cellpadding="5" cellspacing="0" width="100%">
                    <tr class="subheader_img">
                        <td align="left" colspan="9" style="height: 22px" valign="middle">
                            <strong><font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">InterUnit
                                Jounal Voucher &nbsp;&nbsp; </font></strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" style="width: 20%; height: 37px; text-align: left">
                            <strong>Doc No</strong></td>
                        <td class="matters" style="font-weight: bold; height: 37px">
                            :</td>
                        <td align="left" class="matters" colspan="3" style="font-weight: bold; width: 317px;
                            height: 37px; text-align: left">
                            <asp:TextBox ID="txtHDocno" runat="server" CssClass="inputbox" ReadOnly="True" Width="196px"></asp:TextBox>
                            <asp:CheckBox ID="chkMirrorEntry" runat="server" AutoPostBack="True" Text="Mirror Entry" /></td>
                        <td align="right" class="matters" style="width: 19%; height: 37px; text-align: left">
                            Doc Date &nbsp;
                        </td>
                        <td class="matters" style="width: 4%; height: 37px">
                            <strong>&nbsp;:</strong></td>
                        <td class="matters" colspan="2" style="font-weight: bold; width: 21%; height: 37px;
                            text-align: left">
                            <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="True" CssClass="inputbox"
                                Width="107px">12-12-2021</asp:TextBox>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" style="width: 20%; height: 37px; text-align: left">
                            Currency</td>
                        <td class="matters" style="height: 37px">
                            :</td>
                        <td class="matters" colspan="3" style="width: 317px; height: 37px; text-align: left">
                            <asp:DropDownList ID="DDCurrency" runat="server" AutoPostBack="True" CssClass="listbox"
                                Width="72px">
                            </asp:DropDownList>&nbsp; Differ Amount &nbsp;:
                            <asp:TextBox ID="txtDifferAmt" runat="server" ReadOnly="True" Width="97px"></asp:TextBox><asp:RegularExpressionValidator
                                ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtDifferAmt"
                                Display="Dynamic" ErrorMessage="Amount Should be Valid" Height="13px" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                ValidationGroup="Details" Width="1px">*</asp:RegularExpressionValidator></td>
                        <td align="right" class="matters" style="width: 19%; height: 37px; text-align: left">
                            Group Rate</td>
                        <td class="matters" style="width: 4%; height: 37px">
                            :</td>
                        <td class="matters" colspan="4" style="width: 20%; height: 37px; text-align: left">
                            <asp:TextBox ID="txtHGroupRate" runat="server" CssClass="inputbox" ReadOnly="True"
                                Width="110px"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtHGroupRate"
                                Display="Dynamic" ErrorMessage="Amount Should be Valid" Height="13px" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                ValidationGroup="Details" Width="1px">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td class="matters" style="width: 20%; height: 37px; text-align: left">
                            Business Unit (CR)</td>
                        <td class="matters" style="height: 37px">
                            :</td>
                        <td class="matters" colspan="4" style="height: 37px; text-align: left">
                            <asp:TextBox ID="txtBSUCR" runat="server" CssClass="inputbox" Width="290px"></asp:TextBox>
                            &nbsp; &nbsp; &nbsp; &nbsp; Exchange Rate</td>
                        <td class="matters" style="width: 4%; height: 37px">
                            :</td>
                        <td class="matters" colspan="2" style="width: 21%; height: 37px; text-align: left">
                            <asp:TextBox ID="txtHExchRateCR" runat="server" CssClass="inputbox" ReadOnly="True"
                                Width="111px"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtHExchRateCR"
                                Display="Dynamic" ErrorMessage="Amount Should be Valid" Height="13px" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                ValidationGroup="Details" Width="1px">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr style="color: #ffffff">
                        <td class="matters" style="width: 20%; height: 28px; text-align: left">
                            Business Unit (DR)</td>
                        <td class="matters" style="height: 28px">
                            :</td>
                        <td class="matters" colspan="4" style="height: 28px; text-align: left">
                            <asp:TextBox ID="txtBSUDR" runat="server" CssClass="inputbox" Width="289px"></asp:TextBox>
                            &nbsp; &nbsp; Exchange Rate</td>
                        <td class="matters" style="width: 4%; height: 28px">
                            :</td>
                        <td class="matters" colspan="2" style="width: 21%; height: 28px; text-align: left">
                            <asp:TextBox ID="txtHExchRateDR" runat="server" CssClass="inputbox" Width="111px"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtHExchRateDR"
                                Display="Dynamic" ErrorMessage="Amount Should be Valid" Height="13px" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                ValidationGroup="Details" Width="1px">*</asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td class="matters" style="width: 20%; text-align: left">
                            Narration</td>
                        <td class="matters">
                            <strong>:</strong></td>
                        <td class="matters" colspan="7" style="text-align: left">
                            <asp:TextBox ID="txtHNarration" runat="server" CssClass="inputbox" Width="589px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtHNarration"
                                ErrorMessage="Voucher Narration Cannot be blank" ValidationGroup="Details">*</asp:RequiredFieldValidator></td>
                    </tr>
                </table>
                <table id="Table1" align="center" border="1" bordercolor="#1b80b6" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="center" class="matters" colspan="9" width="95%">
                            <br />
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                EmptyDataText="No Transaction details added yet." Width="98%" CssClass="gridstyle">
                                <Columns>
                                    <asp:TemplateField HeaderText="id">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Accountid" HeaderText="Acount Code" ReadOnly="True" />
                                    <asp:TemplateField HeaderText="Account Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Accountname") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Narration" HeaderText="Narration" />
                                    <asp:BoundField DataField="Debit" DataFormatString="{0:0.00}" HeaderText="Debit"
                                        HtmlEncode="False" SortExpression="Debit">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Credit" DataFormatString="{0:0.00}" HeaderText="Credit"
                                        HtmlEncode="False">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edits"
                                                Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            Edit
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="CostCenter" HeaderText="TEST" />
                                    <asp:TemplateField HeaderText="Alloca" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnAlloca" runat="server" CausesValidation="false" CommandName=""
                                                Text="Alloca" Enabled="False"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cost Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRequired" runat="server" Text='<%# Bind("Required") %>' Visible="False"></asp:Label>
                                            <asp:Image ID="imgAllocate" runat="server" Visible="False" />&nbsp;
                                            <asp:LinkButton ID="lbAllocate" runat="server" Visible="False">Allocate</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" />
                                <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <EmptyDataRowStyle CssClass="gridheader" Wrap="True" />
                            </asp:GridView>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="9" style="height: 62px">
                            &nbsp;Debit Total :
                            <asp:TextBox ID="txtTDotalDebit" runat="server" CssClass="inputbox" ReadOnly="True"
                                Width="200px"></asp:TextBox>
                            &nbsp;&nbsp; Credit&nbsp; Total : &nbsp;<asp:TextBox ID="txtTotalCredit" runat="server"
                                CssClass="inputbox" ReadOnly="True" Width="200px"></asp:TextBox>
                            &nbsp; &nbsp;</td>
                    </tr>
                    <tr id="tr_SaveButtons" runat="server">
                        <td align="center" class="matters" colspan="9" style="height: 54px">
                            <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close();"
                                Text="Close" Width="47px" /><br />
                            <asp:HiddenField ID="h_Editid" runat="server" Value="-1" />
                            &nbsp;&nbsp;
                            <br />
                            <input id="h_Memberids" runat="server" type="hidden" />
                            <input id="h_mode" runat="server" type="hidden" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:HiddenField ID="h_BSUID_DR" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>
</body>
</html>

