<%@ Page Language="VB"  AutoEventWireup="false" CodeFile="rptDisplayAuditRJV.aspx.vb" Inherits="Reports_ASPX_Report_oasisAudit_rptDisplayAuditRJV" Title="::::GEMS OASIS:::: Online Student Administration System::::"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Accounts</title>
   <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../../cssfiles/example.css" rel="stylesheet" type="text/css" />
   <base target="_self" />
 
    </head>
   
   
    <script language="javascript" type="text/javascript">
function getDate(pId) 
   {     
            var sFeatures;
            sFeatures="dialogWidth: 229px; ";
            sFeatures+="dialogHeight: 234px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("calendar.aspx","", sFeatures)
            if (result=='' || result==undefined)
            {
//            document.getElementById("txtDate").value=''; 
            return false;
            }
           if (pId==1)
          {  
           document.getElementById('<%=txtDocDate.ClientID %>').value=result;
          } 
         
           return true;
    }      
 
    </script>
    <body  class="no_margin"> 
    <form id="form1" runat="server">

    <table align="center" border="0" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
        width="90%">
        <tbody>
            <tr id="tr_errLNE" runat="server">
                <td align="left">
                    <asp:Label ID="lblError" runat="server" SkinID="Error"></asp:Label>
                    <asp:Label ID="lblSuccess" runat="server" SkinID="Error"></asp:Label></td>
            </tr>
        </tbody>
    </table>
    <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
        width="90%">
        <tbody>
            <tr class="subheader_img">
                <td align="left" class="matters" colspan="6">
                    <strong><font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">Recurring JV
                                &nbsp; &nbsp; &nbsp; </font></strong></td>
            </tr>
        </tbody>
    </table>
    <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
        width="90%">
        <tr>
            <td align="left" class="matters" width="15%">
                Doc No</td>
            <td class="matters" width="2%">
                :</td>
            <td align="left" class="matters" width="25%">
                <asp:TextBox ID="txtDocNo" runat="server" Style="left: -6px; position: relative;
                    top: 0px" Width="69%"></asp:TextBox></td>
            <td align="left" class="matters" width="15%">
                Doc Date</td>
            <td class="matters" width="2%">
                :</td>
            <td align="left" class="matters" width="25%">
                <asp:TextBox ID="txtDocDate" runat="server" Style="left: -2px; position: relative;
                    top: 0px" Width="66%"></asp:TextBox>
                </td>
        </tr>
        <tr>
            <td align="left" class="matters" width="15%">
                No. of Installments</td>
            <td class="matters" width="2%">
                :</td>
            <td align="left" class="matters" colspan="4">
                <asp:TextBox ID="txtMonths" runat="server" Style="left: -6px; top: 0px" Width="10%"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left" class="matters" width="15%">
                Currency</td>
            <td class="matters" width="2%">
                :</td>
            <td align="left" class="matters" width="25%">
                <asp:DropDownList ID="cmbCurrency" runat="server" AutoPostBack="True" Style="left: -6px;
                    position: relative; top: 0px">
                </asp:DropDownList>
                &nbsp;
                <asp:TextBox ID="txtExchRate" runat="server" Style="left: -6px; top: 0px" Width="20%"></asp:TextBox></td>
            <td align="left" class="matters" width="15%">
                Local Rate</td>
            <td class="matters" width="2%">
                :</td>
            <td align="left" class="matters" width="25%">
                <asp:TextBox ID="txtLocalRate" runat="server" Style="left: -6px; top: 0px" Width="20%"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
        width="90%">
        <tr>
            <td align="left" class="matters" width="18%">
                Narration</td>
            <td class="matters" width="2%">
                :</td>
            <td align="left" class="matters" colspan="4">
                <asp:TextBox ID="txtNarrn" runat="server" Style="left: -6px; top: 0px" Width="95%"></asp:TextBox>
            </td>
        </tr>
    </table>
    &nbsp;
    <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
        width="90%">
        <tr>
            <td align="center" class="matters" colspan="6">
                <asp:GridView ID="gvDTL" runat="server" AutoGenerateColumns="False" BorderColor="#1B80B6"
                    DataKeyNames="id" EmptyDataText="No Rransaction details added yet." Font-Names="Verdana"
                    Font-Size="8pt" ForeColor="#1B80B6" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Id">
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="1%" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Accountid" HeaderText="Acount Code" ReadOnly="True">
                            <ItemStyle Width="10%" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Account Name">
                            <ItemTemplate>
                                <asp:Label ID="lblAccountname" runat="server" Text='<%# Bind("Accountname") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="15%" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Narration" HeaderText="Narration">
                            <ItemStyle Width="25%" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Debit" SortExpression="Debit">
                            <ItemTemplate>
                                <asp:Label ID="lblDebit" runat="server" SkinID="Grid" Text='<%# string.format("{0:0.00}", Eval("Debit")) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Credit" SortExpression="Credit">
                            <ItemTemplate>
                                <asp:Label ID="lblCredit" runat="server" SkinID="Grid" Text='<%# string.format("{0:0.00}", Eval("Credit")) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount" SortExpression="Amount" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" SkinID="Grid" Text='<%# string.format("{0:0.00}", Eval("Amount")) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="GUID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" Visible="False">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edits"
                                  Text="Edit"> </asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                                Edit
                            </HeaderTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete" Visible="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="DeleteBtn" runat="server" CommandArgument='<%# Eval("id") %>'
                                    CommandName="Delete">
         Delete</asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cost Center" Visible="False">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="8%" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hlAllocate" runat="server" 
                                    Text="Allocate"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Height="23px" />
                    <SelectedRowStyle BackColor="Aqua" />
                    <HeaderStyle CssClass="gridheader_new" Height="25px" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                    <EmptyDataRowStyle CssClass="gridheader" Wrap="True" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="center" class="matters" colspan="6" style="height: 36px">
                Debit Total:
                <asp:TextBox ID="txtDebit" runat="server"></asp:TextBox>
                &nbsp; Credit Total:
                <asp:TextBox ID="txtCredit" runat="server"></asp:TextBox><br />
                <br />
                <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close();"
                    Text="Close" Width="47px" /></td>
        </tr>
        <tr>
            <td align="center" class="matters" colspan="6">
    <asp:HiddenField ID="hPLY" runat="server" />
    <asp:HiddenField ID="hCostReqd" runat="server" Value="1" />
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</td>
        </tr>
    </table>
    </form>
</body>
</html>

