<%@ Page Language="VB" AutoEventWireup="false" CodeFile="rptDisplayAuditPP.aspx.vb" Inherits="Reports_ASPX_Report_oasisAudit_rptDisplayAuditPP" Title="::::GEMS OASIS:::: Online Student Administration System::::"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    
    <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../../cssfiles/example.css" rel="stylesheet" type="text/css" />

 <base target="_self" />
</head>
<body  class="no_margin">
    <form id="form1" runat="server">
    <div>
        &nbsp;</div>
        <table id="tbl_test" runat="server" align="center" border="1" bordercolor="#1b80b6"
            cellpadding="5" cellspacing="0" width="95%">
            <tr class="subheader_img">
                <td align="left" colspan="13" valign="middle">
                    <strong><font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">Pre Payment
                        Voucher</font></strong></td>
            </tr>
            <tr>
                <td align="left" class="matters" style="width: 31%; height: 28px">
                    <strong>Doc N</strong>o</td>
                <td class="matters" style="width: 4%; height: 28px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="3" style="height: 28px">
                    <asp:TextBox ID="txtHDocno" runat="server" CssClass="inputbox" Width="208px"></asp:TextBox>
                </td>
                <td align="left" class="matters" colspan="3" style="width: 185px; color: #1b80b6;
                    height: 28px">
                    Doc Date</td>
                <td align="center" class="matters" colspan="2" style="width: 21px; height: 28px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="5" style="height: 28px">
                    <asp:TextBox ID="txtHDocdate" runat="server" AutoPostBack="True" CssClass="inputbox"
                        Width="65%"></asp:TextBox>
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr style="color: #1b80b6">
                <td align="left" class="matters" style="width: 31%; height: 28px">
                    Currency</td>
                <td class="matters" style="width: 4%; height: 28px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="1" style="width: 38%; height: 28px">
                    <asp:DropDownList ID="DDCurrency" runat="server" AutoPostBack="True" CssClass="listbox"
                        Width="77px">
                    </asp:DropDownList></td>
                <td align="left" class="matters" style="width: 47%; height: 28px">
                    Exchange Rate</td>
                <td align="left" class="matters" style="width: 4%; height: 28px">
                    <strong>:</strong></td>
                <td align="left" class="matters" style="width: 3%; height: 28px">
                    <strong>
                        <asp:TextBox ID="txtHExchRate" runat="server" CssClass="inputbox" ReadOnly="True"
                            Width="88px"></asp:TextBox></strong></td>
                <td class="matters" colspan="5" style="height: 28px">
                    &nbsp;Group Rate</td>
                <td class="matters" style="width: 184px; height: 28px">
                    <strong>:</strong></td>
                <td align="left" class="matters" style="width: 52%; height: 28px">
                    <asp:TextBox ID="txtHGroupRate" runat="server" CssClass="inputbox" ReadOnly="True"
                        Width="100px"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="matters" style="width: 31%; height: 28px">
                    Cost Unit</td>
                <td class="matters" style="width: 4%; height: 28px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="12" style="height: 28px">
                    <asp:TextBox ID="txtCostUnit" runat="server" CssClass="inputbox" Width="260px"></asp:TextBox>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="left" class="matters" style="width: 31%; height: 9px">
                    Prepaid Account</td>
                <td class="matters" style="width: 4%; height: 9px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="3" style="height: 9px">
                    <asp:TextBox ID="txtPreAcc" runat="server" CssClass="inputbox" Width="260px"></asp:TextBox>
                    &nbsp;
                </td>
                <td align="left" class="matters" style="width: 147px; height: 9px">
                    Code
                </td>
                <td align="center" class="matters" colspan="2" style="width: 21px; height: 9px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="5" style="height: 9px">
                    <asp:TextBox ID="txtPreCode" runat="server" CssClass="inputbox" Width="88px"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="matters" style="width: 31%; height: 9px">
                    Expense Account</td>
                <td class="matters" style="width: 4%; height: 9px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="3" style="height: 9px">
                    <asp:TextBox ID="txtExpAcc" runat="server" CssClass="inputbox" Width="260px"></asp:TextBox>&nbsp;
                </td>
                <td align="left" class="matters" style="width: 147px; color: #1b80b6; height: 9px">
                    Code &nbsp;</td>
                <td align="center" class="matters" colspan="2" style="width: 21px; height: 9px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="5" style="height: 9px">
                    <asp:TextBox ID="txtExpCode" runat="server" CssClass="inputbox" Width="88px"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="matters" style="width: 31%; height: 21px">
                    Party Account</td>
                <td class="matters" style="width: 4%; height: 21px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="3" style="height: 21px">
                    <asp:TextBox ID="txtPartyAcc" runat="server" CssClass="inputbox" Width="260px"></asp:TextBox>
                    &nbsp;
                </td>
                <td align="left" class="matters" style="width: 147px; height: 21px">
                    Code</td>
                <td align="center" class="matters" colspan="2" style="width: 21px; height: 21px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="5" style="height: 21px">
                    <asp:TextBox ID="txtPartCode" runat="server" CssClass="inputbox" Width="88px"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="matters" style="width: 31%; height: 28px">
                    Invoice No</td>
                <td class="matters" style="width: 4%; height: 28px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="3" style="height: 28px">
                    <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="inputbox" Width="140px"></asp:TextBox>
                </td>
                <td align="left" class="matters" style="width: 147px; color: #1b80b6; height: 28px">
                    LP0</td>
                <td align="center" class="matters" colspan="2" style="width: 21px; height: 28px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="5" style="width: 52%; height: 28px">
                    <asp:TextBox ID="txtLpo" runat="server" CssClass="inputbox" Width="90%"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="matters" style="width: 31%; height: 28px">
                    Amount</td>
                <td class="matters" style="width: 4%; height: 28px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="12" style="height: 28px">
                    <asp:TextBox ID="txtAmount" runat="server" CssClass="inputbox" Width="20%"></asp:TextBox>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="left" class="matters" style="width: 31%; height: 28px">
                    Date From</td>
                <td class="matters" style="width: 4%; height: 28px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="3" style="height: 28px">
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputbox" Width="28%"></asp:TextBox>
                    &nbsp;&nbsp;
                </td>
                <td align="left" class="matters" style="width: 147px; height: 28px">
                    Date To</td>
                <td class="matters" colspan="2" style="width: 21px; height: 28px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="5" style="width: 52%; height: 28px">
                    <asp:TextBox ID="txtToDate" runat="server" CssClass="inputbox" Width="56%"></asp:TextBox>
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td align="left" class="matters" style="width: 31%; height: 28px">
                    Narration CR</td>
                <td class="matters" style="width: 4%; height: 28px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="23" style="height: 28px">
                    <asp:TextBox ID="txtNarration1" runat="server" CssClass="inputbox" Width="50%"></asp:TextBox><strong></strong></td>
            </tr>
            <tr>
                <td align="left" class="matters" style="width: 31%; height: 20px">
                    Narration DR</td>
                <td class="matters" style="width: 4%; height: 20px">
                    <strong>:</strong></td>
                <td align="left" class="matters" colspan="23" style="height: 20px">
                    <asp:TextBox ID="txtNarration2" runat="server" CssClass="inputbox" Width="50%"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" class="matters" colspan="25" style="height: 10%" valign="middle">
                    <br />
                    <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" CssClass="gridstyle"
                        Width="95%">
                        <EmptyDataRowStyle CssClass="gridheader" Wrap="True" />
                        <Columns>
                            <asp:BoundField DataField="PRD_PRP_ID" HeaderText="Document No" SortExpression="PRD_PRP_ID" />
                            <asp:BoundField DataField="PRD_DOCDT" DataFormatString="{0:dd MMM yyyy}" HeaderText="Document Date"
                                HtmlEncode="False" SortExpression="PRD_DOCDT" />
                            <asp:BoundField DataField="PRD_JVNO" HeaderText="Journal No" />
                            <asp:BoundField DataField="PRD_ALLOCAMT" DataFormatString="{0:#,0.00}" HeaderText="Amount"
                                HtmlEncode="False" SortExpression="PRD_ALLOCAMTT">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="View" ShowHeader="False" Visible="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbViewChild" runat="server" CausesValidation="false" Text="View"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="False">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("GUID") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGUID" runat="server" Text='<%# Bind("GUID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="griditem" Height="25px" />
                        <SelectedRowStyle BackColor="Aqua" />
                        <HeaderStyle CssClass="gridheader_new" Height="25px" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                    <br />
                    <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close();"
                        Text="Close" Width="47px" /></td>
            </tr>
        </table>
    </form>
</body>
</html>
