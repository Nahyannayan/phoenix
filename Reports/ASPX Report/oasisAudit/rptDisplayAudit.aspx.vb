Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class Reports_ASPX_Report_rptDisplayAudit
    Inherits System.Web.UI.Page
    'Shared dtJournal As DataTable
    'Shared MainMnu_code As String = String.Empty
    'Shared datamode As String = "none"
    'Shared menu_rights As Integer
    'Shared str_editData As String
    Dim Encr_decrData As New Encryption64
    'Shared idJournal, iDeleteCount As Integer
    Sub initialize_components()
        
        gvJournal.Attributes.Add("bordercolor", "#1b80b6")

        Session("dtJournal") = CreateDataTable()
        
        ViewState("idJournal") = 0
        
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                ' btnBack.Attributes.Add("onClick", "return go();")


                Dim Aud_ID As String = Encr_decrData.Decrypt(Request.QueryString("Aud_ID").Replace(" ", "+"))
                Dim DType As String = Encr_decrData.Decrypt(Request.QueryString("DType").Replace(" ", "+"))
                Dim Sel_Flag As String = Request.QueryString("Sel_Flag")
                Dim DocNo As String = Encr_decrData.Decrypt(Request.QueryString("DocNo").Replace(" ", "+"))
                Dim DocName As String = Encr_decrData.Decrypt(Request.QueryString("DocName").Replace(" ", "+"))
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                lblHeader.Text = DocName
                Select Case DType.ToUpper
                    Case "CN", "BP", "BR", "CC", "CR", "CP", "CD", "DN"

                        If Sel_Flag = "B" Then
                            Using AudID_reader As SqlDataReader = AccessRoleUser.GetAud_ID_Voucher(Aud_ID, DocNo, Sel_Flag)
                                If AudID_reader.HasRows = True Then
                                    While AudID_reader.Read
                                        Aud_ID = Convert.ToString(AudID_reader("ID"))
                                    End While
                                End If

                            End Using

                        End If
                        initialize_components()
                        set_viewdata(Aud_ID)

                        setViewData()
                        setModifyHeader(Aud_ID)
                        bind_Currency()
                End Select



            Catch ex As Exception
            UtilityObj.Errorlog("OASISAUDIT", ex.Message)
        End Try
        End If


    End Sub

    Private Function set_viewdata(ByVal Aud_ID As String) As String
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
            & " VHH_AUD_ID='" & Aud_ID & "'" _
            & " AND VHH_BSU_ID='" & Session("sBsuid") & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtHDocdate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0)("VHH_DOCDT"))

                txtHNarration.Text = ds.Tables(0).Rows(0)("VHH_NARRATION")
                Try
                    ViewState("str_editData") = ds.Tables(0).Rows(0)("VHH_DOCNO") & "|" _
                    & ds.Tables(0).Rows(0)("VHH_SUB_ID") & "|" _
                    & ds.Tables(0).Rows(0)("VHH_DOCDT") & "|" _
                    & ds.Tables(0).Rows(0)("VHH_FYEAR") & "|"
                    Return ""
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message)
                End Try
            End If
            Return ""
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
        Return True
    End Function

    Private Sub bind_Currency() 'bind the currency combo according to selected date
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            ' str_Sql = " SELECT A.EXG_ID,A.EXG_CUR_ID,A.EXG_RATE,B.EXG_RATE LOCAL_RATE,( str(B.EXG_RATE)+'__'+ str(a.EXG_RATE) ) as RATES" _

            str_Sql = "SELECT A.EXG_ID,A.EXG_CUR_ID," _
                        & " A.EXG_RATE,B.EXG_RATE LOCAL_RATE," _
                        & " (ltrim(str(B.EXG_RATE))+'__'+" _
                        & " ltrim(str(a.EXG_RATE) ))+'__'+" _
                        & " ltrim(str(a.EXG_ID)) as RATES " _
                        & " FROM EXGRATE_S A,EXGRATE_S B" _
                        & " Where A.EXG_BSU_ID='" & Session("sBsuid") & "'" _
                        & " AND '" & txtHDocdate.Text & "' BETWEEN A.EXG_FDATE AND ISNULL(A.EXG_TDATE , GETDATE())" _
                        & " AND " _
                        & " B.EXG_BSU_ID='" & Session("sBsuid") & "' AND B.EXG_TDATE IS NULL" _
                        & " AND " _
                        & " B.EXG_CUR_ID='DHS'"
            '& " order by gm.GPM_DESCR "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            DDCurrency.Items.Clear()
            DDCurrency.DataSource = ds.Tables(0)
            DDCurrency.DataTextField = "EXG_CUR_ID"
            DDCurrency.DataValueField = "RATES"
            DDCurrency.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then

                If set_default_currency() <> True Then

                    DDCurrency.SelectedIndex = 0

                    txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim

                    txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim

                End If

            Else

            End If
            'getnextdocid()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "AccPrepaymentAdd-Bind_currency")
        End Try
    End Sub

    Private Function set_default_currency() As Boolean

        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString

            Dim str_Sql As String

            str_Sql = "SELECT     BSU_ID," _
            & " BSU_SUB_ID, BSU_CURRENCY" _
            & " FROM vw_OSO_BUSINESSUNIT_M" _
            & " WHERE (BSU_ID = '" & Session("sBsuid") & "')"



            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)



            If ds.Tables(0).Rows.Count > 0 Then

                For Each item As ListItem In DDCurrency.Items

                    If item.Text = ds.Tables(0).Rows(0)("BSU_CURRENCY") Then

                        item.Selected = True

                        txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim

                        txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim

                        Return True

                        Exit For

                    End If

                Next

            Else

            End If

            Return False

        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message)

            Return False

        End Try

    End Function

    
    Private Function CreateDataTable() As DataTable ' made the jvdetail datatable structure
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Accountid", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Accountname", System.Type.GetType("System.String"))
            Dim cNarration As New DataColumn("Narration", System.Type.GetType("System.String"))
            Dim cCashflow As New DataColumn("Cashflow", System.Type.GetType("System.Decimal"))
            Dim cCashflowname As New DataColumn("Cashflowname", System.Type.GetType("System.String"))
            Dim cAmount As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cCostcenter As New DataColumn("Costcenter", System.Type.GetType("System.String"))

            'Dim cCollection As New DataColumn("Collection", System.Type.GetType("System.String"))
            'Dim cCollectionCode As New DataColumn("CollectionCode", System.Type.GetType("System.Decimal"))

            Dim cCCRequired As New DataColumn("Required", System.Type.GetType("System.Decimal"))
            Dim cStatus As New DataColumn("Status", System.Type.GetType("System.String"))
            Dim cGuid As New DataColumn("GUID", System.Type.GetType("System.Guid"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cNarration)
            dtDt.Columns.Add(cCashflow)
            dtDt.Columns.Add(cCashflowname)
            'dtDt.Columns.Add(cCollection)
            'dtDt.Columns.Add(cCollectionCode)

            dtDt.Columns.Add(cAmount)
            dtDt.Columns.Add(cCostcenter)
            dtDt.Columns.Add(cCCRequired)
            dtDt.Columns.Add(cStatus)
            dtDt.Columns.Add(cGuid)

            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function

    Private Sub setModifyCost(ByVal p_Modifyid As String) ' 'setting cost center table dtJournal (view/del)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT GUID, VDS_DOCTYPE ,VDS_DOCNO ," _
            & "VDS_ACT_ID ,VDS_CCS_ID ,VDS_CODE,VDS_DESCR ,VDS_AMOUNT," _
            & "VDS_SLNO,VDS_CCS_ID FROM VOUCHER_D_S WHERE VDS_DOCNO='" _
             & ViewState("str_editData").Split("|")(0) & "' and VDS_BDELETED='False' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim rDt As DataRow
                    'Dim i As Integer
                    'Dim str_actname_cost_mand As String = getAccountname(ds.Tables(0).Rows(iIndex)("JNL_ACT_ID"))
                    rDt = Session("dtCostChild").NewRow
                    rDt("GUID") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Id") = ds.Tables(0).Rows(iIndex)("GUID")

                    rDt("Memberid") = ds.Tables(0).Rows(iIndex)("VDS_CODE")
                    rDt("VoucherId") = ds.Tables(0).Rows(iIndex)("VDS_SLNO")
                    rDt("Costcenter") = ds.Tables(0).Rows(iIndex)("VDS_CCS_ID")
                    rDt("Name") = ds.Tables(0).Rows(iIndex)("VDS_DESCR")
                    rDt("Amount") = ds.Tables(0).Rows(iIndex)("VDS_AMOUNT")

                    'btnAdddetails.Visible = False

                    rDt("Status") = "Normal"
                    'idCostChild = idCostChild + 1
                    Session("dtCostChild").Rows.Add(rDt)
                Next
            Else
            End If
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub setModifyHeader(ByVal Aud_ID As String) 'setting header data on view/edit
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM VOUCHER_H where VHH_AUD_ID='" & Aud_ID & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ' txtHDocno.Text = p_Modifyid
                txtHDocdate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0)("VHH_DOCDT"))
                txtHNarration.Text = ds.Tables(0).Rows(0)("VHH_NARRATION")
                txtHDocno.Text = ds.Tables(0).Rows(0)("VHH_DOCNO")
                'txtHAccountcode.Text = ds.Tables(0).Rows(0)("VHH_ACT_ID")
                'txtHAccountname.Text = check_accountid(ds.Tables(0).Rows(0)("VHH_ACT_ID"))
                bind_Currency()
                Dim DType As String = Encr_decrData.Decrypt(Request.QueryString("DType").Replace(" ", "+"))
                setModifyDetails(Aud_ID, DType)
            Else
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub setViewData() 'setting controls on view/edit
        'btnEditcancel.Visible = False
        tr_SaveButtons.Visible = False

        'gvJournal.Columns(5).Visible = False
        gvJournal.Columns(6).Visible = False
        'gvJournal.Columns(6).Visible = False 
        gvJournal.Columns(9).Visible = False
        gvJournal.Columns(7).Visible = False

        DDCurrency.Enabled = False
        txtHDocdate.Attributes.Add("readonly", "readonly")
    End Sub


    Private Sub gridbind() ' bind the detail table grid
        Try
            Dim i As Integer
            Dim dtTempjournal As New DataTable
            dtTempjournal = CreateDataTable()
            Dim dDebit As Double = 0
            Dim dCredit As Double = 0
            'Dim dTotAmount As Double = 0
            Dim dAllocate As Double = 0
            If Session("dtJournal").Rows.Count > 0 Then
                For i = 0 To Session("dtJournal").Rows.Count - 1
                    If Session("dtJournal").Rows(i)("Status") & "" <> "Deleted" Then
                        Dim rDt As DataRow
                        rDt = dtTempjournal.NewRow
                        For j As Integer = 0 To Session("dtJournal").Columns.Count - 1
                            rDt.Item(j) = Session("dtJournal").Rows(i)(j)
                        Next
                        dCredit = dCredit + Session("dtJournal").Rows(i)("Amount")
                        ' dTotAmount = dTotAmount + dtCostChild.Rows(i)("Amount")
                        dtTempjournal.Rows.Add(rDt)
                    Else
                    End If
                Next
            End If
            gvJournal.DataSource = dtTempjournal
            gvJournal.DataBind()
            txtDTotalamount.Text = dCredit
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


    End Sub
    Private Sub setModifyDetails(ByVal Aud_ID As String, ByVal DocType As String) ' 'setting detail table dtJournal (view/del)
'vw_OSA_ACCOUNTS_M
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString
            Dim str_Sql As String
            'str_Sql = "select * FROM VOUCHER_D where VHD_DOCNO='" & str_editData.Split("|")(0) & "'" _
            str_Sql = "SELECT     VOUCHER_D.GUID, VOUCHER_D.VHD_SUB_ID," _
             & " VOUCHER_D.VHD_BSU_ID, VOUCHER_D.VHD_FYEAR," _
             & " VOUCHER_D.VHD_DOCTYPE, VOUCHER_D.VHD_DOCNO," _
             & " VOUCHER_D.VHD_LINEID, VOUCHER_D.VHD_ACT_ID," _
             & " VOUCHER_D.VHD_AMOUNT, VOUCHER_D.VHD_NARRATION," _
             & " VOUCHER_D.VHD_RSS_ID, VOUCHER_D.VHD_OPBAL," _
             & " vw_OSA_ACCOUNTS_M.ACT_ID, vw_OSA_ACCOUNTS_M.ACT_NAME," _
             & " vw_OSA_RPTSETUP_S.RSS_DESCR, vw_OSA_POLICY_M.PLY_BMANDATORY, " _
             & " ISNULL(vw_OSA_POLICY_M.PLY_COSTCENTER, 'AST') PLY_COSTCENTER " _
             & " FROM         VOUCHER_D INNER JOIN" _
             & " vw_OSA_ACCOUNTS_M ON " _
             & " VOUCHER_D.VHD_ACT_ID = vw_OSA_ACCOUNTS_M.ACT_ID" _
             & " AND VOUCHER_D.VHD_ACT_ID = vw_OSA_ACCOUNTS_M.ACT_ID " _
             & " INNER JOIN vw_OSA_POLICY_M ON " _
             & " vw_OSA_ACCOUNTS_M.ACT_PLY_ID = vw_OSA_POLICY_M.PLY_ID " _
             & " INNER JOIN vw_OSA_RPTSETUP_S ON" _
             & " VOUCHER_D.VHD_RSS_ID = vw_OSA_RPTSETUP_S.RSS_ID " _
             & " WHERE     (VOUCHER_D.VHD_SUB_ID = '" & Session("Sub_ID") & "')" _
             & " AND (VOUCHER_D.VHD_AUD_ID = '" & Aud_ID & "') " _
             & " AND (VOUCHER_D.VHD_BSU_ID = '" & Session("sBsuid") & "') " _
             & " AND (VOUCHER_D.VHD_DOCTYPE = '" & DocType & "')"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    'VHD_DEBIT, VHD_CREDIT, VHD_NARRATION, 
                    Dim rDt As DataRow
                    'Dim i As Integer
                    'Dim str_actname_cost_mand As String = getAccountname(ds.Tables(0).Rows(iIndex)("VHD_ACT_ID"))
                    rDt = Session("dtJournal").NewRow
                    rDt("GUID") = ds.Tables(0).Rows(iIndex)("GUID")
                    rDt("Id") = ds.Tables(0).Rows(iIndex)("VHD_LINEID")

                    ViewState("idJournal") = ViewState("idJournal") + 1
                    rDt("Accountid") = ds.Tables(0).Rows(iIndex)("VHD_ACT_ID")
                    rDt("Accountname") = ds.Tables(0).Rows(iIndex)("ACT_NAME")
                    rDt("Narration") = ds.Tables(0).Rows(iIndex)("VHD_NARRATION")

                    rDt("Amount") = ds.Tables(0).Rows(iIndex)("VHD_AMOUNT")
                    rDt("Cashflow") = ds.Tables(0).Rows(iIndex)("VHD_RSS_ID")
                    rDt("Cashflowname") = ds.Tables(0).Rows(iIndex)("RSS_DESCR")

                    'rDt("Collection") = ds.Tables(0).Rows(iIndex)("COL_DESCR")
                    'rDt("CollectionCode") = ds.Tables(0).Rows(iIndex)("VHD_COL_ID")

                    rDt("CostCenter") = ds.Tables(0).Rows(iIndex)("PLY_COSTCENTER")
                    rDt("Required") = ds.Tables(0).Rows(iIndex)("PLY_BMANDATORY")
                    rDt("Status") = "Normal"
                    Session("dtJournal").Rows.Add(rDt)
                Next
                setModifyCost(Aud_ID)
            Else

            End If
            ViewState("idJournal") = ViewState("idJournal") + 1

            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub DDCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDCurrency.SelectedIndexChanged
        txtHExchRate.Text = DDCurrency.SelectedItem.Value.Split("__")(0).Trim
        txtHLocalRate.Text = DDCurrency.SelectedItem.Value.Split("__")(2).Trim
    End Sub

 
   
End Class
