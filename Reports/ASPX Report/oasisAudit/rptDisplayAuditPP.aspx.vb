Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Reports_ASPX_Report_oasisAudit_rptDisplayAuditPP
    Inherits System.Web.UI.Page
    'Shared MainMnu_code As String
    'Shared menu_rights As Integer
    ' Shared datamode As String = "none"
    Dim Encr_decrData As New Encryption64
    'Shared content As ContentPlaceHolder
    ' Shared PRP_JVNO As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            Dim Aud_ID As String = Encr_decrData.Decrypt(Request.QueryString("Aud_ID").Replace(" ", "+"))
            Dim DType As String = Encr_decrData.Decrypt(Request.QueryString("DType").Replace(" ", "+"))
            Dim Sel_Flag As String = Request.QueryString("Sel_Flag")
            Dim DocNo As String = Encr_decrData.Decrypt(Request.QueryString("DocNo").Replace(" ", "+"))
            Dim DocName As String = Encr_decrData.Decrypt(Request.QueryString("DocName").Replace(" ", "+"))
            'MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            Select Case DType.ToUpper
                Case "PP"

                     If Sel_Flag = "B" Then
                        Using AudID_reader As SqlDataReader = AccessRoleUser.GetAud_ID_Voucher(Aud_ID, DocNo, Sel_Flag)
                            If AudID_reader.HasRows = True Then
                                While AudID_reader.Read
                                    Aud_ID = Convert.ToString(AudID_reader("ID"))
                                End While
                            End If

                        End Using

                    End If


            End Select

            txtHDocno.Attributes.Add("Readonly", "Readonly")
            txtCostUnit.Attributes.Add("Readonly", "Readonly")

            txtExpCode.Attributes.Add("Readonly", "Readonly")
            txtPreCode.Attributes.Add("Readonly", "Readonly")
            txtPartCode.Attributes.Add("Readonly", "Readonly")
            txtPartyAcc.Attributes.Add("Readonly", "Readonly")
            txtExpAcc.Attributes.Add("Readonly", "Readonly")
            txtPreAcc.Attributes.Add("Readonly", "Readonly")

            Using UserreaderPrepayment As SqlDataReader = AccessRoleUser.GetPrepaymentDetail_Aud_ID(Aud_ID)
                While UserreaderPrepayment.Read

                    'handle the null value returned from the reader incase  convert.tostring
                    txtHDocno.Text = Convert.ToString(UserreaderPrepayment("DocNo"))
                    txtPartCode.Text = Convert.ToString(UserreaderPrepayment("PRP_PARTY"))
                    txtPartyAcc.Text = Convert.ToString(UserreaderPrepayment("PARTY_NAME"))
                    ' hfId1.Value = Convert.ToString(UserreaderPrepayment("CostID"))
                    txtCostUnit.Text = Convert.ToString(UserreaderPrepayment("CostDescr"))
                    txtPreCode.Text = Convert.ToString(UserreaderPrepayment("PreAcc"))
                    txtPreAcc.Text = Convert.ToString(UserreaderPrepayment("PreAccDescr"))
                    txtExpCode.Text = Convert.ToString(UserreaderPrepayment("ExpAcc"))
                    txtExpAcc.Text = Convert.ToString(UserreaderPrepayment("ExpDescr"))
                    'Dim d As Date
                    'd = CDate(Convert.ToString(UserreaderPrepayment(8)))
                    txtHDocdate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime(UserreaderPrepayment("DOCDT")))
                    txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime(UserreaderPrepayment("FTFrom")))
                    txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime(UserreaderPrepayment("FTto")))

                    txtAmount.Text = Convert.ToDouble(UserreaderPrepayment("TAmt"))
                    txtInvoiceNo.Text = Convert.ToString(UserreaderPrepayment("InVoice"))
                    ' PRP_JVNO = Convert.ToString(UserreaderPrepayment("JvNo"))
                    txtNarration1.Text = Convert.ToString(UserreaderPrepayment("NR1"))
                    txtNarration2.Text = Convert.ToString(UserreaderPrepayment("NR2"))
                    txtLpo.Text = Convert.ToString(UserreaderPrepayment("LPO"))

                End While
                ' clear of the resource end using

                ' 
            End Using

            Call gridbindmonthwise(Aud_ID)
        End If
    End Sub

    Private Sub gridbindmonthwise(ByVal Aud_ID As String)

        Try


            gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn)

            objConn.Open()
            Try
                Dim str_Sql As String
                Dim str_guid As String = ""

                Dim lblGrpCode As New Label

                str_Sql = "select * FROM PREPAYMENTS_H where PRP_AUD_ID='" & Aud_ID & "' "

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then
                    str_Sql = "SELECT * FROM PREPAYMENTS_D WHERE PRD_AUD_ID='" & ds.Tables(0).Rows(0)("PRP_ID") & "'" _
                   & "  AND PRD_BSU_ID='" & ds.Tables(0).Rows(0)("PRP_BSU_ID") & "' and PRD_bDELETED='false'"
                    Dim dsc As New DataSet
                    dsc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                    gvDetails.DataSource = dsc

                    Dim helper As New GridViewHelper(Me.gvDetails)

                    helper.RegisterSummary("PRD_ALLOCAMT", SummaryOperation.Sum)


                    AddHandler helper.GeneralSummary, AddressOf helper_ManualSummary

                    gvDetails.DataBind()
                Else
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub helper_ManualSummary(ByVal row As GridViewRow)
        Try

            row.Cells(0).HorizontalAlign = HorizontalAlign.Right
            row.Cells(0).Text = "Total Amount:-"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
End Class
