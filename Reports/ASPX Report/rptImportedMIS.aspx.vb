Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports System.Data.SqlTypes
Imports UtilityObj
Partial Class Reports_ASPX_Report_rptImportedMIS
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                txtToDate.Text = UtilityObj.GetDiplayDate()
                lblrptCaption.Text = "Imported MIS Data"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            txtToDate.Text = Format("dd/MMM/yyyy", txtToDate.Text)
            RPTGetImportedMIS()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub RPTGetImportedMIS()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim cmd As New SqlCommand
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = objConn
        Dim strSelectedNodes As String = UsrBSUnits1.GetSelectedNode("|")
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(strSelectedNodes, str_bsu_Segment, _
        str_bsu_shortname, str_bsu_CURRENCY)


        cmd.CommandText = "RPT_ImportedMIS"
        Dim sqlParam(2) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", strSelectedNodes, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlParam(0))
        sqlParam(1) = Mainclass.CreateSqlParameter("@ToDate", CDate(txtToDate.Text).ToString("dd/MMM/yyyy"), SqlDbType.DateTime)
        cmd.Parameters.Add(sqlParam(1))
        sqlParam(2) = Mainclass.CreateSqlParameter("@Type", ddType.SelectedValue, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlParam(2))
        cmd.Connection = objConn

        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("ReportCaption") = "Imported MIS Data"
        params("fromDate") = UtilityObj.GetDataFromSQL("SELECT FYR_FROMDT FROM FINANCIALYEAR_S " _
        & " WHERE '" & txtToDate.Text & "' BETWEEN FYR_FROMDT AND FYR_TODT ", _
        WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        params("fromDate") = Format(CDate(params("fromDate")), OASISConstants.DateFormat)
        params("ToDate") = Format("dd/MMM/yyyy", txtToDate.Text)
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = str_bsu_shortname
        params("Bsu_Segments") = str_bsu_Segment
        Dim formulas As New Hashtable
        Dim c As New SqlConnection
        repSource.Parameter = params
        repSource.Formulas = formulas
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptImportedMIS.rpt"
        repSource.DisplayGroupTree = False
        Session("ReportSource") = repSource
        objConn.Close()
        Response.Redirect("rptviewer.aspx", True)
    End Sub


    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

End Class
