<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="CostcenterAnalysis.aspx.vb" Inherits="Reports_ASPX_Report_CostcenterAnalysis"  %>
<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script language="javascript" type="text/javascript">
     function ChangeCheckBoxState(id, checkState)
        {
            var cb = document.getElementById(id);
            if (cb != null)
               cb.checked = checkState;
        }
        
        function ChangeAllCheckBoxStates(checkState)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked; 
           
            
            if (CheckBoxIDs != null)
            {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                   ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        } 
   
         function GetBSUName()
          {     
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 445px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var mode = document.getElementById('<%=h_Mode.ClientID %>').value;
            
            if( mode == "cashFlow")
             {
                result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx?multiSelect=false","", sFeatures)
             }
            else
             {
                result = window.showModalDialog("../../Accounts/selBussinessUnit.aspx","", sFeatures)
             }
            if(result != "")
            {
              document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
              document.forms[0].submit();
            }
         }
         
           function GetAccounts()
        {
            var sFeatures;
            sFeatures="dialogWidth: 729px; ";
            sFeatures+="dialogHeight: 450px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var mode = 'general';
            if(mode == 'bank')
            {
                result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?bankcash=b&multiSelect=true&forrpt=1","", sFeatures)
            }
            else if(mode == 'cash')
            {
                result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?bankcash=c&multiSelect=true&forrpt=1","", sFeatures)
            }
            else if(mode == 'party')
            {
                result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=s&forrpt=1","", sFeatures)
            }
            else if(mode == 'general')
            {
                result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?multiSelect=true&mode=g&forrpt=1","", sFeatures)
            }
            else //if(mode == 'general')
            {
                result = window.showModalDialog("../../Accounts/accjvshowaccount.aspx?multiSelect=true&forrpt=1","", sFeatures)
            }
            if(result != "")
            {
                document.getElementById('<%=h_ACTIDs.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
                
            }
        }  
        
function getOtherCostCenter()
    { 
    
    var sFeatures;
    if ('<%=ddCostcenter.Selecteditem.value %>'=='others')
    { 
      sFeatures="dialogWidth: 529px; ";
      sFeatures+="dialogHeight: 634px; ";       
    }
    else
    {
      sFeatures="dialogWidth: 729px; ";
      sFeatures+="dialogHeight: 634px; ";
    }
    sFeatures+="help: no; ";
    sFeatures+="resizable: no; ";
    sFeatures+="scroll: yes; ";
    sFeatures+="status: yes; ";
    sFeatures+="unadorned: no; ";
    var NameandCode;
    var result;
    var url='PickCostObject.aspx?ccsmode=' + '<%=ddCostcenter.Selecteditem.value %>'+'&vid='+'<%=Request.QueryString("vid") %>'; 
    url=url+'&dt='+'12-sep-2007' ;
//alert(url);
    result = window.showModalDialog(url,window, sFeatures)
    document.getElementById('<%=h_Memberids.ClientID %>').value=result; 
    document.forms[0].submit();
    } 
    
     function getTEST() 
    { 
   
    var sFeatures;
     
    sFeatures="dialogWidth: 729px; ";
    sFeatures+="dialogHeight: 634px; "; 
    sFeatures+="help: no; ";
    sFeatures+="resizable: no; ";
    sFeatures+="scroll: yes; ";
    sFeatures+="status: yes; ";
    sFeatures+="unadorned: no; ";
    var urlName = "PickCodes.aspx"
    var OthSession = '<%=Session("CostOTH")%>'
    var FromDate = document.getElementById('<%=txtFromDate.ClientID %>' ).value
    var costCenter = document.getElementById('<%=ddCostcenter.ClientID %>' ).value
    if (costCenter == OthSession)
    urlName = "OthercostcenterShow.aspx"
    
   
    var NameandCode;
    var result;
    var url= urlName+ '?ccsmode=' + costCenter+'&vid='+'<%=Request.QueryString("vid") %>'; 
    url=url+'&dt='+FromDate ;
    //alert(url);
    result = window.showModalDialog(url,window, sFeatures)
   
    if (result=='' || result==undefined)
            {
            return false;
            }
      
        document.getElementById('<%=h_Memberids.ClientID %>').value=result; 
       
        document.forms[0].submit();  
  
    } 
       
    </script>
        
    <table align="center"class="BlueTable" cellpadding="5" cellspacing="0"
        style="width: 70%;">
        <tr class="subheader_img">
            <td align="left" colspan="8" style="height: 19px" valign="middle">            
            <asp:Label ID="lblCaption" runat="server" Text="Sub Ledger Report"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" class="matters">From Date</td>
            <td class="matters">:</td>
            <td align="left" class="matters" colspan="2" style="width: 189px; height: 1px; text-align: left">
                <asp:TextBox ID="txtFromDate" runat="server" Width="123px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
            <td align="left" class="matters" style="color: #1b80b6; height: 1px">
                To Date</td>
            <td align="left" class="matters">
                :</td>
            <td align="left" class="matters" colspan="2" style="height: 1px; text-align: left">
                <asp:TextBox ID="txtToDate" runat="server" Width="122px"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif" CausesValidation="False" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td align="left" class="matters" >
                Drill by</td>
            <td class="matters">
                :</td>
            <td align="left" class="matters" colspan="6" >
                &nbsp;<asp:RadioButton ID="rbAccount" runat="server" GroupName="cc" Text="Account" EnableTheming="True" Checked="True" />
                <asp:RadioButton ID="rbCostcenter" runat="server" GroupName="cc" Text="Cost Center" Enabled="False" /></td>
        </tr>
        <tr style="color: #1b80b6" id = "trBSUName" runat = "server">
            <td align="left" valign = "top" class="matters" >
                Business Unit</td>
            <td class="matters" valign = "top">
                :</td>
            <td align="left" class="matters" colspan="6" valign="top">
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left" class="matters"  valign="top">
                <asp:Label ID="lblBankCash" runat="server" Text="Account"></asp:Label></td>
            <td class="matters"  valign="top">
                :<br />
            </td>
            <td align="left" class="matters" colspan="6" valign="top">
                <asp:Label ID="lblACTIDCaption" runat="server" Text="(Enter the ACT ID you want to Add to the Search and click on Add)"
                    Width="377px"></asp:Label>
                <asp:TextBox ID="txtBankNames" runat="server" CssClass="inputbox" Height="18px" Width="330px"></asp:TextBox>
                <asp:LinkButton ID="lblAddActID" runat="server">Add</asp:LinkButton>
                <asp:ImageButton ID="imgBankSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetAccounts()" /><br />
                <asp:GridView ID="grdACTDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    PageSize="5" Width="377px">
                    <Columns>
                        <asp:TemplateField HeaderText="ACT ID">
                            <ItemTemplate>
                                <asp:Label ID="lblACTID" runat="server" Text='<%# Bind("ACT_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ACT_Name" HeaderText="ACT Name" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtngrdACTDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_small" />
                </asp:GridView>
            </td>
        </tr>
        <tr id="tr_CostCenter" runat="server" >
            <td align="left" class="matters" >
                Cost Center</td>
            <td class="matters">
                :</td>
            <td align="left" class="matters" colspan="6" >
                <asp:DropDownList ID="ddCostcenter" runat="server" Width="200px" >
                </asp:DropDownList>&nbsp;
            <asp:ImageButton ID="imgPickOther" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getOtherCostCenter()" />
                <asp:ImageButton ID="imgPickcost" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getTEST();" /></td>
        </tr>
        <tr>
            <td align="left" class="matters">
                Show By</td>
            <td class="matters">
                :</td>
            <td align="left" class="matters" colspan="6">
                <asp:DropDownList ID="ddAccountGroup" runat="server" Width="151px" AutoPostBack="True">
                    <asp:ListItem Value="ACC">By Account</asp:ListItem>
                    <asp:ListItem Value="CTRL">By Control Account</asp:ListItem>
                    <asp:ListItem Value="SUB">By Sub Group</asp:ListItem>
                    <asp:ListItem Value="GRP">By Group</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr style="color: #1b80b6" id = "trChkBoxes" runat = "server">
            <td align="left" class="matters" colspan="8" style="height: 8px">
                <asp:CheckBox ID="chkGroupCurrency" runat="server" Text="View in Group Currency" /><br />
                <asp:CheckBox ID="chkConsolidation" runat="server" Text="Consolidation" />
                </td>
        </tr>       
        <tr>
            <td align="left" class="matters" colspan="8" style="text-align: right">
                &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table> <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
   <asp:HiddenField ID="h_BSUID" runat="server" /><asp:HiddenField ID="h_ACTIDs" runat="server" />
    <asp:HiddenField ID="H_COSTIDS" runat="server" /><asp:HiddenField ID="h_Mode" runat="server" /><asp:HiddenField ID="h_Memberids" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
</asp:Content>

