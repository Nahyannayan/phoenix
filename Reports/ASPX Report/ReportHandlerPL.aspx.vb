Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports System.Net
Imports utilityobj


Partial Class Reports_ASPX_Report_ReportHandlerPL
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

        End If 
        Dim LEVEL As String = IIf(Request.QueryString("LVL") <> Nothing, Request.QueryString("LVL"), String.Empty)
        Dim Type As String = IIf(Request.QueryString("Type") <> Nothing, Request.QueryString("Type"), 0)
        Dim MODE As String = IIf(Request.QueryString("MODE") <> Nothing, Request.QueryString("MODE"), String.Empty)
        Dim CODE As String = IIf(Request.QueryString("CODE") <> Nothing, Request.QueryString("CODE"), String.Empty)
        Dim DESCR As String = IIf(Request.QueryString("DESCR") <> Nothing, Request.QueryString("DESCR"), String.Empty)
        Dim RSSPARENT As String = IIf(Request.QueryString("RSSPARENT") <> Nothing, Request.QueryString("RSSPARENT"), String.Empty)
        Dim BSUID As String = IIf(Request.QueryString("BSUID") <> Nothing, Request.QueryString("BSUID"), String.Empty)
        Dim OTHERID As String = IIf(Request.QueryString("OTHERID") <> Nothing, Request.QueryString("OTHERID"), String.Empty)
        Dim BSUSHORT As String = IIf(Request.QueryString("BSUSHORT") <> Nothing, Request.QueryString("BSUSHORT"), String.Empty)


        If MODE = "MC" And BSUSHORT <> String.Empty Then
            Dim Misparams As New MisParameters
            Misparams = Session("Misparams")

            Misparams.BSUShortnames = BSUSHORT

            Dim sql As String
            sql = "select BSU_ID from BUSINESSUNIT_M where BSU_SHORTNAME='" & BSUSHORT & "'"
            BSUID = Mainclass.getDataValue(sql, "OASISCOnnectionString")
            Misparams.BSU_IDs = BSUID
            Dim str_bsu_Segment As String = ""
            Dim str_bsu_shortname As String = ""
            Dim str_bsu_CURRENCY As String = ""
            getBsuSegmentSplit(BSUID, str_bsu_Segment, str_bsu_shortname, str_bsu_CURRENCY)
            Misparams.BSUShortnames = str_bsu_shortname
            Misparams.BSUSegments = str_bsu_Segment
            Session("Misparams") = Misparams
        End If

        Select Case (MODE)
            Case "PL" 'P n L Report
                RPTGetMISPLDrillDown_Level(Type, LEVEL, CODE, DESCR, RSSPARENT)
            Case "BS" '??
                RPTGetBSDrillDown_Level(Type, LEVEL, CODE, DESCR, RSSPARENT)
            Case "LEDGER" 'old one 
                GetLedger(CODE, BSUID)
            Case "MC" ' Cash Flow
                RPTGetMISCAshflow_DrillDown(CODE, "CASH FLOW", LEVEL, RSSPARENT)
            Case "MCD" ' DAILY Cash Flow
                RPTGetMISCashflowNew_Drilldown(CODE, "DAILY CASH FLOW", LEVEL, RSSPARENT)
            Case "CSTCNT" ' CostCenter
                RPTGetCostcenterDepartment(CODE, OTHERID, LEVEL, BSUID)
            Case "CSTCNT2" ' CostCenter
                CostcenterElement(Request.QueryString("BSUID"), Request.QueryString("DOCTYPE"), Request.QueryString("DOCNO"), Request.QueryString("FYEAR"))
            Case "PLCOLUMNAR" 'P n L Columnar
                RPTGetMISPLColumnarDrillDown_Level(Type, LEVEL, CODE, DESCR, RSSPARENT, BSUSHORT)
            Case "BSCOLUMNAR" 'P n L Columnar
                RPTGetBSColumnarDrillDown_Level(Type, LEVEL, CODE, DESCR, RSSPARENT, BSUSHORT)
            Case "IUADJ" 'InterUnit Adjustment
                RPTGetInterUnitAdjustment_Detail()
            Case "ADJJOURNAL" 'Adjustment Journal - Consolidation
                Dim ACTID As String = IIf(Request.QueryString("ACTID") <> Nothing, Request.QueryString("ACTID"), String.Empty)
                Dim OPPBSUID As String = IIf(Request.QueryString("OPPBSUID") <> Nothing, Request.QueryString("OPPBSUID"), String.Empty)
                RPTAdjustmentJOURNALLIST_sub(ACTID, OPPBSUID)
            Case "MCCOLUMNARNEW" ' Cash Flow
                RPTGetMISCashflowNew_Drilldown_BSUSHORT(CODE, "CASH FLOW", LEVEL, RSSPARENT, BSUSHORT)
            Case "PLPeriod"
                RPTGetMISPLDrillDown_Level_SHORT(Type, LEVEL, CODE, DESCR, RSSPARENT, BSUSHORT)
        End Select
    End Sub

    Private Sub RPTGetInterUnitAdjustment_Detail()
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim rptFilename As String = "../RPT_Files/rptGetInterUnitAdjDetail.rpt"
        Dim FromDate, Todate As String
        FromDate = CDate(IIf(IsDate(Request.QueryString("FromDate")), Request.QueryString("FromDate"), Now.ToShortDateString)).ToString("dd/MMM/yyyy")
        Todate = CDate(IIf(IsDate(Request.QueryString("Todate")), Request.QueryString("Todate"), Now.ToShortDateString)).ToString("dd/MMM/yyyy")
        Dim BSUID As String = IIf(Request.QueryString("BSUID") <> Nothing, Request.QueryString("BSUID"), String.Empty)
        Dim OPPBSUID As String = IIf(Request.QueryString("OPPBSUID") <> Nothing, Request.QueryString("OPPBSUID"), String.Empty)
        Dim Type As String = IIf(Request.QueryString("Type") <> Nothing, Request.QueryString("Type"), String.Empty)
        Dim ActRssID As String = IIf(Request.QueryString("ActRssID") <> Nothing, Request.QueryString("ActRssID"), String.Empty)

        Dim cmd As New SqlCommand
        cmd.CommandType = CommandType.Text
        cmd.Connection = objConn
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = objConn
        cmd.CommandText = "RPTInterUnitAdjustment_Detail"
        Dim sqlParam(5) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@Type", Type, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlParam(0))
        sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_ID", BSUID, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlParam(1))
        sqlParam(2) = Mainclass.CreateSqlParameter("@OPP_BSU_ID", OPPBSUID, SqlDbType.VarChar)
        cmd.Parameters.Add(sqlParam(2))
        sqlParam(3) = Mainclass.CreateSqlParameter("@DTFROM", FromDate, SqlDbType.DateTime)
        cmd.Parameters.Add(sqlParam(3))
        sqlParam(4) = Mainclass.CreateSqlParameter("@DTTO", Todate, SqlDbType.DateTime)
        cmd.Parameters.Add(sqlParam(4))
        If Type = "ACT" Then
            sqlParam(5) = Mainclass.CreateSqlParameter("@ActRssID", ActRssID, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(5))
        Else
            sqlParam(5) = Mainclass.CreateSqlParameter("@ActRssID", CInt(ActRssID), SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(5))
        End If
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("FromDate") = Format("dd/MMM/yyyy", FromDate)
        params("ToDate") = Format("dd/MMM/yyyy", Todate)
        params("ReportCaption") = "Inter Unit Adjustment Detail"
        params("decimal") = Session("BSU_ROUNDOFF")
        params("summary") = False
        repSource.Parameter = params
        Dim formulas As New Hashtable
        Dim c As New SqlConnection
        repSource.Formulas = formulas
        repSource.Command = cmd
        repSource.ResourceName = rptFilename
        Session("ReportSource") = repSource
        objConn.Close()
        Response.Redirect("rptviewer.aspx?newwindow=true", True)
        objConn.Close()
    End Sub
    Private Sub RPTGetBSColumnarDrillDown_Level(ByVal p_RssType As String, ByVal p_Level As Integer, _
    ByVal p_CODE As String, ByVal p_DESCR As String, ByVal p_RSSPARENT As String, ByVal BSUSHORT As String)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim Misparams As New MisParameters
        Misparams = Session("Misparams")
        Dim cmd As New SqlCommand("RPTGetMISPnL_S_BSUSHORT", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_SHORTNAME", SqlDbType.VarChar)
        If BSUSHORT = "" Then
            sqlpBSU_ID.Value = Misparams.BSUShortnames
        Else
            sqlpBSU_ID.Value = BSUSHORT
        End If
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpRSS_TYP As New SqlParameter("@RSS_TYP", SqlDbType.VarChar, 10)
        sqlpRSS_TYP.Value = p_RssType
        cmd.Parameters.Add(sqlpRSS_TYP)

        Dim sqlpLvl As New SqlParameter("@Lvl", SqlDbType.Int)
        sqlpLvl.Value = p_Level
        cmd.Parameters.Add(sqlpLvl)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = Misparams.FromDate
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = Misparams.ToDate
        cmd.Parameters.Add(sqlpDTTO)

        Dim sqlpRSS_CODE As New SqlParameter("@RSS_CODE", SqlDbType.VarChar, 30)
        sqlpRSS_CODE.Value = p_CODE
        cmd.Parameters.Add(sqlpRSS_CODE)

        Dim sqlpRSSPARENT As New SqlParameter("@RSSPARENT", SqlDbType.VarChar, 30)
        sqlpRSSPARENT.Value = p_RSSPARENT
        cmd.Parameters.Add(sqlpRSSPARENT)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", Misparams.ExcludeSpecialAccounts)
        cmd.Parameters.Add(sqlpBSpecial)

        objConn.Close()
        objConn.Open()
        If p_Level = 1 Then
            Misparams.BSUShortnames = BSUSHORT
            Misparams.RSSPARENT = p_CODE
            Misparams.BSUSegments = UtilityObj.GetDataFromSQL(" SELECT BSU_NAME FROM BUSINESSUNIT_M  " _
            & " WHERE BSU_SHORTNAME ='" & BSUSHORT & "'  ", ConnectionManger.GetOASISConnectionString)
            Session("Misparams") = Misparams
        End If
        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = Misparams.BSUShortnames
        params("Bsu_Segments") = Misparams.BSUSegments
        params("ToDate") = Misparams.ToDate
        params("fromDate") = Misparams.FromDate
        params("ReportCaption") = p_DESCR
        params("BSU_CURRENCY") = Misparams.Currency
        params("EXPRESSED_IN") = Misparams.ExpressedIn
        params("RSSPARENT") = Misparams.RSSPARENT
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/RPTGetMISBSPL_" & p_Level & ".rpt"
        Session("ReportSource") = repSource 
        objConn.Close()
        If Session("USR_MIS") = Nothing Then
            Response.Redirect("rptviewer.aspx?newwindow=true", True)
        Else
            If Session("USR_MIS") = 1 Or Session("USR_MIS") = 2 Then
                Response.Redirect("rptviewerMIS.aspx?newwindow=true", True)
            Else
                Response.Redirect("rptviewer.aspx?newwindow=true", True)
            End If
        End If
        objConn.Close()
    End Sub

    Private Sub RPTGetMISPLColumnarDrillDown_Level(ByVal p_RssType As String, ByVal p_Level As Integer, _
    ByVal p_CODE As String, ByVal p_DESCR As String, ByVal p_RSSPARENT As String, ByVal BSUSHORT As String)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim Misparams As New MisParameters
        Misparams = Session("Misparams")
        Dim cmd As New SqlCommand("RPTGetMISPnL_S_BSUSHORT", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_SHORTNAME", SqlDbType.VarChar)
        If BSUSHORT = "" Then
            sqlpBSU_ID.Value = Misparams.BSUShortnames
        Else
            sqlpBSU_ID.Value = BSUSHORT
        End If

        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpRSS_TYP As New SqlParameter("@RSS_TYP", SqlDbType.VarChar, 10)
        sqlpRSS_TYP.Value = p_RssType
        cmd.Parameters.Add(sqlpRSS_TYP)

        Dim sqlpLvl As New SqlParameter("@Lvl", SqlDbType.Int)
        sqlpLvl.Value = p_Level
        cmd.Parameters.Add(sqlpLvl)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = Misparams.FromDate
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = Misparams.ToDate
        cmd.Parameters.Add(sqlpDTTO)

        Dim sqlpRSS_CODE As New SqlParameter("@RSS_CODE", SqlDbType.VarChar, 30)
        sqlpRSS_CODE.Value = p_CODE
        cmd.Parameters.Add(sqlpRSS_CODE)

        Dim sqlpRSSPARENT As New SqlParameter("@RSSPARENT", SqlDbType.VarChar, 30)
        sqlpRSSPARENT.Value = p_RSSPARENT
        cmd.Parameters.Add(sqlpRSSPARENT)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", Misparams.ExcludeSpecialAccounts)
        cmd.Parameters.Add(sqlpBSpecial)

        objConn.Close()
        objConn.Open()
        If p_Level = 1 Then
            Misparams.BSUShortnames = BSUSHORT
            Misparams.RSSPARENT = p_CODE
            Misparams.BSUSegments = UtilityObj.GetDataFromSQL(" SELECT BSU_NAME FROM BUSINESSUNIT_M  " _
            & " WHERE BSU_SHORTNAME ='" & BSUSHORT & "'  ", ConnectionManger.GetOASISConnectionString)
            Session("Misparams") = Misparams
        End If
        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = Misparams.BSUShortnames
        params("Bsu_Segments") = Misparams.BSUSegments
        params("ToDate") = Misparams.ToDate
        params("fromDate") = Misparams.FromDate
        params("ReportCaption") = p_DESCR
        params("BSU_CURRENCY") = Misparams.Currency
        params("EXPRESSED_IN") = Misparams.ExpressedIn
        params("RSSPARENT") = Misparams.RSSPARENT
        repSource.Parameter = params
        repSource.Command = cmd

        repSource.ResourceName = "../RPT_Files/rptGetMISPnLColumnar_" & p_Level & ".rpt"
        Session("ReportSource") = repSource
        'Context.Items.Add("ReportSource", repSource)
        objConn.Close()
        If Session("USR_MIS") = Nothing Then
            Response.Redirect("rptviewer.aspx?newwindow=true", True)
        Else
            If Session("USR_MIS") = 1 Or Session("USR_MIS") = 2 Then
                Response.Redirect("rptviewerMIS.aspx?newwindow=true", True)
            Else
                Response.Redirect("rptviewer.aspx?newwindow=true", True)
            End If
        End If
        objConn.Close()
    End Sub

    Private Sub CostcenterElement(ByVal BSUID As String, ByVal DOCTYPE As String, _
            ByVal DOCNO As String, ByVal FYEAR As String)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)

        Dim spName As String = "[CostCenter_SubRpt]"
        Dim rptHead As String = "Cost Center Elements Details "
        Dim rptFilename As String = "../RPT_Files/CostcenterDeElements.rpt"
        Dim Misparams As New MisParameters
        Misparams = Session("Misparams")
        Dim cmd As New SqlCommand(spName, objConn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@VDS_SUB_ID", "007")
        cmd.Parameters.AddWithValue("@VDS_BSU_ID", BSUID)
        cmd.Parameters.AddWithValue("@VDS_FYEAR", Convert.ToDouble(FYEAR))
        cmd.Parameters.AddWithValue("@VDS_DOCTYPE", DOCTYPE)
        cmd.Parameters.AddWithValue("@VDS_DOCNO", Trim(DOCNO))

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("ReportHeading") = rptHead & Convert.ToDateTime(Misparams.FromDate).ToString("MMMM-yyyy")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = rptFilename
        Session("ReportSource") = repSource
        objConn.Close()

        If Session("USR_MIS") = Nothing Then
            Response.Redirect("rptviewer.aspx?newwindow=true", True)
        Else
            If Session("USR_MIS") = 1 Or Session("USR_MIS") = 2 Then
                Response.Redirect("rptviewerMIS.aspx?newwindow=true", True)
            Else
                Response.Redirect("rptviewer.aspx?newwindow=true", True)
            End If
        End If

        objConn.Close()
    End Sub

    Private Sub RPTGetCostcenterDepartment(ByVal p_CODE As String, ByVal p_OTHERID As String, _
        ByVal p_Level As Integer, ByVal BSUID As String)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)

        Dim spName As String = "[RPTGetCostCenterByDEpartment]"
        Dim rptHead As String = "Cost Detail for the month of "
        Dim rptFilename As String = "../RPT_Files/CostcenterDepartmentReportlevel2.rpt"
        Dim Misparams As New MisParameters
        Misparams = Session("Misparams")
        Dim DptId As String = Request.QueryString("DPTID")

        '-------------------------------------------
        If p_Level = 4 Then
            spName = "[RPTGetCostCenterByDEpartmentACwise]"
        End If

        Dim cmd As New SqlCommand(spName, objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 500)
        sqlpJHD_BSU_IDs.Value = BSUID
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = Misparams.FromDate
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = Misparams.ToDate
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpDTP_ID As New SqlParameter("@DTP_ID", SqlDbType.VarChar, 20)
        DptId = Convert.ToDouble(DptId).ToString()
        DptId = Math.Round(Convert.ToDouble(DptId), 0).ToString()
        sqlpDTP_ID.Value = DptId 'Misparams.RSSPARENT.Replace("||", "|")
        cmd.Parameters.Add(sqlpDTP_ID)

        Dim sqlpLevel As New SqlParameter("@Level", SqlDbType.TinyInt)
        sqlpLevel.Value = p_Level
        cmd.Parameters.Add(sqlpLevel)
        If p_Level = 4 Then
            Dim sqlpJDS_ACT_ID As New SqlParameter("@JDS_ACT_ID", SqlDbType.VarChar, 20)
            sqlpJDS_ACT_ID.Value = p_OTHERID
            cmd.Parameters.Add(sqlpJDS_ACT_ID)
        Else
            Dim sqlpRSS_CODE As New SqlParameter("@RSS_CODE", SqlDbType.VarChar, 20)
            sqlpRSS_CODE.Value = p_CODE
            cmd.Parameters.Add(sqlpRSS_CODE)

            Dim sqlpRSB_ID As New SqlParameter("@RSB_ID", SqlDbType.VarChar, 20)
            sqlpRSB_ID.Value = DBNull.Value
            If p_Level = 3 Then
                p_OTHERID = Convert.ToDouble(p_OTHERID).ToString()
                sqlpRSB_ID.Value = p_OTHERID
            End If
            cmd.Parameters.Add(sqlpRSB_ID)
        End If
        '-------------------------------------------
        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")

        If p_Level = 3 Then
            rptHead = "Cost with Accounts Detail for the month of "
            rptFilename = "../RPT_Files/CostcenterDepartmentReportlevel3.rpt"
        ElseIf p_Level = 4 Then
            rptHead = "Cost Accountwise Detail for the month of "
            rptFilename = "../RPT_Files/CostcenterDepartmentReportACwise.rpt"
        End If

        params("ReportHeading") = rptHead & Convert.ToDateTime(Misparams.FromDate).ToString("MMMM-yyyy")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = rptFilename
        Session("ReportSource") = repSource
        objConn.Close()

        If Session("USR_MIS") = Nothing Then
            Response.Redirect("rptviewer.aspx?newwindow=true", True)
        Else
            If Session("USR_MIS") = 1 Or Session("USR_MIS") = 2 Then
                Response.Redirect("rptviewerMIS.aspx?newwindow=true", True)
            Else
                Response.Redirect("rptviewer.aspx?newwindow=true", True)
            End If
        End If

        objConn.Close()
    End Sub

    Private Sub RPTGetMISCAshflow_DrillDown(ByVal p_CODE As String, ByVal p_DESCR As String, _
    ByVal p_Level As Integer, ByVal p_RSSPARENT As String)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)

        Dim Misparams As New MisParameters
        Misparams = Session("Misparams")
        Dim cmd As New SqlCommand("[RPTGetMISCAshflow_DrillDown]", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 400)
        sqlpBSU_ID.Value = Misparams.BSU_IDs
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = Misparams.ToDate
        cmd.Parameters.Add(sqlpDTTO)

        Dim sqlpRSS_CODE As New SqlParameter("@RSS_CODE", SqlDbType.VarChar, 30)
        sqlpRSS_CODE.Value = p_CODE
        cmd.Parameters.Add(sqlpRSS_CODE)

        Dim sqlpRSSPARENT As New SqlParameter("@RSSPARENT", SqlDbType.VarChar, 30)
        sqlpRSSPARENT.Value = p_RSSPARENT
        cmd.Parameters.Add(sqlpRSSPARENT)

        Dim sqlpLvl As New SqlParameter("@Lvl", SqlDbType.Int)
        sqlpLvl.Value = p_Level
        cmd.Parameters.Add(sqlpLvl)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = Misparams.BSUShortnames
        params("Bsu_Segments") = Misparams.BSUSegments
        params("ToDate") = Misparams.ToDate
        'params("fromDate") = Misparams.FromDate
        params("ReportCaption") = p_DESCR
        params("BSU_CURRENCY") = Misparams.Currency
        params("EXPRESSED_IN") = Misparams.ExpressedIn
        If p_Level = 1 Then
            Misparams.RSSPARENT = p_CODE
            Session("Misparams") = Misparams
        End If
        params("RSSPARENT") = Misparams.RSSPARENT
        repSource.Parameter = params
        repSource.Command = cmd
        If p_Level = 1 Then
            Select Case p_CODE
                Case "N011", "N014", "N017", "N020", "N022", "N024", "N026", "N028"
                    repSource.ResourceName = "../RPT_Files/RPTGetMISCF_DRILLD_ACT.rpt"
                    'Case "N001", "N008", "N005"
                Case Else
                    repSource.ResourceName = "../RPT_Files/RPTGetMISCF_DRILLD_BSU.rpt"
            End Select
        Else
            repSource.ResourceName = "../RPT_Files/rptGetMISPnLDailycashflow_" & p_Level & ".rpt"
        End If
        Session("ReportSource") = repSource
        objConn.Close()

        If Session("USR_MIS") = Nothing Then
            Response.Redirect("rptviewer.aspx?newwindow=true", True)
        Else
            If Session("USR_MIS") = 1 Or Session("USR_MIS") = 2 Then
                Response.Redirect("rptviewerMIS.aspx?newwindow=true", True)
            Else
                Response.Redirect("rptviewer.aspx?newwindow=true", True)
            End If
        End If

        objConn.Close()
    End Sub

    Private Sub RPTGetMISPLDrillDown_Level(ByVal p_RssType As String, ByVal p_Level As Integer, _
    ByVal p_CODE As String, ByVal p_DESCR As String, ByVal p_RSSPARENT As String)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim Misparams As New MisParameters
        Misparams = Session("Misparams")
        Dim cmd As New SqlCommand("RPTGetMISPnL_S", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 400)
        sqlpBSU_ID.Value = Misparams.BSU_IDs
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpRSS_TYP As New SqlParameter("@RSS_TYP", SqlDbType.VarChar, 10)
        sqlpRSS_TYP.Value = p_RssType
        cmd.Parameters.Add(sqlpRSS_TYP)

        Dim sqlpLvl As New SqlParameter("@Lvl", SqlDbType.Int)
        sqlpLvl.Value = p_Level
        cmd.Parameters.Add(sqlpLvl)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = Misparams.FromDate
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = Misparams.ToDate
        cmd.Parameters.Add(sqlpDTTO)

        Dim sqlpRSS_CODE As New SqlParameter("@RSS_CODE", SqlDbType.VarChar, 30)
        sqlpRSS_CODE.Value = p_CODE
        cmd.Parameters.Add(sqlpRSS_CODE)

        Dim sqlpRSSPARENT As New SqlParameter("@RSSPARENT", SqlDbType.VarChar, 30)
        sqlpRSSPARENT.Value = p_RSSPARENT
        cmd.Parameters.Add(sqlpRSSPARENT)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", Misparams.ExcludeSpecialAccounts)
        cmd.Parameters.Add(sqlpBSpecial)

        objConn.Close()
        objConn.Open()
        If p_Level = 1 Then
            Misparams.RSSPARENT = p_CODE
            Session("Misparams") = Misparams
        End If
        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = Misparams.BSUShortnames
        params("Bsu_Segments") = Misparams.BSUSegments
        params("ToDate") = Misparams.ToDate
        params("fromDate") = Misparams.FromDate
        params("ReportCaption") = p_DESCR
        params("BSU_CURRENCY") = Misparams.Currency
        params("EXPRESSED_IN") = Misparams.ExpressedIn
        params("RSSPARENT") = Misparams.RSSPARENT
        repSource.Parameter = params
        repSource.Command = cmd

        repSource.ResourceName = "../RPT_Files/rptGetMISPnLBudget_" & p_Level & ".rpt"
        Session("ReportSource") = repSource
        'Context.Items.Add("ReportSource", repSource)
        objConn.Close()
        If Session("USR_MIS") = Nothing Then
            Response.Redirect("rptviewer.aspx?newwindow=true", True)
        Else
            If Session("USR_MIS") = 1 Or Session("USR_MIS") = 2 Then
                Response.Redirect("rptviewerMIS.aspx?newwindow=true", True)
            Else
                Response.Redirect("rptviewer.aspx?newwindow=true", True)
            End If
        End If
        objConn.Close()
    End Sub

    Private Sub RPTGetBSDrillDown_Level(ByVal p_RssType As String, ByVal p_Level As Integer, _
        ByVal p_CODE As String, ByVal p_DESCR As String, ByVal p_RSSPARENT As String)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        Dim Misparams As New MisParameters
        Misparams = Session("Misparams")
        Dim cmd As New SqlCommand("RPTGetMISPnL_S", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 400)
        sqlpBSU_ID.Value = Misparams.BSU_IDs
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpRSS_TYP As New SqlParameter("@RSS_TYP", SqlDbType.VarChar, 10)
        sqlpRSS_TYP.Value = p_RssType
        cmd.Parameters.Add(sqlpRSS_TYP)

        Dim sqlpLvl As New SqlParameter("@Lvl", SqlDbType.Int)
        sqlpLvl.Value = p_Level
        cmd.Parameters.Add(sqlpLvl)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = Misparams.FromDate
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = Misparams.ToDate
        cmd.Parameters.Add(sqlpDTTO)

        Dim sqlpRSS_CODE As New SqlParameter("@RSS_CODE", SqlDbType.VarChar, 30)
        sqlpRSS_CODE.Value = p_CODE
        cmd.Parameters.Add(sqlpRSS_CODE)

        Dim sqlpRSSPARENT As New SqlParameter("@RSSPARENT", SqlDbType.VarChar, 30)
        sqlpRSSPARENT.Value = p_RSSPARENT
        cmd.Parameters.Add(sqlpRSSPARENT)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", Misparams.ExcludeSpecialAccounts)
        cmd.Parameters.Add(sqlpBSpecial)

        objConn.Close()
        objConn.Open()
        If p_Level = 1 Then
            Misparams.RSSPARENT = p_CODE
            Session("Misparams") = Misparams
        End If
        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = Misparams.BSUShortnames
        params("Bsu_Segments") = Misparams.BSUSegments
        params("ToDate") = Misparams.ToDate
        params("fromDate") = Misparams.FromDate
        params("ReportCaption") = p_DESCR
        params("BSU_CURRENCY") = Misparams.Currency
        params("EXPRESSED_IN") = Misparams.ExpressedIn
        params("RSSPARENT") = Misparams.RSSPARENT
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/RPTGetMISBS_" & p_Level & ".rpt"
        Session("ReportSource") = repSource
        'Context.Items.Add("ReportSource", repSource)
        objConn.Close()
        If Session("USR_MIS") = Nothing Then
            Response.Redirect("rptviewer.aspx?newwindow=true", True)
        Else
            If Session("USR_MIS") = 1 Or Session("USR_MIS") = 2 Then
                Response.Redirect("rptviewerMIS.aspx?newwindow=true", True)
            Else
                Response.Redirect("rptviewer.aspx?newwindow=true", True)
            End If
        End If
        objConn.Close()
    End Sub

    Private Sub GetLedger(ByVal ACTName As String, ByVal BSUID As String)
        Dim Misparams As New MisParameters
        Misparams = Session("Misparams")
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)

        Dim cmd As New SqlCommand("RPTLEDGER", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@JHD_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = GenerateXML(BSUID, XMLType.BSUName)
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        sqlpACT_IDs.Value = GenerateXML(ACTName, XMLType.ACTName)
        cmd.Parameters.Add(sqlpACT_IDs)

        Dim sqlpJHD_FYEAR As New SqlParameter("@JHD_FYEAR", SqlDbType.Int)
        sqlpJHD_FYEAR.Value = CDate(Misparams.FromDate).Year
        cmd.Parameters.Add(sqlpJHD_FYEAR)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = Misparams.FromDate
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@TODOCDT", SqlDbType.DateTime)
        sqlpTODOCDT.Value = Misparams.ToDate
        cmd.Parameters.Add(sqlpTODOCDT)

        'Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        'sqlpConsolidation.Value = chkConsolidation.Checked
        'cmd.Parameters.Add(sqlpConsolidation)

        Dim sqlpGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = 0
        cmd.Parameters.Add(sqlpGroupCurrency)

        'adpt.SelectCommand = cmd
        objConn.Open()
        'adpt.Fill(ds)
        'If ds IsNot Nothing And ds.Tables(0).Rows.Count > 0 Then
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("FromDate") = Misparams.FromDate
        params("ToDate") = Misparams.ToDate
        params("ReportCaption") = "General Ledger"
        params("summary") = False
        params("decimal") = Session("BSU_ROUNDOFF")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptCashBook.rpt"
        Session("ReportSource") = repSource
        objConn.Close()
        'End If
        'objConn.Close()
        'Response.Redirect("rptviewer.aspx?newwindow=true", True)

        If Session("USR_MIS") = Nothing Then
            Response.Redirect("rptviewer.aspx?newwindow=true", True)
        Else
            If Session("USR_MIS") = 1 Or Session("USR_MIS") = 2 Then
                Response.Redirect("rptviewerMIS.aspx?newwindow=true", True)
            Else
                Response.Redirect("rptviewer.aspx?newwindow=true", True)
            End If
        End If
    End Sub

    Private Sub RPTAdjustmentJOURNALLIST_sub(ByVal ACTID As String, ByVal OppBSU_ID As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand
        Dim Misparams As New MisParameters
        Misparams = Session("Misparams")
        cmd = New SqlCommand("[RPTAdjustmentJOURNALLIST_sub]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSU_IDs", SqlDbType.VarChar)
        sqlpJHD_BSU_IDs.Value = Misparams.BSU_IDs
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.VarChar)
        sqlpACT_IDs.Value = ACTID
        cmd.Parameters.Add(sqlpACT_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = Misparams.FromDate
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = Misparams.ToDate
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpOppBSU_ID As New SqlParameter("@OppBSU_ID", SqlDbType.VarChar)
        sqlpOppBSU_ID.Value = OppBSU_ID
        cmd.Parameters.Add(sqlpOppBSU_ID)

        objConn.Close()
        objConn.Open()

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = Misparams.FromDate
        params("ToDate") = Misparams.ToDate

        repSource.ResourceName = "../RPT_Files/rptAdjustmentJOURNALLIST_PL_Consolidation_detail.rpt"
        params("RPT_CAPTION") = "Adjustment Journal - Details"

        repSource.Parameter = params
        repSource.Command = cmd

        Session("ReportSource") = repSource
        'Context.Items.Add("ReportSource", repSource)
        objConn.Close()
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            Response.Redirect("rptviewer.aspx?isExport=true", True)
        Else
            Response.Redirect("rptviewer.aspx", True)
        End If
        objConn.Close()
    End Sub

    Private Sub RPTGetMISCashflowNew_Drilldown(ByVal p_CODE As String, ByVal p_DESCR As String, _
    ByVal p_Level As Integer, ByVal p_RSSPARENT As String)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)

        Dim Misparams As New MisParameters
        Misparams = Session("Misparams")
        Dim cmd As New SqlCommand("[RPTGetMISCashflowNew_Drilldown]", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlpBSU_ID.Value = Misparams.BSU_IDs
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = Misparams.ToDate
        cmd.Parameters.Add(sqlpDTTO)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = Misparams.FromDate
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpRSS_CODE As New SqlParameter("@RSS_CODE", SqlDbType.VarChar, 30)
        sqlpRSS_CODE.Value = p_CODE
        cmd.Parameters.Add(sqlpRSS_CODE)

        Dim sqlpRSSPARENT As New SqlParameter("@RSSPARENT", SqlDbType.VarChar, 30)
        sqlpRSSPARENT.Value = p_RSSPARENT
        cmd.Parameters.Add(sqlpRSSPARENT)

        Dim sqlpLvl As New SqlParameter("@Lvl", SqlDbType.Int)
        sqlpLvl.Value = p_Level
        cmd.Parameters.Add(sqlpLvl)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass

        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = Misparams.BSUShortnames
        params("Bsu_Segments") = Misparams.BSUSegments
        params("ToDate") = Misparams.ToDate
        'params("fromDate") = Misparams.FromDate
        params("ReportCaption") = p_DESCR
        params("BSU_CURRENCY") = Misparams.Currency
        params("EXPRESSED_IN") = Misparams.ExpressedIn
        If p_Level = 1 Then
            Misparams.RSSPARENT = p_CODE
            Session("Misparams") = Misparams
        End If
        params("RSSPARENT") = Misparams.RSSPARENT
        repSource.Parameter = params
        repSource.Command = cmd
        If p_Level = 1 Then
            Select Case p_CODE
                Case "N011", "N014", "N017", "N020", "N022", "N024", "N026", "N028"
                    repSource.ResourceName = "../RPT_Files/RPTGetMISCF_DRILLD_ACT.rpt"
                    'Case "N001", "N008", "N005"
                Case Else
                    repSource.ResourceName = "../RPT_Files/RPTGetMISCF_DRILLD_BSU_daily.rpt"
            End Select
        Else
            repSource.ResourceName = "../RPT_Files/rptGetMISPnLDailycashflow_" & p_Level & ".rpt"
        End If
        Session("ReportSource") = repSource
        objConn.Close()

        If Session("USR_MIS") = Nothing Then
            Response.Redirect("rptviewer.aspx?newwindow=true", True)
        Else
            If Session("USR_MIS") = 1 Or Session("USR_MIS") = 2 Then
                Response.Redirect("rptviewerMIS.aspx?newwindow=true", True)
            Else
                Response.Redirect("rptviewer.aspx?newwindow=true", True)
            End If
        End If

        objConn.Close()
    End Sub

    Private Sub RPTGetMISCashflowNew_Drilldown_BSUSHORT(ByVal p_CODE As String, ByVal p_DESCR As String, _
    ByVal p_Level As Integer, ByVal p_RSSPARENT As String, ByVal BSUSHORT As String)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)

        Dim Misparams As New MisParameters
        Misparams = Session("Misparams")
        Dim cmd As New SqlCommand("[RPTGetMISCashflowNew_Drilldown_BSUSHORT]", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_SHORTNAME", SqlDbType.VarChar)
        If BSUSHORT = "" Then
            sqlpBSU_ID.Value = Misparams.BSUShortnames
        Else
            sqlpBSU_ID.Value = BSUSHORT
        End If
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = Misparams.ToDate
        cmd.Parameters.Add(sqlpDTTO)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = Misparams.FromDate
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpRSS_CODE As New SqlParameter("@RSS_CODE", SqlDbType.VarChar, 30)
        sqlpRSS_CODE.Value = p_CODE
        cmd.Parameters.Add(sqlpRSS_CODE)

        Dim sqlpRSSPARENT As New SqlParameter("@RSSPARENT", SqlDbType.VarChar, 30)
        sqlpRSSPARENT.Value = p_RSSPARENT
        cmd.Parameters.Add(sqlpRSSPARENT)

        Dim sqlpLvl As New SqlParameter("@Lvl", SqlDbType.Int)
        sqlpLvl.Value = p_Level
        cmd.Parameters.Add(sqlpLvl)

        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        If p_Level = 1 Then
            Misparams.BSUShortnames = BSUSHORT
            Misparams.RSSPARENT = p_CODE
            Misparams.BSUSegments = UtilityObj.GetDataFromSQL(" SELECT BSU_NAME FROM BUSINESSUNIT_M  " _
            & " WHERE BSU_SHORTNAME ='" & BSUSHORT & "'  ", ConnectionManger.GetOASISConnectionString)
            Session("Misparams") = Misparams
        End If
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = Misparams.BSUShortnames
        params("Bsu_Segments") = Misparams.BSUSegments
        params("ToDate") = Misparams.ToDate
        'params("fromDate") = Misparams.FromDate
        params("ReportCaption") = p_DESCR
        params("BSU_CURRENCY") = Misparams.Currency
        params("EXPRESSED_IN") = Misparams.ExpressedIn
        If p_Level = 1 Then
            Misparams.RSSPARENT = p_CODE
            Session("Misparams") = Misparams
        End If
        params("RSSPARENT") = Misparams.RSSPARENT
        repSource.Parameter = params
        repSource.Command = cmd

        If p_Level = 1 Then
            Select Case p_CODE
                Case "N011", "N014", "N017", "N020", "N022", "N024", "N026", "N028"
                    repSource.ResourceName = "../RPT_Files/RPTGetMISCF_DRILLD_ACT.rpt"
                    'Case "N001", "N008", "N005"
                Case Else
                    repSource.ResourceName = "../RPT_Files/RPTGetMISCF_DRILLD_BSU_daily.rpt"
            End Select
        Else
            repSource.ResourceName = "../RPT_Files/rptGetMISPnLDailycashflow_" & p_Level & ".rpt"
        End If
        Session("ReportSource") = repSource
        objConn.Close()

        If Session("USR_MIS") = Nothing Then
            Response.Redirect("rptviewer.aspx?newwindow=true", True)
        Else
            If Session("USR_MIS") = 1 Or Session("USR_MIS") = 2 Then
                Response.Redirect("rptviewerMIS.aspx?newwindow=true", True)
            Else
                Response.Redirect("rptviewer.aspx?newwindow=true", True)
            End If
        End If

        objConn.Close()
    End Sub

    Private Sub RPTGetMISPLDrillDown_Level_SHORT(ByVal p_RssType As String, ByVal p_Level As Integer, _
    ByVal p_CODE As String, ByVal p_DESCR As String, ByVal p_RSSPARENT As String, ByVal BSUSHORT As String)

        Dim BSUID As String
        Dim Misparams As New MisParameters
        Misparams = Session("Misparams")

        Misparams.BSUShortnames = BSUSHORT

        Dim sql As String
        sql = "select BSU_ID from BUSINESSUNIT_M where BSU_SHORTNAME='" & BSUSHORT & "'"
        BSUID = Mainclass.getDataValue(sql, "OASISCOnnectionString")
        Misparams.BSU_IDs = BSUID
        Dim str_bsu_Segment As String = ""
        Dim str_bsu_shortname As String = ""
        Dim str_bsu_CURRENCY As String = ""
        getBsuSegmentSplit(BSUID, str_bsu_Segment, str_bsu_shortname, str_bsu_CURRENCY)
        Misparams.BSUShortnames = str_bsu_shortname
        Misparams.BSUSegments = str_bsu_Segment
        Session("Misparams") = Misparams


        Dim objConn As New SqlConnection(ConnectionManger.GetOASISFINConnectionString)
        'Dim Misparams As New MisParameters
        Misparams = Session("Misparams")
        Dim cmd As New SqlCommand("RPTGetMISPnL_S", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 400)
        sqlpBSU_ID.Value = Misparams.BSU_IDs
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpRSS_TYP As New SqlParameter("@RSS_TYP", SqlDbType.VarChar, 10)
        sqlpRSS_TYP.Value = p_RssType
        cmd.Parameters.Add(sqlpRSS_TYP)

        Dim sqlpLvl As New SqlParameter("@Lvl", SqlDbType.Int)
        sqlpLvl.Value = p_Level
        cmd.Parameters.Add(sqlpLvl)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = Misparams.FromDate
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpDTTO As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpDTTO.Value = Misparams.ToDate
        cmd.Parameters.Add(sqlpDTTO)

        Dim sqlpRSS_CODE As New SqlParameter("@RSS_CODE", SqlDbType.VarChar, 30)
        sqlpRSS_CODE.Value = p_CODE
        cmd.Parameters.Add(sqlpRSS_CODE)

        Dim sqlpRSSPARENT As New SqlParameter("@RSSPARENT", SqlDbType.VarChar, 30)
        sqlpRSSPARENT.Value = p_RSSPARENT
        cmd.Parameters.Add(sqlpRSSPARENT)

        Dim sqlpBSpecial As New SqlParameter("@BSpecial", Misparams.ExcludeSpecialAccounts)
        cmd.Parameters.Add(sqlpBSpecial)

        objConn.Close()
        objConn.Open()
        If p_Level = 1 Then
            Misparams.RSSPARENT = p_CODE
            Session("Misparams") = Misparams
        End If
        cmd.Connection = objConn
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("UserName") = Session("sUsr_name")
        params("Bsu_Shortnames") = Misparams.BSUShortnames
        params("Bsu_Segments") = Misparams.BSUSegments
        params("ToDate") = Misparams.ToDate
        params("fromDate") = Misparams.FromDate
        params("ReportCaption") = p_DESCR
        params("BSU_CURRENCY") = Misparams.Currency
        params("EXPRESSED_IN") = Misparams.ExpressedIn
        params("RSSPARENT") = Misparams.RSSPARENT
        repSource.Parameter = params
        repSource.Command = cmd

        repSource.ResourceName = "../RPT_Files/rptGetMISPnLBudget_" & p_Level & ".rpt"
        Session("ReportSource") = repSource
        'Context.Items.Add("ReportSource", repSource)
        objConn.Close()
        If Session("USR_MIS") = Nothing Then
            Response.Redirect("rptviewer.aspx?newwindow=true", True)
        Else
            If Session("USR_MIS") = 1 Or Session("USR_MIS") = 2 Then
                Response.Redirect("rptviewerMIS.aspx?newwindow=true", True)
            Else
                Response.Redirect("rptviewer.aspx?newwindow=true", True)
            End If
        End If
        objConn.Close()
    End Sub

End Class
