<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="trdStudentSearch.aspx.vb" Inherits="Threads_trdStudentSearch" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">




        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}



function change_chk_state(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();//fire the click event of the child element
        }
    }
}

    </script>
    


        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>  <asp:Label ID="lblTitle" Text="Search Student" runat="server" ></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <table width="100%" id="Table3" border="0">


        <tr>

            <td align="left" class="title">

                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>

            </td>
        </tr>

    </table>

     <table id="studTable" runat="server" align="center" cellpadding="5" cellspacing="0"  Width="100%">
                   <tr>
                       <td colspan="7" align="right">
                            <asp:LinkButton ID="lbAdvanced" runat="server" >More Options</asp:LinkButton>
                       </td>
                   </tr>
                    <tr>
                        <td align="left" >
                            <asp:Label ID="Label3" runat="server" Text="Shift"  class="field-label">School Code</asp:Label>
                          </td>
                        <td align="left" >                            
                            <asp:TextBox ID="txtSchool" runat="server">
                            </asp:TextBox>
                        </td>
                        <td align="left" >
                            <asp:Label ID="Label4" runat="server" Text="Student Name"  class="field-label"></asp:Label>
                         </td>   
                        <td align="left" >
                           <asp:TextBox ID="txtStudentName" runat="server">
                            </asp:TextBox>
                        </td>
                        <td align="left">
                            <span class="field-label">Parent Mobile</span>
                        </td>
                        <td align="right">
                            <span class="field-label">+971-</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtParentMobile" runat="server">
                            </asp:TextBox></td>
                    </tr>
                   

                  
                    <tr   >
                        <td align="left" >
                            <span class="field-label">Grade</span>
                         </td>   
                        <td align="left" >
                           <asp:TextBox ID="txtGrade" runat="server">
                            </asp:TextBox>
                        </td>
                        <td align="left">
                            <span class="field-label">Section</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtSection" runat="server">
                            </asp:TextBox>
                        </td>
        </tr>

                    <tr  >
                       <td align="left" >
                            <span class="field-label">Parent Name</span>
                         </td>   
                        <td align="left" >
                           <asp:TextBox ID="txtParentName" runat="server">
                            </asp:TextBox>
                        </td>
                        <td align="left">
                            <span class="field-label">Parent Email</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtParentEmail" runat="server">
                            </asp:TextBox>
                        </td>
                    </tr>
                                 
           <tr>
                        <td align="center"  valign="bottom" colspan="6">
                            <asp:Button ID="btnList" runat="server" CausesValidation="False" CssClass="button"
                                Text="Search" TabIndex="9"  />
                         </td>
                    </tr>
   <tr id="tr3" runat="server">

                        <td colspan="8" style="vertical-align: top" align="center">
                            <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-bordered" EmptyDataText="No Records Found"
                               PageSize="20"  AllowPaging="True" Width="100%" BorderWidth="0">
                                <RowStyle CssClass="griditem"  />
                                <Columns>
                                    <asp:TemplateField HeaderText="Student ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Student Name"  HeaderStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuName" Enabled="false" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                   <asp:TemplateField HeaderText="Grade" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" Enabled="false" runat="server" Text='<%# Bind("GRM_DISPLAY")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Section" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Father / Mother Name "  HeaderStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuName" Enabled="false" runat="server" Text='<%# Bind("PARENT_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="School">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBsu" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                          </Columns>


                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>


                        </td>
                    </tr>






                </table>




               <asp:HiddenField ID="hfAdvanced" runat="server"></asp:HiddenField>
        
                </div>
            </div>
            </div>
    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />

</asp:Content>

