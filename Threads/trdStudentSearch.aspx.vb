Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Threads_trdStudentSearch
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S101465") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                    hfAdvanced.Value = "false"
                    studTable.Rows(2).Visible = False
                    studTable.Rows(3).Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
       
    End Sub

    Protected Sub ddlgvSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strQuery As String

        strQuery = "exec GETSTUDENTIDFORTHREADS " _
                & " @BSU_SHORTNAME='" + txtSchool.Text + "'," _
                & " @STU_NAME='" + txtStudentName.Text + "'," _
                & " @bADVANCED='" + hfAdvanced.Value + "'," _
                & " @GRADE='" + txtGrade.Text + "'," _
                & " @SECTION='" + txtSection.Text + "'," _
                & " @PARENTMOBILE='" + txtParentMobile.Text + "'," _
                & " @PARENTNAME='" + txtParentName.Text + "'," _
                & " @PARENTEMAIL='" + txtParentEmail.Text + "'," _
                & " @LOGINUSER='" + Session("susr_name") + "'"


        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        If ds.Tables(0).Rows.Count = 0 Then
            lblError.Text = "Your search is not returning any records . Please use the more search options to refine your search"
            studTable.Rows(5).Visible = False
            Exit Sub
        ElseIf ds.Tables(0).Rows.Count > 1 Then
            studTable.Rows(5).Visible = False
            lblError.Text = "Your search is not returning a unique record . Please use the more search options to refine your search"
            Exit Sub
        End If

        gvStud.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "No records to view"
        Else
            gvStud.DataBind()
        End If



    End Sub

#End Region


    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        Try

            lblError.Text = ""
            studTable.Rows(5).Visible = True
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub




    Protected Sub lbAdvanced_Click(sender As Object, e As EventArgs) Handles lbAdvanced.Click
        If hfAdvanced.Value = "false" Then
            studTable.Rows(2).Visible = True
            studTable.Rows(3).Visible = True
            hfAdvanced.Value = "true"
        Else
            hfAdvanced.Value = "false"
            studTable.Rows(2).Visible = False
            studTable.Rows(3).Visible = False
        End If
    End Sub
End Class
