<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPSI_DASH.aspx.vb" Inherits="rptPSI_DASH" Title="::::GEMS OASIS:::: Online Student Administration System::::"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">
           function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 250px; ";
            sFeatures+="dialogHeight: 270px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../../Accounts/calendar.aspx","", sFeatures);
            if(result != '' && result != undefined)
            {
                switch(txtControl)
                {
                  case 0:
                    document.getElementById('<%=txtAsOnDate.ClientID %>').value=result;
                    break;
                   case 1:
                    document.getElementById('<%=txtToDate.ClientID %>').value=result;
                    break;
                   }
            }
            return false;    
           }
   
         
    </script>

    <table align="center" style="width: 50%">
        <tr align="left">
            <td style="width: 557px">
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
        <tr align="left">
            <td >
                <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
                    style="width: 80%">
                    <tr class="subheader_img">
                        <td align="center" colspan="8" style="height: 1px" valign="middle">
                            <div align="left">
                                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                    <asp:Label ID="lblCaption" runat="server" Text="Select Date Range"></asp:Label></font>&nbsp;</div>
                        </td>
                    </tr>
                    
                   
                            
                       <tr>
                        <td align="left" class="matters" >
                            From Date</td>
                        <td class="matters" style="width: 2px;">
                            :</td>
                        <td align="left" class="matters" colspan="6" style="text-align: left;">
                            <asp:TextBox ID="txtAsOnDate" runat="server" Width="123px"></asp:TextBox><span style="font-size: 7pt">
                            </span>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return getDate(550, 310, 0)" />
                            <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtAsOnDate"
                                CssClass="error" Display="Dynamic" ErrorMessage="As on Date required" ForeColor=""
                                ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <br />
                            <span style="font-size: 7pt">(dd/mmm/yyyy)</span></td>
                    </tr>
                    
                    
                     <tr>
                        <td align="left" class="matters" >
                            To Date</td>
                        <td class="matters" style="width: 2px;">
                            :</td>
                        <td align="left" class="matters" colspan="6" style="text-align: left;">
                            <asp:TextBox ID="txtToDate" runat="server" Width="123px"></asp:TextBox><span style="font-size: 7pt">
                            </span>
                            <asp:ImageButton ID="imgToate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return getDate(550, 310, 1)" />
                            <asp:RequiredFieldValidator ID="rfvToDate" runat="server" ControlToValidate="txtToDate"
                                CssClass="error" Display="Dynamic" ErrorMessage="As on Date required" ForeColor=""
                                ValidationGroup="dayBook2">*</asp:RequiredFieldValidator>
                            <br />
                            <span style="font-size: 7pt">(dd/mmm/yyyy)</span></td>
                    </tr>
                         
                            
                    <tr>
                        <td align="left" class="matters" colspan="8" style="text-align: right">
                            &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Export"
                                ValidationGroup="dayBook" /></td>
                    </tr>
                </table>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>

