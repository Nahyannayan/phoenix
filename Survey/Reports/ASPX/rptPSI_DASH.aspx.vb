Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports System.Xml
Imports GemBox.Spreadsheet
Imports system
Imports System.Data.SqlTypes


Partial Class rptPSI_DASH
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try

            Dim param(30) As SqlClient.SqlParameter
            Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
            param(1) = New SqlClient.SqlParameter("@FromDate", txtAsOnDate.Text)
            param(2) = New SqlClient.SqlParameter("@ToDate", txtToDate.Text)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PSI_DASH", param)
            Dim dtEXCEL As New DataTable
            dtEXCEL = ds.Tables(0)

            If dtEXCEL.Rows.Count > 0 Then
                Dim str_err As String = String.Empty
                Dim errorMessage As String = String.Empty
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                ' ws.HeadersFooters.AlignWithMargins = True
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + "PSI_Data.xlsx")

                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()

                Dim pathSave As String
                pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"

                ef.Save(cvVirtualPath & "StaffExport\" + pathSave)

                Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)

                Response.Flush()

                Response.End()

                'HttpContext.Current.Response.ContentType = "application/octect-stream"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(path)
                'HttpContext.Current.Response.End()
            Else
                lblError.Text = "No Records To display with this filter condition....!!!"
                lblError.Focus()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Sub CallReport()
        
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


           

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

          

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "M000109") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights



                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                txtAsOnDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))



            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub


    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtAsOnDate.Text.Trim <> "" Then
                Dim strfDate As String = txtAsOnDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>As on Date format is Invalid"
                Else
                    txtAsOnDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtAsOnDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>As on Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>As on Date required"

            End If

            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ADMISSION DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try

    End Function


End Class
