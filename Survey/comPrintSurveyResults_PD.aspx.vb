Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports SurveyChart
Imports System.Drawing
Imports InfoSoftGlobal

Partial Class masscom_comPrintSurveyResults_PD
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private intHeadColCnt As Integer = 0
    Private nmvAnswers As New NameValueCollection
    Private nmvAnswerDesc As New NameValueCollection
    Private nmvSurvResults As New NameValueCollection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            SetFormatTable()
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            Dim str_Conn = ConnectionManger.GetOASISSurveyConnectionString
            If Not IsPostBack Then

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Try
                    ViewState("SurveyId") = "185"
                    ViewState("CM_ID") = Encr_decrData.Decrypt(Request.QueryString("PD_CM_VAL").Replace(" ", "+")).Replace("|", "")
                    'ViewState("CM_ID") = Encr_decrData.Decrypt(Request.QueryString("PD_CM_VAL").Replace(" ", "+"))
                    CurBsUnit = "999998"
                    Session("Bsuid") = CurBsUnit
                Catch ex As Exception

                End Try
                Session("gm") = Nothing

                

                'Session("SurveyId") = cmbSurvey.SelectedValue
                SetFormatTable()
                BuildResulltSet()
              

                Dim Param(10) As SqlClient.SqlParameter
                Param(0) = New SqlClient.SqlParameter("@CM_ID", ViewState("CM_ID"))

                Using SENDDETAILS As SqlDataReader = SqlHelper.ExecuteReader(str_Conn, CommandType.StoredProcedure, "SRV.SURVEYSENDDETAILS_PD", Param)
                    If SENDDETAILS.HasRows Then
                        While SENDDETAILS.Read
                            TSent.Text = Convert.ToString(SENDDETAILS("SENDCOUNT")) 'Sent

                            TResponse.Text = Convert.ToString(SENDDETAILS("RESPONSECOUNT")) 'Response
                        End While
                    End If
                End Using
                lblPDTITLE.Text = GetPD_TITLE(ViewState("CM_ID"))


            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub SetFormatTable()
        With TabResults
            .Width = "827"
            .Height = "347"
            .ForeColor = Drawing.Color.Black
            .BackColor = Drawing.Color.LightBlue
            .Style.Value = ""
            .BorderColor = Drawing.Color.Black
            .BorderWidth = "1"
            .Font.Size = "10"
        End With
    End Sub

    Private Sub BuildResulltSet()
        ''get survey id 
        Dim courseType As String = GetCourseType()
        Dim surveyID As String = "185"

        If courseType = "2" Then
            surveyID = "193"
        Else
            surveyID = "185"
        End If
        ' Get the Survey Groups Based on the Single Survey
        ''replaced survey id 185 with above by nahyan on 1 nov2015 for displaying online pd course survey also
        For Each grpRow As DataRow In GetSurveyGroup(surveyID).Tables(0).Rows
            BuildSUBHeading(TabResults, grpRow.Item("GROUPID").ToString()) ' Table headings 
            BuildQuestions(TabResults, grpRow.Item("GROUPID").ToString())

            Dim Headerrow_1 As New TableRow
            Headerrow_1.Style("font-weight") = "bold"
            Headerrow_1.Style("font-size") = "8pt"
            Headerrow_1.Style("text-align") = "center"
            Headerrow_1.Style("Height") = "25px"
            Headerrow_1.BackColor = Drawing.Color.LightBlue
            Headerrow_1.Style("font-family") = "verdana,timesroman,garamond"
            TabResults.Rows.Add(Headerrow_1)
        Next

    End Sub

    Private Function BuildSUBHeading(ByVal tblReport As Table, ByVal GroupId As String)

        'Sub Headings
        Dim Headerrow_2 As New TableRow
        Headerrow_2.Style("font-weight") = "bold"
        Headerrow_2.Style("font-size") = "9pt"
        Headerrow_2.Style("Height") = "20px"
        Headerrow_2.Style("text-align") = "center"
        Headerrow_2.Style("font-family") = "verdana,timesroman,garamond"
        Headerrow_2.BackColor = Drawing.Color.LightBlue

        Dim Headercell_1 As New TableCell
        Headercell_1.Style("text-align") = "center"
        Headercell_1.Style("Width") = "20px"
        Headercell_1.BackColor = Drawing.Color.LightBlue
        Headercell_1.BorderColor = Drawing.Color.Black
        Headercell_1.BorderStyle = BorderStyle.Solid
        Headercell_1.BorderWidth = "1"
        Headercell_1.Text = "Sl.No"
        Headerrow_2.Cells.Add(Headercell_1)

        Dim Headercell_4 As New TableCell
        Headercell_4.Style("text-align") = "center"
        Headercell_4.Style("Width") = "400px"
        Headercell_4.Style("Height") = "20px"
        Headercell_4.ColumnSpan = "0"
        Headercell_4.BackColor = Drawing.Color.LightBlue
        Headercell_4.BorderColor = Drawing.Color.Black
        Headercell_4.BorderStyle = BorderStyle.Solid
        Headercell_4.BorderWidth = "1"

        Headercell_4.Text = "&nbsp;&nbsp;" & GetGroupName(GroupId).ToString().Replace("&&BR", "") '"Test 123"
        Headerrow_2.Cells.Add(Headercell_4)
        '

        Using SqlAnswers As SqlDataReader = GetSurveyAnswers("", GroupId)
            If SqlAnswers.HasRows = True Then
                While SqlAnswers.Read
                    Dim Headercell_8 As New TableCell
                    Headercell_8.Style("text-align") = "center"
                    Headercell_8.Style("Width") = "150px"
                    Headercell_8.ColumnSpan = "0"
                    Headercell_8.Wrap = False
                    Headercell_8.BackColor = Drawing.Color.LightBlue
                    Headercell_8.BorderColor = Drawing.Color.Black
                    Headercell_8.BorderStyle = BorderStyle.Solid
                    Headercell_8.BorderWidth = "1"
                    Headercell_8.Text = SqlAnswers("ANS_DESC").ToString()
                    'Headerrow_2.Cells.Add(Headercell_8)

                    nmvAnswers.Add(GroupId, SqlAnswers("ANS_ID").ToString()) 'Answer Id
                    nmvAnswerDesc.Add(GroupId, SqlAnswers("ANS_DESC").ToString()) 'Answer
                End While
            End If
            tblReport.Rows.Add(Headerrow_2)
        End Using
        intHeadColCnt = intHeadColCnt + 1

    End Function

    

    Private Function BuildQuestions(ByVal tblReport As Table, ByVal GroupId As String)

        Dim intRow As Integer = 0
        Dim intAnswer As Integer = 0
        Dim intQuestId As Integer = 0
        Dim intGrpRow As Integer = 0
        ''get survey id 
        Dim courseType As String = GetCourseType()
        Dim surveyID As String = "185"

        If courseType = "2" Then
            surveyID = "193"
        Else
            surveyID = "185"
        End If
        ''changed 185 to survey id on 1 nov2015 
        Using SurveyQuest As SqlDataReader = GetSurveyQuestions(surveyID.ToString, GroupId)
            If SurveyQuest.HasRows = True Then
                While SurveyQuest.Read
                    Dim Headerrow_3 As New TableRow
                    Headerrow_3.ID = "row" + intRow.ToString
                    intRow = intRow + 1
                    intGrpRow = intGrpRow + 1
                    ViewState("Qid") = SurveyQuest("QUEST_ID").ToString()
                    ViewState("Group_id") = SurveyQuest("GROUPID").ToString()
                    ViewState("Survey_iid") = SurveyQuest("SURVEYID").ToString()
                    'Building Cells of Each Row
                    For intCols As Integer = 1 To GetGroupResultCnt(GroupId) + 3

                        Dim Headercell_8 As New TableCell
                        Headercell_8.Style("text-align") = "center"
                        Headercell_8.ColumnSpan = "0"
                        Headercell_8.Wrap = False
                        Headercell_8.BackColor = Drawing.Color.LightBlue
                        Headercell_8.BorderColor = Drawing.Color.Black
                        Headercell_8.BorderStyle = BorderStyle.Solid
                        Headercell_8.BorderWidth = "1"
                        If intCols = 1 Then
                            Headercell_8.Text = intRow.ToString '
                            Headercell_8.Style("Width") = "20px"
                            Headercell_8.Style("FONT-SIZE") = "12px"
                            Headercell_8.Style("FONT-FAMILY") = "Verdana"
                            Headercell_8.Style("text-align") = "center"
                            Headerrow_3.Cells.Add(Headercell_8)
                        ElseIf intCols = 2 Then
                            Headercell_8.Text = SurveyQuest("QUESTDESC").ToString().Replace("&&BR", "<br>").Replace("&&B", "<b>")
                            Headercell_8.Style("Width") = "400px"
                            Headercell_8.Style("FONT-SIZE") = "12px"
                            Headercell_8.Style("FONT-FAMILY") = "Verdana"
                            Headercell_8.Style("text-align") = "left"
                            Headerrow_3.Cells.Add(Headercell_8)
                        ElseIf intCols = GetGroupResultCnt(GroupId) + 3 Then
                            Headercell_8.ID = "cell" + intRow.ToString() + intCols.ToString()
                            Headercell_8.Style("Width") = "10px"
                            Headercell_8.BorderStyle = BorderStyle.None
                            Headercell_8.BackColor = Color.White





                            Try

                                Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString

                                Dim strXML As String
                                Dim ANSWER As String = String.Empty
                                Dim TOT_ANSWER As Decimal
                                Dim arr() As String
                                arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}
                                Dim i As Integer = 0
                                strXML = ""
                                strXML = strXML & "<graph caption='EF Analysis' xAxisName='Answers' yAxisName='Percentage' decimalPrecision='2'  yAxisMaxValue='100'  numberSuffix='%25' formatNumberScale='1' rotateNames='1' labelPadding='100' >"

                                'Dim STR_QRY As String = "SELECT DISTINCT(A.ANS_ID),ANS_DESC,SURVEY_ID,(SELECT COUNT(*) FROM SRV.COM_SUR_RESULTDET WHERE ANSWERID=B.ANS_ID AND QUESTIONID=C.QUEST_ID)ANS_COUNT,(SELECT COUNT(*) FROM SRV.COM_SUR_RESULTDET WHERE QUESTIONID=C.QUEST_ID)QUEST_COUNT,QSTORDER FROM SRV.COM_SUR_QSTORDER A INNER JOIN SRV.COM_SUR_ANSWER  B ON A.ANS_ID=B.ANS_ID INNER JOIN SRV.COM_SUR_QUESTION C ON A.GROUP_ID=C.GROUPID WHERE C.QUEST_ID='" & ViewState("Qid") & "' and A.SURVEY_ID='" & ViewState("Survey_iid") & "' ORDER BY QSTORDER"
                                Dim param(10) As SqlClient.SqlParameter
                                param(0) = New SqlClient.SqlParameter("@QUEST_ID", ViewState("Qid"))
                                param(1) = New SqlClient.SqlParameter("@SURVEY_ID", ViewState("Survey_iid"))
                                param(2) = New SqlClient.SqlParameter("@BSU_ID", Session("Bsuid"))
                                param(3) = New SqlClient.SqlParameter("@SENDING_ID", ViewState("CM_ID"))
                                Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "SRV.RESULTDISPLAY_CHART_PD", param)

                                    If readerStudent_Detail.HasRows = True Then
                                        While readerStudent_Detail.Read
                                            ANSWER = readerStudent_Detail("ANS_DESC").ToString
                                            Dim ANS_TOOLTIP As String = Convert.ToString(readerStudent_Detail("ANS_TOOLTIP"))
                                            Dim Ans_id As String = Convert.ToString(readerStudent_Detail("ANS_ID"))

                                            TOT_ANSWER = Convert.ToDecimal(readerStudent_Detail("ANS_COUNT"))
                                            Dim TOT_TIMES_QT_ASKED As Decimal = Convert.ToDecimal(readerStudent_Detail("QUEST_COUNT"))
                                            If TOT_TIMES_QT_ASKED = 0 Then
                                                TOT_TIMES_QT_ASKED = 1
                                            End If
                                            TOT_ANSWER = TOT_ANSWER * (100 / TOT_TIMES_QT_ASKED)
                                            strXML = strXML & "<set name=" + "'" & ANSWER & "' value=" + "'" & TOT_ANSWER & "' color=" + "'" & arr(i) & "' hoverText='" & ANS_TOOLTIP & "' link='javascript:GetResult(" & ViewState("Qid") & ", " & ViewState("Survey_iid") & ", " & Ans_id & ") ;'  />"
                                            ''Qid,SurvId,Result
                                            i = i + 1
                                        End While
                                        strXML = strXML & "</graph>"

                                        Headercell_8.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "500", "250", False)
                                    Else
                                        Dim j As Integer = 0
                                        Dim QRY_str As String = "SELECT DISTINCT(A.ANS_ID),ANS_DESC,SURVEY_ID,QSTORDER FROM SRV.COM_SUR_QSTORDER A INNER JOIN SRV.COM_SUR_ANSWER  B ON A.ANS_ID=B.ANS_ID INNER JOIN SRV.COM_SUR_QUESTION C ON A.GROUP_ID=C.GROUPID WHERE(C.C.QUEST_ID='" & ViewState("Qid") & "' and A.SURVEY_ID='" & ViewState("Survey_iid") & "') ORDER BY QSTORDER"

                                        Using readerQuestion_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, QRY_str)
                                            If readerStudent_Detail.HasRows = True Then
                                                While readerStudent_Detail.Read
                                                    ANSWER = readerStudent_Detail("ANS_DESC").ToString
                                                    TOT_ANSWER = 0
                                                    strXML = strXML & "<set name=" + "'" & ANSWER & "' value=" + "'" & TOT_ANSWER & "' color=" + "'" & arr(j) & "' />"
                                                    j = j + 1
                                                End While
                                                strXML = strXML & "</graph>"
                                                Headercell_8.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "500", "250", False)
                                            End If
                                        End Using
                                    End If
                                End Using
                            Catch ex As Exception

                            End Try
                            '-----------------------------------End of Bar Diagram
                            Headerrow_3.Cells.Add(Headercell_8)

                        Else
                            intQuestId = SurveyQuest("QUEST_ID").ToString() 'Get Question Id
                            intAnswer = nmvAnswers.Item(GroupId).Split(",").GetValue(intCols - 3) 'Get AnswerId 
                            Headercell_8.Text = GetAnswerTotal(intQuestId, intAnswer, Session("Bsuid")).ToString
                            intAnswer = intAnswer + 1
                            nmvSurvResults.Add(intQuestId, Headercell_8.Text) 'Answer
                        End If '

                        'Headerrow_3.Cells.Add(Headercell_8)
                    Next
                    tblReport.Rows.Add(Headerrow_3)
                End While
            End If

        End Using
    End Function

    Private Shared Function GetGroupName(ByVal GroupId As String) As String
        'ByVal SurvId As String
        'SURVEYID= " & SurvId & " AND 
        Dim connection As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT GRPNAME FROM  SRV.COM_SUR_GROUP WHERE GRP_ID=" & GroupId & ""
        Dim dsSurveyInfo As DataSet
        dsSurveyInfo = SqlHelper.ExecuteDataset(connection, CommandType.Text, strSQL)
        If dsSurveyInfo.Tables(0).Rows.Count >= 1 Then
            Return dsSurveyInfo.Tables(0).Rows(0).Item(0).ToString()
        Else
            Return ""
        End If

    End Function

    Private Shared Function GetSurveyGroup(ByVal SurvId As String) As DataSet

        Dim connection As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT DISTINCT GROUPID FROM  SRV.COM_SUR_QUESTION WHERE SURVEYID= " & SurvId & " AND GroupID<>244 ORDER BY GROUPID "
        Dim dsSurveyInfo As DataSet
        dsSurveyInfo = SqlHelper.ExecuteDataset(connection, CommandType.Text, strSQL)
        Return dsSurveyInfo

    End Function

    Private Function GetCourseType() As String

        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT DISTINCT CM_COURSE_TYPE_ID FROM PD_M.COURSE_M where CM_ID in (" & ViewState("CM_ID") & ")"
        Dim courseType As String
        courseType = SqlHelper.ExecuteScalar(connection, CommandType.Text, strSQL)
        Return courseType

    End Function

    Private Shared Function GetSurveyAnswers(ByVal SurvId As String, ByVal IntGrpId As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISSurveyConnection
        Dim strSQL As String = ""
        strSQL = "SELECT A.SURVEY_ID,A.GROUP_ID,A.QSTORDER, B.ANS_ID,B.ANS_DESC  FROM SRV.COM_SUR_QSTORDER A, SRV.COM_SUR_ANSWER B " & _
        "WHERE  A.ANS_ID=B.ANS_ID AND A.GROUP_ID= " & IntGrpId & " ORDER BY QSTORDER "

        '  strSQL = "SELECT A.SURVEY_ID,A.GROUP_ID,A.QSTORDER, B.ANS_ID,B.ANS_DESC  FROM SRV.COM_SUR_QSTORDER A, SRV.COM_SUR_ANSWER B " & _
        '"WHERE  A.ANS_ID=B.ANS_ID AND A.GROUP_ID= " & IntGrpId & "  AND B.ANS_DESC<>'No Answer'  ORDER BY QSTORDER "


        Dim command As SqlCommand = New SqlCommand(strSQL, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Private Shared Function GetSurveyQuestions(ByVal SurvId As String, ByVal GroupId As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISSurveyConnection
        Dim strSQL As String = "SELECT QUEST_ID,SURVEYID,QUESTDESC,ANSTYPE,GROUPID,COMMENTS " & _
              " FROM SRV.COM_SUR_QUESTION " & _
              " WHERE SURVEYID=" & SurvId & " AND GROUPID=" & GroupId & " AND ANSTYPE<>'C' ORDER BY QUEST_ID"

        Dim command As SqlCommand = New SqlCommand(strSQL, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function

    Private Function GetGroupResultCnt(ByVal Grpid As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT COUNT(*) FROM SRV.COM_SUR_QSTORDER WHERE GROUP_ID= " & Grpid
        Dim DsHelper As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

        If DsHelper.Tables(0).Rows.Count >= 0 Then
            Return DsHelper.Tables(0).Rows(0).Item(0)
        Else
            Return 0
        End If
    End Function

    Private Function GetAnswerTotal(ByVal QID As Integer, ByVal AnswerId As Integer, ByVal BusUnit As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        If ViewState("MainMnu_code") = "M000100" Then
            If AnswerId = 16 Then
                Return 0
                Exit Function
            End If
        End If

        strSQL = "SELECT Count(*) FROM SRV.COM_SUR_RESULTDET WHERE QUESTIONID=" & QID & " And AnswerId =" & AnswerId & " AND BUSID IN (" & BusUnit & ")"
        'strSQL = "SELECT Count(*) FROM SRV.COM_SUR_RESULTDET DET,SRV.COM_SUR_RESULTMST MST WHERE MST.RESULT_ID + 86 =DET.RESULTDET_ID AND DET.QUESTIONID=" & QID & " And DET.AnswerId =" & AnswerId & " AND MST.BUSINESSUNIT=" & BusUnit
        Dim DsHelper As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

        If DsHelper.Tables(0).Rows.Count >= 0 Then
            Return DsHelper.Tables(0).Rows(0).Item(0)
        Else
            Return 0
        End If
    End Function
    Private Function GetBusinessUnitName(ByVal BsuId As String) As String

        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = "SELECT BSU_NAME FROM dbo.BUSINESSUNITS WHERE BSU_ID='" & BsuId & "'"
        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsResponse.Tables(0).Rows.Count >= 1 Then
            With dsResponse.Tables(0).Rows(0)
                Return .Item(0)
            End With
        Else
            Return ""
        End If
    End Function
    Private Function GetPD_TITLE(ByVal CM_ID As String) As String

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strSQL As String = ""
        strSQL = "SELECT CM_TITLE FROM OASIS.PD_M.COURSE_M WHERE CM_ID='" & CM_ID & "'"
        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsResponse.Tables(0).Rows.Count >= 1 Then
            With dsResponse.Tables(0).Rows(0)
                Return .Item(0)
            End With
        Else
            Return ""
        End If
    End Function
    Private Function GetTotalRead(ByVal BsuId As String, ByVal SurvyId As String) As Integer

        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = "SELECT COUNT(*) FROM SRV.COM_SUR_READERS WHERE SURVEYID=" & SurvyId & " AND BSUID IN ('" & BsuId & "')"
        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsResponse.Tables(0).Rows.Count >= 1 Then
            With dsResponse.Tables(0).Rows(0)
                Return .Item(0)
            End With
        Else
            Return 0
        End If
    End Function

    Private Function GetTotalSent(ByVal BsuId As String, ByVal SurvyId As String) As Integer

        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = "SELECT COUNT(*) FROM SRV.COM_SUR_SENT WHERE SURVEYID=" & SurvyId & " AND BSUID IN (" & BsuId & ")"
        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsResponse.Tables(0).Rows.Count >= 1 Then
            With dsResponse.Tables(0).Rows(0)
                Return .Item(0)
            End With
        Else
            Return 0
        End If
    End Function

    Private Function GetTotalResponse(ByVal BsuId As String, ByVal SurvyId As String) As Integer

        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = "SELECT COUNT(DISTINCT USER_NAME) AS TRESPONSE FROM SRV.COM_SUR_RESULTMST WHERE SURVEYID=" & SurvyId & " AND BUSINESSUNIT IN (" & BsuId & ")"
        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsResponse.Tables(0).Rows.Count >= 1 Then
            With dsResponse.Tables(0).Rows(0)
                Return .Item(0)
            End With
        Else
            Return 0
        End If
    End Function

    Private Function GetTotalResponse() As Double
        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        ''get survey id 
        Dim courseType As String = GetCourseType()
        Dim surveyID As String = "185"

        If courseType = "2" Then
            surveyID = "193"
        Else
            surveyID = "185"
        End If
        strSQL = "SELECT RESPONSE FROM SRV.COM_SUR_DETAILS WHERE SURVEY_ID=" & surveyID
        Dim DsHelper As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

        If DsHelper.Tables(0).Rows.Count >= 0 Then
            Return DsHelper.Tables(0).Rows(0).Item(0)
        Else
            Return 0
        End If

    End Function


   

   
End Class
