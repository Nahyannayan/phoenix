<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comSurvPopup.aspx.vb" Inherits="masscom_comSurvPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <base target="_self" />
<script language="javascript" type="text/javascript">

 function formclose() 
 {
    //alert ('test');
   window.close();
 }
          
</script>
    
</head>
<body>
    <form id="form1" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> <asp:Label id="Label1" runat="server" Font-Bold="True"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
     <table id="Table1" runat="server" align="center" 
        cellpadding="5" cellspacing="0">
       
        <tr>
            <td colspan="3" valign="top">
                <table id="Table2" runat="server" align="center" border="0" 
                    cellpadding="5" cellspacing="0">
                    <tr>
                        <td align="center" colspan="3" valign="top">
                            &nbsp;<asp:GridView id="gvPopUp" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                BorderStyle="Double" CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                HeaderStyle-Height="30" Width="100%" BackColor="White" BorderColor="#336666" BorderWidth="3px" CellPadding="4" GridLines="Horizontal">
                                <emptydatarowstyle wrap="False" />
                                <columns>
                                    <asp:BoundField HeaderText="Sl No" InsertVisible="False" DataField="Sl No" >
                                        <ControlStyle Width="50px" />
                                        <ItemStyle Width="50px" />
                                        <HeaderStyle Width="50px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="COMMENTS" HeaderText="COMMENTS" InsertVisible="False" >
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </columns>
                                <%--<rowstyle cssclass="griditem" height="17px" wrap="False" BackColor="White" ForeColor="#333333" />
                                <editrowstyle wrap="False" />
                                <selectedrowstyle cssclass="Green" wrap="False" BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                <headerstyle cssclass="gridheader_pop" height="15px" wrap="False" BackColor="#336666" Font-Bold="True" ForeColor="White" />
                                <alternatingrowstyle cssclass="griditem_alternative" wrap="False" />
                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />--%>
                            </asp:GridView>
                            &nbsp;
                            <input id="btnClose" type="button" value="Close" onclick='formclose()' /></td>
                    </tr>
                </table>
                </td>
        </tr>
    </table>
                </div>
            </div>
    </div>
    </form>
</body>
</html>
