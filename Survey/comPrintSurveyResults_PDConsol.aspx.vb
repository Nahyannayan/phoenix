Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports SurveyChart
Imports System.Drawing
Imports InfoSoftGlobal

Partial Class masscom_comPrintSurveyResults_PDConsol
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private intHeadColCnt As Integer = 0
    Private nmvAnswers As New NameValueCollection
    Private nmvAnswerDesc As New NameValueCollection
    Private nmvSurvResults As New NameValueCollection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            SetFormatTable()
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            Dim str_Conn = ConnectionManger.GetOASISSurveyConnectionString
            If Not IsPostBack Then

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Try
                    ViewState("SurveyId") = "185"
                    ViewState("CM_ID") = Encr_decrData.Decrypt(Request.QueryString("PD_CM_VAL").Replace(" ", "+"))
                    'ViewState("Emp_ID") = Encr_decrData.Decrypt(Request.QueryString("Emp_Id").Replace(" ", "+"))
                    CurBsUnit = "999998"
                    Session("Bsuid") = CurBsUnit
                Catch ex As Exception

                End Try
                Session("gm") = Nothing

                'Session("SurveyId") = cmbSurvey.SelectedValue
                SetFormatTable()
                BuildResulltSet()

                ' ''Dim Param(10) As SqlClient.SqlParameter
                ' ''Param(0) = New SqlClient.SqlParameter("@CM_ID", ViewState("CM_ID"))

                ' ''Dim TotalSent, TotalResponse, RecCount As Integer

                ' ''Using SENDDETAILS As SqlDataReader = SqlHelper.ExecuteReader(str_Conn, CommandType.StoredProcedure, "SRV.SURVEYSENDDETAILS_PD", Param)
                ' ''    If SENDDETAILS.HasRows Then
                ' ''        While SENDDETAILS.Read
                ' ''            'TSent.Text = Convert.ToString(SENDDETAILS("SENDCOUNT")) 'Sent
                ' ''            TotalSent += CInt(SENDDETAILS("SENDCOUNT"))

                ' ''            'TResponse.Text = Convert.ToString(SENDDETAILS("RESPONSECOUNT")) 'Response
                ' ''            TotalResponse += CInt(SENDDETAILS("RESPONSECOUNT"))
                ' ''            RecCount += 1
                ' ''        End While
                ' ''    End If
                ' ''End Using

                'TotalSent = Math.Abs(TotalSent / RecCount)
                'TotalResponse = Math.Abs(TotalResponse / RecCount)

                'TSent.Text = Convert.ToString(TotalSent) 'Sent
                'TResponse.Text = Convert.ToString(TotalResponse) 'Response

                'Get Participant count
                str_Conn = ConnectionManger.GetOASISConnectionString
                Dim Param_Participants(3) As SqlClient.SqlParameter
                Param_Participants(0) = New SqlClient.SqlParameter("@CM_IDs", ViewState("CM_ID"))
                Param_Participants(1) = New SqlClient.SqlParameter("@FILTER_COND", "")
                Param_Participants(2) = New SqlClient.SqlParameter("@Attendance", "P")
                Dim dsParticipants As DataSet
                dsParticipants = SqlHelper.ExecuteDataset(str_Conn, CommandType.StoredProcedure, "PD_T.Get_Participants_List_For_Course", Param_Participants)
                If Not dsParticipants Is Nothing Then
                    'TSent.Text = dsParticipants.Tables(0).Rows.Count
                    TSent.Text = dsParticipants.Tables(0).Select("Attendance = 'PRESENT'").Length



                    dsParticipants.Tables.Clear()
                    dsParticipants.Dispose()
                End If

                'Get Response Count
                str_Conn = ConnectionManger.GetOASISSurveyConnectionString
                ''commented on 21Dec2015
                'Dim PARAM_Response(3) As SqlParameter
                'PARAM_Response(0) = New SqlParameter("@CM_ID", ViewState("CM_ID"))
                ''PARAM_Response(1) = New SqlParameter("@EMP_ID", Session("EmployeeId"))
                ''Dim dsResponse As DataSet
                ''dsResponse = SqlHelper.ExecuteDataset(str_Conn, CommandType.StoredProcedure, "SRV.EF_PD_STAT", PARAM_Response)
                ''If Not dsResponse Is Nothing Then
                ''    TResponse.Text = dsResponse.Tables(0).Rows.Count
                ''    dsResponse.Tables.Clear()
                ''    dsResponse.Dispose()
                ''End If

                'PARAM_Response(1) = New SqlParameter("@PARMTYPE ", 1)
                'Dim dsResponse As DataSet
                'dsResponse = SqlHelper.ExecuteDataset(str_Conn, CommandType.StoredProcedure, "SRV.EF_PD_GRAPH_CONSOL", PARAM_Response)
                'If Not dsResponse Is Nothing Then
                '    ''    TResponse.Text = dsResponse.Tables(0).Rows(0).Item("Response")
                '    dsResponse.Tables.Clear()
                '    dsResponse.Dispose()
                'End If

                Dim Param(2) As SqlClient.SqlParameter
                Param(0) = New SqlClient.SqlParameter("@CM_ID", ViewState("CM_ID"))

                Using SENDDETAILS As SqlDataReader = SqlHelper.ExecuteReader(str_Conn, CommandType.StoredProcedure, "SRV.SURVEYRESPONSECOUNT_PD", Param)
                    If SENDDETAILS.HasRows Then
                        While SENDDETAILS.Read
                            ' TSent.Text = Convert.ToString(SENDDETAILS("SENDCOUNT")) 'Sent

                            TResponse.Text = Convert.ToString(SENDDETAILS("RESPONSECOUNT")) 'Response
                        End While
                    End If
                End Using
                'lblPDTITLE.Text = GetPD_TITLE(ViewState("CM_ID"))
                GetPD_TITLE(ViewState("CM_ID"))

            End If

        Catch ex As Exception
            Dim ss As String = ex.Message
        End Try
    End Sub


    Private Function GetCourseType() As String

        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT DISTINCT CM_COURSE_TYPE_ID FROM PD_M.COURSE_M where CM_ID in (" & ViewState("CM_ID") & ")"
        Dim courseType As String
        courseType = SqlHelper.ExecuteScalar(connection, CommandType.Text, strSQL)
        Return courseType

    End Function
    Private Sub SetFormatTable()
        With TabResults
            .Width = "827"
            .Height = "347"
            .ForeColor = Drawing.Color.Black
            .BackColor = Drawing.Color.White
            .Style.Value = ""
            .BorderColor = Drawing.Color.Black
            .BorderWidth = "1"
            .Font.Size = "10"
        End With
    End Sub

    Private Sub BuildResulltSet()

        ''get survey id 
        Dim courseType As String = GetCourseType()
        Dim surveyID As String = "185"

        If courseType = "2" Then
            surveyID = "193"
        Else
            surveyID = "185"
        End If
        ViewState("surveyID") = surveyID
        ' Get the Survey Groups Based on the Single Survey
        ''replaced survey id 185 with above by nahyan on 1 nov2015 for displaying online pd course survey also
        ' Get the Survey Groups Based on the Single Survey........
        ' For Each grpRow As DataRow In GetSurveyGroup("185").Tables(0).Rows
        ' BuildSUBHeading(TabResults, 1) ' Table headings 
        BuildQuestions(TabResults, 1)

        Dim Headerrow_1 As New TableRow
        Headerrow_1.Style("font-weight") = "bold"
        Headerrow_1.Style("font-size") = "8pt"
        Headerrow_1.Style("text-align") = "center"
        Headerrow_1.Style("Height") = "25px"
        Headerrow_1.BackColor = Drawing.Color.White
        Headerrow_1.Style("font-family") = "verdana,timesroman,garamond"
        TabResults.Rows.Add(Headerrow_1)
        ' Next

    End Sub

    Private Function BuildQuestions(ByVal tblReport As Table, ByVal GroupId As String)

        Dim intRow As Integer = 0
        Dim intAnswer As Integer = 0
        Dim intQuestId As Integer = 0
        Dim intGrpRow As Integer = 0


        Dim Headerrow_3 As New TableRow
        Headerrow_3.ID = "row" + intRow.ToString
        intRow = intRow + 1
        intGrpRow = intGrpRow + 1
        Dim Headercell_8 As New TableCell
        Headercell_8.Style("text-align") = "center"
        Headercell_8.ColumnSpan = "0"
        Headercell_8.Wrap = False
        Headercell_8.BackColor = Drawing.Color.White
        Headercell_8.BorderColor = Drawing.Color.Black
        Headercell_8.BorderStyle = BorderStyle.Solid
        Headercell_8.BorderWidth = "1"
        Headercell_8.ID = "cell" + intRow.ToString() + "1"
        Headercell_8.Style("Width") = "10px"
        Headercell_8.BorderStyle = BorderStyle.None
        Headercell_8.BackColor = Color.White

        Dim strXML As String
        Dim ANSWER As String = String.Empty
        Dim TOT_ANSWER As Decimal
        Dim arr() As String
        arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}


        'strXML = ""
        'strXML = strXML & "<graph caption='EF Analysis' xAxisName='' yAxisName='Average' decimalPrecision='2'  yAxisMaxValue='5'  numberSuffix='' formatNumberScale='1' rotateNames='0' labelPadding='100' >"
        'strXML = strXML & "<set name=" + "'Q' value=" + "'2.25' color=" + "'" & arr(0) & "' hoverText=''  />"
        'strXML = strXML & "<set name=" + "'A' value=" + "'3' color=" + "'" & arr(1) & "' hoverText=''  />"
        'strXML = strXML & "<set name=" + "'B' value=" + "'4' color=" + "'" & arr(2) & "' hoverText=''  />"
        'strXML = strXML & "<set name=" + "'C' value=" + "'4.12' color=" + "'" & arr(3) & "' hoverText=''  />"
        'strXML = strXML & "<set name=" + "'D' value=" + "'3.56' color=" + "'" & arr(4) & "' hoverText=''  />"

        'strXML = strXML & "</graph>"
        'Headercell_8.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "800", "450", False)
        'Headerrow_3.Cells.Add(Headercell_8)
        'tblReport.Rows.Add(Headerrow_3)


        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim param(10) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@CM_ID", ViewState("CM_ID"))
        param(1) = New SqlClient.SqlParameter("@PARMTYPE", 2)
        ''added by nahyan on 20 oct 2015 to display data based on PD co ordinator
        param(2) = New SqlParameter("@EMP_ID", Session("EmployeeId"))
        '' remove delivery skills for 193-online course

        Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "SRV.EF_PD_GRAPH_CONSOL", param)
            If readerStudent_Detail.HasRows = True Then
                While readerStudent_Detail.Read
                    strXML = ""
                    strXML = strXML & "<graph caption='EF Analysis' xAxisName='' yAxisName='Average' decimalPrecision='2'  yAxisMaxValue='4'  numberSuffix='' formatNumberScale='1' rotateNames='1' labelPadding='200' >"
                    If (ViewState("surveyID") = "185") Then


                        strXML = strXML & "<set name=" + "'Preparation and Planning' value=" + "'" & readerStudent_Detail("A") & "' color=" + "'" & arr(0) & "' hoverText=''  />"
                        strXML = strXML & "<set name=" + "'Use of resources and facilities' value=" + "'" & readerStudent_Detail("B") & "' color=" + "'" & arr(0) & "' hoverText=''  />"
                        strXML = strXML & "<set name=" + "'Delivery skills' value=" + "'" & readerStudent_Detail("C") & "' color=" + "'" & arr(0) & "' hoverText=''  />"
                        strXML = strXML & "<set name=" + "'Content' value=" + "'" & readerStudent_Detail("D") & "' color=" + "'" & arr(0) & "' hoverText=''  />"
                        strXML = strXML & "<set name=" + "'Activities' value=" + "'" & readerStudent_Detail("E") & "' color=" + "'" & arr(0) & "' hoverText=''  />"
                        strXML = strXML & "<set name=" + "'Participant learning' value=" + "'" & Convert.ToInt32(readerStudent_Detail("F")) & "' color=" + "'" & arr(0) & "' hoverText=''  />"
                        strXML = strXML & "<set name=" + "'Total' value=" + "'" & readerStudent_Detail("Total") & "' color=" + "'" & arr(2) & "' hoverText=''  />"
                    Else
                        strXML = strXML & "<set name=" + "'Preparation and Planning' value=" + "'" & readerStudent_Detail("A") & "' color=" + "'" & arr(0) & "' hoverText=''  />"
                        strXML = strXML & "<set name=" + "'Use of resources and facilities' value=" + "'" & readerStudent_Detail("B") & "' color=" + "'" & arr(0) & "' hoverText=''  />"
                        '' strXML = strXML & "<set name=" + "'Delivery skills' value=" + "'" & readerStudent_Detail("C") & "' color=" + "'" & arr(0) & "' hoverText=''  />"
                        strXML = strXML & "<set name=" + "'Content' value=" + "'" & readerStudent_Detail("C") & "' color=" + "'" & arr(0) & "' hoverText=''  />"
                        strXML = strXML & "<set name=" + "'Activities' value=" + "'" & readerStudent_Detail("D") & "' color=" + "'" & arr(0) & "' hoverText=''  />"
                        strXML = strXML & "<set name=" + "'Participant learning' value=" + "'" & Convert.ToInt32(readerStudent_Detail("E")) & "' color=" + "'" & arr(0) & "' hoverText=''  />"
                        strXML = strXML & "<set name=" + "'Total' value=" + "'" & readerStudent_Detail("Total") & "' color=" + "'" & arr(2) & "' hoverText=''  />"
                    End If
                    strXML = strXML & "</graph>"
                    Headercell_8.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "800", "450", False)
                    Headerrow_3.Cells.Add(Headercell_8)
                    tblReport.Rows.Add(Headerrow_3)
                End While
            End If
        End Using


    End Function

    Private Shared Function GetGroupName(ByVal GroupId As String) As String
        'ByVal SurvId As String
        'SURVEYID= " & SurvId & " AND 
        Dim connection As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT GRPNAME FROM  SRV.COM_SUR_GROUP WHERE GRP_ID=" & GroupId & ""
        Dim dsSurveyInfo As DataSet
        dsSurveyInfo = SqlHelper.ExecuteDataset(connection, CommandType.Text, strSQL)
        If dsSurveyInfo.Tables(0).Rows.Count >= 1 Then
            Return dsSurveyInfo.Tables(0).Rows(0).Item(0).ToString()
        Else
            Return ""
        End If

    End Function

    Private Shared Function GetSurveyGroup(ByVal SurvId As String) As DataSet

        Dim connection As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT DISTINCT GROUPID FROM  SRV.COM_SUR_QUESTION WHERE SURVEYID= " & SurvId & " AND GroupID<>244 ORDER BY GROUPID "
        Dim dsSurveyInfo As DataSet
        dsSurveyInfo = SqlHelper.ExecuteDataset(connection, CommandType.Text, strSQL)
        Return dsSurveyInfo

    End Function

    Private Shared Function GetSurveyAnswers(ByVal SurvId As String, ByVal IntGrpId As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISSurveyConnection
        Dim strSQL As String = ""
        strSQL = "SELECT A.SURVEY_ID,A.GROUP_ID,A.QSTORDER, B.ANS_ID,B.ANS_DESC  FROM SRV.COM_SUR_QSTORDER A, SRV.COM_SUR_ANSWER B " & _
        "WHERE  A.ANS_ID=B.ANS_ID AND A.GROUP_ID= " & IntGrpId & " ORDER BY QSTORDER "

        '  strSQL = "SELECT A.SURVEY_ID,A.GROUP_ID,A.QSTORDER, B.ANS_ID,B.ANS_DESC  FROM SRV.COM_SUR_QSTORDER A, SRV.COM_SUR_ANSWER B " & _
        '"WHERE  A.ANS_ID=B.ANS_ID AND A.GROUP_ID= " & IntGrpId & "  AND B.ANS_DESC<>'No Answer'  ORDER BY QSTORDER "


        Dim command As SqlCommand = New SqlCommand(strSQL, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Private Shared Function GetSurveyQuestions(ByVal SurvId As String, ByVal GroupId As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISSurveyConnection
        Dim strSQL As String = "SELECT QUEST_ID,SURVEYID,QUESTDESC,ANSTYPE,GROUPID,COMMENTS " & _
              " FROM SRV.COM_SUR_QUESTION " & _
              " WHERE SURVEYID=" & SurvId & " AND GROUPID=" & GroupId & " AND ANSTYPE<>'C' ORDER BY QUEST_ID"

        Dim command As SqlCommand = New SqlCommand(strSQL, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function

    Private Function GetGroupResultCnt(ByVal Grpid As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT COUNT(*) FROM SRV.COM_SUR_QSTORDER WHERE GROUP_ID= " & Grpid
        Dim DsHelper As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

        If DsHelper.Tables(0).Rows.Count >= 0 Then
            Return DsHelper.Tables(0).Rows(0).Item(0)
        Else
            Return 0
        End If
    End Function

    Private Function GetAnswerTotal(ByVal QID As Integer, ByVal AnswerId As Integer, ByVal BusUnit As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        If ViewState("MainMnu_code") = "M000100" Then
            If AnswerId = 16 Then
                Return 0
                Exit Function
            End If
        End If

        strSQL = "SELECT Count(*) FROM SRV.COM_SUR_RESULTDET WHERE QUESTIONID=" & QID & " And AnswerId =" & AnswerId & " AND BUSID IN (" & BusUnit & ")"
        'strSQL = "SELECT Count(*) FROM SRV.COM_SUR_RESULTDET DET,SRV.COM_SUR_RESULTMST MST WHERE MST.RESULT_ID + 86 =DET.RESULTDET_ID AND DET.QUESTIONID=" & QID & " And DET.AnswerId =" & AnswerId & " AND MST.BUSINESSUNIT=" & BusUnit
        Dim DsHelper As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

        If DsHelper.Tables(0).Rows.Count >= 0 Then
            Return DsHelper.Tables(0).Rows(0).Item(0)
        Else
            Return 0
        End If
    End Function
    Private Function GetBusinessUnitName(ByVal BsuId As String) As String

        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = "SELECT BSU_NAME FROM dbo.BUSINESSUNITS WHERE BSU_ID='" & BsuId & "'"
        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsResponse.Tables(0).Rows.Count >= 1 Then
            With dsResponse.Tables(0).Rows(0)
                Return .Item(0)
            End With
        Else
            Return ""
        End If
    End Function
    Private Function GetPD_TITLE(ByVal CM_ID As String) As String
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim strSQL As String = ""
        'strSQL = "SELECT CM_TITLE FROM OASIS.PD_M.COURSE_M WHERE CM_ID='" & CM_ID & "'"
        'Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        'If dsResponse.Tables(0).Rows.Count >= 1 Then
        '    With dsResponse.Tables(0).Rows(0)
        '        Return .Item(0)
        '    End With
        'Else
        '    Return ""
        'End If

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strSQL As String = ""
        strSQL = "SELECT CM_TITLE FROM OASIS.PD_M.COURSE_M WHERE CM_ID In (Select ID FROM OASIS.dbo.fnSplitMe('" & CM_ID & "', '|'))"
        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsResponse.Tables(0).Rows.Count >= 1 Then

            Me.lstCourses.DataSource = dsResponse.Tables(0)
            Me.lstCourses.DataBind()

            Dim tmpStr As String = String.Empty
            For Each row As DataRow In dsResponse.Tables(0).Rows
                tmpStr &= row.Item("CM_Title") & ", "
            Next
            tmpStr = tmpStr.TrimEnd(" ")
            tmpStr = tmpStr.TrimEnd(",")
            'Me.DataList1.DataSource = dsResponse.Tables(0)
            'Me.DataList1.DataKeyField = "cm_title"
            'Me.DataList1.DataBind()
            Return tmpStr
        Else
            Return ""
        End If

    End Function
    Private Function GetTotalRead(ByVal BsuId As String, ByVal SurvyId As String) As Integer

        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = "SELECT COUNT(*) FROM SRV.COM_SUR_READERS WHERE SURVEYID=" & SurvyId & " AND BSUID IN ('" & BsuId & "')"
        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsResponse.Tables(0).Rows.Count >= 1 Then
            With dsResponse.Tables(0).Rows(0)
                Return .Item(0)
            End With
        Else
            Return 0
        End If
    End Function

    Private Function GetTotalSent(ByVal BsuId As String, ByVal SurvyId As String) As Integer

        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = "SELECT COUNT(*) FROM SRV.COM_SUR_SENT WHERE SURVEYID=" & SurvyId & " AND BSUID IN (" & BsuId & ")"
        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsResponse.Tables(0).Rows.Count >= 1 Then
            With dsResponse.Tables(0).Rows(0)
                Return .Item(0)
            End With
        Else
            Return 0
        End If
    End Function

    Private Function GetTotalResponse(ByVal BsuId As String, ByVal SurvyId As String) As Integer

        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = "SELECT COUNT(DISTINCT USER_NAME) AS TRESPONSE FROM SRV.COM_SUR_RESULTMST WHERE SURVEYID=" & SurvyId & " AND BUSINESSUNIT IN (" & BsuId & ")"
        Dim dsResponse As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsResponse.Tables(0).Rows.Count >= 1 Then
            With dsResponse.Tables(0).Rows(0)
                Return .Item(0)
            End With
        Else
            Return 0
        End If
    End Function

    Private Function GetTotalResponse() As Double
        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        ''get survey id 
        Dim courseType As String = GetCourseType()
        Dim surveyID As String = "185"

        If courseType = "2" Then
            surveyID = "193"
        Else
            surveyID = "185"
        End If

        strSQL = "SELECT RESPONSE FROM SRV.COM_SUR_DETAILS WHERE SURVEY_ID=" & surveyID
        Dim DsHelper As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

        If DsHelper.Tables(0).Rows.Count >= 0 Then
            Return DsHelper.Tables(0).Rows(0).Item(0)
        Else
            Return 0
        End If

    End Function





End Class
