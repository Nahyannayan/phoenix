﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comSurvThanks_GEN.aspx.vb" Inherits="masscom_comSurvThanks_GEN" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>:::GEMS OASIS::::</title>
    <script language="javascript" type="text/javascript">
// <!CDATA[

function Submit_onclick() 
{
 // alert("Test"); 
  window.close();
}
function Submitnot() 
{
 //alert ("test")
 window.history.forward(1);
}

// ]]>
</script>
<link href="../cssfiles/parentSurvey.css" rel="stylesheet" type="text/css" />
</head>
<body class="Surveybody"><div class="wrapper"><div class="header"></div> <div class="clear"></div>



    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        
        
    <div>
        <table style="width: 100%" border="0" class="BlueTable_simple">
            
            <tr>
                <td align="left" colspan="3" valign="middle">
                    <br />
                    <asp:Label ID="Label1" runat="server" cssclass="titleText" style="font-size:11pt;font-weight:bold;">Thank you for your interest in our survey. Your feedback is important to us.  Our records indicate that you have already participated in this survey.</asp:Label></td>
            </tr>
            <tr>
                <td align="center" colspan="3" valign="middle">
                    <br />
                    <asp:Label ID="Label2" runat="server" style="font-size:11pt;text-align:right;">نشكركم على اهتمامكم في استكمال الاستبيان الخاص بآرائكم وملاحظاتكم الكبيرة الأهمية بالنسبة لنا. سجلاتنا تشير إلى أنكم كنتم قد شاركتم بالفعل في هذا الاستطلاع.</asp:Label></td>
            </tr>
            <tr>
                <td align="center" colspan="3" style="font-weight: bold; color: blue; height: 21px">
                </td>
            </tr>
            
             <tr>
                <td  colspan="3" >
                    <br />
                    <asp:Label ID="Label3" runat="server" cssclass="titleText" style="font-size:11pt;font-weight:bold;"> &nbsp;</asp:Label></td>
            </tr>
            
             <tr>
                <td align="right" colspan="3" >
                    <br />
                    <asp:Label ID="Label4" runat="server" style="font-size:11pt;text-align:right;">شكرًا لاستكمالكم الاستبيان الخاص بأولياء الأمور حول مدرسة WINCHESTER SCHOOL - DUBAI ومؤسسة جيمس التعليمية. سوف تساعدنا ملاحظاتكم وآراؤكم القيمة على الاستمرار في تطوير خدماتنا التي نقدمها لأطفالكم. <br /><br />مع أطيب التحية والتقدير. <br /> رانجو أناند <br /> المدير / الرئيس التنفيذي</asp:Label></td>
            </tr>
            
            
                    <tr>
                    </tr>
                    
                    <td  id="A1" colspan="7" style="text-align: center" >
                    <input id="Button1" runat="server" visible='false' style="width: 117px; text-align: center; font-weight: bold; border-left-color: black; border-bottom-color: black; border-top-style: double; border-top-color: black; border-right-style: double; border-left-style: double; border-right-color: black; border-bottom-style: double;" type="button" value="Close" onclick='Submit_onclick()' />&nbsp;
                    </td>
            <tr>
                <td colspan="3" align="center">
                    </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
