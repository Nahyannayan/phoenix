Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Partial Class masscom_comSurKTAAEF
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private strHeaderImagePath As String
    Private strFooterImagePath As String
    Private intHeadCols As Integer
    Private nmvAnswers As New NameValueCollection
    Private nmvSurvRadioGrps As New NameValueCollection
    Private nmvSurvChkGrps As New NameValueCollection
    Private intPrintRows As Integer = 0
    Private intHeadColCnt As Integer = 0
    Private intHeaderRows As Integer = 0
    Dim intRow As Integer = 0
    Dim intAnswer As Integer = 0
    Dim intGrpRow As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Not IsPostBack Then
                ViewState("errMessage") = ""
                Dim CurBsUnit As String
                Dim USR_NAME As String

                ViewState("PD_CM_ID") = Encr_decrData.Decrypt(Request.QueryString("PD_CM_VAL").Replace(" ", "+"))
                ViewState("SurveyId_Encrypt") = "qIv2Q5+mFfs=" 'Request.QueryString("SurveyId").Replace(" ", "+")
                ViewState("SurveyId") = "185"
                ViewState("sBsuid") = Encr_decrData.Decrypt(Request.QueryString("BSU_ID").Replace(" ", "+"))
                ViewState("EmployeeId") = Encr_decrData.Decrypt(Request.QueryString("EMP_ID").Replace(" ", "+"))
                HF_Survey.Value = ViewState("SurveyId")
                Session("gm") = Nothing
                '&IsAdmin


                If ViewState("PD_CM_ID") = "" Then
                    Exit Sub
                End If

                '' Check to exist their their answer........
                'If GetIsExist(ViewState("SurveyId"), USR_NAME) >= 1 Then
                '    Response.Redirect("comSurvThanks.aspx?SurveyId=0")
                '    Exit Sub
                'End If

                If Request.QueryString("CRID") IsNot Nothing And Request.QueryString("CRID") <> "" Then
                    Dim id As Integer = Convert.ToInt32(Encr_decrData.Decrypt(Request.QueryString("CRID").Replace(" ", "+")))
                    ViewState("CR_ID") = id
                End If

                Header_table()
                Dim preview As String = 0
                ''included below by nahyan for mergin ktaa and eval on 15Nov2016
                If Request.QueryString("CRID") IsNot Nothing And Request.QueryString("CRID") <> "" Then
                    Dim id As Integer = Convert.ToInt32(Encr_decrData.Decrypt(Request.QueryString("CRID").Replace(" ", "+")))
                    ViewState("CR_ID") = id
                    ViewState("KTAA_ID") = ""
                    BindRelevanceToRole()
                    BindCourseDetails(Convert.ToInt32(ViewState("CR_ID")))
                    Session("COURSEACTION") = Nothing
                    Bind_KTAA_MASTER(ViewState("CR_ID"))

                End If

                If Request.QueryString("KTAAERR") Is Nothing Then
                    HF_KTAACHECK.Value = 0
                Else
                    Dim KTAAERR As String = Convert.ToString(Request.QueryString("KTAAERR").Replace(" ", "+"))
                    If (KTAAERR = "1") Then
                        HF_KTAACHECK.Value = 1
                        '' lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>******Please complete all mandatory* information required******</div>"
                        ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "isActive", "SubmitEvalClick();", True)
                    End If
                End If
              
                If Request.QueryString("DRAFSAVE") Is Nothing Then
                Else
                    Dim DRAFSAVE As String = Convert.ToString(Request.QueryString("DRAFSAVE").Replace(" ", "+"))
                    If (DRAFSAVE = "1") Then
                        ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "isActive", "DraftClick();", True)
                    End If

                End If
                'Updating Survey Read Count
            Else
                nmvSurvRadioGrps = Session("RadioGroups")

                'For intI As Integer = 0 To nmvSurvRadioGrps.Count - 1
                '    Response.Write(intI + 1.ToString() & " - " & nmvSurvRadioGrps.Item(intI).Split(",").GetValue(4) & " --- " & Request.Form(nmvSurvRadioGrps.Item(intI).Split(",").GetValue(2)) + "<br/>")
                '    If nmvSurvRadioGrps.Item(intI).Split(",").Length >= 6 Then
                '        Response.Write(Request.Form(nmvSurvRadioGrps.Item(intI).Split(",").GetValue(5)))
                '    End If
                'Next

                'Reading Survey Results ---------------------------------------------------------------------------------------
                Dim strAnswer As String = ""
                Dim strAnsId As Integer
                Dim QstId As Integer
                Dim GrpId As Integer
                Dim AddComments As String = ""
                Dim strDatamode As String = ""
                Dim NvcAnswerList As NameValueCollection
                'Build a collection for Survey answers
                NvcAnswerList = GetSurveyAnswerList()
                '-------------------------------------------------------------------------------------------
                'To check the All Questions are not Answerd
                ''commented on 27nov2016

                'Dim blnAnswered As Boolean = False
                'Dim strChkAnswer As String
                'For intI As Integer = 0 To 24 'nmvSurvRadioGrps.Count - 1
                '    Try
                '        strChkAnswer = Request.Form(nmvSurvRadioGrps.Item(intI).Split(",").GetValue(2)).ToString ' Question Answer
                '        If strChkAnswer <> "" Then ''AndstrChkAnswer <> "No Answer"
                '            blnAnswered = True
                '        End If
                '    Catch ex As Exception
                '        Response.Write("<script language='javascript' type='text/javascript'> alert(' Dear Parent, you are requested to complete all questions. Thank you.');history.back(1);</script>")
                '        Exit Sub
                '        ' Page.Response.Redirect("#")
                '    End Try
                'Next
                'If blnAnswered = False Then
                '    Response.Write("<script language='javascript' type='text/javascript'> alert('Dear Parent, you are requested to complete all questions. Thank you.');history.back(1);</script>")
                '    Exit Sub
                '    ' Page.Response.Redirect("#")
                'End If

                '-------------------------------------------------------------------------------------------
                If Request.QueryString("Ispreview") Is Nothing Then
                    ''22Nov2016
                    'For intI As Integer = 0 To 24 'nmvSurvRadioGrps.Count - 1
                    '    QstId = nmvSurvRadioGrps.Item(intI).Split(",").GetValue(3).ToString 'Question Id
                    '    GrpId = nmvSurvRadioGrps.Item(intI).Split(",").GetValue(0) 'Group Id
                    '    strAnswer = Request.Form(nmvSurvRadioGrps.Item(intI).Split(",").GetValue(2)).ToString ' Question Answer

                    '    If nmvSurvRadioGrps.Item(intI).Split(",").Length >= 6 Then
                    '        AddComments = Request.Form(nmvSurvRadioGrps.Item(intI).Split(",").GetValue(5)).ToString 'nmvSurvRadioGrps.Item(5).ToString ' Additional Comments, if any
                    '    End If
                    '    Try
                    '        strAnsId = NvcAnswerList.Get(strAnswer.Split(",").GetValue(0)) 'Answer Id
                    '    Catch ex As Exception
                    '    End Try

                    '    SaveSurveyResult(ViewState("SurveyId"), GrpId, QstId, strAnsId, strAnswer.Split(",").GetValue(0), AddComments, "Add")

                    '    For intSplitCnt As Integer = 1 To strAnswer.Split(",").Length
                    '        Try
                    '            If strAnswer.Split(",").GetValue(intSplitCnt) <> "No Answer" Then
                    '                strAnsId = NvcAnswerList.Get(strAnswer.Split(",").GetValue(intSplitCnt)) 'Answer Id 
                    '                SaveSurveyResult(ViewState("SurveyId"), GrpId, QstId, strAnsId, strAnswer.Split(",").GetValue(intSplitCnt), AddComments, "Child")
                    '            End If
                    '        Catch ex As Exception
                    '        End Try

                    '    Next ' End Answer spliting loop

                    'Next 'End Group Loop
                    'Savecontact_opinion(ViewState("SurveyId"))
                    '-----------------------------------------------------------------------------------------------
                End If

                If Request.QueryString("Ispreview") Is Nothing Then
                    ''22Nov2016
                    ''  UpdateSurveyResponse(ViewState("SurveyId"))
                End If

                ViewState("PartcId") = Encr_decrData.Encrypt(Session("sUsr_name").ToString())

                'Instead of thanks, redirect the parent to the leadership survey if the current survey id is 181
                '
                Dim connection As String = ConnectionManger.GetOASISSurveyConnectionString
                Dim strThanks As String
                Dim PartcId As String
                Dim lstrLeadSurSendingId As String
                If ViewState("SurveyId") = "181" Then
                    Dim sql_query = "SELECT * FROM SRV.COM_SUR_BUSNSUNIT  A INNER JOIN dbo.SURVEY_SENDING_LOG  B ON SURVEYID=178 AND [ID]=RECORD_GROUP_ID WHERE SurveyId=178 AND BUSUNITID='" & ViewState("sBsuid") & "'"
                    Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.Text, sql_query)
                        If readerStudent_Detail.HasRows = True Then
                            While readerStudent_Detail.Read
                                lstrLeadSurSendingId = Encr_decrData.Encrypt(Convert.ToString(readerStudent_Detail("SENDING_ID")))
                                Response.Redirect("comSurPrintSurvey_PSI.aspx?SurveyId=p7OG5Gwv6Gk=&PartcId=" + ViewState("PartcId") + "&SendingId=" + lstrLeadSurSendingId + "")
                            End While
                        End If
                    End Using
                Else
                    'If ViewState("errMessage") <> "" Then
                    '    Dim cmId As String = Encr_decrData.Encrypt(ViewState("PD_CM_ID"))
                    '    Dim BSU_ID As String = Encr_decrData.Encrypt(ViewState("sBsuid"))
                    '    Dim EMP_ID As String = Encr_decrData.Encrypt(ViewState("EmployeeId"))
                    '    Dim CRID As String = Encr_decrData.Encrypt(ViewState("CR_ID"))
                    '    Response.Redirect("comSurKTAAEF.aspx?PD_CM_VAL=" & cmId & "&BSU_ID=" & BSU_ID & "&EMP_ID=" & EMP_ID & "&CRID=" & CRID & "&KTAAERR=1" + "")
                    'Else
                    '    ''Response.Redirect("comSurvThanks_GEN.aspx?SurveyId=" & ViewState("SurveyId_Encrypt") & "&PartcId=" + ViewState("PartcId") + "")
                    'End If
                End If


            End If

        Catch ex As Exception

        End Try

    End Sub
    Protected Sub btnSaveEval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveEval.Click
        Dim strAnswer As String = ""
        Dim strAnsId As Integer
        Dim QstId As Integer
        Dim GrpId As Integer
        Dim AddComments As String = ""
        Dim strDatamode As String = ""
        Dim NvcAnswerList As NameValueCollection

        ViewState("SAVEDRAFT") = "0"
        'Build a collection for Survey answers
        NvcAnswerList = GetSurveyAnswerList()

        Dim blnAnswered As Boolean = False
        Dim strChkAnswer As String
        For intI As Integer = 0 To 24 'nmvSurvRadioGrps.Count - 1
            Try
                strChkAnswer = Request.Form(nmvSurvRadioGrps.Item(intI).Split(",").GetValue(2)).ToString ' Question Answer
                If strChkAnswer <> "" Then ''AndstrChkAnswer <> "No Answer"
                    blnAnswered = True
                End If
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "isActive", "SubmitEvalClick();", True)
                Header_table()
                Exit Sub
            End Try
        Next
        If blnAnswered = False Then
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "isActive", "SubmitEvalClick();", True)
            Header_table()
            Exit Sub


        Else

            For intI As Integer = 0 To 24 'nmvSurvRadioGrps.Count - 1
                QstId = nmvSurvRadioGrps.Item(intI).Split(",").GetValue(3).ToString 'Question Id
                GrpId = nmvSurvRadioGrps.Item(intI).Split(",").GetValue(0) 'Group Id
                strAnswer = Request.Form(nmvSurvRadioGrps.Item(intI).Split(",").GetValue(2)).ToString ' Question Answer

                If nmvSurvRadioGrps.Item(intI).Split(",").Length >= 6 Then
                    AddComments = Convert.ToString(Request.Form(nmvSurvRadioGrps.Item(intI).Split(",").GetValue(5))) 'nmvSurvRadioGrps.Item(5).ToString ' Additional Comments, if any
                End If
                Try
                    strAnsId = NvcAnswerList.Get(strAnswer.Split(",").GetValue(0)) 'Answer Id
                Catch ex As Exception
                End Try

                SaveSurveyResult(ViewState("SurveyId"), GrpId, QstId, strAnsId, strAnswer.Split(",").GetValue(0), AddComments, "Add")

                For intSplitCnt As Integer = 1 To strAnswer.Split(",").Length
                    Try
                        If strAnswer.Split(",").GetValue(intSplitCnt) <> "No Answer" Then
                            strAnsId = NvcAnswerList.Get(strAnswer.Split(",").GetValue(intSplitCnt)) 'Answer Id 
                            SaveSurveyResult(ViewState("SurveyId"), GrpId, QstId, strAnsId, strAnswer.Split(",").GetValue(intSplitCnt), AddComments, "Child")
                        End If
                    Catch ex As Exception
                    End Try

                Next ' End Answer spliting loop

            Next 'End Group Loop
            Savecontact_opinion(ViewState("SurveyId"))

            UpdateSurveyResponse(ViewState("SurveyId"))
        End If

        If ViewState("errMessage") <> "" Then
            Dim cmId As String = Encr_decrData.Encrypt(ViewState("PD_CM_ID"))
            Dim BSU_ID As String = Encr_decrData.Encrypt(ViewState("sBsuid"))
            Dim EMP_ID As String = Encr_decrData.Encrypt(ViewState("EmployeeId"))
            Dim CRID As String = Encr_decrData.Encrypt(ViewState("CR_ID"))
            Response.Redirect("comSurKTAAEF.aspx?PD_CM_VAL=" & cmId & "&BSU_ID=" & BSU_ID & "&EMP_ID=" & EMP_ID & "&CRID=" & CRID & "&KTAAERR=1" + "")
        Else
            Response.Redirect("comSurvThanks_GEN.aspx?SurveyId=" & ViewState("SurveyId_Encrypt") & "&PartcId=" + ViewState("PartcId") + "")
        End If
    End Sub

    Protected Sub btnDraftEval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDraftEval.Click
        Dim strAnswer As String = ""
        Dim strAnsId As Integer
        Dim QstId As Integer
        Dim GrpId As Integer
        Dim AddComments As String = ""
        Dim strDatamode As String = ""
        Dim NvcAnswerList As NameValueCollection

        ViewState("SAVEDRAFT") = "1"
        'Build a collection for Survey answers
        NvcAnswerList = GetSurveyAnswerList()

        For intI As Integer = 0 To 24 'nmvSurvRadioGrps.Count - 1
            QstId = nmvSurvRadioGrps.Item(intI).Split(",").GetValue(3).ToString 'Question Id
            GrpId = nmvSurvRadioGrps.Item(intI).Split(",").GetValue(0) 'Group Id
            ''included convert.tostring nahyan 22nov2016
            strAnswer = Convert.ToString(Request.Form(nmvSurvRadioGrps.Item(intI).Split(",").GetValue(2))) ' Question Answer

            If nmvSurvRadioGrps.Item(intI).Split(",").Length >= 6 Then
                AddComments = Convert.ToString(Request.Form(nmvSurvRadioGrps.Item(intI).Split(",").GetValue(5))) 'nmvSurvRadioGrps.Item(5).ToString ' Additional Comments, if any
            End If
            Try
                ''included checking by nahyan 22nov2016
                If (strAnswer <> "") Then
                    strAnsId = NvcAnswerList.Get(strAnswer.Split(",").GetValue(0)) 'Answer Id
                End If
            Catch ex As Exception
            End Try
            ''included checking by nahyan 22nov2016
            If (strAnswer <> "") Then


                SaveSurveyResult(ViewState("SurveyId"), GrpId, QstId, strAnsId, strAnswer.Split(",").GetValue(0), AddComments, "Add")

                For intSplitCnt As Integer = 1 To strAnswer.Split(",").Length
                    Try
                        If strAnswer.Split(",").GetValue(intSplitCnt) <> "No Answer" Then
                            strAnsId = NvcAnswerList.Get(strAnswer.Split(",").GetValue(intSplitCnt)) 'Answer Id 
                            SaveSurveyResult(ViewState("SurveyId"), GrpId, QstId, strAnsId, strAnswer.Split(",").GetValue(intSplitCnt), AddComments, "Child")
                        End If
                    Catch ex As Exception
                    End Try

                Next ' End Answer spliting loop
            End If
        Next 'End Group Loop

        Savecontact_opinion(ViewState("SurveyId"))

        UpdateSurveyResponse(ViewState("SurveyId"))


        If ViewState("errMessage") <> "" Then
            Dim cmId As String = Encr_decrData.Encrypt(ViewState("PD_CM_ID"))
            Dim BSU_ID As String = Encr_decrData.Encrypt(ViewState("sBsuid"))
            Dim EMP_ID As String = Encr_decrData.Encrypt(ViewState("EmployeeId"))
            Dim CRID As String = Encr_decrData.Encrypt(ViewState("CR_ID"))
            Response.Redirect("comSurKTAAEF.aspx?PD_CM_VAL=" & cmId & "&BSU_ID=" & BSU_ID & "&EMP_ID=" & EMP_ID & "&CRID=" & CRID & "&KTAAERR=1" + "")
        Else
            Dim cmId As String = Encr_decrData.Encrypt(ViewState("PD_CM_ID"))
            Dim BSU_ID As String = Encr_decrData.Encrypt(ViewState("sBsuid"))
            Dim EMP_ID As String = Encr_decrData.Encrypt(ViewState("EmployeeId"))
            Dim CRID As String = Encr_decrData.Encrypt(ViewState("CR_ID"))
            Response.Redirect("comSurKTAAEF.aspx?PD_CM_VAL=" & cmId & "&BSU_ID=" & BSU_ID & "&EMP_ID=" & EMP_ID & "&CRID=" & CRID & "&DRAFSAVE=1" + "")

            ' Response.Redirect("comSurvThanks_GEN.aspx?SurveyId=" & ViewState("SurveyId_Encrypt") & "&PartcId=" + ViewState("PartcId") + "")
        End If
    End Sub

    Private Function Savecontact_opinion(ByVal SurveyId As Integer) As Boolean
        Dim contactparent As Integer
        Dim contactparent2 As Integer
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISSurveyConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try





                Dim COMM1 As String = String.Empty
                Dim COMM2 As String = String.Empty
                Dim COMM3 As String = String.Empty

                Dim CHK1 As String = String.Empty
                Dim CHK2 As String = String.Empty
                Dim CHK3 As String = String.Empty
                Dim CHK4 As String = String.Empty
                Dim CHK5 As String = String.Empty
                Dim CHK6 As String = String.Empty


                If ViewState("SurveyId") <> "181" Then
                    CHK1 = Request.Form("HF_2441").ToString
                    CHK2 = Request.Form("HF_2442").ToString
                    CHK3 = Request.Form("HF_2443").ToString
                    CHK4 = Request.Form("HF_2444").ToString
                    CHK5 = Request.Form("HF_2445").ToString
                    CHK6 = Request.Form("HF_2446").ToString
                    COMM1 = Request.Form("HF_COMM1").ToString
                    COMM2 = Request.Form("HF_COMM2").ToString
                    COMM3 = Request.Form("HF_COMM3").ToString
                Else

                End If

                ''#34798 added by nahyan on 16nov2016 for inserting schollevel details 
                Dim schoolevel As String = ""
                ''EC
                If chkLEVELEC.Checked Then
                    schoolevel = "EC,"

                End If

                If chkLEVELP.Checked Then
                    schoolevel = schoolevel & "P,"

                End If
                If chkLEVELS.Checked Then
                    schoolevel = schoolevel & "S,"

                End If
                If chkLEVELALL.Checked Then
                    schoolevel = schoolevel & "ALL"

                End If

                schoolevel = schoolevel.TrimEnd(",")




                'Dim contactparent As Integer = 1
                Dim param(15) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@CM_ID", ViewState("PD_CM_ID"))
                param(1) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EmployeeId"))
                param(2) = New SqlClient.SqlParameter("@Survey_id  ", "185")
                param(3) = New SqlClient.SqlParameter("@Bsu_id ", ViewState("sBsuid"))
                param(4) = New SqlClient.SqlParameter("@CH1", CHK1)
                param(5) = New SqlClient.SqlParameter("@CH2", CHK2)
                param(6) = New SqlClient.SqlParameter("@CH3", CHK3)
                param(7) = New SqlClient.SqlParameter("@CH4", CHK4)
                param(8) = New SqlClient.SqlParameter("@CH5", CHK5)
                param(9) = New SqlClient.SqlParameter("@CH6", CHK6)
                param(10) = New SqlClient.SqlParameter("@COMM1", COMM1)
                param(11) = New SqlClient.SqlParameter("@COMM2", COMM2)
                param(12) = New SqlClient.SqlParameter("@COMM3", COMM3)
                ''incvluded bynahyan on 16nov2016 for new eval form
                param(13) = New SqlClient.SqlParameter("@schoollevel", schoolevel)

                If ViewState("SAVEDRAFT") = "1" Then
                    param(14) = New SqlClient.SqlParameter("@submit", 0)
                ElseIf ViewState("SAVEDRAFT") = "0" Then
                    param(14) = New SqlClient.SqlParameter("@submit", 1)
                End If



                'Dim str_query As String = "exec SRV.SAVE_COM_SUR_CON_PARENT " &  ViewState("sBsuid") & ",'" & Session("sUsr_name") & _
                '"'," & SurveyId & ",'" & GrpName & "'," & QuestionId & "," & AnsId & ",'" & strAnswer.Replace("'", "''") & "'," & _
                '"'" & strComments.Replace("'", "''") & "','" & Datamode & "'"

                SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "SRV.SAVE_PD_SURVEY", param)



                'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "RESULT_ID(" + HiddenField1.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)

                'If flagAudit <> 0 Then
                '    Throw New ArgumentException("Could not process your request")
                'End If
                transaction.Commit()
                ''included by nahyan 17Nov2016 to invoke KTAA Send to pdc
                If ViewState("SAVEDRAFT") = "1" Then
                    btnSave_Click(btnSavetoPDC, New EventArgs())
                ElseIf ViewState("SAVEDRAFT") = "0" Then
                    btnSavetoPDC_Click(btnSavetoPDC, New EventArgs())
                End If


                '' Response.Write("Record Saved Successfully")

            Catch myex As ArgumentException
                transaction.Rollback()
                Response.Write(myex.Message)
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                Response.Write("Record could not be Saved")
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Function
    Private Function SaveSurveyResult(ByVal SurveyId As Integer, ByVal GrpName As String, ByVal QuestionId As Integer, ByVal AnsId As Integer, _
    ByVal strAnswer As String, ByVal strComments As String, ByVal Datamode As String) As Boolean

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISSurveyConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                'ViewState("SENDING_ID") = 11
                strAnswer = strAnswer.Replace("Additional comments and / or suggestions.", " ")
                Dim param(10) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@BUSINESSUNIT", ViewState("sBsuid"))
                param(1) = New SqlClient.SqlParameter("@USERNAME", ViewState("EmployeeId"))
                param(2) = New SqlClient.SqlParameter("@SURVEYID", SurveyId)
                param(3) = New SqlClient.SqlParameter("@GROUPNAME", GrpName)
                param(4) = New SqlClient.SqlParameter("@QUESTIONID", QuestionId)
                param(5) = New SqlClient.SqlParameter("@ANSWERID", AnsId)
                param(6) = New SqlClient.SqlParameter("@ANSWER", strAnswer.Replace("'", "''"))
                param(7) = New SqlClient.SqlParameter("@COMMENTS", strComments.Replace("'", "''"))
                param(8) = New SqlClient.SqlParameter("@DATAMODE", Datamode)
                param(9) = New SqlClient.SqlParameter("@SENDING_ID", ViewState("PD_CM_ID"))

                'Dim str_query As String = "exec SRV.saveSURVEYRESULTS " &  ViewState("sBsuid") & ",'" & Session("sUsr_name") & _
                '"'," & SurveyId & ",'" & GrpName & "'," & QuestionId & "," & AnsId & ",'" & strAnswer.Replace("'", "''") & "'," & _
                '"'" & strComments.Replace("'", "''") & "','" & Datamode & "'"

                HiddenField1.Value = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "SRV.saveSURVEYRESULTS", param)

                'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "RESULT_ID(" + HiddenField1.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)

                'If flagAudit <> 0 Then
                '    Throw New ArgumentException("Could not process your request")
                'End If
                transaction.Commit()
                '' Response.Write("Record Saved Successfully")

            Catch myex As ArgumentException
                transaction.Rollback()
                Response.Write(myex.Message)
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                Response.Write("Record could not be Saved")
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Function

    Sub Header_table()
        Try
            Dim strTest As String
            Dim tblReport As New Table
            Dim tblSubmit As New Table
            Dim intGroupCount As Integer
            tblReport.CssClass = "BlueTable_simple"
            ' ''tblReport.BorderColor = Drawing.Color.Black
            ' ''tblReport.BackColor = System.Drawing.ColorTranslator.FromHtml("#BFEDFB")

            tblReport.Style("Width") = "950px"
            tblReport.CellSpacing = "0"
            tblReport.CellPadding = "0"

            Dim dsSurvey As DataSet = GetSurveyInfo(ViewState("SurveyId"))

            For intRows As Integer = 2 To 3

                Dim Headercell_Desc As New TableCell
                Headercell_Desc.Style("text-align") = "left"
                Headercell_Desc.Style("letter-spacing") = "1px"
                ''Headercell_Desc.Style("Width") = "950px"
                Headercell_Desc.Style("Height") = "18px"
                ' '' Headercell_Desc.BackColor = System.Drawing.ColorTranslator.FromHtml("#BFEDFB")
                ' ''Headercell_Desc.Style("font-family") = "calibri,verdana,corisande,timesroman,garamond"
                ' ''Headercell_Desc.BorderColor = Drawing.Color.Black
                ' ''Headercell_Desc.BorderStyle = BorderStyle.Solid
                Headercell_Desc.BorderWidth = "0"
                Headercell_Desc.ColumnSpan = GetAnswerCount("") + 1
                If intRows = 1 Then
                    Headercell_Desc.Style("text-align") = "left"
                    strTest = getBusinessUnitLogo(ViewState("sBsuid"))
                    strTest = strTest.Substring(1, strTest.Length - 1)
                    '' Headercell_Desc.Text = "<Img Src='.." + strTest + "' height='72'>" '"..\Images/Gems.GIF"   "..\images\schools\rds\RDS-OSAS.jpg"
                ElseIf intRows = 2 Then
                    Headercell_Desc.CssClass = "mainheading"
                    ''commented by nahyan on 20nov2016
                    '' Headercell_Desc.Text = dsSurvey.Tables(0).Rows(0).Item("SURVTITLE").ToString().Replace("&&BR", "<BR>") 'SURVTITLE
                    Headercell_Desc.Text = "* indicates mandatory section / fields"
                    Headercell_Desc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF")
                    'Headercell_Desc.Style("font-weight") = "bold"
                    Headercell_Desc.Style("font-size") = "10pt"
                ElseIf intRows = 3 Then
                    'Headercell_Desc.Style("font-size") = "8pt"
                    'Headercell_Desc.Style("TEXT-ALIGN") = "justify"
                    'Headercell_Desc.Style("TEXT-JUSTIFY") = "auto"
                    'changed here to add this comment
                    'Headercell_Desc.Text = "At GEMS Education we respect your privacy, therefore please tick the box if you do not mind us contacting you regarding your survey responses-<br/> <center>Yes<input id='contactyes' name='yesno' value='1' checked='checked' type='radio' />      No<input id='contactno' name='yesno'value='0' type='radio' /></center>"
                    '  Headercell_Desc.Text = dsSurvey.Tables(0).Rows(0).Item("SURVDESC").ToString().Replace("&&BR", "<BR>")
                    If ViewState("SurveyId") = "181" Then

                        Dim chkLabel_Main As New Label
                        Dim chkLabel_Main2 As New Label
                        chkLabel_Main.Text = "Thank you for choosing to provide us with your important feedback as it helps guide school improvement. The survey will take a few minutes of your time and simply requires you to click on the word that best describes your view on educational aspects (top half) or the additional services provided by/for the school (bottom half). Survey responses will go directly to GEMS Corporate Office.  Once results have been collated the school will receive the overall response to each question. <br><br>"
                        Dim chkCheck As New HtmlInputCheckBox

                        Dim chkLabel As New Label
                        Dim chkLabelNew As New Label
                        Dim chkLabelNew2 As New Label
                        'chkCheck.ID = "Chk123"
                        'chkCheck.Name = "Chk123"
                        'chkCheck.Value = "Off"
                        'chkCheck.Checked = True
                        chkLabel_Main2.Text = "<div style='padding-top:10px;'>We appreciate your support.</div>"
                        'chkLabel.CssClass = "chkLabel"
                        'chkLabel.Text = "<input type='checkbox' id='Chk123' runat='server' value='1' checked=True>I am happy for someone from the school to contact me"

                        chkLabelNew.CssClass = "chkLabel"
                        chkLabelNew.Text = "<br><input type='checkbox' id='Chk1232' runat='server' value='1' checked=True> Please tick if you are happy for someone to contact you about your responses"

                        chkLabelNew2.CssClass = "chkLabel"
                        chkLabelNew2.Text = "<br><input type='checkbox' id='Chk1231' runat='server' value='1' checked=True> I am happy for someone from GEMS central office to contact me"



                        Headercell_Desc.Controls.Add(chkLabel_Main)
                        ' Headercell_Desc.Controls.Add(chkCheck)
                        'Headercell_Desc.Controls.Add(chkLabel)
                        Headercell_Desc.Controls.Add(chkLabelNew)
                        ' Headercell_Desc.Controls.Add(chkLabelNew2)
                        Headercell_Desc.Controls.Add(chkLabel_Main2)
                    ElseIf ViewState("SurveyId") = "178" Then
                        Dim chkLabel_Main As New Label
                        Dim chkLabel_Main2 As New Label
                        chkLabel_Main.Text = "In this questionnaire the term Principal is applicable to the most senior leader in a school. <br><br> e.g. Superintendent / Head of School whichever title is applicable  <br><br>"
                        Headercell_Desc.Controls.Add(chkLabel_Main)
                    End If
                    Headercell_Desc.CssClass = "titleText"

                End If



                Dim Headerrow_1 As New TableRow
                Headerrow_1.Style("font-weight") = "bold"
                Headerrow_1.Style("font-size") = "8pt"
                Headerrow_1.Style("text-align") = "center"
                Headerrow_1.Style("Height") = "20px"
                ' '' Headerrow_1.BackColor = System.Drawing.ColorTranslator.FromHtml("#BFEDFB")
                ' ''Headerrow_1.Style("font-family") = "calibri,verdana,corisande,timesroman,garamond"
                Headerrow_1.Cells.Add(Headercell_Desc)
                tblReport.Rows.Add(Headerrow_1)

            Next

            ' Get the Survey Groups Based on the Single Survey........
            For Each grpRow As DataRow In GetSurveyGroup(ViewState("SurveyId")).Tables(0).Rows

                intGroupCount = GetSurveyGroup(ViewState("SurveyId")).Tables(0).Rows.Count
                If intGroupCount > 2 Then
                    'Create A Group Row for Group Seperation----------
                    Dim Headerrow_1 As New TableRow
                    Headerrow_1.CssClass = "trSub_Header"
                    'Headerrow_1.Style("font-weight") = "bold"
                    'Headerrow_1.Style("font-size") = "12pt"
                    Headerrow_1.Style("text-align") = "left"
                    '' Headerrow_1.Style("Height") = "30px"
                    ' ''Headerrow_1.ForeColor = Drawing.Color.DarkBlue
                    ' ''Headerrow_1.BackColor = System.Drawing.ColorTranslator.FromHtml("#BFEDFB")
                    ' '' Headerrow_1.Style("font-family") = "calibri,verdana,corisande,timesroman,garamond"

                    Dim Groupcell_Desc As New TableCell
                    '' Groupcell_Desc.CssClass = "trSub_Header"
                    '' Groupcell_Desc.Style("text-align") = "left"
                    ' Groupcell_Desc.BackColor = System.Drawing.ColorTranslator.FromHtml("#BFEDFB")
                    Groupcell_Desc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF")
                    Groupcell_Desc.ColumnSpan = GetAnswerCount("") + 1
                    ''commented by nahyan on 21nov2016
                    '' Groupcell_Desc.Text = "" & grpRow.Item("GRPNAME").ToString().Replace("&&BR", "<br>") '"Test 123"
                    If ViewState("SurveyId") <> "178" And ViewState("SurveyId") <> "179" Then
                        Headerrow_1.Cells.Add(Groupcell_Desc)
                    End If

                    tblReport.Rows.Add(Headerrow_1)
                End If
                '--------------------------------------------------
                'If ViewState("SurveyId") <> "178" Then
                BuildSUBHeading(tblReport, grpRow.Item("GROUPID").ToString()) ' Table headings 
                'End If

                BuildQuestions(tblReport, grpRow.Item("GROUPID").ToString())
                'Create A blank Row for Group Seperation
                'If intGroupCount <= 2 Then
                '    Dim Headerrow_1 As New TableRow
                '    ' Headerrow_1.Style("font-weight") = "bold"
                '    Headerrow_1.Style("font-size") = "11pt"
                '    Headerrow_1.Style("text-align") = "center"
                '    Headerrow_1.Style("Height") = "35px"
                '    ' ''Headerrow_1.ForeColor = Drawing.Color.DarkBlue
                '    ' ''Headerrow_1.BackColor = System.Drawing.ColorTranslator.FromHtml("#BFEDFB")
                '    ' ''Headerrow_1.Style("font-family") = "calibri,verdana,corisande,timesroman,garamond"
                '    tblReport.Rows.Add(Headerrow_1)
                'End If

            Next

            'Buld a LAst footer Row for Displaying Footer Image
            ''added by nahyan on 13thoct2016 to include KTAA form 
            ' Form.Controls.Add(tbltitleInfo)
            ' Form.Controls.Add(tblEmpInfo)
            ''nahyan ends here
            CreateBottomRow(tblReport)
            ' Form.Controls.Add(tblReport)
            pnlSur.Controls.Add(tblReport)


            'Page.Controls.Add(tblReport)
            'Page.Controls.Add(New LiteralControl("<br>"))
            Form.Controls.Add(New LiteralControl("<br>"))
            ''added by nahyan on 13thoct2016 to include KTAA form 
            CreateButtonRow(tblSubmit)

            'CreateBottomRow(tblKTAA)
            'Form.Controls.Add(tblKTAA)
            Form.Controls.Add(tblSubmit)
            ' -- Fetch the answers for the checkbox and text area part here


        Catch ex As Exception

        End Try
    End Sub

    Private Function BuildSUBHeading(ByVal tblReport As Table, ByVal GroupId As String)

        ' Dim intCnt As Integer = 1
        'Sub Headings
        Dim Headerrow_2 As New TableRow


        Dim Headercell_1 As New TableCell
        Headercell_1.Style("Width") = "65px"
        Headercell_1.CssClass = "headerMainCell"
        Headercell_1.Text = "No."
        Headerrow_2.Cells.Add(Headercell_1)

        Dim Headercell_4 As New TableCell
        Headercell_4.CssClass = "headerMainCell"
        Headercell_4.ColumnSpan = "0"

        ''commented and added new by nahyan on 21Nov2016
        'If GroupId <> "205" Then
        '    Headercell_4.Text = "Questions"
        'Else
        '    Headercell_4.Text = "Services"
        'End If

        If GroupId = "208" Then
            Headercell_4.Text = "Preparation and Planning *"
        End If

        If GroupId = "209" Then
            Headercell_4.Text = "Use of resources and facilities *"
        End If
        If GroupId = "210" Then
            Headercell_4.Text = "Delivery skills *"
        End If
        If GroupId = "211" Then
            Headercell_4.Text = "Content *"
        End If
        If GroupId = "212" Then
            Headercell_4.Text = "Activities *"
        End If
        If GroupId = "213" Then
            Headercell_4.Text = "Participant learning *"
        End If

        If GroupId = "244" Then
            Headercell_4.Text = "How will the workshop influence your professional practice? "
        End If

        Headerrow_2.Cells.Add(Headercell_4)
        Dim intRecAffected As Integer = 0
        Using SqlAnswers As SqlDataReader = GetSurveyAnswers("", GroupId)
            If SqlAnswers.HasRows = True Then
                While SqlAnswers.Read
                    Dim Headercell_8 As New TableCell
                    Headercell_8.CssClass = "headerMainCell"

                    Headercell_8.ColumnSpan = "0"
                    Headercell_8.Wrap = True

                    Headercell_8.Text = SqlAnswers("ANS_DESC").ToString()
                    Headercell_8.Style("Width") = "200px"
                    Headercell_8.Style("color") = "#000000"
                    ''changing the colors for agree disagree etc nahyan on 20 ov2016
                    If (Headercell_8.Text = "Strongly agree") Then
                        Headercell_8.Style("background-color") = "#3FD117"

                    End If
                    If (Headercell_8.Text = "Agree") Then
                        Headercell_8.Style("background-color") = "#F7F55C"
                    End If

                    If (Headercell_8.Text = "Disagree") Then
                        Headercell_8.Style("background-color") = "#f4bc42"
                    End If
                    If (Headercell_8.Text = "Strongly disagree") Then
                        Headercell_8.Style("background-color") = "#F73802"
                    End If

                    Headerrow_2.Cells.Add(Headercell_8)
                    nmvAnswers.Add(intHeadColCnt, SqlAnswers("ANS_DESC").ToString())
                    'intHeadColCnt = intHeadColCnt + 1
                End While
            End If
            tblReport.Rows.Add(Headerrow_2)
        End Using
        intHeadColCnt = intHeadColCnt + 1
    End Function

    Private Function BuildQuestions(ByVal tblReport As Table, ByVal GroupId As String)
        Dim connection As SqlConnection = ConnectionManger.GetOASISSurveyConnection

        If ViewState("SurveyId") <> "178" And ViewState("SurveyId") <> "179" Then
            intRow = 0
            intAnswer = 0
            intGrpRow = 0
        End If

        Using SurveyQuest As SqlDataReader = GetSurveyQuestions(ViewState("SurveyId"), GroupId)
            If SurveyQuest.HasRows = True Then
                While SurveyQuest.Read
                    Dim Headerrow_3 As New TableRow
                    Headerrow_3.ID = SurveyQuest("QUEST_ID").ToString()
                    Dim pParms(3) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EmployeeId"))
                    pParms(1) = New SqlClient.SqlParameter("@PD_CM_ID", ViewState("PD_CM_ID"))
                    pParms(2) = New SqlClient.SqlParameter("@QUEST_ID", SurveyQuest("QUEST_ID").ToString())
                    Dim lstrAnswer = 10
                    Using Ansreader As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "GET_PD_ANSWERS", pParms)
                        While Ansreader.Read
                            lstrAnswer = Convert.ToString(Ansreader("AnsKey"))
                        End While
                    End Using

                    HF_qustid.Value = HF_qustid.Value + "," + SurveyQuest("QUEST_ID").ToString()

                    intRow = intRow + 1
                    intGrpRow = intGrpRow + 1
                    intAnswer = 0
                    'Building Cells of Each Row
                    For intCols As Integer = 1 To GetGroupResultCnt(GroupId) + 2

                        Dim Headercell_8 As New TableCell

                        Headercell_8.ColumnSpan = "0"
                        Headercell_8.Wrap = False
                        If intRow Mod 2 = 0 Then
                            Headercell_8.CssClass = "trStyleEven"
                        Else

                            Headercell_8.CssClass = "trStyleOdd"
                        End If

                        If intCols = 1 Then

                            Headercell_8.Text = intRow.ToString 'SurveyQuest("QUEST_ID").ToString()

                            Headercell_8.Style("text-align") = "center !important;"
                        ElseIf intCols = 2 Then
                            'padding-left:7


                            Headercell_8.Style("padding-left") = "7px"
                            Headercell_8.Text = SurveyQuest("QUESTDESC").ToString().Replace("&&BR", "<br>").Replace("&&B", "<b>")
                            Headercell_8.Style("Width") = "900px"
                            Headercell_8.Style("text-align") = "left"
                        Else

                            If SurveyQuest("ANSTYPE") = "S" Then
                                If ViewState("intRow") <> intRow Then
                                    Dim cnt As Integer = Convert.ToInt64(HF_Cnt.Value) + 1
                                    HF_Cnt.Value = cnt

                                    If ViewState("SurveyId") = "177" Then
                                        HF_Cnt.Value = 6
                                    End If
                                    ViewState("intRow") = intRow
                                End If

                                Dim RadRadio As New HtmlInputRadioButton
                                ' RadRadio.ID = "rad" & intPrintRows.ToString()
                                RadRadio.ID = "rad" & intPrintRows.ToString & intCols.ToString()
                                RadRadio.Name = "Rad" & GroupId.ToString & intRow.ToString()
                                RadRadio.Value = nmvAnswers.Item(intHeaderRows).Split(",").GetValue(intAnswer).ToString
                                If lstrAnswer = intCols - 2 Then
                                    RadRadio.Checked = True
                                End If
                                RadRadio.Attributes.Add("onclick", "aa(this,'" & RadRadio.Name & "','" & SurveyQuest("QUEST_ID").ToString() & "')")
                                Headercell_8.Controls.Add(RadRadio)

                                If intCols = GetGroupResultCnt(GroupId) + 3 Then
                                    RadRadio.Checked = True  'Settings of No Answer Column
                                    Headercell_8.Style("Width") = "5px"
                                    Headercell_8.Style("display") = "none"
                                    Headercell_8.Visible = False
                                End If
                                If intCols = 3 Then
                                    'RadRadio.Checked = True
                                End If

                            ElseIf SurveyQuest("ANSTYPE") = "M" Then
                                Dim chkCheck As New HtmlInputCheckBox
                                chkCheck.ID = "Chk" & GroupId.ToString & intRow.ToString() '"chk" & intPrintRows.ToString() '& intRow.ToString() ' + intCols.ToString()
                                chkCheck.Name = "Chk" & GroupId.ToString & intRow.ToString()
                                chkCheck.Value = nmvAnswers.Item(intHeaderRows).Split(",").GetValue(intAnswer).ToString
                                If lstrAnswer = "1" Then
                                    chkCheck.Checked = True
                                End If
                                Headercell_8.Controls.Add(chkCheck)
                                If intCols = GetGroupResultCnt(GroupId) + 3 Then
                                    chkCheck.Checked = True 'Settings of No Answer Column
                                    Headercell_8.Style("Width") = "5px"
                                    Headercell_8.Style("display") = "none"
                                    Headercell_8.Visible = False
                                End If
                                If intCols = 3 Then
                                    'chkCheck.Checked = True
                                End If
                            Else
                                ' Dim txtArea As New HtmlTextArea
                                Dim txtArea As New TextBox
                                txtArea.TextMode = TextBoxMode.MultiLine
                                txtArea.CssClass = "multipleText"
                                txtArea.Height = "50"
                                txtArea.Width = "550"
                                '  txtArea.Rows = 2
                                'txtArea.Cols = 70
                                ''txtArea.Style.Value = "border-right: black 1px ridge;border-top: black 1px ridge; border-left: black 1px ridge; border-bottom: black 1px ridge;Height:50px"
                                ''Headercell_8.Style("text-align") = "left"
                                txtArea.ID = "txt" & GroupId.ToString & intRow.ToString() ')' intPrintRows.ToString() 'intRow.ToString() + intCols.ToString()
                                '' txtArea.Value = "asdas"


                                Headercell_8.ColumnSpan = GetGroupResultCnt(GroupId)
                                Headercell_8.Controls.Add(txtArea)
                                'Dim idwater = "TextBoxWatermark" + txtArea.ID.ToString
                                'Dim strwatermark As String = "<ajaxToolkit:TextBoxWatermarkExtender ID='" & idwater & "' runat='server'                    TargetControlID='" & txtArea.ID.ToString & "' WatermarkText='Free comment if required' WatermarkCssClass='watermarked'></ajaxToolkit:TextBoxWatermarkExtender>"
                                Dim s As New AjaxControlToolkit.TextBoxWatermarkExtender
                                s.ID = "TW" + txtArea.ID.ToString
                                s.TargetControlID = txtArea.ID
                                s.WatermarkText = "Additional comments and / or suggestions."
                                s.WatermarkCssClass = "watermarked"
                                Headercell_8.Controls.Add(s)

                                Headerrow_3.Cells.Add(Headercell_8)
                                Exit For

                            End If
                            intAnswer = intAnswer + 1
                        End If '

                        Headerrow_3.Cells.Add(Headercell_8)
                    Next

                    If SurveyQuest("ANSTYPE") = "S" Then
                        intPrintRows = intPrintRows + 1
                        nmvSurvRadioGrps.Add(intPrintRows, GroupId)
                        nmvSurvRadioGrps.Add(intPrintRows, "R")
                        nmvSurvRadioGrps.Add(intPrintRows, "Rad" & GroupId.ToString & intRow.ToString())
                        nmvSurvRadioGrps.Add(intPrintRows, SurveyQuest("QUEST_ID").ToString())
                        nmvSurvRadioGrps.Add(intPrintRows, SurveyQuest("QUESTDESC").ToString().Replace(",", " "))

                    ElseIf SurveyQuest("ANSTYPE") = "M" Then

                        intPrintRows = intPrintRows + 1
                        nmvSurvRadioGrps.Add(intPrintRows, GroupId)
                        nmvSurvRadioGrps.Add(intPrintRows, "C")
                        nmvSurvRadioGrps.Add(intPrintRows, "Chk" & GroupId.ToString & intRow.ToString())
                        nmvSurvRadioGrps.Add(intPrintRows, SurveyQuest("QUEST_ID").ToString())
                        nmvSurvRadioGrps.Add(intPrintRows, SurveyQuest("QUESTDESC").ToString().Replace(",", " "))
                    Else
                        intPrintRows = intPrintRows + 1
                        nmvSurvRadioGrps.Add(intPrintRows, GroupId)
                        nmvSurvRadioGrps.Add(intPrintRows, "T")
                        nmvSurvRadioGrps.Add(intPrintRows, "txt" & GroupId.ToString & intRow.ToString())
                        nmvSurvRadioGrps.Add(intPrintRows, SurveyQuest("QUEST_ID").ToString())
                        nmvSurvRadioGrps.Add(intPrintRows, SurveyQuest("QUESTDESC").ToString().Replace(",", " "))
                    End If

                    tblReport.Rows.Add(Headerrow_3)
                    Dim Strcomments As String = ""
                    'If Survey have Comments then Add a  text area column under the Row.
                    If SurveyQuest("COMMENTS") = "True" Then
                        Strcomments = CreateCommentsRow(tblReport, intRow, GroupId)
                        nmvSurvRadioGrps.Add(intPrintRows, Strcomments)
                    End If
                End While
            End If

        End Using
        intHeaderRows = intHeaderRows + 1
        Session("RadioGroups") = nmvSurvRadioGrps
        Session("CheckGroups") = nmvSurvChkGrps

    End Function

    Private Function CreateCommentsRow(ByVal tblReport As Table, ByVal IntRow As Integer, ByVal GroupId As String) As String
        Dim Commentrow As New TableRow
        Dim txtName As String = ""

        For intCols As Integer = 1 To 3
            Dim CommentRowCell As New TableCell
            CommentRowCell.CssClass = "remark_info"
            CommentRowCell.Style("text-align") = "left"
            CommentRowCell.Wrap = False
            ''CommentRowCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#BFEDFB")
            ''CommentRowCell.BorderColor = Drawing.Color.Black
            ''CommentRowCell.BorderStyle = BorderStyle.Solid
            ''CommentRowCell.BorderWidth = "1"
            If intCols = 2 Then
                CommentRowCell.ColumnSpan = "1"
                CommentRowCell.Style("padding-left") = "7px"
                CommentRowCell.Text = "Any comments you wish to make-"
                CommentRowCell.Wrap = False
                ''CommentRowCell.Style("font-family") = "calibri,verdana,corisande,timesroman,garamond"
            ElseIf intCols = 3 Then
                Dim txtArea As New HtmlTextArea
                txtArea.Rows = 2
                txtArea.Cols = 70
                txtArea.ID = "txtC" & GroupId.ToString & IntRow.ToString() '"txt" & IntRow.ToString() + intCols.ToString()
                ' txtArea.Style.Value = "border-right: black 1px ridge;border-top: black 1px ridge; border-left: black 1px ridge; border-bottom: black 1px ridge"
                txtArea.Style.Value = "width:550px !important; height:50px !important;color:#666; border:1px solid #ccc;font-family:Verdana; font-size:11px;"

                CommentRowCell.ColumnSpan = "4"
                txtName = txtArea.ID
                CommentRowCell.Controls.Add(txtArea)


            End If
            Commentrow.Cells.Add(CommentRowCell)
        Next
        tblReport.Rows.Add(Commentrow)
        Return txtName
    End Function

    Private Function CreateButtonRow(ByVal tblSubmit As Table)
        Dim connection As SqlConnection = ConnectionManger.GetOASISSurveyConnection
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EmployeeId"))
        pParms(1) = New SqlClient.SqlParameter("@PD_CM_ID", ViewState("PD_CM_ID"))
        pParms(2) = New SqlClient.SqlParameter("@QUEST_ID", "COMMENTS")
        Dim lstrCOMM1 = ""
        Dim lstrCOMM2 = ""
        Dim lstrCOMM3 = ""
        Dim lstrParti = "0"
        Dim schoollevel As String = ""


        Using Ansreader As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "GET_PD_ANSWERS", pParms)
            While Ansreader.Read
                lstrCOMM1 = Convert.ToString(Ansreader("COMM1"))
                lstrCOMM2 = Convert.ToString(Ansreader("COMM2"))
                lstrCOMM3 = Convert.ToString(Ansreader("COMM3"))
                lstrParti = Convert.ToString(Ansreader("Participated"))
                ''includee by nahyan on 16nov2016 
                schoollevel = Convert.ToString(Ansreader("EMP_SCHOOL_LEVEL"))
                Dim schoollevelArray As String() = schoollevel.Split(","c)

                For Each slvalue As String In schoollevelArray
                    If (slvalue = "EC") Then
                        chkLEVELEC.Checked = True
                    ElseIf (slvalue = "P") Then
                        chkLEVELP.Checked = True
                    ElseIf (slvalue = "S") Then
                        chkLEVELS.Checked = True
                    ElseIf (slvalue = "ALL") Then
                        chkLEVELALL.Checked = True

                    End If

                Next

                ''nahyan ends
            End While
        End Using

        'Building Cells of Each Row
        Dim BottomCell As New TableCell
        Dim BottomRow As New TableRow
        BottomCell.ID = "A1"
        BottomCell.Style("font-weight") = "bold"
        BottomCell.Style("Width") = "100%"
        BottomCell.ColumnSpan = "0"
        BottomCell.Wrap = False
        ' ''BottomCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#BFEDFB")
        BottomCell.BorderWidth = "0"
        BottomCell.Style("text-align") = "center"
        Dim KTAAEXIST As Integer = GETKTAA_STATUS(Convert.ToUInt32(ViewState("CR_ID")))
        If ViewState("SurveyId") = "185" And lstrParti <> "1" Then
            ''commented and added by nahyan 21nov2016 
            ''BottomCell.Text = "<input id='Submit1' type='button' runat='Server' value='Submit' style='width: 149px; height: 39px ;border:1px solid #ccc; font-weight:bold; font-size:11px; color:#000;' onclick='return btn_submit()' /> "
            btnSaveEval.Visible = True
            btnDraftEval.Visible = True
        ElseIf (KTAAEXIST <= 0) Then
            HF_KTAACHECK.Value = 1
            ''commented and added by nahyan 21nov2016 
            ' BottomCell.Text = "<input id='Submit1' type='button' runat='Server' value='Submit' style='width: 149px; height: 39px ;border:1px solid #ccc; font-weight:bold; font-size:11px; color:#000;' onclick='return btn_submit()' /> "
            btnSaveEval.Visible = True
            btnDraftEval.Visible = True

        End If
        ''included by nahyan to display submitbutton if ketaa not exist



        BottomRow.Cells.Add(BottomCell)




        tblSubmit.Rows.Add(BottomRow)
    End Function

    Private Function CreateBottomRow(ByVal tblReport As Table)
        Dim connection As SqlConnection = ConnectionManger.GetOASISSurveyConnection
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EmployeeId"))
        pParms(1) = New SqlClient.SqlParameter("@PD_CM_ID", ViewState("PD_CM_ID"))
        pParms(2) = New SqlClient.SqlParameter("@QUEST_ID", "COMMENTS")
        Dim lstrCOMM1 = ""
        Dim lstrCOMM2 = ""
        Dim lstrCOMM3 = ""
        Dim lstrParti = "0"

        Using Ansreader As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "GET_PD_ANSWERS", pParms)
            While Ansreader.Read
                lstrCOMM1 = Convert.ToString(Ansreader("COMM1"))
                lstrCOMM2 = Convert.ToString(Ansreader("COMM2"))
                lstrCOMM3 = Convert.ToString(Ansreader("COMM3"))
                lstrParti = Convert.ToString(Ansreader("Participated"))
            End While
        End Using



        For intBtRow As Integer = 1 To 3
            Dim BottomRow As New TableRow

            Dim BottomRow3 As New TableRow


            Dim BottomRow2 As New TableRow

            Dim BottomRow3_1 As New TableRow
            Dim BottomRow2_1 As New TableRow

            Dim BottomRow3_2 As New TableRow
            Dim BottomRow2_2 As New TableRow


            'Building Cells of Each Row
            Dim BottomCell As New TableCell

            Dim BottomCell2 As New TableCell
            BottomCell2.ID = "genComm"
            BottomCell2.Style("text-align") = "left"
            BottomCell2.Style("font-weight") = "bold"
            BottomCell2.Style("Width") = "100%"
            BottomCell2.ColumnSpan = "0"
            BottomCell2.Wrap = False
            BottomCell2.BorderWidth = "0"
            BottomCell.ID = "A1" & intBtRow
            BottomCell.Style("text-align") = "center"


            Dim BottomCell3 As New TableCell
            BottomCell3.ID = "genComm"
            BottomCell3.Style("text-align") = "left"
            BottomCell3.Style("font-weight") = "bold"
            BottomCell3.Style("Width") = "100%"
            BottomCell3.ColumnSpan = "0"
            BottomCell3.Wrap = False
            BottomCell3.BorderWidth = "0"


            Dim BottomCell2_1 As New TableCell
            BottomCell2_1.ID = "genComm"
            BottomCell2_1.Style("text-align") = "left"
            BottomCell2_1.Style("font-weight") = "bold"
            BottomCell2_1.Style("Width") = "100%"
            BottomCell2_1.ColumnSpan = "0"
            BottomCell2_1.Wrap = False
            BottomCell2_1.BorderWidth = "0"
            BottomCell2_1.ID = "A1" & intBtRow
            BottomCell2_1.Style("text-align") = "left"


            Dim BottomCell2_2 As New TableCell
            BottomCell2_2.ID = "genComm"
            BottomCell2_2.Style("text-align") = "left"
            BottomCell2_2.Style("font-weight") = "bold"
            BottomCell2_2.Style("Width") = "100%"
            BottomCell2_2.ColumnSpan = "0"
            BottomCell2_2.Wrap = False
            BottomCell2_2.BorderWidth = "0"
            BottomCell2_2.ID = "A1" & intBtRow
            BottomCell2_2.Style("text-align") = "left"


            Dim BottomCell3_1 As New TableCell
            BottomCell3_1.ID = "genComm"
            BottomCell3_1.Style("text-align") = "left"
            BottomCell3_1.Style("font-weight") = "bold"
            BottomCell3_1.Style("Width") = "100%"
            BottomCell3_1.ColumnSpan = "0"
            BottomCell3_1.Wrap = False
            BottomCell3_1.BorderWidth = "0"


            Dim BottomCell3_2 As New TableCell
            BottomCell3_2.ID = "genComm"
            BottomCell3_2.Style("text-align") = "left"
            BottomCell3_2.Style("font-weight") = "bold"
            BottomCell3_2.Style("Width") = "100%"
            BottomCell3_2.ColumnSpan = "0"
            BottomCell3_2.Wrap = False
            BottomCell3_2.BorderWidth = "0"



            BottomCell.Style("font-weight") = "bold"
            BottomCell.Style("Width") = "100%"
            BottomCell.ColumnSpan = "0"
            BottomCell.Wrap = False
            ' ''BottomCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#BFEDFB")
            BottomCell.BorderWidth = "0"
            If intBtRow = 5 Then
                ' BottomCell.Style("text-align") = "Left"
                BottomCell.Style("Height") = "10px"
                BottomCell.Style("Width") = "100%"
                BottomCell.ColumnSpan = "8"
                ' BottomCell.Text = "Would you like GEMS to contact you -   Yes<input id='contactyes' name='yesno' value='1' checked='checked' type='radio' />      No<input id='contactno' name='yesno'value='0' type='radio' /><br/>"
                ''BottomCell.Text = "<hr style='font-size: 1px; color: black; height: 1px'/>sss"
            ElseIf intBtRow = 5 Then
                BottomCell.Style("font-weight") = "bold"
                BottomCell.Style("font-size") = "10pt"
                BottomCell.Style("Height") = "40px"
                BottomCell.ForeColor = Drawing.Color.Red
                BottomCell.Text = " "
            ElseIf intBtRow = 3 Then ' 
                'BottomCell.Style("Height") = "10px"
                'BottomCell.Style("font-size") = "10pt"
                'BottomCell.Style("Width") = "100%"
                'BottomCell.Style("font-weight") = "bold"
                '  BottomCell.ColumnSpan = "8"



                '    If Not Request.QueryString("Ispreview") Is Nothing Then
                If ViewState("SurveyId") <> "178" Then
                    ''commented by nahyan and added below on 15nov2016 merging eval and ktaa
                    'BottomCell2.Text = "<div></div><br><div style='align:left;font-family: verdana,Arial, Helvetica, sans-serif;		font-size:13px;color:#fff;p	'>WWW � What Went Well <br>What elements of the PD Workshop that you thought went well, i.e. what you enjoyed / found most useful?</div>"
                    'BottomCell3.Text = "<div><textArea id='txtCOMM1' runat='server' style='overflow:auto;width:950px ; height:50px !important;color:#666; border:1px solid #ccc;font-family:Verdana; font-size:11px;' rows=5 cols=100> " + lstrCOMM1 + "</textArea> <br><br><br> </div>"

                    'BottomCell2_1.Text = "<div></div><br><div style='align:left;font-family: verdana,Arial, Helvetica, sans-serif;		font-size:13px;color:#fff;p	'>EBI � Even Better If <br>What elements of the PD Workshop do you think need improvement? <br> Is there something you would have liked to have seen or have been told about?</div>"
                    'BottomCell3_1.Text = "<div><textArea id='txtCOMM2' runat='server' style='overflow:auto;width:950px ; height:50px !important;color:#666; border:1px solid #ccc;font-family:Verdana; font-size:11px;' rows=5 cols=100>" + lstrCOMM2 + " </textArea> <br><br><br> </div>"

                    'BottomCell2_2.Text = "<div></div><br><div style='align:left;font-family: verdana,Arial, Helvetica, sans-serif;		font-size:13px;color:#fff;p	'>Any other comments?</div>"
                    'BottomCell3_2.Text = "<div><textArea id='txtCOMM3' runat='server' style='overflow:auto;width:950px ; height:50px !important;color:#666; border:1px solid #ccc;font-family:Verdana; font-size:11px;' rows=5 cols=100> " + lstrCOMM3 + "</textArea> <br><br><br> </div>"

                    BottomCell2_2.Text = "<div></div><br><div style='align:left;font-family: verdana,Arial, Helvetica, sans-serif;		font-size:13px;color:#fff;p	'>WWW � What Went Well <br>What elements of the PD Workshop that you thought went well, i.e. what you enjoyed / found most useful?</div>"
                    BottomCell3_2.Text = "<div><textArea id='txtCOMM1' runat='server' style='overflow:auto;width:950px ; height:50px !important;color:#666; border:1px solid #ccc;font-family:Verdana; font-size:11px;' rows=5 cols=100> " + lstrCOMM1 + "</textArea> <br><br><br> </div>"

                    BottomCell2.Text = "<div></div><br><div style='align:left;font-family: verdana,Arial, Helvetica, sans-serif;		font-size:13px;color:#fff;p	'>EBI � Even Better If <br>What elements of the PD Workshop do you think need improvement? <br> Is there something you would have liked to have seen or have been told about?</div>"
                    BottomCell3.Text = "<div><textArea id='txtCOMM2' runat='server' style='overflow:auto;width:950px ; height:50px !important;color:#666; border:1px solid #ccc;font-family:Verdana; font-size:11px;' rows=5 cols=100>" + lstrCOMM2 + " </textArea> <br><br><br> </div>"

                    BottomCell2_1.Text = "<div></div><br><div style='align:left;font-family: verdana,Arial, Helvetica, sans-serif;		font-size:13px;color:#fff;p	'>Any other comments?</div>"
                    BottomCell3_1.Text = "<div><textArea id='txtCOMM3' runat='server' style='overflow:auto;width:950px ; height:50px !important;color:#666; border:1px solid #ccc;font-family:Verdana; font-size:11px;' rows=5 cols=100> " + lstrCOMM3 + "</textArea> <br><br><br> </div>"

                    ' ''commented by nahyan 13thOCt2016
                    'If ViewState("SurveyId") = "185" And lstrParti <> "1" Then
                    '    BottomCell.Text = "<input id='Submit1' type='button' runat='Server' value='Submit' style='width: 149px; height: 39px ;border:1px solid #ccc; font-weight:bold; font-size:11px; color:#000;' onclick='return btn_submit()' /> "
                    '    'Else
                    '    '    BottomCell.Text = "<input id='Submit1' type='button' runat='Server' value='Save & Go To Leadership Survey' style='width: 189px; height: 39px ;border:1px solid #ccc; font-weight:bold; font-size:11px; color:#000;' onclick='return btn_submit()' /> "
                    'End If

                End If
                'BottomCell.Text = "<div><input id='Submit1' type='button' runat='Server' value='Submit' disabled='disabled' style='width: 169px; height: 39px;border:1px solid #000; font-weight:bold; font-size:11px; color:#000;' onclick='return btn_submit()' /> </div>"

                'Else
                '    If ViewState("SurveyId") <> "178" Then
                '        BottomCell2.Text = "<div></div><br><div style='align:left;font-family: verdana,Arial, Helvetica, sans-serif;		font-size:13px;color:#fff;p	'>General Comments</div>"
                '        BottomCell3.Text = "<div><textArea id='txtGenComments' runat='server' style='overflow:auto;width:950px ; height:50px !important;color:#666; border:1px solid #ccc;font-family:Verdana; font-size:11px;' rows=5 cols=100> </textArea> <br><br><br> </div>"
                '    End If
                '    If ViewState("SurveyId") <> "181" Then
                '        BottomCell.Text = "<input id='Submit1' type='button' runat='Server' value='Submit' style='width: 149px; height: 39px ;border:1px solid #ccc; font-weight:bold; font-size:11px; color:#000;' onclick='return btn_submit()' /> "
                '    Else
                '        BottomCell.Text = "<input id='Submit1' type='button' runat='Server' value='Save & Go To Leadership Survey' style='width: 189px; height: 39px ;border:1px solid #ccc; font-weight:bold; font-size:11px; color:#000;' onclick='return btn_submit()' /> "
                '    End If
                '    'BottomCell.Text = "<input id='Submit1' type='button' runat='Server' value='Submit' style='width: 149px; height: 39px ;border:1px solid #ccc; font-weight:bold; font-size:11px; color:#000;' onclick='return btn_submit()' /> "
                'End If
            ElseIf intBtRow = 5 Then
                'BottomCell.Style("font-weight") = "bold"
                'BottomCell.Style("font-size") = "10pt"
                ' BottomCell.Style("Height") = "40px"
                BottomCell.ForeColor = Drawing.Color.Red
                BottomCell.Text = " " '....... Thank you For Participating This Survey.............
            Else
                ' BottomCell.Style("Height") = "20px"
                ' BottomCell.Text = "<Img Src='..\Images/GEMS_A4_BLUE_EDUCATION_CMYK.jpg' height='500' width='750'>"
            End If
            BottomCell3.ColumnSpan = GetMaxColumns(ViewState("SurveyId")) '"10"
            BottomCell2.ColumnSpan = GetMaxColumns(ViewState("SurveyId")) '"10"

            BottomCell3_1.ColumnSpan = GetMaxColumns(ViewState("SurveyId")) '"10"
            BottomCell2_1.ColumnSpan = GetMaxColumns(ViewState("SurveyId")) '"10"

            BottomCell3_2.ColumnSpan = GetMaxColumns(ViewState("SurveyId")) '"10"
            BottomCell2_2.ColumnSpan = GetMaxColumns(ViewState("SurveyId")) '"10"


            BottomCell.ColumnSpan = GetMaxColumns(ViewState("SurveyId")) '"10"
            BottomRow3.Cells.Add(BottomCell3)
            BottomRow2.Cells.Add(BottomCell2)

            BottomRow3_1.Cells.Add(BottomCell3_1)
            BottomRow2_1.Cells.Add(BottomCell2_1)

            BottomRow3_2.Cells.Add(BottomCell3_2)
            BottomRow2_2.Cells.Add(BottomCell2_2)


            BottomRow.Cells.Add(BottomCell)

            tblReport.Rows.Add(BottomRow2_2)
            tblReport.Rows.Add(BottomRow3_2)


            tblReport.Rows.Add(BottomRow2)
            tblReport.Rows.Add(BottomRow3)

            tblReport.Rows.Add(BottomRow2_1)
            tblReport.Rows.Add(BottomRow3_1)




            tblReport.Rows.Add(BottomRow)
        Next
        Dim BottomRowimg As New TableRow
        Dim BottomCellimg As New TableCell
        BottomCellimg.ID = "A6"
        BottomCellimg.Style("text-align") = "right"
        BottomCellimg.Style("Width") = "100%"
        BottomCellimg.ColumnSpan = "0"
        BottomCellimg.Wrap = False
        ' ''BottomCellimg.BackColor = System.Drawing.ColorTranslator.FromHtml("#BFEDFB")
        BottomCellimg.BorderWidth = "0"
        ' BottomCellimg.Style("Height") = "20px"
        ''BottomCellimg.Text = "<Img Src='..\Images/SOOSH.jpg' height='300' width='350'>"
        BottomCellimg.ColumnSpan = 60
        BottomRowimg.Cells.Add(BottomCellimg)
        tblReport.Rows.Add(BottomRowimg)
    End Function

    Private Shared Function GetSurveyAnswers(ByVal SurvId As String, ByVal IntGrpId As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISSurveyConnection
        Dim strSQL As String = ""
        strSQL = "SELECT A.SURVEY_ID,A.GROUP_ID,A.QSTORDER, B.ANS_ID,B.ANS_DESC  FROM SRV.COM_SUR_QSTORDER A, SRV.COM_SUR_ANSWER B " & _
        "WHERE  A.ANS_ID=B.ANS_ID AND A.GROUP_ID= " & IntGrpId & " ORDER BY QSTORDER "

        Dim command As SqlCommand = New SqlCommand(strSQL, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function
    Private Shared Function GetSurveyQuestion_common(ByVal SurvId As String, ByVal GroupId As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISSurveyConnection
        Dim strSQL As String = "SELECT QUEST_ID,SURVEYID,QUESTDESC,ANSTYPE,GROUPID,COMMENTS " & _
              " FROM SRV.COM_SUR_QUESTION " & _
              " WHERE SURVEYID='1' AND GROUPID='1' ORDER BY QUEST_ID"

        Dim command As SqlCommand = New SqlCommand(strSQL, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function
    Private Shared Function GetSurveyQuestions(ByVal SurvId As String, ByVal GroupId As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISSurveyConnection
        Dim strSQL As String = "SELECT QUEST_ID,SURVEYID,QUESTDESC,ANSTYPE,GROUPID,COMMENTS " & _
              " FROM SRV.COM_SUR_QUESTION " & _
              " WHERE SURVEYID=" & SurvId & " AND GROUPID=" & GroupId & " ORDER BY QUEST_ID"

        Dim command As SqlCommand = New SqlCommand(strSQL, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function

    Private Shared Function GetMaxColumns(ByVal SurvId As String) As Integer
        Dim intReturn As Integer = 0
        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = "select max(col) from ( " & _
                                "SELECT COUNT(B.ANS_ID) AS COL,A.GROUP_ID " & _
                                "FROM SRV.COM_SUR_QSTORDER A, SRV.COM_SUR_ANSWER B " & _
                                "WHERE A.ANS_ID = B.ANS_ID And SURVEY_ID =" & SurvId & " " & _
                                "GROUP BY A.GROUP_ID ) A"

        Dim DsHelper As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If DsHelper.Tables(0).Rows.Count >= 0 Then
            intReturn = DsHelper.Tables(0).Rows(0).Item(0)
            intReturn = intReturn + 1
        Else
            intReturn = 0
        End If

        Return intReturn

    End Function

    Private Shared Function GetSurveyInfo(ByVal SurvId As String) As DataSet
        Dim connection As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT SURVEY_ID,SURVEYDATE,SURVTITLE,SURVDESC,TARGETED,INITIATORID,EXPIRYDATE,STATUS FROM SRV.COM_SUR_DETAILS WHERE SURVEY_ID= " & SurvId
        Dim dsSurveyInfo As DataSet
        dsSurveyInfo = SqlHelper.ExecuteDataset(connection, CommandType.Text, strSQL)
        Return dsSurveyInfo

    End Function

    Private Shared Function GetSurveyGroup(ByVal SurvId As String) As DataSet
        Dim connection As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT GRP_ID AS GROUPID,GRPNAME FROM  SRV.COM_SUR_GROUP WHERE SURVEYID= " & SurvId & " ORDER BY GROUPID "
        Dim dsSurveyInfo As DataSet
        dsSurveyInfo = SqlHelper.ExecuteDataset(connection, CommandType.Text, strSQL)
        Return dsSurveyInfo

    End Function

    Private Function GetGroupResultCnt(ByVal Grpid As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT COUNT(*) FROM SRV.COM_SUR_QSTORDER WHERE GROUP_ID= " & Grpid
        Dim DsHelper As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

        If DsHelper.Tables(0).Rows.Count >= 0 Then
            Return DsHelper.Tables(0).Rows(0).Item(0)
        Else
            Return 0
        End If

    End Function

    Private Shared Function GetAnswerCount(ByVal SurvId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT COUNT(*) FROM SRV.COM_SUR_ANSWER "
        Dim DsHelper As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

        If DsHelper.Tables(0).Rows.Count >= 0 Then
            Return DsHelper.Tables(0).Rows(0).Item(0)
        Else
            Return 0
        End If

    End Function

    Private Shared Function GetImagepath(ByVal SurvId As String, ByVal ImageMode As String) As String
        Dim connection As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        Dim strPath As String = ""
        strSQL = " SELECT HIMAGEPATH,FIMAGEPATH FROM SRV.COM_SUR_DETAILS WHERE SURVEY_ID=" & SurvId

        Dim dsSurveyInfo As DataSet
        dsSurveyInfo = SqlHelper.ExecuteDataset(connection, CommandType.Text, strSQL)
        If dsSurveyInfo.Tables(0).Rows.Count >= 1 Then
            If ImageMode = "H" Then 'HeDER iMAGE
                strPath = dsSurveyInfo.Tables(0).Rows(0).Item(0)
            ElseIf ImageMode = "F" Then ' Foother Image
                strPath = dsSurveyInfo.Tables(0).Rows(0).Item(1)
            End If

        End If
        Return strPath

    End Function

    Private Function ProcessResult()

        Dim nvcAnswers As New NameValueCollection
        Dim nvcQuestions As New NameValueCollection
        Dim StrAnsType As String = ""

        For Each grpRow As DataRow In GetSurveyGroup(ViewState("SurveyId")).Tables(0).Rows ' Survey Group

            Using SqlAnswers As SqlDataReader = GetSurveyAnswers("", grpRow.Item("GROUPID").ToString())
                If SqlAnswers.HasRows = True Then
                    While SqlAnswers.Read ' Group ResultSet
                        nvcAnswers.Add(SqlAnswers("ANS_DESC").ToString(), "")
                    End While
                End If
            End Using

            '----------------------------------------------------------------------------------
            Using SurveyQuest As SqlDataReader = GetSurveyQuestions(ViewState("SurveyId"), grpRow.Item("GROUPID").ToString())
                If SurveyQuest.HasRows = True Then
                    While SurveyQuest.Read

                        If SurveyQuest("ANSTYPE") = "S" Then ' Single Option 

                        ElseIf SurveyQuest("ANSTYPE") = "M" Then ' Multipple Option

                        Else ' Comments Only

                        End If




                        nvcQuestions.Add(SurveyQuest("QUEST_ID").ToString(), "Test1")

                    End While
                End If

            End Using
            '-------------------------------------------------------------------------------------

        Next

    End Function

    Private Function GetSurveyAnswerList() As NameValueCollection

        Dim nvcAnswerList As New NameValueCollection
        Dim connection As SqlConnection = ConnectionManger.GetOASISSurveyConnection
        Dim strSQL As String = ""
        strSQL = "SELECT ANS_ID,ANS_DESC FROM SRV.COM_SUR_ANSWER ORDER BY ANS_ID "

        Dim command As SqlCommand = New SqlCommand(strSQL, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        While reader.Read
            nvcAnswerList.Add(reader.Item("ANS_DESC").ToString, reader.Item("ANS_ID").ToString)
        End While
        SqlConnection.ClearPool(connection)
        Return nvcAnswerList

    End Function

    Private Function UpdateSurveyResponse(ByVal SurveyId As Integer)

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISSurveyConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim str_query As String = "exec SRV.updateSURVEYRESPONSE " & SurveyId

                HiddenField1.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                transaction.Commit()
            Catch myex As ArgumentException
                transaction.Rollback()
                Response.Write(myex.Message)
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Function

    Private Function UpdateSurveyREAD(ByVal SurveyId As Integer, ByVal strUser As String, ByVal strBSUNIT As String)

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISSurveyConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim SENDING_ID As String = ""

                If Not Request.QueryString("SendingId") Is Nothing Then
                    SENDING_ID = Encr_decrData.Decrypt(Request.QueryString("SendingId").Replace(" ", "+"))
                End If

                Dim str_query As String = "exec SRV.updateSURVEYREAD " & SurveyId & ",'" & strUser & "','" & strBSUNIT & "','" & SENDING_ID & "'"

                HiddenField1.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                transaction.Commit()
            Catch myex As ArgumentException
                transaction.Rollback()
                Response.Write(myex.Message)
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Function

    Private Function getBusinessUnitLogo(ByVal BSUID As String) As String

        Dim connStr As String = ConnectionManger.GetOASISConnectionString
        Try
            Dim str_query As String = "SELECT BSU_LOGO  FROM BUSINESSUNIT_M WHERE BSU_ID= '" & BSUID & "'"
            Return SqlHelper.ExecuteScalar(connStr, CommandType.Text, str_query)
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Function getStudentBusinessUnit(ByVal StudentId As String) As String
        '-- For a Aprticular case ... There is no records in Database So that I am getting the First 6 Chr of the Student Number.
        Dim connStr As String = ConnectionManger.GetOASISConnectionString
        Try
            Dim str_query As String = "SELECT STU_BSU_ID  FROM STUDENT_M WHERE STU_ID= " & StudentId
            Return SqlHelper.ExecuteScalar(connStr, CommandType.Text, str_query)
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Function GetBusinessUnit(ByVal StudentNo As String)
        Try
            If StudentNo.Trim.Length >= 13 Then
                StudentNo = StudentNo.Substring(0, 6)
            End If
            Return StudentNo
        Catch ex As Exception

        End Try
    End Function

    Private Function getStudentName(ByVal StudentId As String) As String
        '-- For a Aprticular case ... There is no records in Database So that I am passing the same Student Number as Student NAme
        'Dim connection As String = ConnectionManger.GetOASISConnectionString
        Try
            'Dim str_query As String = "SELECT STU_FIRSTNAME  FROM STUDENT_M WHERE STU_ID= " & StudentId
            'Dim dsStudentInfo As DataSet
            'dsStudentInfo = SqlHelper.ExecuteDataset(connection, CommandType.Text, str_query)
            'If dsStudentInfo.Tables(0).Rows.Count >= 1 Then
            '    Return dsStudentInfo.Tables(0).Rows(0).Item(0).ToString
            'End If
            'Return ""
            Return StudentId

        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Shared Function GetIsExist(ByVal SurvId As String, ByVal UserId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT COUNT(*) FROM SRV.COM_SUR_RESULTMST WHERE SURVEYID=" & SurvId & " AND USER_NAME='" & UserId & "'"
        Dim DsHelper As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

        If DsHelper.Tables(0).Rows.Count >= 0 Then
            Return DsHelper.Tables(0).Rows(0).Item(0)
        Else
            Return 0
        End If
    End Function

    Private Shared Function GetIsSurveyExpired(ByVal SurvId As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        strSQL = " SELECT SURVTITLE,EXPIRYDATE FROM SRV.COM_SUR_DETAILS WHERE SURVEY_ID=" & SurvId
        Dim DsHelper As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

        If DsHelper.Tables(0).Rows.Count >= 0 Then
            If (DateDiff(DateInterval.Day, Date.Today, DsHelper.Tables(0).Rows(0).Item(1))) < 0 Then
                Return True
            Else
                Return False
            End If
        End If
    End Function

    ''included below by nahyan for mergin ktaa and eval on 15Nov2016

    Private Function GETKTAA_STATUS(ByVal CRID As Integer) As Integer
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim ktaacount As Integer = 0
            param(0) = New SqlClient.SqlParameter("@CR_ID", CRID)
            ktaacount = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "PD_M.GETKTAA_STATUS", param)
            Return ktaacount

        Catch ex As Exception

        End Try
    End Function
    Sub Bind_KTAA_ACTION_DETAILS(ByVal KTAA_ID As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@KTAA_ID", KTAA_ID)
            'param(1) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_KTAA_ACTIONS_BY_KTAID", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)
                BindKTAAGrid(ds)
                'gvCourseActions.DataSource = ds
                'gvCourseActions.DataBind()

                Session("COURSEACTION") = Dt
            End If

        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub
    Private Sub BindKTAAGrid(ByVal ds As DataSet)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim tmpRow As DataRow
            ds.Tables(0).Columns.Add("SNo")
            For i As Integer = 1 To 5
                tmpRow = ds.Tables(0).NewRow()
                tmpRow.Item("PD_KTA_ACTION_STEP") = ""
                tmpRow.Item("PD_KTA_SUCCESS") = ""
                tmpRow.Item("PD_KTA_TIMELINE") = ""
                tmpRow.Item("PD_KTA_RESOURCES") = ""
                tmpRow.Item("PD_KTA_RELEVANCE") = ""
                tmpRow.Item("PD_KTA_RELEVANCETEXT") = ""
                tmpRow.Item("PD_KTA_RELEVANCETEXT") = ""
                'tmpRow.Item("UniqueId") = i
                tmpRow.Item("SNo") = i
                ds.Tables(0).Rows.Add(tmpRow)
            Next
            ds.AcceptChanges()
            Me.gvActions.DataSource = ds.Tables(0)
            Me.gvActions.DataBind()
        Else
            Dim dv As New DataView(ds.Tables(0), Nothing, "UniqueId Desc", DataViewRowState.CurrentRows)
            ds.Tables.RemoveAt(0)
            ds.Tables.Add(dv.ToTable)
            ds.AcceptChanges()
            ds.Tables(0).Columns.Add("SNo")
            If ds.Tables(0).Rows.Count < 5 Then
                Dim tmpRow As DataRow
                For i As Integer = ds.Tables(0).Rows.Count To 4
                    tmpRow = ds.Tables(0).NewRow()
                    tmpRow.Item("PD_KTA_ACTION_STEP") = ""
                    tmpRow.Item("PD_KTA_SUCCESS") = ""
                    tmpRow.Item("PD_KTA_TIMELINE") = ""
                    tmpRow.Item("PD_KTA_RESOURCES") = ""
                    tmpRow.Item("PD_KTA_RELEVANCE") = ""
                    tmpRow.Item("PD_KTA_RELEVANCETEXT") = ""
                    ds.Tables(0).Rows.Add(tmpRow)
                Next
                ds.AcceptChanges()
            End If
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                ds.Tables(0).Rows(i).Item("SNo") = i + 1
            Next
            ds.AcceptChanges()
            Me.gvActions.DataSource = ds.Tables(0)
            Me.gvActions.DataBind()
        End If



    End Sub

    Protected Sub gvActions_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvActions.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cbl As CheckBoxList = CType(e.Row.FindControl("cblRelevance"), CheckBoxList)
            Dim lbl As Label = CType(e.Row.FindControl("lblRelevanceId"), Label)
            Dim lblSNo As Label = CType(e.Row.FindControl("lblSNo"), Label)
            Dim lblMandatory As Label = CType(e.Row.FindControl("lblMandatory"), Label)
            If Not lblSNo Is Nothing AndAlso CInt(lblSNo.Text) = 1 Then
                lblMandatory.Visible = True
            End If
            If Not cbl Is Nothing AndAlso Not lbl Is Nothing Then
                If Not Session("PD_Actions_Relevance") Is Nothing Then
                    With CType(Session("PD_Actions_Relevance"), DataSet)
                        cbl.DataValueField = "PD_Rlvnc_ID"
                        cbl.DataTextField = "PD_Rlvnc_DESC"
                        cbl.DataSource = .Tables(0)
                        cbl.DataBind()
                    End With
                    Dim tmpStr() As String = IIf(lbl.Text = Nothing, "", lbl.Text).ToString.Split("|")
                    For i As Integer = 0 To tmpStr.Length - 1
                        For j As Integer = 0 To cbl.Items.Count - 1
                            If tmpStr(i) = cbl.Items(j).Value Then
                                cbl.Items(j).Selected = True
                            End If
                        Next
                    Next
                End If
            End If
        End If
    End Sub

    Sub BindRelevanceToRole()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim row
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.KTAA_GET_RELEVANCE_ROLE_M")

        If ds.Tables(0).Rows.Count > 0 Then
            If Session("PD_Actions_Relevance") Is Nothing Then
                Session.Add("PD_Actions_Relevance", ds)
            End If

            For Each row In ds.Tables(0).Rows

                Dim str1 As String = row("PD_Rlvnc_DESC")
                Dim str2 As String = row("PD_Rlvnc_ID")
                'Dim FLAG As Boolean = Mid(row("GRD_ID"), InStr(1, row("GRD_ID"), "|"), 1)
                'cbRelevance.Items.Add(New ListItem(str1, str2))
            Next
        End If


    End Sub

    Sub Bind_KTAA_MASTER(ByVal CRId As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@CR_ID", CRId)
            'param(1) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_KTAA_BY_REQUEST", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)

                If Dt.Rows.Count > 0 Then
                    txtlearning1.Text = Dt.Rows(0)("PD_KTA_LEARNIG1").ToString()
                    txtlearning2.Text = Dt.Rows(0)("PD_KTA_LEARNING2").ToString()
                    txtlearning3.Text = Dt.Rows(0)("PD_KTA_LEARNING3").ToString()
                    ViewState("KTAA_ID") = Dt.Rows(0)("PD_KTA_ID").ToString()
                    Dim PDCStatus As Integer = Convert.ToInt16(Dt.Rows(0)("PD_KTA_Status").ToString())

                    If PDCStatus > 0 Then
                        ''commented by nahyan on 17th nov2016 
                        'btnSave.Visible = False
                        'btnSavetoPDC.Visible = False
                        'tableActions.Visible = False
                        'Me.gvCourseActions.Columns("6").Visible = False
                    Else
                        ''commented by nahyan on 17th nov2016 
                        'btnSavetoPDC.Visible = True
                    End If
                    If ViewState("KTAA_ID") <> "" Then
                        Bind_KTAA_ACTION_DETAILS(ViewState("KTAA_ID"))
                    Else
                        Bind_KTAA_ACTION_DETAILS(0)
                    End If
                Else
                    ''commented by nahyan on 17th nov2016 
                    'btnSavetoPDC.Visible = True
                    Bind_KTAA_ACTION_DETAILS(0)
                End If
            Else

            End If


        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try


    End Sub
    Sub BindCourseDetails(ByVal Id As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@CR_ID", Id)
            'param(1) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.GET_COURSE_INFO_BY_CR_ID", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)

                If Dt.Rows.Count > 0 Then
                    lblName.Text = Dt.Rows(0)("EMP_NAME").ToString()
                    lblActivity.Text = Dt.Rows(0)("CM_TITLE").ToString()
                    lblCourseDate.Text = Dt.Rows(0)("EVENTDATE").ToString()
                    lblTrainername.Text = Dt.Rows(0)("Trainers").ToString()
                    lblTraininglocation.Text = Dt.Rows(0)("location").ToString() & "," & Dt.Rows(0)("region").ToString()
                    lblPosition.Text = Dt.Rows(0)("position").ToString()
                    lblBsu.Text = Dt.Rows(0)("BSU_NAME").ToString()



                End If
            End If


        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ViewState("SEND_PDC") = 0
        Dim errormsg As String = String.Empty

        If ViewState("KTAA_ID") = "" Then
            'If callTransaction(errormsg) <> 0 Then
            If callTransactionNew(errormsg) <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

            Else

                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
                Bind_KTAA_MASTER(ViewState("CR_ID"))

            End If
        Else
            If updateTransactionNew(errormsg) <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

            Else

                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
                Bind_KTAA_MASTER(ViewState("CR_ID"))

            End If
        End If
    End Sub

    Protected Sub btnSavetoPDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavetoPDC.Click
        ViewState("SEND_PDC") = 0
        Dim errormsg As String = String.Empty

        If ViewState("KTAA_ID") = "" Then
            'If callTransaction(errormsg) <> 0 Then
            If callTransactionNew(errormsg) <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"
                Exit Sub
            Else
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
                Bind_KTAA_MASTER(ViewState("CR_ID"))

            End If
        Else
            If updateTransactionNew(errormsg) <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"
                Exit Sub
            Else
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
                Bind_KTAA_MASTER(ViewState("CR_ID"))

            End If
        End If

        If Not ValidateSendToPDC() Then Exit Sub
        ViewState("SEND_PDC") = 1
        'Dim errormsg As String = String.Empty
        If SendtoPDC(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"
        Else
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
            Bind_KTAA_MASTER(ViewState("CR_ID"))
        End If
    End Sub

    Private Function callTransactionNew(ByRef errormsg As String) As Integer

        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Dim CM_ID As String = ViewState("CR_ID")
        Dim iReturnvalue As Integer
        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try

                Dim dtCOURSEACTIONDetails As DataTable
                If Session("COURSEACTION") IsNot Nothing Then
                    dtCOURSEACTIONDetails = Session("COURSEACTION")
                    dtCOURSEACTIONDetails.Columns.Remove("UniqueID")

                End If

                Dim status As String = String.Empty


                Dim param(6) As SqlParameter

                param(0) = New SqlParameter("@PD_KTA_LEARNIG1", txtlearning1.Text)
                param(1) = New SqlParameter("@PD_KTA_LEARNIG2", txtlearning2.Text)
                param(2) = New SqlParameter("@PD_KTA_LEARNIG3", txtlearning3.Text)

                param(3) = New SqlParameter("@CR_ID", ViewState("CR_ID"))
                param(4) = New SqlParameter("@STATUS", ViewState("SEND_PDC"))

                param(6) = New SqlParameter("@RETID", SqlDbType.Int)
                param(6).Direction = ParameterDirection.Output
                'param(5) = New SqlParameter("@MyKTAATableType", dtCOURSEACTIONDetails)
                param(5) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(5).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_SAVE_MASTER", param)
                Dim ReturnFlag As Integer = param(5).Value
                Dim id As Integer = param(6).Value
                ViewState("KTAA_ID") = id
                ' ''setting hdn url 
                'If Convert.ToString(ViewState("CR_ID")) <> "" AndAlso Convert.ToString(Session("KTAA_ID")) <> "" Then
                '    Dim CREqID = ViewState("CR_ID")
                '    Dim KTAAA_ID = Convert.ToString(Session("KTAA_ID"))
                '    hidden_link.Attributes("href") = "PD_KTAA_ADD_More.aspx?id=" & CREqID & "&KTA=" & KTAAA_ID

                'End If

                '' Add actions
                Dim paramA(8) As SqlParameter
                Dim iIndex As Integer
                If id > 0 Then
                    'If Session("COURSEACTION") IsNot Nothing Then
                    '    If Session("COURSEACTION").Rows.Count > 0 Then
                    '        For iIndex = 0 To Session("COURSEACTION").Rows.Count - 1

                    '            Dim dr As DataRow = Session("COURSEACTION").Rows(iIndex)

                    '            paramA(0) = New SqlParameter("@PD_KTA_Action_Step", dr("PD_KTA_Action_Step"))
                    '            paramA(1) = New SqlParameter("@PD_KTA_Success", dr("PD_KTA_Success"))
                    '            paramA(2) = New SqlParameter("@PD_KTA_Timeline", dr("PD_KTA_Timeline"))
                    '            paramA(3) = New SqlParameter("@PD_KTA_Resources", dr("PD_KTA_Resources"))
                    '            paramA(4) = New SqlParameter("@PD_ACT_OTHER", "")
                    '            paramA(5) = New SqlParameter("@KTA_ID", ViewState("KTAA_ID"))
                    '            paramA(6) = New SqlParameter("@RelvnceIDs", dr("PD_KTA_Relevance"))

                    '            paramA(7) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                    '            paramA(7).Direction = ParameterDirection.ReturnValue

                    '            SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_SAVE_ACTIONS", paramA)
                    '            iReturnvalue = paramA(7).Value
                    '            If iReturnvalue <> 0 Then
                    '                Exit For

                    '            End If
                    '        Next
                    '    End If
                    'End If
                    For iIndex = 0 To gvActions.Rows.Count - 1
                        Dim RelvIds As String = ""
                        Dim row As GridViewRow = gvActions.Rows(iIndex)
                        Dim cbl As CheckBoxList = CType(row.FindControl("cblRelevance"), CheckBoxList)
                        Dim txtRelevanceOther As TextBox = CType(row.FindControl("txtRelevanceOther"), TextBox)
                        For i As Integer = 0 To cbl.Items.Count - 1
                            If cbl.Items(i).Selected = True Then
                                RelvIds &= cbl.Items(i).Value & "|"
                            End If
                        Next
                        If RelvIds.EndsWith("|") Then RelvIds = RelvIds.TrimEnd("|")

                        'If CType(row.FindControl("txtAction"), TextBox).Text <> Nothing And CType(row.FindControl("txtSuccess"), TextBox).Text <> Nothing Then
                        paramA(0) = New SqlParameter("@PD_KTA_Action_Step", CType(row.FindControl("txtAction"), TextBox).Text)
                        paramA(1) = New SqlParameter("@PD_KTA_Success", CType(row.FindControl("txtSuccess"), TextBox).Text)
                        paramA(2) = New SqlParameter("@PD_KTA_Timeline", CType(row.FindControl("txtTimeLine"), TextBox).Text)
                        paramA(3) = New SqlParameter("@PD_KTA_Resources", CType(row.FindControl("txtResources"), TextBox).Text)
                        paramA(4) = New SqlParameter("@PD_ACT_OTHER", txtRelevanceOther.Text)
                        paramA(5) = New SqlParameter("@KTA_ID", ViewState("KTAA_ID"))
                        paramA(6) = New SqlParameter("@RelvnceIDs", RelvIds)

                        paramA(7) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                        paramA(7).Direction = ParameterDirection.ReturnValue

                        SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_SAVE_ACTIONS_EVAL", paramA)
                        iReturnvalue = paramA(7).Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                        'End If
                    Next
                End If
                ''end add actions 

                If ReturnFlag = -1 Then
                    callTransactionNew = "-1"
                    errormsg = "Record cannot be saved. !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransactionNew = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransactionNew = "0"

                End If
            Catch ex As Exception
                callTransactionNew = "1"
                errormsg = ex.Message
            Finally
                If callTransactionNew = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransactionNew <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function

    Private Function updateTransactionNew(ByRef errormsg As String) As Integer


        Dim tran As SqlTransaction

        Dim KTAA_ID As String = ViewState("KTAA_ID")
        Dim iReturnvalue As Integer
        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try

                Dim dtCOURSEACTIONDetails As DataTable
                If Session("COURSEACTION") IsNot Nothing Then
                    dtCOURSEACTIONDetails = Session("COURSEACTION")
                    dtCOURSEACTIONDetails.Columns.Remove("UniqueID")

                End If

                Dim status As String = String.Empty


                Dim param(7) As SqlParameter

                param(0) = New SqlParameter("@PD_KTA_LEARNIG1", txtlearning1.Text)
                param(1) = New SqlParameter("@PD_KTA_LEARNIG2", txtlearning2.Text)
                param(2) = New SqlParameter("@PD_KTA_LEARNIG3", txtlearning3.Text)

                param(3) = New SqlParameter("@PD_KTA_ID", KTAA_ID)
                param(4) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(4).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_UPDATE_MASTER", param)
                Dim ReturnFlag As Integer = param(4).Value


                '' Delete and insert actions 

                If KTAA_ID > 0 Then
                    Dim paramA(9) As SqlParameter
                    Dim paramD(2) As SqlParameter


                    'If Session("COURSEACTION") IsNot Nothing Then
                    'If Session("COURSEACTION").Rows.Count > 0 Then
                    ''Delte existing data
                    paramD(0) = New SqlParameter("@KTA_ID", KTAA_ID)
                    paramD(1) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                    paramD(1).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_DELETE_ACTIONS", paramD)
                    Dim ReturnFlagD As Integer = paramD(1).Value

                    Dim iIndex As Integer
                    'For iIndex = 0 To Session("COURSEACTION").Rows.Count - 1
                    '    ''insert newly added actions
                    '    Dim dr As DataRow = Session("COURSEACTION").Rows(iIndex)

                    '    paramA(0) = New SqlParameter("@PD_KTA_Action_Step", dr("PD_KTA_Action_Step"))
                    '    paramA(1) = New SqlParameter("@PD_KTA_Success", dr("PD_KTA_Success"))
                    '    paramA(2) = New SqlParameter("@PD_KTA_Timeline", dr("PD_KTA_Timeline"))
                    '    paramA(3) = New SqlParameter("@PD_KTA_Resources", dr("PD_KTA_Resources"))
                    '    paramA(4) = New SqlParameter("@PD_ACT_OTHER", "")
                    '    paramA(5) = New SqlParameter("@KTA_ID", ViewState("KTAA_ID"))
                    '    paramA(6) = New SqlParameter("@RelvnceIDs", dr("PD_KTA_Relevance"))

                    '    paramA(7) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                    '    paramA(7).Direction = ParameterDirection.ReturnValue
                    '    If dr("Status") <> "DELETED" Then
                    '        SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_SAVE_ACTIONS", paramA)
                    '        iReturnvalue = paramA(7).Value
                    '    End If

                    '    If iReturnvalue <> 0 Then
                    '        Exit For

                    '    End If
                    'Next
                    For iIndex = 0 To gvActions.Rows.Count - 1
                        Dim RelvIds As String = ""
                        Dim row As GridViewRow = gvActions.Rows(iIndex)
                        Dim cbl As CheckBoxList = CType(row.FindControl("cblRelevance"), CheckBoxList)
                        Dim txtRelevanceOther As TextBox = CType(row.FindControl("txtRelevanceOther"), TextBox)
                        For i As Integer = 0 To cbl.Items.Count - 1
                            If cbl.Items(i).Selected = True Then
                                RelvIds &= cbl.Items(i).Value & "|"

                            End If
                        Next
                        If RelvIds.EndsWith("|") Then RelvIds = RelvIds.TrimEnd("|")

                        'If CType(row.FindControl("txtAction"), TextBox).Text <> Nothing And CType(row.FindControl("txtSuccess"), TextBox).Text <> Nothing Then
                        paramA(0) = New SqlParameter("@PD_KTA_Action_Step", CType(row.FindControl("txtAction"), TextBox).Text)
                        paramA(1) = New SqlParameter("@PD_KTA_Success", CType(row.FindControl("txtSuccess"), TextBox).Text)
                        paramA(2) = New SqlParameter("@PD_KTA_Timeline", CType(row.FindControl("txtTimeLine"), TextBox).Text)
                        paramA(3) = New SqlParameter("@PD_KTA_Resources", CType(row.FindControl("txtResources"), TextBox).Text)
                        paramA(4) = New SqlParameter("@PD_ACT_OTHER", txtRelevanceOther.Text)
                        paramA(5) = New SqlParameter("@KTA_ID", ViewState("KTAA_ID"))
                        paramA(6) = New SqlParameter("@RelvnceIDs", RelvIds)


                        paramA(7) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                        paramA(7).Direction = ParameterDirection.ReturnValue

                        SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_SAVE_ACTIONS_EVAL", paramA)
                        iReturnvalue = paramA(7).Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                        'End If
                    Next
                    'End If
                    'End If
                End If
                ''end add actions 

                If ReturnFlag = -1 Then
                    updateTransactionNew = "-1"
                    errormsg = "Record cannot be saved. !!!"
                ElseIf ReturnFlag <> 0 Then
                    updateTransactionNew = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    updateTransactionNew = "0"

                End If
            Catch ex As Exception
                updateTransactionNew = "1"
                errormsg = ex.Message
            Finally
                If updateTransactionNew = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf updateTransactionNew <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function

    Private Function ValidateSendToPDC() As Boolean
        ValidateSendToPDC = False
        Dim ErrorMsg As String

        If Me.txtlearning1.Text.Trim = Nothing And Me.txtlearning2.Text.Trim = Nothing And Me.txtlearning3.Text.Trim = Nothing Then
            'ErrorMsg = "Please enter atleast one Key Take-away Learning and Save as Draft before sending to PDC"
            ErrorMsg = "Please enter atleast one Key Take-away Learning"
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & ErrorMsg & "</div>"
            ViewState("errMessage") = ErrorMsg
            Return False
        End If

        Dim dt As DataTable = Nothing
        If Not Session("COURSEACTION") Is Nothing Then
            dt = CType(Session("COURSEACTION"), DataTable)
            Dim DataEntered As Boolean = False
            For Each row As DataRow In dt.Rows
                If row.Item("PD_KTA_ACTION_STEP") <> "" And row.Item("PD_KTA_SUCCESS") <> "" And row.Item("PD_KTA_RELEVANCE").ToString <> "" Then
                    DataEntered = True
                    Exit For
                End If
            Next
            If Not DataEntered Then
                'ErrorMsg = "Please enter atleast one Action Step and Success Criteria and Save as Draft before sending to PDC"
                ErrorMsg = "Please enter atleast one Action Step and Success Criteria"
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & ErrorMsg & "</div>"
                ViewState("errMessage") = ErrorMsg
                Return False
            End If
        Else
            'ErrorMsg = "Please enter atleast one Key Take-away Learning and Action Step and Success Criteria and Save as Draft before sending to PDC"
            ErrorMsg = "Please enter atleast one Key Take-away Learning, Action Step and Success Criteria"
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & ErrorMsg & "</div>"
            ViewState("errMessage") = ErrorMsg
            Return False
        End If
        Return True
    End Function

    Private Function SendtoPDC(ByRef errormsg As String) As Integer

        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Dim KTAA_ID As String = ViewState("KTAA_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim param(6) As SqlParameter

                param(0) = New SqlParameter("@KTAA_ID", ViewState("KTAA_ID"))

                param(1) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(1).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.UpdateKTAA_STATUS", param)
                Dim ReturnFlag As Integer = param(1).Value

                If ReturnFlag = -1 Then
                    SendtoPDC = "-1"
                    errormsg = "Record cannot be saved. !!!"
                ElseIf ReturnFlag <> 0 Then
                    SendtoPDC = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    SendtoPDC = "0"

                End If
            Catch ex As Exception
                SendtoPDC = "1"
                errormsg = ex.Message
            Finally
                If SendtoPDC = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf SendtoPDC <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function

End Class
