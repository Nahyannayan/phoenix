<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comPrintSurveyResults_PD.aspx.vb" Inherits="masscom_comPrintSurveyResults_PD" title="Untitled Page" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Survey Results Analysis</title>
     <script type="text/javascript">
    window.print();
    </script>
   <%-- <link href="../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../cssfiles/tabber.js"></script> 
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/example.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" />
<script language="javascript" type="text/javascript">
 function GetResult(Qid,SurvId,Result) 
 {
 //alert(Qid);
 window.showModalDialog("comSurvPopup.aspx?Qid="+Qid+"&SurvId="+SurvId+"&Result='"+Result+"'","null","height=200,width=550,status=yes,toolbar=no,menubar=no,location=no");
 }
 
</script>
 </head>
<body>
<form id="form1" runat="server">
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> <asp:Label id="lblPDTITLE" runat="server" Font-Bold="True"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
   
    <table border="0"  cellpadding="5" cellspacing="0" style="width: 845px">
       
       
        <tr>
            <td align="left" colspan="2">
            <table border="0">
                <tr>
                <td  > <span class="field-label" > Total Participants : </span></td>
                <td > <span class="field-label" > <asp:Label id="TSent" runat="server" Width="41px" Font-Bold="True" ForeColor="Red"></asp:Label></span></td>
                <td style="width: 231px; color: green;" > <span class="field-label" > Total Response :</span></td>
                <td style="width: 203px" > <span class="field-label" > <asp:Label id="TResponse" runat="server" Width="41px" ForeColor="Green"></asp:Label></span></td>
                </tr>
            </table>    
           </td>
        </tr>
               
        <tr>
            <td align="left" colspan="2" style="height: 347px" valign="top">
                <asp:Table id="TabResults" runat="server" Width="827px">
                </asp:Table><table style="width: 827px">
                   
                </table>
                <%--<asp:Button id="Button1" runat="server" onclick="Button1_Click" Text="Show Result" />--%></td>
        </tr>
    </table>
                </div>
            </div>
    </div>
    </form>
</body>
</html>

