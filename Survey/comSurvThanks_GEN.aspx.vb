Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Web.UI.HtmlControls

Partial Class masscom_comSurvThanks_GEN
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strThanks As String
        Dim PartcId As String

        Try
            Label1.Visible = False
            Label2.Visible = False
            Label3.Visible = False
            Label4.Visible = False
            Dim strSurvayId As String, strSurvayId_encr As String
            strSurvayId_encr = Encr_decrData.Decrypt(Request.QueryString("SurveyId").Replace(" ", "+"))
            strSurvayId = Request.QueryString("SurveyId").ToString()


            If Not Request.QueryString("PartcId") Is Nothing Then
                ViewState("PartcId") = Encr_decrData.Decrypt(Request.QueryString("PartcId").Replace(" ", "+"))
                ViewState("PartcId_ENCRYPT") = Request.QueryString("PartcId").Replace(" ", "+")
                Dim connection As String = ConnectionManger.GetOASISSurveyConnectionString
                Dim sql_query = "SELECT * FROM SRV.COM_SUR_BUSNSUNIT  A  WHERE SurveyId=181 AND BUSUNITID='" & Left(ViewState("PartcId"), 6) & "'"

                Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.Text, sql_query)
                    If readerStudent_Detail.HasRows = True Then
                        While readerStudent_Detail.Read
                            If strSurvayId_encr = "181" Then
                                strThanks = "Thank you for completing the GEMS parent satisfaction survey.  Your valuable feedback on your expectations and experiences will assist us to continuously improve the services we provide to you and your child.<br /><br />Yours sincerely, <br />##PRINCI_NAME## <br />##PRINCI_TITLE##"
                            Else
                                strThanks = "Thank you for taking time to complete the evaluation form."
                            End If

                            'strThanks = strThanks.Replace("##STU_ID_ENCRYPT##", ViewState("PartcId_ENCRYPT"))
                            'strThanks = strThanks.Replace("##SENDING_ID##", Encr_decrData.Encrypt(Convert.ToString(readerStudent_Detail("SENDING_ID"))))
                            If strThanks.Contains("##SCHOOLNAME##") = True Then
                                strThanks = strThanks.Replace("##SCHOOLNAME##", Convert.ToString(readerStudent_Detail("R_DISPLAYNAME")))
                            End If
                            If strThanks.Contains("##PRINCI_NAME##") = True Then
                                strThanks = strThanks.Replace("##PRINCI_NAME##", Convert.ToString(readerStudent_Detail("R_PRINCI")))
                            End If
                            If strThanks.Contains("##PRINCI_TITLE##") = True Then
                                strThanks = strThanks.Replace("##PRINCI_TITLE##", Convert.ToString(readerStudent_Detail("R_PRINCI_DESIG")))
                            End If
                        End While

                    End If
                End Using

            End If
            If strSurvayId = "0" Then
                Label1.Visible = True
                Label2.Visible = False
                Label3.Visible = False
                Label4.Visible = False
            ElseIf strSurvayId_encr = "185" Or strSurvayId_encr = "193" Then
                Dim strredirect As String = "../PD/PDMyEventRequests.aspx?MainMnu_code=t0YwlO9ry8Y=&datamode=Zo4HhpVNpXc="
                Dim meta As New HtmlMeta()
                meta.HttpEquiv = "Refresh"
                meta.Content = "5;url=" & strredirect
                Me.Page.Controls.Add(meta)

                Label1.Visible = False
                Label2.Visible = False
                Label3.Visible = True
                Label3.Text = "Thank you for taking time to complete the evaluation form.You will be redirected to MY PD REQUESTS in 5 seconds"
                'strThanks = strThanks.Replace("##SCHOOLNAME##", Convert.ToString(readerStudent_Detail("BSU_NAME")))
                Label4.Visible = False


            Else
                Label1.Visible = False
                Label2.Visible = False
                Label3.Visible = True
                Label3.Text = "Thank you for taking time to complete the evaluation form."
                'strThanks = strThanks.Replace("##SCHOOLNAME##", Convert.ToString(readerStudent_Detail("BSU_NAME")))
                Label4.Visible = False

            End If


        Catch ex As Exception

        End Try

    End Sub

    Private Function DispalyThanksMessage(ByVal SurvId As String) As String
        Dim connection As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        Dim strMessage As String = ""
        strSQL = " SELECT SURVEY_ID,SURVEYURL FROM SRV.COM_SUR_DETAILS WHERE SURVEY_ID=" & SurvId
        Dim dsSurveyInfo As DataSet
        dsSurveyInfo = SqlHelper.ExecuteDataset(connection, CommandType.Text, strSQL)
        If dsSurveyInfo.Tables(0).Rows.Count >= 1 Then
            strMessage = IIf(IsDBNull(dsSurveyInfo.Tables(0).Rows(0).Item(1)), "", dsSurveyInfo.Tables(0).Rows(0).Item(1))
            strMessage = mergethanksmsg(strMessage)
        End If
        Return strMessage

    End Function
    Public Function mergethanksmsg(ByVal strMessage As String) As String
        Dim str_conn As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()
        Dim sql_query = "SELECT STU_NO,BSU_ID,BSU_NAME,BSU_EMAIL,ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')PRINCIPLE FROM STUDENT_M INNER JOIN BUSINESSUNIT_M ON BUSINESSUNIT_M.BSU_ID=STU_BSU_ID INNER JOIN EMPLOYEE_M ON BUSINESSUNIT_M.BSU_ID=EMP_BSU_ID INNER JOIN dbo.EMPDESIGNATION_M ON DES_ID=EMP_DES_ID WHERE DES_FLAG='SD' AND EMP_DES_ID=298 AND STU_NO='" & ViewState("PartcId") & "'"

        Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, sql_query)
            If readerStudent_Detail.HasRows = True Then
                While readerStudent_Detail.Read


                    If strMessage.Contains("#school#") = True Then
                        strMessage = strMessage.Replace("#school#", Convert.ToString(readerStudent_Detail("BSU_NAME")))
                    End If
                    If strMessage.Contains("#principal#") = True Then
                        strMessage = strMessage.Replace("#principal#", Convert.ToString(readerStudent_Detail("PRINCIPLE")))
                    End If
                    If strMessage.Contains("#contact#") = True Then
                        strMessage = strMessage.Replace("#contact#", Convert.ToString(readerStudent_Detail("BSU_EMAIL")))
                    End If
                End While

            End If
        End Using
        Return strMessage
    End Function
    Private Shared Function GetSurveyLanguage(ByVal SurvId As String) As String
        Dim connection As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = ""
        Dim strReturn As String = ""
        strSQL = " SELECT SURVEY_ID,SURVLANGUAGE FROM SRV.COM_SUR_DETAILS WHERE SURVEY_ID= " & SurvId
        Dim dsSurveyInfo As DataSet
        dsSurveyInfo = SqlHelper.ExecuteDataset(connection, CommandType.Text, strSQL)
        If dsSurveyInfo.Tables(0).Rows.Count >= 1 Then

            If IsDBNull(dsSurveyInfo.Tables(0).Rows(0).Item("SURVLANGUAGE")) = False Then
                If dsSurveyInfo.Tables(0).Rows(0).Item("SURVLANGUAGE") = "E" Then
                    strReturn = "E"
                Else
                    strReturn = "A"
                End If
            Else
                strReturn = "E"
            End If
        End If

        Return strReturn

    End Function


End Class
