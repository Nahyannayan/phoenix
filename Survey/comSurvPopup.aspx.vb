Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports SurveyChart
Imports System.Drawing

Partial Class masscom_comSurvPopup
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim QId As String
    Dim survId As String
    Dim strRes As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")

            If Not IsPostBack Then
              
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                QId = Request.QueryString("Qid")
                strRes = Request.QueryString("Result").Substring(1, Request.QueryString("Result").Length - 2)
                survId = Request.QueryString("SurvId")
                strRes = strRes.Replace(".", " ")
                ViewState("Qid") = QId
                ViewState("survId") = survId
                ViewState("Result") = strRes

                GridBind(survId, QId, strRes)
                Dim DsQuestion As DataSet = GetSurveyQuestion(survId, QId)
                If DsQuestion.Tables(0).Rows.Count >= 1 Then
                    Label1.Text = DsQuestion.Tables(0).Rows(0).Item("QUESTDESC")
                    Label1.Text = Label1.Text + "?"
                End If

            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Function GridBind(ByVal SurvId As String, ByVal Qid As String, ByVal answer As String)
        Dim str_conn As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim str_query As String = "SELECT row_number() Over (Order By [QUESTIONID])As 'Sl No',QUESTIONID,COMMENTS FROM SRV.COM_SUR_RESULTDET WHERE SURVEYID=" & ViewState("survId") & " " & _
        " AND QUESTIONID=" & ViewState("Qid") & " AND BUSID IN(" & Session("Bsuid") & ") AND ANSWER='" & ViewState("Result") & "' AND  COMMENTS<>''"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvPopUp.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvPopUp.DataBind()
            Dim columnCount As Integer = gvPopUp.Rows(0).Cells.Count
            gvPopUp.Rows(0).Cells.Clear()
            gvPopUp.Rows(0).Cells.Add(New TableCell)
            gvPopUp.Rows(0).Cells(0).ColumnSpan = columnCount
            gvPopUp.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvPopUp.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvPopUp.DataBind()
        End If
        gvPopUp.HeaderRow.Cells(1).Text = "Comments - " & answer

    End Function

    Private Shared Function GetSurveyQuestion(ByVal SurvId As String, ByVal Qid As String) As DataSet

        Dim connection As String = ConnectionManger.GetOASISSurveyConnectionString
        Dim strSQL As String = "SELECT QUEST_ID,SURVEYID,QUESTDESC " & _
                     " FROM SRV.COM_SUR_QUESTION " & _
                     " WHERE SURVEYID=" & SurvId & " AND QUEST_ID=" & Qid '& " AND ANSTYPE<>'C' ORDER BY QUEST_ID"

        Dim dsSurveyInfo As DataSet
        dsSurveyInfo = SqlHelper.ExecuteDataset(connection, CommandType.Text, strSQL)
        Return dsSurveyInfo

    End Function

    Protected Sub gvPopUp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPopUp.PageIndexChanging
        Try
            gvPopUp.PageIndex = e.NewPageIndex
            If Session("gm") Is Nothing Then
                GridBind(survId, QId, strRes)
            Else
                Dim dsCommentsList As DataSet
                dsCommentsList = Session("gm")
                gvPopUp.DataSource = dsCommentsList
                gvPopUp.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub


    Protected Sub gvPopUp_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPopUp.RowCreated
        Try
            'If e.Row.RowType = DataControlRowType.DataRow Then
            '    e.Row.Cells(0).Text = e.Row.RowIndex + 1
            'End If

        Catch ex As Exception

        End Try
    End Sub
End Class
