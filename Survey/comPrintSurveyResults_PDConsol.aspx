<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comPrintSurveyResults_PDConsol.aspx.vb" Inherits="masscom_comPrintSurveyResults_PDConsol" title=":::PD:::" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Survey Results Analysis</title>
     <script type="text/javascript">
    window.print();
    </script>
    <link href="../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../cssfiles/tabber.js"></script> 
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/example.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
 function GetResult(Qid,SurvId,Result) 
 {
 //alert(Qid);
 window.showModalDialog("comSurvPopup.aspx?Qid="+Qid+"&SurvId="+SurvId+"&Result='"+Result+"'","null","height=200,width=550,status=yes,toolbar=no,menubar=no,location=no");
 }
 
</script>
 </head>
<body>
<form id="form1" runat="server">
    <div>
   
    <table border="0"  cellpadding="5" cellspacing="0" style="width: 845px">
       
        <tr>
            <td align="center" class="subheader_img" style="text-align: left;">
                <asp:Label id="lblPDTITLE" runat="server" Font-Bold="True">Selected Course(s):</asp:Label>
            </td>
        </tr>
       
        <tr>
            <td align="center" class="matters"  style="text-align: left;" height="100%">
                <asp:DataList ID="lstCourses" runat="server" CellPadding="2" CellSpacing="2" RepeatColumns="2" RepeatDirection="Horizontal" Width="100%">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# bind("CM_TITLE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
        <tr>
            <td align="left">
            <table border="0">
                <tr>
                <td style="width: 273px; color: red;" class="subheader_img" >Total Participants : </td>
                <td style="width: 257px; font-size: 12px; font-family: Verdana;" class="subheader_img"><asp:Label id="TSent" runat="server" Width="41px" Font-Bold="True" ForeColor="Red"></asp:Label></td>
                <td style="width: 231px; color: green;" class="subheader_img">Total Response :</td>
                <td style="width: 203px" class="subheader_img"><asp:Label id="TResponse" runat="server" Width="41px" ForeColor="Green"></asp:Label></td>
                </tr>
            </table>    
           </td>
        </tr>
               
        <tr>
            <td align="left" style="height: 347px" valign="top">
                <asp:Table id="TabResults" runat="server" Width="827px">
                </asp:Table><table style="width: 827px">
                   
                </table>
                <%--<asp:Button id="Button1" runat="server" onclick="Button1_Click" Text="Show Result" />--%></td>
        </tr>
    </table>


       

    </div>
    </form>
</body>
</html>

