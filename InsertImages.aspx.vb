Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class InsertImages
    Inherits System.Web.UI.Page

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        InsertLogo()
        ' lblImg.Text = "Image Saved"
    End Sub
    Sub BindBusinessUnit()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_query As String = "Select bsu_id,bsu_name from businessunit_m order by bsu_name"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "bsu_name"
        ddlBsu.DataValueField = "bsu_id"
        ddlBsu.DataBind()
    End Sub
    Sub InsertLogo()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
         Dim fileStr As FileStream
        Dim binRed As BinaryReader
        Dim x As Byte()
        Dim param() As Object
        'fileStr = New FileStream(flImage.FileName, FileMode.Open)
        fileStr = New FileStream("C:\0JUNK\logo_gma.jpg", FileMode.Open)
        binRed = New BinaryReader(fileStr)
        x = binRed.ReadBytes(binRed.BaseStream.Length)
        fileStr.Close()
        binRed.Close()
        param = New Object() {ddlBsu.SelectedValue, "LOGO", x}
        SqlHelper.ExecuteNonQuery(str_conn, "AddBsuImage", param)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            BindBusinessUnit()
        End If
    End Sub
End Class
