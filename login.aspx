<%@ Page Language="VB" AutoEventWireup="false" CodeFile="login.aspx.vb" Inherits="loginMain" Title="PHOENIXBETA" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<html lang="en">
<head runat="server">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>PHOENIXBETA</title>
    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="cssfiles/sb-admin.css" rel="stylesheet">

    <link rel="shortcut icon" href="Images/favicon.ico" type="image/x-icon">
    <%--<link href="cssfiles/title.css" rel="stylesheet" type="text/css" />--%>

    <style type="text/css">
        .card-login {
            box-shadow: 2px 0px 12px #333333;
        }
    </style>
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
</head>
<body class="bg-dark" style="background: url('images/loginImage/bg-gems.jpg') no-repeat center bottom transparent; background-size: 100%;">
    <script lang="javascript" type="text/javascript">
        self.moveTo(0, 0); self.resizeTo(screen.availWidth, screen.availHeight);

        function CloseWindow() {
            window.open('', '_self', '');
            window.close();
        }
        function CloseFrame() {
            
                var mPopup = $find('fancybox');
                alert(mPopup);
                if (mPopup) mPopup.close();
            
            //$.fancybox.close();
        }
        function show() {
            if (document.getElementById('<%=txtUsername.ClientID %>').value != '' && document.getElementById('<%=txtPassword.ClientID %>').value != '')
                setTimeout('showGif()', 500);
        }
        function showGif() {
            setTimeout('showGif()', 500);
            //document.getElementById('yourImage').style.display = 'block';
        }
        function CheckOnPostback() {
            if (document.getElementById('<%=hfExpired.ClientID %>').value == 'expired')
                ChangePassword();
        }

        function ChangePassword() {
            var sFeatures;
            sFeatures = "dialogWidth: 450px; ";
            sFeatures += "dialogHeight: 250px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            result = window.showModalDialog("ChangePassword.aspx", "", sFeatures)
        }

        function ForgotPassword() {
            var sFeatures;
            sFeatures = "dialogWidth: 550px; ";
            sFeatures += "dialogHeight: 300px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var userName;
            userName = document.getElementById('<%=txtUsername.ClientID %>').value
        //result = window.showModalDialog("forgotpassword.aspx?id=" + userName, "", sFeatures)
            ShowWindowWithClose("/forgotpassword.aspx?id=" + userName, "", "30%", "40%");


    }

    function openfpassword() {
        Response.Redirect("https://selfreset.gemseducation.com");

    }
    var cflag = 0
    function capLock(e) {
        if (cflag == 0) {


            kc = e.keyCode ? e.keyCode : e.which;
            sk = e.shiftKey ? e.shiftKey : ((kc == 16) ? true : false);
            if (((kc >= 65 && kc <= 90) && !sk) || ((kc >= 97 && kc <= 122) && sk))
                alert('Caps Lock is ON')
            cflag = 1
        }


    }
    function ShowWindowWithClose(gotourl, pageTitle, w, h) {
        $.fancybox({
            type: 'iframe',
            //maxWidth: 300,
            href: gotourl,
            //maxHeight: 600,
            fitToView: true,
            padding: 6,
            width: w,
            height: h,
            autoSize: false,
            openEffect: 'none',
            showLoading: true,
            closeClick: true,
            closeEffect: 'fade',
            'closeBtn': true,
            afterLoad: function () {
                this.title = '';//ShowTitle(pageTitle);
            },
            helpers: {
                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                title: { type: 'inside' }
            },
            onComplete: function () {
                $("#fancybox-wrap").css({ 'top': '90px' });
            }
        });
        return false;
    }
    </script>


    <%--Boootstrap html goes here--%>
    <div class="container">
        <div class="row">
            <div class="Absolute-Center is-Responsive">
                <div class="card card-login mx-auto mt-5">
                    <div class="card-header text-center">
                        <img src="images/loginImage/oasias-logo-login.png">
                    </div>
                    <div class="card-body">

                        <form id="form1" defaultfocus="imgBtnLogin" runat="server">
                            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager2" runat="Server">
                            </ajaxToolkit:ToolkitScriptManager>

                            <asp:Label ID="lblResult" runat="server" EnableViewState="False" CssClass="error" ForeColor="Red" />

                            <div class="form-group">

                                <asp:TextBox ID="txtUsername" runat="server" placeholder="Enter Username" class="form-control" aria-describedby="emailHelp"></asp:TextBox>
                                <%--<input class="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Enter email">--%>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txtPassword" class="form-control" runat="server" onkeypress="capLock(event)" type="password" EnableViewState="False" placeholder="Password"></asp:TextBox>
                                <%--<input class="form-control" id="exampleInputPassword1" type="password" placeholder="Password">--%>
                            </div>
                            <%--<div class="form-group">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox"> Remember Password
                                    </label>
                                </div>
                            </div>--%>
                            <div class="row mt-2">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <asp:Button ID="imgBtnLogin" runat="server" class="btn btn-primary btn-block" OnClientClick="show();" Text="Login" />
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <asp:Button ID="imgBtnExit" runat="server" class="btn btn-primary btn-block" OnClientClick="CloseWindow();" Text="Exit" />
                                </div>
                            </div>

                            <asp:Label ID="lblForgotPassword" Text="Forgot Password? :" runat="server" CssClass="font-small"></asp:Label>
                            <asp:LinkButton ID="lnkfpassword" CausesValidation="false" runat="server" CssClass="font-small">GEMS Staff</asp:LinkButton>
                            &nbsp;
                            <asp:Label ID="lblEmpty" Text=":" runat="server"></asp:Label>
                            &nbsp;
                <asp:LinkButton ID="lnkfSpassword" CausesValidation="false" runat="server" CssClass="font-small">Others</asp:LinkButton>


                            <asp:RequiredFieldValidator ID="NReq" runat="server" ControlToValidate="txtUsername"
                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />Username is required."></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="PNReq" runat="server" ControlToValidate="txtPassword"
                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />Password is required."></asp:RequiredFieldValidator>&nbsp;
                <ajaxToolkit:ValidatorCalloutExtender ID="PNReqE" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                    TargetControlID="PNReq" Width="230px">
                </ajaxToolkit:ValidatorCalloutExtender>
                            <ajaxToolkit:ValidatorCalloutExtender ID="NReqE" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                TargetControlID="NReq" Width="230px">
                            </ajaxToolkit:ValidatorCalloutExtender>
                            <asp:HiddenField ID="hfExpired" runat="server" />





                            <script>                                //document.getElementById('yourImage').style.display ='none';</script>
                        </form>

                        <%--<div class="float-right"> <img src="images/loginImage/gems-logo.png" style="width: 80px;"> </div>--%>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
     <script type="text/javascript" lang="javascript">
         
    </script>
</body>
</html>
