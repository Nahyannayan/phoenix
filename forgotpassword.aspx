<%@ Page Language="VB" AutoEventWireup="false" CodeFile="forgotpassword.aspx.vb" Inherits="forgotpassword" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <title>Password Recovery</title>
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />

    <%--<link href="/cssfiles/Popup.css" rel="stylesheet" />--%>
    <script type="text/javascript">
        var fullURL = parent.document.URL
        var un = fullURL.substring(fullURL.indexOf('?') + 4, fullURL.length)
        window.setTimeout('setval();', 1000);
        function setval() {
            var txtbox = document.getElementById('<%=txtusername.ClientID%>');
            if (txtbox)
                txtbox.value = '';//un
        }
        function handleError() {
            return true;
        }
        window.onerror = handleError;
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="div1" runat="server" class="alert alert-success" style="width: 100%; padding: 5px !important; font-size: smaller !important;">
            If the answer to the security question matches your answer
            during the time of registering for the password recovery process, then the new password would be sent to the registered email.
        </div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td>
                    <div style="width: 100%;">
                        <br />
                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnok">
                            <table cellpadding="5" cellspacing="0" style="width: 100%;">
                                <tr>
                                    <td class="subheader_img">Forgot Password ?</td>
                                </tr>
                                <tr>
                                    <td align="center">Please complete the following details.<br />
                                        <br />
                                        <asp:Panel ID="Panel3" runat="server">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td>Enter Username</td>
                                                    <td>:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtusername" runat="server" ValidationGroup="enter" Width="100%"></asp:TextBox></td>
                                                    <td>
                                                        <asp:Button ID="btnok" CssClass="button" runat="server" Text="Ok" ValidationGroup="enter" /></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="Panel1" runat="server" Visible="False" DefaultButton="btnsent">
                                            <table>
                                                <tr>
                                                    <td>Question</td>
                                                    <td>:</td>
                                                    <td align="left">
                                                        <asp:Label ID="lblquestion" runat="server" Width="191px"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td>Answer</td>
                                                    <td>:</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtanswer" runat="server" TextMode="Password" ValidationGroup="sent" Width="100%"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="3">
                                                        <asp:Button ID="btnsent" runat="server" CssClass="button" Text="Send" ValidationGroup="sent" /></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter username" Display="None" SetFocusOnError="True" ControlToValidate="txtusername" ValidationGroup="enter"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please answer the requested question." Display="None" SetFocusOnError="True" ControlToValidate="txtanswer" ValidationGroup="sent"></asp:RequiredFieldValidator></td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="Panel4" runat="server" Visible="False">
                            <table style="border: solid 1px #1b80b6" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td class="subheader_img">Message</td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="lblmessage" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Button ID="btnclose" runat="server" Text="Try again" CssClass="button" Visible="False" CausesValidation="False" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:HiddenField ID="Hiddenquestionid" runat="server" />
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                            ShowSummary="False" ValidationGroup="enter" />
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
                            ShowSummary="False" ValidationGroup="sent" />
                    </div>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
