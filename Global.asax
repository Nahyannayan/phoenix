<%@ Application Language="VB" %>
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="Microsoft.ApplicationBlocks.Data" %> 
<%@ Import Namespace="System.Web.Configuration" %> 
<%@ Import Namespace="System.Data.SqlClient" %> 
<%@ Import Namespace="System.IO" %> 

<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
        'DevExpress.XtraReports.Web.Extensions.ReportStorageWebExtension.RegisterExtensionGlobal(New ReportStorageWebExtension())
        'DevExpress.XtraReports.Web.ReportDesigner.DefaultReportDesignerContainer.EnableCustomSql()
      
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
        'Added by Jacob to catch trace unhandled exceptions from pages
        Dim lastErrorWrapper As HttpException = TryCast(Server.GetLastError(), HttpException)
        Dim lastError As Exception = lastErrorWrapper
        If lastErrorWrapper.InnerException IsNot Nothing Then
            lastError = lastErrorWrapper.InnerException
        End If

        Dim lastErrorTypeName As String = lastError.GetType().ToString()
        Dim lastErrorMessage As String = lastError.Message
        Dim lastErrorStackTrace As String = lastError.StackTrace
        
        UtilityObj.Errorlog(Left(lastErrorMessage.Trim() & "-" & lastErrorStackTrace.Trim, 800), "PHOENIXBETA_GLOBALASAX")
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
           
        ' Code that runs when a new session is started
        Session("sUsr_id") = ""
        ' Code that runs when a new session is started
        Session("LineId") = New Integer
        Session("gdtSub") = New DataTable
       
        
        
        '   ---- Access Database and Fill The Default Values
        Dim gdsSysInfo As New DataSet
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        str_Sql = " SELECT * From sysinfo_s"
        gdsSysInfo = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If gdsSysInfo.Tables(0).Rows.Count > 0 Then
            Session("IntrAC") = gdsSysInfo.Tables(0).Rows(0)("SYS_INTEREST_SGP_ID")
            Session("AcrdAc") = gdsSysInfo.Tables(0).Rows(0)("SYS_ACCRINTR_CTRL_ACT_ID")
            Session("PrepdAC") = gdsSysInfo.Tables(0).Rows(0)("SYS_PREEXPPDC_SGP_ID")
            Session("ChqissAC") = gdsSysInfo.Tables(0).Rows(0)("SYS_CHQISSPDC_SGP_ID")
        End If
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        'clear the session ID from users_m
        'Session.RemoveAll()
        Try
            
        
            Dim auditFlag As Integer = UtilityObj.operOnAudiTable("Logout", "logout", "Logout")
            If auditFlag <> 0 Then
                Throw New ArgumentException("Unable to track your request")
            End If
            
        Catch
            UtilityObj.Errorlog("Session Error")
        End Try
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
    
    'Sub Application_EndRequest(ByVal sender As Object, ByVal e As EventArgs)

    '    Dim strMessage As String = String.Empty

    '    Try

    '        Dim fp As StreamReader

    '        fp = File.OpenText(Server.MapPath("~") & "\FooterMessage\Footer.txt")

    '        strMessage = fp.ReadToEnd()

    '        fp.Close()

    '    Catch err As Exception

    '    End Try

    '    If strMessage <> String.Empty Then

    '        With Response

    '            .Write(strMessage)

    '        End With

    '    End If

    'End Sub


       
</script>