Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports EmailService

Imports System.Data.SqlClient

Imports System.Web.Configuration
Imports system
Imports Encryption64

Partial Class forgotpassword
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnok.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim username As String = String.Empty
        username = txtusername.Text.Trim()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", username)
        '' pParms(1) = New SqlClient.SqlParameter("@NEW_PWD", New_Password)
        ''   Dim sql_query = "select  QUESTIONS,QUES_ID  from PASSWORD_QUESTIONS a inner join USER_PASSWORD_QUESTIONS b on a.QUES_ID=b.QUESTION_ID where USER_NAME='" & username & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GETPASSWORD_QUESTIONS", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            Panel3.Visible = False
            lblquestion.Text = ds.Tables(0).Rows(0).Item("QUESTIONS").ToString()
            Hiddenquestionid.Value = ds.Tables(0).Rows(0).Item("QUES_ID").ToString()
            Panel1.Visible = True
        Else
            lblmessage.Text = "You have not registered for the password recovery process. Please contact the administrator."
            Panel2.Visible = False
            Panel4.Visible = True
            btnclose.Visible = True
            div1.Visible = False
        End If


    End Sub

    Private Function RandomNumber(ByVal min As Integer, ByVal max As Integer) As Integer
        Dim random As New Random()
        Return random.Next(min, max)
    End Function 'RandomNumber 

    Public Function RandomString(ByVal size As Integer, ByVal lowerCase As Boolean) As String

        'Dim builder As New StringBuilder()
        'Dim random As New Random()
        'Dim ch As Char
        'Dim i As Integer
        'For i = 0 To size - 1
        '    ch = Convert.ToChar(Convert.ToInt32((26 * random.NextDouble() + 65)))
        '    builder.Append(ch)
        'Next

        'If lowerCase Then
        '    Return builder.ToString().ToLower()
        'End If

        'Return builder.ToString()

        Dim _allowedChars As String = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ"
        Dim randNum As New Random()
        Dim chars As Char() = New Char(size - 1) {}
        Dim allowedCharCount As Integer = _allowedChars.Length

        For i As Integer = 0 To size - 1
            chars(i) = _allowedChars(CInt(((_allowedChars.Length) * randNum.NextDouble())))
        Next

        Return New String(chars)


    End Function


    Public Function generatenewpassword() As String
        Dim builder As New StringBuilder()
        builder.Append(RandomString(4, True))
        builder.Append(RandomNumber(1000, 9999))
        builder.Append(RandomString(2, False))
        Return builder.ToString()

    End Function

    Protected Sub btnsent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsent.Click
        Dim bPasswordUpdate As Boolean = False
        Dim status2 As Integer
        Try
            btnclose.Visible = False
            Dim ds As DataSet

            Dim str_conn_CHECK = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

            Dim username As String = String.Empty
            username = txtusername.Text.Trim()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", username)

            '  Dim sql_query_CHECK = "select * from BUSINESSUNIT_M where BSU_ID IN (select USR_BSU_ID from USERS_M where USR_NAME='" & username & "' AND USR_ROL_ID IN (SELECT ROL_ID From Roles_M WHERE ROL_INGLG=1) ) "

            ds = SqlHelper.ExecuteDataset(str_conn_CHECK, CommandType.StoredProcedure, "dbo.GETPASSWORD_RESETACCESS", pParms)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim ALLOW_CHANGE = ds.Tables(0).Rows(0).Item("BSU_bGLG_IMPL").ToString().ToUpper
                If ALLOW_CHANGE = "TRUE" Then
                   
                End If

            End If


            Dim passwordEncr As New Encryption64
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
           
            Dim answer As String = String.Empty
            answer = txtanswer.Text.Trim()
            Dim Parms(3) As SqlClient.SqlParameter
            Parms(0) = New SqlClient.SqlParameter("@USR_NAME", username)
            Parms(1) = New SqlClient.SqlParameter("@QuestionId", Hiddenquestionid.Value.Trim())
            Parms(2) = New SqlClient.SqlParameter("@answer", answer)
            '  Dim sql_query = "select EMAIL_ID from USER_PASSWORD_QUESTIONS where USER_NAME='" & username & "' and QUESTION_ID='" & Hiddenquestionid.Value.Trim() & "' and ANSWER='" & answer & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GETUSER_EMAILID_FORPASSWORDRESET", Parms)
            Dim toemailid As String
            If ds.Tables(0).Rows.Count > 0 Then
                Dim password = generatenewpassword()
                Dim iParms(3) As SqlClient.SqlParameter
                iParms(0) = New SqlClient.SqlParameter("@USR_NAME", username)
                iParms(1) = New SqlClient.SqlParameter("@Password", passwordEncr.Encrypt(password))
                '' sql_query = "update  USERS_M  set USR_PASSWORD='" & passwordEncr.Encrypt(password) & "', USR_EXPDATE=DATEADD(month,1,USR_EXPDATE), USR_bDisable='False',USR_TRYCOUNT=0 where USR_NAME='" & username & "'"

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, "dbo.RESET_USER_PASSWORD", iParms)


                bPasswordUpdate = True
                If bPasswordUpdate Then
                    status2 = VerifyandUpdatePassword(Encr_decrData.Encrypt(password), txtusername.Text.Trim())
                    Dim vGLGUPDPWD As New com.ChangePWDWebService
                    vGLGUPDPWD.Url = "http://10.10.1.35/release/ChangePWDWebService.asmx"
                    Dim respon As String = vGLGUPDPWD.ChangePassword(txtusername.Text.Trim(), Encr_decrData.Encrypt(password), Encr_decrData.Encrypt(password))
                End If


                'password = passwordEncr.Decrypt(Convert.ToString(password).Replace(" ", "+"))

                toemailid = ds.Tables(0).Rows(0).Item("EMAIL_ID").ToString()
                Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()

                ' sql_query = "select * from BSU_COMMUNICATION_M where BSC_BSU_ID='" & "999998" & "' and BSC_TYPE='HELPDESK' "

                ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.StoredProcedure, "dbo.GET_BSU_COMMUNICATION")

                Dim eusername = ""
                Dim epassword = ""
                Dim port = ""
                Dim host = ""
                Dim fromemailid = ""
                Dim status As String = ""
                If ds.Tables(0).Rows.Count > 0 Then
                    eusername = ds.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
                    epassword = ds.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
                    port = ds.Tables(0).Rows(0).Item("BSC_PORT").ToString()
                    host = ds.Tables(0).Rows(0).Item("BSC_HOST").ToString()
                    fromemailid = ds.Tables(0).Rows(0).Item("BSC_FROMEMAIL").ToString()
                    status = EmailService.email.SendPlainTextEmails(fromemailid, toemailid, "GEMS OASIS Password Recovery", MailBody(txtusername.Text.Trim(), password), eusername, epassword, host, port, 0, False)
                End If

                If status.IndexOf("Error") > -1 Then
                    lblmessage.Text = status
                    btnclose.Visible = True
                Else
                    lblmessage.Text = "You have successfully reset your password. The new password has been sent to your registered email id (" & toemailid & ")."
                End If
            Else
                lblmessage.Text = "The answer to the security question does not match with our record."
                btnclose.Visible = True
            End If
        Catch ex As Exception
            lblmessage.Text = "An error occurred while processing your request. Please try again."
            btnclose.Visible = True
        End Try

        Panel2.Visible = False
        Panel4.Visible = True
        'btnclose.Visible = True
        div1.Visible = False
    End Sub
    Public Shared Function VerifyandUpdatePassword(ByVal New_Password As String, _
                ByVal username As String) As Integer
        Using connection As SqlConnection = ConnectionManger.GetGLGConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", username)
            pParms(1) = New SqlClient.SqlParameter("@NEW_PWD", New_Password)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            'SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            Return pParms(2).Value
        End Using
    End Function


    Public Function MailBody(ByVal username As String, ByVal password As String) As String

        Dim sb As New StringBuilder

        sb.Append("<table border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>")
        sb.Append("<tr><td colspan='3' style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>GEMS OASIS - Password Recovery</td></tr>")
        sb.Append("<tr><td>User Name</td><td>:</td><td>" & username & "</td></tr>")
        sb.Append("<tr><td>Password</td><td>:</td><td>" & password & "</td></tr>")
        sb.Append("<tr><td>Request Time</td><td>:</td><td>" & Convert.ToDateTime(Date.Now()).ToString("dd/MMM/yyyy HH:mm:ss") & "</td></tr>")
        sb.Append("<tr><td colspan='3' rowspan='3'><br /><br />From<br /><br />GEMS- Help Desk <br /> <br /> <span style='font-size: xx-small; font-style: italic'> <b>This is a system generated email, please do not reply.</b> <br> <br> The contents of this email and any attachments are confidential. If you are not the intended recipient, please notify us by return, delete the same from your system and note that any copying, disclosure or reliance upon the same is prohibited. </span> </td>")
        sb.Append("</tr><tr></tr><tr></tr>")
        sb.Append("</table>")

        Dim val As String = sb.ToString()

        Return val

    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnclose_Click(sender As Object, e As EventArgs) Handles btnclose.Click
        Panel2.Visible = True
        Panel4.Visible = False
        'btnclose.Visible = True
        div1.Visible = True
    End Sub
End Class
