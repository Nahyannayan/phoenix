Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml
Imports System.IO
Imports System.Data

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class gpsGenerateXml
     Inherits System.Web.Services.WebService

#Region "GetGeoFencingPoints"
    <WebMethod()> _
          Public Function GetGeoFencingPoints(ByVal UnitId As String) As XmlDocument
        Dim returnvalue As XmlDocument
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@UNIT_ID", UnitId)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_GEO_FENCING_POINTS_FOR_GIVEN_UNIT_ID", pParms)
        returnvalue = GenerateXMLForMap_GetGeoFencingPoints(ds)

        Return returnvalue

    End Function
    Public Function GenerateXMLForMap_GetGeoFencingPoints(ByVal ds As DataSet) As XmlDocument
        Dim data As String = ""
        Try
            Dim ms As New StringWriter
            Dim writer As New XmlTextWriter(ms)
            writer.Formatting = Formatting.None
            writer.Indentation = 6
            writer.WriteStartDocument()
            writer.WriteStartElement("markers")
            If ds.Tables(0).Rows.Count >= 1 Then
                Dim i = 0
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i <> ds.Tables(0).Rows.Count - 1 Then
                        WriteItem_GetGeoFencingPoints(writer, ds.Tables(0).Rows(i).Item("LATITUDE").ToString(), ds.Tables(0).Rows(i).Item("LONGITUDE").ToString(), ds.Tables(0).Rows(i).Item("UNIT_ID").ToString(), "", ds.Tables(0).Rows(i).Item("SPEED").ToString(), ds.Tables(0).Rows(i).Item("DIRECTION").ToString())
                    Else
                        Dim lat = ds.Tables(0).Rows(i).Item("LATITUDE").ToString()
                        Dim lng = ds.Tables(0).Rows(i).Item("LONGITUDE").ToString()
                        Dim unitid = ds.Tables(0).Rows(i).Item("UNIT_ID").ToString()
                        Dim inout = status(unitid, lat, lng).ToString()
                        WriteItem_GetGeoFencingPoints(writer, lat, lng, unitid, inout, ds.Tables(0).Rows(i).Item("SPEED").ToString(), ds.Tables(0).Rows(i).Item("DIRECTION").ToString())
                    End If
                Next
            End If
            writer.WriteEndElement()
            writer.Flush()
            writer.Close()
            data = ms.ToString()
            Dim xmlDoc As New XmlDocument()
            xmlDoc.LoadXml(data)
            Return xmlDoc


        Catch ex As Exception
            'Throw ex
        Finally
            ds = Nothing
        End Try



    End Function

    Protected Function WriteItem_GetGeoFencingPoints(ByVal writer As XmlTextWriter, ByVal Y As String, ByVal X As String, ByVal Unit_id As String, ByVal Status As String, ByVal speed As String, ByVal directions As String) As XmlTextWriter
        writer.WriteStartElement("marker")
        writer.WriteAttributeString("lat", Y + ",")
        writer.WriteAttributeString("lng", X + ",")
        writer.WriteAttributeString("Unit_Id", Unit_id)
        writer.WriteAttributeString("Speed", speed)
        writer.WriteAttributeString("Directions", directions)
        writer.WriteAttributeString("Status", Status)
        writer.WriteEndElement()
        Return writer

    End Function
#End Region


#Region "GetPathHistory"

    <WebMethod()> _
        Public Function GetPathHistory(ByVal UnitId As String, ByVal DDATE As String, ByVal FHRS As String, ByVal FMINS As String, ByVal THRS As String, ByVal TMINS As String) As XmlDocument
        Dim returnvalue As XmlDocument
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DATE", DDATE)
        pParms(1) = New SqlClient.SqlParameter("@FHRS", FHRS)
        pParms(2) = New SqlClient.SqlParameter("@FMINS", FMINS)
        pParms(3) = New SqlClient.SqlParameter("@THRS", THRS)
        pParms(4) = New SqlClient.SqlParameter("@TMINS", TMINS)
        pParms(5) = New SqlClient.SqlParameter("@UNIT_ID", UnitId)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_GET_PATH_HISTORY", pParms)
        returnvalue = GenerateXMLForMap_GetPathHistory(ds)

        Return returnvalue

    End Function

    Public Function GenerateXMLForMap_GetPathHistory(ByVal ds As DataSet) As XmlDocument
        Dim data As String = ""
        Try
            Dim ms As New StringWriter
            Dim writer As New XmlTextWriter(ms)
            writer.Formatting = Formatting.None
            writer.Indentation = 6
            writer.WriteStartDocument()
            writer.WriteStartElement("markers")
            If ds.Tables(0).Rows.Count >= 1 Then
                Dim i = 0
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim flag = 0
                    Dim Tripid = "Trip1"
                    Dim NextTripId = ""
                    If (i = ds.Tables(0).Rows.Count - 1) Then
                        flag = 1
                    ElseIf i < ds.Tables(0).Rows.Count - 1 Then
                        NextTripId = Tripid ''ds.Tables(0).Rows(i + 1).Item("TRIP_ID").ToString()
                    Else
                        NextTripId = Tripid
                    End If


                    Dim Html As New StringBuilder

                    If Tripid <> NextTripId Or flag = 1 Then
                        Dim unitid = ds.Tables(0).Rows(i).Item("FK_UnitID").ToString()
                        Dim bsu = ds.Tables(0).Rows(i).Item("BSU_SHORTNAME").ToString()
                        Dim regid = ds.Tables(0).Rows(i).Item("VEH_REGNO").ToString()
                        Html.Append("Vehicle Number :" & regid)
                        Html.Append("<br>Driver :" & ds.Tables(0).Rows(i).Item("Driver").ToString())
                        Html.Append("<br>Mobile :" & ds.Tables(0).Rows(i).Item("DrivMob").ToString())
                        Html.Append("<br>Conductor :" & ds.Tables(0).Rows(i).Item("conductor").ToString())
                        Html.Append("<br>Mobile :" & ds.Tables(0).Rows(i).Item("cndmob").ToString())
                        Html.Append("<br>Bsu :" & bsu)
                        Html.Append("<br>Speed :" & ds.Tables(0).Rows(i).Item("Speed").ToString())
                        Html.Append("<br>Unit :" & unitid)

                        Dim val As String = "javascript:openPopUp('"
                        val = val & ds.Tables(0).Rows(i).Item("FK_UnitID").ToString()
                        val = val & "','" & ds.Tables(0).Rows(i).Item("Latitude").ToString()
                        val = val & "','" & ds.Tables(0).Rows(i).Item("Longitude").ToString()
                        val = val & "')"

                        Html.Append("<br><input id='Button1' onclick=" & val & "  type='button' value='More Info' />")
                    End If

                    WriteItem_GetPathHistory(writer, ds.Tables(0).Rows(i).Item("Latitude").ToString(), ds.Tables(0).Rows(i).Item("Longitude").ToString(), Html, "Trip1", ds.Tables(0).Rows(i).Item("FK_UnitID").ToString(), ds.Tables(0).Rows(i).Item("PositionLogDateTime").ToString(), ds.Tables(0).Rows(i).Item("Speed").ToString(), ds.Tables(0).Rows(i).Item("Direction").ToString())
                Next
            End If
            writer.WriteEndElement()
            writer.Flush()
            writer.Close()
            data = ms.ToString()
            Dim xmlDoc As New XmlDocument()
            xmlDoc.LoadXml(data)
            Return xmlDoc

        Catch ex As Exception
            'Throw ex
        Finally
            ds = Nothing
        End Try
    End Function

    Protected Function WriteItem_GetPathHistory(ByVal writer As XmlTextWriter, ByVal Y As String, ByVal X As String, ByVal des As StringBuilder, ByVal Trip_ID As String, ByVal Bus_ID As String, ByVal datetime As String, ByVal speed As String, ByVal directions As String) As XmlTextWriter
        writer.WriteStartElement("marker")
        writer.WriteAttributeString("lat", Y + ",")
        writer.WriteAttributeString("lng", X + ",")
        writer.WriteAttributeString("Date", Convert.ToDateTime(datetime).ToString("dd/MM/yyyy HH:mm:ss"))
        writer.WriteAttributeString("info", des.ToString())
        writer.WriteAttributeString("Speed", speed)
        writer.WriteAttributeString("Directions", directions)
        writer.WriteAttributeString("Trip_ID", Trip_ID)
        writer.WriteAttributeString("Bus_ID", Bus_ID)
        writer.WriteEndElement()
        Return writer
    End Function

#End Region


#Region "GetCurrentLocations"
    <WebMethod()> _
          Public Function GetCurrentLocations(ByVal UnitIds As String) As XmlDocument
        Dim returnvalue As XmlDocument
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@UNITS", UnitIds)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_VECHILE_CURRENT_LOCATION_FOR_GIVEN_UNITS", pParms)
        returnvalue = GenerateXMLForMap_GetCurrentLocations(ds)

        Return returnvalue

    End Function
    Public Function GenerateXMLForMap_GetCurrentLocations(ByVal ds As DataSet) As XmlDocument
        Dim data As String = ""
        Try
            Dim ms As New StringWriter
            Dim writer As New XmlTextWriter(ms)
            writer.Formatting = Formatting.None
            writer.Indentation = 6
            writer.WriteStartDocument()
            writer.WriteStartElement("markers")
            If ds.Tables(0).Rows.Count >= 1 Then
                Dim i = 0
                For i = 0 To ds.Tables(0).Rows.Count - 1

                    Dim lat = ds.Tables(0).Rows(i).Item("LATITUDE").ToString()
                    Dim lng = ds.Tables(0).Rows(i).Item("LONGITUDE").ToString()
                    Dim unitid = ds.Tables(0).Rows(i).Item("UNIT_ID").ToString()
                    Dim bsu = ds.Tables(0).Rows(i).Item("BSU_SHORTNAME").ToString()
                    Dim regid = ds.Tables(0).Rows(i).Item("VEH_REGNO").ToString()
                    Dim inout = status(unitid, lat, lng).ToString()

                    Dim Html As New StringBuilder
                    Html.Append("Vehicle Number :" & regid)
                    Html.Append("<br>Driver :" & ds.Tables(0).Rows(i).Item("Driver").ToString())
                    Html.Append("<br>Mobile :" & ds.Tables(0).Rows(i).Item("DrivMob").ToString())
                    Html.Append("<br>Conductor :" & ds.Tables(0).Rows(i).Item("conductor").ToString())
                    Html.Append("<br>Mobile :" & ds.Tables(0).Rows(i).Item("cndmob").ToString())
                    Html.Append("<br>Speed :" & ds.Tables(0).Rows(i).Item("Speed").ToString())
                    Html.Append("<br>Unit :" & unitid)
                    WriteItem_GetCurrentLocations(writer, lat, lng, unitid, inout, ds.Tables(0).Rows(i).Item("Speed").ToString(), ds.Tables(0).Rows(i).Item("Direction").ToString(), bsu, Html, regid)

                Next
            End If
            writer.WriteEndElement()
            writer.Flush()
            writer.Close()
            data = ms.ToString()
            Dim xmlDoc As New XmlDocument()
            xmlDoc.LoadXml(data)
            Return xmlDoc


        Catch ex As Exception
            'Throw ex
        Finally
            ds = Nothing
        End Try



    End Function

    Protected Function WriteItem_GetCurrentLocations(ByVal writer As XmlTextWriter, ByVal Y As String, ByVal X As String, ByVal Unit_id As String, ByVal Status As String, ByVal speed As String, ByVal directions As String, ByVal bsu As String, ByVal info As StringBuilder, ByVal regid As String) As XmlTextWriter
        writer.WriteStartElement("marker")
        writer.WriteAttributeString("lat", Y + ",")
        writer.WriteAttributeString("lng", X + ",")
        writer.WriteAttributeString("Unit_Id", Unit_id)
        writer.WriteAttributeString("Speed", speed)
        writer.WriteAttributeString("Directions", directions)
        writer.WriteAttributeString("info", info.ToString())
        writer.WriteAttributeString("Bsu", bsu)
        writer.WriteAttributeString("regid", regid)
        writer.WriteAttributeString("Bsu_regid", bsu & "<br>" & regid)
        writer.WriteAttributeString("Status", Status)
        writer.WriteEndElement()
        Return writer

    End Function
#End Region

#Region "V2"
    <WebMethod()> _
    Public Function V2_PathHistory(ByVal SearchString As String) As XmlDocument
        Dim ds As DataSet
        Dim Xdoc As New XmlDocument


        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@SEARCH_STRING", SearchString)
        pParms(1) = New SqlClient.SqlParameter("@OPTION", 1)

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "V2_GPS_MAP_PATH_HISTORY", pParms)
        Dim strXdoc As String
        strXdoc = ds.GetXml
        Xdoc.LoadXml(strXdoc)

        Return Xdoc


    End Function

    <WebMethod()> _
    Public Function V2_LiveTracking(ByVal SearchString As String) As XmlDocument
        Dim ds As DataSet
        Dim Xdoc As New XmlDocument


        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@SEARCH_STRING", SearchString)
        pParms(1) = New SqlClient.SqlParameter("@OPTION", 1)

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "V2_GPS_MAP_LIVE_TRACKING", pParms)
        Dim strXdoc As String
        strXdoc = ds.GetXml
        Xdoc.LoadXml(strXdoc)

        Return Xdoc


    End Function

#End Region

    Public Function status(ByVal Unitid As String, ByVal x As String, ByVal y As String) As Boolean
        Dim oddNodes As Boolean = True

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim sql_query = "select CONVERT(FLOAT,LATITUDE) LATITUDE,CONVERT(FLOAT,LONGITUDE) LONGITUDE from GEO_FENCING_POINTS where UNIT_ID='" & Unitid & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim polySides As Integer = 0
            Dim polyX As String()
            Dim polyY As String()

            Dim i = 0
            Dim valX = "", valY = ""
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If i = 0 Then
                    valX = ds.Tables(0).Rows(i).Item("LATITUDE").ToString()
                    valY = ds.Tables(0).Rows(i).Item("LONGITUDE").ToString()
                Else
                    valX = valX & "," & ds.Tables(0).Rows(i).Item("LATITUDE").ToString()
                    valY = valY & "," & ds.Tables(0).Rows(i).Item("LONGITUDE").ToString()
                End If

            Next

            polyX = valX.Split(",")
            polyY = valY.Split(",")
            oddNodes = CheckGEOFence(polyX, polyY, ds.Tables(0).Rows.Count, x, y)
        End If

        Return oddNodes

    End Function
    Public Function CheckGEOFence(ByVal PolyX As String(), ByVal PolyY As String(), ByVal polySides As Integer, ByVal X As String, ByVal Y As String) As Boolean
        Dim i As Integer, j As Integer = polySides - 1
        Dim oddNodes As Boolean = False

        For i = 0 To polySides - 1
            If PolyY(i) < Y AndAlso PolyY(j) >= Y OrElse PolyY(j) < Y AndAlso PolyY(i) >= Y Then
                If PolyX(i) + (Y - PolyY(i)) / (PolyY(j) - PolyY(i)) * (PolyX(j) - PolyX(i)) < X Then
                    oddNodes = Not oddNodes
                End If
            End If
            j = i
        Next

        Return oddNodes

    End Function


End Class
