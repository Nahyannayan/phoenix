Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports UtilityObj

Public Class VoucherFunctions

    Public Shared Function UnPost_voucher(ByVal p_guid As String, _
    ByVal p_doctype As String, ByVal p_USER As String, _
     ByVal p_Narration As String, ByRef p_unpost_docno As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim iReturnvalue As String = "1000"
            Try
                ''get header info
                Dim str_Sql As String
                str_Sql = "select * FROM JOURNAL_H where GUID='" & p_guid & "' "
                If p_doctype = "PP" Then
                    str_Sql = "SELECT GUID, " _
                         & " PRP_SUB_ID AS JHD_SUB_ID, " _
                         & " PRP_BSU_ID AS JHD_BSU_ID," _
                         & " PRP_DRNARRATION+' '+PRP_CRNARRATION " _
                         & " AS JHD_NARRATION , " _
                         & " PRP_bPosted AS JHD_bPOSTED," _
                         & " PRP_AMOUNT AS AMOUNT," _
                         & " PRP_ID AS JHD_DOCNO," _
                         & " PRP_DOCDT AS JHD_DOCDT," _
                         & " PRP_CUR_ID AS JHD_CUR_ID, PRP_FYEAR AS JHD_FYEAR" _
                         & " FROM PREPAYMENTS_H " _
                         & " WHERE GUID='" & p_guid & "'"
                End If
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then
                    ' ''@DOCNO = N'0709DN-00007',
                    ' ''		@DOCTYPE = N'DN',
                    ' ''		@VHH_BSU_ID = N'125016',
                    ' ''		@VHH_FYEAR = 2007,
                    ' ''		@Narration = N'VERUTHE',
                    ' ''		@User = N'ADMIN'
                    Dim cmd As SqlCommand
                    Dim str_bsuid, str_year As String
                    If p_doctype = "JV" Or p_doctype = "PJ" Or p_doctype = "PP" Or p_doctype = "IJV" Or p_doctype = "SJV" Then
                        cmd = New SqlCommand("UNPOSTTRANSACTION", objConn, stTrans)
                        str_bsuid = "@BSU_ID"
                        str_year = "@FYEAR"
                    Else
                        cmd = New SqlCommand("UNPOSTVOUCHER", objConn, stTrans)
                        str_bsuid = "@VHH_BSU_ID"
                        str_year = "@VHH_FYEAR"
                    End If
                    cmd.CommandType = CommandType.StoredProcedure
                    Dim sqlpDOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                    sqlpDOCNO.Value = ds.Tables(0).Rows(0)("JHD_DOCNO") & ""
                    cmd.Parameters.Add(sqlpDOCNO)
                    p_unpost_docno = ds.Tables(0).Rows(0)("JHD_DOCNO")
                    Dim sqlpsqlpVHH_BSU_ID As New SqlParameter(str_bsuid, SqlDbType.VarChar, 20)
                    sqlpsqlpVHH_BSU_ID.Value = ds.Tables(0).Rows(0)("JHD_BSU_ID") & ""
                    cmd.Parameters.Add(sqlpsqlpVHH_BSU_ID)

                    Dim sqlpJHD_FYEAR As New SqlParameter(str_year, SqlDbType.Int)
                    sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("JHD_FYEAR") & ""
                    cmd.Parameters.Add(sqlpJHD_FYEAR)

                    Dim sqlpDOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                    sqlpDOCTYPE.Value = p_doctype
                    cmd.Parameters.Add(sqlpDOCTYPE)

                    Dim sqlpNarration As New SqlParameter("@Narration", SqlDbType.VarChar, 20)
                    sqlpNarration.Value = p_Narration
                    cmd.Parameters.Add(sqlpNarration)

                    Dim sqlpUser As New SqlParameter("@User", SqlDbType.VarChar, 30)
                    sqlpUser.Value = p_USER
                    cmd.Parameters.Add(sqlpUser)

                    Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
                    retValParam.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(retValParam)

                    cmd.ExecuteNonQuery()
                    iReturnvalue = retValParam.Value
                    cmd.Parameters.Clear()
                    If (iReturnvalue = 0) Then
                        stTrans.Commit()
                    Else
                        stTrans.Rollback()
                    End If
                Else
                End If
                Return iReturnvalue
            Catch ex As Exception
                stTrans.Rollback()
                Errorlog(ex.Message)
                Return "1000"
            Finally
                objConn.Close() 'Finally, close the connection
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            Return "1000"
        End Try
    End Function

    Public Shared Function POSTSJOURNAL(ByVal p_Bsu_id As String, _
        ByVal p_Sub_id As String, ByVal p_year As String, _
        ByVal p_Docno As String, ByVal stTrans As SqlTransaction) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Try
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SHD_BSU_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = p_Bsu_id
            pParms(1) = New SqlClient.SqlParameter("@SHD_FYEAR", SqlDbType.Int)
            pParms(1).Value = p_year
            pParms(2) = New SqlClient.SqlParameter("@SHD_SUB_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = p_Sub_id
            pParms(3) = New SqlClient.SqlParameter("@SHD_DOCNO", SqlDbType.VarChar, 20)
            pParms(3).Value = p_Docno
            pParms(4) = New SqlClient.SqlParameter("@SHD_DOCTYPE", SqlDbType.VarChar, 20)
            pParms(4).Value = "SJV"
            pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "POSTSJOURNAL", pParms)
            If pParms(5).Value = "0" Then
                POSTSJOURNAL = pParms(5).Value
            Else
                POSTSJOURNAL = pParms(5).Value
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            POSTSJOURNAL = 1000
        Finally
        End Try
    End Function

    Public Shared Function SAVEOPENING_H(ByVal p_Bsu_id As String, _
        ByVal p_Sub_id As String, ByVal p_year As String, _
        ByVal p_Accountid As String, ByVal p_Drcr As String, ByVal p_Docdate As String, _
        ByVal p_Docno As String, ByVal p_Amount As Double, ByVal p_edit As Boolean, _
        ByVal stTrans As SqlTransaction, ByRef p_Newdocno As String, _
        ByVal P_SESSION As String, ByVal P_USER As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Try
            ''@OPH_BSU_ID = N'125016',
            ''@OPH_DOCYEAR = 2007,
            ''@OPH_SUB_ID='007',
            ''@OPH_DRCR = N'DR',
            Dim pParms(28) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@OPH_BSU_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = p_Bsu_id
            pParms(1) = New SqlClient.SqlParameter("@OPH_DOCYEAR", SqlDbType.Int)
            pParms(1).Value = p_year
            pParms(2) = New SqlClient.SqlParameter("@OPH_SUB_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = p_Sub_id
            pParms(3) = New SqlClient.SqlParameter("@OPH_DRCR", SqlDbType.VarChar, 50)
            pParms(3).Value = p_Drcr
            ''@OPH_DOCNO = N'12',		 
            ''@OPH_DOCDT = N'12/SEP/2007',
            ''@OPH_AMOUNT = 12.5,
            ''@OPH_ACT_ID = N'01201101',
            ''--@OPH_bPOSTED = 0,

            pParms(4) = New SqlClient.SqlParameter("@OPH_DOCNO", SqlDbType.VarChar, 20)
            pParms(4).Value = p_Newdocno
            pParms(5) = New SqlClient.SqlParameter("@OPH_DOCDT", SqlDbType.VarChar, 30)
            pParms(5).Value = p_Docdate
            pParms(6) = New SqlClient.SqlParameter("@OPH_AMOUNT", SqlDbType.Decimal, 21)
            pParms(6).Value = p_Amount
            pParms(7) = New SqlClient.SqlParameter("@OPH_ACT_ID", SqlDbType.VarChar, 20)
            pParms(7).Value = p_Accountid

            'pParms(8) = New SqlClient.SqlParameter("@OPH_bPOSTED", SqlDbType.Bit)
            'pParms(8).Value = False

            ''@bEdit = 0,
            ''@bGenerateNewNo = 1,
            ''@OPH_SESSIONID = N'123',
            ''@OPH_LOCK = N'TEST',
            ''@OPH_NEWDOCNO = @OPH_NEWDOCNO OUTPUT
            pParms(8) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
            pParms(8).Value = p_edit
            pParms(9) = New SqlClient.SqlParameter("@bGenerateNewNo", SqlDbType.Bit)
            pParms(9).Value = Not p_edit
            pParms(10) = New SqlClient.SqlParameter("@OPH_SESSIONID", SqlDbType.VarChar, 50)
            pParms(10).Value = P_SESSION
            pParms(11) = New SqlClient.SqlParameter("@OPH_LOCK", SqlDbType.VarChar, 20)
            pParms(11).Value = P_USER

            pParms(12) = New SqlClient.SqlParameter("@OPH_NEWDOCNO", SqlDbType.VarChar, 20)
            pParms(12).Direction = ParameterDirection.Output

            pParms(13) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(13).Direction = ParameterDirection.ReturnValue
            'Dim stTrans As SqlTransaction = objConn.BeginTransaction

            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SAVEOPENING_H", pParms)
            If pParms(13).Value = "0" Then
                p_Newdocno = pParms(12).Value & ""
                SAVEOPENING_H = 0
            Else
                SAVEOPENING_H = pParms(13).Value
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            SAVEOPENING_H = 1000
        Finally
        End Try
    End Function

    Public Shared Function SAVEOPENING_D(ByVal p_guid As Object, _
        ByVal p_Bsu_id As String, _
        ByVal p_Sub_id As String, ByVal p_year As String, _
         ByVal p_HDocno As String, ByVal p_Docdate As String, _
        ByVal p_DDocno As String, ByVal p_DocTYPE As String, ByVal p_Amount As Double, ByVal p_edit As Boolean, _
        ByVal stTrans As SqlTransaction, ByVal p_Last As Boolean) As String
        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Try
            '@OPH_SUB_ID = N'007',
            '@OPD_DOCNO = N'07CP007',
            '@OPD_BSU_ID = N'125016',
            '@OPH_DOCNO = N'07OP-0000003',
            Dim pParms(28) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@OPH_SUB_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = p_Sub_id
            pParms(1) = New SqlClient.SqlParameter("@OPD_DOCNO", SqlDbType.VarChar, 20)
            pParms(1).Value = p_DDocno
            pParms(2) = New SqlClient.SqlParameter("@OPD_BSU_ID", SqlDbType.VarChar, 50)
            pParms(2).Value = p_Bsu_id
            pParms(3) = New SqlClient.SqlParameter("@OPH_DOCNO", SqlDbType.VarChar, 20)
            pParms(3).Value = p_HDocno
            '@OPD_DOCTYPE = N'CP',
            '@OPH_DOCYEAR = 2007,
            '@OPD_DOCDT = N'12/SEP/2007',
            '@bLastRec = 0,
            pParms(4) = New SqlClient.SqlParameter("@OPD_DOCTYPE", SqlDbType.VarChar, 20)
            pParms(4).Value = p_DocTYPE
            pParms(5) = New SqlClient.SqlParameter("@OPH_DOCYEAR", SqlDbType.Int)
            pParms(5).Value = p_year
            pParms(6) = New SqlClient.SqlParameter("@OPD_DOCDT", SqlDbType.DateTime)
            pParms(6).Value = p_Docdate
            pParms(7) = New SqlClient.SqlParameter("@bLastRec", SqlDbType.Bit)
            pParms(7).Value = p_Last
            '@OPD_AMOUNT = 13.5,
            '@bEdit = 0,
            pParms(8) = New SqlClient.SqlParameter("@OPD_AMOUNT", SqlDbType.Decimal, 21)
            pParms(8).Value = p_Amount
            pParms(9) = New SqlClient.SqlParameter("@bEdit", SqlDbType.Bit)
            pParms(9).Value = p_edit
            pParms(10) = New SqlClient.SqlParameter("@guid", SqlDbType.UniqueIdentifier)
            pParms(10).Value = p_guid
            pParms(11) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(11).Direction = ParameterDirection.ReturnValue
            'Dim stTrans As SqlTransaction = objConn.BeginTransaction
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SAVEOPENING_D", pParms)
            If pParms(11).Value = "0" Then
                SAVEOPENING_D = 0
            Else
                SAVEOPENING_D = pParms(11).Value
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            SAVEOPENING_D = 1000
        Finally
        End Try
    End Function

    Public Shared Function DeleteVOUCHER(ByVal p_Type As String, ByVal p_bsuid As String, ByVal p_subid As String, ByVal p_User As String, ByVal p_guid As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As New DataSet
        Dim objConn As New SqlConnection(str_conn)
        Dim str_Sql As String
        str_Sql = "SELECT * FROM VOUCHER_H WHERE" _
               & " GUID='" & p_guid & "'"
        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Dim cmd As New SqlCommand("DeleteVOUCHER", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpDOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                sqlpDOCNO.Value = ds.Tables(0).Rows(0)("VHH_DOCNO") & ""
                cmd.Parameters.Add(sqlpDOCNO)
                '@DOCNO	varchar(20),
                '@DOCTYPE	varchar(20) ,
                '@VHH_BSU_ID	varchar(10), 
                '@VHH_FYEAR	int,
                '@VHH_SUB_ID varchar(20),
                '@VHH_LOCK varc
                Dim sqlpDOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                sqlpDOCTYPE.Value = p_Type
                cmd.Parameters.Add(sqlpDOCTYPE)

                Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@VHH_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpJHD_BSU_ID.Value = p_bsuid
                cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                Dim sqlpJHD_FYEAR As New SqlParameter("@VHH_FYEAR", SqlDbType.Int)
                sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("VHH_FYEAR") & ""
                cmd.Parameters.Add(sqlpJHD_FYEAR)

                Dim sqlpVHH_SUB_ID As New SqlParameter("@VHH_SUB_ID", SqlDbType.VarChar, 20)
                sqlpVHH_SUB_ID.Value = p_subid
                cmd.Parameters.Add(sqlpVHH_SUB_ID)

                Dim sqlpVHH_LOCK As New SqlParameter("@VHH_LOCK", SqlDbType.VarChar, 30)
                sqlpVHH_LOCK.Value = p_User
                cmd.Parameters.Add(sqlpVHH_LOCK)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                If iReturnvalue <> 0 Then
                    stTrans.Rollback()
                Else
                    stTrans.Commit()
                End If
                Return iReturnvalue
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return "1000"
        Finally
            objConn.Close()
        End Try
        Return "1000"
    End Function

    Public Shared Function DeleteRJV(ByVal p_Type As String, ByVal p_bsuid As String, ByVal p_guid As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim ds As New DataSet
        Dim objConn As New SqlConnection(str_conn)
        Dim str_Sql As String
        str_Sql = "SELECT * FROM RJOURNAL_H WHERE" _
              & " GUID='" & p_guid & "'"
        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Dim cmd As New SqlCommand("DeleteRJOURNAL_H", objConn, stTrans)
                cmd.CommandType = CommandType.StoredProcedure
                '@return_value = [dbo].[DeleteRJOURNAL_H]
                '		@DOCNO = N'0710RJV-0001',
                '		@DOCTYPE = N'RJV',
                '		@RJH_SUB_ID = N'007',
                '		@RJH_BSU_ID = N'XXXXXX',
                '		@RJH_FYEAR = 2007 
                Dim sqlpDOCNO As New SqlParameter("@DOCNO", SqlDbType.VarChar, 20)
                sqlpDOCNO.Value = ds.Tables(0).Rows(0)("RJH_DOCNO") & ""
                cmd.Parameters.Add(sqlpDOCNO)

                Dim sqlpDOCTYPE As New SqlParameter("@DOCTYPE", SqlDbType.VarChar, 20)
                sqlpDOCTYPE.Value = p_Type
                cmd.Parameters.Add(sqlpDOCTYPE)

                Dim sqlpRJH_SUB_ID As New SqlParameter("@RJH_SUB_ID", SqlDbType.VarChar, 20)
                sqlpRJH_SUB_ID.Value = ds.Tables(0).Rows(0)("RJH_SUB_ID") & ""
                cmd.Parameters.Add(sqlpRJH_SUB_ID)

                Dim sqlpsqlpJHD_BSU_ID As New SqlParameter("@RJH_BSU_ID", SqlDbType.VarChar, 20)
                sqlpsqlpJHD_BSU_ID.Value = p_bsuid
                cmd.Parameters.Add(sqlpsqlpJHD_BSU_ID)

                Dim sqlpJHD_FYEAR As New SqlParameter("@RJH_FYEAR", SqlDbType.Int)
                sqlpJHD_FYEAR.Value = ds.Tables(0).Rows(0)("RJH_FYEAR") & ""
                cmd.Parameters.Add(sqlpJHD_FYEAR)

                Dim iReturnvalue As Integer
                Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int, 8)
                retValParam.Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add(retValParam)
                cmd.ExecuteNonQuery()
                iReturnvalue = retValParam.Value
                If iReturnvalue <> 0 Then
                    stTrans.Rollback()
                Else
                    stTrans.Commit()
                End If
                Return iReturnvalue
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return "1000"
        Finally
            objConn.Close()
        End Try
        Return "1000"
    End Function

    Public Shared Function get_Voucher_Guid(ByVal p_Docno As String, ByVal p_bsuid As String, ByVal p_subid As String, ByVal p_doctype As String, ByVal p_year As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String

            str_Sql = "SELECT GUID FROM VOUCHER_H" _
                          & " WHERE VHH_BSU_ID='" & p_bsuid & "'" _
                          & " AND VHH_SUB_ID='" & p_subid & "'" _
                          & " AND VHH_DOCNO='" & p_Docno & "'" _
                          & " AND VHH_DOCTYPE='" & p_doctype & "'" _
                          & " AND VHH_FYEAR='" & p_year & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("GUID").ToString
            Else
                Return ""
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Return ""
        End Try
    End Function

    Public Shared Function SavePREPAYMENTS_H(ByVal PRP_ID As String, ByVal PRP_sub_ID As String, ByVal PRP_BSU_ID As String, _
     ByVal PRP_FYEAR As Integer, ByVal PRP_PARTY As String, ByVal PRP_PREPAYACC As String, ByVal PRP_EXPENSEACC As String, _
     ByVal PRP_DOCDT As String, ByVal PRP_FTFROM As String, ByVal PRP_DTTO As String, ByVal PRP_AMOUNT As Double, _
     ByVal PRP_INVOICENO As String, ByVal PRP_NOOFINST As Integer, ByVal PRP_CUR_ID As String, _
     ByVal PRP_EXGRATE1 As Double, ByVal PRP_EXGRATE2 As Double, ByVal PRP_JVNO As String, _
     ByVal PRP_CUT_ID As Integer, ByVal PRP_CRNARRATION As String, ByVal PRP_DRNARRATION As String, _
     ByVal PRP_LPO As String, ByVal bEdit As Boolean, ByVal bGenerateNewNo As Boolean, _
     ByRef PRP_NEWDOCNO As String, ByVal PRP_REFDOCNO As String, ByVal PRP_REFTYPE As String, _
     ByVal PRP_REFDOCID As String, ByVal PRP_TAX_CODE As String, ByVal trans As SqlTransaction) As Integer

        Dim pParms(29) As SqlClient.SqlParameter
        '		@PRP_PREPAYACC = N'sbvdfr432',
        '		@PRP_EXPENSEACC = N'ewqr321',
        '		@PRP_DOCDT = N'09/09/2007',
        '		@PRP_FTFROM = N'09/09/2007',
        '		@PRP_DTTO = N'02/02/2008',
        '		@PRP_AMOUNT = 965,
        '		@PRP_INVOICENO = N'sadfd',
        '		@PRP_NOOFINST = 7,
        '		@PRP_CUR_ID = N'DHS',
        '		@PRP_EXGRATE1 = 1,
        '		@PRP_EXGRATE2 = 1,
        '		@PRP_JVNO = N'sfdasdf',
        '		@PRP_CUT_ID = 4,
        '		@bEdit = 0,
        '		@bGenerateNewNo = 0,
        '		@PRP_CRNARRATION = N'sdfdsf',
        '		@PRP_DRNARRATION = N'sdfdsf',
        '		@PRP_LPO = N'dsafdsf',
        '		@PRP_NEWDOCNO = @PRP_NEWDOCNO OUTPUT
        '       @PRP_ID = N'321213',
        '		@PRP_sub_ID = N'wqe',
        '		@PRP_BSU_ID = N'126008',
        '		@PRP_FYEAR = 2007,
        '		@PRP_PARTY = N'Se21',
        pParms(0) = New SqlClient.SqlParameter("@PRP_ID", PRP_ID)
        pParms(1) = New SqlClient.SqlParameter("@PRP_sub_ID", PRP_sub_ID)
        pParms(2) = New SqlClient.SqlParameter("@PRP_BSU_ID", PRP_BSU_ID)

        pParms(3) = New SqlClient.SqlParameter("@PRP_FYEAR", PRP_FYEAR)
        pParms(4) = New SqlClient.SqlParameter("@PRP_PARTY", PRP_PARTY)
        pParms(5) = New SqlClient.SqlParameter("@PRP_PREPAYACC", PRP_PREPAYACC)
        pParms(6) = New SqlClient.SqlParameter("@PRP_EXPENSEACC", PRP_EXPENSEACC)
        pParms(7) = New SqlClient.SqlParameter("@PRP_DOCDT", PRP_DOCDT)
        pParms(8) = New SqlClient.SqlParameter("@PRP_FTFROM", PRP_FTFROM)

        pParms(9) = New SqlClient.SqlParameter("@PRP_DTTO", PRP_DTTO)
        pParms(10) = New SqlClient.SqlParameter("@PRP_AMOUNT", PRP_AMOUNT)
        pParms(11) = New SqlClient.SqlParameter("@PRP_INVOICENO", PRP_INVOICENO)
        pParms(12) = New SqlClient.SqlParameter("@PRP_NOOFINST", PRP_NOOFINST)
        pParms(13) = New SqlClient.SqlParameter("@PRP_CUR_ID", PRP_CUR_ID)
        pParms(14) = New SqlClient.SqlParameter("@PRP_EXGRATE1", PRP_EXGRATE1)


        pParms(15) = New SqlClient.SqlParameter("@PRP_EXGRATE2", PRP_EXGRATE2)
        pParms(16) = New SqlClient.SqlParameter("@PRP_JVNO", PRP_JVNO)
        pParms(17) = New SqlClient.SqlParameter("@PRP_CUT_ID", PRP_CUT_ID)
        pParms(18) = New SqlClient.SqlParameter("@PRP_CRNARRATION", PRP_CRNARRATION)
        pParms(19) = New SqlClient.SqlParameter("@PRP_DRNARRATION", PRP_DRNARRATION)
        pParms(20) = New SqlClient.SqlParameter("@PRP_LPO", PRP_LPO)

        pParms(21) = New SqlClient.SqlParameter("@bEdit", bEdit)
        pParms(22) = New SqlClient.SqlParameter("@bGenerateNewNo", bGenerateNewNo)
        pParms(23) = New SqlClient.SqlParameter("@PRP_NEWDOCNO", SqlDbType.VarChar, 20)
        pParms(23).Direction = ParameterDirection.Output

        pParms(24) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(24).Direction = ParameterDirection.ReturnValue

        pParms(25) = New SqlClient.SqlParameter("@PRP_REFDOCNO", PRP_REFDOCNO)
        pParms(26) = New SqlClient.SqlParameter("@PRP_REFTYPE", PRP_REFTYPE)
        pParms(27) = New SqlClient.SqlParameter("@PRP_REFDOCID", PRP_REFDOCID)
        'pParms(28) = New SqlClient.SqlParameter("@PRP_TAX_CODE", PRP_TAX_CODE)
        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SavePREPAYMENTS_H", pParms)
        PRP_NEWDOCNO = pParms(23).Value.ToString()

        Dim ReturnFlag As Integer = pParms(24).Value
        Return ReturnFlag

    End Function

    Public Shared Function SavePREPAYMENTS_D(ByVal GUID As Guid, ByVal PRD_ID As Integer, ByVal PRD_PRP_ID As String, _
    ByVal PRD_BSU_ID As String, ByVal PRP_FYEAR As Integer, ByVal PRD_DOCDT As String, _
    ByVal PRP_ALLOCAMT As Double, ByVal PRP_JVNO As String, ByVal bEdit As Boolean, _
    ByVal trans As SqlTransaction, Optional ByVal EXPENSEACC As String = "") As Integer
        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@GUID", GUID)
        pParms(1) = New SqlClient.SqlParameter("@PRD_ID", PRD_ID)
        pParms(2) = New SqlClient.SqlParameter("@PRD_PRP_ID", PRD_PRP_ID)

        pParms(3) = New SqlClient.SqlParameter("@PRD_BSU_ID", PRD_BSU_ID)
        pParms(4) = New SqlClient.SqlParameter("@PRP_FYEAR", PRP_FYEAR)
        pParms(5) = New SqlClient.SqlParameter("@PRD_DOCDT", PRD_DOCDT)

        pParms(6) = New SqlClient.SqlParameter("@PRP_ALLOCAMT", PRP_ALLOCAMT)
        pParms(7) = New SqlClient.SqlParameter("@PRP_JVNO", PRP_JVNO)
        pParms(8) = New SqlClient.SqlParameter("@bEdit", bEdit)
        pParms(9) = New SqlClient.SqlParameter("@PRD_EXPENSEACC", EXPENSEACC)

        pParms(10) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SavePREPAYMENTS_D_New", pParms)
        Return pParms(10).Value
    End Function

    Public Shared Function DeletePREPAYMENTS_D(ByVal PRD_PRP_ID As String, ByVal PRD_BSU_ID As String, ByVal PRP_FYEAR As Integer, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISFinConnection()
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@PRD_PRP_ID", PRD_PRP_ID)
            pParms(1) = New SqlClient.SqlParameter("@PRD_BSU_ID", PRD_BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@PRP_FYEAR", PRP_FYEAR)
            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeletePREPAYMENTS_D", pParms)
            Dim ReturnFlag As Integer = pParms(3).Value
            Return ReturnFlag
            SqlConnection.ClearPool(connection)
        End Using
    End Function

    Public Shared Function DeletePREPAYMENTS_H(ByVal PRP_ID As String, ByVal PRP_SUB_ID As String, ByVal PRP_BSU_ID As String, ByVal PRP_FYEAR As Integer, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISFinConnection()
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@PRP_ID", PRP_ID)
            pParms(1) = New SqlClient.SqlParameter("@PRP_SUB_ID", PRP_SUB_ID)
            pParms(2) = New SqlClient.SqlParameter("@PRP_BSU_ID", PRP_BSU_ID)
            pParms(3) = New SqlClient.SqlParameter("@PRP_FYEAR", PRP_FYEAR)

            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeletePREPAYMENTS_H", pParms)
            Dim ReturnFlag As Integer = pParms(4).Value
            Return ReturnFlag
            SqlConnection.ClearPool(connection)
        End Using
    End Function

    Public Shared Function UpdateVOUCHER_D_SUB(ByVal VSB_VHD_ID As String, ByVal VSB_PRP_ID As String, _
    ByVal VSB_bPosted As Boolean, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISFinConnection
            Dim pParms(4) As SqlClient.SqlParameter
            'UpdateVOUCHER_D_SUB] 
            '@VSB_VHD_ID bigint,  
            '@VSB_PRP_ID varchar(20),
            '@VSB_bPosted bit 
            pParms(0) = New SqlClient.SqlParameter("@VSB_VHD_ID", VSB_VHD_ID)
            pParms(1) = New SqlClient.SqlParameter("@VSB_PRP_ID", VSB_PRP_ID)
            pParms(2) = New SqlClient.SqlParameter("@VSB_bPosted", VSB_bPosted)
            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "UpdateVOUCHER_D_SUB", pParms)
            Dim ReturnFlag As Integer = pParms(3).Value
            Return ReturnFlag
        End Using

    End Function

    Public Shared Function GetPrepaymentDetail_Guid(ByVal viewid As String) As SqlDataReader
        Dim sqlPrePaymentDocID As String = "SELECT  PREPAYMENTS_H.Guid as Guid,PREPAYMENTS_H.PRP_ID as DocNo,PREPAYMENTS_H.PRP_SUB_ID as subID,PREPAYMENTS_H.PRP_FYEAR as Fyear,PREPAYMENTS_H.PRP_BSU_ID as BSU_ID, PREPAYMENTS_H.PRP_PARTY as PRP_PARTY, ACCOUNTS_M.ACT_NAME as PARTY_NAME, PREPAYMENTS_H.PRP_CUT_ID as CostID, COSTUNIT_M.CUT_DESCR as CostDescr, " & _
            " PREPAYMENTS_H.PRP_PREPAYACC as PreAcc, ACCOUNTS_M_1.ACT_NAME AS PreAccDescr, PREPAYMENTS_H.PRP_EXPENSEACC as ExpAcc, " & _
            " ACCOUNTS_M_3.ACT_NAME AS ExpDescr,PREPAYMENTS_H.PRP_DOCDT As DOCDT, PREPAYMENTS_H.PRP_FTFROM as FTFrom, " & _
            " PREPAYMENTS_H.PRP_DTTO as FTto, PREPAYMENTS_H.PRP_AMOUNT as TAmt, PREPAYMENTS_H.PRP_INVOICENO as InVoice, PREPAYMENTS_H.PRP_JVNO as JvNo, " & _
            " PREPAYMENTS_H.PRP_CRNARRATION as NR1, PREPAYMENTS_H.PRP_DRNARRATION as NR2, PREPAYMENTS_H.PRP_LPO as LPO, PREPAYMENTS_H.PRP_REFDOCNO , PREPAYMENTS_H.PRP_REFTYPE" & _
            " FROM  PREPAYMENTS_H left JOIN ACCOUNTS_M " & _
            " ON ACCOUNTS_M.ACT_ID = PREPAYMENTS_H.PRP_PARTY INNER JOIN " & _
            " COSTUNIT_M ON PREPAYMENTS_H.PRP_CUT_ID = COSTUNIT_M.CUT_ID INNER JOIN " & _
            " ACCOUNTS_M AS ACCOUNTS_M_1 ON " & _
            " PREPAYMENTS_H.PRP_PREPAYACC = ACCOUNTS_M_1.ACT_ID INNER JOIN " & _
            " ACCOUNTS_M AS ACCOUNTS_M_2 ON PRP_PREPAYACC = ACCOUNTS_M_2.ACT_ID INNER JOIN " & _
            " ACCOUNTS_M AS ACCOUNTS_M_3 ON PREPAYMENTS_H.PRP_EXPENSEACC = ACCOUNTS_M_3.ACT_ID " & _
            " WHERE PREPAYMENTS_H.Guid = '" & viewid & "'" ', PREPAYMENTS_H.PRP_TAX_CODE
        Dim connection As SqlConnection = ConnectionManger.GetOASISFinConnection
        Dim command As SqlCommand = New SqlCommand(sqlPrePaymentDocID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetPrepaymentDetailDatatable(ByVal viewid As String) As DataTable
        Dim ds As New DataSet
        Dim table As New DataTable
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String = "SELECT  PREPAYMENTS_H.Guid as Guid,PREPAYMENTS_H.PRP_ID as DocNo,PREPAYMENTS_H.PRP_SUB_ID as subID,PREPAYMENTS_H.PRP_FYEAR as Fyear,PREPAYMENTS_H.PRP_BSU_ID as BSU_ID, PREPAYMENTS_H.PRP_PARTY as PRP_PARTY, ACCOUNTS_M.ACT_NAME as PARTY_NAME, PREPAYMENTS_H.PRP_CUT_ID as CostID, COSTUNIT_M.CUT_DESCR as CostDescr, " & _
                " PREPAYMENTS_H.PRP_PREPAYACC as PreAcc, ACCOUNTS_M_1.ACT_NAME AS PreAccDescr, PREPAYMENTS_H.PRP_EXPENSEACC as ExpAcc, " & _
                " ACCOUNTS_M_3.ACT_NAME AS ExpDescr,PREPAYMENTS_H.PRP_DOCDT As DOCDT, PREPAYMENTS_H.PRP_FTFROM as FTFrom, " & _
                " PREPAYMENTS_H.PRP_DTTO as FTto, PREPAYMENTS_H.PRP_AMOUNT as TAmt, PREPAYMENTS_H.PRP_INVOICENO as InVoice, PREPAYMENTS_H.PRP_JVNO as JvNo, " & _
                " PREPAYMENTS_H.PRP_CRNARRATION as NR1, PREPAYMENTS_H.PRP_DRNARRATION as NR2, PREPAYMENTS_H.PRP_LPO as LPO, PREPAYMENTS_H.PRP_REFDOCNO , PREPAYMENTS_H.PRP_REFTYPE," & _
                " PREPAYMENTS_H.PRP_SUB_ID, PREPAYMENTS_H.PRP_FYEAR, PREPAYMENTS_H.PRP_BSU_ID " & _
                " FROM  PREPAYMENTS_H left JOIN ACCOUNTS_M " & _
                " ON ACCOUNTS_M.ACT_ID = PREPAYMENTS_H.PRP_PARTY INNER JOIN " & _
                " COSTUNIT_M ON PREPAYMENTS_H.PRP_CUT_ID = COSTUNIT_M.CUT_ID INNER JOIN " & _
                " ACCOUNTS_M AS ACCOUNTS_M_1 ON " & _
                " PREPAYMENTS_H.PRP_PREPAYACC = ACCOUNTS_M_1.ACT_ID INNER JOIN " & _
                " ACCOUNTS_M AS ACCOUNTS_M_2 ON PRP_PREPAYACC = ACCOUNTS_M_2.ACT_ID INNER JOIN " & _
                " ACCOUNTS_M AS ACCOUNTS_M_3 ON PREPAYMENTS_H.PRP_EXPENSEACC = ACCOUNTS_M_3.ACT_ID " & _
                " WHERE PREPAYMENTS_H.Guid = '" & viewid & "'"


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            table = ds.Tables(0)
        Catch ex As Exception
            Errorlog(ex.Message)

        End Try
        Return table

    End Function

End Class
