﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Web
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Public Class EmpServiceRequest
#Region "PublicProperties1"
    Private pGSR_ID As Integer
    Public Property GSR_ID() As Integer
        Get
            Return pGSR_ID
        End Get
        Set(ByVal value As Integer)
            pGSR_ID = value
        End Set
    End Property

    Private pGSR_NO As String
    Public Property GSR_NO() As String
        Get
            Return pGSR_NO
        End Get
        Set(ByVal value As String)
            pGSR_NO = value
        End Set
    End Property

    Private pGSR_DATE As DateTime
    Public Property GSR_DATE() As DateTime
        Get
            Return pGSR_DATE
        End Get
        Set(ByVal value As DateTime)
            pGSR_DATE = value
        End Set
    End Property

    Private pGSR_EMP_ID As Integer
    Public Property GSR_EMP_ID() As Integer
        Get
            Return pGSR_EMP_ID
        End Get
        Set(ByVal value As Integer)
            pGSR_EMP_ID = value
        End Set
    End Property

    Private pGSR_BSU_ID As String
    Public Property GSR_BSU_ID() As String
        Get
            Return pGSR_BSU_ID
        End Get
        Set(ByVal value As String)
            pGSR_BSU_ID = value
        End Set
    End Property

    Private pGSR_TYPE As String
    Public Property GSR_TYPE() As String
        Get
            Return pGSR_TYPE
        End Get
        Set(ByVal value As String)
            pGSR_TYPE = value
        End Set
    End Property

    Private pGSR_STATUS As String
    Public Property GSR_STATUS() As String
        Get
            Return pGSR_STATUS
        End Get
        Set(ByVal value As String)
            pGSR_STATUS = value
        End Set
    End Property

    Private pGSR_REMARKS As String
    Public Property GSR_REMARKS() As String
        Get
            Return pGSR_REMARKS
        End Get
        Set(ByVal value As String)
            pGSR_REMARKS = value
        End Set
    End Property

    Private pGSR_APPR_EMP_ID As Integer
    Public Property GSR_APPR_EMP_ID() As Integer
        Get
            Return pGSR_APPR_EMP_ID
        End Get
        Set(ByVal value As Integer)
            pGSR_APPR_EMP_ID = value
        End Set
    End Property

    Private pGSR_APPR_DATE As DateTime
    Public Property GSR_APPR_DATE() As DateTime
        Get
            Return pGSR_APPR_DATE
        End Get
        Set(ByVal value As DateTime)
            pGSR_APPR_DATE = value
        End Set
    End Property

    Private pGSR_bDeleted As Boolean
    Public Property GSR_bDeleted() As Boolean
        Get
            Return pGSR_bDeleted
        End Get
        Set(ByVal value As Boolean)
            pGSR_bDeleted = value
        End Set
    End Property

    Private pPROCESS As String
    Public Property PROCESS() As String
        Get
            Return pPROCESS
        End Get
        Set(ByVal value As String)
            pPROCESS = value
        End Set
    End Property

#Region "Detail Table"
    Private pGSD_ID As Integer
    Public Property GSD_ID() As Integer
        Get
            Return pGSD_ID
        End Get
        Set(ByVal value As Integer)
            pGSD_ID = value
        End Set
    End Property

    Private pGSD_GSR_ID As Integer
    Public Property GSD_GSR_ID() As Integer
        Get
            Return pGSD_GSR_ID
        End Get
        Set(ByVal value As Integer)
            pGSD_GSR_ID = value
        End Set
    End Property

    Private pGSD_SRV_DESCR As String
    Public Property GSD_SRV_DESCR() As String
        Get
            Return pGSD_SRV_DESCR
        End Get
        Set(ByVal value As String)
            pGSD_SRV_DESCR = value
        End Set
    End Property

    Private pGSD_YesORNo As String
    Public Property GSD_YesORNo() As String
        Get
            Return pGSD_YesORNo
        End Get
        Set(ByVal value As String)
            pGSD_YesORNo = value
        End Set
    End Property
#End Region
#End Region

    Public Function SAVE_SERVICE_REQUEST_H(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Integer
        SAVE_SERVICE_REQUEST_H = 0
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("HR.SAVE_GEN_SERVICE_REQUEST_H", objCon, TRANS)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpGSR_DATE As New SqlParameter("@GSR_DATE", SqlDbType.VarChar)
            sqlpGSR_DATE.Value = GSR_DATE
            cmd.Parameters.Add(sqlpGSR_DATE)

            Dim sqlpGSR_EMP_ID As New SqlParameter("@GSR_EMP_ID", SqlDbType.Int)
            sqlpGSR_EMP_ID.Value = GSR_EMP_ID
            cmd.Parameters.Add(sqlpGSR_EMP_ID)

            Dim sqlpGSR_BSU_ID As New SqlParameter("@GSR_BSU_ID", SqlDbType.VarChar, 10)
            sqlpGSR_BSU_ID.Value = GSR_BSU_ID
            cmd.Parameters.Add(sqlpGSR_BSU_ID)

            Dim sqlpGSR_TYPE As New SqlParameter("@GSR_TYPE", SqlDbType.VarChar)
            sqlpGSR_TYPE.Value = GSR_TYPE
            cmd.Parameters.Add(sqlpGSR_TYPE)

            Dim sqlpGSR_STATUS As New SqlParameter("@GSR_STATUS", SqlDbType.VarChar)
            sqlpGSR_STATUS.Value = GSR_STATUS
            cmd.Parameters.Add(sqlpGSR_STATUS)

            Dim sqlpGSR_REMARKS As New SqlParameter("@GSR_REMARKS", SqlDbType.VarChar, 300)
            sqlpGSR_REMARKS.Value = GSR_REMARKS
            cmd.Parameters.Add(sqlpGSR_REMARKS)

            If GSR_ID <> 0 And (PROCESS = "APPROVE" Or PROCESS = "REJECT") Then
                Dim sqlpGSR_APPR_EMP_ID As New SqlParameter("@GSR_APPR_EMP_ID", SqlDbType.Int)
                sqlpGSR_APPR_EMP_ID.Value = GSR_APPR_EMP_ID
                cmd.Parameters.Add(sqlpGSR_APPR_EMP_ID)

                Dim sqlpGSR_APPR_DATE As New SqlParameter("@GSR_APPR_DATE", SqlDbType.DateTime)
                sqlpGSR_APPR_DATE.Value = DateTime.Now
                cmd.Parameters.Add(sqlpGSR_APPR_DATE)
            End If

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            Dim sqlpGSR_ID As New SqlParameter("@GSR_ID", SqlDbType.Int)
            sqlpGSR_ID.Direction = ParameterDirection.InputOutput
            sqlpGSR_ID.Value = GSR_ID
            cmd.Parameters.Add(sqlpGSR_ID)


            cmd.Parameters.AddWithValue("@PROCESS", PROCESS)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()

            GSR_ID = sqlpGSR_ID.Value
            SAVE_SERVICE_REQUEST_H = retSValParam.Value

        Catch ex As Exception
            SAVE_SERVICE_REQUEST_H = 1
            'TRANS.Rollback()
        End Try
    End Function

    Public Function SAVE_SERVICE_REQUEST_D(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction) As Integer
        SAVE_SERVICE_REQUEST_D = 0
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("HR.SAVE_GEN_SERVICE_REQUEST_D", objCon, TRANS)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpGSD_GSR_ID As New SqlParameter("@GSR_ID", SqlDbType.Int)
            sqlpGSD_GSR_ID.Value = GSR_ID
            cmd.Parameters.Add(sqlpGSD_GSR_ID)

            Dim sqlpGSD_SRV_DESCR As New SqlParameter("@GSD_SRV_DESCR", SqlDbType.VarChar, 10)
            sqlpGSD_SRV_DESCR.Value = GSD_SRV_DESCR
            cmd.Parameters.Add(sqlpGSD_SRV_DESCR)

            Dim sqlpGSD_YesORNo As New SqlParameter("@GSD_YesORNo", SqlDbType.VarChar, 1)
            sqlpGSD_YesORNo.Value = GSD_YesORNo
            cmd.Parameters.Add(sqlpGSD_YesORNo)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()

            SAVE_SERVICE_REQUEST_D = retSValParam.Value
        Catch ex As Exception
            SAVE_SERVICE_REQUEST_D = 1
        End Try
    End Function

    Public Sub GET_SERVICE_REQUEST_H()
        Dim strCon = ConnectionManger.GetOASISConnectionString

        Dim QRY As String = "SELECT  GSR_DATE ,GSR_EMP_ID ,GSR_BSU_ID ,GSR_TYPE ,GSR_STATUS ,GSR_REMARKS ," & _
            "ISNULL(GSR_APPR_EMP_ID,0)GSR_APPR_EMP_ID ,ISNULL(GSR_APPR_DATE,GETDATE())GSR_APPR_DATE ,GSR_bDeleted FROM HR.EMP_GEN_SERVICE_REQUEST_H WITH(NOLOCK) WHERE " & _
            "GSR_ID=" & GSR_ID & ""

        Dim ds As DataSet = SqlHelper.ExecuteDataset(strCon, CommandType.Text, QRY)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            GSR_DATE = Format(ds.Tables(0).Rows(0)("GSR_DATE"), OASISConstants.DataBaseDateFormat)
            GSR_EMP_ID = ds.Tables(0).Rows(0)("GSR_EMP_ID").ToString
            GSR_BSU_ID = ds.Tables(0).Rows(0)("GSR_BSU_ID").ToString
            GSR_TYPE = ds.Tables(0).Rows(0)("GSR_TYPE").ToString
            GSR_STATUS = ds.Tables(0).Rows(0)("GSR_STATUS").ToString
            GSR_REMARKS = ds.Tables(0).Rows(0)("GSR_REMARKS").ToString
            GSR_APPR_EMP_ID = ds.Tables(0).Rows(0)("GSR_APPR_EMP_ID").ToString
            GSR_APPR_DATE = ds.Tables(0).Rows(0)("GSR_APPR_DATE").ToString
            'QRY = "SELECT  GSD_ID ,GSD_GSR_ID ,GSD_SRV_DESCR ,GSD_YesORNo FROM HR.EMP_GEN_SERVICE_REQUEST_D WITH(NOLOCK) WHERE GSD_GSR_ID=" & GSR_ID & ""
            'Dim ds_D As DataSet = SqlHelper.ExecuteDataset(objCon, CommandType.Text, QRY)
        Else
            GSR_EMP_ID = 0
            GSR_BSU_ID = ""
            GSR_TYPE = ""
            GSR_STATUS = ""
            GSR_REMARKS = ""
            GSR_APPR_EMP_ID = 0
        End If
    End Sub

    Public Function GET_SERVICE_REQ_D() As DataTable
        GET_SERVICE_REQ_D = Nothing
        Dim strCon = ConnectionManger.GetOASISConnectionString
        Dim QRY As String = "SELECT  GSD_ID ,GSD_GSR_ID ,GSD_SRV_DESCR ,GSD_YesORNo FROM HR.EMP_GEN_SERVICE_REQUEST_D WITH(NOLOCK) WHERE GSD_GSR_ID=" & GSR_ID & ""
        Dim ds_D As DataSet = SqlHelper.ExecuteDataset(strCon, CommandType.Text, QRY)
        If ds_D.Tables.Count > 0 AndAlso ds_D.Tables(0).Rows.Count > 0 Then
            GET_SERVICE_REQ_D = ds_D.Tables(0)
        End If
    End Function
End Class
