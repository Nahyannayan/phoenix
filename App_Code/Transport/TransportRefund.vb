﻿Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Public Class TransportRefund
    Public Shared Function F_SaveTRANSPORT_REFUND_H(ByVal p_FRH_ID As Integer, ByVal p_FRH_BSU_ID As String, ByVal p_FRH_STU_BSU_ID As String, _
  ByVal p_FRH_ACD_ID As String, ByVal p_FRH_STU_TYPE As String, ByVal p_FRH_STU_ID As String, _
  ByVal p_FRH_FAR_ID As String, ByVal p_FRH_DATE As String, ByVal p_FRH_CHQDT As String, _
  ByVal p_FRH_bPosted As Boolean, ByRef NEW_FRH_ID As String, ByVal p_FRH_bDeleted As Boolean, _
  ByVal p_FRH_NARRATION As String, ByVal p_FRH_VHH_DOCNO As String, ByVal p_FRH_BANK_CASH As String, _
  ByVal p_FRH_ACT_ID As String, ByVal p_FRH_PAIDTO As String, ByVal p_FRH_TOTAL As String, _
  ByVal p_FRH_BANKCHARGE As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(19) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FRH_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FRH_ID
        pParms(1) = New SqlClient.SqlParameter("@FRH_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_FRH_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@FRH_ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_FRH_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@FRH_STU_TYPE", SqlDbType.VarChar, 4)
        pParms(3).Value = p_FRH_STU_TYPE
        pParms(4) = New SqlClient.SqlParameter("@FRH_STU_ID", SqlDbType.BigInt)
        pParms(4).Value = p_FRH_STU_ID
        pParms(5) = New SqlClient.SqlParameter("@FRH_FAR_ID", SqlDbType.Int)
        pParms(5).Value = p_FRH_FAR_ID
        pParms(6) = New SqlClient.SqlParameter("@FRH_DATE", SqlDbType.DateTime)
        pParms(6).Value = p_FRH_DATE
        pParms(7) = New SqlClient.SqlParameter("@FRH_CHQDT", SqlDbType.DateTime)
        pParms(7).Value = p_FRH_CHQDT
        pParms(8) = New SqlClient.SqlParameter("@FRH_bPosted", SqlDbType.Bit)
        pParms(8).Value = p_FRH_bPosted
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        pParms(10) = New SqlClient.SqlParameter("@NEW_FRH_ID", SqlDbType.BigInt)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@FRH_bDeleted", SqlDbType.Bit)
        pParms(11).Value = p_FRH_bDeleted
        pParms(12) = New SqlClient.SqlParameter("@FRH_NARRATION", SqlDbType.VarChar, 100)
        pParms(12).Value = p_FRH_NARRATION
        pParms(13) = New SqlClient.SqlParameter("@FRH_VHH_DOCNO", SqlDbType.VarChar, 20)
        pParms(13).Value = p_FRH_VHH_DOCNO
        pParms(14) = New SqlClient.SqlParameter("@FRH_BANK_CASH", SqlDbType.VarChar, 2)
        pParms(14).Value = p_FRH_BANK_CASH
        pParms(15) = New SqlClient.SqlParameter("@FRH_ACT_ID", SqlDbType.VarChar, 20)
        pParms(15).Value = p_FRH_ACT_ID
        pParms(16) = New SqlClient.SqlParameter("@FRH_PAIDTO", SqlDbType.VarChar, 200)
        pParms(16).Value = p_FRH_PAIDTO
        pParms(17) = New SqlClient.SqlParameter("@FRH_TOTAL", SqlDbType.Decimal)
        pParms(17).Value = p_FRH_TOTAL
        pParms(18) = New SqlClient.SqlParameter("@FRH_BANKCHARGE", SqlDbType.Decimal)
        pParms(18).Value = p_FRH_BANKCHARGE
        pParms(19) = New SqlClient.SqlParameter("@FRH_STU_BSU_ID", SqlDbType.VarChar)
        pParms(19).Value = p_FRH_STU_BSU_ID
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveTRANSPORT_REFUND_H", pParms)
        If pParms(9).Value = 0 Then
            NEW_FRH_ID = pParms(10).Value
        End If
        F_SaveTRANSPORT_REFUND_H = pParms(9).Value
    End Function


    Public Shared Function F_SaveTRANSPORT_REFUND_D(ByVal p_FRD_ID As Integer, _
        ByVal p_FRD_FRH_ID As Integer, ByVal p_FRD_AMOUNT As String, ByVal p_FRD_FEE_ID As String, _
         ByVal p_FRD_NARRATION As String, ByVal p_FRD_STU_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(7) As SqlClient.SqlParameter
        'ALTER   PROCEDURE [FEES].[F_SaveFEE_REFUND_D] 
        '@FRD_ID BIGINT, 
        '@FRD_FRH_ID BIGINT, 
        '@FRD_AMOUNT NUMERIC(18,3), 
        '@FRD_FEE_ID BIGINT, 
        '@FRD_NARRATION VARCHAR(100))
        pParms(0) = New SqlClient.SqlParameter("@FRD_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FRD_ID
        pParms(1) = New SqlClient.SqlParameter("@FRD_FRH_ID", SqlDbType.BigInt)
        pParms(1).Value = p_FRD_FRH_ID
        pParms(2) = New SqlClient.SqlParameter("@FRD_AMOUNT", SqlDbType.Decimal, 21)
        pParms(2).Value = p_FRD_AMOUNT
        pParms(3) = New SqlClient.SqlParameter("@FRD_FEE_ID", SqlDbType.Int)
        pParms(3).Value = p_FRD_FEE_ID
        pParms(4) = New SqlClient.SqlParameter("@FRD_NARRATION", SqlDbType.VarChar, 100)
        pParms(4).Value = p_FRD_NARRATION
        pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(5).Direction = ParameterDirection.ReturnValue
        pParms(6) = New SqlClient.SqlParameter("@FRD_STU_ID", SqlDbType.BigInt)
        pParms(6).Value = p_FRD_STU_ID

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveTRANSPORT_REFUND_D", pParms)
        F_SaveTRANSPORT_REFUND_D = pParms(5).Value
    End Function

    Public Shared Function GetRefundData(ByVal p_BSU_ID As String, ByVal p_STU_TYPE As String, _
  ByVal p_STU_ID As String, ByVal p_MODE As String) As DataTable
        'ALTER PROCEDURE FEES.GetRefundData 
        '@BSU_ID VARCHAR(20)='125016',
        '@STU_ID BIGINT=93691,
        '@STU_TYPE VARCHAR(2)='S' ,
        '@MODE VARCHAR(10)='DETAIL' 

        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 100)
        pParms(0).Value = p_BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(1).Value = p_STU_ID
        pParms(3) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(3).Value = p_STU_TYPE
        pParms(4) = New SqlClient.SqlParameter("@MODE", SqlDbType.VarChar, 10)
        pParms(4).Value = p_MODE
        'pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        'pParms(5).Direction = ParameterDirection.ReturnValue

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
          CommandType.StoredProcedure, "FEES.GetRefundData", pParms)

        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetPreviousACD(ByVal p_ACD_ID As Integer, ByVal p_BSU_ID As String, _
   ByVal p_str_conn As String) As String
        Dim pParms(6) As SqlClient.SqlParameter
        'EXEC     @return_value = [fees.GetPreviousACD
        '@ACD_ID = 79,    
        '@BSU_ID = N'125016', 
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(0).Value = p_ACD_ID
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID
        Dim retval
        retval = SqlHelper.ExecuteScalar(p_str_conn, CommandType.StoredProcedure, "fees.GetPreviousACD", pParms)
        If retval Is DBNull.Value Then
            Return String.Empty
        Else
            Return ""
        End If
    End Function

    Public Shared Function PostTransportFeeRefund(ByVal p_FRH_ID As Integer, _
    ByVal p_FRH_VHH_DOCNO As String, ByVal p_DATE As String, _
    ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        '  procedure FEES.PostFeeRefund  
        '@FRH_ID bigint ,
        '@FRH_VHH_DOCNO varchar(20) , 
        '@ACD_ID bigint=80 ,
        '@DATE DATETIME  
        pParms(0) = New SqlClient.SqlParameter("@FRH_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FRH_ID
        pParms(1) = New SqlClient.SqlParameter("@FRH_VHH_DOCNO", SqlDbType.VarChar, 20)
        pParms(1).Value = p_FRH_VHH_DOCNO
        pParms(2) = New SqlClient.SqlParameter("@DATE", SqlDbType.DateTime)
        pParms(2).Value = p_DATE
        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.PostTransportFeeRefund", pParms)
        PostTransportFeeRefund = pParms(3).Value
        'PostTransportFeeRefund = retval
    End Function


End Class
