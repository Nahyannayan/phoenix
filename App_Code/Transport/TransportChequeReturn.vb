﻿Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Data.SqlClient

Public Class TransportChequeReturn

#Region "Public Variables"

    Private pFCR_ID As Integer
    Public Property FCR_ID() As Integer
        Get
            Return pFCR_ID
        End Get
        Set(ByVal value As Integer)
            pFCR_ID = value
        End Set
    End Property

    Private pFCR_BSU_ID As String
    Public Property FCR_BSU_ID() As String
        Get
            Return pFCR_BSU_ID
        End Get
        Set(ByVal value As String)
            pFCR_BSU_ID = value
        End Set
    End Property
    Private pFCR_STU_BSU_ID As String
    Public Property FCR_STU_BSU_ID() As String
        Get
            Return pFCR_STU_BSU_ID
        End Get
        Set(ByVal value As String)
            pFCR_STU_BSU_ID = value
        End Set
    End Property
    Private pFCR_STU_ID As Long
    Public Property FCR_STU_ID() As Long
        Get
            Return pFCR_STU_ID
        End Get
        Set(ByVal value As Long)
            pFCR_STU_ID = value
        End Set
    End Property

    Private pFCR_bSendSMS As Boolean
    Public Property FCR_bSendSMS() As Boolean
        Get
            Return pFCR_bSendSMS
        End Get
        Set(ByVal value As Boolean)
            pFCR_bSendSMS = value
        End Set
    End Property
    Private pFCR_bSendEmail As Boolean
    Public Property FCR_bSendEmail() As Boolean
        Get
            Return pFCR_bSendEmail
        End Get
        Set(ByVal value As Boolean)
            pFCR_bSendEmail = value
        End Set
    End Property

    Private pUserName As String
    Public Property UserName() As String
        Get
            Return pUserName
        End Get
        Set(ByVal value As String)
            pUserName = value
        End Set
    End Property

#End Region
    Public Function GetEmailedCount() As Integer
        Dim Qry As String = ""
        Try
            Qry = "SELECT COUNT(EML_ID) FROM dbo.COM_EMAIL_SCHEDULE WITH(NOLOCK) WHERE EML_BSU_ID='" & FCR_BSU_ID & "' AND ISNULL(EML_STATUS,'')='COMPLETED' AND EML_TYPE='FCR' AND EML_DOC_ID='" & FCR_ID & "' AND ISNULL(EML_PROFILE_ID,'')='" & FCR_STU_ID & "'"
            GetEmailedCount = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, Qry)
        Catch ex As Exception
            GetEmailedCount = 0
        End Try
        Return GetEmailedCount
    End Function

    Public Function GetSMSCount() As Integer
        Dim Qry As String = ""
        Try
            Qry = "SELECT COUNT(LOG_ID) FROM FEES.FEE_REMINDER_SMS WITH(NOLOCK) WHERE LOG_BSU_ID='" & FCR_BSU_ID & "' AND ISNULL(LOG_STATUS,'')='COMPLETED' AND LOG_STU_ID=" & FCR_STU_ID & " AND ISNULL(LOG_TYPE,'')='FCR' AND LOG_FRH_ID='" & FCR_ID & "'"
            GetSMSCount = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, Qry)
        Catch ex As Exception
            GetSMSCount = 0
        End Try
        Return GetSMSCount
    End Function

    Public Function SendAlert() As Boolean
        SendAlert = True
        Try

            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FCR_ID", FCR_ID)
            pParms(1) = New SqlClient.SqlParameter("@STU_ID", FCR_STU_ID)
            pParms(2) = New SqlClient.SqlParameter("@bSendSMS", FCR_bSendSMS)
            pParms(3) = New SqlClient.SqlParameter("@bSendEmail", FCR_bSendEmail)
            pParms(4) = New SqlClient.SqlParameter("@UserName", UserName)
            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.StoredProcedure, "TRANSPORT.ALERT_CHQ_RETURN", pParms)
            Dim ReturnFlag As Integer = pParms(5).Value
            If ReturnFlag <> 0 Then
                SendAlert = False
            End If
        Catch ex As Exception
            SendAlert = False
        End Try
        Return SendAlert
    End Function

End Class
