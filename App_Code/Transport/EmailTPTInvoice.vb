﻿Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web

Public Class EmailTPTInvoice
#Region "Public Variables"
    Private pBSU_ID As String
    Public Property BSU_ID() As String
        Get
            Return pBSU_ID
        End Get
        Set(ByVal value As String)
            pBSU_ID = value
        End Set
    End Property

    Private pInvType As String
    Public Property InvType() As String
        Get
            Return pInvType
        End Get
        Set(ByVal value As String)
            pInvType = value
        End Set
    End Property
    Private pFCL_ID As Long
    Public Property FCL_ID() As Long
        Get
            Return pFCL_ID
        End Get
        Set(ByVal value As Long)
            pFCL_ID = value
        End Set
    End Property
    Private pFCO_ID As Long
    Public Property FCO_ID() As Long
        Get
            Return pFCO_ID
        End Get
        Set(ByVal value As Long)
            pFCO_ID = value
        End Set
    End Property
    Private pLogoPath As String
    Public Property LogoPath() As String
        Get
            Return pLogoPath
        End Get
        Set(ByVal value As String)
            pLogoPath = value
        End Set
    End Property
    Private pEmailStatus As String
    Public Property EmailStatusMsg() As String
        Get
            Return pEmailStatus
        End Get
        Set(ByVal value As String)
            pEmailStatus = value
        End Set
    End Property
    Private pInvNo As String
    Public Property InvNo() As String
        Get
            Return pInvNo
        End Get
        Set(ByVal value As String)
            pInvNo = value
        End Set
    End Property
    Private boolEmailed As Boolean
    Public Property bEmailSuccess() As Boolean
        Get
            Return boolEmailed
        End Get
        Set(ByVal value As Boolean)
            boolEmailed = value
        End Set
    End Property
    Private boolMobileApp As Boolean
    Public Property bMobileApp() As Boolean
        Get
            Return boolMobileApp
        End Get
        Set(ByVal value As Boolean)
            boolMobileApp = value
        End Set
    End Property
    Private pAction As String
    Public Property Action() As String
        Get
            Return pAction
        End Get
        Set(ByVal value As String)
            pAction = value
        End Set
    End Property
#End Region
    Shared reportHeader As String
    Dim rs As New ReportDocument
    Public Shared Function ConvertFiletoBytes(ByVal FilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(FilePath) = True Then
            Throw New ArgumentNullException("File Name Cannot be Null or Empty", "FilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(FilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Shared Sub DownloadFile(ByVal context As HttpContext, ByVal BytesData As Byte(), ByVal contentType As String, ByVal Extension As String, ByVal Title As String)
        context.Response.Clear()
        context.Response.AddHeader("Content-Length", BytesData.Length.ToString())
        context.Response.ContentType = contentType
        context.Response.AddHeader("Expires", "0")
        context.Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
        context.Response.AddHeader("Pragma", "public")
        context.Response.AddHeader("Content-Disposition", "attachment; filename=" & Title & Extension)
        context.Response.BinaryWrite(BytesData)
        context.Response.Flush()
        context.Response.End()
    End Sub
    Private Shared Function GetFileExtension(ByVal ContentType As String) As String
        ' Dim filePath As String = fuUpload.PostedFile.FileName
        Select Case ContentType
            Case "application/vnd.ms-word"
                GetFileExtension = ".doc"
                Exit Select
            Case "application/vnd.ms-word"
                GetFileExtension = ".docx"
                Exit Select
            Case "application/vnd.ms-excel"
                GetFileExtension = ".xls"
                Exit Select
            Case "image/jpg"
                GetFileExtension = ".jpg"
                Exit Select
            Case "image/png"
                GetFileExtension = ".png"
                Exit Select
            Case "image/gif"
                GetFileExtension = ".gif"
                Exit Select
            Case "application/pdf"
                GetFileExtension = ".pdf"
                Exit Select
        End Select
    End Function
    Private Shared Sub SubReport(ByVal repSource As MyReportClass, ByVal subRep As MyReportClass, ByRef repClassVal As RepClass)
        If subRep.SubReport IsNot Nothing Then
            Dim ii As Integer = 0
            If subRep.IncludeBSUImage Then
                ii = 1
            End If
            For i As Integer = 0 To subRep.SubReport.Length - 1
                Dim myrep As MyReportClass = subRep.SubReport(i)
                If myrep IsNot Nothing Then
                    If myrep.SubReport IsNot Nothing Then
                        SubReport(repSource, subRep, repClassVal)
                    Else
                        Dim objConn As SqlConnection = repSource.Command.Connection
                        'objConn.Close()
                        objConn.Open()
                        Dim ds As New DataSet
                        Dim adpt As New SqlDataAdapter
                        adpt.SelectCommand = myrep.Command
                        adpt.Fill(ds)
                        objConn.Close()
                        If String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Transport.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Portrait.rpt", True) <> 0 Then
                            repClassVal.Subreports(i + ii).SetDataSource(ds.Tables(0))
                        Else
                            reportHeader = repClassVal.Subreports(i + ii).Name
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Private Shared Sub IncludeBSUmage(ByRef repClassVal As RepClass, ByVal BSUID As String)
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(lstrConn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("getBsuInFoWithImage", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpIMG_BSU_ID As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 100)
        If BSUID = "" Then
            sqlpIMG_BSU_ID.Value = BSUID
        Else
            sqlpIMG_BSU_ID.Value = BSUID
        End If

        cmd.Parameters.Add(sqlpIMG_BSU_ID)

        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmd.Parameters.Add(sqlpIMG_TYPE)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        objConn.Close()
        If Not repClassVal.Subreports("rptHeader.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Transport.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Transport.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Portrait.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Portrait.rpt").SetDataSource(ds.Tables(0))
        End If
        'repClassVal.Subreports("bsuimage").SetDataSource(dtDt)
        'repClassVal.Subreports("BSUIMAGE").SetDataSource(dtDt)
    End Sub

    Sub LoadReports(ByVal rptClass As rptClass)
        Try
            Dim iRpt As New DictionaryEntry
            Dim rptStr As String = ""
            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues

            With rptClass
                rs.Load(.reportPath)

                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword
                'Logger.LogInfo("Calling SetDBLogonForSubreports")
                'UtilityObj.Errorlog("Calling SetDBLogonForSubreports", "OASIS_TPT")
                SetDBLogonForSubreports(myConnectionInfo, rs, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs, .reportParameters)
                'UtilityObj.Errorlog("setting report parameters", "OASIS_TPT")
                'Logger.LogInfo("Setting report parameters")
                crParameterFieldDefinitions = rs.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If

                If .selectionFormula <> "" Then
                    rs.RecordSelectionFormula = .selectionFormula
                End If
                'Logger.LogInfo("Calling exportReport")
                'UtilityObj.Errorlog("Calling exportreport", "OASIS_TPT")
                exportReport(rs, CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat)
                rs.Close()
                rs.Dispose()
            End With
        Catch ex As Exception
            rs.Close()
            rs.Dispose()
        End Try
    End Sub

    Protected Sub exportReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
        selectedReport.ExportOptions.ExportFormatType = eft

        Dim contentType As String = ""
        ' Make sure asp.net has create and delete permissions in the directory
        Dim tempDir As String = Web.Configuration.WebConfigurationManager.AppSettings.Item("UploadExcelFile")
        'Dim tempFileName As String = HttpContext.Current.Session("sBsuId") & "_" & InvNo & "."
        Dim tempFileName As String = InvNo & "."
        Select Case eft
            Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                tempFileName += "pdf"
                contentType = "application/pdf"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
                tempFileName += "doc"
                contentType = "application/msword"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.Excel
                tempFileName += "xls"
                contentType = "application/vnd.ms-excel"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
                tempFileName += "htm"
                contentType = "text/html"
                Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
                hop.HTMLBaseFolderName = tempDir
                hop.HTMLFileName = tempFileName
                selectedReport.ExportOptions.FormatOptions = hop
                Exit Select
        End Select
        Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
        dfo.DiskFileName = tempDir + tempFileName
        selectedReport.ExportOptions.DestinationOptions = dfo
        selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        selectedReport.Export()
        selectedReport.Close()
        Dim tempFileNameUsed As String
        If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
            Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
            Dim leafDir As String = fp(fp.Length - 1)
            ' strip .rpt extension
            leafDir = leafDir.Substring(0, leafDir.Length - 4)
            tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
        Else
            tempFileNameUsed = tempDir + tempFileName
        End If
        If Not Action Is Nothing AndAlso Action.ToUpper = "DOWNLOAD" Then
            Dim BytesData As Byte()
            BytesData = ConvertFiletoBytes(tempFileNameUsed)
            Dim Extension As String
            Extension = GetFileExtension(contentType)
            Dim Title As String = "Receipt"
            Title = tempFileName
            DownloadFile(System.Web.HttpContext.Current, BytesData, contentType, Extension, Title)
            Try
                If System.IO.File.Exists(tempFileNameUsed) Then
                    System.IO.File.Delete(tempFileNameUsed)
                End If
            Catch ex As Exception

            End Try
        Else
            EmailiInvoiceReceipt(tempFileNameUsed)

        End If
    End Sub
    Public Sub EmailiInvoiceReceipt(ByVal pdfFilePath As String)
        Dim ds As DataSet = Nothing

        If InvType = "JOB" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, _
                  "EXEC [GET_INVOICE_EMAILINFO] @INV_NO='" & InvNo & "', @INV_TYPE='" & InvType & "',@LOGIN_BSU_ID='" & BSU_ID & "'")

        ElseIf InvType = "LEASING" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, _
                              "EXEC [GET_INVOICE_EMAILINFO] @INV_NO='" & InvNo & "', @INV_TYPE='" & InvType & "',@LOGIN_BSU_ID='" & BSU_ID & "'")
        ElseIf InvType = "DRIVER" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, _
                              "EXEC [GET_INVOICE_EMAILINFO] @INV_NO='" & InvNo & "', @INV_TYPE='" & InvType & "',@LOGIN_BSU_ID='" & BSU_ID & "'")
        ElseIf InvType = "HIRE" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, _
                              "EXEC [GET_INVOICE_EMAILINFO] @INV_NO='" & InvNo & "', @INV_TYPE='" & InvType & "',@LOGIN_BSU_ID='" & BSU_ID & "'")
        ElseIf InvType = "EXTRATRIP" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, _
                              "EXEC [GET_INVOICE_EMAILINFO] @INV_NO='" & InvNo & "', @INV_TYPE='" & InvType & "',@LOGIN_BSU_ID='" & BSU_ID & "'")
        ElseIf InvType = "EXGRATIA" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, _
                              "EXEC [GET_INVOICE_EMAILINFO] @INV_NO='" & InvNo & "', @INV_TYPE='" & InvType & "',@LOGIN_BSU_ID='" & BSU_ID & "'")
        ElseIf InvType = "IDCARD" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, _
                              "EXEC [GET_INVOICE_EMAILINFO] @INV_NO='" & InvNo & "', @INV_TYPE='" & InvType & "',@LOGIN_BSU_ID='" & BSU_ID & "'")
        ElseIf InvType = "ACHARGE" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, _
                              "EXEC [GET_INVOICE_EMAILINFO] @INV_NO='" & InvNo & "', @INV_TYPE='" & InvType & "',@LOGIN_BSU_ID='" & BSU_ID & "'")
        End If
        'SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "exec SAVE_INVOICE_EMAIL_LOG  '1','" & InvType & "','" & EmailStatusMsg & "','" & HttpContext.Current.Session("sBsuId") & "','1'")


        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To ds.Tables(1).Rows.Count - 1

                'Dim subject As String = IIf(bMobileApp, "Invoice Receipt", "Invoice Receipt")
                Dim subject As String = "Invoice Receipt :" & InvNo

                Dim ContactName, BSU_Name, EMailID As String, Emailed As Boolean
                ContactName = ds.Tables(1).Rows(i).Item("TO_EMAIL_NAME").ToString
                BSU_Name = ds.Tables(0).Rows(0).Item("BSU_Name").ToString
                EMailID = ds.Tables(1).Rows(i).Item("TO_EMAIL_ID").ToString
                Emailed = ds.Tables(0).Rows(0).Item("Emailed")
                Dim DummyImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ToString() & "/DUMMYIMAGE/dummy_profile.png"
                'SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "exec SAVE_INVOICE_EMAIL_LOG  '2','" & InvType & "','" & EmailStatusMsg & "','" & HttpContext.Current.Session("sBsuId") & "','2'")
                If Not Emailed Or 1 = 1 Then
                    Dim sb As New StringBuilder '
                    sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>")
                    sb.Append("<html xmlns='http://www.w3.org/1999/xhtml'>")
                    sb.Append("<head id='Head1' runat='server'><title>Untitled Page</title></head><body>")
                    sb.Append("<form id='form1' runat='server'><div style='width: 100%;'><center>")
                    sb.Append("<table border='0' style='width:50%; border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: white; border-bottom-style: none;font-size: 10pt;'>")
                    sb.Append("<tr><td align='left'><div style='width:80px; height:60px;'><img src='" & LogoPath & "' alt='' style='width:80px; height:60px;'/></div></td></tr>")
                    sb.Append("<tr><td><hr /></td></tr>")
                    'sb.Append("<tr><td align='left' style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Dear " & ContactName & ", </td></tr>")
                    sb.Append("<tr><td align='left'><br />GREETINGS from <strong>" & BSU_Name & "</strong><br /><br /></td></tr>")
                    sb.Append("<tr><td align='left' ><br />Please find the attached Invoice.  <br /></td></tr>")
                    sb.Append("<tr><td align='left'><br />Thanks & regards </td></tr>")
                    sb.Append("<tr><td align='left'>Accounts </td></tr>")
                    sb.Append("<tr><td align='left'><strong>" & BSU_Name & "</strong><br /></td></tr>")
                    sb.Append("<tr><td></td></tr>")
                    sb.Append("<tr></tr><tr></tr>")
                    sb.Append("</table></center></div></form></body></html>")
                    'SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "exec SAVE_INVOICE_EMAIL_LOG  '3','" & InvType & "','" & Replace(sb.ToString(), "'", "`") & "','" & HttpContext.Current.Session("sBsuId") & "','3'")
                    Try
                        If EMailID <> "" Then
                            EmailStatusMsg = eMailReceipt.SendNewsLetters(ds.Tables(0).Rows(0)("EML_FROMEMAIL").ToString, EMailID, _
                                                         subject, sb.ToString, ds.Tables(0).Rows(0)("EML_FROMEMAIL").ToString, ds.Tables(0).Rows(0)("SYS_PASSWORD").ToString, _
                                                                ds.Tables(0).Rows(0)("SYS_EMAIL_HOST").ToString, ds.Tables(0).Rows(0)("SYS_EMAIL_PORT").ToString, pdfFilePath)
                            'SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "exec SAVE_INVOICE_EMAIL_LOG  '" & EMailID & "','" & InvType & "','" & EmailStatusMsg & "','" & HttpContext.Current.Session("sBsuId") & "','" & InvNo & "'")
                        Else
                            EmailStatusMsg = eMailReceipt.SendNewsLetters(ds.Tables(0).Rows(0)("EML_FROMEMAIL").ToString, "mahesh.kumar@gemseducation.com", _
                                         subject, sb.ToString, ds.Tables(0).Rows(0)("EML_FROMEMAIL").ToString, ds.Tables(0).Rows(0)("SYS_PASSWORD").ToString, _
                                                ds.Tables(0).Rows(0)("SYS_EMAIL_HOST").ToString, ds.Tables(0).Rows(0)("SYS_EMAIL_PORT").ToString, pdfFilePath)
                            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "exec SAVE_INVOICE_EMAIL_LOG  '" & EMailID & "','" & InvType & "','" & EmailStatusMsg & "','" & HttpContext.Current.Session("sBsuId") & "','" & InvNo & "'")
                        End If
                    Catch ex As Exception
                        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "exec SAVE_INVOICE_EMAIL_LOG  '" & EMailID & "','" & InvType & "','" & ex.Message & "','" & HttpContext.Current.Session("sBsuId") & "','" & InvNo & "'")
                    End Try


                    If EmailStatusMsg.ToString.ToUpper.Contains("SUCCESS") Then
                        EmailStatusMsg = IIf(EMailID <> "", " Receipt successfully emailed to " & EMailID, "")
                        bEmailSuccess = True

                        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "exec SAVE_INVOICE_EMAIL_LOG  '" & EMailID & "','" & InvType & "','Email Sent Sucessfully...','" & HttpContext.Current.Session("sBsuId") & "','" & InvNo & "'")
                        Dim sqlStr As String = "UPDATE TPTHIRING set TPT_bEMAILED=1 WHERE TRN_BATCHSTR='" & InvNo & "' "
                        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, sqlStr)
                    Else
                        EmailStatusMsg = "Email Sending Failed"
                        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, "exec SAVE_INVOICE_EMAIL_LOG  '" & EMailID & "','" & InvType & "','" & EmailStatusMsg & "','" & HttpContext.Current.Session("sBsuId") & "','" & InvNo & "'")
                    End If
                End If
            Next
            Try
                If System.IO.File.Exists(pdfFilePath) Then
                    System.IO.File.Delete(pdfFilePath)
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
        myReportDocument.VerifyDatabase()
    End Sub
    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

                    Select Case subReportDocument.Name

                        Case Else
                            SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    End Select
                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next
    End Sub

End Class
