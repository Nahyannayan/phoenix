Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Public Class TransportClass
#Region " All Get function"
    Public Shared Function GetVehicle_M_Details(ByVal VEH_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --07/aug/2008
        'Purpose--Get Vehicle_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISTransportConnection
        Dim sqlVehicle_M_Details As String = ""

        sqlVehicle_M_Details = " SELECT  (select max(VAL_FROMDT)   from TRANSPORT.VEHICLE_ALLOCATION where VAL_VEH_ID='" & VEH_ID & "' and VAL_TYPE='O' and VAL_TODT is null) as VAL_FROMDT_O," & _
" VEHICLE_M.VEH_OWN_BSU_ID, BSU_OWNED.BSU_NAME AS OWNED_BSU," & _
" (select max(VAL_FROMDT)   from TRANSPORT.VEHICLE_ALLOCATION where VAL_VEH_ID='" & VEH_ID & "' and VAL_TYPE='P' and VAL_TODT is null) as VAL_FROMDT_P," & _
" VEHICLE_M.VEH_OPRT_BSU_ID,BSU_OPERA.BSU_NAME AS OPERA_BSU, " & _
"(select max(VAL_FROMDT)   from TRANSPORT.VEHICLE_ALLOCATION where VAL_VEH_ID='" & VEH_ID & "' and VAL_TYPE='A' and VAL_TODT is null) as VAL_FROMDT_A," & _
" VEHICLE_M.VEH_ALTO_BSU_ID, BSU_ALLOC.BSU_NAME AS ALLOC_BSU," & _
" TRANSPORT.VW_INSURANCE_M.INC_NAME, TRANSPORT.VV_CATEGORY_M.CAT_DESCRIPTION,VW_OSF_Suppliers.ACT_NAME, " & _
 " TRANSPORT.VW_HYPOTHICATED_M.HPM_NAME, TRANSPORT.VW_VMK_MAKE.VMK_MAKE, VEHICLE_M.VEH_CAT_ID, VEHICLE_M.VEH_REGNO, " & _
"  VEHICLE_M.VEH_REGDATE, VEHICLE_M.VEH_CAPACITY, VEHICLE_M.VEH_MODEL,VEHICLE_M.VEH_PLATE_COLOR,VEHICLE_M.VEH_ORDERNO, " & _
                     " VEHICLE_M.VEH_INC_ID, VEHICLE_M.VEH_HPM_ID, VEHICLE_M.VEH_INVOICENO, VEHICLE_M.VEH_CHASISNO, VEHICLE_M.VEH_ENGINENO," & _
                     " VEHICLE_M.VEH_INVOICE_DATE, VEHICLE_M.VEH_INVOICE_VALUE, VEHICLE_M.VEH_REV_VALUE, VEHICLE_M.VEH_DEPR_VALUE, " & _
                     " VEHICLE_M.VEH_INC_VALUE, VEHICLE_M.VEH_INC_STARTDT,VEHICLE_M.VEH_INC_EXPDT, VEHICLE_M.VEH_REG_EXPDT, VEHICLE_M.VEH_RECOMMD_MILAGE, " & _
                     " VEHICLE_M.VEH_VMK_ID,VEHICLE_M.VEH_ID,VEHICLE_M.VEH_SUPPLIER_ACT_ID,VEHICLE_M.VEH_FUT_ID,VEHICLE_M.VEH_CARD_NUMBER,isnull(VEHICLE_M.VEH_SALE_INVNO,'') VEH_SALE_INVNO,isnull(VEHICLE_M.VEH_SALE_VALUE,0)VEH_SALE_VALUE,isnull(VEHICLE_M.VEH_SALE_DATE,'') VEH_SALE_DATE  FROM  VEHICLE_M INNER JOIN " & _
                     " BUSINESSUNIT_M AS BSU_OWNED ON VEHICLE_M.VEH_OWN_BSU_ID = BSU_OWNED.BSU_ID LEFT OUTER JOIN " & _
                      " [BUSINESSUNIT_M] AS BSU_OPERA ON VEHICLE_M.VEH_OPRT_BSU_ID = BSU_OPERA.BSU_ID LEFT OUTER JOIN " & _
                     " [BUSINESSUNIT_M] AS BSU_ALLOC ON VEHICLE_M.VEH_ALTO_BSU_ID = BSU_ALLOC.BSU_ID LEFT OUTER JOIN " & _
                      " TRANSPORT.VW_VMK_MAKE ON VEHICLE_M.VEH_VMK_ID = TRANSPORT.VW_VMK_MAKE.VMK_ID LEFT OUTER JOIN " & _
                      " TRANSPORT.VW_HYPOTHICATED_M ON VEHICLE_M.VEH_HPM_ID = TRANSPORT.VW_HYPOTHICATED_M.HPM_ID LEFT OUTER JOIN " & _
                     " TRANSPORT.VW_INSURANCE_M ON VEHICLE_M.VEH_INC_ID = TRANSPORT.VW_INSURANCE_M.INC_ID LEFT OUTER JOIN " & _
                     " TRANSPORT.VV_CATEGORY_M ON VEHICLE_M.VEH_CAT_ID = TRANSPORT.VV_CATEGORY_M.CAT_ID LEFT OUTER JOIN " & _
                     " VW_OSF_Suppliers ON VEHICLE_M.VEH_SUPPLIER_ACT_ID = VW_OSF_Suppliers.ACT_ID where  VEHICLE_M.VEH_ID='" & VEH_ID & "'"


        Dim command As SqlCommand = New SqlCommand(sqlVehicle_M_Details, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetVehicleAlloc_ID(ByVal VID As String, ByVal VTYPE As String) As SqlDataReader


        'Author(--Lijo)
        'Date   --05/AUG/2008
        'Purpose--Get VEHICLE_ALLOCATION
        Dim connection As SqlConnection = ConnectionManger.GetOASISTransportConnection
        Dim sqlVehicleAlloc_ID As String = ""

        sqlVehicleAlloc_ID = " SELECT    VEHICLE_M.VEH_REGNO as REGNO,  BUSINESSUNIT_M.BSU_NAME as BSU,  TRANSPORT.VEHICLE_ALLOCATION.VAL_FROMDT as FROMDT, TRANSPORT.VEHICLE_ALLOCATION.VAL_TODT as TODT, " & _
      "  TRANSPORT.VEHICLE_ALLOCATION.VAL_VEH_ID ,TRANSPORT.VEHICLE_ALLOCATION.VAL_ID as VAL_ID, TRANSPORT.VEHICLE_ALLOCATION.VAL_TYPE " & _
" FROM  TRANSPORT.VEHICLE_ALLOCATION INNER JOIN  BUSINESSUNIT_M ON TRANSPORT.VEHICLE_ALLOCATION.VAL_BSU_ID = BUSINESSUNIT_M.BSU_ID INNER JOIN " & _
 " VEHICLE_M ON TRANSPORT.VEHICLE_ALLOCATION.VAL_VEH_ID = VEHICLE_M.VEH_ID WHERE " & _
 " TRANSPORT.VEHICLE_ALLOCATION.VAL_VEH_ID='" & VID & "' AND TRANSPORT.VEHICLE_ALLOCATION.VAL_TYPE='" & VTYPE & "' order by TRANSPORT.VEHICLE_ALLOCATION.VAL_ID desc "

        Dim command As SqlCommand = New SqlCommand(sqlVehicleAlloc_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.Text, sqlVehicleAlloc_ID)  'command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader


    End Function

    Public Shared Function GetVehicleChanged_ID(ByVal VID As String, ByVal VTYPE As String) As SqlDataReader


        'Author(--Lijo)
        'Date   --06/AUG/2008
        'Purpose--Get VEHICLE_to be changed to...
        Dim connection As SqlConnection = ConnectionManger.GetOASISTransportConnection
        Dim sqlVehicleChanged_ID As String = ""
        If VTYPE = "O" Then
            sqlVehicleChanged_ID = " SELECT VEHICLE_M.VEH_OWN_BSU_ID AS BSU_OLD, BSU_OWNED.BSU_NAME as BSU_NAME, TRANSPORT.VEHICLE_ALLOCATION.VAL_VEH_ID AS VEH_ID, TRANSPORT.VEHICLE_ALLOCATION.VAL_ID  as VAL_ID , TRANSPORT.VEHICLE_ALLOCATION.VAL_FROMDT as FROMDT" & _
          " FROM VEHICLE_M INNER JOIN  TRANSPORT.VEHICLE_ALLOCATION ON VEHICLE_M.VEH_ID = TRANSPORT.VEHICLE_ALLOCATION.VAL_VEH_ID INNER JOIN " & _
          " [BUSINESSUNIT_M] AS BSU_OWNED ON VEHICLE_M.VEH_OWN_BSU_ID = BSU_OWNED.BSU_ID WHERE   (TRANSPORT.VEHICLE_ALLOCATION.VAL_VEH_ID = '" & VID & "') " & _
          " AND (TRANSPORT.VEHICLE_ALLOCATION.VAL_TYPE = '" & VTYPE & "') AND  (TRANSPORT.VEHICLE_ALLOCATION.VAL_TODT IS NULL)"
        ElseIf VTYPE = "P" Then
            sqlVehicleChanged_ID = "SELECT  VEHICLE_M.VEH_OPRT_BSU_ID as BSU_OLD,BSU_Operated.BSU_NAME as BSU_NAME, TRANSPORT.VEHICLE_ALLOCATION.VAL_VEH_ID AS VEH_ID, TRANSPORT.VEHICLE_ALLOCATION.VAL_ID as VAL_ID, TRANSPORT.VEHICLE_ALLOCATION.VAL_FROMDT as FROMDT " & _
 " FROM VEHICLE_M INNER JOIN TRANSPORT.VEHICLE_ALLOCATION ON VEHICLE_M.VEH_ID = TRANSPORT.VEHICLE_ALLOCATION.VAL_VEH_ID " & _
 " INNER JOIN    [BUSINESSUNIT_M] AS BSU_Operated ON VEHICLE_M.VEH_OPRT_BSU_ID = BSU_Operated.BSU_ID WHERE  (TRANSPORT.VEHICLE_ALLOCATION.VAL_VEH_ID ='" & VID & "') " & _
 " AND (TRANSPORT.VEHICLE_ALLOCATION.VAL_TYPE ='" & VTYPE & "') AND (TRANSPORT.VEHICLE_ALLOCATION.VAL_TODT IS NULL) "
        ElseIf VTYPE = "A" Then
            sqlVehicleChanged_ID = "SELECT VEHICLE_M.VEH_ALTO_BSU_ID AS BSU_OLD,   BSU_Allocated.BSU_NAME as BSU_NAME, TRANSPORT.VEHICLE_ALLOCATION.VAL_VEH_ID AS VEH_ID, TRANSPORT.VEHICLE_ALLOCATION.VAL_ID as VAL_ID, TRANSPORT.VEHICLE_ALLOCATION.VAL_FROMDT as FROMDT  " & _
" FROM    VEHICLE_M INNER JOIN  TRANSPORT.VEHICLE_ALLOCATION ON VEHICLE_M.VEH_ID = TRANSPORT.VEHICLE_ALLOCATION.VAL_VEH_ID INNER JOIN " & _
 " [BUSINESSUNIT_M] AS BSU_Allocated ON VEHICLE_M.VEH_ALTO_BSU_ID = BSU_Allocated.BSU_ID WHERE(TRANSPORT.VEHICLE_ALLOCATION.VAL_VEH_ID ='" & VID & "') " & _
 " AND (TRANSPORT.VEHICLE_ALLOCATION.VAL_TYPE = '" & VTYPE & "') AND  (TRANSPORT.VEHICLE_ALLOCATION.VAL_TODT IS NULL)"
        End If


        Dim command As SqlCommand = New SqlCommand(sqlVehicleChanged_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.Text, sqlVehicleChanged_ID)  'command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function

#End Region

#Region "All Save function "
    Public Shared Function SAVE_VEHICLE_ALLOC(ByVal VAL_ID As String, ByVal VEH_ID As String, ByVal BSU_TO As String, ByVal FROMDT As String _
    , ByVal VTYPE As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --06/Jul/2008
        'Purpose--To save AVE_VEHICLE_ALLOC data based 
        Using connection As SqlConnection = ConnectionManger.GetOASISTransportConnection
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@VAL_ID", VAL_ID)
            pParms(1) = New SqlClient.SqlParameter("@VEH_ID", VEH_ID)
            pParms(2) = New SqlClient.SqlParameter("@BSU_TO", BSU_TO)
            pParms(3) = New SqlClient.SqlParameter("@FROMDT", FROMDT)
            pParms(4) = New SqlClient.SqlParameter("@VTYPE", VTYPE)
            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVETRANS_VEHICLE_ALLOC_D", pParms)
            Dim ReturnFlag As Integer = pParms(5).Value
            Return ReturnFlag

        End Using
    End Function
    ''added two new fields VEH_FUEL_TYPE, VEH_CARD_NUMBER,  by nahyan on dec10-2013
    Public Shared Function SAVE_VEHICLE_ALLOC_M(ByVal VEH_BSU_ID As String, ByVal VEH_ID As String, ByVal VEH_CAT_ID As String, ByVal VEH_REGNO As String, _
    ByVal VEH_REGDAT As String, ByVal VEH_CAPACITY As String, ByVal VEH_MODEL As String, ByVal VEH_ORDERNO As String, _
    ByVal VEH_SPM_ID As String, ByVal VEH_PLATE_COLOR As String, ByVal VEH_INC_ID As String, ByVal VEH_HPM_ID As String, ByVal VEH_INVOICENO As String, _
    ByVal VEH_CHASISNO As String, ByVal VEH_ENGINENO As String, ByVal VEH_INVOICE_DATE As String, ByVal VEH_INVOICE_VALUE As Double, _
    ByVal VEH_REV_VALUE As Double, ByVal VEH_DEPR_VALUE As String, ByVal VEH_INC_VALUE As Double, ByVal VEH_INC_STARTDT As String, ByVal VEH_INC_EXPDT As String, _
    ByVal VEH_REG_EXPDT As String, ByVal VEH_RECOMMD_MILAGE As Double, ByVal VEH_OWN_BSU_ID As String, ByVal VEH_OPRT_BSU_ID As String, _
    ByVal VEH_ALTO_BSU_ID As String, ByVal VEH_VMK_ID As String, ByVal VAL_TYPE_O As Boolean, ByVal VAL_FROMDT_O As String, _
    ByVal VAL_TYPE_P As Boolean, ByVal VAL_FROMDT_P As String, ByVal VAL_TYPE_A As Boolean, ByVal VAL_FROMDT_A As String, _
     ByVal bEdit As Boolean, ByVal VEH_FUEL_TYPE As Integer, ByVal VEH_CARD_NUMBER As String, ByVal transaction As SqlTransaction, VEH_SALE_INVNO As String, VEH_SALE_VALUE As Double, VEH_SALE_DATE As Date) As Integer
        'Author(--Lijo)
        'Date   --06/Jul/2008
        'Purpose--To save SAVE_VEHICLE_ALLOC_M data based 
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASISTransportConnection
                Dim pParms(41) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@VEH_BSU_ID", VEH_BSU_ID)
                pParms(1) = New SqlClient.SqlParameter("@VEH_ID", VEH_ID)
                pParms(2) = New SqlClient.SqlParameter("@VEH_CAT_ID", VEH_CAT_ID)
                pParms(3) = New SqlClient.SqlParameter("@VEH_REGNO", VEH_REGNO)
                pParms(4) = New SqlClient.SqlParameter("@VEH_REGDATE", VEH_REGDAT)
                pParms(5) = New SqlClient.SqlParameter("@VEH_CAPACITY", VEH_CAPACITY)
                pParms(6) = New SqlClient.SqlParameter("@VEH_MODEL", VEH_MODEL)
                pParms(7) = New SqlClient.SqlParameter("@VEH_ORDERNO", VEH_ORDERNO)
                pParms(8) = New SqlClient.SqlParameter("@VEH_SPM_ID", VEH_SPM_ID)
                pParms(9) = New SqlClient.SqlParameter("@VEH_INC_ID", VEH_INC_ID)
                pParms(10) = New SqlClient.SqlParameter("@VEH_HPM_ID", VEH_HPM_ID)
                pParms(11) = New SqlClient.SqlParameter("@VEH_INVOICENO", VEH_INVOICENO)
                pParms(12) = New SqlClient.SqlParameter("@VEH_CHASISNO", VEH_CHASISNO)
                pParms(13) = New SqlClient.SqlParameter("@VEH_ENGINENO", VEH_ENGINENO)
                pParms(14) = New SqlClient.SqlParameter("@VEH_INVOICE_DATE", VEH_INVOICE_DATE)
                pParms(15) = New SqlClient.SqlParameter("@VEH_INVOICE_VALUE", VEH_INVOICE_VALUE)
                pParms(16) = New SqlClient.SqlParameter("@VEH_REV_VALUE", VEH_REV_VALUE)
                pParms(17) = New SqlClient.SqlParameter("@VEH_DEPR_VALUE", VEH_DEPR_VALUE)
                pParms(18) = New SqlClient.SqlParameter("@VEH_INC_VALUE", VEH_INC_VALUE)
                pParms(19) = New SqlClient.SqlParameter("@VEH_INC_EXPDT", VEH_INC_EXPDT)
                pParms(20) = New SqlClient.SqlParameter("@VEH_REG_EXPDT", VEH_REG_EXPDT)
                pParms(21) = New SqlClient.SqlParameter("@VEH_RECOMMD_MILAGE", VEH_RECOMMD_MILAGE)
                pParms(22) = New SqlClient.SqlParameter("@VEH_OWN_BSU_ID", VEH_OWN_BSU_ID)
                pParms(23) = New SqlClient.SqlParameter("@VEH_OPRT_BSU_ID", VEH_OPRT_BSU_ID)
                pParms(24) = New SqlClient.SqlParameter("@VEH_ALTO_BSU_ID", VEH_ALTO_BSU_ID)
                pParms(25) = New SqlClient.SqlParameter("@VEH_VMK_ID", VEH_VMK_ID)
                pParms(26) = New SqlClient.SqlParameter("@VAL_TYPE_O", VAL_TYPE_O)
                pParms(27) = New SqlClient.SqlParameter("@VAL_FROMDT_O", VAL_FROMDT_O)
                pParms(28) = New SqlClient.SqlParameter("@VAL_TYPE_P", VAL_TYPE_P)
                pParms(29) = New SqlClient.SqlParameter("@VAL_FROMDT_P", VAL_FROMDT_P)
                pParms(30) = New SqlClient.SqlParameter("@VAL_TYPE_A", VAL_TYPE_A)
                pParms(31) = New SqlClient.SqlParameter("@VAL_FROMDT_A", VAL_FROMDT_A)
                pParms(32) = New SqlClient.SqlParameter("@VEH_PLATE_COLOR", VEH_PLATE_COLOR)
                pParms(33) = New SqlClient.SqlParameter("@VEH_INC_STARTDT", VEH_INC_STARTDT)

                pParms(34) = New SqlClient.SqlParameter("@bEdit", bEdit)
                ''added two new fields VEH_FUEL_TYPE, VEH_CARD_NUMBER,  by nahyan on dec10-2013
                pParms(36) = New SqlClient.SqlParameter("@VEH_FUEL_TYPE", VEH_FUEL_TYPE)
                pParms(37) = New SqlClient.SqlParameter("@VEH_CARD_NUMBER", VEH_CARD_NUMBER)


                ''added three new fields VEH_FUEL_TYPE, VEH_CARD_NUMBER,  by mahesh  on dec7-2015
                pParms(38) = New SqlClient.SqlParameter("@VEH_SALE_INVNO", VEH_SALE_INVNO)
                pParms(39) = New SqlClient.SqlParameter("@VEH_SALE_VALUE", VEH_SALE_VALUE)
                pParms(40) = New SqlClient.SqlParameter("@VEH_SALE_DATE", VEH_SALE_DATE)




                pParms(35) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(35).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVETRANS_VEHICLE_ALLOC_M", pParms)
                Dim ReturnFlag As Integer = pParms(35).Value
                Return ReturnFlag

            End Using
        Catch ex As Exception
            Return -1
        End Try
    End Function
#End Region
End Class
