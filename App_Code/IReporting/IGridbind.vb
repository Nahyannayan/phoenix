﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Public Class IGridbind


    Public Shared Sub bindIncident_info(ByVal InfoType As String, ByVal gvObject As GridView, ByVal IM_ID As String, ByVal CAT_ID As String, ByVal BSU_ID As String, Optional ByRef recordExist As Boolean = False)
        Try

            Dim conn As String = ConnectionManger.GetOASIS_REPORTINGConnectionString
            Dim DS As New DataSet
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlParameter("@IM_ID", IM_ID)
            param(1) = New SqlParameter("@CAT_ID", CAT_ID)
            param(2) = New SqlParameter("@BSU_ID", BSU_ID)

            If InfoType = "alert" Then
                DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[INC_T].[GETINCIDENT_ALERT]", param)
            ElseIf InfoType = "foll" Then
                DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[INC_T].[GETINCIDENT_FOLLOW_UP]", param)
            ElseIf InfoType = "inj" Then
                DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[INC_T].[GETINCIDENT_INJURY]", param)
            ElseIf InfoType = "loc" Then
                DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[INC_T].[GETINCIDENT_LOC]", param)
            ElseIf InfoType = "proDam" Then
                DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[INC_T].[GETINCIDENT_PRO_DAM]", param)
            ElseIf InfoType = "prop" Then
                DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[INC_T].[GETINCIDENT_PRO]", param)
            ElseIf InfoType = "susp" Then
                DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[INC_T].[GETINCIDENT_SUSPECT]", param)
            ElseIf InfoType = "veh" Then
                DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[INC_T].[GETINCIDENT_VEH]", param)
            ElseIf InfoType = "wit" Then
                DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[INC_T].[GETINCIDENT_WITNESS]", param)
            ElseIf InfoType = "vict" Then
                DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[INC_T].[GETINCIDENT_VICTIM]", param)
            End If
            If DS.Tables(0).Rows.Count > 0 Then
                recordExist = True
                gvObject.DataSource = DS.Tables(0)
                gvObject.DataBind()
            Else
                recordExist = False

                DS.Tables(0).Rows.Add(DS.Tables(0).NewRow())

                If InfoType = "inj" Then
                    DS.Tables(0).Rows(0)("SHOW_DETAIL") = False
                End If
                'start the count from 1 no matter gridcolumn is visible or not
                gvObject.DataSource = DS.Tables(0)
                Try
                    gvObject.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvObject.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvObject.Rows(0).Cells.Clear()
                gvObject.Rows(0).Cells.Add(New TableCell)
                gvObject.Rows(0).Cells(0).ColumnSpan = columnCount
                gvObject.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvObject.Rows(0).Cells(0).Text = "No record available!!!"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
        End Try
    End Sub
End Class
