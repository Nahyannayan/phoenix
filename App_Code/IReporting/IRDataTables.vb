﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections.Generic
Imports System.Management
Imports System.Net.Dns
Public Class IRDataTables
    Private Shared Function GetComputerName(ByVal clientIP As String) As String
        Try
            Dim hostEntry = System.Net.Dns.GetHostEntry(clientIP)
            Return hostEntry.HostName
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    Public Shared Sub GetClientInfo(ByRef ClientBrs As String, ByRef ClientIP As String, ByRef mcName As String)
        Try

            Dim computer_name As String() = System.Net.Dns.GetHostEntry(HttpContext.Current.Request.ServerVariables("remote_addr")).HostName.Split(New [Char]() {"."c})
            Dim ecn As [String] = System.Environment.MachineName
            mcName = computer_name(0).ToString() & " |" & GetComputerName(ClientIP)

            ClientIP = HttpContext.Current.Request.UserHostAddress()
            With HttpContext.Current.Request.Browser
                ClientBrs &= "Browser Capabilities" & vbCrLf
                ClientBrs &= "Type = " & .Type & vbCrLf
                ClientBrs &= "Name = " & .Browser & vbCrLf
                ClientBrs &= "Version = " & .Version & vbCrLf
                ClientBrs &= "Major Version = " & .MajorVersion & vbCrLf
                ClientBrs &= "Minor Version = " & .MinorVersion & vbCrLf
                ClientBrs &= "Platform = " & .Platform & vbCrLf
                ClientBrs &= "Is Beta = " & .Beta & vbCrLf
                ClientBrs &= "Is Crawler = " & .Crawler & vbCrLf
                ClientBrs &= "Is AOL = " & .AOL & vbCrLf
                ClientBrs &= "Is Win16 = " & .Win16 & vbCrLf
                ClientBrs &= "Is Win32 = " & .Win32 & vbCrLf
                ClientBrs &= "Supports Frames = " & .Frames & vbCrLf
                ClientBrs &= "Supports Tables = " & .Tables & vbCrLf
                ClientBrs &= "Supports Cookies = " & .Cookies & vbCrLf
                ClientBrs &= "Supports VBScript = " & .VBScript & vbCrLf
                ClientBrs &= "Supports JavaScript = " & _
                    .EcmaScriptVersion.ToString() & vbCrLf
                ClientBrs &= "Supports Java Applets = " & .JavaApplets & vbCrLf
                ClientBrs &= "Supports ActiveX Controls = " & .ActiveXControls & _
                    vbCrLf
                ClientBrs &= "Supports JavaScript Version = " & _
                .JScriptVersion.ToString & vbCrLf
            End With
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
        End Try

    End Sub


    'Public Shared Function GetMACAddress() As String
    '    Dim mc As New ManagementClass("Win32_NetworkAdapterConfiguration")
    '    Dim moc As ManagementObjectCollection = mc.GetInstances()
    '    Dim MACAddress As String = [String].Empty
    '    For Each mo As ManagementObject In moc
    '        If MACAddress = [String].Empty Then
    '            ' only return MAC Address from first card
    '            If CBool(mo("IPEnabled")) = True Then
    '                MACAddress = mo("MacAddress").ToString()
    '            End If
    '        End If
    '        mo.Dispose()
    '    Next

    '    MACAddress = MACAddress.Replace(":", "-")
    '    Return MACAddress
    'End Function

    Public Shared Function GetMACAddress() As String
        Try


            Dim str As String = String.Empty

            Dim queryobjCS As String = "Select * From Win32_ComputerSystem"
            Dim queryobjOS As String = "SELECT * FROM Win32_OperatingSystem"
            Dim queryobjNW As String = "SELECT * FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = True"
            Dim searcherobjCS As New ManagementObjectSearcher(queryobjCS)
            Dim searcherobjOS As New ManagementObjectSearcher(queryobjOS)
            Dim searcherobjNW As New ManagementObjectSearcher(queryobjNW)

            Dim objCS As ManagementObjectCollection = searcherobjCS.Get()
            Dim objOS As ManagementObjectCollection = searcherobjOS.Get()
            Dim objNW As ManagementObjectCollection = searcherobjNW.Get()

            For Each entryCurrent As ManagementObject In objCS
                str += " Domain :" & entryCurrent("Domain")
                str += ", Manufacturer :" & entryCurrent("Manufacturer")
                str += ", Model :" & entryCurrent("Model")
                str += ", Name :" & entryCurrent("Name")
                str += ", PartOfDomain :" & entryCurrent("PartOfDomain")
                str += ", SystemType :" & entryCurrent("SystemType")
                str += ", UserName :" & entryCurrent("UserName")
                str += ", Workgroup :" & entryCurrent("Workgroup")
                entryCurrent.Dispose()
                Exit For
            Next entryCurrent
            For Each entryCurrent As ManagementObject In objOS
                str += ", OSName :" & entryCurrent("name")
                str += ", Version :" & entryCurrent("version")
                str += ", Computer Name :" & entryCurrent("csname")
                entryCurrent.Dispose()
                Exit For
            Next entryCurrent
            For Each entryCurrent As ManagementObject In objNW

                Dim addresses As String() = DirectCast(entryCurrent("IPAddress"), String())
                If Not addresses Is Nothing Then
                    For Each ipaddress As String In addresses
                        str += ",IPAddress : " & ipaddress
                    Next
                End If

                ' IPSubnets, probably have more than one value

                Dim subnets As String() = DirectCast(entryCurrent("IPSubnet"), String())
                If Not subnets Is Nothing Then
                    For Each ipsubnet As String In subnets
                        str += ",IPSubnet : " & ipsubnet
                    Next
                End If
                str += ",MacAddress : " & entryCurrent("MacAddress").ToString()

                entryCurrent.Dispose()
                Exit For
            Next entryCurrent

            Return str
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
        End Try
    End Function

    Public Shared Function CreateDataTable_LinkIncident() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim ROW_ID As New DataColumn("ROW_ID", System.Type.GetType("System.String"))
            Dim IM_ID As New DataColumn("IM_ID", System.Type.GetType("System.String"))
            Dim CAT_DESCR As New DataColumn("CAT_DESCR", System.Type.GetType("System.String"))
            Dim IM_SHORT_DESCR As New DataColumn("IM_SHORT_DESCR", System.Type.GetType("System.String"))
            Dim IM_DOC_NO As New DataColumn("IM_DOC_NO", System.Type.GetType("System.String"))
            Dim LEVEL_DESCR As New DataColumn("LEVEL_DESCR", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ROW_ID)
            dtDt.Columns.Add(IM_ID)
            dtDt.Columns.Add(CAT_DESCR)
            dtDt.Columns.Add(IM_SHORT_DESCR)
            dtDt.Columns.Add(IM_DOC_NO)
            dtDt.Columns.Add(LEVEL_DESCR)
            dtDt.Columns.Add(STATUS)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
            Return dtDt
        End Try
    End Function

    Public Shared Function CreatedDateTable_INCIDENT_D() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim ROW_ID As New DataColumn("ROW_ID", System.Type.GetType("System.Int32"))
            Dim ID_ID As New DataColumn("ID_ID", System.Type.GetType("System.Int32"))
            Dim ID_IM_ID As New DataColumn("ID_IM_ID", System.Type.GetType("System.Int32"))
            Dim ID_TYPE As New DataColumn("ID_TYPE", System.Type.GetType("System.String"))
            Dim ID_OTHER As New DataColumn("ID_OTHER", System.Type.GetType("System.String"))
            Dim ID_DESCR As New DataColumn("ID_DESCR", System.Type.GetType("System.String"))
            Dim ID_NAME As New DataColumn("ID_NAME", System.Type.GetType("System.String"))
            Dim ID_EMAIL As New DataColumn("ID_EMAIL", System.Type.GetType("System.String"))
            Dim ID_PHONE As New DataColumn("ID_PHONE", System.Type.GetType("System.String"))
            Dim ID_AMT As New DataColumn("ID_AMT", System.Type.GetType("System.String"))
            Dim ID_REMARKS As New DataColumn("ID_REMARKS", System.Type.GetType("System.String"))
            Dim ID_GRD_CODE As New DataColumn("ID_GRP_CODE", System.Type.GetType("System.String"))
            Dim bEDIT As New DataColumn("bEDIT", System.Type.GetType("System.Boolean"))
            Dim ID_STATUS As New DataColumn("ID_STATUS", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ROW_ID)
            dtDt.Columns.Add(ID_ID)
            dtDt.Columns.Add(ID_IM_ID)
            dtDt.Columns.Add(ID_TYPE)
            dtDt.Columns.Add(ID_OTHER)
            dtDt.Columns.Add(ID_DESCR)
            dtDt.Columns.Add(ID_NAME)
            dtDt.Columns.Add(ID_EMAIL)
            dtDt.Columns.Add(ID_PHONE)
            dtDt.Columns.Add(ID_AMT)
            dtDt.Columns.Add(ID_REMARKS)
            dtDt.Columns.Add(ID_GRD_CODE)
            dtDt.Columns.Add(bEDIT)
            dtDt.Columns.Add(ID_STATUS)

            Return dtDt

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
            Return dtDt
        End Try
    End Function
    Public Shared Function CreatedDateTable_INCIDENT_ALERT() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim ROW_ID As New DataColumn("ROW_ID", System.Type.GetType("System.Int32"))
            Dim IALE_ID As New DataColumn("IALE_ID", System.Type.GetType("System.Int32"))
            Dim IALE_IM_ID As New DataColumn("IALE_IM_ID", System.Type.GetType("System.Int32"))
            Dim IALE_AM_ID As New DataColumn("IALE_AM_ID", System.Type.GetType("System.String"))
            Dim IALE_OTHER As New DataColumn("IALE_OTHER", System.Type.GetType("System.String"))
            Dim IALE_TYPE As New DataColumn("IALE_TYPE", System.Type.GetType("System.String"))
            Dim IALE_REF_ID As New DataColumn("IALE_REF_ID", System.Type.GetType("System.String"))
            Dim IALE_NAME As New DataColumn("IALE_NAME", System.Type.GetType("System.String"))
            Dim IALE_EMAIL_ADDR As New DataColumn("IALE_EMAIL_ADDR", System.Type.GetType("System.String"))
            Dim IALE_SMS_NO As New DataColumn("IALE_SMS_NO", System.Type.GetType("System.String"))
            Dim IALE_EMAIL_MSG As New DataColumn("IALE_EMAIL_MSG", System.Type.GetType("System.String"))
            Dim IALE_SMS_MSG As New DataColumn("IALE_SMS_MSG", System.Type.GetType("System.String"))
            Dim IALE_EMAIL_COUNT As New DataColumn("IALE_EMAIL_COUNT", System.Type.GetType("System.String"))
            Dim IALE_SMS_COUNT As New DataColumn("IALE_SMS_COUNT", System.Type.GetType("System.String"))
            Dim bADD As New DataColumn("bADD", System.Type.GetType("System.Boolean"))
            Dim ENABLE_DELETE As New DataColumn("ENABLE_DELETE", System.Type.GetType("System.Boolean"))
            Dim IALE_STATUS As New DataColumn("IALE_STATUS", System.Type.GetType("System.String"))
            dtDt.Columns.Add(ROW_ID)
            dtDt.Columns.Add(IALE_ID)
            dtDt.Columns.Add(IALE_IM_ID)
            dtDt.Columns.Add(IALE_AM_ID)
            dtDt.Columns.Add(IALE_OTHER)
            dtDt.Columns.Add(IALE_TYPE)
            dtDt.Columns.Add(IALE_REF_ID)
            dtDt.Columns.Add(IALE_NAME)
            dtDt.Columns.Add(IALE_EMAIL_ADDR)
            dtDt.Columns.Add(IALE_SMS_NO)
            dtDt.Columns.Add(IALE_EMAIL_MSG)
            dtDt.Columns.Add(IALE_SMS_MSG)
            dtDt.Columns.Add(IALE_EMAIL_COUNT)
            dtDt.Columns.Add(IALE_SMS_COUNT)
            dtDt.Columns.Add(bADD) 'other information to be added to lookup
            dtDt.Columns.Add(ENABLE_DELETE) 'to disable delete link
            dtDt.Columns.Add(IALE_STATUS)

            Return dtDt

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Create table INCIDENT_ALERT")
            Return dtDt
        End Try
    End Function
    Public Shared Function CreatedDateTable_INCIDENT_FOLLOW_UP() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim ROW_ID As New DataColumn("ROW_ID", System.Type.GetType("System.Int32"))
            Dim IFU_ID As New DataColumn("IFU_ID", System.Type.GetType("System.Int32"))
            Dim IFU_IM_ID As New DataColumn("IFU_IM_ID", System.Type.GetType("System.String"))
            Dim IFU_DT As New DataColumn("IFU_DT", System.Type.GetType("System.String"))
            Dim IFU_USR_ID As New DataColumn("IFU_USR_ID", System.Type.GetType("System.String"))
            Dim IFU_REMARKS As New DataColumn("IFU_REMARKS", System.Type.GetType("System.String"))
            Dim bEDIT As New DataColumn("bEDIT", System.Type.GetType("System.Boolean"))
            Dim IFU_STATUS As New DataColumn("IFU_STATUS", System.Type.GetType("System.String"))
            dtDt.Columns.Add(ROW_ID)
            dtDt.Columns.Add(IFU_ID)
            dtDt.Columns.Add(IFU_IM_ID)
            dtDt.Columns.Add(IFU_DT)
            dtDt.Columns.Add(IFU_USR_ID)
            dtDt.Columns.Add(IFU_REMARKS)
            dtDt.Columns.Add(bEDIT)
            dtDt.Columns.Add(IFU_STATUS)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
            Return dtDt
        End Try
    End Function
    Public Shared Function CreatedDateTable_Victim() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim ROW_ID As New DataColumn("ROW_ID", System.Type.GetType("System.Int32"))
            Dim IV_ID As New DataColumn("IV_ID", System.Type.GetType("System.Int32"))
            Dim IV_VM_ID As New DataColumn("IV_VM_ID", System.Type.GetType("System.String"))
            Dim IV_IM_ID As New DataColumn("IV_IM_ID", System.Type.GetType("System.String"))
            Dim IV_OTHER As New DataColumn("IV_OTHER", System.Type.GetType("System.String"))
            Dim IV_bADD As New DataColumn("IV_bADD", System.Type.GetType("System.Boolean"))
            Dim VIC_TYPE As New DataColumn("VIC_TYPE", System.Type.GetType("System.String"))
            Dim IV_REMARKS As New DataColumn("IV_REMARKS", System.Type.GetType("System.String"))
            Dim IV_REF_ID As New DataColumn("IV_REF_ID", System.Type.GetType("System.String"))
            Dim IV_NAME As New DataColumn("IV_NAME", System.Type.GetType("System.String"))
            Dim IV_EMAIL As New DataColumn("IV_EMAIL", System.Type.GetType("System.String"))
            Dim IV_MOB_NO As New DataColumn("IV_MOB_NO", System.Type.GetType("System.String"))
            Dim IV_STATUS As New DataColumn("IV_STATUS", System.Type.GetType("System.String"))
            dtDt.Columns.Add(ROW_ID)
            dtDt.Columns.Add(IV_ID)
            dtDt.Columns.Add(IV_VM_ID)
            dtDt.Columns.Add(IV_IM_ID)
            dtDt.Columns.Add(IV_OTHER)
            dtDt.Columns.Add(IV_bADD)
            dtDt.Columns.Add(VIC_TYPE)
            dtDt.Columns.Add(IV_REF_ID)
            dtDt.Columns.Add(IV_NAME)
            dtDt.Columns.Add(IV_EMAIL)
            dtDt.Columns.Add(IV_MOB_NO)
            dtDt.Columns.Add(IV_STATUS)
            dtDt.Columns.Add(IV_REMARKS)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
            Return dtDt
        End Try
    End Function

    Public Shared Function CreatedDateTable_INCIDENT_INJURY() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim ROW_ID As New DataColumn("ROW_ID", System.Type.GetType("System.Int32"))
            Dim IINJ_ID As New DataColumn("IINJ_ID", System.Type.GetType("System.Int32"))
            Dim IINJ_IM_ID As New DataColumn("IINJ_IM_ID", System.Type.GetType("System.Int32"))
            Dim IINJ_INJ_ID As New DataColumn("IINJ_INJ_ID", System.Type.GetType("System.String"))
            Dim IINJ_TYPE As New DataColumn("IINJ_TYPE", System.Type.GetType("System.String"))
            Dim IINJ_INJ_S_ID As New DataColumn("IINJ_INJ_S_ID", System.Type.GetType("System.String"))
            Dim IINJ_OTHER As New DataColumn("IINJ_OTHER", System.Type.GetType("System.String"))
            Dim IINJ_bADD As New DataColumn("IINJ_bADD", System.Type.GetType("System.Boolean"))
            Dim ACT_TAKEN As New DataColumn("ACT_TAKEN", System.Type.GetType("System.String"))
            Dim IINJ_KNOWN_PERSON As New DataColumn("IINJ_KNOWN_PERSON", System.Type.GetType("System.String"))
            Dim IINJ_IOCR_ID As New DataColumn("IINJ_IOCR_ID", System.Type.GetType("System.String"))
            Dim IOCR_DESCR As New DataColumn("IOCR_DESCR", System.Type.GetType("System.String"))
            Dim IV_VM_ID As New DataColumn("IV_VM_ID", System.Type.GetType("System.String"))
            Dim IV_OTHER As New DataColumn("IV_OTHER", System.Type.GetType("System.String"))
            Dim IV_bADD As New DataColumn("IV_bADD", System.Type.GetType("System.Boolean"))
            Dim VIC_TYPE As New DataColumn("VIC_TYPE", System.Type.GetType("System.String"))
            Dim IV_REMARKS As New DataColumn("IV_REMARKS", System.Type.GetType("System.String"))
            Dim IV_REF_ID As New DataColumn("IV_REF_ID", System.Type.GetType("System.String"))
            Dim IV_NAME As New DataColumn("IV_NAME", System.Type.GetType("System.String"))
            Dim IV_EMAIL As New DataColumn("IV_EMAIL", System.Type.GetType("System.String"))
            Dim IV_MOB_NO As New DataColumn("IV_MOB_NO", System.Type.GetType("System.String"))
            Dim IINJ_IOCR_REMARKS As New DataColumn("IINJ_IOCR_REMARKS", System.Type.GetType("System.String"))
            Dim IINJ_STATUS As New DataColumn("IINJ_STATUS", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ROW_ID)
            dtDt.Columns.Add(IINJ_ID)
            dtDt.Columns.Add(IINJ_IM_ID)
            dtDt.Columns.Add(IINJ_INJ_ID)
            dtDt.Columns.Add(IINJ_TYPE)
            dtDt.Columns.Add(IINJ_INJ_S_ID)
            dtDt.Columns.Add(IINJ_OTHER)
            dtDt.Columns.Add(IINJ_bADD)
            dtDt.Columns.Add(ACT_TAKEN)
            dtDt.Columns.Add(IINJ_KNOWN_PERSON)
            dtDt.Columns.Add(IINJ_IOCR_ID)
            dtDt.Columns.Add(IOCR_DESCR)
            dtDt.Columns.Add(IV_VM_ID)
            dtDt.Columns.Add(IV_OTHER)
            dtDt.Columns.Add(IV_bADD)
            dtDt.Columns.Add(VIC_TYPE)
            dtDt.Columns.Add(IV_REMARKS)
            dtDt.Columns.Add(IV_REF_ID)
            dtDt.Columns.Add(IV_NAME)
            dtDt.Columns.Add(IV_EMAIL)
            dtDt.Columns.Add(IV_MOB_NO)
            dtDt.Columns.Add(IINJ_IOCR_REMARKS)

            dtDt.Columns.Add(IINJ_STATUS)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
            Return dtDt
        End Try
    End Function

    Public Shared Function CreatedDateTable_INCIDENT_DAM_PRO() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim ROW_ID As New DataColumn("ROW_ID", System.Type.GetType("System.Int32"))
            Dim IPRO_ID As New DataColumn("IPRO_ID", System.Type.GetType("System.Int32"))
            Dim IPRO_IM_ID As New DataColumn("IPRO_IM_ID", System.Type.GetType("System.Int32"))
            Dim IPRO_DM_ID As New DataColumn("IPRO_DM_ID", System.Type.GetType("System.Int32"))
            Dim DM_DESCR As New DataColumn("DM_DESCR", System.Type.GetType("System.String"))
            Dim IPRO_PM_ID As New DataColumn("IPRO_PM_ID", System.Type.GetType("System.String"))
            Dim IPRO_PM_OTHER As New DataColumn("IPRO_PM_OTHER", System.Type.GetType("System.String"))
            Dim PM_bADD As New DataColumn("PM_bADD", System.Type.GetType("System.Boolean"))
            Dim PRO_TYPE As New DataColumn("PRO_TYPE", System.Type.GetType("System.String"))
            Dim IPRO_PS_ID As New DataColumn("IPRO_PS_ID", System.Type.GetType("System.String"))
            Dim IPRO_PS_OTHER As New DataColumn("IPRO_PS_OTHER", System.Type.GetType("System.String"))
            Dim PS_bADD As New DataColumn("PS_bADD", System.Type.GetType("System.Boolean"))
            Dim PRO_SUB_TYPE As New DataColumn("PRO_SUB_TYPE", System.Type.GetType("System.String"))
            Dim IPRO_DESCR As New DataColumn("IPRO_DESCR", System.Type.GetType("System.String"))
            Dim IPRO_NAME As New DataColumn("IPRO_NAME", System.Type.GetType("System.String"))
            Dim DISPLAY_NAME As New DataColumn("DISPLAY_NAME", System.Type.GetType("System.String"))
            Dim IPRO_REF_ID As New DataColumn("IPRO_REF_ID", System.Type.GetType("System.String"))
            Dim IPRO_PLATE_NO As New DataColumn("IPRO_PLATE_NO", System.Type.GetType("System.String"))
            Dim IPRO_EMAIL As New DataColumn("IPRO_EMAIL", System.Type.GetType("System.String"))
            Dim IPRO_MOB_NO As New DataColumn("IPRO_MOB_NO", System.Type.GetType("System.String"))
            Dim IPRO_AMT As New DataColumn("IPRO_AMT", System.Type.GetType("System.String"))
            Dim IPRO_REMARKS As New DataColumn("IPRO_REMARKS", System.Type.GetType("System.String"))
            Dim bEDIT As New DataColumn("bEDIT", System.Type.GetType("System.Boolean"))
            Dim IPRO_STATUS As New DataColumn("IPRO_STATUS", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ROW_ID)
            dtDt.Columns.Add(IPRO_ID)
            dtDt.Columns.Add(IPRO_IM_ID)
            dtDt.Columns.Add(IPRO_DM_ID)
            dtDt.Columns.Add(DM_DESCR)
            dtDt.Columns.Add(IPRO_PM_ID)
            dtDt.Columns.Add(IPRO_PM_OTHER)
            dtDt.Columns.Add(PM_bADD)
            dtDt.Columns.Add(PRO_TYPE)
            dtDt.Columns.Add(IPRO_PS_ID)
            dtDt.Columns.Add(IPRO_PS_OTHER)
            dtDt.Columns.Add(PS_bADD)
            dtDt.Columns.Add(PRO_SUB_TYPE)
            dtDt.Columns.Add(IPRO_DESCR)
            dtDt.Columns.Add(IPRO_NAME)
            dtDt.Columns.Add(DISPLAY_NAME)
            dtDt.Columns.Add(IPRO_REF_ID)
            dtDt.Columns.Add(IPRO_PLATE_NO)
            dtDt.Columns.Add(IPRO_EMAIL)
            dtDt.Columns.Add(IPRO_MOB_NO)
            dtDt.Columns.Add(IPRO_AMT)
            dtDt.Columns.Add(IPRO_REMARKS)
            dtDt.Columns.Add(bEDIT)
            dtDt.Columns.Add(IPRO_STATUS)

            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
            Return dtDt
        End Try
    End Function
    Public Shared Function CreatedDateTable_INCIDENT_VEHICLE() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim ROW_ID As New DataColumn("ROW_ID", System.Type.GetType("System.Int32"))
            Dim IVEH_ID As New DataColumn("IVEH_ID", System.Type.GetType("System.Int32"))
            Dim IVEH_IM_ID As New DataColumn("IVEH_IM_ID", System.Type.GetType("System.Int32"))
            Dim IVEH_VEH_ID As New DataColumn("IVEH_VEH_ID", System.Type.GetType("System.String"))
            Dim IVEH_OTHER As New DataColumn("IVEH_OTHER", System.Type.GetType("System.String"))
            Dim VEH_bADD As New DataColumn("VEH_bADD", System.Type.GetType("System.Boolean"))
            Dim VEH_CATEGORY As New DataColumn("VEH_CATEGORY", System.Type.GetType("System.String"))

            Dim IVEH_VS_ID As New DataColumn("IVEH_VS_ID", System.Type.GetType("System.String"))
            Dim IVEH_VS_OTHER As New DataColumn("IVEH_VS_OTHER", System.Type.GetType("System.String"))
            Dim VS_bADD As New DataColumn("VS_bADD", System.Type.GetType("System.Boolean"))
            Dim VEH_TYPE As New DataColumn("VEH_TYPE", System.Type.GetType("System.String"))

            Dim IVEH_DM_ID As New DataColumn("IVEH_DM_ID", System.Type.GetType("System.String"))
            Dim VEH_DM_TYPE As New DataColumn("VEH_DM_TYPE", System.Type.GetType("System.String"))
            Dim IVEH_REF_ID As New DataColumn("IVEH_REF_ID", System.Type.GetType("System.String"))
            Dim IVEH_PLATE_NO As New DataColumn("IVEH_PLATE_NO", System.Type.GetType("System.String"))
            Dim IVEH_OWNER_NAME As New DataColumn("IVEH_OWNER_NAME", System.Type.GetType("System.String"))
            Dim IVEH_DAMAGE_REMARKS As New DataColumn("IVEH_DAMAGE_REMARKS", System.Type.GetType("System.String"))
            Dim IVEH_STATUS As New DataColumn("IVEH_STATUS", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ROW_ID)
            dtDt.Columns.Add(IVEH_ID)
            dtDt.Columns.Add(IVEH_IM_ID)
            dtDt.Columns.Add(IVEH_VEH_ID)
            dtDt.Columns.Add(IVEH_OTHER)
            dtDt.Columns.Add(VEH_bADD)
            dtDt.Columns.Add(VEH_CATEGORY)
            dtDt.Columns.Add(IVEH_VS_ID)
            dtDt.Columns.Add(IVEH_VS_OTHER)
            dtDt.Columns.Add(VS_bADD)
            dtDt.Columns.Add(VEH_TYPE)

            dtDt.Columns.Add(IVEH_DM_ID)
            dtDt.Columns.Add(VEH_DM_TYPE)
            dtDt.Columns.Add(IVEH_REF_ID)
            dtDt.Columns.Add(IVEH_PLATE_NO)
            dtDt.Columns.Add(IVEH_OWNER_NAME)
            dtDt.Columns.Add(IVEH_DAMAGE_REMARKS)
            dtDt.Columns.Add(IVEH_STATUS)


            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
            Return dtDt
        End Try
    End Function
    Public Shared Function CreatedDateTable_ALERT_USERS() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim ROW_ID As New DataColumn("ROW_ID", System.Type.GetType("System.Int32"))
            Dim AUM_ID As New DataColumn("AUM_ID", System.Type.GetType("System.Int32"))
            Dim AUM_EMP_ID As New DataColumn("AUM_EMP_ID", System.Type.GetType("System.Int32"))
            Dim AUM_BSU_ID As New DataColumn("AUM_BSU_ID", System.Type.GetType("System.String"))
            Dim ENAME As New DataColumn("ENAME", System.Type.GetType("System.String"))
            Dim AUM_EMAIL As New DataColumn("AUM_EMAIL", System.Type.GetType("System.String"))
            Dim AUM_MOBILE As New DataColumn("AUM_MOBILE", System.Type.GetType("System.String"))
            Dim AUM_bALL_UNIT As New DataColumn("AUM_bALL_UNIT", System.Type.GetType("System.Boolean"))
            Dim AUM_bEDITED1 As New DataColumn("AUM_bEDITED1", System.Type.GetType("System.Boolean"))
            Dim AUM_bEDITED2 As New DataColumn("AUM_bEDITED2", System.Type.GetType("System.Boolean"))
            Dim AUM_bEDITED3 As New DataColumn("AUM_bEDITED3", System.Type.GetType("System.Boolean"))
            Dim EDIT_FLAG As New DataColumn("EDIT_FLAG", System.Type.GetType("System.Boolean"))


            Dim AUS_bEMAIL1 As New DataColumn("AUS_bEMAIL1", System.Type.GetType("System.Boolean"))
            Dim AUS_bEMAIL2 As New DataColumn("AUS_bEMAIL2", System.Type.GetType("System.Boolean"))
            Dim AUS_bEMAIL3 As New DataColumn("AUS_bEMAIL3", System.Type.GetType("System.Boolean"))
            Dim EMAIL_FLAG As New DataColumn("EMAIL_FLAG", System.Type.GetType("System.Boolean"))
            Dim EMAIL_IMG_FLAG As New DataColumn("EMAIL_IMG_FLAG", System.Type.GetType("System.String"))
            Dim AUS_bSMS1 As New DataColumn("AUS_bSMS1", System.Type.GetType("System.Boolean"))
            Dim AUS_bSMS2 As New DataColumn("AUS_bSMS2", System.Type.GetType("System.Boolean"))
            Dim AUS_bSMS3 As New DataColumn("AUS_bSMS3", System.Type.GetType("System.Boolean"))
            Dim SMS_FLAG As New DataColumn("SMS_FLAG", System.Type.GetType("System.Boolean"))
            Dim SMS_IMG_FLAG As New DataColumn("SMS_IMG_FLAG", System.Type.GetType("System.String"))
            Dim AUS_STATUS As New DataColumn("AUS_STATUS", System.Type.GetType("System.String"))
            Dim SHOW_BUTTON As New DataColumn("SHOW_BUTTON", System.Type.GetType("System.Boolean"))
            dtDt.Columns.Add(ROW_ID)
            dtDt.Columns.Add(AUM_ID)
            dtDt.Columns.Add(AUM_BSU_ID)
            dtDt.Columns.Add(AUM_EMP_ID)
            dtDt.Columns.Add(ENAME)
            dtDt.Columns.Add(AUM_EMAIL)
            dtDt.Columns.Add(AUM_MOBILE)
            dtDt.Columns.Add(AUM_bALL_UNIT)
            dtDt.Columns.Add(AUM_bEDITED1)
            dtDt.Columns.Add(AUM_bEDITED2)
            dtDt.Columns.Add(AUM_bEDITED3)
            dtDt.Columns.Add(EDIT_FLAG)
            dtDt.Columns.Add(AUS_bEMAIL1)
            dtDt.Columns.Add(AUS_bEMAIL2)
            dtDt.Columns.Add(AUS_bEMAIL3)
            dtDt.Columns.Add(EMAIL_FLAG)
            dtDt.Columns.Add(EMAIL_IMG_FLAG)
            dtDt.Columns.Add(AUS_bSMS1)
            dtDt.Columns.Add(AUS_bSMS2)
            dtDt.Columns.Add(AUS_bSMS3)
            dtDt.Columns.Add(SMS_FLAG)
            dtDt.Columns.Add(SMS_IMG_FLAG)
            dtDt.Columns.Add(AUS_STATUS)
            dtDt.Columns.Add(SHOW_BUTTON)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
            Return dtDt
        End Try
    End Function
    Public Shared Function CreatedDateTable_INCIDENT_LOC() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim ROW_ID As New DataColumn("ROW_ID", System.Type.GetType("System.Int32"))
            Dim ILOC_ID As New DataColumn("ILOC_ID", System.Type.GetType("System.Int32"))
            Dim ILOC_IM_ID As New DataColumn("ILOC_IM_ID", System.Type.GetType("System.Int32"))
            Dim ILOC_LOC_ID As New DataColumn("ILOC_LOC_ID", System.Type.GetType("System.String"))
            Dim ILOC_OTHER As New DataColumn("ILOC_OTHER", System.Type.GetType("System.String"))
            Dim LOC_bADD As New DataColumn("LOC_bADD", System.Type.GetType("System.Boolean"))
            Dim LOC As New DataColumn("LOC", System.Type.GetType("System.String"))
            Dim ILOC_LS_ID As New DataColumn("ILOC_LS_ID", System.Type.GetType("System.String"))
            Dim ILOC_LS_OTHER As New DataColumn("ILOC_LS_OTHER", System.Type.GetType("System.String"))
            Dim LS_bADD As New DataColumn("LS_bADD", System.Type.GetType("System.Boolean"))
            Dim SUB_LOC As New DataColumn("SUB_LOC", System.Type.GetType("System.String"))
            Dim ILOC_AMT As New DataColumn("ILOC_AMT", System.Type.GetType("System.String"))
            Dim ILOC_REMARKS As New DataColumn("ILOC_REMARKS", System.Type.GetType("System.String"))
            Dim bEDIT As New DataColumn("bEDIT", System.Type.GetType("System.Boolean"))
            Dim ILOC_STATUS As New DataColumn("ILOC_STATUS", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ROW_ID)
            dtDt.Columns.Add(ILOC_ID)
            dtDt.Columns.Add(ILOC_IM_ID)
            dtDt.Columns.Add(ILOC_LOC_ID)
            dtDt.Columns.Add(ILOC_OTHER)
            dtDt.Columns.Add(LOC_bADD)
            dtDt.Columns.Add(LOC)
            dtDt.Columns.Add(ILOC_LS_ID)
            dtDt.Columns.Add(ILOC_LS_OTHER)
            dtDt.Columns.Add(LS_bADD)
            dtDt.Columns.Add(SUB_LOC)
            dtDt.Columns.Add(ILOC_AMT)
            dtDt.Columns.Add(ILOC_REMARKS)
            dtDt.Columns.Add(bEDIT)
            dtDt.Columns.Add(ILOC_STATUS)


            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
            Return dtDt
        End Try
    End Function
    Public Shared Function CreatedDateTable_INCIDENT_SUSPECT() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim ROW_ID As New DataColumn("ROW_ID", System.Type.GetType("System.Int32"))
            Dim ISUS_ID As New DataColumn("ISUS_ID", System.Type.GetType("System.Int32"))
            Dim ISUS_IM_ID As New DataColumn("ISUS_IM_ID", System.Type.GetType("System.Int32"))
            Dim ISUS_SUS_ID As New DataColumn("ISUS_SUS_ID", System.Type.GetType("System.String"))
            Dim ISUS_OTHER As New DataColumn("ISUS_OTHER", System.Type.GetType("System.String"))
            Dim ISUS_REF_ID As New DataColumn("ISUS_REF_ID", System.Type.GetType("System.String"))
            Dim ISUS_bADD As New DataColumn("ISUS_bADD", System.Type.GetType("System.Boolean"))
            Dim SUS_TYPE As New DataColumn("SUS_TYPE", System.Type.GetType("System.String"))
            Dim ISUS_NAME As New DataColumn("ISUS_NAME", System.Type.GetType("System.String"))
            Dim ISUS_EMAIL As New DataColumn("ISUS_EMAIL", System.Type.GetType("System.String"))
            Dim ISUS_MOB_NO As New DataColumn("ISUS_MOB_NO", System.Type.GetType("System.String"))
            Dim ISUS_REMARKS As New DataColumn("ISUS_REMARKS", System.Type.GetType("System.String"))
            Dim ISUS_OM_ID As New DataColumn("ISUS_OM_ID", System.Type.GetType("System.String"))
            Dim ISUS_ADDRESS As New DataColumn("ISUS_ADDRESS", System.Type.GetType("System.String"))
            Dim OM_DESCR As New DataColumn("OM_DESCR", System.Type.GetType("System.String"))
            Dim ISUS_STATUS As New DataColumn("ISUS_STATUS", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ROW_ID)
            dtDt.Columns.Add(ISUS_ID)
            dtDt.Columns.Add(ISUS_IM_ID)
            dtDt.Columns.Add(ISUS_SUS_ID)
            dtDt.Columns.Add(ISUS_OTHER)
            dtDt.Columns.Add(ISUS_REF_ID)
            dtDt.Columns.Add(ISUS_bADD)
            dtDt.Columns.Add(SUS_TYPE)
            dtDt.Columns.Add(ISUS_NAME)
            dtDt.Columns.Add(ISUS_EMAIL)
            dtDt.Columns.Add(ISUS_MOB_NO)
            dtDt.Columns.Add(ISUS_REMARKS)
            dtDt.Columns.Add(ISUS_OM_ID)
            dtDt.Columns.Add(ISUS_ADDRESS)
            dtDt.Columns.Add(OM_DESCR)
            dtDt.Columns.Add(ISUS_STATUS)


            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
            Return dtDt
        End Try
    End Function






    Public Shared Function CreatedDateTable_INCIDENT_WITNESS() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim ROW_ID As New DataColumn("ROW_ID", System.Type.GetType("System.Int32"))
            Dim IWIT_ID As New DataColumn("IWIT_ID", System.Type.GetType("System.Int32"))
            Dim IWIT_IM_ID As New DataColumn("IWIT_IM_ID", System.Type.GetType("System.Int32"))
            Dim IWIT_WIT_ID As New DataColumn("IWIT_WIT_ID", System.Type.GetType("System.String"))
            Dim IWIT_OTHER As New DataColumn("IWIT_OTHER", System.Type.GetType("System.String"))
            Dim IWIT_bADD As New DataColumn("IWIT_bADD", System.Type.GetType("System.Boolean"))
            Dim IWIT_TYPE As New DataColumn("IWIT_TYPE", System.Type.GetType("System.String"))
            Dim IWIT_REF_ID As New DataColumn("IWIT_REF_ID", System.Type.GetType("System.String"))
            Dim IWIT_NAME As New DataColumn("IWIT_NAME", System.Type.GetType("System.String"))
            Dim IWIT_EMAIL As New DataColumn("IWIT_EMAIL", System.Type.GetType("System.String"))
            Dim IWIT_MOB_NO As New DataColumn("IWIT_MOB_NO", System.Type.GetType("System.String"))
            Dim IWIT_ADDRESS As New DataColumn("IWIT_ADDRESS", System.Type.GetType("System.String"))
            Dim IWIT_REMARKS As New DataColumn("IWIT_REMARKS", System.Type.GetType("System.String"))
            Dim IWIT_STATUS As New DataColumn("IWIT_STATUS", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ROW_ID)
            dtDt.Columns.Add(IWIT_ID)
            dtDt.Columns.Add(IWIT_IM_ID)
            dtDt.Columns.Add(IWIT_WIT_ID)
            dtDt.Columns.Add(IWIT_OTHER)
            dtDt.Columns.Add(IWIT_bADD)
            dtDt.Columns.Add(IWIT_TYPE)
            dtDt.Columns.Add(IWIT_REF_ID)
            dtDt.Columns.Add(IWIT_NAME)
            dtDt.Columns.Add(IWIT_EMAIL)
            dtDt.Columns.Add(IWIT_MOB_NO)
            dtDt.Columns.Add(IWIT_ADDRESS)
            dtDt.Columns.Add(IWIT_REMARKS)
            dtDt.Columns.Add(IWIT_STATUS)


            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
            Return dtDt
        End Try
    End Function
End Class
