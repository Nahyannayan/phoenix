Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class BUDGET_H

    Dim vBUH_ID As Integer
    Dim vBUH_BSU_ID As String
    Dim vBUH_FYEAR As Integer
    Dim vBUH_Months As Integer
    Dim vBUH_BUH_ID As Integer
    Dim vBUH_bActive As Boolean
    Dim vBUH_DOCDT As DateTime
    Dim vBUH_DTFROM As DateTime
    Dim vBUH_LASTUPDATE As DateTime
    Dim vBUH_REMARKS As String
    Dim vNEW_BUH_ID As String
    Dim vBUH_bFinalize As Boolean
    Dim vEdit As Boolean
    Dim vDelete As Boolean
    Dim vBUH_EXGRATE As Decimal
    

    ''' <summary>
    '''  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Delete() As Boolean
        Get
            Return vDelete
        End Get
        Set(ByVal value As Boolean)
            vDelete = value
        End Set
    End Property


    ''' <summary>
    '''  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Edit() As Boolean
        Get
            Return vEdit
        End Get
        Set(ByVal value As Boolean)
            vEdit = value
        End Set
    End Property

 
    ''' <summary>
    '''  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUH_ID() As Integer
        Get
            Return vBUH_ID
        End Get
        Set(ByVal value As Integer)
            vBUH_ID = value
        End Set
    End Property

    Public Property BUH_EXGRATE() As Decimal
        Get
            Return vBUH_EXGRATE
        End Get
        Set(ByVal value As Decimal)
            vBUH_EXGRATE = value
        End Set
    End Property
    ''' <summary>
    '''  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUH_BSU_ID() As String
        Get
            Return vBUH_BSU_ID
        End Get
        Set(ByVal value As String)
            vBUH_BSU_ID = value
        End Set
    End Property

    ''' <summary>
    '''  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUH_FYEAR() As Integer
        Get
            Return vBUH_FYEAR
        End Get
        Set(ByVal value As Integer)
            vBUH_FYEAR = value
        End Set
    End Property

    ''' <summary>
    '''  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUH_Months() As Integer
        Get
            Return vBUH_Months
        End Get
        Set(ByVal value As Integer)
            vBUH_Months = value
        End Set
    End Property

    ''' <summary>
    '''  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUH_BUH_ID() As Integer
        Get
            Return vBUH_BUH_ID
        End Get
        Set(ByVal value As Integer)
            vBUH_BUH_ID = value
        End Set
    End Property

    ''' <summary>
    '''  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUH_bActive() As Boolean
        Get
            Return vBUH_bActive
        End Get
        Set(ByVal value As Boolean)
            vBUH_bActive = value
        End Set
    End Property

    ''' <summary>
    '''  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUH_DOCDT() As DateTime
        Get
            Return vBUH_DOCDT
        End Get
        Set(ByVal value As DateTime)
            vBUH_DOCDT = value
        End Set
    End Property

    ''' <summary>
    '''  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUH_DTFROM() As DateTime
        Get
            Return vBUH_DTFROM
        End Get
        Set(ByVal value As DateTime)
            vBUH_DTFROM = value
        End Set
    End Property

    ''' <summary>
    '''  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUH_LASTUPDATE() As DateTime
        Get
            Return vBUH_LASTUPDATE
        End Get
        Set(ByVal value As DateTime)
            vBUH_LASTUPDATE = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUH_REMARKS() As String
        Get
            Return vBUH_REMARKS
        End Get
        Set(ByVal value As String)
            vBUH_REMARKS = value
        End Set
    End Property

    Public Property NEW_BUH_ID() As String
        Get
            Return vNEW_BUH_ID
        End Get
        Set(ByVal value As String)
            vNEW_BUH_ID = value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUH_bFinalize() As Boolean
        Get
            Return vBUH_bFinalize
        End Get
        Set(ByVal value As Boolean)
            vBUH_bFinalize = value
        End Set
    End Property


    'Public Shared Function SaveDetails(ByVal vBUDGET_H As BUDGET_H) As Integer

    '    ' The code to save the details

    '    SaveSubDetails(vBUDGET_H.SUBDETAILS)
    'End Function

    Public Function SaveBUDGET_H(ByVal dtSubdetails As DataTable, ByVal p_stTrans As SqlTransaction) As Integer
        If vDelete Then
            'delete here
            Exit Function
        End If

        If Not vEdit Then
            vBUH_ID = 0
        End If
        '@return_value = [dbo].[SaveBUDGET_H]
        '		@BUH_ID = 0,
        '		@BUH_BSU_ID = N'125016',
        '		@BUH_FYEAR = 2008,
        '		@BUH_DOCDT = N'12-jun-2008',
        '		@BUH_DTFROM = N'12-jun-2009',
        '		@BUH_Months = 12,
        '		@BUH_BUH_ID = NULL,
        '		@BUH_bActive = 1,
        '		@BUH_LASTUPDATE = Null,
        '		@BUH_REMARKS = N'veruthe'
        '@BUH_bFinalize
        Dim pParms(13) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@BUH_ID", SqlDbType.Int)
        pParms(0).Value = BUH_ID
        pParms(1) = New SqlClient.SqlParameter("@BUH_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = vBUH_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@BUH_FYEAR", SqlDbType.VarChar, 20)
        pParms(2).Value = vBUH_FYEAR
        pParms(3) = New SqlClient.SqlParameter("@BUH_DOCDT", SqlDbType.DateTime)
        pParms(3).Value = vBUH_DOCDT
        pParms(4) = New SqlClient.SqlParameter("@BUH_DTFROM", SqlDbType.DateTime)
        pParms(4).Value = vBUH_DTFROM

        pParms(5) = New SqlClient.SqlParameter("@BUH_Months", SqlDbType.Int)
        pParms(5).Value = vBUH_Months
        pParms(6) = New SqlClient.SqlParameter("@BUH_BUH_ID", SqlDbType.Int)
        pParms(6).Value = vBUH_BUH_ID
        pParms(7) = New SqlClient.SqlParameter("@BUH_bActive", SqlDbType.Bit)
        pParms(7).Value = vBUH_bActive
        pParms(8) = New SqlClient.SqlParameter("@BUH_LASTUPDATE", SqlDbType.DateTime)
        pParms(8).Value = vBUH_LASTUPDATE
        pParms(9) = New SqlClient.SqlParameter("@BUH_REMARKS", SqlDbType.VarChar, 200)
        pParms(9).Value = vBUH_REMARKS

        pParms(10) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.ReturnValue
        pParms(11) = New SqlClient.SqlParameter("@NEW_BUH_ID", SqlDbType.Int)
        pParms(11).Direction = ParameterDirection.Output
        pParms(12) = New SqlClient.SqlParameter("@BUH_bFinalize", SqlDbType.Bit)
        pParms(12).Value = vBUH_bFinalize
        pParms(13) = New SqlClient.SqlParameter("@BUH_EXGRATE", SqlDbType.Decimal)
        pParms(13).Value = vBUH_EXGRATE
        Dim retval As Integer

        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveBUDGET_H", pParms)
        retval = pParms(10).Value
        If retval = 0 Then
            vNEW_BUH_ID = pParms(11).Value
            SaveSubDetails(dtSubdetails, p_stTrans)
        Else
            Return retval
        End If

    End Function

    Private Function SaveSubDetails(ByVal p_dtSubdetails As DataTable, ByVal p_stTrans As SqlTransaction) As Integer
        For iRows As Integer = 0 To p_dtSubdetails.Rows.Count - 1
            Dim BudgetDetail As New BUDGET_D(p_dtSubdetails.Rows(iRows), vNEW_BUH_ID, vEdit)
            Dim retval As Integer = BudgetDetail.SaveBUDGET_D(p_stTrans)
            If retval <> 0 Then
                Return retval
            End If
        Next
    End Function

End Class

Public Class BUDGET_D
    Dim vBUD_ID As Integer
    Dim vBUD_BUH_ID As Integer
    Dim vBUD_ACT_ID As String
    Dim vBUD_MONTH1 As Decimal
    Dim vBUD_MONTH2 As Decimal
    Dim vBUD_MONTH3 As Decimal
    Dim vBUD_MONTH4 As Decimal
    Dim vBUD_MONTH5 As Decimal
    Dim vBUD_MONTH6 As Decimal
    Dim vBUD_MONTH7 As Decimal
    Dim vBUD_MONTH8 As Decimal
    Dim vBUD_MONTH9 As Decimal
    Dim vBUD_MONTH10 As Decimal
    Dim vBUD_MONTH11 As Decimal
    Dim vBUD_MONTH12 As Decimal
    Dim vBUD_TOTAL As Decimal
    Dim vBUD_MAIN As String
    Dim vBUH_SUBHEAD As String
    Dim vEdit As Boolean
    Dim vDelete As Boolean

    Sub New(ByVal dtBUDGET_D_Row As DataRow, ByVal BUD_BUH_ID As Integer, ByVal Edit As Boolean)
        vEdit = Edit
        vBUD_ID = dtBUDGET_D_Row("ID")
        vBUD_BUH_ID = BUD_BUH_ID
        vBUD_ACT_ID = dtBUDGET_D_Row("BUD_ACT_ID")
        vBUD_MONTH1 = dtBUDGET_D_Row("BUD_MONTH1")
        vBUD_MONTH2 = dtBUDGET_D_Row("BUD_MONTH2")
        vBUD_MONTH3 = dtBUDGET_D_Row("BUD_MONTH3")
        vBUD_MONTH4 = dtBUDGET_D_Row("BUD_MONTH4")
        vBUD_MONTH5 = dtBUDGET_D_Row("BUD_MONTH5")
        vBUD_MONTH6 = dtBUDGET_D_Row("BUD_MONTH6")
        vBUD_MONTH7 = dtBUDGET_D_Row("BUD_MONTH7")
        vBUD_MONTH8 = dtBUDGET_D_Row("BUD_MONTH8")
        vBUD_MONTH9 = dtBUDGET_D_Row("BUD_MONTH9")
        vBUD_MONTH10 = dtBUDGET_D_Row("BUD_MONTH10")
        vBUD_MONTH11 = dtBUDGET_D_Row("BUD_MONTH11")
        vBUD_MONTH12 = dtBUDGET_D_Row("BUD_MONTH12")
        vBUD_MAIN = IIf(dtBUDGET_D_Row("BUD_MAIN") Is System.DBNull.Value, "", dtBUDGET_D_Row("BUD_MAIN"))
        vBUH_SUBHEAD = IIf(dtBUDGET_D_Row("BUD_SUBHEAD") Is System.DBNull.Value, "", dtBUDGET_D_Row("BUD_SUBHEAD"))
        vBUD_TOTAL = vBUD_MONTH1 + vBUD_MONTH2 + vBUD_MONTH3 + vBUD_MONTH4 + vBUD_MONTH5 + vBUD_MONTH6 + vBUD_MONTH7 + vBUD_MONTH8 + vBUD_MONTH9 + vBUD_MONTH10 + vBUD_MONTH11 + vBUD_MONTH12
        vDelete = False
        If Not Edit Then
            vBUD_ID = 0
        End If
        Select Case dtBUDGET_D_Row("STATUS").ToString.ToUpper
            Case "DELETED"
                vDelete = True
            Case "NEW"
                vBUD_ID = 0
        End Select


    End Sub
    ''' <summary>
    '''  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Delete() As Boolean
        Get
            Return vDelete
        End Get
        Set(ByVal value As Boolean)
            vDelete = value
        End Set
    End Property


    ''' <summary>
    '''  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Edit() As Boolean
        Get
            Return vEdit
        End Get
        Set(ByVal value As Boolean)
            vEdit = value
        End Set
    End Property
    ''' <summary>
    ''' The Indentifier or the autogenerate key (used whn editing)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_ID() As Integer
        Get
            Return vBUD_ID
        End Get
        Set(ByVal value As Integer)
            vBUD_ID = value
        End Set
    End Property

    ''' <summary>
    ''' The Header table reference
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_BUH_ID() As Integer
        Get
            Return vBUD_BUH_ID
        End Get
        Set(ByVal value As Integer)
            vBUD_BUH_ID = value
        End Set
    End Property

    ''' <summary>
    ''' The Account ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_ACT_ID() As String
        Get
            Return vBUD_ACT_ID
        End Get
        Set(ByVal value As String)
            vBUD_ACT_ID = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_MONTH1() As Decimal
        Get
            Return vBUD_MONTH1
        End Get
        Set(ByVal value As Decimal)
            vBUD_MONTH1 = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_MONTH2() As Decimal
        Get
            Return vBUD_MONTH2
        End Get
        Set(ByVal value As Decimal)
            vBUD_MONTH2 = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_MONTH3() As Decimal
        Get
            Return vBUD_MONTH3
        End Get
        Set(ByVal value As Decimal)
            vBUD_MONTH3 = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_MONTH4() As Decimal
        Get
            Return vBUD_MONTH4
        End Get
        Set(ByVal value As Decimal)
            vBUD_MONTH4 = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_MONTH5() As Decimal
        Get
            Return vBUD_MONTH5
        End Get
        Set(ByVal value As Decimal)
            vBUD_MONTH5 = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_MONTH6() As Decimal
        Get
            Return vBUD_MONTH6
        End Get
        Set(ByVal value As Decimal)
            vBUD_MONTH6 = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_MONTH7() As Decimal
        Get
            Return vBUD_MONTH7
        End Get
        Set(ByVal value As Decimal)
            vBUD_MONTH7 = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_MONTH8() As Decimal
        Get
            Return vBUD_MONTH8
        End Get
        Set(ByVal value As Decimal)
            vBUD_MONTH8 = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_MONTH9() As Decimal
        Get
            Return vBUD_MONTH9
        End Get
        Set(ByVal value As Decimal)
            vBUD_MONTH9 = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_MONTH10() As Decimal
        Get
            Return vBUD_MONTH10
        End Get
        Set(ByVal value As Decimal)
            vBUD_MONTH10 = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_MONTH11() As Decimal
        Get
            Return vBUD_MONTH11
        End Get
        Set(ByVal value As Decimal)
            vBUD_MONTH11 = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_MONTH12() As Decimal
        Get
            Return vBUD_MONTH12
        End Get
        Set(ByVal value As Decimal)
            vBUD_MONTH12 = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property BUD_TOTAL() As Decimal
        Get
            Return vBUD_TOTAL
        End Get
        Set(ByVal value As Decimal)
            vBUD_TOTAL = value
        End Set
    End Property

    Function SaveBUDGET_D(ByVal p_stTrans As SqlTransaction) As Integer
        '@return_value = [dbo].[SaveBUDGET_D]
        '@BUD_ID = 0,
        '		@BUD_BUH_ID = 1,
        '		@BUD_ACT_ID = N'1016',
        '		@BUD_MONTH1 = 1,
        '		@BUD_MONTH2 = 2,
        '		@BUD_MONTH3 = 3,
        '		@BUD_MONTH4 = 4,
        '		@BUD_MONTH5 = 5,
        '		@BUD_MONTH6 = 6,
        '		@BUD_MONTH7 = 7,
        '		@BUD_MONTH8 = 8,
        '		@BUD_MONTH9 = 9,
        '		@BUD_MONTH10 = 10,
        '		@BUD_MONTH11 = 11,
        '		@BUD_MONTH12 = 12,
        '		@BUD_TOTAL = 34
        '       @BUD_MAIN varchar(100), 
        '       @BUD_SUBHEAD varchar(100), 

        Dim retval As Integer
        If vDelete Then
            retval = DeleteBUDGET_D(p_stTrans)
            Return retval
        End If

        Dim pParms(18) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@BUD_ID", SqlDbType.Int)
        pParms(0).Value = vBUD_ID
        pParms(1) = New SqlClient.SqlParameter("@BUD_BUH_ID", SqlDbType.Int)
        pParms(1).Value = vBUD_BUH_ID
        pParms(2) = New SqlClient.SqlParameter("@BUD_ACT_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = vBUD_ACT_ID

        pParms(3) = New SqlClient.SqlParameter("@BUD_MONTH1", SqlDbType.Decimal)
        pParms(3).Value = vBUD_MONTH1
        pParms(4) = New SqlClient.SqlParameter("@BUD_MONTH2", SqlDbType.Decimal)
        pParms(4).Value = vBUD_MONTH2
        pParms(5) = New SqlClient.SqlParameter("@BUD_MONTH3", SqlDbType.Decimal)
        pParms(5).Value = vBUD_MONTH3
        pParms(6) = New SqlClient.SqlParameter("@BUD_MONTH4", SqlDbType.Decimal)
        pParms(6).Value = vBUD_MONTH4
        pParms(7) = New SqlClient.SqlParameter("@BUD_MONTH5", SqlDbType.Decimal)
        pParms(7).Value = vBUD_MONTH5
        pParms(8) = New SqlClient.SqlParameter("@BUD_MONTH6", SqlDbType.Decimal)
        pParms(8).Value = vBUD_MONTH6

        pParms(9) = New SqlClient.SqlParameter("@BUD_MONTH7", SqlDbType.Decimal)
        pParms(9).Value = vBUD_MONTH7
        pParms(10) = New SqlClient.SqlParameter("@BUD_MONTH8", SqlDbType.Decimal)
        pParms(10).Value = vBUD_MONTH8
        pParms(11) = New SqlClient.SqlParameter("@BUD_MONTH9", SqlDbType.Decimal)
        pParms(11).Value = vBUD_MONTH9
        pParms(12) = New SqlClient.SqlParameter("@BUD_MONTH10", SqlDbType.Decimal)
        pParms(12).Value = vBUD_MONTH10
        pParms(13) = New SqlClient.SqlParameter("@BUD_MONTH11", SqlDbType.Decimal)
        pParms(13).Value = vBUD_MONTH11
        pParms(14) = New SqlClient.SqlParameter("@BUD_MONTH12", SqlDbType.Decimal)
        pParms(14).Value = vBUD_MONTH12
        pParms(15) = New SqlClient.SqlParameter("@BUD_TOTAL", SqlDbType.Decimal)
        pParms(15).Value = vBUD_TOTAL

        pParms(16) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(16).Direction = ParameterDirection.ReturnValue
        pParms(17) = New SqlClient.SqlParameter("@BUD_MAIN", SqlDbType.VarChar, 100)
        pParms(17).Value = vBUD_MAIN
        pParms(18) = New SqlClient.SqlParameter("@BUD_SUBHEAD", SqlDbType.VarChar, 100)
        pParms(18).Value = vBUH_SUBHEAD
        If vBUD_ACT_ID.Trim <> "" Then
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveBUDGET_D", pParms)

            SaveBUDGET_D = pParms(16).Value
        Else
            Return 0
        End If



    End Function


    Function DeleteBUDGET_D(ByVal p_stTrans As SqlTransaction) As Integer
        '     EXEC	@return_value = [dbo].[DeleteBUDGET_D]
        '@BUD_ID = 13  
        Dim pParms(2) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@BUD_ID", SqlDbType.Int)
        pParms(0).Value = vBUD_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "DeleteBUDGET_D", pParms)

        DeleteBUDGET_D = pParms(1).Value

    End Function

    Public Shared Function CreateDataTableBUDGET_D() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            '@BUD_ACT_ID = N'1016',
            '@BUD_MONTH1 = 1,
            '@BUD_MONTH2 = 2,
            '@BUD_MONTH3 = 3,
            '@BUD_MONTH4 = 4,
            '@BUD_MONTH5 = 5,
            '@BUD_MONTH6 = 6,
            '@BUD_MONTH7 = 7,
            '@BUD_MONTH8 = 8,
            '@BUD_MONTH9 = 9,
            '@BUD_MONTH10 = 10,
            '@BUD_MONTH11 = 11,
            '@BUD_MONTH12 = 12,
            '@BUD_TOTAL = 34
            Dim cID As New DataColumn("ID", System.Type.GetType("System.Decimal"))
            Dim cBUD_ACT_ID As New DataColumn("BUD_ACT_ID", System.Type.GetType("System.String"))
            Dim cBUD_MONTH1 As New DataColumn("BUD_MONTH1", System.Type.GetType("System.Decimal"))
            Dim cBUD_MONTH2 As New DataColumn("BUD_MONTH2", System.Type.GetType("System.Decimal"))
            Dim cBUD_MONTH3 As New DataColumn("BUD_MONTH3", System.Type.GetType("System.Decimal"))
            Dim cBUD_MONTH4 As New DataColumn("BUD_MONTH4", System.Type.GetType("System.Decimal"))
            Dim cBUD_MONTH5 As New DataColumn("BUD_MONTH5", System.Type.GetType("System.Decimal"))
            Dim cBUD_MONTH6 As New DataColumn("BUD_MONTH6", System.Type.GetType("System.Decimal"))
            Dim cBUD_MONTH7 As New DataColumn("BUD_MONTH7", System.Type.GetType("System.Decimal"))
            Dim cBUD_MONTH8 As New DataColumn("BUD_MONTH8", System.Type.GetType("System.Decimal"))
            Dim cBUD_MONTH9 As New DataColumn("BUD_MONTH9", System.Type.GetType("System.Decimal"))
            Dim cBUD_MONTH10 As New DataColumn("BUD_MONTH10", System.Type.GetType("System.Decimal"))
            Dim cBUD_MONTH11 As New DataColumn("BUD_MONTH11", System.Type.GetType("System.Decimal"))
            Dim cBUD_MONTH12 As New DataColumn("BUD_MONTH12", System.Type.GetType("System.Decimal"))
            Dim cBUD_TOTAL As New DataColumn("BUD_TOTAL", System.Type.GetType("System.Decimal"))
            Dim cACT_NAME As New DataColumn("ACT_NAME", System.Type.GetType("System.String"))
            Dim cACT_TYPE As New DataColumn("ACT_TYPE", System.Type.GetType("System.String"))
            Dim cBUD_MAIN As New DataColumn("BUD_MAIN", System.Type.GetType("System.String"))
            Dim cBUH_SUBHEAD As New DataColumn("BUD_SUBHEAD", System.Type.GetType("System.String"))

            Dim cSTATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))



            dtDt.Columns.Add(cID)
            dtDt.Columns.Add(cBUD_ACT_ID)
            dtDt.Columns.Add(cBUD_MONTH1)
            dtDt.Columns.Add(cBUD_MONTH2)
            dtDt.Columns.Add(cBUD_MONTH3)
            dtDt.Columns.Add(cBUD_MONTH4)
            dtDt.Columns.Add(cBUD_MONTH5)
            dtDt.Columns.Add(cBUD_MONTH6)
            dtDt.Columns.Add(cBUD_MONTH7)
            dtDt.Columns.Add(cBUD_MONTH8)
            dtDt.Columns.Add(cBUD_MONTH9)
            dtDt.Columns.Add(cBUD_MONTH10)
            dtDt.Columns.Add(cBUD_MONTH11)
            dtDt.Columns.Add(cBUD_MONTH12)
            dtDt.Columns.Add(cBUD_TOTAL)
            dtDt.Columns.Add(cACT_NAME)
            dtDt.Columns.Add(cACT_TYPE)
            dtDt.Columns.Add(cBUD_MAIN)
            dtDt.Columns.Add(cBUH_SUBHEAD)
            dtDt.Columns.Add(cSTATUS)
            Return dtDt
        Catch ex As Exception

            Return dtDt
        End Try
    End Function


End Class
