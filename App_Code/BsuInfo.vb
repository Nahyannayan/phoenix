﻿Imports Microsoft.VisualBasic

Public Class BsuInfo
    Public Sub New(ByVal BSU_ID As String, ByVal BSU_NAME As String)
        Me.BSU_ID = BSU_ID
        Me.BSU_NAME = BSU_NAME
    End Sub
    Private _BSUID As String
    Private _BSUNAME As String
    Public Property BSU_ID() As String
        Get
            Return _BSUID
        End Get
        Set(ByVal value As String)
            _BSUID = value
        End Set
    End Property
    Public Property BSU_NAME() As String
        Get
            Return _BSUNAME
        End Get
        Set(ByVal value As String)
            _BSUNAME = value
        End Set
    End Property
End Class
