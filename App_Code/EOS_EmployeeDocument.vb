﻿Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Public Class EOS_EmployeeDocument
    Structure DocumentDetail
        Public EDC_ID As Integer
        Public EDC_EMP_NAME As String
        Public EDC_EDD_NAME As String
        Public EDC_TYPE As String
        Public EDC_ESD_ID As Integer
        Public EDC_BSU_ID As String
        Public EDC_EMP_ID As String
        Public EDC_EDD_ID As Integer
        Public EDC_NARRATION As String
        Public EDC_EXP_DT As Date
        Public EDC_bREqReminder As Boolean
        Public EDC_Bactive As Boolean
        Public EDC_DOCUMENT As Byte()
        Public EDC_CONTENT_TYPE As String
    End Structure

    Public Shared Function GetDocumentDetail(ByVal EDC_ID As Integer) As EOS_EmployeeDocument.DocumentDetail
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EDC_ID", EDC_ID, SqlDbType.Int)
            mTable = Mainclass.getDataTable("EOS_GetEmpDocumentDetail", sqlParam, str_conn)
            If mTable.Rows.Count > 0 Then
                Dim EDC_Detail As New EOS_EmployeeDocument.DocumentDetail
                EDC_Detail.EDC_ID = mTable.Rows(0).Item("EDC_ID")
                EDC_Detail.EDC_TYPE = mTable.Rows(0).Item("EDC_TYPE")
                EDC_Detail.EDC_BSU_ID = mTable.Rows(0).Item("EDC_BSU_ID")
                EDC_Detail.EDC_EMP_ID = mTable.Rows(0).Item("EDC_EMP_ID")
                EDC_Detail.EDC_EMP_NAME = mTable.Rows(0).Item("EDC_EMP_NAME")
                EDC_Detail.EDC_EDD_ID = mTable.Rows(0).Item("EDC_EDD_ID")
                EDC_Detail.EDC_EDD_NAME = mTable.Rows(0).Item("EDC_EDD_NAME")
                EDC_Detail.EDC_NARRATION = mTable.Rows(0).Item("EDC_NARRATION")
                EDC_Detail.EDC_EXP_DT = mTable.Rows(0).Item("EDC_EXP_DT")
                EDC_Detail.EDC_ESD_ID = mTable.Rows(0).Item("EDC_ESD_ID")
                EDC_Detail.EDC_bREqReminder = mTable.Rows(0).Item("EDC_bREqReminder")
                EDC_Detail.EDC_Bactive = mTable.Rows(0).Item("EDC_Bactive")
                EDC_Detail.EDC_DOCUMENT = mTable.Rows(0).Item("EDC_DOCUMENT")
                EDC_Detail.EDC_CONTENT_TYPE = mTable.Rows(0).Item("EDC_CONTENT_TYPE")
                If Not IsDBNull(mTable.Rows(0).Item("EDC_DOCUMENT")) Then
                    EDC_Detail.EDC_DOCUMENT = mTable.Rows(0).Item("EDC_DOCUMENT")
                    EDC_Detail.EDC_CONTENT_TYPE = mTable.Rows(0).Item("EDC_CONTENT_TYPE")
                Else
                    EDC_Detail.EDC_DOCUMENT = Nothing
                    EDC_Detail.EDC_CONTENT_TYPE = ""
                End If
                Return EDC_Detail
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function GetEmployeeDocumentDetailList(ByVal EDC_ID As Integer) As DataTable
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EDC_EMP_ID", EDC_ID, SqlDbType.Int)
            mTable = Mainclass.getDataTable("EOS_GetEmpDocumentDetailList", sqlParam, str_conn)
            Return mTable
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function GetDependantDocumentDetailList(ByVal EDC_EDD_ID As Integer) As DataTable
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EDC_EDD_ID", EDC_EDD_ID, SqlDbType.Int)
            mTable = Mainclass.getDataTable("EOS_GetEDDDocumentDetailList", sqlParam, str_conn)
            Return mTable
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function SaveDocumentDetail(ByRef EDC_Detail As EOS_EmployeeDocument.DocumentDetail) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(12) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EDC_ID", EDC_Detail.EDC_ID, SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@EDC_TYPE", EDC_Detail.EDC_TYPE, SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@EDC_ESD_ID", EDC_Detail.EDC_ESD_ID, SqlDbType.Int)
            sqlParam(3) = Mainclass.CreateSqlParameter("@EDC_BSU_ID", EDC_Detail.EDC_BSU_ID, SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@EDC_EMP_ID", EDC_Detail.EDC_EMP_ID, SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@EDC_EDD_ID", EDC_Detail.EDC_EDD_ID, SqlDbType.Int)
            sqlParam(6) = Mainclass.CreateSqlParameter("@EDC_NARRATION", EDC_Detail.EDC_NARRATION, SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@EDC_EXP_DT", EDC_Detail.EDC_EXP_DT, SqlDbType.DateTime)
            sqlParam(8) = Mainclass.CreateSqlParameter("@EDC_bREqReminder", EDC_Detail.EDC_bREqReminder, SqlDbType.Bit)
            sqlParam(9) = Mainclass.CreateSqlParameter("@EDC_Bactive", EDC_Detail.EDC_Bactive, SqlDbType.Bit)
            sqlParam(10) = Mainclass.CreateSqlParameter("@EDC_DOCUMENT", EDC_Detail.EDC_DOCUMENT, SqlDbType.VarBinary)
            sqlParam(11) = Mainclass.CreateSqlParameter("@EDC_CONTENT_TYPE", EDC_Detail.EDC_CONTENT_TYPE, SqlDbType.VarChar)
            sqlParam(12) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "EOS_SaveEmpDocumentDetail", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(12).Value = "" Then
                SaveDocumentDetail = ""
                EDC_Detail.EDC_ID = sqlParam(0).Value
            Else
                SaveDocumentDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(12).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
    Public Shared Function DeleteDocumentDetail(ByVal EDC_ID As Integer) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EDC_ID", EDC_ID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "EOS_DeleteEmpDocumentDetail", sqlParam)
            If (Retval = "0" Or Retval = "") Then
                DeleteDocumentDetail = ""
            Else
                DeleteDocumentDetail = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(1).Value)
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function
End Class
