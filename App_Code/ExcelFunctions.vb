Imports System.Data.DataSet
Imports System.Data.OleDb.OleDbDataAdapter
Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.IO.StringWriter
Imports System.Web.Configuration
Imports System.Web.UI.HtmlTextWriter
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports Microsoft.Office.Interop.Excel
Imports System.Xml
Imports System.Xml.Xsl

Public Class ExcelFunctions

    Public Shared Function GetExcelSheetNames(ByVal excelFile As String) As String 'make string() for all
        Dim objConn As System.Data.OleDb.OleDbConnection = Nothing
        Dim dt As System.Data.DataTable = Nothing

        Try
            ' Connection String. Change the excel file to the file you 
            ' will search. 
            Dim connString As String
            If excelFile.EndsWith("xls") Then
                connString = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + excelFile + ";Extended Properties=Excel 8.0;"

            Else
                connString = "Provider=Microsoft.ACE.OLEDB.12.0;" & _
                                "Data Source=" & excelFile & ";" & _
                                "Extended Properties=""Excel 12.0;HDR=YES;"""
            End If

            ' Create connection object by using the preceding connection string. 
            objConn = New System.Data.OleDb.OleDbConnection(connString)
            ' Open connection with the database. 
            objConn.Open()
            ' Get the data table containg the schema guid. 
            dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)

            If dt Is Nothing Then
                Return Nothing
            End If

            Dim excelSheets As String() = New String(dt.Rows.Count - 1) {}
            Dim i As Integer = 0

            ' Add the sheet name to the string array. 
            For Each row As DataRow In dt.Rows
                excelSheets(i) = row("TABLE_NAME").ToString()
                i += 1
            Next
            For j As Integer = 0 To excelSheets.Length - 1

                ' Query each excel sheet. 
                ' Loop through all of the sheets if you want too... 
            Next

            '
            Return excelSheets(0)
        Catch ex As Exception
            Return Nothing
        Finally
            ' Clean up. 
            If objConn IsNot Nothing Then
                objConn.Close()
                objConn.Dispose()
            End If
            If dt IsNot Nothing Then
                dt.Dispose()
            End If
        End Try
    End Function

End Class

'Class to convert a dataset to an html stream which can be used to display the dataset
'in MS Excel
'The Convert method is overloaded three times as follows
' 1) Default to first table in dataset
' 2) Pass an index to tell us which table in the dataset to use
' 3) Pass a table name to tell us which table in the dataset to use

Public Class DataSetToExcel

 


    'Public Shared Function Convert(ByVal ds As DataSet, ByVal directoryPath As String, ByVal fileName As String) As String
    '    Dim oXL As Application
    '    Dim oWB As _Workbook
    '    Dim oSheet As _Worksheet
    '    Dim oRng As Range
    '    Dim strFile As String = directoryPath & "\" & fileName & ".xls"
    '    Try
    '        oXL = New Application()
    '        oXL.Visible = False
    '        'Get a new workbook. 
    '        oWB = oXL.Workbooks.Add(Type.Missing) 'DirectCast((oXL.Workbooks.Add(Missing.Value)), _Workbook)
    '        oSheet = DirectCast(oWB.ActiveSheet, _Worksheet)
    '        'System.Data.DataTable dtGridData=ds.Tables[0]; 
    '        Dim iRow As Integer = 2
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
    '                oSheet.Cells(1, j + 1) = ds.Tables(0).Columns(j).ColumnName
    '            Next
    '            For rowNo As Integer = 0 To ds.Tables(0).Rows.Count - 1
    '                ' For each row, print the values of each column. 
    '                For colNo As Integer = 0 To ds.Tables(0).Columns.Count - 1
    '                    oSheet.Cells(iRow, colNo + 1) = ds.Tables(0).Rows(rowNo)(colNo).ToString()
    '                Next
    '                iRow += 1
    '            Next
    '        End If
    '        oRng = oSheet.Range("A1", "IV1")
    '        oRng.EntireColumn.AutoFit()
    '        oXL.Visible = False
    '        oXL.UserControl = False

    '        oWB.SaveAs(strFile, XlFileFormat.xlWorkbookNormal, Nothing, Nothing, False, False, _
    '        XlSaveAsAccessMode.xlShared, False, False, Nothing, Nothing)

    '        ' Need all following code to clean up and remove all references!!! 

    '        oWB.Close(Nothing, Nothing, Nothing)

    '        oXL.Workbooks.Close()

    '        oXL.Quit()

    '        releaseObject(oXL)
    '        releaseObject(oWB)
    '        releaseObject(oSheet)
    '        releaseObject(oRng)

    '        'Marshal.ReleaseComObject(oRng)
    '        'Marshal.ReleaseComObject(oXL)
    '        'Marshal.ReleaseComObject(oSheet)
    '        'Marshal.ReleaseComObject(oWB)

    '        'Dim strMachineName As String = Request.ServerVariables("SERVER_NAME")
    '        'Response.Redirect("http://" + strMachineName + "/" + "ViewNorthWindSample/reports/" + strFile)
    '    Catch theException As Exception
    '        'Response.Write(theException.Message)
    '        Return ""
    '    End Try
    '    Return strFile
    'End Function

    'Private Shared Sub releaseObject(ByVal obj As Object)
    '    Try
    '        System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
    '        obj = Nothing
    '    Catch ex As Exception
    '        obj = Nothing
    '    Finally
    '        GC.Collect()
    '    End Try
    'End Sub

    Public Shared Sub Convert(ByVal ds As DataSet, ByVal response As HttpResponse, ByVal xmlPath As String, ByVal strFName As String)
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment; filename=" & strFName & ".xls")
        response.Charset = ""
        Dim xdd As New XmlDataDocument(ds)
        Dim xt As New XslCompiledTransform()
        xt.Load(xmlPath)
        xt.Transform(xdd, Nothing, response.OutputStream)
        response.End()
    End Sub

    'Public Shared Sub Convert(ByVal ds As DataSet, ByVal response As HttpResponse)
    '    'first let's clean up the response.object
    '    response.Clear()
    '    response.Charset = ""
    '    'set the response mime type for excel
    '    response.ContentType = "application/vnd.ms-excel"
    '    'create a string writer
    '    Dim stringWrite As New System.IO.StringWriter
    '    'create an htmltextwriter which uses the stringwriter
    '    Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
    '    'instantiate a datagrid
    '    Dim dg As New DataGrid
    '    'set the datagrid datasource to the dataset passed in
    '    dg.DataSource = ds.Tables(0)
    '    'bind the datagrid
    '    dg.DataBind()
    '    'tell the datagrid to render itself to our htmltextwriter
    '    dg.RenderControl(htmlWrite)
    '    'all that's left is to output the html
    '    response.Write(stringWrite.ToString)
    '    response.End()
    'End Sub

    'Public Shared Sub Convert(ByVal ds As DataSet, ByVal TableIndex As Integer, ByVal response As HttpResponse)
    '    'lets make sure a table actually exists at the passed in value
    '    'if it is not call the base method
    '    If TableIndex > ds.Tables.Count - 1 Then
    '        Convert(ds, response)
    '    End If
    '    'we've got a good table so
    '    'let's clean up the response.object
    '    response.Clear()
    '    response.Charset = ""
    '    'set the response mime type for excel
    '    response.ContentType = "application/vnd.ms-excel"        
    '    response.AppendHeader("content-disposition", "attachment; filename=test.xls")
    '    'create a string writerl
    '    Dim stringWrite As New System.IO.StringWriter
    '    'create an htmltextwriter which uses the stringwriter
    '    Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
    '    'instantiate a datagrid
    '    Dim dg As New DataGrid
    '    'set the datagrid datasource to the dataset passed in
    '    dg.DataSource = ds.Tables(TableIndex)
    '    'bind the datagrid
    '    dg.DataBind()
    '    'tell the datagrid to render itself to our htmltextwriter
    '    dg.RenderControl(htmlWrite)
    '    'all that's left is to output the html
    '    response.Write(stringWrite.ToString)
    '    response.End()
    'End Sub

    'Public Shared Sub Convert(ByVal ds As DataSet, ByVal TableName As String, ByVal response As HttpResponse)
    '    'let's make sure the table name exists
    '    'if it does not then call the default method
    '    If ds.Tables(TableName) Is Nothing Then
    '        Convert(ds, response)
    '    End If
    '    'we've got a good table so
    '    'let's clean up the response.object
    '    response.Clear()
    '    response.Charset = ""
    '    'set the response mime type for excel
    '    response.ContentType = "application/vnd.ms-excel"
    '    'create a string writer
    '    Dim stringWrite As New System.IO.StringWriter
    '    'create an htmltextwriter which uses the stringwriter
    '    Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
    '    'instantiate a datagrid
    '    Dim dg As New DataGrid
    '    'set the datagrid datasource to the dataset passed in
    '    dg.DataSource = ds.Tables(TableName)
    '    'bind the datagrid
    '    dg.DataBind()
    '    'tell the datagrid to render itself to our htmltextwriter
    '    dg.RenderControl(htmlWrite)
    '    'all that's left is to output the html
    '    response.Write(stringWrite.ToString)
    '    response.End()
    'End Sub

End Class

