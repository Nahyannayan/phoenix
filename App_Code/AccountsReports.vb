Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports UtilityObj

Public Class AccountsReports

    Public Shared Function JournalVouchers(ByVal DOcNo As String, ByVal DOcDate As String, ByVal BSuId As String, ByVal FYear As String) As MyReportClass
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim str_Sql As String
        Dim docType As String = DOcNo.Substring(2, 2)
        Dim strFilter As String = String.Empty
        strFilter = "BSU_ID = '" & BSuId & "' and FYEAR = " & FYear & " and DOCTYPE = '" & docType & "' and DOCNO = '" & DOcNo & "'"
        If DOcDate <> "" Then
            strFilter += " and DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(DOcDate)) & "'"
        End If
        str_Sql = "SELECT * FROM vw_OSA_JOURNAL where " + strFilter
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Dim repSource As New MyReportClass
        If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.Connection = New SqlConnection(str_conn)
            cmd.CommandType = CommandType.Text

            Dim params As New Hashtable
            params("userName") = HttpContext.Current.Session("sUsr_name") 
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(BSuId, docType, DOcNo)
            params("VoucherName") = "JOURNAL VOUCHER"
            params("reportHeading") = "JOURNAL VOUCHER"
            params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.VoucherName = "JOURNAL VOUCHER"
            repSource.IncludeBSUImage = True

            Dim repSourceSubRep() As MyReportClass
            repSourceSubRep = repSource.SubReport
            Dim subReportLength As Integer = 0
            If repSource.SubReport Is Nothing Then
                ReDim repSourceSubRep(0)
                repSource.SubReport = repSourceSubRep
            Else
                subReportLength = repSourceSubRep.Length
                ReDim Preserve repSourceSubRep(subReportLength)
            End If

            repSourceSubRep(subReportLength) = New MyReportClass
            Dim cmdNarrationDetails As New SqlCommand
            cmdNarrationDetails.CommandText = "SELECT SUB_ID VHH_SUB_ID, BSU_ID VHH_BSU_ID, " & _
            " FYEAR VHH_FYEAR, DOCTYPE VHH_DOCTYPE, DOCNO VHH_DOCNO, DEBIT VHD_LINEID, " & _
            " JHD_NARRATION VHD_NARRATION  FROM vw_OSA_JOURNAL where " + strFilter
            cmdNarrationDetails.Connection = New SqlConnection(str_conn)
            cmdNarrationDetails.CommandType = CommandType.Text
            repSourceSubRep(subReportLength).Command = cmdNarrationDetails
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/JournalVoucherReport.rpt"
        End If
        Return repSource
    End Function

    Public Shared Function CashPayments(ByVal DOcNo As String, ByVal DOcDate As String, ByVal BSuId As String, ByVal FYear As String) As MyReportClass
        Dim str_Sql, strFilter As String
        Dim docType As String = DOcNo.Substring(2, 2)
        strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & DOcNo _
        & "' and VHH_DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(DOcDate)) & "' and VHH_BSU_ID in('" & BSuId & "')"
        str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter

        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text 
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        Dim repSource As New MyReportClass
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)

            Dim params As New Hashtable
            params("UserName") = HttpContext.Current.Session("sUsr_name")
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(BSuId, docType, DOcNo)
            params("voucherName") = "CASH PAYMENT VOUCHER"
            params("Summary") = False
            params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
            repSource.VoucherName = "CASH PAYMENT VOUCHER"
            repSource.Parameter = params
            repSource.IncludeBSUImage = True
            repSource.SubReport = AccountFunctions.AddPostingDetails(DOcNo, docType, BSuId)
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/CashPaymentVoucher.rpt"
        End If
        Return repSource
    End Function

    Public Shared Function BankPayment(ByVal DOcNo As String, ByVal DOcDate As String, ByVal BSuId As String, ByVal FYear As String) As MyReportClass
        Dim str_Sql, strFilter As String
        Dim docType As String = DOcNo.Substring(2, 2)
        strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & DOcNo _
        & "' and VHH_DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(DOcDate)) & "' and VHH_BSU_ID in('" & BSuId & "')"
        str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)

            params("UserName") = HttpContext.Current.Session("sUsr_name")
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(BSuId, HttpContext.Current.Session("BANKTRAN"), DOcDate)
            'params("voucherName") = "PAYMENT VOUCHER(BANK)"
            params("voucherName") = "BANK PAYMENT VOUCHER"
            params("Summary") = False
            params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
            repSource.VoucherName = "PAYMENT VOUCHER(BANK)"
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            repSource.SubReport = AccountFunctions.AddPostingDetails(DOcNo, HttpContext.Current.Session("BANKTRAN"), BSuId)

            Dim repSourceSubRep() As MyReportClass
            repSourceSubRep = repSource.SubReport
            Dim subReportLength As Integer = 0
            If repSource.SubReport Is Nothing Then
                ReDim repSourceSubRep(0)
                repSource.SubReport = repSourceSubRep
            Else
                subReportLength = repSourceSubRep.Length
                ReDim Preserve repSourceSubRep(subReportLength)
            End If

            repSourceSubRep(subReportLength) = New MyReportClass
            Dim cmdNarrationDetails As New SqlCommand
            cmdNarrationDetails.CommandText = "SELECT VHH_SUB_ID, VHH_BSU_ID, VHH_FYEAR, VHH_DOCTYPE, " & _
            " VHH_DOCNO, VHD_LINEID, VHD_NARRATION FROM vw_OSA_VOUCHER where" + strFilter
            cmdNarrationDetails.Connection = New SqlConnection(str_conn)
            cmdNarrationDetails.CommandType = CommandType.Text
            repSourceSubRep(subReportLength).Command = cmdNarrationDetails
            repSource.SubReport = repSourceSubRep
            repSource.ResourceName = "../Reports/RPT_Files/BankPaymentReport.rpt"
        End If
        Return repSource
    End Function

    Public Shared Function BankReceipt(ByVal DOcNo As String, ByVal DOcDate As String, ByVal BSuId As String, ByVal FYear As String) As MyReportClass
        Dim str_Sql, strFilter As String
        strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = 'BR' and VHH_DOCNO = '" & DOcNo _
        & "' and VHH_DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(DOcDate)) & "' and VHH_BSU_ID in('" & BSuId & "')"
        str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
           
            params("UserName") = HttpContext.Current.Session("sUsr_name")
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(BSuId, "BR", DOcDate)
            params("voucherName") = "BANK RECEIPT VOUCHER"
            'params("voucherName") = "RECEIPT VOUCHER(BANK)"
            params("Summary") = False
            repSource.IncludeBSUImage = True
            repSource.SubReport = AccountFunctions.AddPostingDetails(DOcDate, "BR", BSuId)

            Dim repSourceSubRep() As MyReportClass
            repSourceSubRep = repSource.SubReport
            Dim subReportLength As Integer = 0
            If repSource.SubReport Is Nothing Then
                ReDim repSourceSubRep(0)
                repSource.SubReport = repSourceSubRep
            Else
                subReportLength = repSourceSubRep.Length
                ReDim Preserve repSourceSubRep(subReportLength)
            End If

            repSourceSubRep(subReportLength) = New MyReportClass
            Dim cmdNarrationDetails As New SqlCommand
            cmdNarrationDetails.CommandText = "SELECT VHH_SUB_ID, VHH_BSU_ID, VHH_FYEAR, VHH_DOCTYPE, " & _
            " VHH_DOCNO, VHD_LINEID, VHD_NARRATION FROM vw_OSA_VOUCHER where" + strFilter
            cmdNarrationDetails.Connection = New SqlConnection(str_conn)
            cmdNarrationDetails.CommandType = CommandType.Text
            repSourceSubRep(subReportLength).Command = cmdNarrationDetails
            repSource.SubReport = repSourceSubRep

            repSource.VoucherName = "BANK RECEIPT VOUCHER"
            params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/BankReceiptReport.rpt"
        End If
        Return repSource
    End Function

    Public Shared Function CreditCard(ByVal DOcNo As String, ByVal DOcDate As String, ByVal BSuId As String, ByVal FYear As String) As MyReportClass
        Dim str_Sql, strFilter As String
        strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = 'CC' and VHH_DOCNO = '" & DOcNo _
        & "' and VHH_DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(DOcDate)) & "' and VHH_BSU_ID in('" & BSuId & "')"
        str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter

        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
           
            params("UserName") = HttpContext.Current.Session("sUsr_name")
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(BSuId, "CC", DOcDate)
            params("voucherName") = "CREDIT CARD RECEIPTS"
            params("Summary") = False
            repSource.VoucherName = "CREDIT CARD RECEIPTS"
            params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
            repSource.IncludeBSUImage = True
            repSource.SubReport = AccountFunctions.AddPostingDetails(DOcDate, "CC", BSuId)
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/CreditCardReport.rpt"
        End If
        Return repSource
    End Function

    Public Shared Function CashReceipt(ByVal DOcNo As String, ByVal DOcDate As String, ByVal BSuId As String, ByVal FYear As String) As MyReportClass
        Dim str_Sql, strFilter As String
        strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = 'CR' and VHH_DOCNO = '" & DOcNo _
        & "' and VHH_DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(DOcDate)) & "' and VHH_BSU_ID in('" & BSuId & "')"
        str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            params("UserName") = HttpContext.Current.Session("sUsr_name")
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(BSuId, "CR", DOcNo)
            params("voucherName") = "CASH RECEIPT VOUCHER"
            params("Summary") = False
            params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
            repSource.VoucherName = "CASH RECEIPT VOUCHER"
            repSource.Parameter = params
            repSource.IncludeBSUImage = True
            repSource.SubReport = AccountFunctions.AddPostingDetails(DOcNo, "CR", BSuId)
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/CashPaymentReport.rpt"
        End If
        Return repSource
    End Function

    Public Shared Function InternetCollection(ByVal DOcNo As String, ByVal DOcDate As String, ByVal BSuId As String, ByVal FYear As String) As MyReportClass
        Dim str_Sql, strFilter As String
        strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = 'IC' and VHH_DOCNO = '" & DOcNo _
        & "' and VHH_DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(DOcDate)) & "' and VHH_BSU_ID in('" & BSuId & "')"
        str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            params("UserName") = HttpContext.Current.Session("sUsr_name")
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(BSuId, "IC", DOcNo)
            params("voucherName") = "INTERNET COLLECTION"
            params("Summary") = False
            repSource.VoucherName = "INTERNET COLLECTION"
            params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
            repSource.IncludeBSUImage = True
            repSource.SubReport = AccountFunctions.AddPostingDetails(DOcNo, "IC", BSuId)
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../RPT_Files/CreditCardReport.rpt"
        End If
        Return repSource
    End Function

    Public Shared Function TrailBalance(ByVal FromDate As String, ByVal ToDate As String, ByVal BSUIDS As String, _
    ByVal IsConsolidation As Boolean, ByVal USR_NAME As String, ByVal BSU_Roundoff As String) As MyReportClass
        Dim strXMLBSUNames As String
        Dim Consold, Currency As Boolean
        Consold = False
        Currency = True
        If IsConsolidation Then
            Consold = True
        Else
            Currency = False
        End If
        strXMLBSUNames = UtilityObj.GenerateXML(BSUIDS, XMLType.BSUName)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)

        Dim cmd As New SqlCommand("RPTGETTRIALBALANCE", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpJHD_BSU_IDs As New SqlParameter("@BSUID", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpJHD_ACT_IDs As New SqlParameter("@ACT_IDs", SqlDbType.Xml)
        sqlpJHD_ACT_IDs.Value = DBNull.Value
        cmd.Parameters.Add(sqlpJHD_ACT_IDs)

        Dim sqlpJHD_DOCTYPE As New SqlParameter("@Type", SqlDbType.VarChar, 20)
        sqlpJHD_DOCTYPE.Value = "2" 'cmbGroupType.SelectedItem.Value
        cmd.Parameters.Add(sqlpJHD_DOCTYPE)

        Dim sqlpFROMDOCDT As New SqlParameter("@DTFROM", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = FromDate
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpTODOCDT As New SqlParameter("@DTTO", SqlDbType.DateTime)
        sqlpTODOCDT.Value = ToDate
        cmd.Parameters.Add(sqlpTODOCDT)

        Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        Dim sqlpGroupCurrency As New SqlParameter("@bShowinGrpCurrency", SqlDbType.Bit)
        sqlpConsolidation.Value = Consold
        sqlpGroupCurrency.Value = Currency
        cmd.Parameters.Add(sqlpConsolidation)
        cmd.Parameters.Add(sqlpGroupCurrency)

        objConn.Close()
        objConn.Open()
        cmd.Connection = New SqlConnection(str_conn)
        HttpContext.Current.Session("BSUNames") = strXMLBSUNames
        HttpContext.Current.Session("rptFromDate") = FromDate
        HttpContext.Current.Session("rptToDate") = ToDate
        HttpContext.Current.Session("rptConsolidation") = Consold
        HttpContext.Current.Session("rptGroupCurrency") = Currency

        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = USR_NAME
        params("FromDate") = FromDate
        params("ToDate") = ToDate
        params("Type") = "2"
        params("parent") = ""
        params("RPT_CAPTION") = "Trial Balance"
        params("decimal") = BSU_Roundoff
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/rptTrailBalance.rpt"
        objConn.Close()
        Return repSource
    End Function

    Public Shared Function PeriodicCollectionAnalysis(ByVal FromDate As String, ByVal ToDate As String, ByVal BSUIDS As String, _
    ByVal IsConsolidation As Boolean, ByVal USR_NAME As String, ByVal BSU_Roundoff As String) As MyReportClass
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand
        Dim Consold, Currency As Boolean
        Consold = False
        Currency = True
        If IsConsolidation Then
            Consold = True
        Else
            Currency = False
        End If

        Dim str_bsu_ids As String
        str_bsu_ids = BSUIDS
        cmd = New SqlCommand("RPTCollectionSummaryForaPeriod", objConn)

        cmd.CommandType = CommandType.StoredProcedure
        Dim strXMLBSUNames As String
        strXMLBSUNames = UtilityObj.GenerateXML(BSUIDS, XMLType.BSUName)
        Dim sqlpJHD_BSU_IDs As New SqlParameter("@JHD_BSU_IDs", SqlDbType.Xml)
        sqlpJHD_BSU_IDs.Value = strXMLBSUNames
        cmd.Parameters.Add(sqlpJHD_BSU_IDs)

        Dim sqlpFROMDOCDT As New SqlParameter("@FROMDOCDT", SqlDbType.DateTime)
        sqlpFROMDOCDT.Value = FromDate
        cmd.Parameters.Add(sqlpFROMDOCDT)

        Dim sqlpDTTO As New SqlParameter("@TODOCDT", SqlDbType.VarChar)
        sqlpDTTO.Value = ToDate
        cmd.Parameters.Add(sqlpDTTO)

        Dim sqlpConsolidation As New SqlParameter("@bConsolidation", SqlDbType.Bit)
        sqlpConsolidation.Value = Consold
        cmd.Parameters.Add(sqlpConsolidation)
        Dim sqlpGroupCurrency As New SqlParameter("@bGroupCurrency", SqlDbType.Bit)
        sqlpGroupCurrency.Value = Currency
        cmd.Parameters.Add(sqlpGroupCurrency)
        adpt.SelectCommand = cmd

        objConn.Open()
        adpt.Fill(ds)
        If ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
            HttpContext.Current.Session("BSUNames") = strXMLBSUNames
            HttpContext.Current.Session("rptFromDate") = FromDate
            HttpContext.Current.Session("rptToDate") = ToDate
            HttpContext.Current.Session("rptConsolidation") = Consold
            HttpContext.Current.Session("rptGroupCurrency") = Currency

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("userName") = USR_NAME
            params("FromDate") = FromDate
            params("toDate") = ToDate
            repSource.ResourceName = "../RPT_Files/rptCollectionsummaryForaPeriod.rpt"
            params("header") = "Periodic Collection Analysis"

            'repSource.GetDataSourceFromCommand = False
            repSource.Parameter = params
            repSource.Command = cmd
            objConn.Close()
            Return repSource
        Else
            objConn.Close()
            Return Nothing
        End If
    End Function

    Public Shared Function Prepayment(ByVal viewid As String, ByVal DOcDate As String, ByVal BSuId As String, ByVal FYear As String) As MyReportClass
        Dim str_Sql, strFilter As String
        strFilter = "WHERE  GUID = '" & viewid & "'"
        str_Sql = "SELECT * FROM vw_OSA_PREPAYMENT " + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)
           
            params("UserName") = HttpContext.Current.Session("sUsr_name")
            params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            repSource.SubReport = AccountFunctions.AddPostingDetails(DOcDate, "PP", BSuId)
            repSource.ResourceName = "../RPT_Files/rptPrepaymentVoucher.rpt"
        End If
        Return repSource
    End Function

    Public Shared Function UserLoggedDetails(ByVal RoleId As String, ByVal UserName As String, ByVal Fdate As String, ByVal Tdate As String) As MyReportClass

        Dim repSource As New MyReportClass
        Dim cmd As New SqlCommand("UserLoggedDetails")
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        cmd.Parameters.AddWithValue("@ROLL_ID", RoleId)
        cmd.Parameters.AddWithValue("@USR_NAME", UserName)
        cmd.Parameters.AddWithValue("@FROMDT", Fdate)
        cmd.Parameters.AddWithValue("@TODT", Tdate)

        cmd.Connection = New SqlConnection(str_conn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim params As New Hashtable
        params("ReportHead") = "GEMS OASIS Login History Period From " & Fdate & " Till " & Tdate
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.ResourceName = "../RPT_Files/UserLoggedDetails.rpt"
        Return repSource
    End Function

    Public Shared Function ChquePrint(ByVal DOcNo As String, ByVal DOcDate As String, ByVal BSuId As String, ByVal FYear As String, ByVal BankCode As String) As MyReportClass
        Dim strFilter As String
        Dim docType As String = DOcNo.Substring(2, 2)
        Dim cmd As New SqlCommand
        If BankCode = "BP" Then
            strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & DOcNo _
            & "' and VHH_BSU_ID in('" & BSuId & "')"
            If DOcDate <> "" Then
                strFilter = strFilter & " and VHH_DOCDT = '" & String.Format("{0:dd/MMM/yyyy}", CDate(DOcDate)) & "' "
            End If
            cmd.CommandText = " SELECT VHD_CHQNO AS VHD_CHQID, CUR_DENOMINATION, '' AS GUID, VHD_CHQDT, DETAILACCOUNT, " & _
          " SUM(VHD_AMOUNT) AS VHD_AMOUNT, VHH_BBearer, CHQID, CHQ_M.CHQ_FILE, CHQ_M.CHQ_ID,CHQ_M.CHQ_DESCR,VHD_bCheque FROM CHQ_M INNER JOIN " & _
          " CHQBOOK_M ON CHQ_M.CHQ_ID = CHQBOOK_M.CHQ_ID RIGHT OUTER JOIN (SELECT VHD_CHQNO, CUR_DENOMINATION, VHD_CHQDT, " & _
          " CASE WHEN isnull(VHH_RECEIVEDBY, '') = ''THEN DETAILACCOUNT ELSE VHH_RECEIVEDBY END AS DETAILACCOUNT, " & _
          " VHD_AMOUNT, VHD_CHQID, VHH_BBearer, VHD_CHQID AS CHQID, '' CHQ_DESCR,VHD_bCheque FROM vw_OSA_VOUCHER WHERE " & strFilter & ")db " & _
          " ON CHQBOOK_M.CHB_ID = CHQID <%FILTER%> GROUP BY CUR_DENOMINATION, VHD_CHQDT, DETAILACCOUNT, " & _
          " VHD_CHQID, VHD_CHQNO, VHH_BBearer, CHQID, CHQ_M.CHQ_FILE, CHQ_M.CHQ_ID,CHQ_M.CHQ_DESCR,VHD_bCheque "
        Else
            cmd.CommandText = " SELECT  VOUCHER_D.VHD_CHQNO AS VHD_CHQID, CASE WHEN isnull(VHH_RECEIVEDBY, '')= '' THEN ACT_NAME " _
                           & " ELSE VHH_RECEIVEDBY END AS DETAILACCOUNT, VOUCHER_D.GUID, VOUCHER_D.VHD_AMOUNT, VOUCHER_D.VHD_CHQDT, " _
                           & " CURRENCY_M.CUR_DENOMINATION, VOUCHER_H.VHH_BBearer, VOUCHER_D.VHD_CHQID AS CHQID, CHQ_M.CHQ_ID, CHQ_M.CHQ_FILE,CHQ_M.CHQ_DESCR, " _
                           & " VHD_bCheque FROM   CHQBOOK_M INNER JOIN CHQ_M ON CHQBOOK_M.CHQ_ID = CHQ_M.CHQ_ID RIGHT OUTER JOIN " _
                           & " VOUCHER_D INNER JOIN VOUCHER_H ON VOUCHER_D.VHD_SUB_ID = VOUCHER_H.VHH_SUB_ID " _
                           & " AND VOUCHER_D.VHD_BSU_ID = VOUCHER_H.VHH_BSU_ID AND VOUCHER_D.VHD_FYEAR = VOUCHER_H.VHH_FYEAR " _
                           & " AND VOUCHER_D.VHD_DOCTYPE = VOUCHER_H.VHH_DOCTYPE AND VOUCHER_D.VHD_DOCNO = VOUCHER_H.VHH_DOCNO INNER JOIN " _
                           & " CURRENCY_M ON VOUCHER_H.VHH_CUR_ID = CURRENCY_M.CUR_ID INNER JOIN " _
                           & " ACCOUNTS_M ON VOUCHER_D.VHD_ACT_ID = ACCOUNTS_M.ACT_ID ON CHQBOOK_M.CHB_ID = VOUCHER_D.VHD_CHQID " _
                           & " WHERE VHD_BSU_ID='" & BSuId & "' " _
                           & " AND VHD_DOCTYPE='" & docType & "' AND VHD_DOCNO='" & DOcNo & "' <%FILTER%>"
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim repSource As New MyReportClass
        cmd.Connection = New SqlConnection(str_conn)
        repSource.Command = cmd
        Return repSource
    End Function

    'Swapna added
    Public Shared Function BankPaymentVoucher(ByVal DOcNo As String, ByVal DOcDate As String, ByVal BSuId As String, ByVal FYear As String) As MyReportClass
        Dim str_Sql, strFilter As String
        Dim docType As String = DOcNo.Substring(2, 2)
        'strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & DOcNo _
        '& "' and VHH_DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(DOcDate)) & "' and VHH_BSU_ID in('" & BSuId & "')"
        'str_Sql = "SELECT * FROM vw_OSA_VOUCHER where" + strFilter
        ' code changed by swapna
        strFilter = " VHH_FYEAR = " & FYear & " and VHH_DOCTYPE = '" & docType & "' and VHH_DOCNO = '" & DOcNo _
       & "' and VHH_BSU_ID in ('" & BSuId & "') "
        If DOcDate <> "" Then
            strFilter = strFilter & " and VHH_DOCDT = '" & String.Format("{0:MM-dd-yyyy}", CDate(DOcDate)) & "'"
        End If
        str_Sql = "SELECT * FROM vw_OSA_VOUCHER where " + strFilter

        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)

            params("UserName") = HttpContext.Current.Session("sUsr_name")
            params("Duplicated") = PrinterFunctions.UpdatePrintCopy(BSuId, HttpContext.Current.Session("BANKTRAN"), DOcDate)
            'params("voucherName") = "PAYMENT VOUCHER(BANK)"
            params("voucherName") = "BANK PAYMENT VOUCHER"
            params("Summary") = False
            params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
            repSource.VoucherName = "PAYMENT VOUCHER(BANK)"
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = True
            repSource.SubReport = AccountFunctions.AddPostingDetails(DOcNo, HttpContext.Current.Session("BANKTRAN"), BSuId)

            Dim repSourceSubRep() As MyReportClass
            repSourceSubRep = repSource.SubReport
            Dim subReportLength As Integer = 0
            If repSource.SubReport Is Nothing Then
                ReDim repSourceSubRep(0)
                repSource.SubReport = repSourceSubRep
            Else
                subReportLength = repSourceSubRep.Length
                ReDim Preserve repSourceSubRep(subReportLength)
            End If

            repSourceSubRep(subReportLength) = New MyReportClass
            Dim cmdNarrationDetails As New SqlCommand
            cmdNarrationDetails.CommandText = "SELECT VHH_SUB_ID, VHH_BSU_ID, VHH_FYEAR, VHH_DOCTYPE, " & _
            " VHH_DOCNO, VHD_LINEID, VHD_NARRATION FROM vw_OSA_VOUCHER where" + strFilter
            cmdNarrationDetails.Connection = New SqlConnection(str_conn)
            cmdNarrationDetails.CommandType = CommandType.Text
            repSourceSubRep(subReportLength).Command = cmdNarrationDetails
            repSource.SubReport = repSourceSubRep
            repSource.ResourceName = "../RPT_Files/BankPaymentReport.rpt"
        End If
        Return repSource
    End Function
End Class
