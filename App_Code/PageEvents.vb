Imports System.Web
'Namespace BrockAllen.Web
Public Class MasterAndThemeModule
    Implements IHttpModule
    Public Event PreInit As PageEvent

    Public Sub Dispose() Implements IHttpModule.Dispose
    End Sub

    Public Sub Init(ByVal context As System.Web.HttpApplication) _
            Implements IHttpModule.Init

        AddHandler context.PreRequestHandlerExecute, _
                   AddressOf Application_PreRequestHandlerExecute
    End Sub

    Public Sub Application_PreRequestHandlerExecute(ByVal sender As Object, _
                                                    ByVal e As EventArgs)
        Dim application As HttpApplication
        application = DirectCast(sender, HttpApplication)

        Dim page As Page
        page = TryCast(application.Context.CurrentHandler, Page)

        If Not page Is Nothing Then

            AddHandler page.PreInit, _
                    AddressOf Page_PreInit
        End If
    End Sub

    Public Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs)

        'Dim page As Page
        'page = DirectCast(sender, Page)
        ' page.MasterPageFile = "code to select master page"
        RaiseEvent PreInit(Me, New PageEventArgs(DirectCast(sender, Page)))

    End Sub

End Class
Public Delegate Sub PageEvent(ByVal sender As Object, ByVal e As PageEventArgs)

Public Class PageEventArgs
    Inherits EventArgs
    Public Sub New(ByVal page As Page)
        _page = page
    End Sub
    Private _page As Page
    Public ReadOnly Property Page() As Page
        Get
            Return _page
        End Get
    End Property
End Class
'End Namespace