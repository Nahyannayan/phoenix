Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class MyReportClass
    Public Sub New()
        MyBase.New() 
        frmCmd = True
    End Sub

    'Dim cmdType As CommandType
    'Dim cmdText As String
    'Dim cmdParams As SqlParameterCollection

    Dim cmd As SqlCommand

    Dim resName As String
    Dim voucher As String
    Dim param As Hashtable
    Dim Formula As Hashtable
    Dim frmCmd As Boolean
    Dim subRep As MyReportClass()
    Dim lDisplayGroupTree As Boolean = False
    Dim bIncludeBSUImage As Boolean
    Dim strHeaderBSUID As String
    Dim bReportUniqueName As String

    Public Property ReportUniqueName() As String
        Get
            Return bReportUniqueName
        End Get
        Set(ByVal value As String)
            bReportUniqueName = value
        End Set
    End Property

    Public Property DisplayGroupTree() As Boolean
        Get
            Return lDisplayGroupTree
        End Get
        Set(ByVal value As Boolean)
            lDisplayGroupTree = value
        End Set
    End Property
    Public Property IncludeBSUImage() As Boolean
        Get
            Return bIncludeBSUImage
        End Get
        Set(ByVal value As Boolean)
            bIncludeBSUImage = value
        End Set
    End Property

    Public Property SubReport() As MyReportClass()
        Get
            Return subRep
        End Get
        Set(ByVal value As MyReportClass())
            subRep = value
        End Set
    End Property
    Public Property Parameter() As Hashtable
        Get
            Return param
        End Get
        Set(ByVal value As Hashtable)
            If param Is Nothing Then
                param = New Hashtable()
            End If
            param = value
        End Set
    End Property
    Public Property Formulas() As Hashtable
        Get
            Return Formula
        End Get
        Set(ByVal value As Hashtable)
            If Formula Is Nothing Then
                Formula = New Hashtable()
            End If
            Formula = value
        End Set
    End Property
    Public Property ResourceName() As String
        Get
            Return resName
        End Get
        Set(ByVal value As [String])
            resName = value
        End Set
    End Property

    Public Property VoucherName() As String
        Get
            Return voucher
        End Get
        Set(ByVal value As [String])
            voucher = value
        End Set
    End Property

    Public Property Command() As SqlCommand
        Get
            Return cmd
        End Get
        Set(ByVal value As SqlCommand)
            cmd = value
        End Set
    End Property

    Public Property GetDataSourceFromCommand() As Boolean
        Get
            Return frmCmd
        End Get
        Set(ByVal value As Boolean)
            frmCmd = value
        End Set
    End Property

    Public Property HeaderBSUID() As String
        Get
            Return strHeaderBSUID
        End Get
        Set(ByVal value As [String])
            strHeaderBSUID = value
        End Set
    End Property


    'Public WriteOnly Property CommandType() As CommandType
    '    Set(ByVal value As CommandType)
    '        cmdType = value
    '    End Set
    'End Property

    'Public WriteOnly Property CommandText() As String
    '    Set(ByVal value As String)
    '        cmdText = value
    '    End Set
    'End Property

    'Public WriteOnly Property Parameter() As SqlParameterCollection
    '    Set(ByVal value As SqlParameterCollection)
    '        cmdParams = value
    '    End Set
    'End Property

End Class

Public Class RepClass
    Inherits CrystalDecisions.CrystalReports.Engine.ReportClass
    Public Sub New()
        MyBase.New()
    End Sub

    Dim resName As String

    Public Overrides Property ResourceName() As String
        Get
            Return resName
        End Get
        Set(ByVal value As [String])
            resName = value
        End Set
    End Property
End Class
