﻿Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Data.SqlClient

Namespace OasisYearly

    Public Class Oasis_Yearly


        Public Shared Sub BindDepartmentsHead(ByVal TreeItemDepartments As TreeView, ByVal Expand As Boolean, Optional ByVal Emp_id As String = Nothing, Optional ByVal edit As Boolean = False)

            TreeItemDepartments.Nodes.Clear()
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@DEPT_PRI_ID", 0)
            param(1) = New SqlClient.SqlParameter("@EMP_ID", Emp_id)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "BIND_TREE_DEPARTMENTS", param)


            If ds.Tables(0).Rows.Count > 0 Then
                Dim i = 0
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim ParentNode As New TreeNode
                    ParentNode.Text = "<font color='" & ds.Tables(0).Rows(i).Item("DEPT_COLOR").ToString() & "'><strong>" + ds.Tables(0).Rows(i).Item("DEPT_DES").ToString() + "</strong></font>"
                    ParentNode.Value = ds.Tables(0).Rows(i).Item("DEPT_ID").ToString()
                    If edit Then
                        ParentNode.NavigateUrl = GetNavigateDepartmentEdit(ds.Tables(0).Rows(i).Item("DEPT_ID").ToString())
                    End If
                    TreeItemDepartments.Nodes.Add(ParentNode)
                Next

            End If

            If Expand Then
                TreeItemDepartments.ExpandAll()
            Else
                TreeItemDepartments.CollapseAll()
            End If

        End Sub

        Public Shared Sub BindDepartments(ByVal TreeItemDepartments As TreeView, ByVal Expand As Boolean, Optional ByVal Emp_id As String = Nothing)

            TreeItemDepartments.Nodes.Clear()
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@DEPT_PRI_ID", 0)
            param(1) = New SqlClient.SqlParameter("@EMP_ID", Emp_id)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "BIND_TREE_DEPARTMENTS", param)


            If ds.Tables(0).Rows.Count > 0 Then
                Dim i = 0
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim ParentNode As New TreeNode
                    ParentNode.Text = "<font color='" & ds.Tables(0).Rows(i).Item("DEPT_COLOR").ToString() & "'><strong>" + ds.Tables(0).Rows(i).Item("DEPT_DES").ToString() + "</strong></font>"
                    ParentNode.Value = ds.Tables(0).Rows(i).Item("DEPT_ID").ToString()
                    TreeItemDepartments.Nodes.Add(ParentNode)
                Next

            End If

            For Each node As TreeNode In TreeItemDepartments.Nodes
                BindDepartments_BindChildNodes(node, Emp_id)
            Next

            If Expand Then
                TreeItemDepartments.ExpandAll()
            Else
                TreeItemDepartments.CollapseAll()
            End If

        End Sub

        Private Shared Sub BindDepartments_BindChildNodes(ByVal ParentNode As TreeNode, Optional ByVal Emp_id As String = Nothing)

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()

            Dim childnodeValue = ParentNode.Value

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@DEPT_PRI_ID", childnodeValue)
            param(1) = New SqlClient.SqlParameter("@EMP_ID", Emp_id)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "BIND_TREE_DEPARTMENTS", param)


            If ds.Tables(0).Rows.Count > 0 Then
                Dim i = 0
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim ChildNode As New TreeNode
                    ChildNode.Text = "<font color='" & ds.Tables(0).Rows(i).Item("DEPT_COLOR").ToString() & "'><strong>" + ds.Tables(0).Rows(i).Item("DEPT_DES").ToString() + "</strong></font>"
                    ChildNode.Value = ds.Tables(0).Rows(i).Item("DEPT_ID").ToString()
                    ParentNode.ChildNodes.Add(ChildNode)

                Next
            End If


            For Each node As TreeNode In ParentNode.ChildNodes
                BindDepartments_BindChildNodes(node, Emp_id)
            Next


        End Sub



        Public Shared Function GetTreeNodeValue(ByVal Tree As TreeView, Optional ByRef SelectedNode As TreeNode = Nothing) As String
            Dim val = ""
            For Each node As TreeNode In Tree.Nodes
                If node.Checked Then
                    val = node.Value
                    SelectedNode = node
                    node.Checked = False
                    Exit For
                Else
                    val = GetNodeValue(node, SelectedNode)
                    If val <> "" Then
                        Exit For
                    End If
                End If
            Next
            Return val
        End Function

        Private Shared Function GetNodeValue(ByVal ParentNode As TreeNode, Optional ByRef SelectedNode As TreeNode = Nothing) As String
            Dim val = ""
            For Each node As TreeNode In ParentNode.ChildNodes
                If node.Checked Then
                    val = node.Value
                    SelectedNode = node
                    node.Checked = False
                    Exit For
                Else
                    val = GetNodeValue(node, SelectedNode)
                    If val <> "" Then
                        Exit For
                    End If
                End If
            Next
            Return val
        End Function

        Private Shared Function GetNavigateLevelLevelReports(ByVal pId As String) As String

            Return String.Format("javascript:var popup = window.showModalDialog('../../Reports/ASPX/rptTaskStatusPhaseTaskReports.aspx?LevelId={0}', '','dialogHeight:500px;dialogWidth:800px;scroll:auto;resizable:yes;'); window.location.reload();", pId)

        End Function

        Private Shared Function GetNavigateLevelEditUrl(ByVal pId As String) As String

            Return String.Format("javascript:var popup = window.showModalDialog('YearlyProgramPlannerEdit.aspx?LevelId={0}', '','dialogHeight:500px;dialogWidth:500px;scroll:auto;resizable:yes;'); window.location.reload();", pId)

        End Function

        Private Shared Function GetNavigateLevelEmpAssignUrl(ByVal pId As String) As String

            Return String.Format("javascript:var popup = window.showModalDialog('YearlyProgramPlannerTaskEmpAssign.aspx?LevelId={0}', '','dialogHeight:800px;dialogWidth:1000px;scroll:auto;resizable:yes;');", pId)

        End Function

        Private Shared Function GetNavigateDepartmentEdit(ByVal pId As String) As String

            Return String.Format("javascript:var popup = window.showModalDialog('YearlyDepartmentsEdit.aspx?dep_id={0}', '','dialogHeight:500px;dialogWidth:600px;scroll:auto;resizable:yes;'); window.location.reload();", pId)

        End Function


        Private Shared Function GetNavigateLevelAssignUrl(ByVal pId As String) As String

            Return String.Format("javascript:var popup = window.showModalDialog('YearlyPhaseView.aspx?LevelId={0}', '','dialogHeight:800px;dialogWidth:1000px;scroll:auto;resizable:yes;');", pId)

        End Function

        Private Shared Function GetNavigateTaskDetails(ByVal pId As String) As String

            Return String.Format("javascript:var popup = window.showModalDialog('YearlyTaskDetailView.aspx?dep_id={0}', '','dialogHeight:500px;dialogWidth:850px;scroll:auto;resizable:yes;'); window.location.reload();", pId)

        End Function

        Private Shared Function GetNavigateTaskEntry(ByVal pId As String) As String

            Return String.Format("javascript:var popup = window.showModalDialog('YearlyTaskEntry.aspx?dep_id={0}', '','dialogHeight:800px;dialogWidth:850px;scroll:auto;resizable:yes;'); window.location.reload();", pId)

        End Function

        Public Shared Function BindDepartmentsTask(ByVal pParms() As SqlClient.SqlParameter) As DataSet

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim Sql_Query = "INSERT_EDIT_DELETE_YEARLY_TASK"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, Sql_Query, pParms)
            Return ds

        End Function

        Public Shared Sub BindDepartmentsTask(ByVal TreeItemDepartments As TreeView, ByVal Expand As Boolean, Optional ByVal Emp_id As String = Nothing, Optional ByVal Entry As Boolean = False)

            TreeItemDepartments.Nodes.Clear()
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@DEPT_PRI_ID", 0)
            param(1) = New SqlClient.SqlParameter("@EMP_ID", Emp_id)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "BIND_TREE_TASK_DEPARTMENTS", param)

            If ds.Tables(0).Rows.Count > 0 Then
                Dim i = 0
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim ParentNode As New TreeNode
                    ParentNode.Text = "<font color='" & ds.Tables(0).Rows(i).Item("DEPT_COLOR").ToString() & "'><strong>" + ds.Tables(0).Rows(i).Item("DEPT_DES").ToString() + "</strong></font>"
                    ParentNode.Value = ds.Tables(0).Rows(i).Item("DEPT_ID").ToString()
                    ParentNode.ShowCheckBox = False
                    If Entry = True And ds.Tables(0).Rows(i).Item("DEPT_PRI_ID").ToString() <> 0 Then
                        ParentNode.NavigateUrl = GetNavigateTaskEntry(ds.Tables(0).Rows(i).Item("DEPT_ID").ToString())
                    End If

                    TreeItemDepartments.Nodes.Add(ParentNode)

                Next

            End If

            For Each node As TreeNode In TreeItemDepartments.Nodes
                BindDepartmentsTasking(node, Emp_id, Entry)
            Next

            If Expand Then
                TreeItemDepartments.ExpandAll()
            Else
                TreeItemDepartments.CollapseAll()
            End If

        End Sub

        Private Shared Sub BindDepartmentsTasking(ByVal ParentNode As TreeNode, Optional ByVal Emp_id As String = Nothing, Optional ByVal Entry As Boolean = False)

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()

            Dim childnodeValue = ParentNode.Value

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@DEPT_PRI_ID", childnodeValue)
            param(1) = New SqlClient.SqlParameter("@EMP_ID", Emp_id)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "BIND_TREE_TASK_DEPARTMENTS", param)

            If ds.Tables(0).Rows.Count > 0 Then

                Dim i = 0
                Dim dep_name = ""
                Dim task_no = ""
                Dim cost = ""
                Dim com_days = ""
                Dim dep_id = ""
                Dim TaskFlag = ""
                For i = 0 To ds.Tables(0).Rows.Count - 1

                    TaskFlag = ds.Tables(0).Rows(i).Item("TASK_FLAG").ToString()

                    Dim ChildNode As New TreeNode

                    If TaskFlag = "False" Then

                        ChildNode.Text = "<font color='" & ds.Tables(0).Rows(i).Item("DEPT_COLOR").ToString() & "'><strong>" + ds.Tables(0).Rows(i).Item("DEPT_DES").ToString() + "</strong></font>"

                        If Entry Then
                            ChildNode.NavigateUrl = GetNavigateTaskEntry(ds.Tables(0).Rows(i).Item("DEPT_ID").ToString())
                        End If
                        ChildNode.ShowCheckBox = False
                    Else

                        dep_name = ds.Tables(0).Rows(i).Item("DEPT_DES").ToString()
                        task_no = ds.Tables(0).Rows(i).Item("DEPT_ID").ToString()
                        cost = ds.Tables(0).Rows(i).Item("COST").ToString()
                        com_days = ds.Tables(0).Rows(i).Item("TASK_COMPLETION_DAYS").ToString()
                        dep_id = ds.Tables(0).Rows(i).Item("DEPT_ID").ToString()

                        ChildNode.Text = "<font color='" & ds.Tables(0).Rows(i).Item("DEPT_COLOR").ToString() & "'><strong>" + task_no + " - " + dep_name + "</strong></font>"
                        ChildNode.Text += "<font color='BLUE'><strong> - Cost : " + cost + " AED </strong></font>"
                        ChildNode.Text += "<font color='ORANGE'><strong> - Days : " + com_days + "</strong></font>"
                        ChildNode.NavigateUrl = GetNavigateTaskDetails(dep_id)
                        ChildNode.ToolTip = "Task"
                        ChildNode.ShowCheckBox = True
                    End If
                    ChildNode.Value = ds.Tables(0).Rows(i).Item("DEPT_ID").ToString()
                    ParentNode.ChildNodes.Add(ChildNode)

                Next
            End If


            For Each node As TreeNode In ParentNode.ChildNodes
                BindDepartmentsTasking(node, Emp_id, Entry)
            Next


        End Sub


        Public Shared Function BindProjectsDetails(ByVal pParms() As SqlClient.SqlParameter) As DataSet

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim Sql_Query = "INSERT_EDIT_DELETE_YEARLY_PROGRAM"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, Sql_Query, pParms)
            Return ds

        End Function

        Public Shared Function GetProjects(ByVal Projectid As String) As DataSet

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim Sql_Query = "Select * from YEARLY_PROGRAM WHERE PROGRAM_ID ='" & Projectid & "'  "
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            Return ds

        End Function


        Public Shared Function CheckDates(ByVal Startdate As String, ByVal EndDate As String) As Boolean

            If Startdate.Trim() <> "" And EndDate.Trim() <> "" Then

                Return Convert.ToDateTime(Startdate) <= Convert.ToDateTime(EndDate)
            Else
                Return True
            End If

        End Function


        Public Shared Function BindCountries(ByVal Drop As DropDownList, ByVal DefaultVal As Boolean, ByVal SelectedVal As String) As DropDownList
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
            Dim Sql_Query = "SELECT CTY_ID,CTY_DESCR FROM COUNTRY_M ORDER BY CTY_DESCR "
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            Drop.DataSource = ds
            Drop.DataTextField = "CTY_DESCR"
            Drop.DataValueField = "CTY_ID"
            Drop.DataBind()
            If DefaultVal Then
                Dim list As New ListItem
                list.Text = "Select a Country"
                list.Value = "-1"
                Drop.Items.Insert(0, list)
            End If

            If SelectedVal <> "" Then
                Drop.SelectedValue = SelectedVal
            End If
            Return Drop
        End Function


        Public Shared Function BindPIStuatus(ByVal Drop As DropDownList, ByVal DefaultVal As Boolean, ByVal SelectedVal As String) As DropDownList

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim Sql_Query = "select * from dbo.PI_STATUS_MASTER order by PI_STATUS_ID "
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)


            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim id = ds.Tables(0).Rows(i).Item("PI_STATUS_ID").ToString()
                Dim text = ds.Tables(0).Rows(i).Item("PI_STATUS_DESC").ToString()
                Dim color = ds.Tables(0).Rows(i).Item("PI_STATUS_COLOUR").ToString()

                Dim list As New ListItem(text, id)
                list.Attributes.Add("style", "background-color:" & color & ";font-weight:bold")
                Drop.Items.Add(list)

            Next

            If DefaultVal Then
                Dim list As New ListItem
                list.Text = "Select a PI Status"
                list.Value = "-1"
                Drop.Items.Insert(0, list)
            End If

            If SelectedVal <> "" Then
                Drop.SelectedValue = SelectedVal
            End If

            Return Drop

        End Function


        Public Shared Function BindPIType(ByVal Drop As DropDownList, ByVal DefaultVal As Boolean, ByVal SelectedVal As String) As DropDownList
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim Sql_Query = "SELECT TYPE_ID,TYPE_DESC FROM PI_TYPE_MASTER ORDER BY TYPE_DESC "
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            Drop.DataSource = ds
            Drop.DataTextField = "TYPE_DESC"
            Drop.DataValueField = "TYPE_ID"
            Drop.DataBind()
            If DefaultVal Then
                Dim list As New ListItem
                list.Text = "Select a Type"
                list.Value = "-1"
                Drop.Items.Insert(0, list)
            End If

            If SelectedVal <> "" Then
                Drop.SelectedValue = SelectedVal
            End If
            Return Drop
        End Function

        Public Shared Function BindPartner(ByVal Drop As DropDownList, ByVal DefaultVal As Boolean, ByVal SelectedVal As String) As DropDownList
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim Sql_Query = "SELECT PARNER_ID,PARTNER_NAME FROM PARTNER_MASTER ORDER BY PARTNER_NAME "
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            Drop.DataSource = ds
            Drop.DataTextField = "PARTNER_NAME"
            Drop.DataValueField = "PARNER_ID"
            Drop.DataBind()
            If DefaultVal Then
                Dim list As New ListItem
                list.Text = "    --    "
                list.Value = "-1"
                Drop.Items.Insert(0, list)
            End If

            If SelectedVal <> "" Then
                Drop.SelectedValue = SelectedVal
            End If
            Return Drop
        End Function

        Public Shared Function BindPartnerDetails(ByVal pParms() As SqlClient.SqlParameter) As DataSet
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_PARTNER_MASTER", pParms)

            Return ds
        End Function

        Public Shared Function BindPIInitiation(ByVal pParms() As SqlClient.SqlParameter) As DataSet

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_PI_MASTER", pParms)

            Return ds

        End Function


        Public Shared Sub BindProgramLevels(ByVal TreeItemDepartments As TreeView, ByVal Expand As Boolean, ByVal Program_id As String, Optional ByVal Emp_id As String = Nothing, Optional ByVal AssignNavigate As Boolean = False, Optional ByVal ShowDates As Boolean = False, Optional ByVal NavigateNumber As Integer = Nothing)

            TreeItemDepartments.Nodes.Clear()
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@LEVEL_PRI_ID", 0)
            param(1) = New SqlClient.SqlParameter("@PROGRAM_ID", Program_id)
            param(2) = New SqlClient.SqlParameter("@EMP_ID", Emp_id)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "BIND_TREE_PHASE", param)


            Dim startdate = ""
            Dim enddate = ""
            Dim datevalue = ""
            Dim ParentValue = 0
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i = 0
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim ParentNode As New TreeNode
                    ParentNode.Text = ds.Tables(0).Rows(i).Item("LEVEL_DES").ToString()
                    ParentNode.Value = ds.Tables(0).Rows(i).Item("LEVEL_ID").ToString()

                    If ShowDates Then

                        startdate = ds.Tables(0).Rows(i).Item("START_DATE").ToString()
                        enddate = ds.Tables(0).Rows(i).Item("END_DATE").ToString()
                        datevalue = ""

                        If startdate <> "" Then
                            datevalue = "<font color='GREEN' style='font-size:x-small'><strong> ( Start Date:</strong></font>" & Convert.ToDateTime(startdate).ToString("dd/MMM/yyyy")
                        End If
                        If enddate <> "" Then
                            datevalue &= "<font color='RED' style='font-size:x-small'><strong> - End Date:</strong></font>" & Convert.ToDateTime(enddate).ToString("dd/MMM/yyyy") & " )"
                        End If

                        ParentNode.Text &= datevalue

                    End If

                    If AssignNavigate And ParentValue <> 0 Then
                        ParentValue = 1
                        If NavigateNumber = 1 Then ''Phase Emp Assign
                            ParentNode.NavigateUrl = GetNavigateLevelEmpAssignUrl(ds.Tables(0).Rows(i).Item("LEVEL_ID").ToString())
                        ElseIf NavigateNumber = 2 Then '' Phase Edit
                            ParentNode.NavigateUrl = GetNavigateLevelEditUrl(ds.Tables(0).Rows(i).Item("LEVEL_ID").ToString())
                        ElseIf NavigateNumber = 3 Then '' Phase Reports
                            ParentNode.NavigateUrl = GetNavigateLevelLevelReports(ds.Tables(0).Rows(i).Item("LEVEL_ID").ToString())
                        Else
                            ParentNode.NavigateUrl = GetNavigateLevelAssignUrl(ds.Tables(0).Rows(i).Item("LEVEL_ID").ToString())
                        End If

                    End If

                    TreeItemDepartments.Nodes.Add(ParentNode)
                Next

            End If

            For Each node As TreeNode In TreeItemDepartments.Nodes
                BindProgramLevels_BindChildNodes(node, Program_id, Emp_id, AssignNavigate, ShowDates, NavigateNumber)
            Next

            If Expand Then
                TreeItemDepartments.ExpandAll()
            End If

        End Sub

        Private Shared Sub BindProgramLevels_BindChildNodes(ByVal ParentNode As TreeNode, ByVal Program_id As String, Optional ByVal Emp_id As String = Nothing, Optional ByVal AssignNavigate As Boolean = False, Optional ByVal ShowDates As Boolean = False, Optional ByVal NavigateNumber As Integer = Nothing)

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()

            Dim childnodeValue = ParentNode.Value

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@LEVEL_PRI_ID", childnodeValue)
            param(1) = New SqlClient.SqlParameter("@PROGRAM_ID", Program_id)
            param(2) = New SqlClient.SqlParameter("@EMP_ID", Emp_id)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "BIND_TREE_PHASE", param)

            Dim startdate = ""
            Dim enddate = ""
            Dim datevalue = ""

            If ds.Tables(0).Rows.Count > 0 Then
                Dim i = 0
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim ChildNode As New TreeNode
                    ChildNode.Text = ds.Tables(0).Rows(i).Item("LEVEL_DES").ToString()
                    ChildNode.Value = ds.Tables(0).Rows(i).Item("LEVEL_ID").ToString()

                    If ShowDates Then

                        startdate = ds.Tables(0).Rows(i).Item("START_DATE").ToString()
                        enddate = ds.Tables(0).Rows(i).Item("END_DATE").ToString()
                        datevalue = ""

                        If startdate <> "" Then
                            datevalue = "<font color='GREEN'><strong> ( Start Date:</strong></font>" & Convert.ToDateTime(startdate).ToString("dd/MMM/yyyy")
                        End If
                        If enddate <> "" Then
                            datevalue &= "<font color='RED'><strong> - End Date:</strong></font>" & Convert.ToDateTime(enddate).ToString("dd/MMM/yyyy") & " )"
                        End If

                        ChildNode.Text &= datevalue

                    End If

                    If AssignNavigate Then
                        If NavigateNumber = 1 Then ''Phase Emp Assign

                            ChildNode.NavigateUrl = GetNavigateLevelEmpAssignUrl(ds.Tables(0).Rows(i).Item("LEVEL_ID").ToString())

                        ElseIf NavigateNumber = 2 Then '' Phase Edit
                            ChildNode.NavigateUrl = GetNavigateLevelEditUrl(ds.Tables(0).Rows(i).Item("LEVEL_ID").ToString())
                        ElseIf NavigateNumber = 3 Then '' Phase Reports
                            ChildNode.NavigateUrl = GetNavigateLevelLevelReports(ds.Tables(0).Rows(i).Item("LEVEL_ID").ToString())
                        Else
                            ChildNode.NavigateUrl = GetNavigateLevelAssignUrl(ds.Tables(0).Rows(i).Item("LEVEL_ID").ToString())
                        End If

                    End If

                    ParentNode.ChildNodes.Add(ChildNode)

                Next
            End If


            For Each node As TreeNode In ParentNode.ChildNodes
                BindProgramLevels_BindChildNodes(node, Program_id, Emp_id, AssignNavigate, ShowDates, NavigateNumber)
            Next


        End Sub


        Public Shared Function BindProgramStatus(ByVal Drop As DropDownList, ByVal DefaultVal As Boolean, ByVal SelectedVal As String) As DropDownList
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim Sql_Query = "SELECT * FROM PROGRAM_STATUS_MASTER ORDER BY PROGRAM_STATUS_ID "
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim id = ds.Tables(0).Rows(i).Item("PROGRAM_STATUS_ID").ToString()
                Dim text = ds.Tables(0).Rows(i).Item("PROGRAM_STATUS").ToString()
                Dim color = ds.Tables(0).Rows(i).Item("PROGRAM_COLOR").ToString()

                Dim list As New ListItem(text, id)
                list.Attributes.Add("style", "background-color:" & color & ";font-weight:bold")
                Drop.Items.Add(list)

            Next

            If DefaultVal Then
                Dim list As New ListItem
                list.Text = "   --   "
                list.Value = "-1"
                Drop.Items.Insert(0, list)
            End If

            If SelectedVal <> "" Then
                Drop.SelectedValue = SelectedVal
            End If

            Return Drop


        End Function

        Public Shared Function BindPercentage(ByVal DropPCompleted As DropDownList, Optional ByVal StartVal As Integer = 0) As DropDownList
            Dim i = 0
            For i = StartVal To 100
                Dim list As New ListItem
                list.Text = i & " %"
                list.Value = i
                DropPCompleted.Items.Insert((i - StartVal), list)
            Next

            Dim list2 As New ListItem
            list2.Text = "% Completed"
            list2.Value = "-1"
            DropPCompleted.Items.Insert(0, list2)

            Return DropPCompleted
        End Function

        Public Shared Function BindProgramAssignedTasks(ByVal pParms() As SqlClient.SqlParameter) As DataSet

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_YEARLY_PROGRAM_LEVELS_TASK", pParms)

            Return ds

        End Function

        Public Shared Function BindPhaseMeetings(ByVal pParms() As SqlClient.SqlParameter) As DataSet

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_YEARLY_PHASE_MEETINGS", pParms)

            Return ds

        End Function


        Public Shared Function GetTaskAssignedEmployees(ByVal pParms() As SqlClient.SqlParameter) As DataSet

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_YEARLY_PROGRAM_LEVELS_TASK", pParms)
            Return ds

        End Function

        Public Shared Function ValidatePhaseDates(ByVal Program_id As Integer, ByVal level_pri_id As Integer, ByVal StartDate As String, ByVal EndDate As String, ByVal spoption As Integer) As String

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@PROGRAM_ID", Program_id)
            pParms(1) = New SqlClient.SqlParameter("@LEVEL_PRI_ID", level_pri_id)
            pParms(2) = New SqlClient.SqlParameter("@START_DATE", StartDate)
            pParms(3) = New SqlClient.SqlParameter("@END_DATE", EndDate)
            pParms(4) = New SqlClient.SqlParameter("@OPTION", spoption)

            Dim returnval = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "VALIDATE_PHASE_DATES", pParms)
            Return returnval

        End Function

        Public Shared Function ValidatePhaseDatesEdit(ByVal levelid As String, ByVal startdate As String, ByVal endate As String, ByRef TextValue As String) As Boolean

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim validate As Boolean = False
            Dim program_id As Integer

            Dim query = "SELECT PROGRAM_ID FROM YEARLY_PROGRAM_LEVELS WHERE LEVEL_ID='" & levelid & "'"
            program_id = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, query)

            ''Validate Phase Dates with Child Phase Dates
            Dim vdate = OasisYearly.Oasis_Yearly.ValidatePhaseDates(program_id, levelid, startdate, endate, 2)
            If vdate = "DONE" Then

                '' Now validate for the parent child dates
                vdate = OasisYearly.Oasis_Yearly.ValidatePhaseDates(program_id, levelid, startdate, endate, 3)
                If vdate = "DONE" Then
                    validate = True
                Else
                    TextValue = vdate
                    validate = False
                End If

            Else
                TextValue = vdate
                validate = False
            End If

            Return validate
        End Function


        Public Shared Function ValidateTaskAssignDates(ByVal level_id As Integer, ByVal program_task_id As String, ByVal StartDate As String, ByVal EndDate As String, ByVal spoption As Integer, ByRef Textvalue As String) As Boolean
            Dim returnval = False
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionYearly").ConnectionString()
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@LEVEL_ID", level_id)

            If program_task_id <> "" Then
                pParms(1) = New SqlClient.SqlParameter("@PROGRAM_TASK_ID", program_task_id)
            End If

            pParms(2) = New SqlClient.SqlParameter("@START_DATE", StartDate)
            pParms(3) = New SqlClient.SqlParameter("@END_DATE", EndDate)
            pParms(4) = New SqlClient.SqlParameter("@OPTION", spoption)

            Textvalue = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "VALIDATE_TASK_DATES", pParms)
            If Textvalue = "DONE" Then
                returnval = True
            End If

            Return returnval

        End Function

        Public Shared Function ValidateTaskAssignDatesEdit(ByVal level_id As Integer, ByVal program_task_id As String, ByVal StartDate As String, ByVal EndDate As String, ByVal spoption As Integer, ByRef Textvalue As String, ByVal transaction As SqlTransaction) As Boolean
            Dim returnval = False

            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@LEVEL_ID", level_id)

            If program_task_id <> "" Then
                pParms(1) = New SqlClient.SqlParameter("@PROGRAM_TASK_ID", program_task_id)
            End If

            pParms(2) = New SqlClient.SqlParameter("@START_DATE", StartDate)
            pParms(3) = New SqlClient.SqlParameter("@END_DATE", EndDate)
            pParms(4) = New SqlClient.SqlParameter("@OPTION", spoption)

            Textvalue = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "VALIDATE_TASK_DATES", pParms)
            If Textvalue = "DONE" Then
                returnval = True
            End If

            Return returnval

        End Function




    End Class
End Namespace
