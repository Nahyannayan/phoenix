﻿Imports Microsoft.VisualBasic
Imports Microsoft.WindowsAzure.Storage
Imports Microsoft.WindowsAzure.Storage.Blob
Imports Microsoft.WindowsAzure
Imports Microsoft.Azure
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.IO
Imports System.Configuration
Public Class StorageHelper

    Public Function UploadBlob(ByVal stream As Stream, ByVal fileName As String, ByVal containerName As String, ByVal subContainerName As String, ByRef uploadedPath As String) As Boolean
        Dim bFlag As Boolean = False
        uploadedPath = ""

        Try
            '  Retrieve storage account from connection string.
            Dim storageAccount As CloudStorageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings("AzureStorageConnectionString"))
            '   Create the blob client.
            Dim blobClient As CloudBlobClient = storageAccount.CreateCloudBlobClient()
            '   Retrieve reference to a previously created container.
            Dim container As CloudBlobContainer = blobClient.GetContainerReference(containerName)
            '   Retrieve reference to a blob named "myblob".
            Dim blockBlob As CloudBlockBlob = container.GetBlockBlobReference(subContainerName & fileName)
            ' Create Or overwrite the "myblob" blob with contents from a local file.
            '   using (var fileStream = System.IO.File.OpenRead(path))
            '   {
            stream.Position = 0
            blockBlob.UploadFromStream(stream)
            '   }
            bFlag = True
            uploadedPath = blockBlob.Uri.ToString()
        Catch ex As Exception
            Dim errmsg = ex.Message
        End Try

        Return bFlag
    End Function
    Public Function DownloadBlob(ByVal fileName As String, ByVal containerName As String, ByVal subContainerName As String) As Stream
        ' Retrieve storage account from connection string.
        Dim storageAccount As CloudStorageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings("AzureStorageConnectionString"))
        ' Create the blob client.
        Dim blobClient As CloudBlobClient = storageAccount.CreateCloudBlobClient()
        ' Retrieve reference to a previously created container.
        Dim container As CloudBlobContainer = blobClient.GetContainerReference(containerName)
        ' Retrieve reference to a blob named "photo1.jpg".
        Dim blockBlob As CloudBlockBlob = container.GetBlockBlobReference(subContainerName & fileName)
        Return blockBlob.OpenRead()
    End Function

    Public Function RenameBlob(ByVal OldFileName As String, ByVal NewFileName As String, ByVal containerName As String, ByVal subContainerName As String) As Boolean

        Dim bFlag As Boolean = False
        Try
            Dim storageAccount As CloudStorageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings("AzureStorageConnectionString"))
            Dim blobClient As CloudBlobClient = storageAccount.CreateCloudBlobClient()
            Dim container As CloudBlobContainer = blobClient.GetContainerReference(containerName)
            Dim blobCopy As CloudBlockBlob = container.GetBlockBlobReference(subContainerName & OldFileName)
            If blobCopy.Exists() Then
                Dim Blob As CloudBlockBlob = container.GetBlockBlobReference(subContainerName & NewFileName)
                If Blob.Exists() Then
                    blobCopy.StartCopyAsync(Blob)
                    Blob.DeleteIfExistsAsync()
                End If
            End If
            bFlag = True
        Catch ex As Exception
        End Try
        Return bFlag
    End Function
    Public Function DeleteBlob(ByVal FileName As String, ByVal containerName As String, ByVal subContainerName As String) As Boolean

        Dim bFlag As Boolean = False
        Try
            Dim storageAccount As CloudStorageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings("AzureStorageConnectionString"))
            Dim blobClient As CloudBlobClient = storageAccount.CreateCloudBlobClient()
            Dim container As CloudBlobContainer = blobClient.GetContainerReference(containerName)
            Dim blobDeleted As CloudBlockBlob = container.GetBlockBlobReference(subContainerName & FileName)
            If blobDeleted.Exists() Then
                blobDeleted.DeleteIfExistsAsync()
            End If
            bFlag = True
        Catch ex As Exception
        End Try
        Return bFlag
    End Function



End Class
