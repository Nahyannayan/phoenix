Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Threading
Imports System.IO
Imports System.Net.Mail
Namespace EmailService
    Public Class email



        Public Shared Function SendEmailPlainText(ByVal FromEmailid As String, ByVal FromDisplayName As String, ByVal ToAddressStringBuilder As StringBuilder, ByVal Subject As String, ByVal MailBody As String, ByVal TemplateId As String) As String

            Dim ReturnValue As String = ""
            Try
                Dim mailMessage As New System.Net.Mail.MailMessage()

                ''mailMessage.To.Add(ToAddressStringBuilder.ToString())
                ''mailMessage.Bcc.Add(ToAddressStringBuilder.ToString())
                mailMessage.Bcc.Add("prasanthxp@gmail.com")

                Dim mailfrom As New System.Net.Mail.MailAddress(FromEmailid, FromDisplayName)

                mailMessage.From = mailfrom

                Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString

                Dim d As New DirectoryInfo(serverpath + TemplateId + "/Attachments/")

                Dim fi() As System.IO.FileInfo
                fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)

                If fi.Length > 0 Then '' If Having Attachments

                    For Each f As System.IO.FileInfo In fi

                        Dim attach As New System.Net.Mail.Attachment(HttpContext.Current.Server.MapPath(serverpath) + TemplateId + "/Attachments/" + f.Name)
                        mailMessage.Attachments.Add(attach)

                    Next

                End If

                mailMessage.Subject = Subject
                mailMessage.Body = MailBody


                mailMessage.IsBodyHtml = True



                Dim config As System.Configuration.Configuration = _
                    WebConfigurationManager.OpenWebConfiguration( _
                    HttpContext.Current.Request.ApplicationPath)

                Dim settings As System.Net.Configuration.MailSettingsSectionGroup = _
                CType(config.GetSectionGroup("system.net/mailSettings"), _
                System.Net.Configuration.MailSettingsSectionGroup)

                'Dim credential As New System.Net.NetworkCredential( _
                '           settings.Smtp.Network.UserName, settings.Smtp.Network.Password)

                Dim client As New System.Net.Mail.SmtpClient()

                client.Host = settings.Smtp.Network.Host
                client.Port = settings.Smtp.Network.Port

                'client.Credentials = credential

                client.Send(mailMessage)

                ReturnValue = "Emails Send Successfully"
            Catch ex As Exception
                ReturnValue = "Error :" & ex.Message
            End Try

            Return ReturnValue
        End Function


        Public Shared Function SendEmailNewsLetters(ByVal FromEmailid As String, ByVal FromDisplayName As String, ByVal ToAddressStringBuilder As StringBuilder, ByVal Subject As String, ByVal MailBody As String, ByVal TemplateId As String) As String
            Dim ReturnValue As String = ""
            Try
                Dim mailMessage As New System.Net.Mail.MailMessage()

                mailMessage.To.Add(ToAddressStringBuilder.ToString())
                ''mailMessage.Bcc.Add(ToAddressStringBuilder.ToString())
                ''mailMessage.Bcc.Add("prasanthxp@gmail.com")
                ''mailMessage.To.Add("prem.sunder@gemseducation.com,charles@gemseducation.com,prasanth.s@gemseducation.com,prasanthxp@gmail.com")
                ''mailMessage.Bcc.Add("prasanth.s@gemseducation.com,prem.sunder@gemseducation.com,charles@gemseducation.com,prasanthxp@gmail.com")
                Dim mailfrom As New System.Net.Mail.MailAddress(FromEmailid, FromDisplayName)

                mailMessage.From = mailfrom

                Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString

                Dim d As New DirectoryInfo(serverpath + TemplateId + "/Attachments/")

                Dim fi() As System.IO.FileInfo
                fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)

                If fi.Length > 0 Then '' If Having Attachments

                    For Each f As System.IO.FileInfo In fi

                        Dim attach As New System.Net.Mail.Attachment(HttpContext.Current.Server.MapPath(serverpath) + TemplateId + "/Attachments/" + f.Name)
                        mailMessage.Attachments.Add(attach)

                    Next

                End If

                mailMessage.Subject = Subject
                mailMessage.Body = MailBody
                ''mailMessage.AlternateViews.Add(convertToEmbedResource(MailBody, TemplateId))

                mailMessage.IsBodyHtml = True

                Dim config As System.Configuration.Configuration = _
                    WebConfigurationManager.OpenWebConfiguration( _
                    HttpContext.Current.Request.ApplicationPath)

                Dim settings As System.Net.Configuration.MailSettingsSectionGroup = _
                CType(config.GetSectionGroup("system.net/mailSettings"), _
                System.Net.Configuration.MailSettingsSectionGroup)

                Dim credential As New System.Net.NetworkCredential( _
                           settings.Smtp.Network.UserName, settings.Smtp.Network.Password)

                Dim client As New System.Net.Mail.SmtpClient()

                ''client.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis

                client.Host = settings.Smtp.Network.Host
                client.Port = settings.Smtp.Network.Port

                client.Credentials = credential

                client.Send(mailMessage)
                ReturnValue = "News Letter Emails Send Successfully"
            Catch ex As Exception
                ReturnValue = "Error :" & ex.Message
            End Try

            Return ReturnValue


        End Function


        Public Shared Function convertToEmbedResource(ByVal emailHtml$, ByVal Templateid As String) As AlternateView

            'This is the website where the resources are located
            Dim ResourceUrl As String = WebConfigurationManager.AppSettings("WebsiteURLResource").ToString
            Dim webSiteUrl$ = ResourceUrl + Templateid + "/"

            ' The first regex finds all the url/src tags.
            Dim matchesCol As MatchCollection = Regex.Matches(emailHtml, "url\(['|\""]+.*['|\""]\)|src=[""|'][^""']+[""|']")

            Dim normalRes As Match


            Dim resCol As AlternateView = AlternateView.CreateAlternateViewFromString("", Nothing, "text/html")

            Dim resId% = 0

            ' Between the findings
            For Each normalRes In matchesCol

                Dim resPath$

                ' Replace it for the new content ID that will be embeded
                If Left(normalRes.Value, 3) = "url" Then
                    emailHtml = emailHtml.Replace(normalRes.Value, "url(cid:EmbedRes_" & resId & ")")
                Else
                    emailHtml = emailHtml.Replace(normalRes.Value, "src=""cid:EmbedRes_" & resId & """")
                End If

                ' Clean the path
                resPath = Regex.Replace(normalRes.Value, "url\(['|\""]", "")
                resPath = Regex.Replace(resPath, "src=['|\""]", "")
                resPath = Regex.Replace(resPath, "['|\""]\)", "").Replace(webSiteUrl, "").Replace("""", "")

                Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString

                ' Map it on the server
                ''resPath = Server.MapPath(resPath)
                resPath = serverpath + Templateid + "/News Letters/" + resPath
                resPath = resPath.Replace("%20", " ")
                ' Embed the resource
                If resPath.LastIndexOf("http://") = -1 Then
                    Dim theResource As LinkedResource = New LinkedResource(resPath)
                    theResource.ContentId = "EmbedRes_" & resId
                    resCol.LinkedResources.Add(theResource)
                End If

                ' Next resource ID
                resId = resId + 1

            Next


            ' Create our final object
            Dim finalEmail As AlternateView = Net.Mail.AlternateView.CreateAlternateViewFromString(emailHtml, Nothing, "text/html")
            Dim transferResource As LinkedResource

            ' And transfer all the added resources to the output object
            For Each transferResource In resCol.LinkedResources
                finalEmail.LinkedResources.Add(transferResource)
            Next

            Return finalEmail

        End Function


        Public Shared Function SendNewsLetters(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As AlternateView, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, ByVal HasAttachments As Boolean) As String
            Dim RetutnValue = ""
            Try

                Dim msg As New System.Net.Mail.MailMessage(FromEmailId, ToEmailId)

                msg.Subject = Subject

                msg.AlternateViews.Add(MailBody)

                msg.Priority = Net.Mail.MailPriority.High

                msg.IsBodyHtml = True

                '' If Attachments.

                If HasAttachments Then
                    Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
                    Dim d As New DirectoryInfo(serverpath + Templateid + "/Attachments/")
                    Dim fi() As System.IO.FileInfo
                    fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fi.Length > 0 Then '' If Having Attachments files
                        For Each f As System.IO.FileInfo In fi
                            Dim attach As New System.Net.Mail.Attachment(serverpath + Templateid + "/Attachments/" + f.Name)
                            attach.Name = f.Name
                            msg.Attachments.Add(attach)
                        Next
                    End If
                End If

                Dim client As New System.Net.Mail.SmtpClient(Host, Port)

                If Username <> "" And password <> "" Then
                    Dim creds As New System.Net.NetworkCredential(Username, password)
                    client.Credentials = creds
                End If


                ' or for other authentication with local server

                'client.Credentials= system.Net.CredentialCache.DefaultCredentials

                'client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials

                ' Async send

                'Dim MailToken As String = My.User.Name

                'client.SendAsync(msg, emaildid)

                'AddHandler client.SendCompleted, AddressOf Me.MailSendComplete

                client.Send(msg)

                RetutnValue = "Successfully sent"

            Catch ex As Exception
                RetutnValue = "Error : " & ex.Message
            End Try

            Return RetutnValue

        End Function

        'Private Sub MailSendComplete(ByVal Sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)

        '    If Not Nothing Is e.Error Then

        '        ''Microsoft.VisualBasic.MsgBox("Mail Sending error: " & e.Error.Message)

        '    Else
        '        ''Microsoft.VisualBasic.MsgBox("Message Send")

        '    End If

        'End Sub
        Public Shared Function SendPlainTextEmails(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As String, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, ByVal HasAttachments As Boolean) As String
            Dim RetutnValue = ""
            Try

                Dim msg As New System.Net.Mail.MailMessage(FromEmailId, ToEmailId)

                msg.Subject = Subject

                msg.Body = MailBody

                msg.Priority = Net.Mail.MailPriority.High

                msg.IsBodyHtml = True

                '' If Attachments.

                If HasAttachments Then
                    Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString
                    Dim d As New DirectoryInfo(serverpath + Templateid + "/Attachments/")
                    Dim fi() As System.IO.FileInfo
                    fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fi.Length > 0 Then '' If Having Attachments files
                        For Each f As System.IO.FileInfo In fi
                            Dim attach As New System.Net.Mail.Attachment(serverpath + Templateid + "/Attachments/" + f.Name)
                            attach.Name = f.Name
                            msg.Attachments.Add(attach)
                        Next
                    End If
                End If

                Dim client As New System.Net.Mail.SmtpClient(Host, Port)

                If Username <> "" And password <> "" Then
                    Dim creds As New System.Net.NetworkCredential(Username, password)
                    client.Credentials = creds
                End If


                ' or for other authentication with local server

                'client.Credentials= system.Net.CredentialCache.DefaultCredentials

                'client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials

                ' Async send

                'Dim MailToken As String = My.User.Name

                'client.SendAsync(msg, emaildid)

                'AddHandler client.SendCompleted, AddressOf Me.MailSendComplete

                client.Send(msg)

                RetutnValue = "Successfully sent"

            Catch ex As Exception
                RetutnValue = "Error : " & ex.Message
            End Try

            Return RetutnValue

        End Function
    End Class
End Namespace